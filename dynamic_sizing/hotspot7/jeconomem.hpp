#ifndef JECONOMEM_H
#define JECONOMEM_H

class PSAdaptiveSizePolicy;

class JEconoMem
{
public:
    static size_t heap_size(PSAdaptiveSizePolicy* that);
    static void gc_start(PSAdaptiveSizePolicy* that);
    static void gc_end(PSAdaptiveSizePolicy* that);
    static void __attribute__ ((destructor)) daemon_proxy_cleanup();


    static void psasp_created(PSAdaptiveSizePolicy* that);
    static void psasp_major_collection_begin(PSAdaptiveSizePolicy* that);
    static void psasp_update_minor_pause_old_estimator(PSAdaptiveSizePolicy* that, double minor_pause_in_ms);
    static void psasp_major_collection_end(PSAdaptiveSizePolicy* that, size_t amount_live, GCCause::Cause gc_cause);
    static bool psasp_should_full_GC(PSAdaptiveSizePolicy* that, size_t live_in_old_gen);
    static void psasp_clear_generation_free_space_flags(PSAdaptiveSizePolicy* that);

    static void psasp_compute_generation_free_space(PSAdaptiveSizePolicy* that,
                                              size_t young_live,
                                              size_t eden_live,
                                              size_t old_live,
                                              size_t perm_live,
                                              size_t cur_eden,  // current eden in bytes
                                              size_t max_old_gen_size,
                                              size_t max_eden_size,
                                              bool   is_full_gc,
                                              GCCause::Cause gc_cause,
                                              CollectorPolicy* collector_policy);

    static void psasp_decay_supplemental_growth(PSAdaptiveSizePolicy* that, bool is_full_gc);

    static void psasp_adjust_for_minor_pause_time(PSAdaptiveSizePolicy* that, bool is_full_gc,
                                            size_t* desired_promo_size_ptr,
                                            size_t* desired_eden_size_ptr);

    static void psasp_adjust_for_pause_time(PSAdaptiveSizePolicy* that, bool is_full_gc,
                                      size_t* desired_promo_size_ptr,
                                      size_t* desired_eden_size_ptr);

    static void psasp_adjust_for_throughput(PSAdaptiveSizePolicy* that, bool is_full_gc,
                                      size_t* desired_promo_size_ptr,
                                      size_t* desired_eden_size_ptr);

    static size_t psasp_adjust_promo_for_footprint(PSAdaptiveSizePolicy* that, size_t desired_promo_size,
                                             size_t desired_total);

    static size_t psasp_adjust_eden_for_footprint(PSAdaptiveSizePolicy* that, size_t desired_promo_size,
                                            size_t desired_total);

    static size_t psasp_scale_down(PSAdaptiveSizePolicy* that, size_t change, double part, double total);

    static size_t psasp_eden_increment(PSAdaptiveSizePolicy* that, size_t cur_eden, uint percent_change);
    static size_t psasp_eden_increment(PSAdaptiveSizePolicy* that, size_t cur_eden);

    static size_t psasp_eden_increment_aligned_up(PSAdaptiveSizePolicy* that, size_t cur_eden);
    static size_t psasp_eden_increment_aligned_down(PSAdaptiveSizePolicy* that, size_t cur_eden);
    static size_t psasp_eden_increment_with_supplement_aligned_up(PSAdaptiveSizePolicy* that, size_t cur_eden);
    static size_t psasp_eden_decrement_aligned_down(PSAdaptiveSizePolicy* that, size_t cur_eden);
    static size_t psasp_eden_decrement(PSAdaptiveSizePolicy* that, size_t cur_eden);
    static size_t psasp_promo_increment(PSAdaptiveSizePolicy* that, size_t cur_promo, uint percent_change);
    static size_t psasp_promo_increment(PSAdaptiveSizePolicy* that, size_t cur_promo);
    static size_t psasp_promo_increment_aligned_up(PSAdaptiveSizePolicy* that, size_t cur_promo);
    static size_t psasp_promo_increment_aligned_down(PSAdaptiveSizePolicy* that, size_t cur_promo);
    static size_t psasp_promo_increment_with_supplement_aligned_up(PSAdaptiveSizePolicy* that, size_t cur_promo);
    static size_t psasp_promo_decrement_aligned_down(PSAdaptiveSizePolicy* that, size_t cur_promo);
    static size_t psasp_promo_decrement(PSAdaptiveSizePolicy* that, size_t cur_promo);

    static int psasp_compute_survivor_space_size_and_threshold(PSAdaptiveSizePolicy* that,
                                                         bool   is_survivor_overflow,
                                                         int    tenuring_threshold,
                                                         size_t survivor_limit);

    static void psasp_update_averages(PSAdaptiveSizePolicy* that, bool is_survivor_overflow,
                                size_t survived,
                                size_t promoted);

    static bool psasp_print_adaptive_size_policy_on(const PSAdaptiveSizePolicy* that, outputStream* st);

    /////////////////////////////////////////////////////////////////////

    static bool psScavenge_invoke();
    static bool psScavenge_invoke_no_policy();
    static void psMarkSweep_invoke(bool maximum_heap_compaction);
    static bool psMarkSweep_invoke_no_policy(bool maximum_heap_compaction);
    static void psParallelCompact_invoke(bool maximum_heap_compaction);
    static bool psParallelCompact_invoke_no_policy(bool maximum_heap_compaction);

    /////////////////////////////////////////////////////////////////////

    static void asp_create(AdaptiveSizePolicy* that);
    static int asp_calc_default_active_workers(uintx total_workers,
                                               const uintx min_workers,
                                               uintx active_workers,
                                               uintx application_workers);
    static int asp_calc_active_workers(uintx total_workers,
                                       uintx active_workers,
                                       uintx application_workers);
    static int asp_calc_active_conc_workers(uintx total_workers,
                                            uintx active_workers,
                                            uintx application_workers);
    static bool asp_tenuring_threshold_change(const AdaptiveSizePolicy* that);
    static void asp_minor_collection_begin(AdaptiveSizePolicy* that);
    static void asp_update_minor_pause_young_estimator(AdaptiveSizePolicy* that,
                                                       double minor_pause_in_ms);
    static void asp_minor_collection_end(AdaptiveSizePolicy* that, GCCause::Cause gc_cause);
    static size_t asp_eden_increment(AdaptiveSizePolicy* that,
                                     size_t cur_eden,
                                     uint percent_change);
    static size_t asp_eden_increment(AdaptiveSizePolicy* that, size_t cur_eden);
    static size_t asp_eden_decrement(AdaptiveSizePolicy* that, size_t cur_eden);
    static size_t asp_promo_increment(AdaptiveSizePolicy* that,
                                      size_t cur_promo,
                                      uint percent_change);
    static size_t asp_promo_increment(AdaptiveSizePolicy* that, size_t cur_promo);
    static size_t asp_promo_decrement(AdaptiveSizePolicy* that, size_t cur_promo);
    static double asp_time_since_major_gc(const AdaptiveSizePolicy* that);
    static double asp_decaying_major_gc_cost(const AdaptiveSizePolicy* that);
    static double asp_decaying_gc_cost(const AdaptiveSizePolicy* that);
    static void asp_clear_generation_free_space_flags(AdaptiveSizePolicy* that);
    static void asp_check_gc_overhead_limit(AdaptiveSizePolicy* that,
                                            size_t young_live,
                                            size_t eden_live,
                                            size_t max_old_gen_size,
                                            size_t max_eden_size,
                                            bool   is_full_gc,
                                            GCCause::Cause gc_cause,
                                            CollectorPolicy* collector_policy);
    static bool asp_print_adaptive_size_policy_on(const AdaptiveSizePolicy* that, outputStream* st);
    static bool asp_print_adaptive_size_policy_on(const AdaptiveSizePolicy* that,
                                                  outputStream* st,
                                                  int tenuring_threshold_arg);



};

#endif
