#!/bin/bash
# This is a horrible, non-general hack done in the rush for the paper!

function usage()
{
    echo "Usage: $(basename "${0}") timer_comparison_many_dir"
    exit 1
}

test -z "${1}" && usage
test ! -d "${1}" && usage

DIR="$(readlink -f "${1}")"

i='1'

while true; do
    SUBDIR="${DIR}/sub${i}_timerComparisonSingle"
    test ! -d "${SUBDIR}" && break

    SUBDIR="${SUBDIR}/sub4_timeBenchmarkDaemon"
    test ! -d "${SUBDIR}" && break

    cd "${SUBDIR}"
    sort -r highest_total_mem_run* | head -n 1

    i=$(( i + 1 ))
done
