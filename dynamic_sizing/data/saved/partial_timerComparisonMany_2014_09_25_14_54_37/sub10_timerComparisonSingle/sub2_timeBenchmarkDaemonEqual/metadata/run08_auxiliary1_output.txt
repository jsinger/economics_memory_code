economemd
    total memory: 4876 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub10_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
TimerThread: starting
ReadingThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 1522: heap size 9 MB, throughput 0.988716
Clients: 1
Client 1522 has a minimum heap size of 8 MB
Reading from 1521: heap size 9 MB, throughput 0.99231
Clients: 2
Client 1521 has a minimum heap size of 1211 MB
Reading from 1521: heap size 9 MB, throughput 0.978621
Reading from 1522: heap size 9 MB, throughput 0.982351
Reading from 1521: heap size 9 MB, throughput 0.751521
Reading from 1522: heap size 11 MB, throughput 0.940354
Reading from 1521: heap size 9 MB, throughput 0.946187
Reading from 1522: heap size 11 MB, throughput 0.95292
Reading from 1521: heap size 11 MB, throughput 0.977532
Reading from 1522: heap size 15 MB, throughput 0.865628
Reading from 1521: heap size 11 MB, throughput 0.974488
Reading from 1522: heap size 20 MB, throughput 0.980767
Reading from 1521: heap size 17 MB, throughput 0.903408
Reading from 1522: heap size 23 MB, throughput 0.984962
Reading from 1522: heap size 29 MB, throughput 0.963253
Reading from 1522: heap size 29 MB, throughput 0.965801
Reading from 1522: heap size 29 MB, throughput 0.953057
Reading from 1522: heap size 32 MB, throughput 0.965474
Reading from 1521: heap size 17 MB, throughput 0.549983
Reading from 1522: heap size 32 MB, throughput 0.978256
Reading from 1522: heap size 36 MB, throughput 0.978538
Reading from 1521: heap size 30 MB, throughput 0.860262
Reading from 1522: heap size 36 MB, throughput 0.977374
Reading from 1522: heap size 39 MB, throughput 0.978514
Reading from 1521: heap size 31 MB, throughput 0.92893
Reading from 1522: heap size 40 MB, throughput 0.975835
Reading from 1522: heap size 46 MB, throughput 0.976397
Reading from 1522: heap size 46 MB, throughput 0.972566
Reading from 1522: heap size 50 MB, throughput 0.981923
Reading from 1521: heap size 34 MB, throughput 0.452697
Reading from 1522: heap size 50 MB, throughput 0.978387
Reading from 1521: heap size 47 MB, throughput 0.855059
Reading from 1522: heap size 54 MB, throughput 0.88678
Reading from 1522: heap size 55 MB, throughput 0.988853
Reading from 1522: heap size 62 MB, throughput 0.984182
Reading from 1521: heap size 49 MB, throughput 0.445042
Reading from 1522: heap size 62 MB, throughput 0.983912
Reading from 1521: heap size 64 MB, throughput 0.801732
Reading from 1521: heap size 69 MB, throughput 0.788427
Reading from 1522: heap size 68 MB, throughput 0.988435
Reading from 1522: heap size 69 MB, throughput 0.987487
Reading from 1522: heap size 74 MB, throughput 0.988515
Reading from 1521: heap size 70 MB, throughput 0.251891
Reading from 1521: heap size 97 MB, throughput 0.769035
Reading from 1522: heap size 74 MB, throughput 0.990583
Reading from 1522: heap size 78 MB, throughput 0.986835
Reading from 1522: heap size 79 MB, throughput 0.989629
Reading from 1521: heap size 98 MB, throughput 0.188842
Reading from 1522: heap size 83 MB, throughput 0.972473
Reading from 1521: heap size 129 MB, throughput 0.85967
Reading from 1521: heap size 129 MB, throughput 0.699715
Reading from 1522: heap size 83 MB, throughput 0.992001
Reading from 1521: heap size 130 MB, throughput 0.883711
Reading from 1522: heap size 87 MB, throughput 0.992524
Reading from 1521: heap size 133 MB, throughput 0.752063
Reading from 1522: heap size 88 MB, throughput 0.977171
Reading from 1522: heap size 92 MB, throughput 0.992489
Reading from 1521: heap size 136 MB, throughput 0.20187
Reading from 1522: heap size 92 MB, throughput 0.991164
Reading from 1521: heap size 174 MB, throughput 0.671447
Reading from 1521: heap size 179 MB, throughput 0.633351
Reading from 1522: heap size 95 MB, throughput 0.992699
Reading from 1521: heap size 180 MB, throughput 0.698276
Reading from 1522: heap size 96 MB, throughput 0.991867
Reading from 1521: heap size 189 MB, throughput 0.741036
Reading from 1522: heap size 99 MB, throughput 0.993084
Reading from 1522: heap size 99 MB, throughput 0.989382
Reading from 1521: heap size 191 MB, throughput 0.163884
Reading from 1522: heap size 103 MB, throughput 0.995187
Reading from 1521: heap size 242 MB, throughput 0.671002
Reading from 1522: heap size 103 MB, throughput 0.992486
Reading from 1521: heap size 244 MB, throughput 0.710982
Reading from 1522: heap size 106 MB, throughput 0.994929
Reading from 1521: heap size 247 MB, throughput 0.70043
Reading from 1522: heap size 106 MB, throughput 0.990243
Reading from 1521: heap size 252 MB, throughput 0.6825
Reading from 1522: heap size 109 MB, throughput 0.993552
Reading from 1521: heap size 261 MB, throughput 0.642033
Reading from 1522: heap size 109 MB, throughput 0.992009
Reading from 1521: heap size 266 MB, throughput 0.598077
Reading from 1522: heap size 111 MB, throughput 0.992098
Reading from 1521: heap size 270 MB, throughput 0.536297
Reading from 1522: heap size 112 MB, throughput 0.99573
Reading from 1521: heap size 279 MB, throughput 0.559597
Reading from 1522: heap size 114 MB, throughput 0.995548
Reading from 1522: heap size 114 MB, throughput 0.988603
Reading from 1522: heap size 117 MB, throughput 0.988585
Reading from 1521: heap size 286 MB, throughput 0.122455
Reading from 1522: heap size 117 MB, throughput 0.996366
Reading from 1521: heap size 344 MB, throughput 0.464648
Reading from 1522: heap size 120 MB, throughput 0.994321
Reading from 1521: heap size 352 MB, throughput 0.693928
Reading from 1522: heap size 120 MB, throughput 0.995967
Reading from 1522: heap size 123 MB, throughput 0.997741
Reading from 1522: heap size 123 MB, throughput 0.995795
Reading from 1521: heap size 355 MB, throughput 0.105339
Reading from 1522: heap size 125 MB, throughput 0.991861
Reading from 1521: heap size 407 MB, throughput 0.500087
Reading from 1521: heap size 355 MB, throughput 0.67026
Reading from 1522: heap size 125 MB, throughput 0.995193
Reading from 1521: heap size 408 MB, throughput 0.641224
Reading from 1522: heap size 127 MB, throughput 0.99589
Reading from 1521: heap size 409 MB, throughput 0.60684
Reading from 1521: heap size 412 MB, throughput 0.615187
Reading from 1522: heap size 127 MB, throughput 0.995233
Reading from 1521: heap size 418 MB, throughput 0.537257
Reading from 1522: heap size 129 MB, throughput 0.996131
Reading from 1521: heap size 421 MB, throughput 0.488909
Reading from 1522: heap size 129 MB, throughput 0.993887
Reading from 1521: heap size 433 MB, throughput 0.543368
Reading from 1521: heap size 438 MB, throughput 0.468356
Reading from 1522: heap size 131 MB, throughput 0.9886
Reading from 1521: heap size 448 MB, throughput 0.462392
Reading from 1522: heap size 131 MB, throughput 0.991243
Reading from 1522: heap size 134 MB, throughput 0.99534
Reading from 1522: heap size 134 MB, throughput 0.990586
Reading from 1522: heap size 137 MB, throughput 0.991987
Reading from 1522: heap size 137 MB, throughput 0.996728
Reading from 1521: heap size 454 MB, throughput 0.0731373
Reading from 1522: heap size 140 MB, throughput 0.993953
Reading from 1521: heap size 518 MB, throughput 0.35072
Reading from 1521: heap size 517 MB, throughput 0.520233
Reading from 1522: heap size 141 MB, throughput 0.995651
Equal recommendation: 2438 MB each
Reading from 1522: heap size 143 MB, throughput 0.9718
Reading from 1522: heap size 143 MB, throughput 0.99493
Reading from 1522: heap size 148 MB, throughput 0.994376
Reading from 1521: heap size 450 MB, throughput 0.0884837
Reading from 1522: heap size 148 MB, throughput 0.985627
Reading from 1521: heap size 574 MB, throughput 0.406298
Reading from 1521: heap size 581 MB, throughput 0.642237
Reading from 1522: heap size 152 MB, throughput 0.995809
Reading from 1521: heap size 572 MB, throughput 0.544252
Reading from 1522: heap size 152 MB, throughput 0.997599
Reading from 1521: heap size 577 MB, throughput 0.527744
Reading from 1522: heap size 155 MB, throughput 0.99538
Reading from 1521: heap size 576 MB, throughput 0.565595
Reading from 1521: heap size 580 MB, throughput 0.60296
Reading from 1522: heap size 156 MB, throughput 0.995527
Reading from 1521: heap size 585 MB, throughput 0.563481
Reading from 1522: heap size 159 MB, throughput 0.994828
Reading from 1522: heap size 159 MB, throughput 0.989565
Reading from 1522: heap size 162 MB, throughput 0.993345
Reading from 1522: heap size 162 MB, throughput 0.979762
Reading from 1521: heap size 590 MB, throughput 0.084116
Reading from 1522: heap size 166 MB, throughput 0.996689
Reading from 1521: heap size 663 MB, throughput 0.445507
Reading from 1522: heap size 166 MB, throughput 0.995056
Reading from 1521: heap size 669 MB, throughput 0.559391
Reading from 1522: heap size 170 MB, throughput 0.993668
Reading from 1522: heap size 170 MB, throughput 0.99275
Reading from 1521: heap size 671 MB, throughput 0.865283
Reading from 1522: heap size 174 MB, throughput 0.995741
Reading from 1522: heap size 174 MB, throughput 0.99482
Reading from 1521: heap size 674 MB, throughput 0.82082
Reading from 1522: heap size 177 MB, throughput 0.995915
Reading from 1522: heap size 178 MB, throughput 0.994952
Reading from 1521: heap size 673 MB, throughput 0.760973
Reading from 1522: heap size 181 MB, throughput 0.995568
Reading from 1522: heap size 181 MB, throughput 0.993542
Reading from 1522: heap size 185 MB, throughput 0.994672
Reading from 1521: heap size 689 MB, throughput 0.816221
Reading from 1522: heap size 185 MB, throughput 0.995664
Reading from 1521: heap size 695 MB, throughput 0.454237
Reading from 1521: heap size 710 MB, throughput 0.247161
Reading from 1522: heap size 188 MB, throughput 0.996758
Reading from 1521: heap size 720 MB, throughput 0.48047
Reading from 1522: heap size 188 MB, throughput 0.996521
Reading from 1522: heap size 191 MB, throughput 0.996302
Reading from 1522: heap size 191 MB, throughput 0.996155
Reading from 1522: heap size 194 MB, throughput 0.996859
Reading from 1521: heap size 724 MB, throughput 0.0186108
Reading from 1521: heap size 794 MB, throughput 0.19762
Reading from 1521: heap size 677 MB, throughput 0.304649
Reading from 1522: heap size 194 MB, throughput 0.997525
Reading from 1521: heap size 788 MB, throughput 0.306375
Reading from 1522: heap size 196 MB, throughput 0.996845
Reading from 1522: heap size 197 MB, throughput 0.995467
Reading from 1521: heap size 792 MB, throughput 0.0380908
Reading from 1522: heap size 199 MB, throughput 0.997787
Equal recommendation: 2438 MB each
Reading from 1522: heap size 199 MB, throughput 0.992874
Reading from 1521: heap size 877 MB, throughput 0.61985
Reading from 1522: heap size 201 MB, throughput 0.992024
Reading from 1522: heap size 201 MB, throughput 0.986873
Reading from 1522: heap size 205 MB, throughput 0.998077
Reading from 1521: heap size 877 MB, throughput 0.157637
Reading from 1521: heap size 981 MB, throughput 0.971476
Reading from 1521: heap size 985 MB, throughput 0.97299
Reading from 1521: heap size 993 MB, throughput 0.951303
Reading from 1522: heap size 205 MB, throughput 0.996393
Reading from 1521: heap size 995 MB, throughput 0.972849
Reading from 1521: heap size 990 MB, throughput 0.936711
Reading from 1522: heap size 208 MB, throughput 0.997292
Reading from 1521: heap size 768 MB, throughput 0.906568
Reading from 1521: heap size 978 MB, throughput 0.938942
Reading from 1522: heap size 208 MB, throughput 0.996536
Reading from 1521: heap size 773 MB, throughput 0.92345
Reading from 1522: heap size 211 MB, throughput 0.996911
Reading from 1521: heap size 962 MB, throughput 0.926338
Reading from 1521: heap size 781 MB, throughput 0.910457
Reading from 1522: heap size 211 MB, throughput 0.9973
Reading from 1521: heap size 945 MB, throughput 0.88154
Reading from 1522: heap size 214 MB, throughput 0.967045
Reading from 1521: heap size 815 MB, throughput 0.749554
Reading from 1521: heap size 931 MB, throughput 0.702329
Reading from 1522: heap size 217 MB, throughput 0.996168
Reading from 1521: heap size 829 MB, throughput 0.675237
Reading from 1521: heap size 921 MB, throughput 0.688788
Reading from 1522: heap size 221 MB, throughput 0.995873
Reading from 1521: heap size 834 MB, throughput 0.736652
Reading from 1521: heap size 915 MB, throughput 0.720925
Reading from 1522: heap size 221 MB, throughput 0.993687
Reading from 1521: heap size 921 MB, throughput 0.722882
Reading from 1521: heap size 910 MB, throughput 0.736378
Reading from 1521: heap size 916 MB, throughput 0.76011
Reading from 1522: heap size 226 MB, throughput 0.997848
Reading from 1521: heap size 904 MB, throughput 0.735851
Reading from 1521: heap size 910 MB, throughput 0.765938
Reading from 1522: heap size 226 MB, throughput 0.996321
Reading from 1521: heap size 899 MB, throughput 0.74935
Reading from 1522: heap size 231 MB, throughput 0.997539
Reading from 1521: heap size 905 MB, throughput 0.982559
Reading from 1522: heap size 231 MB, throughput 0.996787
Reading from 1522: heap size 236 MB, throughput 0.996859
Reading from 1522: heap size 236 MB, throughput 0.996307
Reading from 1522: heap size 241 MB, throughput 0.9969
Reading from 1521: heap size 905 MB, throughput 0.973298
Reading from 1522: heap size 241 MB, throughput 0.995562
Reading from 1521: heap size 908 MB, throughput 0.878732
Reading from 1521: heap size 905 MB, throughput 0.734498
Reading from 1522: heap size 246 MB, throughput 0.987484
Reading from 1521: heap size 910 MB, throughput 0.719997
Reading from 1521: heap size 917 MB, throughput 0.721345
Reading from 1522: heap size 246 MB, throughput 0.996841
Reading from 1521: heap size 920 MB, throughput 0.720593
Reading from 1521: heap size 928 MB, throughput 0.69228
Reading from 1522: heap size 253 MB, throughput 0.99817
Reading from 1521: heap size 929 MB, throughput 0.739774
Reading from 1521: heap size 938 MB, throughput 0.755772
Reading from 1522: heap size 254 MB, throughput 0.997657
Reading from 1521: heap size 938 MB, throughput 0.739895
Reading from 1521: heap size 945 MB, throughput 0.751324
Reading from 1522: heap size 260 MB, throughput 0.997946
Reading from 1521: heap size 946 MB, throughput 0.856688
Reading from 1522: heap size 260 MB, throughput 0.998224
Reading from 1521: heap size 947 MB, throughput 0.829267
Reading from 1521: heap size 951 MB, throughput 0.788225
Reading from 1522: heap size 265 MB, throughput 0.998119
Equal recommendation: 2438 MB each
Reading from 1521: heap size 954 MB, throughput 0.668853
Reading from 1521: heap size 956 MB, throughput 0.577225
Reading from 1522: heap size 265 MB, throughput 0.997363
Reading from 1521: heap size 986 MB, throughput 0.64065
Reading from 1521: heap size 988 MB, throughput 0.627179
Reading from 1522: heap size 270 MB, throughput 0.991533
Reading from 1521: heap size 1001 MB, throughput 0.635799
Reading from 1522: heap size 270 MB, throughput 0.996735
Reading from 1521: heap size 1003 MB, throughput 0.625831
Reading from 1521: heap size 1016 MB, throughput 0.630913
Reading from 1521: heap size 1019 MB, throughput 0.607197
Reading from 1522: heap size 276 MB, throughput 0.998521
Reading from 1521: heap size 1034 MB, throughput 0.607979
Reading from 1522: heap size 277 MB, throughput 0.99656
Reading from 1521: heap size 1037 MB, throughput 0.637568
Reading from 1521: heap size 1054 MB, throughput 0.600018
Reading from 1522: heap size 283 MB, throughput 0.998451
Reading from 1522: heap size 283 MB, throughput 0.996559
Reading from 1521: heap size 1057 MB, throughput 0.901272
Reading from 1522: heap size 288 MB, throughput 0.99591
Reading from 1522: heap size 288 MB, throughput 0.994119
Reading from 1522: heap size 295 MB, throughput 0.993852
Reading from 1522: heap size 295 MB, throughput 0.996148
Reading from 1522: heap size 303 MB, throughput 0.994384
Reading from 1521: heap size 1076 MB, throughput 0.951732
Reading from 1522: heap size 304 MB, throughput 0.996947
Reading from 1522: heap size 311 MB, throughput 0.996292
Reading from 1522: heap size 312 MB, throughput 0.995061
Reading from 1522: heap size 320 MB, throughput 0.988401
Reading from 1521: heap size 1079 MB, throughput 0.956032
Reading from 1522: heap size 320 MB, throughput 0.996418
Reading from 1522: heap size 330 MB, throughput 0.996781
Reading from 1522: heap size 331 MB, throughput 0.996167
Reading from 1521: heap size 1089 MB, throughput 0.949071
Reading from 1522: heap size 341 MB, throughput 0.997437
Equal recommendation: 2438 MB each
Reading from 1522: heap size 341 MB, throughput 0.996739
Reading from 1522: heap size 349 MB, throughput 0.996111
Reading from 1521: heap size 1093 MB, throughput 0.939266
Reading from 1522: heap size 349 MB, throughput 0.996241
Reading from 1522: heap size 358 MB, throughput 0.997829
Reading from 1522: heap size 358 MB, throughput 0.99779
Reading from 1522: heap size 365 MB, throughput 0.998569
Reading from 1521: heap size 1089 MB, throughput 0.940009
Reading from 1522: heap size 366 MB, throughput 0.996218
Reading from 1522: heap size 372 MB, throughput 0.997042
Reading from 1522: heap size 372 MB, throughput 0.996802
Reading from 1521: heap size 1096 MB, throughput 0.930877
Reading from 1522: heap size 379 MB, throughput 0.997637
Reading from 1522: heap size 380 MB, throughput 0.99707
Reading from 1522: heap size 387 MB, throughput 0.997347
Reading from 1522: heap size 387 MB, throughput 0.994072
Reading from 1521: heap size 1087 MB, throughput 0.942172
Reading from 1522: heap size 395 MB, throughput 0.997368
Reading from 1522: heap size 395 MB, throughput 0.997264
Reading from 1522: heap size 403 MB, throughput 0.992254
Reading from 1521: heap size 1094 MB, throughput 0.952645
Reading from 1522: heap size 403 MB, throughput 0.997414
Reading from 1522: heap size 412 MB, throughput 0.997209
Equal recommendation: 2438 MB each
Reading from 1522: heap size 413 MB, throughput 0.99615
Reading from 1521: heap size 1084 MB, throughput 0.943148
Reading from 1522: heap size 423 MB, throughput 0.997141
Reading from 1522: heap size 423 MB, throughput 0.997263
Reading from 1522: heap size 431 MB, throughput 0.997019
Reading from 1521: heap size 1090 MB, throughput 0.928685
Reading from 1522: heap size 432 MB, throughput 0.996325
Reading from 1522: heap size 442 MB, throughput 0.996925
Reading from 1522: heap size 442 MB, throughput 0.997397
Reading from 1521: heap size 1092 MB, throughput 0.929559
Reading from 1522: heap size 450 MB, throughput 0.997956
Reading from 1522: heap size 451 MB, throughput 0.99717
Reading from 1522: heap size 459 MB, throughput 0.996979
Reading from 1521: heap size 1093 MB, throughput 0.94021
Reading from 1522: heap size 459 MB, throughput 0.997283
Reading from 1522: heap size 467 MB, throughput 0.997684
Reading from 1522: heap size 467 MB, throughput 0.996564
Reading from 1521: heap size 1100 MB, throughput 0.939887
Reading from 1522: heap size 476 MB, throughput 0.998037
Equal recommendation: 2438 MB each
Reading from 1522: heap size 476 MB, throughput 0.996763
Reading from 1521: heap size 1101 MB, throughput 0.93392
Reading from 1522: heap size 484 MB, throughput 0.997359
Reading from 1522: heap size 484 MB, throughput 0.997723
Reading from 1522: heap size 492 MB, throughput 0.997225
Reading from 1522: heap size 492 MB, throughput 0.996535
Reading from 1522: heap size 501 MB, throughput 0.998463
Reading from 1521: heap size 1107 MB, throughput 0.468042
Reading from 1522: heap size 501 MB, throughput 0.998146
Reading from 1522: heap size 508 MB, throughput 0.997465
Reading from 1522: heap size 509 MB, throughput 0.997411
Reading from 1521: heap size 1195 MB, throughput 0.912244
Reading from 1522: heap size 518 MB, throughput 0.998
Reading from 1522: heap size 518 MB, throughput 0.998065
Reading from 1522: heap size 525 MB, throughput 0.998313
Reading from 1521: heap size 1205 MB, throughput 0.954123
Equal recommendation: 2438 MB each
Reading from 1522: heap size 525 MB, throughput 0.997491
Reading from 1522: heap size 533 MB, throughput 0.998081
Reading from 1521: heap size 1206 MB, throughput 0.955697
Reading from 1522: heap size 533 MB, throughput 0.997967
Reading from 1522: heap size 540 MB, throughput 0.997712
Reading from 1522: heap size 540 MB, throughput 0.997443
Reading from 1521: heap size 1207 MB, throughput 0.956423
Reading from 1522: heap size 548 MB, throughput 0.998316
Reading from 1522: heap size 548 MB, throughput 0.998216
Reading from 1522: heap size 556 MB, throughput 0.998045
Reading from 1521: heap size 1210 MB, throughput 0.953466
Reading from 1522: heap size 556 MB, throughput 0.99732
Reading from 1522: heap size 564 MB, throughput 0.998246
Reading from 1521: heap size 1207 MB, throughput 0.952157
Reading from 1522: heap size 564 MB, throughput 0.998334
Reading from 1522: heap size 572 MB, throughput 0.998206
Equal recommendation: 2438 MB each
Reading from 1522: heap size 572 MB, throughput 0.997375
Reading from 1521: heap size 1211 MB, throughput 0.954322
Reading from 1522: heap size 580 MB, throughput 0.998442
Reading from 1522: heap size 580 MB, throughput 0.997991
Reading from 1522: heap size 588 MB, throughput 0.998215
Reading from 1521: heap size 1207 MB, throughput 0.951523
Reading from 1522: heap size 588 MB, throughput 0.998202
Reading from 1522: heap size 596 MB, throughput 0.998168
Reading from 1522: heap size 596 MB, throughput 0.997834
Reading from 1521: heap size 1210 MB, throughput 0.946801
Reading from 1522: heap size 604 MB, throughput 0.998292
Reading from 1522: heap size 604 MB, throughput 0.997956
Reading from 1521: heap size 1216 MB, throughput 0.952107
Reading from 1522: heap size 612 MB, throughput 0.998558
Reading from 1522: heap size 612 MB, throughput 0.997851
Equal recommendation: 2438 MB each
Reading from 1522: heap size 620 MB, throughput 0.998039
Reading from 1521: heap size 1217 MB, throughput 0.953201
Reading from 1522: heap size 620 MB, throughput 0.998309
Reading from 1522: heap size 628 MB, throughput 0.998295
Reading from 1522: heap size 628 MB, throughput 0.99817
Reading from 1521: heap size 1223 MB, throughput 0.946635
Reading from 1522: heap size 636 MB, throughput 0.998649
Reading from 1522: heap size 636 MB, throughput 0.99805
Reading from 1521: heap size 1227 MB, throughput 0.94459
Reading from 1522: heap size 643 MB, throughput 0.998407
Reading from 1522: heap size 643 MB, throughput 0.9982
Reading from 1522: heap size 651 MB, throughput 0.998608
Reading from 1521: heap size 1234 MB, throughput 0.950904
Reading from 1522: heap size 651 MB, throughput 0.998433
Equal recommendation: 2438 MB each
Reading from 1522: heap size 659 MB, throughput 0.998274
Reading from 1521: heap size 1241 MB, throughput 0.934444
Reading from 1522: heap size 659 MB, throughput 0.998522
Reading from 1522: heap size 666 MB, throughput 0.997901
Reading from 1522: heap size 667 MB, throughput 0.998473
Reading from 1521: heap size 1249 MB, throughput 0.937801
Reading from 1522: heap size 674 MB, throughput 0.998613
Reading from 1522: heap size 674 MB, throughput 0.997979
Reading from 1522: heap size 682 MB, throughput 0.998492
Reading from 1521: heap size 1256 MB, throughput 0.943796
Reading from 1522: heap size 682 MB, throughput 0.998591
Reading from 1522: heap size 690 MB, throughput 0.998386
Reading from 1521: heap size 1267 MB, throughput 0.939698
Equal recommendation: 2438 MB each
Reading from 1522: heap size 690 MB, throughput 0.998257
Reading from 1522: heap size 698 MB, throughput 0.998662
Reading from 1522: heap size 698 MB, throughput 0.998241
Reading from 1521: heap size 1272 MB, throughput 0.947112
Reading from 1522: heap size 706 MB, throughput 0.998739
Reading from 1522: heap size 706 MB, throughput 0.998292
Reading from 1522: heap size 713 MB, throughput 0.997166
Reading from 1522: heap size 713 MB, throughput 0.996007
Reading from 1521: heap size 1281 MB, throughput 0.514328
Reading from 1522: heap size 723 MB, throughput 0.998784
Reading from 1522: heap size 723 MB, throughput 0.998415
Equal recommendation: 2438 MB each
Reading from 1521: heap size 1385 MB, throughput 0.91873
Reading from 1522: heap size 733 MB, throughput 0.998625
Reading from 1522: heap size 734 MB, throughput 0.998235
Reading from 1522: heap size 742 MB, throughput 0.998739
Reading from 1521: heap size 1397 MB, throughput 0.959857
Reading from 1522: heap size 742 MB, throughput 0.998732
Reading from 1522: heap size 751 MB, throughput 0.998321
Reading from 1521: heap size 1397 MB, throughput 0.954436
Reading from 1522: heap size 751 MB, throughput 0.998401
Reading from 1522: heap size 759 MB, throughput 0.99879
Reading from 1521: heap size 1396 MB, throughput 0.960828
Reading from 1522: heap size 759 MB, throughput 0.998589
Equal recommendation: 2438 MB each
Reading from 1522: heap size 767 MB, throughput 0.998892
Reading from 1522: heap size 767 MB, throughput 0.998742
Reading from 1521: heap size 1401 MB, throughput 0.964447
Reading from 1522: heap size 775 MB, throughput 0.998369
Reading from 1522: heap size 775 MB, throughput 0.998918
Reading from 1522: heap size 782 MB, throughput 0.998532
Reading from 1522: heap size 783 MB, throughput 0.998647
Reading from 1522: heap size 790 MB, throughput 0.998769
Reading from 1522: heap size 790 MB, throughput 0.998419
Equal recommendation: 2438 MB each
Reading from 1522: heap size 797 MB, throughput 0.999006
Reading from 1522: heap size 797 MB, throughput 0.998782
Reading from 1522: heap size 805 MB, throughput 0.998911
Reading from 1522: heap size 805 MB, throughput 0.998874
Reading from 1522: heap size 812 MB, throughput 0.998743
Reading from 1522: heap size 812 MB, throughput 0.998882
Reading from 1522: heap size 819 MB, throughput 0.99878
Reading from 1522: heap size 819 MB, throughput 0.998503
Equal recommendation: 2438 MB each
Reading from 1522: heap size 826 MB, throughput 0.999126
Reading from 1522: heap size 826 MB, throughput 0.999095
Reading from 1521: heap size 1394 MB, throughput 0.831323
Reading from 1522: heap size 832 MB, throughput 0.998952
Reading from 1522: heap size 833 MB, throughput 0.998913
Reading from 1522: heap size 839 MB, throughput 0.998717
Reading from 1522: heap size 839 MB, throughput 0.998891
Reading from 1522: heap size 846 MB, throughput 0.998941
Equal recommendation: 2438 MB each
Reading from 1522: heap size 846 MB, throughput 0.998824
Reading from 1522: heap size 853 MB, throughput 0.998777
Reading from 1522: heap size 853 MB, throughput 0.998707
Reading from 1522: heap size 860 MB, throughput 0.998961
Reading from 1522: heap size 860 MB, throughput 0.998289
Reading from 1522: heap size 867 MB, throughput 0.999008
Reading from 1521: heap size 1538 MB, throughput 0.9794
Reading from 1522: heap size 867 MB, throughput 0.99893
Reading from 1522: heap size 874 MB, throughput 0.998904
Equal recommendation: 2438 MB each
Reading from 1521: heap size 1570 MB, throughput 0.968986
Reading from 1522: heap size 874 MB, throughput 0.998687
Reading from 1521: heap size 1588 MB, throughput 0.739035
Reading from 1521: heap size 1568 MB, throughput 0.667497
Reading from 1521: heap size 1592 MB, throughput 0.637728
Reading from 1522: heap size 882 MB, throughput 0.998919
Reading from 1521: heap size 1623 MB, throughput 0.656662
Reading from 1521: heap size 1637 MB, throughput 0.667879
Reading from 1521: heap size 1676 MB, throughput 0.765039
Reading from 1522: heap size 882 MB, throughput 0.998831
Reading from 1522: heap size 889 MB, throughput 0.998576
Reading from 1521: heap size 1682 MB, throughput 0.956382
Reading from 1522: heap size 889 MB, throughput 0.998786
Reading from 1522: heap size 897 MB, throughput 0.99893
Reading from 1522: heap size 897 MB, throughput 0.998806
Reading from 1521: heap size 1691 MB, throughput 0.960792
Equal recommendation: 2438 MB each
Reading from 1522: heap size 905 MB, throughput 0.998915
Reading from 1522: heap size 905 MB, throughput 0.998871
Reading from 1521: heap size 1702 MB, throughput 0.965187
Reading from 1522: heap size 913 MB, throughput 0.998792
Reading from 1522: heap size 913 MB, throughput 0.998752
Reading from 1521: heap size 1703 MB, throughput 0.967677
Reading from 1522: heap size 921 MB, throughput 0.99892
Reading from 1522: heap size 921 MB, throughput 0.999042
Reading from 1522: heap size 928 MB, throughput 0.998813
Equal recommendation: 2438 MB each
Reading from 1521: heap size 1716 MB, throughput 0.959592
Reading from 1522: heap size 929 MB, throughput 0.9987
Reading from 1522: heap size 936 MB, throughput 0.999121
Reading from 1521: heap size 1712 MB, throughput 0.958381
Reading from 1522: heap size 936 MB, throughput 0.998883
Reading from 1522: heap size 943 MB, throughput 0.999074
Reading from 1522: heap size 943 MB, throughput 0.999142
Reading from 1521: heap size 1723 MB, throughput 0.96228
Reading from 1522: heap size 950 MB, throughput 0.998527
Reading from 1522: heap size 951 MB, throughput 0.99883
Equal recommendation: 2438 MB each
Reading from 1521: heap size 1718 MB, throughput 0.957268
Reading from 1522: heap size 958 MB, throughput 0.998964
Reading from 1522: heap size 958 MB, throughput 0.9988
Reading from 1522: heap size 966 MB, throughput 0.99892
Reading from 1521: heap size 1728 MB, throughput 0.964989
Reading from 1522: heap size 966 MB, throughput 0.999008
Reading from 1522: heap size 973 MB, throughput 0.99894
Reading from 1522: heap size 974 MB, throughput 0.999008
Reading from 1521: heap size 1735 MB, throughput 0.959081
Equal recommendation: 2438 MB each
Reading from 1522: heap size 981 MB, throughput 0.998912
Reading from 1522: heap size 981 MB, throughput 0.998997
Reading from 1522: heap size 988 MB, throughput 0.998591
Reading from 1521: heap size 1741 MB, throughput 0.966035
Reading from 1522: heap size 988 MB, throughput 0.999049
Reading from 1522: heap size 996 MB, throughput 0.999138
Reading from 1521: heap size 1734 MB, throughput 0.962707
Reading from 1522: heap size 996 MB, throughput 0.998797
Reading from 1522: heap size 1004 MB, throughput 0.998909
Equal recommendation: 2438 MB each
Reading from 1522: heap size 1004 MB, throughput 0.998847
Reading from 1521: heap size 1743 MB, throughput 0.959836
Reading from 1522: heap size 1012 MB, throughput 0.999164
Reading from 1522: heap size 1012 MB, throughput 0.999022
Reading from 1522: heap size 1019 MB, throughput 0.998899
Reading from 1521: heap size 1754 MB, throughput 0.960315
Reading from 1522: heap size 1019 MB, throughput 0.998995
Reading from 1522: heap size 1027 MB, throughput 0.998919
Equal recommendation: 2438 MB each
Reading from 1521: heap size 1757 MB, throughput 0.951353
Reading from 1522: heap size 1027 MB, throughput 0.999104
Reading from 1522: heap size 1034 MB, throughput 0.998879
Reading from 1522: heap size 1035 MB, throughput 0.998994
Reading from 1521: heap size 1770 MB, throughput 0.947214
Reading from 1522: heap size 1042 MB, throughput 0.999159
Reading from 1522: heap size 1042 MB, throughput 0.999098
Reading from 1522: heap size 1050 MB, throughput 0.999078
Equal recommendation: 2438 MB each
Reading from 1521: heap size 1779 MB, throughput 0.731027
Reading from 1522: heap size 1050 MB, throughput 0.999148
Reading from 1522: heap size 1057 MB, throughput 0.998863
Reading from 1522: heap size 1058 MB, throughput 0.998919
Reading from 1521: heap size 1766 MB, throughput 0.988964
Reading from 1522: heap size 1064 MB, throughput 0.999309
Reading from 1522: heap size 1064 MB, throughput 0.998916
Reading from 1522: heap size 1071 MB, throughput 0.998991
Reading from 1521: heap size 1775 MB, throughput 0.984185
Equal recommendation: 2438 MB each
Reading from 1522: heap size 1071 MB, throughput 0.999151
Reading from 1522: heap size 1079 MB, throughput 0.998788
Reading from 1521: heap size 1801 MB, throughput 0.984553
Reading from 1522: heap size 1079 MB, throughput 0.999235
Reading from 1522: heap size 1086 MB, throughput 0.998933
Reading from 1522: heap size 1087 MB, throughput 0.999015
Reading from 1521: heap size 1807 MB, throughput 0.977571
Reading from 1522: heap size 1094 MB, throughput 0.999027
Equal recommendation: 2438 MB each
Reading from 1522: heap size 1094 MB, throughput 0.998903
Reading from 1522: heap size 1102 MB, throughput 0.99908
Reading from 1521: heap size 1813 MB, throughput 0.976395
Reading from 1522: heap size 1102 MB, throughput 0.999113
Reading from 1522: heap size 1110 MB, throughput 0.999053
Reading from 1522: heap size 1110 MB, throughput 0.999136
Reading from 1521: heap size 1818 MB, throughput 0.970703
Reading from 1522: heap size 1118 MB, throughput 0.999232
Equal recommendation: 2438 MB each
Reading from 1522: heap size 1118 MB, throughput 0.998946
Reading from 1522: heap size 1125 MB, throughput 0.999303
Reading from 1522: heap size 1125 MB, throughput 0.999232
Reading from 1522: heap size 1132 MB, throughput 0.999083
Reading from 1522: heap size 1133 MB, throughput 0.998921
Equal recommendation: 2438 MB each
Reading from 1522: heap size 1139 MB, throughput 0.999186
Reading from 1522: heap size 1139 MB, throughput 0.999161
Reading from 1522: heap size 1147 MB, throughput 0.999081
Reading from 1522: heap size 1147 MB, throughput 0.998963
Reading from 1521: heap size 1804 MB, throughput 0.987105
Reading from 1522: heap size 1154 MB, throughput 0.999149
Reading from 1522: heap size 1154 MB, throughput 0.99923
Equal recommendation: 2438 MB each
Reading from 1522: heap size 1162 MB, throughput 0.9992
Reading from 1522: heap size 1162 MB, throughput 0.999103
Reading from 1522: heap size 1169 MB, throughput 0.999092
Reading from 1522: heap size 1169 MB, throughput 0.999091
Reading from 1522: heap size 1177 MB, throughput 0.999081
Equal recommendation: 2438 MB each
Reading from 1522: heap size 1177 MB, throughput 0.999122
Reading from 1522: heap size 1184 MB, throughput 0.999045
Reading from 1522: heap size 1185 MB, throughput 0.99909
Reading from 1521: heap size 1814 MB, throughput 0.991234
Reading from 1522: heap size 1192 MB, throughput 0.999131
Reading from 1521: heap size 1837 MB, throughput 0.841362
Reading from 1521: heap size 1844 MB, throughput 0.769469
Reading from 1522: heap size 1192 MB, throughput 0.999034
Reading from 1521: heap size 1878 MB, throughput 0.775732
Client 1521 died
Clients: 1
Reading from 1522: heap size 1200 MB, throughput 0.99914
Recommendation: one client; give it all the memory
Reading from 1522: heap size 1200 MB, throughput 0.998773
Reading from 1522: heap size 1208 MB, throughput 0.999008
Reading from 1522: heap size 1208 MB, throughput 0.998839
Client 1522 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
