#ifndef LOG_HPP
#define LOG_HPP

#include <string>
#include "Mutex.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
    //! Thread-safe class for logging or otherwise printing readings and related events.
    class Log
    {
    public:
        //! Log to the given \p filename.
        Log(const std::string& filename, bool logReadings, bool logFunctions);

        //! Log to the given \p fd, which is _not_ closed when the log is destroyed.
        Log(FILE* fd, bool logReadings, bool logFunctions);

        ~Log();

        void logCreation(pid_t pid);
        void logReading(pid_t pid, unsigned long long heapSizeMB, double throughput);
        void logMinHeap(pid_t pid, unsigned long long heapSizeMB);
        void logFunction(pid_t pid, const std::string& cacheName, const std::string& func);
        void logRecommendation(pid_t pid, unsigned long long heapSizeMB);
        void logDeath(pid_t pid, unsigned long long totalReadingsReceived);
        void logTotalMemMB(unsigned int totalMemMB);

        bool logsReadings();
        void allowReadingLogging(bool allow);
        bool logsFunctions();
        void allowFunctionLogging(bool allow);

    private:
        void log(pid_t pid, const char* event) EXCLUSIVE_LOCKS_REQUIRED(m_Lock);
        void log(pid_t pid, const char* event, const char* fmt, ...) __attribute__((format (printf, 4, 5))) EXCLUSIVE_LOCKS_REQUIRED(m_Lock);

        economem::Mutex m_Lock;
        bool m_LogReadings GUARDED_BY(m_Lock);
        bool m_LogFunctions GUARDED_BY(m_Lock);
        const bool m_CloseFd;
        FILE* m_Fd GUARDED_BY(m_Lock);
    };
}

/*! @} */

#endif
