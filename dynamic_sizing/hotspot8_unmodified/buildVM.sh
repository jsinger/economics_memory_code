#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
VM_DIR="${DIR}/openjdk8"

cd "${VM_DIR}/jdk8"
ARCH="$(uname -m)"

if [ "${ARCH}" = 'x86_64' ]; then
    make
elif [ "${ARCH}" = 'i686' ]; then
    # Not entirely sure why this is needed, but it seems to work...
    DEBUG_BINARIES=true make
else
    echo "Don't know how to build for this architecture!"
    exit 1
fi
