economemd
    total memory: 4461 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub3_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
TimerThread: starting
ReadingThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 4765: heap size 9 MB, throughput 0.991904
Clients: 1
Client 4765 has a minimum heap size of 276 MB
Reading from 4764: heap size 9 MB, throughput 0.986146
Clients: 2
Client 4764 has a minimum heap size of 1211 MB
Reading from 4764: heap size 9 MB, throughput 0.96892
Reading from 4765: heap size 9 MB, throughput 0.975796
Reading from 4765: heap size 9 MB, throughput 0.961126
Reading from 4764: heap size 11 MB, throughput 0.974195
Reading from 4765: heap size 9 MB, throughput 0.946872
Reading from 4765: heap size 11 MB, throughput 0.943968
Reading from 4764: heap size 11 MB, throughput 0.983293
Reading from 4765: heap size 11 MB, throughput 0.959624
Reading from 4764: heap size 15 MB, throughput 0.867289
Reading from 4765: heap size 17 MB, throughput 0.947835
Reading from 4764: heap size 18 MB, throughput 0.890843
Reading from 4765: heap size 17 MB, throughput 0.526842
Reading from 4764: heap size 25 MB, throughput 0.812973
Reading from 4765: heap size 30 MB, throughput 0.928917
Reading from 4765: heap size 31 MB, throughput 0.847121
Reading from 4764: heap size 28 MB, throughput 0.485765
Reading from 4764: heap size 43 MB, throughput 0.893163
Reading from 4764: heap size 44 MB, throughput 0.79279
Reading from 4765: heap size 35 MB, throughput 0.500792
Reading from 4765: heap size 48 MB, throughput 0.840677
Reading from 4765: heap size 50 MB, throughput 0.910715
Reading from 4764: heap size 47 MB, throughput 0.393127
Reading from 4764: heap size 62 MB, throughput 0.727144
Reading from 4765: heap size 52 MB, throughput 0.326825
Reading from 4764: heap size 66 MB, throughput 0.747187
Reading from 4765: heap size 74 MB, throughput 0.832496
Reading from 4765: heap size 74 MB, throughput 0.271932
Reading from 4764: heap size 68 MB, throughput 0.25292
Reading from 4764: heap size 90 MB, throughput 0.742994
Reading from 4765: heap size 100 MB, throughput 0.722264
Reading from 4764: heap size 93 MB, throughput 0.85814
Reading from 4765: heap size 101 MB, throughput 0.882748
Reading from 4764: heap size 97 MB, throughput 0.745058
Reading from 4765: heap size 102 MB, throughput 0.75962
Reading from 4764: heap size 98 MB, throughput 0.167022
Reading from 4765: heap size 104 MB, throughput 0.234482
Reading from 4765: heap size 141 MB, throughput 0.782833
Reading from 4764: heap size 131 MB, throughput 0.603897
Reading from 4764: heap size 132 MB, throughput 0.747856
Reading from 4765: heap size 141 MB, throughput 0.817548
Reading from 4764: heap size 136 MB, throughput 0.71047
Reading from 4765: heap size 143 MB, throughput 0.775026
Reading from 4764: heap size 142 MB, throughput 0.718244
Reading from 4765: heap size 146 MB, throughput 0.775067
Reading from 4764: heap size 146 MB, throughput 0.723863
Reading from 4765: heap size 149 MB, throughput 0.755077
Reading from 4764: heap size 151 MB, throughput 0.596666
Reading from 4765: heap size 152 MB, throughput 0.119478
Reading from 4765: heap size 198 MB, throughput 0.602474
Reading from 4764: heap size 156 MB, throughput 0.0787524
Reading from 4765: heap size 200 MB, throughput 0.782934
Reading from 4764: heap size 196 MB, throughput 0.675755
Reading from 4764: heap size 203 MB, throughput 0.638384
Reading from 4765: heap size 202 MB, throughput 0.737533
Reading from 4764: heap size 206 MB, throughput 0.580223
Reading from 4764: heap size 209 MB, throughput 0.624826
Reading from 4764: heap size 216 MB, throughput 0.561871
Reading from 4765: heap size 208 MB, throughput 0.883911
Reading from 4764: heap size 222 MB, throughput 0.111211
Reading from 4764: heap size 265 MB, throughput 0.495343
Reading from 4764: heap size 269 MB, throughput 0.76249
Reading from 4765: heap size 208 MB, throughput 0.33897
Reading from 4764: heap size 272 MB, throughput 0.681548
Reading from 4765: heap size 262 MB, throughput 0.78357
Reading from 4765: heap size 262 MB, throughput 0.724033
Reading from 4764: heap size 274 MB, throughput 0.744335
Reading from 4765: heap size 264 MB, throughput 0.864078
Reading from 4764: heap size 277 MB, throughput 0.441695
Reading from 4765: heap size 262 MB, throughput 0.84636
Reading from 4764: heap size 279 MB, throughput 0.598485
Reading from 4765: heap size 264 MB, throughput 0.666895
Reading from 4765: heap size 265 MB, throughput 0.720453
Reading from 4765: heap size 265 MB, throughput 0.773871
Reading from 4765: heap size 266 MB, throughput 0.733034
Reading from 4764: heap size 286 MB, throughput 0.120842
Reading from 4765: heap size 269 MB, throughput 0.664062
Reading from 4765: heap size 275 MB, throughput 0.628804
Reading from 4764: heap size 334 MB, throughput 0.528127
Reading from 4765: heap size 277 MB, throughput 0.661148
Reading from 4764: heap size 339 MB, throughput 0.649865
Reading from 4765: heap size 282 MB, throughput 0.805303
Reading from 4764: heap size 342 MB, throughput 0.661134
Reading from 4764: heap size 343 MB, throughput 0.630234
Reading from 4765: heap size 283 MB, throughput 0.890543
Reading from 4765: heap size 287 MB, throughput 0.669866
Reading from 4764: heap size 347 MB, throughput 0.140388
Reading from 4765: heap size 287 MB, throughput 0.862081
Reading from 4765: heap size 288 MB, throughput 0.733965
Reading from 4764: heap size 393 MB, throughput 0.549242
Reading from 4765: heap size 290 MB, throughput 0.85394
Reading from 4764: heap size 398 MB, throughput 0.632073
Reading from 4765: heap size 292 MB, throughput 0.858865
Reading from 4764: heap size 400 MB, throughput 0.590615
Reading from 4765: heap size 294 MB, throughput 0.863077
Reading from 4764: heap size 403 MB, throughput 0.668172
Reading from 4765: heap size 299 MB, throughput 0.850558
Reading from 4764: heap size 409 MB, throughput 0.619766
Reading from 4765: heap size 299 MB, throughput 0.747189
Reading from 4765: heap size 304 MB, throughput 0.596621
Reading from 4764: heap size 414 MB, throughput 0.598327
Reading from 4765: heap size 306 MB, throughput 0.747535
Reading from 4764: heap size 421 MB, throughput 0.54147
Reading from 4765: heap size 313 MB, throughput 0.601125
Reading from 4765: heap size 315 MB, throughput 0.665157
Reading from 4764: heap size 426 MB, throughput 0.454713
Equal recommendation: 2230 MB each
Reading from 4764: heap size 433 MB, throughput 0.0656893
Reading from 4765: heap size 324 MB, throughput 0.978837
Reading from 4764: heap size 499 MB, throughput 0.456096
Reading from 4764: heap size 502 MB, throughput 0.465165
Reading from 4764: heap size 506 MB, throughput 0.563165
Reading from 4765: heap size 324 MB, throughput 0.950803
Reading from 4764: heap size 506 MB, throughput 0.117008
Reading from 4764: heap size 569 MB, throughput 0.469303
Reading from 4764: heap size 570 MB, throughput 0.509236
Reading from 4764: heap size 571 MB, throughput 0.543446
Reading from 4764: heap size 574 MB, throughput 0.497668
Reading from 4765: heap size 326 MB, throughput 0.502777
Reading from 4764: heap size 580 MB, throughput 0.107744
Reading from 4764: heap size 647 MB, throughput 0.540317
Reading from 4764: heap size 653 MB, throughput 0.641564
Reading from 4764: heap size 656 MB, throughput 0.608156
Reading from 4765: heap size 375 MB, throughput 0.967864
Reading from 4764: heap size 658 MB, throughput 0.817599
Reading from 4764: heap size 666 MB, throughput 0.860504
Reading from 4765: heap size 380 MB, throughput 0.987998
Reading from 4764: heap size 667 MB, throughput 0.833257
Reading from 4764: heap size 685 MB, throughput 0.684254
Reading from 4765: heap size 381 MB, throughput 0.983671
Reading from 4764: heap size 696 MB, throughput 0.0304182
Reading from 4764: heap size 775 MB, throughput 0.530474
Reading from 4764: heap size 784 MB, throughput 0.33818
Equal recommendation: 2230 MB each
Reading from 4764: heap size 785 MB, throughput 0.33245
Reading from 4765: heap size 384 MB, throughput 0.978896
Reading from 4764: heap size 770 MB, throughput 0.0551322
Reading from 4764: heap size 855 MB, throughput 0.295563
Reading from 4764: heap size 856 MB, throughput 0.868053
Reading from 4764: heap size 856 MB, throughput 0.590504
Reading from 4764: heap size 859 MB, throughput 0.670868
Reading from 4764: heap size 860 MB, throughput 0.60157
Reading from 4765: heap size 385 MB, throughput 0.988187
Reading from 4764: heap size 861 MB, throughput 0.878112
Reading from 4765: heap size 382 MB, throughput 0.976562
Reading from 4764: heap size 863 MB, throughput 0.107785
Reading from 4764: heap size 948 MB, throughput 0.514793
Reading from 4765: heap size 385 MB, throughput 0.96279
Reading from 4764: heap size 949 MB, throughput 0.906364
Reading from 4764: heap size 954 MB, throughput 0.92869
Reading from 4764: heap size 957 MB, throughput 0.919815
Reading from 4764: heap size 943 MB, throughput 0.802623
Reading from 4764: heap size 785 MB, throughput 0.814143
Reading from 4764: heap size 930 MB, throughput 0.865717
Reading from 4765: heap size 383 MB, throughput 0.983553
Reading from 4764: heap size 790 MB, throughput 0.809171
Reading from 4764: heap size 919 MB, throughput 0.872512
Reading from 4764: heap size 796 MB, throughput 0.820605
Reading from 4764: heap size 910 MB, throughput 0.860941
Reading from 4764: heap size 795 MB, throughput 0.861459
Reading from 4764: heap size 902 MB, throughput 0.866718
Reading from 4765: heap size 384 MB, throughput 0.980899
Reading from 4764: heap size 801 MB, throughput 0.857585
Reading from 4765: heap size 386 MB, throughput 0.979757
Reading from 4764: heap size 898 MB, throughput 0.977127
Reading from 4764: heap size 807 MB, throughput 0.895182
Reading from 4764: heap size 894 MB, throughput 0.77193
Equal recommendation: 2230 MB each
Reading from 4764: heap size 898 MB, throughput 0.799762
Reading from 4764: heap size 891 MB, throughput 0.768316
Reading from 4765: heap size 387 MB, throughput 0.97592
Reading from 4764: heap size 895 MB, throughput 0.80142
Reading from 4764: heap size 891 MB, throughput 0.79936
Reading from 4764: heap size 895 MB, throughput 0.748489
Reading from 4764: heap size 893 MB, throughput 0.819902
Reading from 4764: heap size 896 MB, throughput 0.899383
Reading from 4764: heap size 898 MB, throughput 0.869737
Reading from 4765: heap size 388 MB, throughput 0.984076
Reading from 4764: heap size 900 MB, throughput 0.884569
Reading from 4765: heap size 391 MB, throughput 0.874665
Reading from 4765: heap size 386 MB, throughput 0.788958
Reading from 4765: heap size 394 MB, throughput 0.648955
Reading from 4765: heap size 400 MB, throughput 0.756782
Reading from 4765: heap size 404 MB, throughput 0.913273
Reading from 4764: heap size 904 MB, throughput 0.0832985
Reading from 4764: heap size 994 MB, throughput 0.398065
Reading from 4765: heap size 410 MB, throughput 0.971354
Reading from 4764: heap size 1015 MB, throughput 0.629902
Reading from 4764: heap size 1023 MB, throughput 0.696875
Reading from 4764: heap size 1033 MB, throughput 0.680773
Reading from 4764: heap size 1037 MB, throughput 0.700349
Reading from 4764: heap size 1038 MB, throughput 0.706535
Reading from 4764: heap size 1042 MB, throughput 0.747264
Reading from 4764: heap size 1051 MB, throughput 0.721305
Reading from 4765: heap size 412 MB, throughput 0.986854
Reading from 4764: heap size 1053 MB, throughput 0.694988
Reading from 4764: heap size 1069 MB, throughput 0.707563
Reading from 4765: heap size 412 MB, throughput 0.977546
Reading from 4764: heap size 1069 MB, throughput 0.961832
Reading from 4765: heap size 415 MB, throughput 0.980043
Equal recommendation: 2230 MB each
Reading from 4764: heap size 1091 MB, throughput 0.953494
Reading from 4765: heap size 412 MB, throughput 0.980938
Reading from 4765: heap size 415 MB, throughput 0.982811
Reading from 4764: heap size 1093 MB, throughput 0.951449
Reading from 4765: heap size 412 MB, throughput 0.97701
Reading from 4764: heap size 1102 MB, throughput 0.965673
Reading from 4765: heap size 415 MB, throughput 0.983203
Reading from 4764: heap size 1105 MB, throughput 0.949758
Reading from 4765: heap size 414 MB, throughput 0.984498
Reading from 4764: heap size 1101 MB, throughput 0.952844
Reading from 4765: heap size 416 MB, throughput 0.980647
Equal recommendation: 2230 MB each
Reading from 4764: heap size 1108 MB, throughput 0.937858
Reading from 4765: heap size 416 MB, throughput 0.979879
Reading from 4765: heap size 417 MB, throughput 0.986551
Reading from 4764: heap size 1107 MB, throughput 0.955684
Reading from 4765: heap size 419 MB, throughput 0.893553
Reading from 4765: heap size 420 MB, throughput 0.837465
Reading from 4765: heap size 425 MB, throughput 0.860021
Reading from 4765: heap size 427 MB, throughput 0.982876
Reading from 4764: heap size 1110 MB, throughput 0.951433
Reading from 4765: heap size 434 MB, throughput 0.987139
Reading from 4764: heap size 1117 MB, throughput 0.950099
Reading from 4765: heap size 435 MB, throughput 0.987498
Equal recommendation: 2230 MB each
Reading from 4764: heap size 1119 MB, throughput 0.950514
Reading from 4765: heap size 437 MB, throughput 0.98704
Reading from 4765: heap size 438 MB, throughput 0.965502
Reading from 4764: heap size 1126 MB, throughput 0.945442
Reading from 4765: heap size 438 MB, throughput 0.986594
Reading from 4764: heap size 1129 MB, throughput 0.957249
Reading from 4765: heap size 439 MB, throughput 0.982905
Reading from 4764: heap size 1137 MB, throughput 0.955117
Reading from 4765: heap size 438 MB, throughput 0.985361
Reading from 4764: heap size 1142 MB, throughput 0.950811
Reading from 4765: heap size 439 MB, throughput 0.980619
Equal recommendation: 2230 MB each
Reading from 4764: heap size 1148 MB, throughput 0.949852
Reading from 4765: heap size 441 MB, throughput 0.991102
Reading from 4765: heap size 442 MB, throughput 0.976311
Reading from 4765: heap size 444 MB, throughput 0.880101
Reading from 4765: heap size 445 MB, throughput 0.865433
Reading from 4765: heap size 450 MB, throughput 0.974934
Reading from 4764: heap size 1155 MB, throughput 0.539499
Reading from 4765: heap size 451 MB, throughput 0.990533
Reading from 4764: heap size 1259 MB, throughput 0.932521
Reading from 4765: heap size 456 MB, throughput 0.988752
Equal recommendation: 2230 MB each
Reading from 4764: heap size 1263 MB, throughput 0.968766
Reading from 4765: heap size 457 MB, throughput 0.990019
Reading from 4764: heap size 1271 MB, throughput 0.975841
Reading from 4765: heap size 455 MB, throughput 0.988994
Reading from 4764: heap size 1272 MB, throughput 0.970163
Reading from 4765: heap size 458 MB, throughput 0.986975
Reading from 4765: heap size 456 MB, throughput 0.969393
Reading from 4764: heap size 1268 MB, throughput 0.9656
Reading from 4765: heap size 458 MB, throughput 0.984744
Equal recommendation: 2230 MB each
Reading from 4764: heap size 1272 MB, throughput 0.964354
Reading from 4765: heap size 460 MB, throughput 0.983172
Reading from 4764: heap size 1262 MB, throughput 0.965461
Reading from 4765: heap size 461 MB, throughput 0.984128
Reading from 4765: heap size 467 MB, throughput 0.893379
Reading from 4765: heap size 467 MB, throughput 0.876223
Reading from 4764: heap size 1268 MB, throughput 0.96477
Reading from 4765: heap size 473 MB, throughput 0.988804
Reading from 4765: heap size 474 MB, throughput 0.986169
Reading from 4764: heap size 1269 MB, throughput 0.95808
Equal recommendation: 2230 MB each
Reading from 4764: heap size 1270 MB, throughput 0.959913
Reading from 4765: heap size 477 MB, throughput 0.990823
Reading from 4765: heap size 479 MB, throughput 0.974453
Reading from 4764: heap size 1276 MB, throughput 0.957663
Reading from 4765: heap size 478 MB, throughput 0.987599
Reading from 4764: heap size 1279 MB, throughput 0.955986
Reading from 4765: heap size 480 MB, throughput 0.989608
Reading from 4764: heap size 1286 MB, throughput 0.961557
Equal recommendation: 2230 MB each
Reading from 4765: heap size 480 MB, throughput 0.987393
Reading from 4764: heap size 1291 MB, throughput 0.954574
Reading from 4765: heap size 481 MB, throughput 0.985377
Reading from 4765: heap size 480 MB, throughput 0.982071
Reading from 4764: heap size 1298 MB, throughput 0.958991
Reading from 4765: heap size 484 MB, throughput 0.920635
Reading from 4765: heap size 487 MB, throughput 0.922726
Reading from 4765: heap size 488 MB, throughput 0.992135
Reading from 4764: heap size 1305 MB, throughput 0.957745
Reading from 4765: heap size 492 MB, throughput 0.991399
Reading from 4764: heap size 1315 MB, throughput 0.958262
Equal recommendation: 2230 MB each
Reading from 4765: heap size 493 MB, throughput 0.991497
Reading from 4764: heap size 1321 MB, throughput 0.957838
Reading from 4765: heap size 493 MB, throughput 0.989214
Reading from 4764: heap size 1331 MB, throughput 0.953998
Reading from 4765: heap size 495 MB, throughput 0.987979
Reading from 4764: heap size 1335 MB, throughput 0.958901
Reading from 4765: heap size 494 MB, throughput 0.987614
Equal recommendation: 2230 MB each
Reading from 4765: heap size 495 MB, throughput 0.982188
Reading from 4764: heap size 1345 MB, throughput 0.557326
Reading from 4765: heap size 497 MB, throughput 0.990501
Reading from 4765: heap size 497 MB, throughput 0.886147
Reading from 4765: heap size 501 MB, throughput 0.930075
Reading from 4765: heap size 502 MB, throughput 0.9924
Equal recommendation: 2230 MB each
Reading from 4765: heap size 507 MB, throughput 0.992175
Reading from 4765: heap size 508 MB, throughput 0.99013
Reading from 4765: heap size 507 MB, throughput 0.990264
Reading from 4764: heap size 1458 MB, throughput 0.987328
Reading from 4765: heap size 509 MB, throughput 0.989611
Equal recommendation: 2230 MB each
Reading from 4765: heap size 509 MB, throughput 0.988289
Reading from 4765: heap size 510 MB, throughput 0.993488
Reading from 4765: heap size 512 MB, throughput 0.936982
Reading from 4765: heap size 513 MB, throughput 0.899818
Reading from 4765: heap size 517 MB, throughput 0.990117
Equal recommendation: 2230 MB each
Reading from 4765: heap size 519 MB, throughput 0.991062
Reading from 4765: heap size 520 MB, throughput 0.986839
Reading from 4764: heap size 1458 MB, throughput 0.99334
Reading from 4765: heap size 522 MB, throughput 0.991039
Reading from 4765: heap size 520 MB, throughput 0.988895
Equal recommendation: 2230 MB each
Reading from 4764: heap size 1463 MB, throughput 0.981958
Reading from 4764: heap size 1456 MB, throughput 0.802428
Reading from 4765: heap size 522 MB, throughput 0.980534
Reading from 4764: heap size 1478 MB, throughput 0.713024
Reading from 4764: heap size 1516 MB, throughput 0.732841
Reading from 4764: heap size 1530 MB, throughput 0.719722
Reading from 4764: heap size 1560 MB, throughput 0.749047
Reading from 4764: heap size 1569 MB, throughput 0.779039
Reading from 4765: heap size 525 MB, throughput 0.993496
Reading from 4765: heap size 525 MB, throughput 0.90259
Reading from 4765: heap size 525 MB, throughput 0.907583
Reading from 4764: heap size 1590 MB, throughput 0.962162
Reading from 4765: heap size 527 MB, throughput 0.99191
Reading from 4764: heap size 1599 MB, throughput 0.968578
Equal recommendation: 2230 MB each
Reading from 4765: heap size 532 MB, throughput 0.991302
Reading from 4764: heap size 1600 MB, throughput 0.971994
Reading from 4765: heap size 533 MB, throughput 0.992317
Reading from 4764: heap size 1612 MB, throughput 0.969664
Reading from 4765: heap size 533 MB, throughput 0.990855
Equal recommendation: 2230 MB each
Reading from 4764: heap size 1605 MB, throughput 0.973087
Reading from 4765: heap size 535 MB, throughput 0.989227
Reading from 4764: heap size 1618 MB, throughput 0.968104
Reading from 4765: heap size 538 MB, throughput 0.992888
Reading from 4765: heap size 538 MB, throughput 0.976895
Reading from 4765: heap size 538 MB, throughput 0.912646
Reading from 4764: heap size 1621 MB, throughput 0.970012
Reading from 4765: heap size 541 MB, throughput 0.990516
Equal recommendation: 2230 MB each
Reading from 4765: heap size 547 MB, throughput 0.992867
Reading from 4764: heap size 1627 MB, throughput 0.790927
Reading from 4765: heap size 548 MB, throughput 0.992559
Reading from 4764: heap size 1648 MB, throughput 0.991664
Reading from 4765: heap size 546 MB, throughput 0.990401
Equal recommendation: 2230 MB each
Reading from 4764: heap size 1651 MB, throughput 0.988628
Reading from 4765: heap size 549 MB, throughput 0.989539
Reading from 4765: heap size 551 MB, throughput 0.994304
Reading from 4764: heap size 1675 MB, throughput 0.985457
Reading from 4765: heap size 551 MB, throughput 0.977491
Reading from 4765: heap size 550 MB, throughput 0.916896
Reading from 4765: heap size 553 MB, throughput 0.991006
Reading from 4764: heap size 1684 MB, throughput 0.984474
Equal recommendation: 2230 MB each
Reading from 4765: heap size 558 MB, throughput 0.99292
Reading from 4764: heap size 1692 MB, throughput 0.979086
Reading from 4765: heap size 560 MB, throughput 0.991141
Reading from 4764: heap size 1696 MB, throughput 0.977631
Reading from 4765: heap size 559 MB, throughput 0.99017
Equal recommendation: 2230 MB each
Reading from 4765: heap size 561 MB, throughput 0.988796
Reading from 4764: heap size 1684 MB, throughput 0.977284
Reading from 4765: heap size 563 MB, throughput 0.993432
Reading from 4765: heap size 563 MB, throughput 0.942071
Reading from 4765: heap size 563 MB, throughput 0.970093
Reading from 4764: heap size 1693 MB, throughput 0.974784
Reading from 4765: heap size 565 MB, throughput 0.993082
Equal recommendation: 2230 MB each
Reading from 4764: heap size 1675 MB, throughput 0.974195
Reading from 4765: heap size 567 MB, throughput 0.992322
Reading from 4765: heap size 569 MB, throughput 0.990934
Reading from 4764: heap size 1684 MB, throughput 0.967215
Reading from 4765: heap size 567 MB, throughput 0.991438
Equal recommendation: 2230 MB each
Reading from 4764: heap size 1692 MB, throughput 0.972061
Reading from 4765: heap size 569 MB, throughput 0.989862
Reading from 4765: heap size 570 MB, throughput 0.988391
Reading from 4765: heap size 572 MB, throughput 0.917937
Reading from 4764: heap size 1694 MB, throughput 0.966578
Reading from 4765: heap size 575 MB, throughput 0.91159
Equal recommendation: 2230 MB each
Reading from 4764: heap size 1705 MB, throughput 0.964362
Reading from 4765: heap size 592 MB, throughput 0.996423
Reading from 4764: heap size 1714 MB, throughput 0.96661
Reading from 4765: heap size 594 MB, throughput 0.993856
Reading from 4765: heap size 597 MB, throughput 0.992409
Equal recommendation: 2230 MB each
Reading from 4764: heap size 1728 MB, throughput 0.960339
Reading from 4765: heap size 597 MB, throughput 0.991171
Reading from 4764: heap size 1742 MB, throughput 0.953435
Reading from 4765: heap size 599 MB, throughput 0.989728
Reading from 4765: heap size 596 MB, throughput 0.916919
Client 4765 died
Clients: 1
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 4764: heap size 1763 MB, throughput 0.995493
Reading from 4764: heap size 1776 MB, throughput 0.982033
Recommendation: one client; give it all the memory
Reading from 4764: heap size 1797 MB, throughput 0.839099
Reading from 4764: heap size 1802 MB, throughput 0.787583
Reading from 4764: heap size 1838 MB, throughput 0.826276
Client 4764 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
