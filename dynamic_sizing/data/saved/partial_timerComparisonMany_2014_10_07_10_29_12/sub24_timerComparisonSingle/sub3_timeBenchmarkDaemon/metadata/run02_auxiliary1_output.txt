economemd
    total memory: 3312 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub24_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 1150: heap size 9 MB, throughput 0.991773
Clients: 1
Client 1150 has a minimum heap size of 276 MB
Reading from 1149: heap size 9 MB, throughput 0.992025
Clients: 2
Client 1149 has a minimum heap size of 276 MB
Reading from 1150: heap size 9 MB, throughput 0.987509
Reading from 1149: heap size 9 MB, throughput 0.98891
Reading from 1149: heap size 9 MB, throughput 0.983399
Reading from 1150: heap size 9 MB, throughput 0.982901
Reading from 1149: heap size 9 MB, throughput 0.974719
Reading from 1150: heap size 9 MB, throughput 0.976329
Reading from 1150: heap size 11 MB, throughput 0.975374
Reading from 1149: heap size 11 MB, throughput 0.981389
Reading from 1149: heap size 11 MB, throughput 0.984019
Reading from 1150: heap size 11 MB, throughput 0.982625
Reading from 1149: heap size 17 MB, throughput 0.948811
Reading from 1150: heap size 17 MB, throughput 0.961658
Reading from 1150: heap size 17 MB, throughput 0.892804
Reading from 1149: heap size 17 MB, throughput 0.909318
Reading from 1149: heap size 30 MB, throughput 0.847798
Reading from 1150: heap size 30 MB, throughput 0.73541
Reading from 1150: heap size 31 MB, throughput 0.336799
Reading from 1149: heap size 31 MB, throughput 0.583115
Reading from 1150: heap size 35 MB, throughput 0.677598
Reading from 1150: heap size 48 MB, throughput 0.50289
Reading from 1150: heap size 50 MB, throughput 0.433292
Reading from 1149: heap size 34 MB, throughput 0.704977
Reading from 1149: heap size 46 MB, throughput 0.706883
Reading from 1149: heap size 49 MB, throughput 0.271585
Reading from 1150: heap size 52 MB, throughput 0.49766
Reading from 1150: heap size 76 MB, throughput 0.49777
Reading from 1149: heap size 50 MB, throughput 0.43625
Reading from 1149: heap size 68 MB, throughput 0.127792
Reading from 1150: heap size 76 MB, throughput 0.534828
Reading from 1149: heap size 70 MB, throughput 0.313149
Reading from 1150: heap size 103 MB, throughput 0.715636
Reading from 1150: heap size 104 MB, throughput 0.599628
Reading from 1149: heap size 74 MB, throughput 0.486938
Reading from 1150: heap size 106 MB, throughput 0.375675
Reading from 1149: heap size 94 MB, throughput 0.192763
Reading from 1149: heap size 102 MB, throughput 0.214743
Reading from 1150: heap size 110 MB, throughput 0.494832
Reading from 1149: heap size 103 MB, throughput 0.525581
Reading from 1150: heap size 137 MB, throughput 0.0740211
Reading from 1150: heap size 141 MB, throughput 0.118384
Reading from 1149: heap size 134 MB, throughput 0.447175
Reading from 1150: heap size 144 MB, throughput 0.735135
Reading from 1149: heap size 135 MB, throughput 0.161871
Reading from 1149: heap size 138 MB, throughput 0.779231
Reading from 1149: heap size 143 MB, throughput 0.728732
Reading from 1149: heap size 147 MB, throughput 0.737265
Reading from 1150: heap size 147 MB, throughput 0.330358
Reading from 1150: heap size 189 MB, throughput 0.00928951
Reading from 1150: heap size 190 MB, throughput 0.728492
Reading from 1149: heap size 151 MB, throughput 0.4318
Reading from 1150: heap size 192 MB, throughput 0.767034
Reading from 1149: heap size 193 MB, throughput 0.688185
Reading from 1150: heap size 198 MB, throughput 0.848076
Reading from 1149: heap size 197 MB, throughput 0.609425
Reading from 1149: heap size 200 MB, throughput 0.806584
Reading from 1150: heap size 201 MB, throughput 0.927755
Reading from 1149: heap size 206 MB, throughput 0.872278
Reading from 1150: heap size 207 MB, throughput 0.603135
Reading from 1150: heap size 259 MB, throughput 0.738006
Reading from 1149: heap size 209 MB, throughput 0.572613
Reading from 1149: heap size 258 MB, throughput 0.684298
Reading from 1150: heap size 261 MB, throughput 0.780202
Reading from 1150: heap size 264 MB, throughput 0.749131
Reading from 1149: heap size 263 MB, throughput 0.77684
Reading from 1149: heap size 264 MB, throughput 0.682692
Reading from 1150: heap size 265 MB, throughput 0.810845
Reading from 1149: heap size 268 MB, throughput 0.727685
Reading from 1150: heap size 269 MB, throughput 0.809209
Reading from 1149: heap size 268 MB, throughput 0.806324
Reading from 1150: heap size 270 MB, throughput 0.825926
Reading from 1149: heap size 266 MB, throughput 0.808782
Reading from 1150: heap size 270 MB, throughput 0.798768
Reading from 1149: heap size 268 MB, throughput 0.786234
Reading from 1150: heap size 275 MB, throughput 0.784598
Reading from 1149: heap size 274 MB, throughput 0.787257
Reading from 1150: heap size 280 MB, throughput 0.737548
Reading from 1149: heap size 275 MB, throughput 0.709241
Reading from 1149: heap size 282 MB, throughput 0.684481
Reading from 1150: heap size 283 MB, throughput 0.724174
Reading from 1150: heap size 289 MB, throughput 0.808509
Reading from 1149: heap size 282 MB, throughput 0.818922
Reading from 1149: heap size 283 MB, throughput 0.862241
Reading from 1150: heap size 290 MB, throughput 0.865431
Reading from 1150: heap size 289 MB, throughput 0.805548
Reading from 1149: heap size 286 MB, throughput 0.633654
Reading from 1149: heap size 329 MB, throughput 0.693386
Reading from 1149: heap size 330 MB, throughput 0.715219
Reading from 1150: heap size 290 MB, throughput 0.642986
Reading from 1149: heap size 337 MB, throughput 0.817442
Reading from 1150: heap size 339 MB, throughput 0.658428
Reading from 1149: heap size 338 MB, throughput 0.832342
Reading from 1150: heap size 339 MB, throughput 0.79423
Reading from 1149: heap size 344 MB, throughput 0.750429
Reading from 1149: heap size 345 MB, throughput 0.723745
Reading from 1150: heap size 345 MB, throughput 0.871991
Reading from 1149: heap size 349 MB, throughput 0.736021
Reading from 1150: heap size 345 MB, throughput 0.872056
Reading from 1149: heap size 350 MB, throughput 0.741382
Reading from 1150: heap size 345 MB, throughput 0.836854
Reading from 1150: heap size 348 MB, throughput 0.77822
Reading from 1149: heap size 357 MB, throughput 0.748955
Reading from 1150: heap size 345 MB, throughput 0.747099
Reading from 1150: heap size 348 MB, throughput 0.725994
Numeric result:
Recommendation: 2 clients, utility 0.457977:
    h1: 276 MB (U(h) = 0.61924*h^0.001)
    h2: 3036 MB (U(h) = 0.43332*h^0.0659721)
Recommendation: 2 clients, utility 0.457977:
    h1: 276 MB (U(h) = 0.61924*h^0.001)
    h2: 3036 MB (U(h) = 0.43332*h^0.0659721)
Reading from 1150: heap size 351 MB, throughput 0.698849
Reading from 1149: heap size 358 MB, throughput 0.954182
Reading from 1150: heap size 353 MB, throughput 0.947276
Reading from 1150: heap size 340 MB, throughput 0.966897
Reading from 1149: heap size 364 MB, throughput 0.966885
Reading from 1150: heap size 313 MB, throughput 0.967672
Reading from 1149: heap size 366 MB, throughput 0.964295
Reading from 1150: heap size 342 MB, throughput 0.972907
Reading from 1149: heap size 367 MB, throughput 0.970983
Reading from 1150: heap size 312 MB, throughput 0.971424
Reading from 1149: heap size 370 MB, throughput 0.974579
Reading from 1150: heap size 337 MB, throughput 0.963412
Reading from 1150: heap size 315 MB, throughput 0.96621
Reading from 1149: heap size 370 MB, throughput 0.976122
Numeric result:
Recommendation: 2 clients, utility 0.559452:
    h1: 1172.07 MB (U(h) = 0.447455*h^0.0576896)
    h2: 2139.93 MB (U(h) = 0.370836*h^0.105328)
Recommendation: 2 clients, utility 0.559452:
    h1: 1172.07 MB (U(h) = 0.447455*h^0.0576896)
    h2: 2139.93 MB (U(h) = 0.370836*h^0.105328)
Reading from 1150: heap size 332 MB, throughput 0.791691
Reading from 1149: heap size 373 MB, throughput 0.978827
Reading from 1150: heap size 334 MB, throughput 0.59746
Reading from 1150: heap size 336 MB, throughput 0.793341
Reading from 1149: heap size 372 MB, throughput 0.977922
Reading from 1150: heap size 336 MB, throughput 0.621675
Reading from 1149: heap size 374 MB, throughput 0.976803
Reading from 1150: heap size 338 MB, throughput 0.511174
Reading from 1150: heap size 339 MB, throughput 0.313485
Reading from 1149: heap size 378 MB, throughput 0.974968
Reading from 1150: heap size 341 MB, throughput 0.969015
Reading from 1149: heap size 379 MB, throughput 0.977418
Reading from 1150: heap size 342 MB, throughput 0.969029
Reading from 1150: heap size 344 MB, throughput 0.968214
Reading from 1149: heap size 381 MB, throughput 0.97543
Numeric result:
Recommendation: 2 clients, utility 0.653084:
    h1: 1090.77 MB (U(h) = 0.429875*h^0.0679999)
    h2: 2221.23 MB (U(h) = 0.324965*h^0.138417)
Recommendation: 2 clients, utility 0.653084:
    h1: 1091.07 MB (U(h) = 0.429875*h^0.0679999)
    h2: 2220.93 MB (U(h) = 0.324965*h^0.138417)
Reading from 1150: heap size 345 MB, throughput 0.970844
Reading from 1149: heap size 384 MB, throughput 0.98099
Reading from 1149: heap size 388 MB, throughput 0.963388
Reading from 1149: heap size 389 MB, throughput 0.94369
Reading from 1149: heap size 396 MB, throughput 0.907439
Reading from 1149: heap size 399 MB, throughput 0.931236
Reading from 1150: heap size 347 MB, throughput 0.978076
Reading from 1150: heap size 347 MB, throughput 0.968702
Reading from 1150: heap size 349 MB, throughput 0.946763
Reading from 1150: heap size 349 MB, throughput 0.908555
Reading from 1150: heap size 358 MB, throughput 0.87412
Reading from 1150: heap size 361 MB, throughput 0.843671
Reading from 1150: heap size 369 MB, throughput 0.921397
Reading from 1149: heap size 407 MB, throughput 0.972815
Reading from 1150: heap size 370 MB, throughput 0.977032
Reading from 1149: heap size 409 MB, throughput 0.981
Reading from 1150: heap size 374 MB, throughput 0.981779
Reading from 1149: heap size 410 MB, throughput 0.980686
Reading from 1150: heap size 376 MB, throughput 0.984241
Numeric result:
Recommendation: 2 clients, utility 0.822431:
    h1: 1324.6 MB (U(h) = 0.36308*h^0.110449)
    h2: 1987.4 MB (U(h) = 0.290862*h^0.165716)
Recommendation: 2 clients, utility 0.822431:
    h1: 1324.6 MB (U(h) = 0.36308*h^0.110449)
    h2: 1987.4 MB (U(h) = 0.290862*h^0.165716)
Reading from 1150: heap size 376 MB, throughput 0.983944
Reading from 1149: heap size 413 MB, throughput 0.983862
Reading from 1150: heap size 378 MB, throughput 0.950345
Reading from 1149: heap size 411 MB, throughput 0.984655
Reading from 1150: heap size 406 MB, throughput 0.990479
Reading from 1149: heap size 414 MB, throughput 0.984185
Reading from 1150: heap size 408 MB, throughput 0.991575
Reading from 1149: heap size 411 MB, throughput 0.984037
Reading from 1150: heap size 413 MB, throughput 0.991324
Reading from 1149: heap size 414 MB, throughput 0.978912
Reading from 1150: heap size 414 MB, throughput 0.990398
Numeric result:
Recommendation: 2 clients, utility 0.909618:
    h1: 1435.43 MB (U(h) = 0.330167*h^0.13339)
    h2: 1876.57 MB (U(h) = 0.280681*h^0.174378)
Recommendation: 2 clients, utility 0.909618:
    h1: 1435.46 MB (U(h) = 0.330167*h^0.13339)
    h2: 1876.54 MB (U(h) = 0.280681*h^0.174378)
Reading from 1150: heap size 414 MB, throughput 0.988831
Reading from 1149: heap size 414 MB, throughput 0.983109
Reading from 1150: heap size 415 MB, throughput 0.987273
Reading from 1149: heap size 415 MB, throughput 0.986505
Reading from 1150: heap size 410 MB, throughput 0.978743
Reading from 1149: heap size 416 MB, throughput 0.958338
Reading from 1149: heap size 453 MB, throughput 0.936987
Reading from 1149: heap size 456 MB, throughput 0.933542
Reading from 1150: heap size 413 MB, throughput 0.985691
Reading from 1149: heap size 458 MB, throughput 0.978418
Reading from 1150: heap size 410 MB, throughput 0.979562
Reading from 1150: heap size 411 MB, throughput 0.964676
Reading from 1150: heap size 416 MB, throughput 0.948444
Reading from 1150: heap size 421 MB, throughput 0.900818
Reading from 1150: heap size 429 MB, throughput 0.969426
Reading from 1149: heap size 464 MB, throughput 0.986765
Reading from 1150: heap size 432 MB, throughput 0.982502
Numeric result:
Recommendation: 2 clients, utility 1.0165:
    h1: 1505.58 MB (U(h) = 0.300556*h^0.155797)
    h2: 1806.42 MB (U(h) = 0.266235*h^0.186943)
Recommendation: 2 clients, utility 1.0165:
    h1: 1505.51 MB (U(h) = 0.300556*h^0.155797)
    h2: 1806.49 MB (U(h) = 0.266235*h^0.186943)
Reading from 1149: heap size 466 MB, throughput 0.983205
Reading from 1150: heap size 430 MB, throughput 0.985732
Reading from 1149: heap size 465 MB, throughput 0.9854
Reading from 1150: heap size 434 MB, throughput 0.985396
Reading from 1150: heap size 430 MB, throughput 0.986457
Reading from 1149: heap size 468 MB, throughput 0.983411
Reading from 1150: heap size 433 MB, throughput 0.987874
Reading from 1149: heap size 470 MB, throughput 0.981854
Reading from 1150: heap size 432 MB, throughput 0.985674
Reading from 1149: heap size 471 MB, throughput 0.982011
Numeric result:
Recommendation: 2 clients, utility 1.07546:
    h1: 1510.51 MB (U(h) = 0.289681*h^0.164483)
    h2: 1801.49 MB (U(h) = 0.255918*h^0.196171)
Recommendation: 2 clients, utility 1.07546:
    h1: 1510.5 MB (U(h) = 0.289681*h^0.164483)
    h2: 1801.5 MB (U(h) = 0.255918*h^0.196171)
Reading from 1150: heap size 434 MB, throughput 0.983829
Reading from 1149: heap size 474 MB, throughput 0.981624
Reading from 1150: heap size 436 MB, throughput 0.983622
Reading from 1150: heap size 436 MB, throughput 0.982517
Reading from 1149: heap size 477 MB, throughput 0.984661
Reading from 1149: heap size 485 MB, throughput 0.97275
Reading from 1149: heap size 485 MB, throughput 0.949226
Reading from 1149: heap size 495 MB, throughput 0.960707
Reading from 1150: heap size 439 MB, throughput 0.987692
Reading from 1150: heap size 439 MB, throughput 0.982154
Reading from 1150: heap size 439 MB, throughput 0.972252
Reading from 1150: heap size 441 MB, throughput 0.95391
Reading from 1150: heap size 448 MB, throughput 0.961735
Reading from 1149: heap size 501 MB, throughput 0.977364
Numeric result:
Recommendation: 2 clients, utility 1.13005:
    h1: 1527.64 MB (U(h) = 0.27846*h^0.17373)
    h2: 1784.36 MB (U(h) = 0.248562*h^0.202904)
Recommendation: 2 clients, utility 1.13005:
    h1: 1527.73 MB (U(h) = 0.27846*h^0.17373)
    h2: 1784.27 MB (U(h) = 0.248562*h^0.202904)
Reading from 1150: heap size 449 MB, throughput 0.983789
Reading from 1149: heap size 502 MB, throughput 0.982794
Reading from 1150: heap size 453 MB, throughput 0.989418
Reading from 1149: heap size 505 MB, throughput 0.986384
Reading from 1150: heap size 455 MB, throughput 0.988966
Reading from 1150: heap size 455 MB, throughput 0.989351
Reading from 1149: heap size 503 MB, throughput 0.986722
Reading from 1150: heap size 457 MB, throughput 0.988623
Reading from 1149: heap size 507 MB, throughput 0.987484
Numeric result:
Recommendation: 2 clients, utility 1.17822:
    h1: 1548.51 MB (U(h) = 0.268113*h^0.182502)
    h2: 1763.49 MB (U(h) = 0.243258*h^0.207825)
Recommendation: 2 clients, utility 1.17822:
    h1: 1548.57 MB (U(h) = 0.268113*h^0.182502)
    h2: 1763.43 MB (U(h) = 0.243258*h^0.207825)
Reading from 1150: heap size 455 MB, throughput 0.987481
Reading from 1149: heap size 507 MB, throughput 0.984706
Reading from 1150: heap size 457 MB, throughput 0.986843
Reading from 1150: heap size 459 MB, throughput 0.985583
Reading from 1149: heap size 509 MB, throughput 0.989476
Reading from 1149: heap size 511 MB, throughput 0.985217
Reading from 1149: heap size 512 MB, throughput 0.972761
Reading from 1149: heap size 517 MB, throughput 0.976067
Reading from 1150: heap size 459 MB, throughput 0.989591
Reading from 1150: heap size 461 MB, throughput 0.986589
Reading from 1150: heap size 462 MB, throughput 0.978101
Reading from 1150: heap size 466 MB, throughput 0.964221
Numeric result:
Recommendation: 2 clients, utility 1.21851:
    h1: 1564.96 MB (U(h) = 0.259876*h^0.189681)
    h2: 1747.04 MB (U(h) = 0.239082*h^0.211756)
Recommendation: 2 clients, utility 1.21851:
    h1: 1564.94 MB (U(h) = 0.259876*h^0.189681)
    h2: 1747.06 MB (U(h) = 0.239082*h^0.211756)
Reading from 1149: heap size 517 MB, throughput 0.989395
Reading from 1150: heap size 467 MB, throughput 0.987915
Reading from 1150: heap size 475 MB, throughput 0.990815
Reading from 1149: heap size 524 MB, throughput 0.990895
Reading from 1150: heap size 475 MB, throughput 0.989537
Reading from 1149: heap size 525 MB, throughput 0.990156
Reading from 1150: heap size 476 MB, throughput 0.989872
Reading from 1149: heap size 526 MB, throughput 0.989343
Numeric result:
Recommendation: 2 clients, utility 1.24785:
    h1: 1575.68 MB (U(h) = 0.254151*h^0.194763)
    h2: 1736.32 MB (U(h) = 0.236055*h^0.214626)
Recommendation: 2 clients, utility 1.24785:
    h1: 1575.65 MB (U(h) = 0.254151*h^0.194763)
    h2: 1736.35 MB (U(h) = 0.236055*h^0.214626)
Reading from 1150: heap size 478 MB, throughput 0.989603
Reading from 1149: heap size 528 MB, throughput 0.989017
Reading from 1150: heap size 476 MB, throughput 0.990765
Reading from 1149: heap size 526 MB, throughput 0.987564
Reading from 1150: heap size 479 MB, throughput 0.988485
Reading from 1149: heap size 528 MB, throughput 0.988094
Reading from 1149: heap size 531 MB, throughput 0.981536
Reading from 1150: heap size 481 MB, throughput 0.987081
Reading from 1149: heap size 532 MB, throughput 0.980283
Numeric result:
Recommendation: 2 clients, utility 1.27349:
    h1: 1586.01 MB (U(h) = 0.249101*h^0.199318)
    h2: 1725.99 MB (U(h) = 0.233667*h^0.216909)
Recommendation: 2 clients, utility 1.27349:
    h1: 1586.01 MB (U(h) = 0.249101*h^0.199318)
    h2: 1725.99 MB (U(h) = 0.233667*h^0.216909)
Reading from 1150: heap size 481 MB, throughput 0.988781
Reading from 1150: heap size 482 MB, throughput 0.980403
Reading from 1150: heap size 484 MB, throughput 0.970091
Reading from 1149: heap size 539 MB, throughput 0.990928
Reading from 1150: heap size 491 MB, throughput 0.984763
Reading from 1150: heap size 492 MB, throughput 0.989736
Reading from 1149: heap size 540 MB, throughput 0.990743
Reading from 1150: heap size 497 MB, throughput 0.991286
Reading from 1149: heap size 542 MB, throughput 0.990729
Numeric result:
Recommendation: 2 clients, utility 1.30617:
    h1: 1603.81 MB (U(h) = 0.242123*h^0.205717)
    h2: 1708.19 MB (U(h) = 0.231364*h^0.21912)
Recommendation: 2 clients, utility 1.30617:
    h1: 1603.76 MB (U(h) = 0.242123*h^0.205717)
    h2: 1708.24 MB (U(h) = 0.231364*h^0.21912)
Reading from 1150: heap size 498 MB, throughput 0.992214
Reading from 1149: heap size 544 MB, throughput 0.991139
Reading from 1150: heap size 497 MB, throughput 0.989809
Reading from 1149: heap size 542 MB, throughput 0.990657
Reading from 1150: heap size 500 MB, throughput 0.988418
Reading from 1150: heap size 502 MB, throughput 0.98742
Reading from 1149: heap size 544 MB, throughput 0.992852
Reading from 1149: heap size 546 MB, throughput 0.987928
Reading from 1149: heap size 547 MB, throughput 0.978661
Numeric result:
Recommendation: 2 clients, utility 1.92198:
    h1: 1331.79 MB (U(h) = 0.174232*h^0.263473)
    h2: 1980.21 MB (U(h) = 0.0847128*h^0.391744)
Recommendation: 2 clients, utility 1.92198:
    h1: 1331.81 MB (U(h) = 0.174232*h^0.263473)
    h2: 1980.19 MB (U(h) = 0.0847128*h^0.391744)
Reading from 1150: heap size 502 MB, throughput 0.990096
Reading from 1150: heap size 506 MB, throughput 0.981334
Reading from 1150: heap size 506 MB, throughput 0.970736
Reading from 1149: heap size 554 MB, throughput 0.986184
Reading from 1150: heap size 513 MB, throughput 0.985995
Reading from 1149: heap size 555 MB, throughput 0.990144
Reading from 1150: heap size 515 MB, throughput 0.989499
Reading from 1149: heap size 559 MB, throughput 0.989655
Numeric result:
Recommendation: 2 clients, utility 3.28296:
    h1: 1541.05 MB (U(h) = 0.0529399*h^0.468389)
    h2: 1770.95 MB (U(h) = 0.0355509*h^0.538295)
Recommendation: 2 clients, utility 3.28296:
    h1: 1541 MB (U(h) = 0.0529399*h^0.468389)
    h2: 1771 MB (U(h) = 0.0355509*h^0.538295)
Reading from 1150: heap size 518 MB, throughput 0.990535
Reading from 1149: heap size 561 MB, throughput 0.990064
Reading from 1150: heap size 521 MB, throughput 0.991401
Reading from 1149: heap size 560 MB, throughput 0.990604
Reading from 1150: heap size 520 MB, throughput 0.992107
Reading from 1150: heap size 523 MB, throughput 0.990394
Reading from 1149: heap size 563 MB, throughput 0.992401
Numeric result:
Recommendation: 2 clients, utility 4.93485:
    h1: 1682.9 MB (U(h) = 0.0178493*h^0.652561)
    h2: 1629.1 MB (U(h) = 0.0203005*h^0.631687)
Recommendation: 2 clients, utility 4.93485:
    h1: 1682.92 MB (U(h) = 0.0178493*h^0.652561)
    h2: 1629.08 MB (U(h) = 0.0203005*h^0.631687)
Reading from 1149: heap size 565 MB, throughput 0.989156
Reading from 1149: heap size 566 MB, throughput 0.985017
Reading from 1150: heap size 526 MB, throughput 0.993513
Reading from 1150: heap size 526 MB, throughput 0.987694
Reading from 1150: heap size 527 MB, throughput 0.981509
Reading from 1149: heap size 574 MB, throughput 0.992357
Reading from 1150: heap size 530 MB, throughput 0.987727
Reading from 1150: heap size 537 MB, throughput 0.992642
Reading from 1149: heap size 574 MB, throughput 0.992615
Numeric result:
Recommendation: 2 clients, utility 7.08827:
    h1: 2016.5 MB (U(h) = 0.00355815*h^0.922198)
    h2: 1295.5 MB (U(h) = 0.0255754*h^0.592461)
Recommendation: 2 clients, utility 7.08827:
    h1: 2016.51 MB (U(h) = 0.00355815*h^0.922198)
    h2: 1295.49 MB (U(h) = 0.0255754*h^0.592461)
Reading from 1150: heap size 538 MB, throughput 0.992476
Reading from 1149: heap size 574 MB, throughput 0.991897
Reading from 1150: heap size 537 MB, throughput 0.991442
Reading from 1149: heap size 576 MB, throughput 0.991656
Reading from 1150: heap size 540 MB, throughput 0.99067
Reading from 1149: heap size 578 MB, throughput 0.984864
Numeric result:
Recommendation: 2 clients, utility 6.22815:
    h1: 2246.98 MB (U(h) = 0.00196582*h^0.999)
    h2: 1065.02 MB (U(h) = 0.052372*h^0.473505)
Recommendation: 2 clients, utility 6.22815:
    h1: 2246.98 MB (U(h) = 0.00196582*h^0.999)
    h2: 1065.02 MB (U(h) = 0.052372*h^0.473505)
Reading from 1150: heap size 540 MB, throughput 0.989895
Reading from 1149: heap size 596 MB, throughput 0.990105
Reading from 1149: heap size 601 MB, throughput 0.985873
Reading from 1150: heap size 541 MB, throughput 0.991172
Reading from 1150: heap size 545 MB, throughput 0.980711
Reading from 1150: heap size 545 MB, throughput 0.982845
Reading from 1149: heap size 601 MB, throughput 0.990274
Reading from 1150: heap size 553 MB, throughput 0.99093
Reading from 1149: heap size 608 MB, throughput 0.992355
Numeric result:
Recommendation: 2 clients, utility 4.52483:
    h1: 2299.12 MB (U(h) = 0.00725991*h^0.802302)
    h2: 1012.88 MB (U(h) = 0.108483*h^0.353453)
Recommendation: 2 clients, utility 4.52483:
    h1: 2299.12 MB (U(h) = 0.00725991*h^0.802302)
    h2: 1012.88 MB (U(h) = 0.108483*h^0.353453)
Reading from 1150: heap size 555 MB, throughput 0.992494
Reading from 1149: heap size 609 MB, throughput 0.993282
Reading from 1150: heap size 555 MB, throughput 0.993834
Reading from 1149: heap size 610 MB, throughput 0.992273
Reading from 1150: heap size 557 MB, throughput 0.991906
Numeric result:
Recommendation: 2 clients, utility 2.32518:
    h1: 1546.81 MB (U(h) = 0.118504*h^0.341412)
    h2: 1765.19 MB (U(h) = 0.0868568*h^0.389618)
Recommendation: 2 clients, utility 2.32518:
    h1: 1546.8 MB (U(h) = 0.118504*h^0.341412)
    h2: 1765.2 MB (U(h) = 0.0868568*h^0.389618)
Reading from 1149: heap size 612 MB, throughput 0.991353
Reading from 1150: heap size 559 MB, throughput 0.990636
Reading from 1149: heap size 615 MB, throughput 0.990365
Reading from 1149: heap size 616 MB, throughput 0.98687
Reading from 1150: heap size 560 MB, throughput 0.992197
Reading from 1150: heap size 563 MB, throughput 0.986934
Reading from 1150: heap size 564 MB, throughput 0.989081
Reading from 1149: heap size 623 MB, throughput 0.992354
Reading from 1150: heap size 571 MB, throughput 0.992472
Numeric result:
Recommendation: 2 clients, utility 2.28923:
    h1: 1731.25 MB (U(h) = 0.0945686*h^0.377991)
    h2: 1580.75 MB (U(h) = 0.113728*h^0.345124)
Recommendation: 2 clients, utility 2.28923:
    h1: 1731.27 MB (U(h) = 0.0945686*h^0.377991)
    h2: 1580.73 MB (U(h) = 0.113728*h^0.345124)
Reading from 1149: heap size 623 MB, throughput 0.993293
Reading from 1150: heap size 572 MB, throughput 0.992808
Reading from 1149: heap size 625 MB, throughput 0.992756
Reading from 1150: heap size 570 MB, throughput 0.992846
Reading from 1149: heap size 627 MB, throughput 0.99264
Numeric result:
Recommendation: 2 clients, utility 2.34469:
    h1: 1632.73 MB (U(h) = 0.100612*h^0.367647)
    h2: 1679.27 MB (U(h) = 0.0926185*h^0.378131)
Recommendation: 2 clients, utility 2.34469:
    h1: 1632.72 MB (U(h) = 0.100612*h^0.367647)
    h2: 1679.28 MB (U(h) = 0.0926185*h^0.378131)
Reading from 1150: heap size 573 MB, throughput 0.991406
Reading from 1149: heap size 630 MB, throughput 0.993689
Reading from 1149: heap size 632 MB, throughput 0.988755
Reading from 1150: heap size 575 MB, throughput 0.990514
Reading from 1149: heap size 632 MB, throughput 0.988125
Reading from 1150: heap size 576 MB, throughput 0.98523
Reading from 1150: heap size 578 MB, throughput 0.977504
Reading from 1150: heap size 585 MB, throughput 0.98519
Numeric result:
Recommendation: 2 clients, utility 2.17176:
    h1: 1699.89 MB (U(h) = 0.109424*h^0.353322)
    h2: 1612.11 MB (U(h) = 0.120672*h^0.335074)
Recommendation: 2 clients, utility 2.17176:
    h1: 1699.9 MB (U(h) = 0.109424*h^0.353322)
    h2: 1612.1 MB (U(h) = 0.120672*h^0.335074)
Reading from 1149: heap size 635 MB, throughput 0.991858
Reading from 1150: heap size 593 MB, throughput 0.987556
Reading from 1149: heap size 637 MB, throughput 0.992506
Reading from 1150: heap size 597 MB, throughput 0.989214
Reading from 1149: heap size 640 MB, throughput 0.991608
Numeric result:
Recommendation: 2 clients, utility 2.02621:
    h1: 1678.27 MB (U(h) = 0.132701*h^0.321813)
    h2: 1633.73 MB (U(h) = 0.137853*h^0.31327)
Recommendation: 2 clients, utility 2.02621:
    h1: 1678.28 MB (U(h) = 0.132701*h^0.321813)
    h2: 1633.72 MB (U(h) = 0.137853*h^0.31327)
Reading from 1150: heap size 603 MB, throughput 0.989219
Reading from 1149: heap size 645 MB, throughput 0.99054
Reading from 1150: heap size 608 MB, throughput 0.9878
Reading from 1149: heap size 646 MB, throughput 0.988625
Reading from 1149: heap size 653 MB, throughput 0.986559
Reading from 1150: heap size 613 MB, throughput 0.988124
Reading from 1150: heap size 619 MB, throughput 0.978928
Reading from 1150: heap size 619 MB, throughput 0.981656
Reading from 1149: heap size 655 MB, throughput 0.991784
Numeric result:
Recommendation: 2 clients, utility 1.783:
    h1: 1630.82 MB (U(h) = 0.191642*h^0.262191)
    h2: 1681.18 MB (U(h) = 0.179695*h^0.270293)
Recommendation: 2 clients, utility 1.783:
    h1: 1630.8 MB (U(h) = 0.191642*h^0.262191)
    h2: 1681.2 MB (U(h) = 0.179695*h^0.270293)
Reading from 1150: heap size 625 MB, throughput 0.987508
Reading from 1149: heap size 659 MB, throughput 0.992729
Reading from 1150: heap size 623 MB, throughput 0.987026
Reading from 1149: heap size 660 MB, throughput 0.991343
Numeric result:
Recommendation: 2 clients, utility 1.71717:
    h1: 1756.74 MB (U(h) = 0.186511*h^0.266214)
    h2: 1555.26 MB (U(h) = 0.22291*h^0.235665)
Recommendation: 2 clients, utility 1.71717:
    h1: 1756.8 MB (U(h) = 0.186511*h^0.266214)
    h2: 1555.2 MB (U(h) = 0.22291*h^0.235665)
Reading from 1150: heap size 635 MB, throughput 0.993588
Reading from 1149: heap size 662 MB, throughput 0.991221
Reading from 1150: heap size 635 MB, throughput 0.993896
Reading from 1149: heap size 663 MB, throughput 0.989747
Reading from 1149: heap size 667 MB, throughput 0.987439
Reading from 1150: heap size 639 MB, throughput 0.995113
Reading from 1150: heap size 641 MB, throughput 0.990812
Numeric result:
Recommendation: 2 clients, utility 1.66978:
    h1: 2059.89 MB (U(h) = 0.159573*h^0.290788)
    h2: 1252.11 MB (U(h) = 0.322508*h^0.176759)
Recommendation: 2 clients, utility 1.66978:
    h1: 2059.88 MB (U(h) = 0.159573*h^0.290788)
    h2: 1252.12 MB (U(h) = 0.322508*h^0.176759)
Reading from 1150: heap size 643 MB, throughput 0.988184
Reading from 1149: heap size 669 MB, throughput 0.992136
Reading from 1150: heap size 643 MB, throughput 0.993919
Reading from 1149: heap size 672 MB, throughput 0.993086
Reading from 1150: heap size 645 MB, throughput 0.993073
Numeric result:
Recommendation: 2 clients, utility 1.62147:
    h1: 2193 MB (U(h) = 0.162235*h^0.287865)
    h2: 1119 MB (U(h) = 0.389189*h^0.146887)
Recommendation: 2 clients, utility 1.62147:
    h1: 2192.99 MB (U(h) = 0.162235*h^0.287865)
    h2: 1119.01 MB (U(h) = 0.389189*h^0.146887)
Reading from 1149: heap size 674 MB, throughput 0.993887
Reading from 1150: heap size 642 MB, throughput 0.992235
Reading from 1149: heap size 675 MB, throughput 0.995484
Reading from 1150: heap size 645 MB, throughput 0.991123
Reading from 1149: heap size 676 MB, throughput 0.992136
Client 1149 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 1150: heap size 639 MB, throughput 0.993361
Reading from 1150: heap size 643 MB, throughput 0.98964
Client 1150 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
