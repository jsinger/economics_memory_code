economemd
    total memory: 938 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_30_14_13_41/sub4_timerComparisonSingle/sub4_timeBenchmarkDaemon/daemon_run09_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
ReadingThread: starting
TimerThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 6293: heap size 9 MB, throughput 0.993491
Clients: 1
Client 6293 has a minimum heap size of 276 MB
Reading from 6294: heap size 9 MB, throughput 0.992084
Clients: 2
Client 6294 has a minimum heap size of 276 MB
Reading from 6294: heap size 9 MB, throughput 0.988484
Reading from 6293: heap size 9 MB, throughput 0.988875
Reading from 6294: heap size 9 MB, throughput 0.981538
Reading from 6293: heap size 9 MB, throughput 0.982432
Reading from 6294: heap size 9 MB, throughput 0.971753
Reading from 6293: heap size 9 MB, throughput 0.965504
Reading from 6293: heap size 11 MB, throughput 0.978227
Reading from 6294: heap size 11 MB, throughput 0.976867
Reading from 6293: heap size 11 MB, throughput 0.966572
Reading from 6294: heap size 11 MB, throughput 0.972651
Reading from 6294: heap size 17 MB, throughput 0.954519
Reading from 6293: heap size 17 MB, throughput 0.92476
Reading from 6293: heap size 17 MB, throughput 0.891161
Reading from 6294: heap size 17 MB, throughput 0.883784
Reading from 6294: heap size 30 MB, throughput 0.755893
Reading from 6293: heap size 30 MB, throughput 0.820993
Reading from 6294: heap size 31 MB, throughput 0.404107
Reading from 6293: heap size 31 MB, throughput 0.554229
Reading from 6294: heap size 34 MB, throughput 0.598829
Reading from 6294: heap size 47 MB, throughput 0.649675
Reading from 6294: heap size 50 MB, throughput 0.422532
Reading from 6293: heap size 35 MB, throughput 0.600257
Reading from 6293: heap size 47 MB, throughput 0.384471
Reading from 6294: heap size 51 MB, throughput 0.544372
Reading from 6293: heap size 49 MB, throughput 0.523117
Reading from 6294: heap size 70 MB, throughput 0.573529
Reading from 6293: heap size 64 MB, throughput 0.412242
Reading from 6294: heap size 71 MB, throughput 0.226641
Reading from 6293: heap size 71 MB, throughput 0.344617
Reading from 6294: heap size 72 MB, throughput 0.830889
Reading from 6294: heap size 76 MB, throughput 0.764151
Reading from 6293: heap size 71 MB, throughput 0.441154
Reading from 6293: heap size 102 MB, throughput 0.428244
Reading from 6294: heap size 78 MB, throughput 0.507562
Reading from 6294: heap size 103 MB, throughput 0.694847
Reading from 6294: heap size 105 MB, throughput 0.676791
Reading from 6293: heap size 103 MB, throughput 0.322673
Reading from 6294: heap size 108 MB, throughput 0.683566
Reading from 6294: heap size 111 MB, throughput 0.664721
Reading from 6293: heap size 134 MB, throughput 0.640735
Reading from 6293: heap size 134 MB, throughput 0.481258
Reading from 6293: heap size 136 MB, throughput 0.216987
Reading from 6294: heap size 117 MB, throughput 0.38152
Reading from 6294: heap size 146 MB, throughput 0.600192
Reading from 6294: heap size 148 MB, throughput 0.677504
Reading from 6294: heap size 150 MB, throughput 0.624831
Reading from 6293: heap size 140 MB, throughput 0.495567
Reading from 6294: heap size 151 MB, throughput 0.653487
Reading from 6293: heap size 172 MB, throughput 0.097131
Reading from 6294: heap size 156 MB, throughput 0.694638
Reading from 6293: heap size 177 MB, throughput 0.197123
Reading from 6294: heap size 159 MB, throughput 0.61888
Reading from 6293: heap size 179 MB, throughput 0.731933
Reading from 6294: heap size 165 MB, throughput 0.401135
Reading from 6294: heap size 196 MB, throughput 0.553471
Reading from 6293: heap size 182 MB, throughput 0.388838
Reading from 6294: heap size 202 MB, throughput 0.595071
Reading from 6293: heap size 224 MB, throughput 0.798326
Reading from 6294: heap size 204 MB, throughput 0.867455
Reading from 6294: heap size 206 MB, throughput 0.858458
Reading from 6293: heap size 227 MB, throughput 0.898187
Reading from 6294: heap size 210 MB, throughput 0.813727
Reading from 6294: heap size 215 MB, throughput 0.72462
Reading from 6293: heap size 228 MB, throughput 0.855631
Reading from 6294: heap size 219 MB, throughput 0.679793
Reading from 6293: heap size 235 MB, throughput 0.845178
Reading from 6294: heap size 223 MB, throughput 0.628322
Reading from 6293: heap size 239 MB, throughput 0.860873
Reading from 6293: heap size 240 MB, throughput 0.842992
Reading from 6293: heap size 247 MB, throughput 0.86719
Reading from 6294: heap size 227 MB, throughput 0.543772
Reading from 6293: heap size 247 MB, throughput 0.861003
Reading from 6294: heap size 262 MB, throughput 0.623773
Reading from 6294: heap size 264 MB, throughput 0.697677
Reading from 6293: heap size 251 MB, throughput 0.846691
Reading from 6293: heap size 252 MB, throughput 0.767473
Reading from 6294: heap size 258 MB, throughput 0.768855
Reading from 6294: heap size 261 MB, throughput 0.740851
Reading from 6293: heap size 257 MB, throughput 0.781842
Reading from 6294: heap size 266 MB, throughput 0.711503
Reading from 6293: heap size 260 MB, throughput 0.784546
Reading from 6294: heap size 266 MB, throughput 0.715685
Reading from 6294: heap size 269 MB, throughput 0.658176
Reading from 6293: heap size 266 MB, throughput 0.750607
Reading from 6294: heap size 270 MB, throughput 0.615375
Reading from 6293: heap size 268 MB, throughput 0.897793
Reading from 6293: heap size 272 MB, throughput 0.851995
Reading from 6294: heap size 311 MB, throughput 0.828043
Reading from 6294: heap size 311 MB, throughput 0.836075
Reading from 6294: heap size 316 MB, throughput 0.813414
Reading from 6294: heap size 317 MB, throughput 0.81503
Reading from 6294: heap size 321 MB, throughput 0.846792
Reading from 6293: heap size 275 MB, throughput 0.722214
Reading from 6293: heap size 316 MB, throughput 0.694483
Reading from 6294: heap size 322 MB, throughput 0.89667
Reading from 6294: heap size 326 MB, throughput 0.838324
Reading from 6293: heap size 319 MB, throughput 0.743377
Reading from 6294: heap size 327 MB, throughput 0.780079
Reading from 6294: heap size 327 MB, throughput 0.703877
Reading from 6293: heap size 321 MB, throughput 0.798117
Reading from 6294: heap size 329 MB, throughput 0.645132
Reading from 6293: heap size 322 MB, throughput 0.814769
Reading from 6294: heap size 334 MB, throughput 0.662657
Reading from 6293: heap size 322 MB, throughput 0.788013
Reading from 6294: heap size 335 MB, throughput 0.705298
Reading from 6293: heap size 324 MB, throughput 0.746823
Reading from 6293: heap size 331 MB, throughput 0.672373
Numeric result:
Recommendation: 2 clients, utility 0.427485:
    h1: 471.666 MB (U(h) = 0.550781*h^0.0217533)
    h2: 466.334 MB (U(h) = 0.594814*h^0.0215088)
Recommendation: 2 clients, utility 0.427485:
    h1: 471.651 MB (U(h) = 0.550781*h^0.0217533)
    h2: 466.349 MB (U(h) = 0.594814*h^0.0215088)
Reading from 6293: heap size 332 MB, throughput 0.697515
Reading from 6293: heap size 340 MB, throughput 0.658731
Reading from 6293: heap size 340 MB, throughput 0.68857
Reading from 6294: heap size 342 MB, throughput 0.957919
Reading from 6293: heap size 348 MB, throughput 0.955345
Reading from 6294: heap size 343 MB, throughput 0.969487
Reading from 6293: heap size 349 MB, throughput 0.966346
Reading from 6294: heap size 345 MB, throughput 0.968858
Reading from 6293: heap size 349 MB, throughput 0.839618
Reading from 6294: heap size 348 MB, throughput 0.971921
Reading from 6293: heap size 395 MB, throughput 0.984336
Reading from 6294: heap size 349 MB, throughput 0.973663
Reading from 6293: heap size 399 MB, throughput 0.98716
Reading from 6294: heap size 351 MB, throughput 0.97532
Reading from 6293: heap size 402 MB, throughput 0.990239
Numeric result:
Recommendation: 2 clients, utility 0.513982:
    h1: 495.982 MB (U(h) = 0.457319*h^0.0684001)
    h2: 442.018 MB (U(h) = 0.507102*h^0.0609587)
Recommendation: 2 clients, utility 0.513982:
    h1: 495.979 MB (U(h) = 0.457319*h^0.0684001)
    h2: 442.021 MB (U(h) = 0.507102*h^0.0609587)
Reading from 6294: heap size 349 MB, throughput 0.974233
Reading from 6293: heap size 405 MB, throughput 0.990259
Reading from 6294: heap size 352 MB, throughput 0.973795
Reading from 6293: heap size 406 MB, throughput 0.988794
Reading from 6294: heap size 355 MB, throughput 0.970105
Reading from 6293: heap size 404 MB, throughput 0.986671
Reading from 6294: heap size 356 MB, throughput 0.972095
Reading from 6293: heap size 406 MB, throughput 0.984359
Reading from 6294: heap size 359 MB, throughput 0.974469
Reading from 6293: heap size 399 MB, throughput 0.984592
Reading from 6294: heap size 361 MB, throughput 0.963194
Reading from 6293: heap size 403 MB, throughput 0.982958
Reading from 6294: heap size 364 MB, throughput 0.968591
Reading from 6293: heap size 400 MB, throughput 0.981697
Numeric result:
Recommendation: 2 clients, utility 0.581352:
    h1: 499.107 MB (U(h) = 0.401638*h^0.100324)
    h2: 438.893 MB (U(h) = 0.453745*h^0.0882159)
Recommendation: 2 clients, utility 0.581352:
    h1: 499.12 MB (U(h) = 0.401638*h^0.100324)
    h2: 438.88 MB (U(h) = 0.453745*h^0.0882159)
Reading from 6294: heap size 366 MB, throughput 0.97446
Reading from 6294: heap size 371 MB, throughput 0.96222
Reading from 6294: heap size 372 MB, throughput 0.935584
Reading from 6294: heap size 378 MB, throughput 0.895396
Reading from 6293: heap size 401 MB, throughput 0.973951
Reading from 6294: heap size 381 MB, throughput 0.933763
Reading from 6293: heap size 398 MB, throughput 0.979892
Reading from 6293: heap size 403 MB, throughput 0.967549
Reading from 6293: heap size 401 MB, throughput 0.940238
Reading from 6293: heap size 409 MB, throughput 0.905606
Reading from 6293: heap size 418 MB, throughput 0.87088
Reading from 6294: heap size 426 MB, throughput 0.964659
Reading from 6293: heap size 422 MB, throughput 0.94369
Reading from 6294: heap size 427 MB, throughput 0.983285
Reading from 6293: heap size 425 MB, throughput 0.971985
Reading from 6293: heap size 430 MB, throughput 0.976697
Reading from 6294: heap size 437 MB, throughput 0.987134
Reading from 6293: heap size 425 MB, throughput 0.979134
Reading from 6294: heap size 438 MB, throughput 0.987191
Numeric result:
Recommendation: 2 clients, utility 0.6461:
    h1: 489.297 MB (U(h) = 0.361584*h^0.125954)
    h2: 448.703 MB (U(h) = 0.4046*h^0.1155)
Recommendation: 2 clients, utility 0.6461:
    h1: 489.306 MB (U(h) = 0.361584*h^0.125954)
    h2: 448.694 MB (U(h) = 0.4046*h^0.1155)
Reading from 6293: heap size 429 MB, throughput 0.979907
Reading from 6294: heap size 439 MB, throughput 0.987036
Reading from 6293: heap size 424 MB, throughput 0.981516
Reading from 6294: heap size 442 MB, throughput 0.984874
Reading from 6293: heap size 428 MB, throughput 0.982718
Reading from 6294: heap size 437 MB, throughput 0.979439
Reading from 6293: heap size 424 MB, throughput 0.982065
Reading from 6294: heap size 441 MB, throughput 0.98046
Reading from 6293: heap size 427 MB, throughput 0.980124
Reading from 6294: heap size 439 MB, throughput 0.981568
Reading from 6293: heap size 423 MB, throughput 0.981479
Numeric result:
Recommendation: 2 clients, utility 0.676881:
    h1: 501.339 MB (U(h) = 0.338828*h^0.14166)
    h2: 436.661 MB (U(h) = 0.391083*h^0.123388)
Recommendation: 2 clients, utility 0.676881:
    h1: 501.333 MB (U(h) = 0.338828*h^0.14166)
    h2: 436.667 MB (U(h) = 0.391083*h^0.123388)
Reading from 6294: heap size 441 MB, throughput 0.986277
Reading from 6293: heap size 425 MB, throughput 0.980825
Reading from 6294: heap size 427 MB, throughput 0.982361
Reading from 6294: heap size 436 MB, throughput 0.967094
Reading from 6294: heap size 435 MB, throughput 0.947772
Reading from 6294: heap size 440 MB, throughput 0.948905
Reading from 6293: heap size 421 MB, throughput 0.979682
Reading from 6294: heap size 424 MB, throughput 0.97985
Reading from 6293: heap size 424 MB, throughput 0.983227
Reading from 6293: heap size 428 MB, throughput 0.97278
Reading from 6293: heap size 428 MB, throughput 0.95506
Reading from 6293: heap size 433 MB, throughput 0.940304
Reading from 6294: heap size 438 MB, throughput 0.986173
Reading from 6293: heap size 434 MB, throughput 0.977112
Reading from 6294: heap size 419 MB, throughput 0.985959
Reading from 6293: heap size 441 MB, throughput 0.984918
Reading from 6294: heap size 431 MB, throughput 0.984558
Reading from 6293: heap size 442 MB, throughput 0.986608
Numeric result:
Recommendation: 2 clients, utility 0.707799:
    h1: 498.828 MB (U(h) = 0.322789*h^0.153297)
    h2: 439.172 MB (U(h) = 0.372181*h^0.13496)
Recommendation: 2 clients, utility 0.707799:
    h1: 498.835 MB (U(h) = 0.322789*h^0.153297)
    h2: 439.165 MB (U(h) = 0.372181*h^0.13496)
Reading from 6294: heap size 428 MB, throughput 0.984916
Reading from 6293: heap size 444 MB, throughput 0.985197
Reading from 6294: heap size 431 MB, throughput 0.981708
Reading from 6293: heap size 446 MB, throughput 0.985568
Reading from 6293: heap size 444 MB, throughput 0.98655
Reading from 6294: heap size 432 MB, throughput 0.982931
Reading from 6293: heap size 446 MB, throughput 0.984533
Reading from 6294: heap size 433 MB, throughput 0.985179
Reading from 6294: heap size 435 MB, throughput 0.983738
Reading from 6293: heap size 444 MB, throughput 0.985637
Numeric result:
Recommendation: 2 clients, utility 0.72104:
    h1: 496.068 MB (U(h) = 0.316963*h^0.157626)
    h2: 441.932 MB (U(h) = 0.363563*h^0.140429)
Recommendation: 2 clients, utility 0.72104:
    h1: 496.061 MB (U(h) = 0.316963*h^0.157626)
    h2: 441.939 MB (U(h) = 0.363563*h^0.140429)
Reading from 6294: heap size 436 MB, throughput 0.976191
Reading from 6293: heap size 446 MB, throughput 0.977041
Reading from 6294: heap size 439 MB, throughput 0.969011
Reading from 6294: heap size 439 MB, throughput 0.956496
Reading from 6294: heap size 445 MB, throughput 0.943465
Reading from 6294: heap size 411 MB, throughput 0.981306
Reading from 6293: heap size 448 MB, throughput 0.984508
Reading from 6293: heap size 448 MB, throughput 0.982448
Reading from 6293: heap size 450 MB, throughput 0.9732
Reading from 6293: heap size 451 MB, throughput 0.957443
Reading from 6294: heap size 444 MB, throughput 0.985807
Reading from 6293: heap size 457 MB, throughput 0.972503
Reading from 6294: heap size 411 MB, throughput 0.987426
Reading from 6293: heap size 458 MB, throughput 0.987892
Reading from 6294: heap size 441 MB, throughput 0.989476
Reading from 6293: heap size 462 MB, throughput 0.988593
Numeric result:
Recommendation: 2 clients, utility 0.741427:
    h1: 504.331 MB (U(h) = 0.303268*h^0.16804)
    h2: 433.669 MB (U(h) = 0.357273*h^0.144503)
Recommendation: 2 clients, utility 0.741427:
    h1: 504.318 MB (U(h) = 0.303268*h^0.16804)
    h2: 433.682 MB (U(h) = 0.357273*h^0.144503)
Reading from 6294: heap size 440 MB, throughput 0.987479
Reading from 6293: heap size 464 MB, throughput 0.985143
Reading from 6294: heap size 424 MB, throughput 0.984882
Reading from 6293: heap size 463 MB, throughput 0.988116
Reading from 6294: heap size 433 MB, throughput 0.985524
Reading from 6293: heap size 465 MB, throughput 0.987591
Reading from 6294: heap size 435 MB, throughput 0.982716
Reading from 6293: heap size 463 MB, throughput 0.987374
Reading from 6294: heap size 422 MB, throughput 0.984159
Numeric result:
Recommendation: 2 clients, utility 0.750589:
    h1: 507.851 MB (U(h) = 0.297286*h^0.172698)
    h2: 430.149 MB (U(h) = 0.354585*h^0.146275)
Recommendation: 2 clients, utility 0.750589:
    h1: 507.85 MB (U(h) = 0.297286*h^0.172698)
    h2: 430.15 MB (U(h) = 0.354585*h^0.146275)
Reading from 6293: heap size 465 MB, throughput 0.986037
Reading from 6294: heap size 432 MB, throughput 0.985788
Reading from 6294: heap size 404 MB, throughput 0.977722
Reading from 6294: heap size 427 MB, throughput 0.964174
Reading from 6294: heap size 430 MB, throughput 0.955671
Reading from 6294: heap size 439 MB, throughput 0.982755
Reading from 6293: heap size 467 MB, throughput 0.988711
Reading from 6293: heap size 468 MB, throughput 0.984974
Reading from 6293: heap size 470 MB, throughput 0.975893
Reading from 6293: heap size 472 MB, throughput 0.96136
Reading from 6294: heap size 405 MB, throughput 0.987148
Reading from 6293: heap size 477 MB, throughput 0.983738
Reading from 6294: heap size 436 MB, throughput 0.987579
Reading from 6294: heap size 407 MB, throughput 0.987929
Reading from 6293: heap size 478 MB, throughput 0.989358
Numeric result:
Recommendation: 2 clients, utility 0.768547:
    h1: 509.967 MB (U(h) = 0.287844*h^0.180192)
    h2: 428.033 MB (U(h) = 0.347254*h^0.151242)
Recommendation: 2 clients, utility 0.768547:
    h1: 509.967 MB (U(h) = 0.287844*h^0.180192)
    h2: 428.033 MB (U(h) = 0.347254*h^0.151242)
Reading from 6294: heap size 432 MB, throughput 0.986101
Reading from 6293: heap size 482 MB, throughput 0.988719
Reading from 6294: heap size 409 MB, throughput 0.986123
Reading from 6293: heap size 484 MB, throughput 0.989258
Reading from 6294: heap size 427 MB, throughput 0.986063
Reading from 6293: heap size 482 MB, throughput 0.988616
Reading from 6294: heap size 426 MB, throughput 0.986382
Reading from 6293: heap size 484 MB, throughput 0.988373
Reading from 6294: heap size 428 MB, throughput 0.985026
Numeric result:
Recommendation: 2 clients, utility 0.773882:
    h1: 510.816 MB (U(h) = 0.284964*h^0.182504)
    h2: 427.184 MB (U(h) = 0.345239*h^0.152624)
Recommendation: 2 clients, utility 0.773882:
    h1: 510.816 MB (U(h) = 0.284964*h^0.182504)
    h2: 427.184 MB (U(h) = 0.345239*h^0.152624)
Reading from 6294: heap size 418 MB, throughput 0.988327
Reading from 6293: heap size 483 MB, throughput 0.987587
Reading from 6294: heap size 427 MB, throughput 0.980512
Reading from 6294: heap size 427 MB, throughput 0.968472
Reading from 6294: heap size 432 MB, throughput 0.948279
Reading from 6294: heap size 421 MB, throughput 0.97612
Reading from 6293: heap size 485 MB, throughput 0.990254
Reading from 6294: heap size 432 MB, throughput 0.983221
Reading from 6293: heap size 486 MB, throughput 0.986702
Reading from 6293: heap size 487 MB, throughput 0.977412
Reading from 6293: heap size 486 MB, throughput 0.972422
Reading from 6294: heap size 399 MB, throughput 0.985139
Reading from 6293: heap size 456 MB, throughput 0.987163
Reading from 6294: heap size 430 MB, throughput 0.986889
Reading from 6294: heap size 402 MB, throughput 0.986143
Reading from 6293: heap size 487 MB, throughput 0.988559
Numeric result:
Recommendation: 2 clients, utility 0.787601:
    h1: 510.518 MB (U(h) = 0.278886*h^0.187481)
    h2: 427.482 MB (U(h) = 0.33897*h^0.156986)
Recommendation: 2 clients, utility 0.787601:
    h1: 510.519 MB (U(h) = 0.278886*h^0.187481)
    h2: 427.481 MB (U(h) = 0.33897*h^0.156986)
Reading from 6294: heap size 426 MB, throughput 0.985791
Reading from 6293: heap size 460 MB, throughput 0.989299
Reading from 6294: heap size 425 MB, throughput 0.985841
Reading from 6293: heap size 488 MB, throughput 0.988987
Reading from 6294: heap size 424 MB, throughput 0.984263
Reading from 6294: heap size 425 MB, throughput 0.984328
Reading from 6293: heap size 467 MB, throughput 0.988974
Reading from 6294: heap size 428 MB, throughput 0.981273
Numeric result:
Recommendation: 2 clients, utility 0.791907:
    h1: 511.215 MB (U(h) = 0.276686*h^0.189319)
    h2: 426.785 MB (U(h) = 0.337428*h^0.158054)
Recommendation: 2 clients, utility 0.791907:
    h1: 511.212 MB (U(h) = 0.276686*h^0.189319)
    h2: 426.788 MB (U(h) = 0.337428*h^0.158054)
Reading from 6293: heap size 486 MB, throughput 0.98894
Reading from 6294: heap size 425 MB, throughput 0.98029
Reading from 6294: heap size 432 MB, throughput 0.967688
Reading from 6294: heap size 427 MB, throughput 0.945657
Reading from 6294: heap size 429 MB, throughput 0.915442
Reading from 6293: heap size 486 MB, throughput 0.988574
Reading from 6294: heap size 404 MB, throughput 0.97623
Reading from 6294: heap size 430 MB, throughput 0.982462
Reading from 6293: heap size 488 MB, throughput 0.990827
Reading from 6293: heap size 489 MB, throughput 0.984303
Reading from 6293: heap size 486 MB, throughput 0.976388
Reading from 6294: heap size 405 MB, throughput 0.985674
Reading from 6293: heap size 479 MB, throughput 0.984384
Reading from 6294: heap size 426 MB, throughput 0.984297
Reading from 6293: heap size 487 MB, throughput 0.988596
Numeric result:
Recommendation: 2 clients, utility 0.794747:
    h1: 512.955 MB (U(h) = 0.274654*h^0.191012)
    h2: 425.045 MB (U(h) = 0.337104*h^0.158277)
Recommendation: 2 clients, utility 0.794747:
    h1: 512.954 MB (U(h) = 0.274654*h^0.191012)
    h2: 425.046 MB (U(h) = 0.337104*h^0.158277)
Reading from 6294: heap size 425 MB, throughput 0.982907
Reading from 6294: heap size 425 MB, throughput 0.982309
Reading from 6293: heap size 458 MB, throughput 0.989932
Reading from 6294: heap size 412 MB, throughput 0.980638
Reading from 6293: heap size 488 MB, throughput 0.989467
Reading from 6294: heap size 424 MB, throughput 0.980527
Reading from 6293: heap size 465 MB, throughput 0.98845
Reading from 6294: heap size 423 MB, throughput 0.980462
Reading from 6294: heap size 451 MB, throughput 0.988739
Reading from 6293: heap size 486 MB, throughput 0.98873
Numeric result:
Recommendation: 2 clients, utility 0.857597:
    h1: 370.783 MB (U(h) = 0.274602*h^0.191056)
    h2: 567.217 MB (U(h) = 0.158085*h^0.292272)
Recommendation: 2 clients, utility 0.857597:
    h1: 370.784 MB (U(h) = 0.274602*h^0.191056)
    h2: 567.216 MB (U(h) = 0.158085*h^0.292272)
Reading from 6294: heap size 448 MB, throughput 0.985871
Reading from 6294: heap size 453 MB, throughput 0.976305
Reading from 6294: heap size 452 MB, throughput 0.964234
Reading from 6294: heap size 459 MB, throughput 0.954135
Reading from 6293: heap size 486 MB, throughput 0.987593
Reading from 6294: heap size 460 MB, throughput 0.98482
Reading from 6293: heap size 473 MB, throughput 0.989999
Reading from 6294: heap size 467 MB, throughput 0.988853
Reading from 6293: heap size 454 MB, throughput 0.986177
Reading from 6293: heap size 470 MB, throughput 0.977318
Reading from 6293: heap size 459 MB, throughput 0.967499
Reading from 6294: heap size 468 MB, throughput 0.99075
Reading from 6293: heap size 461 MB, throughput 0.983822
Reading from 6294: heap size 469 MB, throughput 0.989158
Numeric result:
Recommendation: 2 clients, utility 0.895552:
    h1: 388.128 MB (U(h) = 0.203787*h^0.243088)
    h2: 549.872 MB (U(h) = 0.11745*h^0.344388)
Recommendation: 2 clients, utility 0.895552:
    h1: 388.129 MB (U(h) = 0.203787*h^0.243088)
    h2: 549.871 MB (U(h) = 0.11745*h^0.344388)
Reading from 6293: heap size 431 MB, throughput 0.986263
Reading from 6294: heap size 472 MB, throughput 0.990041
Reading from 6293: heap size 451 MB, throughput 0.987744
Reading from 6294: heap size 471 MB, throughput 0.988005
Reading from 6293: heap size 427 MB, throughput 0.987695
Reading from 6294: heap size 473 MB, throughput 0.986716
Reading from 6293: heap size 440 MB, throughput 0.98532
Reading from 6294: heap size 476 MB, throughput 0.985431
Reading from 6293: heap size 425 MB, throughput 0.985292
Numeric result:
Recommendation: 2 clients, utility 0.95377:
    h1: 433.12 MB (U(h) = 0.0920912*h^0.37929)
    h2: 504.88 MB (U(h) = 0.0660722*h^0.442132)
Recommendation: 2 clients, utility 0.95377:
    h1: 433.12 MB (U(h) = 0.0920912*h^0.37929)
    h2: 504.88 MB (U(h) = 0.0660722*h^0.442132)
Reading from 6293: heap size 431 MB, throughput 0.73902
Reading from 6294: heap size 476 MB, throughput 0.988203
Reading from 6294: heap size 478 MB, throughput 0.981907
Reading from 6294: heap size 480 MB, throughput 0.968431
Reading from 6294: heap size 487 MB, throughput 0.95879
Reading from 6293: heap size 436 MB, throughput 0.984293
Reading from 6294: heap size 489 MB, throughput 0.986395
Reading from 6293: heap size 429 MB, throughput 0.982139
Reading from 6294: heap size 496 MB, throughput 0.990408
Reading from 6293: heap size 434 MB, throughput 0.984939
Reading from 6293: heap size 426 MB, throughput 0.97726
Reading from 6293: heap size 432 MB, throughput 0.964527
Reading from 6293: heap size 437 MB, throughput 0.947429
Reading from 6293: heap size 426 MB, throughput 0.978769
Reading from 6294: heap size 498 MB, throughput 0.99147
Reading from 6293: heap size 436 MB, throughput 0.987687
Numeric result:
Recommendation: 2 clients, utility 1.01128:
    h1: 503.462 MB (U(h) = 0.0279967*h^0.58086)
    h2: 434.538 MB (U(h) = 0.0463176*h^0.501341)
Recommendation: 2 clients, utility 1.01128:
    h1: 503.461 MB (U(h) = 0.0279967*h^0.58086)
    h2: 434.539 MB (U(h) = 0.0463176*h^0.501341)
Reading from 6294: heap size 498 MB, throughput 0.991833
Reading from 6293: heap size 405 MB, throughput 0.990337
Reading from 6294: heap size 477 MB, throughput 0.990119
Reading from 6293: heap size 435 MB, throughput 0.988711
Reading from 6293: heap size 434 MB, throughput 0.987502
Reading from 6294: heap size 484 MB, throughput 0.990105
Reading from 6293: heap size 433 MB, throughput 0.986737
Reading from 6294: heap size 473 MB, throughput 0.987645
Reading from 6293: heap size 435 MB, throughput 0.985522
Numeric result:
Recommendation: 2 clients, utility 1.0347:
    h1: 520.02 MB (U(h) = 0.018419*h^0.651529)
    h2: 417.98 MB (U(h) = 0.0404874*h^0.523686)
Recommendation: 2 clients, utility 1.0347:
    h1: 520.019 MB (U(h) = 0.018419*h^0.651529)
    h2: 417.981 MB (U(h) = 0.0404874*h^0.523686)
Reading from 6294: heap size 478 MB, throughput 0.989503
Reading from 6294: heap size 453 MB, throughput 0.983319
Reading from 6294: heap size 473 MB, throughput 0.972765
Reading from 6294: heap size 464 MB, throughput 0.965348
Reading from 6293: heap size 433 MB, throughput 0.79197
Reading from 6294: heap size 467 MB, throughput 0.983278
Reading from 6293: heap size 434 MB, throughput 0.795101
Reading from 6294: heap size 436 MB, throughput 0.98611
Reading from 6293: heap size 436 MB, throughput 0.981138
Reading from 6294: heap size 459 MB, throughput 0.986908
Reading from 6293: heap size 437 MB, throughput 0.983584
Reading from 6293: heap size 444 MB, throughput 0.974809
Reading from 6293: heap size 444 MB, throughput 0.956069
Reading from 6293: heap size 449 MB, throughput 0.956018
Reading from 6294: heap size 435 MB, throughput 0.98728
Reading from 6293: heap size 450 MB, throughput 0.984377
Numeric result:
Recommendation: 2 clients, utility 1.04556:
    h1: 523.946 MB (U(h) = 0.0144937*h^0.691383)
    h2: 414.054 MB (U(h) = 0.0353356*h^0.546373)
Recommendation: 2 clients, utility 1.04556:
    h1: 523.946 MB (U(h) = 0.0144937*h^0.691383)
    h2: 414.054 MB (U(h) = 0.0353356*h^0.546373)
Reading from 6294: heap size 449 MB, throughput 0.988448
Reading from 6293: heap size 454 MB, throughput 0.986176
Reading from 6294: heap size 433 MB, throughput 0.985717
Reading from 6293: heap size 456 MB, throughput 0.98496
Reading from 6294: heap size 441 MB, throughput 0.987138
Reading from 6294: heap size 431 MB, throughput 0.982412
Reading from 6293: heap size 455 MB, throughput 0.986238
Reading from 6294: heap size 439 MB, throughput 0.979633
Reading from 6293: heap size 457 MB, throughput 0.985684
Reading from 6294: heap size 430 MB, throughput 0.982013
Reading from 6294: heap size 434 MB, throughput 0.975957
Reading from 6294: heap size 429 MB, throughput 0.963621
Numeric result:
Recommendation: 2 clients, utility 1.0609:
    h1: 553.247 MB (U(h) = 0.0112325*h^0.734144)
    h2: 384.753 MB (U(h) = 0.0438159*h^0.510552)
Recommendation: 2 clients, utility 1.0609:
    h1: 553.249 MB (U(h) = 0.0112325*h^0.734144)
    h2: 384.751 MB (U(h) = 0.0438159*h^0.510552)
Reading from 6294: heap size 434 MB, throughput 0.926801
Reading from 6294: heap size 424 MB, throughput 0.901218
Reading from 6293: heap size 456 MB, throughput 0.982714
Reading from 6294: heap size 430 MB, throughput 0.976275
Reading from 6293: heap size 474 MB, throughput 0.987608
Reading from 6294: heap size 399 MB, throughput 0.984086
Reading from 6293: heap size 475 MB, throughput 0.990098
Reading from 6294: heap size 425 MB, throughput 0.985029
Reading from 6294: heap size 398 MB, throughput 0.984333
Reading from 6293: heap size 477 MB, throughput 0.99119
Reading from 6293: heap size 478 MB, throughput 0.983923
Reading from 6293: heap size 480 MB, throughput 0.972245
Reading from 6294: heap size 418 MB, throughput 0.983128
Reading from 6293: heap size 486 MB, throughput 0.966055
Reading from 6294: heap size 397 MB, throughput 0.982796
Numeric result:
Recommendation: 2 clients, utility 1.08417:
    h1: 585.575 MB (U(h) = 0.00790463*h^0.792322)
    h2: 352.425 MB (U(h) = 0.0536792*h^0.476859)
Recommendation: 2 clients, utility 1.08417:
    h1: 585.573 MB (U(h) = 0.00790463*h^0.792322)
    h2: 352.427 MB (U(h) = 0.0536792*h^0.476859)
Reading from 6293: heap size 489 MB, throughput 0.986426
Reading from 6294: heap size 411 MB, throughput 0.982975
Reading from 6294: heap size 396 MB, throughput 0.984053
Reading from 6293: heap size 490 MB, throughput 0.989315
Reading from 6294: heap size 405 MB, throughput 0.982319
Reading from 6293: heap size 493 MB, throughput 0.989752
Reading from 6294: heap size 395 MB, throughput 0.980459
Reading from 6294: heap size 403 MB, throughput 0.979322
Reading from 6293: heap size 491 MB, throughput 0.98885
Reading from 6294: heap size 394 MB, throughput 0.972105
Reading from 6294: heap size 402 MB, throughput 0.981044
Reading from 6294: heap size 380 MB, throughput 0.971931
Reading from 6294: heap size 398 MB, throughput 0.934353
Reading from 6293: heap size 494 MB, throughput 0.986523
Reading from 6294: heap size 400 MB, throughput 0.908645
Reading from 6294: heap size 400 MB, throughput 0.885185
Reading from 6294: heap size 400 MB, throughput 0.87011
Numeric result:
Recommendation: 2 clients, utility 1.0315:
    h1: 376.835 MB (U(h) = 0.0838005*h^0.400796)
    h2: 561.165 MB (U(h) = 0.0261172*h^0.596846)
Recommendation: 2 clients, utility 1.0315:
    h1: 376.835 MB (U(h) = 0.0838005*h^0.400796)
    h2: 561.165 MB (U(h) = 0.0261172*h^0.596846)
Reading from 6294: heap size 399 MB, throughput 0.971742
Reading from 6294: heap size 400 MB, throughput 0.981593
Reading from 6293: heap size 497 MB, throughput 0.987737
Reading from 6294: heap size 406 MB, throughput 0.983317
Reading from 6294: heap size 408 MB, throughput 0.983089
Reading from 6293: heap size 481 MB, throughput 0.986557
Reading from 6294: heap size 410 MB, throughput 0.981521
Reading from 6294: heap size 412 MB, throughput 0.981948
Reading from 6293: heap size 487 MB, throughput 0.990028
Reading from 6294: heap size 412 MB, throughput 0.97738
Reading from 6293: heap size 463 MB, throughput 0.982472
Reading from 6293: heap size 479 MB, throughput 0.972689
Reading from 6293: heap size 470 MB, throughput 0.958178
Numeric result:
Recommendation: 2 clients, utility 1.01688:
    h1: 365.707 MB (U(h) = 0.129731*h^0.328492)
    h2: 572.293 MB (U(h) = 0.0431198*h^0.51406)
Recommendation: 2 clients, utility 1.01688:
    h1: 365.705 MB (U(h) = 0.129731*h^0.328492)
    h2: 572.295 MB (U(h) = 0.0431198*h^0.51406)
Reading from 6294: heap size 414 MB, throughput 0.979877
Reading from 6293: heap size 476 MB, throughput 0.982625
Reading from 6294: heap size 414 MB, throughput 0.98367
Reading from 6293: heap size 440 MB, throughput 0.987196
Reading from 6294: heap size 415 MB, throughput 0.981869
Reading from 6293: heap size 465 MB, throughput 0.987599
Reading from 6294: heap size 414 MB, throughput 0.981502
Reading from 6294: heap size 416 MB, throughput 0.978972
Reading from 6293: heap size 436 MB, throughput 0.9882
Reading from 6294: heap size 418 MB, throughput 0.975448
Reading from 6293: heap size 454 MB, throughput 0.986542
Reading from 6294: heap size 419 MB, throughput 0.97152
Reading from 6294: heap size 427 MB, throughput 0.980007
Reading from 6294: heap size 442 MB, throughput 0.954633
Reading from 6294: heap size 456 MB, throughput 0.962073
Reading from 6294: heap size 460 MB, throughput 0.985176
Numeric result:
Recommendation: 2 clients, utility 1.00085:
    h1: 394.656 MB (U(h) = 0.125997*h^0.333303)
    h2: 543.344 MB (U(h) = 0.0602034*h^0.458876)
Recommendation: 2 clients, utility 1.00085:
    h1: 394.656 MB (U(h) = 0.125997*h^0.333303)
    h2: 543.344 MB (U(h) = 0.0602034*h^0.458876)
Reading from 6293: heap size 434 MB, throughput 0.987326
Reading from 6294: heap size 468 MB, throughput 0.992677
Reading from 6293: heap size 445 MB, throughput 0.985316
Reading from 6294: heap size 469 MB, throughput 0.992138
Reading from 6293: heap size 431 MB, throughput 0.983493
Reading from 6293: heap size 440 MB, throughput 0.98261
Reading from 6294: heap size 470 MB, throughput 0.993068
Reading from 6294: heap size 472 MB, throughput 0.991253
Reading from 6293: heap size 429 MB, throughput 0.986053
Reading from 6293: heap size 438 MB, throughput 0.985938
Reading from 6293: heap size 410 MB, throughput 0.97858
Reading from 6293: heap size 436 MB, throughput 0.959165
Reading from 6293: heap size 424 MB, throughput 0.932872
Reading from 6294: heap size 466 MB, throughput 0.989554
Reading from 6293: heap size 433 MB, throughput 0.971389
Numeric result:
Recommendation: 2 clients, utility 0.989154:
    h1: 473.323 MB (U(h) = 0.0905026*h^0.388144)
    h2: 464.677 MB (U(h) = 0.0963674*h^0.381054)
Recommendation: 2 clients, utility 0.989154:
    h1: 473.323 MB (U(h) = 0.0905026*h^0.388144)
    h2: 464.677 MB (U(h) = 0.0963674*h^0.381054)
Reading from 6293: heap size 399 MB, throughput 0.973507
Reading from 6294: heap size 431 MB, throughput 0.986174
Reading from 6293: heap size 433 MB, throughput 0.979123
Reading from 6294: heap size 465 MB, throughput 0.983582
Reading from 6293: heap size 433 MB, throughput 0.983212
Reading from 6294: heap size 467 MB, throughput 0.98417
Reading from 6293: heap size 434 MB, throughput 0.984458
Reading from 6294: heap size 459 MB, throughput 0.981786
Reading from 6293: heap size 436 MB, throughput 0.983958
Reading from 6294: heap size 465 MB, throughput 0.984171
Reading from 6293: heap size 435 MB, throughput 0.983053
Reading from 6294: heap size 467 MB, throughput 0.980047
Reading from 6294: heap size 466 MB, throughput 0.964833
Reading from 6294: heap size 465 MB, throughput 0.949525
Numeric result:
Recommendation: 2 clients, utility 0.98846:
    h1: 483.996 MB (U(h) = 0.089936*h^0.3893)
    h2: 454.004 MB (U(h) = 0.106053*h^0.365177)
Recommendation: 2 clients, utility 0.98846:
    h1: 483.995 MB (U(h) = 0.089936*h^0.3893)
    h2: 454.005 MB (U(h) = 0.106053*h^0.365177)
Reading from 6294: heap size 471 MB, throughput 0.929223
Client 6294 died
Clients: 1
Reading from 6293: heap size 437 MB, throughput 0.985911
Reading from 6293: heap size 436 MB, throughput 0.988121
Reading from 6293: heap size 437 MB, throughput 0.987766
Reading from 6293: heap size 439 MB, throughput 0.986815
Reading from 6293: heap size 439 MB, throughput 0.99015
Reading from 6293: heap size 441 MB, throughput 0.984425
Reading from 6293: heap size 442 MB, throughput 0.972927
Reading from 6293: heap size 446 MB, throughput 0.957647
Client 6293 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
