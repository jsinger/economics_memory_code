	Command being timed: "cgexec -g memory:economem /home/callum/economics_memory_code/dynamic_sizing/hotspot8/openjdk8/jdk8/build/linux-x86_64-normal-server-release/jdk/bin/java -XX:+EconomemSimpleThroughput -XX:-EconomemVengerovThroughput -XX:EconomemMinHeap=276 -Xms10m -Xmx1420m -XX:+DisableExplicitGC -XX:+UseAdaptiveSizePolicyWithSystemGC -jar /home/callum/economics_memory_code/dynamic_sizing/haskell_common/dacapo-9.12-bach.jar --no-pre-iteration-gc --scratch-directory /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub33_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/scratch1 -t 2 -n 13 -s large h2"
	User time (seconds): 290.80
	System time (seconds): 1.77
	Percent of CPU this job got: 110%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 4:24.83
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 804076
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 0
	Minor (reclaiming a frame) page faults: 112410
	Voluntary context switches: 171260
	Involuntary context switches: 51768
	Swaps: 0
	File system inputs: 0
	File system outputs: 15872
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
