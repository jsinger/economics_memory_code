economemd
    total memory: 2506 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub21_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 6904: heap size 9 MB, throughput 0.991399
Clients: 1
Client 6904 has a minimum heap size of 30 MB
Reading from 6903: heap size 9 MB, throughput 0.992151
Clients: 2
Client 6903 has a minimum heap size of 1223 MB
Reading from 6904: heap size 9 MB, throughput 0.966294
Reading from 6904: heap size 9 MB, throughput 0.964404
Reading from 6903: heap size 9 MB, throughput 0.965977
Reading from 6904: heap size 9 MB, throughput 0.96187
Reading from 6904: heap size 11 MB, throughput 0.739718
Reading from 6904: heap size 11 MB, throughput 0.800179
Reading from 6904: heap size 16 MB, throughput 0.677231
Reading from 6904: heap size 16 MB, throughput 0.694224
Reading from 6904: heap size 23 MB, throughput 0.875488
Reading from 6904: heap size 24 MB, throughput 0.830097
Reading from 6903: heap size 9 MB, throughput 0.955022
Reading from 6904: heap size 33 MB, throughput 0.914659
Reading from 6904: heap size 33 MB, throughput 0.86821
Reading from 6904: heap size 48 MB, throughput 0.944131
Reading from 6904: heap size 48 MB, throughput 0.933962
Reading from 6903: heap size 9 MB, throughput 0.944557
Reading from 6904: heap size 72 MB, throughput 0.962688
Reading from 6904: heap size 72 MB, throughput 0.934204
Reading from 6904: heap size 110 MB, throughput 0.95855
Reading from 6904: heap size 110 MB, throughput 0.959
Reading from 6903: heap size 11 MB, throughput 0.980513
Reading from 6903: heap size 11 MB, throughput 0.965552
Reading from 6903: heap size 17 MB, throughput 0.960069
Reading from 6903: heap size 17 MB, throughput 0.523199
Reading from 6903: heap size 30 MB, throughput 0.838851
Reading from 6903: heap size 31 MB, throughput 0.925455
Reading from 6903: heap size 35 MB, throughput 0.356226
Reading from 6903: heap size 47 MB, throughput 0.890416
Reading from 6904: heap size 151 MB, throughput 0.959927
Reading from 6903: heap size 47 MB, throughput 0.924898
Reading from 6903: heap size 50 MB, throughput 0.185926
Reading from 6903: heap size 71 MB, throughput 0.804906
Reading from 6904: heap size 157 MB, throughput 0.890614
Reading from 6903: heap size 71 MB, throughput 0.205042
Reading from 6903: heap size 98 MB, throughput 0.896878
Reading from 6903: heap size 98 MB, throughput 0.78177
Reading from 6903: heap size 100 MB, throughput 0.671303
Reading from 6903: heap size 102 MB, throughput 0.631091
Reading from 6904: heap size 208 MB, throughput 0.985727
Reading from 6903: heap size 105 MB, throughput 0.12281
Reading from 6903: heap size 135 MB, throughput 0.685392
Reading from 6903: heap size 138 MB, throughput 0.738576
Reading from 6903: heap size 141 MB, throughput 0.654999
Reading from 6903: heap size 149 MB, throughput 0.751917
Reading from 6903: heap size 151 MB, throughput 0.722284
Reading from 6904: heap size 225 MB, throughput 0.987657
Reading from 6903: heap size 154 MB, throughput 0.153061
Reading from 6903: heap size 193 MB, throughput 0.62897
Reading from 6903: heap size 196 MB, throughput 0.718842
Reading from 6903: heap size 201 MB, throughput 0.655174
Reading from 6904: heap size 241 MB, throughput 0.993494
Reading from 6903: heap size 204 MB, throughput 0.561757
Reading from 6903: heap size 212 MB, throughput 0.569658
Reading from 6903: heap size 216 MB, throughput 0.612481
Reading from 6903: heap size 225 MB, throughput 0.436677
Reading from 6903: heap size 231 MB, throughput 0.356067
Reading from 6904: heap size 242 MB, throughput 0.819532
Reading from 6903: heap size 238 MB, throughput 0.0662625
Reading from 6903: heap size 283 MB, throughput 0.351942
Reading from 6903: heap size 227 MB, throughput 0.505706
Reading from 6903: heap size 272 MB, throughput 0.136915
Reading from 6904: heap size 280 MB, throughput 0.985527
Reading from 6903: heap size 274 MB, throughput 0.432509
Reading from 6903: heap size 315 MB, throughput 0.629567
Reading from 6903: heap size 318 MB, throughput 0.577677
Reading from 6903: heap size 313 MB, throughput 0.656695
Reading from 6903: heap size 315 MB, throughput 0.679428
Reading from 6903: heap size 316 MB, throughput 0.457183
Reading from 6904: heap size 285 MB, throughput 0.987787
Reading from 6903: heap size 320 MB, throughput 0.449637
Reading from 6903: heap size 325 MB, throughput 0.500925
Reading from 6903: heap size 330 MB, throughput 0.640608
Reading from 6903: heap size 334 MB, throughput 0.533381
Reading from 6903: heap size 339 MB, throughput 0.584717
Reading from 6903: heap size 343 MB, throughput 0.522149
Reading from 6903: heap size 348 MB, throughput 0.569116
Reading from 6904: heap size 305 MB, throughput 0.983605
Reading from 6903: heap size 352 MB, throughput 0.0594903
Equal recommendation: 1253 MB each
Reading from 6903: heap size 400 MB, throughput 0.384479
Reading from 6903: heap size 408 MB, throughput 0.47359
Reading from 6903: heap size 408 MB, throughput 0.429999
Reading from 6904: heap size 306 MB, throughput 0.955261
Reading from 6903: heap size 410 MB, throughput 0.0763985
Reading from 6904: heap size 323 MB, throughput 0.98574
Reading from 6903: heap size 462 MB, throughput 0.43748
Reading from 6903: heap size 453 MB, throughput 0.66478
Reading from 6903: heap size 410 MB, throughput 0.599564
Reading from 6903: heap size 455 MB, throughput 0.598611
Reading from 6903: heap size 456 MB, throughput 0.585853
Reading from 6903: heap size 458 MB, throughput 0.552298
Reading from 6903: heap size 463 MB, throughput 0.557849
Reading from 6903: heap size 467 MB, throughput 0.468312
Reading from 6904: heap size 327 MB, throughput 0.972344
Reading from 6903: heap size 472 MB, throughput 0.0793728
Reading from 6904: heap size 351 MB, throughput 0.967783
Reading from 6903: heap size 530 MB, throughput 0.376731
Reading from 6903: heap size 534 MB, throughput 0.566265
Reading from 6903: heap size 536 MB, throughput 0.48417
Reading from 6903: heap size 538 MB, throughput 0.466967
Reading from 6903: heap size 541 MB, throughput 0.539565
Reading from 6903: heap size 547 MB, throughput 0.54603
Reading from 6904: heap size 354 MB, throughput 0.975359
Reading from 6903: heap size 551 MB, throughput 0.49123
Reading from 6903: heap size 558 MB, throughput 0.0911347
Reading from 6903: heap size 620 MB, throughput 0.365303
Reading from 6903: heap size 625 MB, throughput 0.474175
Reading from 6904: heap size 384 MB, throughput 0.983368
Reading from 6903: heap size 630 MB, throughput 0.446513
Reading from 6903: heap size 630 MB, throughput 0.568959
Reading from 6903: heap size 634 MB, throughput 0.864737
Reading from 6904: heap size 388 MB, throughput 0.968006
Reading from 6904: heap size 412 MB, throughput 0.965411
Reading from 6903: heap size 635 MB, throughput 0.243383
Reading from 6903: heap size 698 MB, throughput 0.759656
Equal recommendation: 1253 MB each
Reading from 6904: heap size 416 MB, throughput 0.96607
Reading from 6903: heap size 706 MB, throughput 0.827119
Reading from 6903: heap size 711 MB, throughput 0.436962
Reading from 6903: heap size 719 MB, throughput 0.639728
Reading from 6903: heap size 726 MB, throughput 0.483359
Reading from 6904: heap size 440 MB, throughput 0.982153
Reading from 6904: heap size 446 MB, throughput 0.950678
Reading from 6903: heap size 659 MB, throughput 0.0270591
Reading from 6903: heap size 785 MB, throughput 0.30237
Reading from 6903: heap size 790 MB, throughput 0.440292
Reading from 6903: heap size 791 MB, throughput 0.812245
Reading from 6903: heap size 792 MB, throughput 0.772841
Reading from 6903: heap size 794 MB, throughput 0.576251
Reading from 6904: heap size 471 MB, throughput 0.971127
Reading from 6903: heap size 796 MB, throughput 0.553747
Reading from 6903: heap size 800 MB, throughput 0.87527
Reading from 6904: heap size 475 MB, throughput 0.898105
Reading from 6903: heap size 801 MB, throughput 0.0728602
Reading from 6903: heap size 884 MB, throughput 0.748853
Reading from 6904: heap size 506 MB, throughput 0.990669
Reading from 6903: heap size 886 MB, throughput 0.72083
Reading from 6903: heap size 890 MB, throughput 0.922358
Reading from 6903: heap size 894 MB, throughput 0.877596
Reading from 6903: heap size 900 MB, throughput 0.895933
Reading from 6903: heap size 768 MB, throughput 0.828797
Reading from 6903: heap size 879 MB, throughput 0.795115
Reading from 6904: heap size 510 MB, throughput 0.975507
Reading from 6903: heap size 773 MB, throughput 0.830786
Reading from 6903: heap size 870 MB, throughput 0.813607
Reading from 6903: heap size 779 MB, throughput 0.820959
Reading from 6903: heap size 866 MB, throughput 0.845481
Reading from 6903: heap size 871 MB, throughput 0.828312
Equal recommendation: 1253 MB each
Reading from 6903: heap size 861 MB, throughput 0.87874
Reading from 6904: heap size 543 MB, throughput 0.98961
Reading from 6903: heap size 866 MB, throughput 0.856938
Reading from 6903: heap size 859 MB, throughput 0.864482
Reading from 6904: heap size 543 MB, throughput 0.972789
Reading from 6903: heap size 863 MB, throughput 0.981987
Reading from 6903: heap size 859 MB, throughput 0.939422
Reading from 6904: heap size 572 MB, throughput 0.974745
Reading from 6903: heap size 862 MB, throughput 0.0914134
Reading from 6903: heap size 948 MB, throughput 0.472428
Reading from 6903: heap size 949 MB, throughput 0.619367
Reading from 6904: heap size 576 MB, throughput 0.980269
Reading from 6903: heap size 944 MB, throughput 0.671781
Reading from 6903: heap size 947 MB, throughput 0.66122
Reading from 6903: heap size 946 MB, throughput 0.65825
Reading from 6903: heap size 949 MB, throughput 0.720455
Reading from 6903: heap size 951 MB, throughput 0.760735
Reading from 6903: heap size 954 MB, throughput 0.733829
Reading from 6904: heap size 610 MB, throughput 0.983076
Reading from 6903: heap size 960 MB, throughput 0.810553
Reading from 6903: heap size 969 MB, throughput 0.611751
Reading from 6903: heap size 953 MB, throughput 0.514978
Reading from 6903: heap size 982 MB, throughput 0.575708
Reading from 6904: heap size 613 MB, throughput 0.983732
Equal recommendation: 1253 MB each
Reading from 6903: heap size 996 MB, throughput 0.072915
Reading from 6903: heap size 1119 MB, throughput 0.505919
Reading from 6903: heap size 1127 MB, throughput 0.779296
Reading from 6903: heap size 1129 MB, throughput 0.787169
Reading from 6903: heap size 1139 MB, throughput 0.740991
Reading from 6904: heap size 645 MB, throughput 0.985185
Reading from 6903: heap size 1141 MB, throughput 0.671135
Reading from 6903: heap size 1148 MB, throughput 0.687091
Reading from 6903: heap size 1151 MB, throughput 0.698625
Reading from 6903: heap size 1155 MB, throughput 0.791366
Reading from 6904: heap size 649 MB, throughput 0.978911
Reading from 6903: heap size 1160 MB, throughput 0.974148
Reading from 6904: heap size 687 MB, throughput 0.992345
Reading from 6904: heap size 688 MB, throughput 0.98434
Reading from 6903: heap size 1158 MB, throughput 0.973411
Reading from 6904: heap size 721 MB, throughput 0.985048
Equal recommendation: 1253 MB each
Reading from 6903: heap size 1166 MB, throughput 0.969178
Reading from 6904: heap size 721 MB, throughput 0.988487
Reading from 6904: heap size 753 MB, throughput 0.989372
Reading from 6903: heap size 1175 MB, throughput 0.968894
Reading from 6904: heap size 756 MB, throughput 0.951851
Reading from 6903: heap size 1177 MB, throughput 0.971989
Reading from 6904: heap size 796 MB, throughput 0.99579
Equal recommendation: 1253 MB each
Reading from 6903: heap size 1178 MB, throughput 0.960387
Reading from 6904: heap size 797 MB, throughput 0.989831
Reading from 6904: heap size 829 MB, throughput 0.99076
Reading from 6903: heap size 1183 MB, throughput 0.972534
Reading from 6904: heap size 830 MB, throughput 0.991172
Reading from 6903: heap size 1180 MB, throughput 0.971489
Reading from 6904: heap size 831 MB, throughput 0.990991
Equal recommendation: 1253 MB each
Reading from 6903: heap size 1184 MB, throughput 0.972064
Reading from 6904: heap size 832 MB, throughput 0.991015
Reading from 6904: heap size 834 MB, throughput 0.988084
Reading from 6903: heap size 1189 MB, throughput 0.956565
Reading from 6904: heap size 835 MB, throughput 0.992069
Reading from 6903: heap size 1190 MB, throughput 0.969372
Reading from 6904: heap size 840 MB, throughput 0.991885
Equal recommendation: 1253 MB each
Reading from 6903: heap size 1195 MB, throughput 0.968015
Reading from 6904: heap size 841 MB, throughput 0.995971
Reading from 6904: heap size 841 MB, throughput 0.989742
Reading from 6903: heap size 1198 MB, throughput 0.972269
Reading from 6904: heap size 823 MB, throughput 0.991663
Reading from 6903: heap size 1203 MB, throughput 0.965516
Equal recommendation: 1253 MB each
Reading from 6904: heap size 842 MB, throughput 0.990273
Reading from 6903: heap size 1207 MB, throughput 0.971063
Reading from 6904: heap size 840 MB, throughput 0.992642
Reading from 6904: heap size 843 MB, throughput 0.994832
Reading from 6903: heap size 1211 MB, throughput 0.968269
Reading from 6904: heap size 842 MB, throughput 0.991262
Reading from 6903: heap size 1217 MB, throughput 0.967824
Equal recommendation: 1253 MB each
Reading from 6904: heap size 847 MB, throughput 0.991469
Reading from 6903: heap size 1223 MB, throughput 0.966634
Reading from 6904: heap size 847 MB, throughput 0.992732
Reading from 6904: heap size 846 MB, throughput 0.995671
Reading from 6903: heap size 1227 MB, throughput 0.96784
Reading from 6904: heap size 847 MB, throughput 0.991387
Equal recommendation: 1253 MB each
Reading from 6904: heap size 850 MB, throughput 0.981906
Reading from 6903: heap size 1233 MB, throughput 0.635501
Reading from 6904: heap size 837 MB, throughput 0.991107
Reading from 6903: heap size 1302 MB, throughput 0.993793
Reading from 6904: heap size 849 MB, throughput 0.991908
Reading from 6903: heap size 1299 MB, throughput 0.991667
Reading from 6904: heap size 848 MB, throughput 0.992903
Equal recommendation: 1253 MB each
Reading from 6903: heap size 1306 MB, throughput 0.989116
Reading from 6904: heap size 851 MB, throughput 0.991946
Reading from 6904: heap size 851 MB, throughput 0.992212
Reading from 6903: heap size 1307 MB, throughput 0.984829
Reading from 6904: heap size 852 MB, throughput 0.992781
Reading from 6903: heap size 1178 MB, throughput 0.985931
Equal recommendation: 1253 MB each
Reading from 6904: heap size 852 MB, throughput 0.991863
Reading from 6903: heap size 1303 MB, throughput 0.983773
Reading from 6904: heap size 852 MB, throughput 0.994853
Reading from 6904: heap size 852 MB, throughput 0.990983
Reading from 6903: heap size 1194 MB, throughput 0.982076
Reading from 6904: heap size 852 MB, throughput 0.991319
Equal recommendation: 1253 MB each
Reading from 6903: heap size 1289 MB, throughput 0.979804
Reading from 6904: heap size 852 MB, throughput 0.990565
Reading from 6903: heap size 1210 MB, throughput 0.982456
Reading from 6904: heap size 852 MB, throughput 0.992736
Reading from 6904: heap size 842 MB, throughput 0.991398
Reading from 6903: heap size 1280 MB, throughput 0.969894
Reading from 6904: heap size 852 MB, throughput 0.990265
Equal recommendation: 1253 MB each
Reading from 6903: heap size 1226 MB, throughput 0.974769
Reading from 6904: heap size 852 MB, throughput 0.991982
Reading from 6903: heap size 1276 MB, throughput 0.973758
Reading from 6904: heap size 852 MB, throughput 0.993781
Reading from 6904: heap size 852 MB, throughput 0.995719
Reading from 6903: heap size 1274 MB, throughput 0.974489
Reading from 6904: heap size 853 MB, throughput 0.994346
Equal recommendation: 1253 MB each
Reading from 6903: heap size 1273 MB, throughput 0.97235
Reading from 6904: heap size 853 MB, throughput 0.99035
Reading from 6903: heap size 1275 MB, throughput 0.966888
Reading from 6904: heap size 853 MB, throughput 0.990817
Reading from 6903: heap size 1274 MB, throughput 0.963075
Reading from 6904: heap size 854 MB, throughput 0.995723
Equal recommendation: 1253 MB each
Reading from 6904: heap size 853 MB, throughput 0.989693
Reading from 6903: heap size 1278 MB, throughput 0.966848
Reading from 6904: heap size 853 MB, throughput 0.990603
Reading from 6903: heap size 1279 MB, throughput 0.965947
Reading from 6904: heap size 852 MB, throughput 0.99175
Reading from 6903: heap size 1233 MB, throughput 0.96988
Reading from 6904: heap size 853 MB, throughput 0.992386
Equal recommendation: 1253 MB each
Reading from 6903: heap size 1281 MB, throughput 0.965196
Reading from 6904: heap size 852 MB, throughput 0.990696
Reading from 6903: heap size 1229 MB, throughput 0.96168
Reading from 6904: heap size 853 MB, throughput 0.99534
Reading from 6904: heap size 853 MB, throughput 0.997217
Reading from 6903: heap size 1281 MB, throughput 0.971661
Reading from 6904: heap size 832 MB, throughput 0.993502
Equal recommendation: 1253 MB each
Reading from 6903: heap size 1226 MB, throughput 0.969766
Reading from 6904: heap size 853 MB, throughput 0.992789
Reading from 6903: heap size 1278 MB, throughput 0.969651
Reading from 6904: heap size 835 MB, throughput 0.992841
Reading from 6903: heap size 1224 MB, throughput 0.964293
Reading from 6904: heap size 853 MB, throughput 0.991978
Equal recommendation: 1253 MB each
Reading from 6904: heap size 852 MB, throughput 0.996005
Reading from 6904: heap size 853 MB, throughput 0.991151
Reading from 6904: heap size 853 MB, throughput 0.990956
Reading from 6904: heap size 853 MB, throughput 0.991184
Equal recommendation: 1253 MB each
Reading from 6904: heap size 854 MB, throughput 0.990447
Reading from 6904: heap size 853 MB, throughput 0.989987
Reading from 6903: heap size 1274 MB, throughput 0.990863
Reading from 6904: heap size 853 MB, throughput 0.991477
Reading from 6904: heap size 853 MB, throughput 0.988992
Reading from 6904: heap size 853 MB, throughput 0.990891
Equal recommendation: 1253 MB each
Reading from 6904: heap size 854 MB, throughput 0.991224
Reading from 6904: heap size 845 MB, throughput 0.99034
Reading from 6904: heap size 853 MB, throughput 0.990479
Reading from 6903: heap size 1270 MB, throughput 0.992946
Reading from 6904: heap size 853 MB, throughput 0.989814
Equal recommendation: 1253 MB each
Reading from 6904: heap size 851 MB, throughput 0.99011
Reading from 6903: heap size 1267 MB, throughput 0.720114
Reading from 6904: heap size 852 MB, throughput 0.992088
Reading from 6904: heap size 850 MB, throughput 0.990472
Reading from 6903: heap size 1370 MB, throughput 0.99051
Reading from 6903: heap size 1408 MB, throughput 0.958345
Reading from 6903: heap size 1411 MB, throughput 0.878316
Reading from 6903: heap size 1392 MB, throughput 0.797157
Reading from 6903: heap size 1301 MB, throughput 0.765131
Reading from 6904: heap size 851 MB, throughput 0.983474
Reading from 6903: heap size 1374 MB, throughput 0.812789
Reading from 6903: heap size 1298 MB, throughput 0.79914
Reading from 6903: heap size 1358 MB, throughput 0.750814
Equal recommendation: 1253 MB each
Reading from 6903: heap size 1295 MB, throughput 0.81248
Reading from 6903: heap size 1344 MB, throughput 0.816788
Reading from 6903: heap size 1292 MB, throughput 0.915905
Reading from 6904: heap size 853 MB, throughput 0.991337
Reading from 6903: heap size 1337 MB, throughput 0.986968
Reading from 6904: heap size 854 MB, throughput 0.990931
Reading from 6903: heap size 1343 MB, throughput 0.987484
Reading from 6904: heap size 852 MB, throughput 0.991116
Reading from 6903: heap size 1347 MB, throughput 0.985514
Reading from 6904: heap size 853 MB, throughput 0.992001
Equal recommendation: 1253 MB each
Reading from 6903: heap size 1207 MB, throughput 0.982077
Reading from 6904: heap size 855 MB, throughput 0.991358
Reading from 6903: heap size 1350 MB, throughput 0.977426
Client 6904 died
Clients: 1
Reading from 6903: heap size 1202 MB, throughput 0.988965
Reading from 6903: heap size 1346 MB, throughput 0.988039
Recommendation: one client; give it all the memory
Reading from 6903: heap size 1216 MB, throughput 0.98708
Reading from 6903: heap size 1334 MB, throughput 0.987106
Reading from 6903: heap size 1232 MB, throughput 0.985423
Reading from 6903: heap size 1317 MB, throughput 0.985438
Recommendation: one client; give it all the memory
Reading from 6903: heap size 1248 MB, throughput 0.982793
Reading from 6903: heap size 1312 MB, throughput 0.983814
Reading from 6903: heap size 1317 MB, throughput 0.981663
Recommendation: one client; give it all the memory
Reading from 6903: heap size 1317 MB, throughput 0.980629
Reading from 6903: heap size 1318 MB, throughput 0.979516
Reading from 6903: heap size 1321 MB, throughput 0.979046
Recommendation: one client; give it all the memory
Reading from 6903: heap size 1323 MB, throughput 0.979494
Reading from 6903: heap size 1327 MB, throughput 0.980372
Reading from 6903: heap size 1328 MB, throughput 0.979054
Recommendation: one client; give it all the memory
Reading from 6903: heap size 1332 MB, throughput 0.979811
Reading from 6903: heap size 1332 MB, throughput 0.979314
Reading from 6903: heap size 1336 MB, throughput 0.980784
Recommendation: one client; give it all the memory
Reading from 6903: heap size 1336 MB, throughput 0.979833
Reading from 6903: heap size 1340 MB, throughput 0.98058
Reading from 6903: heap size 1340 MB, throughput 0.980389
Recommendation: one client; give it all the memory
Reading from 6903: heap size 1344 MB, throughput 0.980931
Reading from 6903: heap size 1344 MB, throughput 0.979828
Reading from 6903: heap size 1348 MB, throughput 0.979938
Recommendation: one client; give it all the memory
Reading from 6903: heap size 1348 MB, throughput 0.979548
Reading from 6903: heap size 1353 MB, throughput 0.978688
Reading from 6903: heap size 1353 MB, throughput 0.980228
Recommendation: one client; give it all the memory
Reading from 6903: heap size 1357 MB, throughput 0.979238
Reading from 6903: heap size 1357 MB, throughput 0.979306
Recommendation: one client; give it all the memory
Reading from 6903: heap size 1362 MB, throughput 0.782891
Reading from 6903: heap size 1376 MB, throughput 0.995649
Reading from 6903: heap size 1371 MB, throughput 0.99401
Recommendation: one client; give it all the memory
Reading from 6903: heap size 1381 MB, throughput 0.99225
Reading from 6903: heap size 1389 MB, throughput 0.992097
Reading from 6903: heap size 1391 MB, throughput 0.990154
Recommendation: one client; give it all the memory
Reading from 6903: heap size 1388 MB, throughput 0.989858
Reading from 6903: heap size 1293 MB, throughput 0.987695
Reading from 6903: heap size 1378 MB, throughput 0.986613
Recommendation: one client; give it all the memory
Reading from 6903: heap size 1311 MB, throughput 0.985979
Reading from 6903: heap size 1373 MB, throughput 0.985745
Recommendation: one client; give it all the memory
Reading from 6903: heap size 1377 MB, throughput 0.983563
Reading from 6903: heap size 1379 MB, throughput 0.983715
Reading from 6903: heap size 1380 MB, throughput 0.982447
Recommendation: one client; give it all the memory
Reading from 6903: heap size 1382 MB, throughput 0.981976
Reading from 6903: heap size 1387 MB, throughput 0.980052
Reading from 6903: heap size 1391 MB, throughput 0.976816
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 6903: heap size 1398 MB, throughput 0.991275
Recommendation: one client; give it all the memory
Reading from 6903: heap size 1377 MB, throughput 0.989633
Recommendation: one client; give it all the memory
Reading from 6903: heap size 1410 MB, throughput 0.981638
Reading from 6903: heap size 1442 MB, throughput 0.861606
Reading from 6903: heap size 1451 MB, throughput 0.744308
Reading from 6903: heap size 1434 MB, throughput 0.174728
Reading from 6903: heap size 1476 MB, throughput 0.991765
Reading from 6903: heap size 1511 MB, throughput 0.992521
Reading from 6903: heap size 1512 MB, throughput 0.992386
Reading from 6903: heap size 1526 MB, throughput 0.993067
Reading from 6903: heap size 1529 MB, throughput 0.992029
Reading from 6903: heap size 1540 MB, throughput 0.992096
Client 6903 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
