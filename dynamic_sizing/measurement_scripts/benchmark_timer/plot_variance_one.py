import sys
import matplotlib.pyplot as plt

outfile = sys.argv[2]
benchmark = sys.argv[1].strip()
variance = float(sys.argv[3])

print "Plotting %s..." % benchmark

data = []
for line in sys.stdin:
    data.append(float(line))

plt.plot(range(1, len(data) + 1), data)
plt.xlabel("Execution")
plt.ylabel("Runtime (s)")
plt.title("%s (variance: %f)" % (benchmark, variance))
(_, ymax) = plt.ylim()
plt.ylim(0, ymax * 1.1)
plt.savefig(outfile)
