economemd
    total memory: 2506 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub21_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 4822: heap size 9 MB, throughput 0.990552
Clients: 1
Client 4822 has a minimum heap size of 30 MB
Reading from 4821: heap size 9 MB, throughput 0.986904
Clients: 2
Client 4821 has a minimum heap size of 1223 MB
Reading from 4822: heap size 9 MB, throughput 0.97603
Reading from 4822: heap size 9 MB, throughput 0.970708
Reading from 4821: heap size 9 MB, throughput 0.976482
Reading from 4822: heap size 9 MB, throughput 0.958356
Reading from 4822: heap size 11 MB, throughput 0.594056
Reading from 4822: heap size 11 MB, throughput 0.584003
Reading from 4822: heap size 16 MB, throughput 0.414212
Reading from 4822: heap size 16 MB, throughput 0.700648
Reading from 4822: heap size 24 MB, throughput 0.871127
Reading from 4822: heap size 24 MB, throughput 0.819907
Reading from 4822: heap size 34 MB, throughput 0.88172
Reading from 4822: heap size 34 MB, throughput 0.877755
Reading from 4822: heap size 50 MB, throughput 0.942926
Reading from 4822: heap size 50 MB, throughput 0.939946
Reading from 4822: heap size 75 MB, throughput 0.961908
Reading from 4821: heap size 11 MB, throughput 0.978674
Reading from 4822: heap size 75 MB, throughput 0.958643
Reading from 4822: heap size 116 MB, throughput 0.96726
Reading from 4822: heap size 116 MB, throughput 0.881762
Reading from 4821: heap size 11 MB, throughput 0.983862
Reading from 4821: heap size 15 MB, throughput 0.895809
Reading from 4821: heap size 18 MB, throughput 0.958676
Reading from 4821: heap size 24 MB, throughput 0.896811
Reading from 4821: heap size 28 MB, throughput 0.818957
Reading from 4821: heap size 33 MB, throughput 0.34609
Reading from 4821: heap size 43 MB, throughput 0.851756
Reading from 4822: heap size 160 MB, throughput 0.969262
Reading from 4821: heap size 50 MB, throughput 0.883272
Reading from 4821: heap size 53 MB, throughput 0.370908
Reading from 4821: heap size 71 MB, throughput 0.744164
Reading from 4821: heap size 74 MB, throughput 0.827414
Reading from 4822: heap size 166 MB, throughput 0.7916
Reading from 4821: heap size 78 MB, throughput 0.153598
Reading from 4821: heap size 101 MB, throughput 0.796936
Reading from 4821: heap size 108 MB, throughput 0.772681
Reading from 4822: heap size 178 MB, throughput 0.984672
Reading from 4821: heap size 111 MB, throughput 0.203802
Reading from 4821: heap size 143 MB, throughput 0.678856
Reading from 4821: heap size 145 MB, throughput 0.586973
Reading from 4821: heap size 149 MB, throughput 0.551721
Reading from 4821: heap size 154 MB, throughput 0.562501
Reading from 4821: heap size 161 MB, throughput 0.648156
Reading from 4821: heap size 168 MB, throughput 0.529718
Reading from 4822: heap size 210 MB, throughput 0.893819
Reading from 4821: heap size 176 MB, throughput 0.185375
Reading from 4821: heap size 217 MB, throughput 0.657124
Reading from 4822: heap size 239 MB, throughput 0.994649
Reading from 4821: heap size 225 MB, throughput 0.71292
Reading from 4821: heap size 226 MB, throughput 0.725795
Reading from 4821: heap size 230 MB, throughput 0.723164
Reading from 4821: heap size 236 MB, throughput 0.609973
Reading from 4821: heap size 241 MB, throughput 0.167785
Reading from 4822: heap size 241 MB, throughput 0.908666
Reading from 4821: heap size 288 MB, throughput 0.49449
Reading from 4821: heap size 299 MB, throughput 0.55874
Reading from 4821: heap size 303 MB, throughput 0.616626
Reading from 4821: heap size 306 MB, throughput 0.64422
Reading from 4822: heap size 257 MB, throughput 0.979598
Reading from 4821: heap size 314 MB, throughput 0.0797148
Reading from 4821: heap size 360 MB, throughput 0.512031
Reading from 4822: heap size 283 MB, throughput 0.924919
Reading from 4821: heap size 368 MB, throughput 0.552685
Reading from 4821: heap size 370 MB, throughput 0.530097
Reading from 4821: heap size 370 MB, throughput 0.120177
Reading from 4821: heap size 422 MB, throughput 0.439752
Reading from 4822: heap size 284 MB, throughput 0.996811
Reading from 4821: heap size 423 MB, throughput 0.564635
Equal recommendation: 1253 MB each
Reading from 4821: heap size 424 MB, throughput 0.60774
Reading from 4821: heap size 427 MB, throughput 0.569902
Reading from 4821: heap size 433 MB, throughput 0.532036
Reading from 4822: heap size 301 MB, throughput 0.985378
Reading from 4821: heap size 439 MB, throughput 0.518421
Reading from 4822: heap size 301 MB, throughput 0.988196
Reading from 4821: heap size 446 MB, throughput 0.0971734
Reading from 4821: heap size 501 MB, throughput 0.525807
Reading from 4821: heap size 513 MB, throughput 0.636126
Reading from 4821: heap size 514 MB, throughput 0.598894
Reading from 4821: heap size 517 MB, throughput 0.532931
Reading from 4821: heap size 525 MB, throughput 0.517368
Reading from 4821: heap size 533 MB, throughput 0.577904
Reading from 4822: heap size 322 MB, throughput 0.992487
Reading from 4822: heap size 323 MB, throughput 0.96702
Reading from 4821: heap size 540 MB, throughput 0.0994494
Reading from 4821: heap size 595 MB, throughput 0.455264
Reading from 4821: heap size 605 MB, throughput 0.564185
Reading from 4822: heap size 339 MB, throughput 0.849972
Reading from 4821: heap size 612 MB, throughput 0.105428
Reading from 4821: heap size 672 MB, throughput 0.434515
Reading from 4821: heap size 667 MB, throughput 0.584944
Reading from 4821: heap size 673 MB, throughput 0.434482
Reading from 4822: heap size 369 MB, throughput 0.991387
Reading from 4821: heap size 678 MB, throughput 0.811982
Reading from 4822: heap size 381 MB, throughput 0.990887
Reading from 4821: heap size 680 MB, throughput 0.852318
Reading from 4822: heap size 386 MB, throughput 0.985414
Equal recommendation: 1253 MB each
Reading from 4822: heap size 405 MB, throughput 0.977382
Reading from 4821: heap size 679 MB, throughput 0.336948
Reading from 4821: heap size 755 MB, throughput 0.437691
Reading from 4821: heap size 772 MB, throughput 0.612867
Reading from 4821: heap size 774 MB, throughput 0.466831
Reading from 4821: heap size 769 MB, throughput 0.423459
Reading from 4822: heap size 406 MB, throughput 0.97184
Reading from 4821: heap size 774 MB, throughput 0.0610242
Reading from 4821: heap size 833 MB, throughput 0.713303
Reading from 4821: heap size 838 MB, throughput 0.485492
Reading from 4822: heap size 430 MB, throughput 0.98487
Reading from 4821: heap size 837 MB, throughput 0.544631
Reading from 4821: heap size 840 MB, throughput 0.839573
Reading from 4821: heap size 844 MB, throughput 0.823398
Reading from 4822: heap size 432 MB, throughput 0.982832
Reading from 4821: heap size 846 MB, throughput 0.822189
Reading from 4822: heap size 457 MB, throughput 0.979643
Reading from 4821: heap size 853 MB, throughput 0.187387
Reading from 4821: heap size 929 MB, throughput 0.789616
Reading from 4821: heap size 924 MB, throughput 0.794199
Reading from 4822: heap size 460 MB, throughput 0.979092
Reading from 4821: heap size 931 MB, throughput 0.149617
Reading from 4822: heap size 483 MB, throughput 0.982212
Reading from 4821: heap size 1031 MB, throughput 0.577104
Reading from 4821: heap size 1033 MB, throughput 0.810038
Reading from 4821: heap size 1043 MB, throughput 0.843518
Reading from 4821: heap size 1045 MB, throughput 0.843063
Reading from 4821: heap size 1049 MB, throughput 0.840177
Reading from 4822: heap size 487 MB, throughput 0.981843
Equal recommendation: 1253 MB each
Reading from 4821: heap size 1051 MB, throughput 0.731929
Reading from 4822: heap size 509 MB, throughput 0.99148
Reading from 4821: heap size 1051 MB, throughput 0.96153
Reading from 4821: heap size 1053 MB, throughput 0.850074
Reading from 4822: heap size 512 MB, throughput 0.98125
Reading from 4821: heap size 1053 MB, throughput 0.773672
Reading from 4821: heap size 1054 MB, throughput 0.786112
Reading from 4821: heap size 1055 MB, throughput 0.786582
Reading from 4821: heap size 1058 MB, throughput 0.729846
Reading from 4821: heap size 1060 MB, throughput 0.752424
Reading from 4822: heap size 532 MB, throughput 0.990772
Reading from 4821: heap size 1062 MB, throughput 0.778175
Reading from 4821: heap size 1058 MB, throughput 0.843375
Reading from 4821: heap size 1064 MB, throughput 0.908793
Reading from 4821: heap size 1067 MB, throughput 0.70657
Reading from 4822: heap size 535 MB, throughput 0.983887
Reading from 4821: heap size 1070 MB, throughput 0.635848
Reading from 4821: heap size 1085 MB, throughput 0.614438
Reading from 4821: heap size 1093 MB, throughput 0.613687
Reading from 4821: heap size 1110 MB, throughput 0.613999
Reading from 4822: heap size 555 MB, throughput 0.988531
Reading from 4821: heap size 1117 MB, throughput 0.0960121
Reading from 4821: heap size 1236 MB, throughput 0.604445
Reading from 4822: heap size 557 MB, throughput 0.99096
Reading from 4821: heap size 1244 MB, throughput 0.753963
Reading from 4821: heap size 1260 MB, throughput 0.790302
Equal recommendation: 1253 MB each
Reading from 4822: heap size 574 MB, throughput 0.984694
Reading from 4821: heap size 1136 MB, throughput 0.972027
Reading from 4822: heap size 577 MB, throughput 0.994932
Reading from 4822: heap size 594 MB, throughput 0.989771
Reading from 4821: heap size 1260 MB, throughput 0.979613
Reading from 4822: heap size 597 MB, throughput 0.990376
Reading from 4822: heap size 613 MB, throughput 0.99411
Reading from 4821: heap size 1144 MB, throughput 0.976872
Reading from 4822: heap size 616 MB, throughput 0.987209
Equal recommendation: 1253 MB each
Reading from 4822: heap size 635 MB, throughput 0.995752
Reading from 4821: heap size 1252 MB, throughput 0.973863
Reading from 4822: heap size 635 MB, throughput 0.9943
Reading from 4821: heap size 1246 MB, throughput 0.967602
Reading from 4822: heap size 648 MB, throughput 0.982435
Reading from 4822: heap size 651 MB, throughput 0.990622
Reading from 4821: heap size 1237 MB, throughput 0.97671
Reading from 4822: heap size 669 MB, throughput 0.995375
Equal recommendation: 1253 MB each
Reading from 4822: heap size 671 MB, throughput 0.98791
Reading from 4821: heap size 1244 MB, throughput 0.971632
Reading from 4822: heap size 688 MB, throughput 0.99262
Reading from 4822: heap size 689 MB, throughput 0.996391
Reading from 4821: heap size 1246 MB, throughput 0.970193
Reading from 4822: heap size 702 MB, throughput 0.989619
Reading from 4821: heap size 1249 MB, throughput 0.974184
Reading from 4822: heap size 705 MB, throughput 0.988133
Equal recommendation: 1253 MB each
Reading from 4822: heap size 723 MB, throughput 0.988884
Reading from 4821: heap size 1242 MB, throughput 0.970564
Reading from 4822: heap size 725 MB, throughput 0.99287
Reading from 4822: heap size 743 MB, throughput 0.99435
Reading from 4821: heap size 1247 MB, throughput 0.967501
Reading from 4822: heap size 745 MB, throughput 0.990003
Equal recommendation: 1253 MB each
Reading from 4821: heap size 1240 MB, throughput 0.969954
Reading from 4822: heap size 765 MB, throughput 0.984554
Reading from 4822: heap size 765 MB, throughput 0.988814
Reading from 4821: heap size 1244 MB, throughput 0.965384
Reading from 4822: heap size 792 MB, throughput 0.99049
Reading from 4821: heap size 1248 MB, throughput 0.968196
Reading from 4822: heap size 793 MB, throughput 0.991961
Equal recommendation: 1253 MB each
Reading from 4822: heap size 817 MB, throughput 0.991867
Reading from 4821: heap size 1248 MB, throughput 0.969025
Reading from 4822: heap size 818 MB, throughput 0.995388
Reading from 4822: heap size 836 MB, throughput 0.991926
Reading from 4821: heap size 1253 MB, throughput 0.972028
Reading from 4822: heap size 840 MB, throughput 0.989664
Equal recommendation: 1253 MB each
Reading from 4821: heap size 1225 MB, throughput 0.965128
Reading from 4822: heap size 858 MB, throughput 0.992708
Reading from 4822: heap size 858 MB, throughput 0.989034
Reading from 4821: heap size 1253 MB, throughput 0.970415
Reading from 4822: heap size 858 MB, throughput 0.990659
Reading from 4822: heap size 858 MB, throughput 0.983078
Reading from 4821: heap size 1255 MB, throughput 0.648201
Equal recommendation: 1253 MB each
Reading from 4822: heap size 856 MB, throughput 0.987688
Reading from 4821: heap size 1306 MB, throughput 0.992563
Reading from 4822: heap size 857 MB, throughput 0.995385
Reading from 4821: heap size 1311 MB, throughput 0.991012
Reading from 4822: heap size 856 MB, throughput 0.992142
Equal recommendation: 1253 MB each
Reading from 4822: heap size 857 MB, throughput 0.995231
Reading from 4821: heap size 1316 MB, throughput 0.987872
Reading from 4822: heap size 857 MB, throughput 0.991675
Reading from 4821: heap size 1176 MB, throughput 0.988624
Reading from 4822: heap size 858 MB, throughput 0.994539
Reading from 4821: heap size 1322 MB, throughput 0.982879
Reading from 4822: heap size 858 MB, throughput 0.992339
Equal recommendation: 1253 MB each
Reading from 4822: heap size 858 MB, throughput 0.992536
Reading from 4821: heap size 1190 MB, throughput 0.982345
Reading from 4822: heap size 859 MB, throughput 0.991875
Reading from 4821: heap size 1309 MB, throughput 0.982734
Reading from 4822: heap size 859 MB, throughput 0.992408
Reading from 4822: heap size 860 MB, throughput 0.993464
Reading from 4821: heap size 1206 MB, throughput 0.982359
Equal recommendation: 1253 MB each
Reading from 4822: heap size 860 MB, throughput 0.996378
Reading from 4821: heap size 1293 MB, throughput 0.979903
Reading from 4822: heap size 861 MB, throughput 0.989208
Reading from 4821: heap size 1222 MB, throughput 0.978552
Reading from 4822: heap size 861 MB, throughput 0.990163
Reading from 4822: heap size 859 MB, throughput 0.991899
Equal recommendation: 1253 MB each
Reading from 4821: heap size 1290 MB, throughput 0.977315
Reading from 4822: heap size 860 MB, throughput 0.990319
Reading from 4821: heap size 1237 MB, throughput 0.976351
Reading from 4822: heap size 859 MB, throughput 0.994582
Reading from 4822: heap size 860 MB, throughput 0.995071
Reading from 4821: heap size 1284 MB, throughput 0.971985
Equal recommendation: 1253 MB each
Reading from 4822: heap size 860 MB, throughput 0.991468
Reading from 4821: heap size 1283 MB, throughput 0.970823
Reading from 4822: heap size 860 MB, throughput 0.990913
Reading from 4821: heap size 1282 MB, throughput 0.96821
Reading from 4822: heap size 860 MB, throughput 0.99042
Reading from 4822: heap size 855 MB, throughput 0.991542
Reading from 4821: heap size 1285 MB, throughput 0.965206
Equal recommendation: 1253 MB each
Reading from 4822: heap size 858 MB, throughput 0.991864
Reading from 4821: heap size 1288 MB, throughput 0.968159
Reading from 4822: heap size 859 MB, throughput 0.990922
Reading from 4821: heap size 1290 MB, throughput 0.967121
Reading from 4822: heap size 858 MB, throughput 0.991996
Reading from 4822: heap size 858 MB, throughput 0.989455
Reading from 4821: heap size 1290 MB, throughput 0.966107
Equal recommendation: 1253 MB each
Reading from 4822: heap size 859 MB, throughput 0.995196
Reading from 4821: heap size 1238 MB, throughput 0.970266
Reading from 4822: heap size 859 MB, throughput 0.992093
Reading from 4821: heap size 1291 MB, throughput 0.96462
Reading from 4822: heap size 860 MB, throughput 0.996875
Reading from 4821: heap size 1234 MB, throughput 0.968488
Reading from 4822: heap size 838 MB, throughput 0.992948
Equal recommendation: 1253 MB each
Reading from 4822: heap size 859 MB, throughput 0.992281
Reading from 4821: heap size 1288 MB, throughput 0.957468
Reading from 4822: heap size 841 MB, throughput 0.992026
Reading from 4821: heap size 1232 MB, throughput 0.958096
Reading from 4822: heap size 859 MB, throughput 0.99597
Equal recommendation: 1253 MB each
Reading from 4822: heap size 858 MB, throughput 0.990638
Reading from 4822: heap size 859 MB, throughput 0.990156
Reading from 4822: heap size 851 MB, throughput 0.990167
Reading from 4822: heap size 859 MB, throughput 0.988688
Reading from 4822: heap size 859 MB, throughput 0.990448
Equal recommendation: 1253 MB each
Reading from 4822: heap size 858 MB, throughput 0.991548
Reading from 4822: heap size 859 MB, throughput 0.983536
Reading from 4821: heap size 1285 MB, throughput 0.843164
Reading from 4822: heap size 859 MB, throughput 0.991888
Reading from 4822: heap size 859 MB, throughput 0.990046
Equal recommendation: 1253 MB each
Reading from 4822: heap size 858 MB, throughput 0.990664
Reading from 4822: heap size 859 MB, throughput 0.990077
Reading from 4822: heap size 858 MB, throughput 0.990307
Reading from 4822: heap size 859 MB, throughput 0.984733
Reading from 4821: heap size 1421 MB, throughput 0.984831
Equal recommendation: 1253 MB each
Reading from 4822: heap size 860 MB, throughput 0.990956
Reading from 4822: heap size 851 MB, throughput 0.990123
Reading from 4821: heap size 1448 MB, throughput 0.987051
Reading from 4822: heap size 859 MB, throughput 0.990559
Reading from 4821: heap size 1383 MB, throughput 0.843467
Reading from 4821: heap size 1432 MB, throughput 0.766361
Reading from 4821: heap size 1438 MB, throughput 0.672538
Reading from 4821: heap size 1448 MB, throughput 0.716094
Reading from 4821: heap size 1460 MB, throughput 0.678623
Reading from 4821: heap size 1463 MB, throughput 0.700116
Reading from 4821: heap size 1469 MB, throughput 0.689459
Reading from 4821: heap size 1472 MB, throughput 0.68403
Reading from 4822: heap size 860 MB, throughput 0.984281
Reading from 4821: heap size 1399 MB, throughput 0.662839
Reading from 4821: heap size 1469 MB, throughput 0.185308
Equal recommendation: 1253 MB each
Reading from 4822: heap size 861 MB, throughput 0.992089
Reading from 4821: heap size 1323 MB, throughput 0.990628
Reading from 4822: heap size 861 MB, throughput 0.991468
Reading from 4821: heap size 1320 MB, throughput 0.986624
Reading from 4822: heap size 861 MB, throughput 0.993056
Reading from 4821: heap size 1329 MB, throughput 0.98832
Reading from 4822: heap size 844 MB, throughput 0.996565
Reading from 4821: heap size 1333 MB, throughput 0.982987
Equal recommendation: 1253 MB each
Reading from 4822: heap size 860 MB, throughput 0.993117
Reading from 4821: heap size 1080 MB, throughput 0.986661
Reading from 4822: heap size 860 MB, throughput 0.990089
Reading from 4821: heap size 1336 MB, throughput 0.985452
Client 4822 died
Clients: 1
Reading from 4821: heap size 1096 MB, throughput 0.987417
Recommendation: one client; give it all the memory
Reading from 4821: heap size 1320 MB, throughput 0.98621
Reading from 4821: heap size 1117 MB, throughput 0.986256
Reading from 4821: heap size 1293 MB, throughput 0.984913
Reading from 4821: heap size 1136 MB, throughput 0.984331
Recommendation: one client; give it all the memory
Reading from 4821: heap size 1273 MB, throughput 0.982824
Reading from 4821: heap size 1153 MB, throughput 0.98131
Reading from 4821: heap size 1258 MB, throughput 0.983117
Recommendation: one client; give it all the memory
Reading from 4821: heap size 1171 MB, throughput 0.980807
Reading from 4821: heap size 1257 MB, throughput 0.978933
Reading from 4821: heap size 1262 MB, throughput 0.979891
Recommendation: one client; give it all the memory
Reading from 4821: heap size 1256 MB, throughput 0.979392
Reading from 4821: heap size 1260 MB, throughput 0.98024
Reading from 4821: heap size 1257 MB, throughput 0.978978
Recommendation: one client; give it all the memory
Reading from 4821: heap size 1260 MB, throughput 0.979485
Reading from 4821: heap size 1259 MB, throughput 0.978467
Reading from 4821: heap size 1261 MB, throughput 0.980298
Recommendation: one client; give it all the memory
Reading from 4821: heap size 1261 MB, throughput 0.98077
Reading from 4821: heap size 1263 MB, throughput 0.97867
Reading from 4821: heap size 1266 MB, throughput 0.979607
Recommendation: one client; give it all the memory
Reading from 4821: heap size 1267 MB, throughput 0.979063
Reading from 4821: heap size 1271 MB, throughput 0.978796
Recommendation: one client; give it all the memory
Reading from 4821: heap size 1272 MB, throughput 0.979823
Reading from 4821: heap size 1276 MB, throughput 0.980095
Reading from 4821: heap size 1277 MB, throughput 0.979928
Recommendation: one client; give it all the memory
Reading from 4821: heap size 1283 MB, throughput 0.979907
Reading from 4821: heap size 1283 MB, throughput 0.980045
Recommendation: one client; give it all the memory
Reading from 4821: heap size 1289 MB, throughput 0.791901
Reading from 4821: heap size 1374 MB, throughput 0.995317
Reading from 4821: heap size 1370 MB, throughput 0.993872
Recommendation: one client; give it all the memory
Reading from 4821: heap size 1381 MB, throughput 0.992646
Reading from 4821: heap size 1390 MB, throughput 0.992379
Recommendation: one client; give it all the memory
Reading from 4821: heap size 1392 MB, throughput 0.990099
Reading from 4821: heap size 1388 MB, throughput 0.988352
Reading from 4821: heap size 1286 MB, throughput 0.987461
Recommendation: one client; give it all the memory
Reading from 4821: heap size 1379 MB, throughput 0.987892
Reading from 4821: heap size 1307 MB, throughput 0.985409
Recommendation: one client; give it all the memory
Reading from 4821: heap size 1375 MB, throughput 0.985806
Reading from 4821: heap size 1379 MB, throughput 0.983978
Recommendation: one client; give it all the memory
Reading from 4821: heap size 1381 MB, throughput 0.982937
Reading from 4821: heap size 1383 MB, throughput 0.96977
Recommendation: one client; give it all the memory
Reading from 4821: heap size 1387 MB, throughput 0.899211
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 4821: heap size 1538 MB, throughput 0.985996
Reading from 4821: heap size 1589 MB, throughput 0.986552
Reading from 4821: heap size 1594 MB, throughput 0.848438
Reading from 4821: heap size 1583 MB, throughput 0.769137
Reading from 4821: heap size 1604 MB, throughput 0.749538
Reading from 4821: heap size 1636 MB, throughput 0.78157
Reading from 4821: heap size 1649 MB, throughput 0.776036
Reading from 4821: heap size 1659 MB, throughput 0.794399
Reading from 4821: heap size 1578 MB, throughput 0.856036
Client 4821 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
