#ifndef COMMANDRESULTRECEIVER_HPP
#define COMMANDRESULTRECEIVER_HPP

#include "Messages.hpp"
#include "RecvSock.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
    //! Receive the result of a command from a daemon (for use in management programs).
    class CommandResultReceiver : public RecvSock<economem::CommandResult>
    {
    public:
        CommandResultReceiver();
    };
}

/*! @} */

#endif
