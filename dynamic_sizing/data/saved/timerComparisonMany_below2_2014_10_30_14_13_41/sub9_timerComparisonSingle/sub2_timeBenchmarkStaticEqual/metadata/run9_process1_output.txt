Using scaled threading model. 4 processors detected, 2 threads used to drive the workload, in a possible range of [1,64000]
===== DaCapo 9.12 h2 starting warmup 1 =====
...........................Exception in thread "OE_Thread:1" java.lang.OutOfMemoryError: GC overhead limit exceeded
	at org.h2.table.Table.getTemplateRow(Table.java:473)
	at org.h2.index.IndexCursor.getSearchRow(IndexCursor.java:115)
	at org.h2.index.IndexCursor.find(IndexCursor.java:101)
	at org.h2.table.TableFilter.next(TableFilter.java:255)
	at org.h2.table.TableFilter.next(TableFilter.java:310)
	at org.h2.command.dml.Select.queryGroup(Select.java:307)
	at org.h2.command.dml.Select.queryWithoutCache(Select.java:551)
	at org.h2.command.dml.Query.query(Query.java:236)
	at org.h2.command.CommandContainer.query(CommandContainer.java:82)
	at org.h2.command.Command.executeQueryLocal(Command.java:142)
	at org.h2.command.Command.executeQuery(Command.java:123)
	at org.h2.jdbc.JdbcPreparedStatement.executeQuery(JdbcPreparedStatement.java:98)
	at org.apache.derbyTesting.system.oe.direct.Standard.stockLevel(Standard.java:149)
	at org.apache.derbyTesting.system.oe.client.Submitter.runStockLevel(Submitter.java:477)
	at org.dacapo.h2.TPCCSubmitter.runTransaction(TPCCSubmitter.java:95)
	at org.dacapo.h2.TPCCSubmitter.runTransactions(TPCCSubmitter.java:71)
	at org.dacapo.h2.TPCC$3.run(TPCC.java:723)
Exception in thread "OE_Thread:0" java.lang.OutOfMemoryError: GC overhead limit exceeded

Completed 27040 transactions
	Stock level .............  1057 ( 3.9%)
	Order status by name ....   650 ( 2.4%)
	Order status by ID ......   434 ( 1.6%)
	Payment by name .........  7019 (26.0%)
	Payment by ID ...........  4616 (17.1%)
	Delivery schedule .......  1100 ( 4.1%)
	New order ............... 12039 (44.5%)
	New order rollback ......   125 ( 0.5%)
Resetting database to initial state
java.lang.reflect.InvocationTargetException
java.lang.reflect.InvocationTargetException
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:483)
	at org.dacapo.harness.H2.iterate(H2.java:87)
	at org.dacapo.harness.Benchmark.run(Benchmark.java:166)
	at org.dacapo.harness.TestHarness.runBenchmark(TestHarness.java:218)
	at org.dacapo.harness.TestHarness.main(TestHarness.java:171)
	at Harness.main(Harness.java:17)
Caused by: org.h2.jdbc.JdbcSQLException: Out of memory.; SQL statement:
DELETE FROM HISTORY WHERE H_INITIAL = FALSE [90108-123]
	at org.h2.message.Message.getSQLException(Message.java:111)
	at org.h2.message.Message.convertThrowable(Message.java:303)
	at org.h2.table.TableData.removeRow(TableData.java:392)
	at org.h2.command.dml.Delete.update(Delete.java:71)
	at org.h2.command.CommandContainer.update(CommandContainer.java:72)
	at org.h2.command.Command.executeUpdate(Command.java:209)
	at org.h2.jdbc.JdbcPreparedStatement.execute(JdbcPreparedStatement.java:176)
	at org.dacapo.h2.TPCC.resetToInitialData(TPCC.java:483)
	at org.dacapo.h2.TPCC.iteration(TPCC.java:310)
	... 9 more
Caused by: java.lang.OutOfMemoryError: GC overhead limit exceeded
