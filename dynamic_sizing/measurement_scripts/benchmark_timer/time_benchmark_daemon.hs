{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, DeriveDataTypeable #-}
{-# OPTIONS_GHC -fno-warn-missing-fields -fno-warn-missing-methods #-}

import Economem.ArgHelper
import qualified Economem.BenchmarkTimer as B
import Economem.Common
import Economem.Daemon
import Economem.Experiment
import Economem.Files
import Economem.Java
import Economem.Records
import Economem.Signals
import Economem.DaemonBenchmarkTimer
import System.Console.CmdLib

data Args = Args { inputFile :: FilePath,
                   totalMemMB :: Int,
                   minHeapsFile :: FilePath,
                   equal :: Bool,
                   throughputType :: String,
                   runs :: Int,
                   java :: String }
          deriving (Typeable, Data, Eq, Show)

instance Attributes Args where
  attributes _ = group "Options" [
    inputFile %> [ Positional 0,
                   Required True,
                   Help "Specifies which benchmarks to run",
                   ArgHelp "INPUT_FILE" ],
    totalMemMB %> [ Positional 1,
                    Required True,
                    Help "How much memory the daemon should manage",
                    ArgHelp "MB" ],
    minHeapsFile %> [ Short ['m'],
                      Long ["min-heaps-file"],
                      Default "",
                      Help "Specify a min heaps file to use",
                      ArgHelp "MIN_HEAPS_FILE" ],
    equal %> [ Short ['e'],
               Long ["equal"],
               Default False,
               Help "If true, the daemon will always recommend an equal split of memory rather than using economics." ],
    throughputType %> [ Short ['t'],
                       Long ["throughput-type"],
                       Default $ throughputSocketName $ head (javaDefaultThroughputs defaultJava),
                       Help $ "Throughput type to use in the VM; available options: " ++ show validThroughputNames,
                       ArgHelp "THROUGHPUT" ],
    runs %> [ Short ['r'],
              Long ["runs"],
              Default (fromIntegral B.defaultRuns::Int),
              Help "How many times to repeat, for taking the average",
              ArgHelp "RUNS" ],
    java %> [ Short ['j'],
              Long ["java"],
              Default (javaName defaultJava),
              Help ("Java version to use; available options: " ++ show validJavaNames),
              ArgHelp "JAVA" ]]

instance RecordCommand Args where
  mode_summary _ = "Run multiple benchmark VMs with a daemon and see how long it takes for them all to finish."

main :: IO ()
main = getArgs >>= executeR Args {} >>= \opts -> do
  (i, th, s, r, j, t, m) <- validate opts
  b <- newDaemonBenchmarkTimer i th s r j t m False False Nothing []
  h <- makeSignalHandler
  runExperiment b h

  where
    validate :: Args -> IO (Input BenchmarkThreadsIters,
                            DataSize,
                            ThroughputType,
                            Integer,
                            Java,
                            RecommendationType,
                            Maybe (Input BenchmarkThreadsMB))
    validate a = do
      i <- existingAbsoluteFile (inputFile a)
      m <- positive (totalMemMB a)
      r <- positive (runs a)
      j <- javaFromName (java a)
      th <- throughputFromName (throughputType a)
      return (fromFile i,
              DataSize (fromIntegral m) MB,
              th,
              fromIntegral r,
              j,
              if equal a then Equal else Normal,
              case minHeapsFile a of
               "" -> Nothing
               x -> Just (fromFile x))
