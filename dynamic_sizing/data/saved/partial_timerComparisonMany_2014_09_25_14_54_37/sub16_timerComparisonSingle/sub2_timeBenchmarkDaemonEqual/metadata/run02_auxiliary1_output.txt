economemd
    total memory: 4892 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub16_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 29081: heap size 9 MB, throughput 0.980923
Clients: 1
Client 29081 has a minimum heap size of 12 MB
Reading from 29080: heap size 9 MB, throughput 0.991676
Clients: 2
Client 29080 has a minimum heap size of 1211 MB
Reading from 29080: heap size 9 MB, throughput 0.982054
Reading from 29081: heap size 9 MB, throughput 0.982045
Reading from 29080: heap size 9 MB, throughput 0.96474
Reading from 29080: heap size 9 MB, throughput 0.950107
Reading from 29081: heap size 11 MB, throughput 0.982182
Reading from 29081: heap size 11 MB, throughput 0.965862
Reading from 29080: heap size 11 MB, throughput 0.988441
Reading from 29080: heap size 11 MB, throughput 0.960299
Reading from 29081: heap size 15 MB, throughput 0.989507
Reading from 29080: heap size 17 MB, throughput 0.925923
Reading from 29080: heap size 17 MB, throughput 0.656953
Reading from 29081: heap size 15 MB, throughput 0.974109
Reading from 29080: heap size 29 MB, throughput 0.942618
Reading from 29080: heap size 30 MB, throughput 0.805209
Reading from 29081: heap size 24 MB, throughput 0.979183
Reading from 29080: heap size 35 MB, throughput 0.455887
Reading from 29080: heap size 49 MB, throughput 0.881027
Reading from 29080: heap size 51 MB, throughput 0.904014
Reading from 29081: heap size 25 MB, throughput 0.903412
Reading from 29080: heap size 54 MB, throughput 0.270692
Reading from 29080: heap size 77 MB, throughput 0.764491
Reading from 29080: heap size 77 MB, throughput 0.290455
Reading from 29081: heap size 44 MB, throughput 0.992311
Reading from 29080: heap size 98 MB, throughput 0.778634
Reading from 29080: heap size 101 MB, throughput 0.844476
Reading from 29081: heap size 46 MB, throughput 0.989552
Reading from 29081: heap size 52 MB, throughput 0.961999
Reading from 29080: heap size 102 MB, throughput 0.27126
Reading from 29081: heap size 53 MB, throughput 0.940967
Reading from 29080: heap size 131 MB, throughput 0.659135
Reading from 29080: heap size 134 MB, throughput 0.849741
Reading from 29080: heap size 135 MB, throughput 0.70906
Reading from 29081: heap size 65 MB, throughput 0.965632
Reading from 29081: heap size 64 MB, throughput 0.985093
Reading from 29081: heap size 80 MB, throughput 0.992522
Reading from 29080: heap size 143 MB, throughput 0.153709
Reading from 29080: heap size 172 MB, throughput 0.664242
Reading from 29081: heap size 81 MB, throughput 0.986751
Reading from 29080: heap size 180 MB, throughput 0.768778
Reading from 29080: heap size 182 MB, throughput 0.813559
Reading from 29080: heap size 183 MB, throughput 0.15964
Reading from 29081: heap size 98 MB, throughput 0.997413
Reading from 29080: heap size 221 MB, throughput 0.763847
Reading from 29080: heap size 224 MB, throughput 0.826332
Reading from 29080: heap size 229 MB, throughput 0.686798
Reading from 29080: heap size 238 MB, throughput 0.669082
Reading from 29080: heap size 242 MB, throughput 0.66791
Reading from 29080: heap size 245 MB, throughput 0.534602
Reading from 29081: heap size 99 MB, throughput 0.997541
Reading from 29080: heap size 253 MB, throughput 0.584281
Reading from 29080: heap size 257 MB, throughput 0.543951
Reading from 29081: heap size 110 MB, throughput 0.99737
Reading from 29080: heap size 268 MB, throughput 0.107924
Reading from 29080: heap size 314 MB, throughput 0.461367
Reading from 29080: heap size 323 MB, throughput 0.476046
Reading from 29081: heap size 112 MB, throughput 0.996122
Reading from 29080: heap size 330 MB, throughput 0.116327
Reading from 29080: heap size 376 MB, throughput 0.474136
Reading from 29080: heap size 369 MB, throughput 0.615762
Reading from 29080: heap size 375 MB, throughput 0.659703
Reading from 29080: heap size 376 MB, throughput 0.644413
Reading from 29080: heap size 377 MB, throughput 0.497125
Reading from 29080: heap size 378 MB, throughput 0.621888
Reading from 29081: heap size 121 MB, throughput 0.997275
Reading from 29080: heap size 384 MB, throughput 0.507682
Reading from 29081: heap size 123 MB, throughput 0.996464
Reading from 29080: heap size 390 MB, throughput 0.0972591
Reading from 29080: heap size 442 MB, throughput 0.510675
Reading from 29080: heap size 446 MB, throughput 0.558612
Reading from 29080: heap size 448 MB, throughput 0.614401
Equal recommendation: 2446 MB each
Reading from 29080: heap size 450 MB, throughput 0.558498
Reading from 29080: heap size 455 MB, throughput 0.501428
Reading from 29081: heap size 130 MB, throughput 0.997617
Reading from 29080: heap size 458 MB, throughput 0.49799
Reading from 29080: heap size 465 MB, throughput 0.436161
Reading from 29080: heap size 470 MB, throughput 0.469426
Reading from 29081: heap size 132 MB, throughput 0.996863
Reading from 29080: heap size 477 MB, throughput 0.0724267
Reading from 29080: heap size 539 MB, throughput 0.498638
Reading from 29081: heap size 140 MB, throughput 0.997664
Reading from 29080: heap size 539 MB, throughput 0.55
Reading from 29080: heap size 537 MB, throughput 0.562908
Reading from 29081: heap size 140 MB, throughput 0.997897
Reading from 29080: heap size 539 MB, throughput 0.0828795
Reading from 29080: heap size 601 MB, throughput 0.492642
Reading from 29080: heap size 601 MB, throughput 0.636354
Reading from 29080: heap size 602 MB, throughput 0.63384
Reading from 29081: heap size 144 MB, throughput 0.998315
Reading from 29080: heap size 603 MB, throughput 0.512829
Reading from 29081: heap size 145 MB, throughput 0.756959
Reading from 29080: heap size 606 MB, throughput 0.580964
Reading from 29080: heap size 615 MB, throughput 0.552691
Reading from 29080: heap size 621 MB, throughput 0.567083
Reading from 29081: heap size 155 MB, throughput 0.998423
Reading from 29081: heap size 155 MB, throughput 0.998064
Reading from 29080: heap size 628 MB, throughput 0.281429
Reading from 29080: heap size 695 MB, throughput 0.754799
Reading from 29081: heap size 162 MB, throughput 0.998731
Reading from 29080: heap size 702 MB, throughput 0.842384
Reading from 29080: heap size 702 MB, throughput 0.844212
Reading from 29080: heap size 712 MB, throughput 0.518172
Reading from 29081: heap size 163 MB, throughput 0.998338
Reading from 29080: heap size 720 MB, throughput 0.743951
Reading from 29080: heap size 724 MB, throughput 0.466155
Reading from 29081: heap size 168 MB, throughput 0.998856
Equal recommendation: 2446 MB each
Reading from 29080: heap size 725 MB, throughput 0.0345594
Reading from 29081: heap size 169 MB, throughput 0.996822
Reading from 29080: heap size 790 MB, throughput 0.332783
Reading from 29080: heap size 778 MB, throughput 0.469941
Reading from 29080: heap size 785 MB, throughput 0.738549
Reading from 29080: heap size 781 MB, throughput 0.835095
Reading from 29081: heap size 175 MB, throughput 0.998913
Reading from 29080: heap size 784 MB, throughput 0.0558141
Reading from 29081: heap size 175 MB, throughput 0.998465
Reading from 29080: heap size 857 MB, throughput 0.407947
Reading from 29080: heap size 858 MB, throughput 0.551001
Reading from 29081: heap size 179 MB, throughput 0.996126
Reading from 29080: heap size 864 MB, throughput 0.836004
Reading from 29080: heap size 866 MB, throughput 0.781464
Reading from 29081: heap size 180 MB, throughput 0.990557
Reading from 29080: heap size 870 MB, throughput 0.174062
Reading from 29081: heap size 186 MB, throughput 0.995594
Reading from 29080: heap size 961 MB, throughput 0.785022
Reading from 29080: heap size 968 MB, throughput 0.884625
Reading from 29080: heap size 970 MB, throughput 0.94207
Reading from 29080: heap size 964 MB, throughput 0.905627
Reading from 29080: heap size 819 MB, throughput 0.85028
Reading from 29081: heap size 187 MB, throughput 0.997717
Reading from 29080: heap size 945 MB, throughput 0.843873
Reading from 29080: heap size 824 MB, throughput 0.803702
Reading from 29080: heap size 935 MB, throughput 0.839892
Reading from 29080: heap size 825 MB, throughput 0.862562
Reading from 29080: heap size 928 MB, throughput 0.881466
Reading from 29080: heap size 825 MB, throughput 0.881466
Reading from 29081: heap size 196 MB, throughput 0.997992
Reading from 29080: heap size 921 MB, throughput 0.87519
Reading from 29080: heap size 830 MB, throughput 0.889069
Reading from 29080: heap size 915 MB, throughput 0.892547
Reading from 29081: heap size 197 MB, throughput 0.997553
Reading from 29080: heap size 834 MB, throughput 0.985214
Equal recommendation: 2446 MB each
Reading from 29081: heap size 204 MB, throughput 0.997947
Reading from 29080: heap size 916 MB, throughput 0.964267
Reading from 29080: heap size 918 MB, throughput 0.821862
Reading from 29080: heap size 917 MB, throughput 0.826462
Reading from 29080: heap size 918 MB, throughput 0.823618
Reading from 29080: heap size 917 MB, throughput 0.830982
Reading from 29080: heap size 919 MB, throughput 0.83563
Reading from 29081: heap size 204 MB, throughput 0.997401
Reading from 29080: heap size 918 MB, throughput 0.833729
Reading from 29080: heap size 921 MB, throughput 0.832034
Reading from 29080: heap size 920 MB, throughput 0.850647
Reading from 29080: heap size 923 MB, throughput 0.89925
Reading from 29080: heap size 925 MB, throughput 0.855429
Reading from 29081: heap size 211 MB, throughput 0.997885
Reading from 29080: heap size 926 MB, throughput 0.910197
Reading from 29080: heap size 927 MB, throughput 0.791664
Reading from 29080: heap size 929 MB, throughput 0.674432
Reading from 29081: heap size 211 MB, throughput 0.997277
Reading from 29080: heap size 921 MB, throughput 0.0668791
Reading from 29080: heap size 1043 MB, throughput 0.560537
Reading from 29080: heap size 1038 MB, throughput 0.720608
Reading from 29081: heap size 218 MB, throughput 0.997216
Reading from 29080: heap size 1043 MB, throughput 0.739429
Reading from 29080: heap size 1042 MB, throughput 0.733761
Reading from 29080: heap size 1046 MB, throughput 0.737825
Reading from 29080: heap size 1053 MB, throughput 0.713373
Reading from 29080: heap size 1055 MB, throughput 0.72907
Reading from 29080: heap size 1069 MB, throughput 0.732854
Reading from 29080: heap size 1069 MB, throughput 0.704272
Reading from 29081: heap size 218 MB, throughput 0.997041
Reading from 29080: heap size 1081 MB, throughput 0.930986
Reading from 29081: heap size 226 MB, throughput 0.997807
Equal recommendation: 2446 MB each
Reading from 29080: heap size 1086 MB, throughput 0.959548
Reading from 29081: heap size 226 MB, throughput 0.997462
Reading from 29081: heap size 233 MB, throughput 0.996801
Reading from 29080: heap size 1102 MB, throughput 0.950927
Reading from 29081: heap size 233 MB, throughput 0.992178
Reading from 29080: heap size 1104 MB, throughput 0.968163
Reading from 29081: heap size 240 MB, throughput 0.997174
Reading from 29081: heap size 240 MB, throughput 0.997837
Reading from 29080: heap size 1107 MB, throughput 0.957629
Reading from 29081: heap size 247 MB, throughput 0.998273
Reading from 29080: heap size 1111 MB, throughput 0.961088
Reading from 29081: heap size 249 MB, throughput 0.997995
Reading from 29080: heap size 1105 MB, throughput 0.962764
Equal recommendation: 2446 MB each
Reading from 29081: heap size 255 MB, throughput 0.997953
Reading from 29080: heap size 1111 MB, throughput 0.953832
Reading from 29081: heap size 256 MB, throughput 0.997902
Reading from 29080: heap size 1109 MB, throughput 0.963163
Reading from 29081: heap size 264 MB, throughput 0.998219
Reading from 29080: heap size 1112 MB, throughput 0.95893
Reading from 29081: heap size 264 MB, throughput 0.998179
Reading from 29080: heap size 1118 MB, throughput 0.957548
Reading from 29081: heap size 270 MB, throughput 0.998312
Reading from 29080: heap size 1118 MB, throughput 0.963632
Reading from 29081: heap size 270 MB, throughput 0.998226
Reading from 29081: heap size 277 MB, throughput 0.994923
Equal recommendation: 2446 MB each
Reading from 29080: heap size 1123 MB, throughput 0.965436
Reading from 29081: heap size 277 MB, throughput 0.996131
Reading from 29080: heap size 1126 MB, throughput 0.962045
Reading from 29081: heap size 285 MB, throughput 0.997898
Reading from 29081: heap size 286 MB, throughput 0.997391
Reading from 29080: heap size 1131 MB, throughput 0.95969
Reading from 29081: heap size 294 MB, throughput 0.998113
Reading from 29080: heap size 1135 MB, throughput 0.962059
Reading from 29081: heap size 294 MB, throughput 0.997991
Reading from 29080: heap size 1140 MB, throughput 0.962069
Equal recommendation: 2446 MB each
Reading from 29081: heap size 302 MB, throughput 0.998331
Reading from 29080: heap size 1144 MB, throughput 0.961036
Reading from 29081: heap size 302 MB, throughput 0.997933
Reading from 29080: heap size 1151 MB, throughput 0.956605
Reading from 29081: heap size 308 MB, throughput 0.998267
Reading from 29080: heap size 1154 MB, throughput 0.960877
Reading from 29081: heap size 309 MB, throughput 0.998081
Reading from 29081: heap size 317 MB, throughput 0.992031
Reading from 29080: heap size 1160 MB, throughput 0.941833
Reading from 29081: heap size 317 MB, throughput 0.997592
Reading from 29080: heap size 1162 MB, throughput 0.962028
Equal recommendation: 2446 MB each
Reading from 29081: heap size 329 MB, throughput 0.998083
Reading from 29080: heap size 1168 MB, throughput 0.9635
Reading from 29081: heap size 329 MB, throughput 0.997871
Reading from 29080: heap size 1169 MB, throughput 0.963735
Reading from 29081: heap size 338 MB, throughput 0.997979
Reading from 29080: heap size 1175 MB, throughput 0.96316
Reading from 29081: heap size 339 MB, throughput 0.998005
Reading from 29081: heap size 348 MB, throughput 0.998228
Reading from 29080: heap size 1176 MB, throughput 0.541126
Equal recommendation: 2446 MB each
Reading from 29081: heap size 348 MB, throughput 0.99829
Reading from 29080: heap size 1278 MB, throughput 0.950071
Reading from 29081: heap size 356 MB, throughput 0.995311
Reading from 29080: heap size 1278 MB, throughput 0.984365
Reading from 29081: heap size 357 MB, throughput 0.996754
Reading from 29080: heap size 1288 MB, throughput 0.975676
Reading from 29081: heap size 368 MB, throughput 0.998472
Reading from 29080: heap size 1292 MB, throughput 0.979172
Reading from 29081: heap size 369 MB, throughput 0.998228
Equal recommendation: 2446 MB each
Reading from 29080: heap size 1294 MB, throughput 0.980651
Reading from 29081: heap size 377 MB, throughput 0.998276
Reading from 29080: heap size 1296 MB, throughput 0.976341
Reading from 29081: heap size 378 MB, throughput 0.998026
Reading from 29080: heap size 1287 MB, throughput 0.974114
Reading from 29081: heap size 388 MB, throughput 0.998106
Reading from 29080: heap size 1215 MB, throughput 0.973748
Reading from 29081: heap size 388 MB, throughput 0.998104
Equal recommendation: 2446 MB each
Reading from 29081: heap size 396 MB, throughput 0.905379
Reading from 29080: heap size 1279 MB, throughput 0.970665
Reading from 29081: heap size 404 MB, throughput 0.999049
Reading from 29080: heap size 1285 MB, throughput 0.971867
Reading from 29081: heap size 419 MB, throughput 0.999087
Reading from 29080: heap size 1288 MB, throughput 0.969811
Reading from 29081: heap size 419 MB, throughput 0.999145
Reading from 29080: heap size 1288 MB, throughput 0.964945
Equal recommendation: 2446 MB each
Reading from 29081: heap size 430 MB, throughput 0.999056
Reading from 29080: heap size 1291 MB, throughput 0.967092
Reading from 29081: heap size 431 MB, throughput 0.999114
Reading from 29080: heap size 1295 MB, throughput 0.962938
Reading from 29081: heap size 440 MB, throughput 0.998823
Reading from 29080: heap size 1300 MB, throughput 0.963159
Reading from 29081: heap size 441 MB, throughput 0.996119
Reading from 29080: heap size 1307 MB, throughput 0.954568
Equal recommendation: 2446 MB each
Reading from 29081: heap size 451 MB, throughput 0.998716
Reading from 29080: heap size 1315 MB, throughput 0.959425
Reading from 29081: heap size 452 MB, throughput 0.998593
Reading from 29080: heap size 1321 MB, throughput 0.963032
Reading from 29081: heap size 463 MB, throughput 0.998582
Reading from 29080: heap size 1329 MB, throughput 0.960978
Reading from 29081: heap size 464 MB, throughput 0.998469
Equal recommendation: 2446 MB each
Reading from 29080: heap size 1333 MB, throughput 0.961019
Reading from 29081: heap size 475 MB, throughput 0.998744
Reading from 29081: heap size 476 MB, throughput 0.996265
Reading from 29081: heap size 488 MB, throughput 0.998614
Reading from 29081: heap size 489 MB, throughput 0.998554
Equal recommendation: 2446 MB each
Reading from 29081: heap size 503 MB, throughput 0.998579
Reading from 29081: heap size 503 MB, throughput 0.998434
Reading from 29081: heap size 515 MB, throughput 0.998445
Equal recommendation: 2446 MB each
Reading from 29081: heap size 516 MB, throughput 0.996567
Reading from 29081: heap size 529 MB, throughput 0.998442
Reading from 29081: heap size 530 MB, throughput 0.99844
Equal recommendation: 2446 MB each
Reading from 29081: heap size 544 MB, throughput 0.998474
Reading from 29080: heap size 1342 MB, throughput 0.996508
Reading from 29081: heap size 545 MB, throughput 0.998597
Reading from 29081: heap size 558 MB, throughput 0.996814
Equal recommendation: 2446 MB each
Reading from 29080: heap size 1343 MB, throughput 0.880485
Reading from 29081: heap size 559 MB, throughput 0.998409
Reading from 29080: heap size 1406 MB, throughput 0.995084
Reading from 29080: heap size 1446 MB, throughput 0.92364
Reading from 29080: heap size 1446 MB, throughput 0.893324
Reading from 29080: heap size 1452 MB, throughput 0.892738
Reading from 29080: heap size 1442 MB, throughput 0.8998
Reading from 29080: heap size 1448 MB, throughput 0.899594
Reading from 29080: heap size 1443 MB, throughput 0.905514
Reading from 29080: heap size 1449 MB, throughput 0.986762
Reading from 29081: heap size 576 MB, throughput 0.998506
Reading from 29080: heap size 1454 MB, throughput 0.993172
Reading from 29081: heap size 577 MB, throughput 0.99843
Reading from 29080: heap size 1460 MB, throughput 0.990135
Equal recommendation: 2446 MB each
Reading from 29080: heap size 1470 MB, throughput 0.986709
Reading from 29081: heap size 592 MB, throughput 0.998574
Reading from 29081: heap size 593 MB, throughput 0.996912
Reading from 29080: heap size 1473 MB, throughput 0.986173
Reading from 29080: heap size 1463 MB, throughput 0.98378
Reading from 29081: heap size 608 MB, throughput 0.998633
Equal recommendation: 2446 MB each
Reading from 29080: heap size 1338 MB, throughput 0.982301
Reading from 29081: heap size 609 MB, throughput 0.998435
Reading from 29080: heap size 1450 MB, throughput 0.980953
Reading from 29081: heap size 625 MB, throughput 0.998463
Reading from 29080: heap size 1364 MB, throughput 0.979311
Reading from 29081: heap size 626 MB, throughput 0.99795
Reading from 29080: heap size 1447 MB, throughput 0.97404
Equal recommendation: 2446 MB each
Reading from 29081: heap size 643 MB, throughput 0.99793
Reading from 29080: heap size 1453 MB, throughput 0.975279
Reading from 29080: heap size 1457 MB, throughput 0.971072
Reading from 29081: heap size 643 MB, throughput 0.998558
Reading from 29080: heap size 1458 MB, throughput 0.972039
Equal recommendation: 2446 MB each
Reading from 29081: heap size 660 MB, throughput 0.998622
Reading from 29080: heap size 1464 MB, throughput 0.968971
Reading from 29081: heap size 661 MB, throughput 0.998549
Reading from 29080: heap size 1470 MB, throughput 0.957235
Reading from 29081: heap size 677 MB, throughput 0.962942
Reading from 29080: heap size 1477 MB, throughput 0.968201
Equal recommendation: 2446 MB each
Reading from 29081: heap size 679 MB, throughput 0.999294
Reading from 29080: heap size 1487 MB, throughput 0.965255
Reading from 29080: heap size 1498 MB, throughput 0.964139
Reading from 29081: heap size 642 MB, throughput 0.999292
Reading from 29080: heap size 1507 MB, throughput 0.962102
Equal recommendation: 2446 MB each
Reading from 29081: heap size 612 MB, throughput 0.999272
Reading from 29080: heap size 1518 MB, throughput 0.95141
Reading from 29081: heap size 583 MB, throughput 0.99773
Reading from 29080: heap size 1523 MB, throughput 0.963747
Reading from 29081: heap size 561 MB, throughput 0.998973
Reading from 29080: heap size 1536 MB, throughput 0.963129
Reading from 29081: heap size 533 MB, throughput 0.999015
Equal recommendation: 2446 MB each
Reading from 29080: heap size 1539 MB, throughput 0.961414
Reading from 29081: heap size 508 MB, throughput 0.998915
Reading from 29081: heap size 484 MB, throughput 0.998453
Reading from 29080: heap size 1551 MB, throughput 0.963195
Reading from 29080: heap size 1552 MB, throughput 0.967515
Equal recommendation: 2446 MB each
Client 29081 died
Clients: 1
Reading from 29080: heap size 1564 MB, throughput 0.96821
Reading from 29080: heap size 1565 MB, throughput 0.967222
Reading from 29080: heap size 1575 MB, throughput 0.967531
Recommendation: one client; give it all the memory
Reading from 29080: heap size 1576 MB, throughput 0.749203
Reading from 29080: heap size 1667 MB, throughput 0.992893
Recommendation: one client; give it all the memory
Reading from 29080: heap size 1668 MB, throughput 0.990599
Reading from 29080: heap size 1686 MB, throughput 0.988475
Reading from 29080: heap size 1692 MB, throughput 0.986276
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 29080: heap size 1694 MB, throughput 0.99694
Recommendation: one client; give it all the memory
Reading from 29080: heap size 1569 MB, throughput 0.988065
Reading from 29080: heap size 1680 MB, throughput 0.883701
Reading from 29080: heap size 1693 MB, throughput 0.770587
Reading from 29080: heap size 1717 MB, throughput 0.809104
Reading from 29080: heap size 1741 MB, throughput 0.791036
Client 29080 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
