#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <Utilities.hpp>
#include "Args.hpp"

const char* Args::s_CommandTarget = economem::NAME_DEFAULT;
char* Args::s_SingleCommand = NULL;

void Args::usage()
{
    fprintf(stderr, "Usage: console [-v] [-t CMD_TARGET] [-c COMMAND]\n");
    exit(1);
}

const char* Args::getCommandTarget()
{
    return s_CommandTarget;
}

bool Args::singleCommandMode()
{
    return s_SingleCommand != NULL;
}

char* Args::getSingleCommand()
{
    return s_SingleCommand;
}

void Args::parse(int argc, char** argv)
{
    int opt = 0;
    opterr = 0;

    while ((opt = getopt(argc, argv, "vt:c:")) != -1)
    {
        switch (opt)
        {
        case 'v':
            economem::makeInfoVerbose();
            break;
        case 't':
            if (optarg == NULL || optarg[0] == '\0')
                economem::die("Argumemt to '-t' must not be empty");
            s_CommandTarget = optarg;
            break;
        case 'c':
            if (optarg == NULL || optarg[0] == '\0')
                economem::die("Argument to '-c' must not be empty");
            s_SingleCommand = optarg;
            break;
        default:
            usage();
            break;
        }
    }
}
