#ifndef FAKEVMSIMPLETHROUGHPUT_HPP
#define FAKEVMSIMPLETHROUGHPUT_HPP

#include <functional>
#include <SimpleThroughput.hpp>

class FakeVMSimpleThroughput : public economem::SimpleThroughput
{
public:
    FakeVMSimpleThroughput(std::function<unsigned long long(void)> getCapacityMB);

private:
    std::function<unsigned long long(void)> m_GetCapacityMB;

    virtual unsigned long long getHeapSizeB();
    virtual void logReading(unsigned long long heapSizeB, double throughput, unsigned long long gcTime, unsigned long long interGCTime);
};

#endif
