#ifndef HOTSPOTMAJORONLY_HPP
#define HOTSPOTMAJORONLY_HPP

#include "gc_implementation/shared/MajorOnlyThroughput.hpp"

class HotspotMajorOnly : public economem::MajorOnlyThroughput
{
public:
    HotspotMajorOnly();

private:
    virtual unsigned long long getHeapSizeB();
    virtual void logReading(unsigned long long heapSizeB, double throughput, unsigned long long gcTime, unsigned long long interGCTime);
};

#endif
