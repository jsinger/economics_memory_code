economemd
    total memory: 4461 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub2_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run05_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 31092: heap size 9 MB, throughput 0.99208
Clients: 1
Client 31092 has a minimum heap size of 1211 MB
Reading from 31094: heap size 9 MB, throughput 0.95557
Clients: 2
Client 31094 has a minimum heap size of 276 MB
Reading from 31092: heap size 9 MB, throughput 0.988056
Reading from 31094: heap size 9 MB, throughput 0.961756
Reading from 31092: heap size 9 MB, throughput 0.981113
Reading from 31092: heap size 9 MB, throughput 0.964878
Reading from 31094: heap size 11 MB, throughput 0.967903
Reading from 31092: heap size 11 MB, throughput 0.977754
Reading from 31094: heap size 11 MB, throughput 0.978371
Reading from 31092: heap size 11 MB, throughput 0.976458
Reading from 31092: heap size 17 MB, throughput 0.966006
Reading from 31094: heap size 15 MB, throughput 0.908672
Reading from 31094: heap size 18 MB, throughput 0.901281
Reading from 31092: heap size 17 MB, throughput 0.874499
Reading from 31094: heap size 24 MB, throughput 0.808867
Reading from 31092: heap size 30 MB, throughput 0.732924
Reading from 31094: heap size 28 MB, throughput 0.562899
Reading from 31092: heap size 31 MB, throughput 0.389708
Reading from 31092: heap size 33 MB, throughput 0.780762
Reading from 31094: heap size 31 MB, throughput 0.796952
Reading from 31094: heap size 41 MB, throughput 0.380291
Reading from 31092: heap size 46 MB, throughput 0.810203
Reading from 31094: heap size 47 MB, throughput 0.868473
Reading from 31092: heap size 50 MB, throughput 0.596488
Reading from 31092: heap size 50 MB, throughput 0.105889
Reading from 31094: heap size 48 MB, throughput 0.569558
Reading from 31094: heap size 65 MB, throughput 0.820829
Reading from 31094: heap size 68 MB, throughput 0.743707
Reading from 31092: heap size 56 MB, throughput 0.605217
Reading from 31092: heap size 71 MB, throughput 0.798389
Reading from 31092: heap size 80 MB, throughput 0.775873
Reading from 31094: heap size 72 MB, throughput 0.431193
Reading from 31094: heap size 92 MB, throughput 0.749107
Reading from 31094: heap size 99 MB, throughput 0.772857
Reading from 31092: heap size 81 MB, throughput 0.367046
Reading from 31094: heap size 101 MB, throughput 0.7331
Reading from 31092: heap size 115 MB, throughput 0.0926465
Reading from 31092: heap size 115 MB, throughput 0.7
Reading from 31092: heap size 118 MB, throughput 0.747118
Reading from 31094: heap size 103 MB, throughput 0.466199
Reading from 31092: heap size 123 MB, throughput 0.740222
Reading from 31094: heap size 135 MB, throughput 0.744892
Reading from 31094: heap size 141 MB, throughput 0.716653
Reading from 31094: heap size 143 MB, throughput 0.683802
Reading from 31092: heap size 127 MB, throughput 0.436213
Reading from 31092: heap size 165 MB, throughput 0.700167
Reading from 31092: heap size 170 MB, throughput 0.716239
Reading from 31094: heap size 148 MB, throughput 0.374329
Reading from 31092: heap size 174 MB, throughput 0.687944
Reading from 31094: heap size 183 MB, throughput 0.67741
Reading from 31094: heap size 191 MB, throughput 0.716737
Reading from 31094: heap size 193 MB, throughput 0.667594
Reading from 31092: heap size 177 MB, throughput 0.427031
Reading from 31092: heap size 218 MB, throughput 0.65131
Reading from 31092: heap size 228 MB, throughput 0.703564
Reading from 31092: heap size 228 MB, throughput 0.677989
Reading from 31092: heap size 232 MB, throughput 0.613863
Reading from 31094: heap size 196 MB, throughput 0.543714
Reading from 31092: heap size 238 MB, throughput 0.672436
Reading from 31092: heap size 246 MB, throughput 0.666333
Reading from 31092: heap size 253 MB, throughput 0.631584
Reading from 31094: heap size 237 MB, throughput 0.865292
Reading from 31092: heap size 259 MB, throughput 0.63574
Reading from 31094: heap size 251 MB, throughput 0.857906
Reading from 31094: heap size 253 MB, throughput 0.886414
Reading from 31094: heap size 259 MB, throughput 0.864715
Reading from 31092: heap size 265 MB, throughput 0.441255
Reading from 31094: heap size 261 MB, throughput 0.827481
Reading from 31092: heap size 307 MB, throughput 0.601519
Reading from 31094: heap size 267 MB, throughput 0.801197
Reading from 31094: heap size 269 MB, throughput 0.748966
Reading from 31094: heap size 276 MB, throughput 0.740656
Reading from 31094: heap size 280 MB, throughput 0.692031
Reading from 31092: heap size 313 MB, throughput 0.239705
Reading from 31094: heap size 288 MB, throughput 0.689269
Reading from 31092: heap size 360 MB, throughput 0.595037
Reading from 31092: heap size 360 MB, throughput 0.64374
Reading from 31092: heap size 361 MB, throughput 0.669745
Reading from 31092: heap size 361 MB, throughput 0.658677
Reading from 31094: heap size 290 MB, throughput 0.877043
Reading from 31092: heap size 363 MB, throughput 0.658374
Reading from 31094: heap size 296 MB, throughput 0.835639
Reading from 31092: heap size 370 MB, throughput 0.622969
Reading from 31092: heap size 373 MB, throughput 0.585284
Reading from 31092: heap size 384 MB, throughput 0.549211
Reading from 31092: heap size 391 MB, throughput 0.499943
Reading from 31094: heap size 300 MB, throughput 0.677677
Reading from 31094: heap size 340 MB, throughput 0.716469
Reading from 31094: heap size 343 MB, throughput 0.810282
Reading from 31094: heap size 345 MB, throughput 0.812613
Reading from 31092: heap size 401 MB, throughput 0.376205
Reading from 31094: heap size 347 MB, throughput 0.789839
Reading from 31092: heap size 454 MB, throughput 0.443162
Reading from 31094: heap size 345 MB, throughput 0.692994
Reading from 31094: heap size 348 MB, throughput 0.647857
Reading from 31092: heap size 459 MB, throughput 0.460109
Reading from 31094: heap size 352 MB, throughput 0.644069
Numeric result:
Recommendation: 2 clients, utility 0.82936:
    h1: 2289.93 MB (U(h) = 1.05484*h^0.001)
    h2: 2171.07 MB (U(h) = 0.774209*h^0.001)
Recommendation: 2 clients, utility 0.829361:
    h1: 2230.5 MB (U(h) = 1.05484*h^0.001)
    h2: 2230.5 MB (U(h) = 0.774209*h^0.001)
Reading from 31094: heap size 354 MB, throughput 0.865542
Reading from 31092: heap size 459 MB, throughput 0.171913
Reading from 31092: heap size 511 MB, throughput 0.49922
Reading from 31092: heap size 500 MB, throughput 0.487368
Reading from 31092: heap size 434 MB, throughput 0.459239
Reading from 31092: heap size 498 MB, throughput 0.518937
Reading from 31094: heap size 363 MB, throughput 0.956403
Reading from 31092: heap size 502 MB, throughput 0.274725
Reading from 31092: heap size 559 MB, throughput 0.479768
Reading from 31092: heap size 560 MB, throughput 0.524358
Reading from 31092: heap size 558 MB, throughput 0.552528
Reading from 31092: heap size 559 MB, throughput 0.492467
Reading from 31092: heap size 562 MB, throughput 0.553768
Reading from 31092: heap size 570 MB, throughput 0.557735
Reading from 31092: heap size 576 MB, throughput 0.566293
Reading from 31092: heap size 585 MB, throughput 0.563415
Reading from 31094: heap size 364 MB, throughput 0.95316
Reading from 31092: heap size 591 MB, throughput 0.34743
Reading from 31092: heap size 651 MB, throughput 0.503351
Reading from 31094: heap size 370 MB, throughput 0.962709
Reading from 31092: heap size 660 MB, throughput 0.726798
Reading from 31092: heap size 665 MB, throughput 0.798728
Reading from 31092: heap size 667 MB, throughput 0.810634
Reading from 31094: heap size 372 MB, throughput 0.966687
Reading from 31092: heap size 677 MB, throughput 0.824923
Reading from 31092: heap size 683 MB, throughput 0.727342
Reading from 31092: heap size 694 MB, throughput 0.430868
Reading from 31094: heap size 371 MB, throughput 0.860067
Numeric result:
Recommendation: 2 clients, utility 0.737174:
    h1: 1211 MB (U(h) = 0.946735*h^0.001)
    h2: 3250 MB (U(h) = 0.691616*h^0.0137798)
Recommendation: 2 clients, utility 0.737174:
    h1: 1211 MB (U(h) = 0.946735*h^0.001)
    h2: 3250 MB (U(h) = 0.691616*h^0.0137798)
Reading from 31092: heap size 766 MB, throughput 0.513099
Reading from 31092: heap size 767 MB, throughput 0.430042
Reading from 31092: heap size 755 MB, throughput 0.374419
Reading from 31094: heap size 419 MB, throughput 0.963181
Reading from 31092: heap size 661 MB, throughput 0.1169
Reading from 31092: heap size 839 MB, throughput 0.28874
Reading from 31092: heap size 841 MB, throughput 0.672137
Reading from 31092: heap size 844 MB, throughput 0.691767
Reading from 31092: heap size 846 MB, throughput 0.655299
Reading from 31092: heap size 842 MB, throughput 0.630811
Reading from 31094: heap size 423 MB, throughput 0.973577
Reading from 31092: heap size 845 MB, throughput 0.248146
Reading from 31092: heap size 931 MB, throughput 0.706566
Reading from 31092: heap size 931 MB, throughput 0.727773
Reading from 31094: heap size 425 MB, throughput 0.980004
Reading from 31092: heap size 934 MB, throughput 0.802994
Reading from 31092: heap size 939 MB, throughput 0.882619
Reading from 31092: heap size 940 MB, throughput 0.887452
Reading from 31092: heap size 942 MB, throughput 0.883732
Reading from 31092: heap size 929 MB, throughput 0.891089
Reading from 31092: heap size 780 MB, throughput 0.84132
Reading from 31092: heap size 904 MB, throughput 0.804968
Reading from 31092: heap size 785 MB, throughput 0.784913
Reading from 31092: heap size 890 MB, throughput 0.784375
Reading from 31092: heap size 788 MB, throughput 0.749025
Reading from 31094: heap size 428 MB, throughput 0.979237
Reading from 31092: heap size 883 MB, throughput 0.781799
Reading from 31092: heap size 786 MB, throughput 0.773493
Reading from 31092: heap size 876 MB, throughput 0.787888
Reading from 31092: heap size 882 MB, throughput 0.801385
Reading from 31092: heap size 872 MB, throughput 0.804686
Reading from 31092: heap size 877 MB, throughput 0.933789
Reading from 31094: heap size 430 MB, throughput 0.981244
Reading from 31092: heap size 877 MB, throughput 0.96387
Reading from 31092: heap size 879 MB, throughput 0.934124
Numeric result:
Recommendation: 2 clients, utility 0.59501:
    h1: 1211 MB (U(h) = 0.697153*h^0.001)
    h2: 3250 MB (U(h) = 0.632885*h^0.0361028)
Recommendation: 2 clients, utility 0.59501:
    h1: 1211 MB (U(h) = 0.697153*h^0.001)
    h2: 3250 MB (U(h) = 0.632885*h^0.0361028)
Reading from 31092: heap size 880 MB, throughput 0.872824
Reading from 31094: heap size 426 MB, throughput 0.975154
Reading from 31092: heap size 881 MB, throughput 0.83913
Reading from 31092: heap size 881 MB, throughput 0.814458
Reading from 31092: heap size 883 MB, throughput 0.807336
Reading from 31092: heap size 884 MB, throughput 0.808534
Reading from 31092: heap size 886 MB, throughput 0.779778
Reading from 31092: heap size 887 MB, throughput 0.767082
Reading from 31092: heap size 889 MB, throughput 0.853614
Reading from 31092: heap size 892 MB, throughput 0.847722
Reading from 31094: heap size 429 MB, throughput 0.982367
Reading from 31094: heap size 428 MB, throughput 0.972818
Reading from 31092: heap size 894 MB, throughput 0.861311
Reading from 31094: heap size 430 MB, throughput 0.939767
Reading from 31094: heap size 437 MB, throughput 0.879991
Reading from 31092: heap size 893 MB, throughput 0.804334
Reading from 31094: heap size 440 MB, throughput 0.940441
Reading from 31092: heap size 896 MB, throughput 0.685362
Reading from 31092: heap size 977 MB, throughput 0.622014
Reading from 31092: heap size 997 MB, throughput 0.675169
Reading from 31092: heap size 991 MB, throughput 0.709023
Reading from 31092: heap size 996 MB, throughput 0.74722
Reading from 31092: heap size 995 MB, throughput 0.729742
Reading from 31094: heap size 450 MB, throughput 0.97217
Reading from 31092: heap size 998 MB, throughput 0.718342
Reading from 31092: heap size 1006 MB, throughput 0.747471
Reading from 31092: heap size 1007 MB, throughput 0.734884
Reading from 31092: heap size 1019 MB, throughput 0.738946
Reading from 31092: heap size 1019 MB, throughput 0.726073
Reading from 31092: heap size 1029 MB, throughput 0.900626
Reading from 31094: heap size 453 MB, throughput 0.978865
Reading from 31092: heap size 1035 MB, throughput 0.937466
Numeric result:
Recommendation: 2 clients, utility 0.662833:
    h1: 3119.37 MB (U(h) = 0.287904*h^0.121745)
    h2: 1341.63 MB (U(h) = 0.592975*h^0.0523508)
Recommendation: 2 clients, utility 0.662833:
    h1: 3119.58 MB (U(h) = 0.287904*h^0.121745)
    h2: 1341.42 MB (U(h) = 0.592975*h^0.0523508)
Reading from 31094: heap size 454 MB, throughput 0.983013
Reading from 31092: heap size 1049 MB, throughput 0.949287
Reading from 31094: heap size 458 MB, throughput 0.9844
Reading from 31092: heap size 1050 MB, throughput 0.950001
Reading from 31094: heap size 458 MB, throughput 0.98234
Reading from 31092: heap size 1050 MB, throughput 0.947045
Reading from 31092: heap size 1055 MB, throughput 0.949125
Reading from 31094: heap size 461 MB, throughput 0.982815
Reading from 31092: heap size 1048 MB, throughput 0.950251
Reading from 31094: heap size 463 MB, throughput 0.983258
Reading from 31092: heap size 1054 MB, throughput 0.950483
Numeric result:
Recommendation: 2 clients, utility 0.794637:
    h1: 3422.36 MB (U(h) = 0.17022*h^0.205335)
    h2: 1038.64 MB (U(h) = 0.569483*h^0.0623104)
Recommendation: 2 clients, utility 0.794637:
    h1: 3422.44 MB (U(h) = 0.17022*h^0.205335)
    h2: 1038.56 MB (U(h) = 0.569483*h^0.0623104)
Reading from 31094: heap size 464 MB, throughput 0.981871
Reading from 31092: heap size 1049 MB, throughput 0.951344
Reading from 31094: heap size 467 MB, throughput 0.982693
Reading from 31094: heap size 470 MB, throughput 0.972715
Reading from 31094: heap size 472 MB, throughput 0.960464
Reading from 31092: heap size 1053 MB, throughput 0.951069
Reading from 31094: heap size 473 MB, throughput 0.97937
Reading from 31092: heap size 1057 MB, throughput 0.950113
Reading from 31094: heap size 480 MB, throughput 0.987971
Reading from 31092: heap size 1058 MB, throughput 0.958355
Reading from 31092: heap size 1063 MB, throughput 0.959454
Reading from 31094: heap size 481 MB, throughput 0.990103
Numeric result:
Recommendation: 2 clients, utility 0.974692:
    h1: 3580.62 MB (U(h) = 0.0888893*h^0.305597)
    h2: 880.375 MB (U(h) = 0.540338*h^0.0751442)
Recommendation: 2 clients, utility 0.974692:
    h1: 3580.56 MB (U(h) = 0.0888893*h^0.305597)
    h2: 880.437 MB (U(h) = 0.540338*h^0.0751442)
Reading from 31092: heap size 1065 MB, throughput 0.959898
Reading from 31094: heap size 484 MB, throughput 0.989994
Reading from 31092: heap size 1069 MB, throughput 0.957182
Reading from 31092: heap size 1073 MB, throughput 0.958326
Reading from 31094: heap size 486 MB, throughput 0.989839
Reading from 31092: heap size 1077 MB, throughput 0.951113
Reading from 31094: heap size 485 MB, throughput 0.988737
Numeric result:
Recommendation: 2 clients, utility 1.17488:
    h1: 3737.53 MB (U(h) = 0.0440929*h^0.412335)
    h2: 723.466 MB (U(h) = 0.530005*h^0.0798207)
Recommendation: 2 clients, utility 1.17488:
    h1: 3737.49 MB (U(h) = 0.0440929*h^0.412335)
    h2: 723.511 MB (U(h) = 0.530005*h^0.0798207)
Reading from 31094: heap size 487 MB, throughput 0.979673
Reading from 31092: heap size 1082 MB, throughput 0.887517
Reading from 31092: heap size 1175 MB, throughput 0.937192
Reading from 31094: heap size 488 MB, throughput 0.986927
Reading from 31094: heap size 488 MB, throughput 0.981919
Reading from 31094: heap size 491 MB, throughput 0.974043
Reading from 31092: heap size 1178 MB, throughput 0.957235
Reading from 31094: heap size 492 MB, throughput 0.970913
Reading from 31092: heap size 1185 MB, throughput 0.96705
Reading from 31094: heap size 500 MB, throughput 0.987691
Reading from 31092: heap size 1186 MB, throughput 0.966861
Reading from 31094: heap size 501 MB, throughput 0.988864
Numeric result:
Recommendation: 2 clients, utility 1.64273:
    h1: 3909.08 MB (U(h) = 0.0112654*h^0.616335)
    h2: 551.92 MB (U(h) = 0.5144*h^0.0870187)
Recommendation: 2 clients, utility 1.64273:
    h1: 3909.09 MB (U(h) = 0.0112654*h^0.616335)
    h2: 551.913 MB (U(h) = 0.5144*h^0.0870187)
Reading from 31092: heap size 1183 MB, throughput 0.96778
Reading from 31092: heap size 1186 MB, throughput 0.966253
Reading from 31094: heap size 502 MB, throughput 0.990439
Reading from 31092: heap size 1178 MB, throughput 0.966012
Reading from 31094: heap size 504 MB, throughput 0.990112
Reading from 31092: heap size 1183 MB, throughput 0.967906
Reading from 31094: heap size 503 MB, throughput 0.988775
Reading from 31092: heap size 1178 MB, throughput 0.964836
Numeric result:
Recommendation: 2 clients, utility 1.71703:
    h1: 3909.58 MB (U(h) = 0.00961391*h^0.640011)
    h2: 551.423 MB (U(h) = 0.507447*h^0.0902735)
Recommendation: 2 clients, utility 1.71703:
    h1: 3909.56 MB (U(h) = 0.00961391*h^0.640011)
    h2: 551.443 MB (U(h) = 0.507447*h^0.0902735)
Reading from 31092: heap size 1180 MB, throughput 0.962911
Reading from 31094: heap size 505 MB, throughput 0.987839
Reading from 31092: heap size 1183 MB, throughput 0.962468
Reading from 31094: heap size 507 MB, throughput 0.991142
Reading from 31094: heap size 508 MB, throughput 0.985592
Reading from 31094: heap size 508 MB, throughput 0.976968
Reading from 31092: heap size 1184 MB, throughput 0.961022
Reading from 31094: heap size 510 MB, throughput 0.98581
Reading from 31092: heap size 1188 MB, throughput 0.959293
Reading from 31092: heap size 1192 MB, throughput 0.957557
Reading from 31094: heap size 518 MB, throughput 0.990885
Numeric result:
Recommendation: 2 clients, utility 1.96956:
    h1: 3944.52 MB (U(h) = 0.00543817*h^0.724294)
    h2: 516.476 MB (U(h) = 0.497811*h^0.0948367)
Recommendation: 2 clients, utility 1.96956:
    h1: 3944.52 MB (U(h) = 0.00543817*h^0.724294)
    h2: 516.483 MB (U(h) = 0.497811*h^0.0948367)
Reading from 31092: heap size 1197 MB, throughput 0.955214
Reading from 31094: heap size 519 MB, throughput 0.991111
Reading from 31092: heap size 1203 MB, throughput 0.953547
Reading from 31094: heap size 498 MB, throughput 0.990461
Reading from 31092: heap size 1211 MB, throughput 0.951686
Reading from 31094: heap size 511 MB, throughput 0.988993
Reading from 31092: heap size 1217 MB, throughput 0.95199
Numeric result:
Recommendation: 2 clients, utility 2.57901:
    h1: 4028.03 MB (U(h) = 0.0015849*h^0.904935)
    h2: 432.965 MB (U(h) = 0.49275*h^0.0972703)
Recommendation: 2 clients, utility 2.57901:
    h1: 4028.03 MB (U(h) = 0.0015849*h^0.904935)
    h2: 432.968 MB (U(h) = 0.49275*h^0.0972703)
Reading from 31094: heap size 511 MB, throughput 0.988614
Reading from 31092: heap size 1225 MB, throughput 0.952738
Reading from 31092: heap size 1228 MB, throughput 0.952744
Reading from 31094: heap size 494 MB, throughput 0.990723
Reading from 31094: heap size 492 MB, throughput 0.987045
Reading from 31094: heap size 487 MB, throughput 0.977366
Reading from 31094: heap size 487 MB, throughput 0.966043
Reading from 31092: heap size 1236 MB, throughput 0.949418
Reading from 31094: heap size 455 MB, throughput 0.980995
Numeric result:
Recommendation: 2 clients, utility 2.77971:
    h1: 4043.7 MB (U(h) = 0.00111423*h^0.956065)
    h2: 417.3 MB (U(h) = 0.489972*h^0.0986615)
Recommendation: 2 clients, utility 2.77971:
    h1: 4043.71 MB (U(h) = 0.00111423*h^0.956065)
    h2: 417.292 MB (U(h) = 0.489972*h^0.0986615)
Reading from 31092: heap size 1238 MB, throughput 0.919343
Reading from 31094: heap size 476 MB, throughput 0.987885
Reading from 31092: heap size 1349 MB, throughput 0.946388
Reading from 31094: heap size 450 MB, throughput 0.989131
Reading from 31092: heap size 1349 MB, throughput 0.956272
Reading from 31094: heap size 463 MB, throughput 0.987894
Reading from 31092: heap size 1358 MB, throughput 0.963455
Reading from 31094: heap size 446 MB, throughput 0.988306
Numeric result:
Recommendation: 2 clients, utility 2.69201:
    h1: 4030.11 MB (U(h) = 0.00127375*h^0.935746)
    h2: 430.893 MB (U(h) = 0.487268*h^0.100048)
Recommendation: 2 clients, utility 2.69201:
    h1: 4030.11 MB (U(h) = 0.00127375*h^0.935746)
    h2: 430.892 MB (U(h) = 0.487268*h^0.100048)
Reading from 31092: heap size 1359 MB, throughput 0.966318
Reading from 31094: heap size 453 MB, throughput 0.987429
Reading from 31094: heap size 443 MB, throughput 0.986397
Reading from 31092: heap size 1358 MB, throughput 0.967675
Reading from 31094: heap size 449 MB, throughput 0.989677
Reading from 31094: heap size 424 MB, throughput 0.982224
Reading from 31094: heap size 446 MB, throughput 0.971693
Reading from 31094: heap size 436 MB, throughput 0.94835
Reading from 31092: heap size 1362 MB, throughput 0.96407
Reading from 31094: heap size 442 MB, throughput 0.977268
Reading from 31094: heap size 409 MB, throughput 0.98584
Numeric result:
Recommendation: 2 clients, utility 2.87668:
    h1: 3873.89 MB (U(h) = 0.00101194*h^0.968531)
    h2: 587.114 MB (U(h) = 0.373323*h^0.14679)
Recommendation: 2 clients, utility 2.87668:
    h1: 3873.88 MB (U(h) = 0.00101194*h^0.968531)
    h2: 587.123 MB (U(h) = 0.373323*h^0.14679)
Reading from 31094: heap size 442 MB, throughput 0.98832
Reading from 31094: heap size 442 MB, throughput 0.988215
Reading from 31094: heap size 442 MB, throughput 0.987389
Reading from 31094: heap size 444 MB, throughput 0.986165
Reading from 31094: heap size 442 MB, throughput 0.985061
Numeric result:
Recommendation: 2 clients, utility 2.92039:
    h1: 3791.56 MB (U(h) = 0.00101194*h^0.968531)
    h2: 669.439 MB (U(h) = 0.324236*h^0.171012)
Recommendation: 2 clients, utility 2.92039:
    h1: 3791.54 MB (U(h) = 0.00101194*h^0.968531)
    h2: 669.464 MB (U(h) = 0.324236*h^0.171012)
Reading from 31094: heap size 444 MB, throughput 0.984544
Reading from 31092: heap size 1355 MB, throughput 0.987093
Reading from 31094: heap size 446 MB, throughput 0.983606
Reading from 31094: heap size 447 MB, throughput 0.987061
Reading from 31094: heap size 450 MB, throughput 0.979935
Reading from 31094: heap size 451 MB, throughput 0.966911
Reading from 31094: heap size 456 MB, throughput 0.957516
Reading from 31094: heap size 458 MB, throughput 0.984184
Numeric result:
Recommendation: 2 clients, utility 2.84074:
    h1: 3563.76 MB (U(h) = 0.0014905*h^0.912314)
    h2: 897.243 MB (U(h) = 0.22982*h^0.229692)
Recommendation: 2 clients, utility 2.84074:
    h1: 3563.76 MB (U(h) = 0.0014905*h^0.912314)
    h2: 897.242 MB (U(h) = 0.22982*h^0.229692)
Reading from 31094: heap size 463 MB, throughput 0.98895
Reading from 31094: heap size 464 MB, throughput 0.989565
Reading from 31094: heap size 464 MB, throughput 0.988646
Reading from 31094: heap size 466 MB, throughput 0.987869
Reading from 31092: heap size 1360 MB, throughput 0.988158
Reading from 31094: heap size 466 MB, throughput 0.987295
Numeric result:
Recommendation: 2 clients, utility 2.92581:
    h1: 3476.69 MB (U(h) = 0.00146042*h^0.915037)
    h2: 984.306 MB (U(h) = 0.193227*h^0.259061)
Recommendation: 2 clients, utility 2.92581:
    h1: 3476.69 MB (U(h) = 0.00146042*h^0.915037)
    h2: 984.307 MB (U(h) = 0.193227*h^0.259061)
Reading from 31094: heap size 467 MB, throughput 0.985906
Reading from 31092: heap size 1367 MB, throughput 0.985344
Reading from 31094: heap size 470 MB, throughput 0.989634
Reading from 31092: heap size 1392 MB, throughput 0.972085
Reading from 31094: heap size 471 MB, throughput 0.979624
Reading from 31094: heap size 471 MB, throughput 0.961473
Reading from 31094: heap size 473 MB, throughput 0.941732
Reading from 31092: heap size 1403 MB, throughput 0.959906
Reading from 31092: heap size 1472 MB, throughput 0.952529
Reading from 31092: heap size 1524 MB, throughput 0.958091
Reading from 31092: heap size 1532 MB, throughput 0.965383
Reading from 31092: heap size 1546 MB, throughput 0.973948
Reading from 31092: heap size 1549 MB, throughput 0.98223
Reading from 31094: heap size 483 MB, throughput 0.980884
Reading from 31092: heap size 1544 MB, throughput 0.97977
Numeric result:
Recommendation: 2 clients, utility 2.64842:
    h1: 3365.9 MB (U(h) = 0.00241374*h^0.839341)
    h2: 1095.1 MB (U(h) = 0.177775*h^0.27307)
Recommendation: 2 clients, utility 2.64842:
    h1: 3365.93 MB (U(h) = 0.00241374*h^0.839341)
    h2: 1095.07 MB (U(h) = 0.177775*h^0.27307)
Reading from 31094: heap size 483 MB, throughput 0.988626
Reading from 31092: heap size 1551 MB, throughput 0.986329
Reading from 31094: heap size 486 MB, throughput 0.9899
Reading from 31092: heap size 1530 MB, throughput 0.985231
Reading from 31094: heap size 489 MB, throughput 0.985391
Reading from 31092: heap size 1306 MB, throughput 0.981959
Reading from 31094: heap size 489 MB, throughput 0.986623
Reading from 31092: heap size 1509 MB, throughput 0.980668
Numeric result:
Recommendation: 2 clients, utility 2.94548:
    h1: 3429.58 MB (U(h) = 0.0012035*h^0.936898)
    h2: 1031.42 MB (U(h) = 0.168836*h^0.281767)
Recommendation: 2 clients, utility 2.94548:
    h1: 3429.58 MB (U(h) = 0.0012035*h^0.936898)
    h2: 1031.42 MB (U(h) = 0.168836*h^0.281767)
Reading from 31094: heap size 491 MB, throughput 0.986753
Reading from 31092: heap size 1331 MB, throughput 0.97857
Reading from 31094: heap size 495 MB, throughput 0.986713
Reading from 31092: heap size 1484 MB, throughput 0.976528
Reading from 31094: heap size 496 MB, throughput 0.986834
Reading from 31094: heap size 498 MB, throughput 0.980316
Reading from 31094: heap size 501 MB, throughput 0.965452
Reading from 31092: heap size 1356 MB, throughput 0.973554
Reading from 31094: heap size 509 MB, throughput 0.986552
Reading from 31092: heap size 1477 MB, throughput 0.972404
Numeric result:
Recommendation: 2 clients, utility 2.66135:
    h1: 3167.8 MB (U(h) = 0.00321201*h^0.797474)
    h2: 1293.2 MB (U(h) = 0.129881*h^0.325555)
Recommendation: 2 clients, utility 2.66135:
    h1: 3167.8 MB (U(h) = 0.00321201*h^0.797474)
    h2: 1293.2 MB (U(h) = 0.129881*h^0.325555)
Reading from 31094: heap size 509 MB, throughput 0.989565
Reading from 31092: heap size 1485 MB, throughput 0.972803
Reading from 31094: heap size 512 MB, throughput 0.99105
Reading from 31092: heap size 1486 MB, throughput 0.970815
Reading from 31094: heap size 514 MB, throughput 0.99036
Reading from 31092: heap size 1487 MB, throughput 0.968473
Reading from 31094: heap size 515 MB, throughput 0.990013
Numeric result:
Recommendation: 2 clients, utility 2.32522:
    h1: 2457.07 MB (U(h) = 0.0250375*h^0.507173)
    h2: 2003.93 MB (U(h) = 0.0762669*h^0.413706)
Recommendation: 2 clients, utility 2.32522:
    h1: 2456.89 MB (U(h) = 0.0250375*h^0.507173)
    h2: 2004.11 MB (U(h) = 0.0762669*h^0.413706)
Reading from 31092: heap size 1493 MB, throughput 0.964139
Reading from 31094: heap size 516 MB, throughput 0.989128
Reading from 31092: heap size 1497 MB, throughput 0.961063
Reading from 31094: heap size 520 MB, throughput 0.990921
Reading from 31094: heap size 521 MB, throughput 0.983376
Reading from 31094: heap size 522 MB, throughput 0.977803
Reading from 31092: heap size 1506 MB, throughput 0.959317
Reading from 31094: heap size 524 MB, throughput 0.989238
Numeric result:
Recommendation: 2 clients, utility 2.23066:
    h1: 2370.16 MB (U(h) = 0.0342343*h^0.46296)
    h2: 2090.84 MB (U(h) = 0.0786483*h^0.408363)
Recommendation: 2 clients, utility 2.23066:
    h1: 2370.26 MB (U(h) = 0.0342343*h^0.46296)
    h2: 2090.74 MB (U(h) = 0.0786483*h^0.408363)
Reading from 31092: heap size 1513 MB, throughput 0.958372
Reading from 31094: heap size 528 MB, throughput 0.991002
Reading from 31092: heap size 1525 MB, throughput 0.958206
Reading from 31094: heap size 530 MB, throughput 0.991207
Reading from 31092: heap size 1534 MB, throughput 0.95522
Reading from 31094: heap size 529 MB, throughput 0.990924
Reading from 31092: heap size 1547 MB, throughput 0.956654
Numeric result:
Recommendation: 2 clients, utility 2.13295:
    h1: 2067.65 MB (U(h) = 0.0708488*h^0.360946)
    h2: 2393.35 MB (U(h) = 0.0741681*h^0.417793)
Recommendation: 2 clients, utility 2.13295:
    h1: 2067.68 MB (U(h) = 0.0708488*h^0.360946)
    h2: 2393.32 MB (U(h) = 0.0741681*h^0.417793)
Reading from 31094: heap size 532 MB, throughput 0.989987
Reading from 31092: heap size 1552 MB, throughput 0.956399
Reading from 31094: heap size 535 MB, throughput 0.991864
Reading from 31094: heap size 536 MB, throughput 0.988427
Reading from 31094: heap size 535 MB, throughput 0.981288
Reading from 31092: heap size 1566 MB, throughput 0.957143
Reading from 31094: heap size 538 MB, throughput 0.987124
Reading from 31092: heap size 1569 MB, throughput 0.961266
Numeric result:
Recommendation: 2 clients, utility 2.19221:
    h1: 2007.77 MB (U(h) = 0.0715699*h^0.359052)
    h2: 2453.23 MB (U(h) = 0.0650398*h^0.438714)
Recommendation: 2 clients, utility 2.19221:
    h1: 2007.77 MB (U(h) = 0.0715699*h^0.359052)
    h2: 2453.23 MB (U(h) = 0.0650398*h^0.438714)
Reading from 31094: heap size 545 MB, throughput 0.990813
Reading from 31092: heap size 1583 MB, throughput 0.961607
Reading from 31094: heap size 546 MB, throughput 0.990944
Reading from 31092: heap size 1584 MB, throughput 0.96179
Reading from 31094: heap size 546 MB, throughput 0.991585
Reading from 31092: heap size 1598 MB, throughput 0.960734
Numeric result:
Recommendation: 2 clients, utility 2.32051:
    h1: 1904.58 MB (U(h) = 0.0737094*h^0.354609)
    h2: 2556.42 MB (U(h) = 0.0516553*h^0.475961)
Recommendation: 2 clients, utility 2.32051:
    h1: 1904.61 MB (U(h) = 0.0737094*h^0.354609)
    h2: 2556.39 MB (U(h) = 0.0516553*h^0.475961)
Reading from 31094: heap size 548 MB, throughput 0.990511
Reading from 31092: heap size 1598 MB, throughput 0.959933
Reading from 31094: heap size 550 MB, throughput 0.990645
Reading from 31094: heap size 551 MB, throughput 0.987685
Reading from 31094: heap size 555 MB, throughput 0.981466
Reading from 31092: heap size 1611 MB, throughput 0.960893
Reading from 31094: heap size 557 MB, throughput 0.987781
Numeric result:
Recommendation: 2 clients, utility 2.13822:
    h1: 1993.47 MB (U(h) = 0.0772377*h^0.347992)
    h2: 2467.53 MB (U(h) = 0.0680597*h^0.430714)
Recommendation: 2 clients, utility 2.13822:
    h1: 1993.56 MB (U(h) = 0.0772377*h^0.347992)
    h2: 2467.44 MB (U(h) = 0.0680597*h^0.430714)
Reading from 31092: heap size 1611 MB, throughput 0.961183
Reading from 31094: heap size 563 MB, throughput 0.991671
Reading from 31094: heap size 564 MB, throughput 0.986266
Reading from 31092: heap size 1624 MB, throughput 0.951753
Reading from 31094: heap size 566 MB, throughput 0.988717
Reading from 31092: heap size 1686 MB, throughput 0.976372
Numeric result:
Recommendation: 2 clients, utility 2.07738:
    h1: 1974.59 MB (U(h) = 0.0859301*h^0.332896)
    h2: 2486.41 MB (U(h) = 0.0729337*h^0.419191)
Recommendation: 2 clients, utility 2.07738:
    h1: 1974.57 MB (U(h) = 0.0859301*h^0.332896)
    h2: 2486.43 MB (U(h) = 0.0729337*h^0.419191)
Reading from 31094: heap size 567 MB, throughput 0.989525
Reading from 31092: heap size 1689 MB, throughput 0.982521
Reading from 31094: heap size 569 MB, throughput 0.992186
Reading from 31094: heap size 570 MB, throughput 0.9876
Client 31094 died
Clients: 1
Reading from 31092: heap size 1700 MB, throughput 0.985962
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 31092: heap size 1713 MB, throughput 0.996076
Recommendation: one client; give it all the memory
Reading from 31092: heap size 1716 MB, throughput 0.993911
Reading from 31092: heap size 1714 MB, throughput 0.988065
Reading from 31092: heap size 1717 MB, throughput 0.973902
Reading from 31092: heap size 1741 MB, throughput 0.956324
Reading from 31092: heap size 1764 MB, throughput 0.926822
Client 31092 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
