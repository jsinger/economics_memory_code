#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
THROUGHPUT_MODEL='simple'

function usage()
{
    echo "Usage: $(basename "${0}") prefix [throughput_model=${THROUGHPUT_MODEL}]"
    exit 1
}

test -z "${1}" && usage

PREFIX="${1}"

if [ ! -z "${2}" ]; then
    THROUGHPUT_MODEL="${2}"
fi

function cleanup()
{
    test ! -z "${UNBUFFER}" && kill "${UNBUFFER}" &>/dev/null
    test ! -z "${DEMUX}" && kill "${DEMUX}" &>/dev/null
    wait
    rm "${FIFO}"
    exit
}

trap "cleanup" SIGINT SIGTERM SIGHUP

FIFO="$(mktemp --dry-run)"
mkfifo "${FIFO}"

unbuffer "${DIR}/../../reading_pipe/rp" "${THROUGHPUT_MODEL}" > "${FIFO}" &
UNBUFFER="${!}"

python "${DIR}/demux_to_files.py" "${PREFIX}" "${THROUGHPUT_MODEL}" < "${FIFO}" &
DEMUX="${!}"

wait
cleanup
