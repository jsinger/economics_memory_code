economemd
    total memory: 7518 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub4_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 31811: heap size 9 MB, throughput 0.991748
Clients: 1
Client 31811 has a minimum heap size of 30 MB
Reading from 31810: heap size 9 MB, throughput 0.964232
Clients: 2
Client 31810 has a minimum heap size of 1223 MB
Reading from 31811: heap size 9 MB, throughput 0.941436
Reading from 31811: heap size 9 MB, throughput 0.964963
Reading from 31810: heap size 9 MB, throughput 0.962082
Reading from 31811: heap size 9 MB, throughput 0.968716
Reading from 31811: heap size 11 MB, throughput 0.836093
Reading from 31811: heap size 11 MB, throughput 0.549345
Reading from 31811: heap size 16 MB, throughput 0.597462
Reading from 31811: heap size 16 MB, throughput 0.55497
Reading from 31811: heap size 24 MB, throughput 0.839719
Reading from 31811: heap size 24 MB, throughput 0.839243
Reading from 31811: heap size 34 MB, throughput 0.906256
Reading from 31811: heap size 34 MB, throughput 0.881885
Reading from 31811: heap size 50 MB, throughput 0.926309
Reading from 31811: heap size 50 MB, throughput 0.933755
Reading from 31811: heap size 76 MB, throughput 0.960765
Reading from 31810: heap size 11 MB, throughput 0.974887
Reading from 31811: heap size 76 MB, throughput 0.94082
Reading from 31811: heap size 115 MB, throughput 0.966433
Reading from 31811: heap size 116 MB, throughput 0.937087
Reading from 31810: heap size 11 MB, throughput 0.98817
Reading from 31810: heap size 15 MB, throughput 0.878465
Reading from 31810: heap size 18 MB, throughput 0.949194
Reading from 31810: heap size 24 MB, throughput 0.822452
Reading from 31810: heap size 28 MB, throughput 0.553823
Reading from 31810: heap size 42 MB, throughput 0.899636
Reading from 31810: heap size 43 MB, throughput 0.848411
Reading from 31810: heap size 46 MB, throughput 0.266748
Reading from 31811: heap size 158 MB, throughput 0.94657
Reading from 31810: heap size 61 MB, throughput 0.666527
Reading from 31810: heap size 68 MB, throughput 0.665231
Reading from 31810: heap size 69 MB, throughput 0.704313
Reading from 31811: heap size 167 MB, throughput 0.808879
Reading from 31810: heap size 76 MB, throughput 0.191211
Reading from 31810: heap size 97 MB, throughput 0.719489
Reading from 31810: heap size 106 MB, throughput 0.513118
Reading from 31811: heap size 177 MB, throughput 0.993031
Reading from 31810: heap size 109 MB, throughput 0.254448
Reading from 31810: heap size 140 MB, throughput 0.79965
Reading from 31810: heap size 144 MB, throughput 0.746135
Reading from 31810: heap size 147 MB, throughput 0.71542
Reading from 31810: heap size 153 MB, throughput 0.643375
Reading from 31811: heap size 204 MB, throughput 0.920225
Reading from 31810: heap size 157 MB, throughput 0.587809
Reading from 31810: heap size 166 MB, throughput 0.108707
Reading from 31810: heap size 201 MB, throughput 0.622809
Reading from 31810: heap size 207 MB, throughput 0.662983
Reading from 31811: heap size 229 MB, throughput 0.996939
Reading from 31810: heap size 211 MB, throughput 0.615061
Reading from 31810: heap size 216 MB, throughput 0.595945
Reading from 31810: heap size 223 MB, throughput 0.140056
Reading from 31810: heap size 261 MB, throughput 0.437936
Reading from 31810: heap size 271 MB, throughput 0.586982
Reading from 31810: heap size 271 MB, throughput 0.584938
Reading from 31811: heap size 234 MB, throughput 0.880984
Reading from 31810: heap size 278 MB, throughput 0.668248
Reading from 31811: heap size 253 MB, throughput 0.987302
Reading from 31810: heap size 281 MB, throughput 0.122564
Reading from 31810: heap size 323 MB, throughput 0.493337
Reading from 31810: heap size 327 MB, throughput 0.546741
Reading from 31810: heap size 328 MB, throughput 0.49255
Reading from 31811: heap size 275 MB, throughput 0.977556
Reading from 31810: heap size 333 MB, throughput 0.131819
Reading from 31810: heap size 382 MB, throughput 0.705736
Reading from 31810: heap size 386 MB, throughput 0.692846
Reading from 31810: heap size 389 MB, throughput 0.6653
Reading from 31810: heap size 393 MB, throughput 0.618204
Reading from 31811: heap size 282 MB, throughput 0.990613
Reading from 31810: heap size 396 MB, throughput 0.550924
Reading from 31810: heap size 406 MB, throughput 0.451127
Reading from 31810: heap size 413 MB, throughput 0.490048
Equal recommendation: 3759 MB each
Reading from 31811: heap size 298 MB, throughput 0.867074
Reading from 31810: heap size 424 MB, throughput 0.142676
Reading from 31811: heap size 305 MB, throughput 0.966592
Reading from 31810: heap size 482 MB, throughput 0.452951
Reading from 31810: heap size 487 MB, throughput 0.619923
Reading from 31810: heap size 489 MB, throughput 0.510466
Reading from 31810: heap size 494 MB, throughput 0.540481
Reading from 31810: heap size 499 MB, throughput 0.519822
Reading from 31810: heap size 505 MB, throughput 0.398942
Reading from 31811: heap size 327 MB, throughput 0.994102
Reading from 31811: heap size 327 MB, throughput 0.967633
Reading from 31810: heap size 511 MB, throughput 0.0565521
Reading from 31810: heap size 573 MB, throughput 0.352447
Reading from 31810: heap size 564 MB, throughput 0.469856
Reading from 31811: heap size 343 MB, throughput 0.978397
Reading from 31810: heap size 570 MB, throughput 0.0859218
Reading from 31810: heap size 631 MB, throughput 0.510833
Reading from 31811: heap size 347 MB, throughput 0.984884
Reading from 31810: heap size 556 MB, throughput 0.524066
Reading from 31810: heap size 625 MB, throughput 0.625225
Reading from 31810: heap size 628 MB, throughput 0.492873
Reading from 31810: heap size 634 MB, throughput 0.536954
Reading from 31810: heap size 637 MB, throughput 0.534918
Reading from 31811: heap size 364 MB, throughput 0.972011
Reading from 31810: heap size 643 MB, throughput 0.774228
Reading from 31811: heap size 369 MB, throughput 0.870061
Reading from 31810: heap size 648 MB, throughput 0.278983
Reading from 31811: heap size 402 MB, throughput 0.996563
Equal recommendation: 3759 MB each
Reading from 31810: heap size 711 MB, throughput 0.823689
Reading from 31811: heap size 401 MB, throughput 0.984095
Reading from 31810: heap size 725 MB, throughput 0.541469
Reading from 31810: heap size 736 MB, throughput 0.571605
Reading from 31810: heap size 742 MB, throughput 0.333165
Reading from 31810: heap size 751 MB, throughput 0.37487
Reading from 31810: heap size 751 MB, throughput 0.407828
Reading from 31811: heap size 424 MB, throughput 0.990471
Reading from 31811: heap size 426 MB, throughput 0.982897
Reading from 31810: heap size 744 MB, throughput 0.0899904
Reading from 31810: heap size 832 MB, throughput 0.663512
Reading from 31810: heap size 827 MB, throughput 0.483936
Reading from 31811: heap size 447 MB, throughput 0.974386
Reading from 31810: heap size 832 MB, throughput 0.560604
Reading from 31810: heap size 832 MB, throughput 0.82853
Reading from 31810: heap size 834 MB, throughput 0.774045
Reading from 31811: heap size 451 MB, throughput 0.979868
Reading from 31810: heap size 840 MB, throughput 0.124066
Reading from 31810: heap size 920 MB, throughput 0.756294
Reading from 31810: heap size 926 MB, throughput 0.860528
Reading from 31811: heap size 475 MB, throughput 0.994139
Reading from 31810: heap size 929 MB, throughput 0.2499
Reading from 31811: heap size 478 MB, throughput 0.98395
Reading from 31810: heap size 1029 MB, throughput 0.597469
Reading from 31810: heap size 1031 MB, throughput 0.818969
Reading from 31810: heap size 1037 MB, throughput 0.817676
Reading from 31810: heap size 1038 MB, throughput 0.761477
Equal recommendation: 3759 MB each
Reading from 31810: heap size 1036 MB, throughput 0.755612
Reading from 31811: heap size 497 MB, throughput 0.994706
Reading from 31810: heap size 1039 MB, throughput 0.759474
Reading from 31810: heap size 1034 MB, throughput 0.816796
Reading from 31810: heap size 1038 MB, throughput 0.790904
Reading from 31811: heap size 500 MB, throughput 0.984706
Reading from 31810: heap size 1031 MB, throughput 0.968962
Reading from 31811: heap size 519 MB, throughput 0.994623
Reading from 31810: heap size 912 MB, throughput 0.915451
Reading from 31810: heap size 1030 MB, throughput 0.733656
Reading from 31810: heap size 1033 MB, throughput 0.775824
Reading from 31810: heap size 1042 MB, throughput 0.732175
Reading from 31810: heap size 1043 MB, throughput 0.745472
Reading from 31810: heap size 1039 MB, throughput 0.796192
Reading from 31811: heap size 521 MB, throughput 0.983749
Reading from 31810: heap size 1045 MB, throughput 0.816669
Reading from 31810: heap size 1033 MB, throughput 0.871682
Reading from 31810: heap size 1041 MB, throughput 0.874389
Reading from 31810: heap size 1032 MB, throughput 0.858983
Reading from 31811: heap size 538 MB, throughput 0.99031
Reading from 31810: heap size 1040 MB, throughput 0.743028
Reading from 31810: heap size 1034 MB, throughput 0.66596
Reading from 31810: heap size 1048 MB, throughput 0.640194
Reading from 31810: heap size 1062 MB, throughput 0.656393
Reading from 31811: heap size 540 MB, throughput 0.979427
Reading from 31810: heap size 1072 MB, throughput 0.091656
Reading from 31810: heap size 1188 MB, throughput 0.553822
Reading from 31810: heap size 1195 MB, throughput 0.720614
Reading from 31810: heap size 1206 MB, throughput 0.795233
Reading from 31810: heap size 1208 MB, throughput 0.693105
Reading from 31811: heap size 560 MB, throughput 0.990531
Equal recommendation: 3759 MB each
Reading from 31810: heap size 1212 MB, throughput 0.94526
Reading from 31811: heap size 562 MB, throughput 0.984364
Reading from 31811: heap size 584 MB, throughput 0.967229
Reading from 31810: heap size 1217 MB, throughput 0.97926
Reading from 31811: heap size 583 MB, throughput 0.994605
Reading from 31810: heap size 1209 MB, throughput 0.980043
Reading from 31811: heap size 606 MB, throughput 0.987659
Reading from 31811: heap size 611 MB, throughput 0.980271
Equal recommendation: 3759 MB each
Reading from 31810: heap size 1219 MB, throughput 0.969313
Reading from 31811: heap size 639 MB, throughput 0.994735
Reading from 31811: heap size 641 MB, throughput 0.987511
Reading from 31810: heap size 1209 MB, throughput 0.965505
Reading from 31811: heap size 666 MB, throughput 0.986613
Reading from 31810: heap size 1216 MB, throughput 0.976931
Reading from 31811: heap size 667 MB, throughput 0.991246
Reading from 31811: heap size 689 MB, throughput 0.996567
Equal recommendation: 3759 MB each
Reading from 31810: heap size 1222 MB, throughput 0.974789
Reading from 31811: heap size 692 MB, throughput 0.987839
Reading from 31811: heap size 712 MB, throughput 0.990158
Reading from 31810: heap size 1223 MB, throughput 0.979631
Reading from 31811: heap size 714 MB, throughput 0.992907
Reading from 31810: heap size 1218 MB, throughput 0.977212
Reading from 31811: heap size 735 MB, throughput 0.996887
Equal recommendation: 3759 MB each
Reading from 31811: heap size 738 MB, throughput 0.989302
Reading from 31810: heap size 1223 MB, throughput 0.973706
Reading from 31811: heap size 757 MB, throughput 0.987296
Reading from 31810: heap size 1213 MB, throughput 0.96804
Reading from 31811: heap size 757 MB, throughput 0.989091
Reading from 31811: heap size 782 MB, throughput 0.991089
Reading from 31810: heap size 1219 MB, throughput 0.96929
Reading from 31811: heap size 783 MB, throughput 0.993192
Equal recommendation: 3759 MB each
Reading from 31810: heap size 1221 MB, throughput 0.971623
Reading from 31811: heap size 803 MB, throughput 0.99169
Reading from 31811: heap size 806 MB, throughput 0.99553
Reading from 31810: heap size 1221 MB, throughput 0.963301
Reading from 31811: heap size 824 MB, throughput 0.991843
Reading from 31810: heap size 1225 MB, throughput 0.969886
Equal recommendation: 3759 MB each
Reading from 31811: heap size 827 MB, throughput 0.99126
Reading from 31811: heap size 851 MB, throughput 0.996097
Reading from 31810: heap size 1227 MB, throughput 0.972458
Reading from 31811: heap size 851 MB, throughput 0.994804
Reading from 31810: heap size 1231 MB, throughput 0.96726
Reading from 31811: heap size 869 MB, throughput 0.991782
Equal recommendation: 3759 MB each
Reading from 31811: heap size 871 MB, throughput 0.987486
Reading from 31810: heap size 1235 MB, throughput 0.969461
Reading from 31811: heap size 893 MB, throughput 0.99159
Reading from 31810: heap size 1240 MB, throughput 0.680641
Reading from 31811: heap size 894 MB, throughput 0.991922
Reading from 31811: heap size 921 MB, throughput 0.991339
Equal recommendation: 3759 MB each
Reading from 31810: heap size 1312 MB, throughput 0.993392
Reading from 31811: heap size 921 MB, throughput 0.991327
Reading from 31810: heap size 1309 MB, throughput 0.989617
Reading from 31811: heap size 949 MB, throughput 0.992404
Reading from 31810: heap size 1319 MB, throughput 0.988893
Reading from 31811: heap size 949 MB, throughput 0.994045
Equal recommendation: 3759 MB each
Reading from 31811: heap size 973 MB, throughput 0.991067
Reading from 31810: heap size 1327 MB, throughput 0.988101
Reading from 31811: heap size 975 MB, throughput 0.989267
Reading from 31810: heap size 1328 MB, throughput 0.98464
Reading from 31811: heap size 1004 MB, throughput 0.991573
Reading from 31810: heap size 1322 MB, throughput 0.983844
Equal recommendation: 3759 MB each
Reading from 31811: heap size 1004 MB, throughput 0.991851
Reading from 31810: heap size 1217 MB, throughput 0.982756
Reading from 31811: heap size 1036 MB, throughput 0.990211
Reading from 31810: heap size 1309 MB, throughput 0.981352
Reading from 31811: heap size 1036 MB, throughput 0.990825
Equal recommendation: 3759 MB each
Reading from 31810: heap size 1236 MB, throughput 0.980258
Reading from 31811: heap size 1070 MB, throughput 0.993643
Reading from 31810: heap size 1305 MB, throughput 0.975179
Reading from 31811: heap size 1070 MB, throughput 0.988995
Reading from 31811: heap size 1100 MB, throughput 0.992978
Reading from 31810: heap size 1308 MB, throughput 0.976426
Equal recommendation: 3759 MB each
Reading from 31811: heap size 1102 MB, throughput 0.993093
Reading from 31810: heap size 1310 MB, throughput 0.975949
Reading from 31811: heap size 1131 MB, throughput 0.990446
Reading from 31810: heap size 1312 MB, throughput 0.974488
Reading from 31811: heap size 1133 MB, throughput 0.991382
Equal recommendation: 3759 MB each
Reading from 31810: heap size 1314 MB, throughput 0.973702
Reading from 31811: heap size 1168 MB, throughput 0.994022
Reading from 31810: heap size 1320 MB, throughput 0.971845
Reading from 31811: heap size 1168 MB, throughput 0.990519
Reading from 31810: heap size 1324 MB, throughput 0.965311
Equal recommendation: 3759 MB each
Reading from 31811: heap size 1202 MB, throughput 0.99319
Reading from 31810: heap size 1332 MB, throughput 0.968115
Reading from 31811: heap size 1202 MB, throughput 0.994112
Reading from 31810: heap size 1340 MB, throughput 0.972056
Reading from 31811: heap size 1232 MB, throughput 0.991994
Equal recommendation: 3759 MB each
Reading from 31810: heap size 1344 MB, throughput 0.971635
Reading from 31811: heap size 1234 MB, throughput 0.993392
Reading from 31810: heap size 1351 MB, throughput 0.972622
Reading from 31811: heap size 1267 MB, throughput 0.991496
Reading from 31810: heap size 1354 MB, throughput 0.968694
Equal recommendation: 3759 MB each
Reading from 31811: heap size 1269 MB, throughput 0.992875
Reading from 31810: heap size 1361 MB, throughput 0.970035
Reading from 31811: heap size 1305 MB, throughput 0.993913
Reading from 31810: heap size 1361 MB, throughput 0.97178
Reading from 31811: heap size 1305 MB, throughput 0.993106
Equal recommendation: 3759 MB each
Reading from 31811: heap size 1340 MB, throughput 0.989803
Reading from 31811: heap size 1340 MB, throughput 0.991842
Equal recommendation: 3759 MB each
Reading from 31811: heap size 1382 MB, throughput 0.989952
Reading from 31811: heap size 1384 MB, throughput 0.99131
Reading from 31810: heap size 1367 MB, throughput 0.856378
Reading from 31811: heap size 1430 MB, throughput 0.990731
Equal recommendation: 3759 MB each
Reading from 31811: heap size 1432 MB, throughput 0.987047
Reading from 31811: heap size 1478 MB, throughput 0.993782
Equal recommendation: 3759 MB each
Reading from 31811: heap size 1482 MB, throughput 0.989003
Reading from 31810: heap size 1534 MB, throughput 0.986182
Reading from 31811: heap size 1527 MB, throughput 0.989823
Reading from 31810: heap size 1583 MB, throughput 0.981859
Reading from 31810: heap size 1584 MB, throughput 0.801095
Reading from 31810: heap size 1571 MB, throughput 0.719515
Equal recommendation: 3759 MB each
Reading from 31810: heap size 1591 MB, throughput 0.706683
Reading from 31810: heap size 1618 MB, throughput 0.723126
Reading from 31810: heap size 1629 MB, throughput 0.711308
Reading from 31810: heap size 1662 MB, throughput 0.755753
Reading from 31811: heap size 1531 MB, throughput 1
Reading from 31811: heap size 1531 MB, throughput 1
Reading from 31811: heap size 1531 MB, throughput 0.00331949
Reading from 31810: heap size 1666 MB, throughput 0.959173
Reading from 31811: heap size 1583 MB, throughput 0.993548
Reading from 31810: heap size 1689 MB, throughput 0.982201
Equal recommendation: 3759 MB each
Reading from 31811: heap size 1587 MB, throughput 0.991511
Reading from 31810: heap size 1696 MB, throughput 0.980696
Client 31811 died
Clients: 1
Reading from 31810: heap size 1699 MB, throughput 0.986765
Recommendation: one client; give it all the memory
Reading from 31810: heap size 1710 MB, throughput 0.986992
Reading from 31810: heap size 1701 MB, throughput 0.987931
Recommendation: one client; give it all the memory
Reading from 31810: heap size 1713 MB, throughput 0.986288
Recommendation: one client; give it all the memory
Reading from 31810: heap size 1701 MB, throughput 0.986506
Reading from 31810: heap size 1711 MB, throughput 0.988348
Recommendation: one client; give it all the memory
Reading from 31810: heap size 1714 MB, throughput 0.989173
Reading from 31810: heap size 1718 MB, throughput 0.987782
Recommendation: one client; give it all the memory
Reading from 31810: heap size 1705 MB, throughput 0.987281
Recommendation: one client; give it all the memory
Reading from 31810: heap size 1615 MB, throughput 0.986215
Reading from 31810: heap size 1701 MB, throughput 0.98532
Recommendation: one client; give it all the memory
Reading from 31810: heap size 1707 MB, throughput 0.984266
Recommendation: one client; give it all the memory
Reading from 31810: heap size 1710 MB, throughput 0.982872
Reading from 31810: heap size 1713 MB, throughput 0.983001
Recommendation: one client; give it all the memory
Reading from 31810: heap size 1720 MB, throughput 0.982408
Recommendation: one client; give it all the memory
Reading from 31810: heap size 1725 MB, throughput 0.854712
Reading from 31810: heap size 1683 MB, throughput 0.996658
Recommendation: one client; give it all the memory
Reading from 31810: heap size 1686 MB, throughput 0.995183
Recommendation: one client; give it all the memory
Reading from 31810: heap size 1702 MB, throughput 0.994293
Reading from 31810: heap size 1708 MB, throughput 0.992777
Recommendation: one client; give it all the memory
Reading from 31810: heap size 1708 MB, throughput 0.992048
Recommendation: one client; give it all the memory
Reading from 31810: heap size 1532 MB, throughput 0.991055
Reading from 31810: heap size 1691 MB, throughput 0.990398
Recommendation: one client; give it all the memory
Reading from 31810: heap size 1563 MB, throughput 0.988461
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 31810: heap size 1675 MB, throughput 0.997815
Recommendation: one client; give it all the memory
Reading from 31810: heap size 1580 MB, throughput 0.989443
Reading from 31810: heap size 1658 MB, throughput 0.878424
Reading from 31810: heap size 1680 MB, throughput 0.791238
Reading from 31810: heap size 1713 MB, throughput 0.820236
Reading from 31810: heap size 1736 MB, throughput 0.803939
Client 31810 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
