#include <algorithm>
#include <cstdlib>
#include <unistd.h>
#include <Log.hpp>
#include <Utilities.hpp>
#include "Client.hpp"
#include "ClientList.hpp"
#include "TimerThread.hpp"
#include "UtilityFunction.hpp"
#include "UtilityModel.hpp"

TimerThread::TimerThread(unsigned long long totalMemMB,
                         unsigned long long timerIntervalSeconds,
                         bool timerRunsAtStart,
                         bool makeRecommendations,
                         bool equalRecommendations,
                         bool randomRecommendations,
                         economem::RecommendationSender& sender,
                         UtilityModel& model,
                         ClientList& clients,
                         economem::Log& log)
    : Thread(),
      m_TotalMemMB(totalMemMB),
      m_TimerIntervalSeconds(timerIntervalSeconds),
      m_Paused(!timerRunsAtStart),
      m_Sender(sender),
      m_Model(model),
      m_Clients(clients),
      m_Log(log),
      m_MakeRecommendations(makeRecommendations),
      m_EqualRecommendations(equalRecommendations),
      m_RandomRecommendations(randomRecommendations)
{
    pthread_cond_init(&m_Cond, NULL);
}

TimerThread::~TimerThread()
{
    pthread_cond_destroy(&m_Cond);
}

void TimerThread::onKill()
{
    pthread_cond_signal(&m_Cond);
    signal();
}

bool TimerThread::pause()
{
    bool retval;
    m_Lock.lock();
    if (!m_Paused)
    {
        m_Paused = true;
        retval = true;
        onKill();
    }
    else
        retval = false;
    m_Lock.unlock();
    return retval;
}

bool TimerThread::unpause()
{
    bool retval;
    m_Lock.lock();
    if (m_Paused)
    {
        m_Paused = false;
        retval = true;
        onKill();
    }
    else
        retval = false;
    m_Lock.unlock();
    return retval;
}

bool TimerThread::isPaused()
{
    bool retval;
    m_Lock.lock();
    retval = m_Paused;
    m_Lock.unlock();
    return retval;
}

unsigned long long TimerThread::getTimerIntervalSeconds()
{
    unsigned long long retval;
    m_Lock.lock();
    retval = m_TimerIntervalSeconds;
    m_Lock.unlock();
    return retval;
}

bool TimerThread::setTimerIntervalSeconds(unsigned long long t)
{
    if (t == 0)
        return false;

    m_Lock.lock();
    m_TimerIntervalSeconds = t;
    onKill();
    m_Lock.unlock();
    return true;
}

unsigned long long TimerThread::getTotalMemMB()
{
    unsigned long long retval;
    m_Lock.lock();
    retval = m_TotalMemMB;
    m_Lock.unlock();
    return retval;
}

bool TimerThread::setTotalMemMB(unsigned long long t)
{
    if (t == 0)
        return false;

    m_Lock.lock();
    m_TotalMemMB = t;
    // We don't interrupt here because nothing waits on totalMem
    m_Lock.unlock();

    m_Log.logTotalMemMB(t);
    return true;
}

void TimerThread::threadFunc()
{
    unsigned long long timer;
    bool paused;

    economem::info("TimerThread", "starting");

    while (true)
    {
        m_Lock.lock();

        while (isAlive() && m_Paused)
            condWaitOnLock(&m_Cond);

        if (!isAlive())
        {
            m_Lock.unlock();
            goto cleanup;
        }

        // Else we must have been unpaused
        timer = m_TimerIntervalSeconds;
        paused = m_Paused;
        m_Lock.unlock();

        unsigned long long t = timer;

        while ((t = sleep(t)) != 0)
        {
            // We got interrupted; check what to do
            m_Lock.lock();

            if (!isAlive())
            {
                // We're dead
                m_Lock.unlock();
                goto cleanup;
            }
            else if (m_Paused)
            {
                // We've just been paused, so go back to the condition variable
                paused = m_Paused;
                m_Lock.unlock();
                break;
            }
            else if (m_TimerIntervalSeconds != timer)
            {
                // The timer got changed
                timer = m_TimerIntervalSeconds;
                t = timer;
                m_Lock.unlock();
            }
            else
            {
                // Nothing in particular happened, so just go back to sleep
                m_Lock.unlock();
            }
        }

        if (paused)
        {
            // We've just been paused, so go back to the condition variable
            continue;
        }


        // The timer is now ready to actually fire
        doTimer();
    }

cleanup:
    economem::info("TimerThread", "finished");
}

void TimerThread::doTimer()
{
    unsigned long long totalMemMB = getTotalMemMB();
    m_Clients.lock();
    size_t count = m_Clients.count();

    m_Clients.calculateAllFunctions(m_Model);

    if (m_MakeRecommendations)
    {
        if (count == 0)
            printf("Recommendation: no clients; no recommendation\n");
        else if (m_RandomRecommendations)
        {
            std::vector<economem::Recommendation> l(count);
            printf("Random recommendations:\n");
            for (size_t i = 0; i < count; ++i)
            {
                l[i].setHeapSizeMB(economem::randInt(m_Clients.getClient(i)->getMinHeapMB(), totalMemMB));
                printf("    client %zu: %llu MB\n", i, l[i].getHeapSizeMB());
            }
            m_Clients.makeRecommendations(l, m_Sender);
        }
        else if (count == 1)
        {
            printf("Recommendation: one client; give it all the memory\n");
            std::vector<economem::Recommendation> l(1);
            l[0] = economem::Recommendation(totalMemMB);
            m_Clients.makeRecommendations(l, m_Sender);
        }
        else if (m_EqualRecommendations)
        {
            economem::Recommendation r(totalMemMB / m_Clients.count());
            printf("Equal recommendation: %llu MB each\n", totalMemMB / m_Clients.count());
            m_Clients.recommendToAll(r, m_Sender);
        }
        else
        {
            if (m_Clients.allValid())
            {
                std::vector<const UtilityFunction*> functions(count);
                std::vector<unsigned long long> minHeaps(count);

                for (size_t i = 0; i < count; ++i)
                {
                    Client* c = m_Clients.getClient(i);
                    functions[i] = c->getCurrentFunction();
                    minHeaps[i] = c->getMinHeapMB();
                }

                PartitionResult* r = m_Model.partitionMemory(totalMemMB, functions, minHeaps);
                r->print(functions);

                std::vector<economem::Recommendation> l(count);
                for (size_t i = 0; i < count; ++i)
                    l[i].setHeapSizeMB(std::round(r->heapSizes[i]));
                delete r;
                m_Clients.makeRecommendations(l, m_Sender);
            }
            else
                printf("Recommendation: %zu clients, but at least one doesn't have enough data - no recommendation!\n", count);
        }
    }

    m_Clients.unlock();
}
