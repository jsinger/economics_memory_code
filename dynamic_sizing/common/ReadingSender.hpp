#ifndef READINGSENDER_HPP
#define READINGSENDER_HPP

#include "Messages.hpp"
#include "SendSock.hpp"
#include "Utilities.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
//! Send readings from a client to a daemon.
    class ReadingSender : public SendSock
    {
    public:
        ReadingSender(const char* daemonName=economem::NAME_DEFAULT);

        bool sendReading(unsigned long long heapSizeMB, double throughput);
        bool sendMinHeapMB(unsigned long long minHeapMB);
        bool sendDeathNotification();

        //! Send an existing reading without modifying its sender field
        bool forwardExistingReading(const economem::Reading& reading);

    private:
        bool sendMsg(economem::ReadingType t, unsigned long long heapSizeMB, double throughput);
        const char* m_DaemonName;
    };
}

/*! @} */

#endif
