#include <cmath>
#include <cstdlib>
#include "Timer.hpp"

economem::Timer::Timer(double seconds)
{
    setTimer(seconds);
}

void economem::Timer::setTimer(double seconds)
{
    timeval now;
    gettimeofday(&now, NULL);

    timeval interval;
    interval.tv_sec = (time_t)seconds;
    interval.tv_usec = (suseconds_t)(1000000 * (seconds - floor(seconds)));

    timeradd(&now, &interval, &m_Target);
}

bool economem::Timer::hasElapsed() const
{
    timeval now;
    gettimeofday(&now, NULL);

    return (bool)timercmp(&now, &m_Target, >=);
}
