#!/usr/bin/env python
# Measure the number of iterations a benchmark can complete in a fixed time, on remote machines

import argparse
import argtypes
import distributor.distribute as d
import os.path
import shutil
import staticutility as su
import tempfile

RUNS = 5 # default number of runs


parser = argparse.ArgumentParser(description="Measure the number of iterations a benchmark can complete in a fixed time, on remote machines")

parser.add_argument("hostsFile", type=argtypes.existing_absolute_path, help="hosts to run the remote job on")
parser.add_argument("gatewayFile", type=argtypes.existing_absolute_path, help="Machine for one-off setup actions")
parser.add_argument("configFile", type=argtypes.existing_absolute_path, help="Each line in config file should be of the form 'benchmark input_size threads time min_heap max_heap increment', where time should be in a format suitable for use with 'sleep'")

parser.add_argument("-r", "--runs", type=argtypes.positive_int, default=RUNS, help="how many times to run the measurement, for taking averages")
parser.add_argument("-e", "--email", help="Email address to notify of completion")
parser.add_argument("--gatewayScpFile", type=argtypes.existing_absolute_path, help="Command to copy files to the gateway")
parser.add_argument("--gatewayUserFile", type=argtypes.existing_absolute_path, help="User to log into the gateway as")
parser.add_argument("--hostsSshFile", type=argtypes.existing_absolute_path, help="Command to log into the remote hosts")
parser.add_argument("--hostsUserFile", type=argtypes.existing_absolute_path, help="User to log into the remote hosts as")

args = parser.parse_args()
distributeArgs = {}
remoteMainDir = "economics_memory_code"
remoteThisDir = os.path.join(remoteMainDir, "static_sizing")

hostsFile = args.hostsFile
hosts = su.fileToList(hostsFile)
numHosts = len(hosts)
runs = args.runs
configFile = args.configFile
distributeArgs["gateway"] = su.firstLineOfFile(args.gatewayFile)
distributeArgs["name"] = "Measure"

if args.email:
    distributeArgs["notificationEmail"] = args.email
    distributeArgs["emailSenderConfig"] = os.path.join(remoteMainDir, "email.conf")

def fileOrNone(filename, arg):
    if filename:
        distributeArgs[arg] = su.firstLineOfFile(filename)

fileOrNone(args.gatewayScpFile, "gatewayScpCommand")
fileOrNone(args.gatewayUserFile, "gatewaySshUser")
fileOrNone(args.hostsSshFile, "hostsSshCommand")
fileOrNone(args.hostsUserFile, "hostsSshUser")

tmpdir = su.MeasureDir(tempfile.mkdtemp())
tmpdir.writeRuns(runs)
tmpdir.pullInConfigFile(configFile)

inputLines = tmpdir.genInputFromConfig()

tmpdir.timestampStartRemote(numHosts, sum(map(lambda l: su.timeToSeconds(l.b.time), inputLines)), runs)

remoteOutdir = os.path.join(remoteThisDir, su.measureDirName())

distributeArgs["hosts"] = hosts
distributeArgs["inputFile"] = tmpdir.inputFile()
distributeArgs["remoteCommand"] = os.path.join(remoteThisDir, "measureRemoteHarness.py")
distributeArgs["remoteArgs"] = [remoteOutdir, runs]

distributeArgs["gatewayFiles"] = [(str(tmpdir), remoteOutdir)]


try:
    d.distribute(**distributeArgs)
except d.DistributeException as e:
    print e


shutil.rmtree(str(tmpdir))

print "Don't forget to collect and process the data once it's finished"
