economemd
    total memory: 938 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_30_14_13_41/sub4_timerComparisonSingle/sub3_timeBenchmarkDaemonEqual/daemon_run10_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 5260: heap size 9 MB, throughput 0.991939
Clients: 1
Client 5260 has a minimum heap size of 276 MB
Reading from 5261: heap size 9 MB, throughput 0.992216
Clients: 2
Client 5261 has a minimum heap size of 276 MB
Reading from 5261: heap size 9 MB, throughput 0.976269
Reading from 5260: heap size 9 MB, throughput 0.972259
Reading from 5261: heap size 9 MB, throughput 0.962283
Reading from 5260: heap size 9 MB, throughput 0.962172
Reading from 5261: heap size 9 MB, throughput 0.946867
Reading from 5260: heap size 9 MB, throughput 0.929044
Reading from 5261: heap size 11 MB, throughput 0.985454
Reading from 5260: heap size 11 MB, throughput 0.976875
Reading from 5261: heap size 11 MB, throughput 0.985057
Reading from 5260: heap size 11 MB, throughput 0.970525
Reading from 5261: heap size 17 MB, throughput 0.968875
Reading from 5260: heap size 17 MB, throughput 0.942985
Reading from 5260: heap size 17 MB, throughput 0.472873
Reading from 5261: heap size 17 MB, throughput 0.509106
Reading from 5260: heap size 30 MB, throughput 0.934428
Reading from 5261: heap size 30 MB, throughput 0.952776
Reading from 5260: heap size 31 MB, throughput 0.89922
Reading from 5261: heap size 31 MB, throughput 0.887128
Reading from 5260: heap size 34 MB, throughput 0.435316
Reading from 5261: heap size 33 MB, throughput 0.927011
Reading from 5260: heap size 47 MB, throughput 0.771097
Reading from 5260: heap size 51 MB, throughput 0.893435
Reading from 5261: heap size 34 MB, throughput 0.167307
Reading from 5261: heap size 50 MB, throughput 0.825923
Reading from 5260: heap size 51 MB, throughput 0.309847
Reading from 5261: heap size 50 MB, throughput 0.861095
Reading from 5260: heap size 70 MB, throughput 0.619266
Reading from 5260: heap size 72 MB, throughput 0.731579
Reading from 5261: heap size 53 MB, throughput 0.161847
Reading from 5261: heap size 71 MB, throughput 0.704436
Reading from 5260: heap size 78 MB, throughput 0.376225
Reading from 5260: heap size 97 MB, throughput 0.729055
Reading from 5260: heap size 106 MB, throughput 0.81259
Reading from 5261: heap size 76 MB, throughput 0.324491
Reading from 5261: heap size 99 MB, throughput 0.553875
Reading from 5260: heap size 106 MB, throughput 0.604895
Reading from 5261: heap size 103 MB, throughput 0.726656
Reading from 5261: heap size 104 MB, throughput 0.755762
Reading from 5261: heap size 105 MB, throughput 0.71322
Reading from 5261: heap size 110 MB, throughput 0.688526
Reading from 5260: heap size 109 MB, throughput 0.113942
Reading from 5261: heap size 112 MB, throughput 0.162006
Reading from 5261: heap size 142 MB, throughput 0.560167
Reading from 5261: heap size 148 MB, throughput 0.751462
Reading from 5260: heap size 137 MB, throughput 0.22362
Reading from 5261: heap size 150 MB, throughput 0.668791
Reading from 5261: heap size 153 MB, throughput 0.580531
Reading from 5260: heap size 174 MB, throughput 0.716171
Reading from 5261: heap size 158 MB, throughput 0.694994
Reading from 5260: heap size 176 MB, throughput 0.799451
Reading from 5261: heap size 162 MB, throughput 0.642534
Reading from 5261: heap size 169 MB, throughput 0.519918
Reading from 5260: heap size 179 MB, throughput 0.763256
Reading from 5261: heap size 174 MB, throughput 0.616621
Reading from 5260: heap size 182 MB, throughput 0.71321
Reading from 5261: heap size 181 MB, throughput 0.587622
Reading from 5260: heap size 187 MB, throughput 0.644516
Reading from 5261: heap size 187 MB, throughput 0.0528946
Reading from 5260: heap size 193 MB, throughput 0.252868
Reading from 5261: heap size 222 MB, throughput 0.814302
Reading from 5261: heap size 221 MB, throughput 0.835271
Reading from 5260: heap size 241 MB, throughput 0.916774
Reading from 5261: heap size 224 MB, throughput 0.719619
Reading from 5260: heap size 244 MB, throughput 0.85447
Reading from 5261: heap size 226 MB, throughput 0.70875
Reading from 5260: heap size 249 MB, throughput 0.62738
Reading from 5261: heap size 226 MB, throughput 0.658002
Reading from 5260: heap size 254 MB, throughput 0.602125
Reading from 5261: heap size 228 MB, throughput 0.728555
Reading from 5261: heap size 229 MB, throughput 0.442441
Reading from 5260: heap size 259 MB, throughput 0.833144
Reading from 5260: heap size 262 MB, throughput 0.732375
Reading from 5260: heap size 267 MB, throughput 0.731459
Reading from 5261: heap size 233 MB, throughput 0.149896
Reading from 5260: heap size 270 MB, throughput 0.629621
Reading from 5261: heap size 270 MB, throughput 0.66795
Reading from 5260: heap size 276 MB, throughput 0.682782
Reading from 5261: heap size 267 MB, throughput 0.618912
Reading from 5260: heap size 280 MB, throughput 0.624284
Reading from 5260: heap size 287 MB, throughput 0.688758
Reading from 5261: heap size 269 MB, throughput 0.858164
Reading from 5260: heap size 289 MB, throughput 0.698048
Reading from 5261: heap size 265 MB, throughput 0.702785
Reading from 5261: heap size 268 MB, throughput 0.676649
Reading from 5261: heap size 266 MB, throughput 0.740055
Reading from 5261: heap size 268 MB, throughput 0.773566
Reading from 5261: heap size 269 MB, throughput 0.799859
Reading from 5260: heap size 294 MB, throughput 0.929742
Reading from 5260: heap size 295 MB, throughput 0.672543
Reading from 5261: heap size 271 MB, throughput 0.364482
Reading from 5260: heap size 289 MB, throughput 0.0770437
Reading from 5261: heap size 317 MB, throughput 0.679407
Reading from 5260: heap size 337 MB, throughput 0.692947
Reading from 5261: heap size 318 MB, throughput 0.856469
Reading from 5260: heap size 340 MB, throughput 0.872584
Reading from 5261: heap size 321 MB, throughput 0.774778
Reading from 5261: heap size 322 MB, throughput 0.799366
Reading from 5260: heap size 341 MB, throughput 0.875919
Reading from 5261: heap size 325 MB, throughput 0.880012
Reading from 5260: heap size 342 MB, throughput 0.789459
Reading from 5260: heap size 344 MB, throughput 0.753174
Reading from 5261: heap size 325 MB, throughput 0.898892
Reading from 5260: heap size 341 MB, throughput 0.608022
Reading from 5261: heap size 329 MB, throughput 0.800319
Reading from 5261: heap size 329 MB, throughput 0.733371
Reading from 5260: heap size 344 MB, throughput 0.600563
Reading from 5260: heap size 347 MB, throughput 0.685742
Reading from 5261: heap size 332 MB, throughput 0.625157
Equal recommendation: 469 MB each
Reading from 5261: heap size 333 MB, throughput 0.571971
Reading from 5260: heap size 348 MB, throughput 0.546598
Reading from 5261: heap size 339 MB, throughput 0.761831
Reading from 5261: heap size 339 MB, throughput 0.616484
Reading from 5260: heap size 356 MB, throughput 0.948078
Reading from 5261: heap size 347 MB, throughput 0.978659
Reading from 5260: heap size 356 MB, throughput 0.966914
Reading from 5261: heap size 347 MB, throughput 0.979267
Reading from 5260: heap size 360 MB, throughput 0.962645
Reading from 5261: heap size 348 MB, throughput 0.971239
Reading from 5260: heap size 362 MB, throughput 0.743431
Reading from 5261: heap size 351 MB, throughput 0.977339
Reading from 5261: heap size 352 MB, throughput 0.968376
Reading from 5260: heap size 411 MB, throughput 0.992545
Reading from 5260: heap size 412 MB, throughput 0.992521
Reading from 5261: heap size 355 MB, throughput 0.975901
Reading from 5260: heap size 420 MB, throughput 0.99044
Reading from 5261: heap size 352 MB, throughput 0.967726
Equal recommendation: 469 MB each
Reading from 5260: heap size 421 MB, throughput 0.988639
Reading from 5261: heap size 355 MB, throughput 0.977987
Reading from 5260: heap size 420 MB, throughput 0.986686
Reading from 5261: heap size 355 MB, throughput 0.973848
Reading from 5260: heap size 423 MB, throughput 0.986385
Reading from 5261: heap size 356 MB, throughput 0.972599
Reading from 5260: heap size 418 MB, throughput 0.984824
Reading from 5261: heap size 358 MB, throughput 0.971448
Reading from 5261: heap size 359 MB, throughput 0.9779
Reading from 5260: heap size 380 MB, throughput 0.98175
Reading from 5261: heap size 362 MB, throughput 0.969197
Reading from 5260: heap size 413 MB, throughput 0.977291
Reading from 5261: heap size 363 MB, throughput 0.960884
Equal recommendation: 469 MB each
Reading from 5260: heap size 416 MB, throughput 0.977629
Reading from 5260: heap size 416 MB, throughput 0.952117
Reading from 5260: heap size 417 MB, throughput 0.806623
Reading from 5260: heap size 413 MB, throughput 0.773813
Reading from 5260: heap size 421 MB, throughput 0.769745
Reading from 5261: heap size 365 MB, throughput 0.986123
Reading from 5260: heap size 430 MB, throughput 0.908338
Reading from 5261: heap size 367 MB, throughput 0.769707
Reading from 5261: heap size 363 MB, throughput 0.836395
Reading from 5261: heap size 368 MB, throughput 0.768626
Reading from 5261: heap size 374 MB, throughput 0.798393
Reading from 5261: heap size 377 MB, throughput 0.953087
Reading from 5260: heap size 435 MB, throughput 0.983748
Reading from 5261: heap size 383 MB, throughput 0.979238
Reading from 5260: heap size 430 MB, throughput 0.984024
Reading from 5261: heap size 384 MB, throughput 0.985103
Reading from 5260: heap size 435 MB, throughput 0.980618
Reading from 5261: heap size 385 MB, throughput 0.98588
Reading from 5260: heap size 430 MB, throughput 0.979145
Reading from 5261: heap size 387 MB, throughput 0.983352
Equal recommendation: 469 MB each
Reading from 5260: heap size 434 MB, throughput 0.981802
Reading from 5261: heap size 385 MB, throughput 0.985539
Reading from 5260: heap size 430 MB, throughput 0.980753
Reading from 5261: heap size 388 MB, throughput 0.973911
Reading from 5260: heap size 433 MB, throughput 0.980305
Reading from 5261: heap size 385 MB, throughput 0.980578
Reading from 5260: heap size 431 MB, throughput 0.983327
Reading from 5261: heap size 387 MB, throughput 0.970726
Reading from 5260: heap size 432 MB, throughput 0.979303
Reading from 5261: heap size 383 MB, throughput 0.981679
Reading from 5260: heap size 431 MB, throughput 0.981001
Reading from 5261: heap size 386 MB, throughput 0.706323
Equal recommendation: 469 MB each
Reading from 5260: heap size 432 MB, throughput 0.987894
Reading from 5260: heap size 433 MB, throughput 0.968054
Reading from 5260: heap size 433 MB, throughput 0.828581
Reading from 5260: heap size 436 MB, throughput 0.857159
Reading from 5261: heap size 424 MB, throughput 0.969543
Reading from 5260: heap size 437 MB, throughput 0.929649
Reading from 5261: heap size 426 MB, throughput 0.987736
Reading from 5261: heap size 428 MB, throughput 0.895493
Reading from 5261: heap size 429 MB, throughput 0.854575
Reading from 5260: heap size 443 MB, throughput 0.987527
Reading from 5261: heap size 434 MB, throughput 0.867207
Reading from 5261: heap size 435 MB, throughput 0.986092
Reading from 5260: heap size 444 MB, throughput 0.989629
Reading from 5261: heap size 441 MB, throughput 0.987944
Reading from 5260: heap size 449 MB, throughput 0.987736
Reading from 5261: heap size 442 MB, throughput 0.989391
Reading from 5260: heap size 450 MB, throughput 0.987425
Reading from 5261: heap size 444 MB, throughput 0.985126
Equal recommendation: 469 MB each
Reading from 5260: heap size 449 MB, throughput 0.986568
Reading from 5261: heap size 446 MB, throughput 0.986077
Reading from 5260: heap size 451 MB, throughput 0.984621
Reading from 5261: heap size 444 MB, throughput 0.988966
Reading from 5260: heap size 449 MB, throughput 0.985554
Reading from 5261: heap size 447 MB, throughput 0.985097
Reading from 5260: heap size 451 MB, throughput 0.985483
Reading from 5261: heap size 446 MB, throughput 0.981627
Reading from 5260: heap size 452 MB, throughput 0.983417
Reading from 5261: heap size 447 MB, throughput 0.970772
Equal recommendation: 469 MB each
Reading from 5260: heap size 452 MB, throughput 0.987696
Reading from 5260: heap size 456 MB, throughput 0.886452
Reading from 5260: heap size 456 MB, throughput 0.860835
Reading from 5261: heap size 450 MB, throughput 0.982703
Reading from 5260: heap size 461 MB, throughput 0.956875
Reading from 5261: heap size 451 MB, throughput 0.981839
Reading from 5261: heap size 455 MB, throughput 0.906338
Reading from 5261: heap size 457 MB, throughput 0.843621
Reading from 5260: heap size 463 MB, throughput 0.992689
Reading from 5261: heap size 463 MB, throughput 0.981483
Reading from 5261: heap size 464 MB, throughput 0.988135
Reading from 5260: heap size 467 MB, throughput 0.989377
Reading from 5261: heap size 468 MB, throughput 0.989649
Reading from 5260: heap size 468 MB, throughput 0.988477
Equal recommendation: 469 MB each
Reading from 5261: heap size 469 MB, throughput 0.989521
Reading from 5260: heap size 468 MB, throughput 0.987447
Reading from 5260: heap size 470 MB, throughput 0.976638
Reading from 5261: heap size 453 MB, throughput 0.977231
Reading from 5261: heap size 463 MB, throughput 0.984838
Reading from 5260: heap size 456 MB, throughput 0.985653
Reading from 5261: heap size 462 MB, throughput 0.987259
Reading from 5260: heap size 464 MB, throughput 0.983945
Reading from 5261: heap size 464 MB, throughput 0.984276
Reading from 5260: heap size 466 MB, throughput 0.983243
Equal recommendation: 469 MB each
Reading from 5260: heap size 466 MB, throughput 0.986235
Reading from 5261: heap size 466 MB, throughput 0.983156
Reading from 5260: heap size 470 MB, throughput 0.888568
Reading from 5260: heap size 460 MB, throughput 0.872068
Reading from 5260: heap size 467 MB, throughput 0.987106
Reading from 5261: heap size 466 MB, throughput 0.985692
Reading from 5261: heap size 469 MB, throughput 0.900447
Reading from 5261: heap size 460 MB, throughput 0.875127
Reading from 5261: heap size 467 MB, throughput 0.977637
Reading from 5260: heap size 469 MB, throughput 0.988222
Reading from 5261: heap size 469 MB, throughput 0.991607
Reading from 5260: heap size 472 MB, throughput 0.988638
Reading from 5261: heap size 473 MB, throughput 0.987398
Reading from 5260: heap size 444 MB, throughput 0.989564
Equal recommendation: 469 MB each
Reading from 5261: heap size 444 MB, throughput 0.991004
Reading from 5260: heap size 470 MB, throughput 0.985134
Reading from 5261: heap size 471 MB, throughput 0.989148
Reading from 5260: heap size 446 MB, throughput 0.984461
Reading from 5261: heap size 447 MB, throughput 0.983834
Reading from 5260: heap size 464 MB, throughput 0.985453
Reading from 5261: heap size 465 MB, throughput 0.984126
Reading from 5260: heap size 463 MB, throughput 0.985477
Reading from 5261: heap size 464 MB, throughput 0.982506
Reading from 5260: heap size 465 MB, throughput 0.988093
Equal recommendation: 469 MB each
Reading from 5260: heap size 466 MB, throughput 0.964675
Reading from 5260: heap size 464 MB, throughput 0.88477
Reading from 5260: heap size 467 MB, throughput 0.888006
Reading from 5261: heap size 466 MB, throughput 0.991259
Reading from 5261: heap size 467 MB, throughput 0.971354
Reading from 5261: heap size 467 MB, throughput 0.902657
Reading from 5261: heap size 469 MB, throughput 0.857754
Reading from 5260: heap size 473 MB, throughput 0.990578
Reading from 5261: heap size 461 MB, throughput 0.988336
Reading from 5260: heap size 441 MB, throughput 0.988592
Reading from 5261: heap size 469 MB, throughput 0.98967
Reading from 5260: heap size 472 MB, throughput 0.989212
Reading from 5261: heap size 473 MB, throughput 0.990358
Reading from 5260: heap size 443 MB, throughput 0.992418
Equal recommendation: 469 MB each
Reading from 5261: heap size 444 MB, throughput 0.98902
Reading from 5260: heap size 469 MB, throughput 0.988979
Reading from 5261: heap size 472 MB, throughput 0.988935
Reading from 5260: heap size 467 MB, throughput 0.987422
Reading from 5261: heap size 447 MB, throughput 0.984319
Reading from 5260: heap size 465 MB, throughput 0.9875
Reading from 5261: heap size 466 MB, throughput 0.984384
Reading from 5260: heap size 467 MB, throughput 0.978797
Reading from 5261: heap size 465 MB, throughput 0.985605
Reading from 5260: heap size 469 MB, throughput 0.98908
Reading from 5260: heap size 447 MB, throughput 0.910146
Equal recommendation: 469 MB each
Reading from 5260: heap size 464 MB, throughput 0.868436
Reading from 5260: heap size 466 MB, throughput 0.950045
Reading from 5261: heap size 466 MB, throughput 0.988376
Reading from 5261: heap size 467 MB, throughput 0.964115
Reading from 5261: heap size 467 MB, throughput 0.863745
Reading from 5260: heap size 473 MB, throughput 0.989066
Reading from 5261: heap size 469 MB, throughput 0.886408
Reading from 5261: heap size 458 MB, throughput 0.988366
Reading from 5260: heap size 441 MB, throughput 0.991717
Reading from 5261: heap size 468 MB, throughput 0.992516
Reading from 5260: heap size 470 MB, throughput 0.988398
Reading from 5261: heap size 471 MB, throughput 0.987999
Reading from 5260: heap size 443 MB, throughput 0.9871
Equal recommendation: 469 MB each
Reading from 5261: heap size 445 MB, throughput 0.987903
Reading from 5260: heap size 466 MB, throughput 0.983138
Reading from 5261: heap size 469 MB, throughput 0.985931
Reading from 5260: heap size 465 MB, throughput 0.987134
Reading from 5261: heap size 447 MB, throughput 0.986131
Reading from 5260: heap size 466 MB, throughput 0.985083
Reading from 5261: heap size 463 MB, throughput 0.983928
Reading from 5260: heap size 466 MB, throughput 0.982805
Reading from 5260: heap size 466 MB, throughput 0.978994
Reading from 5260: heap size 469 MB, throughput 0.915661
Reading from 5260: heap size 461 MB, throughput 0.909581
Reading from 5261: heap size 463 MB, throughput 0.98389
Equal recommendation: 469 MB each
Reading from 5260: heap size 466 MB, throughput 0.986452
Reading from 5261: heap size 466 MB, throughput 0.992725
Reading from 5261: heap size 466 MB, throughput 0.921269
Reading from 5261: heap size 466 MB, throughput 0.845905
Reading from 5261: heap size 469 MB, throughput 0.888933
Reading from 5260: heap size 472 MB, throughput 0.992808
Reading from 5260: heap size 440 MB, throughput 0.990663
Reading from 5261: heap size 478 MB, throughput 0.992103
Reading from 5260: heap size 470 MB, throughput 0.989046
Reading from 5261: heap size 445 MB, throughput 0.992761
Reading from 5260: heap size 443 MB, throughput 0.987668
Reading from 5261: heap size 475 MB, throughput 0.989406
Equal recommendation: 469 MB each
Reading from 5260: heap size 465 MB, throughput 0.988167
Reading from 5261: heap size 448 MB, throughput 0.987882
Reading from 5260: heap size 464 MB, throughput 0.986865
Reading from 5261: heap size 471 MB, throughput 0.983152
Reading from 5260: heap size 465 MB, throughput 0.985129
Reading from 5261: heap size 451 MB, throughput 0.986265
Reading from 5260: heap size 465 MB, throughput 0.99083
Reading from 5261: heap size 465 MB, throughput 0.98532
Reading from 5260: heap size 466 MB, throughput 0.9724
Reading from 5260: heap size 467 MB, throughput 0.827829
Reading from 5260: heap size 470 MB, throughput 0.883072
Equal recommendation: 469 MB each
Reading from 5261: heap size 465 MB, throughput 0.987775
Reading from 5260: heap size 459 MB, throughput 0.989114
Reading from 5261: heap size 467 MB, throughput 0.989226
Reading from 5261: heap size 468 MB, throughput 0.897794
Reading from 5261: heap size 469 MB, throughput 0.86341
Reading from 5260: heap size 469 MB, throughput 0.989379
Reading from 5261: heap size 459 MB, throughput 0.984445
Reading from 5260: heap size 469 MB, throughput 0.990579
Reading from 5261: heap size 470 MB, throughput 0.993166
Reading from 5260: heap size 455 MB, throughput 0.991319
Reading from 5261: heap size 437 MB, throughput 0.99053
Reading from 5260: heap size 465 MB, throughput 0.986187
Reading from 5261: heap size 467 MB, throughput 0.988609
Equal recommendation: 469 MB each
Reading from 5260: heap size 463 MB, throughput 0.985575
Reading from 5261: heap size 466 MB, throughput 0.987422
Reading from 5260: heap size 465 MB, throughput 0.985733
Reading from 5261: heap size 463 MB, throughput 0.987292
Reading from 5260: heap size 466 MB, throughput 0.985427
Reading from 5261: heap size 466 MB, throughput 0.986687
Reading from 5260: heap size 467 MB, throughput 0.98902
Reading from 5260: heap size 470 MB, throughput 0.910761
Reading from 5260: heap size 461 MB, throughput 0.885969
Reading from 5261: heap size 468 MB, throughput 0.986741
Reading from 5260: heap size 467 MB, throughput 0.967366
Equal recommendation: 469 MB each
Reading from 5260: heap size 469 MB, throughput 0.99177
Reading from 5261: heap size 468 MB, throughput 0.993158
Reading from 5261: heap size 470 MB, throughput 0.977004
Reading from 5261: heap size 463 MB, throughput 0.903692
Reading from 5261: heap size 468 MB, throughput 0.868147
Reading from 5260: heap size 473 MB, throughput 0.993058
Reading from 5261: heap size 470 MB, throughput 0.981161
Reading from 5260: heap size 444 MB, throughput 0.990542
Reading from 5261: heap size 458 MB, throughput 0.992913
Reading from 5260: heap size 471 MB, throughput 0.987971
Reading from 5261: heap size 468 MB, throughput 0.988135
Equal recommendation: 469 MB each
Reading from 5260: heap size 447 MB, throughput 0.985721
Reading from 5261: heap size 469 MB, throughput 0.986727
Reading from 5260: heap size 466 MB, throughput 0.988699
Reading from 5261: heap size 471 MB, throughput 0.985634
Reading from 5260: heap size 465 MB, throughput 0.984766
Reading from 5261: heap size 454 MB, throughput 0.984628
Reading from 5260: heap size 466 MB, throughput 0.976684
Reading from 5261: heap size 464 MB, throughput 0.985573
Reading from 5260: heap size 467 MB, throughput 0.96687
Reading from 5260: heap size 469 MB, throughput 0.858667
Reading from 5260: heap size 474 MB, throughput 0.853189
Reading from 5260: heap size 475 MB, throughput 0.980089
Reading from 5261: heap size 466 MB, throughput 0.98335
Equal recommendation: 469 MB each
Reading from 5260: heap size 442 MB, throughput 0.986611
Reading from 5261: heap size 466 MB, throughput 0.985205
Reading from 5261: heap size 469 MB, throughput 0.95604
Reading from 5261: heap size 460 MB, throughput 0.883002
Reading from 5261: heap size 467 MB, throughput 0.915824
Reading from 5260: heap size 475 MB, throughput 0.985382
Reading from 5261: heap size 468 MB, throughput 0.988349
Reading from 5260: heap size 445 MB, throughput 0.986314
Reading from 5261: heap size 474 MB, throughput 0.989545
Reading from 5260: heap size 470 MB, throughput 0.872171
Reading from 5261: heap size 443 MB, throughput 0.990994
Equal recommendation: 469 MB each
Reading from 5260: heap size 450 MB, throughput 0.994677
Reading from 5261: heap size 471 MB, throughput 0.987249
Reading from 5260: heap size 463 MB, throughput 0.990662
Reading from 5261: heap size 446 MB, throughput 0.982775
Reading from 5260: heap size 467 MB, throughput 0.991225
Reading from 5261: heap size 466 MB, throughput 0.984442
Reading from 5260: heap size 472 MB, throughput 0.988749
Reading from 5261: heap size 465 MB, throughput 0.983333
Reading from 5260: heap size 426 MB, throughput 0.986082
Reading from 5260: heap size 468 MB, throughput 0.890184
Reading from 5260: heap size 467 MB, throughput 0.86917
Reading from 5260: heap size 473 MB, throughput 0.968337
Reading from 5261: heap size 467 MB, throughput 0.9841
Equal recommendation: 469 MB each
Reading from 5260: heap size 433 MB, throughput 0.990069
Reading from 5261: heap size 467 MB, throughput 0.991313
Reading from 5261: heap size 468 MB, throughput 0.917217
Reading from 5261: heap size 470 MB, throughput 0.870789
Reading from 5261: heap size 464 MB, throughput 0.976965
Reading from 5260: heap size 472 MB, throughput 0.987053
Reading from 5261: heap size 470 MB, throughput 0.993037
Reading from 5260: heap size 436 MB, throughput 0.987598
Reading from 5261: heap size 457 MB, throughput 0.98818
Reading from 5260: heap size 467 MB, throughput 0.986327
Reading from 5261: heap size 467 MB, throughput 0.991443
Reading from 5260: heap size 465 MB, throughput 0.986867
Equal recommendation: 469 MB each
Reading from 5261: heap size 466 MB, throughput 0.985883
Reading from 5260: heap size 466 MB, throughput 0.983864
Reading from 5260: heap size 467 MB, throughput 0.981393
Reading from 5261: heap size 468 MB, throughput 0.988077
Reading from 5260: heap size 470 MB, throughput 0.98191
Reading from 5261: heap size 467 MB, throughput 0.98616
Reading from 5260: heap size 455 MB, throughput 0.989409
Reading from 5260: heap size 470 MB, throughput 0.909744
Reading from 5261: heap size 469 MB, throughput 0.983999
Reading from 5260: heap size 456 MB, throughput 0.835805
Reading from 5260: heap size 467 MB, throughput 0.899123
Equal recommendation: 469 MB each
Reading from 5260: heap size 468 MB, throughput 0.990032
Reading from 5261: heap size 471 MB, throughput 0.981989
Reading from 5261: heap size 468 MB, throughput 0.971632
Reading from 5261: heap size 468 MB, throughput 0.855543
Reading from 5261: heap size 473 MB, throughput 0.867985
Reading from 5260: heap size 473 MB, throughput 0.993092
Reading from 5261: heap size 472 MB, throughput 0.978394
Reading from 5260: heap size 440 MB, throughput 0.98858
Reading from 5261: heap size 443 MB, throughput 0.986926
Reading from 5260: heap size 472 MB, throughput 0.991988
Reading from 5261: heap size 475 MB, throughput 0.988238
Reading from 5260: heap size 442 MB, throughput 0.98889
Equal recommendation: 469 MB each
Reading from 5261: heap size 446 MB, throughput 0.98615
Reading from 5260: heap size 467 MB, throughput 0.983852
Reading from 5261: heap size 469 MB, throughput 0.981157
Reading from 5260: heap size 465 MB, throughput 0.984701
Reading from 5261: heap size 448 MB, throughput 0.982023
Reading from 5260: heap size 467 MB, throughput 0.98189
Reading from 5261: heap size 464 MB, throughput 0.980747
Reading from 5261: heap size 464 MB, throughput 0.981269
Reading from 5260: heap size 467 MB, throughput 0.993913
Reading from 5260: heap size 470 MB, throughput 0.940931
Reading from 5260: heap size 460 MB, throughput 0.883049
Reading from 5260: heap size 468 MB, throughput 0.916224
Client 5260 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 5261: heap size 465 MB, throughput 0.911653
Reading from 5261: heap size 484 MB, throughput 0.990346
Reading from 5261: heap size 485 MB, throughput 0.922226
Reading from 5261: heap size 487 MB, throughput 0.90986
Client 5261 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
