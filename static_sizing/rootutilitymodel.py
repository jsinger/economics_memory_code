# A utility model based on U(h) = ah^b, where b<1; i.e. a 'nth root' function
# Combined utility for multiple VMs is found by multiplying the individual utilities

import math
import numpy as np
import operator
import staticutility as su
import utilitymodel

import scipy.optimize

def name():
    return "root"

class RootUtilityModel(utilitymodel.UtilityModel):
    def modelName(self):
        return name()

    class Params:
        def __init__(self, c, e):
            self.c = c
            self.e = e

        def save(self, filename):
            with open(filename, "w") as f:
                f.write(str(self.c) + "\n")
                f.write(str(self.e) + "\n")

        def calculate(self, heap):
            """Heap can be a scalar or a sequence"""
            return self.c * np.power(np.maximum(heap, 0), self.e)

        def __str__(self):
            return "y = %gx^%g" % (self.c, self.e)

        def __repr__(self):
            return str(self)

        def pretty(self):
            return "T = %.3gh^%.3g" % (self.c, self.e)


    def testingParams(self):
        return self.Params(0.5, 0.5)

    def curveFit(self, xdata, ydata):
        (a, b) = su.fitStraightLine(map(math.log, xdata), map(math.log, ydata))
        return self.Params(math.exp(a), b)

    def calculateBest(self, totalMem, params, minheaps):
        if len(params) == 2:
            return self.calculateBestAnalytic(totalMem, params[0], params[1], minheaps[0], minheaps[1])
        else:
            return self.calculateBestNumeric(totalMem, params, minheaps)

    def calculateBestAnalytic(self, totalMem, params1, params2, minheap1, minheap2):
        a = params1.c
        b = params1.e
        c = params2.c
        d = params2.e

        h1max = (b*totalMem)/(b+d)
        h1 = 0

        if h1max < minheap1:
            h1 = minheap1
        elif h1max > totalMem - minheap2:
            h1 = totalMem - minheap2
        else:
            h1 = h1max

        h2 = totalMem - h1

        return ([h1, h2], self.combinedUtility([params1, params2], [h1, h2]))

    def calculateBestOld(self, totalMem, params1, params2, minheap1, minheap2):

        def U(h1):
            return -self.combinedUtility([params1, params2], [h1, totalMem - h1])

        res = scipy.optimize.minimize_scalar(U, method="Bounded", bounds=(minheap1, totalMem - minheap2))

        h1 = res["x"]
        h2 = totalMem - h1

        return ([h1, h2], self.combinedUtility([params1, params2], [h1, h2]))


    def combineRawUtilities(self, *u):
        return reduce(operator.mul, u, 1)

    def propagateRawErrors(self, itersPerSecs, U, stddevItersPerSecs):
        return U * math.sqrt(sum(map(lambda (x, dx): (dx / x) ** 2, zip(itersPerSecs, stddevItersPerSecs))))
