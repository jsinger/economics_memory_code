economemd
    total memory: 6175 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub13_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 30404: heap size 9 MB, throughput 0.986335
Clients: 1
Client 30404 has a minimum heap size of 12 MB
Reading from 30403: heap size 9 MB, throughput 0.979116
Clients: 2
Client 30403 has a minimum heap size of 1223 MB
Reading from 30403: heap size 9 MB, throughput 0.979629
Reading from 30404: heap size 9 MB, throughput 0.989599
Reading from 30404: heap size 9 MB, throughput 0.985773
Reading from 30403: heap size 11 MB, throughput 0.978537
Reading from 30404: heap size 9 MB, throughput 0.953297
Reading from 30403: heap size 11 MB, throughput 0.951271
Reading from 30404: heap size 11 MB, throughput 0.957205
Reading from 30404: heap size 11 MB, throughput 0.967819
Reading from 30403: heap size 15 MB, throughput 0.9279
Reading from 30404: heap size 16 MB, throughput 0.973218
Reading from 30403: heap size 18 MB, throughput 0.895557
Reading from 30403: heap size 24 MB, throughput 0.817267
Reading from 30404: heap size 16 MB, throughput 0.944843
Reading from 30403: heap size 28 MB, throughput 0.746074
Reading from 30403: heap size 42 MB, throughput 0.774552
Reading from 30403: heap size 43 MB, throughput 0.49056
Reading from 30404: heap size 24 MB, throughput 0.948143
Reading from 30403: heap size 46 MB, throughput 0.511536
Reading from 30403: heap size 61 MB, throughput 0.237648
Reading from 30404: heap size 29 MB, throughput 0.946237
Reading from 30403: heap size 68 MB, throughput 0.821147
Reading from 30403: heap size 70 MB, throughput 0.815912
Reading from 30404: heap size 36 MB, throughput 0.93166
Reading from 30403: heap size 77 MB, throughput 0.447946
Reading from 30403: heap size 97 MB, throughput 0.652306
Reading from 30404: heap size 39 MB, throughput 0.908234
Reading from 30403: heap size 109 MB, throughput 0.758727
Reading from 30404: heap size 44 MB, throughput 0.931421
Reading from 30403: heap size 109 MB, throughput 0.732451
Reading from 30404: heap size 44 MB, throughput 0.939972
Reading from 30404: heap size 52 MB, throughput 0.963825
Reading from 30403: heap size 113 MB, throughput 0.410321
Reading from 30403: heap size 142 MB, throughput 0.611923
Reading from 30404: heap size 53 MB, throughput 0.979816
Reading from 30403: heap size 150 MB, throughput 0.673927
Reading from 30403: heap size 154 MB, throughput 0.649221
Reading from 30404: heap size 62 MB, throughput 0.981975
Reading from 30403: heap size 159 MB, throughput 0.61604
Reading from 30404: heap size 62 MB, throughput 0.961767
Reading from 30404: heap size 74 MB, throughput 0.982135
Reading from 30403: heap size 167 MB, throughput 0.369339
Reading from 30404: heap size 74 MB, throughput 0.989729
Reading from 30403: heap size 209 MB, throughput 0.673208
Reading from 30403: heap size 215 MB, throughput 0.656351
Reading from 30403: heap size 219 MB, throughput 0.653428
Reading from 30403: heap size 225 MB, throughput 0.652285
Reading from 30404: heap size 81 MB, throughput 0.994541
Reading from 30403: heap size 233 MB, throughput 0.4001
Reading from 30403: heap size 276 MB, throughput 0.594767
Reading from 30404: heap size 82 MB, throughput 0.994809
Reading from 30403: heap size 284 MB, throughput 0.644282
Reading from 30403: heap size 287 MB, throughput 0.636015
Reading from 30403: heap size 292 MB, throughput 0.666039
Reading from 30403: heap size 299 MB, throughput 0.629281
Reading from 30403: heap size 303 MB, throughput 0.606128
Reading from 30404: heap size 89 MB, throughput 0.996206
Reading from 30403: heap size 314 MB, throughput 0.569639
Reading from 30404: heap size 90 MB, throughput 0.995807
Reading from 30403: heap size 321 MB, throughput 0.37361
Reading from 30403: heap size 375 MB, throughput 0.535607
Reading from 30403: heap size 377 MB, throughput 0.612401
Reading from 30404: heap size 96 MB, throughput 0.99543
Reading from 30403: heap size 380 MB, throughput 0.605845
Reading from 30403: heap size 383 MB, throughput 0.5618
Reading from 30404: heap size 96 MB, throughput 0.995197
Reading from 30403: heap size 386 MB, throughput 0.321734
Reading from 30403: heap size 441 MB, throughput 0.567085
Reading from 30403: heap size 443 MB, throughput 0.571872
Reading from 30404: heap size 101 MB, throughput 0.995983
Reading from 30403: heap size 445 MB, throughput 0.60814
Reading from 30403: heap size 446 MB, throughput 0.552916
Reading from 30403: heap size 449 MB, throughput 0.520864
Reading from 30403: heap size 457 MB, throughput 0.521088
Reading from 30404: heap size 101 MB, throughput 0.996474
Numeric result:
Recommendation: 2 clients, utility 1.13519:
    h1: 4952 MB (U(h) = 0.900513*h^0.0191413)
    h2: 1223 MB (U(h) = 1.06357*h^0.001)
Recommendation: 2 clients, utility 1.13519:
    h1: 4952 MB (U(h) = 0.900513*h^0.0191413)
    h2: 1223 MB (U(h) = 1.06357*h^0.001)
Reading from 30404: heap size 105 MB, throughput 0.995802
Reading from 30403: heap size 464 MB, throughput 0.322975
Reading from 30403: heap size 525 MB, throughput 0.492977
Reading from 30404: heap size 105 MB, throughput 0.995157
Reading from 30403: heap size 525 MB, throughput 0.528151
Reading from 30403: heap size 528 MB, throughput 0.532155
Reading from 30403: heap size 532 MB, throughput 0.527941
Reading from 30404: heap size 110 MB, throughput 0.995313
Reading from 30403: heap size 535 MB, throughput 0.3096
Reading from 30404: heap size 110 MB, throughput 0.994846
Reading from 30403: heap size 597 MB, throughput 0.475577
Reading from 30403: heap size 601 MB, throughput 0.523082
Reading from 30403: heap size 604 MB, throughput 0.564927
Reading from 30404: heap size 115 MB, throughput 0.995785
Reading from 30404: heap size 115 MB, throughput 0.992931
Reading from 30403: heap size 605 MB, throughput 0.287811
Reading from 30403: heap size 674 MB, throughput 0.59213
Reading from 30403: heap size 674 MB, throughput 0.638941
Reading from 30404: heap size 118 MB, throughput 0.9917
Reading from 30403: heap size 675 MB, throughput 0.754861
Reading from 30404: heap size 118 MB, throughput 0.994013
Reading from 30403: heap size 679 MB, throughput 0.854667
Reading from 30404: heap size 121 MB, throughput 0.995685
Reading from 30403: heap size 679 MB, throughput 0.866345
Reading from 30403: heap size 694 MB, throughput 0.792417
Reading from 30404: heap size 122 MB, throughput 0.996588
Reading from 30404: heap size 124 MB, throughput 0.997295
Reading from 30404: heap size 125 MB, throughput 0.997443
Reading from 30403: heap size 706 MB, throughput 0.560532
Reading from 30403: heap size 783 MB, throughput 0.606971
Reading from 30403: heap size 789 MB, throughput 0.546211
Reading from 30403: heap size 793 MB, throughput 0.504328
Reading from 30404: heap size 128 MB, throughput 0.997908
Numeric result:
Recommendation: 2 clients, utility 1.07784:
    h1: 4952 MB (U(h) = 0.890256*h^0.0226699)
    h2: 1223 MB (U(h) = 0.99127*h^0.001)
Recommendation: 2 clients, utility 1.07784:
    h1: 4952 MB (U(h) = 0.890256*h^0.0226699)
    h2: 1223 MB (U(h) = 0.99127*h^0.001)
Reading from 30403: heap size 777 MB, throughput 0.734555
Reading from 30404: heap size 128 MB, throughput 0.996639
Reading from 30403: heap size 785 MB, throughput 0.318876
Reading from 30404: heap size 130 MB, throughput 0.996852
Reading from 30403: heap size 847 MB, throughput 0.606031
Reading from 30403: heap size 853 MB, throughput 0.623588
Reading from 30403: heap size 859 MB, throughput 0.748711
Reading from 30403: heap size 860 MB, throughput 0.792458
Reading from 30404: heap size 130 MB, throughput 0.997386
Reading from 30403: heap size 867 MB, throughput 0.464425
Reading from 30404: heap size 132 MB, throughput 0.99738
Reading from 30403: heap size 945 MB, throughput 0.707192
Reading from 30404: heap size 133 MB, throughput 0.987485
Reading from 30403: heap size 950 MB, throughput 0.784013
Reading from 30404: heap size 135 MB, throughput 0.979338
Reading from 30404: heap size 135 MB, throughput 0.972323
Reading from 30404: heap size 141 MB, throughput 0.987078
Reading from 30403: heap size 953 MB, throughput 0.389714
Reading from 30403: heap size 1055 MB, throughput 0.711859
Reading from 30403: heap size 1055 MB, throughput 0.753887
Reading from 30403: heap size 1064 MB, throughput 0.788033
Reading from 30404: heap size 141 MB, throughput 0.992612
Reading from 30403: heap size 1064 MB, throughput 0.807218
Reading from 30403: heap size 1061 MB, throughput 0.819114
Reading from 30403: heap size 1065 MB, throughput 0.817158
Reading from 30403: heap size 1056 MB, throughput 0.829504
Reading from 30404: heap size 148 MB, throughput 0.994489
Reading from 30403: heap size 1061 MB, throughput 0.82854
Reading from 30404: heap size 149 MB, throughput 0.995416
Reading from 30403: heap size 1048 MB, throughput 0.945655
Reading from 30404: heap size 153 MB, throughput 0.996263
Reading from 30403: heap size 917 MB, throughput 0.940107
Reading from 30403: heap size 1046 MB, throughput 0.906238
Numeric result:
Recommendation: 2 clients, utility 0.800517:
    h1: 4952 MB (U(h) = 0.893497*h^0.0216049)
    h2: 1223 MB (U(h) = 0.740227*h^0.001)
Recommendation: 2 clients, utility 0.800517:
    h1: 4952 MB (U(h) = 0.893497*h^0.0216049)
    h2: 1223 MB (U(h) = 0.740227*h^0.001)
Reading from 30403: heap size 1048 MB, throughput 0.874452
Reading from 30404: heap size 154 MB, throughput 0.996561
Reading from 30403: heap size 1056 MB, throughput 0.850155
Reading from 30403: heap size 1056 MB, throughput 0.835022
Reading from 30403: heap size 1041 MB, throughput 0.847494
Reading from 30403: heap size 945 MB, throughput 0.855387
Reading from 30404: heap size 158 MB, throughput 0.996824
Reading from 30403: heap size 1033 MB, throughput 0.875503
Reading from 30403: heap size 1042 MB, throughput 0.906326
Reading from 30404: heap size 159 MB, throughput 0.996854
Reading from 30403: heap size 1037 MB, throughput 0.915526
Reading from 30403: heap size 1042 MB, throughput 0.867056
Reading from 30403: heap size 1031 MB, throughput 0.792778
Reading from 30403: heap size 1045 MB, throughput 0.717577
Reading from 30403: heap size 1058 MB, throughput 0.698287
Reading from 30404: heap size 162 MB, throughput 0.997274
Reading from 30404: heap size 163 MB, throughput 0.996976
Reading from 30403: heap size 1068 MB, throughput 0.724689
Reading from 30403: heap size 1185 MB, throughput 0.631558
Reading from 30403: heap size 1191 MB, throughput 0.701281
Reading from 30404: heap size 166 MB, throughput 0.99712
Reading from 30403: heap size 1202 MB, throughput 0.747588
Reading from 30403: heap size 1204 MB, throughput 0.761674
Reading from 30403: heap size 1206 MB, throughput 0.764051
Reading from 30403: heap size 1211 MB, throughput 0.785956
Reading from 30404: heap size 166 MB, throughput 0.993697
Reading from 30404: heap size 170 MB, throughput 0.982715
Reading from 30404: heap size 170 MB, throughput 0.979916
Reading from 30403: heap size 1207 MB, throughput 0.969719
Reading from 30404: heap size 174 MB, throughput 0.983268
Numeric result:
Recommendation: 2 clients, utility 0.671932:
    h1: 4952 MB (U(h) = 0.896899*h^0.0205662)
    h2: 1223 MB (U(h) = 0.618085*h^0.00244411)
Recommendation: 2 clients, utility 0.671932:
    h1: 4952 MB (U(h) = 0.896899*h^0.0205662)
    h2: 1223 MB (U(h) = 0.618085*h^0.00244411)
Reading from 30404: heap size 174 MB, throughput 0.990405
Reading from 30404: heap size 177 MB, throughput 0.993926
Reading from 30404: heap size 177 MB, throughput 0.993462
Reading from 30403: heap size 1215 MB, throughput 0.966244
Reading from 30404: heap size 181 MB, throughput 0.994649
Reading from 30404: heap size 181 MB, throughput 0.995299
Reading from 30404: heap size 186 MB, throughput 0.996435
Reading from 30403: heap size 1203 MB, throughput 0.975447
Reading from 30404: heap size 186 MB, throughput 0.996773
Reading from 30404: heap size 189 MB, throughput 0.99725
Reading from 30404: heap size 189 MB, throughput 0.996891
Reading from 30403: heap size 1213 MB, throughput 0.976783
Numeric result:
Recommendation: 2 clients, utility 0.69144:
    h1: 3728.92 MB (U(h) = 0.897588*h^0.020361)
    h2: 2446.08 MB (U(h) = 0.587051*h^0.013363)
Recommendation: 2 clients, utility 0.69144:
    h1: 3728.18 MB (U(h) = 0.897588*h^0.020361)
    h2: 2446.82 MB (U(h) = 0.587051*h^0.013363)
Reading from 30404: heap size 193 MB, throughput 0.997013
Reading from 30404: heap size 193 MB, throughput 0.996487
Reading from 30404: heap size 197 MB, throughput 0.996835
Reading from 30403: heap size 1204 MB, throughput 0.977023
Reading from 30404: heap size 197 MB, throughput 0.997042
Reading from 30404: heap size 201 MB, throughput 0.996874
Reading from 30403: heap size 1210 MB, throughput 0.979786
Reading from 30404: heap size 201 MB, throughput 0.997253
Reading from 30404: heap size 205 MB, throughput 0.99729
Reading from 30404: heap size 194 MB, throughput 0.993953
Numeric result:
Recommendation: 2 clients, utility 0.700179:
    h1: 3258.2 MB (U(h) = 0.898939*h^0.019966)
    h2: 2916.8 MB (U(h) = 0.574748*h^0.0178538)
Recommendation: 2 clients, utility 0.700179:
    h1: 3259.93 MB (U(h) = 0.898939*h^0.019966)
    h2: 2915.07 MB (U(h) = 0.574748*h^0.0178538)
Reading from 30403: heap size 1202 MB, throughput 0.971079
Reading from 30404: heap size 185 MB, throughput 0.994919
Reading from 30404: heap size 195 MB, throughput 0.986429
Reading from 30404: heap size 201 MB, throughput 0.991983
Reading from 30404: heap size 203 MB, throughput 0.995218
Reading from 30403: heap size 1209 MB, throughput 0.975624
Reading from 30404: heap size 209 MB, throughput 0.997074
Reading from 30404: heap size 209 MB, throughput 0.997551
Reading from 30404: heap size 213 MB, throughput 0.998104
Reading from 30403: heap size 1199 MB, throughput 0.97795
Reading from 30404: heap size 214 MB, throughput 0.998097
Numeric result:
Recommendation: 2 clients, utility 0.737249:
    h1: 1995.48 MB (U(h) = 0.901007*h^0.0193635)
    h2: 4179.52 MB (U(h) = 0.503782*h^0.0405241)
Recommendation: 2 clients, utility 0.737249:
    h1: 1996.57 MB (U(h) = 0.901007*h^0.0193635)
    h2: 4178.43 MB (U(h) = 0.503782*h^0.0405241)
Reading from 30404: heap size 218 MB, throughput 0.997779
Reading from 30403: heap size 1204 MB, throughput 0.977294
Reading from 30404: heap size 218 MB, throughput 0.997998
Reading from 30404: heap size 222 MB, throughput 0.998137
Reading from 30404: heap size 222 MB, throughput 0.998251
Reading from 30403: heap size 1207 MB, throughput 0.978457
Reading from 30404: heap size 225 MB, throughput 0.998481
Reading from 30404: heap size 226 MB, throughput 0.997901
Reading from 30404: heap size 230 MB, throughput 0.987843
Reading from 30403: heap size 1208 MB, throughput 0.97051
Reading from 30404: heap size 230 MB, throughput 0.976122
Numeric result:
Recommendation: 2 clients, utility 0.772535:
    h1: 1437.49 MB (U(h) = 0.903126*h^0.0187706)
    h2: 4737.51 MB (U(h) = 0.442253*h^0.0618218)
Recommendation: 2 clients, utility 0.772535:
    h1: 1438.2 MB (U(h) = 0.903126*h^0.0187706)
    h2: 4736.8 MB (U(h) = 0.442253*h^0.0618218)
Reading from 30404: heap size 236 MB, throughput 0.988458
Reading from 30404: heap size 237 MB, throughput 0.991629
Reading from 30403: heap size 1211 MB, throughput 0.973866
Reading from 30404: heap size 243 MB, throughput 0.995522
Reading from 30404: heap size 244 MB, throughput 0.996181
Reading from 30403: heap size 1213 MB, throughput 0.976804
Reading from 30404: heap size 251 MB, throughput 0.996951
Reading from 30404: heap size 251 MB, throughput 0.997481
Reading from 30404: heap size 256 MB, throughput 0.997859
Numeric result:
Recommendation: 2 clients, utility 0.773362:
    h1: 1374.97 MB (U(h) = 0.905938*h^0.0179975)
    h2: 4800.03 MB (U(h) = 0.440049*h^0.0628327)
Recommendation: 2 clients, utility 0.773362:
    h1: 1374.91 MB (U(h) = 0.905938*h^0.0179975)
    h2: 4800.09 MB (U(h) = 0.440049*h^0.0628327)
Reading from 30403: heap size 1217 MB, throughput 0.97798
Reading from 30404: heap size 257 MB, throughput 0.997765
Reading from 30404: heap size 263 MB, throughput 0.99797
Reading from 30403: heap size 1221 MB, throughput 0.977801
Reading from 30404: heap size 263 MB, throughput 0.997769
Reading from 30404: heap size 269 MB, throughput 0.988898
Reading from 30404: heap size 269 MB, throughput 0.991079
Reading from 30403: heap size 1226 MB, throughput 0.977392
Reading from 30404: heap size 277 MB, throughput 0.992768
Numeric result:
Recommendation: 2 clients, utility 0.902388:
    h1: 727.751 MB (U(h) = 0.908002*h^0.0174459)
    h2: 5447.25 MB (U(h) = 0.287999*h^0.130611)
Recommendation: 2 clients, utility 0.902388:
    h1: 727.616 MB (U(h) = 0.908002*h^0.0174459)
    h2: 5447.38 MB (U(h) = 0.287999*h^0.130611)
Reading from 30404: heap size 277 MB, throughput 0.993815
Reading from 30403: heap size 1229 MB, throughput 0.97818
Reading from 30404: heap size 284 MB, throughput 0.994992
Reading from 30404: heap size 286 MB, throughput 0.996586
Reading from 30403: heap size 1234 MB, throughput 0.978162
Reading from 30404: heap size 293 MB, throughput 0.997342
Reading from 30404: heap size 294 MB, throughput 0.997692
Reading from 30403: heap size 1236 MB, throughput 0.977992
Reading from 30404: heap size 299 MB, throughput 0.997889
Numeric result:
Recommendation: 2 clients, utility 1.01626:
    h1: 523.776 MB (U(h) = 0.909913*h^0.0169442)
    h2: 5651.22 MB (U(h) = 0.207015*h^0.18281)
Recommendation: 2 clients, utility 1.01626:
    h1: 523.796 MB (U(h) = 0.909913*h^0.0169442)
    h2: 5651.2 MB (U(h) = 0.207015*h^0.18281)
Reading from 30404: heap size 300 MB, throughput 0.998071
Reading from 30404: heap size 306 MB, throughput 0.990647
Reading from 30404: heap size 307 MB, throughput 0.991546
Reading from 30403: heap size 1240 MB, throughput 0.953706
Reading from 30404: heap size 315 MB, throughput 0.994773
Reading from 30404: heap size 315 MB, throughput 0.996344
Reading from 30403: heap size 1303 MB, throughput 0.986709
Numeric result:
Recommendation: 2 clients, utility 1.00542:
    h1: 527.291 MB (U(h) = 0.912273*h^0.0163328)
    h2: 5647.71 MB (U(h) = 0.219777*h^0.174788)
Recommendation: 2 clients, utility 1.00542:
    h1: 527.706 MB (U(h) = 0.912273*h^0.0163328)
    h2: 5647.29 MB (U(h) = 0.219777*h^0.174788)
Reading from 30404: heap size 323 MB, throughput 0.99736
Reading from 30404: heap size 324 MB, throughput 0.996041
Reading from 30403: heap size 1298 MB, throughput 0.990308
Reading from 30404: heap size 332 MB, throughput 0.996217
Reading from 30404: heap size 332 MB, throughput 0.997214
Reading from 30403: heap size 1307 MB, throughput 0.990855
Reading from 30404: heap size 339 MB, throughput 0.997772
Numeric result:
Recommendation: 2 clients, utility 1.13041:
    h1: 399.354 MB (U(h) = 0.913858*h^0.0159284)
    h2: 5775.65 MB (U(h) = 0.152903*h^0.230356)
Recommendation: 2 clients, utility 1.13041:
    h1: 399.368 MB (U(h) = 0.913858*h^0.0159284)
    h2: 5775.63 MB (U(h) = 0.152903*h^0.230356)
Reading from 30404: heap size 340 MB, throughput 0.989816
Reading from 30403: heap size 1314 MB, throughput 0.990254
Reading from 30404: heap size 347 MB, throughput 0.993395
Reading from 30404: heap size 347 MB, throughput 0.996054
Reading from 30403: heap size 1315 MB, throughput 0.989105
Reading from 30404: heap size 356 MB, throughput 0.997329
Reading from 30403: heap size 1307 MB, throughput 0.988483
Reading from 30404: heap size 357 MB, throughput 0.997582
Numeric result:
Recommendation: 2 clients, utility 1.1975:
    h1: 348.169 MB (U(h) = 0.916038*h^0.0153786)
    h2: 5826.83 MB (U(h) = 0.128486*h^0.257186)
Recommendation: 2 clients, utility 1.1975:
    h1: 348.404 MB (U(h) = 0.916038*h^0.0153786)
    h2: 5826.6 MB (U(h) = 0.128486*h^0.257186)
Reading from 30404: heap size 365 MB, throughput 0.997552
Reading from 30403: heap size 1202 MB, throughput 0.987834
Reading from 30404: heap size 347 MB, throughput 0.996566
Reading from 30404: heap size 355 MB, throughput 0.996304
Reading from 30403: heap size 1294 MB, throughput 0.987195
Reading from 30404: heap size 339 MB, throughput 0.996758
Reading from 30404: heap size 354 MB, throughput 0.997233
Numeric result:
Recommendation: 2 clients, utility 1.26753:
    h1: 311.27 MB (U(h) = 0.917089*h^0.0151154)
    h2: 5863.73 MB (U(h) = 0.107155*h^0.284714)
Recommendation: 2 clients, utility 1.26753:
    h1: 311.302 MB (U(h) = 0.917089*h^0.0151154)
    h2: 5863.7 MB (U(h) = 0.107155*h^0.284714)
Reading from 30403: heap size 1220 MB, throughput 0.985089
Reading from 30404: heap size 338 MB, throughput 0.998128
Reading from 30404: heap size 322 MB, throughput 0.998586
Reading from 30403: heap size 1286 MB, throughput 0.984251
Reading from 30404: heap size 308 MB, throughput 0.998878
Reading from 30404: heap size 314 MB, throughput 0.99865
Reading from 30403: heap size 1291 MB, throughput 0.982871
Reading from 30404: heap size 301 MB, throughput 0.998592
Numeric result:
Recommendation: 2 clients, utility 1.38184:
    h1: 271.21 MB (U(h) = 0.917553*h^0.0149967)
    h2: 5903.79 MB (U(h) = 0.0813633*h^0.326403)
Recommendation: 2 clients, utility 1.38184:
    h1: 271.249 MB (U(h) = 0.917553*h^0.0149967)
    h2: 5903.75 MB (U(h) = 0.0813633*h^0.326403)
Reading from 30404: heap size 307 MB, throughput 0.998752
Reading from 30403: heap size 1292 MB, throughput 0.983308
Reading from 30404: heap size 292 MB, throughput 0.998872
Reading from 30404: heap size 279 MB, throughput 0.998232
Reading from 30404: heap size 270 MB, throughput 0.996854
Reading from 30403: heap size 1293 MB, throughput 0.981169
Reading from 30404: heap size 281 MB, throughput 0.997839
Reading from 30404: heap size 267 MB, throughput 0.997195
Numeric result:
Recommendation: 2 clients, utility 1.51067:
    h1: 286.149 MB (U(h) = 0.902881*h^0.0180376)
    h2: 5888.85 MB (U(h) = 0.0602478*h^0.371161)
Recommendation: 2 clients, utility 1.51067:
    h1: 286.184 MB (U(h) = 0.902881*h^0.0180376)
    h2: 5888.82 MB (U(h) = 0.0602478*h^0.371161)
Reading from 30404: heap size 276 MB, throughput 0.997692
Reading from 30403: heap size 1294 MB, throughput 0.980266
Reading from 30404: heap size 285 MB, throughput 0.997755
Reading from 30404: heap size 293 MB, throughput 0.997742
Reading from 30403: heap size 1299 MB, throughput 0.979239
Reading from 30404: heap size 281 MB, throughput 0.996032
Reading from 30404: heap size 289 MB, throughput 0.997102
Reading from 30403: heap size 1302 MB, throughput 0.977056
Reading from 30404: heap size 277 MB, throughput 0.997175
Numeric result:
Recommendation: 2 clients, utility 1.57054:
    h1: 215.401 MB (U(h) = 0.922061*h^0.0141292)
    h2: 5959.6 MB (U(h) = 0.0527981*h^0.390892)
Recommendation: 2 clients, utility 1.57054:
    h1: 215.415 MB (U(h) = 0.922061*h^0.0141292)
    h2: 5959.58 MB (U(h) = 0.0527981*h^0.390892)
Reading from 30404: heap size 285 MB, throughput 0.997522
Reading from 30404: heap size 270 MB, throughput 0.995659
Reading from 30403: heap size 1309 MB, throughput 0.976245
Reading from 30404: heap size 258 MB, throughput 0.994816
Reading from 30404: heap size 245 MB, throughput 0.996787
Reading from 30404: heap size 234 MB, throughput 0.997409
Reading from 30403: heap size 1315 MB, throughput 0.975111
Reading from 30404: heap size 224 MB, throughput 0.996891
Reading from 30404: heap size 215 MB, throughput 0.996598
Numeric result:
Recommendation: 2 clients, utility 1.65132:
    h1: 37.9919 MB (U(h) = 0.981877*h^0.00255668)
    h2: 6137.01 MB (U(h) = 0.0454291*h^0.412993)
Recommendation: 2 clients, utility 1.65132:
    h1: 37.9918 MB (U(h) = 0.981877*h^0.00255668)
    h2: 6137.01 MB (U(h) = 0.0454291*h^0.412993)
Reading from 30404: heap size 207 MB, throughput 0.997166
Reading from 30404: heap size 164 MB, throughput 0.991742
Reading from 30403: heap size 1319 MB, throughput 0.97481
Reading from 30404: heap size 140 MB, throughput 0.993776
Reading from 30404: heap size 123 MB, throughput 0.994768
Reading from 30404: heap size 111 MB, throughput 0.994913
Reading from 30404: heap size 102 MB, throughput 0.993442
Reading from 30404: heap size 95 MB, throughput 0.994153
Reading from 30404: heap size 89 MB, throughput 0.993886
Reading from 30404: heap size 84 MB, throughput 0.994158
Reading from 30404: heap size 80 MB, throughput 0.977156
Reading from 30403: heap size 1324 MB, throughput 0.9761
Reading from 30404: heap size 75 MB, throughput 0.977712
Reading from 30404: heap size 71 MB, throughput 0.986052
Reading from 30404: heap size 73 MB, throughput 0.990453
Reading from 30404: heap size 66 MB, throughput 0.992983
Reading from 30404: heap size 68 MB, throughput 0.993998
Reading from 30404: heap size 62 MB, throughput 0.993848
Reading from 30404: heap size 65 MB, throughput 0.994081
Reading from 30404: heap size 59 MB, throughput 0.994805
Reading from 30404: heap size 62 MB, throughput 0.99482
Reading from 30404: heap size 57 MB, throughput 0.994785
Reading from 30404: heap size 59 MB, throughput 0.994103
Reading from 30404: heap size 55 MB, throughput 0.99313
Reading from 30404: heap size 57 MB, throughput 0.993231
Reading from 30404: heap size 53 MB, throughput 0.991861
Reading from 30404: heap size 55 MB, throughput 0.986617
Reading from 30404: heap size 52 MB, throughput 0.981596
Reading from 30404: heap size 52 MB, throughput 0.975738
Reading from 30404: heap size 51 MB, throughput 0.967716
Reading from 30404: heap size 51 MB, throughput 0.966535
Reading from 30404: heap size 51 MB, throughput 0.968667
Reading from 30404: heap size 51 MB, throughput 0.969773
Reading from 30404: heap size 50 MB, throughput 0.966433
Reading from 30404: heap size 50 MB, throughput 0.926291
Reading from 30404: heap size 49 MB, throughput 0.921063
Reading from 30404: heap size 48 MB, throughput 0.911959
Numeric result:
Recommendation: 2 clients, utility 1.79851:
    h1: 161.389 MB (U(h) = 0.930012*h^0.0124309)
    h2: 6013.61 MB (U(h) = 0.0322383*h^0.463229)
Recommendation: 2 clients, utility 1.79851:
    h1: 161.378 MB (U(h) = 0.930012*h^0.0124309)
    h2: 6013.62 MB (U(h) = 0.0322383*h^0.463229)
Reading from 30404: heap size 47 MB, throughput 0.914087
Reading from 30403: heap size 1326 MB, throughput 0.973412
Reading from 30404: heap size 50 MB, throughput 0.928649
Reading from 30404: heap size 54 MB, throughput 0.970362
Reading from 30404: heap size 58 MB, throughput 0.980818
Reading from 30404: heap size 63 MB, throughput 0.988722
Reading from 30404: heap size 71 MB, throughput 0.992552
Reading from 30404: heap size 73 MB, throughput 0.994776
Reading from 30404: heap size 81 MB, throughput 0.995534
Reading from 30404: heap size 83 MB, throughput 0.996167
Reading from 30404: heap size 91 MB, throughput 0.995589
Reading from 30404: heap size 93 MB, throughput 0.996752
Reading from 30404: heap size 100 MB, throughput 0.996721
Reading from 30404: heap size 102 MB, throughput 0.997138
Reading from 30403: heap size 1331 MB, throughput 0.974805
Reading from 30404: heap size 109 MB, throughput 0.997228
Reading from 30404: heap size 111 MB, throughput 0.997232
Reading from 30404: heap size 117 MB, throughput 0.997635
Reading from 30404: heap size 119 MB, throughput 0.997877
Reading from 30404: heap size 124 MB, throughput 0.998034
Reading from 30404: heap size 126 MB, throughput 0.99798
Reading from 30403: heap size 1331 MB, throughput 0.974059
Reading from 30404: heap size 131 MB, throughput 0.998144
Reading from 30404: heap size 133 MB, throughput 0.998217
Numeric result:
Recommendation: 2 clients, utility 1.86377:
    h1: 160.537 MB (U(h) = 0.928415*h^0.0128611)
    h2: 6014.46 MB (U(h) = 0.0284068*h^0.481811)
Recommendation: 2 clients, utility 1.86377:
    h1: 160.546 MB (U(h) = 0.928415*h^0.0128611)
    h2: 6014.45 MB (U(h) = 0.0284068*h^0.481811)
Reading from 30404: heap size 138 MB, throughput 0.998293
Reading from 30404: heap size 140 MB, throughput 0.998275
Reading from 30404: heap size 145 MB, throughput 0.998224
Reading from 30404: heap size 147 MB, throughput 0.998289
Reading from 30404: heap size 152 MB, throughput 0.998238
Reading from 30404: heap size 154 MB, throughput 0.998355
Reading from 30404: heap size 158 MB, throughput 0.998356
Reading from 30404: heap size 161 MB, throughput 0.998389
Reading from 30404: heap size 156 MB, throughput 0.984511
Reading from 30404: heap size 163 MB, throughput 0.974055
Reading from 30404: heap size 159 MB, throughput 0.995802
Reading from 30404: heap size 170 MB, throughput 0.992022
Reading from 30404: heap size 163 MB, throughput 0.995521
Numeric result:
Recommendation: 2 clients, utility 1.86469:
    h1: 159.804 MB (U(h) = 0.929154*h^0.0128024)
    h2: 6015.2 MB (U(h) = 0.0284068*h^0.481811)
Recommendation: 2 clients, utility 1.86469:
    h1: 159.831 MB (U(h) = 0.929154*h^0.0128024)
    h2: 6015.17 MB (U(h) = 0.0284068*h^0.481811)
Reading from 30404: heap size 157 MB, throughput 0.997232
Reading from 30404: heap size 167 MB, throughput 0.997914
Reading from 30404: heap size 158 MB, throughput 0.998245
Reading from 30404: heap size 166 MB, throughput 0.998291
Reading from 30404: heap size 158 MB, throughput 0.998396
Reading from 30404: heap size 166 MB, throughput 0.998343
Reading from 30404: heap size 158 MB, throughput 0.998445
Reading from 30403: heap size 1334 MB, throughput 0.961561
Reading from 30404: heap size 164 MB, throughput 0.998499
Reading from 30404: heap size 157 MB, throughput 0.998548
Reading from 30404: heap size 163 MB, throughput 0.998481
Reading from 30404: heap size 155 MB, throughput 0.998376
Reading from 30404: heap size 161 MB, throughput 0.998314
Numeric result:
Recommendation: 2 clients, utility 1.94111:
    h1: 154.189 MB (U(h) = 0.929088*h^0.0129026)
    h2: 6020.81 MB (U(h) = 0.0244008*h^0.503844)
Recommendation: 2 clients, utility 1.94111:
    h1: 154.182 MB (U(h) = 0.929088*h^0.0129026)
    h2: 6020.82 MB (U(h) = 0.0244008*h^0.503844)
Reading from 30404: heap size 154 MB, throughput 0.998447
Reading from 30404: heap size 151 MB, throughput 0.998248
Reading from 30404: heap size 153 MB, throughput 0.998337
Reading from 30404: heap size 158 MB, throughput 0.993894
Reading from 30404: heap size 152 MB, throughput 0.988193
Reading from 30404: heap size 161 MB, throughput 0.974953
Reading from 30404: heap size 155 MB, throughput 0.989103
Reading from 30404: heap size 150 MB, throughput 0.993733
Reading from 30404: heap size 158 MB, throughput 0.995776
Reading from 30404: heap size 153 MB, throughput 0.996784
Reading from 30404: heap size 160 MB, throughput 0.997294
Reading from 30404: heap size 155 MB, throughput 0.997432
Reading from 30403: heap size 1509 MB, throughput 0.987906
Reading from 30404: heap size 150 MB, throughput 0.997432
Numeric result:
Recommendation: 2 clients, utility 2.02108:
    h1: 148.985 MB (U(h) = 0.928642*h^0.0130192)
    h2: 6026.01 MB (U(h) = 0.02084*h^0.526595)
Recommendation: 2 clients, utility 2.02108:
    h1: 148.984 MB (U(h) = 0.928642*h^0.0130192)
    h2: 6026.02 MB (U(h) = 0.02084*h^0.526595)
Reading from 30404: heap size 155 MB, throughput 0.99738
Reading from 30404: heap size 150 MB, throughput 0.997387
Reading from 30404: heap size 146 MB, throughput 0.997248
Reading from 30404: heap size 151 MB, throughput 0.997079
Reading from 30404: heap size 147 MB, throughput 0.996924
Reading from 30404: heap size 152 MB, throughput 0.995061
Reading from 30403: heap size 1540 MB, throughput 0.988145
Reading from 30404: heap size 147 MB, throughput 0.994115
Reading from 30403: heap size 1469 MB, throughput 0.980846
Reading from 30403: heap size 1543 MB, throughput 0.967004
Reading from 30404: heap size 152 MB, throughput 0.994919
Reading from 30403: heap size 1548 MB, throughput 0.937333
Reading from 30403: heap size 1569 MB, throughput 0.904527
Reading from 30403: heap size 1582 MB, throughput 0.862528
Reading from 30404: heap size 146 MB, throughput 0.996503
Reading from 30403: heap size 1611 MB, throughput 0.833463
Reading from 30403: heap size 1618 MB, throughput 0.81563
Reading from 30404: heap size 151 MB, throughput 0.997643
Reading from 30404: heap size 149 MB, throughput 0.997563
Reading from 30404: heap size 151 MB, throughput 0.997882
Reading from 30404: heap size 148 MB, throughput 0.995562
Reading from 30404: heap size 155 MB, throughput 0.993017
Numeric result:
Recommendation: 2 clients, utility 2.50049:
    h1: 121.144 MB (U(h) = 0.928153*h^0.0131749)
    h2: 6053.86 MB (U(h) = 0.00818244*h^0.658396)
Recommendation: 2 clients, utility 2.50049:
    h1: 121.142 MB (U(h) = 0.928153*h^0.0131749)
    h2: 6053.86 MB (U(h) = 0.00818244*h^0.658396)
Reading from 30404: heap size 151 MB, throughput 0.979521
Reading from 30404: heap size 143 MB, throughput 0.990399
Reading from 30404: heap size 138 MB, throughput 0.994524
Reading from 30403: heap size 1648 MB, throughput 0.972041
Reading from 30404: heap size 134 MB, throughput 0.996172
Reading from 30404: heap size 129 MB, throughput 0.996749
Reading from 30404: heap size 125 MB, throughput 0.996465
Reading from 30404: heap size 122 MB, throughput 0.996858
Reading from 30404: heap size 118 MB, throughput 0.996879
Reading from 30404: heap size 123 MB, throughput 0.996468
Reading from 30404: heap size 119 MB, throughput 0.995637
Reading from 30403: heap size 1651 MB, throughput 0.981688
Reading from 30404: heap size 125 MB, throughput 0.996446
Reading from 30404: heap size 121 MB, throughput 0.996327
Reading from 30404: heap size 118 MB, throughput 0.995806
Reading from 30404: heap size 123 MB, throughput 0.994706
Reading from 30404: heap size 119 MB, throughput 0.99393
Numeric result:
Recommendation: 2 clients, utility 2.48461:
    h1: 127.514 MB (U(h) = 0.925775*h^0.0138291)
    h2: 6047.49 MB (U(h) = 0.00830183*h^0.655933)
Recommendation: 2 clients, utility 2.48461:
    h1: 127.5 MB (U(h) = 0.925775*h^0.0138291)
    h2: 6047.5 MB (U(h) = 0.00830183*h^0.655933)
Reading from 30404: heap size 123 MB, throughput 0.994911
Reading from 30404: heap size 130 MB, throughput 0.996499
Reading from 30404: heap size 125 MB, throughput 0.997127
Reading from 30404: heap size 131 MB, throughput 0.997238
Reading from 30403: heap size 1659 MB, throughput 0.985153
Reading from 30404: heap size 125 MB, throughput 0.997846
Reading from 30404: heap size 131 MB, throughput 0.998059
Reading from 30404: heap size 125 MB, throughput 0.998174
Reading from 30404: heap size 131 MB, throughput 0.99816
Reading from 30404: heap size 125 MB, throughput 0.99793
Reading from 30403: heap size 1668 MB, throughput 0.986693
Client 30404 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 30403: heap size 1664 MB, throughput 0.986827
Reading from 30403: heap size 1674 MB, throughput 0.986404
Recommendation: one client; give it all the memory
Reading from 30403: heap size 1664 MB, throughput 0.985889
Reading from 30403: heap size 1674 MB, throughput 0.986932
Recommendation: one client; give it all the memory
Reading from 30403: heap size 1677 MB, throughput 0.987598
Recommendation: one client; give it all the memory
Reading from 30403: heap size 1681 MB, throughput 0.987236
Reading from 30403: heap size 1670 MB, throughput 0.986577
Recommendation: one client; give it all the memory
Reading from 30403: heap size 1678 MB, throughput 0.986261
Reading from 30403: heap size 1670 MB, throughput 0.986003
Recommendation: one client; give it all the memory
Reading from 30403: heap size 1674 MB, throughput 0.984851
Reading from 30403: heap size 1678 MB, throughput 0.983867
Recommendation: one client; give it all the memory
Reading from 30403: heap size 1682 MB, throughput 0.98256
Recommendation: one client; give it all the memory
Reading from 30403: heap size 1689 MB, throughput 0.982091
Reading from 30403: heap size 1695 MB, throughput 0.981465
Recommendation: one client; give it all the memory
Reading from 30403: heap size 1704 MB, throughput 0.981331
Reading from 30403: heap size 1709 MB, throughput 0.980656
Recommendation: one client; give it all the memory
Reading from 30403: heap size 1717 MB, throughput 0.974652
Recommendation: one client; give it all the memory
Reading from 30403: heap size 1669 MB, throughput 0.988946
Reading from 30403: heap size 1665 MB, throughput 0.991734
Recommendation: one client; give it all the memory
Reading from 30403: heap size 1679 MB, throughput 0.992582
Recommendation: one client; give it all the memory
Reading from 30403: heap size 1691 MB, throughput 0.992337
Reading from 30403: heap size 1693 MB, throughput 0.991856
Recommendation: one client; give it all the memory
Reading from 30403: heap size 1682 MB, throughput 0.991177
Reading from 30403: heap size 1526 MB, throughput 0.990203
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 30403: heap size 1663 MB, throughput 0.995554
Recommendation: one client; give it all the memory
Reading from 30403: heap size 1554 MB, throughput 0.993437
Reading from 30403: heap size 1654 MB, throughput 0.986875
Reading from 30403: heap size 1667 MB, throughput 0.972573
Reading from 30403: heap size 1698 MB, throughput 0.954102
Reading from 30403: heap size 1722 MB, throughput 0.922785
Client 30403 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
