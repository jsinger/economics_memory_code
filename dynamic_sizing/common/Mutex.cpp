#include "Mutex.hpp"
#include "Utilities.hpp"

economem::Mutex::Mutex(bool recursive)
{
    pthread_mutexattr_t attr;
    pthread_mutexattr_init(&attr);

    if (recursive)
        pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);

    if (pthread_mutex_init(&m_Mutex, &attr) != 0)
        economem::die("Failed to create mutex");
}

economem::Mutex::~Mutex()
{
    pthread_mutex_destroy(&m_Mutex);
}

void economem::Mutex::lock()
{
    pthread_mutex_lock(&m_Mutex);
}

void economem::Mutex::unlock()
{
    pthread_mutex_unlock(&m_Mutex);
}

void economem::Mutex::condWaitOnLock(pthread_cond_t* cond)
{
    pthread_cond_wait(cond, &m_Mutex);
}
