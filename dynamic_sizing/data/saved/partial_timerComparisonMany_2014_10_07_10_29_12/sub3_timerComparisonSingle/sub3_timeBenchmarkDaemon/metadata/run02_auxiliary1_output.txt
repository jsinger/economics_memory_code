economemd
    total memory: 6265 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub3_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 23126: heap size 9 MB, throughput 0.983373
Clients: 1
Client 23126 has a minimum heap size of 30 MB
Reading from 23125: heap size 9 MB, throughput 0.990788
Clients: 2
Client 23125 has a minimum heap size of 1223 MB
Reading from 23126: heap size 9 MB, throughput 0.979887
Reading from 23125: heap size 9 MB, throughput 0.986441
Reading from 23126: heap size 11 MB, throughput 0.976315
Reading from 23126: heap size 11 MB, throughput 0.964083
Reading from 23126: heap size 15 MB, throughput 0.948668
Reading from 23126: heap size 15 MB, throughput 0.905054
Reading from 23125: heap size 9 MB, throughput 0.979237
Reading from 23126: heap size 24 MB, throughput 0.860791
Reading from 23126: heap size 24 MB, throughput 0.835192
Reading from 23126: heap size 40 MB, throughput 0.88394
Reading from 23126: heap size 40 MB, throughput 0.896204
Reading from 23126: heap size 60 MB, throughput 0.922159
Reading from 23125: heap size 9 MB, throughput 0.972097
Reading from 23126: heap size 60 MB, throughput 0.93378
Reading from 23126: heap size 91 MB, throughput 0.952119
Reading from 23126: heap size 91 MB, throughput 0.910265
Reading from 23126: heap size 140 MB, throughput 0.947909
Reading from 23125: heap size 11 MB, throughput 0.980964
Reading from 23125: heap size 11 MB, throughput 0.980172
Reading from 23125: heap size 17 MB, throughput 0.960363
Reading from 23125: heap size 17 MB, throughput 0.860467
Reading from 23125: heap size 30 MB, throughput 0.781158
Reading from 23125: heap size 31 MB, throughput 0.424603
Reading from 23125: heap size 34 MB, throughput 0.738858
Reading from 23126: heap size 140 MB, throughput 0.934135
Reading from 23125: heap size 47 MB, throughput 0.765176
Reading from 23125: heap size 51 MB, throughput 0.466896
Reading from 23125: heap size 51 MB, throughput 0.551009
Reading from 23125: heap size 71 MB, throughput 0.281782
Reading from 23125: heap size 72 MB, throughput 0.804444
Reading from 23126: heap size 224 MB, throughput 0.887244
Reading from 23125: heap size 78 MB, throughput 0.466536
Reading from 23125: heap size 97 MB, throughput 0.785615
Reading from 23125: heap size 103 MB, throughput 0.763227
Reading from 23125: heap size 104 MB, throughput 0.744627
Reading from 23125: heap size 108 MB, throughput 0.363301
Reading from 23126: heap size 240 MB, throughput 0.995608
Reading from 23125: heap size 136 MB, throughput 0.716526
Reading from 23125: heap size 141 MB, throughput 0.706732
Reading from 23125: heap size 145 MB, throughput 0.381635
Reading from 23125: heap size 185 MB, throughput 0.704867
Reading from 23125: heap size 187 MB, throughput 0.671595
Reading from 23125: heap size 189 MB, throughput 0.701787
Reading from 23125: heap size 193 MB, throughput 0.677116
Reading from 23126: heap size 278 MB, throughput 0.989773
Reading from 23125: heap size 198 MB, throughput 0.548474
Reading from 23125: heap size 206 MB, throughput 0.575968
Reading from 23125: heap size 212 MB, throughput 0.539854
Reading from 23125: heap size 222 MB, throughput 0.433795
Reading from 23125: heap size 262 MB, throughput 0.457199
Reading from 23126: heap size 286 MB, throughput 0.949574
Reading from 23125: heap size 267 MB, throughput 0.234236
Reading from 23125: heap size 305 MB, throughput 0.532261
Reading from 23125: heap size 308 MB, throughput 0.576074
Reading from 23125: heap size 300 MB, throughput 0.631273
Reading from 23125: heap size 305 MB, throughput 0.620516
Reading from 23126: heap size 323 MB, throughput 0.988009
Reading from 23125: heap size 306 MB, throughput 0.642775
Reading from 23125: heap size 308 MB, throughput 0.60642
Reading from 23125: heap size 309 MB, throughput 0.60161
Reading from 23125: heap size 318 MB, throughput 0.559421
Reading from 23126: heap size 328 MB, throughput 0.984954
Reading from 23125: heap size 323 MB, throughput 0.413835
Reading from 23125: heap size 373 MB, throughput 0.550063
Reading from 23125: heap size 383 MB, throughput 0.594288
Reading from 23125: heap size 385 MB, throughput 0.54577
Reading from 23125: heap size 390 MB, throughput 0.502915
Reading from 23126: heap size 365 MB, throughput 0.952602
Numeric result:
Recommendation: 2 clients, utility 1.08662:
    h1: 5042 MB (U(h) = 0.900619*h^0.010322)
    h2: 1223 MB (U(h) = 1.09706*h^0.001)
Recommendation: 2 clients, utility 1.08662:
    h1: 5042 MB (U(h) = 0.900619*h^0.010322)
    h2: 1223 MB (U(h) = 1.09706*h^0.001)
Reading from 23125: heap size 395 MB, throughput 0.243709
Reading from 23125: heap size 452 MB, throughput 0.467941
Reading from 23125: heap size 388 MB, throughput 0.535042
Reading from 23125: heap size 447 MB, throughput 0.574593
Reading from 23126: heap size 376 MB, throughput 0.989906
Reading from 23125: heap size 450 MB, throughput 0.537684
Reading from 23125: heap size 455 MB, throughput 0.559673
Reading from 23125: heap size 457 MB, throughput 0.344424
Reading from 23125: heap size 517 MB, throughput 0.521761
Reading from 23126: heap size 413 MB, throughput 0.991391
Reading from 23125: heap size 520 MB, throughput 0.536011
Reading from 23125: heap size 522 MB, throughput 0.530185
Reading from 23125: heap size 528 MB, throughput 0.527221
Reading from 23125: heap size 533 MB, throughput 0.545343
Reading from 23125: heap size 540 MB, throughput 0.542129
Reading from 23126: heap size 416 MB, throughput 0.985926
Reading from 23125: heap size 543 MB, throughput 0.337415
Reading from 23125: heap size 609 MB, throughput 0.473104
Reading from 23125: heap size 624 MB, throughput 0.494467
Reading from 23126: heap size 440 MB, throughput 0.988877
Reading from 23125: heap size 625 MB, throughput 0.483398
Reading from 23125: heap size 632 MB, throughput 0.48841
Reading from 23126: heap size 442 MB, throughput 0.97368
Reading from 23125: heap size 637 MB, throughput 0.229147
Reading from 23125: heap size 713 MB, throughput 0.728147
Reading from 23126: heap size 465 MB, throughput 0.976912
Reading from 23125: heap size 714 MB, throughput 0.816827
Reading from 23125: heap size 710 MB, throughput 0.848854
Reading from 23126: heap size 470 MB, throughput 0.976912
Reading from 23125: heap size 718 MB, throughput 0.770391
Reading from 23125: heap size 726 MB, throughput 0.703309
Reading from 23125: heap size 735 MB, throughput 0.581064
Numeric result:
Recommendation: 2 clients, utility 0.973198:
    h1: 5042 MB (U(h) = 0.885409*h^0.0153453)
    h2: 1223 MB (U(h) = 0.95753*h^0.001)
Recommendation: 2 clients, utility 0.973198:
    h1: 5042 MB (U(h) = 0.885409*h^0.0153453)
    h2: 1223 MB (U(h) = 0.95753*h^0.001)
Reading from 23126: heap size 501 MB, throughput 0.982762
Reading from 23125: heap size 749 MB, throughput 0.488872
Reading from 23125: heap size 825 MB, throughput 0.393152
Reading from 23125: heap size 818 MB, throughput 0.714136
Reading from 23126: heap size 506 MB, throughput 0.982019
Reading from 23125: heap size 732 MB, throughput 0.660772
Reading from 23125: heap size 811 MB, throughput 0.588999
Reading from 23125: heap size 817 MB, throughput 0.563263
Reading from 23126: heap size 537 MB, throughput 0.986852
Reading from 23125: heap size 816 MB, throughput 0.367299
Reading from 23125: heap size 899 MB, throughput 0.698786
Reading from 23125: heap size 904 MB, throughput 0.73455
Reading from 23125: heap size 907 MB, throughput 0.835565
Reading from 23126: heap size 541 MB, throughput 0.984866
Reading from 23125: heap size 916 MB, throughput 0.862351
Reading from 23125: heap size 916 MB, throughput 0.554608
Reading from 23125: heap size 1009 MB, throughput 0.699214
Reading from 23126: heap size 564 MB, throughput 0.98783
Reading from 23125: heap size 1012 MB, throughput 0.728782
Reading from 23125: heap size 1018 MB, throughput 0.732151
Reading from 23125: heap size 1019 MB, throughput 0.772926
Reading from 23125: heap size 1015 MB, throughput 0.780767
Reading from 23125: heap size 1020 MB, throughput 0.795951
Reading from 23126: heap size 568 MB, throughput 0.988803
Reading from 23125: heap size 1016 MB, throughput 0.78295
Reading from 23125: heap size 1020 MB, throughput 0.81453
Numeric result:
Recommendation: 2 clients, utility 0.73638:
    h1: 5042 MB (U(h) = 0.880664*h^0.0168004)
    h2: 1223 MB (U(h) = 0.719448*h^0.001)
Recommendation: 2 clients, utility 0.73638:
    h1: 5042 MB (U(h) = 0.880664*h^0.0168004)
    h2: 1223 MB (U(h) = 0.719448*h^0.001)
Reading from 23126: heap size 587 MB, throughput 0.987244
Reading from 23125: heap size 1014 MB, throughput 0.954955
Reading from 23125: heap size 1019 MB, throughput 0.918656
Reading from 23125: heap size 1021 MB, throughput 0.870285
Reading from 23125: heap size 1026 MB, throughput 0.846215
Reading from 23126: heap size 591 MB, throughput 0.989287
Reading from 23125: heap size 1036 MB, throughput 0.827771
Reading from 23125: heap size 1037 MB, throughput 0.810059
Reading from 23125: heap size 1027 MB, throughput 0.788449
Reading from 23125: heap size 1037 MB, throughput 0.779203
Reading from 23125: heap size 1028 MB, throughput 0.854626
Reading from 23125: heap size 1034 MB, throughput 0.861008
Reading from 23126: heap size 613 MB, throughput 0.987942
Reading from 23125: heap size 1027 MB, throughput 0.839107
Reading from 23125: heap size 1035 MB, throughput 0.779433
Reading from 23125: heap size 1028 MB, throughput 0.741905
Reading from 23125: heap size 1042 MB, throughput 0.705585
Reading from 23125: heap size 1058 MB, throughput 0.689153
Reading from 23126: heap size 617 MB, throughput 0.983171
Reading from 23125: heap size 1069 MB, throughput 0.682831
Reading from 23125: heap size 1187 MB, throughput 0.613203
Reading from 23125: heap size 1193 MB, throughput 0.68811
Reading from 23125: heap size 1204 MB, throughput 0.68932
Reading from 23126: heap size 646 MB, throughput 0.989566
Reading from 23125: heap size 1207 MB, throughput 0.733106
Reading from 23125: heap size 1212 MB, throughput 0.82381
Numeric result:
Recommendation: 2 clients, utility 0.627027:
    h1: 3901.71 MB (U(h) = 0.878495*h^0.0174394)
    h2: 2363.29 MB (U(h) = 0.569259*h^0.0105552)
Recommendation: 2 clients, utility 0.627027:
    h1: 3902.82 MB (U(h) = 0.878495*h^0.0174394)
    h2: 2362.18 MB (U(h) = 0.569259*h^0.0105552)
Reading from 23126: heap size 646 MB, throughput 0.987576
Reading from 23126: heap size 668 MB, throughput 0.980967
Reading from 23125: heap size 1217 MB, throughput 0.953791
Reading from 23126: heap size 667 MB, throughput 0.986253
Reading from 23125: heap size 1212 MB, throughput 0.967843
Reading from 23126: heap size 690 MB, throughput 0.989677
Reading from 23126: heap size 693 MB, throughput 0.986577
Reading from 23125: heap size 1222 MB, throughput 0.966656
Numeric result:
Recommendation: 2 clients, utility 0.66891:
    h1: 1969.96 MB (U(h) = 0.87815*h^0.017536)
    h2: 4295.04 MB (U(h) = 0.4843*h^0.0382367)
Recommendation: 2 clients, utility 0.66891:
    h1: 1969.84 MB (U(h) = 0.87815*h^0.017536)
    h2: 4295.16 MB (U(h) = 0.4843*h^0.0382367)
Reading from 23126: heap size 718 MB, throughput 0.988336
Reading from 23125: heap size 1215 MB, throughput 0.969198
Reading from 23126: heap size 719 MB, throughput 0.989852
Reading from 23126: heap size 742 MB, throughput 0.992876
Reading from 23125: heap size 1222 MB, throughput 0.964043
Reading from 23126: heap size 744 MB, throughput 0.991169
Reading from 23125: heap size 1218 MB, throughput 0.964263
Numeric result:
Recommendation: 2 clients, utility 0.760363:
    h1: 1012.14 MB (U(h) = 0.877032*h^0.0178441)
    h2: 5252.86 MB (U(h) = 0.346734*h^0.0925663)
Recommendation: 2 clients, utility 0.760363:
    h1: 1012.52 MB (U(h) = 0.877032*h^0.0178441)
    h2: 5252.48 MB (U(h) = 0.346734*h^0.0925663)
Reading from 23126: heap size 767 MB, throughput 0.991026
Reading from 23126: heap size 767 MB, throughput 0.990057
Reading from 23125: heap size 1225 MB, throughput 0.969478
Reading from 23126: heap size 791 MB, throughput 0.990732
Reading from 23126: heap size 791 MB, throughput 0.992634
Reading from 23125: heap size 1224 MB, throughput 0.973227
Reading from 23126: heap size 809 MB, throughput 0.992539
Numeric result:
Recommendation: 2 clients, utility 0.807462:
    h1: 843.561 MB (U(h) = 0.876392*h^0.018016)
    h2: 5421.44 MB (U(h) = 0.301561*h^0.115779)
Recommendation: 2 clients, utility 0.807462:
    h1: 843.604 MB (U(h) = 0.876392*h^0.018016)
    h2: 5421.4 MB (U(h) = 0.301561*h^0.115779)
Reading from 23125: heap size 1228 MB, throughput 0.97129
Reading from 23126: heap size 812 MB, throughput 0.992044
Reading from 23126: heap size 834 MB, throughput 0.994302
Reading from 23125: heap size 1223 MB, throughput 0.973464
Reading from 23126: heap size 834 MB, throughput 0.992443
Reading from 23125: heap size 1227 MB, throughput 0.974003
Numeric result:
Recommendation: 2 clients, utility 0.928066:
    h1: 588.667 MB (U(h) = 0.875979*h^0.0181255)
    h2: 5676.33 MB (U(h) = 0.208315*h^0.174786)
Recommendation: 2 clients, utility 0.928066:
    h1: 588.644 MB (U(h) = 0.875979*h^0.0181255)
    h2: 5676.36 MB (U(h) = 0.208315*h^0.174786)
Reading from 23126: heap size 852 MB, throughput 0.991853
Reading from 23125: heap size 1232 MB, throughput 0.968386
Reading from 23126: heap size 793 MB, throughput 0.986603
Reading from 23126: heap size 743 MB, throughput 0.994044
Reading from 23125: heap size 1233 MB, throughput 0.971204
Reading from 23126: heap size 710 MB, throughput 0.988723
Reading from 23126: heap size 690 MB, throughput 0.990076
Numeric result:
Recommendation: 2 clients, utility 0.964805:
    h1: 549.577 MB (U(h) = 0.875611*h^0.0182273)
    h2: 5715.42 MB (U(h) = 0.190524*h^0.189573)
Recommendation: 2 clients, utility 0.964805:
    h1: 549.538 MB (U(h) = 0.875611*h^0.0182273)
    h2: 5715.46 MB (U(h) = 0.190524*h^0.189573)
Reading from 23125: heap size 1237 MB, throughput 0.973572
Reading from 23126: heap size 649 MB, throughput 0.99242
Reading from 23126: heap size 616 MB, throughput 0.988196
Reading from 23125: heap size 1241 MB, throughput 0.971468
Reading from 23126: heap size 600 MB, throughput 0.989055
Reading from 23126: heap size 568 MB, throughput 0.991359
Reading from 23125: heap size 1245 MB, throughput 0.97121
Reading from 23126: heap size 543 MB, throughput 0.989445
Numeric result:
Recommendation: 2 clients, utility 1.10017:
    h1: 431.847 MB (U(h) = 0.875045*h^0.018416)
    h2: 5833.15 MB (U(h) = 0.130013*h^0.248788)
Recommendation: 2 clients, utility 1.10017:
    h1: 431.791 MB (U(h) = 0.875045*h^0.018416)
    h2: 5833.21 MB (U(h) = 0.130013*h^0.248788)
Reading from 23126: heap size 576 MB, throughput 0.992578
Reading from 23126: heap size 537 MB, throughput 0.989301
Reading from 23125: heap size 1250 MB, throughput 0.971411
Reading from 23126: heap size 525 MB, throughput 0.988419
Reading from 23126: heap size 501 MB, throughput 0.988429
Reading from 23126: heap size 473 MB, throughput 0.984164
Reading from 23126: heap size 459 MB, throughput 0.970864
Reading from 23125: heap size 1257 MB, throughput 0.940406
Reading from 23126: heap size 452 MB, throughput 0.976971
Numeric result:
Recommendation: 2 clients, utility 1.17194:
    h1: 392.048 MB (U(h) = 0.874755*h^0.0185073)
    h2: 5872.95 MB (U(h) = 0.108217*h^0.2772)
Recommendation: 2 clients, utility 1.17194:
    h1: 392.105 MB (U(h) = 0.874755*h^0.0185073)
    h2: 5872.89 MB (U(h) = 0.108217*h^0.2772)
Reading from 23126: heap size 433 MB, throughput 0.984604
Reading from 23126: heap size 398 MB, throughput 0.983438
Reading from 23125: heap size 1333 MB, throughput 0.982468
Reading from 23126: heap size 376 MB, throughput 0.985983
Reading from 23126: heap size 412 MB, throughput 0.989169
Reading from 23126: heap size 395 MB, throughput 0.986286
Reading from 23126: heap size 385 MB, throughput 0.987325
Reading from 23125: heap size 1331 MB, throughput 0.987525
Reading from 23126: heap size 401 MB, throughput 0.989355
Reading from 23126: heap size 387 MB, throughput 0.982513
Reading from 23126: heap size 421 MB, throughput 0.986591
Numeric result:
Recommendation: 2 clients, utility 1.31096:
    h1: 333.378 MB (U(h) = 0.875234*h^0.0185877)
    h2: 5931.62 MB (U(h) = 0.0759125*h^0.330824)
Recommendation: 2 clients, utility 1.31096:
    h1: 333.28 MB (U(h) = 0.875234*h^0.0185877)
    h2: 5931.72 MB (U(h) = 0.0759125*h^0.330824)
Reading from 23125: heap size 1341 MB, throughput 0.988348
Reading from 23126: heap size 395 MB, throughput 0.986123
Reading from 23126: heap size 382 MB, throughput 0.986409
Reading from 23126: heap size 367 MB, throughput 0.988329
Reading from 23126: heap size 353 MB, throughput 0.986282
Reading from 23125: heap size 1350 MB, throughput 0.988274
Reading from 23126: heap size 345 MB, throughput 0.987944
Reading from 23126: heap size 331 MB, throughput 0.989165
Reading from 23126: heap size 346 MB, throughput 0.978148
Reading from 23126: heap size 346 MB, throughput 0.981951
Reading from 23125: heap size 1351 MB, throughput 0.986495
Reading from 23126: heap size 315 MB, throughput 0.986064
Reading from 23126: heap size 326 MB, throughput 0.988501
Numeric result:
Recommendation: 2 clients, utility 1.45517:
    h1: 290.003 MB (U(h) = 0.876791*h^0.0184879)
    h2: 5975 MB (U(h) = 0.0544191*h^0.380988)
Recommendation: 2 clients, utility 1.45517:
    h1: 289.947 MB (U(h) = 0.876791*h^0.0184879)
    h2: 5975.05 MB (U(h) = 0.0544191*h^0.380988)
Reading from 23126: heap size 376 MB, throughput 0.98959
Reading from 23126: heap size 343 MB, throughput 0.99126
Reading from 23125: heap size 1344 MB, throughput 0.983893
Reading from 23126: heap size 335 MB, throughput 0.983431
Reading from 23126: heap size 329 MB, throughput 0.987007
Reading from 23126: heap size 311 MB, throughput 0.988853
Reading from 23126: heap size 296 MB, throughput 0.984731
Reading from 23126: heap size 296 MB, throughput 0.986435
Reading from 23125: heap size 1237 MB, throughput 0.982551
Reading from 23126: heap size 279 MB, throughput 0.987814
Reading from 23126: heap size 297 MB, throughput 0.989324
Reading from 23126: heap size 271 MB, throughput 0.980297
Reading from 23126: heap size 285 MB, throughput 0.986067
Reading from 23126: heap size 288 MB, throughput 0.988168
Numeric result:
Recommendation: 2 clients, utility 1.54092:
    h1: 266.223 MB (U(h) = 0.879792*h^0.0181605)
    h2: 5998.78 MB (U(h) = 0.0450135*h^0.409208)
Recommendation: 2 clients, utility 1.54092:
    h1: 266.223 MB (U(h) = 0.879792*h^0.0181605)
    h2: 5998.78 MB (U(h) = 0.0450135*h^0.409208)
Reading from 23126: heap size 299 MB, throughput 0.985173
Reading from 23125: heap size 1332 MB, throughput 0.981533
Reading from 23126: heap size 276 MB, throughput 0.984841
Reading from 23126: heap size 273 MB, throughput 0.985187
Reading from 23126: heap size 262 MB, throughput 0.982402
Reading from 23126: heap size 274 MB, throughput 0.981139
Reading from 23126: heap size 254 MB, throughput 0.982293
Reading from 23126: heap size 266 MB, throughput 0.985687
Reading from 23125: heap size 1259 MB, throughput 0.980296
Reading from 23126: heap size 248 MB, throughput 0.986347
Reading from 23126: heap size 259 MB, throughput 0.98678
Reading from 23126: heap size 259 MB, throughput 0.975057
Reading from 23126: heap size 271 MB, throughput 0.979407
Reading from 23126: heap size 252 MB, throughput 0.982408
Reading from 23126: heap size 266 MB, throughput 0.985113
Reading from 23125: heap size 1328 MB, throughput 0.981627
Reading from 23126: heap size 265 MB, throughput 0.990692
Numeric result:
Recommendation: 2 clients, utility 1.78789:
    h1: 220.739 MB (U(h) = 0.883288*h^0.0176937)
    h2: 6044.26 MB (U(h) = 0.0270909*h^0.484469)
Recommendation: 2 clients, utility 1.78789:
    h1: 220.748 MB (U(h) = 0.883288*h^0.0176937)
    h2: 6044.25 MB (U(h) = 0.0270909*h^0.484469)
Reading from 23126: heap size 279 MB, throughput 0.990005
Reading from 23126: heap size 257 MB, throughput 0.991995
Reading from 23126: heap size 252 MB, throughput 0.990688
Reading from 23126: heap size 242 MB, throughput 0.984347
Reading from 23126: heap size 237 MB, throughput 0.984808
Reading from 23126: heap size 228 MB, throughput 0.985567
Reading from 23125: heap size 1332 MB, throughput 0.978894
Reading from 23126: heap size 226 MB, throughput 0.986546
Reading from 23126: heap size 216 MB, throughput 0.988581
Reading from 23126: heap size 227 MB, throughput 0.980903
Reading from 23126: heap size 215 MB, throughput 0.980371
Reading from 23126: heap size 221 MB, throughput 0.985128
Reading from 23126: heap size 207 MB, throughput 0.985643
Reading from 23126: heap size 215 MB, throughput 0.987566
Reading from 23125: heap size 1334 MB, throughput 0.977849
Reading from 23126: heap size 215 MB, throughput 0.982485
Reading from 23126: heap size 226 MB, throughput 0.980954
Reading from 23126: heap size 217 MB, throughput 0.978475
Reading from 23126: heap size 221 MB, throughput 0.983232
Reading from 23126: heap size 213 MB, throughput 0.985261
Numeric result:
Recommendation: 2 clients, utility 1.89324:
    h1: 95.7858 MB (U(h) = 0.939299*h^0.00788526)
    h2: 6169.21 MB (U(h) = 0.0231236*h^0.507812)
Recommendation: 2 clients, utility 1.89324:
    h1: 95.7949 MB (U(h) = 0.939299*h^0.00788526)
    h2: 6169.21 MB (U(h) = 0.0231236*h^0.507812)
Reading from 23126: heap size 217 MB, throughput 0.98562
Reading from 23126: heap size 201 MB, throughput 0.989079
Reading from 23126: heap size 185 MB, throughput 0.982953
Reading from 23126: heap size 183 MB, throughput 0.983856
Reading from 23125: heap size 1336 MB, throughput 0.976447
Reading from 23126: heap size 175 MB, throughput 0.98265
Reading from 23126: heap size 169 MB, throughput 0.960322
Reading from 23126: heap size 161 MB, throughput 0.950572
Reading from 23126: heap size 157 MB, throughput 0.974352
Reading from 23126: heap size 152 MB, throughput 0.972574
Reading from 23126: heap size 152 MB, throughput 0.983136
Reading from 23126: heap size 137 MB, throughput 0.960309
Reading from 23126: heap size 132 MB, throughput 0.966176
Reading from 23126: heap size 138 MB, throughput 0.974324
Reading from 23126: heap size 131 MB, throughput 0.970368
Reading from 23126: heap size 133 MB, throughput 0.971294
Reading from 23126: heap size 128 MB, throughput 0.967777
Reading from 23126: heap size 127 MB, throughput 0.969126
Reading from 23126: heap size 124 MB, throughput 0.685988
Reading from 23125: heap size 1339 MB, throughput 0.975481
Reading from 23126: heap size 125 MB, throughput 0.67647
Reading from 23126: heap size 113 MB, throughput 0.954706
Reading from 23126: heap size 114 MB, throughput 0.500049
Reading from 23126: heap size 99 MB, throughput 0.565509
Reading from 23126: heap size 110 MB, throughput 0.350263
Reading from 23126: heap size 100 MB, throughput 0.866075
Reading from 23126: heap size 108 MB, throughput 0.846176
Reading from 23126: heap size 92 MB, throughput 0.87451
Reading from 23126: heap size 109 MB, throughput 0.926319
Reading from 23126: heap size 95 MB, throughput 0.850913
Reading from 23126: heap size 107 MB, throughput 0.91697
Reading from 23126: heap size 90 MB, throughput 0.946634
Reading from 23126: heap size 107 MB, throughput 0.972431
Reading from 23126: heap size 90 MB, throughput 0.974275
Reading from 23126: heap size 105 MB, throughput 0.982053
Reading from 23126: heap size 89 MB, throughput 0.986468
Reading from 23126: heap size 104 MB, throughput 0.976775
Reading from 23126: heap size 94 MB, throughput 0.974648
Reading from 23126: heap size 103 MB, throughput 0.886352
Reading from 23126: heap size 101 MB, throughput 0.837805
Reading from 23126: heap size 103 MB, throughput 0.885352
Numeric result:
Recommendation: 2 clients, utility 2.03165:
    h1: 982.799 MB (U(h) = 0.549327*h^0.0987406)
    h2: 5282.2 MB (U(h) = 0.0198028*h^0.530731)
Recommendation: 2 clients, utility 2.03165:
    h1: 982.744 MB (U(h) = 0.549327*h^0.0987406)
    h2: 5282.26 MB (U(h) = 0.0198028*h^0.530731)
Reading from 23126: heap size 102 MB, throughput 0.923012
Reading from 23125: heap size 1346 MB, throughput 0.974854
Reading from 23126: heap size 103 MB, throughput 0.851504
Reading from 23126: heap size 103 MB, throughput 0.865181
Reading from 23126: heap size 108 MB, throughput 0.730782
Reading from 23126: heap size 108 MB, throughput 0.848375
Reading from 23126: heap size 113 MB, throughput 0.910122
Reading from 23126: heap size 113 MB, throughput 0.947137
Reading from 23126: heap size 117 MB, throughput 0.972925
Reading from 23126: heap size 118 MB, throughput 0.97727
Reading from 23126: heap size 122 MB, throughput 0.984273
Reading from 23126: heap size 122 MB, throughput 0.981155
Reading from 23126: heap size 128 MB, throughput 0.951581
Reading from 23126: heap size 146 MB, throughput 0.957702
Reading from 23126: heap size 156 MB, throughput 0.977034
Reading from 23126: heap size 157 MB, throughput 0.97927
Reading from 23126: heap size 162 MB, throughput 0.986903
Reading from 23126: heap size 163 MB, throughput 0.98617
Reading from 23126: heap size 167 MB, throughput 0.971271
Reading from 23125: heap size 1349 MB, throughput 0.972504
Reading from 23126: heap size 168 MB, throughput 0.978517
Reading from 23126: heap size 175 MB, throughput 0.981237
Reading from 23126: heap size 176 MB, throughput 0.820829
Reading from 23126: heap size 182 MB, throughput 0.729716
Reading from 23126: heap size 185 MB, throughput 0.970405
Reading from 23126: heap size 194 MB, throughput 0.972054
Reading from 23126: heap size 197 MB, throughput 0.971844
Reading from 23126: heap size 208 MB, throughput 0.966692
Reading from 23126: heap size 209 MB, throughput 0.966744
Reading from 23126: heap size 220 MB, throughput 0.966111
Reading from 23125: heap size 1359 MB, throughput 0.970411
Reading from 23126: heap size 222 MB, throughput 0.964611
Reading from 23126: heap size 236 MB, throughput 0.973418
Numeric result:
Recommendation: 2 clients, utility 2.23852:
    h1: 1205.2 MB (U(h) = 0.471645*h^0.130279)
    h2: 5059.8 MB (U(h) = 0.017741*h^0.546949)
Recommendation: 2 clients, utility 2.23852:
    h1: 1205.2 MB (U(h) = 0.471645*h^0.130279)
    h2: 5059.8 MB (U(h) = 0.017741*h^0.546949)
Reading from 23126: heap size 240 MB, throughput 0.976932
Reading from 23126: heap size 253 MB, throughput 0.978902
Reading from 23126: heap size 254 MB, throughput 0.973465
Reading from 23126: heap size 266 MB, throughput 0.969
Reading from 23126: heap size 266 MB, throughput 0.974581
Reading from 23125: heap size 1367 MB, throughput 0.970681
Reading from 23126: heap size 281 MB, throughput 0.969469
Reading from 23126: heap size 266 MB, throughput 0.963674
Reading from 23126: heap size 284 MB, throughput 0.975994
Reading from 23126: heap size 283 MB, throughput 0.987998
Reading from 23126: heap size 283 MB, throughput 0.987998
Reading from 23126: heap size 283 MB, throughput 0.959812
Reading from 23126: heap size 300 MB, throughput 0.986357
Reading from 23126: heap size 302 MB, throughput 0.979226
Reading from 23125: heap size 1372 MB, throughput 0.968731
Reading from 23126: heap size 320 MB, throughput 0.982948
Reading from 23126: heap size 323 MB, throughput 0.981017
Numeric result:
Recommendation: 2 clients, utility 2.48805:
    h1: 1199.57 MB (U(h) = 0.445552*h^0.141585)
    h2: 5065.43 MB (U(h) = 0.0124841*h^0.59781)
Recommendation: 2 clients, utility 2.48805:
    h1: 1199.67 MB (U(h) = 0.445552*h^0.141585)
    h2: 5065.33 MB (U(h) = 0.0124841*h^0.59781)
Reading from 23126: heap size 345 MB, throughput 0.975922
Reading from 23126: heap size 347 MB, throughput 0.980869
Reading from 23125: heap size 1380 MB, throughput 0.969584
Reading from 23126: heap size 374 MB, throughput 0.982203
Reading from 23126: heap size 374 MB, throughput 0.980087
Reading from 23126: heap size 402 MB, throughput 0.984105
Reading from 23126: heap size 402 MB, throughput 0.977396
Reading from 23125: heap size 1382 MB, throughput 0.968692
Reading from 23126: heap size 425 MB, throughput 0.983594
Reading from 23126: heap size 430 MB, throughput 0.983587
Reading from 23126: heap size 456 MB, throughput 0.984707
Numeric result:
Recommendation: 2 clients, utility 2.66213:
    h1: 1054.53 MB (U(h) = 0.467937*h^0.131895)
    h2: 5210.47 MB (U(h) = 0.00859512*h^0.65164)
Recommendation: 2 clients, utility 2.66213:
    h1: 1054.61 MB (U(h) = 0.467937*h^0.131895)
    h2: 5210.39 MB (U(h) = 0.00859512*h^0.65164)
Reading from 23126: heap size 457 MB, throughput 0.984634
Reading from 23126: heap size 484 MB, throughput 0.987972
Reading from 23126: heap size 485 MB, throughput 0.988096
Reading from 23126: heap size 511 MB, throughput 0.991293
Reading from 23126: heap size 513 MB, throughput 0.987742
Reading from 23126: heap size 540 MB, throughput 0.991033
Reading from 23126: heap size 540 MB, throughput 0.986541
Numeric result:
Recommendation: 2 clients, utility 2.57897:
    h1: 937.909 MB (U(h) = 0.510361*h^0.114727)
    h2: 5327.09 MB (U(h) = 0.00859512*h^0.65164)
Recommendation: 2 clients, utility 2.57897:
    h1: 937.888 MB (U(h) = 0.510361*h^0.114727)
    h2: 5327.11 MB (U(h) = 0.00859512*h^0.65164)
Reading from 23126: heap size 566 MB, throughput 0.990565
Reading from 23126: heap size 567 MB, throughput 0.987349
Reading from 23126: heap size 595 MB, throughput 0.986697
Reading from 23126: heap size 595 MB, throughput 0.990267
Reading from 23126: heap size 622 MB, throughput 0.988676
Reading from 23126: heap size 626 MB, throughput 0.982813
Reading from 23125: heap size 1389 MB, throughput 0.959124
Numeric result:
Recommendation: 2 clients, utility 2.54432:
    h1: 826.129 MB (U(h) = 0.549362*h^0.100106)
    h2: 5438.87 MB (U(h) = 0.00816646*h^0.65899)
Recommendation: 2 clients, utility 2.54432:
    h1: 826.201 MB (U(h) = 0.549362*h^0.100106)
    h2: 5438.8 MB (U(h) = 0.00816646*h^0.65899)
Reading from 23126: heap size 660 MB, throughput 0.989101
Reading from 23126: heap size 660 MB, throughput 0.987892
Reading from 23126: heap size 696 MB, throughput 0.988285
Reading from 23126: heap size 696 MB, throughput 0.987917
Reading from 23126: heap size 734 MB, throughput 0.990046
Numeric result:
Recommendation: 2 clients, utility 2.51037:
    h1: 768.019 MB (U(h) = 0.571915*h^0.0920736)
    h2: 5496.98 MB (U(h) = 0.00816646*h^0.65899)
Recommendation: 2 clients, utility 2.51037:
    h1: 768.032 MB (U(h) = 0.571915*h^0.0920736)
    h2: 5496.97 MB (U(h) = 0.00816646*h^0.65899)
Reading from 23126: heap size 734 MB, throughput 0.991144
Reading from 23126: heap size 767 MB, throughput 0.990746
Reading from 23126: heap size 770 MB, throughput 0.989082
Reading from 23125: heap size 1525 MB, throughput 0.985422
Reading from 23126: heap size 729 MB, throughput 0.989293
Reading from 23125: heap size 1568 MB, throughput 0.984847
Reading from 23125: heap size 1441 MB, throughput 0.974948
Reading from 23126: heap size 766 MB, throughput 0.989516
Reading from 23125: heap size 1556 MB, throughput 0.958834
Reading from 23125: heap size 1568 MB, throughput 0.931958
Reading from 23125: heap size 1594 MB, throughput 0.893259
Numeric result:
Recommendation: 2 clients, utility 2.59048:
    h1: 674.718 MB (U(h) = 0.59706*h^0.083379)
    h2: 5590.28 MB (U(h) = 0.00649972*h^0.69076)
Recommendation: 2 clients, utility 2.59048:
    h1: 674.775 MB (U(h) = 0.59706*h^0.083379)
    h2: 5590.23 MB (U(h) = 0.00649972*h^0.69076)
Reading from 23125: heap size 1600 MB, throughput 0.854902
Reading from 23125: heap size 1634 MB, throughput 0.858908
Reading from 23126: heap size 778 MB, throughput 0.990675
Reading from 23126: heap size 736 MB, throughput 0.984052
Reading from 23125: heap size 1636 MB, throughput 0.96404
Reading from 23126: heap size 693 MB, throughput 0.99251
Reading from 23126: heap size 671 MB, throughput 0.990117
Reading from 23125: heap size 1652 MB, throughput 0.975292
Numeric result:
Recommendation: 2 clients, utility 2.69913:
    h1: 611.81 MB (U(h) = 0.609778*h^0.0788348)
    h2: 5653.19 MB (U(h) = 0.00493305*h^0.728422)
Recommendation: 2 clients, utility 2.69913:
    h1: 611.825 MB (U(h) = 0.609778*h^0.0788348)
    h2: 5653.18 MB (U(h) = 0.00493305*h^0.728422)
Reading from 23126: heap size 715 MB, throughput 0.989094
Reading from 23126: heap size 682 MB, throughput 0.986201
Reading from 23126: heap size 640 MB, throughput 0.990633
Reading from 23125: heap size 1660 MB, throughput 0.980582
Client 23126 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 23125: heap size 1661 MB, throughput 0.984228
Reading from 23125: heap size 1672 MB, throughput 0.985927
Recommendation: one client; give it all the memory
Reading from 23125: heap size 1662 MB, throughput 0.986266
Reading from 23125: heap size 1674 MB, throughput 0.986126
Recommendation: one client; give it all the memory
Reading from 23125: heap size 1661 MB, throughput 0.985905
Recommendation: one client; give it all the memory
Reading from 23125: heap size 1671 MB, throughput 0.987367
Reading from 23125: heap size 1671 MB, throughput 0.987588
Recommendation: one client; give it all the memory
Reading from 23125: heap size 1677 MB, throughput 0.987049
Reading from 23125: heap size 1667 MB, throughput 0.986647
Recommendation: one client; give it all the memory
Reading from 23125: heap size 1674 MB, throughput 0.985966
Recommendation: one client; give it all the memory
Reading from 23125: heap size 1678 MB, throughput 0.985003
Reading from 23125: heap size 1681 MB, throughput 0.983795
Recommendation: one client; give it all the memory
Reading from 23125: heap size 1687 MB, throughput 0.983302
Recommendation: one client; give it all the memory
Reading from 23125: heap size 1696 MB, throughput 0.982407
Reading from 23125: heap size 1706 MB, throughput 0.981255
Recommendation: one client; give it all the memory
Reading from 23125: heap size 1714 MB, throughput 0.981154
Recommendation: one client; give it all the memory
Reading from 23125: heap size 1724 MB, throughput 0.98123
Reading from 23125: heap size 1730 MB, throughput 0.974192
Recommendation: one client; give it all the memory
Reading from 23125: heap size 1740 MB, throughput 0.98895
Recommendation: one client; give it all the memory
Reading from 23125: heap size 1744 MB, throughput 0.991921
Reading from 23125: heap size 1760 MB, throughput 0.992949
Recommendation: one client; give it all the memory
Reading from 23125: heap size 1768 MB, throughput 0.992969
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 23125: heap size 1769 MB, throughput 0.993389
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 23125: heap size 1639 MB, throughput 0.994857
Reading from 23125: heap size 1760 MB, throughput 0.990456
Reading from 23125: heap size 1765 MB, throughput 0.980041
Reading from 23125: heap size 1786 MB, throughput 0.965791
Reading from 23125: heap size 1807 MB, throughput 0.940487
Client 23125 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
