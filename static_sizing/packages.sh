#!/bin/bash
# Required packages for Ubuntu

sudo apt-get install python2.7 python-numpy python-scipy python-matplotlib openjdk-7-jdk cgroup-bin wget ruby python-pip
sudo pip install pytimeparse future
