MemTotal:       16318588 kB
MemFree:        13052108 kB
Buffers:          667064 kB
Cached:          1422476 kB
SwapCached:            0 kB
Active:          1842748 kB
Inactive:         898376 kB
Active(anon):     652384 kB
Inactive(anon):      748 kB
Active(file):    1190364 kB
Inactive(file):   897628 kB
Unevictable:           0 kB
Mlocked:               0 kB
SwapTotal:      16728060 kB
SwapFree:       16728060 kB
Dirty:              1052 kB
Writeback:             4 kB
AnonPages:        651580 kB
Mapped:            17180 kB
Shmem:              1552 kB
Slab:             397172 kB
SReclaimable:     376956 kB
SUnreclaim:        20216 kB
KernelStack:        1592 kB
PageTables:         4244 kB
NFS_Unstable:          0 kB
Bounce:                0 kB
WritebackTmp:          0 kB
CommitLimit:    24887352 kB
Committed_AS:     813236 kB
VmallocTotal:   34359738367 kB
VmallocUsed:      370636 kB
VmallocChunk:   34359363072 kB
HardwareCorrupted:     0 kB
AnonHugePages:    628736 kB
HugePages_Total:       0
HugePages_Free:        0
HugePages_Rsvd:        0
HugePages_Surp:        0
Hugepagesize:       2048 kB
DirectMap4k:       70592 kB
DirectMap2M:    16590848 kB
