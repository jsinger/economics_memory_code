#ifndef PROPORTIONALTHROUGHPUT_HPP
#define PROPORTIONALTHROUGHPUT_HPP

#include "AdaptiveWeightedAverage.hpp"
#include "Stopwatch.hpp"
#include "ThroughputCalculator.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
//! Throughput-calculation algorithm that attempts to smooth the difference between major and minor GCs, based on the number of minor GCs that occur between major GCs.
/*! This algorithm attempts to smooth throughputs based on the ratio of major to minor GCs.

  To use, subclass, and implement getHeapSizeB() and logReading() as necessary for your VM/application. */
    class ProportionalThroughput : public ThroughputCalculator
    {
    public:
        ProportionalThroughput();

        virtual void gcBegin();
        virtual ThroughputCalculator::Result minorGCEnd();
        virtual ThroughputCalculator::Result majorGCEnd();

    protected:
        //! Return the current heap size in bytes.
        virtual unsigned long long getHeapSizeB() = 0;

        //! Use if you want to log the calculated throughputs in your client.
        virtual void logReading(unsigned long long heapSizeB, double throughput, unsigned long long gcTime, unsigned long long interGCTime) = 0;

    private:
        ThroughputCalculator::Result makeResult();

        bool m_MajorGCOccurred;
        unsigned int m_MinorGCCounter;
        economem::AdaptiveWeightedAverage m_AvgMinorsPerMajor;
        economem::AdaptiveWeightedAverage m_AvgMinorTime;
        economem::AdaptiveWeightedAverage m_AvgMajorTime;
        economem::Stopwatch m_GCTimer;
        economem::Stopwatch m_InterGCTimer;
        unsigned long long m_TimeSinceLastMajor;
        unsigned long long m_HeapSizeAtBeginCollectionB;
    };
}

/*! @} */

#endif
