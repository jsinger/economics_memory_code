economemd
    total memory: 5948 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub3_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
TimerThread: starting
CommandThread: starting
ReadingThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 838: heap size 9 MB, throughput 0.991328
Clients: 1
Client 838 has a minimum heap size of 276 MB
Reading from 837: heap size 9 MB, throughput 0.992585
Clients: 2
Client 837 has a minimum heap size of 1211 MB
Reading from 838: heap size 9 MB, throughput 0.987873
Reading from 837: heap size 9 MB, throughput 0.987684
Reading from 838: heap size 9 MB, throughput 0.982589
Reading from 837: heap size 9 MB, throughput 0.980449
Reading from 838: heap size 9 MB, throughput 0.972158
Reading from 837: heap size 9 MB, throughput 0.970224
Reading from 837: heap size 11 MB, throughput 0.969447
Reading from 838: heap size 11 MB, throughput 0.981106
Reading from 837: heap size 11 MB, throughput 0.979262
Reading from 838: heap size 11 MB, throughput 0.967534
Reading from 838: heap size 17 MB, throughput 0.953082
Reading from 837: heap size 17 MB, throughput 0.961511
Reading from 838: heap size 17 MB, throughput 0.879345
Reading from 837: heap size 17 MB, throughput 0.872656
Reading from 838: heap size 30 MB, throughput 0.732079
Reading from 837: heap size 30 MB, throughput 0.657966
Reading from 838: heap size 31 MB, throughput 0.494705
Reading from 837: heap size 31 MB, throughput 0.297545
Reading from 838: heap size 35 MB, throughput 0.690413
Reading from 838: heap size 45 MB, throughput 0.260256
Reading from 837: heap size 35 MB, throughput 0.627702
Reading from 838: heap size 51 MB, throughput 0.0228712
Reading from 837: heap size 47 MB, throughput 0.487543
Reading from 837: heap size 50 MB, throughput 0.379296
Reading from 838: heap size 52 MB, throughput 0.412698
Reading from 837: heap size 52 MB, throughput 0.531664
Reading from 838: heap size 72 MB, throughput 0.771559
Reading from 837: heap size 76 MB, throughput 0.321142
Reading from 838: heap size 74 MB, throughput 0.415791
Reading from 837: heap size 77 MB, throughput 0.432287
Reading from 838: heap size 95 MB, throughput 0.761805
Reading from 837: heap size 98 MB, throughput 0.306825
Reading from 838: heap size 99 MB, throughput 0.819896
Reading from 838: heap size 101 MB, throughput 0.776534
Reading from 837: heap size 102 MB, throughput 0.461934
Reading from 838: heap size 103 MB, throughput 0.462642
Reading from 838: heap size 134 MB, throughput 0.718235
Reading from 837: heap size 104 MB, throughput 0.462925
Reading from 838: heap size 136 MB, throughput 0.649918
Reading from 837: heap size 135 MB, throughput 0.562519
Reading from 837: heap size 139 MB, throughput 0.383027
Reading from 838: heap size 140 MB, throughput 0.657656
Reading from 838: heap size 145 MB, throughput 0.681021
Reading from 838: heap size 150 MB, throughput 0.699417
Reading from 838: heap size 154 MB, throughput 0.568779
Reading from 837: heap size 140 MB, throughput 0.361377
Reading from 837: heap size 180 MB, throughput 0.446236
Reading from 837: heap size 182 MB, throughput 0.493568
Reading from 838: heap size 157 MB, throughput 0.414179
Reading from 837: heap size 183 MB, throughput 0.22154
Reading from 838: heap size 195 MB, throughput 0.588314
Reading from 838: heap size 207 MB, throughput 0.646888
Reading from 837: heap size 188 MB, throughput 0.452391
Reading from 837: heap size 237 MB, throughput 0.348932
Reading from 838: heap size 208 MB, throughput 0.836775
Reading from 837: heap size 238 MB, throughput 0.396935
Reading from 837: heap size 239 MB, throughput 0.747946
Reading from 837: heap size 244 MB, throughput 0.686776
Reading from 838: heap size 208 MB, throughput 0.84165
Reading from 838: heap size 216 MB, throughput 0.755932
Reading from 837: heap size 255 MB, throughput 0.652153
Reading from 838: heap size 222 MB, throughput 0.686818
Reading from 837: heap size 259 MB, throughput 0.662495
Reading from 837: heap size 263 MB, throughput 0.409696
Reading from 838: heap size 227 MB, throughput 0.530978
Reading from 838: heap size 271 MB, throughput 0.575457
Reading from 837: heap size 318 MB, throughput 0.578796
Reading from 838: heap size 272 MB, throughput 0.684399
Reading from 837: heap size 324 MB, throughput 0.611444
Reading from 838: heap size 266 MB, throughput 0.762425
Reading from 838: heap size 269 MB, throughput 0.747056
Reading from 837: heap size 330 MB, throughput 0.582194
Reading from 837: heap size 334 MB, throughput 0.58634
Reading from 838: heap size 267 MB, throughput 0.776163
Reading from 838: heap size 269 MB, throughput 0.667244
Reading from 837: heap size 346 MB, throughput 0.566907
Reading from 838: heap size 274 MB, throughput 0.692453
Reading from 837: heap size 355 MB, throughput 0.574312
Reading from 838: heap size 275 MB, throughput 0.714342
Reading from 838: heap size 270 MB, throughput 0.514754
Reading from 837: heap size 364 MB, throughput 0.356907
Reading from 837: heap size 421 MB, throughput 0.538704
Reading from 838: heap size 313 MB, throughput 0.886311
Reading from 837: heap size 426 MB, throughput 0.593954
Reading from 838: heap size 313 MB, throughput 0.871903
Reading from 837: heap size 427 MB, throughput 0.62972
Reading from 838: heap size 315 MB, throughput 0.843125
Reading from 838: heap size 318 MB, throughput 0.809169
Reading from 837: heap size 427 MB, throughput 0.603731
Reading from 838: heap size 319 MB, throughput 0.752762
Reading from 837: heap size 430 MB, throughput 0.571267
Reading from 837: heap size 435 MB, throughput 0.535931
Reading from 838: heap size 322 MB, throughput 0.840281
Reading from 838: heap size 323 MB, throughput 0.795506
Reading from 838: heap size 325 MB, throughput 0.796299
Reading from 838: heap size 327 MB, throughput 0.640361
Reading from 838: heap size 334 MB, throughput 0.613084
Reading from 838: heap size 335 MB, throughput 0.620769
Reading from 838: heap size 341 MB, throughput 0.661954
Reading from 837: heap size 442 MB, throughput 0.316952
Numeric result:
Recommendation: 2 clients, utility 0.609048:
    h1: 4737 MB (U(h) = 0.407266*h^0.0855592)
    h2: 1211 MB (U(h) = 0.719809*h^0.001)
Recommendation: 2 clients, utility 0.609048:
    h1: 4737 MB (U(h) = 0.407266*h^0.0855592)
    h2: 1211 MB (U(h) = 0.719809*h^0.001)
Reading from 837: heap size 502 MB, throughput 0.436732
Reading from 837: heap size 491 MB, throughput 0.504204
Reading from 837: heap size 499 MB, throughput 0.513763
Reading from 838: heap size 343 MB, throughput 0.936664
Reading from 837: heap size 492 MB, throughput 0.198687
Reading from 837: heap size 554 MB, throughput 0.508309
Reading from 837: heap size 546 MB, throughput 0.552294
Reading from 837: heap size 552 MB, throughput 0.563718
Reading from 837: heap size 553 MB, throughput 0.564504
Reading from 838: heap size 351 MB, throughput 0.968593
Reading from 837: heap size 556 MB, throughput 0.588064
Reading from 837: heap size 558 MB, throughput 0.579066
Reading from 837: heap size 571 MB, throughput 0.553982
Reading from 837: heap size 578 MB, throughput 0.535261
Reading from 837: heap size 591 MB, throughput 0.531738
Reading from 837: heap size 599 MB, throughput 0.514972
Reading from 838: heap size 352 MB, throughput 0.968156
Reading from 837: heap size 608 MB, throughput 0.373182
Reading from 837: heap size 672 MB, throughput 0.446108
Reading from 837: heap size 679 MB, throughput 0.61561
Reading from 837: heap size 679 MB, throughput 0.786348
Reading from 837: heap size 681 MB, throughput 0.784076
Reading from 838: heap size 357 MB, throughput 0.859448
Reading from 837: heap size 681 MB, throughput 0.811705
Reading from 838: heap size 403 MB, throughput 0.9636
Reading from 837: heap size 688 MB, throughput 0.53048
Reading from 837: heap size 761 MB, throughput 0.676559
Reading from 837: heap size 681 MB, throughput 0.649947
Reading from 837: heap size 748 MB, throughput 0.643187
Reading from 838: heap size 408 MB, throughput 0.974049
Numeric result:
Recommendation: 2 clients, utility 0.598233:
    h1: 4737 MB (U(h) = 0.350924*h^0.122606)
    h2: 1211 MB (U(h) = 0.599703*h^0.001)
Recommendation: 2 clients, utility 0.598233:
    h1: 4737 MB (U(h) = 0.350924*h^0.122606)
    h2: 1211 MB (U(h) = 0.599703*h^0.001)
Reading from 837: heap size 666 MB, throughput 0.267124
Reading from 837: heap size 821 MB, throughput 0.445448
Reading from 837: heap size 824 MB, throughput 0.455446
Reading from 837: heap size 829 MB, throughput 0.469851
Reading from 837: heap size 829 MB, throughput 0.795142
Reading from 837: heap size 829 MB, throughput 0.724017
Reading from 837: heap size 832 MB, throughput 0.705079
Reading from 837: heap size 830 MB, throughput 0.665087
Reading from 838: heap size 410 MB, throughput 0.97473
Reading from 837: heap size 833 MB, throughput 0.376801
Reading from 837: heap size 916 MB, throughput 0.661579
Reading from 837: heap size 916 MB, throughput 0.770946
Reading from 837: heap size 922 MB, throughput 0.792316
Reading from 838: heap size 415 MB, throughput 0.980428
Reading from 837: heap size 926 MB, throughput 0.892351
Reading from 837: heap size 927 MB, throughput 0.896168
Reading from 837: heap size 930 MB, throughput 0.925837
Reading from 837: heap size 910 MB, throughput 0.887663
Reading from 837: heap size 782 MB, throughput 0.838313
Reading from 837: heap size 894 MB, throughput 0.813817
Reading from 837: heap size 787 MB, throughput 0.789118
Reading from 838: heap size 416 MB, throughput 0.980965
Reading from 837: heap size 883 MB, throughput 0.812067
Reading from 837: heap size 791 MB, throughput 0.799392
Reading from 837: heap size 874 MB, throughput 0.80258
Reading from 837: heap size 882 MB, throughput 0.798512
Reading from 837: heap size 870 MB, throughput 0.800879
Reading from 837: heap size 876 MB, throughput 0.811408
Reading from 837: heap size 866 MB, throughput 0.823914
Reading from 838: heap size 414 MB, throughput 0.982502
Reading from 837: heap size 872 MB, throughput 0.962992
Reading from 838: heap size 416 MB, throughput 0.972165
Reading from 837: heap size 864 MB, throughput 0.925488
Reading from 837: heap size 870 MB, throughput 0.893871
Reading from 837: heap size 877 MB, throughput 0.8628
Reading from 837: heap size 878 MB, throughput 0.848021
Reading from 837: heap size 886 MB, throughput 0.819043
Reading from 837: heap size 886 MB, throughput 0.81312
Reading from 837: heap size 893 MB, throughput 0.811479
Reading from 837: heap size 894 MB, throughput 0.812756
Reading from 838: heap size 411 MB, throughput 0.976199
Reading from 837: heap size 900 MB, throughput 0.864656
Reading from 837: heap size 901 MB, throughput 0.872199
Numeric result:
Recommendation: 2 clients, utility 0.708733:
    h1: 3890.41 MB (U(h) = 0.316519*h^0.147542)
    h2: 2057.59 MB (U(h) = 0.364501*h^0.0780811)
Recommendation: 2 clients, utility 0.708733:
    h1: 3889.58 MB (U(h) = 0.316519*h^0.147542)
    h2: 2058.42 MB (U(h) = 0.364501*h^0.0780811)
Reading from 837: heap size 900 MB, throughput 0.89035
Reading from 837: heap size 904 MB, throughput 0.842869
Reading from 837: heap size 896 MB, throughput 0.773207
Reading from 838: heap size 414 MB, throughput 0.966393
Reading from 837: heap size 911 MB, throughput 0.698338
Reading from 838: heap size 412 MB, throughput 0.974151
Reading from 838: heap size 416 MB, throughput 0.958034
Reading from 837: heap size 1048 MB, throughput 0.603678
Reading from 838: heap size 412 MB, throughput 0.938427
Reading from 837: heap size 1051 MB, throughput 0.644561
Reading from 838: heap size 418 MB, throughput 0.909779
Reading from 837: heap size 1057 MB, throughput 0.677863
Reading from 837: heap size 1058 MB, throughput 0.690919
Reading from 838: heap size 427 MB, throughput 0.935878
Reading from 837: heap size 1072 MB, throughput 0.685471
Reading from 837: heap size 1072 MB, throughput 0.68109
Reading from 837: heap size 1088 MB, throughput 0.715379
Reading from 837: heap size 1089 MB, throughput 0.705457
Reading from 838: heap size 431 MB, throughput 0.964885
Reading from 837: heap size 1107 MB, throughput 0.906057
Reading from 838: heap size 428 MB, throughput 0.978271
Reading from 837: heap size 1110 MB, throughput 0.938803
Reading from 838: heap size 432 MB, throughput 0.981328
Reading from 837: heap size 1124 MB, throughput 0.944178
Numeric result:
Recommendation: 2 clients, utility 1.03784:
    h1: 2364.56 MB (U(h) = 0.292896*h^0.166147)
    h2: 3583.44 MB (U(h) = 0.124169*h^0.251769)
Recommendation: 2 clients, utility 1.03784:
    h1: 2364.69 MB (U(h) = 0.292896*h^0.166147)
    h2: 3583.31 MB (U(h) = 0.124169*h^0.251769)
Reading from 838: heap size 429 MB, throughput 0.982377
Reading from 837: heap size 1127 MB, throughput 0.948569
Reading from 838: heap size 432 MB, throughput 0.982126
Reading from 837: heap size 1130 MB, throughput 0.951321
Reading from 838: heap size 430 MB, throughput 0.981857
Reading from 837: heap size 1135 MB, throughput 0.951447
Reading from 838: heap size 432 MB, throughput 0.976307
Reading from 837: heap size 1130 MB, throughput 0.95203
Reading from 838: heap size 433 MB, throughput 0.979461
Reading from 837: heap size 1136 MB, throughput 0.95371
Reading from 838: heap size 434 MB, throughput 0.980863
Numeric result:
Recommendation: 2 clients, utility 1.2248:
    h1: 2130.01 MB (U(h) = 0.279798*h^0.177074)
    h2: 3817.99 MB (U(h) = 0.0822175*h^0.317404)
Recommendation: 2 clients, utility 1.2248:
    h1: 2129.99 MB (U(h) = 0.279798*h^0.177074)
    h2: 3818.01 MB (U(h) = 0.0822175*h^0.317404)
Reading from 837: heap size 1137 MB, throughput 0.956424
Reading from 838: heap size 436 MB, throughput 0.980984
Reading from 837: heap size 1139 MB, throughput 0.951571
Reading from 838: heap size 436 MB, throughput 0.983233
Reading from 838: heap size 441 MB, throughput 0.974657
Reading from 838: heap size 441 MB, throughput 0.959532
Reading from 838: heap size 447 MB, throughput 0.948059
Reading from 837: heap size 1147 MB, throughput 0.955587
Reading from 838: heap size 447 MB, throughput 0.982076
Reading from 837: heap size 1148 MB, throughput 0.951408
Reading from 838: heap size 452 MB, throughput 0.98593
Reading from 837: heap size 1155 MB, throughput 0.955005
Numeric result:
Recommendation: 2 clients, utility 1.46571:
    h1: 1890.29 MB (U(h) = 0.269779*h^0.185708)
    h2: 4057.71 MB (U(h) = 0.048781*h^0.398615)
Recommendation: 2 clients, utility 1.46571:
    h1: 1890.38 MB (U(h) = 0.269779*h^0.185708)
    h2: 4057.62 MB (U(h) = 0.048781*h^0.398615)
Reading from 838: heap size 454 MB, throughput 0.98787
Reading from 837: heap size 1158 MB, throughput 0.959732
Reading from 837: heap size 1165 MB, throughput 0.955956
Reading from 838: heap size 455 MB, throughput 0.987973
Reading from 837: heap size 1170 MB, throughput 0.957348
Reading from 838: heap size 457 MB, throughput 0.987849
Reading from 837: heap size 1177 MB, throughput 0.953821
Reading from 838: heap size 454 MB, throughput 0.986531
Reading from 837: heap size 1182 MB, throughput 0.949644
Reading from 838: heap size 457 MB, throughput 0.985284
Numeric result:
Recommendation: 2 clients, utility 1.55435:
    h1: 1875.86 MB (U(h) = 0.263067*h^0.191625)
    h2: 4072.14 MB (U(h) = 0.0439209*h^0.415972)
Recommendation: 2 clients, utility 1.55435:
    h1: 1875.89 MB (U(h) = 0.263067*h^0.191625)
    h2: 4072.11 MB (U(h) = 0.0439209*h^0.415972)
Reading from 837: heap size 1190 MB, throughput 0.949238
Reading from 838: heap size 457 MB, throughput 0.985214
Reading from 837: heap size 1193 MB, throughput 0.949295
Reading from 838: heap size 458 MB, throughput 0.988771
Reading from 838: heap size 459 MB, throughput 0.983219
Reading from 838: heap size 460 MB, throughput 0.971369
Reading from 838: heap size 465 MB, throughput 0.961107
Reading from 837: heap size 1201 MB, throughput 0.951712
Reading from 838: heap size 466 MB, throughput 0.985153
Reading from 837: heap size 1203 MB, throughput 0.954043
Reading from 838: heap size 472 MB, throughput 0.989004
Numeric result:
Recommendation: 2 clients, utility 1.72388:
    h1: 1811.24 MB (U(h) = 0.252959*h^0.200776)
    h2: 4136.76 MB (U(h) = 0.0331927*h^0.458515)
Recommendation: 2 clients, utility 1.72388:
    h1: 1811.36 MB (U(h) = 0.252959*h^0.200776)
    h2: 4136.64 MB (U(h) = 0.0331927*h^0.458515)
Reading from 837: heap size 1211 MB, throughput 0.908398
Reading from 838: heap size 473 MB, throughput 0.990305
Reading from 837: heap size 1306 MB, throughput 0.947466
Reading from 838: heap size 473 MB, throughput 0.989048
Reading from 837: heap size 1309 MB, throughput 0.961019
Reading from 838: heap size 475 MB, throughput 0.988135
Reading from 837: heap size 1315 MB, throughput 0.967585
Reading from 838: heap size 475 MB, throughput 0.987459
Numeric result:
Recommendation: 2 clients, utility 2.09195:
    h1: 1556.85 MB (U(h) = 0.249974*h^0.203526)
    h2: 4391.15 MB (U(h) = 0.0152012*h^0.574064)
Recommendation: 2 clients, utility 2.09195:
    h1: 1556.83 MB (U(h) = 0.249974*h^0.203526)
    h2: 4391.17 MB (U(h) = 0.0152012*h^0.574064)
Reading from 837: heap size 1324 MB, throughput 0.970358
Reading from 838: heap size 476 MB, throughput 0.986134
Reading from 837: heap size 1324 MB, throughput 0.97093
Reading from 838: heap size 479 MB, throughput 0.988394
Reading from 838: heap size 480 MB, throughput 0.983356
Reading from 838: heap size 480 MB, throughput 0.973803
Reading from 838: heap size 482 MB, throughput 0.974723
Reading from 837: heap size 1320 MB, throughput 0.972498
Reading from 838: heap size 490 MB, throughput 0.98853
Reading from 837: heap size 1324 MB, throughput 0.969227
Numeric result:
Recommendation: 2 clients, utility 2.30701:
    h1: 1490.92 MB (U(h) = 0.243898*h^0.209199)
    h2: 4457.08 MB (U(h) = 0.0107117*h^0.625395)
Recommendation: 2 clients, utility 2.30701:
    h1: 1490.92 MB (U(h) = 0.243898*h^0.209199)
    h2: 4457.08 MB (U(h) = 0.0107117*h^0.625395)
Reading from 838: heap size 490 MB, throughput 0.988936
Reading from 837: heap size 1313 MB, throughput 0.968
Reading from 838: heap size 492 MB, throughput 0.989684
Reading from 837: heap size 1319 MB, throughput 0.965675
Reading from 838: heap size 494 MB, throughput 0.988909
Reading from 837: heap size 1316 MB, throughput 0.964708
Reading from 838: heap size 492 MB, throughput 0.987913
Reading from 837: heap size 1318 MB, throughput 0.964379
Numeric result:
Recommendation: 2 clients, utility 2.77576:
    h1: 1320.66 MB (U(h) = 0.241705*h^0.211264)
    h2: 4627.34 MB (U(h) = 0.00487134*h^0.740217)
Recommendation: 2 clients, utility 2.77576:
    h1: 1320.68 MB (U(h) = 0.241705*h^0.211264)
    h2: 4627.32 MB (U(h) = 0.00487134*h^0.740217)
Reading from 838: heap size 494 MB, throughput 0.988117
Reading from 837: heap size 1323 MB, throughput 0.962011
Reading from 838: heap size 496 MB, throughput 0.990199
Reading from 837: heap size 1326 MB, throughput 0.960461
Reading from 838: heap size 497 MB, throughput 0.986482
Reading from 838: heap size 496 MB, throughput 0.977506
Reading from 838: heap size 499 MB, throughput 0.976322
Reading from 837: heap size 1332 MB, throughput 0.960145
Reading from 838: heap size 506 MB, throughput 0.988226
Numeric result:
Recommendation: 2 clients, utility 2.9303:
    h1: 1296.09 MB (U(h) = 0.238059*h^0.214723)
    h2: 4651.91 MB (U(h) = 0.00393836*h^0.770677)
Recommendation: 2 clients, utility 2.9303:
    h1: 1296.09 MB (U(h) = 0.238059*h^0.214723)
    h2: 4651.91 MB (U(h) = 0.00393836*h^0.770677)
Reading from 837: heap size 1338 MB, throughput 0.958517
Reading from 838: heap size 507 MB, throughput 0.989865
Reading from 837: heap size 1346 MB, throughput 0.959478
Reading from 838: heap size 508 MB, throughput 0.990527
Reading from 837: heap size 1354 MB, throughput 0.956133
Reading from 838: heap size 510 MB, throughput 0.989521
Numeric result:
Recommendation: 2 clients, utility 3.28488:
    h1: 1218.1 MB (U(h) = 0.235521*h^0.217147)
    h2: 4729.9 MB (U(h) = 0.00237633*h^0.843173)
Recommendation: 2 clients, utility 3.28488:
    h1: 1218.12 MB (U(h) = 0.235521*h^0.217147)
    h2: 4729.88 MB (U(h) = 0.00237633*h^0.843173)
Reading from 838: heap size 509 MB, throughput 0.989187
Reading from 838: heap size 511 MB, throughput 0.987711
Reading from 838: heap size 513 MB, throughput 0.989408
Reading from 838: heap size 514 MB, throughput 0.982563
Reading from 838: heap size 514 MB, throughput 0.975779
Reading from 838: heap size 517 MB, throughput 0.983696
Numeric result:
Recommendation: 2 clients, utility 3.90993:
    h1: 1679.86 MB (U(h) = 0.121631*h^0.331847)
    h2: 4268.14 MB (U(h) = 0.00237633*h^0.843173)
Recommendation: 2 clients, utility 3.90993:
    h1: 1679.82 MB (U(h) = 0.121631*h^0.331847)
    h2: 4268.18 MB (U(h) = 0.00237633*h^0.843173)
Reading from 837: heap size 1364 MB, throughput 0.928097
Reading from 838: heap size 521 MB, throughput 0.988778
Reading from 838: heap size 522 MB, throughput 0.990018
Reading from 838: heap size 521 MB, throughput 0.989351
Numeric result:
Recommendation: 2 clients, utility 4.50262:
    h1: 2080.46 MB (U(h) = 0.0649763*h^0.438918)
    h2: 3867.54 MB (U(h) = 0.00286662*h^0.815886)
Recommendation: 2 clients, utility 4.50262:
    h1: 2080.55 MB (U(h) = 0.0649763*h^0.438918)
    h2: 3867.45 MB (U(h) = 0.00286662*h^0.815886)
Reading from 838: heap size 524 MB, throughput 0.989261
Reading from 837: heap size 1489 MB, throughput 0.983251
Reading from 838: heap size 526 MB, throughput 0.988586
Reading from 838: heap size 527 MB, throughput 0.987836
Reading from 838: heap size 529 MB, throughput 0.981631
Reading from 838: heap size 532 MB, throughput 0.98273
Reading from 837: heap size 1526 MB, throughput 0.983579
Numeric result:
Recommendation: 2 clients, utility 3.7194:
    h1: 1729.78 MB (U(h) = 0.123969*h^0.330476)
    h2: 4218.22 MB (U(h) = 0.00306103*h^0.805823)
Recommendation: 2 clients, utility 3.7194:
    h1: 1729.89 MB (U(h) = 0.123969*h^0.330476)
    h2: 4218.11 MB (U(h) = 0.00306103*h^0.805823)
Reading from 837: heap size 1532 MB, throughput 0.970458
Reading from 838: heap size 538 MB, throughput 0.989494
Reading from 837: heap size 1521 MB, throughput 0.951356
Reading from 837: heap size 1553 MB, throughput 0.922589
Reading from 837: heap size 1581 MB, throughput 0.892003
Reading from 837: heap size 1597 MB, throughput 0.83726
Reading from 837: heap size 1629 MB, throughput 0.808507
Reading from 838: heap size 539 MB, throughput 0.989135
Reading from 837: heap size 1638 MB, throughput 0.924928
Reading from 837: heap size 1650 MB, throughput 0.95366
Reading from 838: heap size 540 MB, throughput 0.99023
Reading from 837: heap size 1664 MB, throughput 0.960746
Numeric result:
Recommendation: 2 clients, utility 3.17069:
    h1: 2149.74 MB (U(h) = 0.0967425*h^0.371767)
    h2: 3798.26 MB (U(h) = 0.00842254*h^0.656842)
Recommendation: 2 clients, utility 3.17069:
    h1: 2149.77 MB (U(h) = 0.0967425*h^0.371767)
    h2: 3798.23 MB (U(h) = 0.00842254*h^0.656842)
Reading from 838: heap size 542 MB, throughput 0.989924
Reading from 837: heap size 1655 MB, throughput 0.967256
Reading from 838: heap size 542 MB, throughput 0.989214
Reading from 838: heap size 543 MB, throughput 0.987968
Reading from 837: heap size 1669 MB, throughput 0.963815
Reading from 838: heap size 546 MB, throughput 0.9821
Reading from 838: heap size 546 MB, throughput 0.980371
Reading from 837: heap size 1656 MB, throughput 0.962798
Numeric result:
Recommendation: 2 clients, utility 3.33626:
    h1: 2460.54 MB (U(h) = 0.0666101*h^0.433517)
    h2: 3487.46 MB (U(h) = 0.0112945*h^0.614476)
Recommendation: 2 clients, utility 3.33626:
    h1: 2460.47 MB (U(h) = 0.0666101*h^0.433517)
    h2: 3487.53 MB (U(h) = 0.0112945*h^0.614476)
Reading from 838: heap size 554 MB, throughput 0.989303
Reading from 837: heap size 1669 MB, throughput 0.962188
Reading from 838: heap size 555 MB, throughput 0.991111
Reading from 837: heap size 1662 MB, throughput 0.94159
Reading from 838: heap size 555 MB, throughput 0.990974
Numeric result:
Recommendation: 2 clients, utility 3.3534:
    h1: 2699.52 MB (U(h) = 0.0540118*h^0.467889)
    h2: 3248.48 MB (U(h) = 0.0162318*h^0.563027)
Recommendation: 2 clients, utility 3.3534:
    h1: 2699.54 MB (U(h) = 0.0540118*h^0.467889)
    h2: 3248.46 MB (U(h) = 0.0162318*h^0.563027)
Reading from 838: heap size 557 MB, throughput 0.989786
Reading from 837: heap size 1626 MB, throughput 0.977356
Reading from 838: heap size 559 MB, throughput 0.989358
Reading from 837: heap size 1632 MB, throughput 0.982534
Reading from 838: heap size 559 MB, throughput 0.989604
Reading from 838: heap size 561 MB, throughput 0.984161
Reading from 837: heap size 1645 MB, throughput 0.983166
Reading from 838: heap size 564 MB, throughput 0.985559
Numeric result:
Recommendation: 2 clients, utility 3.62561:
    h1: 3044.83 MB (U(h) = 0.0352947*h^0.537274)
    h2: 2903.17 MB (U(h) = 0.0232393*h^0.51224)
Recommendation: 2 clients, utility 3.62561:
    h1: 3044.94 MB (U(h) = 0.0352947*h^0.537274)
    h2: 2903.06 MB (U(h) = 0.0232393*h^0.51224)
Reading from 838: heap size 570 MB, throughput 0.99021
Reading from 837: heap size 1663 MB, throughput 0.983102
Reading from 838: heap size 571 MB, throughput 0.991255
Reading from 837: heap size 1665 MB, throughput 0.981366
Reading from 838: heap size 570 MB, throughput 0.991023
Reading from 837: heap size 1656 MB, throughput 0.97981
Numeric result:
Recommendation: 2 clients, utility 3.31109:
    h1: 2912.34 MB (U(h) = 0.0466278*h^0.491347)
    h2: 3035.66 MB (U(h) = 0.0232165*h^0.512135)
Recommendation: 2 clients, utility 3.31109:
    h1: 2912.39 MB (U(h) = 0.0466278*h^0.491347)
    h2: 3035.61 MB (U(h) = 0.0232165*h^0.512135)
Reading from 838: heap size 573 MB, throughput 0.9908
Reading from 837: heap size 1665 MB, throughput 0.978984
Reading from 838: heap size 575 MB, throughput 0.992695
Reading from 838: heap size 575 MB, throughput 0.98837
Reading from 838: heap size 576 MB, throughput 0.984537
Reading from 837: heap size 1641 MB, throughput 0.975414
Numeric result:
Recommendation: 2 clients, utility 3.55433:
    h1: 3064.53 MB (U(h) = 0.0354974*h^0.535275)
    h2: 2883.47 MB (U(h) = 0.0246419*h^0.50369)
Recommendation: 2 clients, utility 3.55433:
    h1: 3064.41 MB (U(h) = 0.0354974*h^0.535275)
    h2: 2883.59 MB (U(h) = 0.0246419*h^0.50369)
Reading from 838: heap size 578 MB, throughput 0.9907
Reading from 837: heap size 1514 MB, throughput 0.973335
Reading from 838: heap size 581 MB, throughput 0.991769
Reading from 837: heap size 1637 MB, throughput 0.972333
Reading from 838: heap size 582 MB, throughput 0.991675
Reading from 837: heap size 1645 MB, throughput 0.970837
Numeric result:
Recommendation: 2 clients, utility 2.91174:
    h1: 3326.97 MB (U(h) = 0.0492638*h^0.481669)
    h2: 2621.03 MB (U(h) = 0.0599898*h^0.379435)
Recommendation: 2 clients, utility 2.91174:
    h1: 3327.09 MB (U(h) = 0.0492638*h^0.481669)
    h2: 2620.91 MB (U(h) = 0.0599898*h^0.379435)
Reading from 838: heap size 581 MB, throughput 0.99089
Reading from 837: heap size 1654 MB, throughput 0.968918
Reading from 838: heap size 583 MB, throughput 0.992819
Reading from 838: heap size 584 MB, throughput 0.98969
Reading from 838: heap size 585 MB, throughput 0.985353
Reading from 837: heap size 1656 MB, throughput 0.967104
Numeric result:
Recommendation: 2 clients, utility 2.804:
    h1: 3448.53 MB (U(h) = 0.0510663*h^0.47538)
    h2: 2499.47 MB (U(h) = 0.077124*h^0.344552)
Recommendation: 2 clients, utility 2.804:
    h1: 3448.53 MB (U(h) = 0.0510663*h^0.47538)
    h2: 2499.47 MB (U(h) = 0.077124*h^0.344552)
Reading from 838: heap size 592 MB, throughput 0.991538
Reading from 837: heap size 1668 MB, throughput 0.965089
Reading from 838: heap size 592 MB, throughput 0.992515
Reading from 837: heap size 1676 MB, throughput 0.962223
Reading from 838: heap size 591 MB, throughput 0.991896
Numeric result:
Recommendation: 2 clients, utility 2.64487:
    h1: 3464.01 MB (U(h) = 0.0585553*h^0.452994)
    h2: 2483.99 MB (U(h) = 0.0888385*h^0.324825)
Recommendation: 2 clients, utility 2.64487:
    h1: 3464.05 MB (U(h) = 0.0585553*h^0.452994)
    h2: 2483.95 MB (U(h) = 0.0888385*h^0.324825)
Reading from 837: heap size 1691 MB, throughput 0.961082
Reading from 838: heap size 593 MB, throughput 0.991316
Reading from 837: heap size 1704 MB, throughput 0.958722
Reading from 838: heap size 596 MB, throughput 0.991342
Reading from 838: heap size 597 MB, throughput 0.987473
Reading from 837: heap size 1725 MB, throughput 0.958591
Reading from 838: heap size 599 MB, throughput 0.985977
Numeric result:
Recommendation: 2 clients, utility 2.18937:
    h1: 3585.34 MB (U(h) = 0.0916696*h^0.380179)
    h2: 2362.66 MB (U(h) = 0.151934*h^0.250513)
Recommendation: 2 clients, utility 2.18937:
    h1: 3585.43 MB (U(h) = 0.0916696*h^0.380179)
    h2: 2362.57 MB (U(h) = 0.151934*h^0.250513)
Reading from 838: heap size 601 MB, throughput 0.990873
Reading from 838: heap size 603 MB, throughput 0.991623
Numeric result:
Recommendation: 2 clients, utility 2.09168:
    h1: 3497.02 MB (U(h) = 0.105521*h^0.357374)
    h2: 2450.98 MB (U(h) = 0.151934*h^0.250513)
Recommendation: 2 clients, utility 2.09168:
    h1: 3496.8 MB (U(h) = 0.105521*h^0.357374)
    h2: 2451.2 MB (U(h) = 0.151934*h^0.250513)
Reading from 838: heap size 604 MB, throughput 0.991674
Reading from 838: heap size 605 MB, throughput 0.990829
Reading from 838: heap size 605 MB, throughput 0.991784
Reading from 838: heap size 608 MB, throughput 0.986773
Client 838 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 837: heap size 1733 MB, throughput 0.989838
Reading from 837: heap size 1755 MB, throughput 0.988524
Recommendation: one client; give it all the memory
Reading from 837: heap size 1760 MB, throughput 0.978957
Reading from 837: heap size 1750 MB, throughput 0.964788
Reading from 837: heap size 1775 MB, throughput 0.940473
Client 837 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
