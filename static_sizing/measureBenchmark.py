#!/usr/bin/env python
# Measure the average number of completed iterations in a fixed time for a single benchmark/heap size combination
# For internal use; generally you shouldn't call this directly

import argparse
import argtypes
import os
import os.path
import shutil
import staticutility_core as su
import subprocess
import tempfile
from time import sleep

def measureBenchmark(runs, outDir, resultsFile, bid, heap, maxHeap):
    pointID = outDir.pointID(bid, heap, maxHeap)
    seconds = su.timeToSeconds(bid.time)

    class MeasureBenchmark:
        """A single benchmark within a measure run"""

        def __init__(self, bid, heap, maxHeap):
            self.b = bid
            self.heap = int(heap)
            self.maxHeap = int(maxHeap)

        def rawFile(self, run):
            return outDir.pointRawFile(pointID, run, runs)

        def iterationsFile(self):
            return outDir.pointIterationsFile(pointID)

        def start(self, run):
            self.scratch = os.path.realpath(tempfile.mkdtemp())
            with open(self.rawFile(run), "w") as f, open(os.devnull) as devnull:
                self.p = subprocess.Popen(["java", "-Xms%dm" % self.heap, "-Xmx%dm" % self.heap, "-jar", su.dacapo(), "--scratch-directory", self.scratch, "-t", self.b.threads, "-s", self.b.inputSize, "-n", "1000000", self.b.name], stdin=devnull, stdout=f, stderr=subprocess.STDOUT)

        def kill(self):
            self.p.send_signal(15)

        def wait(self):
            self.p.wait()
            shutil.rmtree(self.scratch)
            self.p = None
            self.scratch = None

        def addToIterations(self, run):
            with open(self.iterationsFile(), "a") as f:
                f.write("%d\n" % su.benchmarkIterations(self.rawFile(run)))

        def calculateResults(self):
            l = su.fileToList(self.iterationsFile(), float)
            self.average = su.average(l)
            self.stddev = su.stddev(l)
            self.itersPerSec = self.average / float(seconds)
            self.stddevItersPerSec = self.stddev / float(seconds)
            self.median = su.median(l)
            self.medianItersPerSec = self.median / float(seconds)


    b = MeasureBenchmark(bid, heap, maxHeap)

    for i in range(runs):
        print "%s, heap: %s MB, time: %s, input: %s, threads: %s, run %d of %d" % (bid.name, outDir.prettyHeap(heap, maxHeap), bid.time, bid.inputSize, bid.threads, i + 1, runs)

        b.start(i)
        sleep(seconds)
        b.kill()
        b.wait()
        b.addToIterations(i)

    b.calculateResults()
    result = su.MeasureResult(b.heap, b.average, b.stddev, b.itersPerSec, b.stddevItersPerSec, b.median, b.medianItersPerSec)
    result.append(resultsFile)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Measure the average number of completed iterations in a fixed time for a single benchmark/heap size combination")

    parser.add_argument("benchmark", help="name of the benchmark to run")
    parser.add_argument("inputSize", help="benchmark-specific input size")
    parser.add_argument("threads", type=argtypes.positive_int, help="how many threads to run")
    parser.add_argument("heapSize", type=argtypes.positive_int, help="in MB")
    parser.add_argument("time", help="how long to run for, in a format suitable for input to 'sleep'")
    parser.add_argument("maxHeap", type=argtypes.positive_int, help="biggest heap size used in any run; for pretty-printing")
    parser.add_argument("runs", type=argtypes.positive_int, help="how many times to run the measurement, for taking averages")
    parser.add_argument("outDir", type=argtypes.existing_absolute_dir, help="where to save the output")
    parser.add_argument("resultsFile", type=argtypes.absolute_path, help="running log file for all the results")

    args = parser.parse_args()
    outDir = su.MeasureDir(args.outDir)
    bid = su.BenchmarkId(args.benchmark, args.time, args.inputSize, args.threads)

    measureBenchmark(args.runs, outDir, args.resultsFile, bid, args.heapSize, args.maxHeap)
