#!/usr/bin/env python
# Compare the results of analytical and numerical predictions of the best partition,
# for all existing utility space measurement sets. Results on stdout.

import staticutility as su
import utilitymodel as u

def printAndLog(s, *fs):
    print s
    log(s, *fs)

def log(s, *fs):
    for f in fs:
        f.write(s + "\n")
        f.flush()

model = "root"
m = u.models[model]

with open("numericalOptimisationCheck.txt", "w") as f:

    def pretty(name, (hs, U)):
        printAndLog(name + ": " + str(map(lambda h: int(round(h)), hs)) + ", " + str(U), f)

    for d in su.allUtilitySpaceDirs():
        bids = d.parseBenchmarksFile()

        if len(bids) == 2:
            params = map(lambda b: m.loadParams(d.paramsFile(b, model)), bids)
            minHeaps = map(lambda b: b.minHeap(), bids)
            totalMem = d.totalMem()

            printAndLog(", ".join(map(lambda b: b.pretty(), bids)), f)

            pretty("Analytical", m.calculateBestAnalytic(totalMem, params[0], params[1], minHeaps[0], minHeaps[1]))

            pretty("Numerical", m.calculateBestNumeric(totalMem, params, minHeaps))

            pretty("Old numerical", m.calculateBestOld(totalMem, params[0], params[1], minHeaps[0], minHeaps[1]))

            printAndLog("", f)
