#ifndef RECVSOCK_HPP
#define RECVSOCK_HPP

#include <sys/types.h>

/*! \addtogroup common
  @{
*/

namespace economem
{
//! Base class for receiving messages.
/*! This class uses a named Unix socket to receive on. To use, subclass or instantiate RecvSock instead of using this class directly (unless you want to be able to receive variable length messages). */
    class RecvSockBase
    {
    public:
        virtual ~RecvSockBase();

        //! Interrupt any blocked receive, without using signals.
        /*! This is safe to use when a receiver socket is part of a library linked into another application, and you don't want to interfere with that application's use of signals. */
        void interrupt();

    protected:
        RecvSockBase(char* name);

        //! Blocking receive.
        /*! Returns true if something of the correct length is received; returns false if something of the wrong length is received, or it is interruped by a signal or by interrupt(). */
        bool recvBuf(void* buf, ssize_t len);

    private:

        int m_Socket;
        char* m_SockName;

        int m_InterruptSendSock;
        int m_InterruptRecvSock;
    };

//! Base class for statically-typed fixed-size message receiving.
    template <class T> class RecvSock : public RecvSockBase
    {
    public:
        RecvSock(char* name) : RecvSockBase(name) {}

        //! Blocking receive.
        /*! Returns true if something of the correct length is received; returns false if something of the wrong length is received, or it is interruped by a signal or by interrupt(). */
        bool recv(T& buf)
        {
            return recvBuf(&buf, sizeof(buf));
        }
    };
}

/*! @} */

#endif
