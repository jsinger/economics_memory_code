#!/bin/bash
# Set up everything necessary for an isoquant run

function usage()
{
    echo "Usage: $(basename "${0}") b1 b2 total_mem iterations runs out_dir"
    exit 1
}

test -z "${1}" && usage
test -z "${2}" && usage
test -z "${3}" && usage
test -z "${4}" && usage
test -z "${5}" && usage
test -z "${6}" && usage

B1="${1}"
B2="${2}"
TOTAL="${3}"
ITERATIONS="${4}"
RUNS="${5}"
OUTDIR="$(readlink -f "${6}")"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

"${DIR}/../checkDacapo.sh" || exit 1

BENCHMARKS="${OUTDIR}/benchmarks.txt"

mkdir -p "${OUTDIR}"

echo "${B1}" >> "${BENCHMARKS}"
echo "${B2}" >> "${BENCHMARKS}"
echo "${ITERATIONS}" > "${OUTDIR}/iterations.txt"
echo "${RUNS}" > "${OUTDIR}/runs.txt"
echo "${TOTAL}" > "${OUTDIR}/maxMemory.txt"
