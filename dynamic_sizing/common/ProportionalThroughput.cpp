#include "ProportionalThroughput.hpp"

economem::ProportionalThroughput::ProportionalThroughput()
    : ThroughputCalculator(),
      m_MajorGCOccurred(false),
      m_MinorGCCounter(0),
      m_AvgMinorsPerMajor(50),
      m_AvgMinorTime(50),
      m_AvgMajorTime(50),
      m_TimeSinceLastMajor(0),
      m_HeapSizeAtBeginCollectionB(0)
{
    m_InterGCTimer.start();
}

void economem::ProportionalThroughput::gcBegin()
{
    m_HeapSizeAtBeginCollectionB = getHeapSizeB();
    m_GCTimer.start();
}

economem::ThroughputCalculator::Result economem::ProportionalThroughput::minorGCEnd()
{
    m_GCTimer.stop();
    m_InterGCTimer.stop();

    m_MinorGCCounter++;
    m_TimeSinceLastMajor += m_InterGCTimer.microseconds();
    m_AvgMinorTime.sample(m_GCTimer.seconds());

    m_InterGCTimer.start();

    return makeResult();
}

economem::ThroughputCalculator::Result economem::ProportionalThroughput::majorGCEnd()
{
    m_GCTimer.stop();
    m_InterGCTimer.stop();

    m_TimeSinceLastMajor += m_InterGCTimer.microseconds();
    m_MajorGCOccurred = true;

    m_AvgMinorsPerMajor.sample(m_MinorGCCounter);
    m_MinorGCCounter = 0;

    m_AvgMajorTime.sample(m_GCTimer.seconds());

    ThroughputCalculator::Result result = makeResult();

    m_TimeSinceLastMajor = 0;
    m_InterGCTimer.start();

    return result;
}

economem::ThroughputCalculator::Result economem::ProportionalThroughput::makeResult()
{
    double gcTime =
        (m_MajorGCOccurred ? m_AvgMinorsPerMajor.average() : m_MinorGCCounter) * m_AvgMinorTime.average()
        + m_AvgMajorTime.average();
    double totalTime = m_TimeSinceLastMajor / 1000000.0;
    double throughput = 1.0 - (gcTime/totalTime);

    logReading(m_HeapSizeAtBeginCollectionB, throughput, gcTime * 1000000, totalTime * 1000000);

    return ThroughputCalculator::Result(m_HeapSizeAtBeginCollectionB / (1024 * 1024), throughput);
}
