economemd
    total memory: 3669 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub15_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run09_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 26780: heap size 9 MB, throughput 0.989924
Clients: 1
Client 26780 has a minimum heap size of 12 MB
Reading from 26779: heap size 9 MB, throughput 0.987498
Clients: 2
Client 26779 has a minimum heap size of 1211 MB
Reading from 26780: heap size 9 MB, throughput 0.981884
Reading from 26779: heap size 9 MB, throughput 0.957438
Reading from 26780: heap size 9 MB, throughput 0.940226
Reading from 26779: heap size 11 MB, throughput 0.974075
Reading from 26780: heap size 9 MB, throughput 0.961066
Reading from 26780: heap size 11 MB, throughput 0.975504
Reading from 26779: heap size 11 MB, throughput 0.984886
Reading from 26780: heap size 11 MB, throughput 0.974611
Reading from 26779: heap size 15 MB, throughput 0.863279
Reading from 26780: heap size 16 MB, throughput 0.989286
Reading from 26779: heap size 18 MB, throughput 0.975819
Reading from 26779: heap size 23 MB, throughput 0.913282
Reading from 26780: heap size 16 MB, throughput 0.986232
Reading from 26779: heap size 28 MB, throughput 0.573445
Reading from 26779: heap size 38 MB, throughput 0.861018
Reading from 26779: heap size 40 MB, throughput 0.830335
Reading from 26779: heap size 41 MB, throughput 0.795987
Reading from 26779: heap size 43 MB, throughput 0.777299
Reading from 26780: heap size 24 MB, throughput 0.991758
Reading from 26779: heap size 47 MB, throughput 0.454235
Reading from 26779: heap size 62 MB, throughput 0.806538
Reading from 26779: heap size 68 MB, throughput 0.741709
Reading from 26780: heap size 25 MB, throughput 0.824745
Reading from 26779: heap size 69 MB, throughput 0.249201
Reading from 26779: heap size 93 MB, throughput 0.79761
Reading from 26779: heap size 95 MB, throughput 0.80061
Reading from 26780: heap size 40 MB, throughput 0.993727
Reading from 26779: heap size 98 MB, throughput 0.675395
Reading from 26780: heap size 41 MB, throughput 0.988467
Reading from 26780: heap size 45 MB, throughput 0.980134
Reading from 26780: heap size 46 MB, throughput 0.989456
Reading from 26779: heap size 100 MB, throughput 0.189499
Reading from 26780: heap size 50 MB, throughput 0.985461
Reading from 26779: heap size 124 MB, throughput 0.224231
Reading from 26780: heap size 51 MB, throughput 0.984613
Reading from 26779: heap size 148 MB, throughput 0.204802
Reading from 26780: heap size 57 MB, throughput 0.976816
Reading from 26779: heap size 160 MB, throughput 0.802921
Reading from 26779: heap size 165 MB, throughput 0.794867
Reading from 26780: heap size 57 MB, throughput 0.976671
Reading from 26779: heap size 168 MB, throughput 0.686232
Reading from 26779: heap size 170 MB, throughput 0.586326
Reading from 26780: heap size 63 MB, throughput 0.954159
Reading from 26779: heap size 172 MB, throughput 0.672093
Reading from 26780: heap size 63 MB, throughput 0.961747
Reading from 26779: heap size 176 MB, throughput 0.693225
Reading from 26779: heap size 183 MB, throughput 0.682506
Reading from 26779: heap size 185 MB, throughput 0.664528
Reading from 26779: heap size 191 MB, throughput 0.568536
Reading from 26780: heap size 71 MB, throughput 0.98764
Reading from 26779: heap size 195 MB, throughput 0.481086
Reading from 26779: heap size 203 MB, throughput 0.546674
Reading from 26779: heap size 207 MB, throughput 0.453111
Reading from 26779: heap size 217 MB, throughput 0.451823
Reading from 26780: heap size 72 MB, throughput 0.988352
Reading from 26779: heap size 220 MB, throughput 0.059897
Reading from 26779: heap size 263 MB, throughput 0.32678
Reading from 26780: heap size 79 MB, throughput 0.993648
Reading from 26779: heap size 272 MB, throughput 0.555457
Reading from 26780: heap size 80 MB, throughput 0.992271
Reading from 26779: heap size 273 MB, throughput 0.0744031
Reading from 26779: heap size 312 MB, throughput 0.488066
Reading from 26779: heap size 256 MB, throughput 0.599276
Reading from 26779: heap size 307 MB, throughput 0.610206
Reading from 26779: heap size 309 MB, throughput 0.618192
Reading from 26779: heap size 311 MB, throughput 0.485479
Reading from 26780: heap size 87 MB, throughput 0.98351
Reading from 26779: heap size 312 MB, throughput 0.473587
Reading from 26779: heap size 315 MB, throughput 0.507319
Reading from 26779: heap size 319 MB, throughput 0.551018
Reading from 26780: heap size 88 MB, throughput 0.987724
Reading from 26779: heap size 323 MB, throughput 0.109309
Reading from 26779: heap size 369 MB, throughput 0.471508
Reading from 26779: heap size 369 MB, throughput 0.554833
Reading from 26779: heap size 372 MB, throughput 0.604253
Reading from 26779: heap size 373 MB, throughput 0.643235
Reading from 26780: heap size 94 MB, throughput 0.990199
Reading from 26779: heap size 373 MB, throughput 0.665895
Reading from 26779: heap size 375 MB, throughput 0.61345
Reading from 26779: heap size 378 MB, throughput 0.594351
Reading from 26779: heap size 382 MB, throughput 0.645547
Reading from 26779: heap size 388 MB, throughput 0.518765
Reading from 26779: heap size 391 MB, throughput 0.614418
Reading from 26780: heap size 95 MB, throughput 0.994945
Reading from 26780: heap size 100 MB, throughput 0.988789
Equal recommendation: 1834 MB each
Reading from 26779: heap size 398 MB, throughput 0.0752967
Reading from 26779: heap size 447 MB, throughput 0.502296
Reading from 26779: heap size 453 MB, throughput 0.436388
Reading from 26780: heap size 100 MB, throughput 0.976956
Reading from 26779: heap size 456 MB, throughput 0.462439
Reading from 26779: heap size 459 MB, throughput 0.401108
Reading from 26780: heap size 107 MB, throughput 0.993001
Reading from 26779: heap size 461 MB, throughput 0.076375
Reading from 26779: heap size 517 MB, throughput 0.419361
Reading from 26780: heap size 107 MB, throughput 0.995478
Reading from 26779: heap size 508 MB, throughput 0.519222
Reading from 26779: heap size 461 MB, throughput 0.560329
Reading from 26780: heap size 112 MB, throughput 0.996616
Reading from 26779: heap size 504 MB, throughput 0.0926429
Reading from 26779: heap size 569 MB, throughput 0.393766
Reading from 26780: heap size 113 MB, throughput 0.987441
Reading from 26779: heap size 565 MB, throughput 0.596062
Reading from 26779: heap size 568 MB, throughput 0.60234
Reading from 26779: heap size 569 MB, throughput 0.58392
Reading from 26779: heap size 573 MB, throughput 0.592457
Reading from 26779: heap size 577 MB, throughput 0.632108
Reading from 26780: heap size 118 MB, throughput 0.994932
Reading from 26779: heap size 582 MB, throughput 0.587076
Reading from 26779: heap size 586 MB, throughput 0.623912
Reading from 26780: heap size 118 MB, throughput 0.991686
Reading from 26779: heap size 592 MB, throughput 0.101517
Reading from 26779: heap size 658 MB, throughput 0.478176
Reading from 26780: heap size 123 MB, throughput 0.996692
Reading from 26779: heap size 665 MB, throughput 0.862105
Reading from 26780: heap size 123 MB, throughput 0.997856
Reading from 26779: heap size 664 MB, throughput 0.853634
Reading from 26779: heap size 670 MB, throughput 0.810331
Reading from 26780: heap size 126 MB, throughput 0.998446
Reading from 26779: heap size 671 MB, throughput 0.779622
Reading from 26779: heap size 684 MB, throughput 0.496243
Reading from 26779: heap size 691 MB, throughput 0.417127
Reading from 26780: heap size 127 MB, throughput 0.998329
Reading from 26780: heap size 130 MB, throughput 0.998127
Reading from 26779: heap size 697 MB, throughput 0.0881275
Reading from 26779: heap size 766 MB, throughput 0.200825
Reading from 26779: heap size 690 MB, throughput 0.283773
Equal recommendation: 1834 MB each
Reading from 26779: heap size 760 MB, throughput 0.324904
Reading from 26780: heap size 130 MB, throughput 0.998102
Reading from 26779: heap size 764 MB, throughput 0.0447209
Reading from 26780: heap size 132 MB, throughput 0.998465
Reading from 26779: heap size 847 MB, throughput 0.294758
Reading from 26779: heap size 848 MB, throughput 0.878788
Reading from 26779: heap size 856 MB, throughput 0.888353
Reading from 26779: heap size 858 MB, throughput 0.612637
Reading from 26779: heap size 857 MB, throughput 0.651475
Reading from 26780: heap size 132 MB, throughput 0.998147
Reading from 26779: heap size 859 MB, throughput 0.64104
Reading from 26779: heap size 862 MB, throughput 0.150832
Reading from 26780: heap size 133 MB, throughput 0.998442
Reading from 26779: heap size 952 MB, throughput 0.978469
Reading from 26779: heap size 958 MB, throughput 0.940783
Reading from 26779: heap size 960 MB, throughput 0.967762
Reading from 26779: heap size 958 MB, throughput 0.963964
Reading from 26779: heap size 761 MB, throughput 0.969093
Reading from 26779: heap size 946 MB, throughput 0.946352
Reading from 26780: heap size 134 MB, throughput 0.996687
Reading from 26779: heap size 767 MB, throughput 0.900484
Reading from 26780: heap size 136 MB, throughput 0.854371
Reading from 26779: heap size 924 MB, throughput 0.78195
Reading from 26779: heap size 815 MB, throughput 0.666751
Reading from 26779: heap size 912 MB, throughput 0.685482
Reading from 26780: heap size 140 MB, throughput 0.989262
Reading from 26779: heap size 819 MB, throughput 0.66389
Reading from 26779: heap size 901 MB, throughput 0.729132
Reading from 26779: heap size 825 MB, throughput 0.79681
Reading from 26779: heap size 894 MB, throughput 0.728028
Reading from 26780: heap size 144 MB, throughput 0.996068
Reading from 26779: heap size 901 MB, throughput 0.789256
Reading from 26779: heap size 889 MB, throughput 0.83646
Reading from 26779: heap size 896 MB, throughput 0.810232
Reading from 26779: heap size 886 MB, throughput 0.830223
Reading from 26779: heap size 891 MB, throughput 0.78888
Reading from 26780: heap size 144 MB, throughput 0.99768
Reading from 26779: heap size 887 MB, throughput 0.985921
Reading from 26780: heap size 148 MB, throughput 0.998068
Reading from 26779: heap size 890 MB, throughput 0.968739
Reading from 26780: heap size 149 MB, throughput 0.99741
Reading from 26779: heap size 892 MB, throughput 0.791616
Reading from 26779: heap size 897 MB, throughput 0.772837
Reading from 26779: heap size 904 MB, throughput 0.812967
Reading from 26779: heap size 907 MB, throughput 0.802667
Reading from 26779: heap size 915 MB, throughput 0.818293
Reading from 26780: heap size 153 MB, throughput 0.998229
Reading from 26779: heap size 916 MB, throughput 0.813472
Reading from 26779: heap size 924 MB, throughput 0.8231
Reading from 26779: heap size 924 MB, throughput 0.784682
Reading from 26779: heap size 931 MB, throughput 0.827197
Reading from 26780: heap size 154 MB, throughput 0.994616
Reading from 26779: heap size 932 MB, throughput 0.893332
Equal recommendation: 1834 MB each
Reading from 26779: heap size 933 MB, throughput 0.896273
Reading from 26779: heap size 936 MB, throughput 0.825814
Reading from 26779: heap size 941 MB, throughput 0.737518
Reading from 26780: heap size 158 MB, throughput 0.995238
Reading from 26780: heap size 158 MB, throughput 0.992984
Reading from 26779: heap size 942 MB, throughput 0.0627481
Reading from 26780: heap size 164 MB, throughput 0.997849
Reading from 26779: heap size 1065 MB, throughput 0.586023
Reading from 26779: heap size 1069 MB, throughput 0.822399
Reading from 26779: heap size 1070 MB, throughput 0.810903
Reading from 26779: heap size 1074 MB, throughput 0.784182
Reading from 26779: heap size 1075 MB, throughput 0.799705
Reading from 26779: heap size 1078 MB, throughput 0.773295
Reading from 26780: heap size 164 MB, throughput 0.997667
Reading from 26779: heap size 1085 MB, throughput 0.777243
Reading from 26779: heap size 1087 MB, throughput 0.755831
Reading from 26779: heap size 1100 MB, throughput 0.801398
Reading from 26780: heap size 168 MB, throughput 0.997949
Reading from 26779: heap size 1101 MB, throughput 0.950115
Reading from 26780: heap size 169 MB, throughput 0.997924
Reading from 26780: heap size 173 MB, throughput 0.996638
Reading from 26779: heap size 1118 MB, throughput 0.947874
Reading from 26780: heap size 173 MB, throughput 0.997645
Reading from 26779: heap size 1121 MB, throughput 0.968709
Reading from 26780: heap size 178 MB, throughput 0.997592
Equal recommendation: 1834 MB each
Reading from 26780: heap size 178 MB, throughput 0.998035
Reading from 26779: heap size 1128 MB, throughput 0.968894
Reading from 26780: heap size 181 MB, throughput 0.998608
Reading from 26780: heap size 182 MB, throughput 0.994262
Reading from 26779: heap size 1131 MB, throughput 0.953963
Reading from 26780: heap size 185 MB, throughput 0.994563
Reading from 26780: heap size 185 MB, throughput 0.984766
Reading from 26779: heap size 1128 MB, throughput 0.952934
Reading from 26780: heap size 191 MB, throughput 0.997758
Reading from 26780: heap size 192 MB, throughput 0.997596
Reading from 26779: heap size 1133 MB, throughput 0.959923
Reading from 26780: heap size 198 MB, throughput 0.997909
Reading from 26779: heap size 1135 MB, throughput 0.963424
Reading from 26780: heap size 199 MB, throughput 0.997842
Reading from 26780: heap size 204 MB, throughput 0.998069
Equal recommendation: 1834 MB each
Reading from 26779: heap size 1137 MB, throughput 0.959581
Reading from 26780: heap size 205 MB, throughput 0.997593
Reading from 26779: heap size 1142 MB, throughput 0.96278
Reading from 26780: heap size 211 MB, throughput 0.995642
Reading from 26780: heap size 211 MB, throughput 0.997633
Reading from 26779: heap size 1144 MB, throughput 0.957304
Reading from 26780: heap size 215 MB, throughput 0.997806
Reading from 26779: heap size 1150 MB, throughput 0.959497
Reading from 26780: heap size 216 MB, throughput 0.997464
Reading from 26780: heap size 221 MB, throughput 0.997487
Reading from 26779: heap size 1154 MB, throughput 0.958283
Reading from 26780: heap size 221 MB, throughput 0.996465
Reading from 26780: heap size 227 MB, throughput 0.991093
Reading from 26779: heap size 1159 MB, throughput 0.943094
Equal recommendation: 1834 MB each
Reading from 26780: heap size 227 MB, throughput 0.996032
Reading from 26779: heap size 1164 MB, throughput 0.959664
Reading from 26780: heap size 235 MB, throughput 0.998251
Reading from 26780: heap size 235 MB, throughput 0.997834
Reading from 26779: heap size 1170 MB, throughput 0.959334
Reading from 26780: heap size 241 MB, throughput 0.998345
Reading from 26779: heap size 1175 MB, throughput 0.959039
Reading from 26780: heap size 241 MB, throughput 0.997834
Reading from 26779: heap size 1182 MB, throughput 0.953987
Reading from 26780: heap size 246 MB, throughput 0.998221
Reading from 26780: heap size 247 MB, throughput 0.997911
Equal recommendation: 1834 MB each
Reading from 26779: heap size 1187 MB, throughput 0.960584
Reading from 26780: heap size 252 MB, throughput 0.998176
Reading from 26779: heap size 1194 MB, throughput 0.958709
Reading from 26780: heap size 252 MB, throughput 0.997897
Reading from 26779: heap size 1196 MB, throughput 0.956992
Reading from 26780: heap size 257 MB, throughput 0.998069
Reading from 26780: heap size 257 MB, throughput 0.998257
Reading from 26779: heap size 1203 MB, throughput 0.940343
Reading from 26780: heap size 261 MB, throughput 0.993224
Reading from 26780: heap size 262 MB, throughput 0.996345
Reading from 26779: heap size 1204 MB, throughput 0.95937
Reading from 26780: heap size 270 MB, throughput 0.997759
Equal recommendation: 1834 MB each
Reading from 26779: heap size 1211 MB, throughput 0.959978
Reading from 26780: heap size 270 MB, throughput 0.997465
Reading from 26780: heap size 276 MB, throughput 0.998285
Reading from 26779: heap size 1212 MB, throughput 0.96106
Reading from 26780: heap size 276 MB, throughput 0.998079
Reading from 26779: heap size 1218 MB, throughput 0.96049
Reading from 26780: heap size 282 MB, throughput 0.998389
Reading from 26780: heap size 282 MB, throughput 0.997941
Reading from 26779: heap size 1219 MB, throughput 0.544135
Equal recommendation: 1834 MB each
Reading from 26780: heap size 287 MB, throughput 0.998239
Reading from 26780: heap size 287 MB, throughput 0.997904
Reading from 26779: heap size 1329 MB, throughput 0.950709
Reading from 26780: heap size 293 MB, throughput 0.997177
Reading from 26780: heap size 293 MB, throughput 0.995346
Reading from 26779: heap size 1330 MB, throughput 0.985073
Reading from 26780: heap size 299 MB, throughput 0.998043
Reading from 26779: heap size 1341 MB, throughput 0.981812
Reading from 26780: heap size 299 MB, throughput 0.998023
Reading from 26779: heap size 1346 MB, throughput 0.978951
Reading from 26780: heap size 305 MB, throughput 0.998211
Equal recommendation: 1834 MB each
Reading from 26780: heap size 305 MB, throughput 0.997959
Reading from 26779: heap size 1349 MB, throughput 0.980018
Reading from 26780: heap size 311 MB, throughput 0.998335
Reading from 26779: heap size 1351 MB, throughput 0.977685
Reading from 26780: heap size 311 MB, throughput 0.996675
Reading from 26779: heap size 1341 MB, throughput 0.974952
Reading from 26780: heap size 317 MB, throughput 0.998395
Reading from 26780: heap size 317 MB, throughput 0.99824
Reading from 26779: heap size 1259 MB, throughput 0.964719
Equal recommendation: 1834 MB each
Reading from 26780: heap size 323 MB, throughput 0.873787
Reading from 26780: heap size 330 MB, throughput 0.998947
Reading from 26779: heap size 1331 MB, throughput 0.972275
Reading from 26780: heap size 341 MB, throughput 0.999187
Reading from 26779: heap size 1338 MB, throughput 0.9716
Reading from 26780: heap size 341 MB, throughput 0.999095
Reading from 26779: heap size 1340 MB, throughput 0.969215
Reading from 26780: heap size 347 MB, throughput 0.999162
Equal recommendation: 1834 MB each
Reading from 26779: heap size 1340 MB, throughput 0.967385
Reading from 26780: heap size 349 MB, throughput 0.999023
Reading from 26780: heap size 354 MB, throughput 0.998479
Reading from 26779: heap size 1344 MB, throughput 0.964925
Reading from 26780: heap size 355 MB, throughput 0.999007
Reading from 26779: heap size 1348 MB, throughput 0.960838
Reading from 26780: heap size 361 MB, throughput 0.996473
Reading from 26780: heap size 362 MB, throughput 0.997581
Reading from 26779: heap size 1353 MB, throughput 0.961929
Equal recommendation: 1834 MB each
Reading from 26780: heap size 371 MB, throughput 0.998638
Reading from 26779: heap size 1360 MB, throughput 0.957559
Reading from 26780: heap size 371 MB, throughput 0.998401
Reading from 26780: heap size 379 MB, throughput 0.99842
Reading from 26780: heap size 380 MB, throughput 0.998321
Reading from 26780: heap size 388 MB, throughput 0.998392
Equal recommendation: 1834 MB each
Reading from 26780: heap size 388 MB, throughput 0.998253
Reading from 26780: heap size 396 MB, throughput 0.994704
Reading from 26779: heap size 1369 MB, throughput 0.986033
Reading from 26780: heap size 396 MB, throughput 0.998299
Reading from 26780: heap size 407 MB, throughput 0.998259
Equal recommendation: 1834 MB each
Reading from 26780: heap size 409 MB, throughput 0.998172
Reading from 26780: heap size 418 MB, throughput 0.998374
Reading from 26780: heap size 419 MB, throughput 0.998367
Reading from 26779: heap size 1376 MB, throughput 0.930688
Reading from 26780: heap size 427 MB, throughput 0.998351
Reading from 26780: heap size 428 MB, throughput 0.994701
Equal recommendation: 1834 MB each
Reading from 26780: heap size 437 MB, throughput 0.998432
Reading from 26779: heap size 1442 MB, throughput 0.996731
Reading from 26779: heap size 1467 MB, throughput 0.975581
Reading from 26779: heap size 1475 MB, throughput 0.906161
Reading from 26779: heap size 1346 MB, throughput 0.87326
Reading from 26780: heap size 438 MB, throughput 0.99854
Reading from 26779: heap size 1461 MB, throughput 0.880682
Reading from 26779: heap size 1467 MB, throughput 0.879209
Reading from 26779: heap size 1460 MB, throughput 0.888733
Reading from 26779: heap size 1467 MB, throughput 0.94022
Reading from 26780: heap size 449 MB, throughput 0.998532
Reading from 26779: heap size 1475 MB, throughput 0.983613
Equal recommendation: 1834 MB each
Reading from 26780: heap size 450 MB, throughput 0.998493
Reading from 26779: heap size 1480 MB, throughput 0.987627
Reading from 26780: heap size 459 MB, throughput 0.998484
Reading from 26779: heap size 1489 MB, throughput 0.986006
Reading from 26780: heap size 460 MB, throughput 0.997476
Reading from 26779: heap size 1494 MB, throughput 0.982751
Reading from 26780: heap size 469 MB, throughput 0.997574
Equal recommendation: 1834 MB each
Reading from 26779: heap size 1485 MB, throughput 0.982377
Reading from 26780: heap size 469 MB, throughput 0.99833
Reading from 26779: heap size 1493 MB, throughput 0.982149
Reading from 26780: heap size 479 MB, throughput 0.998385
Reading from 26779: heap size 1474 MB, throughput 0.980003
Reading from 26780: heap size 480 MB, throughput 0.998545
Reading from 26779: heap size 1485 MB, throughput 0.97879
Equal recommendation: 1834 MB each
Reading from 26780: heap size 490 MB, throughput 0.998638
Reading from 26779: heap size 1474 MB, throughput 0.973205
Reading from 26780: heap size 491 MB, throughput 0.996837
Reading from 26780: heap size 499 MB, throughput 0.998035
Reading from 26779: heap size 1481 MB, throughput 0.973168
Reading from 26780: heap size 500 MB, throughput 0.998357
Reading from 26779: heap size 1485 MB, throughput 0.973978
Equal recommendation: 1834 MB each
Reading from 26780: heap size 511 MB, throughput 0.998454
Reading from 26779: heap size 1486 MB, throughput 0.972643
Reading from 26780: heap size 512 MB, throughput 0.998512
Reading from 26779: heap size 1492 MB, throughput 0.969528
Reading from 26780: heap size 522 MB, throughput 0.998532
Reading from 26779: heap size 1499 MB, throughput 0.962102
Equal recommendation: 1834 MB each
Reading from 26780: heap size 523 MB, throughput 0.995958
Reading from 26779: heap size 1507 MB, throughput 0.967849
Reading from 26780: heap size 533 MB, throughput 0.998594
Reading from 26779: heap size 1518 MB, throughput 0.964482
Reading from 26780: heap size 534 MB, throughput 0.99836
Reading from 26779: heap size 1530 MB, throughput 0.964925
Equal recommendation: 1834 MB each
Reading from 26780: heap size 545 MB, throughput 0.998485
Reading from 26779: heap size 1540 MB, throughput 0.962843
Reading from 26780: heap size 546 MB, throughput 0.998627
Reading from 26779: heap size 1553 MB, throughput 0.956746
Reading from 26780: heap size 557 MB, throughput 0.996884
Reading from 26780: heap size 558 MB, throughput 0.935391
Reading from 26779: heap size 1560 MB, throughput 0.964439
Equal recommendation: 1834 MB each
Reading from 26780: heap size 575 MB, throughput 0.999396
Reading from 26779: heap size 1573 MB, throughput 0.962943
Reading from 26780: heap size 575 MB, throughput 0.999436
Reading from 26779: heap size 1577 MB, throughput 0.960532
Reading from 26780: heap size 587 MB, throughput 0.999439
Reading from 26779: heap size 1590 MB, throughput 0.964844
Equal recommendation: 1834 MB each
Reading from 26780: heap size 590 MB, throughput 0.999149
Reading from 26779: heap size 1593 MB, throughput 0.964388
Reading from 26780: heap size 601 MB, throughput 0.99794
Reading from 26779: heap size 1606 MB, throughput 0.965204
Reading from 26780: heap size 602 MB, throughput 0.998959
Equal recommendation: 1834 MB each
Reading from 26779: heap size 1607 MB, throughput 0.963304
Reading from 26780: heap size 614 MB, throughput 0.998906
Reading from 26779: heap size 1620 MB, throughput 0.747841
Reading from 26780: heap size 615 MB, throughput 0.999065
Reading from 26779: heap size 1728 MB, throughput 0.993815
Equal recommendation: 1834 MB each
Client 26780 died
Clients: 1
Reading from 26779: heap size 1727 MB, throughput 0.991279
Recommendation: one client; give it all the memory
Reading from 26779: heap size 1741 MB, throughput 0.992553
Recommendation: one client; give it all the memory
Reading from 26779: heap size 1736 MB, throughput 0.989609
Recommendation: one client; give it all the memory
Reading from 26779: heap size 1747 MB, throughput 0.980405
Reading from 26779: heap size 1749 MB, throughput 0.858919
Reading from 26779: heap size 1760 MB, throughput 0.785286
Reading from 26779: heap size 1787 MB, throughput 0.829497
Reading from 26779: heap size 1804 MB, throughput 0.866491
Client 26779 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
