#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") prefix total_mem [daemon_args...]"
    exit 1
}

test -z "${1}" && usage
test -z "${2}" && usage
PREFIX="${1}"
TOTAL_MEM="${2}"


function cleanup()
{
    test ! -z "${SIMPLE}" && kill "${SIMPLE}" &>/dev/null
    test ! -z "${VENGEROV}" && kill "${VENGEROV}" &>/dev/null
    test ! -z "${AVG_VENGEROV}" && kill "${AVG_VENGEROV}" &>/dev/null
    test ! -z "${MAJOR_ONLY}" && kill "${MAJOR_ONLY}" &>/dev/null
    test ! -z "${SIMPLE_SMOOTHING}" && kill "${SIMPLE_SMOOTHING}" &>/dev/null
    test ! -z "${PROPORTIONAL}" && kill "${PROPORTIONAL}" &>/dev/null
    wait
    exit
}

trap "cleanup" SIGINT SIGHUP SIGTERM

"${DIR}/save_and_fit.sh" 'simple' "${PREFIX}" "${TOTAL_MEM}" "${@:3}" &
SIMPLE="${!}"

"${DIR}/save_and_fit.sh" 'vengerov' "${PREFIX}" "${TOTAL_MEM}" '--dont-recommend' "${@:3}" &
VENGEROV="${!}"

"${DIR}/save_and_fit.sh" 'avgVengerov' "${PREFIX}" "${TOTAL_MEM}" '--dont-recommend' "${@:3}" &
AVG_VENGEROV="${!}"

"${DIR}/save_and_fit.sh" 'majorOnly' "${PREFIX}" "${TOTAL_MEM}" '--dont-recommend' "${@:3}" &
MAJOR_ONLY="${!}"

"${DIR}/save_and_fit.sh" 'simpleSmoothing' "${PREFIX}" "${TOTAL_MEM}" '--dont-recommend' "${@:3}" &
SIMPLE_SMOOTHING="${!}"

"${DIR}/save_and_fit.sh" 'proportional' "${PREFIX}" "${TOTAL_MEM}" '--dont-recommend' "${@:3}" &
PROPORTIONAL="${!}"

wait
cleanup
