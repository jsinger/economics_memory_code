economemd
    total memory: 7518 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub4_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
TimerThread: starting
ReadingThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 32229: heap size 9 MB, throughput 0.988601
Clients: 1
Client 32229 has a minimum heap size of 30 MB
Reading from 32228: heap size 9 MB, throughput 0.98957
Clients: 2
Client 32228 has a minimum heap size of 1223 MB
Reading from 32229: heap size 9 MB, throughput 0.978994
Reading from 32229: heap size 11 MB, throughput 0.977822
Reading from 32229: heap size 11 MB, throughput 0.699707
Reading from 32229: heap size 15 MB, throughput 0.691002
Reading from 32229: heap size 15 MB, throughput 0.860657
Reading from 32228: heap size 9 MB, throughput 0.980143
Reading from 32229: heap size 24 MB, throughput 0.802682
Reading from 32229: heap size 24 MB, throughput 0.791629
Reading from 32229: heap size 40 MB, throughput 0.919138
Reading from 32229: heap size 40 MB, throughput 0.91205
Reading from 32229: heap size 60 MB, throughput 0.948403
Reading from 32228: heap size 9 MB, throughput 0.962244
Reading from 32229: heap size 60 MB, throughput 0.95455
Reading from 32229: heap size 91 MB, throughput 0.968908
Reading from 32228: heap size 9 MB, throughput 0.951631
Reading from 32229: heap size 91 MB, throughput 0.955736
Reading from 32229: heap size 140 MB, throughput 0.969917
Reading from 32228: heap size 11 MB, throughput 0.98426
Reading from 32228: heap size 11 MB, throughput 0.984944
Reading from 32228: heap size 17 MB, throughput 0.951705
Reading from 32228: heap size 17 MB, throughput 0.633193
Reading from 32228: heap size 31 MB, throughput 0.880885
Reading from 32228: heap size 32 MB, throughput 0.687698
Reading from 32228: heap size 34 MB, throughput 0.256058
Reading from 32229: heap size 140 MB, throughput 0.933813
Reading from 32228: heap size 46 MB, throughput 0.811165
Reading from 32228: heap size 49 MB, throughput 0.955382
Reading from 32228: heap size 51 MB, throughput 0.243192
Reading from 32228: heap size 74 MB, throughput 0.789986
Reading from 32229: heap size 224 MB, throughput 0.879577
Reading from 32228: heap size 74 MB, throughput 0.277753
Reading from 32228: heap size 102 MB, throughput 0.79449
Reading from 32228: heap size 102 MB, throughput 0.766203
Reading from 32228: heap size 104 MB, throughput 0.704202
Reading from 32228: heap size 107 MB, throughput 0.164277
Reading from 32229: heap size 241 MB, throughput 0.986477
Reading from 32228: heap size 134 MB, throughput 0.598096
Reading from 32228: heap size 138 MB, throughput 0.760827
Reading from 32228: heap size 142 MB, throughput 0.777517
Reading from 32228: heap size 144 MB, throughput 0.112588
Reading from 32228: heap size 186 MB, throughput 0.610553
Reading from 32229: heap size 278 MB, throughput 0.983255
Reading from 32228: heap size 187 MB, throughput 0.603247
Reading from 32228: heap size 189 MB, throughput 0.657004
Reading from 32228: heap size 196 MB, throughput 0.752297
Reading from 32228: heap size 199 MB, throughput 0.73516
Reading from 32228: heap size 206 MB, throughput 0.154526
Reading from 32228: heap size 253 MB, throughput 0.486445
Reading from 32228: heap size 258 MB, throughput 0.589594
Reading from 32228: heap size 260 MB, throughput 0.454784
Reading from 32229: heap size 286 MB, throughput 0.989395
Reading from 32228: heap size 267 MB, throughput 0.54651
Reading from 32228: heap size 273 MB, throughput 0.564178
Reading from 32228: heap size 280 MB, throughput 0.535897
Reading from 32229: heap size 314 MB, throughput 0.836398
Reading from 32228: heap size 286 MB, throughput 0.113543
Reading from 32228: heap size 333 MB, throughput 0.614888
Reading from 32229: heap size 337 MB, throughput 0.9923
Reading from 32228: heap size 333 MB, throughput 0.110153
Reading from 32228: heap size 388 MB, throughput 0.671598
Reading from 32228: heap size 383 MB, throughput 0.771688
Reading from 32228: heap size 339 MB, throughput 0.702487
Reading from 32228: heap size 385 MB, throughput 0.738451
Reading from 32228: heap size 386 MB, throughput 0.669625
Reading from 32228: heap size 387 MB, throughput 0.642225
Reading from 32228: heap size 394 MB, throughput 0.623207
Reading from 32228: heap size 396 MB, throughput 0.612637
Reading from 32228: heap size 407 MB, throughput 0.444105
Reading from 32229: heap size 381 MB, throughput 0.990281
Reading from 32228: heap size 411 MB, throughput 0.499018
Equal recommendation: 3759 MB each
Reading from 32228: heap size 424 MB, throughput 0.419976
Reading from 32228: heap size 429 MB, throughput 0.36282
Reading from 32228: heap size 438 MB, throughput 0.421483
Reading from 32229: heap size 383 MB, throughput 0.986251
Reading from 32228: heap size 442 MB, throughput 0.0476128
Reading from 32228: heap size 503 MB, throughput 0.313962
Reading from 32228: heap size 489 MB, throughput 0.453517
Reading from 32228: heap size 415 MB, throughput 0.0606529
Reading from 32228: heap size 550 MB, throughput 0.361155
Reading from 32229: heap size 423 MB, throughput 0.981319
Reading from 32228: heap size 555 MB, throughput 0.568662
Reading from 32228: heap size 550 MB, throughput 0.567129
Reading from 32228: heap size 470 MB, throughput 0.501356
Reading from 32228: heap size 540 MB, throughput 0.464839
Reading from 32228: heap size 480 MB, throughput 0.52194
Reading from 32228: heap size 534 MB, throughput 0.586101
Reading from 32228: heap size 488 MB, throughput 0.497536
Reading from 32229: heap size 424 MB, throughput 0.96084
Reading from 32228: heap size 538 MB, throughput 0.091646
Reading from 32228: heap size 603 MB, throughput 0.423326
Reading from 32228: heap size 599 MB, throughput 0.594206
Reading from 32228: heap size 604 MB, throughput 0.63399
Reading from 32228: heap size 607 MB, throughput 0.540738
Reading from 32228: heap size 610 MB, throughput 0.535302
Reading from 32229: heap size 455 MB, throughput 0.990461
Reading from 32228: heap size 614 MB, throughput 0.472393
Reading from 32228: heap size 621 MB, throughput 0.456175
Reading from 32228: heap size 627 MB, throughput 0.844223
Reading from 32229: heap size 457 MB, throughput 0.982626
Reading from 32228: heap size 635 MB, throughput 0.806039
Reading from 32228: heap size 638 MB, throughput 0.775014
Reading from 32228: heap size 651 MB, throughput 0.814656
Reading from 32229: heap size 490 MB, throughput 0.980123
Equal recommendation: 3759 MB each
Reading from 32228: heap size 656 MB, throughput 0.0450921
Reading from 32228: heap size 734 MB, throughput 0.301921
Reading from 32229: heap size 491 MB, throughput 0.990908
Reading from 32228: heap size 742 MB, throughput 0.506157
Reading from 32228: heap size 743 MB, throughput 0.521226
Reading from 32228: heap size 738 MB, throughput 0.276171
Reading from 32228: heap size 665 MB, throughput 0.0362831
Reading from 32228: heap size 805 MB, throughput 0.241642
Reading from 32228: heap size 809 MB, throughput 0.508161
Reading from 32228: heap size 815 MB, throughput 0.507858
Reading from 32229: heap size 524 MB, throughput 0.990584
Reading from 32228: heap size 815 MB, throughput 0.838954
Reading from 32228: heap size 818 MB, throughput 0.771532
Reading from 32228: heap size 820 MB, throughput 0.514409
Reading from 32229: heap size 526 MB, throughput 0.988271
Reading from 32228: heap size 815 MB, throughput 0.0340527
Reading from 32228: heap size 903 MB, throughput 0.369934
Reading from 32228: heap size 902 MB, throughput 0.88115
Reading from 32228: heap size 908 MB, throughput 0.875513
Reading from 32228: heap size 912 MB, throughput 0.946331
Reading from 32228: heap size 913 MB, throughput 0.899204
Reading from 32229: heap size 556 MB, throughput 0.986957
Reading from 32228: heap size 907 MB, throughput 0.961276
Reading from 32228: heap size 745 MB, throughput 0.892314
Reading from 32228: heap size 896 MB, throughput 0.90795
Reading from 32228: heap size 745 MB, throughput 0.923246
Reading from 32228: heap size 885 MB, throughput 0.809171
Reading from 32228: heap size 794 MB, throughput 0.781512
Reading from 32228: heap size 876 MB, throughput 0.790168
Reading from 32229: heap size 558 MB, throughput 0.990798
Reading from 32228: heap size 790 MB, throughput 0.826875
Reading from 32228: heap size 863 MB, throughput 0.759908
Reading from 32228: heap size 794 MB, throughput 0.771015
Reading from 32228: heap size 857 MB, throughput 0.797883
Reading from 32228: heap size 799 MB, throughput 0.787986
Reading from 32228: heap size 851 MB, throughput 0.822887
Reading from 32228: heap size 856 MB, throughput 0.845426
Reading from 32228: heap size 848 MB, throughput 0.865353
Reading from 32229: heap size 583 MB, throughput 0.985659
Reading from 32228: heap size 852 MB, throughput 0.848899
Reading from 32228: heap size 848 MB, throughput 0.983816
Equal recommendation: 3759 MB each
Reading from 32229: heap size 587 MB, throughput 0.989677
Reading from 32228: heap size 851 MB, throughput 0.970246
Reading from 32228: heap size 857 MB, throughput 0.821594
Reading from 32228: heap size 858 MB, throughput 0.788417
Reading from 32228: heap size 864 MB, throughput 0.768344
Reading from 32228: heap size 867 MB, throughput 0.773265
Reading from 32228: heap size 874 MB, throughput 0.789036
Reading from 32228: heap size 875 MB, throughput 0.789465
Reading from 32229: heap size 613 MB, throughput 0.991952
Reading from 32228: heap size 882 MB, throughput 0.784068
Reading from 32228: heap size 882 MB, throughput 0.72689
Reading from 32228: heap size 889 MB, throughput 0.667393
Reading from 32228: heap size 889 MB, throughput 0.795564
Reading from 32228: heap size 895 MB, throughput 0.894388
Reading from 32228: heap size 896 MB, throughput 0.872213
Reading from 32229: heap size 616 MB, throughput 0.992539
Reading from 32228: heap size 899 MB, throughput 0.871422
Reading from 32228: heap size 901 MB, throughput 0.785019
Reading from 32229: heap size 634 MB, throughput 0.974929
Reading from 32228: heap size 899 MB, throughput 0.0765097
Reading from 32228: heap size 992 MB, throughput 0.515755
Reading from 32228: heap size 1003 MB, throughput 0.722085
Reading from 32228: heap size 1006 MB, throughput 0.596297
Reading from 32228: heap size 1016 MB, throughput 0.725751
Reading from 32228: heap size 1019 MB, throughput 0.681017
Reading from 32228: heap size 1031 MB, throughput 0.677842
Reading from 32228: heap size 1034 MB, throughput 0.666184
Reading from 32228: heap size 1047 MB, throughput 0.676682
Reading from 32229: heap size 637 MB, throughput 0.99395
Reading from 32228: heap size 1050 MB, throughput 0.63032
Reading from 32228: heap size 1065 MB, throughput 0.681204
Reading from 32228: heap size 1068 MB, throughput 0.681889
Equal recommendation: 3759 MB each
Reading from 32229: heap size 660 MB, throughput 0.977659
Reading from 32228: heap size 1083 MB, throughput 0.951461
Reading from 32229: heap size 665 MB, throughput 0.985556
Reading from 32228: heap size 1088 MB, throughput 0.967843
Reading from 32229: heap size 697 MB, throughput 0.991902
Reading from 32229: heap size 698 MB, throughput 0.993242
Reading from 32228: heap size 1102 MB, throughput 0.753979
Reading from 32229: heap size 719 MB, throughput 0.98997
Reading from 32228: heap size 1177 MB, throughput 0.993741
Equal recommendation: 3759 MB each
Reading from 32229: heap size 723 MB, throughput 0.985117
Reading from 32228: heap size 1179 MB, throughput 0.989733
Reading from 32229: heap size 749 MB, throughput 0.986617
Reading from 32228: heap size 1189 MB, throughput 0.988431
Reading from 32229: heap size 751 MB, throughput 0.990179
Reading from 32228: heap size 1201 MB, throughput 0.986061
Reading from 32229: heap size 780 MB, throughput 0.991927
Equal recommendation: 3759 MB each
Reading from 32228: heap size 1203 MB, throughput 0.985949
Reading from 32229: heap size 781 MB, throughput 0.995647
Reading from 32228: heap size 1199 MB, throughput 0.984256
Reading from 32229: heap size 800 MB, throughput 0.990176
Reading from 32229: heap size 804 MB, throughput 0.989386
Reading from 32228: heap size 1074 MB, throughput 0.982196
Reading from 32229: heap size 830 MB, throughput 0.991345
Reading from 32228: heap size 1189 MB, throughput 0.976196
Equal recommendation: 3759 MB each
Reading from 32229: heap size 830 MB, throughput 0.991076
Reading from 32228: heap size 1091 MB, throughput 0.977399
Reading from 32229: heap size 855 MB, throughput 0.991424
Reading from 32228: heap size 1175 MB, throughput 0.976541
Reading from 32229: heap size 856 MB, throughput 0.989912
Reading from 32228: heap size 1107 MB, throughput 0.97389
Reading from 32229: heap size 882 MB, throughput 0.98999
Equal recommendation: 3759 MB each
Reading from 32228: heap size 1173 MB, throughput 0.977405
Reading from 32229: heap size 883 MB, throughput 0.990344
Reading from 32228: heap size 1177 MB, throughput 0.974983
Reading from 32229: heap size 910 MB, throughput 0.987282
Reading from 32228: heap size 1178 MB, throughput 0.973799
Reading from 32229: heap size 911 MB, throughput 0.990941
Equal recommendation: 3759 MB each
Reading from 32228: heap size 1179 MB, throughput 0.967049
Reading from 32229: heap size 938 MB, throughput 0.992756
Reading from 32228: heap size 1182 MB, throughput 0.970932
Reading from 32229: heap size 939 MB, throughput 0.991113
Reading from 32228: heap size 1186 MB, throughput 0.96366
Reading from 32229: heap size 963 MB, throughput 0.992311
Equal recommendation: 3759 MB each
Reading from 32229: heap size 965 MB, throughput 0.990696
Reading from 32228: heap size 1191 MB, throughput 0.972046
Reading from 32228: heap size 1193 MB, throughput 0.965584
Reading from 32229: heap size 992 MB, throughput 0.990262
Reading from 32229: heap size 992 MB, throughput 0.97886
Reading from 32228: heap size 1198 MB, throughput 0.961887
Equal recommendation: 3759 MB each
Reading from 32229: heap size 1021 MB, throughput 0.985037
Reading from 32228: heap size 1199 MB, throughput 0.959492
Reading from 32229: heap size 1021 MB, throughput 0.990722
Reading from 32228: heap size 1205 MB, throughput 0.976
Reading from 32229: heap size 1055 MB, throughput 0.993795
Reading from 32228: heap size 1205 MB, throughput 0.974247
Equal recommendation: 3759 MB each
Reading from 32229: heap size 1058 MB, throughput 0.979296
Reading from 32228: heap size 1209 MB, throughput 0.967847
Reading from 32229: heap size 1093 MB, throughput 0.995978
Reading from 32228: heap size 1209 MB, throughput 0.971707
Reading from 32229: heap size 1094 MB, throughput 0.991746
Reading from 32228: heap size 1214 MB, throughput 0.970543
Equal recommendation: 3759 MB each
Reading from 32229: heap size 1128 MB, throughput 0.993235
Reading from 32228: heap size 1214 MB, throughput 0.968654
Reading from 32228: heap size 1218 MB, throughput 0.970602
Reading from 32229: heap size 1130 MB, throughput 0.98862
Reading from 32228: heap size 1218 MB, throughput 0.968318
Reading from 32229: heap size 1161 MB, throughput 0.99136
Equal recommendation: 3759 MB each
Reading from 32228: heap size 1222 MB, throughput 0.972103
Reading from 32229: heap size 1164 MB, throughput 0.990201
Reading from 32228: heap size 1222 MB, throughput 0.96729
Reading from 32229: heap size 1199 MB, throughput 0.952876
Reading from 32228: heap size 1226 MB, throughput 0.967941
Equal recommendation: 3759 MB each
Reading from 32229: heap size 1233 MB, throughput 0.99541
Reading from 32229: heap size 1287 MB, throughput 0.982952
Reading from 32228: heap size 1227 MB, throughput 0.606221
Reading from 32229: heap size 1287 MB, throughput 0.990891
Equal recommendation: 3759 MB each
Reading from 32228: heap size 1342 MB, throughput 0.962561
Reading from 32229: heap size 1337 MB, throughput 0.990905
Reading from 32228: heap size 1342 MB, throughput 0.982336
Reading from 32229: heap size 1338 MB, throughput 0.990168
Reading from 32228: heap size 1352 MB, throughput 0.982285
Equal recommendation: 3759 MB each
Reading from 32228: heap size 1357 MB, throughput 0.985292
Reading from 32229: heap size 1388 MB, throughput 0.991042
Reading from 32228: heap size 1360 MB, throughput 0.984317
Reading from 32229: heap size 1389 MB, throughput 0.992011
Reading from 32228: heap size 1362 MB, throughput 0.979251
Equal recommendation: 3759 MB each
Reading from 32229: heap size 1438 MB, throughput 0.989395
Reading from 32228: heap size 1353 MB, throughput 0.979208
Reading from 32229: heap size 1440 MB, throughput 0.994587
Reading from 32228: heap size 1278 MB, throughput 0.9789
Equal recommendation: 3759 MB each
Reading from 32229: heap size 1489 MB, throughput 0.992933
Reading from 32228: heap size 1344 MB, throughput 0.978912
Reading from 32229: heap size 1494 MB, throughput 0.988798
Reading from 32228: heap size 1350 MB, throughput 0.977841
Equal recommendation: 3759 MB each
Reading from 32228: heap size 1351 MB, throughput 0.975685
Reading from 32229: heap size 1541 MB, throughput 0.993621
Reading from 32229: heap size 1543 MB, throughput 0.991813
Reading from 32229: heap size 1593 MB, throughput 0.991046
Equal recommendation: 3759 MB each
Reading from 32229: heap size 1594 MB, throughput 0.987811
Reading from 32228: heap size 1352 MB, throughput 0.988533
Reading from 32229: heap size 1650 MB, throughput 0.990506
Equal recommendation: 3759 MB each
Reading from 32229: heap size 1653 MB, throughput 0.990452
Reading from 32229: heap size 1713 MB, throughput 0.991635
Equal recommendation: 3759 MB each
Reading from 32228: heap size 1323 MB, throughput 0.992407
Reading from 32229: heap size 1714 MB, throughput 0.990788
Reading from 32229: heap size 1777 MB, throughput 0.992036
Reading from 32228: heap size 1359 MB, throughput 0.981051
Equal recommendation: 3759 MB each
Reading from 32228: heap size 1348 MB, throughput 0.35613
Reading from 32228: heap size 1348 MB, throughput 0.915079
Reading from 32228: heap size 1365 MB, throughput 0.894638
Reading from 32228: heap size 1371 MB, throughput 0.879924
Reading from 32228: heap size 1367 MB, throughput 0.882936
Reading from 32228: heap size 1372 MB, throughput 0.869769
Reading from 32228: heap size 1372 MB, throughput 0.888362
Reading from 32228: heap size 1378 MB, throughput 0.893941
Reading from 32229: heap size 1777 MB, throughput 0.988987
Reading from 32228: heap size 1381 MB, throughput 0.99464
Reading from 32228: heap size 1387 MB, throughput 0.9932
Reading from 32229: heap size 1843 MB, throughput 0.990608
Equal recommendation: 3759 MB each
Reading from 32228: heap size 1411 MB, throughput 0.990778
Client 32229 died
Clients: 1
Reading from 32228: heap size 1414 MB, throughput 0.992956
Recommendation: one client; give it all the memory
Reading from 32228: heap size 1414 MB, throughput 0.992263
Reading from 32228: heap size 1420 MB, throughput 0.990817
Recommendation: one client; give it all the memory
Reading from 32228: heap size 1405 MB, throughput 0.99093
Reading from 32228: heap size 1306 MB, throughput 0.989251
Recommendation: one client; give it all the memory
Reading from 32228: heap size 1395 MB, throughput 0.988822
Reading from 32228: heap size 1404 MB, throughput 0.987442
Recommendation: one client; give it all the memory
Reading from 32228: heap size 1405 MB, throughput 0.986383
Reading from 32228: heap size 1405 MB, throughput 0.985556
Recommendation: one client; give it all the memory
Reading from 32228: heap size 1409 MB, throughput 0.98403
Reading from 32228: heap size 1414 MB, throughput 0.982901
Recommendation: one client; give it all the memory
Reading from 32228: heap size 1419 MB, throughput 0.983447
Reading from 32228: heap size 1428 MB, throughput 0.981294
Recommendation: one client; give it all the memory
Reading from 32228: heap size 1434 MB, throughput 0.981419
Reading from 32228: heap size 1444 MB, throughput 0.97961
Recommendation: one client; give it all the memory
Reading from 32228: heap size 1453 MB, throughput 0.979803
Reading from 32228: heap size 1459 MB, throughput 0.979788
Recommendation: one client; give it all the memory
Reading from 32228: heap size 1467 MB, throughput 0.980301
Reading from 32228: heap size 1470 MB, throughput 0.980688
Recommendation: one client; give it all the memory
Reading from 32228: heap size 1478 MB, throughput 0.979955
Reading from 32228: heap size 1479 MB, throughput 0.980436
Recommendation: one client; give it all the memory
Reading from 32228: heap size 1486 MB, throughput 0.980787
Reading from 32228: heap size 1487 MB, throughput 0.980217
Recommendation: one client; give it all the memory
Reading from 32228: heap size 1492 MB, throughput 0.980677
Reading from 32228: heap size 1493 MB, throughput 0.980085
Recommendation: one client; give it all the memory
Reading from 32228: heap size 1496 MB, throughput 0.827728
Recommendation: one client; give it all the memory
Reading from 32228: heap size 1636 MB, throughput 0.996318
Reading from 32228: heap size 1629 MB, throughput 0.994965
Recommendation: one client; give it all the memory
Reading from 32228: heap size 1642 MB, throughput 0.993693
Reading from 32228: heap size 1654 MB, throughput 0.992347
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 32228: heap size 1655 MB, throughput 0.991782
Recommendation: one client; give it all the memory
Reading from 32228: heap size 1648 MB, throughput 0.990401
Recommendation: one client; give it all the memory
Reading from 32228: heap size 1658 MB, throughput 0.979576
Reading from 32228: heap size 1681 MB, throughput 0.839675
Reading from 32228: heap size 1683 MB, throughput 0.753338
Reading from 32228: heap size 1710 MB, throughput 0.801546
Reading from 32228: heap size 1730 MB, throughput 0.776932
Reading from 32228: heap size 1771 MB, throughput 0.857698
Client 32228 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
