economemd
    total memory: 4461 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub2_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run05_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 30278: heap size 9 MB, throughput 0.992767
Clients: 1
Client 30278 has a minimum heap size of 276 MB
Reading from 30277: heap size 9 MB, throughput 0.992756
Clients: 2
Client 30277 has a minimum heap size of 1211 MB
Reading from 30277: heap size 9 MB, throughput 0.961895
Reading from 30278: heap size 9 MB, throughput 0.976708
Reading from 30277: heap size 9 MB, throughput 0.965317
Reading from 30278: heap size 9 MB, throughput 0.968649
Reading from 30277: heap size 9 MB, throughput 0.951828
Reading from 30278: heap size 9 MB, throughput 0.933691
Reading from 30277: heap size 11 MB, throughput 0.978709
Reading from 30278: heap size 11 MB, throughput 0.983966
Reading from 30278: heap size 11 MB, throughput 0.95797
Reading from 30277: heap size 11 MB, throughput 0.979925
Reading from 30278: heap size 17 MB, throughput 0.925154
Reading from 30277: heap size 17 MB, throughput 0.949563
Reading from 30278: heap size 17 MB, throughput 0.562018
Reading from 30277: heap size 17 MB, throughput 0.486886
Reading from 30277: heap size 30 MB, throughput 0.962589
Reading from 30278: heap size 29 MB, throughput 0.958406
Reading from 30277: heap size 31 MB, throughput 0.868091
Reading from 30278: heap size 30 MB, throughput 0.95009
Reading from 30277: heap size 35 MB, throughput 0.530109
Reading from 30277: heap size 48 MB, throughput 0.916594
Reading from 30278: heap size 35 MB, throughput 0.442721
Reading from 30277: heap size 49 MB, throughput 0.882223
Reading from 30278: heap size 48 MB, throughput 0.922005
Reading from 30278: heap size 52 MB, throughput 0.375778
Reading from 30277: heap size 52 MB, throughput 0.23364
Reading from 30278: heap size 64 MB, throughput 0.723812
Reading from 30277: heap size 74 MB, throughput 0.684824
Reading from 30278: heap size 71 MB, throughput 0.307306
Reading from 30277: heap size 74 MB, throughput 0.342985
Reading from 30278: heap size 87 MB, throughput 0.722201
Reading from 30277: heap size 95 MB, throughput 0.822828
Reading from 30278: heap size 96 MB, throughput 0.631058
Reading from 30277: heap size 98 MB, throughput 0.755681
Reading from 30277: heap size 99 MB, throughput 0.236453
Reading from 30277: heap size 126 MB, throughput 0.768703
Reading from 30278: heap size 97 MB, throughput 0.181025
Reading from 30277: heap size 129 MB, throughput 0.805982
Reading from 30278: heap size 134 MB, throughput 0.699932
Reading from 30278: heap size 134 MB, throughput 0.26492
Reading from 30277: heap size 131 MB, throughput 0.146545
Reading from 30277: heap size 170 MB, throughput 0.69589
Reading from 30278: heap size 172 MB, throughput 0.790705
Reading from 30277: heap size 171 MB, throughput 0.720125
Reading from 30278: heap size 172 MB, throughput 0.737545
Reading from 30277: heap size 173 MB, throughput 0.760637
Reading from 30277: heap size 176 MB, throughput 0.633927
Reading from 30277: heap size 181 MB, throughput 0.657131
Reading from 30278: heap size 176 MB, throughput 0.243878
Reading from 30278: heap size 215 MB, throughput 0.700446
Reading from 30278: heap size 222 MB, throughput 0.71604
Reading from 30277: heap size 186 MB, throughput 0.19215
Reading from 30277: heap size 228 MB, throughput 0.530243
Reading from 30277: heap size 232 MB, throughput 0.552832
Reading from 30277: heap size 235 MB, throughput 0.645213
Reading from 30277: heap size 241 MB, throughput 0.581019
Reading from 30278: heap size 224 MB, throughput 0.951294
Reading from 30277: heap size 246 MB, throughput 0.656862
Reading from 30277: heap size 256 MB, throughput 0.690218
Reading from 30277: heap size 262 MB, throughput 0.514665
Reading from 30278: heap size 226 MB, throughput 0.36539
Reading from 30277: heap size 270 MB, throughput 0.0926573
Reading from 30278: heap size 279 MB, throughput 0.922047
Reading from 30277: heap size 313 MB, throughput 0.421759
Reading from 30278: heap size 277 MB, throughput 0.919989
Reading from 30278: heap size 279 MB, throughput 0.935065
Reading from 30277: heap size 318 MB, throughput 0.163908
Reading from 30278: heap size 276 MB, throughput 0.924247
Reading from 30277: heap size 360 MB, throughput 0.48812
Reading from 30277: heap size 362 MB, throughput 0.726948
Reading from 30278: heap size 279 MB, throughput 0.879693
Reading from 30277: heap size 357 MB, throughput 0.713747
Reading from 30278: heap size 283 MB, throughput 0.882884
Reading from 30277: heap size 360 MB, throughput 0.683952
Reading from 30278: heap size 285 MB, throughput 0.880846
Reading from 30277: heap size 361 MB, throughput 0.671601
Reading from 30278: heap size 289 MB, throughput 0.887131
Reading from 30277: heap size 365 MB, throughput 0.602856
Reading from 30277: heap size 367 MB, throughput 0.597301
Reading from 30277: heap size 376 MB, throughput 0.538528
Reading from 30277: heap size 380 MB, throughput 0.535943
Reading from 30277: heap size 391 MB, throughput 0.49592
Reading from 30278: heap size 291 MB, throughput 0.965458
Reading from 30277: heap size 397 MB, throughput 0.495415
Reading from 30278: heap size 292 MB, throughput 0.785762
Reading from 30277: heap size 404 MB, throughput 0.488788
Reading from 30278: heap size 295 MB, throughput 0.805621
Reading from 30278: heap size 301 MB, throughput 0.713554
Reading from 30278: heap size 302 MB, throughput 0.832923
Reading from 30278: heap size 308 MB, throughput 0.903394
Reading from 30278: heap size 309 MB, throughput 0.873525
Reading from 30277: heap size 408 MB, throughput 0.0566807
Reading from 30278: heap size 311 MB, throughput 0.600356
Reading from 30277: heap size 460 MB, throughput 0.364365
Reading from 30278: heap size 313 MB, throughput 0.732313
Reading from 30277: heap size 453 MB, throughput 0.416911
Reading from 30278: heap size 321 MB, throughput 0.637999
Reading from 30278: heap size 322 MB, throughput 0.588872
Reading from 30278: heap size 331 MB, throughput 0.721337
Equal recommendation: 2230 MB each
Reading from 30277: heap size 389 MB, throughput 0.0499621
Reading from 30277: heap size 506 MB, throughput 0.273586
Reading from 30277: heap size 511 MB, throughput 0.494302
Reading from 30278: heap size 332 MB, throughput 0.966822
Reading from 30277: heap size 504 MB, throughput 0.390791
Reading from 30277: heap size 448 MB, throughput 0.530209
Reading from 30277: heap size 496 MB, throughput 0.398297
Reading from 30277: heap size 501 MB, throughput 0.526788
Reading from 30277: heap size 498 MB, throughput 0.0963001
Reading from 30277: heap size 558 MB, throughput 0.407714
Reading from 30278: heap size 336 MB, throughput 0.9425
Reading from 30277: heap size 554 MB, throughput 0.57857
Reading from 30277: heap size 558 MB, throughput 0.626532
Reading from 30277: heap size 560 MB, throughput 0.63943
Reading from 30277: heap size 564 MB, throughput 0.594045
Reading from 30277: heap size 566 MB, throughput 0.532325
Reading from 30277: heap size 575 MB, throughput 0.504316
Reading from 30277: heap size 582 MB, throughput 0.531587
Reading from 30277: heap size 591 MB, throughput 0.510845
Reading from 30278: heap size 338 MB, throughput 0.95329
Reading from 30277: heap size 597 MB, throughput 0.0597969
Reading from 30277: heap size 664 MB, throughput 0.71701
Reading from 30278: heap size 338 MB, throughput 0.966568
Reading from 30277: heap size 674 MB, throughput 0.843642
Reading from 30277: heap size 676 MB, throughput 0.795821
Reading from 30278: heap size 341 MB, throughput 0.935952
Reading from 30277: heap size 678 MB, throughput 0.867955
Reading from 30277: heap size 689 MB, throughput 0.520258
Reading from 30277: heap size 697 MB, throughput 0.384714
Reading from 30278: heap size 342 MB, throughput 0.972548
Reading from 30277: heap size 705 MB, throughput 0.0571984
Reading from 30277: heap size 771 MB, throughput 0.410538
Reading from 30277: heap size 679 MB, throughput 0.348181
Reading from 30277: heap size 761 MB, throughput 0.40704
Equal recommendation: 2230 MB each
Reading from 30278: heap size 344 MB, throughput 0.962517
Reading from 30277: heap size 766 MB, throughput 0.0481301
Reading from 30277: heap size 843 MB, throughput 0.301873
Reading from 30277: heap size 844 MB, throughput 0.6822
Reading from 30277: heap size 850 MB, throughput 0.907129
Reading from 30277: heap size 850 MB, throughput 0.546172
Reading from 30277: heap size 842 MB, throughput 0.682676
Reading from 30277: heap size 846 MB, throughput 0.622365
Reading from 30278: heap size 344 MB, throughput 0.968614
Reading from 30277: heap size 848 MB, throughput 0.16371
Reading from 30277: heap size 934 MB, throughput 0.621174
Reading from 30277: heap size 936 MB, throughput 0.96563
Reading from 30277: heap size 942 MB, throughput 0.896008
Reading from 30278: heap size 345 MB, throughput 0.974194
Reading from 30277: heap size 944 MB, throughput 0.968277
Reading from 30277: heap size 946 MB, throughput 0.93337
Reading from 30277: heap size 936 MB, throughput 0.947513
Reading from 30277: heap size 763 MB, throughput 0.914861
Reading from 30277: heap size 912 MB, throughput 0.774884
Reading from 30277: heap size 805 MB, throughput 0.800985
Reading from 30277: heap size 896 MB, throughput 0.820682
Reading from 30278: heap size 349 MB, throughput 0.974408
Reading from 30277: heap size 809 MB, throughput 0.824815
Reading from 30277: heap size 884 MB, throughput 0.858431
Reading from 30277: heap size 814 MB, throughput 0.813218
Reading from 30277: heap size 875 MB, throughput 0.837518
Reading from 30277: heap size 818 MB, throughput 0.834583
Reading from 30277: heap size 869 MB, throughput 0.872105
Reading from 30277: heap size 875 MB, throughput 0.870125
Reading from 30277: heap size 867 MB, throughput 0.845716
Reading from 30277: heap size 871 MB, throughput 0.975164
Reading from 30278: heap size 349 MB, throughput 0.977833
Reading from 30277: heap size 867 MB, throughput 0.981339
Reading from 30278: heap size 352 MB, throughput 0.971847
Reading from 30277: heap size 822 MB, throughput 0.918499
Reading from 30277: heap size 865 MB, throughput 0.789198
Reading from 30277: heap size 870 MB, throughput 0.749442
Reading from 30277: heap size 875 MB, throughput 0.712622
Reading from 30277: heap size 879 MB, throughput 0.748026
Reading from 30277: heap size 885 MB, throughput 0.76349
Reading from 30277: heap size 887 MB, throughput 0.644117
Reading from 30277: heap size 894 MB, throughput 0.769702
Equal recommendation: 2230 MB each
Reading from 30277: heap size 894 MB, throughput 0.728409
Reading from 30278: heap size 353 MB, throughput 0.971302
Reading from 30277: heap size 901 MB, throughput 0.801666
Reading from 30277: heap size 902 MB, throughput 0.813882
Reading from 30277: heap size 907 MB, throughput 0.85306
Reading from 30277: heap size 908 MB, throughput 0.838198
Reading from 30277: heap size 911 MB, throughput 0.858059
Reading from 30277: heap size 913 MB, throughput 0.707228
Reading from 30277: heap size 913 MB, throughput 0.699844
Reading from 30277: heap size 916 MB, throughput 0.622378
Reading from 30277: heap size 926 MB, throughput 0.65703
Reading from 30277: heap size 933 MB, throughput 0.630564
Reading from 30277: heap size 944 MB, throughput 0.576057
Reading from 30277: heap size 950 MB, throughput 0.522875
Reading from 30278: heap size 356 MB, throughput 0.70938
Reading from 30277: heap size 963 MB, throughput 0.620562
Reading from 30277: heap size 968 MB, throughput 0.611304
Reading from 30277: heap size 982 MB, throughput 0.631411
Reading from 30277: heap size 986 MB, throughput 0.607673
Reading from 30277: heap size 1001 MB, throughput 0.622652
Reading from 30277: heap size 1005 MB, throughput 0.647611
Reading from 30278: heap size 395 MB, throughput 0.945229
Reading from 30278: heap size 403 MB, throughput 0.847058
Reading from 30278: heap size 403 MB, throughput 0.754589
Reading from 30278: heap size 409 MB, throughput 0.791334
Reading from 30278: heap size 409 MB, throughput 0.704561
Reading from 30277: heap size 1020 MB, throughput 0.957648
Reading from 30278: heap size 417 MB, throughput 0.986387
Reading from 30277: heap size 1024 MB, throughput 0.960984
Reading from 30278: heap size 417 MB, throughput 0.987917
Reading from 30277: heap size 1036 MB, throughput 0.951256
Equal recommendation: 2230 MB each
Reading from 30278: heap size 422 MB, throughput 0.989694
Reading from 30277: heap size 1039 MB, throughput 0.942378
Reading from 30278: heap size 424 MB, throughput 0.975411
Reading from 30277: heap size 1038 MB, throughput 0.951309
Reading from 30277: heap size 1043 MB, throughput 0.948709
Reading from 30278: heap size 424 MB, throughput 0.984385
Reading from 30277: heap size 1033 MB, throughput 0.956204
Reading from 30278: heap size 427 MB, throughput 0.986463
Reading from 30278: heap size 425 MB, throughput 0.969295
Reading from 30277: heap size 1040 MB, throughput 0.448821
Reading from 30278: heap size 428 MB, throughput 0.984289
Equal recommendation: 2230 MB each
Reading from 30277: heap size 1112 MB, throughput 0.931122
Reading from 30278: heap size 430 MB, throughput 0.984599
Reading from 30277: heap size 1119 MB, throughput 0.967327
Reading from 30278: heap size 431 MB, throughput 0.984049
Reading from 30277: heap size 1118 MB, throughput 0.966524
Reading from 30277: heap size 1121 MB, throughput 0.960727
Reading from 30278: heap size 434 MB, throughput 0.991234
Reading from 30278: heap size 435 MB, throughput 0.871253
Reading from 30278: heap size 434 MB, throughput 0.875726
Reading from 30278: heap size 436 MB, throughput 0.852359
Reading from 30277: heap size 1112 MB, throughput 0.972115
Reading from 30278: heap size 444 MB, throughput 0.989673
Reading from 30277: heap size 1057 MB, throughput 0.961986
Reading from 30278: heap size 445 MB, throughput 0.989281
Equal recommendation: 2230 MB each
Reading from 30277: heap size 1108 MB, throughput 0.961371
Reading from 30278: heap size 449 MB, throughput 0.989418
Reading from 30277: heap size 1112 MB, throughput 0.95533
Reading from 30278: heap size 451 MB, throughput 0.98848
Reading from 30277: heap size 1115 MB, throughput 0.956579
Reading from 30278: heap size 451 MB, throughput 0.986137
Reading from 30277: heap size 1115 MB, throughput 0.966723
Reading from 30277: heap size 1120 MB, throughput 0.953406
Reading from 30278: heap size 453 MB, throughput 0.971938
Equal recommendation: 2230 MB each
Reading from 30277: heap size 1122 MB, throughput 0.954051
Reading from 30278: heap size 453 MB, throughput 0.986499
Reading from 30277: heap size 1126 MB, throughput 0.960319
Reading from 30278: heap size 454 MB, throughput 0.984014
Reading from 30277: heap size 1129 MB, throughput 0.956769
Reading from 30278: heap size 456 MB, throughput 0.990381
Reading from 30278: heap size 457 MB, throughput 0.964929
Reading from 30278: heap size 456 MB, throughput 0.740947
Reading from 30277: heap size 1134 MB, throughput 0.950579
Reading from 30278: heap size 459 MB, throughput 0.88642
Reading from 30278: heap size 468 MB, throughput 0.990476
Reading from 30277: heap size 1138 MB, throughput 0.953164
Equal recommendation: 2230 MB each
Reading from 30277: heap size 1144 MB, throughput 0.959432
Reading from 30278: heap size 469 MB, throughput 0.991858
Reading from 30277: heap size 1147 MB, throughput 0.949653
Reading from 30278: heap size 474 MB, throughput 0.990016
Reading from 30277: heap size 1153 MB, throughput 0.960717
Reading from 30278: heap size 476 MB, throughput 0.989339
Reading from 30277: heap size 1155 MB, throughput 0.961492
Reading from 30278: heap size 476 MB, throughput 0.987999
Reading from 30277: heap size 1161 MB, throughput 0.952896
Equal recommendation: 2230 MB each
Reading from 30278: heap size 479 MB, throughput 0.98894
Reading from 30277: heap size 1163 MB, throughput 0.961631
Reading from 30278: heap size 480 MB, throughput 0.98739
Reading from 30277: heap size 1169 MB, throughput 0.523871
Reading from 30278: heap size 481 MB, throughput 0.990391
Reading from 30278: heap size 483 MB, throughput 0.959673
Reading from 30278: heap size 484 MB, throughput 0.896186
Reading from 30277: heap size 1268 MB, throughput 0.925378
Reading from 30278: heap size 488 MB, throughput 0.977345
Reading from 30277: heap size 1273 MB, throughput 0.968035
Equal recommendation: 2230 MB each
Reading from 30278: heap size 490 MB, throughput 0.991647
Reading from 30277: heap size 1275 MB, throughput 0.972078
Reading from 30278: heap size 494 MB, throughput 0.992236
Reading from 30277: heap size 1280 MB, throughput 0.969738
Reading from 30278: heap size 495 MB, throughput 0.991717
Reading from 30277: heap size 1281 MB, throughput 0.977211
Reading from 30278: heap size 494 MB, throughput 0.989396
Reading from 30277: heap size 1276 MB, throughput 0.973175
Equal recommendation: 2230 MB each
Reading from 30278: heap size 496 MB, throughput 0.988056
Reading from 30277: heap size 1281 MB, throughput 0.964993
Reading from 30277: heap size 1272 MB, throughput 0.964678
Reading from 30278: heap size 498 MB, throughput 0.988063
Reading from 30277: heap size 1277 MB, throughput 0.962736
Reading from 30278: heap size 498 MB, throughput 0.993657
Reading from 30278: heap size 500 MB, throughput 0.936477
Reading from 30278: heap size 501 MB, throughput 0.88201
Reading from 30277: heap size 1277 MB, throughput 0.961898
Reading from 30278: heap size 505 MB, throughput 0.990696
Equal recommendation: 2230 MB each
Reading from 30277: heap size 1279 MB, throughput 0.960218
Reading from 30278: heap size 507 MB, throughput 0.992844
Reading from 30277: heap size 1285 MB, throughput 0.968132
Reading from 30278: heap size 510 MB, throughput 0.991606
Reading from 30277: heap size 1287 MB, throughput 0.963143
Reading from 30278: heap size 512 MB, throughput 0.989334
Reading from 30277: heap size 1292 MB, throughput 0.957988
Equal recommendation: 2230 MB each
Reading from 30278: heap size 511 MB, throughput 0.98992
Reading from 30277: heap size 1297 MB, throughput 0.96572
Reading from 30278: heap size 513 MB, throughput 0.98793
Reading from 30278: heap size 515 MB, throughput 0.990714
Reading from 30278: heap size 515 MB, throughput 0.967504
Reading from 30278: heap size 516 MB, throughput 0.916211
Reading from 30278: heap size 518 MB, throughput 0.986381
Equal recommendation: 2230 MB each
Reading from 30278: heap size 525 MB, throughput 0.993268
Reading from 30278: heap size 525 MB, throughput 0.991061
Reading from 30277: heap size 1304 MB, throughput 0.989478
Reading from 30278: heap size 526 MB, throughput 0.991295
Equal recommendation: 2230 MB each
Reading from 30278: heap size 528 MB, throughput 0.990361
Reading from 30278: heap size 527 MB, throughput 0.988171
Reading from 30278: heap size 528 MB, throughput 0.994239
Reading from 30278: heap size 531 MB, throughput 0.929098
Reading from 30278: heap size 532 MB, throughput 0.929422
Equal recommendation: 2230 MB each
Reading from 30278: heap size 539 MB, throughput 0.994281
Reading from 30277: heap size 1310 MB, throughput 0.941041
Reading from 30278: heap size 539 MB, throughput 0.992273
Reading from 30278: heap size 540 MB, throughput 0.991585
Equal recommendation: 2230 MB each
Reading from 30277: heap size 1353 MB, throughput 0.992647
Reading from 30278: heap size 542 MB, throughput 0.990446
Reading from 30277: heap size 1370 MB, throughput 0.941641
Reading from 30277: heap size 1376 MB, throughput 0.869521
Reading from 30277: heap size 1375 MB, throughput 0.838858
Reading from 30277: heap size 1390 MB, throughput 0.811117
Reading from 30277: heap size 1393 MB, throughput 0.792203
Reading from 30277: heap size 1415 MB, throughput 0.836192
Reading from 30277: heap size 1417 MB, throughput 0.881479
Reading from 30278: heap size 542 MB, throughput 0.990691
Reading from 30277: heap size 1443 MB, throughput 0.979145
Reading from 30278: heap size 543 MB, throughput 0.993985
Reading from 30278: heap size 546 MB, throughput 0.94071
Reading from 30277: heap size 1444 MB, throughput 0.978334
Reading from 30278: heap size 547 MB, throughput 0.933118
Equal recommendation: 2230 MB each
Reading from 30277: heap size 1452 MB, throughput 0.977244
Reading from 30278: heap size 554 MB, throughput 0.994194
Reading from 30277: heap size 1459 MB, throughput 0.975177
Reading from 30278: heap size 554 MB, throughput 0.992498
Reading from 30277: heap size 1452 MB, throughput 0.975359
Reading from 30278: heap size 554 MB, throughput 0.992062
Equal recommendation: 2230 MB each
Reading from 30277: heap size 1462 MB, throughput 0.971499
Reading from 30278: heap size 556 MB, throughput 0.990474
Reading from 30277: heap size 1457 MB, throughput 0.972913
Reading from 30278: heap size 558 MB, throughput 0.990176
Reading from 30277: heap size 1463 MB, throughput 0.970828
Reading from 30278: heap size 559 MB, throughput 0.991818
Reading from 30278: heap size 561 MB, throughput 0.92452
Equal recommendation: 2230 MB each
Reading from 30278: heap size 564 MB, throughput 0.987059
Reading from 30277: heap size 1466 MB, throughput 0.973013
Reading from 30277: heap size 1471 MB, throughput 0.972432
Reading from 30278: heap size 570 MB, throughput 0.993354
Reading from 30277: heap size 1468 MB, throughput 0.970004
Reading from 30278: heap size 571 MB, throughput 0.992428
Equal recommendation: 2230 MB each
Reading from 30277: heap size 1472 MB, throughput 0.966293
Reading from 30278: heap size 570 MB, throughput 0.991989
Reading from 30277: heap size 1480 MB, throughput 0.973583
Reading from 30278: heap size 572 MB, throughput 0.9905
Reading from 30277: heap size 1484 MB, throughput 0.96446
Equal recommendation: 2230 MB each
Reading from 30278: heap size 574 MB, throughput 0.995647
Reading from 30278: heap size 575 MB, throughput 0.946322
Reading from 30278: heap size 574 MB, throughput 0.972602
Reading from 30277: heap size 1493 MB, throughput 0.962752
Reading from 30278: heap size 577 MB, throughput 0.993599
Reading from 30277: heap size 1502 MB, throughput 0.958876
Reading from 30278: heap size 579 MB, throughput 0.99291
Reading from 30277: heap size 1514 MB, throughput 0.963271
Equal recommendation: 2230 MB each
Reading from 30278: heap size 580 MB, throughput 0.992001
Reading from 30277: heap size 1525 MB, throughput 0.954992
Reading from 30278: heap size 580 MB, throughput 0.990809
Reading from 30277: heap size 1539 MB, throughput 0.962391
Equal recommendation: 2230 MB each
Reading from 30278: heap size 581 MB, throughput 0.99355
Reading from 30278: heap size 585 MB, throughput 0.940142
Reading from 30277: heap size 1546 MB, throughput 0.954983
Reading from 30278: heap size 585 MB, throughput 0.956126
Reading from 30277: heap size 1561 MB, throughput 0.96991
Reading from 30278: heap size 592 MB, throughput 0.99431
Reading from 30278: heap size 592 MB, throughput 0.990124
Reading from 30277: heap size 1566 MB, throughput 0.72385
Equal recommendation: 2230 MB each
Reading from 30278: heap size 592 MB, throughput 0.992991
Reading from 30277: heap size 1713 MB, throughput 0.990446
Reading from 30278: heap size 594 MB, throughput 0.99147
Reading from 30277: heap size 1717 MB, throughput 0.989134
Equal recommendation: 2230 MB each
Reading from 30277: heap size 1739 MB, throughput 0.983603
Reading from 30278: heap size 596 MB, throughput 0.995529
Reading from 30278: heap size 597 MB, throughput 0.945466
Reading from 30278: heap size 597 MB, throughput 0.987442
Reading from 30277: heap size 1745 MB, throughput 0.98397
Reading from 30278: heap size 600 MB, throughput 0.994235
Reading from 30277: heap size 1747 MB, throughput 0.981416
Equal recommendation: 2230 MB each
Reading from 30278: heap size 601 MB, throughput 0.993213
Reading from 30278: heap size 603 MB, throughput 0.992383
Reading from 30278: heap size 603 MB, throughput 0.990764
Equal recommendation: 2230 MB each
Reading from 30278: heap size 604 MB, throughput 0.991842
Reading from 30278: heap size 605 MB, throughput 0.921892
Client 30278 died
Clients: 1
Reading from 30277: heap size 1752 MB, throughput 0.994813
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 30277: heap size 1729 MB, throughput 0.990579
Reading from 30277: heap size 1742 MB, throughput 0.97468
Reading from 30277: heap size 1762 MB, throughput 0.848203
Reading from 30277: heap size 1776 MB, throughput 0.790189
Reading from 30277: heap size 1804 MB, throughput 0.824045
Client 30277 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
