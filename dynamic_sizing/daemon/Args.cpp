#include <Utilities.hpp>
#include <tclap/CmdLine.h>
#include "Args.hpp"
#include "ReadingCache.hpp"
#include "UtilityModel.hpp"

#define DEFAULT_TIMER_INTERVAL 10
#define DEFAULT_DUMP_FILE_NAME "economemd.dump.log"
#define DEFAULT_LOG_FILE_NAME "economemd.log"

class PositiveIntConstraint : public TCLAP::Constraint<unsigned int>
{
public:
    virtual std::string description() const { return "positive integer"; }
    virtual std::string shortID() const { return description(); }
    virtual bool check(const unsigned int& value) const { return value > 0; }
};

Args::Args(int argc, char** argv)
{
    try
    {
        PositiveIntConstraint positiveInt;

        std::vector<std::string> allModels = UtilityModel::allModelNames();
        TCLAP::ValuesConstraint<std::string> utilityConstraint(allModels);

        std::vector<std::string> allCaches = ReadingCache::allCacheNames();
        TCLAP::ValuesConstraint<std::string> cacheConstraint(allCaches);

        TCLAP::CmdLine cmd("economemd: the economic memory management daemon");


        TCLAP::UnlabeledValueArg<unsigned int> totalMemMB("total-mem-MB",
                                                          "total memory budget of the daemon's clients, in MB",
                                                          true,
                                                          1,
                                                          &positiveInt,
                                                          cmd);

        TCLAP::ValueArg<unsigned int> timerIntervalSeconds("t",
                                                           "timer-interval",
                                                           "recommendation timer interval in seconds",
                                                           false,
                                                           DEFAULT_TIMER_INTERVAL,
                                                           &positiveInt,
                                                           cmd);

        TCLAP::MultiSwitchArg paused("p",
                                     "start-paused",
                                     "start the recommendation timer in the paused state",
                                     cmd);

        TCLAP::ValueArg<std::string> utilityModel("u",
                                                  "utility-model",
                                                  "utility model for calculating functions based on readings; "
                                                  "defaults to " + UtilityModel::DEFAULT_UTILITY_MODEL,
                                                  false,
                                                  UtilityModel::DEFAULT_UTILITY_MODEL,
                                                  &utilityConstraint,
                                                  cmd);

        TCLAP::MultiArg<std::string> readingCaches("c",
                                                   "cache",
                                                   "add a reading cache; can specify many. The first is used to make "
                                                   "recommendations. If none specified, defaults to " + ReadingCache::DEFAULT_CACHE,
                                                   false,
                                                   &cacheConstraint,
                                                   cmd);

        TCLAP::MultiSwitchArg dontForceFunctionValidity("",
                                                        "dont-force-function-validity",
                                                        "don't force invalid functions to meet the model's constraints",
                                                        cmd);

        TCLAP::MultiSwitchArg alwaysNumeric("",
                                            "always-numeric",
                                            "always perform optimisation using numeric method, even when analytic is possible",
                                            cmd);

        TCLAP::MultiSwitchArg dontRecommend("",
                                            "dont-recommend",
                                            "don't make recommendations when the timer fires. Utility functions will "
                                            "still be calculated (useful together with --function-log-file). Note that "
                                            "manual recommendations from the console will still be sent.",
                                            cmd);

        TCLAP::MultiSwitchArg equalRecommendations("",
                                                   "equal-recommendations",
                                                   "always recommend (total memory)/(num clients) to all clients, rather than using economics",
                                                   cmd);

        TCLAP::MultiSwitchArg randomRecommendations("",
                                                    "random-recommendations",
                                                    "make random recommendations up to total memory, rather than using economics",
                                                    cmd);

        TCLAP::ValueArg<std::string> readingRecv("",
                                                 "reading-recv",
                                                 "socket name to listen for readings on",
                                                 false,
                                                 economem::NAME_DEFAULT,
                                                 "name",
                                                 cmd);

        TCLAP::ValueArg<std::string> commandRecv("",
                                                 "command-recv",
                                                 "socket name to listen for commands on",
                                                 false,
                                                 economem::NAME_DEFAULT,
                                                 "name",
                                                 cmd);

        TCLAP::ValueArg<std::string> dumpFile("",
                                              "dump-file",
                                              "file to dump internal state to when the 'dump' command is received",
                                              false,
                                              DEFAULT_DUMP_FILE_NAME,
                                              "filename",
                                              cmd);

        TCLAP::ValueArg<std::string> logFile("",
                                             "log-file",
                                             "specify the logging file; if empty, use '" DEFAULT_LOG_FILE_NAME "'. "
                                             "Logging must be turned on with --log-readings and/or --log-functions",
                                             false,
                                             DEFAULT_LOG_FILE_NAME,
                                             "filename",
                                             cmd);

        TCLAP::MultiSwitchArg logReadings("",
                                          "log-readings",
                                          "log readings and other events",
                                          cmd);

        TCLAP::MultiSwitchArg logFunctions("",
                                           "log-functions",
                                           "log all calculated utility functions",
                                           cmd);

        TCLAP::MultiSwitchArg verbose("v",
                                      "verbose",
                                      "display more output",
                                      cmd);

        cmd.parse(argc, argv);

        m_TotalMemMB = totalMemMB.getValue();
        m_TimerIntervalSeconds = timerIntervalSeconds.getValue();
        m_TimerRunsAtStart = paused.getValue() == 0;
        m_UtilityModel = utilityModel.getValue();

        m_ReadingCacheNames = readingCaches.getValue();
        if (m_ReadingCacheNames.empty())
            m_ReadingCacheNames.push_back(ReadingCache::DEFAULT_CACHE);

        m_ForceFunctionValidity = dontForceFunctionValidity.getValue() == 0;
        m_AlwaysNumericOptimisation = alwaysNumeric.getValue() > 0;
        m_MakeRecommendations = dontRecommend.getValue() == 0;
        m_EqualRecommendations = equalRecommendations.getValue() > 0;
        m_RandomRecommendations = randomRecommendations.getValue() > 0;

        m_ReadingReceiverName = readingRecv.getValue();
        m_CommandReceiverName = commandRecv.getValue();

        m_DumpFileName = dumpFile.getValue();
        m_LogFileName = logFile.getValue();
        m_LogReadings = logReadings.getValue() > 0;
        m_LogFunctions = logFunctions.getValue() > 0;

        if (m_EqualRecommendations && m_RandomRecommendations)
        {
            std::cerr << "Only one of --equal-recommendations and --random-recommendations may be specified" << std::endl;
            exit(1);
        }

        if (verbose.getValue() > 0)
        {
            economem::makeInfoVerbose();

            printf("economemd\n");
            printf("    total memory: %llu MB\n", m_TotalMemMB);
            printf("    timer interval: %llu s\n", m_TimerIntervalSeconds);
            printf("    timer start state: %s\n", m_TimerRunsAtStart ? "running" : "paused");
            printf("    utility model: %s\n", m_UtilityModel.c_str());
            printf("\n");
            printf("    caches:\n");
            for (const std::string& s : m_ReadingCacheNames)
                printf("        %s\n", s.c_str());
            printf("\n");
            printf("    force utility function validity: %s\n", m_ForceFunctionValidity ? "yes" : "no");
            printf("    always use numeric optimisation: %s\n", m_AlwaysNumericOptimisation ? "yes" : "no");
            printf("    make recommendations: %s\n", m_MakeRecommendations ? "yes" : "no");
            printf("    equal recommendations: %s\n", m_EqualRecommendations ? "yes" : "no");
            printf("    random recommendations: %s\n", m_RandomRecommendations ? "yes" : "no");
            printf("\n");
            printf("    reading receiver name: %s\n", m_ReadingReceiverName.c_str());
            printf("    command receiver name: %s\n", m_CommandReceiverName.c_str());
            printf("\n");
            printf("    dump file: %s\n", m_DumpFileName.c_str());
            printf("    log file: %s\n", m_LogFileName.c_str());
            printf("    log readings: %s\n", m_LogReadings ? "yes" : "no");
            printf("    log functions: %s\n", m_LogFunctions ? "yes" : "no");
            printf("\n");
        }
    }
    catch (TCLAP::ArgException& e)
    {
        std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl;
        exit(1);
    }
}

unsigned long long Args::totalMemMB() const
{
    return m_TotalMemMB;
}

unsigned long long Args::timerIntervalSeconds() const
{
    return m_TimerIntervalSeconds;
}

bool Args::timerRunsAtStart() const
{
    return m_TimerRunsAtStart;
}

const std::string& Args::utilityModel() const
{
    return m_UtilityModel;
}

const std::vector<std::string>& Args::readingCacheNames() const
{
    return m_ReadingCacheNames;
}

bool Args::forceFunctionValidity() const
{
    return m_ForceFunctionValidity;
}

bool Args::alwaysNumericOptimisation() const
{
    return m_AlwaysNumericOptimisation;
}

bool Args::makeRecommendations() const
{
    return m_MakeRecommendations;
}

bool Args::equalRecommendations() const
{
    return m_EqualRecommendations;
}

bool Args::randomRecommendations() const
{
    return m_RandomRecommendations;
}

const std::string& Args::readingReceiverName() const
{
    return m_ReadingReceiverName;
}

const std::string& Args::commandReceiverName() const
{
    return m_CommandReceiverName;
}

const std::string& Args::dumpFileName() const
{
    return m_DumpFileName;
}

const std::string& Args::logFileName() const
{
    return m_LogFileName;
}

bool Args::logReadings() const
{
    return m_LogReadings;
}

bool Args::logFunctions() const
{
    return m_LogFunctions;
}
