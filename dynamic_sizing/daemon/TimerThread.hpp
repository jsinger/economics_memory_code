#ifndef TIMERTHREAD_HPP
#define TIMERTHREAD_HPP

#include <cstdio>
#include <string>
#include <Log.hpp>
#include <RecommendationSender.hpp>
#include <Thread.hpp>

class ClientList;
class UtilityModel;

class TimerThread : public economem::Thread
{
public:
    TimerThread(unsigned long long totalMemMB,
                unsigned long long timerIntervalSeconds,
                bool timerRunsAtStart,
                bool makeRecommendations,
                bool equalRecommendations,
                bool randomRecommendations,
                economem::RecommendationSender& sender,
                UtilityModel& model,
                ClientList& clients,
                economem::Log& log);

    virtual ~TimerThread();

    bool pause();
    bool unpause();
    bool isPaused();
    unsigned long long getTimerIntervalSeconds();
    bool setTimerIntervalSeconds(unsigned long long t);
    unsigned long long getTotalMemMB();
    bool setTotalMemMB(unsigned long long m);

private:
    virtual void threadFunc();
    void doTimer();
    virtual void onKill() EXCLUSIVE_LOCKS_REQUIRED(m_Lock);

    unsigned long long m_TotalMemMB GUARDED_BY(m_Lock);
    unsigned long long m_TimerIntervalSeconds GUARDED_BY(m_Lock);
    bool m_Paused GUARDED_BY(m_Lock);
    pthread_cond_t m_Cond GUARDED_BY(m_Lock);
    economem::RecommendationSender& m_Sender;
    UtilityModel& m_Model;
    ClientList& m_Clients;
    economem::Log& m_Log;
    bool m_MakeRecommendations;
    bool m_EqualRecommendations;
    bool m_RandomRecommendations;
};

#endif
