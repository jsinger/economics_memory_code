module Economem.MinHeapAttempt (MinHeapAttemptDir(..),
                                newMinHeapAttempt,
                                resumeMinHeapAttempt)where

import Control.Applicative
import Control.Conditional(ifM)
import Control.Monad
import Economem.Common
import Economem.Benchmark
import Economem.Experiment
import Economem.Files
import Economem.Java
import Economem.Log
import qualified Economem.MetadataDir as M
import Economem.Process
import Economem.Records
import Economem.Run
import System.Directory


title :: String
title = "minHeapAttempt"


data MinHeapAttemptDir =
  MinHeapAttemptDir { dirPath :: FilePath,
                      benchmarkFile :: WholeFile BenchmarkThreadsMB,
                      javaNameFile :: WholeFile String,
                      iterationsFile :: IntFile,
                      auxiliaryBenchmarksFile :: CsvFile BenchmarkThreads,
                      pidFile :: IntFile,
                      exitCodeFile :: IntFile,
                      doesItCrashFile :: WholeFile Bool }

instance Path MinHeapAttemptDir where
  getPath = dirPath
instance Dir MinHeapAttemptDir where
  asDir path =
    MinHeapAttemptDir { dirPath = getPath path,
                        benchmarkFile = WholeFile (path </> "benchmark.txt"),
                        javaNameFile = WholeFile (path </> "java_name.txt"),
                        iterationsFile = WholeFile (path </> "iterations.txt"),
                        auxiliaryBenchmarksFile = CsvFile (path </> "auxiliary_benchmarks.csv"),
                        pidFile = WholeFile (path </> "pid.txt"),
                        exitCodeFile = WholeFile (path </> "exit_code.txt"),
                        doesItCrashFile = WholeFile (path </> "does_it_crash.txt") }

newMinHeapAttempt :: BenchmarkThreadsMB ->
                     Java ->
                     Integer ->
                     Maybe (Input BenchmarkThreads) ->
                     Maybe SubexpInfo ->
                     IO Experiment
newMinHeapAttempt bm j iterations auxBMLoader info = do
  (expDir, num, parentLog) <- newExperimentProperties title info
  makeMinHeapAttempt expDir num bm j iterations auxBMLoader parentLog

resumeMinHeapAttempt :: FilePath -> IO Experiment
resumeMinHeapAttempt =
  resumeExperiment
  (\expDir -> do let minHeapDir = asDir expDir
                 bm <- get (benchmarkFile minHeapDir)
                 java <- javaFromName =<< get (javaNameFile minHeapDir)
                 iterations <- get (iterationsFile minHeapDir)
                 auxBMs <- maybeGet (auxiliaryBenchmarksFile minHeapDir)
                 makeMinHeapAttempt expDir topLevel bm java iterations auxBMs Nothing)

makeMinHeapAttempt :: ExperimentDir ->
                      ExperimentNum ->
                      BenchmarkThreadsMB ->
                      Java ->
                      Integer ->
                      Maybe (Input BenchmarkThreads) ->
                      Maybe Log ->
                      IO Experiment
makeMinHeapAttempt expDir expNum bm j iterations auxBMLoader parentLog = do
  let ex = makeExperiment title expDir expNum 1 parentLog
  let minHeapDir = asDir expDir
  return (ex { experimentIntro = intro,
               saveAdditionalStaticMetadata = saveStatic minHeapDir,
               canResume = canResumeMinHeap minHeapDir,
               createRun = createRunMinHeap minHeapDir,
               cleanup = cleanupMinHeap minHeapDir ex })

  where
    intro :: [String]
    intro = ["Testing benchmark:",
             "    " ++ pretty (btmBenchmark bm),
             "At heap size:",
             "    " ++ pretty (DataSize (fromIntegral (btmMB bm)) MB)]

    saveStatic :: MinHeapAttemptDir -> IO ()
    saveStatic d = do
      bm |> benchmarkFile d
      javaName j |> javaNameFile d
      iterations |> iterationsFile d
      maybePut (auxiliaryBenchmarksFile d) auxBMLoader

    canResumeMinHeap :: MinHeapAttemptDir -> IO Bool
    canResumeMinHeap minHeapDir =
      and <$> mapM doesFileExist [getPath (benchmarkFile minHeapDir),
                                  getPath (javaNameFile minHeapDir),
                                  getPath (iterationsFile minHeapDir)]

    createRunMinHeap :: MinHeapAttemptDir -> Experiment -> RunNum -> IO Run
    createRunMinHeap d _ run = do
      let heapSize = DataSize (fromIntegral (btmMB bm)) MB
      bmDef <- (\b -> b  { benchmarkStartingHeap = heapSize }) <$> (makeBenchmark
                                                                    (BenchmarkThreadsIters
                                                                     (btmBenchmark bm)
                                                                     (fromIntegral iterations))
                                                                    heapSize)
      bmP <- (`addStartedAction` (SavePid (getPath (pidFile d))))
             . (`addCleanupAction` (SaveExitCode (getPath (exitCodeFile d))))
             <$> benchmarkProcessLauncher
             bmDef
             j
             (Just devnull)
             (Just (M.procOutputFile
                    (metadataDir expDir)
                    run
                    (makeNumAndMax 1 1)))
             (getPath d </> "working")
             (getPath d </> "scratch")
             Nothing
             Nothing
             Nothing
             []
             Nothing
             False
      auxBMPs <- makeAuxBMs
      watcherP <- makeWatcher (fromIntegral (length auxBMPs + 1))
      return Run { runNum = run,
                   runType = RunToCompletion,
                   mainProcesses = [bmP],
                   auxiliaryProcesses = auxBMPs ++ [watcherP],
                   auxiliaryProcessDelay = 10,
                   runSetupActions = [],
                   runCleanupActions = [] }

      where
        makeWatcher :: Integer -> IO ProcessLauncher
        makeWatcher i = do
          cmd <- (</> "min_heap" </> "kill_on_exception.sh") <$> measurementScriptsDir
          return ProcessLauncher { procCmd = cmd,
                                   procArgs = [getPath (pidFile d),
                                               getPath (M.procOutputFile
                                                        (metadataDir expDir)
                                                        run
                                                        (makeNumAndMax 1 1)),
                                               getPath (doesItCrashFile d)],
                                   procWorkingDir = getPath d,
                                   procExtraEnvironment = [],
                                   procInfile = Just devnull,
                                   procOutfile = Just (M.auxOutputFile
                                                       (metadataDir expDir)
                                                       run
                                                       (makeNumAndMax i i)),
                                   procSetupActions = [],
                                   procStartedActions = [],
                                   procCleanupActions = [] }

        makeAuxBMs :: IO [ProcessLauncher]
        makeAuxBMs =
          case auxBMLoader of
           Nothing -> return []
           Just loader -> do
             input <- loadInput loader
             forM (zip input [1..])
               (\(b, i) -> do maxHeap <- systemMemory
                              spec <- makeBenchmark (BenchmarkThreadsIters b 1) maxHeap
                              benchmarkProcessLauncher
                                spec
                                j
                                (Just devnull)
                                (Just (M.auxOutputFile
                                       (metadataDir expDir)
                                       run
                                       (makeNumAndMax i (fromIntegral (length input + 1)))))
                                (getPath d </> ("auxWorking" ++ show i))
                                (getPath d </> ("auxScratch" ++ show i))
                                Nothing
                                Nothing
                                Nothing
                                []
                                Nothing
                                False)

    cleanupMinHeap :: MinHeapAttemptDir -> Experiment -> IO ()
    cleanupMinHeap d e = do
      crashes <- ifM (exists (doesItCrashFile d))
                 (get (doesItCrashFile d))
                 (do exitCode <- get (exitCodeFile d)
                     let result = exitCode /= 0
                     put (doesItCrashFile d) result
                     return result)
      putLog (experimentLog e)
        ("Benchmark " ++
         (if crashes then
            "did not complete"
          else
            "completed") ++
         " successfully")
