economemd
    total memory: 2446 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub14_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 23653: heap size 9 MB, throughput 0.997947
Clients: 1
Client 23653 has a minimum heap size of 12 MB
Reading from 23652: heap size 9 MB, throughput 0.997873
Clients: 2
Client 23652 has a minimum heap size of 1211 MB
Reading from 23652: heap size 9 MB, throughput 0.97563
Reading from 23653: heap size 9 MB, throughput 0.994561
Reading from 23652: heap size 9 MB, throughput 0.959879
Reading from 23653: heap size 9 MB, throughput 0.958591
Reading from 23652: heap size 9 MB, throughput 0.955902
Reading from 23653: heap size 9 MB, throughput 0.957292
Reading from 23652: heap size 11 MB, throughput 0.982788
Reading from 23652: heap size 11 MB, throughput 0.975432
Reading from 23652: heap size 17 MB, throughput 0.923999
Reading from 23653: heap size 11 MB, throughput 0.989479
Reading from 23653: heap size 11 MB, throughput 0.956066
Reading from 23652: heap size 17 MB, throughput 0.763947
Reading from 23652: heap size 29 MB, throughput 0.961417
Reading from 23653: heap size 16 MB, throughput 0.959184
Reading from 23652: heap size 30 MB, throughput 0.874857
Reading from 23653: heap size 16 MB, throughput 0.965802
Reading from 23652: heap size 36 MB, throughput 0.365898
Reading from 23652: heap size 48 MB, throughput 0.883574
Reading from 23652: heap size 51 MB, throughput 0.397753
Reading from 23653: heap size 24 MB, throughput 0.988511
Reading from 23652: heap size 68 MB, throughput 0.879637
Reading from 23652: heap size 73 MB, throughput 0.328594
Reading from 23652: heap size 90 MB, throughput 0.775998
Reading from 23652: heap size 97 MB, throughput 0.825509
Reading from 23653: heap size 25 MB, throughput 0.855724
Reading from 23652: heap size 98 MB, throughput 0.247273
Reading from 23652: heap size 132 MB, throughput 0.789906
Reading from 23653: heap size 40 MB, throughput 0.99386
Reading from 23653: heap size 41 MB, throughput 0.962735
Reading from 23652: heap size 132 MB, throughput 0.206553
Reading from 23652: heap size 171 MB, throughput 0.87747
Reading from 23653: heap size 49 MB, throughput 0.987411
Reading from 23652: heap size 171 MB, throughput 0.777114
Reading from 23652: heap size 171 MB, throughput 0.747736
Reading from 23653: heap size 48 MB, throughput 0.988416
Reading from 23653: heap size 56 MB, throughput 0.985466
Reading from 23652: heap size 174 MB, throughput 0.142225
Reading from 23653: heap size 57 MB, throughput 0.991575
Reading from 23652: heap size 225 MB, throughput 0.678481
Reading from 23653: heap size 65 MB, throughput 0.988011
Reading from 23652: heap size 226 MB, throughput 0.761928
Reading from 23652: heap size 227 MB, throughput 0.733103
Reading from 23653: heap size 66 MB, throughput 0.987204
Reading from 23652: heap size 232 MB, throughput 0.780241
Reading from 23653: heap size 73 MB, throughput 0.984164
Reading from 23653: heap size 73 MB, throughput 0.983786
Reading from 23652: heap size 234 MB, throughput 0.161121
Reading from 23652: heap size 291 MB, throughput 0.687073
Reading from 23653: heap size 82 MB, throughput 0.994358
Reading from 23652: heap size 295 MB, throughput 0.784795
Reading from 23652: heap size 297 MB, throughput 0.731326
Reading from 23652: heap size 307 MB, throughput 0.813685
Reading from 23652: heap size 309 MB, throughput 0.714675
Reading from 23653: heap size 82 MB, throughput 0.99444
Reading from 23652: heap size 310 MB, throughput 0.137977
Reading from 23653: heap size 89 MB, throughput 0.995469
Reading from 23652: heap size 379 MB, throughput 0.660446
Reading from 23652: heap size 386 MB, throughput 0.612812
Reading from 23652: heap size 389 MB, throughput 0.62415
Reading from 23652: heap size 391 MB, throughput 0.606001
Reading from 23653: heap size 90 MB, throughput 0.995083
Reading from 23652: heap size 401 MB, throughput 0.53132
Reading from 23652: heap size 405 MB, throughput 0.52213
Reading from 23652: heap size 418 MB, throughput 0.451944
Reading from 23652: heap size 422 MB, throughput 0.398323
Reading from 23653: heap size 96 MB, throughput 0.99265
Reading from 23652: heap size 436 MB, throughput 0.377827
Reading from 23653: heap size 97 MB, throughput 0.98945
Reading from 23652: heap size 447 MB, throughput 0.0731044
Reading from 23652: heap size 516 MB, throughput 0.486654
Equal recommendation: 1223 MB each
Reading from 23652: heap size 503 MB, throughput 0.64179
Reading from 23652: heap size 425 MB, throughput 0.598714
Reading from 23653: heap size 103 MB, throughput 0.992714
Reading from 23652: heap size 497 MB, throughput 0.103156
Reading from 23653: heap size 103 MB, throughput 0.994935
Reading from 23652: heap size 498 MB, throughput 0.512911
Reading from 23652: heap size 566 MB, throughput 0.692261
Reading from 23652: heap size 569 MB, throughput 0.730464
Reading from 23652: heap size 558 MB, throughput 0.631322
Reading from 23652: heap size 564 MB, throughput 0.611576
Reading from 23653: heap size 108 MB, throughput 0.987984
Reading from 23652: heap size 565 MB, throughput 0.519139
Reading from 23652: heap size 566 MB, throughput 0.590425
Reading from 23652: heap size 570 MB, throughput 0.500762
Reading from 23652: heap size 580 MB, throughput 0.553616
Reading from 23652: heap size 586 MB, throughput 0.481965
Reading from 23653: heap size 109 MB, throughput 0.996074
Reading from 23652: heap size 598 MB, throughput 0.549022
Reading from 23652: heap size 603 MB, throughput 0.502633
Reading from 23652: heap size 613 MB, throughput 0.496703
Reading from 23652: heap size 619 MB, throughput 0.497308
Reading from 23652: heap size 625 MB, throughput 0.431384
Reading from 23653: heap size 115 MB, throughput 0.996045
Reading from 23653: heap size 115 MB, throughput 0.996196
Reading from 23652: heap size 629 MB, throughput 0.112407
Reading from 23653: heap size 120 MB, throughput 0.99748
Reading from 23652: heap size 703 MB, throughput 0.712092
Reading from 23652: heap size 700 MB, throughput 0.831914
Reading from 23653: heap size 121 MB, throughput 0.997311
Reading from 23652: heap size 704 MB, throughput 0.835664
Reading from 23652: heap size 702 MB, throughput 0.833566
Reading from 23653: heap size 124 MB, throughput 0.997837
Reading from 23652: heap size 705 MB, throughput 0.784258
Reading from 23653: heap size 125 MB, throughput 0.997103
Reading from 23653: heap size 128 MB, throughput 0.998097
Reading from 23652: heap size 711 MB, throughput 0.0622016
Reading from 23652: heap size 785 MB, throughput 0.333444
Reading from 23652: heap size 774 MB, throughput 0.695573
Reading from 23652: heap size 689 MB, throughput 0.42951
Reading from 23652: heap size 770 MB, throughput 0.445778
Reading from 23652: heap size 774 MB, throughput 0.476188
Reading from 23652: heap size 772 MB, throughput 0.517821
Reading from 23653: heap size 128 MB, throughput 0.998206
Reading from 23652: heap size 774 MB, throughput 0.0568685
Reading from 23653: heap size 131 MB, throughput 0.998326
Equal recommendation: 1223 MB each
Reading from 23652: heap size 854 MB, throughput 0.804008
Reading from 23652: heap size 854 MB, throughput 0.703872
Reading from 23652: heap size 857 MB, throughput 0.72182
Reading from 23652: heap size 858 MB, throughput 0.724978
Reading from 23652: heap size 858 MB, throughput 0.873187
Reading from 23653: heap size 131 MB, throughput 0.998177
Reading from 23652: heap size 860 MB, throughput 0.894384
Reading from 23652: heap size 866 MB, throughput 0.855269
Reading from 23652: heap size 867 MB, throughput 0.765538
Reading from 23652: heap size 865 MB, throughput 0.952984
Reading from 23653: heap size 134 MB, throughput 0.998631
Reading from 23652: heap size 868 MB, throughput 0.842387
Reading from 23652: heap size 860 MB, throughput 0.934411
Reading from 23652: heap size 745 MB, throughput 0.871799
Reading from 23653: heap size 134 MB, throughput 0.998112
Reading from 23652: heap size 844 MB, throughput 0.808213
Reading from 23652: heap size 766 MB, throughput 0.815611
Reading from 23652: heap size 838 MB, throughput 0.816821
Reading from 23653: heap size 135 MB, throughput 0.998128
Reading from 23653: heap size 135 MB, throughput 0.998309
Reading from 23652: heap size 844 MB, throughput 0.104709
Reading from 23652: heap size 913 MB, throughput 0.523102
Reading from 23652: heap size 920 MB, throughput 0.747444
Reading from 23653: heap size 137 MB, throughput 0.901683
Reading from 23652: heap size 918 MB, throughput 0.669311
Reading from 23653: heap size 141 MB, throughput 0.989661
Reading from 23652: heap size 920 MB, throughput 0.683566
Reading from 23652: heap size 920 MB, throughput 0.706675
Reading from 23653: heap size 144 MB, throughput 0.985098
Reading from 23652: heap size 922 MB, throughput 0.719895
Reading from 23653: heap size 144 MB, throughput 0.997472
Reading from 23652: heap size 922 MB, throughput 0.97148
Reading from 23653: heap size 151 MB, throughput 0.998086
Reading from 23652: heap size 925 MB, throughput 0.916002
Reading from 23652: heap size 924 MB, throughput 0.774141
Reading from 23652: heap size 940 MB, throughput 0.74768
Reading from 23653: heap size 152 MB, throughput 0.993594
Reading from 23652: heap size 960 MB, throughput 0.80908
Reading from 23652: heap size 960 MB, throughput 0.82993
Reading from 23652: heap size 957 MB, throughput 0.827424
Reading from 23652: heap size 961 MB, throughput 0.834607
Reading from 23652: heap size 958 MB, throughput 0.840984
Reading from 23653: heap size 160 MB, throughput 0.998056
Reading from 23652: heap size 962 MB, throughput 0.832682
Equal recommendation: 1223 MB each
Reading from 23652: heap size 961 MB, throughput 0.894218
Reading from 23652: heap size 965 MB, throughput 0.90353
Reading from 23653: heap size 160 MB, throughput 0.997905
Reading from 23652: heap size 964 MB, throughput 0.770017
Reading from 23653: heap size 165 MB, throughput 0.997929
Reading from 23652: heap size 968 MB, throughput 0.105976
Reading from 23652: heap size 1051 MB, throughput 0.513069
Reading from 23653: heap size 166 MB, throughput 0.997753
Reading from 23652: heap size 1071 MB, throughput 0.721476
Reading from 23652: heap size 1083 MB, throughput 0.790179
Reading from 23652: heap size 1083 MB, throughput 0.771678
Reading from 23652: heap size 1089 MB, throughput 0.76529
Reading from 23652: heap size 1091 MB, throughput 0.717241
Reading from 23652: heap size 1095 MB, throughput 0.767549
Reading from 23653: heap size 170 MB, throughput 0.998097
Reading from 23652: heap size 1099 MB, throughput 0.667755
Reading from 23652: heap size 1102 MB, throughput 0.757776
Reading from 23652: heap size 1106 MB, throughput 0.703836
Reading from 23653: heap size 171 MB, throughput 0.997212
Reading from 23653: heap size 177 MB, throughput 0.998537
Reading from 23652: heap size 1118 MB, throughput 0.970765
Reading from 23653: heap size 177 MB, throughput 0.997668
Reading from 23653: heap size 182 MB, throughput 0.997441
Reading from 23652: heap size 1119 MB, throughput 0.959863
Reading from 23653: heap size 182 MB, throughput 0.997353
Equal recommendation: 1223 MB each
Reading from 23652: heap size 1132 MB, throughput 0.953433
Reading from 23653: heap size 186 MB, throughput 0.998196
Reading from 23653: heap size 186 MB, throughput 0.998209
Reading from 23652: heap size 1133 MB, throughput 0.952411
Reading from 23653: heap size 191 MB, throughput 0.996939
Reading from 23653: heap size 191 MB, throughput 0.990825
Reading from 23653: heap size 195 MB, throughput 0.996551
Reading from 23652: heap size 1145 MB, throughput 0.956924
Reading from 23653: heap size 196 MB, throughput 0.997608
Reading from 23652: heap size 1151 MB, throughput 0.961301
Reading from 23653: heap size 203 MB, throughput 0.997488
Reading from 23653: heap size 204 MB, throughput 0.997383
Reading from 23652: heap size 1148 MB, throughput 0.962277
Reading from 23653: heap size 211 MB, throughput 0.997929
Equal recommendation: 1223 MB each
Reading from 23652: heap size 1153 MB, throughput 0.961659
Reading from 23653: heap size 211 MB, throughput 0.997598
Reading from 23653: heap size 217 MB, throughput 0.997461
Reading from 23652: heap size 1158 MB, throughput 0.964184
Reading from 23653: heap size 217 MB, throughput 0.997607
Reading from 23652: heap size 1159 MB, throughput 0.959431
Reading from 23653: heap size 224 MB, throughput 0.997743
Reading from 23653: heap size 224 MB, throughput 0.997584
Reading from 23652: heap size 1166 MB, throughput 0.965047
Reading from 23653: heap size 229 MB, throughput 0.998103
Reading from 23652: heap size 1168 MB, throughput 0.960046
Reading from 23653: heap size 230 MB, throughput 0.997889
Equal recommendation: 1223 MB each
Reading from 23653: heap size 236 MB, throughput 0.996857
Reading from 23653: heap size 236 MB, throughput 0.991743
Reading from 23652: heap size 1175 MB, throughput 0.954982
Reading from 23653: heap size 242 MB, throughput 0.997697
Reading from 23652: heap size 1179 MB, throughput 0.961691
Reading from 23653: heap size 242 MB, throughput 0.997827
Reading from 23653: heap size 250 MB, throughput 0.998002
Reading from 23652: heap size 1186 MB, throughput 0.9618
Reading from 23653: heap size 250 MB, throughput 0.997974
Reading from 23652: heap size 1191 MB, throughput 0.960648
Reading from 23653: heap size 256 MB, throughput 0.997992
Equal recommendation: 1223 MB each
Reading from 23653: heap size 257 MB, throughput 0.997804
Reading from 23653: heap size 264 MB, throughput 0.998468
Reading from 23652: heap size 1199 MB, throughput 0.569428
Reading from 23653: heap size 264 MB, throughput 0.997724
Reading from 23652: heap size 1273 MB, throughput 0.993126
Reading from 23653: heap size 270 MB, throughput 0.998031
Reading from 23652: heap size 1274 MB, throughput 0.990472
Reading from 23653: heap size 270 MB, throughput 0.997787
Reading from 23653: heap size 277 MB, throughput 0.99318
Reading from 23652: heap size 1280 MB, throughput 0.988203
Reading from 23653: heap size 277 MB, throughput 0.996936
Equal recommendation: 1223 MB each
Reading from 23653: heap size 287 MB, throughput 0.99785
Reading from 23652: heap size 1279 MB, throughput 0.986106
Reading from 23653: heap size 287 MB, throughput 0.99773
Reading from 23652: heap size 1160 MB, throughput 0.978459
Reading from 23653: heap size 295 MB, throughput 0.997958
Reading from 23652: heap size 1276 MB, throughput 0.978783
Reading from 23653: heap size 296 MB, throughput 0.998066
Reading from 23652: heap size 1177 MB, throughput 0.979202
Reading from 23653: heap size 303 MB, throughput 0.998119
Reading from 23652: heap size 1263 MB, throughput 0.975935
Equal recommendation: 1223 MB each
Reading from 23653: heap size 304 MB, throughput 0.998005
Reading from 23652: heap size 1193 MB, throughput 0.972476
Reading from 23653: heap size 310 MB, throughput 0.998161
Reading from 23653: heap size 311 MB, throughput 0.998106
Reading from 23652: heap size 1258 MB, throughput 0.953295
Reading from 23653: heap size 318 MB, throughput 0.897942
Reading from 23652: heap size 1208 MB, throughput 0.969338
Reading from 23653: heap size 325 MB, throughput 0.998989
Reading from 23652: heap size 1250 MB, throughput 0.967058
Reading from 23653: heap size 336 MB, throughput 0.999226
Equal recommendation: 1223 MB each
Reading from 23652: heap size 1250 MB, throughput 0.967672
Reading from 23653: heap size 336 MB, throughput 0.999119
Reading from 23652: heap size 1249 MB, throughput 0.963887
Reading from 23653: heap size 344 MB, throughput 0.999189
Reading from 23652: heap size 1252 MB, throughput 0.963502
Reading from 23653: heap size 346 MB, throughput 0.999034
Reading from 23652: heap size 1250 MB, throughput 0.95731
Reading from 23653: heap size 353 MB, throughput 0.999038
Reading from 23652: heap size 1256 MB, throughput 0.957742
Reading from 23653: heap size 354 MB, throughput 0.999024
Equal recommendation: 1223 MB each
Reading from 23653: heap size 360 MB, throughput 0.997161
Reading from 23652: heap size 1257 MB, throughput 0.95983
Reading from 23653: heap size 361 MB, throughput 0.997597
Reading from 23652: heap size 1259 MB, throughput 0.95797
Reading from 23653: heap size 370 MB, throughput 0.998619
Reading from 23652: heap size 1259 MB, throughput 0.958844
Reading from 23653: heap size 371 MB, throughput 0.99851
Reading from 23652: heap size 1206 MB, throughput 0.958585
Reading from 23653: heap size 380 MB, throughput 0.998585
Reading from 23652: heap size 1259 MB, throughput 0.959525
Equal recommendation: 1223 MB each
Reading from 23652: heap size 1203 MB, throughput 0.957597
Reading from 23653: heap size 380 MB, throughput 0.998405
Reading from 23652: heap size 1256 MB, throughput 0.960076
Reading from 23653: heap size 390 MB, throughput 0.998386
Reading from 23652: heap size 1201 MB, throughput 0.961494
Reading from 23653: heap size 390 MB, throughput 0.998277
Reading from 23653: heap size 399 MB, throughput 0.994591
Reading from 23652: heap size 1252 MB, throughput 0.960884
Equal recommendation: 1223 MB each
Reading from 23653: heap size 399 MB, throughput 0.998184
Reading from 23652: heap size 1200 MB, throughput 0.960737
Reading from 23653: heap size 414 MB, throughput 0.998285
Reading from 23652: heap size 1248 MB, throughput 0.494521
Reading from 23653: heap size 415 MB, throughput 0.998369
Reading from 23652: heap size 1303 MB, throughput 0.940539
Reading from 23653: heap size 426 MB, throughput 0.998549
Equal recommendation: 1223 MB each
Reading from 23653: heap size 427 MB, throughput 0.998347
Reading from 23653: heap size 438 MB, throughput 0.998373
Reading from 23653: heap size 439 MB, throughput 0.994899
Reading from 23653: heap size 450 MB, throughput 0.998425
Equal recommendation: 1223 MB each
Reading from 23653: heap size 451 MB, throughput 0.998602
Reading from 23653: heap size 463 MB, throughput 0.998413
Reading from 23652: heap size 1334 MB, throughput 0.992885
Reading from 23653: heap size 465 MB, throughput 0.99839
Reading from 23653: heap size 477 MB, throughput 0.99838
Equal recommendation: 1223 MB each
Reading from 23653: heap size 478 MB, throughput 0.995428
Reading from 23653: heap size 490 MB, throughput 0.998587
Reading from 23652: heap size 1340 MB, throughput 0.995302
Reading from 23653: heap size 491 MB, throughput 0.998708
Equal recommendation: 1223 MB each
Reading from 23653: heap size 505 MB, throughput 0.998461
Reading from 23652: heap size 1356 MB, throughput 0.988043
Reading from 23653: heap size 506 MB, throughput 0.998662
Reading from 23652: heap size 1357 MB, throughput 0.868501
Reading from 23652: heap size 1362 MB, throughput 0.752432
Reading from 23652: heap size 1368 MB, throughput 0.649281
Reading from 23652: heap size 1387 MB, throughput 0.666613
Reading from 23652: heap size 1401 MB, throughput 0.653712
Reading from 23652: heap size 1405 MB, throughput 0.664801
Reading from 23652: heap size 1413 MB, throughput 0.667427
Reading from 23652: heap size 1416 MB, throughput 0.665837
Reading from 23652: heap size 1335 MB, throughput 0.14055
Reading from 23652: heap size 1398 MB, throughput 0.991791
Reading from 23652: heap size 1402 MB, throughput 0.980296
Reading from 23653: heap size 518 MB, throughput 0.998471
Reading from 23652: heap size 1405 MB, throughput 0.987082
Reading from 23653: heap size 519 MB, throughput 0.979775
Reading from 23652: heap size 1137 MB, throughput 0.990934
Equal recommendation: 1223 MB each
Reading from 23652: heap size 1405 MB, throughput 0.986811
Reading from 23652: heap size 1146 MB, throughput 0.986327
Equal recommendation: 1223 MB each
Equal recommendation: 1223 MB each
Equal recommendation: 1223 MB each
Equal recommendation: 1223 MB each
Equal recommendation: 1223 MB each
Reading from 23653: heap size 532 MB, throughput 0.0585921
Equal recommendation: 1223 MB each
Equal recommendation: 1223 MB each
Equal recommendation: 1223 MB each
Equal recommendation: 1223 MB each
Equal recommendation: 1223 MB each
Equal recommendation: 1223 MB each
Equal recommendation: 1223 MB each
Equal recommendation: 1223 MB each
Equal recommendation: 1223 MB each
Equal recommendation: 1223 MB each
Reading from 23653: heap size 533 MB, throughput 0.996515
Equal recommendation: 1223 MB each
Reading from 23653: heap size 626 MB, throughput 0.993393
Reading from 23653: heap size 628 MB, throughput 0.994259
Equal recommendation: 1223 MB each
Reading from 23653: heap size 736 MB, throughput 0.998446
Reading from 23653: heap size 739 MB, throughput 0.998895
Equal recommendation: 1223 MB each
Reading from 23653: heap size 830 MB, throughput 0.998289
Equal recommendation: 1223 MB each
Reading from 23653: heap size 831 MB, throughput 0.999224
Reading from 23653: heap size 831 MB, throughput 0.452491
Equal recommendation: 1223 MB each
Reading from 23653: heap size 830 MB, throughput 0.999321
Reading from 23653: heap size 830 MB, throughput 0.998515
Equal recommendation: 1223 MB each
Reading from 23653: heap size 830 MB, throughput 0.999255
Reading from 23653: heap size 830 MB, throughput 0.949087
Equal recommendation: 1223 MB each
Reading from 23653: heap size 823 MB, throughput 0.995871
Reading from 23653: heap size 830 MB, throughput 0.948809
Equal recommendation: 1223 MB each
Reading from 23653: heap size 825 MB, throughput 0.954149
Reading from 23652: heap size 1404 MB, throughput 0.957615
Equal recommendation: 1223 MB each
Reading from 23653: heap size 829 MB, throughput 0.994194
Reading from 23652: heap size 1153 MB, throughput 0.907948
Reading from 23652: heap size 1406 MB, throughput 0.926979
Client 23653 died
Clients: 1
Reading from 23652: heap size 1161 MB, throughput 0.880767
Reading from 23652: heap size 1393 MB, throughput 0.801006
Recommendation: one client; give it all the memory
Reading from 23652: heap size 1181 MB, throughput 0.841107
Reading from 23652: heap size 1382 MB, throughput 0.817715
Reading from 23652: heap size 1377 MB, throughput 0.871881
Reading from 23652: heap size 1359 MB, throughput 0.796475
Reading from 23652: heap size 1372 MB, throughput 0.878497
Recommendation: one client; give it all the memory
Reading from 23652: heap size 1358 MB, throughput 0.746868
Reading from 23652: heap size 1370 MB, throughput 0.896163
Reading from 23652: heap size 1365 MB, throughput 0.778848
Recommendation: one client; give it all the memory
Reading from 23652: heap size 1376 MB, throughput 0.84443
Reading from 23652: heap size 1381 MB, throughput 0.756157
Reading from 23652: heap size 1389 MB, throughput 0.818778
Recommendation: one client; give it all the memory
Reading from 23652: heap size 1407 MB, throughput 0.726476
Recommendation: one client; give it all the memory
Reading from 23652: heap size 1412 MB, throughput 0.256718
Reading from 23652: heap size 1417 MB, throughput 0.938303
Recommendation: one client; give it all the memory
Reading from 23652: heap size 1420 MB, throughput 0.849542
Reading from 23652: heap size 1461 MB, throughput 0.959258
Recommendation: one client; give it all the memory
Reading from 23652: heap size 1462 MB, throughput 0.991568
Reading from 23652: heap size 1488 MB, throughput 0.992649
Recommendation: one client; give it all the memory
Reading from 23652: heap size 1496 MB, throughput 0.98946
Reading from 23652: heap size 1507 MB, throughput 0.947855
Recommendation: one client; give it all the memory
Reading from 23652: heap size 1517 MB, throughput 0.986188
Reading from 23652: heap size 1545 MB, throughput 0.925567
Recommendation: one client; give it all the memory
Reading from 23652: heap size 1550 MB, throughput 0.98242
Reading from 23652: heap size 1580 MB, throughput 0.980493
Recommendation: one client; give it all the memory
Reading from 23652: heap size 1592 MB, throughput 0.977547
Reading from 23652: heap size 1572 MB, throughput 0.978261
Reading from 23652: heap size 1572 MB, throughput 0.97625
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 23652: heap size 1532 MB, throughput 0.842957
Recommendation: one client; give it all the memory
Reading from 23652: heap size 1700 MB, throughput 0.989705
Reading from 23652: heap size 1603 MB, throughput 0.952491
Recommendation: one client; give it all the memory
Reading from 23652: heap size 1652 MB, throughput 0.829771
Reading from 23652: heap size 1618 MB, throughput 0.757132
Reading from 23652: heap size 1579 MB, throughput 0.757321
Reading from 23652: heap size 1616 MB, throughput 0.735414
Reading from 23652: heap size 1619 MB, throughput 0.74312
Reading from 23652: heap size 1619 MB, throughput 0.756955
Client 23652 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
TimerThread: finished
ReadingThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
