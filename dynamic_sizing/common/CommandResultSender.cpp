#include "CommandResultSender.hpp"
#include "Utilities.hpp"

economem::CommandResultSender::CommandResultSender() : SendSock() {}

bool economem::CommandResultSender::sendCommandResult(const economem::CommandResult& r, pid_t dest)
{
    return sendToClient(economem::TYPE_CMDRESULT, dest, &r, sizeof(r));
}
