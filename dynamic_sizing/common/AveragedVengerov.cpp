#include "AveragedVengerov.hpp"

#define AVG_WEIGHT 10

economem::AveragedVengerov::AveragedVengerov() : ThroughputCalculator(), m_AvgInitialOldUsedB(AVG_WEIGHT), m_AvgMinorGCTime(AVG_WEIGHT), m_AvgEdenFillTime(AVG_WEIGHT), m_AvgMinorPromoted(AVG_WEIGHT)
{
    m_MajorToMajorTimer.start();
    m_EdenFillTimer.start();
}

void economem::AveragedVengerov::gcBegin()
{
    m_GCTimer.start();

    m_GCStartHeapSizeB = getHeapSizeB();
    m_GCStartYoungSizeB = getYoungGenSizeB();
    m_GCStartOldUsedB = getOldGenSizeB();

    m_EdenFillTimer.stop();
    m_MutatorTime += m_EdenFillTimer.microseconds();
    m_AvgEdenFillTime.sample(m_EdenFillTimer.microseconds());
}

economem::ThroughputCalculator::Result economem::AveragedVengerov::minorGCEnd()
{
    m_GCTimer.stop();
    m_AvgMinorGCTime.sample(m_GCTimer.microseconds());

    m_AvgMinorPromoted.sample(getOldGenSizeB() - m_GCStartOldUsedB);

    double throughput = 1.0 / (1.0 + (((m_MajorGCTime * m_AvgMinorPromoted.average())/(m_GCStartHeapSizeB - m_GCStartYoungSizeB - m_AvgInitialOldUsedB.average()) + m_AvgMinorGCTime.average())/(m_AvgEdenFillTime.average())));

    logReading(m_GCStartHeapSizeB, throughput);

    m_EdenFillTimer.start();

    return economem::ThroughputCalculator::Result(m_GCStartHeapSizeB / (1024 * 1024), throughput);
}

economem::ThroughputCalculator::Result economem::AveragedVengerov::majorGCEnd()
{
    m_GCTimer.stop();
    m_MajorGCTime = m_GCTimer.microseconds();

    m_MajorToMajorTimer.stop();
    unsigned long long majorMajorTime = m_MajorToMajorTimer.microseconds();

    double throughput = (double)m_MutatorTime/(double)majorMajorTime;
    m_MutatorTime = 0;

    m_AvgInitialOldUsedB.sample(getOldGenSizeB());

    logReading(m_GCStartHeapSizeB, throughput);

    m_MajorToMajorTimer.start();
    m_EdenFillTimer.start();

    return economem::ThroughputCalculator::Result(m_GCStartHeapSizeB / (1024 * 1024), throughput);
}
