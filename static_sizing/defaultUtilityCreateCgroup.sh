#!/bin/bash

function usage()
{
    echo "Usage: $(basename "${0}") cgroup_path"
    exit 1
}

test -z "${1}" && usage

CGROUP_PATH="${1}"
GROUP='defaultUtility'

if [ ! -e "${CGROUP_PATH}/${GROUP}" ]; then
    sudo cgcreate -a "$(id -nu):$(id -ng)" -t "$(id -nu):$(id -ng)" -g "memory:${GROUP}"
fi
