#!/bin/bash
# Download and build OpenJDK 8

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
VM_DIR="${DIR}/openjdk8"

mkdir -p "${VM_DIR}" || exit 1

function download()
{
    if [ -e "${VM_DIR}/download.done" ]; then
        return 0
    else
        cd "${VM_DIR}" &&
        hg clone http://hg.openjdk.java.net/jdk8u/jdk8u jdk8 &&
        cd "${VM_DIR}/jdk8" &&
        bash ./get_source.sh &&
        sh ./common/bin/hgforest.sh checkout jdk8u20-b18 &&
        touch "${VM_DIR}/download.done" &&
        return 0

        return 1
    fi
}

function configure()
{
    if [ -e "${VM_DIR}/configure.done" ]; then
        return 0
    else
        cd "${VM_DIR}/jdk8" &&
        # Valid debug levels are release, fastdebug, slowdebug
        bash ./configure --prefix="${VM_DIR}/openjdk8-install" --with-jvm-variants=server --with-debug-level=release &&
        make clean &&
        touch "${VM_DIR}/configure.done" &&
        return 0

        return 1
    fi
}

function build()
{
    if [ -e "${VM_DIR}/install.done" ]; then
        return 0
    else
        "${DIR}/buildVM.sh" &&
        touch "${VM_DIR}/install.done" &&
        return 0

        return 1
    fi
}

download || exit 1
configure || exit 1
build || exit 1
exit 0
