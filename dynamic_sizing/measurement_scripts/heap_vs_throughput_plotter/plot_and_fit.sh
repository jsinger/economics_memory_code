#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") throughput_model total_mem [daemon_args...]"
    exit 1
}

test -z "${1}" && usage
test -z "${2}" && usage

THROUGHPUT_MODEL="${1}"
TOTAL_MEM="${2}"

function cleanup()
{
    test ! -z "${PLOT}" && kill "${PLOT}" &>/dev/null
    test ! -z "${TEE}" && kill "${TEE}" &>/dev/null
    test ! -z "${DAEMON}" && kill "${DAEMON}" &>/dev/null
    wait
    rm "${FIFO1}"
    rm "${FIFO2}"
    exit
}

trap "cleanup" SIGINT SIGHUP SIGTERM


FIFO1="$(mktemp --dry-run)"
mkfifo "${FIFO1}"

FIFO2="$(mktemp --dry-run)"
mkfifo "${FIFO2}"

python "${DIR}/demux_to_plotters.py" "${THROUGHPUT_MODEL}" < "${FIFO2}" &
PLOT="${!}"

tee "${FIFO2}" < "${FIFO1}" > "${DIR}/plot_and_fit.log" &
TEE="${!}"

"${DIR}/../../daemon/economemd" -c lru --reading-recv "${THROUGHPUT_MODEL}" --command-recv "${THROUGHPUT_MODEL}" --log-file "${FIFO1}" --log-readings --log-functions "${@:3}" "${TOTAL_MEM}" &
DAEMON="${!}"

wait
cleanup
