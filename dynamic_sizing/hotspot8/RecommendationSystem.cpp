#include "gc_implementation/shared/DaemonProxy.hpp"
#include "gc_implementation/shared/HotspotSimpleThroughput.hpp"
#include "gc_implementation/shared/HotspotNaiveVengerov.hpp"
#include "gc_implementation/shared/HotspotAveragedVengerov.hpp"
#include "gc_implementation/shared/HotspotMajorOnly.hpp"
#include "gc_implementation/shared/HotspotSimpleSmoothing.hpp"
#include "gc_implementation/shared/HotspotProportional.hpp"
#include "gc_implementation/shared/ReadingSender.hpp"
#include "gc_implementation/shared/RecommendationSystem.hpp"

/*
  To set up with a fresh copy of the HotSpot sources:

  Insert the following somewhere in the big macro in global.hpp

  product(uintx, EconomemMinHeap, 0,                                        \
          "Set the minimum heap size this VM is allowed, for use by the "   \
          "recommendation daemon")                                          \
                                                                            \
  product(bool, EconomemSimpleThroughput, false,                            \
          "Enable the simple throughput model for communication with the "  \
          "recommendation daemon")                                          \
                                                                            \
  product(bool, EconomemVengerovThroughput, true,                           \
          "Enable the Vengerov throughput model for communication with "    \
          "the recommendation daemon")                                      \
                                                                            \
  product(bool, EconomemAvgVengerovThroughput, false,                       \
          "Enable the averaged Vengerov throughput model for communication "\
          "with the recommendation daemon")                                 \
                                                                            \
  product(bool, EconomemMajorOnlyThroughput, false,                         \
          "Enable the major-only throughput model for communication with "  \
          "the recommendation daemon")                                      \
                                                                            \
  product(bool, EconomemSimpleSmoothingThroughput, false,                   \
          "Enable the simple smoothing throughput model for communication " \
          "with the recommendation daemon")                                 \
                                                                            \
  product(bool, EconomemProportionalThroughput, false,                      \
          "Enable the proportional throughput model for communication "     \
          "with the recommendation daemon")                                 \
                                                                            \




  Insert the following into PSAdaptiveSizePolicy constructor

/////////////////////////////////////////////@
  RecommendationSystem::init();
/////////////////////////////////////////////@



  Insert the following into PSAdaptiveSizePolicy::major_collection_begin

/////////////////////////////////////////////@
  RecommendationSystem::report_major_collection();
/////////////////////////////////////////////@



  Insert the following into PSAdaptiveSizePolicy::compute_eden_space_size, at the beginning of the 'which way should we go' if chain

/////////////////////////////////////////////@
  if (RecommendationSystem::should_adjust_eden_size()) {
    unsigned long long desired_sum = RecommendationSystem::recommendation_size_bytes();
    desired_eden_size = adjust_eden_for_footprint(desired_eden_size, desired_sum);
  }
  else
/////////////////////////////////////////////@



  Insert the following into PSAdaptiveSizePolicy::compute_old_gen_free_space, at the beginning of the 'which way should we go' if chain

/////////////////////////////////////////////@
  if (is_full_gc && RecommendationSystem::should_adjust_old_size()) {
    set_decide_at_full_gc(decide_at_full_gc_true);
    unsigned long long desired_sum = RecommendationSystem::recommendation_size_bytes();
    desired_promo_size = adjust_promo_for_footprint(desired_promo_size, desired_sum);
  }
  else
/////////////////////////////////////////////@



  Insert the following into each of psMarkSweep, psParallelCompact, and psScavenge, near the beginning of invoke, and change NAME appropriately

/////////////////////////////////////////////@
  RecommendationSystem::gc_begin("NAME");
/////////////////////////////////////////////@



  Insert the following into each of psMarkSweep, psParallelCompact, and psScavenge, near the end of invoke, and change NAME appropriately

/////////////////////////////////////////////@
  RecommendationSystem::gc_end("NAME");
/////////////////////////////////////////////@



  Each of the four files will also need a new include

#include "gc_implementation/shared/RecommendationSystem.hpp"

*/



bool RecommendationSystem::_has_recommendation = false;
economem::Recommendation RecommendationSystem::_recommendation;
bool RecommendationSystem::_collection_was_major = false;
unsigned long long RecommendationSystem::_heap_size_at_begin_collection = 0;
economem::DaemonProxy* RecommendationSystem::_proxy = NULL;


void RecommendationSystem::init()
{
    static bool once = true;
    if (once)
    {
        _proxy = new economem::DaemonProxy;

        if (EconomemSimpleThroughput)
            _proxy->addTarget(new HotspotSimpleThroughput, new economem::ReadingSender);

        if (EconomemVengerovThroughput)
            _proxy->addTarget(new HotspotNaiveVengerov, new economem::ReadingSender("vengerov"));

        if (EconomemAvgVengerovThroughput)
            _proxy->addTarget(new HotspotAveragedVengerov, new economem::ReadingSender("avgVengerov"));

        if (EconomemMajorOnlyThroughput)
            _proxy->addTarget(new HotspotMajorOnly, new economem::ReadingSender("majorOnly"));

        if (EconomemSimpleSmoothingThroughput)
            _proxy->addTarget(new HotspotSimpleSmoothing, new economem::ReadingSender("simpleSmoothing"));

        if (EconomemProportionalThroughput)
            _proxy->addTarget(new HotspotProportional, new economem::ReadingSender("proportional"));

        if (EconomemMinHeap > 0)
            _proxy->setMinHeapMB(EconomemMinHeap);

        once = false;
    }
}

void RecommendationSystem::gc_begin(const char* source)
{
    if (PrintGC)
        gclog_or_tty->print("################################################################################\n#INVOKE %s\n", source);

    _heap_size_at_begin_collection = ParallelScavengeHeap::heap()->capacity();
    _collection_was_major = false;
    if (_proxy->getRecommendation(_recommendation))
    {
        _has_recommendation = true;
        if (PrintGC)
            gclog_or_tty->print("# RECOMMENDATION: %llu MB\n", _recommendation.getHeapSizeMB());
    }
    _proxy->gcBegin();
}

void RecommendationSystem::gc_end(const char* source)
{
    if (PrintGC)
        gclog_or_tty->print("#END %s (%s):\n",
                            source,
                            _collection_was_major ? "major" : "minor");

    if (_collection_was_major)
        _proxy->majorGCEnd();
    else
        _proxy->minorGCEnd();

    if (PrintGC)
        gclog_or_tty->print("################################################################################\n");
}

void RecommendationSystem::report_major_collection()
{
    _collection_was_major = true;
}

bool RecommendationSystem::should_adjust_size(const char* name)
{
    if (PrintGC)
        gclog_or_tty->print("# CHECK RECOMMENDED SIZE %s...\n", name);

    bool adjust = _has_recommendation && _heap_size_at_begin_collection > _recommendation.getHeapSizeBytes();

    if (adjust && PrintGC)
        gclog_or_tty->print("# ADJUST %s FOR RECOMMENDED SIZE: %llu MB\n", name, _recommendation.getHeapSizeMB());

    return adjust;
}

bool RecommendationSystem::should_adjust_eden_size()
{
    return should_adjust_size("EDEN");
}

bool RecommendationSystem::should_adjust_old_size()
{
    return should_adjust_size("OLD");
}

unsigned long long::RecommendationSystem::recommendation_size_bytes()
{
    return _recommendation.getHeapSizeBytes();
}

void RecommendationSystem::daemon_proxy_cleanup()
{
    delete _proxy;
}
