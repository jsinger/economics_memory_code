#ifndef DAEMONPROXY_HPP
#define DAEMONPROXY_HPP

#include <cstddef>
#include <list>
#include "Messages.hpp"
#include "Mutex.hpp"
#include "ReceiverThread.hpp"
#include "RecommendationReceiver.hpp"
#include "ThroughputCalculator.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
    class ReadingSender;

//! Communicate with the heap sizing daemon.
/*! This is the main class through which clients communicate with the daemon. Hook the following sequence of events into your VM/application:

  1. Create a DaemonProxy instance at startup. Optionally report a minimum heap size for the client with setMinHeapMB().

  2. Call addTarget() at least once so the client can send readings to the daemon.

  3. When a GC begins, call gcBegin().

  4. When a GC ends, call the appropriate minorGCEnd() or majorGCEnd() method. This is when a reading will be sent to the daemon. If your client only has one kind of GC, pick one of these methods (doesn't matter which) and stick with it!

  5. The proxy receives recommendations in a background thread, so you can query it quickly at any time (e.g. when the client's heap-sizing code is invoked) using getRecommendation().

  6. Make sure you delete your proxy instance cleanly at shutdown -- the destructor sends the death message to the daemon, so if you don't do this, the daemon will think your client is still alive. The daemon will realise your client has died next time it tries to send a recommendation, but in the meantime it will consume memory, and slow down the redistribution of your client's memory to the other clients. More importantly, you will leave sockets lying around in `/tmp`, which might cause problems later if another client gets allocated the same PID -- so clean up properly! In particular, make sure to clean up properly when terminated by a signal.

  The system's idea of a GC is basically 'a pause in mutator execution during which some GC activity occurs'. Each such pause should be wrapped in EXACTLY ONE beginGC/endGC pair (doing otherwise can cause zero throughputs, incorrect results, and general unpleasantness), no matter what the client does internally during that time. This is particularly important when dealing with e.g. HotSpot, where a minor GC can be followed immediately by a major GC, within the same pause. This whole thing should be reported as a single major GC, surrounded by a beginGC call at the very beginning, and an endMajorGC call at the very end -- NOT as a minor GC followed by a major GC (even though you know that's what's going on internally).

  The proxy uses a ThroughputCalculator to work out the throughput readings to send to the daemon. Different algorithms are provided by SimpleThroughput, NaiveVengerov, and AveragedVengerov; you'll need to subclass one of these to work with your VM/application. */
    class DaemonProxy
    {
    public:
        DaemonProxy();
        ~DaemonProxy();

        //! Tell the proxy where to send readings, and how to calculate throughput.
        /*! Usually you should call this during the client's initialisation.

          Note that no communication takes place in this method; this just sets things up. The daemon won't know this client exists until one of the endGC methods is called.

          Takes ownership of both arguments, so you can do things like `addTarget(new MyThroughputCalculator, new ReadingSender)` and not have to worry about memory leaks.

          The proxy can send readings to more than one daemon, with different throughput models (that's how the measurement scripts can show a side-by-side comparison of the different models). Each call to this method adds such a (model, daemon) pair. Unless you want to do comparisons, though, you'll generally only want to call this once.

          If you do have multiple daemons running, make sure (using the appropriate command-line arguments to the daemon) that only one of them sends recommendations, or else things will get very confused very fast! */
        void addTarget(ThroughputCalculator* calc, ReadingSender* sender);

        //! Set the minimum heap size that this client is allowed to have.
        /*! Usually you should call this during the client's initialisation. The daemon will take this value into account when calculating recommendations.

          Note that no communication takes place in this method; this just sets things up. The daemon will be notified of the min heap immediately after the first reading has been sent, i.e. after the first endGC method call. Calling this after that message has been sent has no effect.

          If this method is never called, the daemon will assume a default minimum heap size for this client. */
        void setMinHeapMB(unsigned long long minHeapMB);

        //! Call this method at start of GC activity.
        /*! Should be called exactly once at the beginning of a mutator pause in which GC will happen -- see main class documentation for details. */
        void gcBegin();

        //! Call this method when minor GC completes.
        /*! This calculates throughput and sends a reading to each of the (model, daemon) pairs registered by addTarget().

          Should be called exactly once at the end of a mutator pause in which only minor GC activity has happened -- see main class documentation for details. */
        void minorGCEnd();

        //! Call this method when major GC completes.
        /*! This calculates throughput and sends a reading to each of the (model, daemon) pairs registered by addTarget().

          Should be called exactly once at the end of a mutator pause in which any major GC activity has happened (even if there was some minor GC as well) -- see main class documentation for details. */
        void majorGCEnd();

        //! Get a max heap size recommendation from the daemon.
        /*! Returns true and fills in \p r if there is a new recommendation; if no new recommendation, returns false and \p r unchanged.

          This should be fast; it just acquires a mutex and checks for a new recommendation. No communication with the daemon occurs -- that's done asynchronously in a background thread. */
        bool getRecommendation(economem::Recommendation& r);

    private:
        typedef ThroughputCalculator::Result (ThroughputCalculator::*GCEndFunc)(void);

        void sendDeath();
        void gcEnd(GCEndFunc func);
        void setRecommendation(const economem::Recommendation& r);

        unsigned long long m_MinHeapMB;

        economem::Mutex m_Lock;
        economem::Recommendation m_Recommendation GUARDED_BY(m_Lock);
        bool m_NewRecommendation GUARDED_BY(m_Lock);

        struct Target
        {
            Target(ThroughputCalculator* calc, ReadingSender* sender);
            ThroughputCalculator* calc;
            ReadingSender* sender;
            bool sentMinHeapMsg;
        };

        std::list<Target> m_Targets;

        class IncomingThread : public ReceiverThread<economem::Recommendation>
        {
        public:
            IncomingThread(DaemonProxy* parent, RecommendationReceiver& receiver);

        protected:
            virtual void received(const economem::Recommendation& msg);

        private:
            DaemonProxy* m_Parent;
        };

        RecommendationReceiver m_Receiver;
        IncomingThread m_ReceiverThread;

    };
}

/*! @} */

#endif
