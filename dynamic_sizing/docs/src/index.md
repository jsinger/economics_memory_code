Dynamic Heap Sizing                         {#mainpage}
===================

C++ API
-------

Everything in the \ref economem namespace (`common` folder).

### Communicating with the Daemon

If you want to make a program communicate with the daemon, start with economem::DaemonProxy and the classes referenced by it. These are a high-level wrapper that completely hide the details of the communications protocol.

### Communication Protocol

For the low-level details of the communication protocol, see \ref comms (but you don't need this if you just want to use the high-level wrappers).

### C++ Version

Code linked into clients must be C++98 compatible, and not use exceptions (these constraints are imposed by HotSpot's build system). The daemon and other independent programs are C++11.


Experiment Scripts
------------------

 - \ref heapVsThroughput
 - \ref behaviourPlotter
 - \ref minHeap
 - \ref benchmarkTimer

Programs
--------

 - Daemon
 - Interactive console
 - Fake VM
 - Reading tee
 - Reading pipe
 - GC Log parser
 - Hotspot 7
 - Hotspot 8
