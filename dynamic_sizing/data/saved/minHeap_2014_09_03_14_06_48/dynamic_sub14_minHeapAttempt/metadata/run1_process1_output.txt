GCBench: 1 threads, 3 iters



##########
GCBench iteration 1
##########

[0] Garbage Collector Test
[0]  Stretching memory with a binary tree of depth 22
 Total memory available=663224320 bytes  Free memory=649382904 bytes
[0]  Creating a long-lived binary tree of depth 22
[0]  Creating a long-lived array of 700000 doubles
 Total memory available=663224320 bytes  Free memory=384845592 bytes
Creating 8196 trees of depth 10
	Top down construction took 740msecs
	Bottom up construction took 281msecs
Creating 2048 trees of depth 12
	Top down construction took 92msecs
	Bottom up construction took 95msecs
Creating 512 trees of depth 14
	Top down construction took 93msecs
	Bottom up construction took 95msecs
Creating 128 trees of depth 16
	Top down construction took 100msecs
	Bottom up construction took 78msecs
Creating 32 trees of depth 18
	Top down construction took 94msecs
	Bottom up construction took 88msecs
Creating 8 trees of depth 20
	Top down construction took 132msecs
	Bottom up construction took 145msecs
Creating 2 trees of depth 22
[harness] Finished all threads


##########
GCBench iteration 2
##########

[0] Garbage Collector Test
[0java.lang.OutOfMemoryError: Java heap space] 
 Stretching memory with a binary tree of depth 22
 Total memory available=603979776 bytes  Free memory=600149592 bytes
	at GCBench.Populate(GCBench.java:72)
	at GCBench.Populate(GCBench.java:73)
	at GCBench.Populate(GCBench.java:73)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:73)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:73)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:73)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:73)
	at GCBench.Populate(GCBench.java:73)
	at GCBench.Populate(GCBench.java:73)
	at GCBench.Populate(GCBench.java:73)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.TimeConstruction(GCBench.java:108)
	at GCBenchRunner.run(GCBenchRunner.java:111)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)
	at java.lang.Thread.run(Thread.java:745)
