{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, DeriveDataTypeable #-}
{-# OPTIONS_GHC -fno-warn-missing-fields -fno-warn-missing-methods #-}

import Control.Applicative
import Economem.ArgHelper
import Economem.Benchmark
import Economem.BenchmarkIterationLength
import Economem.BenchmarkTimer
import Economem.Experiment
import Economem.Files
import Economem.Java
import Economem.Records
import Economem.Signals
import System.Console.CmdLib

defaultMaxThreads :: Int
defaultMaxThreads = 4


data Args = Args { ignoreFile :: FilePath,
                   iterations :: Int,
                   runs :: Int,
                   maxThreads :: Int,
                   java :: String }
          deriving (Typeable, Data, Eq, Show)

instance Attributes Args where
  attributes _ = group "Options" [
    ignoreFile %> [ Positional 0,
                    Required False,
                    Help "Filter out benchmarks that you don't want to measure",
                    ArgHelp "IGNORE_FILE" ],
    iterations %> [ Short ['i'],
                    Long ["iterations"],
                    Default (fromIntegral defaultIterations::Int),
                    Help "How many iterations to run each benchmark for, for taking the average",
                    ArgHelp "ITERATIONS" ],
    runs %> [ Short ['r'],
              Long ["runs"],
              Default (fromIntegral defaultRuns::Int),
              Help "How many times to repeat, for taking the average",
              ArgHelp "RUNS" ],
    maxThreads %> [ Short ['t'],
                    Long ["max-threads"],
                    Default defaultMaxThreads,
                    Help "Maximum number of threads to use with benchmarks",
                    ArgHelp "MAX_THREADS" ],
    java %> [ Short ['j'],
              Long ["java"],
              Default (javaName defaultJava),
              Help ("Java version to use; available options: " ++ show validJavaNames),
              ArgHelp "JAVA" ]]

instance RecordCommand Args where
  mode_summary _ = "Measure the length of one iteration for all benchmarks."

main :: IO ()
main = getArgs >>= executeR Args {} >>= \opts -> do
  (i, iters, r, m, j) <- validate opts
  ignored <- case i of
    Nothing -> return []
    Just f -> get (CsvFile f :: CsvFile BenchmarkFilter)
  bms <- (`benchmarksWithThreads` m) <$> (`filterBenchmarks` ignored) <$> allDacapoBenchmarks
  d <- newIterationLength (Raw bms) iters r j Nothing
  h <- makeSignalHandler
  runExperiment d h

  where validate :: Args -> IO (Maybe FilePath, Integer, Integer, Int, Java)
        validate a = do
          i <- if ignoreFile a == "" then return Nothing
               else Just <$> existingAbsoluteFile (ignoreFile a)
          iters <- positive (iterations a)
          r <- positive (runs a)
          m <- positive (maxThreads a)
          j <- javaFromName (java a)
          return (i, fromIntegral iters, fromIntegral r, m, j)
