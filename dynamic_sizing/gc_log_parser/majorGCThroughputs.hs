import EconoGCLog

main :: IO ()
main = interact (\s -> unlines (map (show . throughput) (majorOnly (parseGCs (lines s)))))
