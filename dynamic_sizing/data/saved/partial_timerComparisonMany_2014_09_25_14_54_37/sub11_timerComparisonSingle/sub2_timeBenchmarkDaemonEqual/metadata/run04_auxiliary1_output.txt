economemd
    total memory: 6095 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub11_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 6335: heap size 9 MB, throughput 0.983073
Clients: 1
Client 6335 has a minimum heap size of 8 MB
Reading from 6334: heap size 9 MB, throughput 0.991836
Clients: 2
Client 6334 has a minimum heap size of 1211 MB
Reading from 6334: heap size 9 MB, throughput 0.977252
Reading from 6335: heap size 9 MB, throughput 0.976808
Reading from 6334: heap size 9 MB, throughput 0.966058
Reading from 6335: heap size 11 MB, throughput 0.950424
Reading from 6335: heap size 11 MB, throughput 0.897775
Reading from 6334: heap size 9 MB, throughput 0.893605
Reading from 6334: heap size 11 MB, throughput 0.976328
Reading from 6335: heap size 15 MB, throughput 0.769168
Reading from 6334: heap size 11 MB, throughput 0.966344
Reading from 6334: heap size 17 MB, throughput 0.907425
Reading from 6335: heap size 20 MB, throughput 0.992253
Reading from 6335: heap size 23 MB, throughput 0.914689
Reading from 6334: heap size 17 MB, throughput 0.718825
Reading from 6335: heap size 28 MB, throughput 0.971568
Reading from 6335: heap size 29 MB, throughput 0.967396
Reading from 6335: heap size 28 MB, throughput 0.952149
Reading from 6335: heap size 31 MB, throughput 0.933274
Reading from 6334: heap size 29 MB, throughput 0.971118
Reading from 6335: heap size 31 MB, throughput 0.941677
Reading from 6334: heap size 30 MB, throughput 0.782646
Reading from 6335: heap size 34 MB, throughput 0.973613
Reading from 6335: heap size 35 MB, throughput 0.967913
Reading from 6335: heap size 38 MB, throughput 0.986998
Reading from 6334: heap size 35 MB, throughput 0.385073
Reading from 6335: heap size 38 MB, throughput 0.978063
Reading from 6334: heap size 46 MB, throughput 0.850446
Reading from 6335: heap size 41 MB, throughput 0.976631
Reading from 6335: heap size 42 MB, throughput 0.96961
Reading from 6335: heap size 44 MB, throughput 0.970643
Reading from 6335: heap size 44 MB, throughput 0.978629
Reading from 6335: heap size 47 MB, throughput 0.97471
Reading from 6334: heap size 48 MB, throughput 0.38959
Reading from 6335: heap size 47 MB, throughput 0.980505
Reading from 6335: heap size 50 MB, throughput 0.985923
Reading from 6334: heap size 63 MB, throughput 0.883098
Reading from 6335: heap size 50 MB, throughput 0.978165
Reading from 6335: heap size 53 MB, throughput 0.986048
Reading from 6335: heap size 53 MB, throughput 0.988429
Reading from 6334: heap size 70 MB, throughput 0.459355
Reading from 6335: heap size 54 MB, throughput 0.988444
Reading from 6334: heap size 86 MB, throughput 0.800449
Reading from 6335: heap size 55 MB, throughput 0.987594
Reading from 6335: heap size 57 MB, throughput 0.920759
Reading from 6334: heap size 95 MB, throughput 0.796977
Reading from 6335: heap size 57 MB, throughput 0.983572
Reading from 6335: heap size 60 MB, throughput 0.992037
Reading from 6335: heap size 60 MB, throughput 0.984693
Reading from 6334: heap size 95 MB, throughput 0.325107
Reading from 6335: heap size 63 MB, throughput 0.989048
Reading from 6334: heap size 129 MB, throughput 0.749485
Reading from 6335: heap size 63 MB, throughput 0.987856
Reading from 6335: heap size 65 MB, throughput 0.989194
Reading from 6335: heap size 65 MB, throughput 0.990836
Reading from 6335: heap size 67 MB, throughput 0.988029
Reading from 6334: heap size 129 MB, throughput 0.29416
Reading from 6335: heap size 67 MB, throughput 0.988332
Reading from 6334: heap size 168 MB, throughput 0.764724
Reading from 6335: heap size 69 MB, throughput 0.987817
Reading from 6334: heap size 168 MB, throughput 0.778819
Reading from 6335: heap size 69 MB, throughput 0.993796
Reading from 6335: heap size 70 MB, throughput 0.983963
Reading from 6334: heap size 169 MB, throughput 0.757337
Reading from 6335: heap size 71 MB, throughput 0.984145
Reading from 6334: heap size 173 MB, throughput 0.669426
Reading from 6335: heap size 72 MB, throughput 0.991462
Reading from 6335: heap size 72 MB, throughput 0.992367
Reading from 6335: heap size 74 MB, throughput 0.988858
Reading from 6334: heap size 176 MB, throughput 0.192865
Reading from 6335: heap size 74 MB, throughput 0.994923
Reading from 6335: heap size 75 MB, throughput 0.989911
Reading from 6334: heap size 226 MB, throughput 0.617041
Reading from 6335: heap size 75 MB, throughput 0.990157
Reading from 6334: heap size 230 MB, throughput 0.755798
Reading from 6335: heap size 77 MB, throughput 0.991867
Reading from 6335: heap size 77 MB, throughput 0.982436
Reading from 6334: heap size 232 MB, throughput 0.578994
Reading from 6335: heap size 78 MB, throughput 0.99177
Reading from 6334: heap size 242 MB, throughput 0.718522
Reading from 6335: heap size 78 MB, throughput 0.988278
Reading from 6335: heap size 80 MB, throughput 0.975853
Reading from 6334: heap size 245 MB, throughput 0.662651
Reading from 6335: heap size 80 MB, throughput 0.988947
Reading from 6335: heap size 82 MB, throughput 0.99273
Reading from 6335: heap size 82 MB, throughput 0.993976
Reading from 6335: heap size 83 MB, throughput 0.985548
Reading from 6335: heap size 84 MB, throughput 0.991158
Reading from 6335: heap size 85 MB, throughput 0.989981
Reading from 6334: heap size 249 MB, throughput 0.138371
Reading from 6335: heap size 85 MB, throughput 0.994057
Reading from 6334: heap size 306 MB, throughput 0.5999
Reading from 6335: heap size 87 MB, throughput 0.993926
Reading from 6335: heap size 87 MB, throughput 0.989702
Reading from 6334: heap size 311 MB, throughput 0.591175
Reading from 6335: heap size 88 MB, throughput 0.993886
Reading from 6334: heap size 317 MB, throughput 0.617529
Reading from 6335: heap size 88 MB, throughput 0.994241
Reading from 6334: heap size 319 MB, throughput 0.559911
Reading from 6335: heap size 90 MB, throughput 0.99541
Reading from 6335: heap size 90 MB, throughput 0.994874
Reading from 6335: heap size 91 MB, throughput 0.993407
Reading from 6335: heap size 91 MB, throughput 0.990082
Reading from 6334: heap size 332 MB, throughput 0.114114
Reading from 6335: heap size 92 MB, throughput 0.99348
Reading from 6335: heap size 92 MB, throughput 0.986685
Reading from 6334: heap size 394 MB, throughput 0.508669
Reading from 6335: heap size 93 MB, throughput 0.987007
Reading from 6334: heap size 398 MB, throughput 0.595848
Reading from 6335: heap size 93 MB, throughput 0.986164
Reading from 6334: heap size 399 MB, throughput 0.590561
Reading from 6335: heap size 95 MB, throughput 0.989682
Reading from 6334: heap size 401 MB, throughput 0.530489
Reading from 6335: heap size 95 MB, throughput 0.989144
Reading from 6334: heap size 407 MB, throughput 0.566704
Reading from 6335: heap size 97 MB, throughput 0.988558
Reading from 6335: heap size 97 MB, throughput 0.989916
Reading from 6335: heap size 99 MB, throughput 0.990875
Reading from 6335: heap size 99 MB, throughput 0.991088
Reading from 6334: heap size 412 MB, throughput 0.105668
Reading from 6335: heap size 101 MB, throughput 0.995604
Reading from 6335: heap size 101 MB, throughput 0.989335
Reading from 6334: heap size 475 MB, throughput 0.475034
Reading from 6335: heap size 103 MB, throughput 0.994588
Reading from 6334: heap size 478 MB, throughput 0.648172
Reading from 6335: heap size 103 MB, throughput 0.994698
Reading from 6334: heap size 480 MB, throughput 0.606419
Reading from 6335: heap size 104 MB, throughput 0.992383
Reading from 6335: heap size 104 MB, throughput 0.990219
Reading from 6334: heap size 481 MB, throughput 0.542583
Reading from 6335: heap size 106 MB, throughput 0.99435
Reading from 6335: heap size 106 MB, throughput 0.991489
Reading from 6334: heap size 486 MB, throughput 0.512504
Reading from 6335: heap size 107 MB, throughput 0.992295
Reading from 6334: heap size 494 MB, throughput 0.556372
Equal recommendation: 3047 MB each
Reading from 6335: heap size 107 MB, throughput 0.995525
Reading from 6334: heap size 500 MB, throughput 0.514417
Reading from 6335: heap size 109 MB, throughput 0.994388
Reading from 6335: heap size 109 MB, throughput 0.992358
Reading from 6335: heap size 110 MB, throughput 0.991255
Reading from 6335: heap size 110 MB, throughput 0.99209
Reading from 6335: heap size 112 MB, throughput 0.990773
Reading from 6335: heap size 112 MB, throughput 0.992765
Reading from 6335: heap size 113 MB, throughput 0.992469
Reading from 6334: heap size 508 MB, throughput 0.0865077
Reading from 6335: heap size 114 MB, throughput 0.994117
Reading from 6334: heap size 576 MB, throughput 0.447159
Reading from 6335: heap size 115 MB, throughput 0.995809
Reading from 6335: heap size 115 MB, throughput 0.991906
Reading from 6334: heap size 581 MB, throughput 0.514753
Reading from 6335: heap size 117 MB, throughput 0.99328
Reading from 6335: heap size 117 MB, throughput 0.992755
Reading from 6335: heap size 117 MB, throughput 0.992888
Reading from 6335: heap size 118 MB, throughput 0.993697
Reading from 6334: heap size 586 MB, throughput 0.131688
Reading from 6335: heap size 120 MB, throughput 0.995094
Reading from 6335: heap size 120 MB, throughput 0.987552
Reading from 6334: heap size 655 MB, throughput 0.501327
Reading from 6335: heap size 121 MB, throughput 0.987213
Reading from 6334: heap size 653 MB, throughput 0.673732
Reading from 6335: heap size 121 MB, throughput 0.989497
Reading from 6334: heap size 658 MB, throughput 0.612908
Reading from 6335: heap size 123 MB, throughput 0.990212
Reading from 6334: heap size 660 MB, throughput 0.543914
Reading from 6335: heap size 123 MB, throughput 0.993356
Reading from 6335: heap size 125 MB, throughput 0.99251
Reading from 6335: heap size 125 MB, throughput 0.989728
Reading from 6335: heap size 127 MB, throughput 0.994888
Reading from 6334: heap size 667 MB, throughput 0.844753
Reading from 6335: heap size 127 MB, throughput 0.993935
Reading from 6335: heap size 129 MB, throughput 0.995014
Reading from 6335: heap size 129 MB, throughput 0.992775
Reading from 6334: heap size 666 MB, throughput 0.802978
Reading from 6335: heap size 131 MB, throughput 0.995337
Reading from 6335: heap size 131 MB, throughput 0.994196
Reading from 6335: heap size 133 MB, throughput 0.99366
Reading from 6335: heap size 133 MB, throughput 0.988782
Reading from 6335: heap size 134 MB, throughput 0.994537
Reading from 6334: heap size 684 MB, throughput 0.825607
Reading from 6335: heap size 134 MB, throughput 0.991309
Reading from 6334: heap size 698 MB, throughput 0.525313
Reading from 6335: heap size 136 MB, throughput 0.934703
Reading from 6334: heap size 714 MB, throughput 0.540636
Reading from 6334: heap size 720 MB, throughput 0.238307
Reading from 6335: heap size 136 MB, throughput 0.995004
Reading from 6335: heap size 139 MB, throughput 0.994887
Reading from 6335: heap size 140 MB, throughput 0.99509
Reading from 6335: heap size 143 MB, throughput 0.99592
Reading from 6335: heap size 143 MB, throughput 0.995738
Reading from 6335: heap size 145 MB, throughput 0.9953
Reading from 6335: heap size 145 MB, throughput 0.996402
Reading from 6334: heap size 732 MB, throughput 0.0196605
Reading from 6335: heap size 147 MB, throughput 0.995425
Reading from 6334: heap size 814 MB, throughput 0.176947
Reading from 6334: heap size 686 MB, throughput 0.255604
Reading from 6335: heap size 147 MB, throughput 0.994146
Reading from 6335: heap size 149 MB, throughput 0.995697
Reading from 6335: heap size 149 MB, throughput 0.990219
Equal recommendation: 3047 MB each
Reading from 6334: heap size 793 MB, throughput 0.0400943
Reading from 6335: heap size 151 MB, throughput 0.995424
Reading from 6335: heap size 151 MB, throughput 0.996524
Reading from 6334: heap size 886 MB, throughput 0.722478
Reading from 6335: heap size 152 MB, throughput 0.996405
Reading from 6334: heap size 887 MB, throughput 0.569676
Reading from 6335: heap size 153 MB, throughput 0.988146
Reading from 6334: heap size 888 MB, throughput 0.611756
Reading from 6335: heap size 154 MB, throughput 0.990179
Reading from 6335: heap size 154 MB, throughput 0.993315
Reading from 6335: heap size 157 MB, throughput 0.995681
Reading from 6335: heap size 157 MB, throughput 0.98743
Reading from 6335: heap size 159 MB, throughput 0.992521
Reading from 6335: heap size 159 MB, throughput 0.995876
Reading from 6334: heap size 892 MB, throughput 0.04092
Reading from 6334: heap size 988 MB, throughput 0.331779
Reading from 6335: heap size 161 MB, throughput 0.997102
Reading from 6335: heap size 161 MB, throughput 0.996442
Reading from 6334: heap size 994 MB, throughput 0.925183
Reading from 6334: heap size 996 MB, throughput 0.779187
Reading from 6335: heap size 163 MB, throughput 0.996583
Reading from 6334: heap size 991 MB, throughput 0.893364
Reading from 6335: heap size 163 MB, throughput 0.996592
Reading from 6334: heap size 995 MB, throughput 0.95402
Reading from 6335: heap size 164 MB, throughput 0.904541
Reading from 6334: heap size 982 MB, throughput 0.860161
Reading from 6335: heap size 166 MB, throughput 0.994296
Reading from 6334: heap size 777 MB, throughput 0.891824
Reading from 6335: heap size 172 MB, throughput 0.995735
Reading from 6335: heap size 172 MB, throughput 0.993924
Reading from 6334: heap size 969 MB, throughput 0.833095
Reading from 6334: heap size 823 MB, throughput 0.6885
Reading from 6335: heap size 178 MB, throughput 0.996438
Reading from 6334: heap size 941 MB, throughput 0.727691
Reading from 6335: heap size 178 MB, throughput 0.99444
Reading from 6334: heap size 828 MB, throughput 0.726465
Reading from 6334: heap size 924 MB, throughput 0.751263
Reading from 6335: heap size 183 MB, throughput 0.995077
Reading from 6334: heap size 829 MB, throughput 0.740282
Reading from 6335: heap size 183 MB, throughput 0.994106
Reading from 6334: heap size 915 MB, throughput 0.762717
Reading from 6334: heap size 835 MB, throughput 0.739203
Reading from 6335: heap size 187 MB, throughput 0.997302
Reading from 6334: heap size 907 MB, throughput 0.777778
Reading from 6334: heap size 915 MB, throughput 0.748476
Reading from 6335: heap size 187 MB, throughput 0.997411
Reading from 6334: heap size 905 MB, throughput 0.796143
Reading from 6335: heap size 190 MB, throughput 0.995443
Reading from 6335: heap size 191 MB, throughput 0.99509
Reading from 6334: heap size 910 MB, throughput 0.96122
Reading from 6335: heap size 194 MB, throughput 0.995592
Reading from 6335: heap size 194 MB, throughput 0.994428
Reading from 6335: heap size 198 MB, throughput 0.995986
Reading from 6335: heap size 198 MB, throughput 0.99672
Reading from 6335: heap size 201 MB, throughput 0.996491
Reading from 6334: heap size 911 MB, throughput 0.961039
Reading from 6335: heap size 201 MB, throughput 0.996924
Reading from 6334: heap size 913 MB, throughput 0.702052
Reading from 6335: heap size 204 MB, throughput 0.997023
Reading from 6334: heap size 919 MB, throughput 0.739137
Reading from 6335: heap size 204 MB, throughput 0.996908
Reading from 6334: heap size 920 MB, throughput 0.736727
Reading from 6334: heap size 927 MB, throughput 0.7474
Reading from 6335: heap size 207 MB, throughput 0.997983
Reading from 6334: heap size 927 MB, throughput 0.755088
Reading from 6334: heap size 933 MB, throughput 0.76285
Reading from 6335: heap size 207 MB, throughput 0.99738
Reading from 6334: heap size 934 MB, throughput 0.75342
Equal recommendation: 3047 MB each
Reading from 6335: heap size 210 MB, throughput 0.997425
Reading from 6334: heap size 939 MB, throughput 0.770925
Reading from 6335: heap size 210 MB, throughput 0.997469
Reading from 6334: heap size 941 MB, throughput 0.811682
Reading from 6335: heap size 212 MB, throughput 0.998002
Reading from 6334: heap size 945 MB, throughput 0.801007
Reading from 6335: heap size 212 MB, throughput 0.996725
Reading from 6334: heap size 947 MB, throughput 0.856083
Reading from 6335: heap size 214 MB, throughput 0.996944
Reading from 6334: heap size 950 MB, throughput 0.660782
Reading from 6335: heap size 214 MB, throughput 0.995886
Reading from 6334: heap size 952 MB, throughput 0.575818
Reading from 6335: heap size 217 MB, throughput 0.99695
Reading from 6334: heap size 976 MB, throughput 0.587011
Reading from 6335: heap size 217 MB, throughput 0.995519
Reading from 6335: heap size 219 MB, throughput 0.996443
Reading from 6335: heap size 219 MB, throughput 0.996476
Reading from 6335: heap size 222 MB, throughput 0.990006
Reading from 6334: heap size 983 MB, throughput 0.0753842
Reading from 6335: heap size 222 MB, throughput 0.992153
Reading from 6334: heap size 1091 MB, throughput 0.506267
Reading from 6334: heap size 1092 MB, throughput 0.604239
Reading from 6335: heap size 225 MB, throughput 0.995798
Reading from 6334: heap size 1096 MB, throughput 0.590011
Reading from 6335: heap size 226 MB, throughput 0.996019
Reading from 6334: heap size 1099 MB, throughput 0.549526
Reading from 6335: heap size 229 MB, throughput 0.997215
Reading from 6334: heap size 1104 MB, throughput 0.557886
Reading from 6335: heap size 230 MB, throughput 0.994017
Reading from 6334: heap size 1108 MB, throughput 0.57791
Reading from 6335: heap size 234 MB, throughput 0.988988
Reading from 6335: heap size 234 MB, throughput 0.996558
Reading from 6335: heap size 238 MB, throughput 0.996983
Reading from 6335: heap size 239 MB, throughput 0.995347
Reading from 6334: heap size 1122 MB, throughput 0.932949
Reading from 6335: heap size 243 MB, throughput 0.995875
Reading from 6335: heap size 243 MB, throughput 0.995956
Reading from 6335: heap size 247 MB, throughput 0.995229
Reading from 6335: heap size 247 MB, throughput 0.996148
Reading from 6335: heap size 251 MB, throughput 0.997193
Reading from 6334: heap size 1124 MB, throughput 0.935281
Reading from 6335: heap size 251 MB, throughput 0.996598
Reading from 6335: heap size 254 MB, throughput 0.99629
Equal recommendation: 3047 MB each
Reading from 6335: heap size 255 MB, throughput 0.991845
Reading from 6335: heap size 258 MB, throughput 0.998011
Reading from 6335: heap size 258 MB, throughput 0.997281
Reading from 6334: heap size 1141 MB, throughput 0.929967
Reading from 6335: heap size 262 MB, throughput 0.997524
Reading from 6335: heap size 262 MB, throughput 0.996898
Reading from 6335: heap size 266 MB, throughput 0.99712
Reading from 6335: heap size 266 MB, throughput 0.997566
Reading from 6335: heap size 268 MB, throughput 0.997496
Reading from 6334: heap size 1143 MB, throughput 0.921395
Reading from 6335: heap size 269 MB, throughput 0.997784
Reading from 6335: heap size 272 MB, throughput 0.997319
Reading from 6335: heap size 272 MB, throughput 0.996179
Reading from 6335: heap size 274 MB, throughput 0.996842
Reading from 6335: heap size 275 MB, throughput 0.995695
Reading from 6334: heap size 1158 MB, throughput 0.934601
Reading from 6335: heap size 278 MB, throughput 0.996843
Reading from 6335: heap size 278 MB, throughput 0.996117
Reading from 6335: heap size 282 MB, throughput 0.99773
Reading from 6335: heap size 282 MB, throughput 0.995858
Reading from 6335: heap size 285 MB, throughput 0.996439
Reading from 6335: heap size 286 MB, throughput 0.994375
Reading from 6335: heap size 290 MB, throughput 0.996045
Reading from 6334: heap size 1164 MB, throughput 0.63141
Reading from 6335: heap size 290 MB, throughput 0.996461
Reading from 6335: heap size 294 MB, throughput 0.997597
Reading from 6335: heap size 294 MB, throughput 0.996376
Reading from 6335: heap size 298 MB, throughput 0.997901
Equal recommendation: 3047 MB each
Reading from 6334: heap size 1244 MB, throughput 0.987172
Reading from 6335: heap size 298 MB, throughput 0.996351
Reading from 6335: heap size 302 MB, throughput 0.997617
Reading from 6335: heap size 302 MB, throughput 0.996827
Reading from 6335: heap size 305 MB, throughput 0.998069
Reading from 6335: heap size 305 MB, throughput 0.997175
Reading from 6334: heap size 1245 MB, throughput 0.980232
Reading from 6335: heap size 309 MB, throughput 0.998439
Reading from 6335: heap size 309 MB, throughput 0.99773
Reading from 6335: heap size 312 MB, throughput 0.998143
Reading from 6335: heap size 312 MB, throughput 0.995599
Reading from 6335: heap size 313 MB, throughput 0.997125
Reading from 6334: heap size 1264 MB, throughput 0.979188
Reading from 6335: heap size 314 MB, throughput 0.996749
Reading from 6335: heap size 317 MB, throughput 0.996095
Reading from 6335: heap size 318 MB, throughput 0.996942
Reading from 6335: heap size 321 MB, throughput 0.997395
Reading from 6334: heap size 1267 MB, throughput 0.970679
Reading from 6335: heap size 322 MB, throughput 0.996068
Reading from 6335: heap size 326 MB, throughput 0.997154
Reading from 6335: heap size 326 MB, throughput 0.995318
Reading from 6335: heap size 330 MB, throughput 0.997816
Reading from 6335: heap size 330 MB, throughput 0.995579
Reading from 6334: heap size 1271 MB, throughput 0.965444
Reading from 6335: heap size 334 MB, throughput 0.997879
Reading from 6335: heap size 334 MB, throughput 0.996587
Equal recommendation: 3047 MB each
Reading from 6335: heap size 338 MB, throughput 0.997778
Reading from 6335: heap size 338 MB, throughput 0.995593
Reading from 6335: heap size 342 MB, throughput 0.968838
Reading from 6334: heap size 1274 MB, throughput 0.966263
Reading from 6335: heap size 347 MB, throughput 0.997489
Reading from 6335: heap size 351 MB, throughput 0.997877
Reading from 6335: heap size 351 MB, throughput 0.997106
Reading from 6335: heap size 356 MB, throughput 0.995541
Reading from 6335: heap size 356 MB, throughput 0.99612
Reading from 6334: heap size 1265 MB, throughput 0.963838
Reading from 6335: heap size 362 MB, throughput 0.997191
Reading from 6335: heap size 362 MB, throughput 0.995237
Reading from 6335: heap size 369 MB, throughput 0.99772
Reading from 6335: heap size 369 MB, throughput 0.995584
Reading from 6334: heap size 1272 MB, throughput 0.96684
Reading from 6335: heap size 375 MB, throughput 0.997064
Reading from 6335: heap size 375 MB, throughput 0.995691
Reading from 6335: heap size 382 MB, throughput 0.997273
Reading from 6335: heap size 382 MB, throughput 0.996217
Reading from 6334: heap size 1261 MB, throughput 0.956735
Reading from 6335: heap size 389 MB, throughput 0.997596
Reading from 6335: heap size 389 MB, throughput 0.996501
Reading from 6335: heap size 396 MB, throughput 0.997614
Equal recommendation: 3047 MB each
Reading from 6335: heap size 396 MB, throughput 0.995947
Reading from 6335: heap size 402 MB, throughput 0.99789
Reading from 6334: heap size 1266 MB, throughput 0.96553
Reading from 6335: heap size 402 MB, throughput 0.997703
Reading from 6335: heap size 408 MB, throughput 0.998925
Reading from 6335: heap size 408 MB, throughput 0.996676
Reading from 6335: heap size 414 MB, throughput 0.997271
Reading from 6334: heap size 1271 MB, throughput 0.959716
Reading from 6335: heap size 414 MB, throughput 0.997277
Reading from 6335: heap size 420 MB, throughput 0.998135
Reading from 6335: heap size 420 MB, throughput 0.99733
Reading from 6335: heap size 425 MB, throughput 0.997551
Reading from 6334: heap size 1272 MB, throughput 0.948289
Reading from 6335: heap size 425 MB, throughput 0.995877
Reading from 6335: heap size 431 MB, throughput 0.998238
Reading from 6335: heap size 431 MB, throughput 0.997266
Reading from 6334: heap size 1277 MB, throughput 0.946751
Reading from 6335: heap size 438 MB, throughput 0.998412
Reading from 6335: heap size 438 MB, throughput 0.996697
Reading from 6335: heap size 444 MB, throughput 0.997846
Equal recommendation: 3047 MB each
Reading from 6335: heap size 444 MB, throughput 0.996725
Reading from 6334: heap size 1283 MB, throughput 0.946702
Reading from 6335: heap size 450 MB, throughput 0.998279
Reading from 6335: heap size 450 MB, throughput 0.997027
Reading from 6335: heap size 456 MB, throughput 0.997899
Reading from 6335: heap size 456 MB, throughput 0.997216
Reading from 6334: heap size 1289 MB, throughput 0.942635
Reading from 6335: heap size 462 MB, throughput 0.997846
Reading from 6335: heap size 462 MB, throughput 0.997738
Reading from 6335: heap size 469 MB, throughput 0.997634
Reading from 6335: heap size 469 MB, throughput 0.996457
Reading from 6334: heap size 1298 MB, throughput 0.937153
Reading from 6335: heap size 475 MB, throughput 0.997769
Reading from 6335: heap size 475 MB, throughput 0.997524
Reading from 6335: heap size 482 MB, throughput 0.998044
Reading from 6335: heap size 482 MB, throughput 0.99716
Reading from 6334: heap size 1309 MB, throughput 0.938674
Reading from 6335: heap size 488 MB, throughput 0.997567
Reading from 6335: heap size 488 MB, throughput 0.997509
Equal recommendation: 3047 MB each
Reading from 6335: heap size 495 MB, throughput 0.998078
Reading from 6334: heap size 1317 MB, throughput 0.94856
Reading from 6335: heap size 495 MB, throughput 0.997523
Reading from 6335: heap size 502 MB, throughput 0.998213
Reading from 6335: heap size 502 MB, throughput 0.997621
Reading from 6335: heap size 508 MB, throughput 0.997996
Reading from 6334: heap size 1328 MB, throughput 0.938966
Reading from 6335: heap size 508 MB, throughput 0.997454
Reading from 6335: heap size 515 MB, throughput 0.9977
Reading from 6335: heap size 515 MB, throughput 0.997869
Reading from 6334: heap size 1333 MB, throughput 0.938987
Reading from 6335: heap size 521 MB, throughput 0.998408
Reading from 6335: heap size 521 MB, throughput 0.997698
Reading from 6335: heap size 527 MB, throughput 0.998003
Reading from 6335: heap size 527 MB, throughput 0.997334
Reading from 6334: heap size 1344 MB, throughput 0.934973
Reading from 6335: heap size 534 MB, throughput 0.998527
Equal recommendation: 3047 MB each
Reading from 6335: heap size 534 MB, throughput 0.997286
Reading from 6335: heap size 540 MB, throughput 0.998002
Reading from 6335: heap size 540 MB, throughput 0.997747
Reading from 6334: heap size 1347 MB, throughput 0.950236
Reading from 6335: heap size 547 MB, throughput 0.99825
Reading from 6335: heap size 547 MB, throughput 0.997308
Reading from 6335: heap size 553 MB, throughput 0.99814
Reading from 6334: heap size 1358 MB, throughput 0.944781
Reading from 6335: heap size 553 MB, throughput 0.998347
Reading from 6335: heap size 560 MB, throughput 0.998705
Reading from 6335: heap size 560 MB, throughput 0.997827
Reading from 6335: heap size 566 MB, throughput 0.993307
Reading from 6334: heap size 1360 MB, throughput 0.943319
Reading from 6335: heap size 566 MB, throughput 0.998123
Reading from 6335: heap size 575 MB, throughput 0.997941
Equal recommendation: 3047 MB each
Reading from 6335: heap size 576 MB, throughput 0.997726
Reading from 6334: heap size 1371 MB, throughput 0.941
Reading from 6335: heap size 583 MB, throughput 0.998294
Reading from 6335: heap size 583 MB, throughput 0.998015
Reading from 6335: heap size 591 MB, throughput 0.998276
Reading from 6335: heap size 591 MB, throughput 0.998057
Reading from 6334: heap size 1371 MB, throughput 0.946182
Reading from 6335: heap size 598 MB, throughput 0.998484
Reading from 6335: heap size 598 MB, throughput 0.998436
Reading from 6335: heap size 604 MB, throughput 0.998054
Reading from 6334: heap size 1380 MB, throughput 0.943645
Reading from 6335: heap size 604 MB, throughput 0.997812
Reading from 6335: heap size 611 MB, throughput 0.998279
Reading from 6335: heap size 611 MB, throughput 0.997554
Equal recommendation: 3047 MB each
Reading from 6335: heap size 617 MB, throughput 0.997459
Reading from 6335: heap size 617 MB, throughput 0.998547
Reading from 6334: heap size 1381 MB, throughput 0.527702
Reading from 6335: heap size 624 MB, throughput 0.998797
Reading from 6335: heap size 625 MB, throughput 0.997593
Reading from 6335: heap size 631 MB, throughput 0.998451
Reading from 6335: heap size 631 MB, throughput 0.998064
Reading from 6334: heap size 1501 MB, throughput 0.939029
Reading from 6335: heap size 638 MB, throughput 0.998336
Reading from 6335: heap size 638 MB, throughput 0.99789
Reading from 6335: heap size 645 MB, throughput 0.998559
Reading from 6334: heap size 1502 MB, throughput 0.978693
Reading from 6335: heap size 645 MB, throughput 0.998275
Equal recommendation: 3047 MB each
Reading from 6335: heap size 651 MB, throughput 0.998721
Reading from 6335: heap size 651 MB, throughput 0.998311
Reading from 6334: heap size 1519 MB, throughput 0.974566
Reading from 6335: heap size 657 MB, throughput 0.998644
Reading from 6335: heap size 657 MB, throughput 0.998228
Reading from 6335: heap size 663 MB, throughput 0.998826
Reading from 6335: heap size 663 MB, throughput 0.998007
Reading from 6334: heap size 1522 MB, throughput 0.958029
Reading from 6335: heap size 669 MB, throughput 0.998672
Reading from 6335: heap size 669 MB, throughput 0.998696
Reading from 6335: heap size 675 MB, throughput 0.998473
Reading from 6335: heap size 675 MB, throughput 0.998219
Equal recommendation: 3047 MB each
Reading from 6335: heap size 681 MB, throughput 0.998931
Reading from 6335: heap size 681 MB, throughput 0.998328
Reading from 6335: heap size 687 MB, throughput 0.998537
Reading from 6335: heap size 687 MB, throughput 0.99853
Reading from 6335: heap size 693 MB, throughput 0.998955
Reading from 6335: heap size 693 MB, throughput 0.998281
Reading from 6335: heap size 699 MB, throughput 0.998899
Reading from 6335: heap size 699 MB, throughput 0.998629
Reading from 6335: heap size 704 MB, throughput 0.998411
Reading from 6335: heap size 704 MB, throughput 0.998595
Equal recommendation: 3047 MB each
Reading from 6335: heap size 710 MB, throughput 0.998226
Reading from 6335: heap size 711 MB, throughput 0.998012
Reading from 6335: heap size 717 MB, throughput 0.99879
Reading from 6335: heap size 717 MB, throughput 0.998532
Reading from 6335: heap size 724 MB, throughput 0.998838
Reading from 6335: heap size 724 MB, throughput 0.998634
Reading from 6335: heap size 729 MB, throughput 0.998676
Reading from 6335: heap size 729 MB, throughput 0.998391
Reading from 6335: heap size 735 MB, throughput 0.998872
Equal recommendation: 3047 MB each
Reading from 6335: heap size 735 MB, throughput 0.998489
Reading from 6335: heap size 741 MB, throughput 0.998892
Reading from 6335: heap size 741 MB, throughput 0.998812
Reading from 6335: heap size 747 MB, throughput 0.998888
Reading from 6335: heap size 747 MB, throughput 0.998809
Reading from 6334: heap size 1523 MB, throughput 0.996214
Reading from 6335: heap size 752 MB, throughput 0.998958
Reading from 6335: heap size 752 MB, throughput 0.998325
Reading from 6335: heap size 758 MB, throughput 0.999046
Reading from 6335: heap size 758 MB, throughput 0.998693
Equal recommendation: 3047 MB each
Reading from 6335: heap size 763 MB, throughput 0.998577
Reading from 6335: heap size 763 MB, throughput 0.998624
Reading from 6335: heap size 769 MB, throughput 0.998779
Reading from 6334: heap size 1527 MB, throughput 0.980885
Reading from 6335: heap size 769 MB, throughput 0.998324
Reading from 6334: heap size 1512 MB, throughput 0.841589
Reading from 6334: heap size 1533 MB, throughput 0.669403
Reading from 6335: heap size 775 MB, throughput 0.998822
Reading from 6334: heap size 1551 MB, throughput 0.691523
Reading from 6334: heap size 1583 MB, throughput 0.63626
Reading from 6335: heap size 775 MB, throughput 0.995066
Reading from 6334: heap size 1618 MB, throughput 0.713652
Reading from 6334: heap size 1638 MB, throughput 0.737808
Reading from 6335: heap size 781 MB, throughput 0.999078
Reading from 6335: heap size 781 MB, throughput 0.998522
Reading from 6335: heap size 789 MB, throughput 0.998495
Equal recommendation: 3047 MB each
Reading from 6334: heap size 1659 MB, throughput 0.96872
Reading from 6335: heap size 789 MB, throughput 0.998848
Reading from 6335: heap size 797 MB, throughput 0.998842
Reading from 6334: heap size 1674 MB, throughput 0.953136
Reading from 6335: heap size 797 MB, throughput 0.998412
Reading from 6335: heap size 804 MB, throughput 0.998621
Reading from 6335: heap size 804 MB, throughput 0.998258
Reading from 6334: heap size 1663 MB, throughput 0.957803
Reading from 6335: heap size 810 MB, throughput 0.998907
Reading from 6335: heap size 811 MB, throughput 0.998531
Reading from 6335: heap size 817 MB, throughput 0.998817
Equal recommendation: 3047 MB each
Reading from 6334: heap size 1679 MB, throughput 0.951802
Reading from 6335: heap size 818 MB, throughput 0.998759
Reading from 6335: heap size 824 MB, throughput 0.998872
Reading from 6335: heap size 824 MB, throughput 0.998764
Reading from 6334: heap size 1676 MB, throughput 0.952373
Reading from 6335: heap size 830 MB, throughput 0.998667
Reading from 6335: heap size 830 MB, throughput 0.998737
Reading from 6335: heap size 836 MB, throughput 0.998907
Reading from 6334: heap size 1688 MB, throughput 0.957722
Reading from 6335: heap size 836 MB, throughput 0.998286
Reading from 6335: heap size 842 MB, throughput 0.998903
Equal recommendation: 3047 MB each
Reading from 6335: heap size 842 MB, throughput 0.99864
Reading from 6334: heap size 1684 MB, throughput 0.959152
Reading from 6335: heap size 849 MB, throughput 0.998781
Reading from 6335: heap size 849 MB, throughput 0.998794
Reading from 6335: heap size 855 MB, throughput 0.998681
Reading from 6335: heap size 855 MB, throughput 0.998968
Reading from 6334: heap size 1694 MB, throughput 0.742776
Reading from 6335: heap size 861 MB, throughput 0.999051
Reading from 6335: heap size 862 MB, throughput 0.998801
Equal recommendation: 3047 MB each
Reading from 6335: heap size 867 MB, throughput 0.999199
Reading from 6334: heap size 1683 MB, throughput 0.988517
Reading from 6335: heap size 867 MB, throughput 0.998418
Reading from 6335: heap size 873 MB, throughput 0.999077
Reading from 6335: heap size 873 MB, throughput 0.998435
Reading from 6334: heap size 1688 MB, throughput 0.984318
Reading from 6335: heap size 879 MB, throughput 0.999036
Reading from 6335: heap size 879 MB, throughput 0.998557
Reading from 6335: heap size 885 MB, throughput 0.998932
Reading from 6334: heap size 1717 MB, throughput 0.983115
Reading from 6335: heap size 885 MB, throughput 0.998704
Equal recommendation: 3047 MB each
Reading from 6335: heap size 891 MB, throughput 0.998731
Reading from 6335: heap size 891 MB, throughput 0.998739
Reading from 6334: heap size 1725 MB, throughput 0.979951
Reading from 6335: heap size 898 MB, throughput 0.998926
Reading from 6335: heap size 898 MB, throughput 0.99902
Reading from 6335: heap size 904 MB, throughput 0.998755
Reading from 6334: heap size 1733 MB, throughput 0.974948
Reading from 6335: heap size 904 MB, throughput 0.998962
Reading from 6335: heap size 910 MB, throughput 0.99911
Reading from 6335: heap size 910 MB, throughput 0.998755
Equal recommendation: 3047 MB each
Reading from 6335: heap size 916 MB, throughput 0.998939
Reading from 6334: heap size 1738 MB, throughput 0.978724
Reading from 6335: heap size 916 MB, throughput 0.998904
Reading from 6335: heap size 922 MB, throughput 0.999171
Reading from 6335: heap size 922 MB, throughput 0.998591
Reading from 6334: heap size 1723 MB, throughput 0.968763
Reading from 6335: heap size 928 MB, throughput 0.999131
Reading from 6335: heap size 928 MB, throughput 0.99892
Reading from 6335: heap size 934 MB, throughput 0.999013
Equal recommendation: 3047 MB each
Reading from 6334: heap size 1734 MB, throughput 0.968938
Reading from 6335: heap size 934 MB, throughput 0.998858
Reading from 6335: heap size 940 MB, throughput 0.999205
Reading from 6335: heap size 940 MB, throughput 0.998785
Reading from 6335: heap size 945 MB, throughput 0.996693
Reading from 6334: heap size 1713 MB, throughput 0.966462
Reading from 6335: heap size 945 MB, throughput 0.998996
Reading from 6335: heap size 953 MB, throughput 0.998637
Reading from 6335: heap size 954 MB, throughput 0.998916
Reading from 6334: heap size 1725 MB, throughput 0.969399
Equal recommendation: 3047 MB each
Reading from 6335: heap size 961 MB, throughput 0.999047
Reading from 6335: heap size 961 MB, throughput 0.999041
Reading from 6335: heap size 968 MB, throughput 0.998874
Reading from 6334: heap size 1734 MB, throughput 0.963582
Reading from 6335: heap size 968 MB, throughput 0.99891
Reading from 6335: heap size 975 MB, throughput 0.998945
Reading from 6335: heap size 975 MB, throughput 0.998926
Reading from 6334: heap size 1735 MB, throughput 0.962432
Reading from 6335: heap size 981 MB, throughput 0.998944
Equal recommendation: 3047 MB each
Reading from 6335: heap size 981 MB, throughput 0.998852
Reading from 6335: heap size 988 MB, throughput 0.999083
Reading from 6335: heap size 988 MB, throughput 0.999064
Reading from 6334: heap size 1748 MB, throughput 0.960944
Reading from 6335: heap size 994 MB, throughput 0.999264
Reading from 6335: heap size 994 MB, throughput 0.998725
Reading from 6335: heap size 999 MB, throughput 0.999059
Reading from 6334: heap size 1756 MB, throughput 0.955155
Equal recommendation: 3047 MB each
Reading from 6335: heap size 999 MB, throughput 0.999025
Reading from 6335: heap size 1005 MB, throughput 0.999061
Reading from 6335: heap size 1005 MB, throughput 0.99893
Reading from 6334: heap size 1772 MB, throughput 0.955065
Reading from 6335: heap size 1011 MB, throughput 0.999253
Reading from 6335: heap size 1011 MB, throughput 0.998759
Reading from 6335: heap size 1017 MB, throughput 0.999214
Reading from 6335: heap size 1017 MB, throughput 0.998992
Equal recommendation: 3047 MB each
Reading from 6335: heap size 1023 MB, throughput 0.999171
Reading from 6335: heap size 1023 MB, throughput 0.998892
Reading from 6335: heap size 1029 MB, throughput 0.999234
Reading from 6335: heap size 1029 MB, throughput 0.998686
Reading from 6335: heap size 1035 MB, throughput 0.999133
Reading from 6335: heap size 1035 MB, throughput 0.99876
Reading from 6335: heap size 1041 MB, throughput 0.999116
Equal recommendation: 3047 MB each
Reading from 6335: heap size 1041 MB, throughput 0.998796
Reading from 6335: heap size 1047 MB, throughput 0.999125
Reading from 6335: heap size 1047 MB, throughput 0.999029
Reading from 6335: heap size 1054 MB, throughput 0.999155
Reading from 6335: heap size 1054 MB, throughput 0.999014
Reading from 6335: heap size 1060 MB, throughput 0.999196
Equal recommendation: 3047 MB each
Reading from 6335: heap size 1060 MB, throughput 0.998965
Reading from 6335: heap size 1066 MB, throughput 0.999242
Reading from 6335: heap size 1066 MB, throughput 0.998886
Client 6335 died
Clients: 1
Reading from 6334: heap size 1787 MB, throughput 0.99442
Recommendation: one client; give it all the memory
Reading from 6334: heap size 1800 MB, throughput 0.964741
Reading from 6334: heap size 1824 MB, throughput 0.803924
Reading from 6334: heap size 1848 MB, throughput 0.824549
Client 6334 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
