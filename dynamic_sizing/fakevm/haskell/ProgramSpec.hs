module ProgramSpec where

import Data.List(find, nub)
import System.Random

data Program = Program { phases :: [Phase],
                         minHeapMB :: Int }
               deriving Show

data Phase = Phase { name :: String,
                     allocRateB :: Int,
                     lengthSeconds :: Double,
                     lenPercentError :: Int,
                     transitions :: [Transition] }
             deriving Show

data Transition = Transition { target :: String,
                               probability :: Int }
                  deriving Show


programIsValid :: Program -> Bool
programIsValid p = (not . null) (phases p) &&
                   minHeapMB p > 0 &&
                   all phaseIsValid (phases p) &&
                   allTransitionTargetsValid

  where phaseIsValid :: Phase -> Bool
        phaseIsValid ph = name ph /= "" &&
                          allocRateB ph > 0 &&
                          lengthSeconds ph > 0 &&
                          lenPercentError ph >= 0 &&
                          lenPercentError ph <= 100 &&
                          (not . null) (transitions ph) &&
                          all transitionIsValid (transitions ph) &&
                          sum (map probability (transitions ph)) == 100

        transitionIsValid :: Transition -> Bool
        transitionIsValid t = target t /= "" &&
                              probability t > 0 &&
                              probability t <= 100

        allTransitionTargetsValid :: Bool
        allTransitionTargetsValid = all transitionTargetValid allTransitionTargets

        transitionTargetValid :: String -> Bool
        transitionTargetValid t = t `elem` (map name (phases p))

        allTransitionTargets :: [String]
        allTransitionTargets = nub (concat (map (\ph -> map target (transitions ph)) (phases p)))


changeInSeconds :: Phase -> IO Double
changeInSeconds ph = do let percent = lenPercentError ph
                            len = lengthSeconds ph
                        r <- getStdRandom (randomR (-percent, percent))
                        if percent == 0
                          then return len
                          else return (len + (len * (fromIntegral r) / 100.0))


-- These must only be called with a validated program, or they could crash!
phaseByName :: Program -> String -> Phase
phaseByName p n = case find (\ph -> name ph == n) (phases p) of
  Just ph -> ph
  Nothing -> error "Invalid phase name specified"


initialPhase :: Program -> Phase
initialPhase p = head (phases p)


nextPhase :: Program -> Phase -> IO Phase
nextPhase p ph = do r <- getStdRandom (randomR (0, 99))
                    return (phaseByName p (probabilityList !! r))
  where probabilityList :: [String]
        probabilityList = concat (map (\t -> take (probability t) (repeat (target t))) (transitions ph))
