economemd
    total memory: 1656 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub21_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 23397: heap size 9 MB, throughput 0.99207
Clients: 1
Client 23397 has a minimum heap size of 276 MB
Reading from 23396: heap size 9 MB, throughput 0.982914
Clients: 2
Client 23396 has a minimum heap size of 276 MB
Reading from 23396: heap size 9 MB, throughput 0.977834
Reading from 23397: heap size 9 MB, throughput 0.978805
Reading from 23397: heap size 9 MB, throughput 0.966589
Reading from 23396: heap size 11 MB, throughput 0.975161
Reading from 23397: heap size 9 MB, throughput 0.957591
Reading from 23396: heap size 11 MB, throughput 0.982113
Reading from 23397: heap size 11 MB, throughput 0.952681
Reading from 23397: heap size 11 MB, throughput 0.977738
Reading from 23396: heap size 15 MB, throughput 0.776142
Reading from 23396: heap size 19 MB, throughput 0.957938
Reading from 23397: heap size 17 MB, throughput 0.926826
Reading from 23396: heap size 23 MB, throughput 0.957591
Reading from 23397: heap size 17 MB, throughput 0.454482
Reading from 23397: heap size 30 MB, throughput 0.944161
Reading from 23396: heap size 26 MB, throughput 0.372125
Reading from 23397: heap size 31 MB, throughput 0.892658
Reading from 23396: heap size 36 MB, throughput 0.954038
Reading from 23396: heap size 39 MB, throughput 0.6729
Reading from 23396: heap size 40 MB, throughput 0.764011
Reading from 23397: heap size 34 MB, throughput 0.338908
Reading from 23396: heap size 41 MB, throughput 0.778525
Reading from 23396: heap size 45 MB, throughput 0.887601
Reading from 23397: heap size 46 MB, throughput 0.884163
Reading from 23397: heap size 49 MB, throughput 0.883306
Reading from 23396: heap size 47 MB, throughput 0.191638
Reading from 23396: heap size 66 MB, throughput 0.712304
Reading from 23396: heap size 68 MB, throughput 0.797818
Reading from 23397: heap size 50 MB, throughput 0.347093
Reading from 23397: heap size 68 MB, throughput 0.72638
Reading from 23397: heap size 69 MB, throughput 0.729039
Reading from 23396: heap size 73 MB, throughput 0.245812
Reading from 23397: heap size 72 MB, throughput 0.642478
Reading from 23396: heap size 92 MB, throughput 0.709779
Reading from 23397: heap size 75 MB, throughput 0.719541
Reading from 23396: heap size 99 MB, throughput 0.84826
Reading from 23396: heap size 100 MB, throughput 0.796184
Reading from 23397: heap size 78 MB, throughput 0.158362
Reading from 23396: heap size 102 MB, throughput 0.68668
Reading from 23397: heap size 106 MB, throughput 0.717526
Reading from 23396: heap size 107 MB, throughput 0.596521
Reading from 23397: heap size 111 MB, throughput 0.808895
Reading from 23397: heap size 112 MB, throughput 0.594093
Reading from 23397: heap size 115 MB, throughput 0.654047
Reading from 23397: heap size 119 MB, throughput 0.543577
Reading from 23396: heap size 111 MB, throughput 0.129987
Reading from 23396: heap size 138 MB, throughput 0.524446
Reading from 23396: heap size 148 MB, throughput 0.742933
Reading from 23396: heap size 148 MB, throughput 0.456771
Reading from 23397: heap size 125 MB, throughput 0.141972
Reading from 23397: heap size 162 MB, throughput 0.517459
Reading from 23397: heap size 165 MB, throughput 0.69643
Reading from 23396: heap size 151 MB, throughput 0.174231
Reading from 23396: heap size 191 MB, throughput 0.511927
Reading from 23397: heap size 167 MB, throughput 0.180905
Reading from 23396: heap size 196 MB, throughput 0.724638
Reading from 23397: heap size 203 MB, throughput 0.686006
Reading from 23396: heap size 199 MB, throughput 0.708089
Reading from 23397: heap size 205 MB, throughput 0.781605
Reading from 23397: heap size 207 MB, throughput 0.690338
Reading from 23396: heap size 202 MB, throughput 0.874726
Reading from 23396: heap size 209 MB, throughput 0.888112
Reading from 23396: heap size 213 MB, throughput 0.691647
Reading from 23397: heap size 209 MB, throughput 0.919466
Reading from 23396: heap size 224 MB, throughput 0.626043
Reading from 23396: heap size 230 MB, throughput 0.667061
Reading from 23396: heap size 236 MB, throughput 0.670031
Reading from 23396: heap size 242 MB, throughput 0.415086
Reading from 23397: heap size 210 MB, throughput 0.338639
Reading from 23397: heap size 256 MB, throughput 0.609003
Reading from 23396: heap size 246 MB, throughput 0.125434
Reading from 23397: heap size 256 MB, throughput 0.887233
Reading from 23396: heap size 278 MB, throughput 0.46064
Reading from 23397: heap size 259 MB, throughput 0.70116
Reading from 23397: heap size 259 MB, throughput 0.864343
Reading from 23397: heap size 260 MB, throughput 0.792385
Reading from 23396: heap size 283 MB, throughput 0.291539
Reading from 23397: heap size 261 MB, throughput 0.901438
Reading from 23396: heap size 327 MB, throughput 0.811493
Reading from 23397: heap size 262 MB, throughput 0.788676
Reading from 23396: heap size 328 MB, throughput 0.852508
Reading from 23396: heap size 326 MB, throughput 0.662881
Reading from 23397: heap size 267 MB, throughput 0.656371
Reading from 23396: heap size 328 MB, throughput 0.831687
Reading from 23397: heap size 269 MB, throughput 0.780364
Reading from 23396: heap size 324 MB, throughput 0.847291
Reading from 23396: heap size 327 MB, throughput 0.612712
Reading from 23397: heap size 274 MB, throughput 0.754792
Reading from 23396: heap size 322 MB, throughput 0.845848
Reading from 23397: heap size 276 MB, throughput 0.80882
Reading from 23396: heap size 325 MB, throughput 0.97531
Reading from 23397: heap size 276 MB, throughput 0.940847
Reading from 23396: heap size 323 MB, throughput 0.900291
Reading from 23397: heap size 278 MB, throughput 0.764404
Reading from 23396: heap size 260 MB, throughput 0.662308
Reading from 23396: heap size 317 MB, throughput 0.728829
Reading from 23396: heap size 321 MB, throughput 0.79914
Reading from 23396: heap size 315 MB, throughput 0.855784
Reading from 23396: heap size 318 MB, throughput 0.918858
Reading from 23397: heap size 279 MB, throughput 0.0972877
Reading from 23396: heap size 314 MB, throughput 0.888608
Reading from 23396: heap size 317 MB, throughput 0.742662
Reading from 23397: heap size 321 MB, throughput 0.589535
Reading from 23396: heap size 319 MB, throughput 0.734202
Reading from 23397: heap size 326 MB, throughput 0.837431
Reading from 23396: heap size 319 MB, throughput 0.627603
Reading from 23396: heap size 324 MB, throughput 0.620004
Reading from 23397: heap size 327 MB, throughput 0.877038
Reading from 23396: heap size 324 MB, throughput 0.474547
Reading from 23396: heap size 331 MB, throughput 0.666372
Reading from 23397: heap size 331 MB, throughput 0.834618
Reading from 23396: heap size 331 MB, throughput 0.751517
Reading from 23397: heap size 332 MB, throughput 0.587292
Equal recommendation: 828 MB each
Reading from 23397: heap size 338 MB, throughput 0.560886
Reading from 23397: heap size 338 MB, throughput 0.598422
Reading from 23397: heap size 347 MB, throughput 0.597339
Reading from 23397: heap size 347 MB, throughput 0.537431
Reading from 23396: heap size 336 MB, throughput 0.973519
Reading from 23397: heap size 358 MB, throughput 0.956221
Reading from 23396: heap size 338 MB, throughput 0.975963
Reading from 23397: heap size 358 MB, throughput 0.958821
Reading from 23396: heap size 339 MB, throughput 0.971207
Reading from 23397: heap size 362 MB, throughput 0.958666
Reading from 23396: heap size 342 MB, throughput 0.97742
Reading from 23397: heap size 365 MB, throughput 0.96693
Reading from 23396: heap size 339 MB, throughput 0.970756
Reading from 23397: heap size 368 MB, throughput 0.98329
Reading from 23396: heap size 343 MB, throughput 0.981226
Reading from 23397: heap size 369 MB, throughput 0.980659
Reading from 23396: heap size 338 MB, throughput 0.977191
Equal recommendation: 828 MB each
Reading from 23397: heap size 368 MB, throughput 0.9752
Reading from 23396: heap size 341 MB, throughput 0.975799
Reading from 23396: heap size 340 MB, throughput 0.976811
Reading from 23397: heap size 371 MB, throughput 0.974995
Reading from 23396: heap size 342 MB, throughput 0.970933
Reading from 23397: heap size 371 MB, throughput 0.970539
Reading from 23396: heap size 344 MB, throughput 0.970365
Reading from 23397: heap size 373 MB, throughput 0.972199
Reading from 23396: heap size 344 MB, throughput 0.973092
Reading from 23397: heap size 376 MB, throughput 0.969184
Reading from 23396: heap size 346 MB, throughput 0.966137
Reading from 23397: heap size 377 MB, throughput 0.972111
Reading from 23396: heap size 347 MB, throughput 0.97048
Reading from 23396: heap size 349 MB, throughput 0.949839
Reading from 23397: heap size 381 MB, throughput 0.969696
Equal recommendation: 828 MB each
Reading from 23396: heap size 351 MB, throughput 0.985676
Reading from 23396: heap size 351 MB, throughput 0.832537
Reading from 23396: heap size 353 MB, throughput 0.720389
Reading from 23396: heap size 358 MB, throughput 0.683678
Reading from 23397: heap size 382 MB, throughput 0.974178
Reading from 23396: heap size 362 MB, throughput 0.731805
Reading from 23396: heap size 370 MB, throughput 0.826465
Reading from 23397: heap size 383 MB, throughput 0.971269
Reading from 23397: heap size 385 MB, throughput 0.839482
Reading from 23397: heap size 380 MB, throughput 0.817689
Reading from 23397: heap size 386 MB, throughput 0.814493
Reading from 23396: heap size 372 MB, throughput 0.985133
Reading from 23397: heap size 393 MB, throughput 0.955446
Reading from 23396: heap size 374 MB, throughput 0.984461
Reading from 23397: heap size 395 MB, throughput 0.98641
Reading from 23396: heap size 376 MB, throughput 0.9863
Reading from 23397: heap size 397 MB, throughput 0.98593
Reading from 23397: heap size 399 MB, throughput 0.975911
Reading from 23396: heap size 376 MB, throughput 0.699513
Reading from 23396: heap size 414 MB, throughput 0.981006
Reading from 23397: heap size 399 MB, throughput 0.986125
Equal recommendation: 828 MB each
Reading from 23396: heap size 414 MB, throughput 0.991434
Reading from 23397: heap size 401 MB, throughput 0.982668
Reading from 23396: heap size 417 MB, throughput 0.991009
Reading from 23397: heap size 399 MB, throughput 0.981443
Reading from 23396: heap size 419 MB, throughput 0.988221
Reading from 23397: heap size 401 MB, throughput 0.984935
Reading from 23396: heap size 420 MB, throughput 0.991778
Reading from 23397: heap size 398 MB, throughput 0.984675
Reading from 23396: heap size 417 MB, throughput 0.988147
Reading from 23397: heap size 400 MB, throughput 0.74949
Reading from 23396: heap size 385 MB, throughput 0.980222
Equal recommendation: 828 MB each
Reading from 23397: heap size 440 MB, throughput 0.993926
Reading from 23396: heap size 414 MB, throughput 0.984735
Reading from 23396: heap size 416 MB, throughput 0.955259
Reading from 23396: heap size 414 MB, throughput 0.834346
Reading from 23396: heap size 419 MB, throughput 0.808677
Reading from 23396: heap size 425 MB, throughput 0.838622
Reading from 23397: heap size 440 MB, throughput 0.987998
Reading from 23397: heap size 445 MB, throughput 0.936256
Reading from 23397: heap size 446 MB, throughput 0.865493
Reading from 23397: heap size 449 MB, throughput 0.877143
Reading from 23396: heap size 428 MB, throughput 0.982439
Reading from 23397: heap size 450 MB, throughput 0.982942
Reading from 23396: heap size 432 MB, throughput 0.985938
Reading from 23397: heap size 457 MB, throughput 0.991258
Reading from 23396: heap size 436 MB, throughput 0.98496
Reading from 23397: heap size 458 MB, throughput 0.990764
Reading from 23396: heap size 432 MB, throughput 0.98549
Equal recommendation: 828 MB each
Reading from 23397: heap size 458 MB, throughput 0.991931
Reading from 23396: heap size 436 MB, throughput 0.983666
Reading from 23397: heap size 460 MB, throughput 0.988199
Reading from 23396: heap size 432 MB, throughput 0.985677
Reading from 23397: heap size 458 MB, throughput 0.987768
Reading from 23396: heap size 435 MB, throughput 0.984715
Reading from 23397: heap size 460 MB, throughput 0.983539
Reading from 23396: heap size 435 MB, throughput 0.984784
Reading from 23397: heap size 461 MB, throughput 0.986737
Reading from 23396: heap size 436 MB, throughput 0.982644
Equal recommendation: 828 MB each
Reading from 23397: heap size 461 MB, throughput 0.9789
Reading from 23396: heap size 438 MB, throughput 0.981481
Reading from 23396: heap size 438 MB, throughput 0.987176
Reading from 23396: heap size 441 MB, throughput 0.89331
Reading from 23396: heap size 441 MB, throughput 0.860068
Reading from 23396: heap size 447 MB, throughput 0.836466
Reading from 23397: heap size 463 MB, throughput 0.981929
Reading from 23397: heap size 465 MB, throughput 0.959507
Reading from 23397: heap size 466 MB, throughput 0.845115
Reading from 23397: heap size 468 MB, throughput 0.877888
Reading from 23396: heap size 448 MB, throughput 0.993208
Reading from 23397: heap size 475 MB, throughput 0.99357
Reading from 23396: heap size 453 MB, throughput 0.992317
Reading from 23397: heap size 476 MB, throughput 0.991061
Equal recommendation: 828 MB each
Reading from 23396: heap size 455 MB, throughput 0.986043
Reading from 23397: heap size 480 MB, throughput 0.989683
Reading from 23396: heap size 457 MB, throughput 0.984699
Reading from 23397: heap size 482 MB, throughput 0.989755
Reading from 23396: heap size 459 MB, throughput 0.985599
Reading from 23397: heap size 481 MB, throughput 0.987825
Reading from 23396: heap size 457 MB, throughput 0.986142
Reading from 23397: heap size 483 MB, throughput 0.985478
Reading from 23396: heap size 459 MB, throughput 0.985619
Equal recommendation: 828 MB each
Reading from 23397: heap size 482 MB, throughput 0.980249
Reading from 23396: heap size 462 MB, throughput 0.984469
Reading from 23397: heap size 484 MB, throughput 0.98453
Reading from 23396: heap size 462 MB, throughput 0.989617
Reading from 23396: heap size 464 MB, throughput 0.916557
Reading from 23396: heap size 465 MB, throughput 0.875373
Reading from 23396: heap size 471 MB, throughput 0.974713
Reading from 23397: heap size 487 MB, throughput 0.991769
Reading from 23397: heap size 487 MB, throughput 0.910146
Reading from 23397: heap size 487 MB, throughput 0.866766
Reading from 23396: heap size 472 MB, throughput 0.991402
Reading from 23397: heap size 489 MB, throughput 0.982869
Reading from 23396: heap size 477 MB, throughput 0.989508
Reading from 23397: heap size 498 MB, throughput 0.992841
Equal recommendation: 828 MB each
Reading from 23396: heap size 478 MB, throughput 0.987469
Reading from 23397: heap size 499 MB, throughput 0.991172
Reading from 23396: heap size 477 MB, throughput 0.988552
Reading from 23397: heap size 500 MB, throughput 0.990497
Reading from 23396: heap size 479 MB, throughput 0.991904
Reading from 23397: heap size 502 MB, throughput 0.989217
Reading from 23396: heap size 479 MB, throughput 0.985921
Reading from 23397: heap size 501 MB, throughput 0.987099
Equal recommendation: 828 MB each
Reading from 23396: heap size 480 MB, throughput 0.985827
Reading from 23397: heap size 503 MB, throughput 0.988244
Reading from 23396: heap size 483 MB, throughput 0.991367
Reading from 23396: heap size 484 MB, throughput 0.894019
Reading from 23396: heap size 483 MB, throughput 0.888145
Reading from 23397: heap size 506 MB, throughput 0.98131
Reading from 23396: heap size 485 MB, throughput 0.990682
Reading from 23397: heap size 506 MB, throughput 0.980024
Reading from 23397: heap size 512 MB, throughput 0.912149
Reading from 23397: heap size 515 MB, throughput 0.904886
Reading from 23396: heap size 493 MB, throughput 0.993561
Reading from 23397: heap size 523 MB, throughput 0.987922
Equal recommendation: 828 MB each
Reading from 23396: heap size 494 MB, throughput 0.986751
Reading from 23397: heap size 523 MB, throughput 0.991281
Reading from 23396: heap size 495 MB, throughput 0.989345
Reading from 23397: heap size 524 MB, throughput 0.990997
Reading from 23396: heap size 497 MB, throughput 0.989769
Reading from 23397: heap size 526 MB, throughput 0.99085
Reading from 23396: heap size 497 MB, throughput 0.988601
Equal recommendation: 828 MB each
Reading from 23397: heap size 526 MB, throughput 0.989838
Reading from 23396: heap size 498 MB, throughput 0.990082
Reading from 23397: heap size 528 MB, throughput 0.986784
Reading from 23396: heap size 500 MB, throughput 0.986861
Reading from 23396: heap size 501 MB, throughput 0.892505
Reading from 23396: heap size 505 MB, throughput 0.961882
Reading from 23397: heap size 531 MB, throughput 0.9927
Reading from 23397: heap size 532 MB, throughput 0.925203
Reading from 23397: heap size 532 MB, throughput 0.901812
Reading from 23396: heap size 506 MB, throughput 0.992547
Reading from 23397: heap size 534 MB, throughput 0.991022
Equal recommendation: 828 MB each
Reading from 23396: heap size 510 MB, throughput 0.991126
Reading from 23397: heap size 542 MB, throughput 0.990092
Reading from 23396: heap size 511 MB, throughput 0.989792
Reading from 23397: heap size 543 MB, throughput 0.992094
Reading from 23396: heap size 511 MB, throughput 0.991301
Reading from 23397: heap size 544 MB, throughput 0.990336
Reading from 23396: heap size 513 MB, throughput 0.990024
Equal recommendation: 828 MB each
Reading from 23397: heap size 546 MB, throughput 0.990914
Reading from 23396: heap size 514 MB, throughput 0.988168
Reading from 23396: heap size 514 MB, throughput 0.99397
Reading from 23396: heap size 517 MB, throughput 0.936697
Reading from 23397: heap size 545 MB, throughput 0.989083
Reading from 23396: heap size 518 MB, throughput 0.907275
Reading from 23396: heap size 524 MB, throughput 0.991852
Reading from 23397: heap size 547 MB, throughput 0.993785
Reading from 23397: heap size 547 MB, throughput 0.945453
Reading from 23397: heap size 549 MB, throughput 0.929583
Equal recommendation: 828 MB each
Reading from 23396: heap size 524 MB, throughput 0.992487
Reading from 23397: heap size 556 MB, throughput 0.993944
Reading from 23396: heap size 525 MB, throughput 0.992184
Reading from 23397: heap size 556 MB, throughput 0.991787
Reading from 23396: heap size 527 MB, throughput 0.99025
Reading from 23397: heap size 557 MB, throughput 0.991099
Equal recommendation: 828 MB each
Reading from 23396: heap size 527 MB, throughput 0.988589
Reading from 23397: heap size 559 MB, throughput 0.989935
Reading from 23396: heap size 528 MB, throughput 0.98936
Reading from 23397: heap size 559 MB, throughput 0.988993
Reading from 23396: heap size 530 MB, throughput 0.984159
Reading from 23396: heap size 532 MB, throughput 0.924712
Reading from 23396: heap size 536 MB, throughput 0.986079
Reading from 23397: heap size 561 MB, throughput 0.991354
Reading from 23397: heap size 564 MB, throughput 0.966494
Reading from 23397: heap size 565 MB, throughput 0.915458
Reading from 23396: heap size 538 MB, throughput 0.991925
Equal recommendation: 828 MB each
Reading from 23397: heap size 570 MB, throughput 0.992936
Reading from 23396: heap size 540 MB, throughput 0.992274
Reading from 23397: heap size 571 MB, throughput 0.993836
Reading from 23396: heap size 541 MB, throughput 0.991374
Reading from 23396: heap size 539 MB, throughput 0.992173
Equal recommendation: 828 MB each
Reading from 23397: heap size 574 MB, throughput 0.991947
Reading from 23396: heap size 541 MB, throughput 0.988242
Reading from 23397: heap size 576 MB, throughput 0.99042
Reading from 23396: heap size 543 MB, throughput 0.986857
Reading from 23396: heap size 544 MB, throughput 0.913046
Reading from 23396: heap size 543 MB, throughput 0.981926
Reading from 23397: heap size 575 MB, throughput 0.990452
Reading from 23396: heap size 545 MB, throughput 0.992069
Equal recommendation: 828 MB each
Reading from 23397: heap size 577 MB, throughput 0.994611
Reading from 23397: heap size 578 MB, throughput 0.930661
Reading from 23397: heap size 579 MB, throughput 0.978361
Reading from 23396: heap size 548 MB, throughput 0.991719
Reading from 23397: heap size 587 MB, throughput 0.993933
Reading from 23396: heap size 549 MB, throughput 0.99087
Reading from 23397: heap size 588 MB, throughput 0.988515
Reading from 23396: heap size 548 MB, throughput 0.988425
Equal recommendation: 828 MB each
Reading from 23397: heap size 588 MB, throughput 0.991637
Reading from 23396: heap size 550 MB, throughput 0.98914
Reading from 23396: heap size 553 MB, throughput 0.988501
Reading from 23396: heap size 554 MB, throughput 0.91512
Reading from 23397: heap size 590 MB, throughput 0.991266
Reading from 23396: heap size 557 MB, throughput 0.98825
Equal recommendation: 828 MB each
Reading from 23396: heap size 559 MB, throughput 0.991987
Reading from 23397: heap size 592 MB, throughput 0.993002
Reading from 23397: heap size 593 MB, throughput 0.977741
Reading from 23397: heap size 596 MB, throughput 0.959897
Reading from 23396: heap size 561 MB, throughput 0.99135
Reading from 23397: heap size 598 MB, throughput 0.994942
Reading from 23396: heap size 563 MB, throughput 0.988568
Reading from 23397: heap size 601 MB, throughput 0.993654
Equal recommendation: 828 MB each
Reading from 23396: heap size 561 MB, throughput 0.990427
Reading from 23397: heap size 602 MB, throughput 0.993052
Reading from 23396: heap size 563 MB, throughput 0.992948
Reading from 23396: heap size 564 MB, throughput 0.974942
Reading from 23396: heap size 565 MB, throughput 0.940418
Reading from 23397: heap size 600 MB, throughput 0.990008
Reading from 23396: heap size 571 MB, throughput 0.992507
Equal recommendation: 828 MB each
Reading from 23397: heap size 603 MB, throughput 0.98911
Reading from 23396: heap size 571 MB, throughput 0.991947
Reading from 23397: heap size 604 MB, throughput 0.985227
Reading from 23397: heap size 606 MB, throughput 0.952719
Reading from 23396: heap size 572 MB, throughput 0.991026
Reading from 23397: heap size 611 MB, throughput 0.992954
Reading from 23396: heap size 574 MB, throughput 0.988083
Equal recommendation: 828 MB each
Reading from 23397: heap size 611 MB, throughput 0.993637
Reading from 23396: heap size 576 MB, throughput 0.98967
Reading from 23397: heap size 611 MB, throughput 0.991394
Reading from 23396: heap size 576 MB, throughput 0.98612
Reading from 23396: heap size 579 MB, throughput 0.935375
Reading from 23397: heap size 613 MB, throughput 0.992043
Reading from 23396: heap size 581 MB, throughput 0.994717
Equal recommendation: 828 MB each
Reading from 23396: heap size 587 MB, throughput 0.992689
Reading from 23397: heap size 615 MB, throughput 0.995503
Reading from 23397: heap size 615 MB, throughput 0.970983
Reading from 23397: heap size 615 MB, throughput 0.978285
Reading from 23396: heap size 588 MB, throughput 0.992337
Reading from 23397: heap size 617 MB, throughput 0.994333
Reading from 23396: heap size 586 MB, throughput 0.931372
Equal recommendation: 828 MB each
Reading from 23397: heap size 619 MB, throughput 0.993474
Reading from 23396: heap size 605 MB, throughput 0.99481
Reading from 23396: heap size 605 MB, throughput 0.98765
Reading from 23396: heap size 608 MB, throughput 0.938534
Client 23396 died
Clients: 1
Reading from 23397: heap size 620 MB, throughput 0.99436
Recommendation: one client; give it all the memory
Reading from 23397: heap size 620 MB, throughput 0.99372
Reading from 23397: heap size 621 MB, throughput 0.995381
Reading from 23397: heap size 624 MB, throughput 0.949317
Client 23397 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
