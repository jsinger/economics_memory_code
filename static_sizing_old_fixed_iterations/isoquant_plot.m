args = argv();

plotTitle = args{1};
b1 = args{2};
b2 = args{3};
a = load(args{4});
outFile = args{5};

scatter3(a(:, 1), a(:, 2), a(:, 3));
xlabel(sprintf("%s heap (MB)", b1));
ylabel(sprintf("%s heap (MB)", b2));
zlabel("Runtime (s)");
title(plotTitle);

print(outFile);
