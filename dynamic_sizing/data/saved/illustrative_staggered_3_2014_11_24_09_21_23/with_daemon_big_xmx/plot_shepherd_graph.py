#!/usr/bin/python 

# gen_results_graph.py
# Jeremy Singer
# 2 Apr 15

# After ISMM review, our paper is subject to shepherding
# (i.e. it has been conditionally accepted)
# I need to change the resultsTable.tex into a graph, 
# as advised by shepherd.

import warnings
import sys

import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import numpy as np
import re
from pylab import rcParams
rcParams['figure.figsize'] = 7, 10

logFile = 'daemon_log.txt'

textSize = 12 

# list of processes running Java benchmarks
processes = []
allPids = []

# recommended heap size - dict - index on process id, returns recommendation
recommendedSize = {}

# actual heap size - dict - index on process id, returns actual heap size
actualSize = {}

# data series for output graphs
# index on pid, value is array of time or error values
timeData = {}
errorData = {}
actualSizeData = {}
recommendedSizeData = {}

minTime = sys.maxint

# read in a daemon log file line-by-line, update data structures as we go...
for line in open(logFile):
    # each line has form:
    # ^TIME PID EVENT HEAPSIZE THROUGHPUT
    data = line.rstrip().split(' ')
    time = int(data[0])
    if time<minTime:
        minTime = time
    pid = int(data[1])
    event = data[2]
    if (len(data)>3 and (event == 'RECOMMEND' or event == 'READING')):
        heapsize = int(data[3])
    else:
        heapsize = 0

    if pid not in processes and event != 'CREATION':
        print "*** ERROR unknown pid:", pid

    if event == 'CREATION':
        processes.append(pid)
        allPids.append(pid)
        timeData[pid] = []
        errorData[pid] = []
        actualSizeData[pid] = []
        recommendedSizeData[pid] = []
        # initialize to 0
        actualSize[pid] = 0
        recommendedSize[pid] = 0

    if event == 'DEATH':
        processes.remove(pid)

    if event == 'RECOMMEND':
        recommendedSize[pid] = heapsize

    if event == 'READING':
        actualSize[pid] = heapsize

    for p in processes:
        error = actualSize[p] - recommendedSize[p]
        timeData[p].append(float(time-minTime)/1000)
        errorData[p].append(error)
        actualSizeData[p].append(actualSize[p])
        recommendedSizeData[p].append(recommendedSize[p])
        print p, ",", timeData[p][-1], ",", errorData[p][-1]

# read line labels from csv file
benchmarks = []
for line in open('benchmarks.csv'):
    if 'Hotspot8' in line:
        benchmark = line.split(',Hot')[0]
        benchmarks.append(benchmark)

# at end draw graphs
i = 0
fig, axes = plt.subplots(nrows=len(timeData), ncols=1, sharex=False)
for ax in axes.flat:
    p = allPids[i]
    ax.plot(timeData[p], actualSizeData[p], color='LightBlue', label=benchmarks[i], linewidth=2)
    ax.plot(timeData[p], recommendedSizeData[p], color='Black', label='Recommended', linewidth=3)
    ax.set_xbound(0,700)
    #ax.set_yscale('symlog')
    ax.set_ybound(lower=0,upper=250)
    #ax.set_yticks([-100,-10,-1,1,10,100])
    ax.set_ylabel('heap size difference\nfrom recommendation/MB', fontsize=textSize, multialignment='center')
    if i<2:
        ax.legend(loc='upper right')
    else:
        ax.legend(loc='upper left')
    i = i+1

ax.set_xlabel('time/seconds')
plt.tight_layout(pad=0.1, w_pad=0.2, h_pad=0.5)
plt.show()
    
    


        

