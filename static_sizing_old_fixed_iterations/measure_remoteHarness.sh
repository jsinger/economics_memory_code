#!/bin/bash
# Measure benchmarks on a remote machine
#
# Usage: measure_remoteHarness.sh out_dir iterations n_for_nover
#

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") out_dir iterations n_for_nover"
    exit 1
}

test -z "${1}" && usage
test -z "${2}" && usage
test -z "${3}" && usage

OUTDIR="$(readlink -f "${1}")"
ITERATIONS="${2}"
N_FOR_NOVER="${3}"

ME="$(whoami)@$(hostname)"

mkdir -p "${OUTDIR}"

"${DIR}/../machineInfo.sh" "${OUTDIR}"

while read BENCHMARK HEAP_SIZE MAX_HEAP; do
    RESULTS_FILE="${OUTDIR}/${BENCHMARK}_${ME}_results.txt"

    "${DIR}/measure_benchmark.sh" "${BENCHMARK}" "${HEAP_SIZE}" "${ITERATIONS}" "${MAX_HEAP}" "${OUTDIR}" "${N_FOR_NOVER}" "${RESULTS_FILE}"
done
