/*
 * Copyright (c) 2002, 2010, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 *
 */

#include "precompiled.hpp"
#include "gc_implementation/parallelScavenge/generationSizer.hpp"
#include "gc_implementation/parallelScavenge/psAdaptiveSizePolicy.hpp"
#include "gc_implementation/parallelScavenge/psGCAdaptivePolicyCounters.hpp"
#include "gc_implementation/parallelScavenge/psScavenge.hpp"
#include "gc_implementation/shared/gcPolicyCounters.hpp"
#include "gc_interface/gcCause.hpp"
#include "memory/collectorPolicy.hpp"
#include "runtime/timer.hpp"
#include "utilities/top.hpp"

#include <math.h>
#include <jeconomem.hpp>

PSAdaptiveSizePolicy::PSAdaptiveSizePolicy(size_t init_eden_size,
                                           size_t init_promo_size,
                                           size_t init_survivor_size,
                                           size_t intra_generation_alignment,
                                           double gc_pause_goal_sec,
                                           double gc_minor_pause_goal_sec,
                                           uint gc_cost_ratio) :
     AdaptiveSizePolicy(init_eden_size,
                        init_promo_size,
                        init_survivor_size,
                        gc_pause_goal_sec,
                        gc_cost_ratio),
     _collection_cost_margin_fraction(AdaptiveSizePolicyCollectionCostMargin/
       100.0),
     _intra_generation_alignment(intra_generation_alignment),
     _live_at_last_full_gc(init_promo_size),
     _gc_minor_pause_goal_sec(gc_minor_pause_goal_sec),
     _latest_major_mutator_interval_seconds(0),
     _young_gen_change_for_major_pause_count(0)
{
  // Sizing policy statistics
  _avg_major_pause    =
    new AdaptivePaddedAverage(AdaptiveTimeWeight, PausePadding);
  _avg_minor_interval = new AdaptiveWeightedAverage(AdaptiveTimeWeight);
  _avg_major_interval = new AdaptiveWeightedAverage(AdaptiveTimeWeight);

  _avg_base_footprint = new AdaptiveWeightedAverage(AdaptiveSizePolicyWeight);
  _major_pause_old_estimator =
    new LinearLeastSquareFit(AdaptiveSizePolicyWeight);
  _major_pause_young_estimator =
    new LinearLeastSquareFit(AdaptiveSizePolicyWeight);
  _major_collection_estimator =
    new LinearLeastSquareFit(AdaptiveSizePolicyWeight);

  _young_gen_size_increment_supplement = YoungGenerationSizeSupplement;
  _old_gen_size_increment_supplement = TenuredGenerationSizeSupplement;

  // Start the timers
  _major_timer.start();

  _old_gen_policy_is_ready = false;

  JEconoMem::psasp_created(this);
}

void PSAdaptiveSizePolicy::major_collection_begin() {
  JEconoMem::psasp_major_collection_begin(this);
}

void PSAdaptiveSizePolicy::update_minor_pause_old_estimator(
    double minor_pause_in_ms) {
  JEconoMem::psasp_update_minor_pause_old_estimator(this, minor_pause_in_ms);
}

void PSAdaptiveSizePolicy::major_collection_end(size_t amount_live,
  GCCause::Cause gc_cause) {
  JEconoMem::psasp_major_collection_end(this, amount_live, gc_cause);
}

// If the remaining free space in the old generation is less that
// that expected to be needed by the next collection, do a full
// collection now.
bool PSAdaptiveSizePolicy::should_full_GC(size_t old_free_in_bytes) {
  return JEconoMem::psasp_should_full_GC(this, old_free_in_bytes);
}

void PSAdaptiveSizePolicy::clear_generation_free_space_flags() {
  JEconoMem::psasp_clear_generation_free_space_flags(this);
}

// If this is not a full GC, only test and modify the young generation.

void PSAdaptiveSizePolicy::compute_generation_free_space(
                                           size_t young_live,
                                           size_t eden_live,
                                           size_t old_live,
                                           size_t perm_live,
                                           size_t cur_eden,
                                           size_t max_old_gen_size,
                                           size_t max_eden_size,
                                           bool   is_full_gc,
                                           GCCause::Cause gc_cause,
                                           CollectorPolicy* collector_policy) {
  JEconoMem::psasp_compute_generation_free_space(this, young_live, eden_live, old_live, perm_live, cur_eden, max_old_gen_size, max_eden_size, is_full_gc, gc_cause, collector_policy);
};

void PSAdaptiveSizePolicy::decay_supplemental_growth(bool is_full_gc) {
  JEconoMem::psasp_decay_supplemental_growth(this, is_full_gc);
}

void PSAdaptiveSizePolicy::adjust_for_minor_pause_time(bool is_full_gc,
    size_t* desired_promo_size_ptr, size_t* desired_eden_size_ptr) {
  JEconoMem::psasp_adjust_for_minor_pause_time(this, is_full_gc, desired_promo_size_ptr, desired_eden_size_ptr);
}

void PSAdaptiveSizePolicy::adjust_for_pause_time(bool is_full_gc,
                                             size_t* desired_promo_size_ptr,
                                             size_t* desired_eden_size_ptr) {
  JEconoMem::psasp_adjust_for_pause_time(this, is_full_gc, desired_promo_size_ptr, desired_eden_size_ptr);
}

void PSAdaptiveSizePolicy::adjust_for_throughput(bool is_full_gc,
                                             size_t* desired_promo_size_ptr,
                                             size_t* desired_eden_size_ptr) {
  JEconoMem::psasp_adjust_for_throughput(this, is_full_gc, desired_promo_size_ptr, desired_eden_size_ptr);
}

size_t PSAdaptiveSizePolicy::adjust_promo_for_footprint(
    size_t desired_promo_size, size_t desired_sum) {
  return JEconoMem::psasp_adjust_promo_for_footprint(this, desired_promo_size, desired_sum);
}

size_t PSAdaptiveSizePolicy::adjust_eden_for_footprint(
  size_t desired_eden_size, size_t desired_sum) {
  return JEconoMem::psasp_adjust_eden_for_footprint(this, desired_eden_size, desired_sum);
}

// Scale down "change" by the factor
//      part / total
// Don't align the results.

size_t PSAdaptiveSizePolicy::scale_down(size_t change,
                                        double part,
                                        double total) {
  return JEconoMem::psasp_scale_down(this, change, part, total);
}

size_t PSAdaptiveSizePolicy::eden_increment(size_t cur_eden,
                                            uint percent_change) {
  return JEconoMem::psasp_eden_increment(this, cur_eden, percent_change);
}

size_t PSAdaptiveSizePolicy::eden_increment(size_t cur_eden) {
  return JEconoMem::psasp_eden_increment(this, cur_eden);
}

size_t PSAdaptiveSizePolicy::eden_increment_aligned_up(size_t cur_eden) {
  return JEconoMem::psasp_eden_increment_aligned_up(this, cur_eden);
}

size_t PSAdaptiveSizePolicy::eden_increment_aligned_down(size_t cur_eden) {
  return JEconoMem::psasp_eden_increment_aligned_down(this, cur_eden);  
}

size_t PSAdaptiveSizePolicy::eden_increment_with_supplement_aligned_up(
  size_t cur_eden) {
  return JEconoMem::psasp_eden_increment_with_supplement_aligned_up(this, cur_eden);
}

size_t PSAdaptiveSizePolicy::eden_decrement_aligned_down(size_t cur_eden) {
  return JEconoMem::psasp_eden_decrement_aligned_down(this, cur_eden);
}

size_t PSAdaptiveSizePolicy::eden_decrement(size_t cur_eden) {
  return JEconoMem::psasp_eden_decrement(this, cur_eden);
}

size_t PSAdaptiveSizePolicy::promo_increment(size_t cur_promo,
                                             uint percent_change) {
  return JEconoMem::psasp_promo_increment(this, cur_promo, percent_change);
}

size_t PSAdaptiveSizePolicy::promo_increment(size_t cur_promo) {
  return JEconoMem::psasp_promo_increment(this, cur_promo);
}

size_t PSAdaptiveSizePolicy::promo_increment_aligned_up(size_t cur_promo) {
  return JEconoMem::psasp_promo_increment_aligned_up(this, cur_promo);
}

size_t PSAdaptiveSizePolicy::promo_increment_aligned_down(size_t cur_promo) {
  return JEconoMem::psasp_promo_increment_aligned_down(this, cur_promo);
}

size_t PSAdaptiveSizePolicy::promo_increment_with_supplement_aligned_up(
  size_t cur_promo) {
  return JEconoMem::psasp_promo_increment_with_supplement_aligned_up(this, cur_promo);
}

size_t PSAdaptiveSizePolicy::promo_decrement_aligned_down(size_t cur_promo) {
  return JEconoMem::psasp_promo_decrement_aligned_down(this, cur_promo);
}

size_t PSAdaptiveSizePolicy::promo_decrement(size_t cur_promo) {
  return JEconoMem::psasp_promo_decrement(this, cur_promo);
}

int PSAdaptiveSizePolicy::compute_survivor_space_size_and_threshold(
                                             bool is_survivor_overflow,
                                             int tenuring_threshold,
                                             size_t survivor_limit) {
  return JEconoMem::psasp_compute_survivor_space_size_and_threshold(this, is_survivor_overflow, tenuring_threshold, survivor_limit);
}

void PSAdaptiveSizePolicy::update_averages(bool is_survivor_overflow,
                                           size_t survived,
                                           size_t promoted) {
  JEconoMem::psasp_update_averages(this, is_survivor_overflow, survived, promoted);
}

bool PSAdaptiveSizePolicy::print_adaptive_size_policy_on(outputStream* st)
  const {
  return JEconoMem::psasp_print_adaptive_size_policy_on(this, st);
}
