#ifndef HOTSPOTPROPORTIONAL_HPP
#define HOTSPOTPROPORTIONAL_HPP

#include "gc_implementation/shared/ProportionalThroughput.hpp"

class HotspotProportional : public economem::ProportionalThroughput
{
public:
    HotspotProportional();

private:
    virtual unsigned long long getHeapSizeB();
    virtual void logReading(unsigned long long heapSizeB, double throughput, unsigned long long gcTime, unsigned long long interGCTime);
};

#endif
