#!/usr/bin/env python
# Process an existing utility space dataset

import argparse
import argtypes
import staticutility as su
import sys
import utilitymodel


def postprocess(dataDir):
    bids = dataDir.parseBenchmarksFile()
    totalMem = dataDir.totalMem()
    runs = dataDir.runs()

    rawResultsFile = dataDir.gatherResults()

    for model in utilitymodel.models:
        if dataDir.haveParamsForUtilityModel(model, bids):
            resultsFile = dataDir.resultsFile(model)
            params = map(lambda b: utilitymodel.get(model).loadParams(dataDir.paramsFile(b, model)), bids)

            maxMeasured = None
            maxPredicted = None
            equalSplit = None

            with open(rawResultsFile) as infile, open(resultsFile, "w") as outfile:
                for line in infile:
                    raw = su.utilityRawResultFromStr(line)
                    U = utilitymodel.get(model).combineRawUtilities(*raw.itersPerSecs)
                    stddevU = utilitymodel.get(model).propagateRawErrors(raw.itersPerSecs, U, raw.stddevItersPerSecs)
                    predicted = utilitymodel.get(model).combinedUtility(params, raw.hs)
                    difference = U - predicted

                    r = su.UtilityFullResult(raw, U, stddevU, predicted, difference)
                    r.append(outfile)

                    if maxMeasured is None or U > maxMeasured.measuredU:
                        maxMeasured = r

                    if maxPredicted is None or predicted > maxPredicted.predictedU:
                        maxPredicted = r

                    if r.isEqualSplit(totalMem):
                        equalSplit = r

            dataDir.writeMaxMeasuredFile(model, maxMeasured)
            dataDir.writeMaxPredictedFile(model, maxPredicted)
            dataDir.writeEuclideanFile(model, maxMeasured.raw.hs, maxPredicted.raw.hs)
            dataDir.writePredictionAccuracyFile(model, maxMeasured.measuredU, maxMeasured.stddevU, maxPredicted.predictedU)
            dataDir.writePracticalAccuracyFile(model, maxPredicted.measuredU, maxPredicted.stddevU, maxMeasured.measuredU, maxMeasured.stddevU)
            dataDir.writeDefaultRatioPredicted(model, maxPredicted.predictedU)
            if equalSplit:
                dataDir.writePracticalVsEqual(model, maxPredicted.measuredU, maxPredicted.stddevU, equalSplit.measuredU, equalSplit.stddevU)
                dataDir.writeDefaultRatioEqualSplit(model, equalSplit.measuredU, equalSplit.stddevU)
            dataDir.writeDefaultRatioMaxMeasured(model, maxMeasured.measuredU, maxMeasured.stddevU)
            dataDir.writeDefaultRatioMaxPredictedMeasured(model, maxPredicted.measuredU, maxPredicted.stddevU)

            results = su.UtilityResultsColumns(su.loadUtilityFullResultsFile(resultsFile))

            if not dataDir.plot(results.hs, results.measuredU, results.predictedU, results.differenceU, bids, totalMem, runs, model):
                print "Cannot plot graphs for this number of VMs"

            if not dataDir.plot(results.hs, results.measuredU, results.predictedU, results.differenceU, bids, totalMem, runs, model, cleaned=True):
                print "Cannot plot cleaned graphs for this number of VMs"

            su.mergePdfs(dataDir.globModelGraphs(model), dataDir.modelGraph(model))
            su.mergePdfs(dataDir.globModelGraphs(model, cleaned=True), dataDir.modelGraph(model, cleaned=True))
        else:
            sys.stderr.write("Parameters unavailable for the '%s' utility model.\n" % model)

    su.mergePdfs(dataDir.globModelResultsGraphs(), dataDir.resultsGraph())
    su.mergePdfs(dataDir.globModelResultsGraphs(cleaned=True), dataDir.resultsGraph(cleaned=True))



if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Process an existing utility space dataset")
    parser.add_argument("dir", type=argtypes.existing_absolute_dir, help="Directory containing data")
    args = parser.parse_args()
    postprocess(su.UtilitySpaceDir(args.dir))
