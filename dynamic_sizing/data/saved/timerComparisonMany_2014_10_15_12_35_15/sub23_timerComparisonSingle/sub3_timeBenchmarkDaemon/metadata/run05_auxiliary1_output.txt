economemd
    total memory: 5012 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub23_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run05_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 3140: heap size 9 MB, throughput 0.992176
Clients: 1
Client 3140 has a minimum heap size of 30 MB
Reading from 3139: heap size 9 MB, throughput 0.987752
Clients: 2
Client 3139 has a minimum heap size of 1223 MB
Reading from 3140: heap size 9 MB, throughput 0.98739
Reading from 3140: heap size 9 MB, throughput 0.982198
Reading from 3139: heap size 9 MB, throughput 0.984022
Reading from 3140: heap size 9 MB, throughput 0.976149
Reading from 3140: heap size 11 MB, throughput 0.945431
Reading from 3140: heap size 11 MB, throughput 0.919888
Reading from 3140: heap size 16 MB, throughput 0.879853
Reading from 3140: heap size 16 MB, throughput 0.837902
Reading from 3140: heap size 24 MB, throughput 0.844269
Reading from 3140: heap size 24 MB, throughput 0.840181
Reading from 3140: heap size 34 MB, throughput 0.870165
Reading from 3140: heap size 34 MB, throughput 0.890032
Reading from 3139: heap size 11 MB, throughput 0.977575
Reading from 3140: heap size 50 MB, throughput 0.921742
Reading from 3140: heap size 50 MB, throughput 0.931169
Reading from 3140: heap size 76 MB, throughput 0.947865
Reading from 3140: heap size 76 MB, throughput 0.946949
Reading from 3140: heap size 116 MB, throughput 0.958495
Reading from 3140: heap size 116 MB, throughput 0.962829
Reading from 3139: heap size 11 MB, throughput 0.985431
Reading from 3139: heap size 15 MB, throughput 0.951479
Reading from 3139: heap size 18 MB, throughput 0.939069
Reading from 3139: heap size 24 MB, throughput 0.895498
Reading from 3139: heap size 28 MB, throughput 0.766201
Reading from 3139: heap size 33 MB, throughput 0.764549
Reading from 3139: heap size 42 MB, throughput 0.210976
Reading from 3139: heap size 49 MB, throughput 0.593056
Reading from 3140: heap size 159 MB, throughput 0.939109
Reading from 3139: heap size 62 MB, throughput 0.802395
Reading from 3139: heap size 72 MB, throughput 0.827479
Reading from 3140: heap size 171 MB, throughput 0.796018
Reading from 3139: heap size 73 MB, throughput 0.434449
Reading from 3139: heap size 97 MB, throughput 0.752548
Reading from 3139: heap size 100 MB, throughput 0.354147
Reading from 3140: heap size 178 MB, throughput 0.970101
Reading from 3139: heap size 135 MB, throughput 0.145049
Reading from 3139: heap size 136 MB, throughput 0.791631
Reading from 3139: heap size 138 MB, throughput 0.762381
Reading from 3139: heap size 144 MB, throughput 0.751978
Reading from 3140: heap size 208 MB, throughput 0.897614
Reading from 3139: heap size 148 MB, throughput 0.452988
Reading from 3139: heap size 181 MB, throughput 0.66714
Reading from 3139: heap size 196 MB, throughput 0.749459
Reading from 3139: heap size 197 MB, throughput 0.68219
Reading from 3139: heap size 202 MB, throughput 0.672913
Reading from 3140: heap size 230 MB, throughput 0.963778
Reading from 3139: heap size 210 MB, throughput 0.688016
Reading from 3139: heap size 215 MB, throughput 0.668047
Reading from 3139: heap size 227 MB, throughput 0.569702
Reading from 3139: heap size 234 MB, throughput 0.422685
Reading from 3140: heap size 237 MB, throughput 0.970893
Reading from 3139: heap size 281 MB, throughput 0.502459
Reading from 3139: heap size 294 MB, throughput 0.467016
Reading from 3140: heap size 249 MB, throughput 0.978417
Reading from 3139: heap size 298 MB, throughput 0.180596
Reading from 3140: heap size 272 MB, throughput 0.983123
Reading from 3140: heap size 272 MB, throughput 0.983124
Reading from 3140: heap size 272 MB, throughput 0.795945
Reading from 3139: heap size 353 MB, throughput 0.109855
Reading from 3139: heap size 337 MB, throughput 0.563007
Reading from 3139: heap size 394 MB, throughput 0.642971
Reading from 3139: heap size 401 MB, throughput 0.662808
Reading from 3139: heap size 399 MB, throughput 0.644121
Reading from 3140: heap size 275 MB, throughput 0.960436
Reading from 3139: heap size 400 MB, throughput 0.50352
Reading from 3139: heap size 402 MB, throughput 0.530919
Numeric result:
Recommendation: 2 clients, utility 1.30307:
    h1: 3789 MB (U(h) = 0.885223*h^0.00910101)
    h2: 1223 MB (U(h) = 1.356*h^0.001)
Recommendation: 2 clients, utility 1.30307:
    h1: 3789 MB (U(h) = 0.885223*h^0.00910101)
    h2: 1223 MB (U(h) = 1.356*h^0.001)
Reading from 3139: heap size 409 MB, throughput 0.569519
Reading from 3139: heap size 413 MB, throughput 0.602475
Reading from 3140: heap size 296 MB, throughput 0.923105
Reading from 3139: heap size 425 MB, throughput 0.512563
Reading from 3139: heap size 432 MB, throughput 0.504652
Reading from 3139: heap size 443 MB, throughput 0.476618
Reading from 3139: heap size 449 MB, throughput 0.489661
Reading from 3139: heap size 458 MB, throughput 0.512987
Reading from 3139: heap size 464 MB, throughput 0.498688
Reading from 3140: heap size 298 MB, throughput 0.980548
Reading from 3139: heap size 470 MB, throughput 0.476941
Reading from 3139: heap size 474 MB, throughput 0.408031
Reading from 3139: heap size 530 MB, throughput 0.415341
Reading from 3140: heap size 322 MB, throughput 0.970782
Reading from 3139: heap size 532 MB, throughput 0.393005
Reading from 3140: heap size 330 MB, throughput 0.969488
Reading from 3139: heap size 534 MB, throughput 0.144749
Reading from 3139: heap size 577 MB, throughput 0.398184
Reading from 3139: heap size 586 MB, throughput 0.455927
Reading from 3139: heap size 573 MB, throughput 0.454188
Reading from 3139: heap size 505 MB, throughput 0.470538
Reading from 3140: heap size 353 MB, throughput 0.980119
Reading from 3139: heap size 570 MB, throughput 0.272188
Reading from 3139: heap size 634 MB, throughput 0.487553
Reading from 3139: heap size 626 MB, throughput 0.529144
Reading from 3140: heap size 354 MB, throughput 0.984495
Reading from 3139: heap size 631 MB, throughput 0.566461
Reading from 3139: heap size 624 MB, throughput 0.567524
Reading from 3139: heap size 628 MB, throughput 0.636036
Reading from 3139: heap size 631 MB, throughput 0.609894
Reading from 3140: heap size 372 MB, throughput 0.977837
Reading from 3139: heap size 632 MB, throughput 0.840844
Reading from 3140: heap size 377 MB, throughput 0.984648
Reading from 3140: heap size 403 MB, throughput 0.980217
Numeric result:
Recommendation: 2 clients, utility 1.22578:
    h1: 3789 MB (U(h) = 0.854923*h^0.0190289)
    h2: 1223 MB (U(h) = 1.21703*h^0.001)
Recommendation: 2 clients, utility 1.22578:
    h1: 3789 MB (U(h) = 0.854923*h^0.0190289)
    h2: 1223 MB (U(h) = 1.21703*h^0.001)
Reading from 3139: heap size 630 MB, throughput 0.479592
Reading from 3139: heap size 707 MB, throughput 0.805159
Reading from 3139: heap size 721 MB, throughput 0.733523
Reading from 3140: heap size 406 MB, throughput 0.982728
Reading from 3139: heap size 730 MB, throughput 0.675172
Reading from 3139: heap size 737 MB, throughput 0.571151
Reading from 3139: heap size 742 MB, throughput 0.496179
Reading from 3139: heap size 750 MB, throughput 0.440684
Reading from 3140: heap size 439 MB, throughput 0.985198
Reading from 3139: heap size 750 MB, throughput 0.340426
Reading from 3139: heap size 818 MB, throughput 0.683292
Reading from 3139: heap size 820 MB, throughput 0.631804
Reading from 3139: heap size 819 MB, throughput 0.612949
Reading from 3140: heap size 440 MB, throughput 0.984513
Reading from 3139: heap size 822 MB, throughput 0.712119
Reading from 3139: heap size 828 MB, throughput 0.701188
Reading from 3139: heap size 828 MB, throughput 0.747319
Reading from 3140: heap size 469 MB, throughput 0.979469
Reading from 3139: heap size 837 MB, throughput 0.399754
Reading from 3139: heap size 929 MB, throughput 0.680158
Reading from 3140: heap size 470 MB, throughput 0.982499
Reading from 3139: heap size 934 MB, throughput 0.82377
Reading from 3139: heap size 940 MB, throughput 0.801581
Reading from 3139: heap size 933 MB, throughput 0.802266
Reading from 3139: heap size 937 MB, throughput 0.793812
Reading from 3139: heap size 928 MB, throughput 0.792353
Reading from 3139: heap size 933 MB, throughput 0.792948
Reading from 3139: heap size 923 MB, throughput 0.799385
Reading from 3139: heap size 929 MB, throughput 0.792262
Reading from 3139: heap size 924 MB, throughput 0.804345
Reading from 3140: heap size 495 MB, throughput 0.984934
Reading from 3139: heap size 928 MB, throughput 0.790219
Reading from 3139: heap size 921 MB, throughput 0.943574
Reading from 3140: heap size 500 MB, throughput 0.985867
Numeric result:
Recommendation: 2 clients, utility 0.797278:
    h1: 3789 MB (U(h) = 0.84306*h^0.0226933)
    h2: 1223 MB (U(h) = 0.778852*h^0.001)
Recommendation: 2 clients, utility 0.797278:
    h1: 3789 MB (U(h) = 0.84306*h^0.0226933)
    h2: 1223 MB (U(h) = 0.778852*h^0.001)
Reading from 3139: heap size 926 MB, throughput 0.95251
Reading from 3139: heap size 922 MB, throughput 0.916696
Reading from 3139: heap size 928 MB, throughput 0.86669
Reading from 3140: heap size 525 MB, throughput 0.984633
Reading from 3139: heap size 917 MB, throughput 0.827258
Reading from 3139: heap size 923 MB, throughput 0.819089
Reading from 3139: heap size 915 MB, throughput 0.823957
Reading from 3139: heap size 921 MB, throughput 0.817252
Reading from 3139: heap size 915 MB, throughput 0.825173
Reading from 3140: heap size 527 MB, throughput 0.983073
Reading from 3139: heap size 920 MB, throughput 0.846153
Reading from 3139: heap size 923 MB, throughput 0.861053
Reading from 3139: heap size 925 MB, throughput 0.809767
Reading from 3140: heap size 558 MB, throughput 0.980948
Reading from 3139: heap size 932 MB, throughput 0.656055
Reading from 3139: heap size 1010 MB, throughput 0.618494
Reading from 3140: heap size 559 MB, throughput 0.985234
Reading from 3139: heap size 1034 MB, throughput 0.665169
Reading from 3139: heap size 1041 MB, throughput 0.661954
Reading from 3139: heap size 1055 MB, throughput 0.70162
Reading from 3139: heap size 1058 MB, throughput 0.718244
Reading from 3139: heap size 1058 MB, throughput 0.723441
Reading from 3139: heap size 1063 MB, throughput 0.748158
Reading from 3139: heap size 1072 MB, throughput 0.746835
Reading from 3140: heap size 589 MB, throughput 0.984538
Reading from 3139: heap size 1074 MB, throughput 0.922985
Numeric result:
Recommendation: 2 clients, utility 0.649387:
    h1: 3789 MB (U(h) = 0.839259*h^0.0238139)
    h2: 1223 MB (U(h) = 0.631396*h^0.001)
Recommendation: 2 clients, utility 0.649387:
    h1: 3789 MB (U(h) = 0.839259*h^0.0238139)
    h2: 1223 MB (U(h) = 0.631396*h^0.001)
Reading from 3140: heap size 593 MB, throughput 0.979953
Reading from 3140: heap size 646 MB, throughput 0.990627
Reading from 3139: heap size 1085 MB, throughput 0.965343
Reading from 3140: heap size 650 MB, throughput 0.991599
Reading from 3139: heap size 1092 MB, throughput 0.971467
Reading from 3140: heap size 676 MB, throughput 0.989574
Reading from 3140: heap size 681 MB, throughput 0.989175
Reading from 3139: heap size 1104 MB, throughput 0.973762
Numeric result:
Recommendation: 2 clients, utility 0.622221:
    h1: 2967.02 MB (U(h) = 0.836178*h^0.0246823)
    h2: 2044.98 MB (U(h) = 0.53654*h^0.0170175)
Recommendation: 2 clients, utility 0.622221:
    h1: 2966.63 MB (U(h) = 0.836178*h^0.0246823)
    h2: 2045.37 MB (U(h) = 0.53654*h^0.0170175)
Reading from 3140: heap size 713 MB, throughput 0.990574
Reading from 3139: heap size 1106 MB, throughput 0.97522
Reading from 3140: heap size 715 MB, throughput 0.986784
Reading from 3140: heap size 746 MB, throughput 0.986588
Reading from 3139: heap size 1104 MB, throughput 0.971901
Reading from 3140: heap size 749 MB, throughput 0.987716
Reading from 3139: heap size 1109 MB, throughput 0.973446
Reading from 3140: heap size 782 MB, throughput 0.988871
Numeric result:
Recommendation: 2 clients, utility 0.686604:
    h1: 1358.48 MB (U(h) = 0.834986*h^0.02501)
    h2: 3653.52 MB (U(h) = 0.395416*h^0.0672564)
Recommendation: 2 clients, utility 0.686604:
    h1: 1358.57 MB (U(h) = 0.834986*h^0.02501)
    h2: 3653.43 MB (U(h) = 0.395416*h^0.0672564)
Reading from 3140: heap size 787 MB, throughput 0.989933
Reading from 3139: heap size 1100 MB, throughput 0.974928
Reading from 3140: heap size 819 MB, throughput 0.98982
Reading from 3139: heap size 1106 MB, throughput 0.972464
Reading from 3140: heap size 822 MB, throughput 0.989011
Reading from 3139: heap size 1106 MB, throughput 0.971493
Reading from 3140: heap size 863 MB, throughput 0.987937
Numeric result:
Recommendation: 2 clients, utility 0.73081:
    h1: 1029.64 MB (U(h) = 0.834618*h^0.0251093)
    h2: 3982.36 MB (U(h) = 0.328891*h^0.0971122)
Recommendation: 2 clients, utility 0.73081:
    h1: 1029.67 MB (U(h) = 0.834618*h^0.0251093)
    h2: 3982.33 MB (U(h) = 0.328891*h^0.0971122)
Reading from 3140: heap size 864 MB, throughput 0.986165
Reading from 3139: heap size 1108 MB, throughput 0.973827
Reading from 3140: heap size 906 MB, throughput 0.988217
Reading from 3139: heap size 1113 MB, throughput 0.972882
Reading from 3140: heap size 908 MB, throughput 0.98906
Numeric result:
Recommendation: 2 clients, utility 0.829828:
    h1: 686.82 MB (U(h) = 0.834984*h^0.025013)
    h2: 4325.18 MB (U(h) = 0.225662*h^0.157561)
Recommendation: 2 clients, utility 0.829828:
    h1: 686.653 MB (U(h) = 0.834984*h^0.025013)
    h2: 4325.35 MB (U(h) = 0.225662*h^0.157561)
Reading from 3139: heap size 1115 MB, throughput 0.972019
Reading from 3140: heap size 952 MB, throughput 0.990537
Reading from 3140: heap size 884 MB, throughput 0.94758
Reading from 3139: heap size 1119 MB, throughput 0.971824
Reading from 3140: heap size 834 MB, throughput 0.989842
Reading from 3139: heap size 1123 MB, throughput 0.970287
Reading from 3140: heap size 809 MB, throughput 0.987247
Numeric result:
Recommendation: 2 clients, utility 0.898274:
    h1: 565.566 MB (U(h) = 0.837174*h^0.0244387)
    h2: 4446.43 MB (U(h) = 0.183004*h^0.192123)
Recommendation: 2 clients, utility 0.898274:
    h1: 565.598 MB (U(h) = 0.837174*h^0.0244387)
    h2: 4446.4 MB (U(h) = 0.183004*h^0.192123)
Reading from 3139: heap size 1126 MB, throughput 0.967966
Reading from 3140: heap size 763 MB, throughput 0.991683
Reading from 3140: heap size 717 MB, throughput 0.991171
Reading from 3140: heap size 686 MB, throughput 0.987749
Reading from 3139: heap size 1131 MB, throughput 0.930406
Reading from 3140: heap size 651 MB, throughput 0.988158
Reading from 3140: heap size 636 MB, throughput 0.987845
Numeric result:
Recommendation: 2 clients, utility 1.03987:
    h1: 427.889 MB (U(h) = 0.836036*h^0.0247679)
    h2: 4584.11 MB (U(h) = 0.114313*h^0.265343)
Recommendation: 2 clients, utility 1.03987:
    h1: 427.894 MB (U(h) = 0.836036*h^0.0247679)
    h2: 4584.11 MB (U(h) = 0.114313*h^0.265343)
Reading from 3139: heap size 1225 MB, throughput 0.960343
Reading from 3140: heap size 606 MB, throughput 0.990914
Reading from 3140: heap size 567 MB, throughput 0.988618
Reading from 3139: heap size 1230 MB, throughput 0.970682
Reading from 3140: heap size 551 MB, throughput 0.988385
Reading from 3140: heap size 517 MB, throughput 0.989843
Reading from 3139: heap size 1234 MB, throughput 0.976937
Reading from 3140: heap size 498 MB, throughput 0.988142
Reading from 3140: heap size 477 MB, throughput 0.98813
Numeric result:
Recommendation: 2 clients, utility 1.16416:
    h1: 364.77 MB (U(h) = 0.835089*h^0.0251416)
    h2: 4647.23 MB (U(h) = 0.0803946*h^0.32031)
Recommendation: 2 clients, utility 1.16416:
    h1: 364.768 MB (U(h) = 0.835089*h^0.0251416)
    h2: 4647.23 MB (U(h) = 0.0803946*h^0.32031)
Reading from 3140: heap size 461 MB, throughput 0.987357
Reading from 3139: heap size 1235 MB, throughput 0.978148
Reading from 3140: heap size 440 MB, throughput 0.986088
Reading from 3140: heap size 421 MB, throughput 0.987174
Reading from 3140: heap size 406 MB, throughput 0.984565
Reading from 3140: heap size 393 MB, throughput 0.988158
Reading from 3139: heap size 1230 MB, throughput 0.979474
Reading from 3140: heap size 373 MB, throughput 0.986575
Reading from 3140: heap size 365 MB, throughput 0.988058
Reading from 3140: heap size 351 MB, throughput 0.990474
Reading from 3139: heap size 1234 MB, throughput 0.979405
Reading from 3140: heap size 369 MB, throughput 0.987973
Numeric result:
Recommendation: 2 clients, utility 1.09267:
    h1: 411.322 MB (U(h) = 0.836059*h^0.0252288)
    h2: 4600.68 MB (U(h) = 0.103992*h^0.282105)
Recommendation: 2 clients, utility 1.09267:
    h1: 411.431 MB (U(h) = 0.836059*h^0.0252288)
    h2: 4600.57 MB (U(h) = 0.103992*h^0.282105)
Reading from 3140: heap size 361 MB, throughput 0.986041
Reading from 3140: heap size 379 MB, throughput 0.990273
Reading from 3140: heap size 400 MB, throughput 0.986279
Reading from 3139: heap size 1223 MB, throughput 0.979446
Reading from 3140: heap size 435 MB, throughput 0.990496
Reading from 3140: heap size 409 MB, throughput 0.988965
Reading from 3140: heap size 435 MB, throughput 0.98788
Reading from 3139: heap size 1229 MB, throughput 0.9767
Reading from 3140: heap size 401 MB, throughput 0.990682
Reading from 3140: heap size 435 MB, throughput 0.985829
Reading from 3140: heap size 411 MB, throughput 0.989923
Numeric result:
Recommendation: 2 clients, utility 1.25121:
    h1: 334.996 MB (U(h) = 0.836621*h^0.0253467)
    h2: 4677 MB (U(h) = 0.0649017*h^0.353829)
Recommendation: 2 clients, utility 1.25121:
    h1: 335.036 MB (U(h) = 0.836621*h^0.0253467)
    h2: 4676.96 MB (U(h) = 0.0649017*h^0.353829)
Reading from 3139: heap size 1223 MB, throughput 0.977248
Reading from 3140: heap size 389 MB, throughput 0.987148
Reading from 3140: heap size 377 MB, throughput 0.988514
Reading from 3140: heap size 362 MB, throughput 0.990178
Reading from 3140: heap size 353 MB, throughput 0.984054
Reading from 3139: heap size 1226 MB, throughput 0.976326
Reading from 3140: heap size 349 MB, throughput 0.979139
Reading from 3140: heap size 332 MB, throughput 0.984306
Reading from 3140: heap size 346 MB, throughput 0.982892
Reading from 3140: heap size 324 MB, throughput 0.984066
Reading from 3140: heap size 354 MB, throughput 0.987905
Reading from 3139: heap size 1228 MB, throughput 0.976825
Reading from 3140: heap size 331 MB, throughput 0.986445
Reading from 3140: heap size 343 MB, throughput 0.987005
Numeric result:
Recommendation: 2 clients, utility 1.39371:
    h1: 290.214 MB (U(h) = 0.83822*h^0.0252576)
    h2: 4721.79 MB (U(h) = 0.0445443*h^0.410936)
Recommendation: 2 clients, utility 1.39371:
    h1: 290.218 MB (U(h) = 0.83822*h^0.0252576)
    h2: 4721.78 MB (U(h) = 0.0445443*h^0.410936)
Reading from 3140: heap size 320 MB, throughput 0.990128
Reading from 3140: heap size 310 MB, throughput 0.992714
Reading from 3140: heap size 294 MB, throughput 0.990077
Reading from 3139: heap size 1229 MB, throughput 0.975234
Reading from 3140: heap size 292 MB, throughput 0.982576
Reading from 3140: heap size 288 MB, throughput 0.986673
Reading from 3140: heap size 304 MB, throughput 0.984109
Reading from 3140: heap size 287 MB, throughput 0.984119
Reading from 3140: heap size 302 MB, throughput 0.987547
Reading from 3139: heap size 1232 MB, throughput 0.973157
Reading from 3140: heap size 283 MB, throughput 0.988937
Reading from 3140: heap size 300 MB, throughput 0.983485
Reading from 3140: heap size 285 MB, throughput 0.985391
Reading from 3140: heap size 301 MB, throughput 0.988647
Reading from 3140: heap size 285 MB, throughput 0.988991
Numeric result:
Recommendation: 2 clients, utility 1.48013:
    h1: 306.939 MB (U(h) = 0.821581*h^0.0289451)
    h2: 4705.06 MB (U(h) = 0.0358196*h^0.443706)
Recommendation: 2 clients, utility 1.48013:
    h1: 306.935 MB (U(h) = 0.821581*h^0.0289451)
    h2: 4705.07 MB (U(h) = 0.0358196*h^0.443706)
Reading from 3140: heap size 299 MB, throughput 0.936206
Reading from 3139: heap size 1236 MB, throughput 0.97157
Reading from 3140: heap size 304 MB, throughput 0.939455
Reading from 3140: heap size 307 MB, throughput 0.94474
Reading from 3140: heap size 326 MB, throughput 0.954486
Reading from 3140: heap size 316 MB, throughput 0.976689
Reading from 3140: heap size 305 MB, throughput 0.983661
Reading from 3139: heap size 1240 MB, throughput 0.971344
Reading from 3140: heap size 319 MB, throughput 0.980206
Reading from 3140: heap size 304 MB, throughput 0.978712
Reading from 3140: heap size 324 MB, throughput 0.979782
Reading from 3140: heap size 308 MB, throughput 0.98184
Reading from 3140: heap size 300 MB, throughput 0.986294
Reading from 3139: heap size 1246 MB, throughput 0.967818
Reading from 3140: heap size 308 MB, throughput 0.983868
Reading from 3140: heap size 300 MB, throughput 0.98246
Numeric result:
Recommendation: 2 clients, utility 1.84619:
    h1: 153.395 MB (U(h) = 0.880353*h^0.0177928)
    h2: 4858.61 MB (U(h) = 0.0160421*h^0.563532)
Recommendation: 2 clients, utility 1.84619:
    h1: 153.404 MB (U(h) = 0.880353*h^0.0177928)
    h2: 4858.6 MB (U(h) = 0.0160421*h^0.563532)
Reading from 3140: heap size 308 MB, throughput 0.987125
Reading from 3140: heap size 280 MB, throughput 0.814781
Reading from 3140: heap size 269 MB, throughput 0.983692
Reading from 3140: heap size 255 MB, throughput 0.89587
Reading from 3139: heap size 1254 MB, throughput 0.966313
Reading from 3140: heap size 249 MB, throughput 0.664818
Reading from 3140: heap size 235 MB, throughput 0.976023
Reading from 3140: heap size 230 MB, throughput 0.967818
Reading from 3140: heap size 223 MB, throughput 0.97464
Reading from 3140: heap size 218 MB, throughput 0.982297
Reading from 3140: heap size 208 MB, throughput 0.981605
Reading from 3140: heap size 207 MB, throughput 0.983762
Reading from 3140: heap size 197 MB, throughput 0.985184
Reading from 3139: heap size 1259 MB, throughput 0.967823
Reading from 3140: heap size 199 MB, throughput 0.979987
Reading from 3140: heap size 191 MB, throughput 0.972773
Reading from 3140: heap size 189 MB, throughput 0.979078
Reading from 3140: heap size 183 MB, throughput 0.979133
Reading from 3140: heap size 182 MB, throughput 0.98124
Reading from 3140: heap size 175 MB, throughput 0.979226
Reading from 3140: heap size 176 MB, throughput 0.979019
Reading from 3140: heap size 170 MB, throughput 0.973713
Reading from 3140: heap size 171 MB, throughput 0.980071
Reading from 3140: heap size 157 MB, throughput 0.967495
Reading from 3140: heap size 168 MB, throughput 0.972827
Numeric result:
Recommendation: 2 clients, utility 2.19416:
    h1: 107.591 MB (U(h) = 0.898075*h^0.0144868)
    h2: 4904.41 MB (U(h) = 0.00834294*h^0.660384)
Recommendation: 2 clients, utility 2.19416:
    h1: 107.587 MB (U(h) = 0.898075*h^0.0144868)
    h2: 4904.41 MB (U(h) = 0.00834294*h^0.660384)
Reading from 3140: heap size 161 MB, throughput 0.977875
Reading from 3139: heap size 1266 MB, throughput 0.968637
Reading from 3140: heap size 161 MB, throughput 0.973036
Reading from 3140: heap size 159 MB, throughput 0.979894
Reading from 3140: heap size 156 MB, throughput 0.978321
Reading from 3140: heap size 150 MB, throughput 0.975537
Reading from 3140: heap size 152 MB, throughput 0.974568
Reading from 3140: heap size 146 MB, throughput 0.920409
Reading from 3140: heap size 147 MB, throughput 0.985025
Reading from 3140: heap size 119 MB, throughput 0.933809
Reading from 3140: heap size 130 MB, throughput 0.922845
Reading from 3140: heap size 124 MB, throughput 0.948847
Reading from 3140: heap size 127 MB, throughput 0.942804
Reading from 3140: heap size 124 MB, throughput 0.1444
Reading from 3140: heap size 126 MB, throughput 0.908821
Reading from 3140: heap size 103 MB, throughput 0.737357
Reading from 3140: heap size 118 MB, throughput 0.78705
Reading from 3140: heap size 97 MB, throughput 0.84366
Reading from 3140: heap size 118 MB, throughput 0.917024
Reading from 3140: heap size 96 MB, throughput 0.95455
Reading from 3140: heap size 117 MB, throughput 0.972637
Reading from 3140: heap size 97 MB, throughput 0.986695
Reading from 3140: heap size 115 MB, throughput 0.961317
Reading from 3140: heap size 99 MB, throughput 0.963645
Reading from 3140: heap size 113 MB, throughput 0.856818
Reading from 3140: heap size 111 MB, throughput 0.768575
Reading from 3140: heap size 111 MB, throughput 0.734249
Reading from 3140: heap size 98 MB, throughput 0.778917
Reading from 3140: heap size 110 MB, throughput 0.868468
Reading from 3139: heap size 1269 MB, throughput 0.946874
Reading from 3140: heap size 93 MB, throughput 0.88036
Reading from 3140: heap size 109 MB, throughput 0.933338
Reading from 3140: heap size 99 MB, throughput 0.930141
Reading from 3140: heap size 115 MB, throughput 0.953648
Reading from 3140: heap size 99 MB, throughput 0.968459
Reading from 3140: heap size 113 MB, throughput 0.975309
Reading from 3140: heap size 99 MB, throughput 0.983355
Reading from 3140: heap size 111 MB, throughput 0.988048
Reading from 3140: heap size 98 MB, throughput 0.988363
Reading from 3140: heap size 110 MB, throughput 0.983896
Reading from 3140: heap size 101 MB, throughput 0.978639
Reading from 3140: heap size 109 MB, throughput 0.948451
Reading from 3140: heap size 107 MB, throughput 0.873233
Reading from 3140: heap size 107 MB, throughput 0.830075
Reading from 3140: heap size 110 MB, throughput 0.844567
Reading from 3140: heap size 107 MB, throughput 0.507163
Reading from 3140: heap size 112 MB, throughput 0.944234
Reading from 3140: heap size 103 MB, throughput 0.713326
Reading from 3140: heap size 110 MB, throughput 0.727631
Numeric result:
Recommendation: 2 clients, utility 2.44348:
    h1: 472.028 MB (U(h) = 0.630524*h^0.0758449)
    h2: 4539.97 MB (U(h) = 0.00522137*h^0.729472)
Recommendation: 2 clients, utility 2.44348:
    h1: 472.032 MB (U(h) = 0.630524*h^0.0758449)
    h2: 4539.97 MB (U(h) = 0.00522137*h^0.729472)
Reading from 3140: heap size 102 MB, throughput 0.848237
Reading from 3140: heap size 108 MB, throughput 0.910244
Reading from 3140: heap size 111 MB, throughput 0.946896
Reading from 3140: heap size 112 MB, throughput 0.969576
Reading from 3140: heap size 114 MB, throughput 0.981763
Reading from 3140: heap size 115 MB, throughput 0.982369
Reading from 3139: heap size 1381 MB, throughput 0.963099
Reading from 3140: heap size 117 MB, throughput 0.989986
Reading from 3140: heap size 118 MB, throughput 0.987187
Reading from 3140: heap size 121 MB, throughput 0.987155
Reading from 3140: heap size 121 MB, throughput 0.90352
Reading from 3140: heap size 122 MB, throughput 0.914051
Reading from 3140: heap size 125 MB, throughput 0.936579
Reading from 3140: heap size 133 MB, throughput 0.894158
Reading from 3140: heap size 135 MB, throughput 0.770454
Reading from 3140: heap size 141 MB, throughput 0.891068
Reading from 3140: heap size 142 MB, throughput 0.958402
Reading from 3140: heap size 149 MB, throughput 0.953831
Reading from 3140: heap size 150 MB, throughput 0.968693
Reading from 3140: heap size 157 MB, throughput 0.983345
Reading from 3140: heap size 158 MB, throughput 0.979748
Reading from 3140: heap size 165 MB, throughput 0.97508
Reading from 3140: heap size 165 MB, throughput 0.975679
Reading from 3139: heap size 1382 MB, throughput 0.971117
Reading from 3140: heap size 176 MB, throughput 0.949655
Reading from 3140: heap size 177 MB, throughput 0.961017
Reading from 3140: heap size 188 MB, throughput 0.932863
Reading from 3140: heap size 188 MB, throughput 0.925666
Reading from 3140: heap size 197 MB, throughput 0.953899
Reading from 3140: heap size 198 MB, throughput 0.963312
Reading from 3140: heap size 207 MB, throughput 0.971559
Reading from 3140: heap size 209 MB, throughput 0.974443
Reading from 3140: heap size 223 MB, throughput 0.981827
Reading from 3139: heap size 1388 MB, throughput 0.975728
Numeric result:
Recommendation: 2 clients, utility 2.48939:
    h1: 480.033 MB (U(h) = 0.625364*h^0.078223)
    h2: 4531.97 MB (U(h) = 0.00489712*h^0.738531)
Recommendation: 2 clients, utility 2.48939:
    h1: 480.014 MB (U(h) = 0.625364*h^0.078223)
    h2: 4531.99 MB (U(h) = 0.00489712*h^0.738531)
Reading from 3140: heap size 223 MB, throughput 0.976633
Reading from 3140: heap size 236 MB, throughput 0.979073
Reading from 3140: heap size 237 MB, throughput 0.982557
Reading from 3140: heap size 251 MB, throughput 0.984446
Reading from 3140: heap size 252 MB, throughput 0.985363
Reading from 3140: heap size 266 MB, throughput 0.973261
Reading from 3140: heap size 266 MB, throughput 0.958996
Reading from 3139: heap size 1390 MB, throughput 0.973166
Reading from 3140: heap size 287 MB, throughput 0.979917
Reading from 3140: heap size 287 MB, throughput 0.979939
Reading from 3140: heap size 303 MB, throughput 0.918265
Reading from 3140: heap size 303 MB, throughput 0.975788
Reading from 3140: heap size 326 MB, throughput 0.97893
Reading from 3140: heap size 329 MB, throughput 0.980862
Reading from 3140: heap size 353 MB, throughput 0.981825
Numeric result:
Recommendation: 2 clients, utility 2.24117:
    h1: 547.924 MB (U(h) = 0.61435*h^0.0818604)
    h2: 4464.08 MB (U(h) = 0.00800971*h^0.666967)
Recommendation: 2 clients, utility 2.24117:
    h1: 547.903 MB (U(h) = 0.61435*h^0.0818604)
    h2: 4464.1 MB (U(h) = 0.00800971*h^0.666967)
Reading from 3140: heap size 353 MB, throughput 0.982235
Reading from 3140: heap size 379 MB, throughput 0.977792
Reading from 3140: heap size 379 MB, throughput 0.978585
Reading from 3140: heap size 409 MB, throughput 0.97879
Reading from 3140: heap size 412 MB, throughput 0.977199
Reading from 3140: heap size 443 MB, throughput 0.980915
Reading from 3140: heap size 445 MB, throughput 0.975158
Reading from 3140: heap size 475 MB, throughput 0.976104
Reading from 3140: heap size 490 MB, throughput 0.97481
Reading from 3139: heap size 1387 MB, throughput 0.985265
Numeric result:
Recommendation: 2 clients, utility 2.29429:
    h1: 475.321 MB (U(h) = 0.6446*h^0.0722928)
    h2: 4536.68 MB (U(h) = 0.0068331*h^0.690018)
Recommendation: 2 clients, utility 2.29429:
    h1: 475.307 MB (U(h) = 0.6446*h^0.0722928)
    h2: 4536.69 MB (U(h) = 0.0068331*h^0.690018)
Reading from 3140: heap size 523 MB, throughput 0.983837
Reading from 3140: heap size 475 MB, throughput 0.983491
Reading from 3140: heap size 505 MB, throughput 0.98861
Reading from 3140: heap size 459 MB, throughput 0.985114
Reading from 3140: heap size 488 MB, throughput 0.989233
Reading from 3140: heap size 444 MB, throughput 0.984953
Reading from 3140: heap size 471 MB, throughput 0.98868
Reading from 3140: heap size 471 MB, throughput 0.987212
Reading from 3140: heap size 498 MB, throughput 0.989562
Numeric result:
Recommendation: 2 clients, utility 2.27517:
    h1: 432.953 MB (U(h) = 0.667392*h^0.0652456)
    h2: 4579.05 MB (U(h) = 0.0068331*h^0.690018)
Recommendation: 2 clients, utility 2.27517:
    h1: 432.976 MB (U(h) = 0.667392*h^0.0652456)
    h2: 4579.02 MB (U(h) = 0.0068331*h^0.690018)
Reading from 3140: heap size 458 MB, throughput 0.987579
Reading from 3139: heap size 1390 MB, throughput 0.985852
Reading from 3140: heap size 446 MB, throughput 0.989964
Reading from 3140: heap size 428 MB, throughput 0.98774
Reading from 3140: heap size 450 MB, throughput 0.989349
Reading from 3140: heap size 414 MB, throughput 0.989396
Reading from 3140: heap size 435 MB, throughput 0.983919
Reading from 3139: heap size 1398 MB, throughput 0.983593
Reading from 3140: heap size 412 MB, throughput 0.988748
Reading from 3139: heap size 1426 MB, throughput 0.970897
Reading from 3139: heap size 1421 MB, throughput 0.949924
Reading from 3139: heap size 1449 MB, throughput 0.913773
Reading from 3139: heap size 1476 MB, throughput 0.862754
Reading from 3140: heap size 421 MB, throughput 0.988268
Reading from 3139: heap size 1498 MB, throughput 0.798446
Reading from 3139: heap size 1531 MB, throughput 0.765454
Reading from 3140: heap size 425 MB, throughput 0.985823
Reading from 3139: heap size 1543 MB, throughput 0.710378
Numeric result:
Recommendation: 2 clients, utility 2.59613:
    h1: 375.147 MB (U(h) = 0.669983*h^0.0644747)
    h2: 4636.85 MB (U(h) = 0.00316584*h^0.796952)
Recommendation: 2 clients, utility 2.59613:
    h1: 375.13 MB (U(h) = 0.669983*h^0.0644747)
    h2: 4636.87 MB (U(h) = 0.00316584*h^0.796952)
Reading from 3140: heap size 450 MB, throughput 0.988197
Reading from 3140: heap size 415 MB, throughput 0.984437
Reading from 3139: heap size 1578 MB, throughput 0.950512
Reading from 3140: heap size 401 MB, throughput 0.988754
Reading from 3140: heap size 386 MB, throughput 0.982106
Reading from 3140: heap size 386 MB, throughput 0.982067
Reading from 3139: heap size 1542 MB, throughput 0.970614
Reading from 3140: heap size 360 MB, throughput 0.987036
Reading from 3140: heap size 380 MB, throughput 0.982975
Reading from 3140: heap size 363 MB, throughput 0.988515
Reading from 3140: heap size 371 MB, throughput 0.986927
Numeric result:
Recommendation: 2 clients, utility 2.65195:
    h1: 383.516 MB (U(h) = 0.662293*h^0.0672431)
    h2: 4628.48 MB (U(h) = 0.00284452*h^0.811571)
Recommendation: 2 clients, utility 2.65195:
    h1: 383.497 MB (U(h) = 0.662293*h^0.0672431)
    h2: 4628.5 MB (U(h) = 0.00284452*h^0.811571)
Reading from 3140: heap size 374 MB, throughput 0.988629
Reading from 3139: heap size 1553 MB, throughput 0.98417
Reading from 3140: heap size 393 MB, throughput 0.991927
Reading from 3140: heap size 365 MB, throughput 0.987371
Reading from 3140: heap size 383 MB, throughput 0.988035
Reading from 3140: heap size 382 MB, throughput 0.990236
Reading from 3139: heap size 1565 MB, throughput 0.987718
Reading from 3140: heap size 398 MB, throughput 0.985209
Reading from 3140: heap size 385 MB, throughput 0.989221
Reading from 3140: heap size 364 MB, throughput 0.98845
Reading from 3140: heap size 383 MB, throughput 0.989011
Reading from 3140: heap size 358 MB, throughput 0.991077
Numeric result:
Recommendation: 2 clients, utility 2.71124:
    h1: 385.395 MB (U(h) = 0.653041*h^0.0692481)
    h2: 4626.6 MB (U(h) = 0.00246784*h^0.831275)
Recommendation: 2 clients, utility 2.71124:
    h1: 385.411 MB (U(h) = 0.653041*h^0.0692481)
    h2: 4626.59 MB (U(h) = 0.00246784*h^0.831275)
Reading from 3139: heap size 1589 MB, throughput 0.989767
Reading from 3140: heap size 386 MB, throughput 0.9933
Reading from 3140: heap size 358 MB, throughput 0.992455
Reading from 3140: heap size 374 MB, throughput 0.992571
Reading from 3140: heap size 389 MB, throughput 0.99023
Reading from 3140: heap size 371 MB, throughput 0.992145
Reading from 3139: heap size 1594 MB, throughput 0.988004
Reading from 3140: heap size 379 MB, throughput 0.992946
Client 3140 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 3139: heap size 1584 MB, throughput 0.98935
Reading from 3139: heap size 1597 MB, throughput 0.989554
Recommendation: one client; give it all the memory
Reading from 3139: heap size 1567 MB, throughput 0.989534
Reading from 3139: heap size 1346 MB, throughput 0.988925
Recommendation: one client; give it all the memory
Reading from 3139: heap size 1543 MB, throughput 0.988361
Reading from 3139: heap size 1379 MB, throughput 0.987573
Recommendation: one client; give it all the memory
Reading from 3139: heap size 1525 MB, throughput 0.986692
Reading from 3139: heap size 1540 MB, throughput 0.98612
Recommendation: one client; give it all the memory
Reading from 3139: heap size 1533 MB, throughput 0.984842
Recommendation: one client; give it all the memory
Reading from 3139: heap size 1537 MB, throughput 0.983875
Reading from 3139: heap size 1542 MB, throughput 0.982792
Recommendation: one client; give it all the memory
Reading from 3139: heap size 1545 MB, throughput 0.982124
Reading from 3139: heap size 1554 MB, throughput 0.982398
Recommendation: one client; give it all the memory
Reading from 3139: heap size 1556 MB, throughput 0.981901
Reading from 3139: heap size 1564 MB, throughput 0.981422
Recommendation: one client; give it all the memory
Reading from 3139: heap size 1566 MB, throughput 0.980986
Recommendation: one client; give it all the memory
Reading from 3139: heap size 1574 MB, throughput 0.981161
Reading from 3139: heap size 1575 MB, throughput 0.980601
Recommendation: one client; give it all the memory
Reading from 3139: heap size 1584 MB, throughput 0.980842
Reading from 3139: heap size 1584 MB, throughput 0.980582
Recommendation: one client; give it all the memory
Reading from 3139: heap size 1592 MB, throughput 0.970955
Recommendation: one client; give it all the memory
Reading from 3139: heap size 1733 MB, throughput 0.982341
Reading from 3139: heap size 1730 MB, throughput 0.987046
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 3139: heap size 1742 MB, throughput 0.994255
Reading from 3139: heap size 1733 MB, throughput 0.992878
Reading from 3139: heap size 1623 MB, throughput 0.985472
Reading from 3139: heap size 1723 MB, throughput 0.973759
Reading from 3139: heap size 1742 MB, throughput 0.952072
Recommendation: one client; give it all the memory
Reading from 3139: heap size 1779 MB, throughput 0.928233
Client 3139 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
