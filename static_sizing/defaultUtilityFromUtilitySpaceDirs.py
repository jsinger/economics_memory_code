#!/usr/bin/env python
# Generate default utility runs based on existing utility space parameters

import argparse
import argtypes
import staticutility_core as su
from defaultUtilityCombination import defaultUtilityCombination


parser = argparse.ArgumentParser(description="Generate default utility runs based on existing utility space parameters")

parser.add_argument("cgroupPath", type=argtypes.existing_absolute_dir, help="location of the cgroups control files")
parser.add_argument("dirs", nargs="+", type=argtypes.existing_absolute_dir, help="Existing utility space dirs")

args = parser.parse_args()


combinations = []
seconds = 0

for d in args.dirs:
    datadir = su.UtilitySpaceDir(d)
    time = datadir.runTime()
    runs = datadir.runs()
    seconds += (su.timeToSeconds(time) * runs)
    combinations.append((datadir.totalMem(), time, runs, datadir.parseBenchmarksFile()))

print su.estimateRuntime(1, seconds)

print "#"*80
print

for (m, t, r, bids) in combinations:
    defaultUtilityCombination(m, t, r, bids, args.cgroupPath)
