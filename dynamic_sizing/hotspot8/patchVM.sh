#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
VM_DIR="${DIR}/openjdk8"
DEST_DIR="${VM_DIR}/jdk8/hotspot/src/share/vm"
COMMON_DIR="${DIR}/../common"

function simpleLink()
{
    if [ -z "${2}" ]; then
        SRC_DIR="${COMMON_DIR}"
    else
        SRC_DIR="${2}"
    fi

    SRC="${SRC_DIR}/${1}"
    DST="${DEST_DIR}/gc_implementation/shared/${1}"

    if [ ! -e "${DST}" ]; then
        ln -s "${SRC}" "${DST}"
    fi

    touch "${DST}"
}

function replaceLink()
{
    SRC="${DIR}/${1}"
    DST="${DEST_DIR}/${2}/${1}"

    if [ ! -e "${DST}.original" ]; then
        mv "${DST}" "${DST}.original"
        ln -s "${SRC}" "${DST}"
    fi

    touch "${DST}"
}

simpleLink 'AdaptiveWeightedAverage.hpp'
simpleLink 'AdaptiveWeightedAverage.cpp'
simpleLink 'AveragedVengerov.hpp'
simpleLink 'AveragedVengerov.cpp'
simpleLink 'Messages.hpp'
simpleLink 'DaemonProxy.hpp'
simpleLink 'DaemonProxy.cpp'
simpleLink 'Stopwatch.hpp'
simpleLink 'Stopwatch.cpp'
simpleLink 'MajorOnlyThroughput.hpp'
simpleLink 'MajorOnlyThroughput.cpp'
simpleLink 'Mutex.hpp'
simpleLink 'Mutex.cpp'
simpleLink 'NaiveVengerov.hpp'
simpleLink 'NaiveVengerov.cpp'
simpleLink 'ProportionalThroughput.hpp'
simpleLink 'ProportionalThroughput.cpp'
simpleLink 'ReadingSender.hpp'
simpleLink 'ReadingSender.cpp'
simpleLink 'RecommendationReceiver.hpp'
simpleLink 'RecommendationReceiver.cpp'
simpleLink 'ReceiverThread.hpp'
simpleLink 'RecvSock.hpp'
simpleLink 'RecvSock.cpp'
simpleLink 'SendSock.hpp'
simpleLink 'SendSock.cpp'
simpleLink 'SimpleThroughput.hpp'
simpleLink 'SimpleThroughput.cpp'
simpleLink 'SimpleSmoothingThroughput.hpp'
simpleLink 'SimpleSmoothingThroughput.cpp'
simpleLink 'Thread.hpp'
simpleLink 'Thread.cpp'
simpleLink 'ThroughputCalculator.hpp'
simpleLink 'Utilities.hpp'
simpleLink 'Utilities.cpp'

simpleLink 'RecommendationSystem.hpp' "${DIR}"
simpleLink 'RecommendationSystem.cpp' "${DIR}"
simpleLink 'HotspotNaiveVengerov.hpp' "${DIR}"
simpleLink 'HotspotNaiveVengerov.cpp' "${DIR}"
simpleLink 'HotspotSimpleThroughput.hpp' "${DIR}"
simpleLink 'HotspotSimpleThroughput.cpp' "${DIR}"
simpleLink 'HotspotAveragedVengerov.hpp' "${DIR}"
simpleLink 'HotspotAveragedVengerov.cpp' "${DIR}"
simpleLink 'HotspotMajorOnly.hpp' "${DIR}"
simpleLink 'HotspotMajorOnly.cpp' "${DIR}"
simpleLink 'HotspotSimpleSmoothing.hpp' "${DIR}"
simpleLink 'HotspotSimpleSmoothing.cpp' "${DIR}"
simpleLink 'HotspotProportional.hpp' "${DIR}"
simpleLink 'HotspotProportional.cpp' "${DIR}"

replaceLink 'psAdaptiveSizePolicy.cpp' 'gc_implementation/parallelScavenge'
replaceLink 'psMarkSweep.cpp' 'gc_implementation/parallelScavenge'
replaceLink 'psParallelCompact.cpp' 'gc_implementation/parallelScavenge'
replaceLink 'psScavenge.cpp' 'gc_implementation/parallelScavenge'
replaceLink 'globals.hpp' 'runtime'

touch "${VM_DIR}/patch.done"
