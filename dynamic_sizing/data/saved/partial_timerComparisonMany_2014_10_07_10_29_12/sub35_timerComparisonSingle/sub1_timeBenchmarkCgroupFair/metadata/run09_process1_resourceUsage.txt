	Command being timed: "cgexec -g memory:economem /home/callum/economics_memory_code/dynamic_sizing/hotspot8/openjdk8/jdk8/build/linux-x86_64-normal-server-release/jdk/bin/java -XX:-EconomemVengerovThroughput -Xms10m -Xmx288m -XX:+DisableExplicitGC -XX:+UseAdaptiveSizePolicyWithSystemGC -jar /home/callum/economics_memory_code/dynamic_sizing/haskell_common/dacapo-9.12-bach.jar --no-pre-iteration-gc --scratch-directory /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub35_timerComparisonSingle/sub1_timeBenchmarkCgroupFair/scratch1 -t 2 -n 13 -s large h2"
	User time (seconds): 826.00
	System time (seconds): 5.92
	Percent of CPU this job got: 168%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 8:15.19
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 382676
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 7849
	Minor (reclaiming a frame) page faults: 244245
	Voluntary context switches: 242333
	Involuntary context switches: 489513
	Swaps: 0
	File system inputs: 502744
	File system outputs: 16624
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
