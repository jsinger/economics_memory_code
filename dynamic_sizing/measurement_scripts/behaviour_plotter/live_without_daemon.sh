#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

THROUGHPUT_MODEL="simple"

if [ ! -z "${1}" ]; then
    THROUGHPUT_MODEL="${1}"
fi

function cleanup()
{
    test ! -z "${UNBUFFER}" && kill "${UNBUFFER}" &>/dev/null
    test ! -z "${PLOT}" && kill "${PLOT}" &>/dev/null
    wait
    rm "${FIFO}"
    exit
}

trap "cleanup" SIGINT SIGTERM SIGHUP

FIFO="$(mktemp --dry-run)"
mkfifo "${FIFO}"

unbuffer "${DIR}/../../reading_pipe/rp" "${THROUGHPUT_MODEL}" > "${FIFO}" &
UNBUFFER="${!}"

python "${DIR}/plot_behaviour_live.py" "${THROUGHPUT_MODEL}" < "${FIFO}" &
PLOT="${!}"

wait
cleanup
