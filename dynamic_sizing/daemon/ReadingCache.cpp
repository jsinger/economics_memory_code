#include <cstring>
#include "ReadingCache.hpp"
#include "LRUCache.hpp"
#include "UnboundedCache.hpp"

const std::string ReadingCache::DEFAULT_CACHE = LRUCache::NAME;

ReadingCache* ReadingCache::fromName(const std::string& name)
{
    if (name == LRUCache::NAME)
        return new LRUCache;
    else if (name == UnboundedCache::NAME)
        return new UnboundedCache;
    else
        return NULL;
}

std::vector<std::string> ReadingCache::allCacheNames()
{
    return { LRUCache::NAME, UnboundedCache::NAME };
}
