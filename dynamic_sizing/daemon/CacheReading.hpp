#ifndef CACHEREADING_HPP
#define CACHEREADING_HPP

#include <cstdio>
#include <sys/types.h>
#include <AdaptiveWeightedAverage.hpp>

#define USE_AVERAGES

class CacheReading
{
public:
    CacheReading(unsigned long long heapSizeMB, double throughput);
    CacheReading& operator=(const CacheReading& r);
    unsigned long long getHeapSizeMB() const;
    double getThroughput() const;
    void updateReading(double throughput);
    void dumpTo(FILE* fd) const;

private:
    unsigned long long m_HeapSizeMB;
#ifdef USE_AVERAGES
    economem::AdaptiveWeightedAverage m_Throughput;
#else
    double m_Throughput;
#endif
};

#endif
