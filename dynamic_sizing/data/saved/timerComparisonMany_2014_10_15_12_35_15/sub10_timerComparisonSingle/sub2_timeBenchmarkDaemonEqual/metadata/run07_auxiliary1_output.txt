economemd
    total memory: 7314 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub10_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 31543: heap size 9 MB, throughput 0.989688
Clients: 1
Client 31543 has a minimum heap size of 8 MB
Reading from 31541: heap size 9 MB, throughput 0.991361
Clients: 2
Client 31541 has a minimum heap size of 1211 MB
Reading from 31543: heap size 9 MB, throughput 0.97403
Reading from 31541: heap size 9 MB, throughput 0.980921
Reading from 31543: heap size 9 MB, throughput 0.968976
Reading from 31541: heap size 9 MB, throughput 0.880725
Reading from 31543: heap size 9 MB, throughput 0.862155
Reading from 31543: heap size 11 MB, throughput 0.951683
Reading from 31541: heap size 9 MB, throughput 0.877772
Reading from 31543: heap size 11 MB, throughput 0.97891
Reading from 31541: heap size 11 MB, throughput 0.975108
Reading from 31543: heap size 16 MB, throughput 0.978314
Reading from 31543: heap size 16 MB, throughput 0.979845
Reading from 31541: heap size 11 MB, throughput 0.979617
Reading from 31543: heap size 23 MB, throughput 0.952077
Reading from 31543: heap size 24 MB, throughput 0.595617
Reading from 31541: heap size 17 MB, throughput 0.940168
Reading from 31543: heap size 36 MB, throughput 0.987107
Reading from 31543: heap size 36 MB, throughput 0.972317
Reading from 31543: heap size 43 MB, throughput 0.987995
Reading from 31541: heap size 17 MB, throughput 0.590471
Reading from 31543: heap size 43 MB, throughput 0.981545
Reading from 31541: heap size 30 MB, throughput 0.977555
Reading from 31543: heap size 51 MB, throughput 0.988331
Reading from 31543: heap size 51 MB, throughput 0.980742
Reading from 31541: heap size 31 MB, throughput 0.905086
Reading from 31543: heap size 61 MB, throughput 0.992492
Reading from 31543: heap size 61 MB, throughput 0.984401
Reading from 31541: heap size 34 MB, throughput 0.507684
Reading from 31543: heap size 68 MB, throughput 0.989145
Reading from 31541: heap size 46 MB, throughput 0.78301
Reading from 31543: heap size 68 MB, throughput 0.991112
Reading from 31543: heap size 74 MB, throughput 0.987884
Reading from 31541: heap size 50 MB, throughput 0.438837
Reading from 31541: heap size 65 MB, throughput 0.786386
Reading from 31543: heap size 75 MB, throughput 0.993948
Reading from 31541: heap size 69 MB, throughput 0.78057
Reading from 31543: heap size 81 MB, throughput 0.988241
Reading from 31541: heap size 70 MB, throughput 0.790778
Reading from 31543: heap size 81 MB, throughput 0.992731
Reading from 31541: heap size 75 MB, throughput 0.184135
Reading from 31543: heap size 88 MB, throughput 0.992981
Reading from 31541: heap size 106 MB, throughput 0.809628
Reading from 31543: heap size 88 MB, throughput 0.987688
Reading from 31541: heap size 109 MB, throughput 0.817946
Reading from 31541: heap size 110 MB, throughput 0.74773
Reading from 31543: heap size 94 MB, throughput 0.992755
Reading from 31541: heap size 117 MB, throughput 0.79672
Reading from 31543: heap size 94 MB, throughput 0.990211
Reading from 31543: heap size 99 MB, throughput 0.993072
Reading from 31543: heap size 99 MB, throughput 0.989145
Reading from 31541: heap size 118 MB, throughput 0.134463
Reading from 31543: heap size 105 MB, throughput 0.99598
Reading from 31541: heap size 159 MB, throughput 0.728386
Reading from 31541: heap size 159 MB, throughput 0.661352
Reading from 31543: heap size 105 MB, throughput 0.994104
Reading from 31543: heap size 110 MB, throughput 0.996259
Reading from 31541: heap size 163 MB, throughput 0.231913
Reading from 31543: heap size 110 MB, throughput 0.995325
Reading from 31541: heap size 202 MB, throughput 0.606245
Reading from 31541: heap size 209 MB, throughput 0.742532
Reading from 31543: heap size 114 MB, throughput 0.973139
Reading from 31541: heap size 209 MB, throughput 0.727284
Reading from 31543: heap size 114 MB, throughput 0.993614
Reading from 31541: heap size 213 MB, throughput 0.666132
Reading from 31543: heap size 121 MB, throughput 0.995554
Reading from 31541: heap size 219 MB, throughput 0.601115
Reading from 31543: heap size 121 MB, throughput 0.993828
Reading from 31541: heap size 227 MB, throughput 0.659732
Reading from 31543: heap size 126 MB, throughput 0.931348
Reading from 31541: heap size 234 MB, throughput 0.686345
Reading from 31543: heap size 127 MB, throughput 0.987944
Reading from 31541: heap size 238 MB, throughput 0.597897
Reading from 31541: heap size 248 MB, throughput 0.580713
Reading from 31543: heap size 138 MB, throughput 0.995548
Reading from 31543: heap size 139 MB, throughput 0.994036
Reading from 31543: heap size 147 MB, throughput 0.997469
Reading from 31541: heap size 253 MB, throughput 0.155244
Reading from 31543: heap size 148 MB, throughput 0.995309
Reading from 31541: heap size 299 MB, throughput 0.445902
Reading from 31541: heap size 308 MB, throughput 0.574656
Reading from 31543: heap size 155 MB, throughput 0.997567
Reading from 31541: heap size 311 MB, throughput 0.604376
Reading from 31543: heap size 155 MB, throughput 0.997685
Reading from 31543: heap size 161 MB, throughput 0.997481
Reading from 31541: heap size 317 MB, throughput 0.113622
Reading from 31541: heap size 360 MB, throughput 0.55422
Reading from 31543: heap size 161 MB, throughput 0.996563
Reading from 31541: heap size 357 MB, throughput 0.646641
Reading from 31541: heap size 362 MB, throughput 0.634059
Reading from 31543: heap size 165 MB, throughput 0.997331
Reading from 31541: heap size 363 MB, throughput 0.577471
Reading from 31543: heap size 166 MB, throughput 0.996299
Reading from 31543: heap size 169 MB, throughput 0.996591
Reading from 31541: heap size 366 MB, throughput 0.120619
Reading from 31541: heap size 415 MB, throughput 0.509062
Reading from 31543: heap size 169 MB, throughput 0.98982
Reading from 31541: heap size 419 MB, throughput 0.522457
Reading from 31543: heap size 173 MB, throughput 0.996995
Reading from 31541: heap size 420 MB, throughput 0.571984
Reading from 31541: heap size 422 MB, throughput 0.52975
Reading from 31543: heap size 173 MB, throughput 0.997022
Reading from 31541: heap size 427 MB, throughput 0.513395
Reading from 31543: heap size 178 MB, throughput 0.996803
Reading from 31541: heap size 432 MB, throughput 0.516593
Reading from 31543: heap size 178 MB, throughput 0.997
Reading from 31543: heap size 182 MB, throughput 0.995698
Reading from 31543: heap size 182 MB, throughput 0.996568
Equal recommendation: 3657 MB each
Reading from 31541: heap size 437 MB, throughput 0.115915
Reading from 31543: heap size 185 MB, throughput 0.997382
Reading from 31541: heap size 493 MB, throughput 0.40686
Reading from 31543: heap size 186 MB, throughput 0.996591
Reading from 31541: heap size 498 MB, throughput 0.496369
Reading from 31543: heap size 189 MB, throughput 0.99531
Reading from 31543: heap size 189 MB, throughput 0.997013
Reading from 31543: heap size 192 MB, throughput 0.995757
Reading from 31541: heap size 500 MB, throughput 0.124066
Reading from 31541: heap size 563 MB, throughput 0.496864
Reading from 31543: heap size 193 MB, throughput 0.995259
Reading from 31541: heap size 564 MB, throughput 0.603823
Reading from 31543: heap size 195 MB, throughput 0.995176
Reading from 31541: heap size 565 MB, throughput 0.512641
Reading from 31543: heap size 196 MB, throughput 0.993543
Reading from 31541: heap size 567 MB, throughput 0.570163
Reading from 31541: heap size 572 MB, throughput 0.59574
Reading from 31543: heap size 201 MB, throughput 0.997425
Reading from 31543: heap size 201 MB, throughput 0.995633
Reading from 31543: heap size 205 MB, throughput 0.99596
Reading from 31543: heap size 205 MB, throughput 0.996941
Reading from 31541: heap size 580 MB, throughput 0.103632
Reading from 31541: heap size 649 MB, throughput 0.465624
Reading from 31543: heap size 209 MB, throughput 0.996217
Reading from 31541: heap size 656 MB, throughput 0.567104
Reading from 31543: heap size 209 MB, throughput 0.992366
Reading from 31541: heap size 658 MB, throughput 0.633143
Reading from 31543: heap size 213 MB, throughput 0.99423
Reading from 31543: heap size 213 MB, throughput 0.995638
Reading from 31541: heap size 663 MB, throughput 0.856444
Reading from 31543: heap size 219 MB, throughput 0.99615
Reading from 31543: heap size 219 MB, throughput 0.995308
Reading from 31541: heap size 663 MB, throughput 0.814151
Reading from 31543: heap size 224 MB, throughput 0.995675
Reading from 31543: heap size 224 MB, throughput 0.995014
Reading from 31541: heap size 679 MB, throughput 0.817379
Reading from 31541: heap size 685 MB, throughput 0.462303
Reading from 31543: heap size 229 MB, throughput 0.998128
Reading from 31541: heap size 706 MB, throughput 0.256706
Reading from 31541: heap size 716 MB, throughput 0.476834
Reading from 31543: heap size 229 MB, throughput 0.995124
Reading from 31543: heap size 234 MB, throughput 0.99696
Reading from 31543: heap size 234 MB, throughput 0.995125
Reading from 31543: heap size 239 MB, throughput 0.998637
Reading from 31541: heap size 723 MB, throughput 0.0193584
Reading from 31541: heap size 798 MB, throughput 0.1843
Reading from 31543: heap size 239 MB, throughput 0.996235
Equal recommendation: 3657 MB each
Reading from 31543: heap size 244 MB, throughput 0.996575
Reading from 31541: heap size 680 MB, throughput 0.0382326
Reading from 31543: heap size 244 MB, throughput 0.996911
Reading from 31541: heap size 859 MB, throughput 0.247839
Reading from 31541: heap size 865 MB, throughput 0.552989
Reading from 31543: heap size 248 MB, throughput 0.997817
Reading from 31541: heap size 867 MB, throughput 0.848704
Reading from 31543: heap size 248 MB, throughput 0.997033
Reading from 31541: heap size 729 MB, throughput 0.499922
Reading from 31541: heap size 860 MB, throughput 0.495979
Reading from 31541: heap size 864 MB, throughput 0.519225
Reading from 31543: heap size 252 MB, throughput 0.997678
Reading from 31543: heap size 252 MB, throughput 0.997473
Reading from 31543: heap size 256 MB, throughput 0.997824
Reading from 31543: heap size 256 MB, throughput 0.997348
Reading from 31541: heap size 861 MB, throughput 0.163369
Reading from 31541: heap size 947 MB, throughput 0.47306
Reading from 31543: heap size 260 MB, throughput 0.960258
Reading from 31541: heap size 946 MB, throughput 0.919326
Reading from 31541: heap size 952 MB, throughput 0.874352
Reading from 31543: heap size 262 MB, throughput 0.996514
Reading from 31541: heap size 960 MB, throughput 0.939592
Reading from 31541: heap size 961 MB, throughput 0.874988
Reading from 31543: heap size 268 MB, throughput 0.99715
Reading from 31541: heap size 953 MB, throughput 0.892346
Reading from 31543: heap size 268 MB, throughput 0.997409
Reading from 31541: heap size 759 MB, throughput 0.838471
Reading from 31541: heap size 926 MB, throughput 0.779369
Reading from 31543: heap size 275 MB, throughput 0.996827
Reading from 31541: heap size 784 MB, throughput 0.765613
Reading from 31541: heap size 909 MB, throughput 0.76476
Reading from 31543: heap size 276 MB, throughput 0.996668
Reading from 31541: heap size 789 MB, throughput 0.797766
Reading from 31541: heap size 896 MB, throughput 0.771486
Reading from 31541: heap size 794 MB, throughput 0.802534
Reading from 31543: heap size 283 MB, throughput 0.996908
Reading from 31541: heap size 886 MB, throughput 0.803205
Reading from 31543: heap size 284 MB, throughput 0.996474
Reading from 31541: heap size 800 MB, throughput 0.781088
Reading from 31541: heap size 879 MB, throughput 0.822459
Reading from 31543: heap size 292 MB, throughput 0.997402
Reading from 31541: heap size 886 MB, throughput 0.792785
Reading from 31543: heap size 292 MB, throughput 0.99731
Reading from 31541: heap size 875 MB, throughput 0.969249
Reading from 31543: heap size 301 MB, throughput 0.998467
Reading from 31543: heap size 301 MB, throughput 0.997673
Reading from 31543: heap size 308 MB, throughput 0.998094
Reading from 31541: heap size 880 MB, throughput 0.962401
Reading from 31541: heap size 882 MB, throughput 0.811755
Reading from 31543: heap size 308 MB, throughput 0.997555
Reading from 31541: heap size 885 MB, throughput 0.71416
Reading from 31541: heap size 891 MB, throughput 0.750221
Equal recommendation: 3657 MB each
Reading from 31541: heap size 891 MB, throughput 0.723006
Reading from 31543: heap size 314 MB, throughput 0.997499
Reading from 31541: heap size 899 MB, throughput 0.737922
Reading from 31541: heap size 899 MB, throughput 0.747909
Reading from 31543: heap size 315 MB, throughput 0.997373
Reading from 31541: heap size 905 MB, throughput 0.744674
Reading from 31543: heap size 322 MB, throughput 0.99716
Reading from 31541: heap size 906 MB, throughput 0.74941
Reading from 31541: heap size 911 MB, throughput 0.782941
Reading from 31543: heap size 322 MB, throughput 0.996933
Reading from 31541: heap size 912 MB, throughput 0.837698
Reading from 31543: heap size 330 MB, throughput 0.998273
Reading from 31541: heap size 913 MB, throughput 0.899013
Reading from 31541: heap size 917 MB, throughput 0.714351
Reading from 31543: heap size 330 MB, throughput 0.998169
Reading from 31543: heap size 337 MB, throughput 0.998066
Reading from 31543: heap size 338 MB, throughput 0.994879
Reading from 31543: heap size 345 MB, throughput 0.998379
Reading from 31541: heap size 922 MB, throughput 0.0813547
Reading from 31541: heap size 1011 MB, throughput 0.420879
Reading from 31541: heap size 1039 MB, throughput 0.737105
Reading from 31543: heap size 345 MB, throughput 0.997818
Reading from 31541: heap size 1042 MB, throughput 0.719245
Reading from 31541: heap size 1046 MB, throughput 0.726153
Reading from 31541: heap size 1049 MB, throughput 0.687575
Reading from 31543: heap size 354 MB, throughput 0.998269
Reading from 31541: heap size 1052 MB, throughput 0.711252
Reading from 31541: heap size 1056 MB, throughput 0.676279
Reading from 31543: heap size 354 MB, throughput 0.997941
Reading from 31541: heap size 1063 MB, throughput 0.641262
Reading from 31541: heap size 1066 MB, throughput 0.642825
Reading from 31543: heap size 362 MB, throughput 0.997879
Reading from 31543: heap size 362 MB, throughput 0.99712
Reading from 31543: heap size 370 MB, throughput 0.997411
Reading from 31543: heap size 370 MB, throughput 0.996969
Reading from 31541: heap size 1082 MB, throughput 0.93894
Reading from 31543: heap size 379 MB, throughput 0.998675
Equal recommendation: 3657 MB each
Reading from 31543: heap size 379 MB, throughput 0.998239
Reading from 31543: heap size 388 MB, throughput 0.998358
Reading from 31541: heap size 1084 MB, throughput 0.942789
Reading from 31543: heap size 388 MB, throughput 0.997956
Reading from 31543: heap size 396 MB, throughput 0.997866
Reading from 31543: heap size 396 MB, throughput 0.998295
Reading from 31541: heap size 1101 MB, throughput 0.933957
Reading from 31543: heap size 404 MB, throughput 0.99849
Reading from 31543: heap size 404 MB, throughput 0.997904
Reading from 31543: heap size 411 MB, throughput 0.99824
Reading from 31543: heap size 411 MB, throughput 0.996787
Reading from 31541: heap size 1104 MB, throughput 0.930593
Reading from 31543: heap size 420 MB, throughput 0.99762
Reading from 31543: heap size 420 MB, throughput 0.997972
Reading from 31543: heap size 430 MB, throughput 0.995607
Reading from 31541: heap size 1118 MB, throughput 0.949418
Reading from 31543: heap size 430 MB, throughput 0.996935
Reading from 31543: heap size 441 MB, throughput 0.998078
Reading from 31543: heap size 441 MB, throughput 0.997795
Reading from 31543: heap size 452 MB, throughput 0.998768
Reading from 31541: heap size 1124 MB, throughput 0.624362
Equal recommendation: 3657 MB each
Reading from 31543: heap size 453 MB, throughput 0.997577
Reading from 31543: heap size 463 MB, throughput 0.99748
Reading from 31543: heap size 463 MB, throughput 0.998041
Reading from 31541: heap size 1232 MB, throughput 0.987308
Reading from 31543: heap size 473 MB, throughput 0.997868
Reading from 31543: heap size 473 MB, throughput 0.997818
Reading from 31543: heap size 484 MB, throughput 0.997828
Reading from 31541: heap size 1236 MB, throughput 0.982514
Reading from 31543: heap size 484 MB, throughput 0.997556
Reading from 31543: heap size 495 MB, throughput 0.99811
Reading from 31541: heap size 1252 MB, throughput 0.978043
Reading from 31543: heap size 495 MB, throughput 0.99703
Reading from 31543: heap size 506 MB, throughput 0.997425
Reading from 31543: heap size 506 MB, throughput 0.997838
Reading from 31541: heap size 1255 MB, throughput 0.974756
Reading from 31543: heap size 518 MB, throughput 0.997678
Reading from 31543: heap size 519 MB, throughput 0.998176
Equal recommendation: 3657 MB each
Reading from 31541: heap size 1257 MB, throughput 0.972087
Reading from 31543: heap size 529 MB, throughput 0.997996
Reading from 31543: heap size 530 MB, throughput 0.997442
Reading from 31543: heap size 542 MB, throughput 0.998113
Reading from 31541: heap size 1261 MB, throughput 0.967583
Reading from 31543: heap size 542 MB, throughput 0.997864
Reading from 31543: heap size 554 MB, throughput 0.997594
Reading from 31543: heap size 554 MB, throughput 0.998009
Reading from 31541: heap size 1253 MB, throughput 0.962782
Reading from 31543: heap size 565 MB, throughput 0.998144
Reading from 31543: heap size 566 MB, throughput 0.997534
Reading from 31543: heap size 577 MB, throughput 0.998096
Reading from 31541: heap size 1258 MB, throughput 0.960858
Reading from 31543: heap size 577 MB, throughput 0.998154
Reading from 31543: heap size 589 MB, throughput 0.997969
Reading from 31541: heap size 1252 MB, throughput 0.954677
Equal recommendation: 3657 MB each
Reading from 31543: heap size 589 MB, throughput 0.998196
Reading from 31543: heap size 601 MB, throughput 0.997575
Reading from 31543: heap size 601 MB, throughput 0.998311
Reading from 31541: heap size 1255 MB, throughput 0.956157
Reading from 31543: heap size 613 MB, throughput 0.997678
Reading from 31543: heap size 614 MB, throughput 0.998262
Reading from 31543: heap size 626 MB, throughput 0.979939
Reading from 31541: heap size 1261 MB, throughput 0.966538
Reading from 31543: heap size 626 MB, throughput 0.998542
Reading from 31543: heap size 655 MB, throughput 0.997936
Reading from 31541: heap size 1263 MB, throughput 0.952289
Reading from 31543: heap size 660 MB, throughput 0.998303
Reading from 31543: heap size 682 MB, throughput 0.998667
Reading from 31541: heap size 1268 MB, throughput 0.949515
Reading from 31543: heap size 682 MB, throughput 0.998225
Equal recommendation: 3657 MB each
Reading from 31543: heap size 701 MB, throughput 0.997937
Reading from 31543: heap size 703 MB, throughput 0.998224
Reading from 31541: heap size 1275 MB, throughput 0.954509
Reading from 31543: heap size 720 MB, throughput 0.998166
Reading from 31543: heap size 720 MB, throughput 0.998546
Reading from 31541: heap size 1281 MB, throughput 0.945685
Reading from 31543: heap size 735 MB, throughput 0.998392
Reading from 31543: heap size 736 MB, throughput 0.998219
Reading from 31541: heap size 1291 MB, throughput 0.935509
Reading from 31543: heap size 750 MB, throughput 0.998931
Reading from 31543: heap size 750 MB, throughput 0.998495
Reading from 31541: heap size 1301 MB, throughput 0.932041
Reading from 31543: heap size 763 MB, throughput 0.998137
Equal recommendation: 3657 MB each
Reading from 31543: heap size 763 MB, throughput 0.998612
Reading from 31543: heap size 776 MB, throughput 0.997976
Reading from 31541: heap size 1309 MB, throughput 0.94827
Reading from 31543: heap size 776 MB, throughput 0.998643
Reading from 31541: heap size 1320 MB, throughput 0.943471
Reading from 31543: heap size 789 MB, throughput 0.998731
Reading from 31543: heap size 790 MB, throughput 0.998279
Reading from 31543: heap size 803 MB, throughput 0.998844
Reading from 31541: heap size 1325 MB, throughput 0.939621
Reading from 31543: heap size 803 MB, throughput 0.998787
Reading from 31543: heap size 814 MB, throughput 0.998388
Reading from 31541: heap size 1335 MB, throughput 0.938973
Equal recommendation: 3657 MB each
Reading from 31543: heap size 815 MB, throughput 0.998868
Reading from 31543: heap size 826 MB, throughput 0.998543
Reading from 31541: heap size 1338 MB, throughput 0.942721
Reading from 31543: heap size 827 MB, throughput 0.998825
Reading from 31543: heap size 838 MB, throughput 0.998718
Reading from 31541: heap size 1349 MB, throughput 0.944593
Reading from 31543: heap size 839 MB, throughput 0.998672
Reading from 31543: heap size 850 MB, throughput 0.999044
Reading from 31541: heap size 1351 MB, throughput 0.943849
Reading from 31543: heap size 851 MB, throughput 0.998662
Equal recommendation: 3657 MB each
Reading from 31543: heap size 862 MB, throughput 0.998963
Reading from 31543: heap size 862 MB, throughput 0.998283
Reading from 31541: heap size 1362 MB, throughput 0.94544
Reading from 31543: heap size 873 MB, throughput 0.998835
Reading from 31543: heap size 873 MB, throughput 0.998654
Reading from 31541: heap size 1362 MB, throughput 0.938027
Reading from 31543: heap size 885 MB, throughput 0.998782
Reading from 31543: heap size 885 MB, throughput 0.998833
Reading from 31541: heap size 1372 MB, throughput 0.949744
Reading from 31543: heap size 897 MB, throughput 0.998803
Reading from 31543: heap size 897 MB, throughput 0.998741
Equal recommendation: 3657 MB each
Reading from 31543: heap size 909 MB, throughput 0.997699
Reading from 31541: heap size 1372 MB, throughput 0.523082
Reading from 31543: heap size 909 MB, throughput 0.999232
Reading from 31543: heap size 922 MB, throughput 0.998631
Reading from 31543: heap size 924 MB, throughput 0.998896
Reading from 31541: heap size 1494 MB, throughput 0.930413
Reading from 31543: heap size 935 MB, throughput 0.999099
Reading from 31543: heap size 935 MB, throughput 0.998553
Reading from 31541: heap size 1495 MB, throughput 0.971404
Equal recommendation: 3657 MB each
Reading from 31543: heap size 947 MB, throughput 0.99896
Reading from 31543: heap size 947 MB, throughput 0.998821
Reading from 31541: heap size 1512 MB, throughput 0.969015
Reading from 31543: heap size 960 MB, throughput 0.998838
Reading from 31543: heap size 960 MB, throughput 0.998622
Reading from 31541: heap size 1515 MB, throughput 0.967835
Reading from 31543: heap size 972 MB, throughput 0.999103
Reading from 31543: heap size 972 MB, throughput 0.998848
Reading from 31543: heap size 984 MB, throughput 0.998922
Equal recommendation: 3657 MB each
Reading from 31543: heap size 984 MB, throughput 0.998682
Reading from 31543: heap size 996 MB, throughput 0.999036
Reading from 31543: heap size 996 MB, throughput 0.998665
Reading from 31543: heap size 1008 MB, throughput 0.999017
Reading from 31543: heap size 1008 MB, throughput 0.999002
Reading from 31543: heap size 1020 MB, throughput 0.998924
Reading from 31543: heap size 1021 MB, throughput 0.998546
Equal recommendation: 3657 MB each
Reading from 31543: heap size 1032 MB, throughput 0.9991
Reading from 31541: heap size 1518 MB, throughput 0.988206
Reading from 31543: heap size 1032 MB, throughput 0.998895
Reading from 31543: heap size 1045 MB, throughput 0.998996
Reading from 31543: heap size 1045 MB, throughput 0.999025
Reading from 31543: heap size 1057 MB, throughput 0.999084
Reading from 31543: heap size 1057 MB, throughput 0.9987
Equal recommendation: 3657 MB each
Reading from 31543: heap size 1069 MB, throughput 0.99931
Reading from 31543: heap size 1069 MB, throughput 0.998714
Reading from 31543: heap size 1080 MB, throughput 0.999196
Reading from 31543: heap size 1080 MB, throughput 0.998921
Reading from 31541: heap size 1522 MB, throughput 0.988465
Reading from 31543: heap size 1092 MB, throughput 0.998923
Reading from 31543: heap size 1092 MB, throughput 0.998996
Equal recommendation: 3657 MB each
Reading from 31543: heap size 1105 MB, throughput 0.999082
Reading from 31543: heap size 1105 MB, throughput 0.998983
Reading from 31541: heap size 1510 MB, throughput 0.974689
Reading from 31541: heap size 1538 MB, throughput 0.739824
Reading from 31541: heap size 1529 MB, throughput 0.635983
Reading from 31541: heap size 1567 MB, throughput 0.6746
Reading from 31543: heap size 1117 MB, throughput 0.998947
Reading from 31541: heap size 1600 MB, throughput 0.639964
Reading from 31541: heap size 1622 MB, throughput 0.665115
Reading from 31541: heap size 1659 MB, throughput 0.739134
Reading from 31543: heap size 1117 MB, throughput 0.998994
Reading from 31543: heap size 1129 MB, throughput 0.998966
Reading from 31541: heap size 1672 MB, throughput 0.95067
Reading from 31543: heap size 1129 MB, throughput 0.999167
Equal recommendation: 3657 MB each
Reading from 31541: heap size 1649 MB, throughput 0.956053
Reading from 31543: heap size 1141 MB, throughput 0.99901
Reading from 31543: heap size 1142 MB, throughput 0.999176
Reading from 31541: heap size 1669 MB, throughput 0.955386
Reading from 31543: heap size 1154 MB, throughput 0.999247
Reading from 31543: heap size 1154 MB, throughput 0.999096
Reading from 31541: heap size 1650 MB, throughput 0.947442
Reading from 31543: heap size 1165 MB, throughput 0.999159
Equal recommendation: 3657 MB each
Reading from 31543: heap size 1165 MB, throughput 0.998794
Reading from 31541: heap size 1668 MB, throughput 0.962769
Reading from 31543: heap size 1176 MB, throughput 0.998986
Reading from 31543: heap size 1177 MB, throughput 0.999174
Reading from 31541: heap size 1653 MB, throughput 0.952182
Reading from 31543: heap size 1188 MB, throughput 0.99915
Reading from 31543: heap size 1189 MB, throughput 0.998904
Reading from 31541: heap size 1668 MB, throughput 0.956685
Reading from 31543: heap size 1200 MB, throughput 0.999079
Equal recommendation: 3657 MB each
Reading from 31543: heap size 1200 MB, throughput 0.999061
Reading from 31541: heap size 1666 MB, throughput 0.730821
Reading from 31543: heap size 1213 MB, throughput 0.999171
Reading from 31543: heap size 1213 MB, throughput 0.999174
Reading from 31541: heap size 1658 MB, throughput 0.988604
Reading from 31543: heap size 1225 MB, throughput 0.999153
Reading from 31543: heap size 1225 MB, throughput 0.999162
Equal recommendation: 3657 MB each
Reading from 31541: heap size 1665 MB, throughput 0.984809
Reading from 31543: heap size 1237 MB, throughput 0.999061
Reading from 31543: heap size 1237 MB, throughput 0.99906
Reading from 31541: heap size 1679 MB, throughput 0.979166
Reading from 31543: heap size 1249 MB, throughput 0.999154
Reading from 31543: heap size 1249 MB, throughput 0.999047
Reading from 31541: heap size 1701 MB, throughput 0.97972
Reading from 31543: heap size 1261 MB, throughput 0.999271
Equal recommendation: 3657 MB each
Reading from 31543: heap size 1261 MB, throughput 0.999129
Reading from 31541: heap size 1703 MB, throughput 0.974863
Reading from 31543: heap size 1273 MB, throughput 0.999252
Reading from 31543: heap size 1273 MB, throughput 0.999184
Reading from 31543: heap size 1285 MB, throughput 0.999186
Reading from 31541: heap size 1695 MB, throughput 0.976599
Reading from 31543: heap size 1285 MB, throughput 0.999232
Equal recommendation: 3657 MB each
Reading from 31543: heap size 1297 MB, throughput 0.999288
Reading from 31541: heap size 1704 MB, throughput 0.970642
Reading from 31543: heap size 1297 MB, throughput 0.999073
Reading from 31543: heap size 1308 MB, throughput 0.999187
Reading from 31541: heap size 1682 MB, throughput 0.964
Reading from 31543: heap size 1244 MB, throughput 0.992413
Reading from 31543: heap size 1184 MB, throughput 0.998615
Equal recommendation: 3657 MB each
Reading from 31541: heap size 1695 MB, throughput 0.967644
Reading from 31543: heap size 1202 MB, throughput 0.999527
Reading from 31543: heap size 1218 MB, throughput 0.999377
Reading from 31543: heap size 1233 MB, throughput 0.999318
Reading from 31541: heap size 1680 MB, throughput 0.962669
Reading from 31543: heap size 1250 MB, throughput 0.999297
Reading from 31543: heap size 1264 MB, throughput 0.999308
Reading from 31541: heap size 1688 MB, throughput 0.96105
Reading from 31543: heap size 1278 MB, throughput 0.999346
Equal recommendation: 3657 MB each
Reading from 31543: heap size 1292 MB, throughput 0.999381
Reading from 31541: heap size 1699 MB, throughput 0.951642
Reading from 31543: heap size 1298 MB, throughput 0.999306
Reading from 31543: heap size 1298 MB, throughput 0.999202
Reading from 31543: heap size 1312 MB, throughput 0.998882
Reading from 31541: heap size 1702 MB, throughput 0.953692
Reading from 31543: heap size 1312 MB, throughput 0.999141
Equal recommendation: 3657 MB each
Reading from 31543: heap size 1329 MB, throughput 0.999064
Reading from 31541: heap size 1717 MB, throughput 0.952921
Reading from 31543: heap size 1329 MB, throughput 0.998885
Reading from 31543: heap size 1347 MB, throughput 0.998949
Reading from 31541: heap size 1726 MB, throughput 0.954136
Reading from 31543: heap size 1347 MB, throughput 0.999104
Equal recommendation: 3657 MB each
Reading from 31543: heap size 1367 MB, throughput 0.999137
Reading from 31541: heap size 1744 MB, throughput 0.967104
Reading from 31543: heap size 1367 MB, throughput 0.999105
Reading from 31543: heap size 1387 MB, throughput 0.999217
Reading from 31543: heap size 1387 MB, throughput 0.999117
Reading from 31543: heap size 1407 MB, throughput 0.999355
Equal recommendation: 3657 MB each
Reading from 31543: heap size 1407 MB, throughput 0.999102
Reading from 31543: heap size 1425 MB, throughput 0.999139
Reading from 31543: heap size 1425 MB, throughput 0.998998
Reading from 31543: heap size 1445 MB, throughput 0.999324
Reading from 31541: heap size 1759 MB, throughput 0.983001
Reading from 31543: heap size 1445 MB, throughput 0.99914
Equal recommendation: 3657 MB each
Reading from 31543: heap size 1464 MB, throughput 0.999155
Reading from 31543: heap size 1464 MB, throughput 0.999253
Reading from 31543: heap size 1483 MB, throughput 0.999242
Reading from 31543: heap size 1483 MB, throughput 0.999279
Equal recommendation: 3657 MB each
Reading from 31543: heap size 1502 MB, throughput 0.999261
Reading from 31543: heap size 1502 MB, throughput 0.999171
Reading from 31543: heap size 1521 MB, throughput 0.999235
Reading from 31543: heap size 1521 MB, throughput 0.999088
Client 31543 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 31541: heap size 1768 MB, throughput 0.995099
Reading from 31541: heap size 1790 MB, throughput 0.88651
Reading from 31541: heap size 1781 MB, throughput 0.812953
Reading from 31541: heap size 1806 MB, throughput 0.804662
Reading from 31541: heap size 1846 MB, throughput 0.83149
Client 31541 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
