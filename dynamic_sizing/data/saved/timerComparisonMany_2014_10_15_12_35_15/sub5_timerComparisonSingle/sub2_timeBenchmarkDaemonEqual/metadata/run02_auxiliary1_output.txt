economemd
    total memory: 8922 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub5_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 4772: heap size 9 MB, throughput 0.99021
Clients: 1
Client 4772 has a minimum heap size of 276 MB
Reading from 4771: heap size 9 MB, throughput 0.990226
Clients: 2
Client 4771 has a minimum heap size of 1211 MB
Reading from 4771: heap size 9 MB, throughput 0.978416
Reading from 4772: heap size 9 MB, throughput 0.963094
Reading from 4771: heap size 9 MB, throughput 0.96167
Reading from 4772: heap size 9 MB, throughput 0.957008
Reading from 4771: heap size 9 MB, throughput 0.952455
Reading from 4772: heap size 9 MB, throughput 0.94967
Reading from 4771: heap size 11 MB, throughput 0.980086
Reading from 4772: heap size 11 MB, throughput 0.983138
Reading from 4772: heap size 11 MB, throughput 0.983532
Reading from 4771: heap size 11 MB, throughput 0.976912
Reading from 4772: heap size 17 MB, throughput 0.812225
Reading from 4771: heap size 17 MB, throughput 0.762414
Reading from 4771: heap size 17 MB, throughput 0.576041
Reading from 4772: heap size 17 MB, throughput 0.671294
Reading from 4772: heap size 30 MB, throughput 0.776843
Reading from 4771: heap size 29 MB, throughput 0.968271
Reading from 4772: heap size 31 MB, throughput 0.9307
Reading from 4771: heap size 30 MB, throughput 0.856083
Reading from 4772: heap size 36 MB, throughput 0.478111
Reading from 4771: heap size 34 MB, throughput 0.45045
Reading from 4772: heap size 48 MB, throughput 0.881173
Reading from 4771: heap size 47 MB, throughput 0.78809
Reading from 4772: heap size 51 MB, throughput 0.403787
Reading from 4771: heap size 51 MB, throughput 0.421338
Reading from 4772: heap size 66 MB, throughput 0.848611
Reading from 4771: heap size 64 MB, throughput 0.731282
Reading from 4772: heap size 72 MB, throughput 0.582328
Reading from 4771: heap size 69 MB, throughput 0.809214
Reading from 4772: heap size 74 MB, throughput 0.230015
Reading from 4771: heap size 71 MB, throughput 0.218398
Reading from 4772: heap size 105 MB, throughput 0.847291
Reading from 4771: heap size 102 MB, throughput 0.774359
Reading from 4772: heap size 106 MB, throughput 0.245973
Reading from 4771: heap size 102 MB, throughput 0.363267
Reading from 4772: heap size 139 MB, throughput 0.852265
Reading from 4771: heap size 133 MB, throughput 0.804376
Reading from 4772: heap size 139 MB, throughput 0.709035
Reading from 4771: heap size 133 MB, throughput 0.876066
Reading from 4772: heap size 140 MB, throughput 0.852109
Reading from 4771: heap size 135 MB, throughput 0.819611
Reading from 4772: heap size 145 MB, throughput 0.693197
Reading from 4771: heap size 139 MB, throughput 0.743816
Reading from 4772: heap size 148 MB, throughput 0.16271
Reading from 4771: heap size 141 MB, throughput 0.141297
Reading from 4772: heap size 189 MB, throughput 0.76643
Reading from 4771: heap size 181 MB, throughput 0.621358
Reading from 4772: heap size 194 MB, throughput 0.826361
Reading from 4771: heap size 184 MB, throughput 0.733918
Reading from 4772: heap size 197 MB, throughput 0.735764
Reading from 4771: heap size 187 MB, throughput 0.235165
Reading from 4771: heap size 232 MB, throughput 0.651812
Reading from 4771: heap size 234 MB, throughput 0.742974
Reading from 4771: heap size 235 MB, throughput 0.702787
Reading from 4771: heap size 238 MB, throughput 0.630109
Reading from 4772: heap size 207 MB, throughput 0.545358
Reading from 4771: heap size 241 MB, throughput 0.647159
Reading from 4771: heap size 250 MB, throughput 0.601751
Reading from 4771: heap size 254 MB, throughput 0.656013
Reading from 4772: heap size 246 MB, throughput 0.900175
Reading from 4772: heap size 253 MB, throughput 0.703433
Reading from 4771: heap size 262 MB, throughput 0.62583
Reading from 4771: heap size 266 MB, throughput 0.541178
Reading from 4772: heap size 256 MB, throughput 0.849979
Reading from 4771: heap size 277 MB, throughput 0.566447
Reading from 4772: heap size 256 MB, throughput 0.81658
Reading from 4772: heap size 258 MB, throughput 0.887485
Reading from 4772: heap size 260 MB, throughput 0.819682
Reading from 4771: heap size 284 MB, throughput 0.104196
Reading from 4772: heap size 262 MB, throughput 0.79789
Reading from 4771: heap size 330 MB, throughput 0.50725
Reading from 4772: heap size 268 MB, throughput 0.844474
Reading from 4772: heap size 271 MB, throughput 0.771702
Reading from 4771: heap size 336 MB, throughput 0.130925
Reading from 4771: heap size 387 MB, throughput 0.514006
Reading from 4772: heap size 277 MB, throughput 0.954979
Reading from 4771: heap size 375 MB, throughput 0.69778
Reading from 4772: heap size 279 MB, throughput 0.831214
Reading from 4771: heap size 382 MB, throughput 0.618136
Reading from 4772: heap size 285 MB, throughput 0.794808
Reading from 4771: heap size 383 MB, throughput 0.527603
Reading from 4772: heap size 288 MB, throughput 0.764063
Reading from 4771: heap size 383 MB, throughput 0.549449
Reading from 4772: heap size 295 MB, throughput 0.887952
Reading from 4771: heap size 386 MB, throughput 0.598309
Reading from 4771: heap size 392 MB, throughput 0.452468
Reading from 4771: heap size 395 MB, throughput 0.414506
Reading from 4771: heap size 405 MB, throughput 0.415552
Reading from 4772: heap size 297 MB, throughput 0.217545
Reading from 4772: heap size 339 MB, throughput 0.663281
Reading from 4772: heap size 341 MB, throughput 0.783757
Reading from 4772: heap size 344 MB, throughput 0.686246
Reading from 4772: heap size 345 MB, throughput 0.66531
Reading from 4772: heap size 352 MB, throughput 0.591213
Reading from 4771: heap size 413 MB, throughput 0.0826434
Reading from 4771: heap size 467 MB, throughput 0.349119
Reading from 4771: heap size 468 MB, throughput 0.503515
Equal recommendation: 4461 MB each
Reading from 4771: heap size 471 MB, throughput 0.487569
Reading from 4772: heap size 352 MB, throughput 0.953293
Reading from 4771: heap size 466 MB, throughput 0.087023
Reading from 4771: heap size 529 MB, throughput 0.440597
Reading from 4771: heap size 521 MB, throughput 0.691208
Reading from 4771: heap size 526 MB, throughput 0.70615
Reading from 4771: heap size 527 MB, throughput 0.568195
Reading from 4771: heap size 527 MB, throughput 0.56701
Reading from 4771: heap size 529 MB, throughput 0.532519
Reading from 4772: heap size 361 MB, throughput 0.977988
Reading from 4771: heap size 537 MB, throughput 0.537979
Reading from 4771: heap size 544 MB, throughput 0.467684
Reading from 4771: heap size 557 MB, throughput 0.526696
Reading from 4771: heap size 564 MB, throughput 0.0837262
Reading from 4772: heap size 362 MB, throughput 0.982949
Reading from 4771: heap size 633 MB, throughput 0.443409
Reading from 4771: heap size 645 MB, throughput 0.597281
Reading from 4771: heap size 646 MB, throughput 0.573519
Reading from 4771: heap size 648 MB, throughput 0.61583
Reading from 4772: heap size 365 MB, throughput 0.973095
Reading from 4771: heap size 649 MB, throughput 0.268467
Reading from 4772: heap size 367 MB, throughput 0.97588
Reading from 4771: heap size 716 MB, throughput 0.804951
Reading from 4771: heap size 718 MB, throughput 0.885198
Reading from 4771: heap size 715 MB, throughput 0.597292
Reading from 4771: heap size 728 MB, throughput 0.463314
Reading from 4772: heap size 368 MB, throughput 0.96627
Reading from 4771: heap size 738 MB, throughput 0.627764
Reading from 4771: heap size 740 MB, throughput 0.340115
Equal recommendation: 4461 MB each
Reading from 4772: heap size 370 MB, throughput 0.966737
Reading from 4771: heap size 732 MB, throughput 0.0291394
Reading from 4771: heap size 807 MB, throughput 0.267281
Reading from 4771: heap size 802 MB, throughput 0.475929
Reading from 4771: heap size 805 MB, throughput 0.865105
Reading from 4771: heap size 799 MB, throughput 0.550231
Reading from 4771: heap size 804 MB, throughput 0.53502
Reading from 4772: heap size 371 MB, throughput 0.967162
Reading from 4771: heap size 805 MB, throughput 0.0686362
Reading from 4771: heap size 879 MB, throughput 0.71701
Reading from 4771: heap size 884 MB, throughput 0.793662
Reading from 4771: heap size 886 MB, throughput 0.840045
Reading from 4772: heap size 372 MB, throughput 0.975478
Reading from 4771: heap size 892 MB, throughput 0.906043
Reading from 4771: heap size 894 MB, throughput 0.875836
Reading from 4771: heap size 902 MB, throughput 0.910925
Reading from 4771: heap size 771 MB, throughput 0.836821
Reading from 4771: heap size 883 MB, throughput 0.844961
Reading from 4771: heap size 783 MB, throughput 0.779355
Reading from 4772: heap size 376 MB, throughput 0.974914
Reading from 4771: heap size 875 MB, throughput 0.828621
Reading from 4771: heap size 789 MB, throughput 0.810769
Reading from 4772: heap size 377 MB, throughput 0.973635
Reading from 4771: heap size 869 MB, throughput 0.152629
Reading from 4771: heap size 959 MB, throughput 0.65805
Reading from 4771: heap size 962 MB, throughput 0.820546
Reading from 4771: heap size 963 MB, throughput 0.812344
Reading from 4771: heap size 965 MB, throughput 0.839385
Reading from 4772: heap size 380 MB, throughput 0.971649
Reading from 4771: heap size 967 MB, throughput 0.96142
Reading from 4772: heap size 383 MB, throughput 0.971604
Reading from 4771: heap size 965 MB, throughput 0.911069
Reading from 4771: heap size 969 MB, throughput 0.758557
Equal recommendation: 4461 MB each
Reading from 4771: heap size 978 MB, throughput 0.755606
Reading from 4771: heap size 984 MB, throughput 0.742126
Reading from 4771: heap size 981 MB, throughput 0.80941
Reading from 4771: heap size 989 MB, throughput 0.811274
Reading from 4771: heap size 979 MB, throughput 0.803206
Reading from 4771: heap size 985 MB, throughput 0.767372
Reading from 4772: heap size 384 MB, throughput 0.980105
Reading from 4772: heap size 388 MB, throughput 0.733556
Reading from 4771: heap size 979 MB, throughput 0.84871
Reading from 4772: heap size 387 MB, throughput 0.743327
Reading from 4772: heap size 391 MB, throughput 0.799128
Reading from 4772: heap size 402 MB, throughput 0.733633
Reading from 4771: heap size 984 MB, throughput 0.868347
Reading from 4771: heap size 982 MB, throughput 0.855979
Reading from 4771: heap size 988 MB, throughput 0.775245
Reading from 4771: heap size 980 MB, throughput 0.606656
Reading from 4771: heap size 998 MB, throughput 0.604267
Reading from 4772: heap size 403 MB, throughput 0.982926
Reading from 4771: heap size 1012 MB, throughput 0.0871473
Reading from 4771: heap size 1126 MB, throughput 0.455191
Reading from 4771: heap size 1135 MB, throughput 0.774396
Reading from 4771: heap size 1135 MB, throughput 0.769653
Reading from 4771: heap size 1144 MB, throughput 0.67558
Reading from 4771: heap size 1147 MB, throughput 0.684512
Reading from 4771: heap size 1153 MB, throughput 0.762636
Reading from 4772: heap size 408 MB, throughput 0.978851
Reading from 4771: heap size 1157 MB, throughput 0.869302
Reading from 4772: heap size 410 MB, throughput 0.981403
Reading from 4771: heap size 1158 MB, throughput 0.970308
Reading from 4772: heap size 411 MB, throughput 0.987135
Equal recommendation: 4461 MB each
Reading from 4771: heap size 1165 MB, throughput 0.955652
Reading from 4772: heap size 414 MB, throughput 0.982873
Reading from 4771: heap size 1165 MB, throughput 0.959115
Reading from 4772: heap size 412 MB, throughput 0.983534
Reading from 4771: heap size 1170 MB, throughput 0.957752
Reading from 4772: heap size 415 MB, throughput 0.983396
Reading from 4771: heap size 1181 MB, throughput 0.954201
Reading from 4772: heap size 413 MB, throughput 0.985646
Reading from 4771: heap size 1183 MB, throughput 0.957227
Reading from 4772: heap size 415 MB, throughput 0.980774
Equal recommendation: 4461 MB each
Reading from 4771: heap size 1184 MB, throughput 0.961251
Reading from 4772: heap size 415 MB, throughput 0.981183
Reading from 4771: heap size 1188 MB, throughput 0.955164
Reading from 4772: heap size 416 MB, throughput 0.986278
Reading from 4772: heap size 420 MB, throughput 0.88999
Reading from 4772: heap size 421 MB, throughput 0.857186
Reading from 4772: heap size 425 MB, throughput 0.894203
Reading from 4771: heap size 1185 MB, throughput 0.955053
Reading from 4772: heap size 426 MB, throughput 0.989661
Reading from 4771: heap size 1189 MB, throughput 0.954868
Reading from 4772: heap size 431 MB, throughput 0.990981
Reading from 4771: heap size 1196 MB, throughput 0.958326
Equal recommendation: 4461 MB each
Reading from 4772: heap size 432 MB, throughput 0.985233
Reading from 4771: heap size 1197 MB, throughput 0.951728
Reading from 4772: heap size 433 MB, throughput 0.986681
Reading from 4771: heap size 1203 MB, throughput 0.957851
Reading from 4772: heap size 435 MB, throughput 0.987431
Reading from 4771: heap size 1206 MB, throughput 0.952264
Reading from 4772: heap size 432 MB, throughput 0.986719
Reading from 4771: heap size 1213 MB, throughput 0.947668
Reading from 4772: heap size 435 MB, throughput 0.985925
Reading from 4771: heap size 1217 MB, throughput 0.950874
Equal recommendation: 4461 MB each
Reading from 4772: heap size 436 MB, throughput 0.981007
Reading from 4771: heap size 1226 MB, throughput 0.952051
Reading from 4772: heap size 436 MB, throughput 0.992113
Reading from 4772: heap size 438 MB, throughput 0.922127
Reading from 4772: heap size 439 MB, throughput 0.790624
Reading from 4772: heap size 443 MB, throughput 0.926517
Reading from 4771: heap size 1231 MB, throughput 0.545987
Reading from 4772: heap size 445 MB, throughput 0.991332
Reading from 4771: heap size 1301 MB, throughput 0.993011
Reading from 4772: heap size 450 MB, throughput 0.991255
Equal recommendation: 4461 MB each
Reading from 4771: heap size 1304 MB, throughput 0.986144
Reading from 4772: heap size 452 MB, throughput 0.98924
Reading from 4771: heap size 1318 MB, throughput 0.987141
Reading from 4772: heap size 451 MB, throughput 0.988758
Reading from 4771: heap size 1322 MB, throughput 0.980046
Reading from 4772: heap size 453 MB, throughput 0.987158
Reading from 4771: heap size 1324 MB, throughput 0.973129
Reading from 4772: heap size 453 MB, throughput 0.987604
Equal recommendation: 4461 MB each
Reading from 4771: heap size 1327 MB, throughput 0.976739
Reading from 4772: heap size 454 MB, throughput 0.984296
Reading from 4771: heap size 1316 MB, throughput 0.976817
Reading from 4771: heap size 1237 MB, throughput 0.969232
Reading from 4772: heap size 457 MB, throughput 0.992378
Reading from 4772: heap size 458 MB, throughput 0.923375
Reading from 4772: heap size 457 MB, throughput 0.893377
Reading from 4772: heap size 460 MB, throughput 0.982804
Reading from 4771: heap size 1310 MB, throughput 0.967607
Reading from 4772: heap size 467 MB, throughput 0.991186
Equal recommendation: 4461 MB each
Reading from 4771: heap size 1315 MB, throughput 0.966826
Reading from 4772: heap size 467 MB, throughput 0.991596
Reading from 4771: heap size 1319 MB, throughput 0.962479
Reading from 4772: heap size 468 MB, throughput 0.989666
Reading from 4771: heap size 1320 MB, throughput 0.962183
Reading from 4772: heap size 469 MB, throughput 0.991265
Reading from 4771: heap size 1325 MB, throughput 0.954619
Equal recommendation: 4461 MB each
Reading from 4771: heap size 1331 MB, throughput 0.965823
Reading from 4772: heap size 468 MB, throughput 0.988698
Reading from 4771: heap size 1338 MB, throughput 0.956639
Reading from 4772: heap size 469 MB, throughput 0.809444
Reading from 4771: heap size 1347 MB, throughput 0.95465
Reading from 4772: heap size 508 MB, throughput 0.985304
Reading from 4772: heap size 509 MB, throughput 0.940463
Reading from 4772: heap size 511 MB, throughput 0.908335
Reading from 4771: heap size 1358 MB, throughput 0.954008
Reading from 4772: heap size 512 MB, throughput 0.990006
Equal recommendation: 4461 MB each
Reading from 4771: heap size 1365 MB, throughput 0.949378
Reading from 4772: heap size 520 MB, throughput 0.991628
Reading from 4772: heap size 520 MB, throughput 0.973661
Reading from 4771: heap size 1377 MB, throughput 0.944639
Reading from 4772: heap size 523 MB, throughput 0.990416
Reading from 4771: heap size 1381 MB, throughput 0.959658
Equal recommendation: 4461 MB each
Reading from 4772: heap size 524 MB, throughput 0.989096
Reading from 4771: heap size 1393 MB, throughput 0.957473
Reading from 4772: heap size 524 MB, throughput 0.989105
Reading from 4771: heap size 1395 MB, throughput 0.957952
Reading from 4772: heap size 525 MB, throughput 0.986853
Reading from 4772: heap size 528 MB, throughput 0.984855
Reading from 4772: heap size 530 MB, throughput 0.899563
Reading from 4772: heap size 534 MB, throughput 0.9825
Equal recommendation: 4461 MB each
Reading from 4772: heap size 536 MB, throughput 0.992494
Reading from 4772: heap size 539 MB, throughput 0.992035
Reading from 4772: heap size 541 MB, throughput 0.989627
Equal recommendation: 4461 MB each
Reading from 4772: heap size 539 MB, throughput 0.990031
Reading from 4772: heap size 541 MB, throughput 0.98868
Reading from 4772: heap size 543 MB, throughput 0.993166
Reading from 4772: heap size 544 MB, throughput 0.971684
Reading from 4772: heap size 543 MB, throughput 0.90182
Equal recommendation: 4461 MB each
Reading from 4772: heap size 546 MB, throughput 0.989224
Reading from 4772: heap size 553 MB, throughput 0.992981
Reading from 4771: heap size 1406 MB, throughput 0.995181
Reading from 4772: heap size 554 MB, throughput 0.991501
Equal recommendation: 4461 MB each
Reading from 4772: heap size 555 MB, throughput 0.991371
Reading from 4771: heap size 1407 MB, throughput 0.891448
Reading from 4771: heap size 1442 MB, throughput 0.988907
Reading from 4772: heap size 557 MB, throughput 0.990531
Reading from 4771: heap size 1475 MB, throughput 0.933018
Reading from 4771: heap size 1479 MB, throughput 0.891505
Reading from 4771: heap size 1485 MB, throughput 0.896235
Reading from 4771: heap size 1480 MB, throughput 0.902081
Reading from 4771: heap size 1486 MB, throughput 0.903968
Reading from 4772: heap size 560 MB, throughput 0.983075
Reading from 4771: heap size 1483 MB, throughput 0.987814
Reading from 4772: heap size 560 MB, throughput 0.988947
Reading from 4772: heap size 563 MB, throughput 0.915289
Equal recommendation: 4461 MB each
Reading from 4771: heap size 1490 MB, throughput 0.991794
Reading from 4772: heap size 565 MB, throughput 0.990248
Reading from 4771: heap size 1510 MB, throughput 0.986892
Reading from 4772: heap size 572 MB, throughput 0.993448
Reading from 4771: heap size 1512 MB, throughput 0.981551
Reading from 4772: heap size 573 MB, throughput 0.992815
Reading from 4771: heap size 1507 MB, throughput 0.979959
Equal recommendation: 4461 MB each
Reading from 4772: heap size 574 MB, throughput 0.991731
Reading from 4771: heap size 1515 MB, throughput 0.977835
Reading from 4772: heap size 575 MB, throughput 0.990059
Reading from 4771: heap size 1494 MB, throughput 0.976856
Reading from 4772: heap size 576 MB, throughput 0.993401
Equal recommendation: 4461 MB each
Reading from 4771: heap size 1385 MB, throughput 0.975577
Reading from 4772: heap size 577 MB, throughput 0.970952
Reading from 4772: heap size 577 MB, throughput 0.939416
Reading from 4771: heap size 1490 MB, throughput 0.97417
Reading from 4772: heap size 579 MB, throughput 0.9952
Reading from 4771: heap size 1497 MB, throughput 0.971203
Reading from 4772: heap size 583 MB, throughput 0.993722
Equal recommendation: 4461 MB each
Reading from 4771: heap size 1504 MB, throughput 0.969299
Reading from 4772: heap size 584 MB, throughput 0.991605
Reading from 4771: heap size 1505 MB, throughput 0.969721
Reading from 4772: heap size 584 MB, throughput 0.99131
Reading from 4771: heap size 1514 MB, throughput 0.971297
Reading from 4772: heap size 586 MB, throughput 0.989844
Equal recommendation: 4461 MB each
Reading from 4772: heap size 588 MB, throughput 0.990859
Reading from 4772: heap size 589 MB, throughput 0.900729
Reading from 4771: heap size 1521 MB, throughput 0.966379
Reading from 4772: heap size 593 MB, throughput 0.991067
Reading from 4771: heap size 1531 MB, throughput 0.962513
Reading from 4772: heap size 595 MB, throughput 0.992849
Reading from 4771: heap size 1542 MB, throughput 0.954376
Equal recommendation: 4461 MB each
Reading from 4772: heap size 598 MB, throughput 0.992799
Reading from 4771: heap size 1555 MB, throughput 0.959985
Reading from 4772: heap size 600 MB, throughput 0.992815
Reading from 4771: heap size 1567 MB, throughput 0.96041
Reading from 4772: heap size 601 MB, throughput 0.990419
Equal recommendation: 4461 MB each
Reading from 4771: heap size 1583 MB, throughput 0.958379
Reading from 4772: heap size 602 MB, throughput 0.991463
Reading from 4772: heap size 605 MB, throughput 0.921786
Reading from 4771: heap size 1591 MB, throughput 0.956419
Reading from 4772: heap size 607 MB, throughput 0.991947
Reading from 4771: heap size 1607 MB, throughput 0.9657
Reading from 4772: heap size 614 MB, throughput 0.993681
Equal recommendation: 4461 MB each
Reading from 4771: heap size 1611 MB, throughput 0.967221
Reading from 4772: heap size 615 MB, throughput 0.993362
Reading from 4771: heap size 1628 MB, throughput 0.964857
Reading from 4772: heap size 615 MB, throughput 0.99274
Equal recommendation: 4461 MB each
Reading from 4771: heap size 1630 MB, throughput 0.962193
Reading from 4772: heap size 617 MB, throughput 0.990693
Reading from 4772: heap size 620 MB, throughput 0.988957
Reading from 4772: heap size 620 MB, throughput 0.947901
Reading from 4771: heap size 1646 MB, throughput 0.639105
Reading from 4772: heap size 626 MB, throughput 0.994559
Equal recommendation: 4461 MB each
Reading from 4771: heap size 1757 MB, throughput 0.990705
Reading from 4772: heap size 626 MB, throughput 0.993837
Reading from 4771: heap size 1762 MB, throughput 0.987496
Reading from 4772: heap size 626 MB, throughput 0.991681
Equal recommendation: 4461 MB each
Reading from 4772: heap size 629 MB, throughput 0.990882
Reading from 4772: heap size 631 MB, throughput 0.994951
Reading from 4772: heap size 631 MB, throughput 0.924533
Client 4772 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 4771: heap size 1773 MB, throughput 0.994086
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 4771: heap size 1766 MB, throughput 0.994815
Reading from 4771: heap size 1779 MB, throughput 0.923436
Reading from 4771: heap size 1764 MB, throughput 0.828408
Reading from 4771: heap size 1785 MB, throughput 0.814035
Reading from 4771: heap size 1819 MB, throughput 0.841332
Client 4771 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
