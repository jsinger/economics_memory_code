# Args: dir benchmark1 benchmark2 total_mem_to_use
# Outputs: throughput benchmark1_heap_size benchmark2_heap_size

import sys

if len(sys.argv) < 5:
    print "Usage: python " + sys.argv[0] + " dir benchmark1 benchmark2 total_mem_to_use"
    exit(1)

dir = sys.argv[1] + "/"
b1 = sys.argv[2]
b2 = sys.argv[3]
total = int(sys.argv[4])

def readFileInts(filename):
    l = []
    for line in open(filename):
        l.append(int(line))
    return l

def readFileFloat(filename):
    for line in open(filename):
        return float(line)

def heapSizesFile(b):
    return dir + b + "_heapSizes.txt"

def coefficientFile(b):
    return dir + b + "_fit_coefficient.txt"

def exponentFile(b):
    return dir + b + "_fit_exponent.txt"

b1_heap = readFileInts(heapSizesFile(b1))
b1_c = readFileFloat(coefficientFile(b1))
b1_e = readFileFloat(exponentFile(b1))
b2_heap = readFileInts(heapSizesFile(b2))
b2_c = readFileFloat(coefficientFile(b2))
b2_e = readFileFloat(exponentFile(b2))

def f1(h):
    return b1_c * (h ** b1_e)

def f2(h):
    return b2_c * (h ** b2_e)


l = []

for h1 in b1_heap:
    for h2 in b2_heap:
        if h1 + h2 <= total:
            l.append((f1(h1) + f2(h2), h1, h2))

l.sort(key=lambda x: x[0], reverse=True)

for t in l:
    print str(t[0]) + " " + str(t[1]) + " " + str(t[2])

