economemd
    total memory: 828 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_30_14_13_41/sub3_timerComparisonSingle/sub3_timeBenchmarkDaemonEqual/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
TimerThread: starting
CommandThread: starting
ReadingThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 451: heap size 9 MB, throughput 0.988087
Clients: 1
Reading from 450: heap size 9 MB, throughput 0.988301
Clients: 2
Client 451 has a minimum heap size of 276 MB
Client 450 has a minimum heap size of 276 MB
Reading from 450: heap size 9 MB, throughput 0.979853
Reading from 451: heap size 9 MB, throughput 0.966119
Reading from 450: heap size 11 MB, throughput 0.958834
Reading from 451: heap size 11 MB, throughput 0.973512
Reading from 451: heap size 11 MB, throughput 0.963333
Reading from 450: heap size 11 MB, throughput 0.986375
Reading from 450: heap size 15 MB, throughput 0.865919
Reading from 451: heap size 15 MB, throughput 0.814028
Reading from 450: heap size 18 MB, throughput 0.943125
Reading from 451: heap size 19 MB, throughput 0.979536
Reading from 451: heap size 23 MB, throughput 0.916225
Reading from 450: heap size 23 MB, throughput 0.938734
Reading from 451: heap size 27 MB, throughput 0.969484
Reading from 451: heap size 27 MB, throughput 0.71622
Reading from 450: heap size 28 MB, throughput 0.563262
Reading from 450: heap size 38 MB, throughput 0.934797
Reading from 450: heap size 39 MB, throughput 0.824746
Reading from 450: heap size 41 MB, throughput 0.705253
Reading from 451: heap size 29 MB, throughput 0.337881
Reading from 451: heap size 42 MB, throughput 0.821185
Reading from 450: heap size 42 MB, throughput 0.868534
Reading from 451: heap size 43 MB, throughput 0.762793
Reading from 451: heap size 46 MB, throughput 0.749898
Reading from 450: heap size 47 MB, throughput 0.252168
Reading from 450: heap size 62 MB, throughput 0.71617
Reading from 450: heap size 67 MB, throughput 0.860414
Reading from 451: heap size 48 MB, throughput 0.191783
Reading from 451: heap size 74 MB, throughput 0.67629
Reading from 451: heap size 75 MB, throughput 0.815962
Reading from 450: heap size 68 MB, throughput 0.254692
Reading from 450: heap size 90 MB, throughput 0.756096
Reading from 451: heap size 79 MB, throughput 0.213064
Reading from 450: heap size 94 MB, throughput 0.679662
Reading from 450: heap size 97 MB, throughput 0.636982
Reading from 451: heap size 98 MB, throughput 0.812876
Reading from 451: heap size 104 MB, throughput 0.711337
Reading from 451: heap size 105 MB, throughput 0.796251
Reading from 450: heap size 100 MB, throughput 0.276411
Reading from 450: heap size 130 MB, throughput 0.715832
Reading from 450: heap size 132 MB, throughput 0.709626
Reading from 450: heap size 136 MB, throughput 0.71092
Reading from 451: heap size 109 MB, throughput 0.232787
Reading from 451: heap size 136 MB, throughput 0.712227
Reading from 451: heap size 142 MB, throughput 0.533795
Reading from 451: heap size 146 MB, throughput 0.607001
Reading from 450: heap size 138 MB, throughput 0.225854
Reading from 450: heap size 171 MB, throughput 0.526782
Reading from 450: heap size 175 MB, throughput 0.693411
Reading from 450: heap size 177 MB, throughput 0.698059
Reading from 451: heap size 156 MB, throughput 0.180298
Reading from 451: heap size 187 MB, throughput 0.58058
Reading from 451: heap size 196 MB, throughput 0.663251
Reading from 450: heap size 182 MB, throughput 0.195769
Reading from 451: heap size 197 MB, throughput 0.788439
Reading from 450: heap size 223 MB, throughput 0.804768
Reading from 451: heap size 202 MB, throughput 0.899188
Reading from 450: heap size 227 MB, throughput 0.916591
Reading from 450: heap size 229 MB, throughput 0.671021
Reading from 450: heap size 235 MB, throughput 0.413379
Reading from 450: heap size 242 MB, throughput 0.82566
Reading from 451: heap size 209 MB, throughput 0.309765
Reading from 451: heap size 261 MB, throughput 0.641681
Reading from 450: heap size 246 MB, throughput 0.146432
Reading from 451: heap size 264 MB, throughput 0.77847
Reading from 450: heap size 284 MB, throughput 0.69984
Reading from 451: heap size 268 MB, throughput 0.790935
Reading from 450: heap size 287 MB, throughput 0.842889
Reading from 451: heap size 270 MB, throughput 0.743194
Reading from 450: heap size 286 MB, throughput 0.815828
Reading from 450: heap size 288 MB, throughput 0.784135
Reading from 451: heap size 275 MB, throughput 0.865825
Reading from 450: heap size 289 MB, throughput 0.804522
Reading from 451: heap size 276 MB, throughput 0.633841
Reading from 450: heap size 291 MB, throughput 0.768603
Reading from 451: heap size 281 MB, throughput 0.657816
Reading from 451: heap size 285 MB, throughput 0.677473
Reading from 450: heap size 293 MB, throughput 0.944154
Reading from 451: heap size 292 MB, throughput 0.710092
Reading from 451: heap size 294 MB, throughput 0.82866
Reading from 450: heap size 294 MB, throughput 0.914557
Reading from 450: heap size 297 MB, throughput 0.785854
Reading from 450: heap size 298 MB, throughput 0.783036
Reading from 450: heap size 302 MB, throughput 0.667945
Reading from 451: heap size 295 MB, throughput 0.905061
Reading from 450: heap size 304 MB, throughput 0.828918
Reading from 450: heap size 309 MB, throughput 0.739121
Reading from 450: heap size 310 MB, throughput 0.62203
Reading from 450: heap size 319 MB, throughput 0.653668
Reading from 451: heap size 299 MB, throughput 0.110718
Reading from 450: heap size 319 MB, throughput 0.711093
Reading from 451: heap size 341 MB, throughput 0.707362
Reading from 450: heap size 330 MB, throughput 0.769893
Reading from 451: heap size 344 MB, throughput 0.807177
Reading from 451: heap size 350 MB, throughput 0.937742
Reading from 451: heap size 350 MB, throughput 0.845582
Reading from 451: heap size 349 MB, throughput 0.803952
Reading from 450: heap size 330 MB, throughput 0.971099
Reading from 451: heap size 352 MB, throughput 0.580233
Equal recommendation: 414 MB each
Reading from 451: heap size 352 MB, throughput 0.576667
Reading from 451: heap size 354 MB, throughput 0.523143
Reading from 451: heap size 358 MB, throughput 0.808688
Reading from 450: heap size 340 MB, throughput 0.97532
Reading from 451: heap size 360 MB, throughput 0.977308
Reading from 450: heap size 341 MB, throughput 0.978339
Reading from 451: heap size 367 MB, throughput 0.976103
Reading from 450: heap size 344 MB, throughput 0.978382
Reading from 451: heap size 369 MB, throughput 0.979597
Reading from 450: heap size 346 MB, throughput 0.687434
Reading from 451: heap size 370 MB, throughput 0.973824
Reading from 450: heap size 388 MB, throughput 0.973472
Reading from 451: heap size 374 MB, throughput 0.972464
Reading from 450: heap size 390 MB, throughput 0.982858
Equal recommendation: 414 MB each
Reading from 451: heap size 374 MB, throughput 0.974917
Reading from 450: heap size 395 MB, throughput 0.987263
Reading from 451: heap size 377 MB, throughput 0.972711
Reading from 451: heap size 374 MB, throughput 0.9698
Reading from 450: heap size 396 MB, throughput 0.98355
Reading from 451: heap size 377 MB, throughput 0.970052
Reading from 450: heap size 394 MB, throughput 0.982882
Reading from 451: heap size 380 MB, throughput 0.97691
Reading from 450: heap size 397 MB, throughput 0.980511
Reading from 451: heap size 381 MB, throughput 0.944612
Reading from 450: heap size 398 MB, throughput 0.979423
Reading from 451: heap size 385 MB, throughput 0.983369
Equal recommendation: 414 MB each
Reading from 450: heap size 399 MB, throughput 0.985206
Reading from 451: heap size 386 MB, throughput 0.969934
Reading from 450: heap size 401 MB, throughput 0.8981
Reading from 451: heap size 384 MB, throughput 0.826641
Reading from 450: heap size 402 MB, throughput 0.814809
Reading from 451: heap size 386 MB, throughput 0.785102
Reading from 450: heap size 410 MB, throughput 0.841826
Reading from 451: heap size 396 MB, throughput 0.839906
Reading from 450: heap size 413 MB, throughput 0.948425
Reading from 451: heap size 387 MB, throughput 0.982015
Reading from 450: heap size 422 MB, throughput 0.986078
Reading from 451: heap size 398 MB, throughput 0.989145
Reading from 450: heap size 392 MB, throughput 0.986007
Reading from 451: heap size 364 MB, throughput 0.984124
Reading from 450: heap size 421 MB, throughput 0.986185
Reading from 451: heap size 397 MB, throughput 0.756967
Reading from 450: heap size 395 MB, throughput 0.989691
Equal recommendation: 414 MB each
Reading from 451: heap size 434 MB, throughput 0.982479
Reading from 450: heap size 417 MB, throughput 0.984901
Reading from 451: heap size 429 MB, throughput 0.985229
Reading from 450: heap size 397 MB, throughput 0.987386
Reading from 451: heap size 379 MB, throughput 0.988681
Reading from 450: heap size 412 MB, throughput 0.984386
Reading from 451: heap size 431 MB, throughput 0.988269
Reading from 450: heap size 412 MB, throughput 0.98005
Reading from 451: heap size 384 MB, throughput 0.977018
Reading from 450: heap size 415 MB, throughput 0.979251
Reading from 451: heap size 428 MB, throughput 0.985616
Equal recommendation: 414 MB each
Reading from 450: heap size 404 MB, throughput 0.983548
Reading from 450: heap size 414 MB, throughput 0.969044
Reading from 450: heap size 404 MB, throughput 0.867532
Reading from 450: heap size 411 MB, throughput 0.845785
Reading from 450: heap size 412 MB, throughput 0.888031
Reading from 451: heap size 391 MB, throughput 0.989806
Reading from 451: heap size 423 MB, throughput 0.984206
Reading from 451: heap size 390 MB, throughput 0.854058
Reading from 451: heap size 414 MB, throughput 0.842831
Reading from 451: heap size 418 MB, throughput 0.79988
Reading from 451: heap size 415 MB, throughput 0.973822
Reading from 450: heap size 420 MB, throughput 0.989173
Reading from 451: heap size 381 MB, throughput 0.985261
Reading from 450: heap size 388 MB, throughput 0.988014
Reading from 451: heap size 419 MB, throughput 0.988965
Reading from 450: heap size 419 MB, throughput 0.988201
Reading from 451: heap size 384 MB, throughput 0.980617
Reading from 450: heap size 389 MB, throughput 0.986871
Reading from 451: heap size 415 MB, throughput 0.982735
Equal recommendation: 414 MB each
Reading from 450: heap size 417 MB, throughput 0.987824
Reading from 451: heap size 387 MB, throughput 0.979906
Reading from 451: heap size 410 MB, throughput 0.985162
Reading from 450: heap size 392 MB, throughput 0.982651
Reading from 451: heap size 409 MB, throughput 0.979717
Reading from 450: heap size 413 MB, throughput 0.98575
Reading from 451: heap size 408 MB, throughput 0.988071
Reading from 450: heap size 411 MB, throughput 0.985759
Reading from 451: heap size 410 MB, throughput 0.983775
Reading from 450: heap size 413 MB, throughput 0.982783
Reading from 451: heap size 412 MB, throughput 0.978175
Reading from 450: heap size 413 MB, throughput 0.991735
Equal recommendation: 414 MB each
Reading from 451: heap size 412 MB, throughput 0.982779
Reading from 450: heap size 415 MB, throughput 0.974532
Reading from 450: heap size 403 MB, throughput 0.855092
Reading from 450: heap size 412 MB, throughput 0.869959
Reading from 450: heap size 413 MB, throughput 0.905255
Reading from 451: heap size 412 MB, throughput 0.983389
Reading from 451: heap size 415 MB, throughput 0.900665
Reading from 451: heap size 412 MB, throughput 0.876782
Reading from 451: heap size 415 MB, throughput 0.868843
Reading from 451: heap size 408 MB, throughput 0.984534
Reading from 450: heap size 421 MB, throughput 0.991767
Reading from 451: heap size 415 MB, throughput 0.98968
Reading from 450: heap size 390 MB, throughput 0.988976
Reading from 451: heap size 404 MB, throughput 0.985656
Reading from 450: heap size 420 MB, throughput 0.986875
Reading from 451: heap size 413 MB, throughput 0.984625
Reading from 450: heap size 391 MB, throughput 0.988689
Reading from 451: heap size 412 MB, throughput 0.984089
Equal recommendation: 414 MB each
Reading from 450: heap size 416 MB, throughput 0.984319
Reading from 451: heap size 414 MB, throughput 0.981957
Reading from 451: heap size 402 MB, throughput 0.980191
Reading from 450: heap size 393 MB, throughput 0.987215
Reading from 451: heap size 409 MB, throughput 0.979041
Reading from 450: heap size 411 MB, throughput 0.981382
Reading from 451: heap size 408 MB, throughput 0.978649
Reading from 450: heap size 410 MB, throughput 0.983423
Reading from 451: heap size 409 MB, throughput 0.981237
Reading from 450: heap size 412 MB, throughput 0.981134
Reading from 451: heap size 411 MB, throughput 0.975811
Reading from 450: heap size 413 MB, throughput 0.990886
Equal recommendation: 414 MB each
Reading from 451: heap size 411 MB, throughput 0.987723
Reading from 450: heap size 414 MB, throughput 0.970537
Reading from 450: heap size 415 MB, throughput 0.855064
Reading from 450: heap size 412 MB, throughput 0.851053
Reading from 451: heap size 413 MB, throughput 0.979127
Reading from 450: heap size 416 MB, throughput 0.964439
Reading from 451: heap size 414 MB, throughput 0.875763
Reading from 451: heap size 414 MB, throughput 0.853536
Reading from 451: heap size 415 MB, throughput 0.85404
Reading from 451: heap size 415 MB, throughput 0.978093
Reading from 450: heap size 406 MB, throughput 0.990394
Reading from 451: heap size 383 MB, throughput 0.991072
Reading from 450: heap size 416 MB, throughput 0.988799
Reading from 451: heap size 415 MB, throughput 0.986972
Reading from 450: heap size 404 MB, throughput 0.986772
Reading from 451: heap size 385 MB, throughput 0.981019
Reading from 450: heap size 413 MB, throughput 0.990213
Reading from 451: heap size 413 MB, throughput 0.985938
Equal recommendation: 414 MB each
Reading from 450: heap size 412 MB, throughput 0.987002
Reading from 451: heap size 412 MB, throughput 0.982532
Reading from 450: heap size 414 MB, throughput 0.986182
Reading from 451: heap size 411 MB, throughput 0.982684
Reading from 450: heap size 412 MB, throughput 0.98727
Reading from 451: heap size 412 MB, throughput 0.982663
Reading from 450: heap size 414 MB, throughput 0.983869
Reading from 451: heap size 413 MB, throughput 0.982543
Reading from 450: heap size 416 MB, throughput 0.983022
Reading from 451: heap size 413 MB, throughput 0.979178
Reading from 451: heap size 415 MB, throughput 0.979113
Equal recommendation: 414 MB each
Reading from 450: heap size 405 MB, throughput 0.985545
Reading from 450: heap size 415 MB, throughput 0.954048
Reading from 450: heap size 407 MB, throughput 0.848107
Reading from 450: heap size 412 MB, throughput 0.871682
Reading from 450: heap size 413 MB, throughput 0.972471
Reading from 451: heap size 405 MB, throughput 0.987429
Reading from 451: heap size 414 MB, throughput 0.895119
Reading from 451: heap size 412 MB, throughput 0.836418
Reading from 451: heap size 413 MB, throughput 0.85911
Reading from 451: heap size 415 MB, throughput 0.915841
Reading from 450: heap size 422 MB, throughput 0.987917
Reading from 451: heap size 405 MB, throughput 0.988356
Reading from 450: heap size 390 MB, throughput 0.992158
Reading from 451: heap size 415 MB, throughput 0.991238
Reading from 450: heap size 420 MB, throughput 0.989414
Reading from 451: heap size 403 MB, throughput 0.991717
Reading from 450: heap size 391 MB, throughput 0.986487
Reading from 451: heap size 412 MB, throughput 0.985595
Equal recommendation: 414 MB each
Reading from 450: heap size 417 MB, throughput 0.971805
Reading from 451: heap size 411 MB, throughput 0.977504
Reading from 451: heap size 413 MB, throughput 0.981448
Reading from 450: heap size 394 MB, throughput 0.988238
Reading from 450: heap size 412 MB, throughput 0.982765
Reading from 451: heap size 412 MB, throughput 0.984538
Reading from 451: heap size 413 MB, throughput 0.967973
Reading from 450: heap size 411 MB, throughput 0.972596
Reading from 451: heap size 415 MB, throughput 0.978365
Reading from 450: heap size 413 MB, throughput 0.98178
Reading from 451: heap size 403 MB, throughput 0.980719
Equal recommendation: 414 MB each
Reading from 450: heap size 414 MB, throughput 0.991132
Reading from 450: heap size 416 MB, throughput 0.926775
Reading from 450: heap size 409 MB, throughput 0.864434
Reading from 450: heap size 415 MB, throughput 0.874202
Reading from 451: heap size 413 MB, throughput 0.988161
Reading from 450: heap size 405 MB, throughput 0.984347
Reading from 451: heap size 413 MB, throughput 0.983093
Reading from 451: heap size 416 MB, throughput 0.877361
Reading from 451: heap size 413 MB, throughput 0.843112
Reading from 451: heap size 413 MB, throughput 0.808772
Reading from 450: heap size 415 MB, throughput 0.987717
Reading from 451: heap size 416 MB, throughput 0.98537
Reading from 450: heap size 383 MB, throughput 0.991463
Reading from 451: heap size 404 MB, throughput 0.985283
Reading from 450: heap size 413 MB, throughput 0.98823
Reading from 451: heap size 415 MB, throughput 0.990054
Reading from 451: heap size 403 MB, throughput 0.988178
Reading from 450: heap size 412 MB, throughput 0.988437
Equal recommendation: 414 MB each
Reading from 450: heap size 411 MB, throughput 0.985118
Reading from 451: heap size 412 MB, throughput 0.987291
Reading from 450: heap size 413 MB, throughput 0.980598
Reading from 451: heap size 410 MB, throughput 0.989021
Reading from 450: heap size 411 MB, throughput 0.978856
Reading from 451: heap size 413 MB, throughput 0.983918
Reading from 451: heap size 410 MB, throughput 0.98876
Reading from 450: heap size 413 MB, throughput 0.985428
Reading from 451: heap size 412 MB, throughput 0.981023
Reading from 450: heap size 415 MB, throughput 0.982937
Reading from 451: heap size 415 MB, throughput 0.981426
Equal recommendation: 414 MB each
Reading from 450: heap size 405 MB, throughput 0.992047
Reading from 451: heap size 403 MB, throughput 0.966517
Reading from 450: heap size 414 MB, throughput 0.929449
Reading from 450: heap size 414 MB, throughput 0.84635
Reading from 450: heap size 418 MB, throughput 0.878504
Reading from 450: heap size 408 MB, throughput 0.976419
Reading from 451: heap size 412 MB, throughput 0.988118
Reading from 451: heap size 413 MB, throughput 0.894713
Reading from 451: heap size 413 MB, throughput 0.814449
Reading from 451: heap size 415 MB, throughput 0.847619
Reading from 451: heap size 409 MB, throughput 0.939416
Reading from 450: heap size 418 MB, throughput 0.990414
Reading from 451: heap size 417 MB, throughput 0.988549
Reading from 450: heap size 386 MB, throughput 0.988196
Reading from 451: heap size 404 MB, throughput 0.990057
Reading from 450: heap size 416 MB, throughput 0.988384
Reading from 451: heap size 415 MB, throughput 0.985264
Reading from 450: heap size 387 MB, throughput 0.987284
Equal recommendation: 414 MB each
Reading from 451: heap size 402 MB, throughput 0.987329
Reading from 450: heap size 413 MB, throughput 0.986435
Reading from 451: heap size 411 MB, throughput 0.984474
Reading from 450: heap size 411 MB, throughput 0.983971
Reading from 451: heap size 409 MB, throughput 0.982654
Reading from 450: heap size 410 MB, throughput 0.9826
Reading from 451: heap size 411 MB, throughput 0.98179
Reading from 450: heap size 411 MB, throughput 0.9857
Reading from 451: heap size 411 MB, throughput 0.984372
Reading from 450: heap size 414 MB, throughput 0.98279
Reading from 451: heap size 412 MB, throughput 0.982325
Equal recommendation: 414 MB each
Reading from 450: heap size 414 MB, throughput 0.990982
Reading from 451: heap size 414 MB, throughput 0.979461
Reading from 450: heap size 407 MB, throughput 0.925631
Reading from 450: heap size 412 MB, throughput 0.874339
Reading from 450: heap size 416 MB, throughput 0.872695
Reading from 450: heap size 406 MB, throughput 0.98475
Reading from 451: heap size 414 MB, throughput 0.994178
Reading from 450: heap size 416 MB, throughput 0.987786
Reading from 451: heap size 408 MB, throughput 0.978568
Reading from 451: heap size 413 MB, throughput 0.868017
Reading from 451: heap size 412 MB, throughput 0.807631
Reading from 451: heap size 415 MB, throughput 0.843803
Reading from 451: heap size 409 MB, throughput 0.983154
Reading from 450: heap size 384 MB, throughput 0.987996
Reading from 451: heap size 417 MB, throughput 0.986106
Reading from 450: heap size 414 MB, throughput 0.986005
Reading from 451: heap size 405 MB, throughput 0.986379
Reading from 450: heap size 386 MB, throughput 0.988902
Equal recommendation: 414 MB each
Reading from 451: heap size 415 MB, throughput 0.984388
Reading from 450: heap size 411 MB, throughput 0.98714
Reading from 451: heap size 402 MB, throughput 0.98387
Reading from 450: heap size 410 MB, throughput 0.982153
Reading from 451: heap size 410 MB, throughput 0.979592
Reading from 450: heap size 409 MB, throughput 0.983812
Reading from 451: heap size 408 MB, throughput 0.981737
Reading from 450: heap size 410 MB, throughput 0.985775
Reading from 451: heap size 410 MB, throughput 0.984719
Reading from 451: heap size 410 MB, throughput 0.97057
Reading from 450: heap size 412 MB, throughput 0.977897
Equal recommendation: 414 MB each
Reading from 451: heap size 411 MB, throughput 0.980646
Reading from 450: heap size 413 MB, throughput 0.991782
Reading from 450: heap size 415 MB, throughput 0.91719
Reading from 450: heap size 408 MB, throughput 0.867212
Reading from 450: heap size 414 MB, throughput 0.874575
Reading from 451: heap size 414 MB, throughput 0.981032
Reading from 450: heap size 415 MB, throughput 0.983121
Reading from 451: heap size 404 MB, throughput 0.988608
Reading from 450: heap size 405 MB, throughput 0.988868
Reading from 451: heap size 413 MB, throughput 0.909796
Reading from 451: heap size 413 MB, throughput 0.837189
Reading from 451: heap size 418 MB, throughput 0.85894
Reading from 451: heap size 409 MB, throughput 0.89633
Reading from 450: heap size 415 MB, throughput 0.989871
Reading from 451: heap size 419 MB, throughput 0.989088
Reading from 450: heap size 403 MB, throughput 0.990348
Reading from 451: heap size 386 MB, throughput 0.990826
Reading from 450: heap size 411 MB, throughput 0.99078
Reading from 451: heap size 417 MB, throughput 0.987774
Equal recommendation: 414 MB each
Reading from 450: heap size 410 MB, throughput 0.987586
Reading from 451: heap size 388 MB, throughput 0.98956
Reading from 450: heap size 412 MB, throughput 0.982252
Reading from 451: heap size 415 MB, throughput 0.985479
Reading from 450: heap size 411 MB, throughput 0.865885
Reading from 451: heap size 390 MB, throughput 0.985483
Reading from 451: heap size 411 MB, throughput 0.982973
Reading from 450: heap size 442 MB, throughput 0.994874
Reading from 451: heap size 410 MB, throughput 0.986657
Reading from 450: heap size 440 MB, throughput 0.98961
Equal recommendation: 414 MB each
Reading from 451: heap size 411 MB, throughput 0.981465
Reading from 450: heap size 401 MB, throughput 0.981203
Reading from 450: heap size 441 MB, throughput 0.962864
Reading from 450: heap size 439 MB, throughput 0.885499
Reading from 450: heap size 440 MB, throughput 0.885375
Reading from 450: heap size 426 MB, throughput 0.916957
Reading from 451: heap size 411 MB, throughput 0.977957
Reading from 450: heap size 435 MB, throughput 0.989521
Reading from 451: heap size 413 MB, throughput 0.989317
Reading from 450: heap size 403 MB, throughput 0.986442
Reading from 451: heap size 414 MB, throughput 0.975241
Reading from 451: heap size 416 MB, throughput 0.877528
Reading from 451: heap size 414 MB, throughput 0.851032
Reading from 451: heap size 415 MB, throughput 0.837331
Reading from 451: heap size 415 MB, throughput 0.946025
Reading from 450: heap size 436 MB, throughput 0.982843
Reading from 451: heap size 413 MB, throughput 0.983481
Reading from 450: heap size 406 MB, throughput 0.983355
Reading from 450: heap size 432 MB, throughput 0.983434
Reading from 451: heap size 415 MB, throughput 0.98859
Equal recommendation: 414 MB each
Reading from 450: heap size 410 MB, throughput 0.985619
Reading from 451: heap size 402 MB, throughput 0.983759
Reading from 450: heap size 428 MB, throughput 0.980562
Reading from 451: heap size 411 MB, throughput 0.977949
Reading from 450: heap size 412 MB, throughput 0.977988
Reading from 451: heap size 410 MB, throughput 0.986401
Reading from 450: heap size 424 MB, throughput 0.985627
Reading from 451: heap size 412 MB, throughput 0.981667
Reading from 450: heap size 412 MB, throughput 0.980073
Reading from 451: heap size 411 MB, throughput 0.86927
Reading from 450: heap size 423 MB, throughput 0.973434
Equal recommendation: 414 MB each
Reading from 451: heap size 426 MB, throughput 0.996463
Reading from 450: heap size 422 MB, throughput 0.985792
Reading from 450: heap size 422 MB, throughput 0.8765
Reading from 450: heap size 410 MB, throughput 0.846283
Reading from 450: heap size 423 MB, throughput 0.811521
Reading from 450: heap size 408 MB, throughput 0.865272
Reading from 451: heap size 424 MB, throughput 0.991903
Reading from 450: heap size 423 MB, throughput 0.983697
Reading from 451: heap size 384 MB, throughput 0.989354
Reading from 450: heap size 386 MB, throughput 0.985562
Reading from 450: heap size 422 MB, throughput 0.985247
Reading from 451: heap size 426 MB, throughput 0.989371
Reading from 451: heap size 399 MB, throughput 0.914854
Reading from 451: heap size 417 MB, throughput 0.834548
Reading from 451: heap size 419 MB, throughput 0.826558
Reading from 451: heap size 420 MB, throughput 0.87306
Reading from 450: heap size 388 MB, throughput 0.985064
Reading from 451: heap size 421 MB, throughput 0.981725
Reading from 450: heap size 420 MB, throughput 0.988154
Reading from 451: heap size 417 MB, throughput 0.988244
Reading from 450: heap size 391 MB, throughput 0.986346
Equal recommendation: 414 MB each
Reading from 451: heap size 384 MB, throughput 0.986022
Reading from 450: heap size 417 MB, throughput 0.977971
Reading from 451: heap size 419 MB, throughput 0.981826
Reading from 450: heap size 394 MB, throughput 0.981945
Reading from 451: heap size 388 MB, throughput 0.988466
Reading from 450: heap size 413 MB, throughput 0.978391
Reading from 451: heap size 415 MB, throughput 0.987272
Reading from 450: heap size 412 MB, throughput 0.978466
Reading from 451: heap size 392 MB, throughput 0.982808
Reading from 451: heap size 410 MB, throughput 0.952834
Reading from 450: heap size 415 MB, throughput 0.959045
Reading from 451: heap size 410 MB, throughput 0.977991
Reading from 450: heap size 402 MB, throughput 0.981509
Equal recommendation: 414 MB each
Reading from 451: heap size 413 MB, throughput 0.982952
Reading from 450: heap size 412 MB, throughput 0.989268
Reading from 450: heap size 413 MB, throughput 0.862642
Reading from 450: heap size 413 MB, throughput 0.846508
Reading from 450: heap size 416 MB, throughput 0.813283
Reading from 450: heap size 410 MB, throughput 0.823568
Reading from 451: heap size 413 MB, throughput 0.977033
Reading from 450: heap size 418 MB, throughput 0.982297
Reading from 451: heap size 416 MB, throughput 0.975152
Reading from 450: heap size 407 MB, throughput 0.987158
Reading from 450: heap size 418 MB, throughput 0.986476
Reading from 451: heap size 404 MB, throughput 0.984682
Reading from 451: heap size 414 MB, throughput 0.948193
Reading from 451: heap size 404 MB, throughput 0.839248
Reading from 451: heap size 411 MB, throughput 0.813537
Reading from 451: heap size 414 MB, throughput 0.821038
Reading from 450: heap size 406 MB, throughput 0.982039
Reading from 451: heap size 423 MB, throughput 0.97583
Reading from 450: heap size 416 MB, throughput 0.985297
Reading from 451: heap size 390 MB, throughput 0.986415
Reading from 450: heap size 403 MB, throughput 0.981988
Reading from 451: heap size 423 MB, throughput 0.984191
Equal recommendation: 414 MB each
Reading from 450: heap size 412 MB, throughput 0.982004
Reading from 451: heap size 391 MB, throughput 0.986033
Reading from 450: heap size 410 MB, throughput 0.981436
Reading from 451: heap size 421 MB, throughput 0.982899
Reading from 450: heap size 412 MB, throughput 0.980675
Reading from 451: heap size 393 MB, throughput 0.98783
Reading from 450: heap size 412 MB, throughput 0.982172
Reading from 451: heap size 418 MB, throughput 0.981981
Reading from 450: heap size 413 MB, throughput 0.978943
Reading from 451: heap size 396 MB, throughput 0.980729
Reading from 450: heap size 415 MB, throughput 0.976049
Reading from 451: heap size 415 MB, throughput 0.983717
Equal recommendation: 414 MB each
Reading from 450: heap size 405 MB, throughput 0.992361
Reading from 451: heap size 399 MB, throughput 0.978337
Reading from 450: heap size 414 MB, throughput 0.98056
Reading from 450: heap size 392 MB, throughput 0.882679
Reading from 450: heap size 409 MB, throughput 0.807029
Reading from 450: heap size 413 MB, throughput 0.775294
Client 450 died
Clients: 1
Reading from 451: heap size 411 MB, throughput 0.985371
Reading from 451: heap size 411 MB, throughput 0.985663
Reading from 451: heap size 413 MB, throughput 0.984328
Reading from 451: heap size 414 MB, throughput 0.966834
Reading from 451: heap size 416 MB, throughput 0.875599
Reading from 451: heap size 415 MB, throughput 0.861616
Reading from 451: heap size 415 MB, throughput 0.868973
Client 451 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
