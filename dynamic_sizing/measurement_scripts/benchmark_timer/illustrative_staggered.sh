#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") input_file max_heap_mb"
    exit 1
}

test -z "${1}" && usage
test -z "${2}" && usage

"${DIR}/staggered_start" "${1}" "${2}" "${DIR}/../../data/saved/min_heaps.csv"
"${DIR}/staggered_start" --dont-recommend "${1}" "${2}" "${DIR}/../../data/saved/min_heaps.csv"
"${DIR}/staggered_start" --force-growth "${1}" "${2}" "${DIR}/../../data/saved/min_heaps.csv"
"${DIR}/staggered_start" --force-growth --dont-recommend "${1}" "${2}" "${DIR}/../../data/saved/min_heaps.csv"
