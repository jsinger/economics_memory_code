#ifndef MAJORONLYTHROUGHPUT_HPP
#define MAJORONLYTHROUGHPUT_HPP

#include "Stopwatch.hpp"
#include "ThroughputCalculator.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
//! Throughput-calculation algorithm that measures throughput for an entire major-major period.
/*! This algorithm applies the basic definition of throughput directly, but only after major GCs. For every major GC, the throughput definition is applied: `throughput = mutatorTime / (mutatorTime + gcTime)`, or equivalently, `1 - (gcTime / (mutatorTime + gcTime))`. `mutatorTime` is the total mutation time since the last major GC, and `gcTime` is the total GC time since the last major GC (all minor GCs plus the major GC itself).

  For a system with only one kind of GC, this algorithm is equivalent to SimpleThroughput.

  For a system with generational GC, you still need to report minor GCs so that the total mutation and GC time can be calculated.

  To use, subclass, and implement getHeapSizeB() and logReading() as necessary for your VM/application. */
    class MajorOnlyThroughput : public ThroughputCalculator
    {
    public:
        MajorOnlyThroughput();

        virtual void gcBegin();
        virtual ThroughputCalculator::Result minorGCEnd();
        virtual ThroughputCalculator::Result majorGCEnd();

    protected:
        //! Return the current heap size in bytes.
        virtual unsigned long long getHeapSizeB() = 0;

        //! Use if you want to log the calculated throughputs in your client.
        virtual void logReading(unsigned long long heapSizeB, double throughput, unsigned long long gcTime, unsigned long long interGCTime) = 0;

    private:
        economem::Stopwatch m_GCTimer;
        economem::Stopwatch m_TotalTimer;
        unsigned long long m_HeapSizeAtBeginCollectionB;
        unsigned long long m_GCTime;
    };
}

/*! @} */

#endif
