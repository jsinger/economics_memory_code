#!/bin/bash
# Run a full set of measurements
#
# Usage: measureLocal.sh config_file
#

function usage()
{
    echo "Usage: $(basename "${0}") config_file"
    exit 1
}

test -z "${1}" && usage
test ! -f "${1}" && usage

CONFIG_FILE="$(readlink -f "${1}")"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

"${DIR}/../checkDacapo.sh" || exit 1

ITERATIONS='10'
ME="$(whoami)@$(hostname)"
OUTDIR="${DIR}/measure_${ME}_$(date '+%Y_%m_%d_%H_%M_%S')"
mkdir -p "${OUTDIR}"

BENCHMARKS_FILE="${OUTDIR}/benchmarks.txt"
TIMESTAMPS_FILE="${OUTDIR}/timestamps.txt"

"${DIR}/../machineInfo.sh" "${OUTDIR}"

echo "${ITERATIONS}" > "${OUTDIR}/iterations.txt"
cp "${CONFIG_FILE}" "${OUTDIR}/config.txt"

N_FOR_NOVER='1'

INPUT_FILE="${OUTDIR}/input.txt"

function setupBenchmark()
{
    BENCHMARK="${1}"
    MIN_HEAP="${2}"
    MAX_HEAP="${3}"
    INCREMENT="${4}"
    HEAP_SIZE="${MIN_HEAP}"

    echo "${BENCHMARK}" >> "${BENCHMARKS_FILE}"

    while [ "${HEAP_SIZE}" -le $(( MAX_HEAP + INCREMENT )) ]; do

        echo "${BENCHMARK}" "${HEAP_SIZE}" $(( MAX_HEAP + INCREMENT )) >> "${INPUT_FILE}"
        HEAP_SIZE=$(( HEAP_SIZE + INCREMENT ))

    done
}

while read benchmark min max inc; do
    setupBenchmark "${benchmark}" "${min}" "${max}" "${inc}"
done < "${CONFIG_FILE}"


echo "Local run on ${ME}" >> "${TIMESTAMPS_FILE}"
echo "Started: $(date '+%Y %m %d, %H:%M:%S')" >> "${TIMESTAMPS_FILE}"
STARTTIME="$(date '+%s')"

while read BENCHMARK HEAP_SIZE MAX_HEAP; do
    RESULTS_FILE="${OUTDIR}/${BENCHMARK}_${ME}_results.txt"

    "${DIR}/measure_benchmark.sh" "${BENCHMARK}" "${HEAP_SIZE}" "${ITERATIONS}" "${MAX_HEAP}" "${OUTDIR}" "${N_FOR_NOVER}" "${RESULTS_FILE}" || exit 1
done < "${INPUT_FILE}"

"${DIR}/measure_postprocess.sh" "${OUTDIR}"

echo "Completed: $(date '+%Y %m %d, %H:%M:%S')" >> "${TIMESTAMPS_FILE}"
ENDTIME="$(date '+%s')"

RUNTIME=$(( ENDTIME - STARTTIME ))
echo "Runtime: ${RUNTIME} s" >> "${TIMESTAMPS_FILE}"
