economemd
    total memory: 6175 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub13_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 29557: heap size 9 MB, throughput 0.987305
Clients: 1
Client 29557 has a minimum heap size of 12 MB
Reading from 29556: heap size 9 MB, throughput 0.963865
Clients: 2
Client 29556 has a minimum heap size of 1223 MB
Reading from 29556: heap size 9 MB, throughput 0.963425
Reading from 29557: heap size 9 MB, throughput 0.987786
Reading from 29556: heap size 11 MB, throughput 0.977145
Reading from 29557: heap size 11 MB, throughput 0.981621
Reading from 29556: heap size 11 MB, throughput 0.980366
Reading from 29557: heap size 11 MB, throughput 0.974236
Reading from 29557: heap size 15 MB, throughput 0.98098
Reading from 29556: heap size 15 MB, throughput 0.859147
Reading from 29557: heap size 15 MB, throughput 0.981316
Reading from 29556: heap size 19 MB, throughput 0.971617
Reading from 29556: heap size 24 MB, throughput 0.918443
Reading from 29556: heap size 29 MB, throughput 0.931667
Reading from 29557: heap size 24 MB, throughput 0.928615
Reading from 29556: heap size 31 MB, throughput 0.411443
Reading from 29556: heap size 41 MB, throughput 0.747884
Reading from 29556: heap size 46 MB, throughput 0.83509
Reading from 29556: heap size 47 MB, throughput 0.828799
Reading from 29557: heap size 30 MB, throughput 0.988222
Reading from 29556: heap size 51 MB, throughput 0.228421
Reading from 29557: heap size 36 MB, throughput 0.981064
Reading from 29556: heap size 66 MB, throughput 0.237266
Reading from 29556: heap size 90 MB, throughput 0.616656
Reading from 29556: heap size 92 MB, throughput 0.683007
Reading from 29557: heap size 42 MB, throughput 0.990002
Reading from 29556: heap size 94 MB, throughput 0.691262
Reading from 29556: heap size 97 MB, throughput 0.674996
Reading from 29557: heap size 43 MB, throughput 0.988663
Reading from 29557: heap size 46 MB, throughput 0.984597
Reading from 29556: heap size 100 MB, throughput 0.148692
Reading from 29557: heap size 52 MB, throughput 0.982105
Reading from 29556: heap size 129 MB, throughput 0.747769
Reading from 29556: heap size 134 MB, throughput 0.743999
Reading from 29557: heap size 52 MB, throughput 0.957288
Reading from 29556: heap size 135 MB, throughput 0.770826
Reading from 29556: heap size 139 MB, throughput 0.669315
Reading from 29557: heap size 59 MB, throughput 0.976196
Reading from 29556: heap size 144 MB, throughput 0.644485
Reading from 29556: heap size 149 MB, throughput 0.628228
Reading from 29557: heap size 59 MB, throughput 0.962655
Reading from 29556: heap size 154 MB, throughput 0.467153
Reading from 29557: heap size 70 MB, throughput 0.609923
Reading from 29557: heap size 74 MB, throughput 0.991935
Reading from 29556: heap size 160 MB, throughput 0.0812113
Reading from 29556: heap size 195 MB, throughput 0.534891
Reading from 29557: heap size 82 MB, throughput 0.995605
Reading from 29556: heap size 206 MB, throughput 0.121941
Reading from 29557: heap size 84 MB, throughput 0.997342
Reading from 29556: heap size 242 MB, throughput 0.631413
Reading from 29556: heap size 241 MB, throughput 0.716788
Reading from 29556: heap size 244 MB, throughput 0.722514
Reading from 29556: heap size 246 MB, throughput 0.625381
Reading from 29556: heap size 250 MB, throughput 0.667023
Reading from 29556: heap size 252 MB, throughput 0.663018
Reading from 29557: heap size 91 MB, throughput 0.997362
Reading from 29556: heap size 260 MB, throughput 0.553428
Reading from 29556: heap size 264 MB, throughput 0.559318
Reading from 29556: heap size 274 MB, throughput 0.431801
Reading from 29556: heap size 278 MB, throughput 0.417033
Reading from 29556: heap size 290 MB, throughput 0.460387
Reading from 29557: heap size 93 MB, throughput 0.99688
Reading from 29556: heap size 297 MB, throughput 0.0657895
Reading from 29556: heap size 342 MB, throughput 0.479716
Reading from 29557: heap size 99 MB, throughput 0.99707
Reading from 29556: heap size 339 MB, throughput 0.0853169
Reading from 29557: heap size 100 MB, throughput 0.995934
Reading from 29556: heap size 320 MB, throughput 0.35509
Reading from 29556: heap size 384 MB, throughput 0.584035
Reading from 29556: heap size 311 MB, throughput 0.653671
Reading from 29556: heap size 376 MB, throughput 0.560263
Reading from 29556: heap size 328 MB, throughput 0.542584
Reading from 29556: heap size 378 MB, throughput 0.505086
Reading from 29556: heap size 379 MB, throughput 0.567155
Reading from 29557: heap size 105 MB, throughput 0.997433
Reading from 29556: heap size 383 MB, throughput 0.527317
Reading from 29557: heap size 106 MB, throughput 0.991147
Reading from 29556: heap size 385 MB, throughput 0.08799
Equal recommendation: 3087 MB each
Reading from 29556: heap size 439 MB, throughput 0.380544
Reading from 29556: heap size 442 MB, throughput 0.52225
Reading from 29556: heap size 442 MB, throughput 0.467845
Reading from 29557: heap size 111 MB, throughput 0.996786
Reading from 29556: heap size 443 MB, throughput 0.538269
Reading from 29556: heap size 446 MB, throughput 0.495581
Reading from 29556: heap size 450 MB, throughput 0.525939
Reading from 29556: heap size 455 MB, throughput 0.564003
Reading from 29557: heap size 111 MB, throughput 0.994536
Reading from 29556: heap size 460 MB, throughput 0.470989
Reading from 29556: heap size 464 MB, throughput 0.469342
Reading from 29557: heap size 114 MB, throughput 0.994915
Reading from 29556: heap size 471 MB, throughput 0.0975101
Reading from 29556: heap size 522 MB, throughput 0.39717
Reading from 29556: heap size 529 MB, throughput 0.555744
Reading from 29557: heap size 115 MB, throughput 0.996148
Reading from 29556: heap size 534 MB, throughput 0.506587
Reading from 29556: heap size 534 MB, throughput 0.466297
Reading from 29557: heap size 119 MB, throughput 0.995206
Reading from 29556: heap size 537 MB, throughput 0.0985779
Reading from 29556: heap size 600 MB, throughput 0.453916
Reading from 29556: heap size 596 MB, throughput 0.659572
Reading from 29557: heap size 119 MB, throughput 0.996169
Reading from 29556: heap size 601 MB, throughput 0.553568
Reading from 29556: heap size 602 MB, throughput 0.614119
Reading from 29556: heap size 606 MB, throughput 0.585971
Reading from 29557: heap size 122 MB, throughput 0.99539
Reading from 29556: heap size 611 MB, throughput 0.0973182
Reading from 29557: heap size 123 MB, throughput 0.996247
Reading from 29556: heap size 678 MB, throughput 0.780084
Reading from 29557: heap size 126 MB, throughput 0.997487
Reading from 29556: heap size 685 MB, throughput 0.891166
Reading from 29557: heap size 126 MB, throughput 0.996988
Reading from 29556: heap size 686 MB, throughput 0.860007
Reading from 29556: heap size 686 MB, throughput 0.812344
Reading from 29556: heap size 699 MB, throughput 0.475289
Reading from 29556: heap size 709 MB, throughput 0.638308
Reading from 29557: heap size 129 MB, throughput 0.997148
Reading from 29557: heap size 129 MB, throughput 0.997255
Reading from 29557: heap size 131 MB, throughput 0.99812
Reading from 29556: heap size 715 MB, throughput 0.0253733
Equal recommendation: 3087 MB each
Reading from 29556: heap size 789 MB, throughput 0.222504
Reading from 29556: heap size 789 MB, throughput 0.400712
Reading from 29556: heap size 783 MB, throughput 0.466513
Reading from 29557: heap size 132 MB, throughput 0.998324
Reading from 29556: heap size 786 MB, throughput 0.880515
Reading from 29556: heap size 780 MB, throughput 0.574066
Reading from 29557: heap size 134 MB, throughput 0.997731
Reading from 29556: heap size 785 MB, throughput 0.0594802
Reading from 29557: heap size 134 MB, throughput 0.998023
Reading from 29556: heap size 860 MB, throughput 0.49759
Reading from 29556: heap size 861 MB, throughput 0.861684
Reading from 29556: heap size 869 MB, throughput 0.754822
Reading from 29556: heap size 869 MB, throughput 0.846011
Reading from 29557: heap size 136 MB, throughput 0.998103
Reading from 29557: heap size 136 MB, throughput 0.989811
Reading from 29557: heap size 137 MB, throughput 0.975606
Reading from 29556: heap size 874 MB, throughput 0.178065
Reading from 29556: heap size 966 MB, throughput 0.50031
Reading from 29557: heap size 138 MB, throughput 0.991762
Reading from 29556: heap size 971 MB, throughput 0.931183
Reading from 29556: heap size 824 MB, throughput 0.858643
Reading from 29556: heap size 961 MB, throughput 0.794963
Reading from 29556: heap size 834 MB, throughput 0.810005
Reading from 29556: heap size 952 MB, throughput 0.776271
Reading from 29557: heap size 145 MB, throughput 0.997406
Reading from 29556: heap size 840 MB, throughput 0.788811
Reading from 29556: heap size 946 MB, throughput 0.832116
Reading from 29556: heap size 952 MB, throughput 0.841833
Reading from 29556: heap size 942 MB, throughput 0.836644
Reading from 29556: heap size 948 MB, throughput 0.847451
Reading from 29557: heap size 145 MB, throughput 0.997174
Reading from 29556: heap size 937 MB, throughput 0.832675
Reading from 29556: heap size 943 MB, throughput 0.980173
Reading from 29557: heap size 150 MB, throughput 0.998113
Reading from 29557: heap size 151 MB, throughput 0.997165
Reading from 29556: heap size 943 MB, throughput 0.96893
Reading from 29556: heap size 944 MB, throughput 0.812973
Reading from 29556: heap size 940 MB, throughput 0.814506
Reading from 29556: heap size 944 MB, throughput 0.82643
Reading from 29557: heap size 154 MB, throughput 0.995008
Reading from 29556: heap size 940 MB, throughput 0.820643
Equal recommendation: 3087 MB each
Reading from 29556: heap size 943 MB, throughput 0.825186
Reading from 29556: heap size 941 MB, throughput 0.841142
Reading from 29556: heap size 944 MB, throughput 0.829905
Reading from 29556: heap size 943 MB, throughput 0.810097
Reading from 29557: heap size 155 MB, throughput 0.996887
Reading from 29556: heap size 945 MB, throughput 0.88818
Reading from 29556: heap size 946 MB, throughput 0.896983
Reading from 29556: heap size 948 MB, throughput 0.845202
Reading from 29557: heap size 159 MB, throughput 0.997994
Reading from 29556: heap size 951 MB, throughput 0.754781
Reading from 29556: heap size 952 MB, throughput 0.68298
Reading from 29556: heap size 980 MB, throughput 0.730879
Reading from 29557: heap size 159 MB, throughput 0.996691
Reading from 29557: heap size 162 MB, throughput 0.99739
Reading from 29556: heap size 981 MB, throughput 0.0737044
Reading from 29556: heap size 1086 MB, throughput 0.629653
Reading from 29556: heap size 1090 MB, throughput 0.825774
Reading from 29557: heap size 162 MB, throughput 0.996158
Reading from 29556: heap size 1093 MB, throughput 0.816438
Reading from 29556: heap size 1096 MB, throughput 0.79793
Reading from 29556: heap size 1099 MB, throughput 0.811022
Reading from 29556: heap size 1102 MB, throughput 0.784198
Reading from 29556: heap size 1109 MB, throughput 0.856173
Reading from 29557: heap size 165 MB, throughput 0.99698
Reading from 29557: heap size 165 MB, throughput 0.997073
Reading from 29557: heap size 168 MB, throughput 0.99797
Reading from 29556: heap size 1111 MB, throughput 0.988112
Reading from 29557: heap size 168 MB, throughput 0.997906
Equal recommendation: 3087 MB each
Reading from 29557: heap size 170 MB, throughput 0.998675
Reading from 29557: heap size 171 MB, throughput 0.998733
Reading from 29557: heap size 172 MB, throughput 0.996643
Reading from 29556: heap size 1128 MB, throughput 0.973044
Reading from 29557: heap size 173 MB, throughput 0.992553
Reading from 29557: heap size 175 MB, throughput 0.996319
Reading from 29557: heap size 175 MB, throughput 0.997804
Reading from 29556: heap size 1132 MB, throughput 0.98367
Reading from 29557: heap size 179 MB, throughput 0.998082
Reading from 29557: heap size 179 MB, throughput 0.996807
Reading from 29557: heap size 182 MB, throughput 0.997382
Reading from 29556: heap size 1136 MB, throughput 0.983515
Reading from 29557: heap size 182 MB, throughput 0.997816
Equal recommendation: 3087 MB each
Reading from 29557: heap size 184 MB, throughput 0.9957
Reading from 29557: heap size 184 MB, throughput 0.997269
Reading from 29556: heap size 1139 MB, throughput 0.983125
Reading from 29557: heap size 186 MB, throughput 0.996915
Reading from 29557: heap size 187 MB, throughput 0.997308
Reading from 29557: heap size 189 MB, throughput 0.997846
Reading from 29556: heap size 1132 MB, throughput 0.982433
Reading from 29557: heap size 190 MB, throughput 0.997583
Reading from 29557: heap size 193 MB, throughput 0.997648
Reading from 29556: heap size 1138 MB, throughput 0.980593
Reading from 29557: heap size 193 MB, throughput 0.997475
Equal recommendation: 3087 MB each
Reading from 29557: heap size 196 MB, throughput 0.997052
Reading from 29557: heap size 196 MB, throughput 0.862069
Reading from 29557: heap size 203 MB, throughput 0.996611
Reading from 29556: heap size 1135 MB, throughput 0.978552
Reading from 29557: heap size 204 MB, throughput 0.998191
Reading from 29557: heap size 208 MB, throughput 0.998056
Reading from 29557: heap size 209 MB, throughput 0.997773
Reading from 29556: heap size 1137 MB, throughput 0.977181
Reading from 29557: heap size 213 MB, throughput 0.998589
Reading from 29557: heap size 214 MB, throughput 0.997609
Reading from 29556: heap size 1141 MB, throughput 0.978679
Reading from 29557: heap size 218 MB, throughput 0.998706
Equal recommendation: 3087 MB each
Reading from 29557: heap size 218 MB, throughput 0.998453
Reading from 29557: heap size 221 MB, throughput 0.998662
Reading from 29556: heap size 1143 MB, throughput 0.979077
Reading from 29557: heap size 221 MB, throughput 0.997776
Reading from 29557: heap size 225 MB, throughput 0.998377
Reading from 29556: heap size 1146 MB, throughput 0.975007
Reading from 29557: heap size 225 MB, throughput 0.998373
Reading from 29557: heap size 228 MB, throughput 0.995206
Reading from 29557: heap size 228 MB, throughput 0.992222
Reading from 29557: heap size 232 MB, throughput 0.996632
Reading from 29556: heap size 1150 MB, throughput 0.976683
Equal recommendation: 3087 MB each
Reading from 29557: heap size 233 MB, throughput 0.997897
Reading from 29557: heap size 239 MB, throughput 0.997662
Reading from 29556: heap size 1152 MB, throughput 0.977085
Reading from 29557: heap size 239 MB, throughput 0.997887
Reading from 29557: heap size 243 MB, throughput 0.998561
Reading from 29556: heap size 1157 MB, throughput 0.977492
Reading from 29557: heap size 244 MB, throughput 0.997801
Reading from 29557: heap size 249 MB, throughput 0.998051
Equal recommendation: 3087 MB each
Reading from 29557: heap size 249 MB, throughput 0.998088
Reading from 29556: heap size 1160 MB, throughput 0.977866
Reading from 29557: heap size 254 MB, throughput 0.997967
Reading from 29557: heap size 254 MB, throughput 0.997215
Reading from 29556: heap size 1165 MB, throughput 0.977638
Reading from 29557: heap size 257 MB, throughput 0.997387
Reading from 29557: heap size 258 MB, throughput 0.994192
Reading from 29557: heap size 262 MB, throughput 0.995801
Reading from 29556: heap size 1169 MB, throughput 0.97646
Reading from 29557: heap size 263 MB, throughput 0.997215
Equal recommendation: 3087 MB each
Reading from 29557: heap size 269 MB, throughput 0.998343
Reading from 29556: heap size 1172 MB, throughput 0.978579
Reading from 29557: heap size 269 MB, throughput 0.998195
Reading from 29557: heap size 273 MB, throughput 0.998304
Reading from 29556: heap size 1176 MB, throughput 0.979504
Reading from 29557: heap size 274 MB, throughput 0.997794
Reading from 29557: heap size 279 MB, throughput 0.998058
Reading from 29556: heap size 1177 MB, throughput 0.97931
Reading from 29557: heap size 279 MB, throughput 0.998215
Equal recommendation: 3087 MB each
Reading from 29557: heap size 283 MB, throughput 0.997845
Reading from 29556: heap size 1180 MB, throughput 0.977287
Reading from 29557: heap size 284 MB, throughput 0.998472
Reading from 29557: heap size 288 MB, throughput 0.991052
Reading from 29557: heap size 288 MB, throughput 0.997452
Reading from 29556: heap size 1181 MB, throughput 0.975461
Reading from 29557: heap size 296 MB, throughput 0.998297
Reading from 29557: heap size 297 MB, throughput 0.998097
Equal recommendation: 3087 MB each
Reading from 29556: heap size 1183 MB, throughput 0.9758
Reading from 29557: heap size 302 MB, throughput 0.99763
Reading from 29557: heap size 303 MB, throughput 0.998435
Reading from 29556: heap size 1183 MB, throughput 0.977342
Reading from 29557: heap size 308 MB, throughput 0.998294
Reading from 29557: heap size 309 MB, throughput 0.99716
Reading from 29557: heap size 314 MB, throughput 0.998251
Reading from 29556: heap size 1185 MB, throughput 0.665449
Equal recommendation: 3087 MB each
Reading from 29557: heap size 314 MB, throughput 0.996515
Reading from 29557: heap size 319 MB, throughput 0.995615
Reading from 29556: heap size 1287 MB, throughput 0.96833
Reading from 29557: heap size 319 MB, throughput 0.997402
Reading from 29557: heap size 327 MB, throughput 0.997533
Reading from 29556: heap size 1280 MB, throughput 0.990391
Reading from 29557: heap size 327 MB, throughput 0.997751
Equal recommendation: 3087 MB each
Reading from 29557: heap size 333 MB, throughput 0.998497
Reading from 29556: heap size 1289 MB, throughput 0.989835
Reading from 29557: heap size 334 MB, throughput 0.997499
Reading from 29557: heap size 340 MB, throughput 0.998321
Reading from 29556: heap size 1296 MB, throughput 0.988838
Reading from 29557: heap size 340 MB, throughput 0.998295
Reading from 29557: heap size 346 MB, throughput 0.99396
Reading from 29556: heap size 1297 MB, throughput 0.987471
Reading from 29557: heap size 346 MB, throughput 0.997668
Equal recommendation: 3087 MB each
Reading from 29557: heap size 354 MB, throughput 0.997797
Reading from 29556: heap size 1291 MB, throughput 0.986096
Reading from 29557: heap size 355 MB, throughput 0.998027
Reading from 29556: heap size 1195 MB, throughput 0.986479
Reading from 29557: heap size 362 MB, throughput 0.998578
Reading from 29557: heap size 362 MB, throughput 0.998352
Equal recommendation: 3087 MB each
Reading from 29556: heap size 1281 MB, throughput 0.984108
Reading from 29557: heap size 368 MB, throughput 0.998386
Reading from 29557: heap size 368 MB, throughput 0.998212
Reading from 29556: heap size 1211 MB, throughput 0.983467
Reading from 29557: heap size 374 MB, throughput 0.996881
Reading from 29557: heap size 374 MB, throughput 0.997301
Reading from 29556: heap size 1273 MB, throughput 0.982884
Equal recommendation: 3087 MB each
Reading from 29557: heap size 381 MB, throughput 0.998489
Reading from 29556: heap size 1278 MB, throughput 0.981393
Reading from 29557: heap size 382 MB, throughput 0.998471
Reading from 29557: heap size 387 MB, throughput 0.99857
Reading from 29556: heap size 1279 MB, throughput 0.981112
Reading from 29557: heap size 387 MB, throughput 0.998527
Equal recommendation: 3087 MB each
Reading from 29556: heap size 1279 MB, throughput 0.976934
Reading from 29557: heap size 392 MB, throughput 0.998254
Reading from 29557: heap size 393 MB, throughput 0.99812
Reading from 29557: heap size 398 MB, throughput 0.688778
Reading from 29556: heap size 1280 MB, throughput 0.97731
Reading from 29557: heap size 401 MB, throughput 0.998102
Reading from 29557: heap size 415 MB, throughput 0.999164
Reading from 29556: heap size 1284 MB, throughput 0.978195
Equal recommendation: 3087 MB each
Reading from 29557: heap size 415 MB, throughput 0.998963
Reading from 29556: heap size 1286 MB, throughput 0.976392
Reading from 29557: heap size 425 MB, throughput 0.999082
Reading from 29557: heap size 427 MB, throughput 0.999061
Reading from 29556: heap size 1292 MB, throughput 0.978254
Reading from 29557: heap size 435 MB, throughput 0.998993
Equal recommendation: 3087 MB each
Reading from 29556: heap size 1297 MB, throughput 0.971489
Reading from 29557: heap size 436 MB, throughput 0.997207
Reading from 29557: heap size 443 MB, throughput 0.997996
Reading from 29556: heap size 1301 MB, throughput 0.975408
Reading from 29557: heap size 444 MB, throughput 0.998592
Reading from 29556: heap size 1306 MB, throughput 0.97543
Reading from 29557: heap size 452 MB, throughput 0.998677
Equal recommendation: 3087 MB each
Reading from 29557: heap size 453 MB, throughput 0.998514
Reading from 29557: heap size 461 MB, throughput 0.998694
Reading from 29557: heap size 461 MB, throughput 0.997642
Reading from 29557: heap size 469 MB, throughput 0.997431
Equal recommendation: 3087 MB each
Reading from 29556: heap size 1307 MB, throughput 0.985703
Reading from 29557: heap size 469 MB, throughput 0.998258
Reading from 29557: heap size 479 MB, throughput 0.998563
Reading from 29557: heap size 479 MB, throughput 0.99846
Equal recommendation: 3087 MB each
Reading from 29557: heap size 488 MB, throughput 0.998636
Reading from 29556: heap size 1284 MB, throughput 0.922656
Reading from 29557: heap size 488 MB, throughput 0.997847
Reading from 29557: heap size 495 MB, throughput 0.997635
Reading from 29556: heap size 1360 MB, throughput 0.987379
Reading from 29557: heap size 496 MB, throughput 0.998537
Equal recommendation: 3087 MB each
Reading from 29556: heap size 1362 MB, throughput 0.976564
Reading from 29556: heap size 1267 MB, throughput 0.88388
Reading from 29556: heap size 1366 MB, throughput 0.811802
Reading from 29556: heap size 1366 MB, throughput 0.78304
Reading from 29556: heap size 1382 MB, throughput 0.796458
Reading from 29556: heap size 1387 MB, throughput 0.782393
Reading from 29556: heap size 1408 MB, throughput 0.816876
Reading from 29557: heap size 505 MB, throughput 0.998518
Reading from 29556: heap size 1410 MB, throughput 0.799151
Reading from 29556: heap size 1435 MB, throughput 0.886308
Reading from 29557: heap size 505 MB, throughput 0.998567
Reading from 29556: heap size 1436 MB, throughput 0.988832
Reading from 29557: heap size 512 MB, throughput 0.998585
Equal recommendation: 3087 MB each
Reading from 29557: heap size 513 MB, throughput 0.995399
Reading from 29556: heap size 1456 MB, throughput 0.988388
Reading from 29557: heap size 521 MB, throughput 0.998695
Reading from 29556: heap size 1460 MB, throughput 0.987648
Reading from 29557: heap size 522 MB, throughput 0.998562
Equal recommendation: 3087 MB each
Reading from 29556: heap size 1462 MB, throughput 0.986558
Reading from 29557: heap size 531 MB, throughput 0.998638
Reading from 29557: heap size 532 MB, throughput 0.998073
Reading from 29556: heap size 1470 MB, throughput 0.98801
Client 29557 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 29556: heap size 1460 MB, throughput 0.987287
Reading from 29556: heap size 1469 MB, throughput 0.989845
Recommendation: one client; give it all the memory
Reading from 29556: heap size 1470 MB, throughput 0.989428
Reading from 29556: heap size 1475 MB, throughput 0.988066
Reading from 29556: heap size 1462 MB, throughput 0.986593
Recommendation: one client; give it all the memory
Reading from 29556: heap size 1389 MB, throughput 0.986443
Recommendation: one client; give it all the memory
Reading from 29556: heap size 1460 MB, throughput 0.985893
Reading from 29556: heap size 1464 MB, throughput 0.984831
Recommendation: one client; give it all the memory
Reading from 29556: heap size 1467 MB, throughput 0.983519
Reading from 29556: heap size 1470 MB, throughput 0.982737
Recommendation: one client; give it all the memory
Reading from 29556: heap size 1473 MB, throughput 0.981449
Reading from 29556: heap size 1481 MB, throughput 0.981263
Recommendation: one client; give it all the memory
Reading from 29556: heap size 1487 MB, throughput 0.98051
Reading from 29556: heap size 1493 MB, throughput 0.980406
Recommendation: one client; give it all the memory
Reading from 29556: heap size 1500 MB, throughput 0.980511
Reading from 29556: heap size 1504 MB, throughput 0.981085
Recommendation: one client; give it all the memory
Reading from 29556: heap size 1511 MB, throughput 0.97916
Reading from 29556: heap size 1513 MB, throughput 0.827905
Recommendation: one client; give it all the memory
Reading from 29556: heap size 1605 MB, throughput 0.996013
Reading from 29556: heap size 1606 MB, throughput 0.994451
Recommendation: one client; give it all the memory
Reading from 29556: heap size 1619 MB, throughput 0.993533
Reading from 29556: heap size 1625 MB, throughput 0.992196
Recommendation: one client; give it all the memory
Reading from 29556: heap size 1625 MB, throughput 0.990915
Reading from 29556: heap size 1462 MB, throughput 0.989993
Recommendation: one client; give it all the memory
Reading from 29556: heap size 1610 MB, throughput 0.989704
Reading from 29556: heap size 1488 MB, throughput 0.987991
Recommendation: one client; give it all the memory
Reading from 29556: heap size 1592 MB, throughput 0.986454
Recommendation: one client; give it all the memory
Reading from 29556: heap size 1602 MB, throughput 0.988754
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 29556: heap size 1569 MB, throughput 0.991636
Reading from 29556: heap size 1602 MB, throughput 0.897167
Reading from 29556: heap size 1616 MB, throughput 0.78154
Reading from 29556: heap size 1636 MB, throughput 0.683652
Reading from 29556: heap size 1675 MB, throughput 0.735087
Reading from 29556: heap size 1704 MB, throughput 0.715019
Reading from 29556: heap size 1749 MB, throughput 0.748636
Client 29556 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
