((nil
  (flycheck-clang-language-standard . "c++11")
  (flycheck-clang-include-path . ("../common"))
  (linuxconfig-compile-command . "./build.sh")
  (linuxconfig-compile-clean-command . "./build.sh clean")
  (linuxconfig-compile-target-command . "./build.sh")))
