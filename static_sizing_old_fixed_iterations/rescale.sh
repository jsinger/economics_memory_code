#!/bin/bash
# Plot graphs of existing data with a different scale

function usage()
{
    echo "Usage: ${0} dir N"
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ -z "${1}" ]; then
    usage
    exit 1
fi

if [ ! -d "${1}" ]; then
    usage
    exit 1
fi

cd "${1}"

if [ -z "${2}" ]; then
    usage
    exit 1
fi

N_FOR_NOVER="${2}"

BENCHMARKS_FILE='benchmarks.txt'

while read line; do
    BENCHMARK="${line}"

    AVG_TIMES_FILE="${BENCHMARK}_avgTimes.txt"
    THROUGHPUT_FILE="${BENCHMARK}_throughput.txt"

    python "${DIR}/nOver.py" "${N_FOR_NOVER}" < "${AVG_TIMES_FILE}" > "${THROUGHPUT_FILE}"

done < "${BENCHMARKS_FILE}"


"${DIR}/plot.sh" "${1}"
