economemd
    total memory: 6095 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub9_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 25352: heap size 9 MB, throughput 0.991573
Clients: 1
Client 25352 has a minimum heap size of 8 MB
Reading from 25351: heap size 9 MB, throughput 0.989403
Clients: 2
Client 25351 has a minimum heap size of 1211 MB
Reading from 25352: heap size 9 MB, throughput 0.983436
Reading from 25351: heap size 9 MB, throughput 0.977652
Reading from 25352: heap size 9 MB, throughput 0.958438
Reading from 25351: heap size 9 MB, throughput 0.958302
Reading from 25352: heap size 9 MB, throughput 0.910709
Reading from 25351: heap size 9 MB, throughput 0.923276
Reading from 25352: heap size 11 MB, throughput 0.942883
Reading from 25352: heap size 11 MB, throughput 0.975193
Reading from 25351: heap size 11 MB, throughput 0.976693
Reading from 25352: heap size 16 MB, throughput 0.977511
Reading from 25352: heap size 16 MB, throughput 0.983602
Reading from 25351: heap size 11 MB, throughput 0.971701
Reading from 25352: heap size 23 MB, throughput 0.9579
Reading from 25351: heap size 17 MB, throughput 0.939117
Reading from 25352: heap size 24 MB, throughput 0.589213
Reading from 25352: heap size 36 MB, throughput 0.972645
Reading from 25352: heap size 36 MB, throughput 0.976889
Reading from 25351: heap size 17 MB, throughput 0.737512
Reading from 25352: heap size 41 MB, throughput 0.981439
Reading from 25351: heap size 29 MB, throughput 0.875942
Reading from 25352: heap size 41 MB, throughput 0.97798
Reading from 25351: heap size 31 MB, throughput 0.885144
Reading from 25352: heap size 47 MB, throughput 0.988455
Reading from 25352: heap size 47 MB, throughput 0.974509
Reading from 25352: heap size 53 MB, throughput 0.986219
Reading from 25351: heap size 36 MB, throughput 0.332984
Reading from 25352: heap size 53 MB, throughput 0.971173
Reading from 25351: heap size 47 MB, throughput 0.78495
Reading from 25352: heap size 59 MB, throughput 0.981658
Reading from 25352: heap size 59 MB, throughput 0.98049
Reading from 25351: heap size 50 MB, throughput 0.491526
Reading from 25352: heap size 64 MB, throughput 0.980274
Reading from 25351: heap size 65 MB, throughput 0.844693
Reading from 25352: heap size 64 MB, throughput 0.978152
Reading from 25351: heap size 71 MB, throughput 0.851773
Reading from 25352: heap size 71 MB, throughput 0.990314
Reading from 25352: heap size 71 MB, throughput 0.984198
Reading from 25352: heap size 77 MB, throughput 0.989739
Reading from 25351: heap size 71 MB, throughput 0.21983
Reading from 25352: heap size 78 MB, throughput 0.904129
Reading from 25351: heap size 100 MB, throughput 0.714489
Reading from 25352: heap size 83 MB, throughput 0.993449
Reading from 25352: heap size 83 MB, throughput 0.98898
Reading from 25351: heap size 101 MB, throughput 0.276133
Reading from 25352: heap size 90 MB, throughput 0.989423
Reading from 25351: heap size 132 MB, throughput 0.814033
Reading from 25352: heap size 91 MB, throughput 0.991086
Reading from 25351: heap size 132 MB, throughput 0.806499
Reading from 25352: heap size 98 MB, throughput 0.993842
Reading from 25352: heap size 98 MB, throughput 0.993509
Reading from 25351: heap size 133 MB, throughput 0.172227
Reading from 25352: heap size 103 MB, throughput 0.996169
Reading from 25351: heap size 167 MB, throughput 0.696518
Reading from 25351: heap size 173 MB, throughput 0.817086
Reading from 25352: heap size 104 MB, throughput 0.993561
Reading from 25351: heap size 174 MB, throughput 0.652226
Reading from 25352: heap size 107 MB, throughput 0.9933
Reading from 25352: heap size 108 MB, throughput 0.993102
Reading from 25351: heap size 176 MB, throughput 0.174479
Reading from 25352: heap size 112 MB, throughput 0.996391
Reading from 25351: heap size 220 MB, throughput 0.729179
Reading from 25351: heap size 224 MB, throughput 0.762892
Reading from 25352: heap size 112 MB, throughput 0.99341
Reading from 25351: heap size 226 MB, throughput 0.701903
Reading from 25352: heap size 115 MB, throughput 0.994447
Reading from 25351: heap size 229 MB, throughput 0.678107
Reading from 25352: heap size 115 MB, throughput 0.993104
Reading from 25351: heap size 236 MB, throughput 0.730404
Reading from 25352: heap size 119 MB, throughput 0.99367
Reading from 25351: heap size 240 MB, throughput 0.667701
Reading from 25352: heap size 119 MB, throughput 0.990738
Reading from 25351: heap size 247 MB, throughput 0.551647
Reading from 25352: heap size 121 MB, throughput 0.99523
Reading from 25351: heap size 251 MB, throughput 0.435023
Reading from 25352: heap size 121 MB, throughput 0.993778
Reading from 25351: heap size 262 MB, throughput 0.544456
Reading from 25352: heap size 124 MB, throughput 0.993079
Reading from 25352: heap size 124 MB, throughput 0.993211
Reading from 25352: heap size 128 MB, throughput 0.993315
Reading from 25351: heap size 271 MB, throughput 0.100214
Reading from 25352: heap size 128 MB, throughput 0.99459
Reading from 25351: heap size 322 MB, throughput 0.52237
Reading from 25352: heap size 131 MB, throughput 0.993135
Reading from 25352: heap size 131 MB, throughput 0.993956
Reading from 25351: heap size 330 MB, throughput 0.0972611
Reading from 25352: heap size 135 MB, throughput 0.995804
Reading from 25351: heap size 378 MB, throughput 0.533814
Reading from 25351: heap size 370 MB, throughput 0.700369
Reading from 25352: heap size 135 MB, throughput 0.995919
Reading from 25351: heap size 376 MB, throughput 0.666071
Reading from 25352: heap size 138 MB, throughput 0.992458
Reading from 25351: heap size 377 MB, throughput 0.67778
Reading from 25351: heap size 377 MB, throughput 0.534855
Reading from 25352: heap size 138 MB, throughput 0.995886
Reading from 25351: heap size 379 MB, throughput 0.567274
Reading from 25351: heap size 386 MB, throughput 0.553084
Reading from 25352: heap size 141 MB, throughput 0.996496
Reading from 25351: heap size 389 MB, throughput 0.51547
Reading from 25352: heap size 141 MB, throughput 0.995696
Reading from 25351: heap size 400 MB, throughput 0.509232
Reading from 25351: heap size 405 MB, throughput 0.449057
Reading from 25352: heap size 143 MB, throughput 0.996478
Reading from 25352: heap size 144 MB, throughput 0.993585
Reading from 25352: heap size 146 MB, throughput 0.993651
Reading from 25352: heap size 146 MB, throughput 0.99262
Reading from 25351: heap size 414 MB, throughput 0.0842989
Reading from 25352: heap size 149 MB, throughput 0.991991
Reading from 25351: heap size 468 MB, throughput 0.392592
Reading from 25352: heap size 149 MB, throughput 0.993502
Reading from 25351: heap size 404 MB, throughput 0.46561
Reading from 25352: heap size 152 MB, throughput 0.9961
Reading from 25352: heap size 152 MB, throughput 0.994845
Equal recommendation: 3047 MB each
Reading from 25352: heap size 156 MB, throughput 0.99792
Reading from 25351: heap size 467 MB, throughput 0.083496
Reading from 25352: heap size 156 MB, throughput 0.995052
Reading from 25351: heap size 526 MB, throughput 0.36945
Reading from 25351: heap size 517 MB, throughput 0.516605
Reading from 25352: heap size 159 MB, throughput 0.996086
Reading from 25351: heap size 454 MB, throughput 0.572011
Reading from 25351: heap size 511 MB, throughput 0.502602
Reading from 25352: heap size 159 MB, throughput 0.996575
Reading from 25352: heap size 161 MB, throughput 0.994825
Reading from 25352: heap size 162 MB, throughput 0.993859
Reading from 25351: heap size 516 MB, throughput 0.09548
Reading from 25352: heap size 164 MB, throughput 0.996554
Reading from 25351: heap size 567 MB, throughput 0.465683
Reading from 25352: heap size 164 MB, throughput 0.994875
Reading from 25351: heap size 572 MB, throughput 0.568062
Reading from 25352: heap size 168 MB, throughput 0.995077
Reading from 25351: heap size 572 MB, throughput 0.541003
Reading from 25351: heap size 573 MB, throughput 0.604999
Reading from 25352: heap size 168 MB, throughput 0.994243
Reading from 25351: heap size 575 MB, throughput 0.529662
Reading from 25352: heap size 171 MB, throughput 0.995929
Reading from 25351: heap size 584 MB, throughput 0.514648
Reading from 25352: heap size 171 MB, throughput 0.993791
Reading from 25351: heap size 590 MB, throughput 0.543384
Reading from 25351: heap size 601 MB, throughput 0.518255
Reading from 25352: heap size 174 MB, throughput 0.99693
Reading from 25351: heap size 607 MB, throughput 0.429324
Reading from 25352: heap size 174 MB, throughput 0.993138
Reading from 25351: heap size 618 MB, throughput 0.475061
Reading from 25352: heap size 177 MB, throughput 0.992219
Reading from 25351: heap size 623 MB, throughput 0.800438
Reading from 25352: heap size 177 MB, throughput 0.995318
Reading from 25352: heap size 180 MB, throughput 0.991727
Reading from 25351: heap size 634 MB, throughput 0.768742
Reading from 25352: heap size 181 MB, throughput 0.995861
Reading from 25352: heap size 184 MB, throughput 0.995591
Reading from 25352: heap size 184 MB, throughput 0.993216
Reading from 25352: heap size 188 MB, throughput 0.993731
Reading from 25352: heap size 188 MB, throughput 0.984085
Reading from 25351: heap size 637 MB, throughput 0.156051
Reading from 25352: heap size 192 MB, throughput 0.996624
Reading from 25352: heap size 192 MB, throughput 0.993585
Reading from 25351: heap size 708 MB, throughput 0.628191
Reading from 25352: heap size 197 MB, throughput 0.998248
Reading from 25351: heap size 714 MB, throughput 0.782148
Reading from 25352: heap size 197 MB, throughput 0.996331
Reading from 25351: heap size 722 MB, throughput 0.484185
Reading from 25352: heap size 201 MB, throughput 0.997289
Reading from 25351: heap size 730 MB, throughput 0.542688
Reading from 25352: heap size 201 MB, throughput 0.99746
Reading from 25352: heap size 203 MB, throughput 0.997445
Reading from 25352: heap size 204 MB, throughput 0.995687
Reading from 25352: heap size 207 MB, throughput 0.995971
Reading from 25351: heap size 644 MB, throughput 0.0149838
Equal recommendation: 3047 MB each
Reading from 25352: heap size 207 MB, throughput 0.9931
Reading from 25351: heap size 782 MB, throughput 0.474726
Reading from 25351: heap size 791 MB, throughput 0.269444
Reading from 25352: heap size 210 MB, throughput 0.993791
Reading from 25351: heap size 784 MB, throughput 0.309648
Reading from 25352: heap size 210 MB, throughput 0.994793
Reading from 25352: heap size 214 MB, throughput 0.997985
Reading from 25351: heap size 788 MB, throughput 0.0335556
Reading from 25351: heap size 872 MB, throughput 0.200165
Reading from 25351: heap size 872 MB, throughput 0.454912
Reading from 25352: heap size 214 MB, throughput 0.993872
Reading from 25351: heap size 878 MB, throughput 0.503412
Reading from 25352: heap size 217 MB, throughput 0.997268
Reading from 25351: heap size 879 MB, throughput 0.883375
Reading from 25351: heap size 881 MB, throughput 0.579721
Reading from 25351: heap size 883 MB, throughput 0.584899
Reading from 25352: heap size 217 MB, throughput 0.997496
Reading from 25351: heap size 876 MB, throughput 0.597012
Reading from 25351: heap size 880 MB, throughput 0.612167
Reading from 25352: heap size 221 MB, throughput 0.997329
Reading from 25351: heap size 876 MB, throughput 0.903583
Reading from 25351: heap size 879 MB, throughput 0.803097
Reading from 25352: heap size 221 MB, throughput 0.997718
Reading from 25351: heap size 869 MB, throughput 0.886632
Reading from 25351: heap size 701 MB, throughput 0.739605
Reading from 25352: heap size 224 MB, throughput 0.996776
Reading from 25352: heap size 225 MB, throughput 0.996301
Reading from 25351: heap size 860 MB, throughput 0.933608
Reading from 25351: heap size 706 MB, throughput 0.807604
Reading from 25352: heap size 228 MB, throughput 0.997007
Reading from 25351: heap size 848 MB, throughput 0.894708
Reading from 25352: heap size 228 MB, throughput 0.996517
Reading from 25351: heap size 721 MB, throughput 0.850065
Reading from 25351: heap size 842 MB, throughput 0.723816
Reading from 25352: heap size 231 MB, throughput 0.996972
Reading from 25351: heap size 847 MB, throughput 0.73739
Reading from 25352: heap size 231 MB, throughput 0.994806
Reading from 25352: heap size 234 MB, throughput 0.9978
Reading from 25352: heap size 234 MB, throughput 0.997533
Reading from 25352: heap size 237 MB, throughput 0.993847
Reading from 25352: heap size 237 MB, throughput 0.996162
Reading from 25351: heap size 838 MB, throughput 0.068841
Reading from 25352: heap size 241 MB, throughput 0.99471
Reading from 25351: heap size 833 MB, throughput 0.508529
Reading from 25351: heap size 908 MB, throughput 0.671727
Reading from 25352: heap size 241 MB, throughput 0.996366
Reading from 25351: heap size 915 MB, throughput 0.716178
Reading from 25351: heap size 905 MB, throughput 0.670316
Reading from 25352: heap size 245 MB, throughput 0.996955
Reading from 25351: heap size 910 MB, throughput 0.646315
Reading from 25352: heap size 245 MB, throughput 0.996396
Reading from 25351: heap size 904 MB, throughput 0.704634
Reading from 25351: heap size 908 MB, throughput 0.678382
Reading from 25352: heap size 248 MB, throughput 0.996985
Reading from 25351: heap size 903 MB, throughput 0.714583
Reading from 25352: heap size 248 MB, throughput 0.997412
Reading from 25352: heap size 252 MB, throughput 0.998321
Equal recommendation: 3047 MB each
Reading from 25352: heap size 252 MB, throughput 0.997759
Reading from 25352: heap size 255 MB, throughput 0.995482
Reading from 25351: heap size 908 MB, throughput 0.956084
Reading from 25352: heap size 255 MB, throughput 0.997021
Reading from 25351: heap size 903 MB, throughput 0.800827
Reading from 25352: heap size 257 MB, throughput 0.994094
Reading from 25351: heap size 913 MB, throughput 0.65975
Reading from 25351: heap size 930 MB, throughput 0.654307
Reading from 25352: heap size 258 MB, throughput 0.994909
Reading from 25351: heap size 938 MB, throughput 0.757349
Reading from 25351: heap size 940 MB, throughput 0.718017
Reading from 25351: heap size 943 MB, throughput 0.714768
Reading from 25352: heap size 262 MB, throughput 0.997667
Reading from 25351: heap size 946 MB, throughput 0.745263
Reading from 25351: heap size 948 MB, throughput 0.751577
Reading from 25352: heap size 262 MB, throughput 0.997596
Reading from 25351: heap size 950 MB, throughput 0.785263
Reading from 25352: heap size 265 MB, throughput 0.997113
Reading from 25351: heap size 953 MB, throughput 0.859065
Reading from 25352: heap size 265 MB, throughput 0.997346
Reading from 25351: heap size 955 MB, throughput 0.853483
Reading from 25351: heap size 958 MB, throughput 0.847014
Reading from 25352: heap size 269 MB, throughput 0.996714
Reading from 25351: heap size 961 MB, throughput 0.682029
Reading from 25352: heap size 270 MB, throughput 0.993468
Reading from 25352: heap size 274 MB, throughput 0.995053
Reading from 25352: heap size 274 MB, throughput 0.997538
Reading from 25352: heap size 278 MB, throughput 0.995732
Reading from 25351: heap size 963 MB, throughput 0.0557211
Reading from 25352: heap size 279 MB, throughput 0.996606
Reading from 25351: heap size 1102 MB, throughput 0.413503
Reading from 25351: heap size 1102 MB, throughput 0.760022
Reading from 25352: heap size 282 MB, throughput 0.99761
Reading from 25351: heap size 1113 MB, throughput 0.642282
Reading from 25352: heap size 283 MB, throughput 0.995755
Reading from 25351: heap size 1114 MB, throughput 0.622598
Reading from 25351: heap size 1121 MB, throughput 0.631568
Reading from 25352: heap size 287 MB, throughput 0.996024
Reading from 25351: heap size 1124 MB, throughput 0.594771
Reading from 25352: heap size 287 MB, throughput 0.995436
Reading from 25351: heap size 1130 MB, throughput 0.635553
Reading from 25351: heap size 1134 MB, throughput 0.640126
Reading from 25352: heap size 291 MB, throughput 0.997046
Reading from 25352: heap size 291 MB, throughput 0.995212
Reading from 25351: heap size 1141 MB, throughput 0.892583
Reading from 25352: heap size 295 MB, throughput 0.997298
Equal recommendation: 3047 MB each
Reading from 25352: heap size 295 MB, throughput 0.99745
Reading from 25352: heap size 299 MB, throughput 0.99731
Reading from 25352: heap size 284 MB, throughput 0.971459
Reading from 25351: heap size 1146 MB, throughput 0.942916
Reading from 25352: heap size 287 MB, throughput 0.997626
Reading from 25352: heap size 295 MB, throughput 0.997632
Reading from 25352: heap size 296 MB, throughput 0.997835
Reading from 25352: heap size 296 MB, throughput 0.996783
Reading from 25352: heap size 301 MB, throughput 0.997322
Reading from 25351: heap size 1157 MB, throughput 0.924166
Reading from 25352: heap size 302 MB, throughput 0.997212
Reading from 25352: heap size 308 MB, throughput 0.996311
Reading from 25352: heap size 308 MB, throughput 0.995828
Reading from 25352: heap size 314 MB, throughput 0.994695
Reading from 25351: heap size 1160 MB, throughput 0.928143
Reading from 25352: heap size 315 MB, throughput 0.995599
Reading from 25352: heap size 324 MB, throughput 0.996674
Reading from 25352: heap size 324 MB, throughput 0.996356
Reading from 25352: heap size 333 MB, throughput 0.996244
Reading from 25351: heap size 1176 MB, throughput 0.927603
Reading from 25352: heap size 333 MB, throughput 0.992432
Reading from 25352: heap size 341 MB, throughput 0.995606
Reading from 25352: heap size 342 MB, throughput 0.997051
Reading from 25352: heap size 350 MB, throughput 0.996484
Reading from 25351: heap size 1180 MB, throughput 0.929485
Equal recommendation: 3047 MB each
Reading from 25352: heap size 351 MB, throughput 0.997052
Reading from 25352: heap size 360 MB, throughput 0.997448
Reading from 25352: heap size 361 MB, throughput 0.99685
Reading from 25352: heap size 370 MB, throughput 0.995466
Reading from 25351: heap size 1193 MB, throughput 0.940134
Reading from 25352: heap size 370 MB, throughput 0.997178
Reading from 25352: heap size 378 MB, throughput 0.998802
Reading from 25352: heap size 379 MB, throughput 0.998366
Reading from 25352: heap size 386 MB, throughput 0.996435
Reading from 25351: heap size 1197 MB, throughput 0.943807
Reading from 25352: heap size 386 MB, throughput 0.996369
Reading from 25352: heap size 394 MB, throughput 0.997163
Reading from 25352: heap size 394 MB, throughput 0.997327
Reading from 25352: heap size 403 MB, throughput 0.997829
Reading from 25351: heap size 1201 MB, throughput 0.94259
Reading from 25352: heap size 403 MB, throughput 0.996535
Reading from 25352: heap size 411 MB, throughput 0.996912
Reading from 25352: heap size 411 MB, throughput 0.996476
Reading from 25352: heap size 420 MB, throughput 0.991833
Reading from 25351: heap size 1203 MB, throughput 0.944375
Reading from 25352: heap size 420 MB, throughput 0.997361
Equal recommendation: 3047 MB each
Reading from 25352: heap size 431 MB, throughput 0.997511
Reading from 25352: heap size 432 MB, throughput 0.996878
Reading from 25351: heap size 1212 MB, throughput 0.944296
Reading from 25352: heap size 442 MB, throughput 0.996417
Reading from 25352: heap size 442 MB, throughput 0.997552
Reading from 25352: heap size 452 MB, throughput 0.997464
Reading from 25351: heap size 1215 MB, throughput 0.9516
Reading from 25352: heap size 453 MB, throughput 0.997161
Reading from 25352: heap size 463 MB, throughput 0.996839
Reading from 25352: heap size 463 MB, throughput 0.997702
Reading from 25352: heap size 471 MB, throughput 0.988829
Reading from 25351: heap size 1223 MB, throughput 0.939294
Reading from 25352: heap size 472 MB, throughput 0.997554
Reading from 25352: heap size 486 MB, throughput 0.996976
Reading from 25352: heap size 487 MB, throughput 0.997235
Reading from 25351: heap size 1228 MB, throughput 0.934582
Reading from 25352: heap size 500 MB, throughput 0.997776
Reading from 25352: heap size 500 MB, throughput 0.997794
Equal recommendation: 3047 MB each
Reading from 25352: heap size 510 MB, throughput 0.997964
Reading from 25351: heap size 1236 MB, throughput 0.938051
Reading from 25352: heap size 511 MB, throughput 0.99734
Reading from 25352: heap size 521 MB, throughput 0.997932
Reading from 25352: heap size 521 MB, throughput 0.998143
Reading from 25351: heap size 1242 MB, throughput 0.944026
Reading from 25352: heap size 530 MB, throughput 0.997186
Reading from 25352: heap size 531 MB, throughput 0.997744
Reading from 25352: heap size 541 MB, throughput 0.997537
Reading from 25351: heap size 1251 MB, throughput 0.948571
Reading from 25352: heap size 541 MB, throughput 0.997856
Reading from 25352: heap size 551 MB, throughput 0.997539
Reading from 25352: heap size 551 MB, throughput 0.997805
Reading from 25351: heap size 1258 MB, throughput 0.937254
Reading from 25352: heap size 561 MB, throughput 0.997768
Reading from 25352: heap size 561 MB, throughput 0.997998
Equal recommendation: 3047 MB each
Reading from 25352: heap size 570 MB, throughput 0.99756
Reading from 25352: heap size 570 MB, throughput 0.996468
Reading from 25351: heap size 1268 MB, throughput 0.519686
Reading from 25352: heap size 580 MB, throughput 0.998818
Reading from 25352: heap size 580 MB, throughput 0.997826
Reading from 25352: heap size 590 MB, throughput 0.998196
Reading from 25351: heap size 1346 MB, throughput 0.9869
Reading from 25352: heap size 590 MB, throughput 0.99823
Reading from 25352: heap size 600 MB, throughput 0.998497
Reading from 25352: heap size 600 MB, throughput 0.997683
Reading from 25351: heap size 1350 MB, throughput 0.983519
Reading from 25352: heap size 608 MB, throughput 0.998275
Reading from 25352: heap size 609 MB, throughput 0.998335
Reading from 25351: heap size 1359 MB, throughput 0.977383
Equal recommendation: 3047 MB each
Reading from 25352: heap size 617 MB, throughput 0.998634
Reading from 25352: heap size 617 MB, throughput 0.997682
Reading from 25352: heap size 626 MB, throughput 0.998502
Reading from 25351: heap size 1370 MB, throughput 0.971117
Reading from 25352: heap size 626 MB, throughput 0.997974
Reading from 25352: heap size 634 MB, throughput 0.998489
Reading from 25351: heap size 1370 MB, throughput 0.977479
Reading from 25352: heap size 634 MB, throughput 0.998576
Reading from 25352: heap size 643 MB, throughput 0.998299
Reading from 25352: heap size 643 MB, throughput 0.997706
Reading from 25351: heap size 1364 MB, throughput 0.966865
Reading from 25352: heap size 651 MB, throughput 0.998814
Reading from 25352: heap size 651 MB, throughput 0.998362
Equal recommendation: 3047 MB each
Reading from 25352: heap size 660 MB, throughput 0.997745
Reading from 25351: heap size 1260 MB, throughput 0.96293
Reading from 25352: heap size 660 MB, throughput 0.998195
Reading from 25352: heap size 669 MB, throughput 0.998548
Reading from 25351: heap size 1353 MB, throughput 0.958724
Reading from 25352: heap size 669 MB, throughput 0.998257
Reading from 25352: heap size 678 MB, throughput 0.998577
Reading from 25352: heap size 678 MB, throughput 0.998471
Reading from 25351: heap size 1361 MB, throughput 0.957986
Reading from 25352: heap size 687 MB, throughput 0.99862
Reading from 25352: heap size 687 MB, throughput 0.998463
Reading from 25351: heap size 1358 MB, throughput 0.956082
Reading from 25352: heap size 695 MB, throughput 0.998719
Reading from 25352: heap size 695 MB, throughput 0.998387
Equal recommendation: 3047 MB each
Reading from 25352: heap size 703 MB, throughput 0.998392
Reading from 25351: heap size 1360 MB, throughput 0.950099
Reading from 25352: heap size 703 MB, throughput 0.998557
Reading from 25352: heap size 712 MB, throughput 0.998384
Reading from 25351: heap size 1366 MB, throughput 0.949025
Reading from 25352: heap size 712 MB, throughput 0.998654
Reading from 25352: heap size 720 MB, throughput 0.998758
Reading from 25352: heap size 720 MB, throughput 0.998276
Reading from 25351: heap size 1370 MB, throughput 0.9482
Reading from 25352: heap size 728 MB, throughput 0.998702
Reading from 25352: heap size 728 MB, throughput 0.99881
Reading from 25351: heap size 1377 MB, throughput 0.941174
Reading from 25352: heap size 736 MB, throughput 0.998413
Equal recommendation: 3047 MB each
Reading from 25352: heap size 737 MB, throughput 0.998724
Reading from 25352: heap size 745 MB, throughput 0.998309
Reading from 25351: heap size 1385 MB, throughput 0.943976
Reading from 25352: heap size 745 MB, throughput 0.998402
Reading from 25352: heap size 753 MB, throughput 0.998634
Reading from 25352: heap size 753 MB, throughput 0.996957
Reading from 25351: heap size 1394 MB, throughput 0.949135
Reading from 25352: heap size 762 MB, throughput 0.998966
Reading from 25352: heap size 762 MB, throughput 0.998723
Reading from 25351: heap size 1404 MB, throughput 0.937927
Reading from 25352: heap size 771 MB, throughput 0.998657
Equal recommendation: 3047 MB each
Reading from 25352: heap size 772 MB, throughput 0.998584
Reading from 25351: heap size 1417 MB, throughput 0.943263
Reading from 25352: heap size 780 MB, throughput 0.998703
Reading from 25352: heap size 780 MB, throughput 0.998638
Reading from 25352: heap size 789 MB, throughput 0.998662
Reading from 25351: heap size 1423 MB, throughput 0.937343
Reading from 25352: heap size 789 MB, throughput 0.998314
Reading from 25352: heap size 798 MB, throughput 0.998388
Reading from 25352: heap size 799 MB, throughput 0.998766
Reading from 25352: heap size 808 MB, throughput 0.998787
Reading from 25352: heap size 808 MB, throughput 0.998803
Equal recommendation: 3047 MB each
Reading from 25352: heap size 816 MB, throughput 0.998389
Reading from 25352: heap size 817 MB, throughput 0.99866
Reading from 25352: heap size 825 MB, throughput 0.998758
Reading from 25352: heap size 825 MB, throughput 0.998578
Reading from 25352: heap size 834 MB, throughput 0.998846
Reading from 25352: heap size 834 MB, throughput 0.998552
Reading from 25352: heap size 843 MB, throughput 0.998724
Equal recommendation: 3047 MB each
Reading from 25352: heap size 843 MB, throughput 0.998676
Reading from 25352: heap size 852 MB, throughput 0.998712
Reading from 25351: heap size 1436 MB, throughput 0.987228
Reading from 25352: heap size 852 MB, throughput 0.998764
Reading from 25352: heap size 861 MB, throughput 0.998779
Reading from 25352: heap size 861 MB, throughput 0.998695
Reading from 25352: heap size 870 MB, throughput 0.999024
Reading from 25352: heap size 870 MB, throughput 0.998667
Reading from 25352: heap size 879 MB, throughput 0.998715
Equal recommendation: 3047 MB each
Reading from 25352: heap size 879 MB, throughput 0.998486
Reading from 25352: heap size 888 MB, throughput 0.999105
Reading from 25352: heap size 888 MB, throughput 0.998663
Reading from 25352: heap size 896 MB, throughput 0.998898
Reading from 25352: heap size 896 MB, throughput 0.999029
Reading from 25352: heap size 904 MB, throughput 0.998097
Reading from 25351: heap size 1440 MB, throughput 0.912174
Reading from 25351: heap size 1544 MB, throughput 0.990695
Reading from 25352: heap size 905 MB, throughput 0.998753
Reading from 25351: heap size 1574 MB, throughput 0.898461
Equal recommendation: 3047 MB each
Reading from 25351: heap size 1577 MB, throughput 0.87413
Reading from 25351: heap size 1583 MB, throughput 0.86779
Reading from 25352: heap size 914 MB, throughput 0.99858
Reading from 25351: heap size 1575 MB, throughput 0.844913
Reading from 25351: heap size 1582 MB, throughput 0.878981
Reading from 25352: heap size 914 MB, throughput 0.999017
Reading from 25351: heap size 1578 MB, throughput 0.974229
Reading from 25352: heap size 923 MB, throughput 0.998842
Reading from 25352: heap size 924 MB, throughput 0.999107
Reading from 25351: heap size 1587 MB, throughput 0.985246
Reading from 25352: heap size 932 MB, throughput 0.998891
Reading from 25352: heap size 932 MB, throughput 0.998851
Reading from 25351: heap size 1608 MB, throughput 0.98106
Reading from 25352: heap size 940 MB, throughput 0.998925
Equal recommendation: 3047 MB each
Reading from 25352: heap size 940 MB, throughput 0.998912
Reading from 25351: heap size 1611 MB, throughput 0.977249
Reading from 25352: heap size 949 MB, throughput 0.998912
Reading from 25352: heap size 949 MB, throughput 0.998718
Reading from 25352: heap size 957 MB, throughput 0.993767
Reading from 25351: heap size 1605 MB, throughput 0.981853
Reading from 25352: heap size 957 MB, throughput 0.998687
Reading from 25352: heap size 973 MB, throughput 0.999019
Reading from 25351: heap size 1614 MB, throughput 0.970237
Reading from 25352: heap size 975 MB, throughput 0.998794
Equal recommendation: 3047 MB each
Reading from 25352: heap size 988 MB, throughput 0.999074
Reading from 25351: heap size 1593 MB, throughput 0.96718
Reading from 25352: heap size 988 MB, throughput 0.998905
Reading from 25352: heap size 999 MB, throughput 0.999232
Reading from 25352: heap size 1000 MB, throughput 0.998822
Reading from 25351: heap size 1605 MB, throughput 0.964355
Reading from 25352: heap size 1009 MB, throughput 0.999032
Reading from 25352: heap size 1009 MB, throughput 0.999033
Reading from 25351: heap size 1591 MB, throughput 0.961259
Equal recommendation: 3047 MB each
Reading from 25352: heap size 1018 MB, throughput 0.999118
Reading from 25352: heap size 1018 MB, throughput 0.998774
Reading from 25351: heap size 1598 MB, throughput 0.958797
Reading from 25352: heap size 1026 MB, throughput 0.999165
Reading from 25352: heap size 1026 MB, throughput 0.99913
Reading from 25351: heap size 1609 MB, throughput 0.967937
Reading from 25352: heap size 1035 MB, throughput 0.999099
Reading from 25352: heap size 1035 MB, throughput 0.998877
Equal recommendation: 3047 MB each
Reading from 25352: heap size 1043 MB, throughput 0.999294
Reading from 25351: heap size 1610 MB, throughput 0.964465
Reading from 25352: heap size 1043 MB, throughput 0.999066
Reading from 25352: heap size 1051 MB, throughput 0.99923
Reading from 25351: heap size 1620 MB, throughput 0.954394
Reading from 25352: heap size 1051 MB, throughput 0.998949
Reading from 25352: heap size 1058 MB, throughput 0.999011
Reading from 25352: heap size 1058 MB, throughput 0.998802
Reading from 25351: heap size 1628 MB, throughput 0.955536
Equal recommendation: 3047 MB each
Reading from 25352: heap size 1066 MB, throughput 0.999104
Reading from 25352: heap size 1066 MB, throughput 0.99895
Reading from 25351: heap size 1640 MB, throughput 0.953819
Reading from 25352: heap size 1075 MB, throughput 0.999031
Reading from 25352: heap size 1075 MB, throughput 0.999008
Reading from 25351: heap size 1652 MB, throughput 0.94303
Reading from 25352: heap size 1083 MB, throughput 0.999032
Reading from 25352: heap size 1083 MB, throughput 0.99904
Equal recommendation: 3047 MB each
Reading from 25352: heap size 1092 MB, throughput 0.999091
Reading from 25351: heap size 1668 MB, throughput 0.955355
Reading from 25352: heap size 1092 MB, throughput 0.999139
Reading from 25352: heap size 1100 MB, throughput 0.999105
Reading from 25351: heap size 1681 MB, throughput 0.944724
Reading from 25352: heap size 1100 MB, throughput 0.999106
Reading from 25352: heap size 1108 MB, throughput 0.998923
Reading from 25351: heap size 1700 MB, throughput 0.944308
Reading from 25352: heap size 1109 MB, throughput 0.999213
Equal recommendation: 3047 MB each
Reading from 25352: heap size 1117 MB, throughput 0.998804
Reading from 25351: heap size 1708 MB, throughput 0.948256
Reading from 25352: heap size 1117 MB, throughput 0.999189
Reading from 25352: heap size 1125 MB, throughput 0.999055
Reading from 25352: heap size 1126 MB, throughput 0.999104
Reading from 25351: heap size 1727 MB, throughput 0.951264
Reading from 25352: heap size 1134 MB, throughput 0.999191
Reading from 25352: heap size 1134 MB, throughput 0.999094
Equal recommendation: 3047 MB each
Reading from 25351: heap size 1733 MB, throughput 0.939737
Reading from 25352: heap size 1142 MB, throughput 0.998999
Reading from 25352: heap size 1142 MB, throughput 0.999044
Reading from 25352: heap size 1151 MB, throughput 0.998905
Reading from 25351: heap size 1752 MB, throughput 0.95501
Reading from 25352: heap size 1151 MB, throughput 0.999108
Reading from 25352: heap size 1160 MB, throughput 0.999233
Reading from 25351: heap size 1755 MB, throughput 0.948784
Equal recommendation: 3047 MB each
Reading from 25352: heap size 1160 MB, throughput 0.998854
Reading from 25352: heap size 1169 MB, throughput 0.999069
Reading from 25352: heap size 1169 MB, throughput 0.998974
Reading from 25351: heap size 1775 MB, throughput 0.708678
Reading from 25352: heap size 1178 MB, throughput 0.99933
Reading from 25352: heap size 1178 MB, throughput 0.99914
Equal recommendation: 3047 MB each
Reading from 25351: heap size 1874 MB, throughput 0.98827
Reading from 25352: heap size 1186 MB, throughput 0.999032
Reading from 25352: heap size 1186 MB, throughput 0.999286
Reading from 25352: heap size 1194 MB, throughput 0.999001
Reading from 25352: heap size 1195 MB, throughput 0.999171
Reading from 25352: heap size 1203 MB, throughput 0.999115
Reading from 25352: heap size 1203 MB, throughput 0.99906
Equal recommendation: 3047 MB each
Reading from 25352: heap size 1211 MB, throughput 0.999164
Reading from 25352: heap size 1211 MB, throughput 0.998869
Reading from 25352: heap size 1220 MB, throughput 0.999188
Reading from 25352: heap size 1220 MB, throughput 0.99917
Reading from 25352: heap size 1229 MB, throughput 0.999126
Equal recommendation: 3047 MB each
Reading from 25352: heap size 1229 MB, throughput 0.999241
Reading from 25352: heap size 1237 MB, throughput 0.999136
Reading from 25352: heap size 1238 MB, throughput 0.999233
Reading from 25352: heap size 1246 MB, throughput 0.999177
Reading from 25352: heap size 1246 MB, throughput 0.999112
Equal recommendation: 3047 MB each
Reading from 25352: heap size 1254 MB, throughput 0.999223
Reading from 25351: heap size 1883 MB, throughput 0.993793
Reading from 25352: heap size 1254 MB, throughput 0.999135
Reading from 25352: heap size 1263 MB, throughput 0.999159
Reading from 25351: heap size 1895 MB, throughput 0.971259
Reading from 25351: heap size 1898 MB, throughput 0.788893
Reading from 25351: heap size 1902 MB, throughput 0.761344
Reading from 25352: heap size 1263 MB, throughput 0.999131
Reading from 25351: heap size 1938 MB, throughput 0.773931
Client 25351 died
Clients: 1
Reading from 25352: heap size 1271 MB, throughput 0.999116
Recommendation: one client; give it all the memory
Reading from 25352: heap size 1271 MB, throughput 0.998927
Reading from 25352: heap size 1280 MB, throughput 0.99893
Client 25352 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
