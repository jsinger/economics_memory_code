economemd
    total memory: 4892 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub16_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
ReadingThread: starting
TimerThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 29288: heap size 9 MB, throughput 0.983461
Clients: 1
Client 29288 has a minimum heap size of 12 MB
Reading from 29287: heap size 9 MB, throughput 0.991354
Clients: 2
Client 29287 has a minimum heap size of 1211 MB
Reading from 29287: heap size 9 MB, throughput 0.978081
Reading from 29288: heap size 9 MB, throughput 0.987363
Reading from 29287: heap size 9 MB, throughput 0.968283
Reading from 29287: heap size 9 MB, throughput 0.954122
Reading from 29288: heap size 11 MB, throughput 0.955794
Reading from 29287: heap size 11 MB, throughput 0.92702
Reading from 29288: heap size 11 MB, throughput 0.982291
Reading from 29287: heap size 11 MB, throughput 0.90126
Reading from 29287: heap size 17 MB, throughput 0.960626
Reading from 29288: heap size 15 MB, throughput 0.986389
Reading from 29287: heap size 17 MB, throughput 0.643639
Reading from 29288: heap size 15 MB, throughput 0.976402
Reading from 29287: heap size 30 MB, throughput 0.942235
Reading from 29287: heap size 31 MB, throughput 0.863829
Reading from 29287: heap size 35 MB, throughput 0.315951
Reading from 29288: heap size 24 MB, throughput 0.932536
Reading from 29287: heap size 47 MB, throughput 0.855444
Reading from 29287: heap size 48 MB, throughput 0.930929
Reading from 29287: heap size 51 MB, throughput 0.273986
Reading from 29288: heap size 30 MB, throughput 0.991756
Reading from 29287: heap size 73 MB, throughput 0.768591
Reading from 29287: heap size 74 MB, throughput 0.343654
Reading from 29288: heap size 37 MB, throughput 0.984681
Reading from 29287: heap size 100 MB, throughput 0.76098
Reading from 29287: heap size 100 MB, throughput 0.718038
Reading from 29287: heap size 102 MB, throughput 0.733772
Reading from 29288: heap size 44 MB, throughput 0.968324
Reading from 29288: heap size 45 MB, throughput 0.936892
Reading from 29287: heap size 105 MB, throughput 0.150364
Reading from 29288: heap size 46 MB, throughput 0.97711
Reading from 29287: heap size 141 MB, throughput 0.717165
Reading from 29287: heap size 142 MB, throughput 0.722297
Reading from 29287: heap size 144 MB, throughput 0.715186
Reading from 29288: heap size 56 MB, throughput 0.987745
Reading from 29287: heap size 149 MB, throughput 0.717737
Reading from 29288: heap size 56 MB, throughput 0.989398
Reading from 29288: heap size 67 MB, throughput 0.989108
Reading from 29288: heap size 68 MB, throughput 0.867112
Reading from 29287: heap size 152 MB, throughput 0.148483
Reading from 29287: heap size 189 MB, throughput 0.611067
Reading from 29288: heap size 84 MB, throughput 0.988476
Reading from 29287: heap size 198 MB, throughput 0.794589
Reading from 29287: heap size 199 MB, throughput 0.630276
Reading from 29287: heap size 202 MB, throughput 0.620956
Reading from 29288: heap size 85 MB, throughput 0.991817
Reading from 29287: heap size 208 MB, throughput 0.152794
Reading from 29287: heap size 255 MB, throughput 0.768647
Reading from 29287: heap size 261 MB, throughput 0.712811
Reading from 29288: heap size 95 MB, throughput 0.995836
Reading from 29287: heap size 264 MB, throughput 0.777867
Reading from 29287: heap size 269 MB, throughput 0.662904
Reading from 29287: heap size 271 MB, throughput 0.600616
Reading from 29287: heap size 281 MB, throughput 0.692829
Reading from 29287: heap size 284 MB, throughput 0.571709
Reading from 29287: heap size 296 MB, throughput 0.523663
Reading from 29288: heap size 96 MB, throughput 0.995098
Reading from 29287: heap size 300 MB, throughput 0.372294
Reading from 29287: heap size 311 MB, throughput 0.428948
Reading from 29287: heap size 320 MB, throughput 0.0694147
Reading from 29288: heap size 105 MB, throughput 0.996321
Reading from 29287: heap size 371 MB, throughput 0.354713
Reading from 29288: heap size 106 MB, throughput 0.99486
Reading from 29287: heap size 359 MB, throughput 0.0612336
Reading from 29287: heap size 334 MB, throughput 0.41418
Reading from 29287: heap size 416 MB, throughput 0.618961
Reading from 29287: heap size 325 MB, throughput 0.625922
Reading from 29287: heap size 409 MB, throughput 0.607643
Reading from 29287: heap size 340 MB, throughput 0.609921
Reading from 29287: heap size 400 MB, throughput 0.584881
Reading from 29287: heap size 405 MB, throughput 0.625153
Reading from 29287: heap size 405 MB, throughput 0.612198
Reading from 29288: heap size 114 MB, throughput 0.996935
Reading from 29287: heap size 406 MB, throughput 0.580049
Reading from 29287: heap size 409 MB, throughput 0.491379
Reading from 29287: heap size 411 MB, throughput 0.576419
Reading from 29287: heap size 415 MB, throughput 0.555867
Reading from 29287: heap size 418 MB, throughput 0.574395
Reading from 29287: heap size 421 MB, throughput 0.555028
Reading from 29288: heap size 115 MB, throughput 0.996267
Reading from 29287: heap size 426 MB, throughput 0.564044
Equal recommendation: 2446 MB each
Reading from 29288: heap size 122 MB, throughput 0.994849
Reading from 29287: heap size 428 MB, throughput 0.0642735
Reading from 29287: heap size 484 MB, throughput 0.36726
Reading from 29287: heap size 492 MB, throughput 0.509899
Reading from 29287: heap size 493 MB, throughput 0.54313
Reading from 29287: heap size 496 MB, throughput 0.460888
Reading from 29288: heap size 123 MB, throughput 0.996619
Reading from 29287: heap size 498 MB, throughput 0.537229
Reading from 29287: heap size 502 MB, throughput 0.528727
Reading from 29287: heap size 505 MB, throughput 0.53077
Reading from 29288: heap size 129 MB, throughput 0.996667
Reading from 29287: heap size 509 MB, throughput 0.080994
Reading from 29288: heap size 130 MB, throughput 0.984391
Reading from 29287: heap size 564 MB, throughput 0.319813
Reading from 29287: heap size 569 MB, throughput 0.589541
Reading from 29287: heap size 570 MB, throughput 0.582287
Reading from 29287: heap size 575 MB, throughput 0.647931
Reading from 29288: heap size 137 MB, throughput 0.992092
Reading from 29287: heap size 576 MB, throughput 0.101682
Reading from 29287: heap size 641 MB, throughput 0.468974
Reading from 29288: heap size 136 MB, throughput 0.996694
Reading from 29287: heap size 643 MB, throughput 0.516282
Reading from 29287: heap size 644 MB, throughput 0.493146
Reading from 29287: heap size 649 MB, throughput 0.799827
Reading from 29288: heap size 144 MB, throughput 0.996719
Reading from 29287: heap size 652 MB, throughput 0.850392
Reading from 29288: heap size 145 MB, throughput 0.996668
Reading from 29287: heap size 659 MB, throughput 0.832077
Reading from 29287: heap size 661 MB, throughput 0.814993
Reading from 29288: heap size 153 MB, throughput 0.997265
Reading from 29288: heap size 153 MB, throughput 0.996606
Reading from 29287: heap size 675 MB, throughput 0.0426562
Reading from 29287: heap size 749 MB, throughput 0.552309
Reading from 29287: heap size 755 MB, throughput 0.426938
Reading from 29288: heap size 159 MB, throughput 0.99732
Reading from 29287: heap size 751 MB, throughput 0.387901
Reading from 29287: heap size 681 MB, throughput 0.322023
Equal recommendation: 2446 MB each
Reading from 29288: heap size 159 MB, throughput 0.997746
Reading from 29287: heap size 743 MB, throughput 0.071649
Reading from 29287: heap size 824 MB, throughput 0.358892
Reading from 29287: heap size 825 MB, throughput 0.897308
Reading from 29287: heap size 828 MB, throughput 0.777911
Reading from 29287: heap size 829 MB, throughput 0.6452
Reading from 29287: heap size 831 MB, throughput 0.642898
Reading from 29288: heap size 164 MB, throughput 0.997412
Reading from 29287: heap size 832 MB, throughput 0.687735
Reading from 29288: heap size 165 MB, throughput 0.997659
Reading from 29287: heap size 833 MB, throughput 0.12575
Reading from 29288: heap size 168 MB, throughput 0.987935
Reading from 29287: heap size 922 MB, throughput 0.435743
Reading from 29287: heap size 923 MB, throughput 0.927304
Reading from 29287: heap size 932 MB, throughput 0.896999
Reading from 29288: heap size 169 MB, throughput 0.992734
Reading from 29287: heap size 935 MB, throughput 0.82422
Reading from 29287: heap size 934 MB, throughput 0.90341
Reading from 29287: heap size 936 MB, throughput 0.889526
Reading from 29287: heap size 912 MB, throughput 0.801045
Reading from 29288: heap size 178 MB, throughput 0.996507
Reading from 29287: heap size 799 MB, throughput 0.78799
Reading from 29287: heap size 899 MB, throughput 0.712678
Reading from 29287: heap size 804 MB, throughput 0.782056
Reading from 29287: heap size 891 MB, throughput 0.825922
Reading from 29287: heap size 803 MB, throughput 0.826393
Reading from 29287: heap size 884 MB, throughput 0.820109
Reading from 29287: heap size 891 MB, throughput 0.802229
Reading from 29288: heap size 179 MB, throughput 0.997688
Reading from 29287: heap size 880 MB, throughput 0.846049
Reading from 29287: heap size 886 MB, throughput 0.849843
Reading from 29287: heap size 879 MB, throughput 0.980237
Reading from 29288: heap size 186 MB, throughput 0.997647
Reading from 29287: heap size 883 MB, throughput 0.975891
Reading from 29288: heap size 187 MB, throughput 0.997587
Reading from 29287: heap size 890 MB, throughput 0.805461
Reading from 29287: heap size 891 MB, throughput 0.809353
Reading from 29287: heap size 896 MB, throughput 0.803176
Reading from 29287: heap size 896 MB, throughput 0.817098
Reading from 29287: heap size 900 MB, throughput 0.824847
Reading from 29287: heap size 901 MB, throughput 0.796905
Equal recommendation: 2446 MB each
Reading from 29287: heap size 905 MB, throughput 0.815818
Reading from 29288: heap size 192 MB, throughput 0.998123
Reading from 29287: heap size 906 MB, throughput 0.833331
Reading from 29287: heap size 910 MB, throughput 0.89821
Reading from 29287: heap size 912 MB, throughput 0.890614
Reading from 29287: heap size 910 MB, throughput 0.91698
Reading from 29288: heap size 193 MB, throughput 0.997671
Reading from 29287: heap size 914 MB, throughput 0.77044
Reading from 29287: heap size 908 MB, throughput 0.66454
Reading from 29288: heap size 198 MB, throughput 0.996318
Reading from 29287: heap size 920 MB, throughput 0.0666421
Reading from 29287: heap size 1045 MB, throughput 0.59573
Reading from 29288: heap size 198 MB, throughput 0.997158
Reading from 29287: heap size 1045 MB, throughput 0.720923
Reading from 29287: heap size 1056 MB, throughput 0.740818
Reading from 29287: heap size 1056 MB, throughput 0.72094
Reading from 29287: heap size 1068 MB, throughput 0.738832
Reading from 29287: heap size 1069 MB, throughput 0.710868
Reading from 29287: heap size 1084 MB, throughput 0.732733
Reading from 29287: heap size 1085 MB, throughput 0.699127
Reading from 29288: heap size 203 MB, throughput 0.997824
Reading from 29287: heap size 1101 MB, throughput 0.792619
Reading from 29288: heap size 204 MB, throughput 0.997579
Reading from 29287: heap size 1104 MB, throughput 0.974307
Reading from 29288: heap size 209 MB, throughput 0.995977
Reading from 29287: heap size 1123 MB, throughput 0.965321
Reading from 29288: heap size 209 MB, throughput 0.997445
Equal recommendation: 2446 MB each
Reading from 29288: heap size 213 MB, throughput 0.997675
Reading from 29287: heap size 1125 MB, throughput 0.969271
Reading from 29288: heap size 214 MB, throughput 0.996909
Reading from 29288: heap size 219 MB, throughput 0.992654
Reading from 29287: heap size 1133 MB, throughput 0.941477
Reading from 29288: heap size 219 MB, throughput 0.996636
Reading from 29287: heap size 1136 MB, throughput 0.964804
Reading from 29288: heap size 226 MB, throughput 0.998028
Reading from 29287: heap size 1130 MB, throughput 0.963668
Reading from 29288: heap size 227 MB, throughput 0.997844
Reading from 29288: heap size 231 MB, throughput 0.998151
Reading from 29287: heap size 1137 MB, throughput 0.956033
Reading from 29288: heap size 232 MB, throughput 0.997712
Equal recommendation: 2446 MB each
Reading from 29287: heap size 1125 MB, throughput 0.960838
Reading from 29288: heap size 220 MB, throughput 0.998001
Reading from 29288: heap size 211 MB, throughput 0.997396
Reading from 29287: heap size 1132 MB, throughput 0.959968
Reading from 29288: heap size 202 MB, throughput 0.997603
Reading from 29287: heap size 1131 MB, throughput 0.962324
Reading from 29288: heap size 193 MB, throughput 0.997491
Reading from 29288: heap size 185 MB, throughput 0.997313
Reading from 29287: heap size 1133 MB, throughput 0.95931
Reading from 29288: heap size 177 MB, throughput 0.997327
Reading from 29288: heap size 170 MB, throughput 0.997104
Reading from 29287: heap size 1138 MB, throughput 0.961734
Reading from 29288: heap size 163 MB, throughput 0.99715
Reading from 29288: heap size 156 MB, throughput 0.994342
Reading from 29288: heap size 150 MB, throughput 0.989972
Equal recommendation: 2446 MB each
Reading from 29287: heap size 1139 MB, throughput 0.951154
Reading from 29288: heap size 155 MB, throughput 0.994314
Reading from 29288: heap size 162 MB, throughput 0.997562
Reading from 29288: heap size 155 MB, throughput 0.996601
Reading from 29287: heap size 1145 MB, throughput 0.961225
Reading from 29288: heap size 148 MB, throughput 0.997246
Reading from 29288: heap size 142 MB, throughput 0.997055
Reading from 29287: heap size 1147 MB, throughput 0.95975
Reading from 29288: heap size 137 MB, throughput 0.996991
Reading from 29288: heap size 131 MB, throughput 0.996658
Reading from 29288: heap size 126 MB, throughput 0.996725
Reading from 29287: heap size 1152 MB, throughput 0.961269
Reading from 29288: heap size 121 MB, throughput 0.99656
Reading from 29288: heap size 117 MB, throughput 0.995651
Reading from 29288: heap size 112 MB, throughput 0.995803
Reading from 29287: heap size 1157 MB, throughput 0.959328
Reading from 29288: heap size 108 MB, throughput 0.995872
Reading from 29288: heap size 104 MB, throughput 0.995387
Equal recommendation: 2446 MB each
Reading from 29288: heap size 100 MB, throughput 0.995072
Reading from 29288: heap size 96 MB, throughput 0.994688
Reading from 29287: heap size 1164 MB, throughput 0.959337
Reading from 29288: heap size 92 MB, throughput 0.847308
Reading from 29288: heap size 91 MB, throughput 0.997565
Reading from 29288: heap size 98 MB, throughput 0.997357
Reading from 29288: heap size 97 MB, throughput 0.997834
Reading from 29287: heap size 1167 MB, throughput 0.959384
Reading from 29288: heap size 104 MB, throughput 0.99748
Reading from 29288: heap size 103 MB, throughput 0.997753
Reading from 29288: heap size 110 MB, throughput 0.997611
Reading from 29287: heap size 1173 MB, throughput 0.95793
Reading from 29288: heap size 109 MB, throughput 0.998041
Reading from 29288: heap size 116 MB, throughput 0.997777
Reading from 29288: heap size 115 MB, throughput 0.996605
Reading from 29288: heap size 122 MB, throughput 0.987107
Reading from 29288: heap size 127 MB, throughput 0.987732
Reading from 29288: heap size 135 MB, throughput 0.99255
Reading from 29287: heap size 1175 MB, throughput 0.961819
Reading from 29288: heap size 143 MB, throughput 0.997416
Reading from 29288: heap size 151 MB, throughput 0.997343
Reading from 29287: heap size 1181 MB, throughput 0.960001
Reading from 29288: heap size 158 MB, throughput 0.99743
Equal recommendation: 2446 MB each
Reading from 29288: heap size 165 MB, throughput 0.99737
Reading from 29288: heap size 172 MB, throughput 0.997602
Reading from 29287: heap size 1182 MB, throughput 0.959703
Reading from 29288: heap size 177 MB, throughput 0.997665
Reading from 29288: heap size 181 MB, throughput 0.997089
Reading from 29288: heap size 181 MB, throughput 0.996776
Reading from 29287: heap size 1189 MB, throughput 0.537512
Reading from 29288: heap size 187 MB, throughput 0.997795
Reading from 29288: heap size 187 MB, throughput 0.997042
Reading from 29288: heap size 193 MB, throughput 0.995812
Reading from 29287: heap size 1289 MB, throughput 0.944105
Reading from 29288: heap size 193 MB, throughput 0.996879
Equal recommendation: 2446 MB each
Reading from 29288: heap size 199 MB, throughput 0.997837
Reading from 29287: heap size 1291 MB, throughput 0.978669
Reading from 29288: heap size 200 MB, throughput 0.997998
Reading from 29288: heap size 205 MB, throughput 0.997808
Reading from 29287: heap size 1295 MB, throughput 0.962731
Reading from 29288: heap size 206 MB, throughput 0.992248
Reading from 29288: heap size 211 MB, throughput 0.994689
Reading from 29288: heap size 212 MB, throughput 0.99764
Reading from 29287: heap size 1303 MB, throughput 0.979166
Reading from 29288: heap size 219 MB, throughput 0.997925
Reading from 29287: heap size 1303 MB, throughput 0.972234
Reading from 29288: heap size 220 MB, throughput 0.997699
Reading from 29288: heap size 228 MB, throughput 0.997986
Equal recommendation: 2446 MB each
Reading from 29287: heap size 1299 MB, throughput 0.974602
Reading from 29288: heap size 228 MB, throughput 0.99778
Reading from 29288: heap size 234 MB, throughput 0.997736
Reading from 29287: heap size 1303 MB, throughput 0.969768
Reading from 29288: heap size 235 MB, throughput 0.997663
Reading from 29288: heap size 242 MB, throughput 0.997736
Reading from 29287: heap size 1292 MB, throughput 0.968211
Reading from 29288: heap size 242 MB, throughput 0.997591
Reading from 29288: heap size 249 MB, throughput 0.996176
Reading from 29287: heap size 1298 MB, throughput 0.968123
Reading from 29288: heap size 249 MB, throughput 0.997981
Equal recommendation: 2446 MB each
Reading from 29288: heap size 256 MB, throughput 0.991836
Reading from 29287: heap size 1297 MB, throughput 0.966424
Reading from 29288: heap size 256 MB, throughput 0.996857
Reading from 29288: heap size 267 MB, throughput 0.998052
Reading from 29287: heap size 1298 MB, throughput 0.968862
Reading from 29288: heap size 267 MB, throughput 0.997843
Reading from 29287: heap size 1303 MB, throughput 0.96858
Reading from 29288: heap size 275 MB, throughput 0.998391
Reading from 29288: heap size 276 MB, throughput 0.998027
Reading from 29287: heap size 1305 MB, throughput 0.96399
Reading from 29288: heap size 283 MB, throughput 0.998264
Equal recommendation: 2446 MB each
Reading from 29288: heap size 284 MB, throughput 0.997707
Reading from 29287: heap size 1310 MB, throughput 0.963825
Reading from 29288: heap size 292 MB, throughput 0.997977
Reading from 29287: heap size 1316 MB, throughput 0.962921
Reading from 29288: heap size 292 MB, throughput 0.998034
Reading from 29288: heap size 298 MB, throughput 0.996476
Reading from 29287: heap size 1321 MB, throughput 0.962469
Reading from 29288: heap size 299 MB, throughput 0.995054
Reading from 29288: heap size 307 MB, throughput 0.997854
Equal recommendation: 2446 MB each
Reading from 29287: heap size 1329 MB, throughput 0.960156
Reading from 29288: heap size 308 MB, throughput 0.99807
Reading from 29288: heap size 317 MB, throughput 0.997953
Reading from 29288: heap size 317 MB, throughput 0.997787
Reading from 29288: heap size 326 MB, throughput 0.998042
Reading from 29288: heap size 326 MB, throughput 0.998094
Equal recommendation: 2446 MB each
Reading from 29288: heap size 334 MB, throughput 0.99827
Reading from 29287: heap size 1337 MB, throughput 0.988673
Reading from 29288: heap size 334 MB, throughput 0.997267
Reading from 29288: heap size 342 MB, throughput 0.996396
Reading from 29288: heap size 343 MB, throughput 0.998038
Reading from 29288: heap size 353 MB, throughput 0.998248
Equal recommendation: 2446 MB each
Reading from 29288: heap size 353 MB, throughput 0.998158
Reading from 29288: heap size 361 MB, throughput 0.997704
Reading from 29288: heap size 362 MB, throughput 0.997901
Reading from 29287: heap size 1342 MB, throughput 0.990567
Reading from 29288: heap size 371 MB, throughput 0.998322
Reading from 29288: heap size 371 MB, throughput 0.997555
Equal recommendation: 2446 MB each
Reading from 29288: heap size 380 MB, throughput 0.996858
Reading from 29288: heap size 380 MB, throughput 0.997554
Reading from 29287: heap size 1348 MB, throughput 0.89371
Reading from 29287: heap size 1413 MB, throughput 0.994449
Reading from 29287: heap size 1442 MB, throughput 0.934969
Reading from 29287: heap size 1451 MB, throughput 0.905018
Reading from 29287: heap size 1441 MB, throughput 0.909947
Reading from 29287: heap size 1446 MB, throughput 0.89412
Reading from 29288: heap size 392 MB, throughput 0.993707
Reading from 29287: heap size 1441 MB, throughput 0.916721
Reading from 29287: heap size 1446 MB, throughput 0.988928
Reading from 29288: heap size 392 MB, throughput 0.998143
Reading from 29287: heap size 1450 MB, throughput 0.991223
Equal recommendation: 2446 MB each
Reading from 29288: heap size 404 MB, throughput 0.998177
Reading from 29287: heap size 1456 MB, throughput 0.990789
Reading from 29288: heap size 405 MB, throughput 0.9982
Reading from 29287: heap size 1463 MB, throughput 0.988176
Reading from 29288: heap size 416 MB, throughput 0.998212
Reading from 29287: heap size 1467 MB, throughput 0.986444
Reading from 29288: heap size 417 MB, throughput 0.995735
Equal recommendation: 2446 MB each
Reading from 29288: heap size 427 MB, throughput 0.998414
Reading from 29287: heap size 1454 MB, throughput 0.984285
Reading from 29288: heap size 428 MB, throughput 0.998313
Reading from 29287: heap size 1313 MB, throughput 0.978707
Reading from 29288: heap size 439 MB, throughput 0.998356
Reading from 29287: heap size 1439 MB, throughput 0.981
Reading from 29288: heap size 440 MB, throughput 0.9983
Reading from 29287: heap size 1340 MB, throughput 0.979117
Equal recommendation: 2446 MB each
Reading from 29288: heap size 450 MB, throughput 0.998379
Reading from 29287: heap size 1435 MB, throughput 0.977007
Reading from 29288: heap size 451 MB, throughput 0.936689
Reading from 29288: heap size 467 MB, throughput 0.998224
Reading from 29287: heap size 1441 MB, throughput 0.97707
Reading from 29288: heap size 467 MB, throughput 0.998939
Reading from 29287: heap size 1445 MB, throughput 0.974324
Equal recommendation: 2446 MB each
Reading from 29288: heap size 479 MB, throughput 0.999068
Reading from 29287: heap size 1446 MB, throughput 0.968748
Reading from 29288: heap size 481 MB, throughput 0.998857
Reading from 29287: heap size 1452 MB, throughput 0.969968
Reading from 29288: heap size 492 MB, throughput 0.999063
Reading from 29287: heap size 1459 MB, throughput 0.967823
Reading from 29288: heap size 493 MB, throughput 0.997795
Equal recommendation: 2446 MB each
Reading from 29287: heap size 1466 MB, throughput 0.966742
Reading from 29288: heap size 504 MB, throughput 0.998079
Reading from 29287: heap size 1476 MB, throughput 0.958715
Reading from 29288: heap size 505 MB, throughput 0.998538
Reading from 29287: heap size 1487 MB, throughput 0.963022
Reading from 29288: heap size 519 MB, throughput 0.998562
Equal recommendation: 2446 MB each
Reading from 29287: heap size 1497 MB, throughput 0.95943
Reading from 29288: heap size 519 MB, throughput 0.998492
Reading from 29287: heap size 1509 MB, throughput 0.96539
Reading from 29288: heap size 531 MB, throughput 0.998495
Reading from 29288: heap size 531 MB, throughput 0.99643
Reading from 29287: heap size 1515 MB, throughput 0.963937
Equal recommendation: 2446 MB each
Reading from 29288: heap size 544 MB, throughput 0.998232
Reading from 29287: heap size 1527 MB, throughput 0.963717
Reading from 29288: heap size 545 MB, throughput 0.998411
Reading from 29287: heap size 1530 MB, throughput 0.963135
Reading from 29288: heap size 559 MB, throughput 0.998286
Reading from 29287: heap size 1543 MB, throughput 0.965081
Equal recommendation: 2446 MB each
Reading from 29288: heap size 560 MB, throughput 0.998285
Reading from 29287: heap size 1544 MB, throughput 0.965703
Reading from 29288: heap size 575 MB, throughput 0.996756
Reading from 29287: heap size 1556 MB, throughput 0.963947
Reading from 29288: heap size 576 MB, throughput 0.998447
Reading from 29287: heap size 1556 MB, throughput 0.964208
Equal recommendation: 2446 MB each
Reading from 29288: heap size 592 MB, throughput 0.998468
Reading from 29287: heap size 1568 MB, throughput 0.745382
Reading from 29288: heap size 594 MB, throughput 0.998422
Reading from 29287: heap size 1708 MB, throughput 0.993228
Reading from 29288: heap size 610 MB, throughput 0.998444
Equal recommendation: 2446 MB each
Client 29288 died
Clients: 1
Reading from 29287: heap size 1708 MB, throughput 0.99156
Reading from 29287: heap size 1720 MB, throughput 0.988957
Recommendation: one client; give it all the memory
Reading from 29287: heap size 1733 MB, throughput 0.994832
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 29287: heap size 1590 MB, throughput 0.994294
Reading from 29287: heap size 1727 MB, throughput 0.900223
Reading from 29287: heap size 1729 MB, throughput 0.778529
Reading from 29287: heap size 1751 MB, throughput 0.819494
Reading from 29287: heap size 1772 MB, throughput 0.798747
Reading from 29287: heap size 1812 MB, throughput 0.861738
Client 29287 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
