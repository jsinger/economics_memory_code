module Main where

import ProgramSpecParser

main :: IO()
main = do s <- getContents
          print (parseProgramSpec s)
