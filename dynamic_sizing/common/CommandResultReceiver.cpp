#include <unistd.h>
#include "CommandResultReceiver.hpp"
#include "Utilities.hpp"

economem::CommandResultReceiver::CommandResultReceiver() : RecvSock(economem::allocClientSocketName(getpid(), economem::TYPE_CMDRESULT)) {}
