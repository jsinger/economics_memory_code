Using scaled threading model. 4 processors detected, 2 threads used to drive the workload, in a possible range of [1,64000]
java.lang.reflect.InvocationTargetException
java.lang.reflect.InvocationTargetException
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:483)
	at org.dacapo.harness.H2.preIteration(H2.java:77)
	at org.dacapo.harness.Benchmark.run(Benchmark.java:152)
	at org.dacapo.harness.TestHarness.runBenchmark(TestHarness.java:218)
	at org.dacapo.harness.TestHarness.main(TestHarness.java:171)
	at Harness.main(Harness.java:17)
Caused by: org.h2.jdbc.JdbcSQLException: Out of memory.; SQL statement:
INSERT INTO STOCK (S_I_ID, S_W_ID, S_QUANTITY,S_DIST_01, S_DIST_02, S_DIST_03,S_DIST_04,S_DIST_05,S_DIST_06,S_DIST_07,S_DIST_08,S_DIST_09,S_DIST_10,S_ORDER_CNT, S_REMOTE_CNT, S_YTD, S_DATA, S_QUANTITY_INITIAL ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, 0, 0, 0, ?, ?) [90108-123]
	at org.h2.message.Message.getSQLException(Message.java:111)
	at org.h2.message.Message.convertThrowable(Message.java:303)
	at org.h2.command.Command.executeUpdate(Command.java:231)
	at org.h2.jdbc.JdbcPreparedStatement.executeUpdateInternal(JdbcPreparedStatement.java:139)
	at org.h2.jdbc.JdbcPreparedStatement.executeUpdate(JdbcPreparedStatement.java:128)
	at org.apache.derbyTesting.system.oe.load.SimpleInsert.stockTable(SimpleInsert.java:253)
	at org.apache.derbyTesting.system.oe.load.SimpleInsert.populateForOneWarehouse(SimpleInsert.java:145)
	at org.apache.derbyTesting.system.oe.load.SimpleInsert.populateAllTables(SimpleInsert.java:127)
	at org.apache.derbyTesting.system.oe.load.ThreadInsert.populateAllTables(ThreadInsert.java:127)
	at org.dacapo.h2.TPCC.loadData(TPCC.java:400)
	at org.dacapo.h2.TPCC.preIterationMemoryDB(TPCC.java:215)
	at org.dacapo.h2.TPCC.preIteration(TPCC.java:254)
	... 9 more
Caused by: java.lang.OutOfMemoryError: GC overhead limit exceeded
	at org.h2.table.Table.getTemplateRow(Table.java:473)
	at org.h2.command.dml.Insert.insertRows(Insert.java:98)
	at org.h2.command.dml.Insert.update(Insert.java:82)
	at org.h2.command.CommandContainer.update(CommandContainer.java:72)
	at org.h2.command.Command.executeUpdate(Command.java:209)
	... 18 more
