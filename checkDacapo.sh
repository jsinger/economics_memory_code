#!/bin/bash
# Make sure DaCapo is available

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DACAPO="${DIR}/dacapo-9.12-bach.jar"

if [ ! -e "${DACAPO}" ]; then
    wget -O "${DACAPO}" 'http://optimate.dl.sourceforge.net/project/dacapobench/9.12-bach/dacapo-9.12-bach.jar'
fi

if [ ! -e "${DACAPO}" ]; then
    echo "Can't find DaCapo!"
    exit 1
fi

exit 0
