import os
from SCons.Environment import Environment
from SCons.Script import Glob, SConscript

script_globals = None

def scons_setup(globs):
    global script_globals
    script_globals = globs

def setup_env(**kwargs):
    env = Environment(ENV=os.environ, **kwargs)
    env["CC"] = os.getenv("CC") or env["CC"]
    env["CXX"] = os.getenv("CXX") or env["CXX"]
    env["ENV"].update(x for x in os.environ.items() if x[0].startswith("CCC_"))
    script_globals["env"] = env

def setup_clean(*globs):
    if globs:
        clean = Glob(globs[0])

        for i in globs[1:]:
            clean = clean + Glob(i)
        script_globals["clean"] = clean

def envPlusArgs(kwargs):
    d = {}
    for arg in kwargs:
        if arg in script_globals["env"]:
            d[arg] = script_globals["env"][arg] + kwargs[arg]
        else:
            d[arg] = kwargs[arg]
    return d

def obj(name, **kwargs):
    script_globals[name + "_o"] = script_globals["env"].Object(name + ".cpp", **envPlusArgs(kwargs))

def commonObjs(*required):
    env = script_globals["env"]
    objs = SConscript("../common/SConscript", exports="env required", variant_dir=".", duplicate=0)
    for name in objs:
        script_globals[name + "_o"] = objs[name]

def prog(name, *args, **kwargs):
    p = script_globals["env"].Program(name, *args, **envPlusArgs(kwargs))
    if "clean" in script_globals:
        script_globals["env"].Clean(p, script_globals["clean"])
    script_globals[name] = p
