#!/bin/bash
# Process an existing isoquant dataset
#
# Usage: isoquant_postprocess.sh data_dir
#

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") data_dir"
    exit 1
}

test -z "${1}" && usage
test ! -d "${1}" && usage

DATA_DIR="$(readlink -f "${1}")"

cd "${DATA_DIR}"

BENCHMARKS="${DATA_DIR}/benchmarks.txt"
AVGTIMES="${DATA_DIR}/avgTimes.txt"

B1="$(sed -n '1p' < "${BENCHMARKS}")"
B2="$(sed -n '2p' < "${BENCHMARKS}")"
TOTAL="$(cat "${DATA_DIR}/maxMemory.txt")"
ITERATIONS="$(cat "${DATA_DIR}/iterations.txt")"
RUNS="$(cat "${DATA_DIR}/runs.txt")"
PLOT="${DATA_DIR}/isoquant.pdf"

cat avgTimes_*.txt | sort -n > "${AVGTIMES}"

octave -qf "${DIR}/isoquant_plot.m" "${B1}, ${B2}: ${TOTAL} MB, ${ITERATIONS} iterations, ${RUNS} runs" "${B1}" "${B2}" "${AVGTIMES}" "${PLOT}"
