{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, DeriveDataTypeable #-}
{-# OPTIONS_GHC -fno-warn-missing-fields -fno-warn-missing-methods #-}

import Economem.ArgHelper
import Economem.Common
import Economem.BenchmarkTimer
import Economem.Experiment
import Economem.Files
import Economem.Java
import Economem.Records
import Economem.Signals
import qualified Economem.CgroupBenchmarkTimer as C
import System.Console.CmdLib

data Args = Args { inputFile :: FilePath,
                   totalMemMB :: Int,
                   runs :: Int,
                   java :: String }
          deriving (Typeable, Data, Eq, Show)

instance Attributes Args where
  attributes _ = group "Options" [
    inputFile %> [ Positional 0,
                   Required True,
                   Help "Specifies which benchmarks to run",
                   ArgHelp "INPUT_FILE" ],
    totalMemMB %> [ Positional 1,
                    Required True,
                    Help "Total memory for the cgroup",
                    ArgHelp "MB" ],
    runs %> [ Short ['r'],
              Long ["runs"],
              Default (fromIntegral defaultRuns::Int),
              Help "How many times to repeat, for taking the average",
              ArgHelp "RUNS" ],
    java %> [ Short ['j'],
              Long ["java"],
              Default (javaName defaultJava),
              Help ("Java version to use; available options: " ++ show validJavaNames),
              ArgHelp "JAVA" ]]

instance RecordCommand Args where
  mode_summary _ = "Run multiple benchmark VMs within a cgroup, and see how long it takes for them all to finish. Xmx for each is set to the same as the cgroup limit."

main :: IO ()
main = getArgs >>= executeR Args {} >>= \opts -> do
  (i, s, r, j) <- validate opts
  b <- C.newCgroupBenchmarkTimer C.simple i s r j False Nothing []
  h <- makeSignalHandler
  runExperiment b h

  where
    validate :: Args -> IO (Input BenchmarkThreadsIters, DataSize, Integer, Java)
    validate a = do
      i <- existingAbsoluteFile (inputFile a)
      s <- positive (totalMemMB a)
      r <- positive (runs a)
      j <- javaFromName (java a)
      return (fromFile i, DataSize (fromIntegral s) MB, fromIntegral r, j)
