economemd
    total memory: 7518 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub25_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run05_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 23821: heap size 9 MB, throughput 0.99008
Clients: 1
Client 23821 has a minimum heap size of 30 MB
Reading from 23820: heap size 9 MB, throughput 0.960521
Clients: 2
Client 23820 has a minimum heap size of 1223 MB
Reading from 23821: heap size 9 MB, throughput 0.972445
Reading from 23821: heap size 9 MB, throughput 0.956775
Reading from 23821: heap size 9 MB, throughput 0.964789
Reading from 23821: heap size 11 MB, throughput 0.809846
Reading from 23821: heap size 11 MB, throughput 0.382009
Reading from 23820: heap size 9 MB, throughput 0.982158
Reading from 23821: heap size 16 MB, throughput 0.716943
Reading from 23821: heap size 16 MB, throughput 0.656688
Reading from 23821: heap size 24 MB, throughput 0.81641
Reading from 23821: heap size 24 MB, throughput 0.781365
Reading from 23821: heap size 34 MB, throughput 0.913593
Reading from 23821: heap size 34 MB, throughput 0.892367
Reading from 23821: heap size 50 MB, throughput 0.935354
Reading from 23821: heap size 50 MB, throughput 0.927692
Reading from 23821: heap size 75 MB, throughput 0.960073
Reading from 23821: heap size 75 MB, throughput 0.941455
Reading from 23820: heap size 11 MB, throughput 0.976713
Reading from 23821: heap size 116 MB, throughput 0.963597
Reading from 23821: heap size 116 MB, throughput 0.957789
Reading from 23820: heap size 11 MB, throughput 0.983059
Reading from 23820: heap size 15 MB, throughput 0.88952
Reading from 23820: heap size 19 MB, throughput 0.966959
Reading from 23820: heap size 25 MB, throughput 0.876657
Reading from 23820: heap size 29 MB, throughput 0.864954
Reading from 23820: heap size 33 MB, throughput 0.611841
Reading from 23820: heap size 43 MB, throughput 0.900132
Reading from 23820: heap size 50 MB, throughput 0.811478
Reading from 23821: heap size 160 MB, throughput 0.975185
Reading from 23820: heap size 51 MB, throughput 0.239769
Reading from 23820: heap size 70 MB, throughput 0.843244
Reading from 23821: heap size 165 MB, throughput 0.855366
Reading from 23820: heap size 73 MB, throughput 0.226175
Reading from 23820: heap size 99 MB, throughput 0.764313
Reading from 23820: heap size 101 MB, throughput 0.743014
Reading from 23820: heap size 106 MB, throughput 0.203388
Reading from 23820: heap size 131 MB, throughput 0.649576
Reading from 23821: heap size 226 MB, throughput 0.991984
Reading from 23820: heap size 143 MB, throughput 0.797387
Reading from 23820: heap size 144 MB, throughput 0.650336
Reading from 23820: heap size 149 MB, throughput 0.155435
Reading from 23821: heap size 235 MB, throughput 0.930252
Reading from 23820: heap size 182 MB, throughput 0.578956
Reading from 23820: heap size 194 MB, throughput 0.70883
Reading from 23820: heap size 196 MB, throughput 0.69087
Reading from 23820: heap size 203 MB, throughput 0.20303
Reading from 23820: heap size 247 MB, throughput 0.672817
Reading from 23820: heap size 255 MB, throughput 0.65879
Reading from 23821: heap size 266 MB, throughput 0.995503
Reading from 23820: heap size 260 MB, throughput 0.740892
Reading from 23820: heap size 265 MB, throughput 0.589879
Reading from 23820: heap size 275 MB, throughput 0.548548
Reading from 23821: heap size 266 MB, throughput 0.84301
Reading from 23820: heap size 285 MB, throughput 0.180279
Reading from 23820: heap size 334 MB, throughput 0.492427
Reading from 23820: heap size 348 MB, throughput 0.548897
Reading from 23820: heap size 350 MB, throughput 0.682082
Reading from 23821: heap size 307 MB, throughput 0.99519
Reading from 23820: heap size 359 MB, throughput 0.147368
Reading from 23820: heap size 409 MB, throughput 0.525682
Reading from 23821: heap size 312 MB, throughput 0.982437
Reading from 23820: heap size 420 MB, throughput 0.607662
Reading from 23820: heap size 421 MB, throughput 0.597911
Reading from 23820: heap size 425 MB, throughput 0.617609
Reading from 23820: heap size 433 MB, throughput 0.525801
Equal recommendation: 3759 MB each
Reading from 23821: heap size 330 MB, throughput 0.97304
Reading from 23820: heap size 441 MB, throughput 0.142852
Reading from 23820: heap size 498 MB, throughput 0.545786
Reading from 23821: heap size 331 MB, throughput 0.907768
Reading from 23820: heap size 506 MB, throughput 0.552736
Reading from 23820: heap size 512 MB, throughput 0.560021
Reading from 23820: heap size 515 MB, throughput 0.618744
Reading from 23820: heap size 527 MB, throughput 0.543029
Reading from 23821: heap size 355 MB, throughput 0.969757
Reading from 23820: heap size 536 MB, throughput 0.101987
Reading from 23820: heap size 598 MB, throughput 0.330917
Reading from 23820: heap size 603 MB, throughput 0.559862
Reading from 23821: heap size 358 MB, throughput 0.984046
Reading from 23820: heap size 607 MB, throughput 0.112241
Reading from 23820: heap size 676 MB, throughput 0.465293
Reading from 23821: heap size 383 MB, throughput 0.992731
Reading from 23820: heap size 676 MB, throughput 0.640239
Reading from 23820: heap size 676 MB, throughput 0.643465
Reading from 23820: heap size 677 MB, throughput 0.577332
Reading from 23820: heap size 680 MB, throughput 0.805723
Reading from 23821: heap size 385 MB, throughput 0.982615
Reading from 23820: heap size 689 MB, throughput 0.806794
Reading from 23821: heap size 406 MB, throughput 0.986377
Reading from 23821: heap size 412 MB, throughput 0.972157
Reading from 23820: heap size 692 MB, throughput 0.248782
Equal recommendation: 3759 MB each
Reading from 23820: heap size 776 MB, throughput 0.410204
Reading from 23820: heap size 794 MB, throughput 0.58493
Reading from 23820: heap size 795 MB, throughput 0.382915
Reading from 23820: heap size 791 MB, throughput 0.392512
Reading from 23821: heap size 435 MB, throughput 0.980687
Reading from 23820: heap size 796 MB, throughput 0.542996
Reading from 23821: heap size 437 MB, throughput 0.989204
Reading from 23820: heap size 780 MB, throughput 0.212908
Reading from 23820: heap size 858 MB, throughput 0.404012
Reading from 23820: heap size 851 MB, throughput 0.615373
Reading from 23820: heap size 855 MB, throughput 0.784944
Reading from 23821: heap size 462 MB, throughput 0.977108
Reading from 23820: heap size 860 MB, throughput 0.220965
Reading from 23820: heap size 939 MB, throughput 0.702774
Reading from 23821: heap size 463 MB, throughput 0.991171
Reading from 23820: heap size 946 MB, throughput 0.762005
Reading from 23820: heap size 948 MB, throughput 0.776346
Reading from 23821: heap size 488 MB, throughput 0.980134
Reading from 23820: heap size 947 MB, throughput 0.130211
Reading from 23820: heap size 1041 MB, throughput 0.538297
Reading from 23820: heap size 1046 MB, throughput 0.752787
Reading from 23820: heap size 1049 MB, throughput 0.73782
Reading from 23821: heap size 491 MB, throughput 0.990722
Reading from 23820: heap size 1055 MB, throughput 0.75245
Reading from 23820: heap size 1056 MB, throughput 0.735307
Reading from 23820: heap size 1055 MB, throughput 0.835094
Reading from 23820: heap size 1059 MB, throughput 0.832525
Reading from 23821: heap size 513 MB, throughput 0.982503
Equal recommendation: 3759 MB each
Reading from 23820: heap size 1053 MB, throughput 0.966914
Reading from 23820: heap size 932 MB, throughput 0.82158
Reading from 23821: heap size 518 MB, throughput 0.98558
Reading from 23820: heap size 1053 MB, throughput 0.7391
Reading from 23820: heap size 1055 MB, throughput 0.767384
Reading from 23820: heap size 1056 MB, throughput 0.762456
Reading from 23820: heap size 1059 MB, throughput 0.782435
Reading from 23820: heap size 1057 MB, throughput 0.783277
Reading from 23821: heap size 545 MB, throughput 0.990586
Reading from 23820: heap size 1062 MB, throughput 0.88653
Reading from 23820: heap size 1059 MB, throughput 0.901896
Reading from 23820: heap size 1063 MB, throughput 0.883139
Reading from 23820: heap size 1059 MB, throughput 0.755958
Reading from 23820: heap size 1063 MB, throughput 0.645415
Reading from 23821: heap size 546 MB, throughput 0.979392
Reading from 23820: heap size 1076 MB, throughput 0.690293
Reading from 23820: heap size 1085 MB, throughput 0.702013
Reading from 23820: heap size 1101 MB, throughput 0.664225
Reading from 23821: heap size 566 MB, throughput 0.979844
Reading from 23820: heap size 1109 MB, throughput 0.0893728
Reading from 23820: heap size 1224 MB, throughput 0.616
Reading from 23820: heap size 1232 MB, throughput 0.670297
Reading from 23821: heap size 568 MB, throughput 0.986767
Reading from 23820: heap size 1241 MB, throughput 0.740649
Equal recommendation: 3759 MB each
Reading from 23821: heap size 594 MB, throughput 0.991106
Reading from 23820: heap size 1244 MB, throughput 0.97312
Reading from 23821: heap size 595 MB, throughput 0.992496
Reading from 23821: heap size 614 MB, throughput 0.987135
Reading from 23820: heap size 1240 MB, throughput 0.976685
Reading from 23821: heap size 618 MB, throughput 0.990175
Reading from 23821: heap size 638 MB, throughput 0.99433
Reading from 23820: heap size 1250 MB, throughput 0.977067
Reading from 23821: heap size 640 MB, throughput 0.987789
Equal recommendation: 3759 MB each
Reading from 23820: heap size 1238 MB, throughput 0.961138
Reading from 23821: heap size 661 MB, throughput 0.988221
Reading from 23821: heap size 662 MB, throughput 0.995518
Reading from 23820: heap size 1249 MB, throughput 0.970304
Reading from 23821: heap size 680 MB, throughput 0.989818
Reading from 23821: heap size 683 MB, throughput 0.987264
Reading from 23820: heap size 1252 MB, throughput 0.955207
Equal recommendation: 3759 MB each
Reading from 23821: heap size 706 MB, throughput 0.984991
Reading from 23821: heap size 706 MB, throughput 0.98524
Reading from 23820: heap size 1255 MB, throughput 0.77084
Reading from 23821: heap size 731 MB, throughput 0.995634
Reading from 23820: heap size 1323 MB, throughput 0.992831
Reading from 23821: heap size 732 MB, throughput 0.987497
Reading from 23821: heap size 754 MB, throughput 0.98916
Equal recommendation: 3759 MB each
Reading from 23820: heap size 1324 MB, throughput 0.992096
Reading from 23821: heap size 754 MB, throughput 0.988436
Reading from 23821: heap size 780 MB, throughput 0.994778
Reading from 23820: heap size 1338 MB, throughput 0.991409
Reading from 23821: heap size 781 MB, throughput 0.991874
Reading from 23820: heap size 1342 MB, throughput 0.987377
Reading from 23821: heap size 800 MB, throughput 0.991482
Equal recommendation: 3759 MB each
Reading from 23821: heap size 802 MB, throughput 0.995239
Reading from 23820: heap size 1340 MB, throughput 0.985661
Reading from 23821: heap size 820 MB, throughput 0.991487
Reading from 23820: heap size 1214 MB, throughput 0.987849
Reading from 23821: heap size 823 MB, throughput 0.990648
Equal recommendation: 3759 MB each
Reading from 23821: heap size 846 MB, throughput 0.990856
Reading from 23820: heap size 1329 MB, throughput 0.983796
Reading from 23821: heap size 846 MB, throughput 0.990904
Reading from 23820: heap size 1236 MB, throughput 0.981056
Reading from 23821: heap size 870 MB, throughput 0.991
Reading from 23821: heap size 870 MB, throughput 0.990545
Reading from 23820: heap size 1317 MB, throughput 0.982366
Equal recommendation: 3759 MB each
Reading from 23821: heap size 896 MB, throughput 0.991455
Reading from 23820: heap size 1325 MB, throughput 0.978422
Reading from 23821: heap size 896 MB, throughput 0.990826
Reading from 23821: heap size 922 MB, throughput 0.992951
Reading from 23820: heap size 1325 MB, throughput 0.979149
Equal recommendation: 3759 MB each
Reading from 23821: heap size 923 MB, throughput 0.991168
Reading from 23820: heap size 1325 MB, throughput 0.975429
Reading from 23821: heap size 949 MB, throughput 0.991798
Reading from 23820: heap size 1328 MB, throughput 0.977655
Reading from 23821: heap size 949 MB, throughput 0.98862
Reading from 23821: heap size 976 MB, throughput 0.993421
Equal recommendation: 3759 MB each
Reading from 23820: heap size 1332 MB, throughput 0.971364
Reading from 23821: heap size 976 MB, throughput 0.990391
Reading from 23820: heap size 1335 MB, throughput 0.977027
Reading from 23821: heap size 1005 MB, throughput 0.992443
Reading from 23820: heap size 1344 MB, throughput 0.971973
Reading from 23821: heap size 1005 MB, throughput 0.98956
Equal recommendation: 3759 MB each
Reading from 23821: heap size 1034 MB, throughput 0.995871
Reading from 23820: heap size 1350 MB, throughput 0.971189
Reading from 23821: heap size 1035 MB, throughput 0.990238
Reading from 23820: heap size 1358 MB, throughput 0.970991
Reading from 23821: heap size 1063 MB, throughput 0.990729
Equal recommendation: 3759 MB each
Reading from 23820: heap size 1365 MB, throughput 0.970011
Reading from 23821: heap size 1063 MB, throughput 0.991898
Reading from 23820: heap size 1369 MB, throughput 0.965477
Reading from 23821: heap size 1095 MB, throughput 0.992839
Equal recommendation: 3759 MB each
Reading from 23821: heap size 1095 MB, throughput 0.983539
Reading from 23820: heap size 1376 MB, throughput 0.967418
Reading from 23821: heap size 1126 MB, throughput 0.988308
Reading from 23820: heap size 1378 MB, throughput 0.970568
Reading from 23821: heap size 1126 MB, throughput 0.991402
Reading from 23820: heap size 1386 MB, throughput 0.974572
Equal recommendation: 3759 MB each
Reading from 23821: heap size 1167 MB, throughput 0.991972
Reading from 23820: heap size 1386 MB, throughput 0.968785
Reading from 23821: heap size 1169 MB, throughput 0.953206
Reading from 23820: heap size 1390 MB, throughput 0.964552
Reading from 23821: heap size 1224 MB, throughput 0.993008
Equal recommendation: 3759 MB each
Reading from 23820: heap size 1391 MB, throughput 0.974374
Reading from 23821: heap size 1230 MB, throughput 0.989138
Reading from 23821: heap size 1276 MB, throughput 0.990726
Reading from 23820: heap size 1393 MB, throughput 0.967511
Equal recommendation: 3759 MB each
Reading from 23821: heap size 1279 MB, throughput 0.992506
Reading from 23820: heap size 1395 MB, throughput 0.663753
Reading from 23821: heap size 1325 MB, throughput 0.992238
Reading from 23820: heap size 1521 MB, throughput 0.961795
Equal recommendation: 3759 MB each
Reading from 23821: heap size 1329 MB, throughput 0.990532
Reading from 23820: heap size 1522 MB, throughput 0.987775
Reading from 23821: heap size 1380 MB, throughput 0.991653
Reading from 23821: heap size 1382 MB, throughput 0.989262
Equal recommendation: 3759 MB each
Reading from 23821: heap size 1433 MB, throughput 0.991876
Reading from 23821: heap size 1435 MB, throughput 0.987273
Reading from 23820: heap size 1536 MB, throughput 0.992897
Equal recommendation: 3759 MB each
Reading from 23821: heap size 1486 MB, throughput 0.988586
Reading from 23821: heap size 1490 MB, throughput 0.991394
Equal recommendation: 3759 MB each
Reading from 23821: heap size 1548 MB, throughput 0.989775
Reading from 23820: heap size 1436 MB, throughput 0.990559
Reading from 23821: heap size 1551 MB, throughput 0.988287
Reading from 23820: heap size 1530 MB, throughput 0.987087
Reading from 23820: heap size 1538 MB, throughput 0.818715
Reading from 23820: heap size 1527 MB, throughput 0.709188
Reading from 23821: heap size 1618 MB, throughput 0.991436
Reading from 23820: heap size 1562 MB, throughput 0.719034
Equal recommendation: 3759 MB each
Reading from 23820: heap size 1590 MB, throughput 0.742051
Reading from 23820: heap size 1607 MB, throughput 0.73655
Reading from 23820: heap size 1642 MB, throughput 0.744012
Reading from 23820: heap size 1651 MB, throughput 0.958195
Reading from 23821: heap size 1618 MB, throughput 0.991752
Reading from 23820: heap size 1659 MB, throughput 0.98175
Equal recommendation: 3759 MB each
Reading from 23821: heap size 1680 MB, throughput 0.992298
Reading from 23820: heap size 1676 MB, throughput 0.977563
Reading from 23821: heap size 1683 MB, throughput 0.992596
Reading from 23820: heap size 1658 MB, throughput 0.97912
Equal recommendation: 3759 MB each
Reading from 23821: heap size 1742 MB, throughput 0.993551
Reading from 23820: heap size 1676 MB, throughput 0.977803
Client 23821 died
Clients: 1
Reading from 23820: heap size 1658 MB, throughput 0.984813
Recommendation: one client; give it all the memory
Reading from 23820: heap size 1673 MB, throughput 0.984869
Recommendation: one client; give it all the memory
Reading from 23820: heap size 1664 MB, throughput 0.875417
Reading from 23820: heap size 1634 MB, throughput 0.996389
Recommendation: one client; give it all the memory
Reading from 23820: heap size 1636 MB, throughput 0.995312
Reading from 23820: heap size 1650 MB, throughput 0.993865
Recommendation: one client; give it all the memory
Reading from 23820: heap size 1667 MB, throughput 0.993375
Recommendation: one client; give it all the memory
Reading from 23820: heap size 1668 MB, throughput 0.991985
Reading from 23820: heap size 1657 MB, throughput 0.990868
Recommendation: one client; give it all the memory
Reading from 23820: heap size 1479 MB, throughput 0.989762
Recommendation: one client; give it all the memory
Reading from 23820: heap size 1636 MB, throughput 0.988847
Reading from 23820: heap size 1510 MB, throughput 0.987803
Recommendation: one client; give it all the memory
Reading from 23820: heap size 1625 MB, throughput 0.987389
Recommendation: one client; give it all the memory
Reading from 23820: heap size 1634 MB, throughput 0.985825
Reading from 23820: heap size 1636 MB, throughput 0.98476
Recommendation: one client; give it all the memory
Reading from 23820: heap size 1637 MB, throughput 0.984617
Recommendation: one client; give it all the memory
Reading from 23820: heap size 1642 MB, throughput 0.982529
Reading from 23820: heap size 1649 MB, throughput 0.981446
Recommendation: one client; give it all the memory
Reading from 23820: heap size 1655 MB, throughput 0.980995
Reading from 23820: heap size 1667 MB, throughput 0.981403
Recommendation: one client; give it all the memory
Reading from 23820: heap size 1678 MB, throughput 0.981023
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 23820: heap size 1685 MB, throughput 0.989182
Recommendation: one client; give it all the memory
Reading from 23820: heap size 1689 MB, throughput 0.98953
Reading from 23820: heap size 1700 MB, throughput 0.981282
Reading from 23820: heap size 1712 MB, throughput 0.840068
Recommendation: one client; give it all the memory
Reading from 23820: heap size 1717 MB, throughput 0.774452
Reading from 23820: heap size 1747 MB, throughput 0.812495
Reading from 23820: heap size 1763 MB, throughput 0.856128
Client 23820 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
