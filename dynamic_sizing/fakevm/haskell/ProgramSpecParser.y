{
module ProgramSpecParser (parseProgramSpec) where

import qualified ProgramSpec as P
import ProgramSpecLexer
}

%name parseProgramSpecInternal
%tokentype { Token }
%error { parseError }

%token
  INTEGER    { Integer $$ }
  FLOAT      { Float $$ }
  STRING     { String $$ }
  INDENT     { Indent }
  PHASE      { Phase }
  MINHEAP    { Minheap }
  TRANSITION { Transition }

%%

program:
    program phase                        { $1 { P.phases = (P.phases $1) ++ [$2] } }
  | program minHeap                      { $1 { P.minHeapMB = $2 } }
  |                                      { P.Program [] 1 }

phase:
    phaseLine transitionLines            { $1 { P.transitions = $2 } }

transitionLines:
    transitionLines transitionLine       { $1 ++ [$2] }
  | transitionLine                       { [$1] }

phaseLine:
    PHASE STRING INTEGER FLOAT INTEGER   { P.Phase $2 $3 $4 $5 [] }
  | PHASE STRING INTEGER INTEGER INTEGER { P.Phase $2 $3 (fromIntegral $4) $5 [] }
  | PHASE STRING INTEGER FLOAT           { P.Phase $2 $3 $4 0 [] }
  | PHASE STRING INTEGER INTEGER         { P.Phase $2 $3 (fromIntegral $4) 0 [] }

transitionLine:
    INDENT TRANSITION STRING INTEGER     { P.Transition $3 $4 }

minHeap:
    MINHEAP INTEGER                      { $2 }

{
parseError = error "Parse error"

parseProgramSpec :: String -> Maybe P.Program
parseProgramSpec s = let tokens = alexScanTokens s
                         prog = parseProgramSpecInternal tokens in
                     if P.programIsValid prog
                     then Just prog
                     else Nothing
}
