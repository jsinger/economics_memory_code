economemd
    total memory: 7338 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub15_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 15408: heap size 9 MB, throughput 0.983361
Clients: 1
Client 15408 has a minimum heap size of 12 MB
Reading from 15407: heap size 9 MB, throughput 0.991931
Clients: 2
Client 15407 has a minimum heap size of 1211 MB
Reading from 15407: heap size 9 MB, throughput 0.961028
Reading from 15408: heap size 9 MB, throughput 0.985374
Reading from 15407: heap size 9 MB, throughput 0.97622
Reading from 15407: heap size 9 MB, throughput 0.946869
Reading from 15408: heap size 11 MB, throughput 0.985441
Reading from 15407: heap size 11 MB, throughput 0.97877
Reading from 15408: heap size 11 MB, throughput 0.978377
Reading from 15407: heap size 11 MB, throughput 0.98423
Reading from 15408: heap size 15 MB, throughput 0.981409
Reading from 15407: heap size 17 MB, throughput 0.835085
Reading from 15408: heap size 15 MB, throughput 0.982016
Reading from 15407: heap size 17 MB, throughput 0.51752
Reading from 15407: heap size 29 MB, throughput 0.974066
Reading from 15407: heap size 30 MB, throughput 0.802414
Reading from 15408: heap size 24 MB, throughput 0.899824
Reading from 15407: heap size 35 MB, throughput 0.459148
Reading from 15407: heap size 48 MB, throughput 0.765776
Reading from 15408: heap size 30 MB, throughput 0.987127
Reading from 15407: heap size 51 MB, throughput 0.351888
Reading from 15407: heap size 66 MB, throughput 0.789355
Reading from 15407: heap size 73 MB, throughput 0.836409
Reading from 15408: heap size 36 MB, throughput 0.989175
Reading from 15408: heap size 42 MB, throughput 0.98561
Reading from 15407: heap size 73 MB, throughput 0.237328
Reading from 15407: heap size 103 MB, throughput 0.766125
Reading from 15408: heap size 43 MB, throughput 0.986713
Reading from 15408: heap size 46 MB, throughput 0.983603
Reading from 15407: heap size 104 MB, throughput 0.257214
Reading from 15407: heap size 134 MB, throughput 0.702546
Reading from 15408: heap size 52 MB, throughput 0.978884
Reading from 15407: heap size 135 MB, throughput 0.829146
Reading from 15408: heap size 52 MB, throughput 0.985158
Reading from 15407: heap size 137 MB, throughput 0.757979
Reading from 15407: heap size 141 MB, throughput 0.818877
Reading from 15408: heap size 58 MB, throughput 0.987359
Reading from 15408: heap size 58 MB, throughput 0.982702
Reading from 15408: heap size 66 MB, throughput 0.637158
Reading from 15407: heap size 143 MB, throughput 0.14511
Reading from 15408: heap size 70 MB, throughput 0.9893
Reading from 15407: heap size 183 MB, throughput 0.687355
Reading from 15407: heap size 185 MB, throughput 0.749529
Reading from 15408: heap size 77 MB, throughput 0.995497
Reading from 15407: heap size 189 MB, throughput 0.694043
Reading from 15407: heap size 196 MB, throughput 0.816565
Reading from 15407: heap size 199 MB, throughput 0.755984
Reading from 15408: heap size 77 MB, throughput 0.996131
Reading from 15407: heap size 202 MB, throughput 0.126304
Reading from 15407: heap size 251 MB, throughput 0.58381
Reading from 15407: heap size 254 MB, throughput 0.611783
Reading from 15408: heap size 82 MB, throughput 0.990337
Reading from 15407: heap size 259 MB, throughput 0.645066
Reading from 15407: heap size 263 MB, throughput 0.562622
Reading from 15407: heap size 271 MB, throughput 0.55457
Reading from 15407: heap size 275 MB, throughput 0.467007
Reading from 15407: heap size 285 MB, throughput 0.4363
Reading from 15408: heap size 83 MB, throughput 0.995403
Reading from 15408: heap size 89 MB, throughput 0.9965
Reading from 15407: heap size 292 MB, throughput 0.115554
Reading from 15407: heap size 343 MB, throughput 0.437244
Reading from 15407: heap size 337 MB, throughput 0.57636
Reading from 15408: heap size 89 MB, throughput 0.996335
Reading from 15407: heap size 342 MB, throughput 0.112308
Reading from 15407: heap size 385 MB, throughput 0.525819
Reading from 15407: heap size 389 MB, throughput 0.793435
Reading from 15407: heap size 382 MB, throughput 0.787113
Reading from 15408: heap size 94 MB, throughput 0.996967
Reading from 15407: heap size 386 MB, throughput 0.658836
Reading from 15407: heap size 386 MB, throughput 0.688648
Reading from 15407: heap size 389 MB, throughput 0.635174
Reading from 15407: heap size 391 MB, throughput 0.606694
Reading from 15407: heap size 399 MB, throughput 0.592416
Reading from 15408: heap size 94 MB, throughput 0.996657
Reading from 15407: heap size 403 MB, throughput 0.51838
Reading from 15407: heap size 413 MB, throughput 0.553057
Reading from 15407: heap size 420 MB, throughput 0.522583
Reading from 15407: heap size 429 MB, throughput 0.485949
Reading from 15407: heap size 432 MB, throughput 0.450235
Reading from 15408: heap size 97 MB, throughput 0.996303
Reading from 15407: heap size 440 MB, throughput 0.314218
Equal recommendation: 3669 MB each
Reading from 15408: heap size 97 MB, throughput 0.994578
Reading from 15407: heap size 446 MB, throughput 0.0419476
Reading from 15408: heap size 100 MB, throughput 0.995768
Reading from 15407: heap size 503 MB, throughput 0.267886
Reading from 15408: heap size 100 MB, throughput 0.991847
Reading from 15407: heap size 490 MB, throughput 0.0677177
Reading from 15407: heap size 479 MB, throughput 0.4396
Reading from 15407: heap size 556 MB, throughput 0.489004
Reading from 15408: heap size 104 MB, throughput 0.994403
Reading from 15407: heap size 558 MB, throughput 0.507645
Reading from 15407: heap size 550 MB, throughput 0.518536
Reading from 15407: heap size 476 MB, throughput 0.57624
Reading from 15407: heap size 545 MB, throughput 0.556958
Reading from 15408: heap size 104 MB, throughput 0.994677
Reading from 15407: heap size 549 MB, throughput 0.0961649
Reading from 15407: heap size 599 MB, throughput 0.458945
Reading from 15408: heap size 107 MB, throughput 0.996536
Reading from 15407: heap size 605 MB, throughput 0.629451
Reading from 15407: heap size 597 MB, throughput 0.704712
Reading from 15407: heap size 602 MB, throughput 0.545827
Reading from 15407: heap size 603 MB, throughput 0.659769
Reading from 15408: heap size 107 MB, throughput 0.996349
Reading from 15407: heap size 607 MB, throughput 0.647445
Reading from 15407: heap size 610 MB, throughput 0.50431
Reading from 15407: heap size 617 MB, throughput 0.540278
Reading from 15407: heap size 621 MB, throughput 0.740443
Reading from 15408: heap size 110 MB, throughput 0.99703
Reading from 15407: heap size 630 MB, throughput 0.852149
Reading from 15408: heap size 110 MB, throughput 0.997176
Reading from 15407: heap size 632 MB, throughput 0.845364
Reading from 15407: heap size 645 MB, throughput 0.79379
Reading from 15408: heap size 113 MB, throughput 0.998008
Reading from 15407: heap size 648 MB, throughput 0.796837
Reading from 15408: heap size 113 MB, throughput 0.996948
Reading from 15408: heap size 115 MB, throughput 0.997015
Reading from 15407: heap size 662 MB, throughput 0.0732217
Reading from 15407: heap size 728 MB, throughput 0.271627
Reading from 15407: heap size 736 MB, throughput 0.35925
Reading from 15407: heap size 725 MB, throughput 0.304939
Reading from 15408: heap size 115 MB, throughput 0.997974
Reading from 15407: heap size 640 MB, throughput 0.496377
Reading from 15408: heap size 116 MB, throughput 0.997578
Reading from 15407: heap size 712 MB, throughput 0.111509
Equal recommendation: 3669 MB each
Reading from 15407: heap size 693 MB, throughput 0.144077
Reading from 15408: heap size 117 MB, throughput 0.996977
Reading from 15407: heap size 788 MB, throughput 0.311497
Reading from 15407: heap size 789 MB, throughput 0.281208
Reading from 15407: heap size 787 MB, throughput 0.351922
Reading from 15408: heap size 119 MB, throughput 0.997686
Reading from 15407: heap size 789 MB, throughput 0.0362896
Reading from 15407: heap size 875 MB, throughput 0.265575
Reading from 15407: heap size 879 MB, throughput 0.643741
Reading from 15407: heap size 885 MB, throughput 0.633074
Reading from 15407: heap size 888 MB, throughput 0.874593
Reading from 15408: heap size 119 MB, throughput 0.997737
Reading from 15407: heap size 887 MB, throughput 0.963337
Reading from 15407: heap size 706 MB, throughput 0.806285
Reading from 15407: heap size 871 MB, throughput 0.657377
Reading from 15407: heap size 709 MB, throughput 0.7695
Reading from 15407: heap size 858 MB, throughput 0.754862
Reading from 15407: heap size 710 MB, throughput 0.762243
Reading from 15407: heap size 847 MB, throughput 0.785316
Reading from 15407: heap size 708 MB, throughput 0.917899
Reading from 15407: heap size 838 MB, throughput 0.940382
Reading from 15408: heap size 120 MB, throughput 0.998493
Reading from 15407: heap size 710 MB, throughput 0.85425
Reading from 15407: heap size 825 MB, throughput 0.877002
Reading from 15407: heap size 708 MB, throughput 0.928194
Reading from 15407: heap size 813 MB, throughput 0.800382
Reading from 15407: heap size 717 MB, throughput 0.927413
Reading from 15407: heap size 800 MB, throughput 0.86765
Reading from 15407: heap size 722 MB, throughput 0.811919
Reading from 15408: heap size 120 MB, throughput 0.99806
Reading from 15407: heap size 794 MB, throughput 0.929486
Reading from 15407: heap size 715 MB, throughput 0.908528
Reading from 15407: heap size 794 MB, throughput 0.722706
Reading from 15407: heap size 798 MB, throughput 0.560761
Reading from 15407: heap size 802 MB, throughput 0.606715
Reading from 15408: heap size 121 MB, throughput 0.997679
Reading from 15407: heap size 804 MB, throughput 0.574902
Reading from 15407: heap size 809 MB, throughput 0.609614
Reading from 15407: heap size 810 MB, throughput 0.570728
Reading from 15408: heap size 122 MB, throughput 0.989206
Reading from 15407: heap size 812 MB, throughput 0.675531
Reading from 15407: heap size 813 MB, throughput 0.587014
Reading from 15407: heap size 813 MB, throughput 0.615251
Reading from 15408: heap size 124 MB, throughput 0.990943
Reading from 15407: heap size 814 MB, throughput 0.681565
Reading from 15407: heap size 809 MB, throughput 0.697944
Reading from 15407: heap size 813 MB, throughput 0.69557
Reading from 15408: heap size 124 MB, throughput 0.993177
Reading from 15407: heap size 806 MB, throughput 0.735744
Reading from 15407: heap size 810 MB, throughput 0.773427
Reading from 15408: heap size 128 MB, throughput 0.996778
Reading from 15407: heap size 806 MB, throughput 0.116096
Reading from 15407: heap size 900 MB, throughput 0.983263
Reading from 15407: heap size 908 MB, throughput 0.988293
Reading from 15408: heap size 129 MB, throughput 0.996427
Reading from 15408: heap size 132 MB, throughput 0.996317
Reading from 15407: heap size 911 MB, throughput 0.973917
Reading from 15408: heap size 132 MB, throughput 0.996506
Reading from 15407: heap size 915 MB, throughput 0.913214
Reading from 15407: heap size 916 MB, throughput 0.738274
Reading from 15407: heap size 922 MB, throughput 0.685253
Reading from 15407: heap size 930 MB, throughput 0.726585
Reading from 15407: heap size 937 MB, throughput 0.766056
Reading from 15407: heap size 942 MB, throughput 0.714405
Reading from 15408: heap size 136 MB, throughput 0.997465
Reading from 15407: heap size 950 MB, throughput 0.764199
Reading from 15407: heap size 952 MB, throughput 0.774685
Reading from 15407: heap size 960 MB, throughput 0.780921
Reading from 15407: heap size 960 MB, throughput 0.721285
Equal recommendation: 3669 MB each
Reading from 15407: heap size 968 MB, throughput 0.806162
Reading from 15408: heap size 136 MB, throughput 0.996493
Reading from 15407: heap size 968 MB, throughput 0.773278
Reading from 15407: heap size 972 MB, throughput 0.905052
Reading from 15407: heap size 974 MB, throughput 0.866715
Reading from 15408: heap size 139 MB, throughput 0.996525
Reading from 15407: heap size 969 MB, throughput 0.892753
Reading from 15407: heap size 974 MB, throughput 0.827165
Reading from 15407: heap size 983 MB, throughput 0.838942
Reading from 15407: heap size 985 MB, throughput 0.6639
Reading from 15407: heap size 994 MB, throughput 0.664001
Reading from 15407: heap size 1002 MB, throughput 0.666826
Reading from 15408: heap size 139 MB, throughput 0.997614
Reading from 15407: heap size 1013 MB, throughput 0.602313
Reading from 15407: heap size 1018 MB, throughput 0.659815
Reading from 15407: heap size 1031 MB, throughput 0.680091
Reading from 15407: heap size 1035 MB, throughput 0.655042
Reading from 15407: heap size 1048 MB, throughput 0.675392
Reading from 15408: heap size 141 MB, throughput 0.997352
Reading from 15407: heap size 1051 MB, throughput 0.647736
Reading from 15407: heap size 1065 MB, throughput 0.672525
Reading from 15407: heap size 1068 MB, throughput 0.658726
Reading from 15407: heap size 1084 MB, throughput 0.656217
Reading from 15408: heap size 141 MB, throughput 0.996939
Reading from 15407: heap size 1085 MB, throughput 0.780586
Reading from 15408: heap size 143 MB, throughput 0.996651
Reading from 15408: heap size 143 MB, throughput 0.997214
Reading from 15407: heap size 1097 MB, throughput 0.952914
Reading from 15408: heap size 146 MB, throughput 0.995563
Reading from 15407: heap size 1101 MB, throughput 0.96683
Reading from 15408: heap size 146 MB, throughput 0.997488
Reading from 15408: heap size 148 MB, throughput 0.998452
Reading from 15407: heap size 1108 MB, throughput 0.959081
Reading from 15408: heap size 148 MB, throughput 0.998405
Equal recommendation: 3669 MB each
Reading from 15408: heap size 150 MB, throughput 0.998498
Reading from 15407: heap size 1112 MB, throughput 0.955513
Reading from 15408: heap size 150 MB, throughput 0.998406
Reading from 15407: heap size 1107 MB, throughput 0.96238
Reading from 15408: heap size 152 MB, throughput 0.998688
Reading from 15408: heap size 152 MB, throughput 0.995154
Reading from 15408: heap size 154 MB, throughput 0.992678
Reading from 15408: heap size 154 MB, throughput 0.993135
Reading from 15407: heap size 1113 MB, throughput 0.963457
Reading from 15408: heap size 157 MB, throughput 0.99794
Reading from 15407: heap size 1100 MB, throughput 0.963431
Reading from 15408: heap size 157 MB, throughput 0.997595
Reading from 15408: heap size 160 MB, throughput 0.997744
Reading from 15407: heap size 1108 MB, throughput 0.963279
Reading from 15408: heap size 160 MB, throughput 0.99765
Reading from 15408: heap size 162 MB, throughput 0.997736
Reading from 15407: heap size 1094 MB, throughput 0.961177
Reading from 15408: heap size 163 MB, throughput 0.997682
Equal recommendation: 3669 MB each
Reading from 15407: heap size 1028 MB, throughput 0.959703
Reading from 15408: heap size 164 MB, throughput 0.997666
Reading from 15408: heap size 165 MB, throughput 0.997631
Reading from 15407: heap size 1089 MB, throughput 0.959285
Reading from 15408: heap size 167 MB, throughput 0.997379
Reading from 15408: heap size 167 MB, throughput 0.996867
Reading from 15407: heap size 1094 MB, throughput 0.962374
Reading from 15408: heap size 170 MB, throughput 0.997532
Reading from 15408: heap size 170 MB, throughput 0.99714
Reading from 15407: heap size 1091 MB, throughput 0.961576
Reading from 15408: heap size 172 MB, throughput 0.997436
Reading from 15407: heap size 1094 MB, throughput 0.960845
Reading from 15408: heap size 172 MB, throughput 0.997634
Reading from 15408: heap size 174 MB, throughput 0.998404
Reading from 15407: heap size 1097 MB, throughput 0.961395
Reading from 15408: heap size 174 MB, throughput 0.998567
Equal recommendation: 3669 MB each
Reading from 15408: heap size 176 MB, throughput 0.993249
Reading from 15408: heap size 177 MB, throughput 0.990603
Reading from 15407: heap size 1098 MB, throughput 0.961759
Reading from 15408: heap size 179 MB, throughput 0.997386
Reading from 15408: heap size 180 MB, throughput 0.997676
Reading from 15408: heap size 183 MB, throughput 0.997471
Reading from 15407: heap size 1102 MB, throughput 0.486908
Reading from 15408: heap size 183 MB, throughput 0.997556
Reading from 15408: heap size 186 MB, throughput 0.997982
Reading from 15407: heap size 1189 MB, throughput 0.948154
Reading from 15408: heap size 186 MB, throughput 0.997791
Reading from 15407: heap size 1189 MB, throughput 0.986057
Reading from 15408: heap size 189 MB, throughput 0.997939
Reading from 15408: heap size 189 MB, throughput 0.997304
Reading from 15407: heap size 1195 MB, throughput 0.984335
Equal recommendation: 3669 MB each
Reading from 15408: heap size 191 MB, throughput 0.997767
Reading from 15408: heap size 192 MB, throughput 0.997488
Reading from 15407: heap size 1202 MB, throughput 0.981173
Reading from 15408: heap size 194 MB, throughput 0.997574
Reading from 15407: heap size 1203 MB, throughput 0.977996
Reading from 15408: heap size 194 MB, throughput 0.997621
Reading from 15408: heap size 185 MB, throughput 0.99741
Reading from 15407: heap size 1197 MB, throughput 0.977843
Reading from 15408: heap size 177 MB, throughput 0.923138
Reading from 15408: heap size 172 MB, throughput 0.996645
Reading from 15408: heap size 177 MB, throughput 0.991498
Reading from 15407: heap size 1115 MB, throughput 0.966112
Reading from 15408: heap size 183 MB, throughput 0.996296
Reading from 15408: heap size 188 MB, throughput 0.997997
Reading from 15407: heap size 1187 MB, throughput 0.972298
Equal recommendation: 3669 MB each
Reading from 15408: heap size 191 MB, throughput 0.998176
Reading from 15408: heap size 193 MB, throughput 0.99773
Reading from 15407: heap size 1132 MB, throughput 0.967124
Reading from 15408: heap size 197 MB, throughput 0.998297
Reading from 15407: heap size 1187 MB, throughput 0.968159
Reading from 15408: heap size 197 MB, throughput 0.997937
Reading from 15408: heap size 201 MB, throughput 0.998131
Reading from 15407: heap size 1190 MB, throughput 0.960876
Reading from 15408: heap size 201 MB, throughput 0.997742
Reading from 15408: heap size 205 MB, throughput 0.997469
Reading from 15407: heap size 1192 MB, throughput 0.962153
Reading from 15408: heap size 205 MB, throughput 0.99794
Reading from 15407: heap size 1194 MB, throughput 0.964674
Equal recommendation: 3669 MB each
Reading from 15408: heap size 208 MB, throughput 0.998043
Reading from 15408: heap size 208 MB, throughput 0.997488
Reading from 15407: heap size 1197 MB, throughput 0.961741
Reading from 15408: heap size 211 MB, throughput 0.998025
Reading from 15408: heap size 211 MB, throughput 0.992882
Reading from 15407: heap size 1203 MB, throughput 0.947126
Reading from 15408: heap size 215 MB, throughput 0.99167
Reading from 15408: heap size 215 MB, throughput 0.995682
Reading from 15407: heap size 1207 MB, throughput 0.95806
Reading from 15408: heap size 222 MB, throughput 0.998149
Reading from 15408: heap size 222 MB, throughput 0.997803
Reading from 15407: heap size 1215 MB, throughput 0.955314
Reading from 15408: heap size 226 MB, throughput 0.99602
Reading from 15407: heap size 1222 MB, throughput 0.958205
Equal recommendation: 3669 MB each
Reading from 15408: heap size 227 MB, throughput 0.998063
Reading from 15408: heap size 232 MB, throughput 0.998265
Reading from 15407: heap size 1227 MB, throughput 0.956867
Reading from 15408: heap size 233 MB, throughput 0.997744
Reading from 15407: heap size 1234 MB, throughput 0.95933
Reading from 15408: heap size 238 MB, throughput 0.998185
Reading from 15408: heap size 238 MB, throughput 0.997757
Reading from 15407: heap size 1236 MB, throughput 0.956224
Reading from 15408: heap size 242 MB, throughput 0.998198
Reading from 15407: heap size 1243 MB, throughput 0.959721
Reading from 15408: heap size 242 MB, throughput 0.997991
Equal recommendation: 3669 MB each
Reading from 15408: heap size 245 MB, throughput 0.997406
Reading from 15407: heap size 1244 MB, throughput 0.951201
Reading from 15408: heap size 246 MB, throughput 0.99131
Reading from 15408: heap size 250 MB, throughput 0.997052
Reading from 15407: heap size 1251 MB, throughput 0.95819
Reading from 15408: heap size 250 MB, throughput 0.998192
Reading from 15408: heap size 255 MB, throughput 0.998083
Reading from 15407: heap size 1251 MB, throughput 0.956311
Reading from 15408: heap size 256 MB, throughput 0.99817
Reading from 15408: heap size 261 MB, throughput 0.997157
Equal recommendation: 3669 MB each
Reading from 15407: heap size 1257 MB, throughput 0.528863
Reading from 15408: heap size 261 MB, throughput 0.998055
Reading from 15408: heap size 266 MB, throughput 0.998138
Reading from 15407: heap size 1360 MB, throughput 0.943479
Reading from 15408: heap size 266 MB, throughput 0.997741
Reading from 15407: heap size 1362 MB, throughput 0.977881
Reading from 15408: heap size 270 MB, throughput 0.998302
Reading from 15408: heap size 271 MB, throughput 0.998054
Reading from 15407: heap size 1366 MB, throughput 0.970739
Reading from 15408: heap size 275 MB, throughput 0.994811
Reading from 15408: heap size 275 MB, throughput 0.996315
Reading from 15407: heap size 1371 MB, throughput 0.977121
Equal recommendation: 3669 MB each
Reading from 15408: heap size 282 MB, throughput 0.998084
Reading from 15408: heap size 282 MB, throughput 0.997814
Reading from 15407: heap size 1373 MB, throughput 0.976592
Reading from 15408: heap size 287 MB, throughput 0.998139
Reading from 15408: heap size 288 MB, throughput 0.997868
Reading from 15408: heap size 292 MB, throughput 0.998309
Reading from 15408: heap size 293 MB, throughput 0.998042
Equal recommendation: 3669 MB each
Reading from 15408: heap size 297 MB, throughput 0.998307
Reading from 15408: heap size 297 MB, throughput 0.997887
Reading from 15408: heap size 302 MB, throughput 0.996276
Reading from 15408: heap size 302 MB, throughput 0.996193
Reading from 15407: heap size 1368 MB, throughput 0.990478
Reading from 15408: heap size 308 MB, throughput 0.998051
Reading from 15408: heap size 309 MB, throughput 0.99797
Equal recommendation: 3669 MB each
Reading from 15408: heap size 314 MB, throughput 0.998192
Reading from 15408: heap size 314 MB, throughput 0.997852
Reading from 15408: heap size 320 MB, throughput 0.998436
Reading from 15408: heap size 320 MB, throughput 0.998323
Reading from 15407: heap size 1373 MB, throughput 0.991759
Reading from 15408: heap size 325 MB, throughput 0.998625
Equal recommendation: 3669 MB each
Reading from 15408: heap size 325 MB, throughput 0.996991
Reading from 15408: heap size 328 MB, throughput 0.996565
Reading from 15408: heap size 329 MB, throughput 0.998193
Reading from 15407: heap size 1379 MB, throughput 0.985494
Reading from 15408: heap size 335 MB, throughput 0.998414
Reading from 15407: heap size 1393 MB, throughput 0.854171
Reading from 15407: heap size 1408 MB, throughput 0.75469
Reading from 15407: heap size 1424 MB, throughput 0.673076
Reading from 15407: heap size 1459 MB, throughput 0.704172
Reading from 15407: heap size 1482 MB, throughput 0.691087
Reading from 15407: heap size 1511 MB, throughput 0.711199
Reading from 15408: heap size 335 MB, throughput 0.997486
Reading from 15407: heap size 1524 MB, throughput 0.705932
Reading from 15407: heap size 1556 MB, throughput 0.779258
Equal recommendation: 3669 MB each
Reading from 15408: heap size 340 MB, throughput 0.997979
Reading from 15407: heap size 1562 MB, throughput 0.95646
Reading from 15408: heap size 340 MB, throughput 0.998048
Reading from 15407: heap size 1540 MB, throughput 0.970229
Reading from 15408: heap size 346 MB, throughput 0.998558
Reading from 15407: heap size 1558 MB, throughput 0.967603
Reading from 15408: heap size 346 MB, throughput 0.998433
Reading from 15408: heap size 350 MB, throughput 0.994349
Reading from 15407: heap size 1541 MB, throughput 0.969428
Reading from 15408: heap size 351 MB, throughput 0.997976
Equal recommendation: 3669 MB each
Reading from 15408: heap size 358 MB, throughput 0.998342
Reading from 15407: heap size 1556 MB, throughput 0.769692
Reading from 15408: heap size 359 MB, throughput 0.998178
Reading from 15407: heap size 1511 MB, throughput 0.993635
Reading from 15408: heap size 365 MB, throughput 0.998398
Reading from 15407: heap size 1519 MB, throughput 0.991028
Reading from 15408: heap size 365 MB, throughput 0.998182
Equal recommendation: 3669 MB each
Reading from 15408: heap size 370 MB, throughput 0.998367
Reading from 15407: heap size 1545 MB, throughput 0.987405
Reading from 15408: heap size 370 MB, throughput 0.998469
Reading from 15408: heap size 375 MB, throughput 0.843975
Reading from 15407: heap size 1551 MB, throughput 0.982616
Reading from 15408: heap size 382 MB, throughput 0.997962
Reading from 15407: heap size 1556 MB, throughput 0.983827
Reading from 15408: heap size 391 MB, throughput 0.998914
Equal recommendation: 3669 MB each
Reading from 15408: heap size 391 MB, throughput 0.999027
Reading from 15407: heap size 1561 MB, throughput 0.98337
Reading from 15408: heap size 398 MB, throughput 0.999054
Reading from 15407: heap size 1543 MB, throughput 0.981167
Reading from 15408: heap size 399 MB, throughput 0.998987
Reading from 15407: heap size 1368 MB, throughput 0.979932
Reading from 15408: heap size 405 MB, throughput 0.999118
Equal recommendation: 3669 MB each
Reading from 15408: heap size 406 MB, throughput 0.998436
Reading from 15407: heap size 1525 MB, throughput 0.977124
Reading from 15408: heap size 412 MB, throughput 0.995976
Reading from 15408: heap size 412 MB, throughput 0.998439
Reading from 15407: heap size 1399 MB, throughput 0.975913
Reading from 15408: heap size 421 MB, throughput 0.998579
Reading from 15407: heap size 1517 MB, throughput 0.974241
Reading from 15408: heap size 421 MB, throughput 0.998345
Equal recommendation: 3669 MB each
Reading from 15408: heap size 429 MB, throughput 0.998226
Reading from 15407: heap size 1526 MB, throughput 0.973469
Reading from 15408: heap size 429 MB, throughput 0.998333
Reading from 15407: heap size 1532 MB, throughput 0.971557
Reading from 15408: heap size 435 MB, throughput 0.998021
Reading from 15408: heap size 436 MB, throughput 0.996506
Reading from 15407: heap size 1532 MB, throughput 0.969488
Equal recommendation: 3669 MB each
Reading from 15408: heap size 444 MB, throughput 0.998473
Reading from 15407: heap size 1540 MB, throughput 0.965721
Reading from 15408: heap size 445 MB, throughput 0.998535
Reading from 15407: heap size 1546 MB, throughput 0.965631
Reading from 15408: heap size 453 MB, throughput 0.998562
Equal recommendation: 3669 MB each
Reading from 15408: heap size 453 MB, throughput 0.998433
Reading from 15407: heap size 1560 MB, throughput 0.964389
Reading from 15408: heap size 461 MB, throughput 0.998571
Reading from 15408: heap size 461 MB, throughput 0.994999
Reading from 15407: heap size 1567 MB, throughput 0.96515
Reading from 15408: heap size 466 MB, throughput 0.998544
Reading from 15407: heap size 1581 MB, throughput 0.966133
Equal recommendation: 3669 MB each
Reading from 15408: heap size 468 MB, throughput 0.998609
Reading from 15407: heap size 1585 MB, throughput 0.965632
Reading from 15408: heap size 475 MB, throughput 0.998654
Reading from 15408: heap size 476 MB, throughput 0.998247
Reading from 15407: heap size 1600 MB, throughput 0.965691
Reading from 15408: heap size 484 MB, throughput 0.998605
Equal recommendation: 3669 MB each
Reading from 15407: heap size 1603 MB, throughput 0.970013
Client 15408 died
Clients: 1
Reading from 15407: heap size 1617 MB, throughput 0.968863
Reading from 15407: heap size 1619 MB, throughput 0.968092
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 15407: heap size 1634 MB, throughput 0.995421
Reading from 15407: heap size 1634 MB, throughput 0.986317
Reading from 15407: heap size 1628 MB, throughput 0.849962
Reading from 15407: heap size 1637 MB, throughput 0.774392
Recommendation: one client; give it all the memory
Reading from 15407: heap size 1667 MB, throughput 0.314134
Reading from 15407: heap size 1759 MB, throughput 0.995383
Client 15407 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
