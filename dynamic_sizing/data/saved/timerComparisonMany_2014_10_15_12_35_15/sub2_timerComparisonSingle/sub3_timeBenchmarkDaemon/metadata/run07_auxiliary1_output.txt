economemd
    total memory: 4461 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub2_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 31256: heap size 9 MB, throughput 0.991393
Clients: 1
Client 31256 has a minimum heap size of 276 MB
Reading from 31255: heap size 9 MB, throughput 0.990701
Clients: 2
Client 31255 has a minimum heap size of 1211 MB
Reading from 31255: heap size 9 MB, throughput 0.987332
Reading from 31256: heap size 9 MB, throughput 0.988781
Reading from 31255: heap size 9 MB, throughput 0.981687
Reading from 31256: heap size 9 MB, throughput 0.982335
Reading from 31255: heap size 9 MB, throughput 0.970267
Reading from 31256: heap size 9 MB, throughput 0.96465
Reading from 31255: heap size 11 MB, throughput 0.971239
Reading from 31256: heap size 11 MB, throughput 0.969312
Reading from 31255: heap size 11 MB, throughput 0.966175
Reading from 31255: heap size 17 MB, throughput 0.949261
Reading from 31256: heap size 11 MB, throughput 0.978489
Reading from 31256: heap size 17 MB, throughput 0.957968
Reading from 31255: heap size 17 MB, throughput 0.884173
Reading from 31256: heap size 17 MB, throughput 0.905838
Reading from 31255: heap size 30 MB, throughput 0.803678
Reading from 31255: heap size 31 MB, throughput 0.450705
Reading from 31256: heap size 30 MB, throughput 0.843773
Reading from 31256: heap size 31 MB, throughput 0.525049
Reading from 31255: heap size 35 MB, throughput 0.699229
Reading from 31255: heap size 44 MB, throughput 0.2146
Reading from 31256: heap size 35 MB, throughput 0.680577
Reading from 31255: heap size 48 MB, throughput 0.0326177
Reading from 31256: heap size 47 MB, throughput 0.518848
Reading from 31256: heap size 47 MB, throughput 0.467545
Reading from 31255: heap size 50 MB, throughput 0.541448
Reading from 31255: heap size 69 MB, throughput 0.0397794
Reading from 31256: heap size 50 MB, throughput 0.454755
Reading from 31256: heap size 74 MB, throughput 0.4797
Reading from 31255: heap size 71 MB, throughput 0.377985
Reading from 31255: heap size 97 MB, throughput 0.250553
Reading from 31256: heap size 74 MB, throughput 0.386423
Reading from 31255: heap size 98 MB, throughput 0.749909
Reading from 31256: heap size 100 MB, throughput 0.753187
Reading from 31256: heap size 100 MB, throughput 0.660289
Reading from 31255: heap size 101 MB, throughput 0.792525
Reading from 31256: heap size 102 MB, throughput 0.452432
Reading from 31255: heap size 105 MB, throughput 0.775638
Reading from 31256: heap size 104 MB, throughput 0.488026
Reading from 31256: heap size 140 MB, throughput 0.583261
Reading from 31255: heap size 107 MB, throughput 0.546047
Reading from 31256: heap size 140 MB, throughput 0.514279
Reading from 31255: heap size 134 MB, throughput 0.657075
Reading from 31255: heap size 145 MB, throughput 0.694126
Reading from 31256: heap size 143 MB, throughput 0.275147
Reading from 31255: heap size 145 MB, throughput 0.67614
Reading from 31256: heap size 147 MB, throughput 0.3179
Reading from 31255: heap size 149 MB, throughput 0.664197
Reading from 31255: heap size 153 MB, throughput 0.676445
Reading from 31256: heap size 150 MB, throughput 0.389655
Reading from 31256: heap size 191 MB, throughput 0.399069
Reading from 31255: heap size 158 MB, throughput 0.422531
Reading from 31256: heap size 195 MB, throughput 0.266367
Reading from 31255: heap size 193 MB, throughput 0.691378
Reading from 31256: heap size 198 MB, throughput 0.0389104
Reading from 31255: heap size 201 MB, throughput 0.640178
Reading from 31255: heap size 206 MB, throughput 0.611945
Reading from 31255: heap size 212 MB, throughput 0.566775
Reading from 31256: heap size 207 MB, throughput 0.882423
Reading from 31255: heap size 218 MB, throughput 0.326296
Reading from 31255: heap size 266 MB, throughput 0.59882
Reading from 31255: heap size 268 MB, throughput 0.673739
Reading from 31255: heap size 270 MB, throughput 0.671244
Reading from 31256: heap size 209 MB, throughput 0.623713
Reading from 31255: heap size 271 MB, throughput 0.672259
Reading from 31256: heap size 265 MB, throughput 0.884289
Reading from 31255: heap size 274 MB, throughput 0.607675
Reading from 31256: heap size 266 MB, throughput 0.877811
Reading from 31256: heap size 269 MB, throughput 0.85261
Reading from 31256: heap size 271 MB, throughput 0.818539
Reading from 31256: heap size 275 MB, throughput 0.830745
Reading from 31256: heap size 276 MB, throughput 0.718025
Reading from 31255: heap size 281 MB, throughput 0.408711
Reading from 31256: heap size 279 MB, throughput 0.722898
Reading from 31256: heap size 283 MB, throughput 0.694117
Reading from 31255: heap size 330 MB, throughput 0.554824
Reading from 31256: heap size 290 MB, throughput 0.753287
Reading from 31255: heap size 337 MB, throughput 0.583294
Reading from 31255: heap size 338 MB, throughput 0.611704
Reading from 31256: heap size 293 MB, throughput 0.864023
Reading from 31255: heap size 343 MB, throughput 0.617218
Reading from 31256: heap size 298 MB, throughput 0.870127
Reading from 31256: heap size 299 MB, throughput 0.788471
Reading from 31255: heap size 349 MB, throughput 0.35952
Reading from 31256: heap size 306 MB, throughput 0.795771
Reading from 31256: heap size 309 MB, throughput 0.727202
Reading from 31255: heap size 397 MB, throughput 0.541887
Reading from 31256: heap size 309 MB, throughput 0.813945
Reading from 31255: heap size 399 MB, throughput 0.511204
Reading from 31255: heap size 402 MB, throughput 0.517086
Reading from 31256: heap size 312 MB, throughput 0.875103
Reading from 31256: heap size 316 MB, throughput 0.796219
Reading from 31255: heap size 407 MB, throughput 0.55091
Reading from 31256: heap size 317 MB, throughput 0.780467
Reading from 31256: heap size 320 MB, throughput 0.712273
Reading from 31256: heap size 321 MB, throughput 0.597075
Reading from 31256: heap size 329 MB, throughput 0.645362
Reading from 31255: heap size 409 MB, throughput 0.306149
Reading from 31256: heap size 329 MB, throughput 0.889816
Reading from 31255: heap size 464 MB, throughput 0.497506
Reading from 31255: heap size 466 MB, throughput 0.524952
Numeric result:
Recommendation: 2 clients, utility 0.340821:
    h1: 283.451 MB (U(h) = 0.64895*h^0.001)
    h2: 4177.55 MB (U(h) = 0.461643*h^0.0147906)
Recommendation: 2 clients, utility 0.340821:
    h1: 282.509 MB (U(h) = 0.64895*h^0.001)
    h2: 4178.49 MB (U(h) = 0.461643*h^0.0147906)
Reading from 31255: heap size 469 MB, throughput 0.542165
Reading from 31255: heap size 469 MB, throughput 0.507221
Reading from 31255: heap size 475 MB, throughput 0.545927
Reading from 31256: heap size 336 MB, throughput 0.961779
Reading from 31255: heap size 478 MB, throughput 0.300904
Reading from 31255: heap size 545 MB, throughput 0.496762
Reading from 31255: heap size 548 MB, throughput 0.55396
Reading from 31256: heap size 300 MB, throughput 0.957117
Reading from 31255: heap size 553 MB, throughput 0.220481
Reading from 31255: heap size 617 MB, throughput 0.52393
Reading from 31255: heap size 625 MB, throughput 0.642048
Reading from 31256: heap size 328 MB, throughput 0.816522
Reading from 31255: heap size 628 MB, throughput 0.649329
Reading from 31255: heap size 630 MB, throughput 0.65719
Reading from 31255: heap size 640 MB, throughput 0.637422
Reading from 31256: heap size 337 MB, throughput 0.959034
Reading from 31255: heap size 644 MB, throughput 0.824428
Reading from 31255: heap size 661 MB, throughput 0.819053
Reading from 31256: heap size 366 MB, throughput 0.973397
Reading from 31255: heap size 670 MB, throughput 0.833794
Reading from 31255: heap size 690 MB, throughput 0.734429
Reading from 31255: heap size 698 MB, throughput 0.644462
Reading from 31256: heap size 319 MB, throughput 0.977873
Reading from 31255: heap size 714 MB, throughput 0.531664
Reading from 31255: heap size 795 MB, throughput 0.392991
Reading from 31255: heap size 801 MB, throughput 0.359575
Reading from 31256: heap size 362 MB, throughput 0.975667
Numeric result:
Recommendation: 2 clients, utility 0.39788:
    h1: 2315.83 MB (U(h) = 0.532202*h^0.0386731)
    h2: 2145.17 MB (U(h) = 0.42083*h^0.0358537)
Recommendation: 2 clients, utility 0.39788:
    h1: 2314.88 MB (U(h) = 0.532202*h^0.0386731)
    h2: 2146.12 MB (U(h) = 0.42083*h^0.0358537)
Reading from 31255: heap size 785 MB, throughput 0.080607
Reading from 31255: heap size 737 MB, throughput 0.255706
Reading from 31255: heap size 867 MB, throughput 0.509354
Reading from 31256: heap size 321 MB, throughput 0.969092
Reading from 31255: heap size 868 MB, throughput 0.711089
Reading from 31255: heap size 857 MB, throughput 0.64539
Reading from 31256: heap size 362 MB, throughput 0.968548
Reading from 31255: heap size 863 MB, throughput 0.284695
Reading from 31255: heap size 946 MB, throughput 0.477006
Reading from 31255: heap size 949 MB, throughput 0.612824
Reading from 31255: heap size 955 MB, throughput 0.766223
Reading from 31256: heap size 361 MB, throughput 0.973096
Reading from 31255: heap size 955 MB, throughput 0.815692
Reading from 31255: heap size 961 MB, throughput 0.876401
Reading from 31255: heap size 961 MB, throughput 0.921634
Reading from 31255: heap size 950 MB, throughput 0.89451
Reading from 31255: heap size 740 MB, throughput 0.891403
Reading from 31256: heap size 356 MB, throughput 0.972109
Reading from 31255: heap size 938 MB, throughput 0.905987
Reading from 31255: heap size 786 MB, throughput 0.862189
Reading from 31255: heap size 906 MB, throughput 0.833139
Reading from 31255: heap size 790 MB, throughput 0.81174
Reading from 31255: heap size 890 MB, throughput 0.792252
Reading from 31255: heap size 777 MB, throughput 0.816061
Reading from 31256: heap size 359 MB, throughput 0.974303
Reading from 31255: heap size 884 MB, throughput 0.828414
Reading from 31255: heap size 782 MB, throughput 0.822031
Reading from 31255: heap size 875 MB, throughput 0.832871
Reading from 31255: heap size 785 MB, throughput 0.831192
Reading from 31255: heap size 871 MB, throughput 0.844291
Reading from 31256: heap size 358 MB, throughput 0.975251
Reading from 31255: heap size 876 MB, throughput 0.8475
Reading from 31255: heap size 868 MB, throughput 0.954669
Reading from 31256: heap size 359 MB, throughput 0.96856
Reading from 31255: heap size 872 MB, throughput 0.963378
Reading from 31256: heap size 361 MB, throughput 0.971064
Reading from 31255: heap size 874 MB, throughput 0.930725
Reading from 31255: heap size 876 MB, throughput 0.892867
Reading from 31255: heap size 876 MB, throughput 0.847484
Reading from 31255: heap size 877 MB, throughput 0.822327
Reading from 31255: heap size 879 MB, throughput 0.80727
Numeric result:
Recommendation: 2 clients, utility 0.551856:
    h1: 1718.89 MB (U(h) = 0.478646*h^0.0658518)
    h2: 2742.11 MB (U(h) = 0.30729*h^0.105063)
Recommendation: 2 clients, utility 0.551856:
    h1: 1718.78 MB (U(h) = 0.478646*h^0.0658518)
    h2: 2742.22 MB (U(h) = 0.30729*h^0.105063)
Reading from 31255: heap size 880 MB, throughput 0.79498
Reading from 31255: heap size 880 MB, throughput 0.795646
Reading from 31255: heap size 883 MB, throughput 0.787773
Reading from 31256: heap size 361 MB, throughput 0.9762
Reading from 31255: heap size 884 MB, throughput 0.798169
Reading from 31256: heap size 366 MB, throughput 0.95608
Reading from 31256: heap size 367 MB, throughput 0.920342
Reading from 31256: heap size 373 MB, throughput 0.876221
Reading from 31255: heap size 886 MB, throughput 0.827602
Reading from 31256: heap size 377 MB, throughput 0.822003
Reading from 31256: heap size 386 MB, throughput 0.818019
Reading from 31255: heap size 888 MB, throughput 0.866203
Reading from 31255: heap size 890 MB, throughput 0.853924
Reading from 31256: heap size 389 MB, throughput 0.948333
Reading from 31256: heap size 394 MB, throughput 0.968123
Reading from 31255: heap size 894 MB, throughput 0.685573
Reading from 31255: heap size 987 MB, throughput 0.615038
Reading from 31255: heap size 1008 MB, throughput 0.6711
Reading from 31255: heap size 1012 MB, throughput 0.722583
Reading from 31255: heap size 1010 MB, throughput 0.68728
Reading from 31255: heap size 1014 MB, throughput 0.664344
Reading from 31256: heap size 397 MB, throughput 0.978763
Reading from 31255: heap size 1021 MB, throughput 0.708953
Reading from 31255: heap size 1022 MB, throughput 0.692448
Reading from 31255: heap size 1036 MB, throughput 0.673829
Reading from 31255: heap size 1036 MB, throughput 0.678473
Reading from 31255: heap size 1052 MB, throughput 0.834141
Reading from 31256: heap size 396 MB, throughput 0.965123
Reading from 31256: heap size 399 MB, throughput 0.974653
Reading from 31255: heap size 1053 MB, throughput 0.923063
Numeric result:
Recommendation: 2 clients, utility 0.84655:
    h1: 1144.44 MB (U(h) = 0.421958*h^0.0969514)
    h2: 3316.56 MB (U(h) = 0.103939*h^0.280932)
Recommendation: 2 clients, utility 0.84655:
    h1: 1144.54 MB (U(h) = 0.421958*h^0.0969514)
    h2: 3316.46 MB (U(h) = 0.103939*h^0.280932)
Reading from 31256: heap size 397 MB, throughput 0.979776
Reading from 31255: heap size 1068 MB, throughput 0.941281
Reading from 31256: heap size 400 MB, throughput 0.982398
Reading from 31255: heap size 1072 MB, throughput 0.9461
Reading from 31256: heap size 397 MB, throughput 0.98147
Reading from 31255: heap size 1085 MB, throughput 0.94537
Reading from 31256: heap size 400 MB, throughput 0.981984
Reading from 31256: heap size 397 MB, throughput 0.977859
Reading from 31255: heap size 1091 MB, throughput 0.836035
Reading from 31256: heap size 399 MB, throughput 0.978684
Reading from 31255: heap size 1212 MB, throughput 0.969166
Reading from 31256: heap size 399 MB, throughput 0.974585
Numeric result:
Recommendation: 2 clients, utility 0.724368:
    h1: 1715.72 MB (U(h) = 0.41572*h^0.100579)
    h2: 2745.28 MB (U(h) = 0.230393*h^0.16093)
Recommendation: 2 clients, utility 0.724368:
    h1: 1715.75 MB (U(h) = 0.41572*h^0.100579)
    h2: 2745.25 MB (U(h) = 0.230393*h^0.16093)
Reading from 31255: heap size 1213 MB, throughput 0.972637
Reading from 31256: heap size 400 MB, throughput 0.981586
Reading from 31256: heap size 400 MB, throughput 0.971674
Reading from 31256: heap size 402 MB, throughput 0.952401
Reading from 31256: heap size 407 MB, throughput 0.931201
Reading from 31256: heap size 409 MB, throughput 0.944134
Reading from 31255: heap size 1226 MB, throughput 0.980666
Reading from 31256: heap size 418 MB, throughput 0.983133
Reading from 31255: heap size 1231 MB, throughput 0.983424
Reading from 31256: heap size 418 MB, throughput 0.986871
Reading from 31255: heap size 1234 MB, throughput 0.982475
Reading from 31256: heap size 421 MB, throughput 0.986573
Reading from 31255: heap size 1236 MB, throughput 0.980737
Reading from 31256: heap size 422 MB, throughput 0.987106
Numeric result:
Recommendation: 2 clients, utility 0.840259:
    h1: 1583.78 MB (U(h) = 0.386742*h^0.117952)
    h2: 2877.22 MB (U(h) = 0.165359*h^0.21427)
Recommendation: 2 clients, utility 0.840259:
    h1: 1583.83 MB (U(h) = 0.386742*h^0.117952)
    h2: 2877.17 MB (U(h) = 0.165359*h^0.21427)
Reading from 31255: heap size 1227 MB, throughput 0.977985
Reading from 31256: heap size 420 MB, throughput 0.986255
Reading from 31255: heap size 1143 MB, throughput 0.974636
Reading from 31256: heap size 423 MB, throughput 0.985515
Reading from 31255: heap size 1216 MB, throughput 0.972793
Reading from 31256: heap size 421 MB, throughput 0.985383
Reading from 31255: heap size 1223 MB, throughput 0.970558
Reading from 31256: heap size 423 MB, throughput 0.985188
Reading from 31255: heap size 1221 MB, throughput 0.969881
Reading from 31256: heap size 425 MB, throughput 0.984068
Numeric result:
Recommendation: 2 clients, utility 1.02267:
    h1: 1233.72 MB (U(h) = 0.374113*h^0.125865)
    h2: 3227.28 MB (U(h) = 0.0780431*h^0.32926)
Recommendation: 2 clients, utility 1.02267:
    h1: 1233.69 MB (U(h) = 0.374113*h^0.125865)
    h2: 3227.31 MB (U(h) = 0.0780431*h^0.32926)
Reading from 31255: heap size 1222 MB, throughput 0.967069
Reading from 31256: heap size 425 MB, throughput 0.987205
Reading from 31256: heap size 427 MB, throughput 0.979986
Reading from 31256: heap size 428 MB, throughput 0.965779
Reading from 31256: heap size 432 MB, throughput 0.946792
Reading from 31255: heap size 1226 MB, throughput 0.960517
Reading from 31256: heap size 434 MB, throughput 0.979567
Reading from 31255: heap size 1229 MB, throughput 0.95831
Reading from 31256: heap size 441 MB, throughput 0.986657
Reading from 31255: heap size 1233 MB, throughput 0.957418
Reading from 31256: heap size 442 MB, throughput 0.988217
Reading from 31255: heap size 1239 MB, throughput 0.956601
Numeric result:
Recommendation: 2 clients, utility 1.19728:
    h1: 1120.57 MB (U(h) = 0.354607*h^0.13849)
    h2: 3340.43 MB (U(h) = 0.0448058*h^0.412849)
Recommendation: 2 clients, utility 1.19728:
    h1: 1120.55 MB (U(h) = 0.354607*h^0.13849)
    h2: 3340.45 MB (U(h) = 0.0448058*h^0.412849)
Reading from 31256: heap size 444 MB, throughput 0.989533
Reading from 31255: heap size 1247 MB, throughput 0.958609
Reading from 31256: heap size 445 MB, throughput 0.988719
Reading from 31255: heap size 1253 MB, throughput 0.957077
Reading from 31256: heap size 444 MB, throughput 0.987195
Reading from 31255: heap size 1260 MB, throughput 0.956554
Reading from 31256: heap size 446 MB, throughput 0.98772
Reading from 31255: heap size 1264 MB, throughput 0.954889
Reading from 31256: heap size 446 MB, throughput 0.986705
Numeric result:
Recommendation: 2 clients, utility 1.32695:
    h1: 1040.79 MB (U(h) = 0.345955*h^0.144254)
    h2: 3420.21 MB (U(h) = 0.029738*h^0.474032)
Recommendation: 2 clients, utility 1.32695:
    h1: 1040.81 MB (U(h) = 0.345955*h^0.144254)
    h2: 3420.19 MB (U(h) = 0.029738*h^0.474032)
Reading from 31255: heap size 1271 MB, throughput 0.955921
Reading from 31256: heap size 447 MB, throughput 0.987774
Reading from 31256: heap size 448 MB, throughput 0.982895
Reading from 31256: heap size 449 MB, throughput 0.972005
Reading from 31256: heap size 453 MB, throughput 0.964001
Reading from 31255: heap size 1273 MB, throughput 0.957048
Reading from 31256: heap size 455 MB, throughput 0.986136
Reading from 31255: heap size 1280 MB, throughput 0.955797
Reading from 31256: heap size 461 MB, throughput 0.989308
Reading from 31255: heap size 1281 MB, throughput 0.957063
Numeric result:
Recommendation: 2 clients, utility 1.57399:
    h1: 931.829 MB (U(h) = 0.332268*h^0.153612)
    h2: 3529.17 MB (U(h) = 0.0143003*h^0.581802)
Recommendation: 2 clients, utility 1.57399:
    h1: 931.804 MB (U(h) = 0.332268*h^0.153612)
    h2: 3529.2 MB (U(h) = 0.0143003*h^0.581802)
Reading from 31256: heap size 461 MB, throughput 0.990588
Reading from 31255: heap size 1288 MB, throughput 0.956737
Reading from 31256: heap size 462 MB, throughput 0.989213
Reading from 31255: heap size 1289 MB, throughput 0.95619
Reading from 31256: heap size 463 MB, throughput 0.988558
Reading from 31255: heap size 1294 MB, throughput 0.958476
Reading from 31256: heap size 462 MB, throughput 0.987776
Reading from 31255: heap size 1295 MB, throughput 0.958905
Reading from 31256: heap size 463 MB, throughput 0.987246
Numeric result:
Recommendation: 2 clients, utility 1.84932:
    h1: 816.413 MB (U(h) = 0.328114*h^0.156499)
    h2: 3644.59 MB (U(h) = 0.00641143*h^0.698643)
Recommendation: 2 clients, utility 1.84932:
    h1: 816.405 MB (U(h) = 0.328114*h^0.156499)
    h2: 3644.59 MB (U(h) = 0.00641143*h^0.698643)
Reading from 31255: heap size 1300 MB, throughput 0.958677
Reading from 31256: heap size 465 MB, throughput 0.989011
Reading from 31256: heap size 466 MB, throughput 0.983765
Reading from 31256: heap size 465 MB, throughput 0.975528
Reading from 31256: heap size 496 MB, throughput 0.975663
Reading from 31256: heap size 499 MB, throughput 0.987041
Reading from 31255: heap size 1301 MB, throughput 0.93333
Reading from 31256: heap size 504 MB, throughput 0.991397
Reading from 31255: heap size 1417 MB, throughput 0.952092
Numeric result:
Recommendation: 2 clients, utility 1.939:
    h1: 817.251 MB (U(h) = 0.319087*h^0.162783)
    h2: 3643.75 MB (U(h) = 0.00530518*h^0.725776)
Recommendation: 2 clients, utility 1.939:
    h1: 817.249 MB (U(h) = 0.319087*h^0.162783)
    h2: 3643.75 MB (U(h) = 0.00530518*h^0.725776)
Reading from 31256: heap size 509 MB, throughput 0.991518
Reading from 31255: heap size 1417 MB, throughput 0.96528
Reading from 31256: heap size 510 MB, throughput 0.990917
Reading from 31255: heap size 1430 MB, throughput 0.97263
Reading from 31256: heap size 506 MB, throughput 0.990023
Reading from 31255: heap size 1435 MB, throughput 0.975356
Reading from 31256: heap size 509 MB, throughput 0.988517
Numeric result:
Recommendation: 2 clients, utility 2.05629:
    h1: 794.379 MB (U(h) = 0.314096*h^0.166273)
    h2: 3666.62 MB (U(h) = 0.00396715*h^0.767437)
Recommendation: 2 clients, utility 2.05629:
    h1: 794.404 MB (U(h) = 0.314096*h^0.166273)
    h2: 3666.6 MB (U(h) = 0.00396715*h^0.767437)
Reading from 31255: heap size 1438 MB, throughput 0.97654
Reading from 31256: heap size 511 MB, throughput 0.986414
Reading from 31256: heap size 511 MB, throughput 0.983358
Reading from 31256: heap size 514 MB, throughput 0.974208
Reading from 31256: heap size 519 MB, throughput 0.948797
Reading from 31255: heap size 1441 MB, throughput 0.974234
Reading from 31256: heap size 532 MB, throughput 0.981109
Reading from 31255: heap size 1430 MB, throughput 0.97378
Reading from 31256: heap size 531 MB, throughput 0.986678
Numeric result:
Recommendation: 2 clients, utility 2.10374:
    h1: 1011.46 MB (U(h) = 0.234414*h^0.217583)
    h2: 3449.54 MB (U(h) = 0.00472101*h^0.742036)
Recommendation: 2 clients, utility 2.10374:
    h1: 1011.48 MB (U(h) = 0.234414*h^0.217583)
    h2: 3449.52 MB (U(h) = 0.00472101*h^0.742036)
Reading from 31256: heap size 530 MB, throughput 0.987987
Reading from 31256: heap size 534 MB, throughput 0.987165
Reading from 31256: heap size 540 MB, throughput 0.987598
Reading from 31256: heap size 541 MB, throughput 0.986899
Numeric result:
Recommendation: 2 clients, utility 2.91369:
    h1: 1699.3 MB (U(h) = 0.0578199*h^0.456573)
    h2: 2761.7 MB (U(h) = 0.00472101*h^0.742036)
Recommendation: 2 clients, utility 2.91369:
    h1: 1699.28 MB (U(h) = 0.0578199*h^0.456573)
    h2: 2761.72 MB (U(h) = 0.00472101*h^0.742036)
Reading from 31256: heap size 546 MB, throughput 0.989334
Reading from 31256: heap size 548 MB, throughput 0.982806
Reading from 31256: heap size 548 MB, throughput 0.97224
Reading from 31255: heap size 1436 MB, throughput 0.987208
Reading from 31256: heap size 550 MB, throughput 0.985267
Reading from 31256: heap size 557 MB, throughput 0.989523
Numeric result:
Recommendation: 2 clients, utility 4.2075:
    h1: 2114.8 MB (U(h) = 0.0153846*h^0.67916)
    h2: 2346.2 MB (U(h) = 0.00435591*h^0.75347)
Recommendation: 2 clients, utility 4.2075:
    h1: 2114.8 MB (U(h) = 0.0153846*h^0.67916)
    h2: 2346.2 MB (U(h) = 0.00435591*h^0.75347)
Reading from 31256: heap size 560 MB, throughput 0.990892
Reading from 31256: heap size 560 MB, throughput 0.99019
Reading from 31256: heap size 563 MB, throughput 0.990117
Reading from 31255: heap size 1401 MB, throughput 0.98875
Numeric result:
Recommendation: 2 clients, utility 4.95591:
    h1: 2237.86 MB (U(h) = 0.0087708*h^0.772283)
    h2: 2223.14 MB (U(h) = 0.00395616*h^0.7672)
Recommendation: 2 clients, utility 4.95591:
    h1: 2237.87 MB (U(h) = 0.0087708*h^0.772283)
    h2: 2223.13 MB (U(h) = 0.00395616*h^0.7672)
Reading from 31256: heap size 563 MB, throughput 0.988728
Reading from 31256: heap size 565 MB, throughput 0.987576
Reading from 31256: heap size 568 MB, throughput 0.981004
Reading from 31255: heap size 1436 MB, throughput 0.985463
Reading from 31256: heap size 569 MB, throughput 0.986262
Reading from 31255: heap size 1446 MB, throughput 0.972704
Reading from 31255: heap size 1474 MB, throughput 0.948128
Reading from 31255: heap size 1482 MB, throughput 0.912058
Reading from 31255: heap size 1518 MB, throughput 0.853549
Reading from 31255: heap size 1548 MB, throughput 0.789644
Reading from 31255: heap size 1570 MB, throughput 0.728245
Reading from 31255: heap size 1608 MB, throughput 0.953603
Reading from 31256: heap size 576 MB, throughput 0.991082
Reading from 31255: heap size 1632 MB, throughput 0.843072
Numeric result:
Recommendation: 2 clients, utility 4.20491:
    h1: 2583.4 MB (U(h) = 0.00881622*h^0.770057)
    h2: 1877.6 MB (U(h) = 0.0165456*h^0.559692)
Recommendation: 2 clients, utility 4.20491:
    h1: 2583.36 MB (U(h) = 0.00881622*h^0.770057)
    h2: 1877.64 MB (U(h) = 0.0165456*h^0.559692)
Reading from 31255: heap size 1652 MB, throughput 0.966701
Reading from 31256: heap size 577 MB, throughput 0.991681
Reading from 31255: heap size 1663 MB, throughput 0.978154
Reading from 31256: heap size 578 MB, throughput 0.991317
Reading from 31255: heap size 1681 MB, throughput 0.981168
Reading from 31256: heap size 580 MB, throughput 0.990288
Reading from 31255: heap size 1688 MB, throughput 0.98119
Numeric result:
Recommendation: 2 clients, utility 3.32932:
    h1: 2290.26 MB (U(h) = 0.0204589*h^0.630426)
    h2: 2170.74 MB (U(h) = 0.0125779*h^0.597525)
Recommendation: 2 clients, utility 3.32932:
    h1: 2290.26 MB (U(h) = 0.0204589*h^0.630426)
    h2: 2170.74 MB (U(h) = 0.0125779*h^0.597525)
Reading from 31256: heap size 579 MB, throughput 0.989123
Reading from 31255: heap size 1674 MB, throughput 0.980398
Reading from 31256: heap size 581 MB, throughput 0.986902
Reading from 31256: heap size 588 MB, throughput 0.980456
Reading from 31255: heap size 1689 MB, throughput 0.97688
Reading from 31256: heap size 589 MB, throughput 0.987727
Reading from 31255: heap size 1655 MB, throughput 0.975805
Reading from 31256: heap size 594 MB, throughput 0.990706
Numeric result:
Recommendation: 2 clients, utility 2.08318:
    h1: 1453.45 MB (U(h) = 0.150925*h^0.301609)
    h2: 3007.55 MB (U(h) = 0.0103618*h^0.624092)
Recommendation: 2 clients, utility 2.08318:
    h1: 1453.47 MB (U(h) = 0.150925*h^0.301609)
    h2: 3007.53 MB (U(h) = 0.0103618*h^0.624092)
Reading from 31255: heap size 1385 MB, throughput 0.976799
Reading from 31256: heap size 596 MB, throughput 0.99172
Reading from 31255: heap size 1629 MB, throughput 0.973848
Reading from 31256: heap size 596 MB, throughput 0.990946
Reading from 31255: heap size 1418 MB, throughput 0.971909
Reading from 31256: heap size 598 MB, throughput 0.990575
Numeric result:
Recommendation: 2 clients, utility 1.92095:
    h1: 1614.49 MB (U(h) = 0.152404*h^0.299773)
    h2: 2846.51 MB (U(h) = 0.0205676*h^0.528514)
Recommendation: 2 clients, utility 1.92095:
    h1: 1614.52 MB (U(h) = 0.152404*h^0.299773)
    h2: 2846.48 MB (U(h) = 0.0205676*h^0.528514)
Reading from 31255: heap size 1604 MB, throughput 0.970177
Reading from 31256: heap size 599 MB, throughput 0.990161
Reading from 31256: heap size 600 MB, throughput 0.984577
Reading from 31256: heap size 603 MB, throughput 0.987269
Reading from 31255: heap size 1451 MB, throughput 0.966673
Reading from 31256: heap size 604 MB, throughput 0.991502
Reading from 31255: heap size 1601 MB, throughput 0.966913
Numeric result:
Recommendation: 2 clients, utility 1.63457:
    h1: 1666.92 MB (U(h) = 0.213767*h^0.244623)
    h2: 2794.08 MB (U(h) = 0.0481099*h^0.410034)
Recommendation: 2 clients, utility 1.63457:
    h1: 1666.92 MB (U(h) = 0.213767*h^0.244623)
    h2: 2794.08 MB (U(h) = 0.0481099*h^0.410034)
Reading from 31255: heap size 1611 MB, throughput 0.965188
Reading from 31256: heap size 608 MB, throughput 0.992497
Reading from 31255: heap size 1615 MB, throughput 0.963364
Reading from 31256: heap size 609 MB, throughput 0.991981
Reading from 31255: heap size 1616 MB, throughput 0.961973
Reading from 31256: heap size 609 MB, throughput 0.991139
Numeric result:
Recommendation: 2 clients, utility 1.50207:
    h1: 1880.3 MB (U(h) = 0.236147*h^0.228407)
    h2: 2580.7 MB (U(h) = 0.0969175*h^0.313404)
Recommendation: 2 clients, utility 1.50207:
    h1: 1880.59 MB (U(h) = 0.236147*h^0.228407)
    h2: 2580.41 MB (U(h) = 0.0969175*h^0.313404)
Reading from 31256: heap size 611 MB, throughput 0.982944
Reading from 31255: heap size 1630 MB, throughput 0.961895
Reading from 31256: heap size 615 MB, throughput 0.978981
Reading from 31256: heap size 615 MB, throughput 0.98484
Reading from 31255: heap size 1631 MB, throughput 0.959111
Reading from 31256: heap size 624 MB, throughput 0.991061
Reading from 31255: heap size 1645 MB, throughput 0.960988
Numeric result:
Recommendation: 2 clients, utility 1.44715:
    h1: 1750.42 MB (U(h) = 0.277441*h^0.202225)
    h2: 2710.58 MB (U(h) = 0.0969152*h^0.313162)
Recommendation: 2 clients, utility 1.44715:
    h1: 1750.38 MB (U(h) = 0.277441*h^0.202225)
    h2: 2710.62 MB (U(h) = 0.0969152*h^0.313162)
Reading from 31256: heap size 624 MB, throughput 0.992047
Reading from 31255: heap size 1646 MB, throughput 0.960096
Reading from 31256: heap size 624 MB, throughput 0.991556
Reading from 31255: heap size 1661 MB, throughput 0.959328
Reading from 31256: heap size 627 MB, throughput 0.991044
Numeric result:
Recommendation: 2 clients, utility 1.43398:
    h1: 1618.71 MB (U(h) = 0.305037*h^0.186948)
    h2: 2842.29 MB (U(h) = 0.0867658*h^0.328316)
Recommendation: 2 clients, utility 1.43398:
    h1: 1618.54 MB (U(h) = 0.305037*h^0.186948)
    h2: 2842.46 MB (U(h) = 0.0867658*h^0.328316)
Reading from 31255: heap size 1661 MB, throughput 0.959291
Reading from 31256: heap size 630 MB, throughput 0.989993
Reading from 31256: heap size 631 MB, throughput 0.986397
Reading from 31255: heap size 1677 MB, throughput 0.959899
Reading from 31256: heap size 636 MB, throughput 0.991836
Reading from 31255: heap size 1678 MB, throughput 0.95839
Reading from 31256: heap size 637 MB, throughput 0.992936
Numeric result:
Recommendation: 2 clients, utility 1.35378:
    h1: 1229.89 MB (U(h) = 0.441782*h^0.127488)
    h2: 3231.11 MB (U(h) = 0.0825891*h^0.334961)
Recommendation: 2 clients, utility 1.35378:
    h1: 1229.81 MB (U(h) = 0.441782*h^0.127488)
    h2: 3231.19 MB (U(h) = 0.0825891*h^0.334961)
Reading from 31255: heap size 1694 MB, throughput 0.96221
Reading from 31256: heap size 638 MB, throughput 0.992529
Reading from 31255: heap size 1695 MB, throughput 0.960663
Reading from 31256: heap size 640 MB, throughput 0.99217
Numeric result:
Recommendation: 2 clients, utility 1.34064:
    h1: 1090.6 MB (U(h) = 0.491616*h^0.11038)
    h2: 3370.4 MB (U(h) = 0.0789228*h^0.341069)
Recommendation: 2 clients, utility 1.34064:
    h1: 1090.72 MB (U(h) = 0.491616*h^0.11038)
    h2: 3370.28 MB (U(h) = 0.0789228*h^0.341069)
Reading from 31256: heap size 642 MB, throughput 0.992887
Reading from 31256: heap size 643 MB, throughput 0.987496
Client 31256 died
Clients: 1
Reading from 31255: heap size 1711 MB, throughput 0.956693
Reading from 31255: heap size 1779 MB, throughput 0.977548
Recommendation: one client; give it all the memory
Reading from 31255: heap size 1784 MB, throughput 0.984609
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 31255: heap size 1797 MB, throughput 0.99518
Reading from 31255: heap size 1804 MB, throughput 0.993509
Reading from 31255: heap size 1676 MB, throughput 0.986447
Reading from 31255: heap size 1790 MB, throughput 0.975488
Reading from 31255: heap size 1809 MB, throughput 0.955117
Client 31255 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
