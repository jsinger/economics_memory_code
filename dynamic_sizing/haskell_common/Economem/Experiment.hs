{-# LANGUAGE LambdaCase #-}

module Economem.Experiment (SubexpType(..),
                            SubexpInfo(..),
                            seqSubexpInfos,
                            ExperimentNum(..),
                            topLevel,
                            ExperimentDir(..),
                            newExperimentProperties,
                            Experiment(..),
                            getSubexpDirByNum,
                            getAllSubexpDirs,
                            makeExperiment,
                            addToIntro,
                            addStaticMetadataSaver,
                            resumeExperiment,
                            runExperiment) where

import Prelude hiding(log, exp)
import Control.Applicative
import Control.Concurrent
import Control.Conditional
import Control.Exception
import Control.Monad
import Data.List
import Data.Time.Clock
import Economem.Common
import Economem.Files
import qualified Economem.Log as L
import Economem.MetadataDir
import Economem.Process
import Economem.Records
import Economem.Run
import Economem.Signals
import System.Directory
import System.Exit
import Text.Printf


data ExperimentNum = ExperimentNum Integer Integer
instance Pretty ExperimentNum where
  pretty sn = prettyInt (getNum sn) (getMax sn)
instance NumAndMax ExperimentNum where
  makeNumAndMax = ExperimentNum
  getNum (ExperimentNum n _) = n
  getMax (ExperimentNum _ m) = m

topLevel :: ExperimentNum
topLevel = ExperimentNum 1 1


data SubexpType = Static | Dynamic

data SubexpInfo = SubexpInfo { subexpNum :: ExperimentNum,
                               subexpParentDir :: ExperimentDir,
                               subexpParentLog :: L.Log,
                               subexpType :: SubexpType }

seqSubexpInfos :: SubexpType -> [a] -> Experiment -> [SubexpInfo]
seqSubexpInfos subType input e =
  map (\n -> SubexpInfo { subexpNum = ExperimentNum n numSubexps,
                          subexpParentDir = experimentDir e,
                          subexpParentLog = experimentLog e,
                          subexpType = subType })
  [1..numSubexps]
  where
    numSubexps = fromIntegral (length input)


data ExperimentDir = ExperimentDir { experimentPath :: FilePath,
                                     metadataDir :: MetadataDir }
instance Path ExperimentDir where
  getPath = experimentPath

instance Dir ExperimentDir where
  asDir path =
    ExperimentDir { experimentPath = getPath path,
                    metadataDir = asDir (path </> "metadata") }

newExperimentProperties :: String -> Maybe SubexpInfo -> IO (ExperimentDir, ExperimentNum, Maybe L.Log)
newExperimentProperties name info = do
    expDir <- case info of
      Nothing -> makeTimestampedExperimentDir
      Just i -> makeSubexperimentDir i
    let num = case info of
          Nothing -> topLevel
          Just i -> subexpNum i
    let parentLog = case info of
          Nothing -> Nothing
          Just i -> Just (subexpParentLog i)
    return (expDir, num, parentLog)

  where
    makeTimestampedExperimentDir :: IO ExperimentDir
    makeTimestampedExperimentDir = do
      parent <- dataRoot
      s <- nowPathSafe
      asAbsDir (parent </> (name ++ "_" ++ s))

    makeSubexperimentDir :: SubexpInfo -> IO ExperimentDir
    makeSubexperimentDir i =
      asAbsDir
      (subexpParentDir i </>
       (printf "%s%d_%s" (subexpPrefix (subexpType i)) (getNum (subexpNum i)) name :: FilePath))

getSubexpDirByNum :: ExperimentDir -> SubexpType -> Integer -> IO (Maybe ExperimentDir)
getSubexpDirByNum d subType num = do
  let p = subexpPrefix subType ++ show num ++ "_"
  (filter (p `isPrefixOf`) <$> getDirectoryContents (getPath d)) >>=
    filterM (\s -> doesDirectoryExist (d </> s)) >>= \case
    [] -> return Nothing
    (x:_) -> return (Just (asDir (d </> x)))

getAllSubexpDirs :: ExperimentDir -> SubexpType -> IO [ExperimentDir]
getAllSubexpDirs d subType =
  takeWhileM (getSubexpDirByNum d subType) [1..]
  where
    takeWhileM :: Monad m => (a -> m (Maybe b)) -> [a] -> m [b]
    takeWhileM _ [] = return []
    takeWhileM f (x:xs) =
      f x >>= \case
        Just dir -> do
          rest <- takeWhileM f xs
          return (dir:rest)
        Nothing -> return []

subexpPrefix :: SubexpType -> String
subexpPrefix Static = "sub"
subexpPrefix Dynamic = "dynamic_sub"

data Experiment =
  Experiment { experimentName :: String,
               experimentNum :: ExperimentNum,
               experimentIntro :: [String],
               experimentRuns :: Integer,
               experimentDir :: ExperimentDir,
               experimentLog :: L.Log,
               saveAdditionalStaticMetadata :: IO (),
               canResume :: IO Bool,
               setup :: Experiment -> IO (),
               createRun :: Experiment -> RunNum -> IO Run,
               createSubexperiments :: Experiment -> IO [Experiment],
               subexperimentCompleted :: Experiment -> -- the main experiment
                                         Experiment -> -- the subexperiment that just completed
                                         IO (),
               resumeDynamicSubexperiment :: Experiment -> FilePath -> IO Experiment,
               nextDynamicSubexperiment :: Experiment -> Integer -> IO (Maybe Experiment),
               dynamicSubexperimentCompleted :: Experiment -> -- the main experiment
                                                Experiment -> -- the subexperiment that just completed
                                                IO (),
               cleanup :: IO () }

data RunningExperiment =
  RunningExperiment { thisExperiment :: Experiment,
                      thisRuns :: [Run],
                      thisSubexperiments :: [RunningExperiment],
                      signalHandler :: SignalHandler }

makeExperiment :: String -> ExperimentDir -> ExperimentNum -> Integer -> Maybe L.Log -> Experiment
makeExperiment name dir num runs parentLog =
  Experiment { experimentName = name,
               experimentNum = num,
               experimentIntro = [],
               experimentRuns = runs,
               experimentDir = dir,
               experimentLog = L.Log { L.logName = name,
                                       L.logFile = logFile (metadataDir dir),
                                       L.logParent = parentLog },
               saveAdditionalStaticMetadata = return (),
               canResume = return True,
               setup = \_ -> return (),
               createRun = undefined,
               createSubexperiments = \_ -> return [],
               subexperimentCompleted = \_ _ -> return (),
               resumeDynamicSubexperiment = undefined,
               nextDynamicSubexperiment = \_ _ -> return Nothing,
               dynamicSubexperimentCompleted = \_ _ -> return (),
               cleanup = return () }

addToIntro :: Experiment -> [String] -> Experiment
addToIntro e ss = e { experimentIntro = experimentIntro e ++ ss }

addStaticMetadataSaver :: Experiment -> IO () -> Experiment
addStaticMetadataSaver e f =
  let currentSaver = saveAdditionalStaticMetadata e in
  e { saveAdditionalStaticMetadata = do currentSaver
                                        f }

resumeExperiment :: (ExperimentDir -> IO Experiment) -> FilePath -> IO Experiment
resumeExperiment f path =
  catch
  (asAbsDir path >>= f)
  (\ex -> do putStrLn "Cannot resume; required data not present:"
             print (ex :: IOError)
             exitFailure)


runExperiment :: Experiment -> SignalHandler -> IO ()
runExperiment e h =
  catch
  (do rExp <- initialiseExperiment e h
      catch (runInternal rExp) (catchSignal rExp))
  catchException

  where
    catchSignal :: RunningExperiment -> ExitCode -> IO ()
    catchSignal rExp _ = do
      log rExp ""
      log rExp ""
      log rExp "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
      log rExp "Experiment killed by a signal, terminating"
      log rExp ""
      log rExp ""
      exitSuccess

    catchException :: IOError -> IO ()
    catchException ex = do
      let l = experimentLog e
      ifM (not <$> exists l)
        (print ex)
        (do L.putLog l ""
            L.putLog l ""
            L.putLog l "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            L.putLog l "!!!!!!!!!!!!!!!!   ERROR   !!!!!!!!!!!!!!!"
            L.putLog l "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
            L.putLog l "Experiment terminated because of an error:"
            L.putLog l (show ex)
            L.putLog l ""
            L.putLog l "")
      exitFailure

initialiseExperiment :: Experiment -> SignalHandler -> IO RunningExperiment
initialiseExperiment e h = do
  resuming <- exists (argsFile (metadataDir (experimentDir e)))
  (runs, subexperiments) <- if resuming then
                              initialiseResume
                            else
                              initialiseNew
  return (RunningExperiment e runs subexperiments h)
  where
    initialiseNew :: IO ([Run], [RunningExperiment])
    initialiseNew = do
      runs <- mapM ((createRun e) e) (upTo (experimentRuns e))
      subexperiments <- (createSubexperiments e) e
      saveStaticMetadata (metadataDir (experimentDir e)) (experimentName e) runs (length subexperiments)
      saveAdditionalStaticMetadata e
      runningSubexps <- mapM (`initialiseExperiment` h) subexperiments
      return (runs, runningSubexps)

    initialiseResume :: IO ([Run], [RunningExperiment])
    initialiseResume =
      ifM (canResume e)
        (do lastCompletedRun <- get (lastCompletedRunFile (metadataDir (experimentDir e)))
            lastCompletedSubexperiment <- get (lastCompletedSubexperimentFile (metadataDir (experimentDir e)))
            runs <- drop (fromIntegral (lastCompletedRun::Integer))
                    <$> get (argsFile (metadataDir (experimentDir e)))
            subexperiments <- (drop (fromIntegral (lastCompletedSubexperiment::Integer))
                               <$> (createSubexperiments e) e) >>= mapM (`initialiseExperiment` h)
            return (runs, subexperiments))
        (fail "Unable to resume experiment")

runInternal :: RunningExperiment -> IO ()
runInternal e =
  ifM (isCompleted (metadataDir (experimentDir (thisExperiment e))))
    (putStrLn "This experiment is already complete. Nothing to do here.")
    (do startTime <- getCurrentTime
        mainSetup e
        doRuns e
        doSubexperiments e
        doDynamicSubexperiments e
        mainCleanup e
        runTime <- timeSince startTime
        put (totalRuntimeFile (metadataDir (experimentDir (thisExperiment e))))
          (Runtime (realToFrac runTime::Double))
        log e ("Completed in total time " ++ show runTime)
        markCompleted (metadataDir (experimentDir (thisExperiment e))))


mainSetup :: RunningExperiment -> IO ()
mainSetup e = do
  generateDynamicMetadata (metadataDir (experimentDir (thisExperiment e)))
  log e "Initialising..."
  (setup (thisExperiment e)) (thisExperiment e)
  log e "Initialised"
  log e ""
  mapM_ (log e) (experimentIntro (thisExperiment e))
  log e ""

mainCleanup :: RunningExperiment -> IO ()
mainCleanup e = do
  log e "Cleaning up..."
  cleanup (thisExperiment e)
  log e "Cleaned up"
  log e ""

doRuns :: RunningExperiment -> IO ()
doRuns e =
  case runs of
    [] -> return ()
    _ -> do
      log e (if length runs /= totalRuns then
               "Resumed main process runs"
             else
               "Started main process runs")
      log e ""
      mapM_ (doOneRun e) runs
      logAverageRuntime mainRuntimes "main process runs" e
      log e ""
  where
    runs = thisRuns e
    totalRuns = fromIntegral (experimentRuns (thisExperiment e))

doOneRun :: RunningExperiment -> Run -> IO ()
doOneRun e run = do
  log e (printf "Run %s: started" prettyRun)
  prepareRun run
  runTime <- bracket
             startAuxiliaryProcesses
             cleanupAuxiliaryProcesses
             (\_ -> runMainProcesses)
  cleanupRun run
  clearProcesses (signalHandler e)
  put (lastCompletedRunFile (metadataDir (experimentDir (thisExperiment e)))) (getNum (runNum run))
  log e (printf "Run %s: completed with main runtime of %s" prettyRun (show runTime))
  log e ""
  where
    prettyRun = numOf (runNum run)

    startAuxiliaryProcesses :: IO [RunningProcess]
    startAuxiliaryProcesses =
      case auxiliaryProcesses run of
        [] -> return []
        ps -> do log e "Starting auxiliary processes..."
                 mapM_ prepareProcess ps
                 running <- mapM start ps
                 mapM_ (killProcessOnSignal (signalHandler e)) running
                 threadDelay (round (auxiliaryProcessDelay run) * 1000000) -- microseconds
                 log e "Started auxiliary processes"
                 return running

    cleanupAuxiliaryProcesses :: [RunningProcess] -> IO ()
    cleanupAuxiliaryProcesses [] = return ()
    cleanupAuxiliaryProcesses ps = do
      log e "Cleaning up auxiliary processes..."
      threadDelay (round (auxiliaryProcessDelay run) * 1000000) -- microseconds
      mapM_ kill ps
      mapM_ wait ps
      mapM_ cleanupProcess ps
      log e "Cleaned up auxiliary processes"

    runMainProcesses :: IO NominalDiffTime
    runMainProcesses =
      case mainProcesses run of
        [] -> do now <- getCurrentTime
                 return (diffUTCTime now now)
        ps -> do log e "Running main processes..."
                 mapM_ prepareProcess ps
                 startTime <- getCurrentTime
                 running <- actuallyRun ps
                 runTime <- timeSince startTime
                 mapM_ cleanupProcess running
                 append (timesFile (mainRuntimes (metadataDir (experimentDir (thisExperiment e)))))
                   (Runtime (realToFrac runTime))
                 log e ("Main processes completed in " ++ show runTime)
                 return runTime
      where
        actuallyRun :: [ProcessLauncher] -> IO [RunningProcess]
        actuallyRun ps =
          bracket
            (do running <- mapM start ps
                mapM_ (killProcessOnSignal (signalHandler e)) running
                return running)
            (mapM_ wait)
            (\running -> do case runType run of
                              RunToCompletion -> return ()
                              KillAfter t -> do threadDelay (round t * 1000000) -- microseconds
                                                mapM_ kill running
                            return running)


doSubexperiments :: RunningExperiment -> IO ()
doSubexperiments e =
  case subexps of
    [] -> return ()
    _ -> do
      totalSubexps <- get (numSubexperimentsFile (metadataDir (experimentDir (thisExperiment e))))
      log e (if length subexps /= (fromIntegral (totalSubexps::Integer)::Int) then
               "Resumed sub-experiments"
             else
               "Started sub-experiments")
      log e ""
      mapM_ (doOneSubexperiment e) subexps
      logAverageRuntime subexperimentRuntimes "sub-experiments" e
      log e ""
  where
    subexps = thisSubexperiments e

doOneSubexperiment :: RunningExperiment -> RunningExperiment -> IO ()
doOneSubexperiment e subexp = do
  log e (printf "Running sub-experiment %s..." (numOf num))
  startTime <- getCurrentTime
  runInternal subexp
  runTime <- timeSince startTime
  append (timesFile (subexperimentRuntimes (metadataDir (experimentDir (thisExperiment e)))))
    (Runtime (realToFrac runTime))
  (subexperimentCompleted (thisExperiment e)) (thisExperiment e) (thisExperiment subexp)
  put (lastCompletedSubexperimentFile (metadataDir (experimentDir (thisExperiment e))))
    (getNum num)
  log e (printf "Sub-experiment %s completed in %s" (numOf num) (show runTime))
  log e ""
  where
    num = experimentNum (thisExperiment subexp)

doDynamicSubexperiments :: RunningExperiment -> IO ()
doDynamicSubexperiments e =
  somethingToDo >>= \case
    Nothing -> return ()
    Just (ex, resuming) -> do
      log e (if resuming then
               "Resumed dynamic sub-experiments"
             else
               "Started dynamic sub-experiments")
      log e ""
      doDynamicSubexperiment e ex
      logAverageRuntime dynamicSubexpRuntimes "dynamic sub-experiments" e
      log e ""

  where
    somethingToDo :: IO (Maybe (Experiment, Bool))
    somethingToDo = somethingToResume >>= \case
      Just path -> (\exp -> Just (exp, True))
                   <$> (resumeDynamicSubexperiment (thisExperiment e)) (thisExperiment e) path
      Nothing -> do
        lastCompleted <- get (lastCompletedDynamicSubexperimentFile (metadataDir (experimentDir (thisExperiment e))))
        (nextDynamicSubexperiment (thisExperiment e)) (thisExperiment e) (lastCompleted + 1) >>= \case
          Nothing -> return Nothing
          Just exp -> return (Just (exp, False))

    somethingToResume :: IO (Maybe FilePath)
    somethingToResume = do
      lastCompleted <- get (lastCompletedDynamicSubexperimentFile (metadataDir (experimentDir (thisExperiment e))))
      highestExistingDynamicSubexperiment (experimentDir (thisExperiment e)) >>= \case
        Nothing -> return Nothing
        Just (i, dir) -> return (if i > lastCompleted then
                           Just dir
                         else
                           Nothing)

    highestExistingDynamicSubexperiment :: ExperimentDir -> IO (Maybe (Integer, FilePath))
    highestExistingDynamicSubexperiment d = check 1 >>= \case
      Nothing -> return Nothing
      Just i -> return (Just i)
      where
        check :: Integer -> IO (Maybe (Integer, FilePath))
        check i =
          getSubexpDirByNum d Dynamic i >>= \case
            Nothing -> return Nothing
            Just subDir -> check (i + 1) >>= \case
              Nothing -> return (Just (i, getPath subDir))
              Just x -> return (Just x)

doDynamicSubexperiment :: RunningExperiment -> Experiment -> IO ()
doDynamicSubexperiment e subExp = do
  log e (printf "Running dynamic sub-experiment %d..." (getNum num))
  running <- initialiseExperiment subExp (signalHandler e)
  startTime <- getCurrentTime
  runInternal running
  runTime <- timeSince startTime
  append (timesFile (dynamicSubexpRuntimes (metadataDir (experimentDir (thisExperiment e)))))
    (Runtime (realToFrac runTime))
  (dynamicSubexperimentCompleted (thisExperiment e)) (thisExperiment e) (thisExperiment running)
  put (lastCompletedDynamicSubexperimentFile (metadataDir (experimentDir (thisExperiment e))))
    (getNum num)
  log e (printf "Dynamic sub-experiment %d completed in %s" (getNum num) (show runTime))
  log e ""
  (nextDynamicSubexperiment (thisExperiment e)) (thisExperiment e) (getNum num + 1) >>= \case
    Nothing -> return ()
    Just newExp -> doDynamicSubexperiment e newExp
  where
    num = experimentNum subExp

logAverageRuntime :: (MetadataDir -> TimingStats) -> String -> RunningExperiment -> IO ()
logAverageRuntime stats name e = do
  avgRuntime <- generateStats stats (metadataDir (experimentDir (thisExperiment e)))
  log e ("Completed " ++ name ++ "; average runtime " ++ pretty avgRuntime)

log :: RunningExperiment -> String -> IO ()
log e = L.putLog (experimentLog (thisExperiment e))
