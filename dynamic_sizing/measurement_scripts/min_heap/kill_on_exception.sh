#!/bin/bash
# Some benchmarks don't exit cleanly when they run out of memory; instead,
# they generate exceptions and then just hang. This script forcibly kills
# the benchmark if it gets into this situation.

function usage()
{
    echo "Usage: $(basename "${0}") pid_file vm_output_file does_it_crash_file"
    exit 1
}

test -z "${1}" && usage
test -z "${2}" && usage
test -z "${3}" && usage

PID_FILE="${1}"
VM_OUTPUT_FILE="${2}"
DOES_IT_CRASH_FILE="${3}"

function cleanup()
{
    test ! -z "${TAIL}" && kill "${TAIL}" &>/dev/null
    wait
    rm "${FIFO}"
    exit
}

trap "cleanup" SIGINT SIGTERM SIGHUP


touch "${PID_FILE}"
inotifywait -e modify "${PID_FILE}"
sleep 1
PID="$(cat "${PID_FILE}")"

FIFO="$(mktemp --dry-run)"
mkfifo "${FIFO}"

tail -n +1 -f "${VM_OUTPUT_FILE}" > "${FIFO}" &
TAIL="${!}"

while read line; do
    if echo "${line}" | grep -i 'Exception\|java\.lang\.OutOfMemoryError' &>/dev/null; then
        echo 'Killing...'
        echo True > "${DOES_IT_CRASH_FILE}"
        kill -9 "${PID}"
        echo 'Killed'
    fi
done < "${FIFO}"

wait
cleanup
