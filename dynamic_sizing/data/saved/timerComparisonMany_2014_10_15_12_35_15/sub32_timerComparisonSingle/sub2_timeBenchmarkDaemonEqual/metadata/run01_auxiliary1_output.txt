economemd
    total memory: 3705 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub32_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 13560: heap size 9 MB, throughput 0.989656
Clients: 1
Client 13560 has a minimum heap size of 12 MB
Reading from 13559: heap size 9 MB, throughput 0.990651
Clients: 2
Client 13559 has a minimum heap size of 1223 MB
Reading from 13560: heap size 9 MB, throughput 0.991023
Reading from 13559: heap size 9 MB, throughput 0.975227
Reading from 13560: heap size 9 MB, throughput 0.974108
Reading from 13559: heap size 9 MB, throughput 0.940241
Reading from 13559: heap size 9 MB, throughput 0.945973
Reading from 13560: heap size 9 MB, throughput 0.966557
Reading from 13559: heap size 11 MB, throughput 0.96929
Reading from 13560: heap size 11 MB, throughput 0.98009
Reading from 13559: heap size 11 MB, throughput 0.983467
Reading from 13560: heap size 11 MB, throughput 0.988307
Reading from 13559: heap size 17 MB, throughput 0.968459
Reading from 13560: heap size 16 MB, throughput 0.980414
Reading from 13559: heap size 17 MB, throughput 0.62004
Reading from 13560: heap size 16 MB, throughput 0.968109
Reading from 13559: heap size 30 MB, throughput 0.961157
Reading from 13559: heap size 32 MB, throughput 0.9309
Reading from 13559: heap size 33 MB, throughput 0.937208
Reading from 13559: heap size 35 MB, throughput 0.312749
Reading from 13560: heap size 23 MB, throughput 0.935806
Reading from 13559: heap size 50 MB, throughput 0.858808
Reading from 13559: heap size 50 MB, throughput 0.879384
Reading from 13560: heap size 29 MB, throughput 0.985115
Reading from 13559: heap size 52 MB, throughput 0.306585
Reading from 13559: heap size 70 MB, throughput 0.736259
Reading from 13559: heap size 74 MB, throughput 0.788024
Reading from 13560: heap size 34 MB, throughput 0.984548
Reading from 13559: heap size 76 MB, throughput 0.837998
Reading from 13560: heap size 38 MB, throughput 0.982806
Reading from 13559: heap size 78 MB, throughput 0.204563
Reading from 13559: heap size 108 MB, throughput 0.669759
Reading from 13560: heap size 42 MB, throughput 0.986247
Reading from 13559: heap size 111 MB, throughput 0.669414
Reading from 13559: heap size 112 MB, throughput 0.579644
Reading from 13560: heap size 42 MB, throughput 0.979568
Reading from 13559: heap size 119 MB, throughput 0.784597
Reading from 13560: heap size 49 MB, throughput 0.981508
Reading from 13560: heap size 49 MB, throughput 0.990563
Reading from 13559: heap size 120 MB, throughput 0.121644
Reading from 13559: heap size 159 MB, throughput 0.852136
Reading from 13560: heap size 57 MB, throughput 0.984446
Reading from 13559: heap size 161 MB, throughput 0.858263
Reading from 13560: heap size 58 MB, throughput 0.972629
Reading from 13559: heap size 162 MB, throughput 0.710869
Reading from 13559: heap size 165 MB, throughput 0.633367
Reading from 13560: heap size 66 MB, throughput 0.932162
Reading from 13559: heap size 173 MB, throughput 0.633953
Reading from 13560: heap size 66 MB, throughput 0.282732
Reading from 13559: heap size 176 MB, throughput 0.63058
Reading from 13560: heap size 81 MB, throughput 0.990584
Reading from 13559: heap size 180 MB, throughput 0.127174
Reading from 13559: heap size 225 MB, throughput 0.648204
Reading from 13560: heap size 81 MB, throughput 0.996847
Reading from 13559: heap size 232 MB, throughput 0.674344
Reading from 13559: heap size 234 MB, throughput 0.6604
Reading from 13559: heap size 237 MB, throughput 0.700844
Reading from 13560: heap size 98 MB, throughput 0.996658
Reading from 13559: heap size 244 MB, throughput 0.139869
Reading from 13559: heap size 293 MB, throughput 0.58015
Reading from 13559: heap size 297 MB, throughput 0.516077
Reading from 13559: heap size 298 MB, throughput 0.67389
Reading from 13559: heap size 298 MB, throughput 0.638819
Reading from 13559: heap size 302 MB, throughput 0.667283
Reading from 13560: heap size 99 MB, throughput 0.997052
Reading from 13559: heap size 308 MB, throughput 0.527463
Reading from 13559: heap size 313 MB, throughput 0.646732
Reading from 13560: heap size 118 MB, throughput 0.997427
Reading from 13559: heap size 319 MB, throughput 0.11187
Reading from 13559: heap size 370 MB, throughput 0.57352
Reading from 13559: heap size 375 MB, throughput 0.619313
Reading from 13559: heap size 377 MB, throughput 0.577069
Reading from 13559: heap size 381 MB, throughput 0.62124
Reading from 13559: heap size 387 MB, throughput 0.624591
Reading from 13560: heap size 120 MB, throughput 0.997537
Reading from 13559: heap size 392 MB, throughput 0.566167
Reading from 13560: heap size 133 MB, throughput 0.99793
Reading from 13559: heap size 398 MB, throughput 0.0737216
Reading from 13559: heap size 453 MB, throughput 0.562417
Reading from 13559: heap size 448 MB, throughput 0.609725
Equal recommendation: 1852 MB each
Reading from 13559: heap size 453 MB, throughput 0.517031
Reading from 13559: heap size 455 MB, throughput 0.581538
Reading from 13560: heap size 135 MB, throughput 0.99829
Reading from 13559: heap size 458 MB, throughput 0.461456
Reading from 13560: heap size 147 MB, throughput 0.998829
Reading from 13559: heap size 465 MB, throughput 0.112513
Reading from 13559: heap size 524 MB, throughput 0.458059
Reading from 13559: heap size 524 MB, throughput 0.578255
Reading from 13559: heap size 529 MB, throughput 0.551616
Reading from 13559: heap size 534 MB, throughput 0.534164
Reading from 13560: heap size 149 MB, throughput 0.997843
Reading from 13559: heap size 536 MB, throughput 0.104407
Reading from 13560: heap size 158 MB, throughput 0.998079
Reading from 13559: heap size 598 MB, throughput 0.405745
Reading from 13559: heap size 605 MB, throughput 0.564944
Reading from 13559: heap size 606 MB, throughput 0.572989
Reading from 13559: heap size 607 MB, throughput 0.52816
Reading from 13560: heap size 160 MB, throughput 0.998242
Reading from 13559: heap size 614 MB, throughput 0.11147
Reading from 13560: heap size 167 MB, throughput 0.995419
Reading from 13559: heap size 683 MB, throughput 0.491621
Reading from 13559: heap size 695 MB, throughput 0.891895
Reading from 13560: heap size 168 MB, throughput 0.998656
Reading from 13559: heap size 696 MB, throughput 0.89102
Reading from 13559: heap size 692 MB, throughput 0.698125
Reading from 13560: heap size 173 MB, throughput 0.998925
Reading from 13559: heap size 714 MB, throughput 0.724492
Reading from 13559: heap size 718 MB, throughput 0.507786
Reading from 13559: heap size 725 MB, throughput 0.361955
Reading from 13560: heap size 175 MB, throughput 0.998691
Equal recommendation: 1852 MB each
Reading from 13559: heap size 732 MB, throughput 0.0320362
Reading from 13559: heap size 809 MB, throughput 0.329506
Reading from 13560: heap size 180 MB, throughput 0.998434
Reading from 13559: heap size 799 MB, throughput 0.87494
Reading from 13559: heap size 807 MB, throughput 0.545642
Reading from 13559: heap size 797 MB, throughput 0.588463
Reading from 13559: heap size 802 MB, throughput 0.615173
Reading from 13559: heap size 803 MB, throughput 0.833211
Reading from 13560: heap size 180 MB, throughput 0.998752
Reading from 13560: heap size 184 MB, throughput 0.998694
Reading from 13559: heap size 805 MB, throughput 0.139697
Reading from 13559: heap size 892 MB, throughput 0.372482
Reading from 13560: heap size 184 MB, throughput 0.996545
Reading from 13559: heap size 893 MB, throughput 0.843621
Reading from 13560: heap size 188 MB, throughput 0.992636
Reading from 13559: heap size 900 MB, throughput 0.157447
Reading from 13559: heap size 998 MB, throughput 0.950502
Reading from 13560: heap size 188 MB, throughput 0.997176
Reading from 13559: heap size 1007 MB, throughput 0.876156
Reading from 13559: heap size 1008 MB, throughput 0.861321
Reading from 13559: heap size 1005 MB, throughput 0.839735
Reading from 13559: heap size 1009 MB, throughput 0.875325
Reading from 13559: heap size 1001 MB, throughput 0.887048
Reading from 13559: heap size 1006 MB, throughput 0.88391
Reading from 13559: heap size 995 MB, throughput 0.892289
Reading from 13559: heap size 862 MB, throughput 0.892557
Reading from 13560: heap size 195 MB, throughput 0.997949
Reading from 13559: heap size 988 MB, throughput 0.878113
Reading from 13560: heap size 196 MB, throughput 0.997633
Reading from 13559: heap size 868 MB, throughput 0.974466
Reading from 13559: heap size 981 MB, throughput 0.944078
Reading from 13559: heap size 987 MB, throughput 0.781718
Reading from 13560: heap size 201 MB, throughput 0.997909
Reading from 13559: heap size 994 MB, throughput 0.775833
Reading from 13559: heap size 1002 MB, throughput 0.772735
Reading from 13559: heap size 1010 MB, throughput 0.763496
Reading from 13559: heap size 906 MB, throughput 0.830304
Equal recommendation: 1852 MB each
Reading from 13559: heap size 996 MB, throughput 0.839402
Reading from 13559: heap size 1004 MB, throughput 0.840407
Reading from 13559: heap size 989 MB, throughput 0.849814
Reading from 13560: heap size 202 MB, throughput 0.996918
Reading from 13559: heap size 997 MB, throughput 0.900629
Reading from 13559: heap size 992 MB, throughput 0.896097
Reading from 13559: heap size 995 MB, throughput 0.884605
Reading from 13559: heap size 991 MB, throughput 0.816911
Reading from 13559: heap size 994 MB, throughput 0.660444
Reading from 13560: heap size 207 MB, throughput 0.998098
Reading from 13560: heap size 207 MB, throughput 0.997368
Reading from 13559: heap size 1003 MB, throughput 0.0732529
Reading from 13559: heap size 1130 MB, throughput 0.533869
Reading from 13559: heap size 1138 MB, throughput 0.788605
Reading from 13559: heap size 1139 MB, throughput 0.7804
Reading from 13559: heap size 1146 MB, throughput 0.771447
Reading from 13559: heap size 1148 MB, throughput 0.749112
Reading from 13559: heap size 1149 MB, throughput 0.757367
Reading from 13560: heap size 213 MB, throughput 0.998124
Reading from 13559: heap size 1153 MB, throughput 0.763992
Reading from 13559: heap size 1151 MB, throughput 0.739646
Reading from 13559: heap size 1156 MB, throughput 0.71513
Reading from 13560: heap size 213 MB, throughput 0.997273
Reading from 13559: heap size 1152 MB, throughput 0.971887
Reading from 13560: heap size 218 MB, throughput 0.99543
Reading from 13560: heap size 218 MB, throughput 0.997537
Equal recommendation: 1852 MB each
Reading from 13559: heap size 1159 MB, throughput 0.976612
Reading from 13560: heap size 223 MB, throughput 0.99816
Reading from 13560: heap size 223 MB, throughput 0.996013
Reading from 13560: heap size 229 MB, throughput 0.987561
Reading from 13559: heap size 1155 MB, throughput 0.973664
Reading from 13560: heap size 229 MB, throughput 0.996711
Reading from 13560: heap size 239 MB, throughput 0.998329
Reading from 13559: heap size 1162 MB, throughput 0.977617
Reading from 13560: heap size 239 MB, throughput 0.998046
Reading from 13560: heap size 246 MB, throughput 0.99826
Reading from 13559: heap size 1164 MB, throughput 0.978165
Equal recommendation: 1852 MB each
Reading from 13560: heap size 246 MB, throughput 0.998117
Reading from 13560: heap size 233 MB, throughput 0.995737
Reading from 13559: heap size 1168 MB, throughput 0.97031
Reading from 13560: heap size 223 MB, throughput 0.997709
Reading from 13560: heap size 213 MB, throughput 0.997853
Reading from 13560: heap size 203 MB, throughput 0.997448
Reading from 13559: heap size 1161 MB, throughput 0.970648
Reading from 13560: heap size 194 MB, throughput 0.997122
Reading from 13560: heap size 185 MB, throughput 0.99746
Reading from 13560: heap size 177 MB, throughput 0.996994
Reading from 13559: heap size 1167 MB, throughput 0.959413
Equal recommendation: 1852 MB each
Reading from 13560: heap size 170 MB, throughput 0.975859
Reading from 13560: heap size 180 MB, throughput 0.99181
Reading from 13560: heap size 192 MB, throughput 0.997612
Reading from 13560: heap size 199 MB, throughput 0.997219
Reading from 13559: heap size 1166 MB, throughput 0.977117
Reading from 13560: heap size 209 MB, throughput 0.997786
Reading from 13560: heap size 200 MB, throughput 0.998424
Reading from 13559: heap size 1168 MB, throughput 0.97579
Reading from 13560: heap size 191 MB, throughput 0.997869
Reading from 13560: heap size 183 MB, throughput 0.993462
Reading from 13560: heap size 175 MB, throughput 0.997169
Equal recommendation: 1852 MB each
Reading from 13560: heap size 167 MB, throughput 0.996969
Reading from 13559: heap size 1172 MB, throughput 0.976458
Reading from 13560: heap size 160 MB, throughput 0.997493
Reading from 13560: heap size 154 MB, throughput 0.996747
Reading from 13560: heap size 147 MB, throughput 0.997217
Reading from 13559: heap size 1173 MB, throughput 0.97815
Reading from 13560: heap size 141 MB, throughput 0.996189
Reading from 13560: heap size 135 MB, throughput 0.994949
Reading from 13560: heap size 130 MB, throughput 0.88287
Reading from 13560: heap size 126 MB, throughput 0.998115
Reading from 13560: heap size 130 MB, throughput 0.998496
Reading from 13559: heap size 1177 MB, throughput 0.954015
Reading from 13560: heap size 137 MB, throughput 0.995117
Reading from 13560: heap size 141 MB, throughput 0.991498
Reading from 13560: heap size 148 MB, throughput 0.99426
Reading from 13560: heap size 154 MB, throughput 0.996652
Equal recommendation: 1852 MB each
Reading from 13560: heap size 162 MB, throughput 0.997723
Reading from 13559: heap size 1180 MB, throughput 0.972703
Reading from 13560: heap size 169 MB, throughput 0.997754
Reading from 13560: heap size 176 MB, throughput 0.997505
Reading from 13560: heap size 183 MB, throughput 0.997058
Reading from 13559: heap size 1184 MB, throughput 0.978548
Reading from 13560: heap size 190 MB, throughput 0.996723
Reading from 13560: heap size 197 MB, throughput 0.997538
Reading from 13560: heap size 204 MB, throughput 0.997304
Reading from 13559: heap size 1188 MB, throughput 0.976759
Reading from 13560: heap size 205 MB, throughput 0.997909
Equal recommendation: 1852 MB each
Reading from 13560: heap size 205 MB, throughput 0.99782
Reading from 13560: heap size 212 MB, throughput 0.997612
Reading from 13559: heap size 1192 MB, throughput 0.97607
Reading from 13560: heap size 212 MB, throughput 0.99766
Reading from 13560: heap size 218 MB, throughput 0.997792
Reading from 13560: heap size 218 MB, throughput 0.996174
Reading from 13559: heap size 1197 MB, throughput 0.956187
Reading from 13560: heap size 224 MB, throughput 0.992517
Reading from 13560: heap size 224 MB, throughput 0.997151
Reading from 13560: heap size 234 MB, throughput 0.997669
Reading from 13559: heap size 1201 MB, throughput 0.973308
Equal recommendation: 1852 MB each
Reading from 13560: heap size 235 MB, throughput 0.997825
Reading from 13560: heap size 242 MB, throughput 0.99809
Reading from 13559: heap size 1204 MB, throughput 0.97414
Reading from 13560: heap size 243 MB, throughput 0.998031
Reading from 13560: heap size 251 MB, throughput 0.997932
Reading from 13559: heap size 1209 MB, throughput 0.974459
Reading from 13560: heap size 251 MB, throughput 0.997437
Reading from 13560: heap size 257 MB, throughput 0.997737
Equal recommendation: 1852 MB each
Reading from 13560: heap size 258 MB, throughput 0.997908
Reading from 13560: heap size 265 MB, throughput 0.997841
Reading from 13559: heap size 1210 MB, throughput 0.690562
Reading from 13560: heap size 265 MB, throughput 0.996087
Reading from 13560: heap size 272 MB, throughput 0.993704
Reading from 13559: heap size 1274 MB, throughput 0.99424
Reading from 13560: heap size 273 MB, throughput 0.997937
Reading from 13560: heap size 282 MB, throughput 0.998144
Reading from 13559: heap size 1275 MB, throughput 0.992886
Reading from 13560: heap size 283 MB, throughput 0.995598
Equal recommendation: 1852 MB each
Reading from 13560: heap size 293 MB, throughput 0.998368
Reading from 13559: heap size 1283 MB, throughput 0.991761
Reading from 13560: heap size 293 MB, throughput 0.998166
Reading from 13560: heap size 302 MB, throughput 0.997549
Reading from 13559: heap size 1288 MB, throughput 0.988662
Reading from 13560: heap size 302 MB, throughput 0.997594
Reading from 13560: heap size 312 MB, throughput 0.997485
Equal recommendation: 1852 MB each
Reading from 13560: heap size 312 MB, throughput 0.993388
Reading from 13559: heap size 1289 MB, throughput 0.983213
Reading from 13560: heap size 321 MB, throughput 0.997333
Reading from 13560: heap size 321 MB, throughput 0.998175
Reading from 13559: heap size 1177 MB, throughput 0.988116
Reading from 13560: heap size 331 MB, throughput 0.99793
Reading from 13559: heap size 1278 MB, throughput 0.986312
Reading from 13560: heap size 333 MB, throughput 0.998151
Equal recommendation: 1852 MB each
Reading from 13560: heap size 343 MB, throughput 0.997809
Reading from 13559: heap size 1194 MB, throughput 0.98477
Reading from 13560: heap size 343 MB, throughput 0.998311
Reading from 13560: heap size 352 MB, throughput 0.998279
Reading from 13559: heap size 1265 MB, throughput 0.985061
Reading from 13560: heap size 353 MB, throughput 0.998169
Reading from 13560: heap size 360 MB, throughput 0.919436
Reading from 13559: heap size 1213 MB, throughput 0.981913
Equal recommendation: 1852 MB each
Reading from 13560: heap size 368 MB, throughput 0.998998
Reading from 13560: heap size 382 MB, throughput 0.999219
Reading from 13559: heap size 1270 MB, throughput 0.979738
Reading from 13560: heap size 383 MB, throughput 0.999137
Reading from 13559: heap size 1271 MB, throughput 0.980217
Reading from 13560: heap size 392 MB, throughput 0.999293
Equal recommendation: 1852 MB each
Reading from 13560: heap size 394 MB, throughput 0.999055
Reading from 13559: heap size 1271 MB, throughput 0.981749
Reading from 13560: heap size 402 MB, throughput 0.999227
Reading from 13560: heap size 403 MB, throughput 0.997295
Reading from 13559: heap size 1275 MB, throughput 0.975463
Reading from 13560: heap size 410 MB, throughput 0.99832
Equal recommendation: 1852 MB each
Reading from 13559: heap size 1277 MB, throughput 0.979999
Reading from 13560: heap size 411 MB, throughput 0.998669
Reading from 13560: heap size 422 MB, throughput 0.998646
Reading from 13559: heap size 1283 MB, throughput 0.973598
Reading from 13560: heap size 422 MB, throughput 0.998665
Reading from 13559: heap size 1289 MB, throughput 0.973211
Reading from 13560: heap size 432 MB, throughput 0.998289
Equal recommendation: 1852 MB each
Reading from 13560: heap size 432 MB, throughput 0.998704
Reading from 13559: heap size 1294 MB, throughput 0.972851
Reading from 13560: heap size 442 MB, throughput 0.996018
Reading from 13560: heap size 442 MB, throughput 0.998288
Reading from 13559: heap size 1299 MB, throughput 0.973339
Reading from 13560: heap size 457 MB, throughput 0.998554
Reading from 13559: heap size 1302 MB, throughput 0.974916
Equal recommendation: 1852 MB each
Reading from 13560: heap size 457 MB, throughput 0.99808
Reading from 13559: heap size 1307 MB, throughput 0.975166
Reading from 13560: heap size 470 MB, throughput 0.998325
Reading from 13560: heap size 470 MB, throughput 0.996233
Reading from 13559: heap size 1308 MB, throughput 0.970037
Equal recommendation: 1852 MB each
Reading from 13560: heap size 483 MB, throughput 0.996981
Reading from 13559: heap size 1313 MB, throughput 0.976099
Reading from 13560: heap size 483 MB, throughput 0.997865
Reading from 13560: heap size 501 MB, throughput 0.998182
Reading from 13559: heap size 1313 MB, throughput 0.97337
Reading from 13560: heap size 502 MB, throughput 0.998519
Equal recommendation: 1852 MB each
Reading from 13560: heap size 516 MB, throughput 0.998579
Reading from 13560: heap size 517 MB, throughput 0.998429
Reading from 13560: heap size 531 MB, throughput 0.996359
Reading from 13559: heap size 1315 MB, throughput 0.989467
Equal recommendation: 1852 MB each
Reading from 13560: heap size 532 MB, throughput 0.998349
Reading from 13560: heap size 549 MB, throughput 0.998357
Reading from 13560: heap size 550 MB, throughput 0.998314
Equal recommendation: 1852 MB each
Reading from 13559: heap size 1316 MB, throughput 0.987546
Reading from 13560: heap size 565 MB, throughput 0.998628
Reading from 13560: heap size 566 MB, throughput 0.996585
Reading from 13560: heap size 581 MB, throughput 0.998482
Equal recommendation: 1852 MB each
Reading from 13559: heap size 1318 MB, throughput 0.888963
Reading from 13559: heap size 1404 MB, throughput 0.992632
Reading from 13559: heap size 1429 MB, throughput 0.926355
Reading from 13560: heap size 582 MB, throughput 0.998396
Reading from 13559: heap size 1289 MB, throughput 0.885574
Reading from 13559: heap size 1426 MB, throughput 0.883018
Reading from 13559: heap size 1431 MB, throughput 0.891006
Reading from 13559: heap size 1424 MB, throughput 0.894432
Reading from 13559: heap size 1430 MB, throughput 0.893596
Reading from 13559: heap size 1426 MB, throughput 0.902491
Reading from 13559: heap size 1432 MB, throughput 0.991376
Reading from 13560: heap size 598 MB, throughput 0.998433
Reading from 13559: heap size 1436 MB, throughput 0.995221
Reading from 13560: heap size 599 MB, throughput 0.998261
Equal recommendation: 1852 MB each
Reading from 13560: heap size 616 MB, throughput 0.996493
Reading from 13559: heap size 1444 MB, throughput 0.992815
Reading from 13560: heap size 616 MB, throughput 0.997792
Reading from 13559: heap size 1453 MB, throughput 0.990781
Equal recommendation: 1852 MB each
Reading from 13560: heap size 637 MB, throughput 0.998519
Reading from 13559: heap size 1456 MB, throughput 0.990105
Reading from 13560: heap size 638 MB, throughput 0.99834
Reading from 13559: heap size 1445 MB, throughput 0.99112
Client 13560 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 13559: heap size 1320 MB, throughput 0.989048
Reading from 13559: heap size 1432 MB, throughput 0.987621
Recommendation: one client; give it all the memory
Reading from 13559: heap size 1344 MB, throughput 0.986908
Reading from 13559: heap size 1426 MB, throughput 0.985848
Reading from 13559: heap size 1432 MB, throughput 0.984147
Recommendation: one client; give it all the memory
Reading from 13559: heap size 1434 MB, throughput 0.984503
Reading from 13559: heap size 1435 MB, throughput 0.983411
Recommendation: one client; give it all the memory
Reading from 13559: heap size 1438 MB, throughput 0.981415
Reading from 13559: heap size 1444 MB, throughput 0.98118
Recommendation: one client; give it all the memory
Reading from 13559: heap size 1448 MB, throughput 0.980453
Reading from 13559: heap size 1457 MB, throughput 0.979294
Recommendation: one client; give it all the memory
Reading from 13559: heap size 1464 MB, throughput 0.979291
Reading from 13559: heap size 1472 MB, throughput 0.977551
Recommendation: one client; give it all the memory
Reading from 13559: heap size 1479 MB, throughput 0.97785
Reading from 13559: heap size 1483 MB, throughput 0.977038
Reading from 13559: heap size 1490 MB, throughput 0.978633
Recommendation: one client; give it all the memory
Reading from 13559: heap size 1492 MB, throughput 0.978164
Reading from 13559: heap size 1499 MB, throughput 0.977781
Recommendation: one client; give it all the memory
Reading from 13559: heap size 1499 MB, throughput 0.976987
Reading from 13559: heap size 1504 MB, throughput 0.978662
Recommendation: one client; give it all the memory
Reading from 13559: heap size 1505 MB, throughput 0.977117
Reading from 13559: heap size 1509 MB, throughput 0.979674
Recommendation: one client; give it all the memory
Reading from 13559: heap size 1510 MB, throughput 0.978028
Reading from 13559: heap size 1512 MB, throughput 0.808774
Recommendation: one client; give it all the memory
Reading from 13559: heap size 1612 MB, throughput 0.99573
Reading from 13559: heap size 1604 MB, throughput 0.993816
Recommendation: one client; give it all the memory
Reading from 13559: heap size 1617 MB, throughput 0.993092
Reading from 13559: heap size 1627 MB, throughput 0.991434
Recommendation: one client; give it all the memory
Reading from 13559: heap size 1629 MB, throughput 0.989332
Recommendation: one client; give it all the memory
Reading from 13559: heap size 1621 MB, throughput 0.990706
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 13559: heap size 1626 MB, throughput 0.993118
Reading from 13559: heap size 1637 MB, throughput 0.930985
Reading from 13559: heap size 1646 MB, throughput 0.801708
Reading from 13559: heap size 1628 MB, throughput 0.769671
Reading from 13559: heap size 1659 MB, throughput 0.750953
Reading from 13559: heap size 1692 MB, throughput 0.775152
Reading from 13559: heap size 1711 MB, throughput 0.758512
Client 13559 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
