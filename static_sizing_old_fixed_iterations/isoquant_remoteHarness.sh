#!/bin/bash
# Measure isoquant points on a remote machine
#
# Usage: isoquant_remoteHarness.sh b1 b2 max_heap1 max_heap2 runs iterations out_dir
#

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") b1 b2 max_heap1 max_heap2 runs iterations out_dir"
    exit 1
}

test -z "${1}" && usage
test -z "${2}" && usage
test -z "${3}" && usage
test -z "${4}" && usage
test -z "${5}" && usage
test -z "${6}" && usage
test -z "${7}" && usage

B1="${1}"
B2="${2}"
MAXHEAP1="${3}"
MAXHEAP2="${4}"
RUNS="${5}"
ITERATIONS="${6}"
OUTDIR="$(readlink -f "${7}")"

ME="$(whoami)@$(hostname)"
AVG_TIMES_FILE="${OUTDIR}/avgTimes_${ME}.txt"

mkdir -p "${OUTDIR}"

"${DIR}/../machineInfo.sh" "${OUTDIR}"

TEMPDIR="$(readlink -f "$(mktemp -d)")"
TEMPAVGTIMES="$(readlink -f "$(mktemp)")"

cd "${TEMPDIR}"

while read HEAP1 HEAP2; do
    "${DIR}/isoquant_point.sh" "${B1}" "${B2}" "${HEAP1}" "${HEAP2}" "${MAXHEAP1}" "${MAXHEAP2}" "${RUNS}" "${ITERATIONS}" "${TEMPDIR}" "${TEMPAVGTIMES}"
    mv -t "${OUTDIR}" *
done

mv "${TEMPAVGTIMES}" "${AVG_TIMES_FILE}"
cd
rmdir "${TEMPDIR}"
