Using scaled threading model. 4 processors detected, 1 threads used to drive the workload, in a possible range of [1,256000]
java.lang.reflect.InvocationTargetException
java.lang.reflect.InvocationTargetException
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:483)
	at org.dacapo.harness.H2.preIteration(H2.java:77)
	at org.dacapo.harness.Benchmark.run(Benchmark.java:152)
	at org.dacapo.harness.TestHarness.runBenchmark(TestHarness.java:218)
	at org.dacapo.harness.TestHarness.main(TestHarness.java:171)
	at Harness.main(Harness.java:17)
Caused by: org.h2.jdbc.JdbcSQLException: Out of memory.; SQL statement:


-- Index to support the order status and payment transactions
-- that lookup the customer by the last name.
CREATE INDEX CUSTOMER_LAST_NAME ON CUSTOMER(C_W_ID, C_D_ID, C_LAST) [90108-123]
	at org.h2.message.Message.getSQLException(Message.java:111)
	at org.h2.message.Message.convertThrowable(Message.java:303)
	at org.h2.command.Command.executeUpdate(Command.java:231)
	at org.h2.jdbc.JdbcStatement.execute(JdbcStatement.java:162)
	at org.h2.tools.RunScript.process(RunScript.java:166)
	at org.h2.tools.RunScript.execute(RunScript.java:154)
	at org.dacapo.h2.TPCC.runScript(TPCC.java:566)
	at org.dacapo.h2.TPCC.runScript(TPCC.java:556)
	at org.dacapo.h2.TPCC.runScript(TPCC.java:544)
	at org.dacapo.h2.TPCC.createIndexes(TPCC.java:377)
	at org.dacapo.h2.TPCC.preIterationMemoryDB(TPCC.java:220)
	at org.dacapo.h2.TPCC.preIteration(TPCC.java:254)
	... 9 more
Caused by: java.lang.OutOfMemoryError: GC overhead limit exceeded
	at org.h2.index.TreeIndex.find(TreeIndex.java:293)
	at org.h2.index.MultiVersionIndex.removeIfExists(MultiVersionIndex.java:132)
	at org.h2.index.MultiVersionIndex.add(MultiVersionIndex.java:51)
	at org.h2.table.TableData.addRowsToIndex(TableData.java:337)
	at org.h2.table.TableData.addIndex(TableData.java:245)
	at org.h2.command.ddl.CreateIndex.update(CreateIndex.java:91)
	at org.h2.command.CommandContainer.update(CommandContainer.java:72)
	at org.h2.command.Command.executeUpdate(Command.java:209)
	... 18 more
