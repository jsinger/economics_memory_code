economemd
    total memory: 100 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_08_12_14_51_20/sub6_timerComparisonSingle/sub3_timeDacapoDaemon/daemon_run2_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 30983: heap size 9 MB, throughput 0.989594
Clients: 1
Reading from 30983: heap size 9 MB, throughput 0.979644
Reading from 30984: heap size 9 MB, throughput 0.993365
Clients: 2
Reading from 30983: heap size 9 MB, throughput 0.970226
Reading from 30983: heap size 9 MB, throughput 0.961076
Reading from 30984: heap size 9 MB, throughput 0.989367
Reading from 30984: heap size 9 MB, throughput 0.937904
Reading from 30983: heap size 11 MB, throughput 0.973709
Reading from 30984: heap size 9 MB, throughput 0.958997
Reading from 30984: heap size 11 MB, throughput 0.974204
Reading from 30983: heap size 11 MB, throughput 0.971357
Reading from 30984: heap size 11 MB, throughput 0.979508
Reading from 30983: heap size 16 MB, throughput 0.925836
Reading from 30984: heap size 15 MB, throughput 0.985413
Reading from 30984: heap size 16 MB, throughput 0.988011
Reading from 30983: heap size 23 MB, throughput 0.970835
Reading from 30984: heap size 23 MB, throughput 0.986855
Reading from 30984: heap size 24 MB, throughput 0.983718
Reading from 30984: heap size 33 MB, throughput 0.987135
Reading from 30984: heap size 34 MB, throughput 0.963847
Reading from 30983: heap size 23 MB, throughput 0.991103
Reading from 30984: heap size 50 MB, throughput 0.900366
Reading from 30984: heap size 50 MB, throughput 0.92159
Reading from 30984: heap size 62 MB, throughput 0.947018
Reading from 30984: heap size 63 MB, throughput 0.918623
Reading from 30984: heap size 80 MB, throughput 0.941653
Reading from 30984: heap size 80 MB, throughput 0.966861
Reading from 30983: heap size 28 MB, throughput 0.995145
Reading from 30984: heap size 92 MB, throughput 0.911704
Reading from 30984: heap size 93 MB, throughput 0.951423
Reading from 30983: heap size 29 MB, throughput 0.996479
Reading from 30984: heap size 117 MB, throughput 0.891977
Reading from 30984: heap size 117 MB, throughput 0.856051
Reading from 30983: heap size 29 MB, throughput 0.997945
Reading from 30984: heap size 149 MB, throughput 0.947186
Reading from 30984: heap size 150 MB, throughput 0.948006
Reading from 30984: heap size 166 MB, throughput 0.949898
Reading from 30984: heap size 166 MB, throughput 0.952721
Reading from 30984: heap size 181 MB, throughput 0.958946
Reading from 30984: heap size 181 MB, throughput 0.962965
Reading from 30984: heap size 196 MB, throughput 0.968576
Reading from 30983: heap size 30 MB, throughput 0.998283
Reading from 30984: heap size 196 MB, throughput 0.988327
Reading from 30984: heap size 208 MB, throughput 0.98683
Reading from 30984: heap size 211 MB, throughput 0.98757
Reading from 30983: heap size 30 MB, throughput 0.998325
Reading from 30984: heap size 222 MB, throughput 0.997651
Reading from 30984: heap size 224 MB, throughput 0.99709
Reading from 30983: heap size 31 MB, throughput 0.998523
Numeric result:
Recommendation: 2 clients, utility 1.00649:
    h1: 96.9752 MB (U(h) = 0.889493*h^0.0320593)
    h2: 3.02479 MB (U(h) = 0.976104*h^0.001)
Recommendation: 2 clients, utility 1.00649:
    h1: 96.9751 MB (U(h) = 0.889493*h^0.0320593)
    h2: 3.02487 MB (U(h) = 0.976104*h^0.001)
Reading from 30984: heap size 231 MB, throughput 0.996814
Reading from 30983: heap size 31 MB, throughput 0.998171
Reading from 30984: heap size 233 MB, throughput 0.9973
Reading from 30983: heap size 32 MB, throughput 0.998315
Reading from 30983: heap size 32 MB, throughput 0.998447
Reading from 30983: heap size 32 MB, throughput 0.99821
Reading from 30983: heap size 32 MB, throughput 0.997888
Reading from 30983: heap size 33 MB, throughput 0.998091
Reading from 30983: heap size 33 MB, throughput 0.997669
Reading from 30983: heap size 34 MB, throughput 0.773507
Reading from 30983: heap size 34 MB, throughput 0.997192
Numeric result:
Recommendation: 2 clients, utility 0.95765:
    h1: 92.5231 MB (U(h) = 0.936691*h^0.0123745)
    h2: 7.47691 MB (U(h) = 0.964729*h^0.001)
Recommendation: 2 clients, utility 0.95765:
    h1: 92.5231 MB (U(h) = 0.936691*h^0.0123745)
    h2: 7.47692 MB (U(h) = 0.964729*h^0.001)
Reading from 30984: heap size 5358 MB, throughput 0.999595
Reading from 30983: heap size 35 MB, throughput 0.995726
Reading from 30983: heap size 39 MB, throughput 0.992392
Reading from 30983: heap size 42 MB, throughput 0.995788
Reading from 30983: heap size 42 MB, throughput 0.997502
Reading from 30983: heap size 41 MB, throughput 0.997981
Reading from 30983: heap size 42 MB, throughput 0.996557
Reading from 30983: heap size 41 MB, throughput 0.997725
Reading from 30983: heap size 42 MB, throughput 0.998198
Reading from 30984: heap size 5354 MB, throughput 0.999724
Numeric result:
Recommendation: 2 clients, utility 0.960323:
    h1: 81.6365 MB (U(h) = 0.916051*h^0.0204566)
    h2: 18.3635 MB (U(h) = 0.945302*h^0.00460153)
Recommendation: 2 clients, utility 0.960323:
    h1: 81.6365 MB (U(h) = 0.916051*h^0.0204566)
    h2: 18.3635 MB (U(h) = 0.945302*h^0.00460153)
Reading from 30983: heap size 41 MB, throughput 0.998495
Reading from 30983: heap size 41 MB, throughput 0.99844
Reading from 30983: heap size 41 MB, throughput 0.998104
Reading from 30983: heap size 41 MB, throughput 0.997966
Reading from 30983: heap size 41 MB, throughput 0.998233
Reading from 30983: heap size 42 MB, throughput 0.983426
Reading from 30983: heap size 41 MB, throughput 0.973314
Reading from 30984: heap size 5357 MB, throughput 0.999785
Reading from 30983: heap size 42 MB, throughput 0.98993
Numeric result:
Recommendation: 2 clients, utility 0.955316:
    h1: 76.4663 MB (U(h) = 0.923419*h^0.0176179)
    h2: 23.5337 MB (U(h) = 0.942167*h^0.0054221)
Recommendation: 2 clients, utility 0.955316:
    h1: 76.4665 MB (U(h) = 0.923419*h^0.0176179)
    h2: 23.5335 MB (U(h) = 0.942167*h^0.0054221)
Reading from 30983: heap size 44 MB, throughput 0.993863
Reading from 30983: heap size 45 MB, throughput 0.994352
Reading from 30983: heap size 46 MB, throughput 0.996153
Reading from 30983: heap size 46 MB, throughput 0.996987
Reading from 30984: heap size 5357 MB, throughput 0.999676
Reading from 30983: heap size 48 MB, throughput 0.990388
Reading from 30983: heap size 48 MB, throughput 0.99149
Reading from 30983: heap size 48 MB, throughput 0.994757
Numeric result:
Recommendation: 2 clients, utility 0.959308:
    h1: 79.0799 MB (U(h) = 0.915768*h^0.0204859)
    h2: 20.9201 MB (U(h) = 0.942177*h^0.00541938)
Recommendation: 2 clients, utility 0.959308:
    h1: 79.08 MB (U(h) = 0.915768*h^0.0204859)
    h2: 20.92 MB (U(h) = 0.942177*h^0.00541938)
Reading from 30983: heap size 48 MB, throughput 0.995528
Reading from 30984: heap size 5358 MB, throughput 0.999684
Reading from 30983: heap size 48 MB, throughput 0.996658
Reading from 30983: heap size 48 MB, throughput 0.997345
Reading from 30983: heap size 49 MB, throughput 0.994509
Reading from 30983: heap size 49 MB, throughput 0.995436
Reading from 30984: heap size 5358 MB, throughput 0.999703
Reading from 30983: heap size 50 MB, throughput 0.994103
Numeric result:
Recommendation: 2 clients, utility 0.960485:
    h1: 79.7517 MB (U(h) = 0.913395*h^0.0213601)
    h2: 20.2483 MB (U(h) = 0.942163*h^0.00542317)
Recommendation: 2 clients, utility 0.960485:
    h1: 79.7517 MB (U(h) = 0.913395*h^0.0213601)
    h2: 20.2483 MB (U(h) = 0.942163*h^0.00542317)
Reading from 30983: heap size 54 MB, throughput 0.981605
Reading from 30983: heap size 54 MB, throughput 0.998438
Reading from 30984: heap size 5358 MB, throughput 0.999696
Reading from 30983: heap size 55 MB, throughput 0.998659
Reading from 30983: heap size 56 MB, throughput 0.998783
Reading from 30983: heap size 56 MB, throughput 0.998869
Reading from 30984: heap size 5358 MB, throughput 0.999699
Reading from 30983: heap size 55 MB, throughput 0.998171
Numeric result:
Recommendation: 2 clients, utility 0.960766:
    h1: 79.9231 MB (U(h) = 0.912733*h^0.0215926)
    h2: 20.0769 MB (U(h) = 0.942159*h^0.00542415)
Recommendation: 2 clients, utility 0.960766:
    h1: 79.923 MB (U(h) = 0.912733*h^0.0215926)
    h2: 20.077 MB (U(h) = 0.942159*h^0.00542415)
Reading from 30983: heap size 56 MB, throughput 0.998674
Reading from 30984: heap size 5359 MB, throughput 0.999645
Reading from 30983: heap size 55 MB, throughput 0.997379
Reading from 30983: heap size 55 MB, throughput 0.997498
Reading from 30984: heap size 5357 MB, throughput 0.999593
Reading from 30983: heap size 56 MB, throughput 0.99753
Reading from 30983: heap size 57 MB, throughput 0.997425
Numeric result:
Recommendation: 2 clients, utility 0.960278:
    h1: 78.4501 MB (U(h) = 0.912703*h^0.021602)
    h2: 21.5499 MB (U(h) = 0.940216*h^0.00593406)
Recommendation: 2 clients, utility 0.960278:
    h1: 78.4499 MB (U(h) = 0.912703*h^0.021602)
    h2: 21.5501 MB (U(h) = 0.940216*h^0.00593406)
Reading from 30983: heap size 59 MB, throughput 0.996974
Reading from 30984: heap size 5359 MB, throughput 0.99908
Reading from 30983: heap size 59 MB, throughput 0.997235
Reading from 30983: heap size 60 MB, throughput 0.997334
Reading from 30983: heap size 60 MB, throughput 0.997185
Reading from 30984: heap size 5359 MB, throughput 0.999327
Reading from 30983: heap size 61 MB, throughput 0.99729
Reading from 30983: heap size 61 MB, throughput 0.997492
Numeric result:
Recommendation: 2 clients, utility 0.96033:
    h1: 78.5111 MB (U(h) = 0.912607*h^0.0216357)
    h2: 21.4889 MB (U(h) = 0.940262*h^0.00592191)
Recommendation: 2 clients, utility 0.96033:
    h1: 78.5108 MB (U(h) = 0.912607*h^0.0216357)
    h2: 21.4892 MB (U(h) = 0.940262*h^0.00592191)
Reading from 30984: heap size 5360 MB, throughput 0.9994
Reading from 30983: heap size 60 MB, throughput 0.99805
Reading from 30983: heap size 61 MB, throughput 0.998071
Reading from 30984: heap size 5358 MB, throughput 0.999459
Reading from 30983: heap size 60 MB, throughput 0.997898
Reading from 30983: heap size 61 MB, throughput 0.998047
Reading from 30984: heap size 5359 MB, throughput 0.999526
Reading from 30983: heap size 61 MB, throughput 0.962109
Numeric result:
Recommendation: 2 clients, utility 0.958433:
    h1: 76.3668 MB (U(h) = 0.916592*h^0.0202539)
    h2: 23.6332 MB (U(h) = 0.938944*h^0.00626839)
Recommendation: 2 clients, utility 0.958433:
    h1: 76.3656 MB (U(h) = 0.916592*h^0.0202539)
    h2: 23.6344 MB (U(h) = 0.938944*h^0.00626839)
Reading from 30983: heap size 61 MB, throughput 0.970583
Reading from 30984: heap size 5359 MB, throughput 0.999324
Reading from 30983: heap size 61 MB, throughput 0.991618
Reading from 30983: heap size 62 MB, throughput 0.995495
Reading from 30984: heap size 5360 MB, throughput 0.99942
Reading from 30983: heap size 62 MB, throughput 0.994725
Numeric result:
Recommendation: 2 clients, utility 0.958612:
    h1: 76.5077 MB (U(h) = 0.916145*h^0.0204091)
    h2: 23.4923 MB (U(h) = 0.93895*h^0.00626682)
Recommendation: 2 clients, utility 0.958612:
    h1: 76.5075 MB (U(h) = 0.916145*h^0.0204091)
    h2: 23.4925 MB (U(h) = 0.93895*h^0.00626682)
Reading from 30983: heap size 63 MB, throughput 0.997571
Reading from 30984: heap size 5360 MB, throughput 0.999394
Reading from 30983: heap size 62 MB, throughput 0.998261
Reading from 30983: heap size 63 MB, throughput 0.998451
Reading from 30984: heap size 5360 MB, throughput 0.999459
Reading from 30983: heap size 62 MB, throughput 0.998606
Reading from 30983: heap size 63 MB, throughput 0.99863
Numeric result:
Recommendation: 2 clients, utility 0.958927:
    h1: 76.7509 MB (U(h) = 0.915328*h^0.0206905)
    h2: 23.2491 MB (U(h) = 0.938947*h^0.00626751)
Recommendation: 2 clients, utility 0.958927:
    h1: 76.7509 MB (U(h) = 0.915328*h^0.0206905)
    h2: 23.2491 MB (U(h) = 0.938947*h^0.00626751)
Reading from 30984: heap size 5359 MB, throughput 0.99954
Reading from 30983: heap size 63 MB, throughput 0.998783
Reading from 30983: heap size 63 MB, throughput 0.998745
Reading from 30984: heap size 5360 MB, throughput 0.99964
Reading from 30983: heap size 63 MB, throughput 0.998742
Reading from 30983: heap size 63 MB, throughput 0.998519
Reading from 30984: heap size 5360 MB, throughput 0.999652
Numeric result:
Recommendation: 2 clients, utility 0.958943:
    h1: 76.7454 MB (U(h) = 0.915266*h^0.0207118)
    h2: 23.2546 MB (U(h) = 0.938916*h^0.00627585)
Recommendation: 2 clients, utility 0.958943:
    h1: 76.7455 MB (U(h) = 0.915266*h^0.0207118)
    h2: 23.2545 MB (U(h) = 0.938916*h^0.00627585)
Reading from 30983: heap size 63 MB, throughput 0.998735
Reading from 30984: heap size 5361 MB, throughput 0.999648
Reading from 30983: heap size 63 MB, throughput 0.998892
Reading from 30983: heap size 63 MB, throughput 0.998654
Reading from 30984: heap size 5361 MB, throughput 0.999642
Reading from 30983: heap size 63 MB, throughput 0.999068
Numeric result:
Recommendation: 2 clients, utility 0.958745:
    h1: 76.031 MB (U(h) = 0.915205*h^0.0207329)
    h2: 23.969 MB (U(h) = 0.937927*h^0.00653614)
Recommendation: 2 clients, utility 0.958745:
    h1: 76.0309 MB (U(h) = 0.915205*h^0.0207329)
    h2: 23.9691 MB (U(h) = 0.937927*h^0.00653614)
Reading from 30983: heap size 62 MB, throughput 0.998461
Reading from 30984: heap size 5361 MB, throughput 0.999683
Reading from 30983: heap size 63 MB, throughput 0.998573
Reading from 30983: heap size 63 MB, throughput 0.9986
Reading from 30984: heap size 5361 MB, throughput 0.999623
Reading from 30983: heap size 63 MB, throughput 0.996504
Numeric result:
Recommendation: 2 clients, utility 0.958671:
    h1: 75.9718 MB (U(h) = 0.915402*h^0.0206653)
    h2: 24.0282 MB (U(h) = 0.937927*h^0.00653609)
Recommendation: 2 clients, utility 0.958671:
    h1: 75.9715 MB (U(h) = 0.915402*h^0.0206653)
    h2: 24.0285 MB (U(h) = 0.937927*h^0.00653609)
Reading from 30984: heap size 5361 MB, throughput 0.999624
Reading from 30983: heap size 63 MB, throughput 0.997699
Reading from 30983: heap size 64 MB, throughput 0.998365
Reading from 30984: heap size 5361 MB, throughput 0.999686
Reading from 30983: heap size 65 MB, throughput 0.998522
Reading from 30984: heap size 5361 MB, throughput 0.999666
Reading from 30983: heap size 65 MB, throughput 0.998706
Numeric result:
Recommendation: 2 clients, utility 0.958788:
    h1: 76.0646 MB (U(h) = 0.915086*h^0.0207732)
    h2: 23.9354 MB (U(h) = 0.937925*h^0.00653667)
Recommendation: 2 clients, utility 0.958788:
    h1: 76.0648 MB (U(h) = 0.915086*h^0.0207732)
    h2: 23.9352 MB (U(h) = 0.937925*h^0.00653667)
Reading from 30983: heap size 65 MB, throughput 0.998721
Reading from 30984: heap size 5361 MB, throughput 0.999717
Reading from 30983: heap size 65 MB, throughput 0.997457
Reading from 30983: heap size 66 MB, throughput 0.998177
Reading from 30984: heap size 5361 MB, throughput 0.99967
Reading from 30983: heap size 66 MB, throughput 0.998107
Numeric result:
Recommendation: 2 clients, utility 0.958738:
    h1: 76.0239 MB (U(h) = 0.91522*h^0.0207277)
    h2: 23.9761 MB (U(h) = 0.937923*h^0.00653717)
Recommendation: 2 clients, utility 0.958738:
    h1: 76.0234 MB (U(h) = 0.91522*h^0.0207277)
    h2: 23.9766 MB (U(h) = 0.937923*h^0.00653717)
Reading from 30984: heap size 5361 MB, throughput 0.9997
Reading from 30983: heap size 66 MB, throughput 0.99793
Reading from 30983: heap size 67 MB, throughput 0.998428
Reading from 30984: heap size 5361 MB, throughput 0.999581
Reading from 30983: heap size 68 MB, throughput 0.998694
Numeric result:
Recommendation: 2 clients, utility 0.958713:
    h1: 76.0049 MB (U(h) = 0.915294*h^0.0207025)
    h2: 23.9951 MB (U(h) = 0.937928*h^0.00653586)
Recommendation: 2 clients, utility 0.958713:
    h1: 76.005 MB (U(h) = 0.915294*h^0.0207025)
    h2: 23.995 MB (U(h) = 0.937928*h^0.00653586)
Reading from 30984: heap size 5361 MB, throughput 0.999658
Reading from 30983: heap size 68 MB, throughput 0.998731
Reading from 30983: heap size 69 MB, throughput 0.998961
Reading from 30984: heap size 5361 MB, throughput 0.999634
Reading from 30983: heap size 69 MB, throughput 0.99896
Reading from 30984: heap size 5361 MB, throughput 0.999539
Numeric result:
Recommendation: 2 clients, utility 0.958704:
    h1: 75.9999 MB (U(h) = 0.915325*h^0.0206924)
    h2: 24.0001 MB (U(h) = 0.937933*h^0.00653449)
Recommendation: 2 clients, utility 0.958704:
    h1: 75.9999 MB (U(h) = 0.915325*h^0.0206924)
    h2: 24.0001 MB (U(h) = 0.937933*h^0.00653449)
Reading from 30983: heap size 69 MB, throughput 0.998864
Reading from 30984: heap size 5361 MB, throughput 0.999534
Reading from 30983: heap size 69 MB, throughput 0.998845
Reading from 30983: heap size 70 MB, throughput 0.998932
Reading from 30984: heap size 5361 MB, throughput 0.999551
Reading from 30983: heap size 70 MB, throughput 0.998927
Numeric result:
Recommendation: 2 clients, utility 0.95867:
    h1: 75.9732 MB (U(h) = 0.915426*h^0.0206585)
    h2: 24.0268 MB (U(h) = 0.937937*h^0.00653351)
Recommendation: 2 clients, utility 0.95867:
    h1: 75.9727 MB (U(h) = 0.915426*h^0.0206585)
    h2: 24.0273 MB (U(h) = 0.937937*h^0.00653351)
Reading from 30984: heap size 5361 MB, throughput 0.999544
Reading from 30983: heap size 71 MB, throughput 0.998955
Client 30983 died
Clients: 1
Reading from 30984: heap size 5361 MB, throughput 0.999581
Reading from 30984: heap size 5361 MB, throughput 0.999635
Recommendation: one client; give it all the memory
Reading from 30984: heap size 5361 MB, throughput 0.999623
Reading from 30984: heap size 5361 MB, throughput 0.999605
Recommendation: one client; give it all the memory
Reading from 30984: heap size 5361 MB, throughput 0.999546
Reading from 30984: heap size 5361 MB, throughput 0.999508
Reading from 30984: heap size 5361 MB, throughput 0.99947
Recommendation: one client; give it all the memory
Reading from 30984: heap size 5361 MB, throughput 0.999557
Reading from 30984: heap size 5361 MB, throughput 0.999602
Recommendation: one client; give it all the memory
Reading from 30984: heap size 5361 MB, throughput 0.999517
Reading from 30984: heap size 5361 MB, throughput 0.999539
Reading from 30984: heap size 5361 MB, throughput 0.999512
Recommendation: one client; give it all the memory
Reading from 30984: heap size 5361 MB, throughput 0.999549
Reading from 30984: heap size 5361 MB, throughput 0.999517
Recommendation: one client; give it all the memory
Reading from 30984: heap size 5361 MB, throughput 0.999597
Reading from 30984: heap size 5361 MB, throughput 0.999651
Reading from 30984: heap size 5361 MB, throughput 0.99959
Recommendation: one client; give it all the memory
Reading from 30984: heap size 5361 MB, throughput 0.999593
Reading from 30984: heap size 5361 MB, throughput 0.999527
Recommendation: one client; give it all the memory
Reading from 30984: heap size 5361 MB, throughput 0.999576
Reading from 30984: heap size 5361 MB, throughput 0.999538
Reading from 30984: heap size 5361 MB, throughput 0.999567
Recommendation: one client; give it all the memory
Reading from 30984: heap size 5361 MB, throughput 0.999571
Client 30984 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
TimerThread: finished
ReadingThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
