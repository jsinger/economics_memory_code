#ifndef CLIENT_HPP
#define CLIENT_HPP

#include <functional>
#include <vector>
#include <Log.hpp>
#include <Messages.hpp>
#include "ReadingCache.hpp"

class UtilityFunction;
class UtilityModel;

#define CLIENT_MIN_HEAP ((unsigned long long)2) // This is the smallest heap, in MB, which the clients are allowed (NOTE: this is HotSpot specific!)


class Client
{
public:
    Client(const economem::Reading& initialReading, const std::vector<std::string>& cacheNames, bool forceFunctionValidity, economem::Log& log, unsigned long long minHeap=CLIENT_MIN_HEAP);
    ~Client();
    pid_t getPid() const;
    const economem::Reading& getLastReading() const;
    unsigned long long getMinHeapMB() const;
    void setMinHeapMB(unsigned long long minHeapMB);
    const UtilityFunction* calculateFunction(const UtilityModel& u);
    const UtilityFunction* getCurrentFunction() const;
    bool isValid() const;
    void addReading(const economem::Reading& reading);
    unsigned long long getTotalReadingsReceived() const;
    void dumpTo(FILE* fd) const;
    void logUtilityFunctions() const;

private:
    economem::Reading m_LastReading;
    unsigned long long m_TotalReadingsReceived;
    unsigned long long m_MinHeapMB;
    std::vector<ReadingCache*> m_Caches;
    std::vector<UtilityFunction*> m_Functions;
    bool m_ForceFunctionValidity;
    economem::Log& m_Log;
};

#endif
