#!/bin/bash
# Install the MCT library's header files

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -e "${DIR}/mct" ]; then
    TEMPDIR="$(mktemp -d)" &&
    cd "${TEMPDIR}" &&
    wget 'https://launchpad.net/libmct/1.6/1.6.2/+download/mct-1.6.2.tar.gz' &&
    tar -xvf mct-1.6.2.tar.gz &&
    cd mct-1.6.2 &&
    mkdir -p "${DIR}/mct" &&
    scons install prefix="${DIR}/mct" &&
    cd &&
    rm -rf "${TEMPDIR}"
fi
