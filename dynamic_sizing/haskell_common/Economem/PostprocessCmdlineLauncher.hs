{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, DeriveDataTypeable #-}
{-# OPTIONS_GHC -fno-warn-missing-fields -fno-warn-missing-methods #-}

module Economem.PostprocessCmdlineLauncher (postprocess) where

import Control.Applicative
import Economem.ArgHelper
import Economem.Files
import System.Console.CmdLib

data Args = Args { dir :: String }
          deriving (Typeable, Data, Eq, Show)

instance Attributes Args where
  attributes _ = group "Options" [
    dir %> [ Positional 0,
             Required True,
             Help "Dir to postprocess",
             ArgHelp "DIR"]]

instance RecordCommand Args where
  mode_summary _ = "Postprocess a directory"

postprocess :: Dir a => (FilePath -> a) -> IO ()
postprocess f = getArgs >>= executeR Args {} >>= \opts -> do
  d <- f <$> existingAbsoluteDir (dir opts)
  postprocessDir d
