economemd
    total memory: 1152 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub58_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run05_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 12151: heap size 9 MB, throughput 0.985911
Clients: 1
Client 12151 has a minimum heap size of 12 MB
Reading from 12150: heap size 9 MB, throughput 0.991104
Clients: 2
Client 12150 has a minimum heap size of 276 MB
Reading from 12150: heap size 9 MB, throughput 0.973061
Reading from 12151: heap size 9 MB, throughput 0.951953
Reading from 12150: heap size 9 MB, throughput 0.956657
Reading from 12150: heap size 9 MB, throughput 0.96316
Reading from 12151: heap size 11 MB, throughput 0.98087
Reading from 12150: heap size 11 MB, throughput 0.982216
Reading from 12151: heap size 11 MB, throughput 0.978399
Reading from 12150: heap size 11 MB, throughput 0.968224
Reading from 12150: heap size 17 MB, throughput 0.954373
Reading from 12151: heap size 15 MB, throughput 0.987061
Reading from 12150: heap size 17 MB, throughput 0.564442
Reading from 12151: heap size 15 MB, throughput 0.932231
Reading from 12150: heap size 30 MB, throughput 0.8842
Reading from 12150: heap size 31 MB, throughput 0.917405
Reading from 12150: heap size 35 MB, throughput 0.370885
Reading from 12151: heap size 24 MB, throughput 0.870413
Reading from 12150: heap size 48 MB, throughput 0.816618
Reading from 12150: heap size 50 MB, throughput 0.872075
Reading from 12151: heap size 29 MB, throughput 0.985927
Reading from 12150: heap size 53 MB, throughput 0.307663
Reading from 12150: heap size 76 MB, throughput 0.826973
Reading from 12151: heap size 36 MB, throughput 0.991355
Reading from 12150: heap size 76 MB, throughput 0.335298
Reading from 12150: heap size 103 MB, throughput 0.727895
Reading from 12150: heap size 104 MB, throughput 0.741359
Reading from 12150: heap size 105 MB, throughput 0.759687
Reading from 12151: heap size 42 MB, throughput 0.991198
Reading from 12151: heap size 43 MB, throughput 0.955115
Reading from 12151: heap size 45 MB, throughput 0.968894
Reading from 12150: heap size 109 MB, throughput 0.280934
Reading from 12150: heap size 137 MB, throughput 0.767027
Reading from 12151: heap size 52 MB, throughput 0.973891
Reading from 12150: heap size 141 MB, throughput 0.733987
Reading from 12151: heap size 52 MB, throughput 0.963684
Reading from 12150: heap size 143 MB, throughput 0.776564
Reading from 12151: heap size 61 MB, throughput 0.966566
Reading from 12151: heap size 61 MB, throughput 0.975628
Reading from 12151: heap size 72 MB, throughput 0.574627
Reading from 12150: heap size 145 MB, throughput 0.157763
Reading from 12151: heap size 76 MB, throughput 0.986342
Reading from 12150: heap size 186 MB, throughput 0.580797
Reading from 12150: heap size 188 MB, throughput 0.670971
Reading from 12150: heap size 190 MB, throughput 0.76531
Reading from 12151: heap size 87 MB, throughput 0.996557
Reading from 12150: heap size 196 MB, throughput 0.836881
Reading from 12150: heap size 199 MB, throughput 0.914264
Reading from 12151: heap size 87 MB, throughput 0.997431
Reading from 12150: heap size 205 MB, throughput 0.179302
Reading from 12151: heap size 94 MB, throughput 0.995313
Reading from 12150: heap size 257 MB, throughput 0.633583
Reading from 12150: heap size 259 MB, throughput 0.816538
Reading from 12150: heap size 263 MB, throughput 0.650984
Reading from 12150: heap size 264 MB, throughput 0.861155
Reading from 12150: heap size 270 MB, throughput 0.818006
Reading from 12150: heap size 270 MB, throughput 0.763064
Reading from 12151: heap size 95 MB, throughput 0.997205
Reading from 12150: heap size 272 MB, throughput 0.787571
Reading from 12150: heap size 276 MB, throughput 0.729199
Reading from 12150: heap size 282 MB, throughput 0.776719
Reading from 12150: heap size 285 MB, throughput 0.629689
Reading from 12151: heap size 101 MB, throughput 0.99695
Reading from 12150: heap size 291 MB, throughput 0.916683
Reading from 12150: heap size 292 MB, throughput 0.886453
Reading from 12150: heap size 287 MB, throughput 0.787145
Reading from 12151: heap size 102 MB, throughput 0.996382
Reading from 12150: heap size 290 MB, throughput 0.128897
Reading from 12150: heap size 338 MB, throughput 0.716842
Reading from 12150: heap size 339 MB, throughput 0.882242
Reading from 12150: heap size 342 MB, throughput 0.883101
Reading from 12151: heap size 107 MB, throughput 0.997284
Reading from 12150: heap size 343 MB, throughput 0.831941
Reading from 12150: heap size 347 MB, throughput 0.7815
Reading from 12150: heap size 347 MB, throughput 0.625903
Reading from 12150: heap size 353 MB, throughput 0.764178
Reading from 12150: heap size 354 MB, throughput 0.689454
Reading from 12150: heap size 361 MB, throughput 0.747963
Reading from 12151: heap size 108 MB, throughput 0.995555
Equal recommendation: 576 MB each
Reading from 12151: heap size 111 MB, throughput 0.996834
Reading from 12150: heap size 362 MB, throughput 0.980994
Reading from 12151: heap size 111 MB, throughput 0.996033
Reading from 12151: heap size 115 MB, throughput 0.995225
Reading from 12150: heap size 364 MB, throughput 0.982098
Reading from 12151: heap size 115 MB, throughput 0.996564
Reading from 12151: heap size 117 MB, throughput 0.996471
Reading from 12150: heap size 367 MB, throughput 0.979099
Reading from 12151: heap size 117 MB, throughput 0.993794
Reading from 12151: heap size 120 MB, throughput 0.9969
Reading from 12150: heap size 366 MB, throughput 0.977177
Reading from 12151: heap size 120 MB, throughput 0.995878
Reading from 12151: heap size 122 MB, throughput 0.99823
Reading from 12150: heap size 369 MB, throughput 0.979765
Reading from 12151: heap size 122 MB, throughput 0.997846
Reading from 12151: heap size 124 MB, throughput 0.998549
Reading from 12150: heap size 369 MB, throughput 0.982572
Reading from 12151: heap size 124 MB, throughput 0.997859
Reading from 12151: heap size 126 MB, throughput 0.99851
Reading from 12150: heap size 372 MB, throughput 0.97957
Equal recommendation: 576 MB each
Reading from 12151: heap size 126 MB, throughput 0.997033
Reading from 12151: heap size 128 MB, throughput 0.998612
Reading from 12150: heap size 368 MB, throughput 0.98052
Reading from 12151: heap size 128 MB, throughput 0.998212
Reading from 12151: heap size 129 MB, throughput 0.998638
Reading from 12150: heap size 371 MB, throughput 0.978144
Reading from 12151: heap size 129 MB, throughput 0.995181
Reading from 12151: heap size 130 MB, throughput 0.995017
Reading from 12150: heap size 370 MB, throughput 0.943851
Reading from 12151: heap size 130 MB, throughput 0.991775
Reading from 12151: heap size 133 MB, throughput 0.99669
Reading from 12150: heap size 371 MB, throughput 0.980481
Reading from 12151: heap size 134 MB, throughput 0.997129
Reading from 12151: heap size 137 MB, throughput 0.99662
Reading from 12151: heap size 137 MB, throughput 0.996713
Reading from 12150: heap size 374 MB, throughput 0.984231
Reading from 12151: heap size 140 MB, throughput 0.995885
Reading from 12151: heap size 140 MB, throughput 0.99723
Reading from 12150: heap size 374 MB, throughput 0.981656
Equal recommendation: 576 MB each
Reading from 12151: heap size 142 MB, throughput 0.997513
Reading from 12151: heap size 142 MB, throughput 0.997203
Reading from 12150: heap size 376 MB, throughput 0.985849
Reading from 12151: heap size 145 MB, throughput 0.997418
Reading from 12150: heap size 377 MB, throughput 0.980047
Reading from 12150: heap size 378 MB, throughput 0.860186
Reading from 12150: heap size 380 MB, throughput 0.805062
Reading from 12150: heap size 385 MB, throughput 0.818072
Reading from 12150: heap size 388 MB, throughput 0.851596
Reading from 12151: heap size 145 MB, throughput 0.996897
Reading from 12151: heap size 147 MB, throughput 0.997604
Reading from 12150: heap size 397 MB, throughput 0.988098
Reading from 12151: heap size 147 MB, throughput 0.996267
Reading from 12151: heap size 149 MB, throughput 0.995359
Reading from 12150: heap size 397 MB, throughput 0.990059
Reading from 12151: heap size 149 MB, throughput 0.997766
Reading from 12151: heap size 151 MB, throughput 0.997079
Reading from 12150: heap size 399 MB, throughput 0.98418
Reading from 12151: heap size 151 MB, throughput 0.99775
Reading from 12151: heap size 153 MB, throughput 0.998476
Reading from 12150: heap size 401 MB, throughput 0.987489
Equal recommendation: 576 MB each
Reading from 12151: heap size 153 MB, throughput 0.998574
Reading from 12151: heap size 155 MB, throughput 0.997902
Reading from 12150: heap size 399 MB, throughput 0.975268
Reading from 12151: heap size 155 MB, throughput 0.994658
Reading from 12151: heap size 157 MB, throughput 0.993903
Reading from 12151: heap size 157 MB, throughput 0.995337
Reading from 12150: heap size 402 MB, throughput 0.988146
Reading from 12151: heap size 160 MB, throughput 0.997904
Reading from 12151: heap size 161 MB, throughput 0.997701
Reading from 12150: heap size 399 MB, throughput 0.986681
Reading from 12151: heap size 162 MB, throughput 0.997261
Reading from 12151: heap size 163 MB, throughput 0.997491
Reading from 12150: heap size 401 MB, throughput 0.985547
Reading from 12151: heap size 165 MB, throughput 0.997799
Reading from 12151: heap size 165 MB, throughput 0.997339
Reading from 12150: heap size 399 MB, throughput 0.986684
Equal recommendation: 576 MB each
Reading from 12151: heap size 167 MB, throughput 0.997866
Reading from 12151: heap size 167 MB, throughput 0.996715
Reading from 12150: heap size 401 MB, throughput 0.776616
Reading from 12151: heap size 169 MB, throughput 0.997611
Reading from 12151: heap size 169 MB, throughput 0.997349
Reading from 12150: heap size 442 MB, throughput 0.986643
Reading from 12151: heap size 172 MB, throughput 0.997654
Reading from 12150: heap size 443 MB, throughput 0.981325
Reading from 12150: heap size 446 MB, throughput 0.870184
Reading from 12150: heap size 447 MB, throughput 0.872887
Reading from 12150: heap size 452 MB, throughput 0.925041
Reading from 12151: heap size 172 MB, throughput 0.995894
Reading from 12151: heap size 173 MB, throughput 0.996368
Reading from 12150: heap size 453 MB, throughput 0.993309
Reading from 12151: heap size 173 MB, throughput 0.997705
Reading from 12151: heap size 176 MB, throughput 0.99865
Reading from 12151: heap size 176 MB, throughput 0.996838
Reading from 12150: heap size 458 MB, throughput 0.987157
Equal recommendation: 576 MB each
Reading from 12151: heap size 179 MB, throughput 0.993459
Reading from 12151: heap size 179 MB, throughput 0.995624
Reading from 12150: heap size 459 MB, throughput 0.992202
Reading from 12151: heap size 181 MB, throughput 0.998109
Reading from 12151: heap size 182 MB, throughput 0.995728
Reading from 12150: heap size 459 MB, throughput 0.991587
Reading from 12151: heap size 184 MB, throughput 0.99747
Reading from 12151: heap size 184 MB, throughput 0.99728
Reading from 12150: heap size 462 MB, throughput 0.991069
Reading from 12151: heap size 187 MB, throughput 0.998039
Reading from 12151: heap size 187 MB, throughput 0.997173
Reading from 12150: heap size 459 MB, throughput 0.981818
Reading from 12151: heap size 189 MB, throughput 0.998021
Equal recommendation: 576 MB each
Reading from 12151: heap size 189 MB, throughput 0.997827
Reading from 12150: heap size 462 MB, throughput 0.988964
Reading from 12151: heap size 192 MB, throughput 0.997871
Reading from 12151: heap size 192 MB, throughput 0.997252
Reading from 12150: heap size 464 MB, throughput 0.989137
Reading from 12151: heap size 193 MB, throughput 0.99791
Reading from 12150: heap size 464 MB, throughput 0.985059
Reading from 12151: heap size 194 MB, throughput 0.997562
Reading from 12151: heap size 196 MB, throughput 0.932828
Reading from 12150: heap size 466 MB, throughput 0.990712
Reading from 12150: heap size 468 MB, throughput 0.868728
Reading from 12150: heap size 467 MB, throughput 0.856883
Reading from 12151: heap size 200 MB, throughput 0.993404
Reading from 12150: heap size 470 MB, throughput 0.972746
Reading from 12151: heap size 202 MB, throughput 0.991903
Reading from 12151: heap size 202 MB, throughput 0.99675
Reading from 12151: heap size 208 MB, throughput 0.998451
Reading from 12150: heap size 478 MB, throughput 0.993707
Equal recommendation: 576 MB each
Reading from 12151: heap size 208 MB, throughput 0.997599
Reading from 12150: heap size 479 MB, throughput 0.993168
Reading from 12151: heap size 212 MB, throughput 0.998365
Reading from 12151: heap size 213 MB, throughput 0.997378
Reading from 12150: heap size 480 MB, throughput 0.993178
Reading from 12151: heap size 217 MB, throughput 0.998399
Reading from 12151: heap size 217 MB, throughput 0.997992
Reading from 12150: heap size 482 MB, throughput 0.989944
Reading from 12151: heap size 219 MB, throughput 0.998229
Reading from 12151: heap size 220 MB, throughput 0.997811
Reading from 12150: heap size 480 MB, throughput 0.990922
Equal recommendation: 576 MB each
Reading from 12151: heap size 223 MB, throughput 0.998097
Reading from 12151: heap size 223 MB, throughput 0.996986
Reading from 12150: heap size 483 MB, throughput 0.989655
Reading from 12151: heap size 227 MB, throughput 0.998033
Reading from 12151: heap size 227 MB, throughput 0.995699
Reading from 12150: heap size 485 MB, throughput 0.982897
Reading from 12151: heap size 230 MB, throughput 0.991322
Reading from 12151: heap size 230 MB, throughput 0.997693
Reading from 12150: heap size 485 MB, throughput 0.994053
Reading from 12151: heap size 235 MB, throughput 0.998138
Reading from 12150: heap size 488 MB, throughput 0.973297
Reading from 12150: heap size 488 MB, throughput 0.892117
Reading from 12150: heap size 492 MB, throughput 0.939891
Reading from 12151: heap size 237 MB, throughput 0.998188
Equal recommendation: 576 MB each
Reading from 12150: heap size 494 MB, throughput 0.992776
Reading from 12151: heap size 241 MB, throughput 0.998653
Reading from 12151: heap size 241 MB, throughput 0.998088
Reading from 12150: heap size 498 MB, throughput 0.994073
Reading from 12151: heap size 245 MB, throughput 0.998334
Reading from 12151: heap size 245 MB, throughput 0.997462
Reading from 12150: heap size 499 MB, throughput 0.992117
Reading from 12151: heap size 249 MB, throughput 0.998359
Reading from 12151: heap size 249 MB, throughput 0.9979
Reading from 12150: heap size 497 MB, throughput 0.991887
Equal recommendation: 576 MB each
Reading from 12151: heap size 251 MB, throughput 0.99833
Reading from 12151: heap size 252 MB, throughput 0.996845
Reading from 12150: heap size 500 MB, throughput 0.987766
Reading from 12151: heap size 256 MB, throughput 0.992343
Reading from 12151: heap size 256 MB, throughput 0.997634
Reading from 12150: heap size 500 MB, throughput 0.989353
Reading from 12151: heap size 262 MB, throughput 0.998254
Reading from 12150: heap size 501 MB, throughput 0.989945
Reading from 12151: heap size 262 MB, throughput 0.997993
Reading from 12151: heap size 267 MB, throughput 0.998457
Reading from 12150: heap size 503 MB, throughput 0.994129
Reading from 12150: heap size 504 MB, throughput 0.937046
Reading from 12150: heap size 504 MB, throughput 0.918964
Equal recommendation: 576 MB each
Reading from 12151: heap size 267 MB, throughput 0.998056
Reading from 12150: heap size 506 MB, throughput 0.983438
Reading from 12151: heap size 271 MB, throughput 0.997854
Reading from 12150: heap size 513 MB, throughput 0.993837
Reading from 12151: heap size 271 MB, throughput 0.998121
Reading from 12151: heap size 275 MB, throughput 0.997241
Reading from 12150: heap size 514 MB, throughput 0.993152
Reading from 12151: heap size 275 MB, throughput 0.998243
Reading from 12151: heap size 279 MB, throughput 0.997101
Reading from 12150: heap size 513 MB, throughput 0.988428
Reading from 12151: heap size 279 MB, throughput 0.993093
Equal recommendation: 576 MB each
Reading from 12151: heap size 283 MB, throughput 0.998359
Reading from 12150: heap size 516 MB, throughput 0.991279
Reading from 12151: heap size 284 MB, throughput 0.998055
Reading from 12150: heap size 516 MB, throughput 0.992099
Reading from 12151: heap size 289 MB, throughput 0.99834
Reading from 12151: heap size 289 MB, throughput 0.997466
Reading from 12150: heap size 517 MB, throughput 0.989684
Reading from 12151: heap size 294 MB, throughput 0.998521
Equal recommendation: 576 MB each
Reading from 12150: heap size 519 MB, throughput 0.993276
Reading from 12151: heap size 294 MB, throughput 0.998545
Reading from 12150: heap size 520 MB, throughput 0.927656
Reading from 12150: heap size 519 MB, throughput 0.921749
Reading from 12151: heap size 298 MB, throughput 0.997621
Reading from 12150: heap size 521 MB, throughput 0.994506
Reading from 12151: heap size 298 MB, throughput 0.998158
Reading from 12151: heap size 303 MB, throughput 0.99744
Reading from 12150: heap size 527 MB, throughput 0.992628
Reading from 12151: heap size 303 MB, throughput 0.994644
Reading from 12151: heap size 306 MB, throughput 0.998476
Reading from 12150: heap size 528 MB, throughput 0.991437
Equal recommendation: 576 MB each
Reading from 12151: heap size 307 MB, throughput 0.996608
Reading from 12151: heap size 313 MB, throughput 0.998104
Reading from 12150: heap size 528 MB, throughput 0.991912
Reading from 12151: heap size 313 MB, throughput 0.997892
Reading from 12150: heap size 530 MB, throughput 0.992189
Reading from 12151: heap size 318 MB, throughput 0.998452
Reading from 12150: heap size 530 MB, throughput 0.990431
Reading from 12151: heap size 318 MB, throughput 0.998307
Equal recommendation: 576 MB each
Reading from 12151: heap size 322 MB, throughput 0.99859
Reading from 12150: heap size 531 MB, throughput 0.989778
Reading from 12150: heap size 534 MB, throughput 0.968056
Reading from 12150: heap size 535 MB, throughput 0.898793
Reading from 12151: heap size 323 MB, throughput 0.998158
Reading from 12151: heap size 326 MB, throughput 0.900626
Reading from 12150: heap size 539 MB, throughput 0.992997
Reading from 12151: heap size 335 MB, throughput 0.999027
Reading from 12150: heap size 540 MB, throughput 0.994752
Reading from 12151: heap size 342 MB, throughput 0.999198
Equal recommendation: 576 MB each
Reading from 12151: heap size 343 MB, throughput 0.999263
Reading from 12150: heap size 543 MB, throughput 0.994425
Reading from 12151: heap size 348 MB, throughput 0.999226
Reading from 12151: heap size 348 MB, throughput 0.99911
Reading from 12150: heap size 545 MB, throughput 0.992947
Reading from 12151: heap size 352 MB, throughput 0.999276
Reading from 12150: heap size 543 MB, throughput 0.992232
Reading from 12151: heap size 353 MB, throughput 0.998791
Equal recommendation: 576 MB each
Reading from 12151: heap size 357 MB, throughput 0.997285
Reading from 12150: heap size 545 MB, throughput 0.990994
Reading from 12151: heap size 357 MB, throughput 0.997949
Reading from 12151: heap size 362 MB, throughput 0.998752
Reading from 12150: heap size 547 MB, throughput 0.994445
Reading from 12150: heap size 548 MB, throughput 0.937633
Reading from 12150: heap size 547 MB, throughput 0.953268
Reading from 12151: heap size 363 MB, throughput 0.997753
Reading from 12150: heap size 549 MB, throughput 0.994843
Reading from 12151: heap size 368 MB, throughput 0.99846
Equal recommendation: 576 MB each
Reading from 12151: heap size 368 MB, throughput 0.997017
Reading from 12150: heap size 553 MB, throughput 0.991423
Reading from 12151: heap size 373 MB, throughput 0.998824
Reading from 12150: heap size 554 MB, throughput 0.993148
Reading from 12151: heap size 373 MB, throughput 0.998364
Reading from 12151: heap size 378 MB, throughput 0.997072
Reading from 12150: heap size 553 MB, throughput 0.993309
Reading from 12151: heap size 379 MB, throughput 0.99749
Equal recommendation: 576 MB each
Reading from 12151: heap size 387 MB, throughput 0.998578
Reading from 12150: heap size 555 MB, throughput 0.993659
Reading from 12151: heap size 387 MB, throughput 0.998205
Reading from 12150: heap size 557 MB, throughput 0.99413
Reading from 12151: heap size 392 MB, throughput 0.998547
Reading from 12150: heap size 558 MB, throughput 0.977818
Reading from 12150: heap size 557 MB, throughput 0.933621
Reading from 12151: heap size 393 MB, throughput 0.998377
Reading from 12150: heap size 560 MB, throughput 0.99366
Equal recommendation: 576 MB each
Reading from 12151: heap size 398 MB, throughput 0.998283
Reading from 12150: heap size 566 MB, throughput 0.994855
Reading from 12151: heap size 399 MB, throughput 0.997991
Reading from 12151: heap size 404 MB, throughput 0.995817
Reading from 12150: heap size 567 MB, throughput 0.993881
Reading from 12151: heap size 404 MB, throughput 0.997942
Equal recommendation: 576 MB each
Reading from 12151: heap size 406 MB, throughput 0.998808
Reading from 12150: heap size 566 MB, throughput 0.993547
Reading from 12151: heap size 405 MB, throughput 0.998683
Reading from 12150: heap size 568 MB, throughput 0.992426
Reading from 12151: heap size 405 MB, throughput 0.9985
Reading from 12150: heap size 570 MB, throughput 0.994722
Reading from 12151: heap size 405 MB, throughput 0.998628
Reading from 12150: heap size 570 MB, throughput 0.973318
Reading from 12150: heap size 569 MB, throughput 0.933539
Equal recommendation: 576 MB each
Reading from 12151: heap size 405 MB, throughput 0.99866
Reading from 12151: heap size 405 MB, throughput 0.995017
Reading from 12150: heap size 572 MB, throughput 0.99153
Reading from 12151: heap size 404 MB, throughput 0.998863
Reading from 12150: heap size 573 MB, throughput 0.99404
Reading from 12151: heap size 405 MB, throughput 0.998454
Reading from 12150: heap size 547 MB, throughput 0.993536
Reading from 12151: heap size 405 MB, throughput 0.998984
Equal recommendation: 576 MB each
Reading from 12151: heap size 402 MB, throughput 0.998675
Reading from 12150: heap size 573 MB, throughput 0.993435
Reading from 12151: heap size 405 MB, throughput 0.998719
Reading from 12150: heap size 571 MB, throughput 0.992316
Reading from 12151: heap size 405 MB, throughput 0.998626
Reading from 12151: heap size 405 MB, throughput 0.996038
Reading from 12150: heap size 573 MB, throughput 0.995731
Reading from 12150: heap size 573 MB, throughput 0.953018
Equal recommendation: 576 MB each
Reading from 12150: heap size 571 MB, throughput 0.957993
Reading from 12151: heap size 405 MB, throughput 0.998458
Reading from 12151: heap size 405 MB, throughput 0.998799
Reading from 12150: heap size 574 MB, throughput 0.9949
Reading from 12151: heap size 405 MB, throughput 0.998113
Reading from 12150: heap size 572 MB, throughput 0.994393
Reading from 12151: heap size 404 MB, throughput 0.998712
Equal recommendation: 576 MB each
Reading from 12150: heap size 573 MB, throughput 0.991377
Reading from 12151: heap size 405 MB, throughput 0.998669
Reading from 12151: heap size 406 MB, throughput 0.998637
Reading from 12150: heap size 572 MB, throughput 0.992641
Client 12151 died
Clients: 1
Reading from 12150: heap size 573 MB, throughput 0.992586
Recommendation: one client; give it all the memory
Reading from 12150: heap size 575 MB, throughput 0.994455
Reading from 12150: heap size 576 MB, throughput 0.940315
Reading from 12150: heap size 571 MB, throughput 0.985947
Reading from 12150: heap size 574 MB, throughput 0.995219
Reading from 12150: heap size 572 MB, throughput 0.993957
Recommendation: one client; give it all the memory
Reading from 12150: heap size 573 MB, throughput 0.993525
Reading from 12150: heap size 572 MB, throughput 0.992472
Reading from 12150: heap size 573 MB, throughput 0.991551
Recommendation: one client; give it all the memory
Reading from 12150: heap size 575 MB, throughput 0.989173
Reading from 12150: heap size 571 MB, throughput 0.933953
Client 12150 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
