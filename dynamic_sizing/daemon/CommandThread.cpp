#include <Log.hpp>
#include "Client.hpp"
#include "ClientList.hpp"
#include "CommandThread.hpp"
#include "ReadingThread.hpp"
#include "TimerThread.hpp"

CommandThread::CommandThread(ReadingThread& readingThread,
                             TimerThread& timerThread,
                             economem::CommandReceiver& receiver,
                             economem::CommandResultSender& resultSender,
                             economem::RecommendationSender& recommendationSender,
                             ClientList& clients,
                             economem::Log& log,
                             const std::string& dumpFileName)
    : ReceiverThread(receiver, "CommandThread"), m_ReadingThread(readingThread), m_TimerThread(timerThread),
    m_ResultSender(resultSender), m_RecommendationSender(recommendationSender), m_Clients(clients), m_Log(log), m_DumpFileName(dumpFileName)
{

}

void CommandThread::received(const economem::Command& cmd)
{
    switch (cmd.type)
    {
    case economem::COMMANDTYPE_CLIENTS:
    {
        size_t i;
        m_Clients.lock();
        i = m_Clients.count();
        m_Clients.unlock();
        m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_CLIENTS, i), cmd.sender);
    }
    break;
    case economem::COMMANDTYPE_PID: // i holds the 1-based index of a client
    {
        if (cmd.i >= 1)
        {
            pid_t pid;
            const Client* c;
            m_Clients.lock();
            c = m_Clients.getClient(cmd.i - 1);
            if (c == NULL)
                pid = 0;
            else
                pid = c->getPid();
            m_Clients.unlock();
            m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_PID, pid), cmd.sender);
        }
        else
            economem::warning("Invalid client ID: %llu\n", cmd.i);
    }
    break;
    case economem::COMMANDTYPE_GETMINHEAP: // i holds the 1-based index of a client
    {
        if (cmd.i >= 1)
        {
            unsigned long long minHeap;
            Client* c;
            m_Clients.lock();
            c = m_Clients.getClient(cmd.i - 1);
            if (c == NULL)
                minHeap = 0;
            else
                minHeap = c->getMinHeapMB();
            m_Clients.unlock();
            m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_GETMINHEAP, minHeap), cmd.sender);
        }
        else
            economem::warning("Invalid client ID: %llu\n", cmd.i);
    }
    break;
    case economem::COMMANDTYPE_SETMINHEAP: // i holds the 1-based index of a client, j holds size in MB
    {
        if (cmd.i >= 1)
        {
            bool success;
            Client* c;
            m_Clients.lock();
            c = m_Clients.getClient(cmd.i - 1);
            if (c == NULL)
                success = false;
            else
            {
                c->setMinHeapMB(cmd.j);
                success = true;
            }
            m_Clients.unlock();
            m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_SETMINHEAP, success), cmd.sender);
        }
        else
            economem::warning("Invalid client ID: %llu\n", cmd.i);
    }
    break;
    case economem::COMMANDTYPE_GETTOTALMEM:
    {
        m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_GETTOTALMEM, m_TimerThread.getTotalMemMB()), cmd.sender);
    }
    break;
    case economem::COMMANDTYPE_SETTOTALMEM: // i holds size in MB
    {
        m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_SETTOTALMEM, m_TimerThread.setTotalMemMB(cmd.i)), cmd.sender);
    }
    break;
    case economem::COMMANDTYPE_TIMERSTATE:
    {
        m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_TIMERSTATE, m_TimerThread.isPaused()), cmd.sender);
    }
    break;
    case economem::COMMANDTYPE_PAUSE:
    {
        m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_PAUSE, m_TimerThread.pause()), cmd.sender);
    }
    break;
    case economem::COMMANDTYPE_UNPAUSE:
    {
        m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_UNPAUSE, m_TimerThread.unpause()), cmd.sender);
    }
    break;
    case economem::COMMANDTYPE_GETTIMER:
    {
        m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_GETTIMER, m_TimerThread.getTimerIntervalSeconds()), cmd.sender);
    }
    break;
    case economem::COMMANDTYPE_SETTIMER: // i holds the new timer value
    {
        m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_SETTIMER, m_TimerThread.setTimerIntervalSeconds(cmd.i)), cmd.sender);
    }
    break;
    case economem::COMMANDTYPE_LOGSREADINGS:
    {
        m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_LOGSREADINGS, m_Log.logsReadings()), cmd.sender);
    }
    break;
    case economem::COMMANDTYPE_SETLOGREADINGS:
    {
        m_Log.allowReadingLogging(cmd.i != 0);
        m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_LOGSREADINGS, m_Log.logsReadings()), cmd.sender);
    }
    break;
    case economem::COMMANDTYPE_LOGSFUNCTIONS:
    {
        m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_LOGSFUNCTIONS, m_Log.logsFunctions()), cmd.sender);
    }
    break;
    case economem::COMMANDTYPE_SETLOGFUNCTIONS:
    {
        m_Log.allowFunctionLogging(cmd.i != 0);
        m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_LOGSFUNCTIONS, m_Log.logsFunctions()), cmd.sender);
    }
    break;
    case economem::COMMANDTYPE_RECOMMEND: // i holds the 1-based index of a client (0 for all), j holds size in MB
    {
        economem::Recommendation r(cmd.j);
        m_Clients.lock();
        if (cmd.i == 0)
            m_Clients.recommendToAll(r, m_RecommendationSender);
        else if (!m_Clients.recommendToClient(cmd.i - 1, r, m_RecommendationSender))
            economem::warning("Failed to make recommendation to client %llu", cmd.i);
        m_Clients.unlock();
        m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_RECOMMEND, 0), cmd.sender);
    }
    break;
    case economem::COMMANDTYPE_DUMP:
    {
        m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_DUMP, dump()), cmd.sender);
    }
    break;
    case economem::COMMANDTYPE_CLEAR:
    {
        m_Clients.lock();
        m_Clients.clear();
        m_Clients.unlock();
        printf("Clients: 0\n");
        m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_CLEAR, 0), cmd.sender);
    }
    break;
    case economem::COMMANDTYPE_EXIT:
    {
        m_ResultSender.sendCommandResult(economem::CommandResult(economem::CMDRESULTTYPE_EXIT, 0), cmd.sender);
        this->kill();
        m_TimerThread.kill();
        m_ReadingThread.kill();
    }
    break;
    default:
        economem::warning("Unknown command type %u", (unsigned int)cmd.type);
        break;
    }
}

bool CommandThread::dump()
{
    FILE* fd = fopen(m_DumpFileName.c_str(), "a");

    if (fd == NULL)
        return false;

    m_Clients.lock();

    char outstr[200];
    time_t t = time(NULL);
    tm* tmp = localtime(&t);
    strftime(outstr, sizeof(outstr), "%Y/%m/%d %H:%M:%S", tmp);
    fprintf(fd, "Dump at %s\n", outstr);
    fprintf(fd, "Total mem: %llu MB\n", m_TimerThread.getTotalMemMB());
    fprintf(fd, "Timer interval: %llu s\n", m_TimerThread.getTimerIntervalSeconds());
    fprintf(fd, "Timer state: %s\n", (m_TimerThread.isPaused() ? "paused" : "running"));

    m_Clients.dumpTo(fd);

    fprintf(fd, "\n\n\n");

    m_Clients.unlock();

    fclose(fd);
    return true;
}
