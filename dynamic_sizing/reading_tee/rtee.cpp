#include <vector>
#include <Messages.hpp>
#include <ReadingReceiver.hpp>
#include <ReadingSender.hpp>
#include <ReceiverThread.hpp>
#include <SignalHandler.hpp>
#include <Utilities.hpp>

class RecvThread : public economem::ReceiverThread<economem::Reading>
{
public:
    RecvThread(economem::ReadingReceiver& receiver, const std::vector<economem::ReadingSender*>& outputs) : ReceiverThread(receiver, "RecvThread"), m_Outputs(outputs) {}

private:
    virtual void received(const economem::Reading& reading);
    const std::vector<economem::ReadingSender*>& m_Outputs;
};

int main(int argc, char** argv)
{
    if (argc < 3)
        economem::die("Usage: rtee input output [outputs...]");

    economem::ReadingReceiver receiver(argv[1]);

    std::vector<economem::ReadingSender*> outputs(argc - 2);
    for (size_t i = 0; i < outputs.size(); ++i)
        outputs[i] = new economem::ReadingSender(argv[i + 2]);

    RecvThread r(receiver, outputs);

    economem::SignalHandler::init([&] () { r.kill(); });

    if (!r.start())
        economem::die("Failed to create thread");

    r.wait();

    for (economem::ReadingSender* sender : outputs)
        delete sender;

    return 0;
}

void RecvThread::received(const economem::Reading& reading)
{
    for (economem::ReadingSender* sender : m_Outputs)
        sender->forwardExistingReading(reading);
}
