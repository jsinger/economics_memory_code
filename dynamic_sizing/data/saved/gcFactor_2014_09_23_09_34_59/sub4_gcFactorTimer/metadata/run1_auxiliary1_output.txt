economemd
    total memory: 1000 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: no
    equal recommendations: no

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/gcFactor_2014_09_23_09_34_59/daemon_log_4.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
ReadingThread: starting
TimerThread: starting
Reading from 4701: heap size 9 MB, throughput 0.991536
Clients: 1
Reading from 4701: heap size 9 MB, throughput 0.966618
Reading from 4701: heap size 9 MB, throughput 0.957914
Reading from 4701: heap size 9 MB, throughput 0.971903
Reading from 4701: heap size 11 MB, throughput 0.970157
Reading from 4701: heap size 11 MB, throughput 0.958221
Reading from 4701: heap size 14 MB, throughput 0.484184
Reading from 4701: heap size 20 MB, throughput 0.998392
Reading from 4701: heap size 19 MB, throughput 0.996338
Reading from 4701: heap size 19 MB, throughput 0.995666
Reading from 4701: heap size 19 MB, throughput 0.997328
Reading from 4701: heap size 19 MB, throughput 0.99774
Reading from 4701: heap size 19 MB, throughput 0.997745
Reading from 4701: heap size 19 MB, throughput 0.997449
Reading from 4701: heap size 19 MB, throughput 0.996821
Reading from 4701: heap size 19 MB, throughput 0.997816
Reading from 4701: heap size 19 MB, throughput 0.997149
Reading from 4701: heap size 19 MB, throughput 0.997797
Reading from 4701: heap size 19 MB, throughput 0.99716
Reading from 4701: heap size 16 MB, throughput 0.997391
Reading from 4701: heap size 19 MB, throughput 0.996837
Reading from 4701: heap size 19 MB, throughput 0.996721
Reading from 4701: heap size 18 MB, throughput 0.996852
Reading from 4701: heap size 19 MB, throughput 0.997135
Reading from 4701: heap size 19 MB, throughput 0.997102
Reading from 4701: heap size 19 MB, throughput 0.996546
Reading from 4701: heap size 19 MB, throughput 0.997571
Reading from 4701: heap size 19 MB, throughput 0.997494
Reading from 4701: heap size 20 MB, throughput 0.997601
Reading from 4701: heap size 20 MB, throughput 0.997578
Reading from 4701: heap size 20 MB, throughput 0.997864
Reading from 4701: heap size 20 MB, throughput 0.996754
Reading from 4701: heap size 20 MB, throughput 0.997645
Reading from 4701: heap size 20 MB, throughput 0.99707
Reading from 4701: heap size 20 MB, throughput 0.997383
Reading from 4701: heap size 20 MB, throughput 0.997418
Reading from 4701: heap size 20 MB, throughput 0.997612
Reading from 4701: heap size 20 MB, throughput 0.996934
Reading from 4701: heap size 19 MB, throughput 0.997054
Reading from 4701: heap size 20 MB, throughput 0.997213
Reading from 4701: heap size 20 MB, throughput 0.996412
Reading from 4701: heap size 20 MB, throughput 0.995955
Reading from 4701: heap size 20 MB, throughput 0.996252
Reading from 4701: heap size 20 MB, throughput 0.99716
Reading from 4701: heap size 20 MB, throughput 0.996228
Reading from 4701: heap size 19 MB, throughput 0.995697
Reading from 4701: heap size 19 MB, throughput 0.997591
Reading from 4701: heap size 19 MB, throughput 0.995972
Reading from 4701: heap size 20 MB, throughput 0.996377
Reading from 4701: heap size 20 MB, throughput 0.996831
Reading from 4701: heap size 20 MB, throughput 0.997553
Reading from 4701: heap size 20 MB, throughput 0.997615
Reading from 4701: heap size 20 MB, throughput 0.996899
Reading from 4701: heap size 20 MB, throughput 0.996855
Reading from 4701: heap size 20 MB, throughput 0.997813
Reading from 4701: heap size 20 MB, throughput 0.997164
Reading from 4701: heap size 20 MB, throughput 0.996522
Client 4701 died
Clients: 0
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
