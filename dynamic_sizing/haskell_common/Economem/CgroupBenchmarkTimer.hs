module Economem.CgroupBenchmarkTimer (newCgroupBenchmarkTimer,
                                      simple,
                                      fair) where

import Economem.Common
import Economem.Cgroup
import qualified Economem.BenchmarkTimer as B
import Economem.Experiment
import Economem.Files
import Economem.Java
import Economem.Records

type CgroupTimerStyle = (String, DataSize -> Integer -> DataSize)

simple :: CgroupTimerStyle
simple = ("Simple", const)

fair :: CgroupTimerStyle
fair = ("Fair", \size numBMs -> DataSize (sizeToInt (toMB size) `div` numBMs) MB)


newCgroupBenchmarkTimer :: CgroupTimerStyle ->
                           Input BenchmarkThreadsIters ->
                           DataSize ->
                           Integer ->
                           Java ->
                           Bool ->
                           Maybe SubexpInfo ->
                           [String] ->
                           IO Experiment
newCgroupBenchmarkTimer (name, partitionHeap) inputLoader cgroupSize runs j forceGrowth info extraVMArgs = do
  input <- loadInput inputLoader
  let maxHeap = partitionHeap cgroupSize . fromIntegral . length $ input
  ex <- B.newBenchmarkTimer
        ("timeBenchmarkCgroup" ++ name)
        inputLoader
        maxHeap
        runs
        j
        (Just (defaultCgroup, cgroupSize))
        0
        []
        []
        Nothing
        forceGrowth
        info
        extraVMArgs
  return (addToIntro ex ["Total cgroup size: " ++ pretty cgroupSize])
