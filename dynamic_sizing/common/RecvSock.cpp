#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <sys/socket.h>
#include <sys/un.h>
#include <unistd.h>
#include "RecvSock.hpp"
#include "Utilities.hpp"

economem::RecvSockBase::RecvSockBase(char* name) : m_Socket(-1), m_SockName(name), m_InterruptSendSock(-1), m_InterruptRecvSock(-1)
{
    if (m_SockName == NULL || ((m_Socket = socket(AF_UNIX, SOCK_DGRAM, 0)) == -1))
        economem::die("Failed to create receiver socket");

    int sockets[2];
    if (socketpair(AF_UNIX, SOCK_DGRAM, 0, sockets) != 0)
        economem::die("Failed to create receiver interrupt socket pair");

    m_InterruptSendSock = sockets[0];
    m_InterruptRecvSock = sockets[1];

    sockaddr_un recvName;
    memset(&recvName, 0, sizeof(recvName));
    recvName.sun_family = AF_UNIX;
    strcpy(recvName.sun_path, m_SockName);

    if (bind(m_Socket, (sockaddr*)&recvName, sizeof(recvName)) == -1)
        economem::die("Couldn't bind receiver socket");
}

economem::RecvSockBase::~RecvSockBase()
{
    if (m_InterruptSendSock != -1)
        close(m_InterruptSendSock);

    if (m_InterruptRecvSock != -1)
        close(m_InterruptRecvSock);

    if (m_Socket != -1)
        close(m_Socket);

    if (m_SockName != NULL)
        unlink(m_SockName);
    free(m_SockName);
}

void economem::RecvSockBase::interrupt()
{
    char c = '\0';
    send(m_InterruptSendSock, &c, sizeof(c), 0);
}

bool economem::RecvSockBase::recvBuf(void* buf, ssize_t len)
{
    // We have to be able to interrupt the receive without using signals (so we can safely do it
    // inside a host application that uses signals for its own needs (e.g. HotSpot)). So we do it
    // with select and a socket pair instead. Using signals still works where it is safe to do so
    // (e.g. in the daemon).
    fd_set fds;
    FD_ZERO(&fds);
    FD_SET(m_Socket, &fds);
    FD_SET(m_InterruptRecvSock, &fds);

    if (select(std::max(m_Socket, m_InterruptRecvSock) + 1, &fds, NULL, NULL, NULL) > 0)
    {
        if (FD_ISSET(m_InterruptRecvSock, &fds))
        {
            char c;
            recv(m_InterruptRecvSock, &c, sizeof(c), 0);
            return false;
        }
        else if (FD_ISSET(m_Socket, &fds))
        {
            ssize_t recvlen = recvfrom(m_Socket, buf, len, 0, NULL, NULL);
            if (recvlen != len)
            {
                economem::warning("RecvSock received a bad-sized message: actual size %zd, expected size %zd", recvlen, len);
                return false;
            }
            else
                return true;
        }
        else
            assert(false);
    }
    else
        return false;
}
