economemd
    total memory: 7338 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub18_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 2666: heap size 9 MB, throughput 0.984701
Clients: 1
Client 2666 has a minimum heap size of 12 MB
Reading from 2665: heap size 9 MB, throughput 0.969338
Clients: 2
Client 2665 has a minimum heap size of 1211 MB
Reading from 2666: heap size 9 MB, throughput 0.989312
Reading from 2665: heap size 9 MB, throughput 0.981888
Reading from 2666: heap size 11 MB, throughput 0.987247
Reading from 2665: heap size 11 MB, throughput 0.979449
Reading from 2666: heap size 11 MB, throughput 0.974435
Reading from 2665: heap size 11 MB, throughput 0.987342
Reading from 2666: heap size 15 MB, throughput 0.98892
Reading from 2665: heap size 15 MB, throughput 0.834857
Reading from 2666: heap size 15 MB, throughput 0.97548
Reading from 2665: heap size 18 MB, throughput 0.969443
Reading from 2665: heap size 23 MB, throughput 0.956312
Reading from 2665: heap size 28 MB, throughput 0.498901
Reading from 2665: heap size 37 MB, throughput 0.93695
Reading from 2665: heap size 39 MB, throughput 0.934802
Reading from 2666: heap size 25 MB, throughput 0.939997
Reading from 2665: heap size 40 MB, throughput 0.874671
Reading from 2665: heap size 42 MB, throughput 0.79446
Reading from 2665: heap size 44 MB, throughput 0.291419
Reading from 2666: heap size 30 MB, throughput 0.990469
Reading from 2665: heap size 59 MB, throughput 0.742946
Reading from 2665: heap size 64 MB, throughput 0.828246
Reading from 2665: heap size 65 MB, throughput 0.749812
Reading from 2666: heap size 37 MB, throughput 0.962621
Reading from 2665: heap size 69 MB, throughput 0.224567
Reading from 2665: heap size 89 MB, throughput 0.73925
Reading from 2665: heap size 95 MB, throughput 0.804138
Reading from 2666: heap size 44 MB, throughput 0.991912
Reading from 2665: heap size 96 MB, throughput 0.706299
Reading from 2666: heap size 45 MB, throughput 0.987926
Reading from 2665: heap size 100 MB, throughput 0.181389
Reading from 2665: heap size 127 MB, throughput 0.730788
Reading from 2666: heap size 46 MB, throughput 0.987627
Reading from 2665: heap size 132 MB, throughput 0.669593
Reading from 2666: heap size 52 MB, throughput 0.98396
Reading from 2665: heap size 133 MB, throughput 0.681641
Reading from 2666: heap size 52 MB, throughput 0.978491
Reading from 2666: heap size 61 MB, throughput 0.929454
Reading from 2665: heap size 138 MB, throughput 0.17551
Reading from 2665: heap size 170 MB, throughput 0.660331
Reading from 2666: heap size 61 MB, throughput 0.810839
Reading from 2665: heap size 178 MB, throughput 0.71871
Reading from 2665: heap size 180 MB, throughput 0.679022
Reading from 2666: heap size 79 MB, throughput 0.989374
Reading from 2665: heap size 183 MB, throughput 0.708179
Reading from 2665: heap size 190 MB, throughput 0.55925
Reading from 2666: heap size 80 MB, throughput 0.991193
Reading from 2665: heap size 198 MB, throughput 0.634931
Reading from 2665: heap size 204 MB, throughput 0.155587
Reading from 2665: heap size 243 MB, throughput 0.662108
Reading from 2666: heap size 91 MB, throughput 0.997789
Reading from 2665: heap size 248 MB, throughput 0.164546
Reading from 2665: heap size 291 MB, throughput 0.520241
Reading from 2665: heap size 292 MB, throughput 0.730102
Reading from 2665: heap size 292 MB, throughput 0.674355
Reading from 2665: heap size 293 MB, throughput 0.599566
Reading from 2666: heap size 92 MB, throughput 0.989502
Reading from 2665: heap size 295 MB, throughput 0.577015
Reading from 2665: heap size 301 MB, throughput 0.631542
Reading from 2665: heap size 304 MB, throughput 0.559518
Reading from 2665: heap size 316 MB, throughput 0.515533
Reading from 2665: heap size 322 MB, throughput 0.538268
Reading from 2666: heap size 104 MB, throughput 0.997432
Reading from 2666: heap size 105 MB, throughput 0.996416
Reading from 2665: heap size 331 MB, throughput 0.0834139
Reading from 2665: heap size 382 MB, throughput 0.488682
Reading from 2666: heap size 115 MB, throughput 0.996652
Reading from 2665: heap size 386 MB, throughput 0.0966161
Reading from 2665: heap size 431 MB, throughput 0.519489
Reading from 2665: heap size 365 MB, throughput 0.570328
Reading from 2665: heap size 424 MB, throughput 0.55268
Reading from 2665: heap size 429 MB, throughput 0.559082
Reading from 2666: heap size 116 MB, throughput 0.995965
Reading from 2665: heap size 430 MB, throughput 0.523405
Reading from 2665: heap size 432 MB, throughput 0.498227
Reading from 2665: heap size 438 MB, throughput 0.547927
Reading from 2665: heap size 442 MB, throughput 0.480106
Equal recommendation: 3669 MB each
Reading from 2666: heap size 124 MB, throughput 0.995525
Reading from 2665: heap size 448 MB, throughput 0.0956508
Reading from 2666: heap size 126 MB, throughput 0.985966
Reading from 2665: heap size 504 MB, throughput 0.495597
Reading from 2665: heap size 509 MB, throughput 0.510212
Reading from 2665: heap size 511 MB, throughput 0.462482
Reading from 2665: heap size 516 MB, throughput 0.618134
Reading from 2665: heap size 520 MB, throughput 0.574567
Reading from 2666: heap size 134 MB, throughput 0.997504
Reading from 2665: heap size 524 MB, throughput 0.530242
Reading from 2665: heap size 531 MB, throughput 0.473245
Reading from 2666: heap size 134 MB, throughput 0.9962
Reading from 2665: heap size 535 MB, throughput 0.091073
Reading from 2665: heap size 604 MB, throughput 0.490516
Reading from 2665: heap size 610 MB, throughput 0.543609
Reading from 2666: heap size 141 MB, throughput 0.994239
Reading from 2665: heap size 612 MB, throughput 0.628113
Reading from 2665: heap size 617 MB, throughput 0.657068
Reading from 2666: heap size 142 MB, throughput 0.993075
Reading from 2665: heap size 617 MB, throughput 0.0924966
Reading from 2666: heap size 150 MB, throughput 0.995323
Reading from 2665: heap size 689 MB, throughput 0.724957
Reading from 2665: heap size 692 MB, throughput 0.878526
Reading from 2666: heap size 150 MB, throughput 0.997111
Reading from 2665: heap size 688 MB, throughput 0.86185
Reading from 2665: heap size 698 MB, throughput 0.77502
Reading from 2665: heap size 704 MB, throughput 0.400231
Reading from 2666: heap size 157 MB, throughput 0.997953
Reading from 2665: heap size 720 MB, throughput 0.603045
Reading from 2665: heap size 728 MB, throughput 0.346897
Reading from 2666: heap size 158 MB, throughput 0.998028
Reading from 2665: heap size 733 MB, throughput 0.0254863
Reading from 2666: heap size 163 MB, throughput 0.998124
Reading from 2665: heap size 795 MB, throughput 0.25964
Equal recommendation: 3669 MB each
Reading from 2665: heap size 707 MB, throughput 0.411228
Reading from 2665: heap size 791 MB, throughput 0.814646
Reading from 2665: heap size 795 MB, throughput 0.669181
Reading from 2665: heap size 790 MB, throughput 0.57046
Reading from 2666: heap size 163 MB, throughput 0.997986
Reading from 2666: heap size 167 MB, throughput 0.998516
Reading from 2665: heap size 794 MB, throughput 0.0528476
Reading from 2665: heap size 874 MB, throughput 0.670025
Reading from 2665: heap size 874 MB, throughput 0.845187
Reading from 2666: heap size 168 MB, throughput 0.998514
Reading from 2665: heap size 881 MB, throughput 0.872223
Reading from 2665: heap size 882 MB, throughput 0.0696388
Reading from 2666: heap size 171 MB, throughput 0.996787
Reading from 2665: heap size 976 MB, throughput 0.727717
Reading from 2666: heap size 172 MB, throughput 0.991859
Reading from 2665: heap size 981 MB, throughput 0.881672
Reading from 2665: heap size 988 MB, throughput 0.93689
Reading from 2665: heap size 825 MB, throughput 0.844429
Reading from 2665: heap size 968 MB, throughput 0.810147
Reading from 2666: heap size 176 MB, throughput 0.997279
Reading from 2665: heap size 830 MB, throughput 0.850632
Reading from 2665: heap size 957 MB, throughput 0.855015
Reading from 2665: heap size 835 MB, throughput 0.809727
Reading from 2665: heap size 950 MB, throughput 0.871992
Reading from 2665: heap size 832 MB, throughput 0.868582
Reading from 2665: heap size 941 MB, throughput 0.880456
Reading from 2666: heap size 177 MB, throughput 0.997526
Reading from 2665: heap size 837 MB, throughput 0.877811
Reading from 2665: heap size 934 MB, throughput 0.878292
Reading from 2665: heap size 842 MB, throughput 0.974465
Reading from 2666: heap size 182 MB, throughput 0.99836
Reading from 2665: heap size 938 MB, throughput 0.982801
Reading from 2666: heap size 182 MB, throughput 0.997372
Reading from 2665: heap size 851 MB, throughput 0.913056
Reading from 2665: heap size 931 MB, throughput 0.816165
Reading from 2665: heap size 933 MB, throughput 0.805469
Equal recommendation: 3669 MB each
Reading from 2665: heap size 931 MB, throughput 0.817346
Reading from 2665: heap size 933 MB, throughput 0.746364
Reading from 2666: heap size 187 MB, throughput 0.997968
Reading from 2665: heap size 932 MB, throughput 0.823951
Reading from 2665: heap size 934 MB, throughput 0.828178
Reading from 2665: heap size 933 MB, throughput 0.832185
Reading from 2665: heap size 935 MB, throughput 0.823522
Reading from 2665: heap size 934 MB, throughput 0.904499
Reading from 2666: heap size 187 MB, throughput 0.997479
Reading from 2665: heap size 937 MB, throughput 0.869656
Reading from 2665: heap size 938 MB, throughput 0.900534
Reading from 2665: heap size 940 MB, throughput 0.769933
Reading from 2665: heap size 932 MB, throughput 0.635961
Reading from 2665: heap size 945 MB, throughput 0.710265
Reading from 2665: heap size 954 MB, throughput 0.686699
Reading from 2666: heap size 191 MB, throughput 0.998353
Reading from 2665: heap size 957 MB, throughput 0.686455
Reading from 2665: heap size 968 MB, throughput 0.702488
Reading from 2665: heap size 970 MB, throughput 0.668169
Reading from 2665: heap size 982 MB, throughput 0.697412
Reading from 2665: heap size 985 MB, throughput 0.666838
Reading from 2665: heap size 998 MB, throughput 0.692485
Reading from 2665: heap size 1001 MB, throughput 0.637258
Reading from 2666: heap size 191 MB, throughput 0.99744
Reading from 2665: heap size 1015 MB, throughput 0.762285
Reading from 2666: heap size 195 MB, throughput 0.997634
Reading from 2665: heap size 1018 MB, throughput 0.928114
Reading from 2666: heap size 195 MB, throughput 0.996898
Reading from 2665: heap size 1034 MB, throughput 0.967023
Reading from 2666: heap size 199 MB, throughput 0.997664
Reading from 2665: heap size 1037 MB, throughput 0.96577
Reading from 2666: heap size 199 MB, throughput 0.99687
Equal recommendation: 3669 MB each
Reading from 2665: heap size 1042 MB, throughput 0.95823
Reading from 2666: heap size 203 MB, throughput 0.997573
Reading from 2666: heap size 203 MB, throughput 0.996938
Reading from 2665: heap size 1046 MB, throughput 0.912766
Reading from 2666: heap size 207 MB, throughput 0.995758
Reading from 2666: heap size 207 MB, throughput 0.990225
Reading from 2665: heap size 1041 MB, throughput 0.966899
Reading from 2666: heap size 213 MB, throughput 0.997987
Reading from 2665: heap size 1046 MB, throughput 0.951505
Reading from 2666: heap size 213 MB, throughput 0.997737
Reading from 2666: heap size 218 MB, throughput 0.998162
Reading from 2665: heap size 1035 MB, throughput 0.962372
Reading from 2666: heap size 219 MB, throughput 0.997669
Reading from 2665: heap size 1042 MB, throughput 0.962921
Equal recommendation: 3669 MB each
Reading from 2666: heap size 224 MB, throughput 0.998176
Reading from 2666: heap size 224 MB, throughput 0.997448
Reading from 2666: heap size 212 MB, throughput 0.995698
Reading from 2665: heap size 1028 MB, throughput 0.487632
Reading from 2666: heap size 204 MB, throughput 0.997814
Reading from 2665: heap size 1122 MB, throughput 0.943412
Reading from 2666: heap size 194 MB, throughput 0.997657
Reading from 2666: heap size 186 MB, throughput 0.997514
Reading from 2665: heap size 1123 MB, throughput 0.971728
Reading from 2666: heap size 178 MB, throughput 0.997657
Reading from 2665: heap size 1123 MB, throughput 0.972256
Reading from 2666: heap size 170 MB, throughput 0.997542
Reading from 2666: heap size 163 MB, throughput 0.996409
Equal recommendation: 3669 MB each
Reading from 2666: heap size 156 MB, throughput 0.990259
Reading from 2666: heap size 162 MB, throughput 0.991806
Reading from 2665: heap size 1120 MB, throughput 0.966567
Reading from 2666: heap size 168 MB, throughput 0.997301
Reading from 2665: heap size 1123 MB, throughput 0.968689
Reading from 2666: heap size 172 MB, throughput 0.997682
Reading from 2666: heap size 166 MB, throughput 0.997578
Reading from 2665: heap size 1114 MB, throughput 0.969574
Reading from 2666: heap size 159 MB, throughput 0.997507
Reading from 2666: heap size 152 MB, throughput 0.997278
Reading from 2666: heap size 146 MB, throughput 0.997188
Reading from 2665: heap size 1119 MB, throughput 0.969074
Reading from 2666: heap size 140 MB, throughput 0.997136
Reading from 2666: heap size 135 MB, throughput 0.996874
Reading from 2665: heap size 1119 MB, throughput 0.962554
Reading from 2666: heap size 129 MB, throughput 0.996732
Reading from 2666: heap size 124 MB, throughput 0.99636
Reading from 2666: heap size 119 MB, throughput 0.996398
Equal recommendation: 3669 MB each
Reading from 2665: heap size 1120 MB, throughput 0.965532
Reading from 2666: heap size 115 MB, throughput 0.99597
Reading from 2666: heap size 110 MB, throughput 0.875558
Reading from 2666: heap size 107 MB, throughput 0.997766
Reading from 2665: heap size 1123 MB, throughput 0.966506
Reading from 2666: heap size 109 MB, throughput 0.997728
Reading from 2666: heap size 113 MB, throughput 0.998088
Reading from 2666: heap size 119 MB, throughput 0.997996
Reading from 2665: heap size 1125 MB, throughput 0.960706
Reading from 2666: heap size 119 MB, throughput 0.998068
Reading from 2666: heap size 125 MB, throughput 0.998058
Reading from 2666: heap size 125 MB, throughput 0.998278
Reading from 2665: heap size 1128 MB, throughput 0.965385
Reading from 2666: heap size 131 MB, throughput 0.996567
Reading from 2666: heap size 132 MB, throughput 0.989088
Reading from 2666: heap size 140 MB, throughput 0.99036
Reading from 2666: heap size 147 MB, throughput 0.994739
Reading from 2665: heap size 1131 MB, throughput 0.961237
Reading from 2666: heap size 155 MB, throughput 0.997715
Reading from 2666: heap size 161 MB, throughput 0.997438
Equal recommendation: 3669 MB each
Reading from 2665: heap size 1136 MB, throughput 0.963553
Reading from 2666: heap size 168 MB, throughput 0.997817
Reading from 2666: heap size 177 MB, throughput 0.997602
Reading from 2665: heap size 1140 MB, throughput 0.962665
Reading from 2666: heap size 177 MB, throughput 0.997387
Reading from 2666: heap size 182 MB, throughput 0.997707
Reading from 2665: heap size 1144 MB, throughput 0.958325
Reading from 2666: heap size 182 MB, throughput 0.997137
Reading from 2666: heap size 188 MB, throughput 0.99747
Reading from 2665: heap size 1149 MB, throughput 0.960453
Reading from 2666: heap size 188 MB, throughput 0.997209
Reading from 2666: heap size 194 MB, throughput 0.997382
Reading from 2665: heap size 1154 MB, throughput 0.95923
Reading from 2666: heap size 194 MB, throughput 0.997163
Equal recommendation: 3669 MB each
Reading from 2666: heap size 198 MB, throughput 0.997536
Reading from 2665: heap size 1158 MB, throughput 0.96161
Reading from 2666: heap size 199 MB, throughput 0.997388
Reading from 2666: heap size 204 MB, throughput 0.998206
Reading from 2665: heap size 1164 MB, throughput 0.938825
Reading from 2666: heap size 204 MB, throughput 0.994726
Reading from 2666: heap size 209 MB, throughput 0.993152
Reading from 2666: heap size 210 MB, throughput 0.997719
Reading from 2665: heap size 1166 MB, throughput 0.960222
Reading from 2666: heap size 218 MB, throughput 0.997558
Reading from 2666: heap size 218 MB, throughput 0.99757
Reading from 2665: heap size 1172 MB, throughput 0.956684
Reading from 2666: heap size 225 MB, throughput 0.99798
Equal recommendation: 3669 MB each
Reading from 2666: heap size 225 MB, throughput 0.997754
Reading from 2665: heap size 1173 MB, throughput 0.552741
Reading from 2666: heap size 231 MB, throughput 0.998141
Reading from 2666: heap size 232 MB, throughput 0.997811
Reading from 2665: heap size 1280 MB, throughput 0.937421
Reading from 2666: heap size 238 MB, throughput 0.998105
Reading from 2666: heap size 238 MB, throughput 0.99793
Reading from 2665: heap size 1280 MB, throughput 0.97721
Reading from 2666: heap size 244 MB, throughput 0.998171
Reading from 2665: heap size 1289 MB, throughput 0.977765
Reading from 2666: heap size 244 MB, throughput 0.997908
Equal recommendation: 3669 MB each
Reading from 2666: heap size 250 MB, throughput 0.993741
Reading from 2666: heap size 250 MB, throughput 0.995514
Reading from 2665: heap size 1290 MB, throughput 0.975029
Reading from 2666: heap size 259 MB, throughput 0.99822
Reading from 2665: heap size 1290 MB, throughput 0.975037
Reading from 2666: heap size 259 MB, throughput 0.997958
Reading from 2666: heap size 267 MB, throughput 0.998337
Reading from 2665: heap size 1292 MB, throughput 0.974431
Reading from 2666: heap size 267 MB, throughput 0.99835
Reading from 2665: heap size 1286 MB, throughput 0.973834
Reading from 2666: heap size 274 MB, throughput 0.998178
Equal recommendation: 3669 MB each
Reading from 2666: heap size 274 MB, throughput 0.997976
Reading from 2665: heap size 1290 MB, throughput 0.972464
Reading from 2666: heap size 280 MB, throughput 0.998162
Reading from 2665: heap size 1281 MB, throughput 0.972531
Reading from 2666: heap size 280 MB, throughput 0.998135
Reading from 2666: heap size 287 MB, throughput 0.998406
Reading from 2665: heap size 1286 MB, throughput 0.960078
Reading from 2666: heap size 287 MB, throughput 0.99262
Reading from 2666: heap size 292 MB, throughput 0.99738
Reading from 2665: heap size 1289 MB, throughput 0.96949
Equal recommendation: 3669 MB each
Reading from 2666: heap size 293 MB, throughput 0.998039
Reading from 2666: heap size 301 MB, throughput 0.996389
Reading from 2665: heap size 1289 MB, throughput 0.968135
Reading from 2666: heap size 302 MB, throughput 0.997831
Reading from 2665: heap size 1293 MB, throughput 0.965644
Reading from 2666: heap size 309 MB, throughput 0.998396
Reading from 2665: heap size 1296 MB, throughput 0.965126
Reading from 2666: heap size 311 MB, throughput 0.998192
Equal recommendation: 3669 MB each
Reading from 2666: heap size 318 MB, throughput 0.998566
Reading from 2666: heap size 319 MB, throughput 0.998271
Reading from 2666: heap size 325 MB, throughput 0.996777
Reading from 2666: heap size 326 MB, throughput 0.996763
Reading from 2666: heap size 335 MB, throughput 0.99841
Reading from 2666: heap size 335 MB, throughput 0.998294
Equal recommendation: 3669 MB each
Reading from 2666: heap size 343 MB, throughput 0.99832
Reading from 2666: heap size 344 MB, throughput 0.998019
Reading from 2666: heap size 351 MB, throughput 0.998187
Reading from 2666: heap size 351 MB, throughput 0.996307
Reading from 2665: heap size 1302 MB, throughput 0.889298
Reading from 2666: heap size 359 MB, throughput 0.998343
Equal recommendation: 3669 MB each
Reading from 2666: heap size 359 MB, throughput 0.927388
Reading from 2666: heap size 375 MB, throughput 0.99907
Reading from 2666: heap size 375 MB, throughput 0.999107
Reading from 2666: heap size 385 MB, throughput 0.999374
Equal recommendation: 3669 MB each
Reading from 2666: heap size 387 MB, throughput 0.999205
Reading from 2666: heap size 395 MB, throughput 0.999337
Reading from 2665: heap size 1464 MB, throughput 0.983222
Reading from 2666: heap size 396 MB, throughput 0.999142
Reading from 2666: heap size 402 MB, throughput 0.997259
Reading from 2665: heap size 1493 MB, throughput 0.980659
Reading from 2665: heap size 1510 MB, throughput 0.816282
Reading from 2665: heap size 1496 MB, throughput 0.724037
Reading from 2665: heap size 1522 MB, throughput 0.705229
Reading from 2665: heap size 1545 MB, throughput 0.736616
Reading from 2666: heap size 403 MB, throughput 0.996319
Reading from 2665: heap size 1559 MB, throughput 0.70308
Reading from 2665: heap size 1588 MB, throughput 0.750003
Reading from 2665: heap size 1595 MB, throughput 0.807743
Equal recommendation: 3669 MB each
Reading from 2666: heap size 413 MB, throughput 0.998981
Reading from 2665: heap size 1616 MB, throughput 0.968981
Reading from 2666: heap size 413 MB, throughput 0.998764
Reading from 2665: heap size 1623 MB, throughput 0.974343
Reading from 2666: heap size 422 MB, throughput 0.998852
Reading from 2665: heap size 1623 MB, throughput 0.972423
Reading from 2666: heap size 423 MB, throughput 0.998699
Equal recommendation: 3669 MB each
Reading from 2665: heap size 1634 MB, throughput 0.97317
Reading from 2666: heap size 431 MB, throughput 0.998765
Reading from 2665: heap size 1627 MB, throughput 0.975054
Reading from 2666: heap size 432 MB, throughput 0.99554
Reading from 2666: heap size 441 MB, throughput 0.998409
Reading from 2665: heap size 1638 MB, throughput 0.973215
Reading from 2666: heap size 442 MB, throughput 0.998545
Reading from 2665: heap size 1632 MB, throughput 0.971267
Equal recommendation: 3669 MB each
Reading from 2666: heap size 452 MB, throughput 0.998495
Reading from 2665: heap size 1640 MB, throughput 0.977991
Reading from 2666: heap size 453 MB, throughput 0.99839
Reading from 2665: heap size 1650 MB, throughput 0.979173
Reading from 2666: heap size 463 MB, throughput 0.998652
Reading from 2666: heap size 464 MB, throughput 0.997269
Equal recommendation: 3669 MB each
Reading from 2665: heap size 1653 MB, throughput 0.972868
Reading from 2666: heap size 475 MB, throughput 0.997606
Reading from 2665: heap size 1645 MB, throughput 0.975642
Reading from 2666: heap size 475 MB, throughput 0.998371
Reading from 2665: heap size 1653 MB, throughput 0.974427
Reading from 2666: heap size 487 MB, throughput 0.998458
Equal recommendation: 3669 MB each
Reading from 2665: heap size 1638 MB, throughput 0.973589
Reading from 2666: heap size 488 MB, throughput 0.998595
Reading from 2666: heap size 498 MB, throughput 0.998192
Reading from 2665: heap size 1647 MB, throughput 0.970481
Reading from 2666: heap size 500 MB, throughput 0.996482
Reading from 2665: heap size 1653 MB, throughput 0.970245
Reading from 2666: heap size 511 MB, throughput 0.998142
Equal recommendation: 3669 MB each
Reading from 2665: heap size 1654 MB, throughput 0.968513
Reading from 2666: heap size 512 MB, throughput 0.998422
Reading from 2665: heap size 1663 MB, throughput 0.968278
Reading from 2666: heap size 525 MB, throughput 0.998379
Reading from 2666: heap size 526 MB, throughput 0.99845
Equal recommendation: 3669 MB each
Reading from 2665: heap size 1668 MB, throughput 0.767756
Reading from 2666: heap size 539 MB, throughput 0.99846
Reading from 2665: heap size 1635 MB, throughput 0.994086
Reading from 2666: heap size 540 MB, throughput 0.997006
Reading from 2666: heap size 552 MB, throughput 0.99819
Reading from 2665: heap size 1639 MB, throughput 0.989113
Equal recommendation: 3669 MB each
Reading from 2666: heap size 553 MB, throughput 0.998603
Reading from 2665: heap size 1657 MB, throughput 0.988866
Reading from 2666: heap size 565 MB, throughput 0.998688
Reading from 2665: heap size 1663 MB, throughput 0.987295
Reading from 2666: heap size 567 MB, throughput 0.998582
Equal recommendation: 3669 MB each
Reading from 2665: heap size 1663 MB, throughput 0.987113
Client 2666 died
Clients: 1
Reading from 2665: heap size 1668 MB, throughput 0.984537
Reading from 2665: heap size 1650 MB, throughput 0.982907
Recommendation: one client; give it all the memory
Reading from 2665: heap size 1539 MB, throughput 0.981204
Reading from 2665: heap size 1644 MB, throughput 0.980276
Reading from 2665: heap size 1651 MB, throughput 0.978202
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 2665: heap size 1656 MB, throughput 0.988382
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 2665: heap size 1658 MB, throughput 0.990151
Reading from 2665: heap size 1683 MB, throughput 0.849709
Reading from 2665: heap size 1713 MB, throughput 0.725141
Reading from 2665: heap size 1757 MB, throughput 0.753298
Reading from 2665: heap size 1786 MB, throughput 0.743258
Client 2665 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
