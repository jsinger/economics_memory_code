economemd
    total memory: 5012 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub23_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 2299: heap size 9 MB, throughput 0.992053
Clients: 1
Client 2299 has a minimum heap size of 30 MB
Reading from 2298: heap size 9 MB, throughput 0.961525
Clients: 2
Client 2298 has a minimum heap size of 1223 MB
Reading from 2299: heap size 9 MB, throughput 0.984249
Reading from 2299: heap size 9 MB, throughput 0.976435
Reading from 2298: heap size 9 MB, throughput 0.960648
Reading from 2299: heap size 9 MB, throughput 0.971723
Reading from 2299: heap size 11 MB, throughput 0.942253
Reading from 2299: heap size 11 MB, throughput 0.909044
Reading from 2299: heap size 16 MB, throughput 0.878474
Reading from 2299: heap size 16 MB, throughput 0.827111
Reading from 2299: heap size 24 MB, throughput 0.845469
Reading from 2299: heap size 24 MB, throughput 0.845736
Reading from 2299: heap size 34 MB, throughput 0.878323
Reading from 2299: heap size 34 MB, throughput 0.874607
Reading from 2299: heap size 50 MB, throughput 0.907509
Reading from 2299: heap size 50 MB, throughput 0.909898
Reading from 2298: heap size 11 MB, throughput 0.964642
Reading from 2299: heap size 75 MB, throughput 0.944789
Reading from 2299: heap size 75 MB, throughput 0.950048
Reading from 2299: heap size 116 MB, throughput 0.958417
Reading from 2299: heap size 116 MB, throughput 0.972334
Reading from 2298: heap size 11 MB, throughput 0.976378
Reading from 2298: heap size 15 MB, throughput 0.914769
Reading from 2298: heap size 18 MB, throughput 0.931254
Reading from 2298: heap size 24 MB, throughput 0.872648
Reading from 2298: heap size 28 MB, throughput 0.675759
Reading from 2298: heap size 30 MB, throughput 0.704389
Reading from 2298: heap size 40 MB, throughput 0.34881
Reading from 2298: heap size 45 MB, throughput 0.828455
Reading from 2298: heap size 46 MB, throughput 0.856777
Reading from 2299: heap size 159 MB, throughput 0.94362
Reading from 2298: heap size 50 MB, throughput 0.562788
Reading from 2298: heap size 65 MB, throughput 0.830532
Reading from 2298: heap size 70 MB, throughput 0.428088
Reading from 2298: heap size 91 MB, throughput 0.738792
Reading from 2299: heap size 172 MB, throughput 0.827636
Reading from 2298: heap size 98 MB, throughput 0.742194
Reading from 2298: heap size 99 MB, throughput 0.6714
Reading from 2298: heap size 102 MB, throughput 0.587499
Reading from 2298: heap size 108 MB, throughput 0.3747
Reading from 2299: heap size 220 MB, throughput 0.988501
Reading from 2298: heap size 139 MB, throughput 0.715245
Reading from 2298: heap size 141 MB, throughput 0.7474
Reading from 2298: heap size 146 MB, throughput 0.744068
Reading from 2298: heap size 149 MB, throughput 0.745896
Reading from 2298: heap size 154 MB, throughput 0.681499
Reading from 2299: heap size 240 MB, throughput 0.988814
Reading from 2298: heap size 162 MB, throughput 0.59296
Reading from 2298: heap size 169 MB, throughput 0.44538
Reading from 2298: heap size 205 MB, throughput 0.616653
Reading from 2298: heap size 215 MB, throughput 0.655398
Reading from 2298: heap size 216 MB, throughput 0.658304
Reading from 2299: heap size 251 MB, throughput 0.98198
Reading from 2298: heap size 220 MB, throughput 0.319155
Reading from 2298: heap size 261 MB, throughput 0.565827
Reading from 2298: heap size 266 MB, throughput 0.672398
Reading from 2299: heap size 252 MB, throughput 0.980367
Reading from 2298: heap size 269 MB, throughput 0.67055
Reading from 2298: heap size 271 MB, throughput 0.653673
Reading from 2298: heap size 276 MB, throughput 0.6114
Reading from 2299: heap size 271 MB, throughput 0.947634
Reading from 2298: heap size 279 MB, throughput 0.398787
Reading from 2298: heap size 329 MB, throughput 0.563115
Reading from 2298: heap size 341 MB, throughput 0.517096
Reading from 2298: heap size 342 MB, throughput 0.579212
Reading from 2299: heap size 287 MB, throughput 0.987617
Reading from 2298: heap size 348 MB, throughput 0.571995
Reading from 2298: heap size 354 MB, throughput 0.604004
Reading from 2298: heap size 359 MB, throughput 0.35444
Reading from 2298: heap size 408 MB, throughput 0.495957
Reading from 2299: heap size 303 MB, throughput 0.979664
Reading from 2298: heap size 412 MB, throughput 0.532654
Numeric result:
Recommendation: 2 clients, utility 1.25146:
    h1: 3789 MB (U(h) = 0.843778*h^0.0229544)
    h2: 1223 MB (U(h) = 1.21888*h^0.001)
Recommendation: 2 clients, utility 1.25146:
    h1: 3789 MB (U(h) = 0.843778*h^0.0229544)
    h2: 1223 MB (U(h) = 1.21888*h^0.001)
Reading from 2299: heap size 304 MB, throughput 0.97239
Reading from 2298: heap size 415 MB, throughput 0.219129
Reading from 2298: heap size 468 MB, throughput 0.51118
Reading from 2298: heap size 469 MB, throughput 0.526404
Reading from 2298: heap size 470 MB, throughput 0.554161
Reading from 2299: heap size 319 MB, throughput 0.981936
Reading from 2298: heap size 470 MB, throughput 0.56239
Reading from 2298: heap size 473 MB, throughput 0.558828
Reading from 2298: heap size 482 MB, throughput 0.506465
Reading from 2298: heap size 488 MB, throughput 0.433161
Reading from 2299: heap size 322 MB, throughput 0.983257
Reading from 2298: heap size 500 MB, throughput 0.337452
Reading from 2299: heap size 340 MB, throughput 0.986263
Reading from 2298: heap size 564 MB, throughput 0.456602
Reading from 2298: heap size 571 MB, throughput 0.455699
Reading from 2299: heap size 342 MB, throughput 0.986506
Reading from 2298: heap size 574 MB, throughput 0.238844
Reading from 2298: heap size 637 MB, throughput 0.470928
Reading from 2298: heap size 630 MB, throughput 0.510177
Reading from 2298: heap size 637 MB, throughput 0.536068
Reading from 2298: heap size 638 MB, throughput 0.499997
Reading from 2299: heap size 359 MB, throughput 0.979635
Reading from 2298: heap size 640 MB, throughput 0.523818
Reading from 2299: heap size 360 MB, throughput 0.981892
Reading from 2299: heap size 379 MB, throughput 0.983423
Reading from 2298: heap size 647 MB, throughput 0.307423
Reading from 2298: heap size 719 MB, throughput 0.766611
Reading from 2299: heap size 381 MB, throughput 0.971562
Numeric result:
Recommendation: 2 clients, utility 1.39702:
    h1: 3789 MB (U(h) = 0.833316*h^0.0266168)
    h2: 1223 MB (U(h) = 1.33677*h^0.001)
Recommendation: 2 clients, utility 1.39702:
    h1: 3789 MB (U(h) = 0.833316*h^0.0266168)
    h2: 1223 MB (U(h) = 1.33677*h^0.001)
Reading from 2298: heap size 724 MB, throughput 0.823326
Reading from 2298: heap size 730 MB, throughput 0.751407
Reading from 2299: heap size 394 MB, throughput 0.976269
Reading from 2298: heap size 740 MB, throughput 0.690452
Reading from 2298: heap size 752 MB, throughput 0.617902
Reading from 2298: heap size 764 MB, throughput 0.563777
Reading from 2298: heap size 766 MB, throughput 0.501416
Reading from 2298: heap size 759 MB, throughput 0.750565
Reading from 2299: heap size 399 MB, throughput 0.96706
Reading from 2299: heap size 417 MB, throughput 0.972307
Reading from 2298: heap size 766 MB, throughput 0.510313
Reading from 2298: heap size 833 MB, throughput 0.572248
Reading from 2298: heap size 837 MB, throughput 0.678261
Reading from 2298: heap size 835 MB, throughput 0.712219
Reading from 2299: heap size 419 MB, throughput 0.978577
Reading from 2298: heap size 838 MB, throughput 0.377125
Reading from 2299: heap size 440 MB, throughput 0.983621
Reading from 2298: heap size 922 MB, throughput 0.672877
Reading from 2298: heap size 923 MB, throughput 0.808558
Reading from 2298: heap size 928 MB, throughput 0.780368
Reading from 2299: heap size 441 MB, throughput 0.979892
Reading from 2298: heap size 932 MB, throughput 0.461954
Reading from 2298: heap size 1031 MB, throughput 0.62008
Reading from 2298: heap size 1033 MB, throughput 0.676039
Reading from 2299: heap size 459 MB, throughput 0.978769
Reading from 2298: heap size 1044 MB, throughput 0.718652
Reading from 2298: heap size 1045 MB, throughput 0.757833
Reading from 2298: heap size 1049 MB, throughput 0.771572
Reading from 2298: heap size 1051 MB, throughput 0.782052
Numeric result:
Recommendation: 2 clients, utility 0.941136:
    h1: 3789 MB (U(h) = 0.834263*h^0.0263033)
    h2: 1223 MB (U(h) = 0.90185*h^0.001)
Recommendation: 2 clients, utility 0.941136:
    h1: 3789 MB (U(h) = 0.834263*h^0.0263033)
    h2: 1223 MB (U(h) = 0.90185*h^0.001)
Reading from 2299: heap size 459 MB, throughput 0.980148
Reading from 2299: heap size 479 MB, throughput 0.980131
Reading from 2298: heap size 1051 MB, throughput 0.950083
Reading from 2298: heap size 1054 MB, throughput 0.903943
Reading from 2298: heap size 1060 MB, throughput 0.856271
Reading from 2298: heap size 1061 MB, throughput 0.817113
Reading from 2298: heap size 1062 MB, throughput 0.786757
Reading from 2299: heap size 481 MB, throughput 0.976859
Reading from 2298: heap size 1065 MB, throughput 0.796039
Reading from 2298: heap size 1067 MB, throughput 0.802518
Reading from 2298: heap size 1070 MB, throughput 0.849158
Reading from 2298: heap size 1071 MB, throughput 0.880993
Reading from 2299: heap size 502 MB, throughput 0.987519
Reading from 2298: heap size 1074 MB, throughput 0.848943
Reading from 2298: heap size 1063 MB, throughput 0.80445
Reading from 2298: heap size 1072 MB, throughput 0.743796
Reading from 2298: heap size 1087 MB, throughput 0.712237
Reading from 2298: heap size 1096 MB, throughput 0.694723
Reading from 2299: heap size 504 MB, throughput 0.988448
Reading from 2298: heap size 1113 MB, throughput 0.697047
Reading from 2299: heap size 521 MB, throughput 0.98982
Reading from 2298: heap size 1120 MB, throughput 0.670486
Reading from 2298: heap size 1238 MB, throughput 0.668367
Reading from 2298: heap size 1109 MB, throughput 0.726706
Reading from 2299: heap size 523 MB, throughput 0.989565
Numeric result:
Recommendation: 2 clients, utility 0.74151:
    h1: 3789 MB (U(h) = 0.833084*h^0.026656)
    h2: 1223 MB (U(h) = 0.709498*h^0.001)
Recommendation: 2 clients, utility 0.74151:
    h1: 3789 MB (U(h) = 0.833084*h^0.026656)
    h2: 1223 MB (U(h) = 0.709498*h^0.001)
Reading from 2299: heap size 537 MB, throughput 0.98387
Reading from 2298: heap size 1247 MB, throughput 0.93269
Reading from 2299: heap size 539 MB, throughput 0.983138
Reading from 2298: heap size 1119 MB, throughput 0.954557
Reading from 2299: heap size 560 MB, throughput 0.988787
Reading from 2299: heap size 560 MB, throughput 0.985631
Reading from 2298: heap size 1243 MB, throughput 0.968004
Reading from 2299: heap size 581 MB, throughput 0.988011
Reading from 2299: heap size 581 MB, throughput 0.990894
Reading from 2298: heap size 1137 MB, throughput 0.965367
Numeric result:
Recommendation: 2 clients, utility 0.658945:
    h1: 3227.35 MB (U(h) = 0.833054*h^0.0266635)
    h2: 1784.65 MB (U(h) = 0.571084*h^0.0147368)
Recommendation: 2 clients, utility 0.658945:
    h1: 3227.93 MB (U(h) = 0.833054*h^0.0266635)
    h2: 1784.07 MB (U(h) = 0.571084*h^0.0147368)
Reading from 2299: heap size 595 MB, throughput 0.985773
Reading from 2298: heap size 1232 MB, throughput 0.967016
Reading from 2299: heap size 598 MB, throughput 0.98837
Reading from 2299: heap size 616 MB, throughput 0.991589
Reading from 2298: heap size 1229 MB, throughput 0.96307
Reading from 2299: heap size 618 MB, throughput 0.989963
Reading from 2299: heap size 636 MB, throughput 0.991115
Reading from 2298: heap size 1224 MB, throughput 0.960705
Numeric result:
Recommendation: 2 clients, utility 0.729335:
    h1: 1407.62 MB (U(h) = 0.832804*h^0.0267336)
    h2: 3604.38 MB (U(h) = 0.411839*h^0.0684561)
Recommendation: 2 clients, utility 0.729335:
    h1: 1407.6 MB (U(h) = 0.832804*h^0.0267336)
    h2: 3604.4 MB (U(h) = 0.411839*h^0.0684561)
Reading from 2299: heap size 637 MB, throughput 0.992894
Reading from 2298: heap size 1229 MB, throughput 0.965473
Reading from 2299: heap size 650 MB, throughput 0.989367
Reading from 2299: heap size 652 MB, throughput 0.988706
Reading from 2298: heap size 1230 MB, throughput 0.96646
Reading from 2299: heap size 674 MB, throughput 0.990144
Reading from 2299: heap size 674 MB, throughput 0.991697
Reading from 2298: heap size 1232 MB, throughput 0.970629
Numeric result:
Recommendation: 2 clients, utility 0.748871:
    h1: 1249.07 MB (U(h) = 0.832789*h^0.0267382)
    h2: 3762.93 MB (U(h) = 0.382685*h^0.0806127)
Recommendation: 2 clients, utility 0.748871:
    h1: 1248.35 MB (U(h) = 0.832789*h^0.0267382)
    h2: 3763.65 MB (U(h) = 0.382685*h^0.0806127)
Reading from 2299: heap size 689 MB, throughput 0.990385
Reading from 2298: heap size 1225 MB, throughput 0.967481
Reading from 2299: heap size 691 MB, throughput 0.989354
Reading from 2299: heap size 711 MB, throughput 0.99058
Reading from 2298: heap size 1231 MB, throughput 0.971315
Reading from 2299: heap size 712 MB, throughput 0.993125
Numeric result:
Recommendation: 2 clients, utility 0.766529:
    h1: 1161.72 MB (U(h) = 0.833077*h^0.0266597)
    h2: 3850.28 MB (U(h) = 0.367591*h^0.088346)
Recommendation: 2 clients, utility 0.766529:
    h1: 1161.84 MB (U(h) = 0.833077*h^0.0266597)
    h2: 3850.16 MB (U(h) = 0.367591*h^0.088346)
Reading from 2298: heap size 1222 MB, throughput 0.968811
Reading from 2299: heap size 726 MB, throughput 0.994322
Reading from 2299: heap size 729 MB, throughput 0.991776
Reading from 2298: heap size 1227 MB, throughput 0.9719
Reading from 2299: heap size 744 MB, throughput 0.990017
Reading from 2298: heap size 1229 MB, throughput 0.970836
Reading from 2299: heap size 745 MB, throughput 0.988858
Reading from 2299: heap size 766 MB, throughput 0.989622
Numeric result:
Recommendation: 2 clients, utility 0.83402:
    h1: 840.801 MB (U(h) = 0.833747*h^0.0264817)
    h2: 4171.2 MB (U(h) = 0.279971*h^0.131366)
Recommendation: 2 clients, utility 0.83402:
    h1: 840.853 MB (U(h) = 0.833747*h^0.0264817)
    h2: 4171.15 MB (U(h) = 0.279971*h^0.131366)
Reading from 2298: heap size 1230 MB, throughput 0.969926
Reading from 2299: heap size 767 MB, throughput 0.991467
Reading from 2298: heap size 1234 MB, throughput 0.972092
Reading from 2299: heap size 786 MB, throughput 0.993945
Reading from 2299: heap size 788 MB, throughput 0.992157
Reading from 2298: heap size 1236 MB, throughput 0.969968
Numeric result:
Recommendation: 2 clients, utility 0.893555:
    h1: 688.11 MB (U(h) = 0.83404*h^0.0264044)
    h2: 4323.89 MB (U(h) = 0.224865*h^0.165871)
Recommendation: 2 clients, utility 0.893555:
    h1: 688.278 MB (U(h) = 0.83404*h^0.0264044)
    h2: 4323.72 MB (U(h) = 0.224865*h^0.165871)
Reading from 2299: heap size 807 MB, throughput 0.991905
Reading from 2299: heap size 758 MB, throughput 0.987524
Reading from 2298: heap size 1242 MB, throughput 0.942363
Reading from 2299: heap size 722 MB, throughput 0.993605
Reading from 2299: heap size 697 MB, throughput 0.990384
Reading from 2298: heap size 1312 MB, throughput 0.981631
Reading from 2299: heap size 679 MB, throughput 0.99089
Numeric result:
Recommendation: 2 clients, utility 0.932159:
    h1: 619.989 MB (U(h) = 0.834433*h^0.0263013)
    h2: 4392.01 MB (U(h) = 0.19767*h^0.186324)
Recommendation: 2 clients, utility 0.932159:
    h1: 619.974 MB (U(h) = 0.834433*h^0.0263013)
    h2: 4392.03 MB (U(h) = 0.19767*h^0.186324)
Reading from 2298: heap size 1307 MB, throughput 0.985483
Reading from 2299: heap size 693 MB, throughput 0.992999
Reading from 2299: heap size 660 MB, throughput 0.991133
Reading from 2298: heap size 1318 MB, throughput 0.98821
Reading from 2299: heap size 644 MB, throughput 0.988883
Reading from 2299: heap size 615 MB, throughput 0.989853
Reading from 2298: heap size 1326 MB, throughput 0.987351
Reading from 2299: heap size 636 MB, throughput 0.990816
Numeric result:
Recommendation: 2 clients, utility 1.03631:
    h1: 492.068 MB (U(h) = 0.834295*h^0.0263404)
    h2: 4519.93 MB (U(h) = 0.137687*h^0.241952)
Recommendation: 2 clients, utility 1.03631:
    h1: 492.067 MB (U(h) = 0.834295*h^0.0263404)
    h2: 4519.93 MB (U(h) = 0.137687*h^0.241952)
Reading from 2299: heap size 613 MB, throughput 0.988833
Reading from 2298: heap size 1328 MB, throughput 0.986907
Reading from 2299: heap size 589 MB, throughput 0.986755
Reading from 2299: heap size 544 MB, throughput 0.990604
Reading from 2298: heap size 1324 MB, throughput 0.984745
Reading from 2299: heap size 524 MB, throughput 0.990335
Reading from 2299: heap size 517 MB, throughput 0.988064
Numeric result:
Recommendation: 2 clients, utility 1.07639:
    h1: 460.96 MB (U(h) = 0.834057*h^0.0264269)
    h2: 4551.04 MB (U(h) = 0.121886*h^0.260907)
Recommendation: 2 clients, utility 1.07639:
    h1: 460.969 MB (U(h) = 0.834057*h^0.0264269)
    h2: 4551.03 MB (U(h) = 0.121886*h^0.260907)
Reading from 2299: heap size 496 MB, throughput 0.971186
Reading from 2298: heap size 1219 MB, throughput 0.983684
Reading from 2299: heap size 476 MB, throughput 0.991531
Reading from 2299: heap size 453 MB, throughput 0.991112
Reading from 2299: heap size 489 MB, throughput 0.99214
Reading from 2298: heap size 1311 MB, throughput 0.98325
Reading from 2299: heap size 471 MB, throughput 0.987459
Reading from 2299: heap size 465 MB, throughput 0.990252
Reading from 2298: heap size 1239 MB, throughput 0.979379
Reading from 2299: heap size 440 MB, throughput 0.984886
Reading from 2299: heap size 478 MB, throughput 0.988852
Numeric result:
Recommendation: 2 clients, utility 1.23975:
    h1: 362.072 MB (U(h) = 0.833893*h^0.026544)
    h2: 4649.93 MB (U(h) = 0.0714532*h^0.340914)
Recommendation: 2 clients, utility 1.23975:
    h1: 362.052 MB (U(h) = 0.833893*h^0.026544)
    h2: 4649.95 MB (U(h) = 0.0714532*h^0.340914)
Reading from 2299: heap size 450 MB, throughput 0.986617
Reading from 2298: heap size 1306 MB, throughput 0.979119
Reading from 2299: heap size 436 MB, throughput 0.987974
Reading from 2299: heap size 417 MB, throughput 0.986813
Reading from 2299: heap size 403 MB, throughput 0.987655
Reading from 2299: heap size 388 MB, throughput 0.988219
Reading from 2298: heap size 1310 MB, throughput 0.976316
Reading from 2299: heap size 375 MB, throughput 0.984975
Reading from 2299: heap size 372 MB, throughput 0.980337
Reading from 2299: heap size 357 MB, throughput 0.985028
Reading from 2298: heap size 1312 MB, throughput 0.976069
Reading from 2299: heap size 384 MB, throughput 0.975456
Numeric result:
Recommendation: 2 clients, utility 1.35632:
    h1: 317.897 MB (U(h) = 0.834301*h^0.0265821)
    h2: 4694.1 MB (U(h) = 0.0505021*h^0.392536)
Recommendation: 2 clients, utility 1.35632:
    h1: 317.881 MB (U(h) = 0.834301*h^0.0265821)
    h2: 4694.12 MB (U(h) = 0.0505021*h^0.392536)
Reading from 2299: heap size 365 MB, throughput 0.984094
Reading from 2299: heap size 339 MB, throughput 0.988052
Reading from 2299: heap size 341 MB, throughput 0.981601
Reading from 2299: heap size 332 MB, throughput 0.985502
Reading from 2298: heap size 1314 MB, throughput 0.976736
Reading from 2299: heap size 323 MB, throughput 0.989683
Reading from 2299: heap size 301 MB, throughput 0.986138
Reading from 2299: heap size 326 MB, throughput 0.987042
Reading from 2299: heap size 295 MB, throughput 0.988752
Reading from 2299: heap size 318 MB, throughput 0.990056
Reading from 2298: heap size 1317 MB, throughput 0.977233
Reading from 2299: heap size 289 MB, throughput 0.982285
Reading from 2299: heap size 309 MB, throughput 0.987766
Reading from 2299: heap size 310 MB, throughput 0.989037
Numeric result:
Recommendation: 2 clients, utility 1.44096:
    h1: 288.901 MB (U(h) = 0.838516*h^0.0260825)
    h2: 4723.1 MB (U(h) = 0.0402048*h^0.426397)
Recommendation: 2 clients, utility 1.44096:
    h1: 288.909 MB (U(h) = 0.838516*h^0.0260825)
    h2: 4723.09 MB (U(h) = 0.0402048*h^0.426397)
Reading from 2299: heap size 321 MB, throughput 0.995162
Reading from 2299: heap size 321 MB, throughput 0.995162
Reading from 2299: heap size 321 MB, throughput 0.955254
Reading from 2299: heap size 296 MB, throughput 0.979306
Reading from 2298: heap size 1323 MB, throughput 0.972304
Reading from 2299: heap size 296 MB, throughput 0.98556
Reading from 2299: heap size 282 MB, throughput 0.985038
Reading from 2299: heap size 297 MB, throughput 0.982001
Reading from 2299: heap size 276 MB, throughput 0.981224
Reading from 2299: heap size 289 MB, throughput 0.963508
Reading from 2298: heap size 1326 MB, throughput 0.969948
Reading from 2299: heap size 267 MB, throughput 0.98642
Reading from 2299: heap size 282 MB, throughput 0.984075
Reading from 2299: heap size 281 MB, throughput 0.984045
Reading from 2299: heap size 294 MB, throughput 0.986575
Reading from 2299: heap size 278 MB, throughput 0.98891
Reading from 2299: heap size 286 MB, throughput 0.957678
Reading from 2298: heap size 1335 MB, throughput 0.968714
Reading from 2299: heap size 286 MB, throughput 0.965488
Numeric result:
Recommendation: 2 clients, utility 1.60983:
    h1: 102.474 MB (U(h) = 0.926765*h^0.0100866)
    h2: 4909.53 MB (U(h) = 0.0272789*h^0.483252)
Recommendation: 2 clients, utility 1.60983:
    h1: 102.473 MB (U(h) = 0.926765*h^0.0100866)
    h2: 4909.53 MB (U(h) = 0.0272789*h^0.483252)
Reading from 2299: heap size 304 MB, throughput 0.978331
Reading from 2299: heap size 273 MB, throughput 0.973808
Reading from 2299: heap size 252 MB, throughput 0.973521
Reading from 2299: heap size 240 MB, throughput 0.941568
Reading from 2299: heap size 234 MB, throughput 0.945138
Reading from 2298: heap size 1342 MB, throughput 0.96945
Reading from 2299: heap size 224 MB, throughput 0.983806
Reading from 2299: heap size 174 MB, throughput 0.986006
Reading from 2299: heap size 160 MB, throughput 0.989772
Reading from 2299: heap size 155 MB, throughput 0.991707
Reading from 2299: heap size 151 MB, throughput 0.980829
Reading from 2299: heap size 154 MB, throughput 0.978337
Reading from 2299: heap size 150 MB, throughput 0.980438
Reading from 2299: heap size 150 MB, throughput 0.978297
Reading from 2299: heap size 140 MB, throughput 0.976462
Reading from 2299: heap size 142 MB, throughput 0.977166
Reading from 2299: heap size 132 MB, throughput 0.979512
Reading from 2298: heap size 1347 MB, throughput 0.967064
Reading from 2299: heap size 135 MB, throughput 0.986087
Reading from 2299: heap size 119 MB, throughput 0.987583
Reading from 2299: heap size 132 MB, throughput 0.980306
Reading from 2299: heap size 120 MB, throughput 0.970429
Reading from 2299: heap size 125 MB, throughput 0.952369
Reading from 2299: heap size 120 MB, throughput 0.968135
Reading from 2299: heap size 120 MB, throughput 0.964061
Reading from 2299: heap size 116 MB, throughput 0.971021
Reading from 2299: heap size 118 MB, throughput 0.970358
Reading from 2299: heap size 110 MB, throughput 0.97035
Reading from 2299: heap size 113 MB, throughput 0.204563
Reading from 2299: heap size 102 MB, throughput 0.973103
Reading from 2299: heap size 114 MB, throughput 0.980627
Numeric result:
Recommendation: 2 clients, utility 1.60629:
    h1: 539.772 MB (U(h) = 0.681049*h^0.0598513)
    h2: 4472.23 MB (U(h) = 0.0250529*h^0.495891)
Recommendation: 2 clients, utility 1.60629:
    h1: 539.773 MB (U(h) = 0.681049*h^0.0598513)
    h2: 4472.23 MB (U(h) = 0.0250529*h^0.495891)
Reading from 2299: heap size 95 MB, throughput 0.989393
Reading from 2299: heap size 113 MB, throughput 0.988846
Reading from 2299: heap size 113 MB, throughput 0.968716
Reading from 2299: heap size 118 MB, throughput 0.956282
Reading from 2299: heap size 118 MB, throughput 0.958756
Reading from 2298: heap size 1354 MB, throughput 0.967871
Reading from 2299: heap size 126 MB, throughput 0.963412
Reading from 2299: heap size 139 MB, throughput 0.97045
Reading from 2299: heap size 149 MB, throughput 0.977801
Reading from 2299: heap size 149 MB, throughput 0.984438
Reading from 2299: heap size 158 MB, throughput 0.990149
Reading from 2299: heap size 159 MB, throughput 0.9925
Reading from 2299: heap size 167 MB, throughput 0.99339
Reading from 2299: heap size 168 MB, throughput 0.984405
Reading from 2299: heap size 176 MB, throughput 0.978239
Reading from 2299: heap size 176 MB, throughput 0.9602
Reading from 2299: heap size 183 MB, throughput 0.918578
Reading from 2299: heap size 183 MB, throughput 0.857883
Reading from 2298: heap size 1356 MB, throughput 0.970518
Reading from 2299: heap size 191 MB, throughput 0.937491
Reading from 2299: heap size 192 MB, throughput 0.968697
Reading from 2299: heap size 200 MB, throughput 0.968101
Reading from 2299: heap size 201 MB, throughput 0.974541
Reading from 2299: heap size 215 MB, throughput 0.955149
Reading from 2299: heap size 217 MB, throughput 0.972242
Reading from 2299: heap size 225 MB, throughput 0.98411
Reading from 2299: heap size 227 MB, throughput 0.974561
Reading from 2298: heap size 1364 MB, throughput 0.971848
Numeric result:
Recommendation: 2 clients, utility 1.90618:
    h1: 137.835 MB (U(h) = 0.889739*h^0.0167777)
    h2: 4874.16 MB (U(h) = 0.0127936*h^0.593296)
Recommendation: 2 clients, utility 1.90618:
    h1: 137.835 MB (U(h) = 0.889739*h^0.0167777)
    h2: 4874.16 MB (U(h) = 0.0127936*h^0.593296)
Reading from 2299: heap size 240 MB, throughput 0.94541
Reading from 2299: heap size 221 MB, throughput 0.918278
Reading from 2299: heap size 207 MB, throughput 0.933338
Reading from 2299: heap size 196 MB, throughput 0.969016
Reading from 2299: heap size 190 MB, throughput 0.972638
Reading from 2299: heap size 190 MB, throughput 0.968658
Reading from 2299: heap size 182 MB, throughput 0.966861
Reading from 2299: heap size 176 MB, throughput 0.971739
Reading from 2298: heap size 1364 MB, throughput 0.970442
Reading from 2299: heap size 174 MB, throughput 0.93444
Reading from 2299: heap size 164 MB, throughput 0.96774
Reading from 2299: heap size 185 MB, throughput 0.930196
Reading from 2299: heap size 180 MB, throughput 0.951601
Reading from 2299: heap size 180 MB, throughput 0.964586
Reading from 2299: heap size 173 MB, throughput 0.964595
Reading from 2299: heap size 177 MB, throughput 0.962817
Reading from 2299: heap size 171 MB, throughput 0.0881027
Reading from 2299: heap size 171 MB, throughput 0.950773
Reading from 2299: heap size 129 MB, throughput 0.74467
Reading from 2299: heap size 150 MB, throughput 0.858634
Reading from 2299: heap size 134 MB, throughput 0.767384
Reading from 2299: heap size 147 MB, throughput 0.795545
Reading from 2299: heap size 126 MB, throughput 0.840188
Reading from 2299: heap size 144 MB, throughput 0.866408
Reading from 2299: heap size 125 MB, throughput 0.954745
Reading from 2299: heap size 144 MB, throughput 0.933862
Reading from 2299: heap size 123 MB, throughput 0.964219
Reading from 2299: heap size 142 MB, throughput 0.983332
Reading from 2299: heap size 125 MB, throughput 0.985867
Reading from 2299: heap size 139 MB, throughput 0.970847
Reading from 2299: heap size 136 MB, throughput 0.945835
Numeric result:
Recommendation: 2 clients, utility 1.88203:
    h1: 384.984 MB (U(h) = 0.733488*h^0.049372)
    h2: 4627.02 MB (U(h) = 0.0127949*h^0.593279)
Recommendation: 2 clients, utility 1.88203:
    h1: 385.05 MB (U(h) = 0.733488*h^0.049372)
    h2: 4626.95 MB (U(h) = 0.0127949*h^0.593279)
Reading from 2299: heap size 137 MB, throughput 0.915152
Reading from 2299: heap size 138 MB, throughput 0.833355
Reading from 2299: heap size 146 MB, throughput 0.876101
Reading from 2299: heap size 146 MB, throughput 0.887079
Reading from 2299: heap size 154 MB, throughput 0.938008
Reading from 2299: heap size 154 MB, throughput 0.96813
Reading from 2299: heap size 160 MB, throughput 0.984632
Reading from 2299: heap size 161 MB, throughput 0.973746
Reading from 2299: heap size 166 MB, throughput 0.97219
Reading from 2299: heap size 168 MB, throughput 0.948973
Reading from 2299: heap size 181 MB, throughput 0.960676
Reading from 2299: heap size 200 MB, throughput 0.963656
Reading from 2299: heap size 209 MB, throughput 0.980347
Reading from 2299: heap size 210 MB, throughput 0.987263
Reading from 2299: heap size 217 MB, throughput 0.980583
Reading from 2299: heap size 219 MB, throughput 0.981442
Reading from 2299: heap size 232 MB, throughput 0.981784
Reading from 2299: heap size 234 MB, throughput 0.980374
Reading from 2299: heap size 248 MB, throughput 0.976952
Reading from 2299: heap size 249 MB, throughput 0.975409
Reading from 2299: heap size 266 MB, throughput 0.983086
Reading from 2299: heap size 267 MB, throughput 0.982693
Reading from 2299: heap size 284 MB, throughput 0.985564
Reading from 2299: heap size 284 MB, throughput 0.885663
Numeric result:
Recommendation: 2 clients, utility 1.89884:
    h1: 463.36 MB (U(h) = 0.692126*h^0.0604367)
    h2: 4548.64 MB (U(h) = 0.0127949*h^0.593279)
Recommendation: 2 clients, utility 1.89884:
    h1: 463.365 MB (U(h) = 0.692126*h^0.0604367)
    h2: 4548.64 MB (U(h) = 0.0127949*h^0.593279)
Reading from 2299: heap size 297 MB, throughput 0.975794
Reading from 2299: heap size 301 MB, throughput 0.977151
Reading from 2299: heap size 324 MB, throughput 0.967099
Reading from 2299: heap size 325 MB, throughput 0.969341
Reading from 2299: heap size 350 MB, throughput 0.97865
Reading from 2299: heap size 351 MB, throughput 0.98004
Reading from 2299: heap size 374 MB, throughput 0.979508
Reading from 2299: heap size 375 MB, throughput 0.984568
Reading from 2299: heap size 397 MB, throughput 0.985203
Reading from 2299: heap size 400 MB, throughput 0.986255
Reading from 2298: heap size 1371 MB, throughput 0.990974
Reading from 2299: heap size 422 MB, throughput 0.989721
Reading from 2299: heap size 424 MB, throughput 0.982893
Numeric result:
Recommendation: 2 clients, utility 1.99632:
    h1: 416.346 MB (U(h) = 0.704393*h^0.0569626)
    h2: 4595.65 MB (U(h) = 0.010011*h^0.628757)
Recommendation: 2 clients, utility 1.99632:
    h1: 416.346 MB (U(h) = 0.704393*h^0.0569626)
    h2: 4595.65 MB (U(h) = 0.010011*h^0.628757)
Reading from 2299: heap size 448 MB, throughput 0.98883
Reading from 2299: heap size 420 MB, throughput 0.983984
Reading from 2299: heap size 419 MB, throughput 0.988176
Reading from 2299: heap size 392 MB, throughput 0.986488
Reading from 2299: heap size 411 MB, throughput 0.981524
Reading from 2298: heap size 1371 MB, throughput 0.971075
Reading from 2299: heap size 413 MB, throughput 0.986979
Reading from 2299: heap size 434 MB, throughput 0.982388
Reading from 2298: heap size 1471 MB, throughput 0.988298
Reading from 2299: heap size 422 MB, throughput 0.98767
Reading from 2298: heap size 1518 MB, throughput 0.984451
Reading from 2298: heap size 1519 MB, throughput 0.977297
Reading from 2298: heap size 1524 MB, throughput 0.96598
Reading from 2298: heap size 1518 MB, throughput 0.942807
Reading from 2299: heap size 395 MB, throughput 0.986176
Numeric result:
Recommendation: 2 clients, utility 2.23282:
    h1: 346.267 MB (U(h) = 0.719754*h^0.0526648)
    h2: 4665.73 MB (U(h) = 0.00568105*h^0.70961)
Recommendation: 2 clients, utility 2.23282:
    h1: 346.274 MB (U(h) = 0.719754*h^0.0526648)
    h2: 4665.73 MB (U(h) = 0.00568105*h^0.70961)
Reading from 2298: heap size 1523 MB, throughput 0.926622
Reading from 2298: heap size 1520 MB, throughput 0.91821
Reading from 2299: heap size 419 MB, throughput 0.988179
Reading from 2298: heap size 1527 MB, throughput 0.957386
Reading from 2299: heap size 388 MB, throughput 0.991255
Reading from 2299: heap size 372 MB, throughput 0.974983
Reading from 2299: heap size 372 MB, throughput 0.977241
Reading from 2299: heap size 349 MB, throughput 0.981503
Reading from 2298: heap size 1533 MB, throughput 0.989242
Reading from 2299: heap size 341 MB, throughput 0.985296
Reading from 2299: heap size 373 MB, throughput 0.98903
Reading from 2299: heap size 346 MB, throughput 0.985916
Reading from 2299: heap size 339 MB, throughput 0.985568
Reading from 2298: heap size 1540 MB, throughput 0.989752
Reading from 2299: heap size 351 MB, throughput 0.988487
Numeric result:
Recommendation: 2 clients, utility 2.59146:
    h1: 301.568 MB (U(h) = 0.720505*h^0.0523976)
    h2: 4710.43 MB (U(h) = 0.00262919*h^0.818438)
Recommendation: 2 clients, utility 2.59146:
    h1: 301.569 MB (U(h) = 0.720505*h^0.0523976)
    h2: 4710.43 MB (U(h) = 0.00262919*h^0.818438)
Reading from 2299: heap size 329 MB, throughput 0.986657
Reading from 2299: heap size 320 MB, throughput 0.987949
Reading from 2299: heap size 309 MB, throughput 0.989143
Reading from 2299: heap size 300 MB, throughput 0.986333
Reading from 2299: heap size 310 MB, throughput 0.986711
Reading from 2298: heap size 1548 MB, throughput 0.990015
Reading from 2299: heap size 292 MB, throughput 0.988987
Reading from 2299: heap size 317 MB, throughput 0.990369
Reading from 2299: heap size 300 MB, throughput 0.986518
Reading from 2299: heap size 309 MB, throughput 0.986873
Reading from 2299: heap size 292 MB, throughput 0.988724
Reading from 2298: heap size 1553 MB, throughput 0.990537
Reading from 2299: heap size 307 MB, throughput 0.98615
Reading from 2299: heap size 300 MB, throughput 0.987629
Numeric result:
Recommendation: 2 clients, utility 2.47193:
    h1: 372.576 MB (U(h) = 0.678674*h^0.0630436)
    h2: 4639.42 MB (U(h) = 0.00331981*h^0.784997)
Recommendation: 2 clients, utility 2.47193:
    h1: 372.594 MB (U(h) = 0.678674*h^0.0630436)
    h2: 4639.41 MB (U(h) = 0.00331981*h^0.784997)
Reading from 2299: heap size 309 MB, throughput 0.989089
Reading from 2299: heap size 323 MB, throughput 0.990335
Reading from 2299: heap size 322 MB, throughput 0.948623
Reading from 2299: heap size 335 MB, throughput 0.939098
Reading from 2298: heap size 1541 MB, throughput 0.989093
Reading from 2299: heap size 338 MB, throughput 0.941611
Client 2299 died
Clients: 1
Reading from 2298: heap size 1402 MB, throughput 0.989178
Recommendation: one client; give it all the memory
Reading from 2298: heap size 1525 MB, throughput 0.988946
Reading from 2298: heap size 1428 MB, throughput 0.988295
Recommendation: one client; give it all the memory
Reading from 2298: heap size 1519 MB, throughput 0.98782
Reading from 2298: heap size 1526 MB, throughput 0.987024
Recommendation: one client; give it all the memory
Reading from 2298: heap size 1528 MB, throughput 0.986066
Reading from 2298: heap size 1528 MB, throughput 0.984954
Recommendation: one client; give it all the memory
Reading from 2298: heap size 1531 MB, throughput 0.984687
Reading from 2298: heap size 1537 MB, throughput 0.983499
Recommendation: one client; give it all the memory
Reading from 2298: heap size 1541 MB, throughput 0.982575
Reading from 2298: heap size 1551 MB, throughput 0.981175
Recommendation: one client; give it all the memory
Reading from 2298: heap size 1559 MB, throughput 0.980422
Reading from 2298: heap size 1567 MB, throughput 0.980169
Recommendation: one client; give it all the memory
Reading from 2298: heap size 1575 MB, throughput 0.980245
Reading from 2298: heap size 1580 MB, throughput 0.979838
Recommendation: one client; give it all the memory
Reading from 2298: heap size 1587 MB, throughput 0.980051
Reading from 2298: heap size 1589 MB, throughput 0.979395
Recommendation: one client; give it all the memory
Reading from 2298: heap size 1597 MB, throughput 0.979557
Reading from 2298: heap size 1597 MB, throughput 0.979472
Recommendation: one client; give it all the memory
Reading from 2298: heap size 1603 MB, throughput 0.979458
Reading from 2298: heap size 1604 MB, throughput 0.979051
Recommendation: one client; give it all the memory
Reading from 2298: heap size 1608 MB, throughput 0.979208
Recommendation: one client; give it all the memory
Reading from 2298: heap size 1609 MB, throughput 0.979428
Reading from 2298: heap size 1611 MB, throughput 0.97571
Recommendation: one client; give it all the memory
Reading from 2298: heap size 1667 MB, throughput 0.987865
Reading from 2298: heap size 1659 MB, throughput 0.991045
Recommendation: one client; give it all the memory
Reading from 2298: heap size 1672 MB, throughput 0.991826
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 2298: heap size 1684 MB, throughput 0.995351
Recommendation: one client; give it all the memory
Reading from 2298: heap size 1551 MB, throughput 0.993342
Reading from 2298: heap size 1677 MB, throughput 0.986738
Reading from 2298: heap size 1679 MB, throughput 0.970918
Reading from 2298: heap size 1701 MB, throughput 0.951341
Reading from 2298: heap size 1723 MB, throughput 0.918366
Reading from 2298: heap size 1762 MB, throughput 0.888474
Client 2298 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
