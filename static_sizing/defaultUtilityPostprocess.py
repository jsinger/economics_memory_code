#!/usr/bin/env python
# Process an existing default utility dataset

import argparse
import argtypes
import staticutility as su
import utilitymodel


def postprocess(dataDir):
    for model in utilitymodel.models:
        resultsFile = dataDir.resultsFile(model)

        with open(dataDir.rawResultsFile()) as infile, open(resultsFile, "w") as outfile:
            for line in infile:
                raw = su.defaultUtilityRawResultFromStr(line)
                U = utilitymodel.get(model).combineRawUtilities(*raw.itersPerSecs)
                stddevU = utilitymodel.get(model).propagateRawErrors(raw.itersPerSecs, U, raw.stddevItersPerSecs)

                su.DefaultUtilityFullResult(raw, U, stddevU).append(outfile)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Process an existing default utility dataset")
    parser.add_argument("dir", type=argtypes.existing_absolute_dir, help="Directory containing data")
    args = parser.parse_args()
    postprocess(su.DefaultUtilityDir(args.dir))
