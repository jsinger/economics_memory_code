#ifndef READINGCACHE_HPP
#define READINGCACHE_HPP

#include <cstdio>
#include <string>
#include <sys/types.h>
#include <vector>

class ReadingIterator;


class ReadingCache
{
public:
    static const std::string DEFAULT_CACHE;

    virtual ~ReadingCache() {};
    virtual std::string getName() const = 0;
    virtual bool isValid() const = 0;
    virtual void addReading(unsigned long long heapSizeMB, double throughput) = 0;
    virtual ReadingIterator* newIterator() const = 0;
    virtual void dumpTo(FILE* fd) const = 0;

    static ReadingCache* fromName(const std::string& name);
    static std::vector<std::string> allCacheNames();

protected:
    ReadingCache() {}
};

#endif
