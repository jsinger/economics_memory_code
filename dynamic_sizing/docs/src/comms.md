Communications Protocol {#comms}
=======================

Communication takes place over Unix sockets. There are four types of messages:

 - economem::Reading: sent from clients to the daemon
 - economem::Recommendation: sent from the daemon to clients
 - economem::Command: sent from the interactive console (or other management programs) to the daemon
 - economem::CommandResult: send from the daemon to the interactive console in response to a command

Each message type has its own economem::SendSock and economem::RecvSock subclasses:

 - economem::ReadingSender and economem::ReadingReceiver
 - economem::RecommendationSender and economem::RecommendationReceiver
 - economem::CommandSender and economem::CommandReceiver
 - economem::CommandResultSender and economem::CommandResultReceiver

Senders use anonymous Unix sockets, so there can be as many of them as necessary in a process. Receivers bind to named Unix sockets in `/tmp`, and the format of the addresses means there should only ever be one receiver for a particular message type per process (but see below). Receivers can be used directly, or wrapped in an economem::ReceiverThread.

Addressing
----------

Clients and management programs are identified by PID. The economem::Reading and economem::Command classes include a `pid_t sender` field, so the daemon knows where to respond, and the corresponding receivers for these messages use the PID directly in the address of the socket they bind.

Because there is only intended to be one daemon running at a time, the receivers for economem::Recommendation and economem::CommandResult bind to well-known addresses that don't use the daemon's PID.

However, to support the multiple daemons needed for side-by-side comparison of throughput models in the measurement scripts, economem::ReadingSender, economem::ReadingReceiver, economem::CommandSender, and economem::CommandReceiver also support an optional `daemonName` parameter, which allows daemons to avoid trying to bind existing sockets.
