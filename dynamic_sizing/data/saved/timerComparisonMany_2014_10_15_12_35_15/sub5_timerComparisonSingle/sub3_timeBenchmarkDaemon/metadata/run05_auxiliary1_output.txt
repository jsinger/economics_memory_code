economemd
    total memory: 8922 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub5_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run05_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 5831: heap size 9 MB, throughput 0.98943
Clients: 1
Reading from 5832: heap size 9 MB, throughput 0.989253
Clients: 2
Client 5831 has a minimum heap size of 1211 MB
Client 5832 has a minimum heap size of 276 MB
Reading from 5832: heap size 9 MB, throughput 0.986668
Reading from 5831: heap size 9 MB, throughput 0.987186
Reading from 5832: heap size 9 MB, throughput 0.976255
Reading from 5831: heap size 9 MB, throughput 0.979212
Reading from 5832: heap size 9 MB, throughput 0.970435
Reading from 5831: heap size 9 MB, throughput 0.965888
Reading from 5832: heap size 11 MB, throughput 0.967342
Reading from 5831: heap size 11 MB, throughput 0.971372
Reading from 5832: heap size 11 MB, throughput 0.969039
Reading from 5831: heap size 11 MB, throughput 0.981961
Reading from 5832: heap size 17 MB, throughput 0.962865
Reading from 5831: heap size 17 MB, throughput 0.975341
Reading from 5832: heap size 17 MB, throughput 0.89088
Reading from 5831: heap size 17 MB, throughput 0.895305
Reading from 5832: heap size 30 MB, throughput 0.833642
Reading from 5831: heap size 30 MB, throughput 0.832542
Reading from 5832: heap size 31 MB, throughput 0.409231
Reading from 5831: heap size 31 MB, throughput 0.471957
Reading from 5832: heap size 34 MB, throughput 0.670687
Reading from 5831: heap size 34 MB, throughput 0.640477
Reading from 5832: heap size 48 MB, throughput 0.456957
Reading from 5831: heap size 47 MB, throughput 0.425156
Reading from 5832: heap size 49 MB, throughput 0.397655
Reading from 5831: heap size 48 MB, throughput 0.429434
Reading from 5832: heap size 51 MB, throughput 0.506765
Reading from 5831: heap size 51 MB, throughput 0.516603
Reading from 5832: heap size 75 MB, throughput 0.573581
Reading from 5831: heap size 74 MB, throughput 0.464729
Reading from 5832: heap size 75 MB, throughput 0.433112
Reading from 5831: heap size 74 MB, throughput 0.359866
Reading from 5832: heap size 103 MB, throughput 0.679396
Reading from 5831: heap size 99 MB, throughput 0.660978
Reading from 5832: heap size 104 MB, throughput 0.614741
Reading from 5831: heap size 99 MB, throughput 0.480368
Reading from 5832: heap size 105 MB, throughput 0.43797
Reading from 5831: heap size 102 MB, throughput 0.234849
Reading from 5832: heap size 107 MB, throughput 0.468518
Reading from 5831: heap size 105 MB, throughput 0.466271
Reading from 5832: heap size 142 MB, throughput 0.606432
Reading from 5831: heap size 131 MB, throughput 0.146365
Reading from 5832: heap size 143 MB, throughput 0.501192
Reading from 5831: heap size 134 MB, throughput 0.270918
Reading from 5832: heap size 145 MB, throughput 0.212871
Reading from 5831: heap size 136 MB, throughput 0.162461
Reading from 5832: heap size 149 MB, throughput 0.283077
Reading from 5832: heap size 152 MB, throughput 0.44911
Reading from 5831: heap size 138 MB, throughput 0.346688
Reading from 5832: heap size 189 MB, throughput 0.0187163
Reading from 5831: heap size 176 MB, throughput 0.164399
Reading from 5832: heap size 197 MB, throughput 0.0861716
Reading from 5831: heap size 178 MB, throughput 0.74242
Reading from 5832: heap size 199 MB, throughput 0.781535
Reading from 5831: heap size 180 MB, throughput 0.716506
Reading from 5831: heap size 185 MB, throughput 0.690136
Reading from 5831: heap size 192 MB, throughput 0.706037
Reading from 5831: heap size 197 MB, throughput 0.693505
Reading from 5831: heap size 200 MB, throughput 0.67815
Reading from 5831: heap size 208 MB, throughput 0.62757
Reading from 5831: heap size 212 MB, throughput 0.582638
Reading from 5831: heap size 222 MB, throughput 0.511124
Reading from 5832: heap size 202 MB, throughput 0.573456
Reading from 5832: heap size 248 MB, throughput 0.920885
Reading from 5831: heap size 226 MB, throughput 0.442922
Reading from 5832: heap size 259 MB, throughput 0.877975
Reading from 5831: heap size 269 MB, throughput 0.398056
Reading from 5832: heap size 261 MB, throughput 0.854284
Reading from 5832: heap size 265 MB, throughput 0.810144
Reading from 5831: heap size 275 MB, throughput 0.13769
Reading from 5832: heap size 265 MB, throughput 0.816235
Reading from 5831: heap size 318 MB, throughput 0.458024
Reading from 5831: heap size 312 MB, throughput 0.542678
Reading from 5832: heap size 268 MB, throughput 0.787766
Reading from 5831: heap size 257 MB, throughput 0.561972
Reading from 5831: heap size 306 MB, throughput 0.564312
Reading from 5832: heap size 269 MB, throughput 0.748295
Reading from 5832: heap size 275 MB, throughput 0.725851
Reading from 5831: heap size 310 MB, throughput 0.570279
Reading from 5832: heap size 278 MB, throughput 0.721041
Reading from 5831: heap size 310 MB, throughput 0.563118
Reading from 5832: heap size 285 MB, throughput 0.69412
Reading from 5831: heap size 313 MB, throughput 0.505943
Reading from 5831: heap size 318 MB, throughput 0.532859
Reading from 5832: heap size 287 MB, throughput 0.870753
Reading from 5832: heap size 291 MB, throughput 0.84952
Reading from 5831: heap size 322 MB, throughput 0.37574
Reading from 5832: heap size 295 MB, throughput 0.761097
Reading from 5831: heap size 370 MB, throughput 0.449088
Reading from 5832: heap size 302 MB, throughput 0.754646
Reading from 5831: heap size 374 MB, throughput 0.466029
Reading from 5831: heap size 375 MB, throughput 0.47714
Reading from 5832: heap size 302 MB, throughput 0.832145
Reading from 5831: heap size 376 MB, throughput 0.471191
Reading from 5832: heap size 306 MB, throughput 0.897429
Reading from 5831: heap size 379 MB, throughput 0.489114
Reading from 5832: heap size 307 MB, throughput 0.865762
Reading from 5831: heap size 383 MB, throughput 0.479507
Reading from 5832: heap size 312 MB, throughput 0.758836
Reading from 5832: heap size 312 MB, throughput 0.668229
Reading from 5832: heap size 320 MB, throughput 0.649906
Reading from 5832: heap size 320 MB, throughput 0.625628
Reading from 5831: heap size 389 MB, throughput 0.292425
Reading from 5831: heap size 441 MB, throughput 0.451483
Reading from 5831: heap size 435 MB, throughput 0.484087
Reading from 5831: heap size 440 MB, throughput 0.547119
Reading from 5831: heap size 437 MB, throughput 0.556856
Numeric result:
Recommendation: 2 clients, utility 0.59636:
    h1: 4480.57 MB (U(h) = 0.905618*h^0.001)
    h2: 4441.43 MB (U(h) = 0.647537*h^0.001)
Recommendation: 2 clients, utility 0.59636:
    h1: 4461 MB (U(h) = 0.905618*h^0.001)
    h2: 4461 MB (U(h) = 0.647537*h^0.001)
Reading from 5832: heap size 330 MB, throughput 0.91791
Reading from 5831: heap size 440 MB, throughput 0.256877
Reading from 5831: heap size 497 MB, throughput 0.503883
Reading from 5831: heap size 497 MB, throughput 0.544884
Reading from 5831: heap size 499 MB, throughput 0.538906
Reading from 5832: heap size 330 MB, throughput 0.963164
Reading from 5831: heap size 499 MB, throughput 0.533907
Reading from 5831: heap size 504 MB, throughput 0.531212
Reading from 5831: heap size 507 MB, throughput 0.322485
Reading from 5832: heap size 334 MB, throughput 0.964979
Reading from 5831: heap size 567 MB, throughput 0.462028
Reading from 5831: heap size 576 MB, throughput 0.468742
Reading from 5831: heap size 581 MB, throughput 0.517175
Reading from 5831: heap size 585 MB, throughput 0.533022
Reading from 5831: heap size 590 MB, throughput 0.541678
Reading from 5832: heap size 337 MB, throughput 0.972776
Reading from 5831: heap size 597 MB, throughput 0.266102
Reading from 5831: heap size 668 MB, throughput 0.600902
Reading from 5831: heap size 673 MB, throughput 0.787823
Reading from 5832: heap size 336 MB, throughput 0.850734
Reading from 5831: heap size 670 MB, throughput 0.821886
Reading from 5831: heap size 682 MB, throughput 0.813633
Reading from 5831: heap size 696 MB, throughput 0.744136
Reading from 5831: heap size 705 MB, throughput 0.703817
Reading from 5831: heap size 709 MB, throughput 0.589663
Reading from 5831: heap size 716 MB, throughput 0.483022
Reading from 5832: heap size 385 MB, throughput 0.964682
Numeric result:
Recommendation: 2 clients, utility 0.41741:
    h1: 1211 MB (U(h) = 0.627816*h^0.001)
    h2: 7711 MB (U(h) = 0.520243*h^0.0266113)
Recommendation: 2 clients, utility 0.41741:
    h1: 1211 MB (U(h) = 0.627816*h^0.001)
    h2: 7711 MB (U(h) = 0.520243*h^0.0266113)
Reading from 5832: heap size 389 MB, throughput 0.964776
Reading from 5831: heap size 726 MB, throughput 0.41005
Reading from 5831: heap size 722 MB, throughput 0.33007
Reading from 5831: heap size 799 MB, throughput 0.668556
Reading from 5831: heap size 803 MB, throughput 0.72418
Reading from 5832: heap size 390 MB, throughput 0.974767
Reading from 5831: heap size 799 MB, throughput 0.673331
Reading from 5831: heap size 803 MB, throughput 0.633503
Reading from 5831: heap size 796 MB, throughput 0.611006
Reading from 5832: heap size 389 MB, throughput 0.970971
Reading from 5831: heap size 800 MB, throughput 0.348315
Reading from 5831: heap size 889 MB, throughput 0.721543
Reading from 5831: heap size 890 MB, throughput 0.732182
Reading from 5831: heap size 894 MB, throughput 0.868966
Reading from 5831: heap size 899 MB, throughput 0.890615
Reading from 5832: heap size 391 MB, throughput 0.976673
Reading from 5831: heap size 904 MB, throughput 0.907981
Reading from 5831: heap size 769 MB, throughput 0.879151
Reading from 5831: heap size 885 MB, throughput 0.866319
Reading from 5831: heap size 780 MB, throughput 0.841007
Reading from 5831: heap size 874 MB, throughput 0.830436
Reading from 5831: heap size 785 MB, throughput 0.808516
Reading from 5831: heap size 869 MB, throughput 0.817723
Reading from 5832: heap size 392 MB, throughput 0.974839
Reading from 5831: heap size 875 MB, throughput 0.772181
Reading from 5831: heap size 867 MB, throughput 0.799307
Reading from 5831: heap size 871 MB, throughput 0.825162
Reading from 5831: heap size 863 MB, throughput 0.854692
Reading from 5831: heap size 868 MB, throughput 0.922446
Reading from 5832: heap size 393 MB, throughput 0.975051
Reading from 5831: heap size 867 MB, throughput 0.969482
Reading from 5832: heap size 396 MB, throughput 0.969852
Numeric result:
Recommendation: 2 clients, utility 0.509514:
    h1: 3622.56 MB (U(h) = 0.395328*h^0.0539207)
    h2: 5299.44 MB (U(h) = 0.4214*h^0.0788351)
Recommendation: 2 clients, utility 0.509514:
    h1: 3623.8 MB (U(h) = 0.395328*h^0.0539207)
    h2: 5298.2 MB (U(h) = 0.4214*h^0.0788351)
Reading from 5831: heap size 871 MB, throughput 0.722809
Reading from 5832: heap size 398 MB, throughput 0.972798
Reading from 5832: heap size 405 MB, throughput 0.93544
Reading from 5831: heap size 956 MB, throughput 0.790141
Reading from 5832: heap size 408 MB, throughput 0.910917
Reading from 5832: heap size 422 MB, throughput 0.848735
Reading from 5831: heap size 957 MB, throughput 0.768417
Reading from 5831: heap size 951 MB, throughput 0.734057
Reading from 5831: heap size 954 MB, throughput 0.706218
Reading from 5831: heap size 953 MB, throughput 0.736513
Reading from 5832: heap size 426 MB, throughput 0.909609
Reading from 5831: heap size 956 MB, throughput 0.757353
Reading from 5831: heap size 954 MB, throughput 0.74468
Reading from 5831: heap size 958 MB, throughput 0.806021
Reading from 5831: heap size 965 MB, throughput 0.817939
Reading from 5831: heap size 966 MB, throughput 0.824207
Reading from 5831: heap size 978 MB, throughput 0.757914
Reading from 5832: heap size 433 MB, throughput 0.96994
Reading from 5831: heap size 979 MB, throughput 0.719291
Reading from 5831: heap size 999 MB, throughput 0.492239
Reading from 5832: heap size 437 MB, throughput 0.979596
Reading from 5831: heap size 1124 MB, throughput 0.601614
Reading from 5831: heap size 1135 MB, throughput 0.659991
Reading from 5831: heap size 1136 MB, throughput 0.689501
Reading from 5831: heap size 1148 MB, throughput 0.700537
Reading from 5831: heap size 1150 MB, throughput 0.694667
Reading from 5831: heap size 1157 MB, throughput 0.705679
Reading from 5831: heap size 1160 MB, throughput 0.73638
Reading from 5832: heap size 436 MB, throughput 0.977163
Reading from 5831: heap size 1165 MB, throughput 0.889728
Numeric result:
Recommendation: 2 clients, utility 1.07077:
    h1: 6322.59 MB (U(h) = 0.0945112*h^0.28828)
    h2: 2599.41 MB (U(h) = 0.357974*h^0.118493)
Recommendation: 2 clients, utility 1.07077:
    h1: 6323.02 MB (U(h) = 0.0945112*h^0.28828)
    h2: 2598.98 MB (U(h) = 0.357974*h^0.118493)
Reading from 5832: heap size 440 MB, throughput 0.976817
Reading from 5831: heap size 1170 MB, throughput 0.926245
Reading from 5831: heap size 1169 MB, throughput 0.931163
Reading from 5832: heap size 438 MB, throughput 0.981642
Reading from 5831: heap size 1176 MB, throughput 0.937729
Reading from 5832: heap size 442 MB, throughput 0.982859
Reading from 5831: heap size 1188 MB, throughput 0.945163
Reading from 5832: heap size 440 MB, throughput 0.984041
Reading from 5831: heap size 1190 MB, throughput 0.948696
Reading from 5832: heap size 442 MB, throughput 0.982701
Numeric result:
Recommendation: 2 clients, utility 1.40737:
    h1: 6630.63 MB (U(h) = 0.0530929*h^0.37984)
    h2: 2291.37 MB (U(h) = 0.339401*h^0.131269)
Recommendation: 2 clients, utility 1.40737:
    h1: 6630.54 MB (U(h) = 0.0530929*h^0.37984)
    h2: 2291.46 MB (U(h) = 0.339401*h^0.131269)
Reading from 5831: heap size 1196 MB, throughput 0.240094
Reading from 5832: heap size 443 MB, throughput 0.978875
Reading from 5832: heap size 444 MB, throughput 0.97961
Reading from 5831: heap size 1201 MB, throughput 0.577301
Reading from 5832: heap size 450 MB, throughput 0.964258
Reading from 5832: heap size 496 MB, throughput 0.967994
Reading from 5832: heap size 495 MB, throughput 0.983185
Reading from 5831: heap size 1207 MB, throughput 0.600724
Reading from 5832: heap size 500 MB, throughput 0.992098
Reading from 5831: heap size 1208 MB, throughput 0.543105
Reading from 5832: heap size 508 MB, throughput 0.992297
Reading from 5831: heap size 1215 MB, throughput 0.121857
Numeric result:
Recommendation: 2 clients, utility 1.07773:
    h1: 5401.4 MB (U(h) = 0.129067*h^0.237313)
    h2: 3520.6 MB (U(h) = 0.307193*h^0.154658)
Recommendation: 2 clients, utility 1.07773:
    h1: 5401.7 MB (U(h) = 0.129067*h^0.237313)
    h2: 3520.3 MB (U(h) = 0.307193*h^0.154658)
Reading from 5832: heap size 509 MB, throughput 0.992241
Reading from 5831: heap size 1218 MB, throughput 0.958164
Reading from 5832: heap size 507 MB, throughput 0.991363
Reading from 5831: heap size 1225 MB, throughput 0.96208
Reading from 5832: heap size 509 MB, throughput 0.989797
Reading from 5831: heap size 1230 MB, throughput 0.954744
Reading from 5831: heap size 1236 MB, throughput 0.948862
Reading from 5832: heap size 501 MB, throughput 0.986793
Numeric result:
Recommendation: 2 clients, utility 1.23772:
    h1: 5669.27 MB (U(h) = 0.0961491*h^0.283911)
    h2: 3252.73 MB (U(h) = 0.296411*h^0.162891)
Recommendation: 2 clients, utility 1.23772:
    h1: 5669.3 MB (U(h) = 0.0961491*h^0.283911)
    h2: 3252.7 MB (U(h) = 0.296411*h^0.162891)
Reading from 5831: heap size 1242 MB, throughput 0.950853
Reading from 5832: heap size 505 MB, throughput 0.986717
Reading from 5831: heap size 1249 MB, throughput 0.950396
Reading from 5832: heap size 506 MB, throughput 0.989175
Reading from 5832: heap size 475 MB, throughput 0.983168
Reading from 5832: heap size 500 MB, throughput 0.971983
Reading from 5832: heap size 506 MB, throughput 0.953977
Reading from 5831: heap size 1255 MB, throughput 0.949968
Reading from 5832: heap size 515 MB, throughput 0.978686
Reading from 5831: heap size 1263 MB, throughput 0.948853
Reading from 5832: heap size 518 MB, throughput 0.985157
Reading from 5831: heap size 1268 MB, throughput 0.953998
Numeric result:
Recommendation: 2 clients, utility 1.74494:
    h1: 6360.85 MB (U(h) = 0.03627*h^0.430895)
    h2: 2561.15 MB (U(h) = 0.283076*h^0.173514)
Recommendation: 2 clients, utility 1.74494:
    h1: 6360.67 MB (U(h) = 0.03627*h^0.430895)
    h2: 2561.33 MB (U(h) = 0.283076*h^0.173514)
Reading from 5832: heap size 516 MB, throughput 0.985653
Reading from 5831: heap size 1277 MB, throughput 0.950175
Reading from 5832: heap size 520 MB, throughput 0.986914
Reading from 5832: heap size 520 MB, throughput 0.983958
Reading from 5831: heap size 1279 MB, throughput 0.907524
Reading from 5832: heap size 522 MB, throughput 0.984807
Reading from 5831: heap size 1354 MB, throughput 0.968939
Numeric result:
Recommendation: 2 clients, utility 1.94652:
    h1: 6471.78 MB (U(h) = 0.0274366*h^0.473017)
    h2: 2450.22 MB (U(h) = 0.276278*h^0.179069)
Recommendation: 2 clients, utility 1.94652:
    h1: 6471.93 MB (U(h) = 0.0274366*h^0.473017)
    h2: 2450.07 MB (U(h) = 0.276278*h^0.179069)
Reading from 5832: heap size 526 MB, throughput 0.984688
Reading from 5831: heap size 1355 MB, throughput 0.977563
Reading from 5831: heap size 1367 MB, throughput 0.981542
Reading from 5832: heap size 526 MB, throughput 0.987635
Reading from 5832: heap size 528 MB, throughput 0.983086
Reading from 5832: heap size 529 MB, throughput 0.969733
Reading from 5832: heap size 535 MB, throughput 0.968184
Reading from 5831: heap size 1372 MB, throughput 0.980912
Reading from 5832: heap size 536 MB, throughput 0.987769
Reading from 5831: heap size 1372 MB, throughput 0.981989
Numeric result:
Recommendation: 2 clients, utility 1.97889:
    h1: 6365.53 MB (U(h) = 0.0292913*h^0.464521)
    h2: 2556.47 MB (U(h) = 0.267324*h^0.186547)
Recommendation: 2 clients, utility 1.97889:
    h1: 6365.63 MB (U(h) = 0.0292913*h^0.464521)
    h2: 2556.37 MB (U(h) = 0.267324*h^0.186547)
Reading from 5832: heap size 540 MB, throughput 0.990082
Reading from 5831: heap size 1375 MB, throughput 0.979843
Reading from 5832: heap size 542 MB, throughput 0.989577
Reading from 5831: heap size 1362 MB, throughput 0.979988
Reading from 5832: heap size 542 MB, throughput 0.989809
Reading from 5831: heap size 1272 MB, throughput 0.976379
Reading from 5832: heap size 545 MB, throughput 0.982632
Reading from 5831: heap size 1351 MB, throughput 0.969648
Numeric result:
Recommendation: 2 clients, utility 2.36033:
    h1: 6612.41 MB (U(h) = 0.0169088*h^0.545636)
    h2: 2309.59 MB (U(h) = 0.262547*h^0.190611)
Recommendation: 2 clients, utility 2.36033:
    h1: 6612.13 MB (U(h) = 0.0169088*h^0.545636)
    h2: 2309.87 MB (U(h) = 0.262547*h^0.190611)
Reading from 5832: heap size 545 MB, throughput 0.984732
Reading from 5831: heap size 1358 MB, throughput 0.968524
Reading from 5831: heap size 1361 MB, throughput 0.968081
Reading from 5832: heap size 546 MB, throughput 0.989592
Reading from 5832: heap size 548 MB, throughput 0.984695
Reading from 5832: heap size 549 MB, throughput 0.973066
Reading from 5831: heap size 1361 MB, throughput 0.96653
Reading from 5832: heap size 556 MB, throughput 0.985738
Reading from 5831: heap size 1365 MB, throughput 0.96467
Reading from 5832: heap size 557 MB, throughput 0.99018
Numeric result:
Recommendation: 2 clients, utility 2.48809:
    h1: 6610.54 MB (U(h) = 0.0152372*h^0.561343)
    h2: 2311.46 MB (U(h) = 0.255985*h^0.196286)
Recommendation: 2 clients, utility 2.48809:
    h1: 6610.5 MB (U(h) = 0.0152372*h^0.561343)
    h2: 2311.5 MB (U(h) = 0.255985*h^0.196286)
Reading from 5831: heap size 1370 MB, throughput 0.959073
Reading from 5832: heap size 561 MB, throughput 0.991047
Reading from 5831: heap size 1375 MB, throughput 0.956823
Reading from 5832: heap size 563 MB, throughput 0.990464
Reading from 5831: heap size 1384 MB, throughput 0.955816
Reading from 5832: heap size 562 MB, throughput 0.989745
Numeric result:
Recommendation: 2 clients, utility 2.61927:
    h1: 6647.11 MB (U(h) = 0.0131914*h^0.582433)
    h2: 2274.89 MB (U(h) = 0.252476*h^0.19936)
Recommendation: 2 clients, utility 2.61927:
    h1: 6646.86 MB (U(h) = 0.0131914*h^0.582433)
    h2: 2275.14 MB (U(h) = 0.252476*h^0.19936)
Reading from 5831: heap size 1394 MB, throughput 0.955938
Reading from 5832: heap size 565 MB, throughput 0.989783
Reading from 5831: heap size 1401 MB, throughput 0.956387
Reading from 5832: heap size 568 MB, throughput 0.988997
Reading from 5832: heap size 568 MB, throughput 0.983193
Reading from 5832: heap size 570 MB, throughput 0.981776
Reading from 5832: heap size 572 MB, throughput 0.990307
Numeric result:
Recommendation: 2 clients, utility 2.79285:
    h1: 6695.57 MB (U(h) = 0.0109073*h^0.61004)
    h2: 2226.43 MB (U(h) = 0.248536*h^0.202849)
Recommendation: 2 clients, utility 2.79285:
    h1: 6695.59 MB (U(h) = 0.0109073*h^0.61004)
    h2: 2226.41 MB (U(h) = 0.248536*h^0.202849)
Reading from 5832: heap size 575 MB, throughput 0.990863
Reading from 5832: heap size 577 MB, throughput 0.991297
Reading from 5832: heap size 576 MB, throughput 0.990769
Numeric result:
Recommendation: 2 clients, utility 3.98431:
    h1: 5524.19 MB (U(h) = 0.0109073*h^0.61004)
    h2: 3397.81 MB (U(h) = 0.0900857*h^0.375241)
Recommendation: 2 clients, utility 3.98431:
    h1: 5524.09 MB (U(h) = 0.0109073*h^0.61004)
    h2: 3397.91 MB (U(h) = 0.0900857*h^0.375241)
Reading from 5832: heap size 578 MB, throughput 0.986953
Reading from 5831: heap size 1412 MB, throughput 0.936342
Reading from 5832: heap size 581 MB, throughput 0.990715
Reading from 5832: heap size 582 MB, throughput 0.985744
Reading from 5832: heap size 582 MB, throughput 0.981909
Reading from 5832: heap size 584 MB, throughput 0.99041
Numeric result:
Recommendation: 2 clients, utility 5.46968:
    h1: 4649.06 MB (U(h) = 0.013284*h^0.581789)
    h2: 4272.94 MB (U(h) = 0.0346246*h^0.534773)
Recommendation: 2 clients, utility 5.46968:
    h1: 4648.85 MB (U(h) = 0.013284*h^0.581789)
    h2: 4273.15 MB (U(h) = 0.0346246*h^0.534773)
Reading from 5832: heap size 588 MB, throughput 0.990886
Reading from 5832: heap size 590 MB, throughput 0.991938
Reading from 5832: heap size 589 MB, throughput 0.986366
Reading from 5831: heap size 1564 MB, throughput 0.983181
Numeric result:
Recommendation: 2 clients, utility 7.20975:
    h1: 4206.07 MB (U(h) = 0.0132912*h^0.581771)
    h2: 4715.93 MB (U(h) = 0.0169799*h^0.652269)
Recommendation: 2 clients, utility 7.20975:
    h1: 4206.15 MB (U(h) = 0.0132912*h^0.581771)
    h2: 4715.85 MB (U(h) = 0.0169799*h^0.652269)
Reading from 5832: heap size 591 MB, throughput 0.987701
Reading from 5831: heap size 1616 MB, throughput 0.981476
Reading from 5831: heap size 1505 MB, throughput 0.969715
Reading from 5831: heap size 1602 MB, throughput 0.950805
Reading from 5831: heap size 1622 MB, throughput 0.918265
Reading from 5832: heap size 595 MB, throughput 0.986931
Reading from 5831: heap size 1651 MB, throughput 0.866535
Reading from 5832: heap size 596 MB, throughput 0.980756
Reading from 5831: heap size 1661 MB, throughput 0.816713
Reading from 5831: heap size 1697 MB, throughput 0.806374
Reading from 5832: heap size 601 MB, throughput 0.980176
Reading from 5831: heap size 1701 MB, throughput 0.945517
Reading from 5832: heap size 603 MB, throughput 0.988542
Numeric result:
Recommendation: 2 clients, utility 10.4565:
    h1: 3430.82 MB (U(h) = 0.0195917*h^0.52536)
    h2: 5491.18 MB (U(h) = 0.00531379*h^0.840865)
Recommendation: 2 clients, utility 10.4565:
    h1: 3430.81 MB (U(h) = 0.0195917*h^0.52536)
    h2: 5491.19 MB (U(h) = 0.00531379*h^0.840865)
Reading from 5831: heap size 1708 MB, throughput 0.961939
Reading from 5832: heap size 606 MB, throughput 0.990732
Reading from 5831: heap size 1720 MB, throughput 0.967426
Reading from 5832: heap size 608 MB, throughput 0.988773
Reading from 5831: heap size 1719 MB, throughput 0.969357
Reading from 5832: heap size 609 MB, throughput 0.989656
Numeric result:
Recommendation: 2 clients, utility 8.89209:
    h1: 3458.27 MB (U(h) = 0.0235785*h^0.499186)
    h2: 5463.73 MB (U(h) = 0.0072831*h^0.788664)
Recommendation: 2 clients, utility 8.89209:
    h1: 3458.27 MB (U(h) = 0.0235785*h^0.499186)
    h2: 5463.73 MB (U(h) = 0.0072831*h^0.788664)
Reading from 5831: heap size 1731 MB, throughput 0.971612
Reading from 5832: heap size 611 MB, throughput 0.992457
Reading from 5832: heap size 613 MB, throughput 0.982585
Reading from 5831: heap size 1722 MB, throughput 0.971489
Reading from 5832: heap size 614 MB, throughput 0.983783
Reading from 5831: heap size 1735 MB, throughput 0.970563
Reading from 5832: heap size 625 MB, throughput 0.991185
Numeric result:
Recommendation: 2 clients, utility 2.64328:
    h1: 5278.19 MB (U(h) = 0.0399944*h^0.425053)
    h2: 3643.81 MB (U(h) = 0.155892*h^0.29343)
Recommendation: 2 clients, utility 2.64328:
    h1: 5278.24 MB (U(h) = 0.0399944*h^0.425053)
    h2: 3643.76 MB (U(h) = 0.155892*h^0.29343)
Reading from 5831: heap size 1726 MB, throughput 0.96986
Reading from 5832: heap size 626 MB, throughput 0.991833
Reading from 5831: heap size 1736 MB, throughput 0.971713
Reading from 5832: heap size 627 MB, throughput 0.992084
Reading from 5831: heap size 1740 MB, throughput 0.971501
Numeric result:
Recommendation: 2 clients, utility 2.76834:
    h1: 5701.82 MB (U(h) = 0.0261916*h^0.484206)
    h2: 3220.18 MB (U(h) = 0.176235*h^0.273464)
Recommendation: 2 clients, utility 2.76834:
    h1: 5701.81 MB (U(h) = 0.0261916*h^0.484206)
    h2: 3220.19 MB (U(h) = 0.176235*h^0.273464)
Reading from 5832: heap size 630 MB, throughput 0.991299
Reading from 5831: heap size 1746 MB, throughput 0.971564
Reading from 5832: heap size 633 MB, throughput 0.992535
Reading from 5832: heap size 634 MB, throughput 0.985632
Reading from 5832: heap size 636 MB, throughput 0.987548
Reading from 5831: heap size 1736 MB, throughput 0.970487
Reading from 5832: heap size 639 MB, throughput 0.991443
Numeric result:
Recommendation: 2 clients, utility 2.78795:
    h1: 5799.27 MB (U(h) = 0.0237842*h^0.497618)
    h2: 3122.73 MB (U(h) = 0.181924*h^0.267956)
Recommendation: 2 clients, utility 2.78795:
    h1: 5799.24 MB (U(h) = 0.0237842*h^0.497618)
    h2: 3122.76 MB (U(h) = 0.181924*h^0.267956)
Reading from 5831: heap size 1745 MB, throughput 0.969308
Reading from 5832: heap size 641 MB, throughput 0.992285
Reading from 5831: heap size 1753 MB, throughput 0.9698
Reading from 5832: heap size 644 MB, throughput 0.992231
Reading from 5831: heap size 1754 MB, throughput 0.96551
Numeric result:
Recommendation: 2 clients, utility 2.56919:
    h1: 5985.2 MB (U(h) = 0.0268255*h^0.480858)
    h2: 2936.8 MB (U(h) = 0.22224*h^0.235933)
Recommendation: 2 clients, utility 2.56919:
    h1: 5985.31 MB (U(h) = 0.0268255*h^0.480858)
    h2: 2936.69 MB (U(h) = 0.22224*h^0.235933)
Reading from 5832: heap size 647 MB, throughput 0.991724
Reading from 5832: heap size 647 MB, throughput 0.989113
Reading from 5831: heap size 1764 MB, throughput 0.963716
Reading from 5832: heap size 653 MB, throughput 0.986431
Reading from 5831: heap size 1772 MB, throughput 0.961931
Reading from 5832: heap size 655 MB, throughput 0.99256
Numeric result:
Recommendation: 2 clients, utility 2.1368:
    h1: 5796.51 MB (U(h) = 0.0525131*h^0.387753)
    h2: 3125.49 MB (U(h) = 0.26279*h^0.209077)
Recommendation: 2 clients, utility 2.1368:
    h1: 5796.51 MB (U(h) = 0.0525131*h^0.387753)
    h2: 3125.49 MB (U(h) = 0.26279*h^0.209077)
Reading from 5832: heap size 658 MB, throughput 0.988674
Reading from 5831: heap size 1787 MB, throughput 0.951299
Reading from 5832: heap size 659 MB, throughput 0.99122
Reading from 5831: heap size 1754 MB, throughput 0.976805
Numeric result:
Recommendation: 2 clients, utility 2.01179:
    h1: 6115.61 MB (U(h) = 0.0537075*h^0.38465)
    h2: 2806.39 MB (U(h) = 0.322447*h^0.176511)
Recommendation: 2 clients, utility 2.01179:
    h1: 6115.62 MB (U(h) = 0.0537075*h^0.38465)
    h2: 2806.38 MB (U(h) = 0.322447*h^0.176511)
Reading from 5832: heap size 660 MB, throughput 0.991051
Reading from 5831: heap size 1762 MB, throughput 0.982403
Reading from 5832: heap size 661 MB, throughput 0.990905
Reading from 5832: heap size 663 MB, throughput 0.986264
Reading from 5831: heap size 1777 MB, throughput 0.984329
Reading from 5832: heap size 666 MB, throughput 0.991483
Numeric result:
Recommendation: 2 clients, utility 1.73876:
    h1: 7347.7 MB (U(h) = 0.0574325*h^0.375567)
    h2: 1574.3 MB (U(h) = 0.591237*h^0.0804855)
Recommendation: 2 clients, utility 1.73876:
    h1: 7347.42 MB (U(h) = 0.0574325*h^0.375567)
    h2: 1574.58 MB (U(h) = 0.591237*h^0.0804855)
Reading from 5831: heap size 1797 MB, throughput 0.984454
Reading from 5832: heap size 671 MB, throughput 0.992904
Reading from 5831: heap size 1797 MB, throughput 0.983946
Reading from 5832: heap size 673 MB, throughput 0.993125
Numeric result:
Recommendation: 2 clients, utility 1.60467:
    h1: 7107.31 MB (U(h) = 0.0834001*h^0.324294)
    h2: 1814.69 MB (U(h) = 0.582606*h^0.0827788)
Recommendation: 2 clients, utility 1.60467:
    h1: 7107.7 MB (U(h) = 0.0834001*h^0.324294)
    h2: 1814.3 MB (U(h) = 0.582606*h^0.0827788)
Reading from 5831: heap size 1788 MB, throughput 0.981745
Reading from 5832: heap size 674 MB, throughput 0.992377
Reading from 5832: heap size 676 MB, throughput 0.991756
Reading from 5832: heap size 678 MB, throughput 0.98614
Client 5832 died
Clients: 1
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 5831: heap size 1797 MB, throughput 0.992897
Recommendation: one client; give it all the memory
Reading from 5831: heap size 1794 MB, throughput 0.993316
Reading from 5831: heap size 1798 MB, throughput 0.986077
Reading from 5831: heap size 1789 MB, throughput 0.975528
Reading from 5831: heap size 1820 MB, throughput 0.955349
Client 5831 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
