{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, DeriveDataTypeable #-}
{-# OPTIONS_GHC -fno-warn-missing-fields -fno-warn-missing-methods #-}

import Economem.ArgHelper
import Economem.BenchmarkTimer
import Economem.Experiment
import Economem.Files
import Economem.Java
import Economem.Records
import Economem.Signals
import Economem.UnconstrainedBenchmarkTimer
import System.Console.CmdLib

data Args = Args { inputFile :: FilePath,
                   runs :: Int,
                   java :: String }
          deriving (Typeable, Data, Eq, Show)

instance Attributes Args where
  attributes _ = group "Options" [
    inputFile %> [ Positional 0,
                   Required True,
                   Help "Specifies which benchmarks to run",
                   ArgHelp "INPUT_FILE" ],
    runs %> [ Short ['r'],
              Long ["runs"],
              Default (fromIntegral defaultRuns::Int),
              Help "How many times to repeat, for taking the average",
              ArgHelp "RUNS" ],
    java %> [ Short ['j'],
              Long ["java"],
              Default (javaName defaultJava),
              Help ("Java version to use; available options: " ++ show validJavaNames),
              ArgHelp "JAVA" ]]

instance RecordCommand Args where
  mode_summary _ = "Run multiple benchmark VMs with no daemon and no constraints, and see how long it takes for them all to finish."

main :: IO ()
main = getArgs >>= executeR Args {} >>= \opts -> do
  (i, r, j) <- validate opts
  b <- newUnconstrainedBenchmarkTimer i r j Nothing False Nothing []
  h <- makeSignalHandler
  runExperiment b h

  where
    validate :: Args -> IO (Input BenchmarkThreadsIters, Integer, Java)
    validate a = do
      i <- existingAbsoluteFile (inputFile a)
      r <- positive (runs a)
      j <- javaFromName (java a)
      return (fromFile i, fromIntegral r, j)
