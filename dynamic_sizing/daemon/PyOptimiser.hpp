#ifndef PYOPTIMISER_HPP
#define PYOPTIMISER_HPP

#include <functional>
#include <vector>

class PartitionResult;

namespace PyOptimiser
{
    void init();
    void deinit();

    PartitionResult* partitionMemoryNumeric(unsigned long long totalMem, const std::vector<unsigned long long>& minHeaps, std::function<double(const std::vector<double>&)> combineUtilities);
};

#endif
