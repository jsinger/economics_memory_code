economemd
    total memory: 8922 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub6_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run09_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
TimerThread: starting
ReadingThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 12455: heap size 9 MB, throughput 0.992297
Clients: 1
Client 12455 has a minimum heap size of 1211 MB
Reading from 12456: heap size 9 MB, throughput 0.985065
Clients: 2
Client 12456 has a minimum heap size of 276 MB
Reading from 12455: heap size 9 MB, throughput 0.98357
Reading from 12456: heap size 9 MB, throughput 0.965669
Reading from 12455: heap size 9 MB, throughput 0.969968
Reading from 12455: heap size 9 MB, throughput 0.923201
Reading from 12456: heap size 11 MB, throughput 0.976866
Reading from 12455: heap size 11 MB, throughput 0.986406
Reading from 12456: heap size 11 MB, throughput 0.984037
Reading from 12455: heap size 11 MB, throughput 0.965709
Reading from 12456: heap size 15 MB, throughput 0.902191
Reading from 12455: heap size 17 MB, throughput 0.891624
Reading from 12456: heap size 18 MB, throughput 0.970404
Reading from 12455: heap size 17 MB, throughput 0.715487
Reading from 12456: heap size 24 MB, throughput 0.954707
Reading from 12456: heap size 28 MB, throughput 0.871247
Reading from 12455: heap size 30 MB, throughput 0.971866
Reading from 12455: heap size 31 MB, throughput 0.94365
Reading from 12456: heap size 31 MB, throughput 0.636082
Reading from 12456: heap size 41 MB, throughput 0.89261
Reading from 12456: heap size 47 MB, throughput 0.816982
Reading from 12455: heap size 35 MB, throughput 0.357112
Reading from 12455: heap size 48 MB, throughput 0.868356
Reading from 12455: heap size 48 MB, throughput 0.856649
Reading from 12456: heap size 48 MB, throughput 0.279687
Reading from 12456: heap size 67 MB, throughput 0.792335
Reading from 12455: heap size 51 MB, throughput 0.444956
Reading from 12455: heap size 73 MB, throughput 0.580326
Reading from 12456: heap size 70 MB, throughput 0.295345
Reading from 12455: heap size 73 MB, throughput 0.318925
Reading from 12456: heap size 96 MB, throughput 0.821645
Reading from 12456: heap size 97 MB, throughput 0.713185
Reading from 12455: heap size 100 MB, throughput 0.932049
Reading from 12455: heap size 100 MB, throughput 0.788941
Reading from 12456: heap size 101 MB, throughput 0.743603
Reading from 12456: heap size 105 MB, throughput 0.736487
Reading from 12455: heap size 103 MB, throughput 0.815302
Reading from 12455: heap size 105 MB, throughput 0.195411
Reading from 12456: heap size 109 MB, throughput 0.187679
Reading from 12455: heap size 142 MB, throughput 0.72838
Reading from 12456: heap size 143 MB, throughput 0.629704
Reading from 12456: heap size 147 MB, throughput 0.703082
Reading from 12455: heap size 142 MB, throughput 0.806136
Reading from 12456: heap size 150 MB, throughput 0.753995
Reading from 12455: heap size 145 MB, throughput 0.788568
Reading from 12456: heap size 154 MB, throughput 0.525016
Reading from 12455: heap size 150 MB, throughput 0.718919
Reading from 12456: heap size 161 MB, throughput 0.140764
Reading from 12456: heap size 200 MB, throughput 0.669706
Reading from 12455: heap size 154 MB, throughput 0.137999
Reading from 12456: heap size 204 MB, throughput 0.594988
Reading from 12455: heap size 190 MB, throughput 0.574975
Reading from 12455: heap size 201 MB, throughput 0.67565
Reading from 12455: heap size 202 MB, throughput 0.729765
Reading from 12456: heap size 206 MB, throughput 0.217793
Reading from 12455: heap size 205 MB, throughput 0.169387
Reading from 12456: heap size 245 MB, throughput 0.856739
Reading from 12455: heap size 251 MB, throughput 0.597037
Reading from 12455: heap size 259 MB, throughput 0.720589
Reading from 12455: heap size 263 MB, throughput 0.704091
Reading from 12456: heap size 252 MB, throughput 0.93449
Reading from 12456: heap size 253 MB, throughput 0.665171
Reading from 12455: heap size 268 MB, throughput 0.750194
Reading from 12456: heap size 259 MB, throughput 0.841065
Reading from 12455: heap size 277 MB, throughput 0.717557
Reading from 12455: heap size 281 MB, throughput 0.505913
Reading from 12456: heap size 261 MB, throughput 0.764106
Reading from 12455: heap size 294 MB, throughput 0.618335
Reading from 12456: heap size 268 MB, throughput 0.879286
Reading from 12455: heap size 300 MB, throughput 0.600874
Reading from 12456: heap size 270 MB, throughput 0.699231
Reading from 12455: heap size 312 MB, throughput 0.451627
Reading from 12456: heap size 271 MB, throughput 0.618097
Reading from 12456: heap size 276 MB, throughput 0.585632
Reading from 12456: heap size 283 MB, throughput 0.578066
Reading from 12456: heap size 286 MB, throughput 0.507152
Reading from 12455: heap size 319 MB, throughput 0.0839217
Reading from 12455: heap size 373 MB, throughput 0.470649
Reading from 12456: heap size 296 MB, throughput 0.916082
Reading from 12456: heap size 296 MB, throughput 0.595854
Reading from 12455: heap size 366 MB, throughput 0.0943664
Reading from 12456: heap size 304 MB, throughput 0.713023
Reading from 12456: heap size 304 MB, throughput 0.654715
Reading from 12455: heap size 350 MB, throughput 0.47118
Reading from 12455: heap size 423 MB, throughput 0.742288
Reading from 12455: heap size 348 MB, throughput 0.534106
Reading from 12455: heap size 413 MB, throughput 0.594766
Reading from 12455: heap size 419 MB, throughput 0.545307
Reading from 12455: heap size 421 MB, throughput 0.617475
Reading from 12455: heap size 421 MB, throughput 0.528302
Reading from 12456: heap size 304 MB, throughput 0.150697
Reading from 12455: heap size 425 MB, throughput 0.499442
Reading from 12455: heap size 430 MB, throughput 0.478785
Reading from 12456: heap size 350 MB, throughput 0.719661
Reading from 12456: heap size 358 MB, throughput 0.661447
Reading from 12455: heap size 436 MB, throughput 0.43852
Reading from 12456: heap size 359 MB, throughput 0.627459
Reading from 12456: heap size 360 MB, throughput 0.581802
Reading from 12456: heap size 362 MB, throughput 0.753031
Reading from 12456: heap size 366 MB, throughput 0.687664
Reading from 12455: heap size 440 MB, throughput 0.0960886
Equal recommendation: 4461 MB each
Reading from 12455: heap size 505 MB, throughput 0.446452
Reading from 12455: heap size 509 MB, throughput 0.541557
Reading from 12455: heap size 509 MB, throughput 0.539231
Reading from 12455: heap size 511 MB, throughput 0.547454
Reading from 12456: heap size 369 MB, throughput 0.973228
Reading from 12455: heap size 517 MB, throughput 0.105057
Reading from 12455: heap size 580 MB, throughput 0.495373
Reading from 12455: heap size 576 MB, throughput 0.591287
Reading from 12455: heap size 580 MB, throughput 0.557921
Reading from 12455: heap size 582 MB, throughput 0.538586
Reading from 12455: heap size 591 MB, throughput 0.510083
Reading from 12455: heap size 599 MB, throughput 0.525198
Reading from 12455: heap size 610 MB, throughput 0.538242
Reading from 12456: heap size 378 MB, throughput 0.981093
Reading from 12455: heap size 615 MB, throughput 0.548978
Reading from 12456: heap size 380 MB, throughput 0.964733
Reading from 12455: heap size 625 MB, throughput 0.139786
Reading from 12455: heap size 696 MB, throughput 0.794841
Reading from 12455: heap size 703 MB, throughput 0.829848
Reading from 12456: heap size 381 MB, throughput 0.973353
Reading from 12455: heap size 701 MB, throughput 0.885215
Reading from 12455: heap size 711 MB, throughput 0.558932
Reading from 12455: heap size 719 MB, throughput 0.396555
Reading from 12455: heap size 730 MB, throughput 0.55395
Reading from 12455: heap size 736 MB, throughput 0.280741
Reading from 12456: heap size 385 MB, throughput 0.973636
Equal recommendation: 4461 MB each
Reading from 12455: heap size 739 MB, throughput 0.0138704
Reading from 12455: heap size 802 MB, throughput 0.185921
Reading from 12456: heap size 384 MB, throughput 0.979677
Reading from 12455: heap size 811 MB, throughput 0.418437
Reading from 12455: heap size 808 MB, throughput 0.421751
Reading from 12456: heap size 387 MB, throughput 0.960099
Reading from 12455: heap size 810 MB, throughput 0.161092
Reading from 12455: heap size 900 MB, throughput 0.613437
Reading from 12455: heap size 901 MB, throughput 0.649465
Reading from 12455: heap size 907 MB, throughput 0.702889
Reading from 12455: heap size 907 MB, throughput 0.707302
Reading from 12455: heap size 903 MB, throughput 0.844576
Reading from 12455: heap size 906 MB, throughput 0.763261
Reading from 12455: heap size 907 MB, throughput 0.919755
Reading from 12455: heap size 908 MB, throughput 0.74409
Reading from 12456: heap size 387 MB, throughput 0.978474
Reading from 12455: heap size 901 MB, throughput 0.928969
Reading from 12455: heap size 724 MB, throughput 0.84025
Reading from 12455: heap size 890 MB, throughput 0.908546
Reading from 12455: heap size 771 MB, throughput 0.72401
Reading from 12455: heap size 873 MB, throughput 0.725594
Reading from 12455: heap size 776 MB, throughput 0.728577
Reading from 12456: heap size 389 MB, throughput 0.95597
Reading from 12455: heap size 863 MB, throughput 0.082959
Reading from 12455: heap size 861 MB, throughput 0.46329
Reading from 12455: heap size 952 MB, throughput 0.734282
Reading from 12455: heap size 954 MB, throughput 0.668805
Reading from 12455: heap size 952 MB, throughput 0.763605
Reading from 12455: heap size 955 MB, throughput 0.677456
Reading from 12455: heap size 950 MB, throughput 0.706384
Reading from 12456: heap size 394 MB, throughput 0.974134
Reading from 12455: heap size 954 MB, throughput 0.868998
Reading from 12456: heap size 394 MB, throughput 0.973714
Reading from 12455: heap size 946 MB, throughput 0.964268
Reading from 12455: heap size 952 MB, throughput 0.754627
Reading from 12455: heap size 959 MB, throughput 0.709605
Reading from 12455: heap size 970 MB, throughput 0.767188
Reading from 12455: heap size 975 MB, throughput 0.745155
Equal recommendation: 4461 MB each
Reading from 12455: heap size 981 MB, throughput 0.775591
Reading from 12455: heap size 974 MB, throughput 0.771157
Reading from 12455: heap size 979 MB, throughput 0.798157
Reading from 12455: heap size 974 MB, throughput 0.816974
Reading from 12455: heap size 979 MB, throughput 0.836031
Reading from 12456: heap size 397 MB, throughput 0.979686
Reading from 12455: heap size 980 MB, throughput 0.845476
Reading from 12455: heap size 982 MB, throughput 0.887646
Reading from 12456: heap size 399 MB, throughput 0.937063
Reading from 12455: heap size 982 MB, throughput 0.736617
Reading from 12456: heap size 395 MB, throughput 0.81002
Reading from 12455: heap size 985 MB, throughput 0.637716
Reading from 12456: heap size 401 MB, throughput 0.824563
Reading from 12456: heap size 409 MB, throughput 0.805895
Reading from 12455: heap size 990 MB, throughput 0.066814
Reading from 12456: heap size 411 MB, throughput 0.984295
Reading from 12455: heap size 1111 MB, throughput 0.471785
Reading from 12455: heap size 1121 MB, throughput 0.681045
Reading from 12455: heap size 1122 MB, throughput 0.783899
Reading from 12455: heap size 1128 MB, throughput 0.692758
Reading from 12455: heap size 1131 MB, throughput 0.610923
Reading from 12455: heap size 1136 MB, throughput 0.660331
Reading from 12455: heap size 1139 MB, throughput 0.661505
Reading from 12455: heap size 1141 MB, throughput 0.670252
Reading from 12456: heap size 415 MB, throughput 0.979416
Reading from 12455: heap size 1146 MB, throughput 0.946741
Reading from 12456: heap size 417 MB, throughput 0.985384
Reading from 12455: heap size 1146 MB, throughput 0.93491
Equal recommendation: 4461 MB each
Reading from 12456: heap size 417 MB, throughput 0.988706
Reading from 12455: heap size 1154 MB, throughput 0.958216
Reading from 12456: heap size 420 MB, throughput 0.985965
Reading from 12455: heap size 1166 MB, throughput 0.950716
Reading from 12456: heap size 418 MB, throughput 0.969257
Reading from 12455: heap size 1167 MB, throughput 0.936024
Reading from 12455: heap size 1176 MB, throughput 0.955569
Reading from 12456: heap size 420 MB, throughput 0.984013
Reading from 12455: heap size 1180 MB, throughput 0.955164
Reading from 12456: heap size 417 MB, throughput 0.775424
Equal recommendation: 4461 MB each
Reading from 12455: heap size 1178 MB, throughput 0.95338
Reading from 12456: heap size 458 MB, throughput 0.974144
Reading from 12455: heap size 1182 MB, throughput 0.945839
Reading from 12456: heap size 457 MB, throughput 0.990354
Reading from 12456: heap size 460 MB, throughput 0.918875
Reading from 12456: heap size 463 MB, throughput 0.872793
Reading from 12456: heap size 463 MB, throughput 0.904113
Reading from 12455: heap size 1189 MB, throughput 0.953087
Reading from 12456: heap size 470 MB, throughput 0.990244
Reading from 12455: heap size 1190 MB, throughput 0.946683
Reading from 12456: heap size 470 MB, throughput 0.988903
Reading from 12455: heap size 1197 MB, throughput 0.947407
Equal recommendation: 4461 MB each
Reading from 12456: heap size 473 MB, throughput 0.987969
Reading from 12455: heap size 1200 MB, throughput 0.948704
Reading from 12456: heap size 475 MB, throughput 0.987995
Reading from 12455: heap size 1207 MB, throughput 0.95946
Reading from 12456: heap size 473 MB, throughput 0.98726
Reading from 12455: heap size 1212 MB, throughput 0.950444
Reading from 12456: heap size 476 MB, throughput 0.984606
Reading from 12455: heap size 1218 MB, throughput 0.952925
Reading from 12456: heap size 475 MB, throughput 0.978455
Reading from 12455: heap size 1224 MB, throughput 0.944699
Equal recommendation: 4461 MB each
Reading from 12456: heap size 476 MB, throughput 0.98345
Reading from 12455: heap size 1232 MB, throughput 0.951078
Reading from 12456: heap size 479 MB, throughput 0.989975
Reading from 12456: heap size 480 MB, throughput 0.820411
Reading from 12455: heap size 1237 MB, throughput 0.949063
Reading from 12456: heap size 480 MB, throughput 0.881749
Reading from 12456: heap size 483 MB, throughput 0.980938
Reading from 12456: heap size 491 MB, throughput 0.991472
Reading from 12455: heap size 1245 MB, throughput 0.547654
Equal recommendation: 4461 MB each
Reading from 12455: heap size 1316 MB, throughput 0.990153
Reading from 12456: heap size 493 MB, throughput 0.990231
Reading from 12455: heap size 1315 MB, throughput 0.986093
Reading from 12456: heap size 494 MB, throughput 0.992243
Reading from 12455: heap size 1325 MB, throughput 0.982835
Reading from 12456: heap size 497 MB, throughput 0.988563
Reading from 12455: heap size 1335 MB, throughput 0.978925
Reading from 12456: heap size 496 MB, throughput 0.986525
Reading from 12455: heap size 1335 MB, throughput 0.976246
Equal recommendation: 4461 MB each
Reading from 12456: heap size 498 MB, throughput 0.986575
Reading from 12455: heap size 1328 MB, throughput 0.975322
Reading from 12455: heap size 1228 MB, throughput 0.968461
Reading from 12456: heap size 501 MB, throughput 0.991185
Reading from 12456: heap size 501 MB, throughput 0.970022
Reading from 12456: heap size 501 MB, throughput 0.906039
Reading from 12456: heap size 503 MB, throughput 0.977581
Reading from 12455: heap size 1318 MB, throughput 0.969501
Reading from 12456: heap size 511 MB, throughput 0.992414
Reading from 12455: heap size 1250 MB, throughput 0.96837
Equal recommendation: 4461 MB each
Reading from 12455: heap size 1319 MB, throughput 0.965605
Reading from 12456: heap size 511 MB, throughput 0.99083
Reading from 12455: heap size 1322 MB, throughput 0.96326
Reading from 12456: heap size 513 MB, throughput 0.991078
Reading from 12455: heap size 1325 MB, throughput 0.966867
Reading from 12456: heap size 514 MB, throughput 0.989109
Reading from 12455: heap size 1328 MB, throughput 0.956743
Reading from 12456: heap size 514 MB, throughput 0.988019
Equal recommendation: 4461 MB each
Reading from 12455: heap size 1333 MB, throughput 0.956739
Reading from 12456: heap size 515 MB, throughput 0.985133
Reading from 12455: heap size 1339 MB, throughput 0.954234
Reading from 12456: heap size 518 MB, throughput 0.991753
Reading from 12456: heap size 519 MB, throughput 0.920597
Reading from 12456: heap size 520 MB, throughput 0.923546
Reading from 12455: heap size 1346 MB, throughput 0.950923
Reading from 12456: heap size 522 MB, throughput 0.991722
Reading from 12455: heap size 1355 MB, throughput 0.957588
Equal recommendation: 4461 MB each
Reading from 12456: heap size 526 MB, throughput 0.991007
Reading from 12455: heap size 1365 MB, throughput 0.963096
Reading from 12456: heap size 528 MB, throughput 0.989462
Reading from 12455: heap size 1370 MB, throughput 0.954725
Reading from 12456: heap size 526 MB, throughput 0.988864
Reading from 12455: heap size 1380 MB, throughput 0.953801
Reading from 12456: heap size 529 MB, throughput 0.988993
Reading from 12455: heap size 1383 MB, throughput 0.953202
Equal recommendation: 4461 MB each
Reading from 12456: heap size 532 MB, throughput 0.987483
Reading from 12456: heap size 533 MB, throughput 0.989024
Reading from 12456: heap size 535 MB, throughput 0.90934
Reading from 12456: heap size 538 MB, throughput 0.976081
Reading from 12456: heap size 546 MB, throughput 0.99227
Equal recommendation: 4461 MB each
Reading from 12456: heap size 546 MB, throughput 0.992391
Reading from 12456: heap size 547 MB, throughput 0.991346
Reading from 12456: heap size 549 MB, throughput 0.990459
Equal recommendation: 4461 MB each
Reading from 12456: heap size 549 MB, throughput 0.989618
Reading from 12455: heap size 1392 MB, throughput 0.99521
Reading from 12456: heap size 550 MB, throughput 0.993669
Reading from 12456: heap size 551 MB, throughput 0.925604
Reading from 12456: heap size 552 MB, throughput 0.961024
Reading from 12456: heap size 561 MB, throughput 0.988382
Equal recommendation: 4461 MB each
Reading from 12455: heap size 1393 MB, throughput 0.859864
Reading from 12455: heap size 1458 MB, throughput 0.992761
Reading from 12455: heap size 1497 MB, throughput 0.920321
Reading from 12455: heap size 1499 MB, throughput 0.887821
Reading from 12455: heap size 1505 MB, throughput 0.885999
Reading from 12455: heap size 1496 MB, throughput 0.89457
Reading from 12456: heap size 561 MB, throughput 0.991581
Reading from 12455: heap size 1502 MB, throughput 0.892443
Reading from 12455: heap size 1497 MB, throughput 0.98006
Reading from 12455: heap size 1504 MB, throughput 0.992663
Reading from 12456: heap size 561 MB, throughput 0.991739
Reading from 12455: heap size 1524 MB, throughput 0.988671
Equal recommendation: 4461 MB each
Reading from 12456: heap size 564 MB, throughput 0.990772
Reading from 12455: heap size 1526 MB, throughput 0.989421
Reading from 12456: heap size 566 MB, throughput 0.98961
Reading from 12455: heap size 1523 MB, throughput 0.98311
Reading from 12456: heap size 566 MB, throughput 0.989493
Reading from 12456: heap size 569 MB, throughput 0.925127
Reading from 12455: heap size 1530 MB, throughput 0.98024
Reading from 12456: heap size 571 MB, throughput 0.989801
Equal recommendation: 4461 MB each
Reading from 12455: heap size 1511 MB, throughput 0.976117
Reading from 12456: heap size 578 MB, throughput 0.992976
Reading from 12455: heap size 1391 MB, throughput 0.975706
Reading from 12456: heap size 579 MB, throughput 0.992179
Reading from 12455: heap size 1498 MB, throughput 0.97666
Reading from 12455: heap size 1508 MB, throughput 0.971021
Reading from 12456: heap size 579 MB, throughput 0.991822
Equal recommendation: 4461 MB each
Reading from 12455: heap size 1506 MB, throughput 0.969634
Reading from 12456: heap size 581 MB, throughput 0.989877
Reading from 12455: heap size 1508 MB, throughput 0.967234
Reading from 12456: heap size 583 MB, throughput 0.995113
Reading from 12456: heap size 583 MB, throughput 0.945303
Reading from 12456: heap size 584 MB, throughput 0.97763
Reading from 12455: heap size 1513 MB, throughput 0.963572
Equal recommendation: 4461 MB each
Reading from 12456: heap size 586 MB, throughput 0.986833
Reading from 12455: heap size 1517 MB, throughput 0.959156
Reading from 12455: heap size 1525 MB, throughput 0.956867
Reading from 12456: heap size 589 MB, throughput 0.992325
Reading from 12455: heap size 1533 MB, throughput 0.958919
Reading from 12456: heap size 590 MB, throughput 0.992434
Equal recommendation: 4461 MB each
Reading from 12455: heap size 1543 MB, throughput 0.961791
Reading from 12456: heap size 590 MB, throughput 0.990956
Reading from 12455: heap size 1554 MB, throughput 0.964614
Reading from 12456: heap size 592 MB, throughput 0.993395
Reading from 12455: heap size 1568 MB, throughput 0.95581
Reading from 12456: heap size 594 MB, throughput 0.972289
Reading from 12456: heap size 595 MB, throughput 0.974797
Equal recommendation: 4461 MB each
Reading from 12455: heap size 1576 MB, throughput 0.954896
Reading from 12456: heap size 600 MB, throughput 0.992255
Reading from 12455: heap size 1590 MB, throughput 0.958389
Reading from 12456: heap size 601 MB, throughput 0.99285
Reading from 12455: heap size 1594 MB, throughput 0.95655
Equal recommendation: 4461 MB each
Reading from 12456: heap size 599 MB, throughput 0.991948
Reading from 12455: heap size 1608 MB, throughput 0.960656
Reading from 12455: heap size 1610 MB, throughput 0.958372
Reading from 12456: heap size 602 MB, throughput 0.991681
Reading from 12455: heap size 1624 MB, throughput 0.957485
Reading from 12456: heap size 604 MB, throughput 0.994675
Reading from 12456: heap size 605 MB, throughput 0.942075
Equal recommendation: 4461 MB each
Reading from 12456: heap size 604 MB, throughput 0.987964
Reading from 12455: heap size 1625 MB, throughput 0.960014
Reading from 12456: heap size 607 MB, throughput 0.993319
Reading from 12455: heap size 1639 MB, throughput 0.957793
Reading from 12456: heap size 608 MB, throughput 0.992692
Equal recommendation: 4461 MB each
Reading from 12455: heap size 1639 MB, throughput 0.712032
Reading from 12456: heap size 610 MB, throughput 0.99213
Reading from 12455: heap size 1736 MB, throughput 0.991232
Reading from 12456: heap size 612 MB, throughput 0.990702
Reading from 12455: heap size 1736 MB, throughput 0.987805
Reading from 12456: heap size 612 MB, throughput 0.990035
Reading from 12456: heap size 614 MB, throughput 0.92611
Equal recommendation: 4461 MB each
Reading from 12455: heap size 1758 MB, throughput 0.985367
Reading from 12456: heap size 617 MB, throughput 0.992408
Reading from 12456: heap size 623 MB, throughput 0.993758
Equal recommendation: 4461 MB each
Reading from 12456: heap size 624 MB, throughput 0.99314
Reading from 12456: heap size 624 MB, throughput 0.991679
Equal recommendation: 4461 MB each
Reading from 12456: heap size 626 MB, throughput 0.994799
Reading from 12456: heap size 629 MB, throughput 0.948077
Client 12456 died
Clients: 1
Reading from 12455: heap size 1763 MB, throughput 0.996831
Recommendation: one client; give it all the memory
Reading from 12455: heap size 1745 MB, throughput 0.989513
Reading from 12455: heap size 1760 MB, throughput 0.835601
Reading from 12455: heap size 1740 MB, throughput 0.823625
Reading from 12455: heap size 1767 MB, throughput 0.804003
Reading from 12455: heap size 1803 MB, throughput 0.83179
Client 12455 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
