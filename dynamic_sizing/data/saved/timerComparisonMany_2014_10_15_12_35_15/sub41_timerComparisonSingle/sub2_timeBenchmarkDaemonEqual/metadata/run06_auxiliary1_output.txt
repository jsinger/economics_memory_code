economemd
    total memory: 1104 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub41_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 10912: heap size 9 MB, throughput 0.991547
Clients: 1
Client 10912 has a minimum heap size of 276 MB
Reading from 10913: heap size 9 MB, throughput 0.99111
Clients: 2
Client 10913 has a minimum heap size of 276 MB
Reading from 10912: heap size 9 MB, throughput 0.97815
Reading from 10913: heap size 9 MB, throughput 0.976075
Reading from 10912: heap size 9 MB, throughput 0.958684
Reading from 10913: heap size 9 MB, throughput 0.962846
Reading from 10912: heap size 9 MB, throughput 0.950224
Reading from 10913: heap size 9 MB, throughput 0.930339
Reading from 10912: heap size 11 MB, throughput 0.985254
Reading from 10913: heap size 11 MB, throughput 0.983789
Reading from 10912: heap size 11 MB, throughput 0.935408
Reading from 10913: heap size 11 MB, throughput 0.979271
Reading from 10912: heap size 17 MB, throughput 0.899782
Reading from 10913: heap size 17 MB, throughput 0.948918
Reading from 10912: heap size 17 MB, throughput 0.460591
Reading from 10912: heap size 30 MB, throughput 0.930371
Reading from 10913: heap size 17 MB, throughput 0.529932
Reading from 10913: heap size 30 MB, throughput 0.945651
Reading from 10912: heap size 31 MB, throughput 0.832967
Reading from 10913: heap size 31 MB, throughput 0.902498
Reading from 10912: heap size 35 MB, throughput 0.383684
Reading from 10913: heap size 33 MB, throughput 0.330751
Reading from 10912: heap size 48 MB, throughput 0.91139
Reading from 10913: heap size 45 MB, throughput 0.822409
Reading from 10912: heap size 50 MB, throughput 0.916757
Reading from 10913: heap size 49 MB, throughput 0.88324
Reading from 10913: heap size 49 MB, throughput 0.255016
Reading from 10912: heap size 52 MB, throughput 0.2513
Reading from 10913: heap size 68 MB, throughput 0.827244
Reading from 10912: heap size 75 MB, throughput 0.79253
Reading from 10913: heap size 69 MB, throughput 0.792399
Reading from 10913: heap size 73 MB, throughput 0.548232
Reading from 10912: heap size 75 MB, throughput 0.301738
Reading from 10912: heap size 101 MB, throughput 0.775419
Reading from 10913: heap size 76 MB, throughput 0.170915
Reading from 10913: heap size 108 MB, throughput 0.709329
Reading from 10912: heap size 102 MB, throughput 0.772538
Reading from 10913: heap size 108 MB, throughput 0.787043
Reading from 10912: heap size 103 MB, throughput 0.827224
Reading from 10913: heap size 112 MB, throughput 0.776493
Reading from 10912: heap size 106 MB, throughput 0.735351
Reading from 10913: heap size 113 MB, throughput 0.161478
Reading from 10913: heap size 152 MB, throughput 0.774801
Reading from 10912: heap size 109 MB, throughput 0.142932
Reading from 10912: heap size 142 MB, throughput 0.557603
Reading from 10913: heap size 153 MB, throughput 0.74133
Reading from 10912: heap size 145 MB, throughput 0.768725
Reading from 10912: heap size 148 MB, throughput 0.764353
Reading from 10913: heap size 157 MB, throughput 0.227686
Reading from 10912: heap size 157 MB, throughput 0.780495
Reading from 10913: heap size 194 MB, throughput 0.651666
Reading from 10912: heap size 159 MB, throughput 0.659002
Reading from 10913: heap size 202 MB, throughput 0.712757
Reading from 10913: heap size 202 MB, throughput 0.69973
Reading from 10912: heap size 162 MB, throughput 0.16725
Reading from 10912: heap size 203 MB, throughput 0.622647
Reading from 10913: heap size 206 MB, throughput 0.876312
Reading from 10912: heap size 207 MB, throughput 0.888546
Reading from 10913: heap size 213 MB, throughput 0.917749
Reading from 10913: heap size 217 MB, throughput 0.669743
Reading from 10912: heap size 211 MB, throughput 0.909926
Reading from 10913: heap size 225 MB, throughput 0.775868
Reading from 10912: heap size 212 MB, throughput 0.781639
Reading from 10912: heap size 222 MB, throughput 0.721354
Reading from 10913: heap size 231 MB, throughput 0.780781
Reading from 10912: heap size 227 MB, throughput 0.612486
Reading from 10913: heap size 237 MB, throughput 0.739861
Reading from 10912: heap size 229 MB, throughput 0.802825
Reading from 10913: heap size 242 MB, throughput 0.742018
Reading from 10912: heap size 232 MB, throughput 0.863153
Reading from 10913: heap size 246 MB, throughput 0.707217
Reading from 10912: heap size 234 MB, throughput 0.820368
Reading from 10912: heap size 238 MB, throughput 0.646112
Reading from 10913: heap size 251 MB, throughput 0.819534
Reading from 10912: heap size 238 MB, throughput 0.750521
Reading from 10913: heap size 254 MB, throughput 0.450037
Reading from 10913: heap size 260 MB, throughput 0.625733
Reading from 10912: heap size 243 MB, throughput 0.720368
Reading from 10912: heap size 243 MB, throughput 0.534488
Reading from 10913: heap size 264 MB, throughput 0.0778784
Reading from 10912: heap size 250 MB, throughput 0.0955991
Reading from 10912: heap size 289 MB, throughput 0.638934
Reading from 10913: heap size 303 MB, throughput 0.17997
Reading from 10912: heap size 287 MB, throughput 0.681417
Reading from 10913: heap size 347 MB, throughput 0.666982
Reading from 10912: heap size 290 MB, throughput 0.841103
Reading from 10913: heap size 349 MB, throughput 0.935782
Reading from 10912: heap size 292 MB, throughput 0.474294
Reading from 10913: heap size 350 MB, throughput 0.814844
Reading from 10913: heap size 344 MB, throughput 0.741322
Reading from 10913: heap size 348 MB, throughput 0.850573
Reading from 10913: heap size 343 MB, throughput 0.856392
Reading from 10912: heap size 335 MB, throughput 0.911079
Reading from 10912: heap size 338 MB, throughput 0.813939
Reading from 10913: heap size 347 MB, throughput 0.863971
Reading from 10912: heap size 339 MB, throughput 0.824498
Reading from 10912: heap size 340 MB, throughput 0.781334
Reading from 10913: heap size 345 MB, throughput 0.866535
Reading from 10912: heap size 342 MB, throughput 0.867588
Reading from 10913: heap size 347 MB, throughput 0.787693
Reading from 10913: heap size 346 MB, throughput 0.471825
Reading from 10912: heap size 341 MB, throughput 0.89806
Reading from 10913: heap size 347 MB, throughput 0.558329
Reading from 10912: heap size 343 MB, throughput 0.908805
Reading from 10913: heap size 346 MB, throughput 0.71396
Reading from 10912: heap size 342 MB, throughput 0.832333
Reading from 10913: heap size 349 MB, throughput 0.72421
Reading from 10912: heap size 344 MB, throughput 0.690754
Reading from 10913: heap size 349 MB, throughput 0.727617
Reading from 10912: heap size 344 MB, throughput 0.738662
Reading from 10912: heap size 345 MB, throughput 0.531101
Reading from 10912: heap size 351 MB, throughput 0.803872
Reading from 10912: heap size 351 MB, throughput 0.621668
Equal recommendation: 552 MB each
Reading from 10913: heap size 352 MB, throughput 0.919728
Reading from 10912: heap size 358 MB, throughput 0.973225
Reading from 10913: heap size 357 MB, throughput 0.966788
Reading from 10912: heap size 358 MB, throughput 0.976219
Reading from 10913: heap size 358 MB, throughput 0.976693
Reading from 10912: heap size 358 MB, throughput 0.971881
Reading from 10913: heap size 360 MB, throughput 0.954179
Reading from 10912: heap size 362 MB, throughput 0.985815
Reading from 10913: heap size 362 MB, throughput 0.979612
Reading from 10912: heap size 360 MB, throughput 0.97454
Reading from 10913: heap size 361 MB, throughput 0.973169
Reading from 10912: heap size 363 MB, throughput 0.977544
Reading from 10913: heap size 363 MB, throughput 0.970469
Reading from 10912: heap size 361 MB, throughput 0.974699
Reading from 10913: heap size 359 MB, throughput 0.970757
Equal recommendation: 552 MB each
Reading from 10912: heap size 363 MB, throughput 0.966587
Reading from 10913: heap size 362 MB, throughput 0.970253
Reading from 10913: heap size 360 MB, throughput 0.975071
Reading from 10912: heap size 364 MB, throughput 0.985163
Reading from 10913: heap size 361 MB, throughput 0.977173
Reading from 10912: heap size 365 MB, throughput 0.973892
Reading from 10913: heap size 363 MB, throughput 0.972166
Reading from 10912: heap size 368 MB, throughput 0.974209
Reading from 10913: heap size 363 MB, throughput 0.971686
Reading from 10912: heap size 369 MB, throughput 0.971694
Reading from 10913: heap size 364 MB, throughput 0.96686
Reading from 10912: heap size 372 MB, throughput 0.97157
Reading from 10913: heap size 365 MB, throughput 0.960144
Reading from 10912: heap size 373 MB, throughput 0.967663
Equal recommendation: 552 MB each
Reading from 10913: heap size 366 MB, throughput 0.979854
Reading from 10913: heap size 368 MB, throughput 0.876888
Reading from 10913: heap size 364 MB, throughput 0.778139
Reading from 10913: heap size 368 MB, throughput 0.776184
Reading from 10913: heap size 374 MB, throughput 0.79421
Reading from 10913: heap size 376 MB, throughput 0.941386
Reading from 10912: heap size 376 MB, throughput 0.982773
Reading from 10912: heap size 378 MB, throughput 0.96279
Reading from 10912: heap size 376 MB, throughput 0.843794
Reading from 10912: heap size 378 MB, throughput 0.779576
Reading from 10912: heap size 384 MB, throughput 0.796668
Reading from 10912: heap size 388 MB, throughput 0.832961
Reading from 10913: heap size 382 MB, throughput 0.700708
Reading from 10912: heap size 396 MB, throughput 0.987572
Reading from 10913: heap size 406 MB, throughput 0.994698
Reading from 10912: heap size 397 MB, throughput 0.984049
Reading from 10913: heap size 407 MB, throughput 0.98818
Reading from 10912: heap size 397 MB, throughput 0.985507
Reading from 10913: heap size 410 MB, throughput 0.991012
Reading from 10913: heap size 414 MB, throughput 0.991719
Reading from 10912: heap size 400 MB, throughput 0.987695
Equal recommendation: 552 MB each
Reading from 10913: heap size 415 MB, throughput 0.984136
Reading from 10912: heap size 399 MB, throughput 0.984075
Reading from 10913: heap size 411 MB, throughput 0.989326
Reading from 10912: heap size 401 MB, throughput 0.985341
Reading from 10913: heap size 370 MB, throughput 0.977495
Reading from 10912: heap size 398 MB, throughput 0.982536
Reading from 10913: heap size 407 MB, throughput 0.985789
Reading from 10912: heap size 401 MB, throughput 0.982245
Reading from 10913: heap size 379 MB, throughput 0.982854
Reading from 10912: heap size 400 MB, throughput 0.983014
Reading from 10913: heap size 407 MB, throughput 0.981591
Reading from 10912: heap size 401 MB, throughput 0.979008
Equal recommendation: 552 MB each
Reading from 10913: heap size 408 MB, throughput 0.986645
Reading from 10913: heap size 405 MB, throughput 0.973677
Reading from 10913: heap size 408 MB, throughput 0.844344
Reading from 10913: heap size 402 MB, throughput 0.813888
Reading from 10913: heap size 408 MB, throughput 0.802645
Reading from 10912: heap size 401 MB, throughput 0.976094
Reading from 10913: heap size 416 MB, throughput 0.970461
Reading from 10912: heap size 402 MB, throughput 0.982202
Reading from 10912: heap size 407 MB, throughput 0.854638
Reading from 10912: heap size 407 MB, throughput 0.842468
Reading from 10912: heap size 413 MB, throughput 0.790614
Reading from 10913: heap size 420 MB, throughput 0.984092
Reading from 10912: heap size 414 MB, throughput 0.985998
Reading from 10913: heap size 417 MB, throughput 0.985366
Reading from 10912: heap size 422 MB, throughput 0.98981
Reading from 10913: heap size 421 MB, throughput 0.98541
Reading from 10913: heap size 417 MB, throughput 0.984796
Reading from 10912: heap size 423 MB, throughput 0.986171
Equal recommendation: 552 MB each
Reading from 10913: heap size 420 MB, throughput 0.982992
Reading from 10912: heap size 426 MB, throughput 0.987262
Reading from 10913: heap size 417 MB, throughput 0.982855
Reading from 10912: heap size 427 MB, throughput 0.987696
Reading from 10913: heap size 419 MB, throughput 0.982718
Reading from 10912: heap size 426 MB, throughput 0.982764
Reading from 10913: heap size 418 MB, throughput 0.982761
Reading from 10912: heap size 428 MB, throughput 0.985065
Reading from 10913: heap size 420 MB, throughput 0.981819
Reading from 10912: heap size 427 MB, throughput 0.985536
Reading from 10913: heap size 422 MB, throughput 0.986932
Equal recommendation: 552 MB each
Reading from 10912: heap size 428 MB, throughput 0.982663
Reading from 10913: heap size 422 MB, throughput 0.988073
Reading from 10913: heap size 422 MB, throughput 0.893498
Reading from 10913: heap size 423 MB, throughput 0.843071
Reading from 10913: heap size 428 MB, throughput 0.865896
Reading from 10913: heap size 429 MB, throughput 0.986902
Reading from 10912: heap size 430 MB, throughput 0.992075
Reading from 10912: heap size 431 MB, throughput 0.895042
Reading from 10912: heap size 431 MB, throughput 0.879856
Reading from 10912: heap size 433 MB, throughput 0.893331
Reading from 10913: heap size 436 MB, throughput 0.983585
Reading from 10912: heap size 441 MB, throughput 0.987228
Reading from 10913: heap size 437 MB, throughput 0.987865
Reading from 10913: heap size 439 MB, throughput 0.991181
Reading from 10912: heap size 441 MB, throughput 0.990678
Reading from 10913: heap size 441 MB, throughput 0.98531
Equal recommendation: 552 MB each
Reading from 10912: heap size 443 MB, throughput 0.990565
Reading from 10913: heap size 439 MB, throughput 0.985272
Reading from 10912: heap size 445 MB, throughput 0.987079
Reading from 10913: heap size 441 MB, throughput 0.987131
Reading from 10912: heap size 444 MB, throughput 0.987922
Reading from 10913: heap size 441 MB, throughput 0.988393
Reading from 10913: heap size 442 MB, throughput 0.977919
Reading from 10912: heap size 446 MB, throughput 0.987764
Reading from 10913: heap size 444 MB, throughput 0.993738
Reading from 10913: heap size 444 MB, throughput 0.914294
Reading from 10913: heap size 444 MB, throughput 0.901102
Reading from 10912: heap size 446 MB, throughput 0.986454
Reading from 10913: heap size 446 MB, throughput 0.937248
Equal recommendation: 552 MB each
Reading from 10913: heap size 453 MB, throughput 0.988378
Reading from 10912: heap size 447 MB, throughput 0.983873
Reading from 10913: heap size 453 MB, throughput 0.991078
Reading from 10912: heap size 450 MB, throughput 0.992198
Reading from 10912: heap size 451 MB, throughput 0.900297
Reading from 10912: heap size 451 MB, throughput 0.908415
Reading from 10912: heap size 453 MB, throughput 0.984925
Reading from 10913: heap size 454 MB, throughput 0.988485
Reading from 10912: heap size 459 MB, throughput 0.988665
Reading from 10913: heap size 456 MB, throughput 0.987501
Equal recommendation: 552 MB each
Reading from 10912: heap size 460 MB, throughput 0.991074
Reading from 10913: heap size 454 MB, throughput 0.990321
Reading from 10912: heap size 460 MB, throughput 0.989576
Reading from 10913: heap size 456 MB, throughput 0.986702
Reading from 10913: heap size 456 MB, throughput 0.975378
Reading from 10912: heap size 462 MB, throughput 0.985069
Reading from 10913: heap size 457 MB, throughput 0.984095
Reading from 10912: heap size 460 MB, throughput 0.984843
Reading from 10913: heap size 459 MB, throughput 0.990402
Equal recommendation: 552 MB each
Reading from 10913: heap size 460 MB, throughput 0.899528
Reading from 10912: heap size 462 MB, throughput 0.986412
Reading from 10913: heap size 459 MB, throughput 0.872719
Reading from 10913: heap size 462 MB, throughput 0.980438
Reading from 10912: heap size 464 MB, throughput 0.798339
Reading from 10913: heap size 470 MB, throughput 0.993312
Reading from 10912: heap size 502 MB, throughput 0.969149
Reading from 10912: heap size 510 MB, throughput 0.90066
Reading from 10912: heap size 510 MB, throughput 0.927277
Reading from 10913: heap size 470 MB, throughput 0.989978
Reading from 10912: heap size 515 MB, throughput 0.993305
Reading from 10913: heap size 471 MB, throughput 0.990113
Equal recommendation: 552 MB each
Reading from 10912: heap size 516 MB, throughput 0.991928
Reading from 10913: heap size 473 MB, throughput 0.989272
Reading from 10913: heap size 471 MB, throughput 0.988537
Reading from 10912: heap size 519 MB, throughput 0.991189
Reading from 10913: heap size 473 MB, throughput 0.986869
Reading from 10912: heap size 521 MB, throughput 0.988447
Reading from 10913: heap size 474 MB, throughput 0.986028
Reading from 10912: heap size 519 MB, throughput 0.98871
Reading from 10913: heap size 475 MB, throughput 0.989914
Equal recommendation: 552 MB each
Reading from 10913: heap size 479 MB, throughput 0.91798
Reading from 10913: heap size 479 MB, throughput 0.88273
Reading from 10912: heap size 521 MB, throughput 0.987425
Reading from 10913: heap size 484 MB, throughput 0.987109
Reading from 10913: heap size 486 MB, throughput 0.99146
Reading from 10912: heap size 524 MB, throughput 0.991816
Reading from 10912: heap size 525 MB, throughput 0.962521
Reading from 10912: heap size 523 MB, throughput 0.896367
Reading from 10912: heap size 527 MB, throughput 0.986564
Reading from 10913: heap size 488 MB, throughput 0.993632
Reading from 10913: heap size 490 MB, throughput 0.988906
Reading from 10912: heap size 534 MB, throughput 0.994222
Equal recommendation: 552 MB each
Reading from 10913: heap size 488 MB, throughput 0.989901
Reading from 10912: heap size 536 MB, throughput 0.990553
Reading from 10913: heap size 490 MB, throughput 0.987153
Reading from 10912: heap size 534 MB, throughput 0.990831
Reading from 10913: heap size 492 MB, throughput 0.987585
Reading from 10912: heap size 537 MB, throughput 0.989488
Equal recommendation: 552 MB each
Reading from 10913: heap size 492 MB, throughput 0.994181
Reading from 10913: heap size 496 MB, throughput 0.931832
Reading from 10913: heap size 496 MB, throughput 0.877312
Reading from 10912: heap size 538 MB, throughput 0.989178
Reading from 10913: heap size 500 MB, throughput 0.991415
Reading from 10912: heap size 539 MB, throughput 0.985479
Reading from 10913: heap size 502 MB, throughput 0.992032
Reading from 10912: heap size 538 MB, throughput 0.983955
Reading from 10912: heap size 543 MB, throughput 0.899245
Reading from 10912: heap size 547 MB, throughput 0.986017
Reading from 10913: heap size 505 MB, throughput 0.991095
Equal recommendation: 552 MB each
Reading from 10912: heap size 548 MB, throughput 0.992577
Reading from 10913: heap size 506 MB, throughput 0.99345
Reading from 10912: heap size 548 MB, throughput 0.992292
Reading from 10913: heap size 504 MB, throughput 0.992806
Reading from 10912: heap size 549 MB, throughput 0.990935
Reading from 10913: heap size 506 MB, throughput 0.989791
Reading from 10912: heap size 548 MB, throughput 0.989589
Equal recommendation: 552 MB each
Reading from 10913: heap size 508 MB, throughput 0.989749
Reading from 10913: heap size 509 MB, throughput 0.96281
Reading from 10913: heap size 510 MB, throughput 0.896982
Reading from 10912: heap size 549 MB, throughput 0.984248
Reading from 10913: heap size 512 MB, throughput 0.986946
Reading from 10913: heap size 518 MB, throughput 0.993887
Reading from 10912: heap size 551 MB, throughput 0.992326
Reading from 10912: heap size 551 MB, throughput 0.966456
Reading from 10912: heap size 551 MB, throughput 0.902148
Reading from 10913: heap size 519 MB, throughput 0.991468
Reading from 10912: heap size 553 MB, throughput 0.989386
Equal recommendation: 552 MB each
Reading from 10913: heap size 518 MB, throughput 0.991683
Reading from 10912: heap size 543 MB, throughput 0.992561
Reading from 10913: heap size 520 MB, throughput 0.987882
Reading from 10912: heap size 548 MB, throughput 0.990724
Reading from 10913: heap size 520 MB, throughput 0.989821
Reading from 10912: heap size 548 MB, throughput 0.991522
Equal recommendation: 552 MB each
Reading from 10913: heap size 521 MB, throughput 0.995392
Reading from 10913: heap size 523 MB, throughput 0.91353
Reading from 10913: heap size 524 MB, throughput 0.893155
Reading from 10912: heap size 551 MB, throughput 0.98958
Reading from 10913: heap size 530 MB, throughput 0.994068
Reading from 10912: heap size 551 MB, throughput 0.991968
Reading from 10913: heap size 531 MB, throughput 0.99246
Reading from 10912: heap size 552 MB, throughput 0.992812
Reading from 10912: heap size 537 MB, throughput 0.916716
Reading from 10912: heap size 547 MB, throughput 0.934675
Reading from 10913: heap size 532 MB, throughput 0.993321
Equal recommendation: 552 MB each
Reading from 10912: heap size 554 MB, throughput 0.991523
Reading from 10913: heap size 533 MB, throughput 0.990362
Reading from 10912: heap size 520 MB, throughput 0.991739
Reading from 10913: heap size 533 MB, throughput 0.989374
Reading from 10912: heap size 549 MB, throughput 0.990988
Reading from 10913: heap size 534 MB, throughput 0.987711
Reading from 10913: heap size 535 MB, throughput 0.983894
Equal recommendation: 552 MB each
Reading from 10913: heap size 530 MB, throughput 0.9142
Reading from 10912: heap size 548 MB, throughput 0.991607
Reading from 10913: heap size 532 MB, throughput 0.986883
Reading from 10912: heap size 547 MB, throughput 0.990279
Reading from 10913: heap size 502 MB, throughput 0.992255
Reading from 10912: heap size 549 MB, throughput 0.99426
Reading from 10913: heap size 534 MB, throughput 0.991786
Reading from 10912: heap size 551 MB, throughput 0.978157
Reading from 10912: heap size 552 MB, throughput 0.911721
Equal recommendation: 552 MB each
Reading from 10912: heap size 547 MB, throughput 0.989852
Reading from 10913: heap size 508 MB, throughput 0.991342
Reading from 10912: heap size 551 MB, throughput 0.993397
Reading from 10913: heap size 533 MB, throughput 0.989411
Reading from 10912: heap size 552 MB, throughput 0.99086
Reading from 10913: heap size 532 MB, throughput 0.989408
Reading from 10913: heap size 534 MB, throughput 0.990994
Reading from 10912: heap size 553 MB, throughput 0.991138
Equal recommendation: 552 MB each
Reading from 10913: heap size 534 MB, throughput 0.908844
Reading from 10913: heap size 533 MB, throughput 0.967944
Reading from 10912: heap size 533 MB, throughput 0.988406
Reading from 10913: heap size 535 MB, throughput 0.938072
Reading from 10912: heap size 544 MB, throughput 0.992593
Reading from 10913: heap size 540 MB, throughput 0.996095
Reading from 10912: heap size 546 MB, throughput 0.993279
Reading from 10912: heap size 547 MB, throughput 0.921282
Reading from 10912: heap size 546 MB, throughput 0.936922
Equal recommendation: 552 MB each
Reading from 10913: heap size 496 MB, throughput 0.995644
Reading from 10912: heap size 549 MB, throughput 0.991805
Reading from 10913: heap size 539 MB, throughput 0.992331
Reading from 10912: heap size 553 MB, throughput 0.992684
Reading from 10913: heap size 506 MB, throughput 0.990671
Reading from 10912: heap size 526 MB, throughput 0.991766
Reading from 10913: heap size 538 MB, throughput 0.99288
Equal recommendation: 552 MB each
Reading from 10913: heap size 538 MB, throughput 0.931869
Reading from 10913: heap size 534 MB, throughput 0.920506
Reading from 10912: heap size 549 MB, throughput 0.988965
Reading from 10913: heap size 533 MB, throughput 0.990897
Reading from 10912: heap size 548 MB, throughput 0.989781
Reading from 10913: heap size 537 MB, throughput 0.991266
Reading from 10912: heap size 550 MB, throughput 0.9905
Reading from 10913: heap size 511 MB, throughput 0.990476
Reading from 10912: heap size 551 MB, throughput 0.984259
Reading from 10912: heap size 552 MB, throughput 0.911493
Equal recommendation: 552 MB each
Reading from 10912: heap size 539 MB, throughput 0.986629
Reading from 10913: heap size 533 MB, throughput 0.989128
Reading from 10912: heap size 549 MB, throughput 0.99317
Reading from 10913: heap size 533 MB, throughput 0.984066
Reading from 10912: heap size 550 MB, throughput 0.990449
Reading from 10913: heap size 535 MB, throughput 0.987555
Reading from 10913: heap size 528 MB, throughput 0.988921
Equal recommendation: 552 MB each
Reading from 10913: heap size 535 MB, throughput 0.899508
Client 10913 died
Clients: 1
Reading from 10912: heap size 551 MB, throughput 0.993711
Reading from 10912: heap size 551 MB, throughput 0.991864
Reading from 10912: heap size 551 MB, throughput 0.992806
Reading from 10912: heap size 552 MB, throughput 0.995361
Recommendation: one client; give it all the memory
Reading from 10912: heap size 554 MB, throughput 0.947865
Reading from 10912: heap size 555 MB, throughput 0.928447
Client 10912 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
