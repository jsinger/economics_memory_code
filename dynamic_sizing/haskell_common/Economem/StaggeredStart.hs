module Economem.StaggeredStart where

import Control.Applicative
import qualified Data.ByteString.Char8 as B
import qualified Data.Csv as C
import Economem.Benchmark
import Economem.Common
import Economem.Daemon
import Economem.DelayedStart
import Economem.Experiment
import Economem.Files
import Economem.Java
import qualified Economem.Log as L
import qualified Economem.MetadataDir as M
import qualified Economem.MinHeap as MH
import Economem.Process
import Economem.Records
import Economem.ResourceUsageFile
import Economem.Run
import Text.Printf

data StaggeredStartInput = StaggeredStartInput { ssiBenchmark :: BenchmarkThreadsIters,
                                                 ssiJava :: Java,
                                                 ssiStartTime :: Integer,
                                                 ssiStartingHeap :: Maybe Integer }
instance Pretty StaggeredStartInput where
  pretty (StaggeredStartInput bm j t sh) =
    "'" ++ pretty bm ++ "' on " ++ javaName j ++ ", starting at " ++ show t ++ "s"
    ++ (case sh of
         Nothing -> ""
         Just h -> ", starting heap at " ++ show h ++ " MB")

instance C.FromNamedRecord StaggeredStartInput where
  parseNamedRecord r = StaggeredStartInput <$>
                       C.parseNamedRecord r <*>
                       C.parseNamedRecord r <*>
                       r C..: B.pack "START_TIME" <*>
                       r C..: B.pack "STARTING_HEAP"
instance C.ToNamedRecord StaggeredStartInput where
  toNamedRecord tr = C.namedRecord (getNamedRecordInput tr)
instance CsvFields StaggeredStartInput where
  getCsvFieldNames _ = getCsvFieldNames (Nothing :: Maybe BenchmarkThreadsIters) ++
                       getCsvFieldNames (Nothing :: Maybe Java) ++
                       [B.pack "START_TIME",
                        B.pack "STARTING_HEAP"]
  getNamedRecordInput (StaggeredStartInput bm j t sh) =
    getNamedRecordInput bm ++
    getNamedRecordInput j ++
    [B.pack "START_TIME" C..= t,
     B.pack "STARTING_HEAP" C..= sh]


data StaggeredStartDir =
  StaggeredStartDir { dirPath :: FilePath,
                      benchmarksFile :: CsvFile StaggeredStartInput,
                      maxHeapFile :: IntFile,
                      minHeapsFile :: CsvFile BenchmarkThreadsMB,
                      makeRecommendationsFile :: WholeFile Bool,
                      forceGrowthFile :: WholeFile Bool,
                      daemonLogFile :: FilePath,
                      pidNamesFile :: CsvFile PidName,
                      highestUsedMemFile :: IntFile,
                      resultsPlotFile :: FilePath }

instance Path StaggeredStartDir where
  getPath = dirPath
instance Dir StaggeredStartDir where
  asDir path =
    StaggeredStartDir { dirPath = getPath path,
                        benchmarksFile = CsvFile (path </> "benchmarks.csv"),
                        maxHeapFile = WholeFile (path </> "max_heap_MB.txt"),
                        minHeapsFile = CsvFile (path </> "min_heaps.csv"),
                        makeRecommendationsFile = WholeFile (path </> "make_recommendations.txt"),
                        forceGrowthFile = WholeFile (path </> "force_growth.txt"),
                        daemonLogFile = path </> "daemon_log.txt",
                        pidNamesFile = CsvFile (path </> "pid_names.csv"),
                        highestUsedMemFile = WholeFile (path </> "highest_used_mem.txt"),
                        resultsPlotFile = path </> "behaviour.pdf" }

  postprocessingActions = [postprocessStaggered]
    where
      postprocessStaggered :: StaggeredStartDir -> IO ()
      postprocessStaggered d = do
        makeRecommendations <- get (makeRecommendationsFile d)
        forceGrowth <- get (forceGrowthFile d)
        -- Americanised spellings for paper
        plotBehaviour
          ("Behavior " ++ (if makeRecommendations then "with" else "without") ++ " Forseti daemon" ++
           if forceGrowth then " (forced growth)" else "")
          staggeredThroughput
          (daemonLogFile d)
          (resultsPlotFile d)
          (getPath (highestUsedMemFile d))
          (Just (pidNamesFile d))

name :: String
name = "staggeredStart"

staggeredThroughput :: ThroughputType
staggeredThroughput = vengerov


newStaggeredStart :: Input StaggeredStartInput ->
                     DataSize ->
                     Input BenchmarkThreadsMB ->
                     Bool ->
                     Bool ->
                     Maybe SubexpInfo ->
                     IO Experiment
newStaggeredStart input maxHeap minHeaps makeRecommendations forceGrowth info = do
  (expDir, num, parentLog) <- newExperimentProperties name info
  makeStaggeredStart
    expDir
    num
    input
    maxHeap
    minHeaps
    makeRecommendations
    forceGrowth
    parentLog

resumeStaggeredStart :: FilePath -> IO Experiment
resumeStaggeredStart =
  resumeExperiment
  (\expDir -> do let d = asDir expDir
                 let input = FromFile (benchmarksFile d)
                 maxHeap <- (`DataSize` MB) <$> get (maxHeapFile d)
                 let minHeaps = FromFile (minHeapsFile d)
                 makeRecommendations <- get (makeRecommendationsFile d)
                 forceGrowth <- get (forceGrowthFile d)
                 makeStaggeredStart
                   expDir
                   topLevel
                   input
                   maxHeap
                   minHeaps
                   makeRecommendations
                   forceGrowth
                   Nothing)

makeStaggeredStart :: ExperimentDir ->
                      ExperimentNum ->
                      Input StaggeredStartInput ->
                      DataSize ->
                      Input BenchmarkThreadsMB ->
                      Bool ->
                      Bool ->
                      Maybe L.Log ->
                      IO Experiment
makeStaggeredStart expDir expNum inputLoader maxHeap minHeapsLoader makeRecommendations forceGrowth parentLog = do
  let ex = makeExperiment name expDir expNum 1 parentLog
  let d = asDir expDir
  input <- loadInput inputLoader
  minHeaps <- loadInput minHeapsLoader
  return (ex { experimentIntro = intro input,
               saveAdditionalStaticMetadata = saveStatic d input minHeaps,
               canResume = canResumeStaggeredStart d,
               setup = setupStaggeredStart,
               createRun = createRunStaggeredStart d input minHeaps,
               cleanup = cleanupStaggeredStart d input })

  where
    intro :: [StaggeredStartInput] -> [String]
    intro input =
      ["Running benchmarks:"] ++
      map (\s -> "    " ++ pretty s) input ++
      ["With memory budget: " ++ pretty (toMB maxHeap),
       if makeRecommendations then "Making recommendations" else "Not making recommendations"] ++
      if forceGrowth then ["Forcing heap growth"] else []

    saveStatic :: StaggeredStartDir -> [StaggeredStartInput] -> [BenchmarkThreadsMB] -> IO ()
    saveStatic d input minHeaps = do
      input |> benchmarksFile d
      sizeToInt (toMB maxHeap) |> maxHeapFile d
      minHeaps |> minHeapsFile d
      makeRecommendations |> makeRecommendationsFile d
      forceGrowth |> forceGrowthFile d

    canResumeStaggeredStart :: StaggeredStartDir -> IO Bool
    canResumeStaggeredStart d =
      allExistInDir d [getPath . benchmarksFile,
                       getPath . maxHeapFile,
                       getPath . minHeapsFile,
                       getPath . makeRecommendationsFile,
                       getPath . forceGrowthFile]

    setupStaggeredStart :: Experiment -> IO ()
    setupStaggeredStart _ =
      checkBenchmarkPrerequisites

    createRunStaggeredStart :: StaggeredStartDir ->
                               [StaggeredStartInput] ->
                               [BenchmarkThreadsMB] ->
                               Experiment ->
                               RunNum ->
                               IO Run
    createRunStaggeredStart d input minHeaps _ run = do
      launchers <- mapM createLauncher (zip input (upTo (fromIntegral (length input))))
      daemon <- createDaemon
      return Run { runNum = run,
                   runType = RunToCompletion,
                   mainProcesses = launchers,
                   auxiliaryProcesses = [daemon],
                   auxiliaryProcessDelay = 20,
                   runSetupActions = [],
                   runCleanupActions = [] }

      where
        createLauncher :: (StaggeredStartInput, ProcessNum) -> IO ProcessLauncher
        createLauncher (bm, pNum) = do
          realMaxHeap <- min (DataSize (10 * sizeToInt (toMB maxHeap)) MB) <$> systemMemory
          benchmark <- makeBenchmark (ssiBenchmark bm) realMaxHeap
          minHeap <- MH.lookupMinHeap (btiBenchmark (ssiBenchmark bm)) minHeaps
          benchmarkProcessLauncher
            (case ssiStartingHeap bm of
              Nothing -> benchmark
              Just sh -> benchmark { benchmarkStartingHeap = DataSize sh MB })
            (ssiJava bm)
            (Just devnull)
            (Just (M.procOutputFile (metadataDir expDir) run pNum))
            (getPath d </> (printf "working%d" (getNum pNum) :: FilePath))
            (getPath d </> (printf "scratch%d" (getNum pNum) :: FilePath))
            Nothing
            (Just (M.procResourceUsageFile (metadataDir expDir) run pNum))
            (Just (DelayedStart (ssiStartTime bm)))
            [staggeredThroughput]
            (Just minHeap)
            forceGrowth

        createDaemon :: IO ProcessLauncher
        createDaemon = do
          let daemon = (makeDaemon maxHeap) { recommendOnTimer = makeRecommendations,
                                              readingRecv = Just $ throughputSocketName staggeredThroughput,
                                              commandRecv = Just $ throughputSocketName staggeredThroughput,
                                              logFile = Just (daemonLogFile d),
                                              logReadings = True,
                                              logFunctions = True,
                                              verbose = True }
          daemonProcessLauncher
            daemon
            (M.auxOutputFile (metadataDir expDir) run (ProcessNum 1 1))
            (getPath d)
            (Just (M.auxResourceUsageFile (metadataDir expDir) run (ProcessNum 1 1)))

    cleanupStaggeredStart :: StaggeredStartDir -> [StaggeredStartInput] -> IO ()
    cleanupStaggeredStart d input = do
      put (pidNamesFile d) =<< mapM getPidName (zipNumAndMax input)
      where
        getPidName :: (StaggeredStartInput, ProcessNum) -> IO PidName
        getPidName ((StaggeredStartInput bm j _ _), n) = do
          pid <- getPid (M.procResourceUsageFile (metadataDir expDir) (RunNum 1 1) n)
          return (PidName pid (printf "'%s' on %s" (pretty bm) (javaPrettyName j)))
