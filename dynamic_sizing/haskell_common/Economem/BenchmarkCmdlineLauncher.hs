{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, DeriveDataTypeable #-}
{-# OPTIONS_GHC -fno-warn-missing-fields -fno-warn-missing-methods #-}

module Economem.BenchmarkCmdlineLauncher (launchBenchmark) where

import Control.Applicative
import Control.Exception
import Control.Monad
import Data.Maybe
import Economem.ArgHelper
import Economem.Common
import Economem.Benchmark
import qualified Economem.Daemon as D
import Economem.Files
import Economem.Java
import Economem.Process
import Economem.Records
import Economem.Signals
import System.Console.CmdLib
import System.Directory
import System.Posix.Process
import Text.Printf

data Args = Args { benchmark :: String,
                   size :: String,
                   threads :: Int,
                   iterations :: Int,
                   maxHeapMB :: Int,
                   gcLog :: Bool,
                   minHeapMB :: Int,
                   forceGrowth :: Bool,
                   simpleThroughput :: Bool,
                   vengerovThroughput :: Bool,
                   avgVengerovThroughput :: Bool,
                   majorOnlyThroughput :: Bool,
                   simpleSmoothingThroughput :: Bool,
                   proportionalThroughput :: Bool,
                   verbose :: Bool,
                   vmArgs :: [String] }
          deriving (Typeable, Data, Eq, Show)

instance Attributes Args where
  attributes _ = group "Options" [
    benchmark %> [ Positional 0,
                   Required True,
                   Help "Benchmark to run",
                   ArgHelp "BENCHMARK" ],
    size %> [ Short ['s'],
              Long ["size"],
              Default "large",
              Help "Benchmark input size",
              ArgHelp "SIZE" ],
    threads %> [ Short ['t'],
                 Long ["threads"],
                 Default (0::Int),
                 Help "How many threads to use",
                 ArgHelp "THREADS" ],
    iterations %> [ Short ['i'],
                    Long ["iterations"],
                    Default (10000000::Int),
                    Help "How many iterations of the benchmark to run",
                    ArgHelp "ITERATIONS" ],
    maxHeapMB %> [ Short ['m'],
                   Long ["max-heap-mb"],
                   Default (0::Int),
                   Help "Maximum allowed heap size in MB",
                   ArgHelp "MAX_HEAP_MB"],
    gcLog %> [ Short ['l'],
               Long ["gclog"],
               Default False,
               Help "Turn on GC logging"],
    minHeapMB %> [ Long ["min-heap-mb"],
                   Default (0::Int),
                   Help "Min heap to report to the daemon, in MB",
                   ArgHelp "MIN_HEAP_MB" ],
    forceGrowth %> [ Long ["force-growth"],
                     Default False,
                     Help "Force the heap to grow whenever possible"],
    simpleThroughput %> [ Long ["simple"],
                          Default False,
                          Help "Use the simple throughput model"],
    vengerovThroughput %> [ Long ["vengerov"],
                          Default False,
                          Help "Use the vengerov throughput model"],
    avgVengerovThroughput %> [ Long ["avg-vengerov"],
                               Default False,
                               Help "Use the averaged vengerov throughput model"],
    majorOnlyThroughput %> [ Long ["major-only"],
                             Default False,
                             Help "Use the major-only throughput model"],
    simpleSmoothingThroughput %> [ Long ["simple-smoothing"],
                                   Default False,
                                   Help "Use the simple smoothing throughput model"],
    proportionalThroughput %> [ Long ["proportional"],
                                Default False,
                                Help "Use the proportional throughput model"],
    verbose %> [ Short ['v'],
                 Long ["verbose"],
                 Default False,
                 Help "Display the command line being executed"],
    vmArgs %> [ Extra True ]]


instance RecordCommand Args where
  mode_summary _ = "Run a benchmark"

launchBenchmark :: Java -> IO ()
launchBenchmark j = getArgs >>= executeR Args {} >>= \opts -> do
  (bm, xmx, xms, gclog, grow, throughput) <- validate opts
  checkBenchmarkPrerequisites
  pid <- getProcessID
  tempdir <- (</> (printf "scratch%d" (fromIntegral pid :: Int) :: FilePath)) <$> getTemporaryDirectory
  working <- (</> (printf "working%d" (fromIntegral pid :: Int) :: FilePath)) <$> getTemporaryDirectory
  d <- (\b -> b { benchmarkGCLogging = gclog, benchmarkExtraVMArgs = vmArgs opts }) <$> makeBenchmark bm xmx
  p <- benchmarkProcessLauncher d j Nothing Nothing working tempdir Nothing Nothing Nothing throughput xms grow
  when (verbose opts) (print p >> putStrLn "")
  h <- makeSignalHandler
  prepareProcess p
  bracket
    (start p)
    (\running -> cleanupProcess running)
    (\running -> do killProcessOnSignal h running
                    wait running)
  where
    validate :: Args -> IO (BenchmarkThreadsIters,
                            DataSize,
                            Maybe DataSize,
                            Bool,
                            Bool,
                            [D.ThroughputType])
    validate a = do
      let bm = benchmark a
      let sz = size a
      thr <- if threads a > 0 then return (threads a)
             else fromIntegral <$> numProcessors
      iters <- positive (iterations a)
      xmx <- if maxHeapMB a > 0 then return (DataSize (fromIntegral (maxHeapMB a)) MB)
             else systemMemory
      let xms = if minHeapMB a > 0 then Just (DataSize (fromIntegral (minHeapMB a)) MB)
                else Nothing
      return (BenchmarkThreadsIters (BenchmarkThreads (Benchmark bm sz) thr) iters,
              xmx,
              xms,
              gcLog a,
              forceGrowth a,
              catMaybes [thrp D.simpleThroughput simpleThroughput,
                         thrp D.vengerov vengerovThroughput,
                         thrp D.avgVengerov avgVengerovThroughput,
                         thrp D.majorOnly majorOnlyThroughput,
                         thrp D.simpleSmoothing simpleSmoothingThroughput,
                         thrp D.proportional proportionalThroughput])

        where
          thrp :: D.ThroughputType -> (Args -> Bool) -> Maybe D.ThroughputType
          thrp t f = if f a then
                       Just t
                     else
                       Nothing
