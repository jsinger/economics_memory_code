economemd
    total memory: 2446 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub11_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 3238: heap size 9 MB, throughput 0.987776
Clients: 1
Client 3238 has a minimum heap size of 12 MB
Reading from 3237: heap size 9 MB, throughput 0.968179
Clients: 2
Client 3237 has a minimum heap size of 1211 MB
Reading from 3238: heap size 9 MB, throughput 0.988693
Reading from 3237: heap size 9 MB, throughput 0.983114
Reading from 3237: heap size 11 MB, throughput 0.97137
Reading from 3238: heap size 11 MB, throughput 0.981535
Reading from 3237: heap size 11 MB, throughput 0.969796
Reading from 3238: heap size 11 MB, throughput 0.970189
Reading from 3238: heap size 15 MB, throughput 0.990563
Reading from 3237: heap size 15 MB, throughput 0.885783
Reading from 3237: heap size 19 MB, throughput 0.969471
Reading from 3238: heap size 15 MB, throughput 0.987192
Reading from 3237: heap size 25 MB, throughput 0.922997
Reading from 3237: heap size 29 MB, throughput 0.739572
Reading from 3237: heap size 40 MB, throughput 0.957415
Reading from 3238: heap size 24 MB, throughput 0.897909
Reading from 3237: heap size 41 MB, throughput 0.861347
Reading from 3237: heap size 43 MB, throughput 0.793792
Reading from 3238: heap size 29 MB, throughput 0.988569
Reading from 3237: heap size 44 MB, throughput 0.332224
Reading from 3237: heap size 61 MB, throughput 0.775282
Reading from 3237: heap size 64 MB, throughput 0.742466
Reading from 3238: heap size 34 MB, throughput 0.986948
Reading from 3237: heap size 67 MB, throughput 0.268026
Reading from 3238: heap size 39 MB, throughput 0.977943
Reading from 3237: heap size 87 MB, throughput 0.662898
Reading from 3237: heap size 94 MB, throughput 0.754917
Reading from 3237: heap size 95 MB, throughput 0.648292
Reading from 3238: heap size 41 MB, throughput 0.988553
Reading from 3237: heap size 99 MB, throughput 0.734462
Reading from 3238: heap size 45 MB, throughput 0.985937
Reading from 3238: heap size 49 MB, throughput 0.982328
Reading from 3237: heap size 103 MB, throughput 0.220275
Reading from 3238: heap size 50 MB, throughput 0.988101
Reading from 3237: heap size 131 MB, throughput 0.679148
Reading from 3237: heap size 134 MB, throughput 0.576789
Reading from 3238: heap size 56 MB, throughput 0.979642
Reading from 3237: heap size 138 MB, throughput 0.608353
Reading from 3238: heap size 56 MB, throughput 0.952543
Reading from 3237: heap size 143 MB, throughput 0.583659
Reading from 3238: heap size 65 MB, throughput 0.738381
Reading from 3238: heap size 70 MB, throughput 0.989473
Reading from 3237: heap size 152 MB, throughput 0.107146
Reading from 3238: heap size 78 MB, throughput 0.996416
Reading from 3237: heap size 190 MB, throughput 0.626971
Reading from 3237: heap size 197 MB, throughput 0.769
Reading from 3237: heap size 198 MB, throughput 0.719255
Reading from 3237: heap size 201 MB, throughput 0.709917
Reading from 3237: heap size 207 MB, throughput 0.665886
Reading from 3237: heap size 211 MB, throughput 0.627285
Reading from 3238: heap size 78 MB, throughput 0.997477
Reading from 3237: heap size 220 MB, throughput 0.161629
Reading from 3237: heap size 263 MB, throughput 0.534466
Reading from 3238: heap size 85 MB, throughput 0.996054
Reading from 3237: heap size 271 MB, throughput 0.51528
Reading from 3237: heap size 274 MB, throughput 0.616221
Reading from 3237: heap size 280 MB, throughput 0.498082
Reading from 3238: heap size 86 MB, throughput 0.995858
Reading from 3238: heap size 92 MB, throughput 0.995392
Reading from 3237: heap size 286 MB, throughput 0.0843001
Reading from 3237: heap size 335 MB, throughput 0.499904
Reading from 3237: heap size 328 MB, throughput 0.65318
Reading from 3237: heap size 334 MB, throughput 0.638461
Reading from 3237: heap size 336 MB, throughput 0.701046
Reading from 3238: heap size 92 MB, throughput 0.994409
Reading from 3237: heap size 339 MB, throughput 0.636863
Reading from 3237: heap size 341 MB, throughput 0.610484
Reading from 3238: heap size 98 MB, throughput 0.997141
Reading from 3237: heap size 349 MB, throughput 0.110644
Reading from 3237: heap size 402 MB, throughput 0.466969
Reading from 3237: heap size 409 MB, throughput 0.58604
Reading from 3237: heap size 410 MB, throughput 0.548127
Reading from 3238: heap size 98 MB, throughput 0.996661
Reading from 3237: heap size 412 MB, throughput 0.572326
Reading from 3237: heap size 417 MB, throughput 0.538736
Reading from 3237: heap size 421 MB, throughput 0.562029
Reading from 3237: heap size 426 MB, throughput 0.520451
Reading from 3238: heap size 102 MB, throughput 0.995713
Equal recommendation: 1223 MB each
Reading from 3238: heap size 102 MB, throughput 0.996589
Reading from 3237: heap size 429 MB, throughput 0.0951869
Reading from 3237: heap size 485 MB, throughput 0.538546
Reading from 3237: heap size 486 MB, throughput 0.54448
Reading from 3237: heap size 484 MB, throughput 0.612809
Reading from 3238: heap size 104 MB, throughput 0.992756
Reading from 3238: heap size 105 MB, throughput 0.995184
Reading from 3237: heap size 485 MB, throughput 0.107591
Reading from 3237: heap size 545 MB, throughput 0.412026
Reading from 3237: heap size 546 MB, throughput 0.563477
Reading from 3238: heap size 109 MB, throughput 0.99651
Reading from 3237: heap size 546 MB, throughput 0.53634
Reading from 3237: heap size 548 MB, throughput 0.483481
Reading from 3237: heap size 552 MB, throughput 0.613711
Reading from 3237: heap size 558 MB, throughput 0.560914
Reading from 3238: heap size 109 MB, throughput 0.996708
Reading from 3237: heap size 563 MB, throughput 0.556651
Reading from 3238: heap size 111 MB, throughput 0.995438
Reading from 3237: heap size 572 MB, throughput 0.109218
Reading from 3238: heap size 112 MB, throughput 0.994228
Reading from 3237: heap size 634 MB, throughput 0.518193
Reading from 3237: heap size 642 MB, throughput 0.598206
Reading from 3237: heap size 647 MB, throughput 0.606104
Reading from 3238: heap size 115 MB, throughput 0.996092
Reading from 3237: heap size 650 MB, throughput 0.842404
Reading from 3238: heap size 115 MB, throughput 0.99752
Reading from 3237: heap size 654 MB, throughput 0.868805
Reading from 3238: heap size 118 MB, throughput 0.998015
Reading from 3238: heap size 118 MB, throughput 0.996939
Reading from 3237: heap size 660 MB, throughput 0.315132
Reading from 3238: heap size 120 MB, throughput 0.997738
Reading from 3237: heap size 721 MB, throughput 0.677902
Reading from 3237: heap size 735 MB, throughput 0.537211
Reading from 3237: heap size 746 MB, throughput 0.721671
Reading from 3238: heap size 120 MB, throughput 0.997251
Reading from 3237: heap size 747 MB, throughput 0.403243
Equal recommendation: 1223 MB each
Reading from 3238: heap size 122 MB, throughput 0.997663
Reading from 3238: heap size 122 MB, throughput 0.997975
Reading from 3237: heap size 740 MB, throughput 0.0400529
Reading from 3237: heap size 727 MB, throughput 0.365568
Reading from 3237: heap size 807 MB, throughput 0.546707
Reading from 3238: heap size 124 MB, throughput 0.998116
Reading from 3237: heap size 810 MB, throughput 0.881188
Reading from 3237: heap size 807 MB, throughput 0.647502
Reading from 3238: heap size 124 MB, throughput 0.996645
Reading from 3238: heap size 125 MB, throughput 0.998061
Reading from 3237: heap size 811 MB, throughput 0.0717811
Reading from 3238: heap size 126 MB, throughput 0.992895
Reading from 3237: heap size 888 MB, throughput 0.523335
Reading from 3238: heap size 127 MB, throughput 0.993598
Reading from 3237: heap size 889 MB, throughput 0.793996
Reading from 3237: heap size 896 MB, throughput 0.648923
Reading from 3238: heap size 128 MB, throughput 0.982928
Reading from 3237: heap size 897 MB, throughput 0.759019
Reading from 3238: heap size 132 MB, throughput 0.993867
Reading from 3237: heap size 902 MB, throughput 0.255796
Reading from 3237: heap size 995 MB, throughput 0.571628
Reading from 3238: heap size 132 MB, throughput 0.99679
Reading from 3237: heap size 1002 MB, throughput 0.942311
Reading from 3237: heap size 820 MB, throughput 0.829849
Reading from 3237: heap size 986 MB, throughput 0.790219
Reading from 3237: heap size 836 MB, throughput 0.812074
Reading from 3238: heap size 137 MB, throughput 0.997463
Reading from 3237: heap size 975 MB, throughput 0.824461
Reading from 3237: heap size 842 MB, throughput 0.797698
Reading from 3237: heap size 966 MB, throughput 0.807737
Reading from 3237: heap size 843 MB, throughput 0.846155
Reading from 3237: heap size 960 MB, throughput 0.857002
Reading from 3238: heap size 137 MB, throughput 0.996729
Reading from 3237: heap size 966 MB, throughput 0.853118
Reading from 3237: heap size 953 MB, throughput 0.854895
Reading from 3238: heap size 141 MB, throughput 0.997274
Reading from 3237: heap size 960 MB, throughput 0.983758
Reading from 3238: heap size 141 MB, throughput 0.996963
Equal recommendation: 1223 MB each
Reading from 3237: heap size 954 MB, throughput 0.952271
Reading from 3237: heap size 957 MB, throughput 0.799912
Reading from 3238: heap size 144 MB, throughput 0.997388
Reading from 3237: heap size 953 MB, throughput 0.821428
Reading from 3237: heap size 957 MB, throughput 0.82337
Reading from 3237: heap size 951 MB, throughput 0.81365
Reading from 3237: heap size 955 MB, throughput 0.828265
Reading from 3238: heap size 144 MB, throughput 0.996743
Reading from 3237: heap size 951 MB, throughput 0.829851
Reading from 3237: heap size 955 MB, throughput 0.833735
Reading from 3237: heap size 953 MB, throughput 0.847673
Reading from 3238: heap size 147 MB, throughput 0.996361
Reading from 3237: heap size 957 MB, throughput 0.891401
Reading from 3237: heap size 956 MB, throughput 0.924294
Reading from 3237: heap size 959 MB, throughput 0.828593
Reading from 3238: heap size 147 MB, throughput 0.997
Reading from 3237: heap size 960 MB, throughput 0.725581
Reading from 3237: heap size 966 MB, throughput 0.681335
Reading from 3237: heap size 993 MB, throughput 0.712486
Reading from 3238: heap size 150 MB, throughput 0.996768
Reading from 3238: heap size 150 MB, throughput 0.992309
Reading from 3237: heap size 995 MB, throughput 0.0762735
Reading from 3237: heap size 1102 MB, throughput 0.581176
Reading from 3238: heap size 153 MB, throughput 0.997349
Reading from 3237: heap size 1106 MB, throughput 0.681314
Reading from 3237: heap size 1113 MB, throughput 0.722101
Reading from 3237: heap size 1115 MB, throughput 0.665503
Reading from 3237: heap size 1121 MB, throughput 0.734772
Reading from 3238: heap size 153 MB, throughput 0.99757
Reading from 3237: heap size 1124 MB, throughput 0.718853
Reading from 3238: heap size 155 MB, throughput 0.998072
Reading from 3238: heap size 156 MB, throughput 0.997796
Reading from 3237: heap size 1137 MB, throughput 0.957988
Equal recommendation: 1223 MB each
Reading from 3238: heap size 158 MB, throughput 0.99876
Reading from 3238: heap size 158 MB, throughput 0.998282
Reading from 3237: heap size 1138 MB, throughput 0.951745
Reading from 3238: heap size 160 MB, throughput 0.998075
Reading from 3238: heap size 160 MB, throughput 0.992455
Reading from 3238: heap size 161 MB, throughput 0.992267
Reading from 3237: heap size 1152 MB, throughput 0.960758
Reading from 3238: heap size 162 MB, throughput 0.996835
Reading from 3238: heap size 166 MB, throughput 0.997844
Reading from 3237: heap size 1153 MB, throughput 0.956473
Reading from 3238: heap size 166 MB, throughput 0.997681
Reading from 3238: heap size 169 MB, throughput 0.997718
Reading from 3237: heap size 1165 MB, throughput 0.947268
Reading from 3238: heap size 169 MB, throughput 0.997366
Reading from 3238: heap size 171 MB, throughput 0.997783
Reading from 3237: heap size 1171 MB, throughput 0.95914
Reading from 3238: heap size 172 MB, throughput 0.997137
Equal recommendation: 1223 MB each
Reading from 3238: heap size 175 MB, throughput 0.997655
Reading from 3237: heap size 1176 MB, throughput 0.967615
Reading from 3238: heap size 175 MB, throughput 0.997155
Reading from 3238: heap size 178 MB, throughput 0.99779
Reading from 3237: heap size 1177 MB, throughput 0.966202
Reading from 3238: heap size 178 MB, throughput 0.997501
Reading from 3238: heap size 180 MB, throughput 0.997359
Reading from 3237: heap size 1177 MB, throughput 0.966148
Reading from 3238: heap size 180 MB, throughput 0.997233
Reading from 3238: heap size 182 MB, throughput 0.995763
Reading from 3237: heap size 1181 MB, throughput 0.957943
Reading from 3238: heap size 182 MB, throughput 0.93174
Reading from 3238: heap size 189 MB, throughput 0.996997
Reading from 3237: heap size 1182 MB, throughput 0.940343
Reading from 3238: heap size 189 MB, throughput 0.992448
Equal recommendation: 1223 MB each
Reading from 3238: heap size 192 MB, throughput 0.996098
Reading from 3238: heap size 193 MB, throughput 0.995346
Reading from 3237: heap size 1184 MB, throughput 0.963225
Reading from 3238: heap size 199 MB, throughput 0.99785
Reading from 3237: heap size 1191 MB, throughput 0.962976
Reading from 3238: heap size 199 MB, throughput 0.997463
Reading from 3238: heap size 202 MB, throughput 0.998057
Reading from 3238: heap size 203 MB, throughput 0.996107
Reading from 3237: heap size 1192 MB, throughput 0.575953
Reading from 3238: heap size 208 MB, throughput 0.998064
Reading from 3238: heap size 208 MB, throughput 0.997363
Equal recommendation: 1223 MB each
Reading from 3237: heap size 1267 MB, throughput 0.992896
Reading from 3238: heap size 212 MB, throughput 0.997817
Reading from 3238: heap size 213 MB, throughput 0.997409
Reading from 3237: heap size 1268 MB, throughput 0.989594
Reading from 3238: heap size 217 MB, throughput 0.99787
Reading from 3238: heap size 217 MB, throughput 0.996992
Reading from 3237: heap size 1273 MB, throughput 0.987431
Reading from 3238: heap size 221 MB, throughput 0.997825
Reading from 3238: heap size 221 MB, throughput 0.993578
Reading from 3237: heap size 1162 MB, throughput 0.977997
Reading from 3238: heap size 225 MB, throughput 0.994161
Reading from 3238: heap size 226 MB, throughput 0.997934
Reading from 3237: heap size 1278 MB, throughput 0.981738
Reading from 3238: heap size 231 MB, throughput 0.99817
Equal recommendation: 1223 MB each
Reading from 3237: heap size 1176 MB, throughput 0.97838
Reading from 3238: heap size 232 MB, throughput 0.99775
Reading from 3238: heap size 236 MB, throughput 0.998209
Reading from 3237: heap size 1269 MB, throughput 0.97803
Reading from 3238: heap size 237 MB, throughput 0.998001
Reading from 3237: heap size 1193 MB, throughput 0.974697
Reading from 3238: heap size 242 MB, throughput 0.998044
Reading from 3238: heap size 242 MB, throughput 0.997711
Reading from 3237: heap size 1260 MB, throughput 0.972136
Reading from 3238: heap size 247 MB, throughput 0.998185
Reading from 3237: heap size 1210 MB, throughput 0.969865
Equal recommendation: 1223 MB each
Reading from 3238: heap size 247 MB, throughput 0.997589
Reading from 3238: heap size 251 MB, throughput 0.998106
Reading from 3237: heap size 1254 MB, throughput 0.970571
Reading from 3238: heap size 251 MB, throughput 0.997312
Reading from 3238: heap size 255 MB, throughput 0.992223
Reading from 3237: heap size 1252 MB, throughput 0.966049
Reading from 3238: heap size 255 MB, throughput 0.997276
Reading from 3238: heap size 262 MB, throughput 0.997843
Reading from 3237: heap size 1250 MB, throughput 0.965351
Reading from 3238: heap size 263 MB, throughput 0.997898
Reading from 3237: heap size 1252 MB, throughput 0.958779
Reading from 3238: heap size 267 MB, throughput 0.998172
Equal recommendation: 1223 MB each
Reading from 3237: heap size 1251 MB, throughput 0.957131
Reading from 3238: heap size 268 MB, throughput 0.997904
Reading from 3237: heap size 1256 MB, throughput 0.95854
Reading from 3238: heap size 274 MB, throughput 0.998313
Reading from 3238: heap size 274 MB, throughput 0.998038
Reading from 3237: heap size 1259 MB, throughput 0.956005
Reading from 3238: heap size 279 MB, throughput 0.998172
Reading from 3237: heap size 1262 MB, throughput 0.956734
Reading from 3238: heap size 279 MB, throughput 0.997828
Reading from 3237: heap size 1264 MB, throughput 0.956351
Reading from 3238: heap size 284 MB, throughput 0.997036
Equal recommendation: 1223 MB each
Reading from 3238: heap size 284 MB, throughput 0.993712
Reading from 3237: heap size 1214 MB, throughput 0.958882
Reading from 3238: heap size 289 MB, throughput 0.998097
Reading from 3237: heap size 1265 MB, throughput 0.957758
Reading from 3238: heap size 289 MB, throughput 0.997735
Reading from 3237: heap size 1210 MB, throughput 0.956293
Reading from 3238: heap size 295 MB, throughput 0.998087
Reading from 3238: heap size 296 MB, throughput 0.997789
Reading from 3237: heap size 1264 MB, throughput 0.956255
Reading from 3238: heap size 302 MB, throughput 0.998255
Equal recommendation: 1223 MB each
Reading from 3237: heap size 1207 MB, throughput 0.958635
Reading from 3238: heap size 302 MB, throughput 0.998068
Reading from 3237: heap size 1260 MB, throughput 0.958303
Reading from 3238: heap size 307 MB, throughput 0.998406
Reading from 3237: heap size 1205 MB, throughput 0.961493
Reading from 3238: heap size 307 MB, throughput 0.99807
Reading from 3238: heap size 312 MB, throughput 0.996804
Reading from 3237: heap size 1255 MB, throughput 0.948247
Reading from 3238: heap size 312 MB, throughput 0.996187
Reading from 3238: heap size 318 MB, throughput 0.997972
Equal recommendation: 1223 MB each
Reading from 3237: heap size 1204 MB, throughput 0.504664
Reading from 3238: heap size 318 MB, throughput 0.998236
Reading from 3237: heap size 1370 MB, throughput 0.947133
Reading from 3238: heap size 323 MB, throughput 0.998407
Reading from 3237: heap size 1364 MB, throughput 0.983619
Reading from 3238: heap size 324 MB, throughput 0.998011
Reading from 3237: heap size 1368 MB, throughput 0.980926
Reading from 3238: heap size 329 MB, throughput 0.998334
Reading from 3238: heap size 329 MB, throughput 0.998263
Equal recommendation: 1223 MB each
Reading from 3238: heap size 335 MB, throughput 0.998471
Reading from 3238: heap size 335 MB, throughput 0.995884
Reading from 3238: heap size 339 MB, throughput 0.997492
Reading from 3238: heap size 340 MB, throughput 0.998103
Reading from 3238: heap size 346 MB, throughput 0.998298
Equal recommendation: 1223 MB each
Reading from 3238: heap size 346 MB, throughput 0.996621
Reading from 3237: heap size 1251 MB, throughput 0.991604
Reading from 3238: heap size 352 MB, throughput 0.998286
Reading from 3238: heap size 352 MB, throughput 0.997938
Reading from 3238: heap size 358 MB, throughput 0.998383
Reading from 3238: heap size 358 MB, throughput 0.997118
Equal recommendation: 1223 MB each
Reading from 3238: heap size 364 MB, throughput 0.997207
Reading from 3237: heap size 1363 MB, throughput 0.991527
Reading from 3238: heap size 364 MB, throughput 0.998119
Reading from 3238: heap size 370 MB, throughput 0.998313
Reading from 3237: heap size 1371 MB, throughput 0.984838
Reading from 3238: heap size 371 MB, throughput 0.998001
Reading from 3237: heap size 1375 MB, throughput 0.834029
Reading from 3237: heap size 1389 MB, throughput 0.699901
Reading from 3237: heap size 1362 MB, throughput 0.606666
Reading from 3237: heap size 1398 MB, throughput 0.610314
Reading from 3237: heap size 1415 MB, throughput 0.617447
Reading from 3237: heap size 1357 MB, throughput 0.626848
Reading from 3237: heap size 1413 MB, throughput 0.631764
Equal recommendation: 1223 MB each
Reading from 3238: heap size 377 MB, throughput 0.998352
Reading from 3237: heap size 1338 MB, throughput 0.141247
Reading from 3237: heap size 1423 MB, throughput 0.989911
Reading from 3237: heap size 1152 MB, throughput 0.984525
Reading from 3237: heap size 1423 MB, throughput 0.98866
Reading from 3237: heap size 1151 MB, throughput 0.975539
Reading from 3237: heap size 1422 MB, throughput 0.990842
Reading from 3238: heap size 377 MB, throughput 0.997985
Reading from 3237: heap size 1153 MB, throughput 0.991408
Reading from 3237: heap size 1421 MB, throughput 0.988692
Reading from 3238: heap size 382 MB, throughput 0.998281
Reading from 3237: heap size 1159 MB, throughput 0.98641
Reading from 3238: heap size 383 MB, throughput 0.922001
Reading from 3237: heap size 1420 MB, throughput 0.988497
Reading from 3238: heap size 394 MB, throughput 0.997859
Reading from 3237: heap size 1153 MB, throughput 0.988987
Reading from 3237: heap size 1408 MB, throughput 0.982329
Equal recommendation: 1223 MB each
Reading from 3238: heap size 394 MB, throughput 0.998951
Reading from 3237: heap size 1162 MB, throughput 0.98632
Reading from 3238: heap size 402 MB, throughput 0.998965
Reading from 3237: heap size 1382 MB, throughput 0.98394
Reading from 3237: heap size 1175 MB, throughput 0.983152
Reading from 3238: heap size 403 MB, throughput 0.998815
Reading from 3237: heap size 1356 MB, throughput 0.980396
Reading from 3237: heap size 1188 MB, throughput 0.971701
Reading from 3238: heap size 409 MB, throughput 0.99898
Reading from 3237: heap size 1332 MB, throughput 0.975599
Equal recommendation: 1223 MB each
Reading from 3237: heap size 1201 MB, throughput 0.974702
Reading from 3238: heap size 410 MB, throughput 0.998769
Reading from 3237: heap size 1313 MB, throughput 0.972717
Reading from 3238: heap size 416 MB, throughput 0.998289
Reading from 3237: heap size 1214 MB, throughput 0.972538
Reading from 3238: heap size 416 MB, throughput 0.996236
Reading from 3237: heap size 1308 MB, throughput 0.971991
Reading from 3237: heap size 1227 MB, throughput 0.969004
Reading from 3238: heap size 422 MB, throughput 0.99856
Reading from 3237: heap size 1298 MB, throughput 0.964609
Reading from 3238: heap size 423 MB, throughput 0.998352
Equal recommendation: 1223 MB each
Reading from 3237: heap size 1235 MB, throughput 0.962678
Reading from 3237: heap size 1299 MB, throughput 0.961699
Reading from 3238: heap size 432 MB, throughput 0.998474
Reading from 3237: heap size 1239 MB, throughput 0.956744
Reading from 3237: heap size 1296 MB, throughput 0.962279
Reading from 3238: heap size 432 MB, throughput 0.998233
Reading from 3237: heap size 1237 MB, throughput 0.960752
Reading from 3238: heap size 440 MB, throughput 0.998391
Reading from 3237: heap size 1291 MB, throughput 0.961029
Reading from 3238: heap size 440 MB, throughput 0.996708
Reading from 3237: heap size 1236 MB, throughput 0.954103
Equal recommendation: 1223 MB each
Reading from 3237: heap size 1286 MB, throughput 0.961431
Reading from 3238: heap size 448 MB, throughput 0.997697
Reading from 3237: heap size 1234 MB, throughput 0.953902
Reading from 3237: heap size 1280 MB, throughput 0.957747
Reading from 3238: heap size 448 MB, throughput 0.998489
Reading from 3237: heap size 1232 MB, throughput 0.957391
Reading from 3238: heap size 457 MB, throughput 0.998706
Reading from 3237: heap size 1274 MB, throughput 0.956013
Reading from 3237: heap size 1230 MB, throughput 0.956816
Equal recommendation: 1223 MB each
Reading from 3238: heap size 458 MB, throughput 0.998358
Reading from 3237: heap size 1266 MB, throughput 0.960485
Reading from 3237: heap size 1228 MB, throughput 0.958383
Reading from 3238: heap size 465 MB, throughput 0.998473
Reading from 3237: heap size 1260 MB, throughput 0.957727
Reading from 3237: heap size 1225 MB, throughput 0.954825
Reading from 3238: heap size 466 MB, throughput 0.998088
Reading from 3237: heap size 1252 MB, throughput 0.957069
Reading from 3238: heap size 473 MB, throughput 0.996988
Reading from 3237: heap size 1222 MB, throughput 0.954545
Reading from 3237: heap size 1255 MB, throughput 0.95911
Equal recommendation: 1223 MB each
Reading from 3238: heap size 474 MB, throughput 0.998239
Reading from 3237: heap size 1223 MB, throughput 0.953694
Reading from 3237: heap size 1251 MB, throughput 0.952798
Reading from 3238: heap size 484 MB, throughput 0.998583
Reading from 3237: heap size 1223 MB, throughput 0.954221
Reading from 3237: heap size 1239 MB, throughput 0.958619
Reading from 3238: heap size 484 MB, throughput 0.998476
Reading from 3237: heap size 1220 MB, throughput 0.957614
Reading from 3237: heap size 1241 MB, throughput 0.95805
Reading from 3238: heap size 493 MB, throughput 0.998549
Equal recommendation: 1223 MB each
Reading from 3237: heap size 1221 MB, throughput 0.952894
Reading from 3238: heap size 493 MB, throughput 0.997725
Reading from 3237: heap size 1238 MB, throughput 0.936501
Reading from 3238: heap size 502 MB, throughput 0.997369
Reading from 3237: heap size 1222 MB, throughput 0.571532
Reading from 3237: heap size 1223 MB, throughput 0.989105
Reading from 3238: heap size 502 MB, throughput 0.998352
Reading from 3237: heap size 1221 MB, throughput 0.98926
Reading from 3237: heap size 1225 MB, throughput 0.986875
Equal recommendation: 1223 MB each
Reading from 3237: heap size 1230 MB, throughput 0.983046
Reading from 3238: heap size 511 MB, throughput 0.998328
Reading from 3237: heap size 1232 MB, throughput 0.97941
Reading from 3237: heap size 1154 MB, throughput 0.97994
Reading from 3238: heap size 512 MB, throughput 0.998415
Reading from 3237: heap size 1234 MB, throughput 0.975858
Reading from 3237: heap size 1164 MB, throughput 0.974077
Reading from 3238: heap size 521 MB, throughput 0.998553
Reading from 3237: heap size 1229 MB, throughput 0.973439
Reading from 3237: heap size 1176 MB, throughput 0.97263
Equal recommendation: 1223 MB each
Client 3238 died
Clients: 1
Reading from 3237: heap size 1224 MB, throughput 0.969836
Reading from 3237: heap size 1188 MB, throughput 0.968493
Reading from 3237: heap size 1220 MB, throughput 0.966832
Reading from 3237: heap size 1220 MB, throughput 0.963944
Reading from 3237: heap size 1225 MB, throughput 0.963352
Reading from 3237: heap size 1227 MB, throughput 0.96219
Reading from 3237: heap size 1227 MB, throughput 0.961292
Recommendation: one client; give it all the memory
Reading from 3237: heap size 1229 MB, throughput 0.961319
Reading from 3237: heap size 1230 MB, throughput 0.960346
Reading from 3237: heap size 1231 MB, throughput 0.960151
Reading from 3237: heap size 1239 MB, throughput 0.961235
Reading from 3237: heap size 1239 MB, throughput 0.961291
Reading from 3237: heap size 1247 MB, throughput 0.962241
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 3237: heap size 1247 MB, throughput 0.820402
Recommendation: one client; give it all the memory
Reading from 3237: heap size 1389 MB, throughput 0.988731
Reading from 3237: heap size 1430 MB, throughput 0.967355
Reading from 3237: heap size 1437 MB, throughput 0.98999
Reading from 3237: heap size 1438 MB, throughput 0.833977
Reading from 3237: heap size 1438 MB, throughput 0.862843
Reading from 3237: heap size 1443 MB, throughput 0.757271
Reading from 3237: heap size 1434 MB, throughput 0.687283
Reading from 3237: heap size 1453 MB, throughput 0.609318
Reading from 3237: heap size 1475 MB, throughput 0.657017
Reading from 3237: heap size 1492 MB, throughput 0.633268
Reading from 3237: heap size 1516 MB, throughput 0.670421
Reading from 3237: heap size 1526 MB, throughput 0.65381
Reading from 3237: heap size 1539 MB, throughput 0.841674
Recommendation: one client; give it all the memory
Reading from 3237: heap size 1544 MB, throughput 0.155074
Reading from 3237: heap size 1446 MB, throughput 0.992459
Reading from 3237: heap size 1454 MB, throughput 0.989517
Client 3237 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
