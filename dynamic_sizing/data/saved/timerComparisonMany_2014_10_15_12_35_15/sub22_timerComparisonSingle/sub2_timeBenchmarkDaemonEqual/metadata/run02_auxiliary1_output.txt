economemd
    total memory: 3759 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub22_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 17654: heap size 9 MB, throughput 0.992642
Clients: 1
Client 17654 has a minimum heap size of 30 MB
Reading from 17653: heap size 9 MB, throughput 0.987423
Clients: 2
Client 17653 has a minimum heap size of 1223 MB
Reading from 17654: heap size 9 MB, throughput 0.972279
Reading from 17654: heap size 9 MB, throughput 0.960182
Reading from 17653: heap size 9 MB, throughput 0.972392
Reading from 17654: heap size 9 MB, throughput 0.961885
Reading from 17654: heap size 11 MB, throughput 0.683176
Reading from 17654: heap size 11 MB, throughput 0.369407
Reading from 17654: heap size 16 MB, throughput 0.708392
Reading from 17654: heap size 16 MB, throughput 0.726108
Reading from 17654: heap size 24 MB, throughput 0.883627
Reading from 17654: heap size 24 MB, throughput 0.812464
Reading from 17654: heap size 34 MB, throughput 0.900385
Reading from 17654: heap size 34 MB, throughput 0.886337
Reading from 17654: heap size 50 MB, throughput 0.937079
Reading from 17654: heap size 50 MB, throughput 0.940356
Reading from 17653: heap size 11 MB, throughput 0.975379
Reading from 17654: heap size 75 MB, throughput 0.936885
Reading from 17654: heap size 75 MB, throughput 0.955103
Reading from 17654: heap size 116 MB, throughput 0.963549
Reading from 17654: heap size 116 MB, throughput 0.94446
Reading from 17653: heap size 11 MB, throughput 0.962688
Reading from 17653: heap size 15 MB, throughput 0.785923
Reading from 17653: heap size 19 MB, throughput 0.961345
Reading from 17653: heap size 24 MB, throughput 0.875805
Reading from 17653: heap size 29 MB, throughput 0.917328
Reading from 17653: heap size 32 MB, throughput 0.58225
Reading from 17653: heap size 42 MB, throughput 0.922498
Reading from 17654: heap size 160 MB, throughput 0.958417
Reading from 17653: heap size 48 MB, throughput 0.775158
Reading from 17653: heap size 49 MB, throughput 0.278981
Reading from 17653: heap size 67 MB, throughput 0.75123
Reading from 17653: heap size 70 MB, throughput 0.693625
Reading from 17654: heap size 165 MB, throughput 0.828614
Reading from 17653: heap size 74 MB, throughput 0.202978
Reading from 17653: heap size 94 MB, throughput 0.583619
Reading from 17653: heap size 104 MB, throughput 0.830925
Reading from 17654: heap size 222 MB, throughput 0.983669
Reading from 17653: heap size 106 MB, throughput 0.327459
Reading from 17653: heap size 137 MB, throughput 0.681901
Reading from 17653: heap size 139 MB, throughput 0.75312
Reading from 17653: heap size 143 MB, throughput 0.191091
Reading from 17654: heap size 240 MB, throughput 0.993104
Reading from 17653: heap size 180 MB, throughput 0.756627
Reading from 17653: heap size 187 MB, throughput 0.714156
Reading from 17653: heap size 189 MB, throughput 0.665267
Reading from 17653: heap size 194 MB, throughput 0.722379
Reading from 17653: heap size 201 MB, throughput 0.636295
Reading from 17653: heap size 209 MB, throughput 0.609993
Reading from 17653: heap size 216 MB, throughput 0.713165
Reading from 17653: heap size 222 MB, throughput 0.646454
Reading from 17654: heap size 250 MB, throughput 0.979488
Reading from 17653: heap size 231 MB, throughput 0.105556
Reading from 17653: heap size 272 MB, throughput 0.605306
Reading from 17653: heap size 280 MB, throughput 0.572466
Reading from 17654: heap size 252 MB, throughput 0.82467
Reading from 17653: heap size 283 MB, throughput 0.557237
Reading from 17653: heap size 288 MB, throughput 0.0953807
Reading from 17654: heap size 280 MB, throughput 0.968713
Reading from 17653: heap size 334 MB, throughput 0.526254
Reading from 17653: heap size 336 MB, throughput 0.664121
Reading from 17653: heap size 336 MB, throughput 0.62353
Reading from 17653: heap size 338 MB, throughput 0.609648
Reading from 17653: heap size 341 MB, throughput 0.558756
Reading from 17654: heap size 285 MB, throughput 0.989487
Reading from 17653: heap size 348 MB, throughput 0.114984
Reading from 17653: heap size 401 MB, throughput 0.514703
Reading from 17653: heap size 405 MB, throughput 0.4926
Reading from 17653: heap size 407 MB, throughput 0.621064
Reading from 17653: heap size 410 MB, throughput 0.468521
Reading from 17654: heap size 301 MB, throughput 0.985414
Equal recommendation: 1879 MB each
Reading from 17654: heap size 304 MB, throughput 0.972876
Reading from 17653: heap size 416 MB, throughput 0.135511
Reading from 17653: heap size 470 MB, throughput 0.435085
Reading from 17653: heap size 471 MB, throughput 0.561006
Reading from 17653: heap size 475 MB, throughput 0.553575
Reading from 17653: heap size 480 MB, throughput 0.55115
Reading from 17654: heap size 323 MB, throughput 0.988955
Reading from 17653: heap size 484 MB, throughput 0.50557
Reading from 17653: heap size 491 MB, throughput 0.122334
Reading from 17654: heap size 327 MB, throughput 0.98435
Reading from 17653: heap size 547 MB, throughput 0.41634
Reading from 17653: heap size 559 MB, throughput 0.55177
Reading from 17653: heap size 560 MB, throughput 0.591184
Reading from 17653: heap size 566 MB, throughput 0.643294
Reading from 17654: heap size 346 MB, throughput 0.979065
Reading from 17654: heap size 348 MB, throughput 0.98437
Reading from 17653: heap size 571 MB, throughput 0.129519
Reading from 17653: heap size 636 MB, throughput 0.478025
Reading from 17653: heap size 643 MB, throughput 0.524562
Reading from 17653: heap size 646 MB, throughput 0.412571
Reading from 17653: heap size 653 MB, throughput 0.501139
Reading from 17654: heap size 370 MB, throughput 0.975062
Reading from 17654: heap size 371 MB, throughput 0.980855
Reading from 17653: heap size 662 MB, throughput 0.306673
Reading from 17654: heap size 392 MB, throughput 0.987433
Reading from 17653: heap size 735 MB, throughput 0.780035
Reading from 17654: heap size 393 MB, throughput 0.980879
Equal recommendation: 1879 MB each
Reading from 17653: heap size 740 MB, throughput 0.792778
Reading from 17653: heap size 750 MB, throughput 0.545876
Reading from 17653: heap size 756 MB, throughput 0.4267
Reading from 17654: heap size 408 MB, throughput 0.987062
Reading from 17653: heap size 767 MB, throughput 0.286638
Reading from 17653: heap size 779 MB, throughput 0.331589
Reading from 17654: heap size 409 MB, throughput 0.999999
Reading from 17654: heap size 409 MB, throughput 1
Reading from 17654: heap size 409 MB, throughput 0.15034
Reading from 17653: heap size 781 MB, throughput 0.030331
Reading from 17654: heap size 422 MB, throughput 0.978316
Reading from 17653: heap size 852 MB, throughput 0.751217
Reading from 17653: heap size 857 MB, throughput 0.58762
Reading from 17653: heap size 858 MB, throughput 0.0907336
Reading from 17654: heap size 426 MB, throughput 0.988538
Reading from 17653: heap size 948 MB, throughput 0.731266
Reading from 17653: heap size 953 MB, throughput 0.820703
Reading from 17653: heap size 956 MB, throughput 0.852794
Reading from 17654: heap size 438 MB, throughput 0.99178
Reading from 17653: heap size 960 MB, throughput 0.894175
Reading from 17653: heap size 962 MB, throughput 0.839033
Reading from 17653: heap size 960 MB, throughput 0.834135
Reading from 17653: heap size 962 MB, throughput 0.771815
Reading from 17654: heap size 441 MB, throughput 0.994737
Reading from 17653: heap size 951 MB, throughput 0.878741
Reading from 17653: heap size 957 MB, throughput 0.862746
Reading from 17653: heap size 942 MB, throughput 0.829043
Reading from 17653: heap size 950 MB, throughput 0.86774
Reading from 17654: heap size 450 MB, throughput 0.992264
Reading from 17653: heap size 935 MB, throughput 0.88255
Reading from 17653: heap size 806 MB, throughput 0.878086
Reading from 17653: heap size 929 MB, throughput 0.877488
Equal recommendation: 1879 MB each
Reading from 17653: heap size 935 MB, throughput 0.963767
Reading from 17654: heap size 453 MB, throughput 0.987541
Reading from 17654: heap size 462 MB, throughput 0.990566
Reading from 17653: heap size 931 MB, throughput 0.964963
Reading from 17653: heap size 839 MB, throughput 0.777629
Reading from 17653: heap size 922 MB, throughput 0.810324
Reading from 17654: heap size 463 MB, throughput 0.985092
Reading from 17653: heap size 842 MB, throughput 0.125996
Reading from 17653: heap size 998 MB, throughput 0.627802
Reading from 17654: heap size 473 MB, throughput 0.993336
Reading from 17653: heap size 1005 MB, throughput 0.768458
Reading from 17653: heap size 1003 MB, throughput 0.796508
Reading from 17653: heap size 1006 MB, throughput 0.812766
Reading from 17653: heap size 1004 MB, throughput 0.857192
Reading from 17654: heap size 474 MB, throughput 0.988809
Reading from 17653: heap size 1008 MB, throughput 0.79695
Reading from 17653: heap size 1012 MB, throughput 0.688088
Reading from 17653: heap size 1015 MB, throughput 0.652162
Reading from 17653: heap size 1006 MB, throughput 0.667431
Reading from 17653: heap size 1030 MB, throughput 0.602162
Reading from 17654: heap size 483 MB, throughput 0.992946
Reading from 17653: heap size 1046 MB, throughput 0.627327
Reading from 17653: heap size 1056 MB, throughput 0.0936378
Reading from 17654: heap size 484 MB, throughput 0.990242
Reading from 17653: heap size 1171 MB, throughput 0.416436
Reading from 17653: heap size 1177 MB, throughput 0.714789
Reading from 17653: heap size 1194 MB, throughput 0.732618
Equal recommendation: 1879 MB each
Reading from 17653: heap size 1195 MB, throughput 0.777519
Reading from 17653: heap size 1202 MB, throughput 0.716835
Reading from 17654: heap size 494 MB, throughput 0.973307
Reading from 17654: heap size 494 MB, throughput 0.987937
Reading from 17653: heap size 1207 MB, throughput 0.977944
Reading from 17654: heap size 509 MB, throughput 0.99062
Reading from 17654: heap size 510 MB, throughput 0.983035
Reading from 17653: heap size 1204 MB, throughput 0.97504
Reading from 17654: heap size 522 MB, throughput 0.996511
Reading from 17654: heap size 524 MB, throughput 0.991519
Reading from 17653: heap size 1212 MB, throughput 0.978507
Equal recommendation: 1879 MB each
Reading from 17654: heap size 535 MB, throughput 0.99132
Reading from 17654: heap size 537 MB, throughput 0.993795
Reading from 17653: heap size 1201 MB, throughput 0.974206
Reading from 17654: heap size 546 MB, throughput 0.986602
Reading from 17654: heap size 547 MB, throughput 0.992127
Reading from 17653: heap size 1211 MB, throughput 0.966585
Reading from 17654: heap size 562 MB, throughput 0.992185
Reading from 17654: heap size 563 MB, throughput 0.993884
Reading from 17653: heap size 1206 MB, throughput 0.974379
Equal recommendation: 1879 MB each
Reading from 17654: heap size 573 MB, throughput 0.993929
Reading from 17654: heap size 575 MB, throughput 0.92886
Reading from 17653: heap size 1213 MB, throughput 0.9709
Reading from 17654: heap size 596 MB, throughput 0.995679
Reading from 17654: heap size 601 MB, throughput 0.988066
Reading from 17653: heap size 1208 MB, throughput 0.971946
Reading from 17654: heap size 616 MB, throughput 0.985513
Reading from 17653: heap size 1214 MB, throughput 0.973006
Equal recommendation: 1879 MB each
Reading from 17654: heap size 616 MB, throughput 0.989055
Reading from 17654: heap size 636 MB, throughput 0.99473
Reading from 17653: heap size 1208 MB, throughput 0.968243
Reading from 17654: heap size 637 MB, throughput 0.985238
Reading from 17654: heap size 651 MB, throughput 0.988973
Reading from 17653: heap size 1212 MB, throughput 0.972557
Reading from 17654: heap size 653 MB, throughput 0.989324
Reading from 17654: heap size 670 MB, throughput 0.988958
Equal recommendation: 1879 MB each
Reading from 17653: heap size 1217 MB, throughput 0.968724
Reading from 17654: heap size 671 MB, throughput 0.98667
Reading from 17654: heap size 693 MB, throughput 0.986643
Reading from 17653: heap size 1218 MB, throughput 0.949968
Reading from 17654: heap size 695 MB, throughput 0.994454
Reading from 17653: heap size 1224 MB, throughput 0.976097
Reading from 17654: heap size 710 MB, throughput 0.989753
Equal recommendation: 1879 MB each
Reading from 17654: heap size 714 MB, throughput 0.987353
Reading from 17653: heap size 1227 MB, throughput 0.968845
Reading from 17654: heap size 736 MB, throughput 0.989786
Reading from 17654: heap size 737 MB, throughput 0.991075
Reading from 17653: heap size 1232 MB, throughput 0.969036
Reading from 17654: heap size 759 MB, throughput 0.995937
Reading from 17653: heap size 1237 MB, throughput 0.973792
Reading from 17654: heap size 760 MB, throughput 0.990436
Equal recommendation: 1879 MB each
Reading from 17654: heap size 779 MB, throughput 0.987979
Reading from 17653: heap size 1243 MB, throughput 0.972595
Reading from 17654: heap size 779 MB, throughput 0.988809
Reading from 17653: heap size 1247 MB, throughput 0.961879
Reading from 17654: heap size 801 MB, throughput 0.994565
Reading from 17654: heap size 801 MB, throughput 0.991823
Equal recommendation: 1879 MB each
Reading from 17653: heap size 1253 MB, throughput 0.667337
Reading from 17654: heap size 820 MB, throughput 0.993502
Reading from 17654: heap size 821 MB, throughput 0.992641
Reading from 17653: heap size 1320 MB, throughput 0.993253
Reading from 17654: heap size 840 MB, throughput 0.999999
Reading from 17654: heap size 840 MB, throughput 1
Reading from 17654: heap size 840 MB, throughput 0.0273829
Equal recommendation: 1879 MB each
Reading from 17653: heap size 1317 MB, throughput 0.991788
Reading from 17654: heap size 840 MB, throughput 0.990993
Reading from 17654: heap size 858 MB, throughput 0.990239
Reading from 17653: heap size 1327 MB, throughput 0.989124
Reading from 17654: heap size 858 MB, throughput 0.991001
Reading from 17653: heap size 1335 MB, throughput 0.987022
Reading from 17654: heap size 880 MB, throughput 0.992026
Equal recommendation: 1879 MB each
Reading from 17653: heap size 1336 MB, throughput 0.984831
Reading from 17654: heap size 880 MB, throughput 0.991092
Reading from 17654: heap size 902 MB, throughput 0.992253
Reading from 17653: heap size 1328 MB, throughput 0.984225
Reading from 17654: heap size 902 MB, throughput 0.991086
Reading from 17653: heap size 1223 MB, throughput 0.987316
Equal recommendation: 1879 MB each
Reading from 17654: heap size 925 MB, throughput 0.991869
Reading from 17653: heap size 1316 MB, throughput 0.98074
Reading from 17654: heap size 925 MB, throughput 0.988905
Reading from 17654: heap size 948 MB, throughput 0.992572
Reading from 17653: heap size 1244 MB, throughput 0.978406
Reading from 17654: heap size 948 MB, throughput 0.991655
Equal recommendation: 1879 MB each
Reading from 17653: heap size 1312 MB, throughput 0.977277
Reading from 17654: heap size 972 MB, throughput 0.991921
Reading from 17653: heap size 1316 MB, throughput 0.976831
Reading from 17654: heap size 973 MB, throughput 0.991087
Reading from 17653: heap size 1318 MB, throughput 0.971439
Reading from 17654: heap size 999 MB, throughput 0.991801
Equal recommendation: 1879 MB each
Reading from 17654: heap size 999 MB, throughput 0.992662
Reading from 17653: heap size 1320 MB, throughput 0.978331
Reading from 17654: heap size 1024 MB, throughput 0.992911
Reading from 17653: heap size 1323 MB, throughput 0.968494
Reading from 17654: heap size 1025 MB, throughput 0.988815
Equal recommendation: 1879 MB each
Reading from 17653: heap size 1329 MB, throughput 0.968241
Reading from 17654: heap size 1050 MB, throughput 0.991811
Reading from 17653: heap size 1333 MB, throughput 0.972306
Reading from 17654: heap size 1051 MB, throughput 0.992894
Reading from 17653: heap size 1342 MB, throughput 0.968316
Reading from 17654: heap size 1076 MB, throughput 0.99378
Equal recommendation: 1879 MB each
Reading from 17654: heap size 1077 MB, throughput 0.996304
Reading from 17653: heap size 1350 MB, throughput 0.968631
Reading from 17654: heap size 1099 MB, throughput 0.988803
Reading from 17653: heap size 1354 MB, throughput 0.96803
Reading from 17654: heap size 1101 MB, throughput 0.990067
Equal recommendation: 1879 MB each
Reading from 17653: heap size 1362 MB, throughput 0.970844
Reading from 17654: heap size 1132 MB, throughput 0.992964
Reading from 17653: heap size 1364 MB, throughput 0.967937
Reading from 17654: heap size 1133 MB, throughput 0.9905
Equal recommendation: 1879 MB each
Reading from 17654: heap size 1162 MB, throughput 0.990957
Reading from 17654: heap size 1163 MB, throughput 0.991123
Reading from 17654: heap size 1193 MB, throughput 0.989834
Equal recommendation: 1879 MB each
Reading from 17654: heap size 1194 MB, throughput 0.988222
Reading from 17653: heap size 1372 MB, throughput 0.988683
Reading from 17654: heap size 1230 MB, throughput 0.992601
Reading from 17654: heap size 1229 MB, throughput 0.990608
Equal recommendation: 1879 MB each
Reading from 17654: heap size 1263 MB, throughput 0.995186
Reading from 17654: heap size 1263 MB, throughput 0.955195
Reading from 17654: heap size 1273 MB, throughput 0.993035
Reading from 17653: heap size 1372 MB, throughput 0.930834
Equal recommendation: 1879 MB each
Reading from 17654: heap size 1277 MB, throughput 0.989121
Reading from 17653: heap size 1450 MB, throughput 0.995033
Reading from 17653: heap size 1470 MB, throughput 0.962551
Reading from 17654: heap size 1270 MB, throughput 0.99367
Reading from 17653: heap size 1480 MB, throughput 0.896928
Reading from 17653: heap size 1347 MB, throughput 0.866323
Reading from 17653: heap size 1464 MB, throughput 0.846693
Reading from 17653: heap size 1471 MB, throughput 0.829112
Reading from 17653: heap size 1467 MB, throughput 0.865351
Reading from 17653: heap size 1474 MB, throughput 0.982474
Reading from 17654: heap size 1274 MB, throughput 0.989781
Equal recommendation: 1879 MB each
Reading from 17653: heap size 1484 MB, throughput 0.991113
Reading from 17654: heap size 1271 MB, throughput 0.993029
Reading from 17654: heap size 1273 MB, throughput 0.990649
Reading from 17653: heap size 1491 MB, throughput 0.986758
Equal recommendation: 1879 MB each
Reading from 17654: heap size 1273 MB, throughput 0.990026
Reading from 17653: heap size 1503 MB, throughput 0.98535
Client 17654 died
Clients: 1
Reading from 17653: heap size 1508 MB, throughput 0.991452
Recommendation: one client; give it all the memory
Reading from 17653: heap size 1499 MB, throughput 0.990957
Reading from 17653: heap size 1508 MB, throughput 0.98927
Recommendation: one client; give it all the memory
Reading from 17653: heap size 1489 MB, throughput 0.989088
Reading from 17653: heap size 1500 MB, throughput 0.987838
Recommendation: one client; give it all the memory
Reading from 17653: heap size 1490 MB, throughput 0.98785
Reading from 17653: heap size 1497 MB, throughput 0.987288
Recommendation: one client; give it all the memory
Reading from 17653: heap size 1500 MB, throughput 0.98594
Recommendation: one client; give it all the memory
Reading from 17653: heap size 1503 MB, throughput 0.986604
Reading from 17653: heap size 1506 MB, throughput 0.984742
Recommendation: one client; give it all the memory
Reading from 17653: heap size 1514 MB, throughput 0.983232
Reading from 17653: heap size 1518 MB, throughput 0.982745
Recommendation: one client; give it all the memory
Reading from 17653: heap size 1530 MB, throughput 0.980196
Reading from 17653: heap size 1539 MB, throughput 0.981805
Recommendation: one client; give it all the memory
Reading from 17653: heap size 1548 MB, throughput 0.980667
Reading from 17653: heap size 1557 MB, throughput 0.981591
Recommendation: one client; give it all the memory
Reading from 17653: heap size 1563 MB, throughput 0.979607
Recommendation: one client; give it all the memory
Reading from 17653: heap size 1571 MB, throughput 0.980201
Reading from 17653: heap size 1574 MB, throughput 0.980086
Recommendation: one client; give it all the memory
Reading from 17653: heap size 1582 MB, throughput 0.98093
Reading from 17653: heap size 1584 MB, throughput 0.980532
Recommendation: one client; give it all the memory
Reading from 17653: heap size 1591 MB, throughput 0.833108
Recommendation: one client; give it all the memory
Reading from 17653: heap size 1699 MB, throughput 0.996113
Reading from 17653: heap size 1693 MB, throughput 0.994843
Recommendation: one client; give it all the memory
Reading from 17653: heap size 1706 MB, throughput 0.993637
Reading from 17653: heap size 1718 MB, throughput 0.991654
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 17653: heap size 1719 MB, throughput 0.997774
Recommendation: one client; give it all the memory
Reading from 17653: heap size 1697 MB, throughput 0.990531
Reading from 17653: heap size 1709 MB, throughput 0.877128
Reading from 17653: heap size 1689 MB, throughput 0.800381
Reading from 17653: heap size 1718 MB, throughput 0.77994
Reading from 17653: heap size 1751 MB, throughput 0.804172
Reading from 17653: heap size 1769 MB, throughput 0.855045
Client 17653 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
TimerThread: finished
ReadingThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
