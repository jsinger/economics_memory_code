economemd
    total memory: 1104 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub41_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 11771: heap size 9 MB, throughput 0.983556
Clients: 1
Reading from 11772: heap size 9 MB, throughput 0.983593
Clients: 2
Client 11771 has a minimum heap size of 276 MB
Client 11772 has a minimum heap size of 276 MB
Reading from 11772: heap size 9 MB, throughput 0.980741
Reading from 11771: heap size 9 MB, throughput 0.982444
Reading from 11772: heap size 11 MB, throughput 0.976943
Reading from 11771: heap size 11 MB, throughput 0.977275
Reading from 11772: heap size 11 MB, throughput 0.979265
Reading from 11771: heap size 11 MB, throughput 0.980909
Reading from 11772: heap size 15 MB, throughput 0.906107
Reading from 11771: heap size 15 MB, throughput 0.928443
Reading from 11772: heap size 20 MB, throughput 0.884068
Reading from 11771: heap size 18 MB, throughput 0.93267
Reading from 11772: heap size 25 MB, throughput 0.846264
Reading from 11771: heap size 23 MB, throughput 0.831866
Reading from 11772: heap size 30 MB, throughput 0.733079
Reading from 11771: heap size 28 MB, throughput 0.80334
Reading from 11772: heap size 30 MB, throughput 0.742977
Reading from 11771: heap size 38 MB, throughput 0.789605
Reading from 11772: heap size 40 MB, throughput 0.638473
Reading from 11771: heap size 39 MB, throughput 0.740327
Reading from 11772: heap size 46 MB, throughput 0.361524
Reading from 11771: heap size 42 MB, throughput 0.365609
Reading from 11772: heap size 47 MB, throughput 0.763408
Reading from 11771: heap size 43 MB, throughput 0.839475
Reading from 11772: heap size 51 MB, throughput 0.576792
Reading from 11772: heap size 66 MB, throughput 0.684536
Reading from 11771: heap size 48 MB, throughput 0.594092
Reading from 11771: heap size 61 MB, throughput 0.3377
Reading from 11772: heap size 75 MB, throughput 0.304738
Reading from 11771: heap size 86 MB, throughput 0.186896
Reading from 11772: heap size 98 MB, throughput 0.149989
Reading from 11771: heap size 88 MB, throughput 0.440288
Reading from 11772: heap size 100 MB, throughput 0.284785
Reading from 11771: heap size 91 MB, throughput 0.162358
Reading from 11772: heap size 104 MB, throughput 0.42205
Reading from 11772: heap size 130 MB, throughput 0.187016
Reading from 11771: heap size 93 MB, throughput 0.50694
Reading from 11772: heap size 135 MB, throughput 0.330703
Reading from 11771: heap size 125 MB, throughput 0.430576
Reading from 11772: heap size 138 MB, throughput 0.186195
Reading from 11771: heap size 126 MB, throughput 0.00470802
Reading from 11771: heap size 129 MB, throughput 0.635532
Reading from 11771: heap size 134 MB, throughput 0.752495
Reading from 11771: heap size 139 MB, throughput 0.761614
Reading from 11772: heap size 140 MB, throughput 0.39695
Reading from 11772: heap size 178 MB, throughput 0.0635454
Reading from 11772: heap size 181 MB, throughput 0.603159
Reading from 11772: heap size 185 MB, throughput 0.676993
Reading from 11771: heap size 143 MB, throughput 0.493854
Reading from 11772: heap size 191 MB, throughput 0.659531
Reading from 11771: heap size 190 MB, throughput 0.667303
Reading from 11771: heap size 192 MB, throughput 0.653517
Reading from 11771: heap size 195 MB, throughput 0.655393
Reading from 11771: heap size 202 MB, throughput 0.812922
Reading from 11772: heap size 195 MB, throughput 0.521722
Reading from 11771: heap size 208 MB, throughput 0.831723
Reading from 11772: heap size 236 MB, throughput 0.81224
Reading from 11771: heap size 217 MB, throughput 0.789099
Reading from 11772: heap size 252 MB, throughput 0.780763
Reading from 11771: heap size 223 MB, throughput 0.774218
Reading from 11772: heap size 253 MB, throughput 0.795167
Reading from 11772: heap size 259 MB, throughput 0.755717
Reading from 11771: heap size 232 MB, throughput 0.744133
Reading from 11771: heap size 238 MB, throughput 0.661138
Reading from 11772: heap size 260 MB, throughput 0.786972
Reading from 11772: heap size 267 MB, throughput 0.783519
Reading from 11772: heap size 268 MB, throughput 0.802456
Reading from 11771: heap size 244 MB, throughput 0.641901
Reading from 11772: heap size 275 MB, throughput 0.74736
Reading from 11771: heap size 284 MB, throughput 0.626938
Reading from 11772: heap size 279 MB, throughput 0.708859
Reading from 11772: heap size 286 MB, throughput 0.735997
Reading from 11771: heap size 286 MB, throughput 0.387025
Reading from 11771: heap size 324 MB, throughput 0.660927
Reading from 11771: heap size 327 MB, throughput 0.751899
Reading from 11771: heap size 324 MB, throughput 0.769493
Reading from 11772: heap size 288 MB, throughput 0.874414
Reading from 11771: heap size 327 MB, throughput 0.743753
Reading from 11771: heap size 322 MB, throughput 0.78633
Reading from 11772: heap size 294 MB, throughput 0.835171
Reading from 11771: heap size 325 MB, throughput 0.805097
Reading from 11772: heap size 297 MB, throughput 0.807758
Reading from 11771: heap size 320 MB, throughput 0.875854
Reading from 11771: heap size 323 MB, throughput 0.940482
Reading from 11772: heap size 300 MB, throughput 0.681891
Reading from 11771: heap size 319 MB, throughput 0.906001
Reading from 11771: heap size 323 MB, throughput 0.886788
Reading from 11772: heap size 342 MB, throughput 0.821276
Reading from 11771: heap size 318 MB, throughput 0.811179
Reading from 11771: heap size 321 MB, throughput 0.815974
Reading from 11772: heap size 346 MB, throughput 0.791985
Reading from 11772: heap size 346 MB, throughput 0.752229
Reading from 11771: heap size 317 MB, throughput 0.903365
Reading from 11772: heap size 347 MB, throughput 0.712363
Reading from 11771: heap size 320 MB, throughput 0.895324
Reading from 11772: heap size 349 MB, throughput 0.701463
Reading from 11771: heap size 319 MB, throughput 0.859072
Reading from 11772: heap size 351 MB, throughput 0.675623
Reading from 11771: heap size 321 MB, throughput 0.786913
Reading from 11771: heap size 325 MB, throughput 0.670006
Reading from 11771: heap size 326 MB, throughput 0.606569
Reading from 11771: heap size 333 MB, throughput 0.6061
Reading from 11771: heap size 333 MB, throughput 0.537244
Reading from 11772: heap size 354 MB, throughput 0.921139
Numeric result:
Recommendation: 2 clients, utility 0.394014:
    h1: 551.632 MB (U(h) = 0.631749*h^0.001)
    h2: 552.368 MB (U(h) = 0.615861*h^0.001)
Recommendation: 2 clients, utility 0.394014:
    h1: 552 MB (U(h) = 0.631749*h^0.001)
    h2: 552 MB (U(h) = 0.615861*h^0.001)
Reading from 11771: heap size 343 MB, throughput 0.921614
Reading from 11772: heap size 363 MB, throughput 0.970421
Reading from 11771: heap size 343 MB, throughput 0.961196
Reading from 11772: heap size 364 MB, throughput 0.974206
Reading from 11771: heap size 346 MB, throughput 0.955118
Reading from 11771: heap size 349 MB, throughput 0.965721
Reading from 11772: heap size 367 MB, throughput 0.977885
Reading from 11771: heap size 347 MB, throughput 0.968089
Reading from 11772: heap size 370 MB, throughput 0.97625
Reading from 11771: heap size 351 MB, throughput 0.971116
Reading from 11772: heap size 369 MB, throughput 0.973482
Reading from 11771: heap size 346 MB, throughput 0.97494
Reading from 11772: heap size 372 MB, throughput 0.975019
Reading from 11771: heap size 350 MB, throughput 0.971719
Numeric result:
Recommendation: 2 clients, utility 0.428832:
    h1: 499.919 MB (U(h) = 0.516624*h^0.03708)
    h2: 604.081 MB (U(h) = 0.494796*h^0.0448065)
Recommendation: 2 clients, utility 0.428832:
    h1: 499.915 MB (U(h) = 0.516624*h^0.03708)
    h2: 604.085 MB (U(h) = 0.494796*h^0.0448065)
Reading from 11772: heap size 372 MB, throughput 0.976054
Reading from 11771: heap size 349 MB, throughput 0.973871
Reading from 11772: heap size 375 MB, throughput 0.974711
Reading from 11771: heap size 351 MB, throughput 0.973852
Reading from 11772: heap size 378 MB, throughput 0.975216
Reading from 11771: heap size 355 MB, throughput 0.972128
Reading from 11771: heap size 355 MB, throughput 0.964871
Reading from 11772: heap size 378 MB, throughput 0.967222
Reading from 11771: heap size 358 MB, throughput 0.967787
Reading from 11772: heap size 382 MB, throughput 0.972343
Reading from 11771: heap size 359 MB, throughput 0.968388
Reading from 11772: heap size 383 MB, throughput 0.977583
Reading from 11772: heap size 388 MB, throughput 0.965199
Numeric result:
Recommendation: 2 clients, utility 0.486748:
    h1: 469.287 MB (U(h) = 0.479111*h^0.0567418)
    h2: 634.713 MB (U(h) = 0.436723*h^0.0767436)
Recommendation: 2 clients, utility 0.486748:
    h1: 469.287 MB (U(h) = 0.479111*h^0.0567418)
    h2: 634.713 MB (U(h) = 0.436723*h^0.0767436)
Reading from 11772: heap size 388 MB, throughput 0.933517
Reading from 11771: heap size 363 MB, throughput 0.972425
Reading from 11772: heap size 435 MB, throughput 0.867247
Reading from 11771: heap size 364 MB, throughput 0.954328
Reading from 11771: heap size 361 MB, throughput 0.926187
Reading from 11772: heap size 438 MB, throughput 0.941927
Reading from 11771: heap size 400 MB, throughput 0.885318
Reading from 11771: heap size 409 MB, throughput 0.8923
Reading from 11771: heap size 409 MB, throughput 0.963826
Reading from 11772: heap size 445 MB, throughput 0.974402
Reading from 11771: heap size 413 MB, throughput 0.986561
Reading from 11772: heap size 446 MB, throughput 0.976957
Reading from 11771: heap size 415 MB, throughput 0.990919
Reading from 11772: heap size 446 MB, throughput 0.975529
Reading from 11771: heap size 418 MB, throughput 0.989832
Reading from 11771: heap size 419 MB, throughput 0.991009
Reading from 11772: heap size 449 MB, throughput 0.979561
Numeric result:
Recommendation: 2 clients, utility 0.566306:
    h1: 549.381 MB (U(h) = 0.405014*h^0.0992755)
    h2: 554.619 MB (U(h) = 0.396795*h^0.100223)
Recommendation: 2 clients, utility 0.566306:
    h1: 549.377 MB (U(h) = 0.405014*h^0.0992755)
    h2: 554.623 MB (U(h) = 0.396795*h^0.100223)
Reading from 11771: heap size 415 MB, throughput 0.991072
Reading from 11772: heap size 450 MB, throughput 0.978738
Reading from 11771: heap size 377 MB, throughput 0.987353
Reading from 11772: heap size 452 MB, throughput 0.98002
Reading from 11771: heap size 410 MB, throughput 0.984697
Reading from 11772: heap size 455 MB, throughput 0.97714
Reading from 11771: heap size 414 MB, throughput 0.984837
Reading from 11772: heap size 457 MB, throughput 0.975679
Reading from 11771: heap size 414 MB, throughput 0.982991
Reading from 11771: heap size 414 MB, throughput 0.983096
Numeric result:
Recommendation: 2 clients, utility 0.602441:
    h1: 537.003 MB (U(h) = 0.387686*h^0.110308)
    h2: 566.997 MB (U(h) = 0.371178*h^0.116471)
Recommendation: 2 clients, utility 0.602441:
    h1: 536.999 MB (U(h) = 0.387686*h^0.110308)
    h2: 567.001 MB (U(h) = 0.371178*h^0.116471)
Reading from 11772: heap size 461 MB, throughput 0.979645
Reading from 11772: heap size 465 MB, throughput 0.967407
Reading from 11772: heap size 462 MB, throughput 0.947184
Reading from 11772: heap size 468 MB, throughput 0.908622
Reading from 11771: heap size 416 MB, throughput 0.98668
Reading from 11771: heap size 418 MB, throughput 0.981254
Reading from 11771: heap size 415 MB, throughput 0.961521
Reading from 11771: heap size 420 MB, throughput 0.935135
Reading from 11771: heap size 428 MB, throughput 0.911197
Reading from 11772: heap size 483 MB, throughput 0.972354
Reading from 11771: heap size 431 MB, throughput 0.971884
Reading from 11772: heap size 483 MB, throughput 0.985319
Reading from 11771: heap size 436 MB, throughput 0.980262
Reading from 11772: heap size 491 MB, throughput 0.986527
Reading from 11771: heap size 439 MB, throughput 0.983786
Reading from 11772: heap size 493 MB, throughput 0.987223
Reading from 11771: heap size 436 MB, throughput 0.985696
Numeric result:
Recommendation: 2 clients, utility 0.653448:
    h1: 531.1 MB (U(h) = 0.362518*h^0.126857)
    h2: 572.9 MB (U(h) = 0.34101*h^0.136837)
Recommendation: 2 clients, utility 0.653448:
    h1: 531.11 MB (U(h) = 0.362518*h^0.126857)
    h2: 572.89 MB (U(h) = 0.34101*h^0.136837)
Reading from 11771: heap size 440 MB, throughput 0.985151
Reading from 11772: heap size 494 MB, throughput 0.987849
Reading from 11771: heap size 436 MB, throughput 0.984597
Reading from 11772: heap size 498 MB, throughput 0.986668
Reading from 11771: heap size 440 MB, throughput 0.983847
Reading from 11772: heap size 496 MB, throughput 0.985863
Reading from 11771: heap size 439 MB, throughput 0.984479
Reading from 11771: heap size 440 MB, throughput 0.983182
Reading from 11772: heap size 499 MB, throughput 0.982824
Numeric result:
Recommendation: 2 clients, utility 0.671366:
    h1: 517.592 MB (U(h) = 0.358578*h^0.129527)
    h2: 586.408 MB (U(h) = 0.327031*h^0.146756)
Recommendation: 2 clients, utility 0.671366:
    h1: 517.578 MB (U(h) = 0.358578*h^0.129527)
    h2: 586.422 MB (U(h) = 0.327031*h^0.146756)
Reading from 11772: heap size 499 MB, throughput 0.982047
Reading from 11772: heap size 501 MB, throughput 0.972896
Reading from 11772: heap size 505 MB, throughput 0.964432
Reading from 11771: heap size 443 MB, throughput 0.987474
Reading from 11771: heap size 443 MB, throughput 0.983143
Reading from 11771: heap size 444 MB, throughput 0.972604
Reading from 11771: heap size 445 MB, throughput 0.960546
Reading from 11772: heap size 504 MB, throughput 0.987653
Reading from 11771: heap size 452 MB, throughput 0.968698
Reading from 11771: heap size 452 MB, throughput 0.985129
Reading from 11772: heap size 511 MB, throughput 0.991286
Reading from 11771: heap size 456 MB, throughput 0.986879
Reading from 11772: heap size 513 MB, throughput 0.991854
Numeric result:
Recommendation: 2 clients, utility 0.702407:
    h1: 522.639 MB (U(h) = 0.342275*h^0.140833)
    h2: 581.361 MB (U(h) = 0.313565*h^0.156659)
Recommendation: 2 clients, utility 0.702407:
    h1: 522.635 MB (U(h) = 0.342275*h^0.140833)
    h2: 581.365 MB (U(h) = 0.313565*h^0.156659)
Reading from 11771: heap size 458 MB, throughput 0.98919
Reading from 11772: heap size 514 MB, throughput 0.990744
Reading from 11771: heap size 457 MB, throughput 0.988361
Reading from 11772: heap size 516 MB, throughput 0.988728
Reading from 11771: heap size 459 MB, throughput 0.987816
Reading from 11771: heap size 457 MB, throughput 0.987142
Reading from 11772: heap size 514 MB, throughput 0.988528
Reading from 11771: heap size 459 MB, throughput 0.985663
Numeric result:
Recommendation: 2 clients, utility 0.716885:
    h1: 528.123 MB (U(h) = 0.333711*h^0.146942)
    h2: 575.877 MB (U(h) = 0.308822*h^0.160229)
Recommendation: 2 clients, utility 0.716885:
    h1: 528.122 MB (U(h) = 0.333711*h^0.146942)
    h2: 575.878 MB (U(h) = 0.308822*h^0.160229)
Reading from 11772: heap size 516 MB, throughput 0.991156
Reading from 11772: heap size 518 MB, throughput 0.985961
Reading from 11772: heap size 518 MB, throughput 0.975903
Reading from 11771: heap size 461 MB, throughput 0.9886
Reading from 11771: heap size 462 MB, throughput 0.985845
Reading from 11771: heap size 461 MB, throughput 0.976179
Reading from 11771: heap size 464 MB, throughput 0.957075
Reading from 11772: heap size 524 MB, throughput 0.986312
Reading from 11771: heap size 470 MB, throughput 0.98133
Reading from 11772: heap size 524 MB, throughput 0.990107
Reading from 11771: heap size 471 MB, throughput 0.988448
Reading from 11772: heap size 529 MB, throughput 0.990334
Reading from 11771: heap size 474 MB, throughput 0.9892
Numeric result:
Recommendation: 2 clients, utility 0.739141:
    h1: 537.938 MB (U(h) = 0.320241*h^0.156809)
    h2: 566.062 MB (U(h) = 0.302551*h^0.165009)
Recommendation: 2 clients, utility 0.739141:
    h1: 537.935 MB (U(h) = 0.320241*h^0.156809)
    h2: 566.065 MB (U(h) = 0.302551*h^0.165009)
Reading from 11771: heap size 477 MB, throughput 0.988296
Reading from 11772: heap size 529 MB, throughput 0.989633
Reading from 11771: heap size 475 MB, throughput 0.988372
Reading from 11772: heap size 527 MB, throughput 0.989399
Reading from 11771: heap size 478 MB, throughput 0.98824
Reading from 11772: heap size 528 MB, throughput 0.988735
Numeric result:
Recommendation: 2 clients, utility 0.750374:
    h1: 540.697 MB (U(h) = 0.314387*h^0.161194)
    h2: 563.303 MB (U(h) = 0.298768*h^0.167934)
Recommendation: 2 clients, utility 0.750374:
    h1: 540.696 MB (U(h) = 0.314387*h^0.161194)
    h2: 563.304 MB (U(h) = 0.298768*h^0.167934)
Reading from 11772: heap size 530 MB, throughput 0.989945
Reading from 11771: heap size 480 MB, throughput 0.988343
Reading from 11772: heap size 531 MB, throughput 0.983703
Reading from 11772: heap size 532 MB, throughput 0.978359
Reading from 11771: heap size 480 MB, throughput 0.990416
Reading from 11771: heap size 482 MB, throughput 0.985373
Reading from 11772: heap size 534 MB, throughput 0.988387
Reading from 11771: heap size 483 MB, throughput 0.961969
Reading from 11771: heap size 489 MB, throughput 0.970034
Reading from 11772: heap size 533 MB, throughput 0.991912
Reading from 11771: heap size 490 MB, throughput 0.986823
Numeric result:
Recommendation: 2 clients, utility 0.769344:
    h1: 540.509 MB (U(h) = 0.30658*h^0.167135)
    h2: 563.491 MB (U(h) = 0.290747*h^0.17424)
Recommendation: 2 clients, utility 0.769344:
    h1: 540.511 MB (U(h) = 0.30658*h^0.167135)
    h2: 563.489 MB (U(h) = 0.290747*h^0.17424)
Reading from 11772: heap size 533 MB, throughput 0.990768
Reading from 11771: heap size 495 MB, throughput 0.98959
Reading from 11771: heap size 497 MB, throughput 0.990197
Reading from 11772: heap size 531 MB, throughput 0.98967
Reading from 11771: heap size 498 MB, throughput 0.98946
Reading from 11772: heap size 532 MB, throughput 0.991007
Reading from 11771: heap size 500 MB, throughput 0.989356
Reading from 11772: heap size 535 MB, throughput 0.991559
Numeric result:
Recommendation: 2 clients, utility 0.778253:
    h1: 545.778 MB (U(h) = 0.300806*h^0.171583)
    h2: 558.222 MB (U(h) = 0.289175*h^0.175494)
Recommendation: 2 clients, utility 0.778253:
    h1: 545.779 MB (U(h) = 0.300806*h^0.171583)
    h2: 558.221 MB (U(h) = 0.289175*h^0.175494)
Reading from 11772: heap size 535 MB, throughput 0.986738
Reading from 11772: heap size 535 MB, throughput 0.978064
Reading from 11771: heap size 502 MB, throughput 0.988497
Reading from 11772: heap size 529 MB, throughput 0.986806
Reading from 11771: heap size 503 MB, throughput 0.990022
Reading from 11771: heap size 507 MB, throughput 0.982728
Reading from 11771: heap size 507 MB, throughput 0.966862
Reading from 11772: heap size 535 MB, throughput 0.990375
Reading from 11771: heap size 515 MB, throughput 0.986501
Reading from 11772: heap size 509 MB, throughput 0.990642
Reading from 11771: heap size 516 MB, throughput 0.991114
Numeric result:
Recommendation: 2 clients, utility 0.916163:
    h1: 762.572 MB (U(h) = 0.0829549*h^0.394357)
    h2: 341.428 MB (U(h) = 0.287869*h^0.176564)
Recommendation: 2 clients, utility 0.916163:
    h1: 762.575 MB (U(h) = 0.0829549*h^0.394357)
    h2: 341.425 MB (U(h) = 0.287869*h^0.176564)
Reading from 11772: heap size 536 MB, throughput 0.989867
Reading from 11771: heap size 520 MB, throughput 0.990834
Reading from 11772: heap size 507 MB, throughput 0.989127
Reading from 11771: heap size 522 MB, throughput 0.99193
Reading from 11772: heap size 505 MB, throughput 0.989466
Reading from 11771: heap size 522 MB, throughput 0.990533
Reading from 11772: heap size 496 MB, throughput 0.990209
Numeric result:
Recommendation: 2 clients, utility 0.997943:
    h1: 812.569 MB (U(h) = 0.0449498*h^0.498538)
    h2: 291.431 MB (U(h) = 0.285123*h^0.178805)
Recommendation: 2 clients, utility 0.997943:
    h1: 812.566 MB (U(h) = 0.0449498*h^0.498538)
    h2: 291.434 MB (U(h) = 0.285123*h^0.178805)
Reading from 11772: heap size 494 MB, throughput 0.981968
Reading from 11772: heap size 483 MB, throughput 0.974545
Reading from 11771: heap size 524 MB, throughput 0.989831
Reading from 11772: heap size 478 MB, throughput 0.987483
Reading from 11771: heap size 527 MB, throughput 0.990732
Reading from 11771: heap size 528 MB, throughput 0.983654
Reading from 11771: heap size 529 MB, throughput 0.981856
Reading from 11772: heap size 449 MB, throughput 0.989854
Reading from 11772: heap size 463 MB, throughput 0.988884
Reading from 11771: heap size 531 MB, throughput 0.989139
Numeric result:
Recommendation: 2 clients, utility 1.25342:
    h1: 828 MB (U(h) = 0.00803978*h^0.788225)
    h2: 276 MB (U(h) = 0.282901*h^0.180733)
Recommendation: 2 clients, utility 1.25342:
    h1: 828 MB (U(h) = 0.00803978*h^0.788225)
    h2: 276 MB (U(h) = 0.282901*h^0.180733)
Reading from 11772: heap size 440 MB, throughput 0.988779
Reading from 11771: heap size 530 MB, throughput 0.99127
Reading from 11772: heap size 447 MB, throughput 0.987326
Reading from 11772: heap size 432 MB, throughput 0.987844
Reading from 11771: heap size 530 MB, throughput 0.99211
Reading from 11772: heap size 435 MB, throughput 0.984995
Reading from 11771: heap size 528 MB, throughput 0.991116
Reading from 11772: heap size 426 MB, throughput 0.984935
Numeric result:
Recommendation: 2 clients, utility 1.20912:
    h1: 813.953 MB (U(h) = 0.0096101*h^0.758157)
    h2: 290.047 MB (U(h) = 0.168956*h^0.270167)
Recommendation: 2 clients, utility 1.20912:
    h1: 813.951 MB (U(h) = 0.0096101*h^0.758157)
    h2: 290.049 MB (U(h) = 0.168956*h^0.270167)
Reading from 11772: heap size 429 MB, throughput 0.986173
Reading from 11772: heap size 402 MB, throughput 0.975713
Reading from 11772: heap size 424 MB, throughput 0.962601
Reading from 11772: heap size 413 MB, throughput 0.940756
Reading from 11771: heap size 529 MB, throughput 0.989717
Reading from 11772: heap size 419 MB, throughput 0.979939
Reading from 11772: heap size 386 MB, throughput 0.985037
Reading from 11771: heap size 531 MB, throughput 0.993063
Reading from 11771: heap size 532 MB, throughput 0.988428
Reading from 11771: heap size 530 MB, throughput 0.980147
Reading from 11772: heap size 410 MB, throughput 0.98649
Reading from 11772: heap size 383 MB, throughput 0.98462
Reading from 11771: heap size 524 MB, throughput 0.987916
Reading from 11772: heap size 402 MB, throughput 0.983947
Numeric result:
Recommendation: 2 clients, utility 1.27198:
    h1: 603.128 MB (U(h) = 0.00790545*h^0.790463)
    h2: 500.872 MB (U(h) = 0.0172401*h^0.656438)
Recommendation: 2 clients, utility 1.27198:
    h1: 603.131 MB (U(h) = 0.00790545*h^0.790463)
    h2: 500.869 MB (U(h) = 0.0172401*h^0.656438)
Reading from 11772: heap size 380 MB, throughput 0.982793
Reading from 11771: heap size 531 MB, throughput 0.991292
Reading from 11772: heap size 403 MB, throughput 0.983269
Reading from 11772: heap size 402 MB, throughput 0.981829
Reading from 11771: heap size 505 MB, throughput 0.991489
Reading from 11772: heap size 401 MB, throughput 0.981736
Reading from 11772: heap size 402 MB, throughput 0.980232
Reading from 11771: heap size 532 MB, throughput 0.990602
Reading from 11772: heap size 405 MB, throughput 0.980187
Numeric result:
Recommendation: 2 clients, utility 1.32079:
    h1: 524.636 MB (U(h) = 0.0107302*h^0.73936)
    h2: 579.364 MB (U(h) = 0.00665832*h^0.816485)
Recommendation: 2 clients, utility 1.32079:
    h1: 524.637 MB (U(h) = 0.0107302*h^0.73936)
    h2: 579.363 MB (U(h) = 0.00665832*h^0.816485)
Reading from 11772: heap size 405 MB, throughput 0.984874
Reading from 11772: heap size 408 MB, throughput 0.976734
Reading from 11772: heap size 409 MB, throughput 0.956228
Reading from 11771: heap size 530 MB, throughput 0.989988
Reading from 11772: heap size 414 MB, throughput 0.939475
Reading from 11772: heap size 415 MB, throughput 0.925901
Reading from 11772: heap size 424 MB, throughput 0.980685
Reading from 11771: heap size 514 MB, throughput 0.988918
Reading from 11772: heap size 425 MB, throughput 0.984835
Reading from 11771: heap size 522 MB, throughput 0.988526
Reading from 11771: heap size 525 MB, throughput 0.978414
Reading from 11772: heap size 428 MB, throughput 0.984788
Reading from 11771: heap size 527 MB, throughput 0.978752
Reading from 11772: heap size 430 MB, throughput 0.985083
Reading from 11771: heap size 515 MB, throughput 0.988929
Numeric result:
Recommendation: 2 clients, utility 1.29269:
    h1: 719.347 MB (U(h) = 0.00740237*h^0.800648)
    h2: 384.653 MB (U(h) = 0.070467*h^0.428126)
Recommendation: 2 clients, utility 1.29269:
    h1: 719.347 MB (U(h) = 0.00740237*h^0.800648)
    h2: 384.653 MB (U(h) = 0.070467*h^0.428126)
Reading from 11772: heap size 430 MB, throughput 0.983337
Reading from 11772: heap size 445 MB, throughput 0.991508
Reading from 11771: heap size 523 MB, throughput 0.989867
Reading from 11772: heap size 438 MB, throughput 0.993069
Reading from 11771: heap size 524 MB, throughput 0.990086
Reading from 11772: heap size 400 MB, throughput 0.992927
Reading from 11772: heap size 433 MB, throughput 0.989484
Reading from 11771: heap size 526 MB, throughput 0.989501
Reading from 11772: heap size 402 MB, throughput 0.988162
Numeric result:
Recommendation: 2 clients, utility 1.0875:
    h1: 527.122 MB (U(h) = 0.0909043*h^0.384981)
    h2: 576.878 MB (U(h) = 0.0735649*h^0.421316)
Recommendation: 2 clients, utility 1.0875:
    h1: 527.124 MB (U(h) = 0.0909043*h^0.384981)
    h2: 576.876 MB (U(h) = 0.0735649*h^0.421316)
Reading from 11772: heap size 424 MB, throughput 0.982448
Reading from 11772: heap size 428 MB, throughput 0.971223
Reading from 11772: heap size 433 MB, throughput 0.951214
Reading from 11772: heap size 437 MB, throughput 0.925778
Reading from 11771: heap size 526 MB, throughput 0.99005
Reading from 11772: heap size 448 MB, throughput 0.980318
Reading from 11772: heap size 449 MB, throughput 0.984381
Reading from 11771: heap size 527 MB, throughput 0.989546
Reading from 11771: heap size 513 MB, throughput 0.983179
Reading from 11771: heap size 522 MB, throughput 0.975366
Reading from 11772: heap size 449 MB, throughput 0.985872
Reading from 11771: heap size 528 MB, throughput 0.984213
Reading from 11772: heap size 453 MB, throughput 0.986627
Numeric result:
Recommendation: 2 clients, utility 1.08394:
    h1: 571.277 MB (U(h) = 0.0779245*h^0.410392)
    h2: 532.723 MB (U(h) = 0.0930091*h^0.382695)
Recommendation: 2 clients, utility 1.08394:
    h1: 571.278 MB (U(h) = 0.0779245*h^0.410392)
    h2: 532.722 MB (U(h) = 0.0930091*h^0.382695)
Reading from 11771: heap size 493 MB, throughput 0.99057
Reading from 11772: heap size 455 MB, throughput 0.987942
Reading from 11772: heap size 457 MB, throughput 0.985631
Reading from 11771: heap size 524 MB, throughput 0.991029
Reading from 11772: heap size 462 MB, throughput 0.983936
Reading from 11771: heap size 523 MB, throughput 0.990641
Reading from 11772: heap size 463 MB, throughput 0.984834
Reading from 11772: heap size 467 MB, throughput 0.982764
Reading from 11771: heap size 522 MB, throughput 0.989884
Numeric result:
Recommendation: 2 clients, utility 1.09574:
    h1: 590.428 MB (U(h) = 0.0640842*h^0.442698)
    h2: 513.572 MB (U(h) = 0.0917039*h^0.385073)
Recommendation: 2 clients, utility 1.09574:
    h1: 590.427 MB (U(h) = 0.0640842*h^0.442698)
    h2: 513.573 MB (U(h) = 0.0917039*h^0.385073)
Reading from 11772: heap size 468 MB, throughput 0.9805
Reading from 11772: heap size 473 MB, throughput 0.972054
Reading from 11772: heap size 475 MB, throughput 0.957198
Reading from 11772: heap size 483 MB, throughput 0.977888
Reading from 11771: heap size 524 MB, throughput 0.989804
Reading from 11772: heap size 484 MB, throughput 0.986438
Reading from 11771: heap size 526 MB, throughput 0.99201
Reading from 11771: heap size 527 MB, throughput 0.98647
Reading from 11771: heap size 528 MB, throughput 0.979478
Reading from 11772: heap size 489 MB, throughput 0.989482
Reading from 11772: heap size 491 MB, throughput 0.989849
Reading from 11771: heap size 530 MB, throughput 0.991051
Numeric result:
Recommendation: 2 clients, utility 1.09558:
    h1: 590.515 MB (U(h) = 0.0640982*h^0.44266)
    h2: 513.485 MB (U(h) = 0.0917821*h^0.384917)
Recommendation: 2 clients, utility 1.09558:
    h1: 590.515 MB (U(h) = 0.0640982*h^0.44266)
    h2: 513.485 MB (U(h) = 0.0917821*h^0.384917)
Reading from 11772: heap size 491 MB, throughput 0.988371
Reading from 11771: heap size 531 MB, throughput 0.992073
Reading from 11772: heap size 494 MB, throughput 0.98748
Reading from 11771: heap size 532 MB, throughput 0.991483
Reading from 11772: heap size 495 MB, throughput 0.988171
Reading from 11771: heap size 529 MB, throughput 0.991095
Reading from 11772: heap size 496 MB, throughput 0.986171
Numeric result:
Recommendation: 2 clients, utility 1.09582:
    h1: 589.019 MB (U(h) = 0.0640437*h^0.442809)
    h2: 514.981 MB (U(h) = 0.0905215*h^0.387152)
Recommendation: 2 clients, utility 1.09582:
    h1: 589.017 MB (U(h) = 0.0640437*h^0.442809)
    h2: 514.983 MB (U(h) = 0.0905215*h^0.387152)
Reading from 11772: heap size 497 MB, throughput 0.984683
Reading from 11772: heap size 501 MB, throughput 0.975894
Reading from 11772: heap size 507 MB, throughput 0.963983
Reading from 11771: heap size 531 MB, throughput 0.990453
Reading from 11772: heap size 507 MB, throughput 0.986621
Reading from 11771: heap size 533 MB, throughput 0.991044
Reading from 11772: heap size 515 MB, throughput 0.988555
Reading from 11771: heap size 534 MB, throughput 0.987084
Reading from 11771: heap size 531 MB, throughput 0.978362
Reading from 11771: heap size 526 MB, throughput 0.986247
Reading from 11772: heap size 482 MB, throughput 0.989713
Numeric result:
Recommendation: 2 clients, utility 1.07896:
    h1: 618.918 MB (U(h) = 0.0745691*h^0.417506)
    h2: 485.082 MB (U(h) = 0.130639*h^0.32722)
Recommendation: 2 clients, utility 1.07896:
    h1: 618.922 MB (U(h) = 0.0745691*h^0.417506)
    h2: 485.078 MB (U(h) = 0.130639*h^0.32722)
Reading from 11772: heap size 512 MB, throughput 0.989533
Reading from 11771: heap size 532 MB, throughput 0.990039
Reading from 11772: heap size 484 MB, throughput 0.987197
Reading from 11771: heap size 507 MB, throughput 0.990244
Reading from 11772: heap size 505 MB, throughput 0.986248
Reading from 11771: heap size 533 MB, throughput 0.99036
Reading from 11772: heap size 486 MB, throughput 0.989684
Numeric result:
Recommendation: 2 clients, utility 1.07953:
    h1: 611.645 MB (U(h) = 0.0744658*h^0.41775)
    h2: 492.355 MB (U(h) = 0.123558*h^0.336276)
Recommendation: 2 clients, utility 1.07953:
    h1: 611.645 MB (U(h) = 0.0744658*h^0.41775)
    h2: 492.355 MB (U(h) = 0.123558*h^0.336276)
Reading from 11772: heap size 490 MB, throughput 0.985584
Reading from 11772: heap size 495 MB, throughput 0.979391
Reading from 11772: heap size 487 MB, throughput 0.970833
Client 11772 died
Clients: 1
Reading from 11771: heap size 515 MB, throughput 0.986763
Reading from 11771: heap size 552 MB, throughput 0.993392
Reading from 11771: heap size 552 MB, throughput 0.991994
Reading from 11771: heap size 550 MB, throughput 0.987225
Client 11771 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
