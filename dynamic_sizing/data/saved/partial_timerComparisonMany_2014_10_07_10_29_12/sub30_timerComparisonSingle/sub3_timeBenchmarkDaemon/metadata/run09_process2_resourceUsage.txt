	Command being timed: "cgexec -g memory:economem /home/callum/economics_memory_code/dynamic_sizing/hotspot8/openjdk8/jdk8/build/linux-x86_64-normal-server-release/jdk/bin/java -XX:+EconomemVengerovThroughput -XX:EconomemMinHeap=8 -Xms10m -Xmx568m -XX:+DisableExplicitGC -XX:+UseAdaptiveSizePolicyWithSystemGC -jar /home/callum/economics_memory_code/dynamic_sizing/haskell_common/dacapo-9.12-bach.jar --no-pre-iteration-gc --scratch-directory /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub30_timerComparisonSingle/sub3_timeBenchmarkDaemon/scratch2 -t 2 -n 32 -s large sunflow"
	User time (seconds): 645.71
	System time (seconds): 7.72
	Percent of CPU this job got: 145%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 7:28.93
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 249576
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 16371
	Minor (reclaiming a frame) page faults: 210760
	Voluntary context switches: 351235
	Involuntary context switches: 103336
	Swaps: 0
	File system inputs: 963384
	File system outputs: 18720
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
