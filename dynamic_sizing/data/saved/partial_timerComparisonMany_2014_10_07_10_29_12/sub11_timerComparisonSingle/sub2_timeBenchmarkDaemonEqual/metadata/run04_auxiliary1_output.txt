economemd
    total memory: 3705 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub11_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
TimerThread: starting
ReadingThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 23200: heap size 9 MB, throughput 0.982985
Clients: 1
Client 23200 has a minimum heap size of 12 MB
Reading from 23199: heap size 9 MB, throughput 0.992972
Clients: 2
Client 23199 has a minimum heap size of 1223 MB
Reading from 23199: heap size 9 MB, throughput 0.95276
Reading from 23199: heap size 9 MB, throughput 0.924404
Reading from 23200: heap size 9 MB, throughput 0.989982
Reading from 23199: heap size 9 MB, throughput 0.943809
Reading from 23200: heap size 11 MB, throughput 0.98363
Reading from 23199: heap size 11 MB, throughput 0.973358
Reading from 23200: heap size 11 MB, throughput 0.973573
Reading from 23199: heap size 11 MB, throughput 0.989716
Reading from 23200: heap size 15 MB, throughput 0.99005
Reading from 23199: heap size 17 MB, throughput 0.884831
Reading from 23200: heap size 15 MB, throughput 0.980294
Reading from 23199: heap size 17 MB, throughput 0.597299
Reading from 23199: heap size 30 MB, throughput 0.947961
Reading from 23199: heap size 31 MB, throughput 0.960024
Reading from 23200: heap size 24 MB, throughput 0.993433
Reading from 23199: heap size 35 MB, throughput 0.478715
Reading from 23199: heap size 49 MB, throughput 0.876346
Reading from 23199: heap size 50 MB, throughput 0.887896
Reading from 23200: heap size 25 MB, throughput 0.860593
Reading from 23199: heap size 53 MB, throughput 0.317902
Reading from 23199: heap size 77 MB, throughput 0.73112
Reading from 23200: heap size 46 MB, throughput 0.991286
Reading from 23199: heap size 77 MB, throughput 0.335607
Reading from 23199: heap size 104 MB, throughput 0.729339
Reading from 23199: heap size 105 MB, throughput 0.723918
Reading from 23200: heap size 47 MB, throughput 0.95668
Reading from 23199: heap size 107 MB, throughput 0.676576
Reading from 23200: heap size 53 MB, throughput 0.983639
Reading from 23199: heap size 110 MB, throughput 0.279507
Reading from 23200: heap size 53 MB, throughput 0.976891
Reading from 23199: heap size 138 MB, throughput 0.66403
Reading from 23199: heap size 142 MB, throughput 0.79729
Reading from 23200: heap size 61 MB, throughput 0.988213
Reading from 23199: heap size 145 MB, throughput 0.794439
Reading from 23200: heap size 61 MB, throughput 0.988794
Reading from 23200: heap size 69 MB, throughput 0.976056
Reading from 23199: heap size 147 MB, throughput 0.188006
Reading from 23200: heap size 70 MB, throughput 0.960925
Reading from 23199: heap size 188 MB, throughput 0.73134
Reading from 23199: heap size 190 MB, throughput 0.701092
Reading from 23199: heap size 192 MB, throughput 0.61824
Reading from 23200: heap size 81 MB, throughput 0.988798
Reading from 23199: heap size 199 MB, throughput 0.737191
Reading from 23199: heap size 201 MB, throughput 0.774981
Reading from 23199: heap size 207 MB, throughput 0.713217
Reading from 23199: heap size 211 MB, throughput 0.655787
Reading from 23199: heap size 220 MB, throughput 0.684512
Reading from 23199: heap size 225 MB, throughput 0.670175
Reading from 23199: heap size 236 MB, throughput 0.590445
Reading from 23200: heap size 82 MB, throughput 0.994837
Reading from 23199: heap size 241 MB, throughput 0.0714782
Reading from 23199: heap size 287 MB, throughput 0.497173
Reading from 23200: heap size 90 MB, throughput 0.994406
Reading from 23199: heap size 295 MB, throughput 0.0927594
Reading from 23200: heap size 91 MB, throughput 0.982665
Reading from 23199: heap size 342 MB, throughput 0.412844
Reading from 23199: heap size 330 MB, throughput 0.629058
Reading from 23199: heap size 336 MB, throughput 0.667253
Reading from 23199: heap size 336 MB, throughput 0.667049
Reading from 23199: heap size 337 MB, throughput 0.598576
Reading from 23199: heap size 338 MB, throughput 0.543606
Reading from 23199: heap size 343 MB, throughput 0.593062
Reading from 23200: heap size 100 MB, throughput 0.993023
Reading from 23199: heap size 345 MB, throughput 0.533463
Reading from 23199: heap size 353 MB, throughput 0.540273
Reading from 23200: heap size 100 MB, throughput 0.988698
Reading from 23199: heap size 358 MB, throughput 0.0768332
Reading from 23199: heap size 410 MB, throughput 0.497136
Reading from 23199: heap size 405 MB, throughput 0.58049
Reading from 23199: heap size 410 MB, throughput 0.628552
Reading from 23199: heap size 407 MB, throughput 0.610305
Reading from 23199: heap size 409 MB, throughput 0.544054
Reading from 23199: heap size 412 MB, throughput 0.654371
Reading from 23200: heap size 108 MB, throughput 0.995187
Reading from 23199: heap size 413 MB, throughput 0.58441
Reading from 23199: heap size 417 MB, throughput 0.583623
Reading from 23200: heap size 109 MB, throughput 0.994569
Equal recommendation: 1852 MB each
Reading from 23199: heap size 420 MB, throughput 0.0735942
Reading from 23199: heap size 474 MB, throughput 0.523571
Reading from 23200: heap size 116 MB, throughput 0.980525
Reading from 23199: heap size 476 MB, throughput 0.496618
Reading from 23199: heap size 480 MB, throughput 0.506479
Reading from 23199: heap size 480 MB, throughput 0.506769
Reading from 23199: heap size 484 MB, throughput 0.54483
Reading from 23199: heap size 486 MB, throughput 0.522775
Reading from 23200: heap size 117 MB, throughput 0.995814
Reading from 23200: heap size 123 MB, throughput 0.996037
Reading from 23199: heap size 490 MB, throughput 0.103763
Reading from 23199: heap size 550 MB, throughput 0.415681
Reading from 23199: heap size 558 MB, throughput 0.539094
Reading from 23199: heap size 559 MB, throughput 0.503735
Reading from 23200: heap size 124 MB, throughput 0.995821
Reading from 23199: heap size 564 MB, throughput 0.627067
Reading from 23200: heap size 129 MB, throughput 0.994741
Reading from 23199: heap size 569 MB, throughput 0.094622
Reading from 23199: heap size 635 MB, throughput 0.363792
Reading from 23200: heap size 129 MB, throughput 0.995529
Reading from 23199: heap size 641 MB, throughput 0.525081
Reading from 23199: heap size 642 MB, throughput 0.502702
Reading from 23199: heap size 648 MB, throughput 0.69808
Reading from 23200: heap size 135 MB, throughput 0.997112
Reading from 23199: heap size 654 MB, throughput 0.825863
Reading from 23200: heap size 135 MB, throughput 0.997288
Reading from 23199: heap size 662 MB, throughput 0.812363
Reading from 23199: heap size 665 MB, throughput 0.824367
Reading from 23200: heap size 139 MB, throughput 0.996756
Reading from 23200: heap size 140 MB, throughput 0.997086
Reading from 23200: heap size 143 MB, throughput 0.862981
Reading from 23199: heap size 679 MB, throughput 0.0597043
Reading from 23199: heap size 758 MB, throughput 0.416401
Reading from 23199: heap size 763 MB, throughput 0.545517
Reading from 23199: heap size 761 MB, throughput 0.27328
Reading from 23199: heap size 684 MB, throughput 0.277885
Reading from 23200: heap size 148 MB, throughput 0.998306
Equal recommendation: 1852 MB each
Reading from 23199: heap size 748 MB, throughput 0.0536774
Reading from 23199: heap size 833 MB, throughput 0.324528
Reading from 23200: heap size 151 MB, throughput 0.998381
Reading from 23199: heap size 835 MB, throughput 0.895656
Reading from 23199: heap size 837 MB, throughput 0.645346
Reading from 23199: heap size 838 MB, throughput 0.629333
Reading from 23199: heap size 840 MB, throughput 0.619105
Reading from 23200: heap size 151 MB, throughput 0.998483
Reading from 23199: heap size 840 MB, throughput 0.765018
Reading from 23200: heap size 154 MB, throughput 0.99875
Reading from 23199: heap size 842 MB, throughput 0.112316
Reading from 23200: heap size 155 MB, throughput 0.995683
Reading from 23199: heap size 935 MB, throughput 0.62823
Reading from 23199: heap size 936 MB, throughput 0.740342
Reading from 23200: heap size 157 MB, throughput 0.99463
Reading from 23199: heap size 944 MB, throughput 0.895266
Reading from 23199: heap size 947 MB, throughput 0.894563
Reading from 23200: heap size 158 MB, throughput 0.990874
Reading from 23199: heap size 946 MB, throughput 0.899327
Reading from 23199: heap size 786 MB, throughput 0.844699
Reading from 23199: heap size 923 MB, throughput 0.746867
Reading from 23199: heap size 802 MB, throughput 0.747973
Reading from 23199: heap size 911 MB, throughput 0.767197
Reading from 23199: heap size 808 MB, throughput 0.803312
Reading from 23200: heap size 162 MB, throughput 0.997903
Reading from 23199: heap size 904 MB, throughput 0.834198
Reading from 23199: heap size 911 MB, throughput 0.805599
Reading from 23199: heap size 898 MB, throughput 0.8438
Reading from 23199: heap size 904 MB, throughput 0.816537
Reading from 23199: heap size 894 MB, throughput 0.852764
Reading from 23200: heap size 163 MB, throughput 0.997415
Reading from 23199: heap size 899 MB, throughput 0.958452
Reading from 23200: heap size 168 MB, throughput 0.997833
Reading from 23199: heap size 899 MB, throughput 0.976947
Reading from 23199: heap size 902 MB, throughput 0.86127
Reading from 23200: heap size 168 MB, throughput 0.997133
Reading from 23199: heap size 899 MB, throughput 0.818046
Reading from 23199: heap size 901 MB, throughput 0.813651
Reading from 23199: heap size 901 MB, throughput 0.831427
Reading from 23199: heap size 903 MB, throughput 0.751226
Reading from 23199: heap size 904 MB, throughput 0.835747
Equal recommendation: 1852 MB each
Reading from 23199: heap size 906 MB, throughput 0.822633
Reading from 23200: heap size 173 MB, throughput 0.998045
Reading from 23199: heap size 907 MB, throughput 0.818895
Reading from 23199: heap size 909 MB, throughput 0.892173
Reading from 23199: heap size 910 MB, throughput 0.87902
Reading from 23200: heap size 173 MB, throughput 0.996787
Reading from 23199: heap size 913 MB, throughput 0.902728
Reading from 23199: heap size 915 MB, throughput 0.745976
Reading from 23200: heap size 178 MB, throughput 0.995901
Reading from 23200: heap size 178 MB, throughput 0.997101
Reading from 23199: heap size 917 MB, throughput 0.0694252
Reading from 23199: heap size 1027 MB, throughput 0.539252
Reading from 23199: heap size 1036 MB, throughput 0.64806
Reading from 23199: heap size 1044 MB, throughput 0.676454
Reading from 23199: heap size 1046 MB, throughput 0.690958
Reading from 23199: heap size 1048 MB, throughput 0.704835
Reading from 23200: heap size 182 MB, throughput 0.997734
Reading from 23199: heap size 1052 MB, throughput 0.703454
Reading from 23199: heap size 1056 MB, throughput 0.682053
Reading from 23199: heap size 1060 MB, throughput 0.694833
Reading from 23199: heap size 1073 MB, throughput 0.692909
Reading from 23200: heap size 182 MB, throughput 0.997183
Reading from 23200: heap size 187 MB, throughput 0.987128
Reading from 23200: heap size 187 MB, throughput 0.995424
Reading from 23199: heap size 1074 MB, throughput 0.971158
Reading from 23200: heap size 194 MB, throughput 0.997117
Equal recommendation: 1852 MB each
Reading from 23200: heap size 195 MB, throughput 0.997889
Reading from 23200: heap size 200 MB, throughput 0.997603
Reading from 23199: heap size 1094 MB, throughput 0.959157
Reading from 23200: heap size 201 MB, throughput 0.994612
Reading from 23200: heap size 207 MB, throughput 0.995966
Reading from 23200: heap size 207 MB, throughput 0.996899
Reading from 23199: heap size 1095 MB, throughput 0.959829
Reading from 23200: heap size 213 MB, throughput 0.998173
Reading from 23200: heap size 214 MB, throughput 0.996541
Reading from 23200: heap size 219 MB, throughput 0.998217
Reading from 23199: heap size 1110 MB, throughput 0.962517
Equal recommendation: 1852 MB each
Reading from 23200: heap size 219 MB, throughput 0.997607
Reading from 23200: heap size 225 MB, throughput 0.998145
Reading from 23200: heap size 225 MB, throughput 0.99635
Reading from 23199: heap size 1115 MB, throughput 0.785066
Reading from 23200: heap size 229 MB, throughput 0.998026
Reading from 23200: heap size 229 MB, throughput 0.997406
Reading from 23199: heap size 1226 MB, throughput 0.993213
Reading from 23200: heap size 235 MB, throughput 0.997552
Reading from 23200: heap size 235 MB, throughput 0.996959
Equal recommendation: 1852 MB each
Reading from 23200: heap size 240 MB, throughput 0.996158
Reading from 23199: heap size 1229 MB, throughput 0.991658
Reading from 23200: heap size 240 MB, throughput 0.993245
Reading from 23200: heap size 246 MB, throughput 0.997887
Reading from 23200: heap size 246 MB, throughput 0.998173
Reading from 23199: heap size 1245 MB, throughput 0.992913
Reading from 23200: heap size 252 MB, throughput 0.998335
Reading from 23200: heap size 253 MB, throughput 0.99761
Reading from 23199: heap size 1249 MB, throughput 0.991142
Reading from 23200: heap size 258 MB, throughput 0.997101
Equal recommendation: 1852 MB each
Reading from 23200: heap size 258 MB, throughput 0.997845
Reading from 23199: heap size 1252 MB, throughput 0.990401
Reading from 23200: heap size 264 MB, throughput 0.997775
Reading from 23200: heap size 264 MB, throughput 0.997555
Reading from 23199: heap size 1255 MB, throughput 0.988719
Reading from 23200: heap size 270 MB, throughput 0.997642
Reading from 23200: heap size 270 MB, throughput 0.99679
Reading from 23200: heap size 276 MB, throughput 0.991801
Equal recommendation: 1852 MB each
Reading from 23199: heap size 1244 MB, throughput 0.986128
Reading from 23200: heap size 276 MB, throughput 0.997299
Reading from 23200: heap size 285 MB, throughput 0.997841
Reading from 23199: heap size 1168 MB, throughput 0.984307
Reading from 23200: heap size 285 MB, throughput 0.997845
Reading from 23200: heap size 291 MB, throughput 0.998178
Reading from 23199: heap size 1234 MB, throughput 0.987198
Reading from 23200: heap size 292 MB, throughput 0.997754
Equal recommendation: 1852 MB each
Reading from 23200: heap size 298 MB, throughput 0.998493
Reading from 23200: heap size 298 MB, throughput 0.995099
Reading from 23199: heap size 1241 MB, throughput 0.984374
Reading from 23200: heap size 304 MB, throughput 0.995752
Reading from 23200: heap size 304 MB, throughput 0.997837
Reading from 23199: heap size 1242 MB, throughput 0.980352
Reading from 23200: heap size 312 MB, throughput 0.881391
Reading from 23200: heap size 319 MB, throughput 0.998807
Equal recommendation: 1852 MB each
Reading from 23199: heap size 1242 MB, throughput 0.982694
Reading from 23200: heap size 328 MB, throughput 0.998958
Reading from 23200: heap size 329 MB, throughput 0.998625
Reading from 23199: heap size 1244 MB, throughput 0.982251
Reading from 23200: heap size 338 MB, throughput 0.999032
Reading from 23200: heap size 338 MB, throughput 0.998933
Reading from 23199: heap size 1248 MB, throughput 0.979591
Equal recommendation: 1852 MB each
Reading from 23200: heap size 345 MB, throughput 0.999192
Reading from 23200: heap size 346 MB, throughput 0.999107
Reading from 23199: heap size 1250 MB, throughput 0.978943
Reading from 23200: heap size 350 MB, throughput 0.99751
Reading from 23200: heap size 351 MB, throughput 0.996379
Reading from 23199: heap size 1258 MB, throughput 0.976685
Reading from 23200: heap size 359 MB, throughput 0.998609
Reading from 23200: heap size 359 MB, throughput 0.998103
Equal recommendation: 1852 MB each
Reading from 23199: heap size 1263 MB, throughput 0.978538
Reading from 23200: heap size 367 MB, throughput 0.998249
Reading from 23200: heap size 368 MB, throughput 0.993007
Reading from 23199: heap size 1270 MB, throughput 0.97239
Reading from 23200: heap size 377 MB, throughput 0.998496
Reading from 23199: heap size 1276 MB, throughput 0.97724
Reading from 23200: heap size 377 MB, throughput 0.998528
Equal recommendation: 1852 MB each
Reading from 23200: heap size 387 MB, throughput 0.996488
Reading from 23199: heap size 1279 MB, throughput 0.976008
Reading from 23200: heap size 388 MB, throughput 0.997477
Reading from 23200: heap size 401 MB, throughput 0.998111
Reading from 23199: heap size 1285 MB, throughput 0.97816
Reading from 23200: heap size 401 MB, throughput 0.997891
Equal recommendation: 1852 MB each
Reading from 23200: heap size 411 MB, throughput 0.997581
Reading from 23199: heap size 1287 MB, throughput 0.977642
Reading from 23200: heap size 412 MB, throughput 0.998143
Reading from 23199: heap size 1292 MB, throughput 0.97869
Reading from 23200: heap size 423 MB, throughput 0.99825
Reading from 23200: heap size 423 MB, throughput 0.996407
Equal recommendation: 1852 MB each
Reading from 23199: heap size 1292 MB, throughput 0.976109
Reading from 23200: heap size 432 MB, throughput 0.997684
Reading from 23200: heap size 433 MB, throughput 0.998053
Reading from 23199: heap size 1296 MB, throughput 0.976051
Reading from 23200: heap size 445 MB, throughput 0.998413
Reading from 23199: heap size 1297 MB, throughput 0.976628
Reading from 23200: heap size 445 MB, throughput 0.996539
Equal recommendation: 1852 MB each
Reading from 23200: heap size 456 MB, throughput 0.998465
Reading from 23199: heap size 1298 MB, throughput 0.978155
Reading from 23200: heap size 456 MB, throughput 0.998374
Reading from 23200: heap size 466 MB, throughput 0.995125
Reading from 23199: heap size 1299 MB, throughput 0.976949
Equal recommendation: 1852 MB each
Reading from 23200: heap size 467 MB, throughput 0.998357
Reading from 23199: heap size 1299 MB, throughput 0.976937
Reading from 23200: heap size 481 MB, throughput 0.998392
Reading from 23200: heap size 482 MB, throughput 0.998446
Reading from 23199: heap size 1301 MB, throughput 0.691696
Equal recommendation: 1852 MB each
Reading from 23200: heap size 492 MB, throughput 0.998234
Reading from 23199: heap size 1422 MB, throughput 0.970594
Reading from 23200: heap size 493 MB, throughput 0.998173
Reading from 23200: heap size 504 MB, throughput 0.996985
Reading from 23199: heap size 1423 MB, throughput 0.991965
Reading from 23200: heap size 505 MB, throughput 0.998249
Equal recommendation: 1852 MB each
Reading from 23199: heap size 1433 MB, throughput 0.989537
Reading from 23200: heap size 517 MB, throughput 0.998335
Reading from 23200: heap size 518 MB, throughput 0.998529
Reading from 23199: heap size 1438 MB, throughput 0.988104
Reading from 23200: heap size 528 MB, throughput 0.998258
Equal recommendation: 1852 MB each
Reading from 23200: heap size 529 MB, throughput 0.996245
Reading from 23199: heap size 1440 MB, throughput 0.988045
Reading from 23200: heap size 539 MB, throughput 0.99821
Reading from 23199: heap size 1442 MB, throughput 0.986301
Reading from 23200: heap size 540 MB, throughput 0.997769
Equal recommendation: 1852 MB each
Reading from 23199: heap size 1430 MB, throughput 0.984102
Reading from 23200: heap size 553 MB, throughput 0.998378
Reading from 23200: heap size 554 MB, throughput 0.998304
Reading from 23200: heap size 566 MB, throughput 0.997023
Equal recommendation: 1852 MB each
Reading from 23200: heap size 567 MB, throughput 0.998352
Reading from 23200: heap size 580 MB, throughput 0.998392
Reading from 23200: heap size 581 MB, throughput 0.892832
Reading from 23200: heap size 598 MB, throughput 0.999295
Equal recommendation: 1852 MB each
Reading from 23200: heap size 598 MB, throughput 0.999297
Reading from 23200: heap size 614 MB, throughput 0.997745
Reading from 23199: heap size 1338 MB, throughput 0.996456
Reading from 23200: heap size 616 MB, throughput 0.999039
Equal recommendation: 1852 MB each
Reading from 23200: heap size 632 MB, throughput 0.99897
Reading from 23199: heap size 1422 MB, throughput 0.972809
Reading from 23200: heap size 634 MB, throughput 0.999005
Reading from 23199: heap size 1434 MB, throughput 0.959004
Reading from 23199: heap size 1476 MB, throughput 0.751888
Reading from 23199: heap size 1488 MB, throughput 0.645823
Reading from 23199: heap size 1516 MB, throughput 0.673912
Reading from 23199: heap size 1542 MB, throughput 0.663156
Equal recommendation: 1852 MB each
Reading from 23199: heap size 1574 MB, throughput 0.68063
Reading from 23200: heap size 647 MB, throughput 0.998139
Reading from 23199: heap size 1591 MB, throughput 0.170526
Reading from 23199: heap size 1648 MB, throughput 0.991377
Reading from 23200: heap size 649 MB, throughput 0.998576
Reading from 23199: heap size 1653 MB, throughput 0.995026
Reading from 23200: heap size 665 MB, throughput 0.998675
Equal recommendation: 1852 MB each
Reading from 23199: heap size 1694 MB, throughput 0.992785
Reading from 23200: heap size 665 MB, throughput 0.99881
Reading from 23199: heap size 1696 MB, throughput 0.993375
Equal recommendation: 1852 MB each
Client 23200 died
Clients: 1
Reading from 23199: heap size 1698 MB, throughput 0.99193
Reading from 23199: heap size 1711 MB, throughput 0.990947
Recommendation: one client; give it all the memory
Reading from 23199: heap size 1686 MB, throughput 0.989749
Recommendation: one client; give it all the memory
Reading from 23199: heap size 1705 MB, throughput 0.989362
Reading from 23199: heap size 1667 MB, throughput 0.988581
Recommendation: one client; give it all the memory
Reading from 23199: heap size 1484 MB, throughput 0.987231
Reading from 23199: heap size 1646 MB, throughput 0.986758
Recommendation: one client; give it all the memory
Reading from 23199: heap size 1664 MB, throughput 0.985282
Reading from 23199: heap size 1651 MB, throughput 0.984753
Recommendation: one client; give it all the memory
Reading from 23199: heap size 1658 MB, throughput 0.984106
Recommendation: one client; give it all the memory
Reading from 23199: heap size 1665 MB, throughput 0.983712
Reading from 23199: heap size 1666 MB, throughput 0.981654
Recommendation: one client; give it all the memory
Reading from 23199: heap size 1673 MB, throughput 0.980016
Reading from 23199: heap size 1680 MB, throughput 0.981364
Recommendation: one client; give it all the memory
Reading from 23199: heap size 1691 MB, throughput 0.980879
Recommendation: one client; give it all the memory
Reading from 23199: heap size 1695 MB, throughput 0.980108
Reading from 23199: heap size 1705 MB, throughput 0.980396
Recommendation: one client; give it all the memory
Reading from 23199: heap size 1708 MB, throughput 0.980448
Recommendation: one client; give it all the memory
Reading from 23199: heap size 1719 MB, throughput 0.980448
Reading from 23199: heap size 1720 MB, throughput 0.979724
Recommendation: one client; give it all the memory
Reading from 23199: heap size 1731 MB, throughput 0.980424
Recommendation: one client; give it all the memory
Reading from 23199: heap size 1731 MB, throughput 0.981074
Reading from 23199: heap size 1741 MB, throughput 0.980458
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 23199: heap size 1741 MB, throughput 0.92305
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 23199: heap size 1848 MB, throughput 0.991754
Reading from 23199: heap size 1856 MB, throughput 0.982249
Reading from 23199: heap size 1838 MB, throughput 0.865224
Reading from 23199: heap size 1848 MB, throughput 0.848659
Reading from 23199: heap size 1876 MB, throughput 0.859275
Client 23199 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
