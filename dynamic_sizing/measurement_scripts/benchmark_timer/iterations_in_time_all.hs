{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, DeriveDataTypeable #-}
{-# OPTIONS_GHC -fno-warn-missing-fields -fno-warn-missing-methods #-}

import Economem.ArgHelper
import Economem.Common
import Economem.Files
import Economem.Records
import System.Console.CmdLib

data Args = Args { time :: String,
                   inputFile :: FilePath,
                   outputFile :: FilePath }
          deriving (Typeable, Data, Eq, Show)

instance Attributes Args where
  attributes _ = group "Options" [
    time %> [ Positional 0,
              Required True,
              Help "How many iterations are in this much time?",
              ArgHelp "TIME"],
    inputFile %> [ Positional 1,
                   Required True,
                   Help "File containing iteration lengths",
                   ArgHelp "INPUT_FILE" ],
    outputFile %> [ Positional 2,
                    Required True,
                    Help "File to save to",
                    ArgHelp "OUTPUT_FILE" ]]


instance RecordCommand Args where
  mode_summary _ = "Calculate the number of iterations all benchmarks in the input file should be run for to last for the given time."

main :: IO ()
main = getArgs >>= executeR Args {} >>= \opts -> do
  (seconds, input, output) <- validate opts
  bms <- get (CsvFile input)
  put (CsvFile output :: CsvFile BenchmarkThreadsIters) (iterationsInTimeAll seconds bms)

  where
    validate :: Args -> IO (Integer, FilePath, FilePath)
    validate a =
      do input <- existingAbsoluteFile (inputFile a)
         output <- absolutePath (outputFile a)
         t <- timeToSeconds (time a)
         return (t, input, output)
