economemd
    total memory: 828 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_30_14_13_41/sub3_timerComparisonSingle/sub3_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 571: heap size 9 MB, throughput 0.99196
Clients: 1
Client 571 has a minimum heap size of 276 MB
Reading from 572: heap size 9 MB, throughput 0.990808
Clients: 2
Client 572 has a minimum heap size of 276 MB
Reading from 572: heap size 9 MB, throughput 0.978157
Reading from 571: heap size 9 MB, throughput 0.977598
Reading from 572: heap size 9 MB, throughput 0.959438
Reading from 571: heap size 9 MB, throughput 0.953926
Reading from 572: heap size 9 MB, throughput 0.929787
Reading from 571: heap size 9 MB, throughput 0.958756
Reading from 572: heap size 11 MB, throughput 0.9864
Reading from 571: heap size 11 MB, throughput 0.968858
Reading from 572: heap size 11 MB, throughput 0.942158
Reading from 571: heap size 11 MB, throughput 0.988938
Reading from 572: heap size 17 MB, throughput 0.945379
Reading from 571: heap size 17 MB, throughput 0.947688
Reading from 571: heap size 17 MB, throughput 0.659694
Reading from 572: heap size 17 MB, throughput 0.381151
Reading from 571: heap size 30 MB, throughput 0.956813
Reading from 572: heap size 30 MB, throughput 0.961587
Reading from 571: heap size 31 MB, throughput 0.867173
Reading from 572: heap size 31 MB, throughput 0.895408
Reading from 571: heap size 34 MB, throughput 0.309761
Reading from 572: heap size 35 MB, throughput 0.509149
Reading from 572: heap size 48 MB, throughput 0.833208
Reading from 571: heap size 48 MB, throughput 0.91482
Reading from 572: heap size 53 MB, throughput 0.851485
Reading from 571: heap size 50 MB, throughput 0.9013
Reading from 571: heap size 53 MB, throughput 0.269863
Reading from 572: heap size 54 MB, throughput 0.279053
Reading from 571: heap size 75 MB, throughput 0.796722
Reading from 572: heap size 73 MB, throughput 0.277007
Reading from 572: heap size 95 MB, throughput 0.837602
Reading from 571: heap size 75 MB, throughput 0.285497
Reading from 572: heap size 97 MB, throughput 0.882097
Reading from 571: heap size 98 MB, throughput 0.79767
Reading from 572: heap size 99 MB, throughput 0.630349
Reading from 572: heap size 100 MB, throughput 0.73352
Reading from 571: heap size 101 MB, throughput 0.890535
Reading from 572: heap size 104 MB, throughput 0.801305
Reading from 571: heap size 103 MB, throughput 0.234155
Reading from 572: heap size 108 MB, throughput 0.155505
Reading from 571: heap size 133 MB, throughput 0.80684
Reading from 572: heap size 137 MB, throughput 0.682979
Reading from 571: heap size 136 MB, throughput 0.852978
Reading from 572: heap size 142 MB, throughput 0.74028
Reading from 571: heap size 138 MB, throughput 0.80612
Reading from 572: heap size 145 MB, throughput 0.646098
Reading from 572: heap size 154 MB, throughput 0.640329
Reading from 572: heap size 156 MB, throughput 0.567076
Reading from 571: heap size 146 MB, throughput 0.230544
Reading from 571: heap size 178 MB, throughput 0.561626
Reading from 572: heap size 159 MB, throughput 0.177881
Reading from 571: heap size 187 MB, throughput 0.808939
Reading from 572: heap size 194 MB, throughput 0.671275
Reading from 572: heap size 201 MB, throughput 0.680965
Reading from 571: heap size 188 MB, throughput 0.617422
Reading from 572: heap size 205 MB, throughput 0.901719
Reading from 571: heap size 190 MB, throughput 0.295739
Reading from 572: heap size 207 MB, throughput 0.843909
Reading from 572: heap size 216 MB, throughput 0.81154
Reading from 572: heap size 222 MB, throughput 0.56381
Reading from 571: heap size 233 MB, throughput 0.92591
Reading from 571: heap size 235 MB, throughput 0.729239
Reading from 571: heap size 241 MB, throughput 0.772419
Reading from 572: heap size 229 MB, throughput 0.233314
Reading from 571: heap size 248 MB, throughput 0.777346
Reading from 572: heap size 270 MB, throughput 0.411766
Reading from 571: heap size 252 MB, throughput 0.675541
Reading from 571: heap size 258 MB, throughput 0.861862
Reading from 572: heap size 270 MB, throughput 0.146434
Reading from 572: heap size 314 MB, throughput 0.772268
Reading from 571: heap size 261 MB, throughput 0.838498
Reading from 572: heap size 316 MB, throughput 0.928631
Reading from 571: heap size 264 MB, throughput 0.596366
Reading from 572: heap size 316 MB, throughput 0.89504
Reading from 571: heap size 270 MB, throughput 0.596816
Reading from 572: heap size 318 MB, throughput 0.832361
Reading from 571: heap size 277 MB, throughput 0.71896
Reading from 572: heap size 311 MB, throughput 0.830739
Reading from 571: heap size 280 MB, throughput 0.648031
Reading from 572: heap size 254 MB, throughput 0.744761
Reading from 572: heap size 308 MB, throughput 0.803061
Reading from 571: heap size 288 MB, throughput 0.726047
Reading from 572: heap size 311 MB, throughput 0.809034
Reading from 572: heap size 305 MB, throughput 0.822997
Reading from 571: heap size 289 MB, throughput 0.92917
Reading from 572: heap size 308 MB, throughput 0.935979
Reading from 571: heap size 296 MB, throughput 0.651765
Reading from 572: heap size 302 MB, throughput 0.819783
Reading from 571: heap size 297 MB, throughput 0.743158
Reading from 572: heap size 306 MB, throughput 0.842327
Reading from 572: heap size 302 MB, throughput 0.773133
Reading from 572: heap size 305 MB, throughput 0.826782
Reading from 572: heap size 301 MB, throughput 0.873747
Reading from 572: heap size 304 MB, throughput 0.940729
Reading from 572: heap size 303 MB, throughput 0.779961
Reading from 572: heap size 305 MB, throughput 0.622074
Reading from 571: heap size 294 MB, throughput 0.0874044
Reading from 572: heap size 309 MB, throughput 0.725098
Reading from 572: heap size 310 MB, throughput 0.718115
Reading from 571: heap size 344 MB, throughput 0.692682
Reading from 572: heap size 316 MB, throughput 0.610104
Reading from 572: heap size 317 MB, throughput 0.555623
Reading from 572: heap size 323 MB, throughput 0.634644
Reading from 571: heap size 346 MB, throughput 0.919917
Reading from 571: heap size 348 MB, throughput 0.700129
Reading from 571: heap size 350 MB, throughput 0.663935
Reading from 571: heap size 351 MB, throughput 0.649659
Reading from 571: heap size 357 MB, throughput 0.715033
Equal recommendation: 414 MB each
Reading from 571: heap size 357 MB, throughput 0.585524
Reading from 572: heap size 325 MB, throughput 0.963313
Reading from 571: heap size 366 MB, throughput 0.709264
Reading from 572: heap size 329 MB, throughput 0.980779
Reading from 571: heap size 366 MB, throughput 0.984392
Reading from 572: heap size 331 MB, throughput 0.955809
Reading from 571: heap size 369 MB, throughput 0.980582
Reading from 572: heap size 335 MB, throughput 0.957599
Reading from 571: heap size 372 MB, throughput 0.976921
Reading from 572: heap size 336 MB, throughput 0.975287
Reading from 572: heap size 333 MB, throughput 0.966165
Reading from 571: heap size 371 MB, throughput 0.970633
Reading from 572: heap size 336 MB, throughput 0.961881
Reading from 571: heap size 375 MB, throughput 0.976941
Reading from 572: heap size 331 MB, throughput 0.967586
Reading from 571: heap size 378 MB, throughput 0.973901
Reading from 572: heap size 334 MB, throughput 0.974884
Equal recommendation: 414 MB each
Reading from 571: heap size 380 MB, throughput 0.980828
Reading from 572: heap size 333 MB, throughput 0.973446
Reading from 571: heap size 377 MB, throughput 0.976207
Reading from 572: heap size 334 MB, throughput 0.97031
Reading from 571: heap size 380 MB, throughput 0.959143
Reading from 572: heap size 337 MB, throughput 0.972665
Reading from 572: heap size 337 MB, throughput 0.980417
Reading from 571: heap size 377 MB, throughput 0.978363
Reading from 572: heap size 338 MB, throughput 0.97321
Reading from 571: heap size 379 MB, throughput 0.975253
Reading from 572: heap size 339 MB, throughput 0.971789
Reading from 571: heap size 382 MB, throughput 0.968788
Reading from 572: heap size 341 MB, throughput 0.973044
Reading from 571: heap size 382 MB, throughput 0.971742
Reading from 572: heap size 342 MB, throughput 0.985978
Equal recommendation: 414 MB each
Reading from 572: heap size 342 MB, throughput 0.972839
Reading from 572: heap size 344 MB, throughput 0.813311
Reading from 572: heap size 338 MB, throughput 0.783467
Reading from 572: heap size 344 MB, throughput 0.77354
Reading from 572: heap size 350 MB, throughput 0.683644
Reading from 572: heap size 352 MB, throughput 0.834607
Reading from 571: heap size 384 MB, throughput 0.97785
Reading from 571: heap size 386 MB, throughput 0.905108
Reading from 571: heap size 383 MB, throughput 0.746483
Reading from 571: heap size 387 MB, throughput 0.835734
Reading from 571: heap size 395 MB, throughput 0.859078
Reading from 572: heap size 359 MB, throughput 0.723765
Reading from 571: heap size 396 MB, throughput 0.98756
Reading from 572: heap size 391 MB, throughput 0.992495
Reading from 572: heap size 393 MB, throughput 0.983285
Reading from 571: heap size 400 MB, throughput 0.769322
Reading from 572: heap size 396 MB, throughput 0.990401
Reading from 571: heap size 432 MB, throughput 0.99453
Reading from 572: heap size 401 MB, throughput 0.98888
Reading from 571: heap size 428 MB, throughput 0.99217
Equal recommendation: 414 MB each
Reading from 572: heap size 402 MB, throughput 0.987742
Reading from 571: heap size 372 MB, throughput 0.989768
Reading from 572: heap size 400 MB, throughput 0.990388
Reading from 571: heap size 432 MB, throughput 0.984391
Reading from 572: heap size 402 MB, throughput 0.985293
Reading from 571: heap size 377 MB, throughput 0.987295
Reading from 572: heap size 395 MB, throughput 0.984258
Reading from 572: heap size 399 MB, throughput 0.984332
Reading from 571: heap size 429 MB, throughput 0.986148
Reading from 572: heap size 396 MB, throughput 0.973751
Reading from 571: heap size 385 MB, throughput 0.985058
Reading from 572: heap size 398 MB, throughput 0.975675
Reading from 571: heap size 422 MB, throughput 0.969218
Equal recommendation: 414 MB each
Reading from 572: heap size 399 MB, throughput 0.978417
Reading from 571: heap size 392 MB, throughput 0.979436
Reading from 572: heap size 400 MB, throughput 0.979722
Reading from 572: heap size 403 MB, throughput 0.819376
Reading from 572: heap size 405 MB, throughput 0.776182
Reading from 572: heap size 411 MB, throughput 0.818952
Reading from 572: heap size 415 MB, throughput 0.808498
Reading from 571: heap size 419 MB, throughput 0.98488
Reading from 571: heap size 395 MB, throughput 0.833192
Reading from 571: heap size 412 MB, throughput 0.856498
Reading from 571: heap size 416 MB, throughput 0.791272
Reading from 571: heap size 414 MB, throughput 0.825221
Reading from 572: heap size 401 MB, throughput 0.990274
Reading from 571: heap size 382 MB, throughput 0.985907
Reading from 572: heap size 414 MB, throughput 0.985428
Reading from 571: heap size 420 MB, throughput 0.984975
Reading from 572: heap size 412 MB, throughput 0.981958
Reading from 571: heap size 383 MB, throughput 0.984366
Reading from 572: heap size 416 MB, throughput 0.981539
Reading from 571: heap size 416 MB, throughput 0.980216
Equal recommendation: 414 MB each
Reading from 572: heap size 400 MB, throughput 0.981511
Reading from 571: heap size 385 MB, throughput 0.983698
Reading from 572: heap size 409 MB, throughput 0.980899
Reading from 572: heap size 406 MB, throughput 0.971376
Reading from 571: heap size 411 MB, throughput 0.982174
Reading from 572: heap size 408 MB, throughput 0.981541
Reading from 571: heap size 410 MB, throughput 0.975834
Reading from 572: heap size 406 MB, throughput 0.977943
Reading from 571: heap size 408 MB, throughput 0.987338
Reading from 572: heap size 408 MB, throughput 0.980909
Reading from 571: heap size 410 MB, throughput 0.979078
Reading from 572: heap size 410 MB, throughput 0.981343
Reading from 571: heap size 411 MB, throughput 0.983231
Equal recommendation: 414 MB each
Reading from 571: heap size 411 MB, throughput 0.979229
Reading from 572: heap size 410 MB, throughput 0.985911
Reading from 572: heap size 413 MB, throughput 0.919972
Reading from 572: heap size 413 MB, throughput 0.839546
Reading from 572: heap size 416 MB, throughput 0.864069
Reading from 572: heap size 406 MB, throughput 0.879851
Reading from 571: heap size 413 MB, throughput 0.987503
Reading from 572: heap size 417 MB, throughput 0.988816
Reading from 571: heap size 413 MB, throughput 0.96423
Reading from 571: heap size 413 MB, throughput 0.855202
Reading from 571: heap size 414 MB, throughput 0.843135
Reading from 571: heap size 411 MB, throughput 0.879206
Reading from 572: heap size 384 MB, throughput 0.986998
Reading from 571: heap size 416 MB, throughput 0.979953
Reading from 572: heap size 416 MB, throughput 0.987254
Reading from 571: heap size 407 MB, throughput 0.988325
Reading from 572: heap size 386 MB, throughput 0.989763
Reading from 571: heap size 416 MB, throughput 0.987272
Equal recommendation: 414 MB each
Reading from 572: heap size 414 MB, throughput 0.984747
Reading from 571: heap size 406 MB, throughput 0.985295
Reading from 572: heap size 412 MB, throughput 0.985268
Reading from 571: heap size 414 MB, throughput 0.983727
Reading from 572: heap size 410 MB, throughput 0.983665
Reading from 571: heap size 414 MB, throughput 0.982151
Reading from 572: heap size 412 MB, throughput 0.984455
Reading from 571: heap size 415 MB, throughput 0.982546
Reading from 571: heap size 402 MB, throughput 0.980373
Reading from 572: heap size 412 MB, throughput 0.979895
Reading from 572: heap size 413 MB, throughput 0.969466
Reading from 571: heap size 410 MB, throughput 0.978014
Equal recommendation: 414 MB each
Reading from 571: heap size 411 MB, throughput 0.978558
Reading from 572: heap size 415 MB, throughput 0.991268
Reading from 572: heap size 392 MB, throughput 0.967631
Reading from 571: heap size 412 MB, throughput 0.978028
Reading from 572: heap size 414 MB, throughput 0.871688
Reading from 572: heap size 411 MB, throughput 0.864903
Reading from 572: heap size 411 MB, throughput 0.856417
Reading from 572: heap size 413 MB, throughput 0.985895
Reading from 571: heap size 414 MB, throughput 0.98898
Reading from 571: heap size 391 MB, throughput 0.904452
Reading from 571: heap size 411 MB, throughput 0.858295
Reading from 571: heap size 413 MB, throughput 0.822752
Reading from 571: heap size 420 MB, throughput 0.888185
Reading from 572: heap size 421 MB, throughput 0.987603
Reading from 571: heap size 389 MB, throughput 0.990018
Reading from 572: heap size 390 MB, throughput 0.991041
Reading from 571: heap size 420 MB, throughput 0.98903
Reading from 572: heap size 420 MB, throughput 0.986858
Equal recommendation: 414 MB each
Reading from 571: heap size 389 MB, throughput 0.985785
Reading from 572: heap size 392 MB, throughput 0.981687
Reading from 571: heap size 418 MB, throughput 0.984111
Reading from 572: heap size 417 MB, throughput 0.982261
Reading from 571: heap size 391 MB, throughput 0.984887
Reading from 572: heap size 394 MB, throughput 0.982488
Reading from 571: heap size 415 MB, throughput 0.982416
Reading from 572: heap size 412 MB, throughput 0.983117
Reading from 571: heap size 394 MB, throughput 0.981747
Reading from 572: heap size 411 MB, throughput 0.981838
Reading from 571: heap size 412 MB, throughput 0.982653
Reading from 572: heap size 413 MB, throughput 0.981175
Reading from 571: heap size 410 MB, throughput 0.977577
Equal recommendation: 414 MB each
Reading from 571: heap size 413 MB, throughput 0.978663
Reading from 572: heap size 413 MB, throughput 0.990316
Reading from 572: heap size 416 MB, throughput 0.97814
Reading from 572: heap size 394 MB, throughput 0.86429
Reading from 572: heap size 410 MB, throughput 0.863705
Reading from 572: heap size 413 MB, throughput 0.86141
Reading from 571: heap size 413 MB, throughput 0.976514
Reading from 572: heap size 420 MB, throughput 0.987278
Reading from 571: heap size 414 MB, throughput 0.9841
Reading from 571: heap size 417 MB, throughput 0.863125
Reading from 571: heap size 415 MB, throughput 0.879684
Reading from 571: heap size 406 MB, throughput 0.850831
Reading from 572: heap size 388 MB, throughput 0.988566
Reading from 571: heap size 416 MB, throughput 0.971356
Reading from 571: heap size 381 MB, throughput 0.984832
Reading from 572: heap size 419 MB, throughput 0.988726
Reading from 571: heap size 415 MB, throughput 0.989165
Reading from 572: heap size 389 MB, throughput 0.989973
Equal recommendation: 414 MB each
Reading from 571: heap size 382 MB, throughput 0.98615
Reading from 572: heap size 416 MB, throughput 0.986488
Reading from 571: heap size 413 MB, throughput 0.989498
Reading from 572: heap size 391 MB, throughput 0.984551
Reading from 571: heap size 412 MB, throughput 0.981167
Reading from 572: heap size 412 MB, throughput 0.982139
Reading from 571: heap size 409 MB, throughput 0.987389
Reading from 572: heap size 411 MB, throughput 0.978709
Reading from 571: heap size 412 MB, throughput 0.984847
Reading from 572: heap size 412 MB, throughput 0.980262
Reading from 571: heap size 409 MB, throughput 0.981706
Reading from 572: heap size 412 MB, throughput 0.979623
Equal recommendation: 414 MB each
Reading from 571: heap size 411 MB, throughput 0.979245
Reading from 572: heap size 414 MB, throughput 0.986971
Reading from 572: heap size 389 MB, throughput 0.936231
Reading from 571: heap size 413 MB, throughput 0.966052
Reading from 572: heap size 413 MB, throughput 0.863405
Reading from 572: heap size 413 MB, throughput 0.857736
Reading from 572: heap size 418 MB, throughput 0.84727
Reading from 572: heap size 409 MB, throughput 0.984815
Reading from 571: heap size 413 MB, throughput 0.978686
Reading from 571: heap size 413 MB, throughput 0.979303
Reading from 571: heap size 417 MB, throughput 0.868591
Reading from 571: heap size 414 MB, throughput 0.693472
Reading from 572: heap size 420 MB, throughput 0.983263
Reading from 571: heap size 405 MB, throughput 0.879606
Reading from 571: heap size 415 MB, throughput 0.977556
Reading from 572: heap size 387 MB, throughput 0.989526
Reading from 571: heap size 381 MB, throughput 0.987509
Reading from 572: heap size 418 MB, throughput 0.985761
Reading from 571: heap size 415 MB, throughput 0.984968
Equal recommendation: 414 MB each
Reading from 572: heap size 389 MB, throughput 0.979786
Reading from 571: heap size 382 MB, throughput 0.983034
Reading from 572: heap size 414 MB, throughput 0.987085
Reading from 571: heap size 414 MB, throughput 0.982785
Reading from 572: heap size 391 MB, throughput 0.9809
Reading from 571: heap size 412 MB, throughput 0.983166
Reading from 572: heap size 409 MB, throughput 0.982508
Reading from 571: heap size 411 MB, throughput 0.982948
Reading from 571: heap size 413 MB, throughput 0.981445
Reading from 572: heap size 408 MB, throughput 0.981258
Reading from 572: heap size 410 MB, throughput 0.978323
Reading from 571: heap size 410 MB, throughput 0.982359
Equal recommendation: 414 MB each
Reading from 572: heap size 410 MB, throughput 0.974907
Reading from 571: heap size 412 MB, throughput 0.981014
Reading from 572: heap size 410 MB, throughput 0.985871
Reading from 572: heap size 414 MB, throughput 0.888634
Reading from 572: heap size 414 MB, throughput 0.852947
Reading from 572: heap size 404 MB, throughput 0.849875
Reading from 571: heap size 415 MB, throughput 0.983751
Reading from 572: heap size 414 MB, throughput 0.976287
Reading from 572: heap size 379 MB, throughput 0.98932
Reading from 571: heap size 404 MB, throughput 0.990782
Reading from 571: heap size 414 MB, throughput 0.98035
Reading from 571: heap size 414 MB, throughput 0.884017
Reading from 571: heap size 413 MB, throughput 0.877813
Reading from 571: heap size 416 MB, throughput 0.850062
Reading from 572: heap size 414 MB, throughput 0.989958
Reading from 571: heap size 411 MB, throughput 0.983131
Reading from 572: heap size 381 MB, throughput 0.986414
Reading from 571: heap size 418 MB, throughput 0.98797
Reading from 572: heap size 412 MB, throughput 0.98517
Equal recommendation: 414 MB each
Reading from 571: heap size 406 MB, throughput 0.986138
Reading from 572: heap size 411 MB, throughput 0.984272
Reading from 571: heap size 415 MB, throughput 0.985996
Reading from 572: heap size 408 MB, throughput 0.985007
Reading from 571: heap size 404 MB, throughput 0.981564
Reading from 572: heap size 410 MB, throughput 0.981564
Reading from 571: heap size 411 MB, throughput 0.975154
Reading from 572: heap size 409 MB, throughput 0.96363
Reading from 571: heap size 410 MB, throughput 0.974589
Reading from 572: heap size 410 MB, throughput 0.977387
Reading from 571: heap size 411 MB, throughput 0.980765
Reading from 572: heap size 412 MB, throughput 0.979429
Reading from 571: heap size 411 MB, throughput 0.981144
Equal recommendation: 414 MB each
Reading from 572: heap size 412 MB, throughput 0.983115
Reading from 572: heap size 415 MB, throughput 0.958201
Reading from 572: heap size 408 MB, throughput 0.830799
Reading from 572: heap size 414 MB, throughput 0.864802
Reading from 572: heap size 415 MB, throughput 0.897808
Reading from 571: heap size 412 MB, throughput 0.98095
Reading from 572: heap size 406 MB, throughput 0.991277
Reading from 571: heap size 415 MB, throughput 0.980349
Reading from 572: heap size 415 MB, throughput 0.984555
Reading from 571: heap size 405 MB, throughput 0.984302
Reading from 571: heap size 414 MB, throughput 0.952152
Reading from 571: heap size 414 MB, throughput 0.853971
Reading from 571: heap size 413 MB, throughput 0.788122
Reading from 572: heap size 405 MB, throughput 0.977548
Reading from 571: heap size 416 MB, throughput 0.878095
Reading from 571: heap size 405 MB, throughput 0.987511
Reading from 572: heap size 413 MB, throughput 0.985906
Reading from 571: heap size 416 MB, throughput 0.987532
Reading from 572: heap size 412 MB, throughput 0.982673
Equal recommendation: 414 MB each
Reading from 571: heap size 404 MB, throughput 0.985673
Reading from 572: heap size 414 MB, throughput 0.982324
Reading from 571: heap size 414 MB, throughput 0.985226
Reading from 572: heap size 411 MB, throughput 0.983982
Reading from 571: heap size 414 MB, throughput 0.98339
Reading from 572: heap size 413 MB, throughput 0.98017
Reading from 571: heap size 416 MB, throughput 0.985015
Reading from 572: heap size 414 MB, throughput 0.983163
Reading from 571: heap size 403 MB, throughput 0.983729
Reading from 572: heap size 414 MB, throughput 0.980891
Reading from 571: heap size 411 MB, throughput 0.980899
Equal recommendation: 414 MB each
Reading from 572: heap size 409 MB, throughput 0.990436
Reading from 571: heap size 411 MB, throughput 0.982911
Reading from 572: heap size 415 MB, throughput 0.979268
Reading from 572: heap size 408 MB, throughput 0.864637
Reading from 572: heap size 412 MB, throughput 0.857926
Reading from 572: heap size 418 MB, throughput 0.863833
Reading from 571: heap size 412 MB, throughput 0.984516
Reading from 572: heap size 408 MB, throughput 0.989744
Reading from 571: heap size 413 MB, throughput 0.849276
Reading from 572: heap size 419 MB, throughput 0.988144
Reading from 572: heap size 387 MB, throughput 0.986646
Reading from 571: heap size 433 MB, throughput 0.984494
Reading from 571: heap size 428 MB, throughput 0.911113
Reading from 571: heap size 421 MB, throughput 0.836527
Reading from 571: heap size 429 MB, throughput 0.837794
Reading from 571: heap size 417 MB, throughput 0.872037
Reading from 572: heap size 418 MB, throughput 0.988298
Reading from 571: heap size 426 MB, throughput 0.989767
Reading from 572: heap size 389 MB, throughput 0.880517
Reading from 571: heap size 394 MB, throughput 0.986645
Equal recommendation: 414 MB each
Reading from 572: heap size 425 MB, throughput 0.995407
Reading from 571: heap size 426 MB, throughput 0.985158
Reading from 572: heap size 421 MB, throughput 0.991987
Reading from 571: heap size 396 MB, throughput 0.976906
Reading from 572: heap size 422 MB, throughput 0.988466
Reading from 571: heap size 424 MB, throughput 0.984391
Reading from 572: heap size 380 MB, throughput 0.9733
Reading from 571: heap size 399 MB, throughput 0.967035
Reading from 572: heap size 421 MB, throughput 0.985639
Reading from 571: heap size 421 MB, throughput 0.989396
Reading from 572: heap size 387 MB, throughput 0.98187
Reading from 571: heap size 403 MB, throughput 0.984669
Equal recommendation: 414 MB each
Reading from 572: heap size 416 MB, throughput 0.987942
Reading from 572: heap size 396 MB, throughput 0.924585
Reading from 571: heap size 416 MB, throughput 0.982409
Reading from 572: heap size 408 MB, throughput 0.832782
Reading from 572: heap size 414 MB, throughput 0.797496
Reading from 572: heap size 422 MB, throughput 0.836657
Reading from 572: heap size 423 MB, throughput 0.981099
Reading from 571: heap size 405 MB, throughput 0.980045
Reading from 572: heap size 418 MB, throughput 0.984462
Reading from 571: heap size 414 MB, throughput 0.979312
Reading from 572: heap size 387 MB, throughput 0.985105
Reading from 571: heap size 404 MB, throughput 0.981285
Reading from 571: heap size 409 MB, throughput 0.973569
Reading from 572: heap size 420 MB, throughput 0.982442
Reading from 571: heap size 414 MB, throughput 0.827095
Reading from 571: heap size 413 MB, throughput 0.795409
Reading from 571: heap size 419 MB, throughput 0.755431
Reading from 571: heap size 416 MB, throughput 0.776928
Reading from 571: heap size 422 MB, throughput 0.964764
Reading from 572: heap size 391 MB, throughput 0.986292
Reading from 571: heap size 420 MB, throughput 0.983871
Reading from 572: heap size 415 MB, throughput 0.982031
Equal recommendation: 414 MB each
Reading from 571: heap size 383 MB, throughput 0.978264
Reading from 572: heap size 392 MB, throughput 0.98092
Reading from 571: heap size 421 MB, throughput 0.981847
Reading from 572: heap size 411 MB, throughput 0.983858
Reading from 571: heap size 384 MB, throughput 0.980272
Reading from 572: heap size 410 MB, throughput 0.985227
Reading from 571: heap size 420 MB, throughput 0.980255
Reading from 572: heap size 412 MB, throughput 0.985061
Reading from 571: heap size 385 MB, throughput 0.976922
Reading from 572: heap size 412 MB, throughput 0.972125
Reading from 571: heap size 416 MB, throughput 0.978184
Reading from 571: heap size 386 MB, throughput 0.97501
Reading from 572: heap size 414 MB, throughput 0.981694
Equal recommendation: 414 MB each
Reading from 571: heap size 413 MB, throughput 0.980545
Reading from 572: heap size 414 MB, throughput 0.985762
Reading from 572: heap size 416 MB, throughput 0.899988
Reading from 572: heap size 415 MB, throughput 0.806521
Reading from 572: heap size 415 MB, throughput 0.835957
Reading from 572: heap size 406 MB, throughput 0.86821
Reading from 571: heap size 412 MB, throughput 0.979502
Reading from 572: heap size 417 MB, throughput 0.982084
Reading from 571: heap size 410 MB, throughput 0.975902
Reading from 572: heap size 383 MB, throughput 0.985815
Reading from 571: heap size 412 MB, throughput 0.975389
Reading from 572: heap size 416 MB, throughput 0.984234
Reading from 571: heap size 411 MB, throughput 0.975891
Reading from 572: heap size 385 MB, throughput 0.985035
Reading from 571: heap size 412 MB, throughput 0.989785
Reading from 571: heap size 414 MB, throughput 0.891908
Reading from 571: heap size 414 MB, throughput 0.820976
Reading from 571: heap size 414 MB, throughput 0.824654
Reading from 571: heap size 415 MB, throughput 0.813671
Reading from 572: heap size 414 MB, throughput 0.985133
Reading from 571: heap size 415 MB, throughput 0.965001
Reading from 572: heap size 412 MB, throughput 0.983003
Reading from 571: heap size 381 MB, throughput 0.987414
Equal recommendation: 414 MB each
Reading from 572: heap size 410 MB, throughput 0.982374
Reading from 571: heap size 416 MB, throughput 0.982287
Reading from 571: heap size 382 MB, throughput 0.976428
Reading from 572: heap size 412 MB, throughput 0.967953
Reading from 571: heap size 414 MB, throughput 0.98438
Reading from 572: heap size 413 MB, throughput 0.981554
Reading from 571: heap size 384 MB, throughput 0.981215
Reading from 572: heap size 414 MB, throughput 0.985012
Reading from 571: heap size 412 MB, throughput 0.980683
Reading from 572: heap size 416 MB, throughput 0.976199
Reading from 571: heap size 411 MB, throughput 0.974658
Reading from 571: heap size 408 MB, throughput 0.973549
Reading from 572: heap size 405 MB, throughput 0.988262
Equal recommendation: 414 MB each
Reading from 572: heap size 415 MB, throughput 0.971691
Reading from 572: heap size 391 MB, throughput 0.859649
Reading from 572: heap size 409 MB, throughput 0.849364
Reading from 572: heap size 413 MB, throughput 0.843525
Reading from 571: heap size 411 MB, throughput 0.979518
Reading from 572: heap size 421 MB, throughput 0.964757
Reading from 571: heap size 410 MB, throughput 0.986318
Reading from 572: heap size 388 MB, throughput 0.990692
Reading from 571: heap size 411 MB, throughput 0.977591
Reading from 572: heap size 421 MB, throughput 0.987135
Reading from 571: heap size 412 MB, throughput 0.975677
Reading from 572: heap size 390 MB, throughput 0.983906
Reading from 571: heap size 412 MB, throughput 0.991506
Reading from 572: heap size 419 MB, throughput 0.98446
Reading from 571: heap size 415 MB, throughput 0.976959
Reading from 571: heap size 395 MB, throughput 0.833711
Reading from 571: heap size 409 MB, throughput 0.267864
Reading from 571: heap size 423 MB, throughput 0.991605
Reading from 571: heap size 421 MB, throughput 0.98618
Reading from 572: heap size 392 MB, throughput 0.981151
Reading from 571: heap size 426 MB, throughput 0.994146
Equal recommendation: 414 MB each
Reading from 572: heap size 416 MB, throughput 0.979665
Reading from 571: heap size 427 MB, throughput 0.991899
Reading from 572: heap size 395 MB, throughput 0.985761
Reading from 571: heap size 375 MB, throughput 0.989885
Reading from 572: heap size 412 MB, throughput 0.97857
Reading from 571: heap size 428 MB, throughput 0.986922
Reading from 572: heap size 411 MB, throughput 0.985021
Reading from 571: heap size 380 MB, throughput 0.988053
Reading from 572: heap size 413 MB, throughput 0.980866
Reading from 571: heap size 424 MB, throughput 0.984593
Reading from 572: heap size 413 MB, throughput 0.982673
Reading from 571: heap size 388 MB, throughput 0.983397
Reading from 571: heap size 420 MB, throughput 0.983451
Reading from 572: heap size 415 MB, throughput 0.98852
Reading from 572: heap size 392 MB, throughput 0.909829
Reading from 572: heap size 411 MB, throughput 0.837359
Reading from 572: heap size 414 MB, throughput 0.833753
Reading from 572: heap size 421 MB, throughput 0.919591
Equal recommendation: 414 MB each
Client 572 died
Clients: 1
Reading from 571: heap size 395 MB, throughput 0.984749
Reading from 571: heap size 417 MB, throughput 0.985093
Reading from 571: heap size 399 MB, throughput 0.984916
Reading from 571: heap size 415 MB, throughput 0.980021
Reading from 571: heap size 415 MB, throughput 0.981671
Reading from 571: heap size 415 MB, throughput 0.982507
Reading from 571: heap size 416 MB, throughput 0.967025
Reading from 571: heap size 414 MB, throughput 0.851398
Reading from 571: heap size 415 MB, throughput 0.809755
Reading from 571: heap size 416 MB, throughput 0.814505
Reading from 571: heap size 420 MB, throughput 0.818628
Client 571 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
