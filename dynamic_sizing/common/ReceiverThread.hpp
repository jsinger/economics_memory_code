#ifndef RECEIVERTHREAD_HPP
#define RECEIVERTHREAD_HPP

#include "RecvSock.hpp"
#include "Thread.hpp"
#include "Utilities.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
    //! Thread that reads from a RecvSock in a loop until killed.
    template <class T> class ReceiverThread : public economem::Thread
    {
    protected:
        ReceiverThread<T>(RecvSock<T>& receiver, const char* threadName) : Thread(), m_Receiver(receiver), m_ThreadName(threadName) {}

        //! Called whenever a message is received.
        virtual void received(const T& msg) = 0;


    private:
        virtual void threadFunc()
        {
            economem::info(m_ThreadName, "starting");

            while (isAlive())
            {
                T msg;
                if (m_Receiver.recv(msg))
                    received(msg);
            }

            economem::info(m_ThreadName, "finished");
        }

        virtual void onKill() EXCLUSIVE_LOCKS_REQUIRED(m_Lock)
        {
            m_Receiver.interrupt();
        }

        RecvSock<T>& m_Receiver;
        const char* m_ThreadName;
    };
}

/*! @} */

#endif
