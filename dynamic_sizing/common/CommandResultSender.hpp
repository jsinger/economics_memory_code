#ifndef COMMANDRESULTSENDER_HPP
#define COMMANDRESULTSENDER_HPP

#include "Messages.hpp"
#include "SendSock.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
    //! Send the result of a command from a daemon to a management program.
    class CommandResultSender : public SendSock
    {
    public:
        CommandResultSender();

        bool sendCommandResult(const economem::CommandResult& r, pid_t dest);
    };
}

/*! @} */

#endif
