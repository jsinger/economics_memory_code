economemd
    total memory: 2208 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub43_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run10_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 18880: heap size 9 MB, throughput 0.991205
Clients: 1
Client 18880 has a minimum heap size of 276 MB
Reading from 18879: heap size 9 MB, throughput 0.988813
Clients: 2
Client 18879 has a minimum heap size of 276 MB
Reading from 18880: heap size 9 MB, throughput 0.988075
Reading from 18879: heap size 9 MB, throughput 0.98408
Reading from 18880: heap size 9 MB, throughput 0.983086
Reading from 18880: heap size 9 MB, throughput 0.975408
Reading from 18879: heap size 11 MB, throughput 0.982141
Reading from 18880: heap size 11 MB, throughput 0.96825
Reading from 18879: heap size 11 MB, throughput 0.984212
Reading from 18880: heap size 11 MB, throughput 0.978815
Reading from 18879: heap size 15 MB, throughput 0.914546
Reading from 18880: heap size 17 MB, throughput 0.952555
Reading from 18879: heap size 19 MB, throughput 0.930731
Reading from 18880: heap size 17 MB, throughput 0.847069
Reading from 18879: heap size 23 MB, throughput 0.844548
Reading from 18880: heap size 30 MB, throughput 0.674636
Reading from 18879: heap size 26 MB, throughput 0.632079
Reading from 18880: heap size 31 MB, throughput 0.247831
Reading from 18879: heap size 36 MB, throughput 0.807248
Reading from 18879: heap size 39 MB, throughput 0.829966
Reading from 18880: heap size 33 MB, throughput 0.755208
Reading from 18879: heap size 39 MB, throughput 0.644704
Reading from 18880: heap size 46 MB, throughput 0.745957
Reading from 18879: heap size 40 MB, throughput 0.356594
Reading from 18880: heap size 50 MB, throughput 0.491712
Reading from 18879: heap size 42 MB, throughput 0.824772
Reading from 18879: heap size 44 MB, throughput 0.702883
Reading from 18880: heap size 50 MB, throughput 0.558281
Reading from 18879: heap size 47 MB, throughput 0.668148
Reading from 18880: heap size 70 MB, throughput 0.563638
Reading from 18879: heap size 65 MB, throughput 0.653377
Reading from 18879: heap size 70 MB, throughput 0.66142
Reading from 18880: heap size 71 MB, throughput 0.234692
Reading from 18880: heap size 74 MB, throughput 0.783029
Reading from 18879: heap size 71 MB, throughput 0.319105
Reading from 18879: heap size 95 MB, throughput 0.187885
Reading from 18879: heap size 95 MB, throughput 0.334666
Reading from 18880: heap size 78 MB, throughput 0.475284
Reading from 18879: heap size 98 MB, throughput 0.26506
Reading from 18880: heap size 109 MB, throughput 0.142185
Reading from 18879: heap size 99 MB, throughput 0.639464
Reading from 18880: heap size 109 MB, throughput 0.719022
Reading from 18880: heap size 112 MB, throughput 0.372471
Reading from 18879: heap size 106 MB, throughput 0.459046
Reading from 18880: heap size 141 MB, throughput 0.715228
Reading from 18879: heap size 131 MB, throughput 0.673581
Reading from 18880: heap size 147 MB, throughput 0.745892
Reading from 18879: heap size 138 MB, throughput 0.725972
Reading from 18880: heap size 148 MB, throughput 0.766117
Reading from 18879: heap size 139 MB, throughput 0.736962
Reading from 18880: heap size 151 MB, throughput 0.763278
Reading from 18880: heap size 156 MB, throughput 0.711166
Reading from 18879: heap size 141 MB, throughput 0.400822
Reading from 18879: heap size 175 MB, throughput 0.644945
Reading from 18879: heap size 180 MB, throughput 0.633351
Reading from 18880: heap size 159 MB, throughput 0.408952
Reading from 18879: heap size 183 MB, throughput 0.677537
Reading from 18880: heap size 197 MB, throughput 0.589827
Reading from 18879: heap size 187 MB, throughput 0.655213
Reading from 18880: heap size 204 MB, throughput 0.637592
Reading from 18879: heap size 194 MB, throughput 0.758132
Reading from 18880: heap size 205 MB, throughput 0.599859
Reading from 18879: heap size 197 MB, throughput 0.862212
Reading from 18880: heap size 208 MB, throughput 0.852475
Reading from 18879: heap size 203 MB, throughput 0.796988
Reading from 18879: heap size 208 MB, throughput 0.757222
Reading from 18880: heap size 213 MB, throughput 0.866502
Reading from 18879: heap size 216 MB, throughput 0.694782
Reading from 18880: heap size 218 MB, throughput 0.806753
Reading from 18879: heap size 221 MB, throughput 0.727536
Reading from 18879: heap size 226 MB, throughput 0.592804
Reading from 18880: heap size 225 MB, throughput 0.776063
Reading from 18880: heap size 231 MB, throughput 0.722573
Reading from 18880: heap size 235 MB, throughput 0.693918
Reading from 18880: heap size 241 MB, throughput 0.759869
Reading from 18879: heap size 231 MB, throughput 0.574951
Reading from 18879: heap size 269 MB, throughput 0.539433
Reading from 18880: heap size 244 MB, throughput 0.722282
Reading from 18880: heap size 248 MB, throughput 0.665516
Reading from 18880: heap size 250 MB, throughput 0.671157
Reading from 18879: heap size 265 MB, throughput 0.240083
Reading from 18879: heap size 314 MB, throughput 0.597717
Reading from 18879: heap size 315 MB, throughput 0.73441
Reading from 18879: heap size 316 MB, throughput 0.845675
Reading from 18880: heap size 255 MB, throughput 0.658022
Reading from 18879: heap size 314 MB, throughput 0.858739
Reading from 18879: heap size 316 MB, throughput 0.8293
Reading from 18880: heap size 290 MB, throughput 0.591068
Reading from 18879: heap size 310 MB, throughput 0.791276
Reading from 18880: heap size 287 MB, throughput 0.625192
Reading from 18879: heap size 313 MB, throughput 0.718071
Reading from 18879: heap size 308 MB, throughput 0.738014
Reading from 18879: heap size 311 MB, throughput 0.792308
Reading from 18880: heap size 291 MB, throughput 0.270309
Reading from 18880: heap size 340 MB, throughput 0.643944
Reading from 18879: heap size 304 MB, throughput 0.94569
Reading from 18879: heap size 256 MB, throughput 0.921162
Reading from 18879: heap size 299 MB, throughput 0.899291
Reading from 18879: heap size 303 MB, throughput 0.879377
Reading from 18879: heap size 297 MB, throughput 0.868543
Reading from 18880: heap size 340 MB, throughput 0.916135
Reading from 18879: heap size 300 MB, throughput 0.863276
Reading from 18880: heap size 335 MB, throughput 0.85429
Reading from 18879: heap size 294 MB, throughput 0.861481
Reading from 18880: heap size 340 MB, throughput 0.789849
Reading from 18880: heap size 334 MB, throughput 0.757405
Reading from 18880: heap size 338 MB, throughput 0.736954
Reading from 18879: heap size 297 MB, throughput 0.879352
Reading from 18879: heap size 296 MB, throughput 0.852677
Reading from 18880: heap size 335 MB, throughput 0.834401
Reading from 18879: heap size 298 MB, throughput 0.806066
Reading from 18879: heap size 301 MB, throughput 0.759438
Reading from 18879: heap size 303 MB, throughput 0.660395
Reading from 18880: heap size 338 MB, throughput 0.879835
Reading from 18879: heap size 308 MB, throughput 0.634487
Reading from 18880: heap size 338 MB, throughput 0.840696
Reading from 18879: heap size 309 MB, throughput 0.651051
Reading from 18880: heap size 340 MB, throughput 0.792482
Reading from 18879: heap size 315 MB, throughput 0.636341
Reading from 18880: heap size 346 MB, throughput 0.7579
Reading from 18879: heap size 317 MB, throughput 0.683547
Reading from 18880: heap size 346 MB, throughput 0.650708
Reading from 18880: heap size 346 MB, throughput 0.673948
Numeric result:
Recommendation: 2 clients, utility 0.457629:
    h1: 1483.23 MB (U(h) = 0.633953*h^0.00203098)
    h2: 724.767 MB (U(h) = 0.706571*h^0.001)
Recommendation: 2 clients, utility 0.457629:
    h1: 1479.52 MB (U(h) = 0.633953*h^0.00203098)
    h2: 728.477 MB (U(h) = 0.706571*h^0.001)
Reading from 18880: heap size 348 MB, throughput 0.683338
Reading from 18879: heap size 325 MB, throughput 0.955225
Reading from 18880: heap size 353 MB, throughput 0.952668
Reading from 18879: heap size 325 MB, throughput 0.965029
Reading from 18880: heap size 354 MB, throughput 0.966098
Reading from 18879: heap size 326 MB, throughput 0.962132
Reading from 18880: heap size 357 MB, throughput 0.963387
Reading from 18879: heap size 328 MB, throughput 0.964618
Reading from 18879: heap size 324 MB, throughput 0.970552
Reading from 18880: heap size 359 MB, throughput 0.969269
Reading from 18879: heap size 328 MB, throughput 0.968812
Reading from 18880: heap size 362 MB, throughput 0.973787
Reading from 18879: heap size 323 MB, throughput 0.971694
Reading from 18880: heap size 364 MB, throughput 0.974231
Reading from 18879: heap size 326 MB, throughput 0.970756
Reading from 18879: heap size 324 MB, throughput 0.967523
Numeric result:
Recommendation: 2 clients, utility 0.543962:
    h1: 1658.29 MB (U(h) = 0.519438*h^0.0521083)
    h2: 549.71 MB (U(h) = 0.638177*h^0.0172702)
Recommendation: 2 clients, utility 0.543962:
    h1: 1658.37 MB (U(h) = 0.519438*h^0.0521083)
    h2: 549.631 MB (U(h) = 0.638177*h^0.0172702)
Reading from 18880: heap size 361 MB, throughput 0.97894
Reading from 18879: heap size 325 MB, throughput 0.972927
Reading from 18880: heap size 364 MB, throughput 0.976023
Reading from 18879: heap size 325 MB, throughput 0.165475
Reading from 18880: heap size 360 MB, throughput 0.977291
Reading from 18879: heap size 326 MB, throughput 0.17261
Reading from 18880: heap size 362 MB, throughput 0.974007
Reading from 18879: heap size 328 MB, throughput 0.968023
Reading from 18880: heap size 363 MB, throughput 0.974451
Reading from 18879: heap size 328 MB, throughput 0.971499
Reading from 18879: heap size 330 MB, throughput 0.970316
Reading from 18880: heap size 364 MB, throughput 0.973496
Reading from 18879: heap size 331 MB, throughput 0.971054
Reading from 18880: heap size 366 MB, throughput 0.971795
Reading from 18879: heap size 334 MB, throughput 0.97138
Reading from 18880: heap size 367 MB, throughput 0.969691
Numeric result:
Recommendation: 2 clients, utility 0.596471:
    h1: 1831.95 MB (U(h) = 0.4658*h^0.0793855)
    h2: 376.046 MB (U(h) = 0.640348*h^0.0162882)
Recommendation: 2 clients, utility 0.596471:
    h1: 1832.09 MB (U(h) = 0.4658*h^0.0793855)
    h2: 375.906 MB (U(h) = 0.640348*h^0.0162882)
Reading from 18879: heap size 335 MB, throughput 0.970949
Reading from 18879: heap size 338 MB, throughput 0.955564
Reading from 18880: heap size 369 MB, throughput 0.969397
Reading from 18879: heap size 339 MB, throughput 0.923484
Reading from 18879: heap size 379 MB, throughput 0.830089
Reading from 18879: heap size 340 MB, throughput 0.777504
Reading from 18879: heap size 383 MB, throughput 0.758366
Reading from 18880: heap size 370 MB, throughput 0.935737
Reading from 18879: heap size 339 MB, throughput 0.903756
Reading from 18880: heap size 405 MB, throughput 0.913737
Reading from 18880: heap size 410 MB, throughput 0.896524
Reading from 18880: heap size 415 MB, throughput 0.898652
Reading from 18880: heap size 416 MB, throughput 0.956504
Reading from 18879: heap size 382 MB, throughput 0.960338
Reading from 18879: heap size 342 MB, throughput 0.973615
Reading from 18880: heap size 420 MB, throughput 0.98108
Reading from 18879: heap size 380 MB, throughput 0.974539
Reading from 18880: heap size 422 MB, throughput 0.984812
Reading from 18879: heap size 349 MB, throughput 0.976245
Reading from 18880: heap size 419 MB, throughput 0.986016
Reading from 18879: heap size 376 MB, throughput 0.971591
Numeric result:
Recommendation: 2 clients, utility 0.710256:
    h1: 1535.07 MB (U(h) = 0.411115*h^0.109671)
    h2: 672.935 MB (U(h) = 0.565014*h^0.0480796)
Recommendation: 2 clients, utility 0.710256:
    h1: 1535.04 MB (U(h) = 0.411115*h^0.109671)
    h2: 672.961 MB (U(h) = 0.565014*h^0.0480796)
Reading from 18879: heap size 354 MB, throughput 0.970899
Reading from 18880: heap size 422 MB, throughput 0.985302
Reading from 18879: heap size 373 MB, throughput 0.968419
Reading from 18880: heap size 417 MB, throughput 0.985913
Reading from 18879: heap size 373 MB, throughput 0.96803
Reading from 18880: heap size 420 MB, throughput 0.984178
Reading from 18879: heap size 375 MB, throughput 0.966434
Reading from 18879: heap size 376 MB, throughput 0.966379
Reading from 18880: heap size 421 MB, throughput 0.981317
Reading from 18879: heap size 378 MB, throughput 0.971154
Reading from 18880: heap size 421 MB, throughput 0.979072
Reading from 18879: heap size 379 MB, throughput 0.975615
Reading from 18880: heap size 423 MB, throughput 0.977133
Reading from 18879: heap size 376 MB, throughput 0.974777
Numeric result:
Recommendation: 2 clients, utility 0.75283:
    h1: 1464.8 MB (U(h) = 0.396401*h^0.118402)
    h2: 743.196 MB (U(h) = 0.538579*h^0.0600742)
Recommendation: 2 clients, utility 0.75283:
    h1: 1464.8 MB (U(h) = 0.396401*h^0.118402)
    h2: 743.204 MB (U(h) = 0.538579*h^0.0600742)
Reading from 18879: heap size 378 MB, throughput 0.973868
Reading from 18880: heap size 425 MB, throughput 0.977879
Reading from 18879: heap size 379 MB, throughput 0.970272
Reading from 18880: heap size 427 MB, throughput 0.974713
Reading from 18879: heap size 380 MB, throughput 0.975423
Reading from 18879: heap size 382 MB, throughput 0.963292
Reading from 18879: heap size 384 MB, throughput 0.933422
Reading from 18879: heap size 389 MB, throughput 0.900623
Reading from 18879: heap size 391 MB, throughput 0.868384
Reading from 18879: heap size 399 MB, throughput 0.862203
Reading from 18880: heap size 430 MB, throughput 0.978684
Reading from 18880: heap size 432 MB, throughput 0.970364
Reading from 18880: heap size 434 MB, throughput 0.947544
Reading from 18880: heap size 438 MB, throughput 0.920983
Reading from 18880: heap size 441 MB, throughput 0.95589
Reading from 18879: heap size 400 MB, throughput 0.974119
Reading from 18879: heap size 405 MB, throughput 0.980039
Reading from 18880: heap size 446 MB, throughput 0.982484
Reading from 18879: heap size 407 MB, throughput 0.981064
Reading from 18880: heap size 449 MB, throughput 0.984736
Reading from 18879: heap size 407 MB, throughput 0.978649
Numeric result:
Recommendation: 2 clients, utility 0.823573:
    h1: 1434.02 MB (U(h) = 0.367248*h^0.136497)
    h2: 773.975 MB (U(h) = 0.509455*h^0.0736575)
Recommendation: 2 clients, utility 0.823573:
    h1: 1434.11 MB (U(h) = 0.367248*h^0.136497)
    h2: 773.887 MB (U(h) = 0.509455*h^0.0736575)
Reading from 18880: heap size 448 MB, throughput 0.983873
Reading from 18879: heap size 410 MB, throughput 0.981017
Reading from 18880: heap size 451 MB, throughput 0.984502
Reading from 18879: heap size 409 MB, throughput 0.984471
Reading from 18880: heap size 448 MB, throughput 0.985731
Reading from 18879: heap size 411 MB, throughput 0.983712
Reading from 18880: heap size 450 MB, throughput 0.983694
Reading from 18879: heap size 408 MB, throughput 0.984803
Reading from 18880: heap size 447 MB, throughput 0.985798
Reading from 18879: heap size 410 MB, throughput 0.985537
Numeric result:
Recommendation: 2 clients, utility 0.858965:
    h1: 1403.62 MB (U(h) = 0.356642*h^0.143364)
    h2: 804.375 MB (U(h) = 0.491843*h^0.0821602)
Recommendation: 2 clients, utility 0.858965:
    h1: 1403.61 MB (U(h) = 0.356642*h^0.143364)
    h2: 804.39 MB (U(h) = 0.491843*h^0.0821602)
Reading from 18879: heap size 408 MB, throughput 0.982801
Reading from 18880: heap size 449 MB, throughput 0.985851
Reading from 18879: heap size 409 MB, throughput 0.981122
Reading from 18880: heap size 448 MB, throughput 0.983445
Reading from 18879: heap size 410 MB, throughput 0.985192
Reading from 18880: heap size 449 MB, throughput 0.987802
Reading from 18880: heap size 451 MB, throughput 0.980436
Reading from 18879: heap size 410 MB, throughput 0.982341
Reading from 18880: heap size 451 MB, throughput 0.961746
Reading from 18879: heap size 411 MB, throughput 0.96999
Reading from 18880: heap size 456 MB, throughput 0.94556
Reading from 18879: heap size 412 MB, throughput 0.94732
Reading from 18879: heap size 416 MB, throughput 0.922566
Reading from 18879: heap size 419 MB, throughput 0.975208
Reading from 18880: heap size 457 MB, throughput 0.981951
Reading from 18879: heap size 427 MB, throughput 0.982351
Reading from 18880: heap size 464 MB, throughput 0.98743
Numeric result:
Recommendation: 2 clients, utility 0.882901:
    h1: 1379.39 MB (U(h) = 0.350684*h^0.14728)
    h2: 828.608 MB (U(h) = 0.47906*h^0.0884727)
Recommendation: 2 clients, utility 0.882901:
    h1: 1379.39 MB (U(h) = 0.350684*h^0.14728)
    h2: 828.613 MB (U(h) = 0.47906*h^0.0884727)
Reading from 18879: heap size 428 MB, throughput 0.982973
Reading from 18880: heap size 465 MB, throughput 0.988534
Reading from 18879: heap size 429 MB, throughput 0.984666
Reading from 18880: heap size 467 MB, throughput 0.988686
Reading from 18879: heap size 431 MB, throughput 0.984912
Reading from 18880: heap size 469 MB, throughput 0.986821
Reading from 18879: heap size 429 MB, throughput 0.984514
Reading from 18880: heap size 467 MB, throughput 0.987401
Reading from 18879: heap size 431 MB, throughput 0.984643
Reading from 18880: heap size 470 MB, throughput 0.98663
Reading from 18879: heap size 430 MB, throughput 0.984356
Numeric result:
Recommendation: 2 clients, utility 0.912003:
    h1: 1360.1 MB (U(h) = 0.34262*h^0.152652)
    h2: 847.898 MB (U(h) = 0.465759*h^0.0951647)
Recommendation: 2 clients, utility 0.912003:
    h1: 1360.1 MB (U(h) = 0.34262*h^0.152652)
    h2: 847.898 MB (U(h) = 0.465759*h^0.0951647)
Reading from 18880: heap size 469 MB, throughput 0.981638
Reading from 18879: heap size 431 MB, throughput 0.970141
Reading from 18879: heap size 453 MB, throughput 0.988292
Reading from 18880: heap size 470 MB, throughput 0.986834
Reading from 18880: heap size 471 MB, throughput 0.982755
Reading from 18880: heap size 472 MB, throughput 0.970362
Reading from 18880: heap size 476 MB, throughput 0.9614
Reading from 18879: heap size 453 MB, throughput 0.988511
Reading from 18879: heap size 457 MB, throughput 0.979683
Reading from 18879: heap size 458 MB, throughput 0.966146
Reading from 18879: heap size 463 MB, throughput 0.933383
Reading from 18880: heap size 477 MB, throughput 0.984475
Reading from 18879: heap size 465 MB, throughput 0.976792
Reading from 18879: heap size 472 MB, throughput 0.987561
Numeric result:
Recommendation: 2 clients, utility 1.09464:
    h1: 928.735 MB (U(h) = 0.336579*h^0.156741)
    h2: 1279.27 MB (U(h) = 0.237789*h^0.215903)
Recommendation: 2 clients, utility 1.09464:
    h1: 928.726 MB (U(h) = 0.336579*h^0.156741)
    h2: 1279.27 MB (U(h) = 0.237789*h^0.215903)
Reading from 18880: heap size 482 MB, throughput 0.989771
Reading from 18879: heap size 474 MB, throughput 0.98848
Reading from 18880: heap size 484 MB, throughput 0.989312
Reading from 18879: heap size 475 MB, throughput 0.988301
Reading from 18880: heap size 485 MB, throughput 0.989049
Reading from 18879: heap size 478 MB, throughput 0.990737
Reading from 18880: heap size 487 MB, throughput 0.988953
Reading from 18879: heap size 477 MB, throughput 0.98739
Reading from 18880: heap size 486 MB, throughput 0.987638
Numeric result:
Recommendation: 2 clients, utility 1.3152:
    h1: 728.41 MB (U(h) = 0.329224*h^0.161777)
    h2: 1479.59 MB (U(h) = 0.124941*h^0.328606)
Recommendation: 2 clients, utility 1.3152:
    h1: 728.418 MB (U(h) = 0.329224*h^0.161777)
    h2: 1479.58 MB (U(h) = 0.124941*h^0.328606)
Reading from 18879: heap size 479 MB, throughput 0.986142
Reading from 18880: heap size 487 MB, throughput 0.987416
Reading from 18879: heap size 482 MB, throughput 0.985574
Reading from 18880: heap size 490 MB, throughput 0.991035
Reading from 18880: heap size 490 MB, throughput 0.98526
Reading from 18880: heap size 491 MB, throughput 0.974332
Reading from 18879: heap size 483 MB, throughput 0.985644
Reading from 18879: heap size 486 MB, throughput 0.981537
Reading from 18879: heap size 487 MB, throughput 0.972412
Reading from 18880: heap size 493 MB, throughput 0.979218
Reading from 18879: heap size 493 MB, throughput 0.961658
Reading from 18879: heap size 494 MB, throughput 0.985332
Reading from 18880: heap size 501 MB, throughput 0.988837
Numeric result:
Recommendation: 2 clients, utility 1.78858:
    h1: 529.837 MB (U(h) = 0.324584*h^0.16499)
    h2: 1678.16 MB (U(h) = 0.0404127*h^0.522572)
Recommendation: 2 clients, utility 1.78858:
    h1: 529.84 MB (U(h) = 0.324584*h^0.16499)
    h2: 1678.16 MB (U(h) = 0.0404127*h^0.522572)
Reading from 18879: heap size 499 MB, throughput 0.988751
Reading from 18880: heap size 501 MB, throughput 0.989721
Reading from 18879: heap size 501 MB, throughput 0.988966
Reading from 18880: heap size 502 MB, throughput 0.990285
Reading from 18879: heap size 501 MB, throughput 0.98913
Reading from 18880: heap size 504 MB, throughput 0.989403
Reading from 18879: heap size 503 MB, throughput 0.987151
Numeric result:
Recommendation: 2 clients, utility 1.63823:
    h1: 701.361 MB (U(h) = 0.254882*h^0.207175)
    h2: 1506.64 MB (U(h) = 0.0636934*h^0.445038)
Recommendation: 2 clients, utility 1.63823:
    h1: 701.37 MB (U(h) = 0.254882*h^0.207175)
    h2: 1506.63 MB (U(h) = 0.0636934*h^0.445038)
Reading from 18880: heap size 503 MB, throughput 0.988998
Reading from 18879: heap size 503 MB, throughput 0.987748
Reading from 18880: heap size 504 MB, throughput 0.988348
Reading from 18879: heap size 504 MB, throughput 0.986626
Reading from 18880: heap size 506 MB, throughput 0.991448
Reading from 18880: heap size 507 MB, throughput 0.985804
Reading from 18880: heap size 507 MB, throughput 0.974909
Reading from 18879: heap size 507 MB, throughput 0.989476
Reading from 18879: heap size 508 MB, throughput 0.982181
Reading from 18879: heap size 507 MB, throughput 0.970527
Reading from 18880: heap size 509 MB, throughput 0.983222
Reading from 18879: heap size 510 MB, throughput 0.984161
Numeric result:
Recommendation: 2 clients, utility 1.93344:
    h1: 853.183 MB (U(h) = 0.135815*h^0.315744)
    h2: 1354.82 MB (U(h) = 0.0454563*h^0.501402)
Recommendation: 2 clients, utility 1.93344:
    h1: 853.168 MB (U(h) = 0.135815*h^0.315744)
    h2: 1354.83 MB (U(h) = 0.0454563*h^0.501402)
Reading from 18880: heap size 516 MB, throughput 0.989939
Reading from 18879: heap size 517 MB, throughput 0.989331
Reading from 18880: heap size 517 MB, throughput 0.990592
Reading from 18879: heap size 518 MB, throughput 0.990491
Reading from 18880: heap size 518 MB, throughput 0.992086
Reading from 18879: heap size 520 MB, throughput 0.990811
Reading from 18880: heap size 520 MB, throughput 0.990894
Numeric result:
Recommendation: 2 clients, utility 2.08902:
    h1: 1085.79 MB (U(h) = 0.0626829*h^0.446452)
    h2: 1122.21 MB (U(h) = 0.0575591*h^0.46142)
Recommendation: 2 clients, utility 2.08902:
    h1: 1085.8 MB (U(h) = 0.0626829*h^0.446452)
    h2: 1122.2 MB (U(h) = 0.0575591*h^0.46142)
Reading from 18879: heap size 522 MB, throughput 0.989064
Reading from 18880: heap size 520 MB, throughput 0.989103
Reading from 18879: heap size 522 MB, throughput 0.987288
Reading from 18880: heap size 521 MB, throughput 0.988114
Reading from 18879: heap size 523 MB, throughput 0.986721
Reading from 18880: heap size 522 MB, throughput 0.987302
Reading from 18880: heap size 524 MB, throughput 0.980387
Reading from 18880: heap size 528 MB, throughput 0.980885
Reading from 18879: heap size 526 MB, throughput 0.988781
Reading from 18879: heap size 527 MB, throughput 0.982375
Reading from 18879: heap size 529 MB, throughput 0.975425
Numeric result:
Recommendation: 2 clients, utility 2.20678:
    h1: 998.851 MB (U(h) = 0.0663083*h^0.436741)
    h2: 1209.15 MB (U(h) = 0.0382377*h^0.528696)
Recommendation: 2 clients, utility 2.20678:
    h1: 998.848 MB (U(h) = 0.0663083*h^0.436741)
    h2: 1209.15 MB (U(h) = 0.0382377*h^0.528696)
Reading from 18880: heap size 529 MB, throughput 0.990327
Reading from 18879: heap size 531 MB, throughput 0.989763
Reading from 18879: heap size 535 MB, throughput 0.990519
Reading from 18880: heap size 532 MB, throughput 0.991071
Reading from 18880: heap size 534 MB, throughput 0.991942
Reading from 18879: heap size 537 MB, throughput 0.991163
Reading from 18880: heap size 532 MB, throughput 0.990033
Reading from 18879: heap size 536 MB, throughput 0.991828
Numeric result:
Recommendation: 2 clients, utility 2.21449:
    h1: 988.731 MB (U(h) = 0.0662788*h^0.436424)
    h2: 1219.27 MB (U(h) = 0.0359656*h^0.538184)
Recommendation: 2 clients, utility 2.21449:
    h1: 988.73 MB (U(h) = 0.0662788*h^0.436424)
    h2: 1219.27 MB (U(h) = 0.0359656*h^0.538184)
Reading from 18880: heap size 534 MB, throughput 0.989871
Reading from 18879: heap size 539 MB, throughput 0.990298
Reading from 18879: heap size 541 MB, throughput 0.989335
Reading from 18880: heap size 536 MB, throughput 0.991971
Reading from 18880: heap size 536 MB, throughput 0.986644
Reading from 18880: heap size 536 MB, throughput 0.977205
Reading from 18879: heap size 541 MB, throughput 0.991248
Reading from 18879: heap size 544 MB, throughput 0.986754
Reading from 18879: heap size 545 MB, throughput 0.979508
Reading from 18880: heap size 538 MB, throughput 0.98565
Numeric result:
Recommendation: 2 clients, utility 1.9581:
    h1: 1290.74 MB (U(h) = 0.0483237*h^0.488395)
    h2: 917.256 MB (U(h) = 0.114864*h^0.347063)
Recommendation: 2 clients, utility 1.9581:
    h1: 1290.76 MB (U(h) = 0.0483237*h^0.488395)
    h2: 917.24 MB (U(h) = 0.114864*h^0.347063)
Reading from 18879: heap size 554 MB, throughput 0.990218
Reading from 18880: heap size 545 MB, throughput 0.990143
Reading from 18879: heap size 554 MB, throughput 0.991055
Reading from 18880: heap size 546 MB, throughput 0.990871
Reading from 18879: heap size 554 MB, throughput 0.991456
Reading from 18880: heap size 545 MB, throughput 0.990884
Numeric result:
Recommendation: 2 clients, utility 1.91034:
    h1: 1275.29 MB (U(h) = 0.0539881*h^0.469884)
    h2: 932.709 MB (U(h) = 0.117209*h^0.343657)
Recommendation: 2 clients, utility 1.91034:
    h1: 1275.29 MB (U(h) = 0.0539881*h^0.469884)
    h2: 932.705 MB (U(h) = 0.117209*h^0.343657)
Reading from 18879: heap size 556 MB, throughput 0.990788
Reading from 18880: heap size 548 MB, throughput 0.984136
Reading from 18879: heap size 557 MB, throughput 0.99058
Reading from 18880: heap size 557 MB, throughput 0.993543
Reading from 18880: heap size 557 MB, throughput 0.992706
Reading from 18879: heap size 558 MB, throughput 0.9901
Reading from 18880: heap size 560 MB, throughput 0.987721
Reading from 18879: heap size 562 MB, throughput 0.98646
Reading from 18879: heap size 563 MB, throughput 0.977756
Reading from 18880: heap size 562 MB, throughput 0.986045
Numeric result:
Recommendation: 2 clients, utility 1.86968:
    h1: 1275.76 MB (U(h) = 0.0570102*h^0.460361)
    h2: 932.242 MB (U(h) = 0.122208*h^0.336397)
Recommendation: 2 clients, utility 1.86968:
    h1: 1275.77 MB (U(h) = 0.0570102*h^0.460361)
    h2: 932.234 MB (U(h) = 0.122208*h^0.336397)
Reading from 18879: heap size 568 MB, throughput 0.988179
Reading from 18880: heap size 569 MB, throughput 0.992191
Reading from 18879: heap size 570 MB, throughput 0.99199
Reading from 18880: heap size 570 MB, throughput 0.991992
Reading from 18879: heap size 572 MB, throughput 0.991416
Numeric result:
Recommendation: 2 clients, utility 1.96135:
    h1: 1318.9 MB (U(h) = 0.0427927*h^0.506893)
    h2: 889.097 MB (U(h) = 0.118004*h^0.341706)
Recommendation: 2 clients, utility 1.96135:
    h1: 1318.9 MB (U(h) = 0.0427927*h^0.506893)
    h2: 889.097 MB (U(h) = 0.118004*h^0.341706)
Reading from 18880: heap size 568 MB, throughput 0.991021
Reading from 18879: heap size 574 MB, throughput 0.991114
Reading from 18880: heap size 571 MB, throughput 0.990551
Reading from 18879: heap size 574 MB, throughput 0.991697
Reading from 18880: heap size 574 MB, throughput 0.991807
Numeric result:
Recommendation: 2 clients, utility 1.99995:
    h1: 1336.68 MB (U(h) = 0.0375048*h^0.527847)
    h2: 871.321 MB (U(h) = 0.116201*h^0.344081)
Recommendation: 2 clients, utility 1.99995:
    h1: 1336.68 MB (U(h) = 0.0375048*h^0.527847)
    h2: 871.322 MB (U(h) = 0.116201*h^0.344081)
Reading from 18880: heap size 574 MB, throughput 0.990297
Reading from 18879: heap size 576 MB, throughput 0.993303
Reading from 18880: heap size 576 MB, throughput 0.983457
Reading from 18879: heap size 579 MB, throughput 0.990128
Reading from 18879: heap size 579 MB, throughput 0.984841
Reading from 18880: heap size 580 MB, throughput 0.987261
Reading from 18879: heap size 587 MB, throughput 0.991062
Reading from 18880: heap size 586 MB, throughput 0.991631
Reading from 18879: heap size 587 MB, throughput 0.992706
Reading from 18880: heap size 589 MB, throughput 0.991662
Numeric result:
Recommendation: 2 clients, utility 1.94888:
    h1: 1357.23 MB (U(h) = 0.0391381*h^0.520108)
    h2: 850.773 MB (U(h) = 0.129604*h^0.326044)
Recommendation: 2 clients, utility 1.94888:
    h1: 1357.2 MB (U(h) = 0.0391381*h^0.520108)
    h2: 850.799 MB (U(h) = 0.129604*h^0.326044)
Reading from 18879: heap size 587 MB, throughput 0.993196
Reading from 18880: heap size 588 MB, throughput 0.990367
Reading from 18879: heap size 590 MB, throughput 0.991593
Reading from 18880: heap size 590 MB, throughput 0.989733
Reading from 18879: heap size 592 MB, throughput 0.990754
Numeric result:
Recommendation: 2 clients, utility 1.78328:
    h1: 1353.01 MB (U(h) = 0.0574634*h^0.457585)
    h2: 854.988 MB (U(h) = 0.162631*h^0.289154)
Recommendation: 2 clients, utility 1.78328:
    h1: 1353.01 MB (U(h) = 0.0574634*h^0.457585)
    h2: 854.986 MB (U(h) = 0.162631*h^0.289154)
Reading from 18879: heap size 592 MB, throughput 0.989959
Reading from 18880: heap size 595 MB, throughput 0.992633
Reading from 18879: heap size 595 MB, throughput 0.984677
Reading from 18880: heap size 595 MB, throughput 0.98803
Reading from 18880: heap size 596 MB, throughput 0.984138
Reading from 18879: heap size 597 MB, throughput 0.987639
Reading from 18880: heap size 599 MB, throughput 0.990785
Reading from 18879: heap size 604 MB, throughput 0.99071
Reading from 18880: heap size 601 MB, throughput 0.991682
Numeric result:
Recommendation: 2 clients, utility 1.30302:
    h1: 1072.51 MB (U(h) = 0.30414*h^0.188215)
    h2: 1135.49 MB (U(h) = 0.283599*h^0.199269)
Recommendation: 2 clients, utility 1.30302:
    h1: 1072.51 MB (U(h) = 0.30414*h^0.188215)
    h2: 1135.49 MB (U(h) = 0.283599*h^0.199269)
Reading from 18879: heap size 605 MB, throughput 0.991722
Reading from 18880: heap size 603 MB, throughput 0.9928
Reading from 18879: heap size 604 MB, throughput 0.991859
Reading from 18880: heap size 603 MB, throughput 0.991969
Reading from 18879: heap size 607 MB, throughput 0.990677
Numeric result:
Recommendation: 2 clients, utility 1.30115:
    h1: 992.959 MB (U(h) = 0.334077*h^0.173087)
    h2: 1215.04 MB (U(h) = 0.26204*h^0.211822)
Recommendation: 2 clients, utility 1.30115:
    h1: 992.899 MB (U(h) = 0.334077*h^0.173087)
    h2: 1215.1 MB (U(h) = 0.26204*h^0.211822)
Reading from 18880: heap size 605 MB, throughput 0.992194
Reading from 18879: heap size 610 MB, throughput 0.990864
Reading from 18879: heap size 610 MB, throughput 0.985259
Reading from 18880: heap size 608 MB, throughput 0.987633
Reading from 18880: heap size 609 MB, throughput 0.983761
Reading from 18879: heap size 614 MB, throughput 0.988982
Reading from 18880: heap size 617 MB, throughput 0.99172
Reading from 18879: heap size 616 MB, throughput 0.992027
Numeric result:
Recommendation: 2 clients, utility 1.10808:
    h1: 817.542 MB (U(h) = 0.658572*h^0.0642025)
    h2: 1390.46 MB (U(h) = 0.496329*h^0.109192)
Recommendation: 2 clients, utility 1.10808:
    h1: 817.551 MB (U(h) = 0.658572*h^0.0642025)
    h2: 1390.45 MB (U(h) = 0.496329*h^0.109192)
Reading from 18880: heap size 617 MB, throughput 0.993601
Reading from 18879: heap size 618 MB, throughput 0.993068
Reading from 18880: heap size 616 MB, throughput 0.992398
Reading from 18879: heap size 620 MB, throughput 0.993481
Reading from 18880: heap size 619 MB, throughput 0.991539
Numeric result:
Recommendation: 2 clients, utility 1.11633:
    h1: 790.015 MB (U(h) = 0.655273*h^0.0649908)
    h2: 1417.99 MB (U(h) = 0.473592*h^0.116651)
Recommendation: 2 clients, utility 1.11633:
    h1: 790.015 MB (U(h) = 0.655273*h^0.0649908)
    h2: 1417.98 MB (U(h) = 0.473592*h^0.116651)
Reading from 18879: heap size 620 MB, throughput 0.991806
Reading from 18879: heap size 622 MB, throughput 0.990281
Reading from 18879: heap size 625 MB, throughput 0.986384
Client 18879 died
Clients: 1
Reading from 18880: heap size 621 MB, throughput 0.993506
Reading from 18880: heap size 622 MB, throughput 0.989751
Client 18880 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
