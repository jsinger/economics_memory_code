#!/bin/bash
# View an existing isoquant dataset
#
# Usage: isoquant_view.sh data_dir
#

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") data_dir"
    exit 1
}

test -z "${1}" && usage
test ! -d "${1}" && usage

DATA_DIR="$(readlink -f "${1}")"

if [ ! -e "${DATA_DIR}/isoquant.pdf" ]; then
    "${DIR}/isoquant_postprocess.sh" "${DATA_DIR}"
fi

BENCHMARKS="${DATA_DIR}/benchmarks.txt"
AVGTIMES="${DATA_DIR}/avgTimes.txt"

B1="$(sed -n '1p' < "${BENCHMARKS}")"
B2="$(sed -n '2p' < "${BENCHMARKS}")"
TOTAL="$(cat "${DATA_DIR}/maxMemory.txt")"
ITERATIONS="$(cat "${DATA_DIR}/iterations.txt")"
RUNS="$(cat "${DATA_DIR}/runs.txt")"
PLOT="${DATA_DIR}/isoquant.pdf"

octave --persist -qf "${DIR}/isoquant_view.m" "${B1}, ${B2}: ${TOTAL} MB, ${ITERATIONS} iterations, ${RUNS} runs" "${B1}" "${B2}" "${AVGTIMES}" "${PLOT}"
