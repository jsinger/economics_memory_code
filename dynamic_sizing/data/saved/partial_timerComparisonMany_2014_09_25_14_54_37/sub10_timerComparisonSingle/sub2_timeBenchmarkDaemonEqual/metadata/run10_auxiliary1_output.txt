economemd
    total memory: 4876 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub10_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run10_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
TimerThread: starting
ReadingThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 1962: heap size 9 MB, throughput 0.988751
Clients: 1
Client 1962 has a minimum heap size of 8 MB
Reading from 1961: heap size 9 MB, throughput 0.992131
Clients: 2
Client 1961 has a minimum heap size of 1211 MB
Reading from 1962: heap size 9 MB, throughput 0.979527
Reading from 1961: heap size 9 MB, throughput 0.979507
Reading from 1961: heap size 9 MB, throughput 0.937558
Reading from 1962: heap size 11 MB, throughput 0.946679
Reading from 1962: heap size 11 MB, throughput 0.91009
Reading from 1961: heap size 9 MB, throughput 0.932167
Reading from 1961: heap size 11 MB, throughput 0.978839
Reading from 1962: heap size 15 MB, throughput 0.900975
Reading from 1961: heap size 11 MB, throughput 0.961843
Reading from 1961: heap size 17 MB, throughput 0.95785
Reading from 1961: heap size 17 MB, throughput 0.598723
Reading from 1962: heap size 20 MB, throughput 0.995687
Reading from 1962: heap size 24 MB, throughput 0.987972
Reading from 1961: heap size 30 MB, throughput 0.977095
Reading from 1962: heap size 29 MB, throughput 0.963749
Reading from 1962: heap size 31 MB, throughput 0.977292
Reading from 1962: heap size 31 MB, throughput 0.960165
Reading from 1961: heap size 31 MB, throughput 0.889451
Reading from 1962: heap size 35 MB, throughput 0.983007
Reading from 1962: heap size 35 MB, throughput 0.977861
Reading from 1962: heap size 40 MB, throughput 0.988102
Reading from 1962: heap size 40 MB, throughput 0.970434
Reading from 1962: heap size 46 MB, throughput 0.973458
Reading from 1961: heap size 33 MB, throughput 0.547381
Reading from 1962: heap size 46 MB, throughput 0.983396
Reading from 1961: heap size 46 MB, throughput 0.883551
Reading from 1962: heap size 52 MB, throughput 0.988764
Reading from 1961: heap size 48 MB, throughput 0.858069
Reading from 1962: heap size 52 MB, throughput 0.97965
Reading from 1962: heap size 58 MB, throughput 0.97396
Reading from 1962: heap size 58 MB, throughput 0.977531
Reading from 1961: heap size 49 MB, throughput 0.294456
Reading from 1962: heap size 63 MB, throughput 0.983886
Reading from 1961: heap size 69 MB, throughput 0.859015
Reading from 1961: heap size 70 MB, throughput 0.782148
Reading from 1962: heap size 64 MB, throughput 0.983338
Reading from 1962: heap size 70 MB, throughput 0.986577
Reading from 1962: heap size 70 MB, throughput 0.984998
Reading from 1961: heap size 74 MB, throughput 0.259833
Reading from 1961: heap size 93 MB, throughput 0.683263
Reading from 1962: heap size 77 MB, throughput 0.9921
Reading from 1961: heap size 99 MB, throughput 0.653446
Reading from 1962: heap size 77 MB, throughput 0.99141
Reading from 1961: heap size 100 MB, throughput 0.712849
Reading from 1962: heap size 82 MB, throughput 0.9885
Reading from 1962: heap size 82 MB, throughput 0.991723
Reading from 1961: heap size 103 MB, throughput 0.160886
Reading from 1962: heap size 87 MB, throughput 0.990236
Reading from 1961: heap size 131 MB, throughput 0.707602
Reading from 1961: heap size 138 MB, throughput 0.74761
Reading from 1962: heap size 87 MB, throughput 0.989382
Reading from 1961: heap size 139 MB, throughput 0.718288
Reading from 1962: heap size 92 MB, throughput 0.994555
Reading from 1962: heap size 92 MB, throughput 0.993929
Reading from 1962: heap size 96 MB, throughput 0.99118
Reading from 1961: heap size 143 MB, throughput 0.129386
Reading from 1962: heap size 96 MB, throughput 0.993685
Reading from 1961: heap size 178 MB, throughput 0.625972
Reading from 1962: heap size 99 MB, throughput 0.99286
Reading from 1961: heap size 183 MB, throughput 0.622163
Reading from 1961: heap size 186 MB, throughput 0.659359
Reading from 1962: heap size 100 MB, throughput 0.993899
Reading from 1961: heap size 189 MB, throughput 0.570447
Reading from 1962: heap size 103 MB, throughput 0.932369
Reading from 1961: heap size 197 MB, throughput 0.670461
Reading from 1962: heap size 103 MB, throughput 0.988537
Reading from 1962: heap size 111 MB, throughput 0.990558
Reading from 1961: heap size 201 MB, throughput 0.116828
Reading from 1962: heap size 111 MB, throughput 0.994702
Reading from 1961: heap size 242 MB, throughput 0.555965
Reading from 1962: heap size 117 MB, throughput 0.991269
Reading from 1961: heap size 251 MB, throughput 0.659574
Reading from 1962: heap size 118 MB, throughput 0.991339
Reading from 1961: heap size 252 MB, throughput 0.59024
Reading from 1962: heap size 124 MB, throughput 0.993241
Reading from 1961: heap size 254 MB, throughput 0.653504
Reading from 1961: heap size 259 MB, throughput 0.581963
Reading from 1962: heap size 124 MB, throughput 0.992018
Reading from 1961: heap size 264 MB, throughput 0.64635
Reading from 1962: heap size 128 MB, throughput 0.995711
Reading from 1962: heap size 129 MB, throughput 0.992781
Reading from 1962: heap size 133 MB, throughput 0.991079
Reading from 1961: heap size 269 MB, throughput 0.101987
Reading from 1962: heap size 133 MB, throughput 0.99592
Reading from 1961: heap size 312 MB, throughput 0.432074
Reading from 1962: heap size 137 MB, throughput 0.995919
Reading from 1961: heap size 316 MB, throughput 0.521532
Reading from 1961: heap size 318 MB, throughput 0.509789
Reading from 1962: heap size 138 MB, throughput 0.99616
Reading from 1961: heap size 321 MB, throughput 0.550332
Reading from 1962: heap size 141 MB, throughput 0.995771
Reading from 1962: heap size 141 MB, throughput 0.995679
Reading from 1961: heap size 327 MB, throughput 0.100919
Reading from 1962: heap size 145 MB, throughput 0.997613
Reading from 1961: heap size 374 MB, throughput 0.464555
Reading from 1962: heap size 145 MB, throughput 0.993757
Reading from 1961: heap size 372 MB, throughput 0.619764
Reading from 1961: heap size 377 MB, throughput 0.606025
Reading from 1962: heap size 148 MB, throughput 0.997155
Reading from 1962: heap size 148 MB, throughput 0.994072
Reading from 1962: heap size 151 MB, throughput 0.994831
Reading from 1961: heap size 377 MB, throughput 0.122198
Reading from 1961: heap size 427 MB, throughput 0.484104
Reading from 1962: heap size 151 MB, throughput 0.995436
Reading from 1961: heap size 429 MB, throughput 0.583454
Reading from 1962: heap size 154 MB, throughput 0.997259
Reading from 1961: heap size 432 MB, throughput 0.528457
Reading from 1962: heap size 154 MB, throughput 0.995246
Reading from 1961: heap size 434 MB, throughput 0.520368
Reading from 1962: heap size 157 MB, throughput 0.993789
Reading from 1961: heap size 441 MB, throughput 0.492566
Reading from 1962: heap size 157 MB, throughput 0.994894
Reading from 1961: heap size 447 MB, throughput 0.471765
Reading from 1962: heap size 160 MB, throughput 0.996589
Reading from 1961: heap size 456 MB, throughput 0.492955
Reading from 1962: heap size 160 MB, throughput 0.995236
Equal recommendation: 2438 MB each
Reading from 1962: heap size 163 MB, throughput 0.996084
Reading from 1962: heap size 163 MB, throughput 0.996845
Reading from 1961: heap size 462 MB, throughput 0.0922821
Reading from 1962: heap size 166 MB, throughput 0.9974
Reading from 1961: heap size 524 MB, throughput 0.375023
Reading from 1962: heap size 166 MB, throughput 0.997171
Reading from 1961: heap size 532 MB, throughput 0.480609
Reading from 1962: heap size 169 MB, throughput 0.996789
Reading from 1961: heap size 534 MB, throughput 0.490809
Reading from 1962: heap size 169 MB, throughput 0.994849
Reading from 1962: heap size 171 MB, throughput 0.994728
Reading from 1962: heap size 171 MB, throughput 0.992386
Reading from 1962: heap size 174 MB, throughput 0.993708
Reading from 1961: heap size 540 MB, throughput 0.0940518
Reading from 1962: heap size 174 MB, throughput 0.995208
Reading from 1961: heap size 611 MB, throughput 0.39598
Reading from 1962: heap size 178 MB, throughput 0.996551
Reading from 1961: heap size 600 MB, throughput 0.664986
Reading from 1961: heap size 607 MB, throughput 0.60845
Reading from 1962: heap size 178 MB, throughput 0.995877
Reading from 1961: heap size 609 MB, throughput 0.602078
Reading from 1962: heap size 182 MB, throughput 0.99669
Reading from 1961: heap size 614 MB, throughput 0.558306
Reading from 1962: heap size 182 MB, throughput 0.99451
Reading from 1961: heap size 618 MB, throughput 0.54434
Reading from 1962: heap size 186 MB, throughput 0.997072
Reading from 1961: heap size 631 MB, throughput 0.772056
Reading from 1962: heap size 186 MB, throughput 0.996332
Reading from 1962: heap size 189 MB, throughput 0.996638
Reading from 1961: heap size 637 MB, throughput 0.792868
Reading from 1962: heap size 189 MB, throughput 0.995742
Reading from 1962: heap size 193 MB, throughput 0.994457
Reading from 1962: heap size 193 MB, throughput 0.989707
Reading from 1961: heap size 649 MB, throughput 0.759646
Reading from 1962: heap size 196 MB, throughput 0.995603
Reading from 1962: heap size 196 MB, throughput 0.977923
Reading from 1961: heap size 649 MB, throughput 0.78436
Reading from 1961: heap size 673 MB, throughput 0.441569
Reading from 1962: heap size 201 MB, throughput 0.996143
Reading from 1962: heap size 201 MB, throughput 0.996339
Reading from 1962: heap size 206 MB, throughput 0.99624
Reading from 1962: heap size 207 MB, throughput 0.997174
Reading from 1961: heap size 684 MB, throughput 0.0282072
Reading from 1962: heap size 211 MB, throughput 0.996509
Reading from 1961: heap size 766 MB, throughput 0.385584
Reading from 1961: heap size 771 MB, throughput 0.55072
Reading from 1962: heap size 212 MB, throughput 0.997513
Reading from 1961: heap size 664 MB, throughput 0.547465
Reading from 1962: heap size 215 MB, throughput 0.997699
Reading from 1962: heap size 216 MB, throughput 0.994813
Equal recommendation: 2438 MB each
Reading from 1962: heap size 219 MB, throughput 0.997143
Reading from 1961: heap size 748 MB, throughput 0.137102
Reading from 1962: heap size 219 MB, throughput 0.996671
Reading from 1961: heap size 714 MB, throughput 0.487268
Reading from 1962: heap size 222 MB, throughput 0.996153
Reading from 1961: heap size 823 MB, throughput 0.651768
Reading from 1962: heap size 222 MB, throughput 0.996537
Reading from 1961: heap size 827 MB, throughput 0.708256
Reading from 1962: heap size 225 MB, throughput 0.997401
Reading from 1961: heap size 828 MB, throughput 0.751699
Reading from 1962: heap size 226 MB, throughput 0.996564
Reading from 1962: heap size 229 MB, throughput 0.996621
Reading from 1962: heap size 229 MB, throughput 0.995906
Reading from 1962: heap size 233 MB, throughput 0.99643
Reading from 1961: heap size 829 MB, throughput 0.170662
Reading from 1962: heap size 233 MB, throughput 0.997475
Reading from 1961: heap size 911 MB, throughput 0.519451
Reading from 1961: heap size 914 MB, throughput 0.495745
Reading from 1962: heap size 236 MB, throughput 0.996006
Reading from 1961: heap size 916 MB, throughput 0.538297
Reading from 1961: heap size 917 MB, throughput 0.713724
Reading from 1961: heap size 923 MB, throughput 0.718549
Reading from 1962: heap size 237 MB, throughput 0.996564
Reading from 1961: heap size 924 MB, throughput 0.901768
Reading from 1961: heap size 917 MB, throughput 0.856667
Reading from 1962: heap size 240 MB, throughput 0.997082
Reading from 1961: heap size 694 MB, throughput 0.916747
Reading from 1961: heap size 908 MB, throughput 0.83591
Reading from 1962: heap size 229 MB, throughput 0.970315
Reading from 1961: heap size 701 MB, throughput 0.888775
Reading from 1961: heap size 893 MB, throughput 0.882529
Reading from 1962: heap size 219 MB, throughput 0.997665
Reading from 1961: heap size 710 MB, throughput 0.850856
Reading from 1962: heap size 224 MB, throughput 0.993971
Reading from 1961: heap size 877 MB, throughput 0.915306
Reading from 1961: heap size 744 MB, throughput 0.782975
Reading from 1962: heap size 231 MB, throughput 0.994773
Reading from 1961: heap size 861 MB, throughput 0.767864
Reading from 1961: heap size 752 MB, throughput 0.743088
Reading from 1962: heap size 239 MB, throughput 0.993344
Reading from 1961: heap size 844 MB, throughput 0.781068
Reading from 1961: heap size 757 MB, throughput 0.762097
Reading from 1962: heap size 239 MB, throughput 0.996384
Reading from 1961: heap size 835 MB, throughput 0.764748
Reading from 1961: heap size 762 MB, throughput 0.736141
Reading from 1962: heap size 239 MB, throughput 0.995682
Reading from 1961: heap size 828 MB, throughput 0.767551
Reading from 1962: heap size 246 MB, throughput 0.99701
Reading from 1961: heap size 835 MB, throughput 0.785065
Reading from 1961: heap size 823 MB, throughput 0.794235
Reading from 1962: heap size 247 MB, throughput 0.996107
Reading from 1961: heap size 829 MB, throughput 0.789545
Reading from 1961: heap size 822 MB, throughput 0.793845
Reading from 1962: heap size 255 MB, throughput 0.99791
Reading from 1961: heap size 826 MB, throughput 0.950287
Reading from 1962: heap size 255 MB, throughput 0.997226
Reading from 1962: heap size 261 MB, throughput 0.997527
Reading from 1962: heap size 262 MB, throughput 0.997335
Reading from 1962: heap size 268 MB, throughput 0.997599
Reading from 1961: heap size 826 MB, throughput 0.967012
Equal recommendation: 2438 MB each
Reading from 1961: heap size 829 MB, throughput 0.802597
Reading from 1962: heap size 268 MB, throughput 0.997218
Reading from 1961: heap size 831 MB, throughput 0.68664
Reading from 1962: heap size 275 MB, throughput 0.995931
Reading from 1961: heap size 835 MB, throughput 0.683898
Reading from 1961: heap size 843 MB, throughput 0.701685
Reading from 1962: heap size 275 MB, throughput 0.997017
Reading from 1961: heap size 845 MB, throughput 0.704958
Reading from 1961: heap size 855 MB, throughput 0.680513
Reading from 1962: heap size 282 MB, throughput 0.995309
Reading from 1961: heap size 855 MB, throughput 0.730408
Reading from 1961: heap size 865 MB, throughput 0.733651
Reading from 1962: heap size 282 MB, throughput 0.99786
Reading from 1961: heap size 865 MB, throughput 0.730538
Reading from 1961: heap size 873 MB, throughput 0.874416
Reading from 1962: heap size 289 MB, throughput 0.996198
Reading from 1961: heap size 874 MB, throughput 0.870631
Reading from 1962: heap size 289 MB, throughput 0.997737
Reading from 1961: heap size 877 MB, throughput 0.874912
Reading from 1962: heap size 296 MB, throughput 0.996462
Reading from 1961: heap size 879 MB, throughput 0.656464
Reading from 1962: heap size 297 MB, throughput 0.995217
Reading from 1962: heap size 306 MB, throughput 0.99612
Reading from 1962: heap size 306 MB, throughput 0.995134
Reading from 1962: heap size 315 MB, throughput 0.997846
Reading from 1961: heap size 883 MB, throughput 0.0603238
Reading from 1961: heap size 981 MB, throughput 0.442475
Reading from 1962: heap size 315 MB, throughput 0.996702
Reading from 1961: heap size 1010 MB, throughput 0.641743
Reading from 1961: heap size 1013 MB, throughput 0.639338
Reading from 1962: heap size 325 MB, throughput 0.996365
Reading from 1961: heap size 1026 MB, throughput 0.651851
Reading from 1961: heap size 1025 MB, throughput 0.634215
Reading from 1962: heap size 325 MB, throughput 0.993908
Reading from 1961: heap size 1042 MB, throughput 0.696578
Reading from 1961: heap size 1042 MB, throughput 0.596001
Reading from 1962: heap size 334 MB, throughput 0.994683
Reading from 1961: heap size 1061 MB, throughput 0.680048
Reading from 1961: heap size 1063 MB, throughput 0.628954
Reading from 1962: heap size 334 MB, throughput 0.995469
Reading from 1962: heap size 346 MB, throughput 0.993539
Reading from 1962: heap size 346 MB, throughput 0.996468
Equal recommendation: 2438 MB each
Reading from 1961: heap size 1083 MB, throughput 0.951144
Reading from 1962: heap size 357 MB, throughput 0.997528
Reading from 1962: heap size 358 MB, throughput 0.997074
Reading from 1962: heap size 368 MB, throughput 0.995989
Reading from 1962: heap size 369 MB, throughput 0.996997
Reading from 1961: heap size 1086 MB, throughput 0.932466
Reading from 1962: heap size 380 MB, throughput 0.998191
Reading from 1962: heap size 380 MB, throughput 0.998354
Reading from 1962: heap size 389 MB, throughput 0.995988
Reading from 1961: heap size 1100 MB, throughput 0.938545
Reading from 1962: heap size 390 MB, throughput 0.996882
Reading from 1962: heap size 399 MB, throughput 0.997684
Reading from 1962: heap size 400 MB, throughput 0.996807
Reading from 1961: heap size 1103 MB, throughput 0.936596
Reading from 1962: heap size 410 MB, throughput 0.997508
Reading from 1962: heap size 410 MB, throughput 0.996966
Reading from 1962: heap size 420 MB, throughput 0.996777
Reading from 1962: heap size 420 MB, throughput 0.996422
Reading from 1961: heap size 1106 MB, throughput 0.938863
Reading from 1962: heap size 431 MB, throughput 0.997593
Reading from 1962: heap size 431 MB, throughput 0.997171
Equal recommendation: 2438 MB each
Reading from 1962: heap size 441 MB, throughput 0.997365
Reading from 1961: heap size 1112 MB, throughput 0.941272
Reading from 1962: heap size 441 MB, throughput 0.996674
Reading from 1962: heap size 452 MB, throughput 0.996813
Reading from 1962: heap size 452 MB, throughput 0.997294
Reading from 1961: heap size 1107 MB, throughput 0.935462
Reading from 1962: heap size 463 MB, throughput 0.997621
Reading from 1962: heap size 463 MB, throughput 0.996791
Reading from 1962: heap size 474 MB, throughput 0.9977
Reading from 1961: heap size 1114 MB, throughput 0.939637
Reading from 1962: heap size 474 MB, throughput 0.997697
Reading from 1962: heap size 485 MB, throughput 0.997484
Reading from 1962: heap size 485 MB, throughput 0.99684
Reading from 1961: heap size 1118 MB, throughput 0.93584
Reading from 1962: heap size 496 MB, throughput 0.99757
Reading from 1962: heap size 496 MB, throughput 0.997581
Reading from 1962: heap size 506 MB, throughput 0.997228
Reading from 1961: heap size 1120 MB, throughput 0.935371
Reading from 1962: heap size 506 MB, throughput 0.997528
Equal recommendation: 2438 MB each
Reading from 1962: heap size 518 MB, throughput 0.997654
Reading from 1962: heap size 518 MB, throughput 0.997488
Reading from 1961: heap size 1131 MB, throughput 0.945158
Reading from 1962: heap size 529 MB, throughput 0.997881
Reading from 1962: heap size 529 MB, throughput 0.997564
Reading from 1962: heap size 541 MB, throughput 0.997483
Reading from 1961: heap size 1132 MB, throughput 0.943141
Reading from 1962: heap size 541 MB, throughput 0.989639
Reading from 1962: heap size 552 MB, throughput 0.994158
Reading from 1962: heap size 552 MB, throughput 0.997549
Reading from 1961: heap size 1141 MB, throughput 0.936465
Reading from 1962: heap size 570 MB, throughput 0.997752
Reading from 1962: heap size 572 MB, throughput 0.997812
Reading from 1961: heap size 1145 MB, throughput 0.929594
Reading from 1962: heap size 588 MB, throughput 0.998302
Reading from 1962: heap size 588 MB, throughput 0.998175
Equal recommendation: 2438 MB each
Reading from 1962: heap size 601 MB, throughput 0.997879
Reading from 1961: heap size 1154 MB, throughput 0.947993
Reading from 1962: heap size 603 MB, throughput 0.998228
Reading from 1962: heap size 615 MB, throughput 0.997798
Reading from 1962: heap size 615 MB, throughput 0.996192
Reading from 1961: heap size 1159 MB, throughput 0.944255
Reading from 1962: heap size 628 MB, throughput 0.99869
Reading from 1962: heap size 628 MB, throughput 0.998213
Reading from 1961: heap size 1170 MB, throughput 0.947012
Reading from 1962: heap size 640 MB, throughput 0.99786
Reading from 1962: heap size 640 MB, throughput 0.998045
Reading from 1962: heap size 652 MB, throughput 0.998516
Equal recommendation: 2438 MB each
Reading from 1962: heap size 652 MB, throughput 0.998465
Reading from 1961: heap size 1175 MB, throughput 0.490853
Reading from 1962: heap size 663 MB, throughput 0.998532
Reading from 1962: heap size 663 MB, throughput 0.998336
Reading from 1961: heap size 1276 MB, throughput 0.91972
Reading from 1962: heap size 674 MB, throughput 0.99812
Reading from 1962: heap size 674 MB, throughput 0.998417
Reading from 1962: heap size 684 MB, throughput 0.998326
Reading from 1961: heap size 1280 MB, throughput 0.962332
Reading from 1962: heap size 684 MB, throughput 0.998315
Reading from 1962: heap size 694 MB, throughput 0.998295
Reading from 1961: heap size 1287 MB, throughput 0.960622
Reading from 1962: heap size 694 MB, throughput 0.998508
Reading from 1962: heap size 705 MB, throughput 0.998824
Equal recommendation: 2438 MB each
Reading from 1962: heap size 705 MB, throughput 0.997993
Reading from 1961: heap size 1289 MB, throughput 0.963805
Reading from 1962: heap size 715 MB, throughput 0.998578
Reading from 1962: heap size 715 MB, throughput 0.998451
Reading from 1961: heap size 1287 MB, throughput 0.961206
Reading from 1962: heap size 725 MB, throughput 0.998706
Reading from 1962: heap size 725 MB, throughput 0.99853
Reading from 1961: heap size 1291 MB, throughput 0.958906
Reading from 1962: heap size 735 MB, throughput 0.998367
Reading from 1962: heap size 735 MB, throughput 0.998825
Reading from 1962: heap size 745 MB, throughput 0.998786
Reading from 1961: heap size 1284 MB, throughput 0.952498
Reading from 1962: heap size 746 MB, throughput 0.998352
Equal recommendation: 2438 MB each
Reading from 1962: heap size 755 MB, throughput 0.99871
Reading from 1961: heap size 1289 MB, throughput 0.949514
Reading from 1962: heap size 755 MB, throughput 0.998474
Reading from 1962: heap size 765 MB, throughput 0.998517
Reading from 1961: heap size 1293 MB, throughput 0.948297
Reading from 1962: heap size 765 MB, throughput 0.998795
Reading from 1962: heap size 775 MB, throughput 0.998205
Reading from 1962: heap size 775 MB, throughput 0.998601
Reading from 1961: heap size 1294 MB, throughput 0.955813
Reading from 1962: heap size 785 MB, throughput 0.998752
Reading from 1962: heap size 785 MB, throughput 0.998593
Equal recommendation: 2438 MB each
Reading from 1961: heap size 1303 MB, throughput 0.953361
Reading from 1962: heap size 796 MB, throughput 0.998612
Reading from 1962: heap size 796 MB, throughput 0.998703
Reading from 1962: heap size 806 MB, throughput 0.998568
Reading from 1961: heap size 1305 MB, throughput 0.944149
Reading from 1962: heap size 806 MB, throughput 0.998797
Reading from 1962: heap size 816 MB, throughput 0.998541
Reading from 1961: heap size 1314 MB, throughput 0.941567
Reading from 1962: heap size 816 MB, throughput 0.998623
Reading from 1962: heap size 827 MB, throughput 0.998682
Reading from 1962: heap size 827 MB, throughput 0.998348
Reading from 1961: heap size 1320 MB, throughput 0.944719
Equal recommendation: 2438 MB each
Reading from 1962: heap size 837 MB, throughput 0.998742
Reading from 1962: heap size 837 MB, throughput 0.998691
Reading from 1961: heap size 1330 MB, throughput 0.942561
Reading from 1962: heap size 848 MB, throughput 0.998755
Reading from 1962: heap size 848 MB, throughput 0.998624
Reading from 1962: heap size 858 MB, throughput 0.995286
Reading from 1961: heap size 1338 MB, throughput 0.948076
Reading from 1962: heap size 858 MB, throughput 0.998824
Reading from 1962: heap size 873 MB, throughput 0.998713
Reading from 1961: heap size 1351 MB, throughput 0.942401
Reading from 1962: heap size 875 MB, throughput 0.998865
Equal recommendation: 2438 MB each
Reading from 1962: heap size 887 MB, throughput 0.998662
Reading from 1961: heap size 1359 MB, throughput 0.943754
Reading from 1962: heap size 887 MB, throughput 0.998839
Reading from 1962: heap size 899 MB, throughput 0.998928
Reading from 1962: heap size 900 MB, throughput 0.998385
Reading from 1961: heap size 1373 MB, throughput 0.948071
Reading from 1962: heap size 911 MB, throughput 0.998984
Reading from 1962: heap size 911 MB, throughput 0.998784
Reading from 1962: heap size 922 MB, throughput 0.998892
Equal recommendation: 2438 MB each
Reading from 1962: heap size 922 MB, throughput 0.998728
Reading from 1962: heap size 932 MB, throughput 0.998956
Reading from 1962: heap size 932 MB, throughput 0.998617
Reading from 1962: heap size 943 MB, throughput 0.999125
Reading from 1962: heap size 943 MB, throughput 0.998815
Reading from 1962: heap size 953 MB, throughput 0.998962
Reading from 1962: heap size 953 MB, throughput 0.998833
Equal recommendation: 2438 MB each
Reading from 1962: heap size 963 MB, throughput 0.998994
Reading from 1962: heap size 963 MB, throughput 0.99879
Reading from 1962: heap size 973 MB, throughput 0.999151
Reading from 1961: heap size 1378 MB, throughput 0.831241
Reading from 1962: heap size 973 MB, throughput 0.998963
Reading from 1962: heap size 983 MB, throughput 0.998888
Reading from 1962: heap size 983 MB, throughput 0.998858
Equal recommendation: 2438 MB each
Reading from 1962: heap size 993 MB, throughput 0.999068
Reading from 1962: heap size 993 MB, throughput 0.998917
Reading from 1962: heap size 1003 MB, throughput 0.998758
Reading from 1962: heap size 1003 MB, throughput 0.999204
Reading from 1962: heap size 1013 MB, throughput 0.998357
Reading from 1962: heap size 1013 MB, throughput 0.999003
Equal recommendation: 2438 MB each
Reading from 1962: heap size 1024 MB, throughput 0.998882
Reading from 1961: heap size 1526 MB, throughput 0.928154
Reading from 1962: heap size 1024 MB, throughput 0.999183
Reading from 1962: heap size 1034 MB, throughput 0.99884
Reading from 1962: heap size 1035 MB, throughput 0.998856
Reading from 1961: heap size 1514 MB, throughput 0.991236
Reading from 1961: heap size 1522 MB, throughput 0.887439
Reading from 1962: heap size 1045 MB, throughput 0.998645
Reading from 1961: heap size 1531 MB, throughput 0.834732
Reading from 1961: heap size 1538 MB, throughput 0.848819
Reading from 1961: heap size 1539 MB, throughput 0.834752
Reading from 1961: heap size 1553 MB, throughput 0.91544
Reading from 1962: heap size 1045 MB, throughput 0.999119
Equal recommendation: 2438 MB each
Reading from 1962: heap size 1056 MB, throughput 0.998812
Reading from 1961: heap size 1557 MB, throughput 0.985703
Reading from 1962: heap size 1056 MB, throughput 0.998978
Reading from 1962: heap size 1067 MB, throughput 0.998936
Reading from 1962: heap size 1067 MB, throughput 0.999046
Reading from 1961: heap size 1594 MB, throughput 0.982703
Reading from 1962: heap size 1077 MB, throughput 0.998926
Reading from 1962: heap size 1078 MB, throughput 0.999175
Reading from 1961: heap size 1597 MB, throughput 0.980332
Equal recommendation: 2438 MB each
Reading from 1962: heap size 1088 MB, throughput 0.998997
Reading from 1962: heap size 1088 MB, throughput 0.999112
Reading from 1961: heap size 1606 MB, throughput 0.976639
Reading from 1962: heap size 1098 MB, throughput 0.999057
Reading from 1962: heap size 1098 MB, throughput 0.999136
Reading from 1961: heap size 1614 MB, throughput 0.973846
Reading from 1962: heap size 1108 MB, throughput 0.999062
Reading from 1962: heap size 1108 MB, throughput 0.998996
Equal recommendation: 2438 MB each
Reading from 1962: heap size 1118 MB, throughput 0.999084
Reading from 1961: heap size 1605 MB, throughput 0.972027
Reading from 1962: heap size 1118 MB, throughput 0.998939
Reading from 1962: heap size 1128 MB, throughput 0.999342
Reading from 1961: heap size 1616 MB, throughput 0.972407
Reading from 1962: heap size 1128 MB, throughput 0.99882
Reading from 1962: heap size 1138 MB, throughput 0.999156
Reading from 1962: heap size 1138 MB, throughput 0.99896
Reading from 1961: heap size 1627 MB, throughput 0.966638
Equal recommendation: 2438 MB each
Reading from 1962: heap size 1148 MB, throughput 0.999042
Reading from 1962: heap size 1148 MB, throughput 0.99908
Reading from 1961: heap size 1628 MB, throughput 0.965652
Reading from 1962: heap size 1158 MB, throughput 0.999139
Reading from 1962: heap size 1158 MB, throughput 0.998895
Reading from 1962: heap size 1169 MB, throughput 0.998974
Reading from 1961: heap size 1641 MB, throughput 0.964366
Equal recommendation: 2438 MB each
Reading from 1962: heap size 1169 MB, throughput 0.998956
Reading from 1962: heap size 1180 MB, throughput 0.999178
Reading from 1961: heap size 1649 MB, throughput 0.962232
Reading from 1962: heap size 1180 MB, throughput 0.999045
Reading from 1962: heap size 1191 MB, throughput 0.999148
Reading from 1961: heap size 1663 MB, throughput 0.958701
Reading from 1962: heap size 1191 MB, throughput 0.998824
Reading from 1962: heap size 1201 MB, throughput 0.999168
Equal recommendation: 2438 MB each
Reading from 1962: heap size 1201 MB, throughput 0.99918
Reading from 1961: heap size 1674 MB, throughput 0.957329
Reading from 1962: heap size 1212 MB, throughput 0.999189
Reading from 1962: heap size 1212 MB, throughput 0.999215
Reading from 1961: heap size 1690 MB, throughput 0.961445
Reading from 1962: heap size 1222 MB, throughput 0.999169
Reading from 1962: heap size 1222 MB, throughput 0.99917
Equal recommendation: 2438 MB each
Reading from 1962: heap size 1232 MB, throughput 0.99857
Reading from 1961: heap size 1705 MB, throughput 0.953101
Reading from 1962: heap size 1232 MB, throughput 0.999116
Reading from 1962: heap size 1244 MB, throughput 0.999165
Reading from 1961: heap size 1723 MB, throughput 0.954012
Reading from 1962: heap size 1244 MB, throughput 0.998996
Reading from 1962: heap size 1255 MB, throughput 0.999288
Equal recommendation: 2438 MB each
Reading from 1961: heap size 1741 MB, throughput 0.950382
Reading from 1962: heap size 1255 MB, throughput 0.999126
Reading from 1962: heap size 1265 MB, throughput 0.999291
Reading from 1961: heap size 1765 MB, throughput 0.94864
Reading from 1962: heap size 1265 MB, throughput 0.999055
Reading from 1962: heap size 1276 MB, throughput 0.999163
Reading from 1962: heap size 1276 MB, throughput 0.999314
Reading from 1961: heap size 1777 MB, throughput 0.949446
Equal recommendation: 2438 MB each
Reading from 1962: heap size 1286 MB, throughput 0.998971
Reading from 1962: heap size 1286 MB, throughput 0.999038
Reading from 1961: heap size 1801 MB, throughput 0.954115
Reading from 1962: heap size 1296 MB, throughput 0.999234
Reading from 1962: heap size 1297 MB, throughput 0.998975
Reading from 1962: heap size 1306 MB, throughput 0.999263
Reading from 1961: heap size 1809 MB, throughput 0.954124
Equal recommendation: 2438 MB each
Reading from 1962: heap size 1307 MB, throughput 0.999177
Reading from 1962: heap size 1317 MB, throughput 0.999194
Reading from 1961: heap size 1833 MB, throughput 0.954791
Reading from 1962: heap size 1317 MB, throughput 0.999185
Reading from 1962: heap size 1327 MB, throughput 0.999294
Reading from 1962: heap size 1327 MB, throughput 0.999179
Equal recommendation: 2438 MB each
Reading from 1962: heap size 1337 MB, throughput 0.999404
Reading from 1962: heap size 1337 MB, throughput 0.99918
Reading from 1962: heap size 1271 MB, throughput 0.998973
Reading from 1962: heap size 1209 MB, throughput 0.99946
Reading from 1962: heap size 1148 MB, throughput 0.999028
Equal recommendation: 2438 MB each
Reading from 1962: heap size 1093 MB, throughput 0.999373
Reading from 1962: heap size 1040 MB, throughput 0.999156
Reading from 1962: heap size 989 MB, throughput 0.999341
Reading from 1962: heap size 941 MB, throughput 0.998347
Reading from 1961: heap size 1838 MB, throughput 0.896694
Reading from 1962: heap size 897 MB, throughput 0.999232
Reading from 1962: heap size 852 MB, throughput 0.998839
Reading from 1962: heap size 811 MB, throughput 0.999021
Equal recommendation: 2438 MB each
Reading from 1962: heap size 772 MB, throughput 0.998982
Reading from 1962: heap size 735 MB, throughput 0.998782
Reading from 1962: heap size 700 MB, throughput 0.998892
Reading from 1962: heap size 666 MB, throughput 0.998318
Reading from 1962: heap size 635 MB, throughput 0.998382
Reading from 1962: heap size 605 MB, throughput 0.985533
Reading from 1962: heap size 578 MB, throughput 0.998765
Reading from 1962: heap size 587 MB, throughput 0.998445
Reading from 1962: heap size 597 MB, throughput 0.999112
Reading from 1962: heap size 607 MB, throughput 0.999002
Equal recommendation: 2438 MB each
Reading from 1962: heap size 616 MB, throughput 0.999086
Reading from 1962: heap size 624 MB, throughput 0.998759
Reading from 1961: heap size 1988 MB, throughput 0.984016
Reading from 1961: heap size 2013 MB, throughput 0.86286
Reading from 1962: heap size 633 MB, throughput 0.999014
Reading from 1961: heap size 2029 MB, throughput 0.826661
Client 1962 died
Clients: 1
Reading from 1961: heap size 2030 MB, throughput 0.890709
Client 1961 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
