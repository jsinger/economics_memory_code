economemd
    total memory: 8922 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub5_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 5669: heap size 9 MB, throughput 0.991678
Clients: 1
Client 5669 has a minimum heap size of 276 MB
Reading from 5668: heap size 9 MB, throughput 0.991686
Clients: 2
Client 5668 has a minimum heap size of 1211 MB
Reading from 5669: heap size 9 MB, throughput 0.988142
Reading from 5668: heap size 9 MB, throughput 0.98818
Reading from 5669: heap size 9 MB, throughput 0.982584
Reading from 5668: heap size 9 MB, throughput 0.976707
Reading from 5669: heap size 9 MB, throughput 0.975466
Reading from 5668: heap size 9 MB, throughput 0.95604
Reading from 5668: heap size 11 MB, throughput 0.965702
Reading from 5669: heap size 11 MB, throughput 0.949844
Reading from 5668: heap size 11 MB, throughput 0.971903
Reading from 5669: heap size 11 MB, throughput 0.973825
Reading from 5668: heap size 17 MB, throughput 0.97279
Reading from 5669: heap size 17 MB, throughput 0.956374
Reading from 5669: heap size 17 MB, throughput 0.855246
Reading from 5668: heap size 17 MB, throughput 0.886188
Reading from 5669: heap size 30 MB, throughput 0.789083
Reading from 5668: heap size 30 MB, throughput 0.870431
Reading from 5669: heap size 31 MB, throughput 0.388116
Reading from 5668: heap size 31 MB, throughput 0.554588
Reading from 5669: heap size 34 MB, throughput 0.704172
Reading from 5668: heap size 34 MB, throughput 0.795003
Reading from 5668: heap size 44 MB, throughput 0.294057
Reading from 5669: heap size 42 MB, throughput 0.863394
Reading from 5668: heap size 50 MB, throughput 0.109212
Reading from 5669: heap size 49 MB, throughput 0.844103
Reading from 5668: heap size 50 MB, throughput 0.415301
Reading from 5669: heap size 49 MB, throughput 0.406052
Reading from 5668: heap size 68 MB, throughput 0.312507
Reading from 5669: heap size 66 MB, throughput 0.269866
Reading from 5668: heap size 89 MB, throughput 0.606775
Reading from 5669: heap size 87 MB, throughput 0.542404
Reading from 5668: heap size 93 MB, throughput 0.451697
Reading from 5669: heap size 90 MB, throughput 0.386148
Reading from 5669: heap size 92 MB, throughput 0.0607385
Reading from 5668: heap size 94 MB, throughput 0.196752
Reading from 5668: heap size 100 MB, throughput 0.201975
Reading from 5669: heap size 97 MB, throughput 0.515835
Reading from 5669: heap size 122 MB, throughput 0.100049
Reading from 5669: heap size 127 MB, throughput 0.232253
Reading from 5668: heap size 101 MB, throughput 0.427295
Reading from 5669: heap size 128 MB, throughput 0.0755629
Reading from 5668: heap size 136 MB, throughput 0.271983
Reading from 5668: heap size 137 MB, throughput 0.00827742
Reading from 5668: heap size 146 MB, throughput 0.786106
Reading from 5669: heap size 130 MB, throughput 0.39614
Reading from 5669: heap size 164 MB, throughput 0.604056
Reading from 5669: heap size 170 MB, throughput 0.624667
Reading from 5668: heap size 147 MB, throughput 0.416549
Reading from 5669: heap size 172 MB, throughput 0.657413
Reading from 5669: heap size 174 MB, throughput 0.648643
Reading from 5668: heap size 184 MB, throughput 0.72032
Reading from 5669: heap size 181 MB, throughput 0.627107
Reading from 5668: heap size 190 MB, throughput 0.709185
Reading from 5669: heap size 184 MB, throughput 0.654076
Reading from 5668: heap size 192 MB, throughput 0.681458
Reading from 5668: heap size 195 MB, throughput 0.705177
Reading from 5669: heap size 190 MB, throughput 0.834723
Reading from 5668: heap size 199 MB, throughput 0.428908
Reading from 5668: heap size 245 MB, throughput 0.635528
Reading from 5668: heap size 252 MB, throughput 0.54788
Reading from 5668: heap size 256 MB, throughput 0.656006
Reading from 5668: heap size 260 MB, throughput 0.656077
Reading from 5669: heap size 192 MB, throughput 0.583392
Reading from 5668: heap size 269 MB, throughput 0.569434
Reading from 5669: heap size 238 MB, throughput 0.784212
Reading from 5669: heap size 245 MB, throughput 0.694262
Reading from 5668: heap size 273 MB, throughput 0.486771
Reading from 5668: heap size 284 MB, throughput 0.49385
Reading from 5669: heap size 248 MB, throughput 0.737309
Reading from 5669: heap size 253 MB, throughput 0.709409
Reading from 5669: heap size 254 MB, throughput 0.688682
Reading from 5669: heap size 259 MB, throughput 0.702856
Reading from 5669: heap size 260 MB, throughput 0.750687
Reading from 5668: heap size 293 MB, throughput 0.392439
Reading from 5669: heap size 263 MB, throughput 0.738954
Reading from 5668: heap size 344 MB, throughput 0.54455
Reading from 5669: heap size 264 MB, throughput 0.750047
Reading from 5669: heap size 268 MB, throughput 0.675565
Reading from 5669: heap size 269 MB, throughput 0.647952
Reading from 5668: heap size 342 MB, throughput 0.204414
Reading from 5668: heap size 399 MB, throughput 0.505984
Reading from 5668: heap size 393 MB, throughput 0.537954
Reading from 5669: heap size 265 MB, throughput 0.509827
Reading from 5668: heap size 339 MB, throughput 0.609363
Reading from 5669: heap size 311 MB, throughput 0.661821
Reading from 5669: heap size 313 MB, throughput 0.708879
Reading from 5668: heap size 392 MB, throughput 0.634266
Reading from 5668: heap size 394 MB, throughput 0.630903
Reading from 5668: heap size 396 MB, throughput 0.634668
Reading from 5668: heap size 401 MB, throughput 0.611728
Reading from 5669: heap size 313 MB, throughput 0.921621
Reading from 5668: heap size 404 MB, throughput 0.554179
Reading from 5669: heap size 316 MB, throughput 0.910567
Reading from 5669: heap size 318 MB, throughput 0.856638
Reading from 5668: heap size 414 MB, throughput 0.530836
Reading from 5669: heap size 318 MB, throughput 0.840059
Reading from 5668: heap size 420 MB, throughput 0.513538
Reading from 5669: heap size 320 MB, throughput 0.871675
Reading from 5668: heap size 430 MB, throughput 0.480501
Reading from 5669: heap size 318 MB, throughput 0.818311
Reading from 5668: heap size 435 MB, throughput 0.438789
Reading from 5669: heap size 321 MB, throughput 0.853743
Reading from 5669: heap size 321 MB, throughput 0.824321
Reading from 5669: heap size 323 MB, throughput 0.742066
Reading from 5669: heap size 325 MB, throughput 0.71835
Reading from 5669: heap size 325 MB, throughput 0.654728
Reading from 5669: heap size 331 MB, throughput 0.63712
Numeric result:
Recommendation: 2 clients, utility 0.415872:
    h1: 7711 MB (U(h) = 0.466263*h^0.0398954)
    h2: 1211 MB (U(h) = 0.61968*h^0.001)
Recommendation: 2 clients, utility 0.415872:
    h1: 7711 MB (U(h) = 0.466263*h^0.0398954)
    h2: 1211 MB (U(h) = 0.61968*h^0.001)
Reading from 5669: heap size 331 MB, throughput 0.589096
Reading from 5668: heap size 443 MB, throughput 0.376024
Reading from 5668: heap size 505 MB, throughput 0.395711
Reading from 5668: heap size 435 MB, throughput 0.439118
Reading from 5669: heap size 340 MB, throughput 0.915892
Reading from 5668: heap size 498 MB, throughput 0.159519
Reading from 5668: heap size 491 MB, throughput 0.398418
Reading from 5668: heap size 560 MB, throughput 0.478633
Reading from 5668: heap size 563 MB, throughput 0.529586
Reading from 5668: heap size 551 MB, throughput 0.553092
Reading from 5668: heap size 557 MB, throughput 0.495249
Reading from 5669: heap size 340 MB, throughput 0.951418
Reading from 5668: heap size 557 MB, throughput 0.521326
Reading from 5668: heap size 560 MB, throughput 0.58985
Reading from 5668: heap size 565 MB, throughput 0.600865
Reading from 5668: heap size 570 MB, throughput 0.351616
Reading from 5669: heap size 341 MB, throughput 0.969204
Reading from 5668: heap size 640 MB, throughput 0.456773
Reading from 5668: heap size 645 MB, throughput 0.518371
Reading from 5668: heap size 646 MB, throughput 0.546327
Reading from 5668: heap size 649 MB, throughput 0.53278
Reading from 5669: heap size 344 MB, throughput 0.971811
Reading from 5668: heap size 655 MB, throughput 0.792564
Reading from 5668: heap size 661 MB, throughput 0.794697
Reading from 5669: heap size 346 MB, throughput 0.969473
Reading from 5668: heap size 662 MB, throughput 0.829854
Reading from 5668: heap size 677 MB, throughput 0.758639
Reading from 5669: heap size 348 MB, throughput 0.970339
Reading from 5668: heap size 687 MB, throughput 0.504126
Reading from 5668: heap size 764 MB, throughput 0.515294
Reading from 5668: heap size 761 MB, throughput 0.51213
Reading from 5668: heap size 681 MB, throughput 0.440382
Reading from 5668: heap size 749 MB, throughput 0.411859
Reading from 5669: heap size 346 MB, throughput 0.960702
Numeric result:
Recommendation: 2 clients, utility 0.4318:
    h1: 7711 MB (U(h) = 0.389667*h^0.0850559)
    h2: 1211 MB (U(h) = 0.513907*h^0.001)
Recommendation: 2 clients, utility 0.4318:
    h1: 7711 MB (U(h) = 0.389667*h^0.0850559)
    h2: 1211 MB (U(h) = 0.513907*h^0.001)
Reading from 5668: heap size 756 MB, throughput 0.214104
Reading from 5668: heap size 834 MB, throughput 0.277638
Reading from 5668: heap size 835 MB, throughput 0.64873
Reading from 5668: heap size 843 MB, throughput 0.754999
Reading from 5669: heap size 349 MB, throughput 0.967011
Reading from 5668: heap size 843 MB, throughput 0.712458
Reading from 5668: heap size 839 MB, throughput 0.668446
Reading from 5668: heap size 842 MB, throughput 0.637303
Reading from 5669: heap size 348 MB, throughput 0.949884
Reading from 5668: heap size 842 MB, throughput 0.400995
Reading from 5668: heap size 933 MB, throughput 0.682911
Reading from 5668: heap size 934 MB, throughput 0.829071
Reading from 5668: heap size 940 MB, throughput 0.8683
Reading from 5668: heap size 946 MB, throughput 0.922101
Reading from 5668: heap size 946 MB, throughput 0.922127
Reading from 5668: heap size 938 MB, throughput 0.921388
Reading from 5669: heap size 349 MB, throughput 0.963014
Reading from 5668: heap size 804 MB, throughput 0.874272
Reading from 5668: heap size 911 MB, throughput 0.826787
Reading from 5668: heap size 808 MB, throughput 0.789568
Reading from 5668: heap size 898 MB, throughput 0.771316
Reading from 5668: heap size 811 MB, throughput 0.773605
Reading from 5668: heap size 889 MB, throughput 0.785208
Reading from 5668: heap size 817 MB, throughput 0.808463
Reading from 5668: heap size 885 MB, throughput 0.81224
Reading from 5668: heap size 891 MB, throughput 0.808924
Reading from 5668: heap size 881 MB, throughput 0.808592
Reading from 5669: heap size 352 MB, throughput 0.960825
Reading from 5668: heap size 886 MB, throughput 0.788538
Reading from 5668: heap size 881 MB, throughput 0.956093
Reading from 5669: heap size 353 MB, throughput 0.967955
Reading from 5668: heap size 885 MB, throughput 0.950835
Reading from 5668: heap size 887 MB, throughput 0.9166
Reading from 5668: heap size 890 MB, throughput 0.876192
Reading from 5668: heap size 897 MB, throughput 0.842788
Reading from 5668: heap size 899 MB, throughput 0.828505
Reading from 5669: heap size 356 MB, throughput 0.970375
Reading from 5668: heap size 906 MB, throughput 0.809553
Reading from 5668: heap size 907 MB, throughput 0.7954
Reading from 5668: heap size 914 MB, throughput 0.799911
Reading from 5668: heap size 915 MB, throughput 0.800528
Reading from 5668: heap size 922 MB, throughput 0.843415
Reading from 5668: heap size 923 MB, throughput 0.861701
Numeric result:
Recommendation: 2 clients, utility 0.640838:
    h1: 4661.95 MB (U(h) = 0.349238*h^0.112412)
    h2: 4260.05 MB (U(h) = 0.300906*h^0.102718)
Recommendation: 2 clients, utility 0.640838:
    h1: 4662.02 MB (U(h) = 0.349238*h^0.112412)
    h2: 4259.98 MB (U(h) = 0.300906*h^0.102718)
Reading from 5669: heap size 357 MB, throughput 0.972574
Reading from 5668: heap size 927 MB, throughput 0.856346
Reading from 5668: heap size 928 MB, throughput 0.793683
Reading from 5669: heap size 359 MB, throughput 0.922831
Reading from 5668: heap size 915 MB, throughput 0.677032
Reading from 5669: heap size 402 MB, throughput 0.959449
Reading from 5668: heap size 1041 MB, throughput 0.632487
Reading from 5669: heap size 401 MB, throughput 0.946841
Reading from 5669: heap size 402 MB, throughput 0.934524
Reading from 5668: heap size 1037 MB, throughput 0.686678
Reading from 5669: heap size 404 MB, throughput 0.935481
Reading from 5668: heap size 1042 MB, throughput 0.669099
Reading from 5668: heap size 1045 MB, throughput 0.702631
Reading from 5668: heap size 1047 MB, throughput 0.727443
Reading from 5668: heap size 1052 MB, throughput 0.756786
Reading from 5668: heap size 1055 MB, throughput 0.73235
Reading from 5668: heap size 1068 MB, throughput 0.743852
Reading from 5669: heap size 404 MB, throughput 0.98238
Reading from 5668: heap size 1068 MB, throughput 0.721679
Reading from 5668: heap size 1083 MB, throughput 0.740442
Reading from 5669: heap size 411 MB, throughput 0.986376
Reading from 5668: heap size 1084 MB, throughput 0.947626
Reading from 5669: heap size 412 MB, throughput 0.980052
Reading from 5668: heap size 1103 MB, throughput 0.954457
Reading from 5669: heap size 413 MB, throughput 0.982788
Numeric result:
Recommendation: 2 clients, utility 1.54899:
    h1: 2430.19 MB (U(h) = 0.294746*h^0.153386)
    h2: 6491.81 MB (U(h) = 0.043542*h^0.40982)
Recommendation: 2 clients, utility 1.54899:
    h1: 2429.86 MB (U(h) = 0.294746*h^0.153386)
    h2: 6492.14 MB (U(h) = 0.043542*h^0.40982)
Reading from 5668: heap size 1106 MB, throughput 0.957091
Reading from 5669: heap size 415 MB, throughput 0.98535
Reading from 5668: heap size 1114 MB, throughput 0.962856
Reading from 5669: heap size 411 MB, throughput 0.986661
Reading from 5668: heap size 1117 MB, throughput 0.95233
Reading from 5669: heap size 414 MB, throughput 0.984384
Reading from 5668: heap size 1115 MB, throughput 0.951815
Reading from 5669: heap size 415 MB, throughput 0.983105
Reading from 5668: heap size 1120 MB, throughput 0.455079
Reading from 5669: heap size 416 MB, throughput 0.980921
Numeric result:
Recommendation: 2 clients, utility 1.22716:
    h1: 3358.87 MB (U(h) = 0.279838*h^0.165731)
    h2: 5563.13 MB (U(h) = 0.106969*h^0.274566)
Recommendation: 2 clients, utility 1.22716:
    h1: 3358.31 MB (U(h) = 0.279838*h^0.165731)
    h2: 5563.69 MB (U(h) = 0.106969*h^0.274566)
Reading from 5668: heap size 1121 MB, throughput 0.569987
Reading from 5669: heap size 418 MB, throughput 0.974278
Reading from 5668: heap size 1124 MB, throughput 0.505099
Reading from 5669: heap size 420 MB, throughput 0.98218
Reading from 5669: heap size 421 MB, throughput 0.978274
Reading from 5669: heap size 423 MB, throughput 0.960889
Reading from 5669: heap size 422 MB, throughput 0.941174
Reading from 5669: heap size 426 MB, throughput 0.912024
Reading from 5668: heap size 1131 MB, throughput 0.342488
Reading from 5669: heap size 438 MB, throughput 0.974614
Reading from 5668: heap size 1133 MB, throughput 0.953607
Reading from 5669: heap size 438 MB, throughput 0.98476
Reading from 5668: heap size 1139 MB, throughput 0.952527
Reading from 5669: heap size 439 MB, throughput 0.985113
Numeric result:
Recommendation: 2 clients, utility 1.58753:
    h1: 3036.2 MB (U(h) = 0.253621*h^0.188947)
    h2: 5885.8 MB (U(h) = 0.0572702*h^0.366234)
Recommendation: 2 clients, utility 1.58753:
    h1: 3036.46 MB (U(h) = 0.253621*h^0.188947)
    h2: 5885.54 MB (U(h) = 0.0572702*h^0.366234)
Reading from 5668: heap size 1143 MB, throughput 0.947347
Reading from 5669: heap size 442 MB, throughput 0.985634
Reading from 5668: heap size 1149 MB, throughput 0.94811
Reading from 5669: heap size 440 MB, throughput 0.985121
Reading from 5668: heap size 1154 MB, throughput 0.954263
Reading from 5669: heap size 443 MB, throughput 0.986441
Reading from 5668: heap size 1160 MB, throughput 0.957807
Reading from 5669: heap size 443 MB, throughput 0.985962
Reading from 5668: heap size 1166 MB, throughput 0.951793
Reading from 5669: heap size 445 MB, throughput 0.983655
Numeric result:
Recommendation: 2 clients, utility 2.25778:
    h1: 2474.78 MB (U(h) = 0.243133*h^0.198789)
    h2: 6447.22 MB (U(h) = 0.0209078*h^0.517915)
Recommendation: 2 clients, utility 2.25778:
    h1: 2474.66 MB (U(h) = 0.243133*h^0.198789)
    h2: 6447.34 MB (U(h) = 0.0209078*h^0.517915)
Reading from 5668: heap size 1174 MB, throughput 0.953636
Reading from 5669: heap size 447 MB, throughput 0.982509
Reading from 5668: heap size 1178 MB, throughput 0.952318
Reading from 5669: heap size 448 MB, throughput 0.986012
Reading from 5669: heap size 451 MB, throughput 0.978614
Reading from 5669: heap size 452 MB, throughput 0.964389
Reading from 5669: heap size 457 MB, throughput 0.971278
Reading from 5668: heap size 1185 MB, throughput 0.950892
Reading from 5669: heap size 458 MB, throughput 0.984835
Reading from 5668: heap size 1188 MB, throughput 0.953469
Reading from 5669: heap size 463 MB, throughput 0.979985
Reading from 5668: heap size 1195 MB, throughput 0.948146
Numeric result:
Recommendation: 2 clients, utility 2.54513:
    h1: 2484.51 MB (U(h) = 0.229648*h^0.211978)
    h2: 6437.49 MB (U(h) = 0.0170933*h^0.549295)
Recommendation: 2 clients, utility 2.54513:
    h1: 2484.35 MB (U(h) = 0.229648*h^0.211978)
    h2: 6437.65 MB (U(h) = 0.0170933*h^0.549295)
Reading from 5669: heap size 464 MB, throughput 0.975429
Reading from 5668: heap size 1197 MB, throughput 0.945367
Reading from 5669: heap size 466 MB, throughput 0.981571
Reading from 5668: heap size 1204 MB, throughput 0.906871
Reading from 5669: heap size 467 MB, throughput 0.984841
Reading from 5668: heap size 1301 MB, throughput 0.939995
Reading from 5669: heap size 467 MB, throughput 0.986022
Numeric result:
Recommendation: 2 clients, utility 2.91422:
    h1: 2342.21 MB (U(h) = 0.224977*h^0.216684)
    h2: 6579.79 MB (U(h) = 0.0114266*h^0.608747)
Recommendation: 2 clients, utility 2.91422:
    h1: 2342.11 MB (U(h) = 0.224977*h^0.216684)
    h2: 6579.89 MB (U(h) = 0.0114266*h^0.608747)
Reading from 5668: heap size 1301 MB, throughput 0.959686
Reading from 5669: heap size 468 MB, throughput 0.985477
Reading from 5668: heap size 1309 MB, throughput 0.969143
Reading from 5669: heap size 471 MB, throughput 0.989157
Reading from 5669: heap size 472 MB, throughput 0.983388
Reading from 5669: heap size 473 MB, throughput 0.970836
Reading from 5669: heap size 475 MB, throughput 0.972889
Reading from 5668: heap size 1319 MB, throughput 0.972899
Reading from 5669: heap size 483 MB, throughput 0.986326
Reading from 5668: heap size 1319 MB, throughput 0.973019
Numeric result:
Recommendation: 2 clients, utility 3.40196:
    h1: 2231.4 MB (U(h) = 0.217221*h^0.22467)
    h2: 6690.6 MB (U(h) = 0.00733524*h^0.673666)
Recommendation: 2 clients, utility 3.40196:
    h1: 2231.36 MB (U(h) = 0.217221*h^0.22467)
    h2: 6690.64 MB (U(h) = 0.00733524*h^0.673666)
Reading from 5669: heap size 484 MB, throughput 0.987839
Reading from 5668: heap size 1315 MB, throughput 0.971427
Reading from 5669: heap size 486 MB, throughput 0.989299
Reading from 5668: heap size 1319 MB, throughput 0.970862
Reading from 5669: heap size 487 MB, throughput 0.989435
Reading from 5668: heap size 1306 MB, throughput 0.972166
Reading from 5669: heap size 485 MB, throughput 0.988758
Reading from 5668: heap size 1313 MB, throughput 0.972749
Numeric result:
Recommendation: 2 clients, utility 4.24882:
    h1: 2029.32 MB (U(h) = 0.212733*h^0.229381)
    h2: 6892.68 MB (U(h) = 0.00355792*h^0.779127)
Recommendation: 2 clients, utility 4.24882:
    h1: 2029.28 MB (U(h) = 0.212733*h^0.229381)
    h2: 6892.72 MB (U(h) = 0.00355792*h^0.779127)
Reading from 5669: heap size 487 MB, throughput 0.987831
Reading from 5668: heap size 1307 MB, throughput 0.969045
Reading from 5669: heap size 490 MB, throughput 0.989663
Reading from 5668: heap size 1310 MB, throughput 0.965619
Reading from 5669: heap size 491 MB, throughput 0.986265
Reading from 5669: heap size 490 MB, throughput 0.978238
Reading from 5669: heap size 493 MB, throughput 0.977237
Reading from 5668: heap size 1314 MB, throughput 0.96415
Reading from 5669: heap size 500 MB, throughput 0.988286
Reading from 5668: heap size 1316 MB, throughput 0.964122
Numeric result:
Recommendation: 2 clients, utility 6.44374:
    h1: 2662.87 MB (U(h) = 0.0985235*h^0.363706)
    h2: 6259.13 MB (U(h) = 0.00211016*h^0.854853)
Recommendation: 2 clients, utility 6.44374:
    h1: 2662.97 MB (U(h) = 0.0985235*h^0.363706)
    h2: 6259.03 MB (U(h) = 0.00211016*h^0.854853)
Reading from 5669: heap size 500 MB, throughput 0.989694
Reading from 5668: heap size 1321 MB, throughput 0.957831
Reading from 5669: heap size 501 MB, throughput 0.990177
Reading from 5668: heap size 1327 MB, throughput 0.95684
Reading from 5669: heap size 503 MB, throughput 0.990373
Reading from 5668: heap size 1333 MB, throughput 0.957758
Reading from 5669: heap size 501 MB, throughput 0.989272
Numeric result:
Recommendation: 2 clients, utility 7.41272:
    h1: 3554.9 MB (U(h) = 0.0440295*h^0.50216)
    h2: 5367.1 MB (U(h) = 0.00412676*h^0.758107)
Recommendation: 2 clients, utility 7.41272:
    h1: 3555.02 MB (U(h) = 0.0440295*h^0.50216)
    h2: 5366.98 MB (U(h) = 0.00412676*h^0.758107)
Reading from 5668: heap size 1342 MB, throughput 0.958796
Reading from 5669: heap size 503 MB, throughput 0.98785
Reading from 5668: heap size 1351 MB, throughput 0.957161
Reading from 5669: heap size 505 MB, throughput 0.9904
Reading from 5669: heap size 506 MB, throughput 0.982405
Reading from 5669: heap size 506 MB, throughput 0.973287
Reading from 5669: heap size 508 MB, throughput 0.986333
Reading from 5668: heap size 1356 MB, throughput 0.971988
Numeric result:
Recommendation: 2 clients, utility 12.3991:
    h1: 4340.01 MB (U(h) = 0.012449*h^0.717042)
    h2: 4581.99 MB (U(h) = 0.0041545*h^0.757024)
Recommendation: 2 clients, utility 12.3991:
    h1: 4340 MB (U(h) = 0.012449*h^0.717042)
    h2: 4582 MB (U(h) = 0.0041545*h^0.757024)
Reading from 5669: heap size 515 MB, throughput 0.988934
Reading from 5669: heap size 517 MB, throughput 0.989986
Reading from 5669: heap size 517 MB, throughput 0.989673
Reading from 5669: heap size 520 MB, throughput 0.989482
Numeric result:
Recommendation: 2 clients, utility 18.4607:
    h1: 4789.94 MB (U(h) = 0.00478905*h^0.877626)
    h2: 4132.06 MB (U(h) = 0.0041545*h^0.757024)
Recommendation: 2 clients, utility 18.4607:
    h1: 4790.13 MB (U(h) = 0.00478905*h^0.877626)
    h2: 4131.87 MB (U(h) = 0.0041545*h^0.757024)
Reading from 5669: heap size 521 MB, throughput 0.989199
Reading from 5669: heap size 522 MB, throughput 0.990755
Reading from 5669: heap size 525 MB, throughput 0.984935
Reading from 5669: heap size 526 MB, throughput 0.980815
Reading from 5669: heap size 533 MB, throughput 0.989564
Numeric result:
Recommendation: 2 clients, utility 10.9564:
    h1: 4174.74 MB (U(h) = 0.0168901*h^0.665729)
    h2: 4747.26 MB (U(h) = 0.0041545*h^0.757024)
Recommendation: 2 clients, utility 10.9564:
    h1: 4174.75 MB (U(h) = 0.0168901*h^0.665729)
    h2: 4747.25 MB (U(h) = 0.0041545*h^0.757024)
Reading from 5669: heap size 534 MB, throughput 0.990768
Reading from 5668: heap size 1361 MB, throughput 0.990572
Reading from 5669: heap size 534 MB, throughput 0.99121
Reading from 5669: heap size 536 MB, throughput 0.990764
Numeric result:
Recommendation: 2 clients, utility 6.88025:
    h1: 3341.78 MB (U(h) = 0.0598117*h^0.455128)
    h2: 5580.22 MB (U(h) = 0.00407295*h^0.759891)
Recommendation: 2 clients, utility 6.88025:
    h1: 3342.05 MB (U(h) = 0.0598117*h^0.455128)
    h2: 5579.95 MB (U(h) = 0.00407295*h^0.759891)
Reading from 5668: heap size 1369 MB, throughput 0.986223
Reading from 5669: heap size 536 MB, throughput 0.986305
Reading from 5668: heap size 1362 MB, throughput 0.964786
Reading from 5668: heap size 1414 MB, throughput 0.970971
Reading from 5668: heap size 1430 MB, throughput 0.964557
Reading from 5668: heap size 1434 MB, throughput 0.95446
Reading from 5668: heap size 1433 MB, throughput 0.940538
Reading from 5668: heap size 1438 MB, throughput 0.928645
Reading from 5668: heap size 1438 MB, throughput 0.919866
Reading from 5669: heap size 537 MB, throughput 0.989895
Reading from 5668: heap size 1444 MB, throughput 0.972946
Reading from 5669: heap size 539 MB, throughput 0.983927
Reading from 5669: heap size 540 MB, throughput 0.975272
Reading from 5668: heap size 1449 MB, throughput 0.987821
Reading from 5669: heap size 549 MB, throughput 0.988215
Numeric result:
Recommendation: 2 clients, utility 6.66499:
    h1: 3368.39 MB (U(h) = 0.0600141*h^0.453982)
    h2: 5553.61 MB (U(h) = 0.00437872*h^0.7485)
Recommendation: 2 clients, utility 6.66499:
    h1: 3368.39 MB (U(h) = 0.0600141*h^0.453982)
    h2: 5553.61 MB (U(h) = 0.00437872*h^0.7485)
Reading from 5668: heap size 1457 MB, throughput 0.986222
Reading from 5669: heap size 549 MB, throughput 0.992064
Reading from 5668: heap size 1464 MB, throughput 0.987016
Reading from 5669: heap size 550 MB, throughput 0.991742
Reading from 5668: heap size 1468 MB, throughput 0.986582
Reading from 5669: heap size 553 MB, throughput 0.991431
Numeric result:
Recommendation: 2 clients, utility 7.17965:
    h1: 3196.32 MB (U(h) = 0.0617582*h^0.448991)
    h2: 5725.68 MB (U(h) = 0.00294798*h^0.804275)
Recommendation: 2 clients, utility 7.17965:
    h1: 3196.37 MB (U(h) = 0.0617582*h^0.448991)
    h2: 5725.63 MB (U(h) = 0.00294798*h^0.804275)
Reading from 5668: heap size 1455 MB, throughput 0.982234
Reading from 5669: heap size 556 MB, throughput 0.99082
Reading from 5668: heap size 1312 MB, throughput 0.980136
Reading from 5669: heap size 556 MB, throughput 0.989783
Reading from 5668: heap size 1440 MB, throughput 0.980083
Reading from 5669: heap size 559 MB, throughput 0.985497
Reading from 5669: heap size 560 MB, throughput 0.983495
Reading from 5668: heap size 1340 MB, throughput 0.976482
Numeric result:
Recommendation: 2 clients, utility 6.17674:
    h1: 3610.2 MB (U(h) = 0.0555196*h^0.465957)
    h2: 5311.8 MB (U(h) = 0.00683478*h^0.685575)
Recommendation: 2 clients, utility 6.17674:
    h1: 3610.2 MB (U(h) = 0.0555196*h^0.465957)
    h2: 5311.8 MB (U(h) = 0.00683478*h^0.685575)
Reading from 5669: heap size 568 MB, throughput 0.990809
Reading from 5668: heap size 1438 MB, throughput 0.972741
Reading from 5669: heap size 569 MB, throughput 0.991631
Reading from 5668: heap size 1444 MB, throughput 0.971323
Reading from 5669: heap size 567 MB, throughput 0.991208
Reading from 5668: heap size 1449 MB, throughput 0.96997
Numeric result:
Recommendation: 2 clients, utility 5.93369:
    h1: 3517.08 MB (U(h) = 0.0621983*h^0.446908)
    h2: 5404.92 MB (U(h) = 0.00677866*h^0.686775)
Recommendation: 2 clients, utility 5.93369:
    h1: 3517.13 MB (U(h) = 0.0621983*h^0.446908)
    h2: 5404.87 MB (U(h) = 0.00677866*h^0.686775)
Reading from 5669: heap size 570 MB, throughput 0.990703
Reading from 5668: heap size 1451 MB, throughput 0.968709
Reading from 5669: heap size 572 MB, throughput 0.991914
Reading from 5668: heap size 1458 MB, throughput 0.966177
Reading from 5669: heap size 573 MB, throughput 0.98873
Reading from 5669: heap size 572 MB, throughput 0.983901
Numeric result:
Recommendation: 2 clients, utility 4.11744:
    h1: 4170.53 MB (U(h) = 0.0742181*h^0.41768)
    h2: 4751.47 MB (U(h) = 0.0303506*h^0.475918)
Recommendation: 2 clients, utility 4.11744:
    h1: 4170.27 MB (U(h) = 0.0742181*h^0.41768)
    h2: 4751.73 MB (U(h) = 0.0303506*h^0.475918)
Reading from 5668: heap size 1465 MB, throughput 0.964815
Reading from 5669: heap size 575 MB, throughput 0.990914
Reading from 5668: heap size 1473 MB, throughput 0.962465
Reading from 5669: heap size 577 MB, throughput 0.991934
Reading from 5668: heap size 1484 MB, throughput 0.958002
Reading from 5669: heap size 579 MB, throughput 0.991681
Numeric result:
Recommendation: 2 clients, utility 3.64265:
    h1: 4099.39 MB (U(h) = 0.0932332*h^0.38025)
    h2: 4822.61 MB (U(h) = 0.0371992*h^0.447312)
Recommendation: 2 clients, utility 3.64265:
    h1: 4099.5 MB (U(h) = 0.0932332*h^0.38025)
    h2: 4822.5 MB (U(h) = 0.0371992*h^0.447312)
Reading from 5668: heap size 1497 MB, throughput 0.957579
Reading from 5669: heap size 579 MB, throughput 0.991104
Reading from 5668: heap size 1505 MB, throughput 0.958608
Reading from 5669: heap size 580 MB, throughput 0.985166
Reading from 5669: heap size 618 MB, throughput 0.990278
Reading from 5669: heap size 624 MB, throughput 0.979949
Reading from 5668: heap size 1520 MB, throughput 0.958445
Numeric result:
Recommendation: 2 clients, utility 2.50864:
    h1: 3271.39 MB (U(h) = 0.232993*h^0.231612)
    h2: 5650.61 MB (U(h) = 0.0521212*h^0.40005)
Recommendation: 2 clients, utility 2.50864:
    h1: 3271.44 MB (U(h) = 0.232993*h^0.231612)
    h2: 5650.56 MB (U(h) = 0.0521212*h^0.40005)
Reading from 5669: heap size 624 MB, throughput 0.989903
Reading from 5668: heap size 1524 MB, throughput 0.958891
Reading from 5669: heap size 626 MB, throughput 0.989779
Reading from 5668: heap size 1538 MB, throughput 0.957789
Reading from 5668: heap size 1541 MB, throughput 0.959584
Reading from 5669: heap size 631 MB, throughput 0.99181
Numeric result:
Recommendation: 2 clients, utility 2.49205:
    h1: 3061.71 MB (U(h) = 0.254268*h^0.217312)
    h2: 5860.29 MB (U(h) = 0.0463919*h^0.415954)
Recommendation: 2 clients, utility 2.49205:
    h1: 3061.68 MB (U(h) = 0.254268*h^0.217312)
    h2: 5860.32 MB (U(h) = 0.0463919*h^0.415954)
Reading from 5668: heap size 1554 MB, throughput 0.96392
Reading from 5669: heap size 633 MB, throughput 0.991413
Reading from 5668: heap size 1556 MB, throughput 0.961349
Reading from 5669: heap size 635 MB, throughput 0.99277
Reading from 5669: heap size 636 MB, throughput 0.989348
Reading from 5669: heap size 637 MB, throughput 0.986995
Numeric result:
Recommendation: 2 clients, utility 2.30336:
    h1: 2370.48 MB (U(h) = 0.370201*h^0.156617)
    h2: 6551.52 MB (U(h) = 0.041051*h^0.432886)
Recommendation: 2 clients, utility 2.30336:
    h1: 2370.37 MB (U(h) = 0.370201*h^0.156617)
    h2: 6551.63 MB (U(h) = 0.041051*h^0.432886)
Reading from 5668: heap size 1569 MB, throughput 0.952103
Reading from 5669: heap size 640 MB, throughput 0.991764
Reading from 5668: heap size 1703 MB, throughput 0.975985
Reading from 5669: heap size 640 MB, throughput 0.992593
Reading from 5668: heap size 1705 MB, throughput 0.98054
Reading from 5669: heap size 643 MB, throughput 0.991904
Numeric result:
Recommendation: 2 clients, utility 2.03039:
    h1: 1065.46 MB (U(h) = 0.683784*h^0.0583089)
    h2: 7856.54 MB (U(h) = 0.0418269*h^0.429929)
Recommendation: 2 clients, utility 2.03039:
    h1: 1065.53 MB (U(h) = 0.683784*h^0.0583089)
    h2: 7856.47 MB (U(h) = 0.0418269*h^0.429929)
Reading from 5668: heap size 1717 MB, throughput 0.982753
Reading from 5669: heap size 645 MB, throughput 0.991345
Reading from 5668: heap size 1731 MB, throughput 0.983471
Reading from 5669: heap size 646 MB, throughput 0.991913
Reading from 5669: heap size 651 MB, throughput 0.986769
Client 5669 died
Clients: 1
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 5668: heap size 1733 MB, throughput 0.995887
Recommendation: one client; give it all the memory
Reading from 5668: heap size 1711 MB, throughput 0.993907
Reading from 5668: heap size 1725 MB, throughput 0.986433
Reading from 5668: heap size 1715 MB, throughput 0.975618
Reading from 5668: heap size 1744 MB, throughput 0.955404
Reading from 5668: heap size 1777 MB, throughput 0.93178
Client 5668 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
