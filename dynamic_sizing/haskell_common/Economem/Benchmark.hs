{-# LANGUAGE OverloadedStrings #-}

module Economem.Benchmark (BenchmarkSpec(..),
                           makeBenchmark,
                           benchmarkProcessLauncher,
                           checkBenchmarkPrerequisites,
                           AvailableBenchmark(..),
                           allDacapoBenchmarks,
                           filterBenchmarks,
                           benchmarksWithThreads) where

import Codec.Archive.Zip
import Control.Applicative
import Control.Conditional
import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as L
import qualified Data.ByteString.Lazy.Char8 as L8
import Data.List hiding(group)
import Data.Maybe
import Economem.Common
import Economem.Cgroup
import Economem.Daemon
import Economem.DelayedStart
import Economem.Java
import Economem.Process
import Economem.Records
import Economem.ResourceUsageFile
import Network.Download
import System.Directory
import System.FilePath

data BenchmarkType = Dacapo912 String
                   | Dacapo2006 String
                   | SpecJBB2005
                   | SpecJBB2013C -- 'composite' (single-VM) version of specjbb2013
                   | SpecJBB2013MultiController
                   | SpecJBB2013TxInjector Int Int -- group, vmID
                   | SpecJBB2013Backend Int Int -- group, vmID
                   | GCBench

data BenchmarkSpec = BenchmarkSpec { benchmarkType :: BenchmarkType,
                                     benchmarkSize :: String,
                                     benchmarkIterations :: Int,
                                     benchmarkThreads :: Int,
                                     benchmarkMaxHeap :: DataSize,
                                     benchmarkStartingHeap :: DataSize,
                                     benchmarkGCLogging :: Bool,
                                     benchmarkExtraVMArgs :: [String] }

makeBenchmark :: BenchmarkThreadsIters -> DataSize -> IO BenchmarkSpec
makeBenchmark (BenchmarkThreadsIters (BenchmarkThreads (Benchmark name size) threads) iters) maxHeap =
  benchmarkFromName >>= \n ->
  return BenchmarkSpec { benchmarkType = n,
                         benchmarkSize = size,
                         benchmarkIterations = iters,
                         benchmarkThreads = threads,
                         benchmarkMaxHeap = maxHeap,
                         benchmarkStartingHeap = DataSize (min 10 (sizeToInt (toMB maxHeap))) MB,
                         benchmarkGCLogging = False,
                         benchmarkExtraVMArgs = [] }
  where
    benchmarkFromName :: IO BenchmarkType
    benchmarkFromName = case args of
      "jbb2005":[] -> return SpecJBB2005
      "jbb2013c":[] -> return SpecJBB2013C
      "gcbench":[] -> return GCBench
      "jbb2013multiController":[] -> return SpecJBB2013MultiController
      "jbb2013txInjector":g:i:[] -> parseJBBMultiArgs g i SpecJBB2013TxInjector
      "jbb2013backend":g:i:[] -> parseJBBMultiArgs g i SpecJBB2013Backend
      s:[] -> return (if "2006" `isSuffixOf` s then
                        Dacapo2006 (take (length s - 4) s)
                      else
                        Dacapo912 s)
      _ -> fail ("Invalid benchmark name: '" ++ name ++ "'")

    parseJBBMultiArgs :: String -> String -> (Int -> Int -> BenchmarkType) -> IO BenchmarkType
    parseJBBMultiArgs group vmID f = f <$> readIO group <*> readIO vmID

    args :: [String]
    args = split name '-'

    split :: Eq a => [a] -> a -> [[a]]
    split [] _ = []
    split l e =
      let (h, t) = break (== e) l in
      h:(case t of
          [] -> []
          _:t' -> split t' e)

benchmarkToArgs :: BenchmarkSpec -> FilePath -> FilePath -> Java -> IO [String]
benchmarkToArgs b scratchDir workingDir j = do
  bm <- bmArgs
  return (
    vmArgs ++
    benchmarkExtraVMArgs b ++
    bm)
  where
    vmArgs :: [String]
    vmArgs =
      javaStartingHeapToArgs j (benchmarkStartingHeap b) ++
      javaMaxHeapToArgs j (benchmarkMaxHeap b) ++
      javaDisableExplicitGCArgs j ++
      (if benchmarkGCLogging b then
         javaGCLogToArgs j "gc.log"
       else
         [])

    bmArgs :: IO [String]
    bmArgs = case benchmarkType b of
      Dacapo912 s -> do
        jarPath <- dacapoJarPath
        return (["-jar",
                 jarPath,
                 "--no-pre-iteration-gc",
                 "--scratch-directory",
                 scratchDir,
                 "-t",
                 show (benchmarkThreads b),
                 "-n",
                 show (benchmarkIterations b),
                 "-s",
                 benchmarkSize b,
                 s])
      Dacapo2006 s -> do
        jarPath <- dacapo2006JarPath
        return (["-jar",
                 jarPath,
                 "-s",
                 benchmarkSize b,
                 "-n",
                 show (benchmarkIterations b),
                 s])
      SpecJBB2005 -> do
        specPath <- (</> "SpecJBB05" </> "build" </> "dist") <$> specjbb2005Dir
        return (["-classpath",
                 (specPath </> "jbb.jar") ++ ":" ++ (specPath </> "check.jar"),
                 "spec.jbb.JBBmain",
                 "-propfile",
                 workingDir </> "SPECjbb.props",
                 "-n",
                 show (benchmarkIterations b)])
      SpecJBB2013C -> do
        specPath <- specjbb2013Dir
        return (["-jar",
                 specPath </> "specjbb2013" </> "specjbb2013.jar",
                 "-m",
                 "COMPOSITE",
                 "-p",
                 specPath </> "composite.props",
                 "-skipReport"])
      SpecJBB2013MultiController -> do
        specPath <- specjbb2013Dir
        return (["-jar",
                 specPath </> "specjbb2013" </> "specjbb2013.jar",
                 "-m",
                 "MULTICONTROLLER",
                 "-p",
                 specPath </> "multi.props",
                 "-v",
                 "-skipReport"])
      SpecJBB2013TxInjector group vmID -> do
        specPath <- specjbb2013Dir
        return (["-jar",
                 specPath </> "specjbb2013" </> "specjbb2013.jar",
                 "-m",
                 "TXINJECTOR",
                 "-p",
                 specPath </> "multi.props",
                 "-G",
                 show group,
                 "-J",
                 show vmID,
                 "-v"])
      SpecJBB2013Backend group vmID -> do
        specPath <- specjbb2013Dir
        return (["-jar",
                 specPath </> "specjbb2013" </> "specjbb2013.jar",
                 "-m",
                 "BACKEND",
                 "-p",
                 specPath </> "multi.props",
                 "-G",
                 show group,
                 "-J",
                 show vmID,
                 "-v"])
      GCBench -> do
        dir <- gcbenchDir
        return (["-classpath",
                 (dir </> "gcbench" </> "dist" </> "gcbench.jar") ++ ":" ++ dir,
                 "GCBenchWrapper",
                 show (benchmarkIterations b),
                 show (benchmarkThreads b)])


dacapoJarPath :: IO FilePath
dacapoJarPath = getJarPath "dacapo-9.12-bach.jar"

dacapo2006JarPath :: IO FilePath
dacapo2006JarPath = getJarPath "dacapo-2006-10-MR2.jar"

getJarPath :: FilePath -> IO FilePath
getJarPath f = (</> "haskell_common" </> f) <$> economemRoot

checkBenchmarkPrerequisites :: IO ()
checkBenchmarkPrerequisites = do
  dacapoJarPath >>= checkJar
    "http://optimate.dl.sourceforge.net/project/dacapobench/9.12-bach/dacapo-9.12-bach.jar"
    "Dacapo 9.12"
  dacapo2006JarPath >>= checkJar
    "http://downloads.sourceforge.net/project/dacapobench/archive/2006-10-MR2/dacapo-2006-10-MR2.jar"
    "Dacapo 2006"

checkJar :: String -> String -> FilePath -> IO ()
checkJar url name path = do
  exists <- doesFileExist path
  unless exists
    (do putStrLn ("Downloading " ++ name ++ " jar...")
        download <- openURI url
        case download of
         Left _ -> fail "Download failed"
         Right downloadData -> do
           B.writeFile path downloadData
           putStrLn "Download succeeded"
           return ())

specjbb2005Dir :: IO FilePath
specjbb2005Dir = (</> "specjbb2005") <$> economemRoot

specjbb2013Dir :: IO FilePath
specjbb2013Dir = (</> "specjbb2013") <$> economemRoot

gcbenchDir :: IO FilePath
gcbenchDir = (</> "gcbench") <$> economemRoot


benchmarkProcessLauncher :: BenchmarkSpec ->
                            Java ->
                            Maybe FilePath ->
                            Maybe FilePath ->
                            FilePath ->
                            FilePath ->
                            Maybe Cgroup ->
                            Maybe ResourceUsageFile ->
                            Maybe DelayedStart ->
                            [ThroughputType] ->
                            Maybe DataSize ->
                            Bool ->
                            IO ProcessLauncher
benchmarkProcessLauncher b j infile outfile workingDir scratchDir cg res delay throughputs daemonMinHeap forceGrowth = do
  validateDir workingDir
  validateDir scratchDir
  args <- benchmarkToArgs b scratchDir workingDir j
  p <- javaProcessLauncher
       j
       infile
       outfile
       workingDir
       args
       cg
       res
       delay
       throughputs
       daemonMinHeap
       forceGrowth
  extra <- extraSetup
  return (p { procSetupActions = [MkDir scratchDir, MkDir workingDir] ++ extra,
              procCleanupActions = [RmDir scratchDir, RmDir workingDir] })

  where
    validateDir :: FilePath -> IO ()
    validateDir dir = whenM (doesDirectoryExist dir)
                      (fail "Cannot specify an existing directory for the working dir or scratch dir")

    extraSetup :: IO [ProcSetupAction]
    extraSetup = case benchmarkType b of
      Dacapo912 _ -> return []
      Dacapo2006 _ -> return []
      SpecJBB2005 -> do
        setupCmd <- (</> "setup_specjbb2005.sh") <$> specjbb2005Dir
        return [Command setupCmd [workingDir, show (benchmarkThreads b)]]
      SpecJBB2013C -> return []
      SpecJBB2013MultiController -> return []
      SpecJBB2013TxInjector _ _ -> return []
      SpecJBB2013Backend _ _ -> return []
      GCBench -> return []


data AvailableBenchmark = AvailableBenchmark { availName :: String,
                                               availSize :: String,
                                               availMultithreaded :: Bool } deriving (Show, Eq, Ord)

allDacapoBenchmarks :: IO [AvailableBenchmark]
allDacapoBenchmarks = do
  checkBenchmarkPrerequisites
  path <- dacapoJarPath
  raw <- L.readFile path
  let zipData = toArchive raw
  let confFiles = filter isInteresting (zEntries zipData)
  return (sort (concatMap details confFiles))

  where
    isInteresting :: Entry -> Bool
    isInteresting e = let path = eRelativePath e in
      "cnf/" `isPrefixOf` path && ".cnf" `isSuffixOf` path

    details :: Entry -> [AvailableBenchmark]
    details e = case name of
      Nothing -> []
      Just n -> map (\s -> AvailableBenchmark { availName = L8.unpack n,
                                                availSize = L8.unpack s,
                                                availMultithreaded = multiThreaded})
                sizes
      where
        name :: Maybe L8.ByteString
        name = getProp "benchmark"

        multiThreaded :: Bool
        multiThreaded = case getProp "thread-model" of
          Just "per_cpu" -> True
          _ -> False

        sizes :: [L8.ByteString]
        sizes = mapMaybe getSize (filter (\ws -> not (null ws) && head ws == "size") text)
          where getSize :: [L.ByteString] -> Maybe L.ByteString
                getSize (_:s:_) = Just s
                getSize _ = Nothing

        getProp :: L8.ByteString -> Maybe L8.ByteString
        getProp p = do
          line <- find (\ws -> not (null ws) && head ws == p) text
          case line of
            (_:n:_) -> Just n
            _ -> Nothing

        text :: [[L8.ByteString]]
        text = map L8.words (L8.lines (fromEntry e))


filterBenchmarks :: [AvailableBenchmark] -> [BenchmarkFilter] -> [AvailableBenchmark]
filterBenchmarks bs fs = filter shouldKeep bs
  where
    shouldKeep :: AvailableBenchmark -> Bool
    shouldKeep b = not (any matches fs)
      where
        matches :: BenchmarkFilter -> Bool
        matches f = (availName b == filterName f || filterName f == "*") &&
                    (availSize b == filterSize f || filterSize f == "*")


benchmarksWithThreads :: [AvailableBenchmark] -> Int -> [BenchmarkThreads]
benchmarksWithThreads bs maxThreads = concatMap each bs
  where
    each :: AvailableBenchmark -> [BenchmarkThreads]
    each b = if availMultithreaded b then
               map (BenchmarkThreads (Benchmark name size))
               [1..maxThreads]
             else [BenchmarkThreads (Benchmark name size) 1]
      where
        name = availName b
        size = availSize b
