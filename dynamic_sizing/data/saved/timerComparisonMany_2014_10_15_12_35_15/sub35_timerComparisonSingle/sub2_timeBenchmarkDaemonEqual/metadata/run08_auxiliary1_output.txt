economemd
    total memory: 7410 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub35_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 23741: heap size 9 MB, throughput 0.984273
Clients: 1
Client 23741 has a minimum heap size of 12 MB
Reading from 23740: heap size 9 MB, throughput 0.992251
Clients: 2
Client 23740 has a minimum heap size of 1223 MB
Reading from 23740: heap size 9 MB, throughput 0.965996
Reading from 23741: heap size 9 MB, throughput 0.991704
Reading from 23740: heap size 9 MB, throughput 0.948448
Reading from 23740: heap size 9 MB, throughput 0.954017
Reading from 23741: heap size 11 MB, throughput 0.981446
Reading from 23740: heap size 11 MB, throughput 0.984263
Reading from 23741: heap size 11 MB, throughput 0.982795
Reading from 23740: heap size 11 MB, throughput 0.972833
Reading from 23741: heap size 15 MB, throughput 0.984104
Reading from 23740: heap size 17 MB, throughput 0.938381
Reading from 23740: heap size 17 MB, throughput 0.652307
Reading from 23741: heap size 15 MB, throughput 0.986242
Reading from 23740: heap size 30 MB, throughput 0.937061
Reading from 23740: heap size 31 MB, throughput 0.891423
Reading from 23740: heap size 36 MB, throughput 0.351945
Reading from 23740: heap size 48 MB, throughput 0.71963
Reading from 23741: heap size 25 MB, throughput 0.916919
Reading from 23740: heap size 51 MB, throughput 0.275107
Reading from 23740: heap size 67 MB, throughput 0.86027
Reading from 23741: heap size 30 MB, throughput 0.989804
Reading from 23740: heap size 74 MB, throughput 0.341947
Reading from 23740: heap size 91 MB, throughput 0.770692
Reading from 23741: heap size 35 MB, throughput 0.987332
Reading from 23740: heap size 98 MB, throughput 0.85584
Reading from 23741: heap size 38 MB, throughput 0.990122
Reading from 23740: heap size 98 MB, throughput 0.283824
Reading from 23741: heap size 39 MB, throughput 0.952153
Reading from 23740: heap size 133 MB, throughput 0.784346
Reading from 23741: heap size 43 MB, throughput 0.981807
Reading from 23740: heap size 133 MB, throughput 0.735939
Reading from 23741: heap size 49 MB, throughput 0.990759
Reading from 23741: heap size 49 MB, throughput 0.990707
Reading from 23740: heap size 133 MB, throughput 0.15063
Reading from 23740: heap size 170 MB, throughput 0.761551
Reading from 23741: heap size 54 MB, throughput 0.959709
Reading from 23740: heap size 175 MB, throughput 0.792727
Reading from 23741: heap size 54 MB, throughput 0.983951
Reading from 23740: heap size 176 MB, throughput 0.681489
Reading from 23741: heap size 62 MB, throughput 0.699729
Reading from 23740: heap size 178 MB, throughput 0.718451
Reading from 23740: heap size 185 MB, throughput 0.73195
Reading from 23741: heap size 67 MB, throughput 0.991094
Reading from 23741: heap size 74 MB, throughput 0.989124
Reading from 23740: heap size 187 MB, throughput 0.15186
Reading from 23740: heap size 230 MB, throughput 0.570388
Reading from 23740: heap size 236 MB, throughput 0.549222
Reading from 23741: heap size 74 MB, throughput 0.997594
Reading from 23740: heap size 239 MB, throughput 0.598877
Reading from 23740: heap size 243 MB, throughput 0.549141
Reading from 23740: heap size 251 MB, throughput 0.63004
Reading from 23740: heap size 255 MB, throughput 0.547236
Reading from 23740: heap size 265 MB, throughput 0.519575
Reading from 23741: heap size 80 MB, throughput 0.997503
Reading from 23740: heap size 273 MB, throughput 0.0971854
Reading from 23741: heap size 81 MB, throughput 0.989869
Reading from 23740: heap size 319 MB, throughput 0.435271
Reading from 23740: heap size 327 MB, throughput 0.653588
Reading from 23740: heap size 327 MB, throughput 0.606484
Reading from 23740: heap size 331 MB, throughput 0.620736
Reading from 23740: heap size 335 MB, throughput 0.552622
Reading from 23741: heap size 88 MB, throughput 0.996547
Reading from 23741: heap size 88 MB, throughput 0.99552
Reading from 23740: heap size 340 MB, throughput 0.121462
Reading from 23740: heap size 386 MB, throughput 0.522789
Reading from 23740: heap size 387 MB, throughput 0.696182
Reading from 23740: heap size 390 MB, throughput 0.590566
Reading from 23740: heap size 391 MB, throughput 0.574115
Reading from 23741: heap size 92 MB, throughput 0.997377
Reading from 23740: heap size 396 MB, throughput 0.599462
Reading from 23740: heap size 398 MB, throughput 0.477437
Reading from 23740: heap size 408 MB, throughput 0.511894
Reading from 23740: heap size 413 MB, throughput 0.522308
Reading from 23741: heap size 93 MB, throughput 0.996737
Reading from 23741: heap size 96 MB, throughput 0.995297
Reading from 23740: heap size 420 MB, throughput 0.0920914
Reading from 23740: heap size 477 MB, throughput 0.43655
Equal recommendation: 3705 MB each
Reading from 23740: heap size 480 MB, throughput 0.568919
Reading from 23741: heap size 96 MB, throughput 0.995428
Reading from 23740: heap size 479 MB, throughput 0.516116
Reading from 23740: heap size 480 MB, throughput 0.498643
Reading from 23741: heap size 99 MB, throughput 0.995067
Reading from 23741: heap size 99 MB, throughput 0.994447
Reading from 23740: heap size 484 MB, throughput 0.0871332
Reading from 23740: heap size 543 MB, throughput 0.417578
Reading from 23740: heap size 533 MB, throughput 0.550294
Reading from 23741: heap size 103 MB, throughput 0.992347
Reading from 23740: heap size 540 MB, throughput 0.502666
Reading from 23740: heap size 541 MB, throughput 0.511254
Reading from 23740: heap size 544 MB, throughput 0.570628
Reading from 23741: heap size 103 MB, throughput 0.995078
Reading from 23741: heap size 107 MB, throughput 0.994009
Reading from 23740: heap size 548 MB, throughput 0.113607
Reading from 23740: heap size 615 MB, throughput 0.533533
Reading from 23740: heap size 627 MB, throughput 0.632217
Reading from 23741: heap size 107 MB, throughput 0.996583
Reading from 23740: heap size 628 MB, throughput 0.555625
Reading from 23740: heap size 631 MB, throughput 0.564392
Reading from 23740: heap size 640 MB, throughput 0.701887
Reading from 23741: heap size 110 MB, throughput 0.997268
Reading from 23740: heap size 646 MB, throughput 0.83633
Reading from 23741: heap size 110 MB, throughput 0.99704
Reading from 23740: heap size 658 MB, throughput 0.827497
Reading from 23741: heap size 113 MB, throughput 0.997769
Reading from 23740: heap size 661 MB, throughput 0.824713
Reading from 23741: heap size 113 MB, throughput 0.997552
Reading from 23741: heap size 115 MB, throughput 0.996712
Reading from 23741: heap size 115 MB, throughput 0.997356
Reading from 23740: heap size 678 MB, throughput 0.0441552
Reading from 23740: heap size 754 MB, throughput 0.221207
Reading from 23740: heap size 762 MB, throughput 0.582289
Reading from 23740: heap size 762 MB, throughput 0.275502
Reading from 23740: heap size 671 MB, throughput 0.269072
Reading from 23741: heap size 118 MB, throughput 0.99855
Equal recommendation: 3705 MB each
Reading from 23741: heap size 118 MB, throughput 0.997281
Reading from 23740: heap size 744 MB, throughput 0.0420742
Reading from 23740: heap size 828 MB, throughput 0.236185
Reading from 23740: heap size 828 MB, throughput 0.535004
Reading from 23741: heap size 120 MB, throughput 0.998386
Reading from 23741: heap size 120 MB, throughput 0.9979
Reading from 23740: heap size 828 MB, throughput 0.299993
Reading from 23740: heap size 915 MB, throughput 0.44729
Reading from 23741: heap size 121 MB, throughput 0.998492
Reading from 23740: heap size 917 MB, throughput 0.70819
Reading from 23740: heap size 924 MB, throughput 0.725234
Reading from 23740: heap size 925 MB, throughput 0.855649
Reading from 23740: heap size 926 MB, throughput 0.754953
Reading from 23740: heap size 927 MB, throughput 0.936714
Reading from 23741: heap size 121 MB, throughput 0.998113
Reading from 23740: heap size 931 MB, throughput 0.806602
Reading from 23740: heap size 933 MB, throughput 0.950137
Reading from 23740: heap size 924 MB, throughput 0.916624
Reading from 23741: heap size 122 MB, throughput 0.992545
Reading from 23740: heap size 727 MB, throughput 0.8466
Reading from 23741: heap size 123 MB, throughput 0.98973
Reading from 23740: heap size 901 MB, throughput 0.646834
Reading from 23740: heap size 776 MB, throughput 0.67113
Reading from 23741: heap size 124 MB, throughput 0.991701
Reading from 23740: heap size 884 MB, throughput 0.720553
Reading from 23740: heap size 782 MB, throughput 0.707731
Reading from 23741: heap size 125 MB, throughput 0.990045
Reading from 23741: heap size 129 MB, throughput 0.997246
Reading from 23740: heap size 873 MB, throughput 0.147524
Reading from 23740: heap size 973 MB, throughput 0.596345
Reading from 23740: heap size 970 MB, throughput 0.706112
Reading from 23740: heap size 972 MB, throughput 0.805553
Reading from 23741: heap size 130 MB, throughput 0.993713
Reading from 23740: heap size 971 MB, throughput 0.788272
Reading from 23740: heap size 974 MB, throughput 0.807764
Reading from 23740: heap size 970 MB, throughput 0.885276
Reading from 23741: heap size 134 MB, throughput 0.997264
Reading from 23741: heap size 134 MB, throughput 0.99686
Reading from 23740: heap size 974 MB, throughput 0.977217
Reading from 23741: heap size 137 MB, throughput 0.997166
Reading from 23740: heap size 970 MB, throughput 0.799791
Reading from 23740: heap size 977 MB, throughput 0.774709
Reading from 23740: heap size 987 MB, throughput 0.777428
Reading from 23740: heap size 993 MB, throughput 0.78304
Reading from 23741: heap size 138 MB, throughput 0.996114
Reading from 23740: heap size 986 MB, throughput 0.827176
Equal recommendation: 3705 MB each
Reading from 23740: heap size 991 MB, throughput 0.833252
Reading from 23740: heap size 985 MB, throughput 0.840323
Reading from 23740: heap size 990 MB, throughput 0.845662
Reading from 23741: heap size 140 MB, throughput 0.991686
Reading from 23740: heap size 985 MB, throughput 0.910986
Reading from 23740: heap size 990 MB, throughput 0.893004
Reading from 23740: heap size 986 MB, throughput 0.913982
Reading from 23741: heap size 141 MB, throughput 0.996683
Reading from 23740: heap size 992 MB, throughput 0.780052
Reading from 23740: heap size 989 MB, throughput 0.728846
Reading from 23741: heap size 144 MB, throughput 0.996725
Reading from 23741: heap size 144 MB, throughput 0.996204
Reading from 23740: heap size 992 MB, throughput 0.0862771
Reading from 23740: heap size 1132 MB, throughput 0.534182
Reading from 23740: heap size 1134 MB, throughput 0.832155
Reading from 23740: heap size 1146 MB, throughput 0.814748
Reading from 23740: heap size 1147 MB, throughput 0.802515
Reading from 23741: heap size 147 MB, throughput 0.997624
Reading from 23740: heap size 1152 MB, throughput 0.805233
Reading from 23740: heap size 1155 MB, throughput 0.793126
Reading from 23740: heap size 1157 MB, throughput 0.746962
Reading from 23740: heap size 1161 MB, throughput 0.756583
Reading from 23741: heap size 147 MB, throughput 0.997013
Reading from 23741: heap size 150 MB, throughput 0.997343
Reading from 23741: heap size 150 MB, throughput 0.995355
Reading from 23740: heap size 1163 MB, throughput 0.976162
Reading from 23741: heap size 153 MB, throughput 0.997553
Reading from 23741: heap size 153 MB, throughput 0.998295
Equal recommendation: 3705 MB each
Reading from 23741: heap size 155 MB, throughput 0.998652
Reading from 23741: heap size 156 MB, throughput 0.998432
Reading from 23740: heap size 1168 MB, throughput 0.977855
Reading from 23741: heap size 157 MB, throughput 0.998381
Reading from 23741: heap size 158 MB, throughput 0.992917
Reading from 23741: heap size 159 MB, throughput 0.99385
Reading from 23741: heap size 159 MB, throughput 0.996014
Reading from 23740: heap size 1170 MB, throughput 0.974615
Reading from 23741: heap size 163 MB, throughput 0.99806
Reading from 23741: heap size 163 MB, throughput 0.996568
Reading from 23741: heap size 166 MB, throughput 0.997821
Reading from 23741: heap size 166 MB, throughput 0.997701
Reading from 23740: heap size 1174 MB, throughput 0.977413
Reading from 23741: heap size 169 MB, throughput 0.997341
Equal recommendation: 3705 MB each
Reading from 23741: heap size 169 MB, throughput 0.996841
Reading from 23741: heap size 171 MB, throughput 0.99741
Reading from 23740: heap size 1183 MB, throughput 0.977132
Reading from 23741: heap size 172 MB, throughput 0.997263
Reading from 23741: heap size 175 MB, throughput 0.998067
Reading from 23741: heap size 175 MB, throughput 0.99763
Reading from 23740: heap size 1186 MB, throughput 0.977057
Reading from 23741: heap size 177 MB, throughput 0.997069
Reading from 23741: heap size 177 MB, throughput 0.996918
Reading from 23741: heap size 179 MB, throughput 0.9977
Reading from 23741: heap size 179 MB, throughput 0.997388
Reading from 23740: heap size 1188 MB, throughput 0.978298
Equal recommendation: 3705 MB each
Reading from 23741: heap size 182 MB, throughput 0.99874
Reading from 23741: heap size 182 MB, throughput 0.989107
Reading from 23741: heap size 184 MB, throughput 0.988725
Reading from 23741: heap size 184 MB, throughput 0.996456
Reading from 23740: heap size 1193 MB, throughput 0.978837
Reading from 23741: heap size 190 MB, throughput 0.998279
Reading from 23741: heap size 190 MB, throughput 0.997608
Reading from 23741: heap size 193 MB, throughput 0.998242
Reading from 23740: heap size 1198 MB, throughput 0.976997
Reading from 23741: heap size 194 MB, throughput 0.997756
Reading from 23741: heap size 197 MB, throughput 0.998076
Reading from 23741: heap size 198 MB, throughput 0.997526
Equal recommendation: 3705 MB each
Reading from 23740: heap size 1199 MB, throughput 0.978036
Reading from 23741: heap size 200 MB, throughput 0.996292
Reading from 23741: heap size 200 MB, throughput 0.997604
Reading from 23741: heap size 204 MB, throughput 0.9983
Reading from 23740: heap size 1203 MB, throughput 0.974375
Reading from 23741: heap size 204 MB, throughput 0.997156
Reading from 23741: heap size 194 MB, throughput 0.997683
Reading from 23741: heap size 186 MB, throughput 0.9966
Reading from 23740: heap size 1206 MB, throughput 0.977664
Reading from 23741: heap size 178 MB, throughput 0.893354
Reading from 23741: heap size 184 MB, throughput 0.987666
Reading from 23741: heap size 190 MB, throughput 0.995307
Equal recommendation: 3705 MB each
Reading from 23741: heap size 198 MB, throughput 0.998064
Reading from 23740: heap size 1211 MB, throughput 0.978528
Reading from 23741: heap size 203 MB, throughput 0.998383
Reading from 23741: heap size 204 MB, throughput 0.998016
Reading from 23741: heap size 208 MB, throughput 0.998244
Reading from 23740: heap size 1215 MB, throughput 0.977255
Reading from 23741: heap size 209 MB, throughput 0.998155
Reading from 23741: heap size 213 MB, throughput 0.998049
Reading from 23741: heap size 213 MB, throughput 0.998005
Reading from 23740: heap size 1219 MB, throughput 0.978945
Equal recommendation: 3705 MB each
Reading from 23741: heap size 217 MB, throughput 0.996966
Reading from 23741: heap size 217 MB, throughput 0.99752
Reading from 23740: heap size 1224 MB, throughput 0.975565
Reading from 23741: heap size 222 MB, throughput 0.997768
Reading from 23741: heap size 222 MB, throughput 0.997715
Reading from 23741: heap size 225 MB, throughput 0.997749
Reading from 23741: heap size 226 MB, throughput 0.991198
Reading from 23740: heap size 1229 MB, throughput 0.970992
Reading from 23741: heap size 230 MB, throughput 0.995909
Reading from 23741: heap size 231 MB, throughput 0.997467
Equal recommendation: 3705 MB each
Reading from 23741: heap size 236 MB, throughput 0.996324
Reading from 23740: heap size 1233 MB, throughput 0.975129
Reading from 23741: heap size 237 MB, throughput 0.997921
Reading from 23741: heap size 242 MB, throughput 0.998372
Reading from 23740: heap size 1239 MB, throughput 0.975462
Reading from 23741: heap size 243 MB, throughput 0.997584
Reading from 23741: heap size 248 MB, throughput 0.998157
Reading from 23740: heap size 1241 MB, throughput 0.977958
Reading from 23741: heap size 248 MB, throughput 0.998051
Reading from 23741: heap size 253 MB, throughput 0.997787
Equal recommendation: 3705 MB each
Reading from 23741: heap size 253 MB, throughput 0.997514
Reading from 23740: heap size 1246 MB, throughput 0.975929
Reading from 23741: heap size 258 MB, throughput 0.996713
Reading from 23741: heap size 258 MB, throughput 0.992517
Reading from 23741: heap size 262 MB, throughput 0.996828
Reading from 23741: heap size 263 MB, throughput 0.996664
Reading from 23740: heap size 1247 MB, throughput 0.700921
Reading from 23741: heap size 270 MB, throughput 0.997984
Reading from 23741: heap size 270 MB, throughput 0.996753
Equal recommendation: 3705 MB each
Reading from 23740: heap size 1310 MB, throughput 0.995912
Reading from 23741: heap size 277 MB, throughput 0.998227
Reading from 23741: heap size 277 MB, throughput 0.998175
Reading from 23740: heap size 1311 MB, throughput 0.99443
Reading from 23741: heap size 281 MB, throughput 0.998554
Reading from 23741: heap size 282 MB, throughput 0.99699
Reading from 23741: heap size 288 MB, throughput 0.998144
Reading from 23740: heap size 1320 MB, throughput 0.992869
Equal recommendation: 3705 MB each
Reading from 23741: heap size 288 MB, throughput 0.997396
Reading from 23741: heap size 293 MB, throughput 0.992557
Reading from 23740: heap size 1324 MB, throughput 0.990351
Reading from 23741: heap size 293 MB, throughput 0.997937
Reading from 23741: heap size 300 MB, throughput 0.997
Reading from 23741: heap size 301 MB, throughput 0.997578
Reading from 23740: heap size 1325 MB, throughput 0.988811
Reading from 23741: heap size 308 MB, throughput 0.998302
Equal recommendation: 3705 MB each
Reading from 23741: heap size 308 MB, throughput 0.997377
Reading from 23740: heap size 1204 MB, throughput 0.987913
Reading from 23741: heap size 315 MB, throughput 0.998482
Reading from 23740: heap size 1313 MB, throughput 0.988172
Reading from 23741: heap size 315 MB, throughput 0.998201
Reading from 23741: heap size 319 MB, throughput 0.998616
Reading from 23741: heap size 320 MB, throughput 0.923492
Reading from 23740: heap size 1223 MB, throughput 0.985106
Equal recommendation: 3705 MB each
Reading from 23741: heap size 333 MB, throughput 0.998164
Reading from 23741: heap size 334 MB, throughput 0.999085
Reading from 23740: heap size 1299 MB, throughput 0.985421
Reading from 23741: heap size 342 MB, throughput 0.998722
Reading from 23741: heap size 342 MB, throughput 0.999079
Reading from 23740: heap size 1241 MB, throughput 0.982513
Reading from 23741: heap size 348 MB, throughput 0.999211
Equal recommendation: 3705 MB each
Reading from 23741: heap size 349 MB, throughput 0.999099
Reading from 23740: heap size 1304 MB, throughput 0.984094
Reading from 23741: heap size 353 MB, throughput 0.998845
Reading from 23741: heap size 354 MB, throughput 0.998271
Reading from 23740: heap size 1305 MB, throughput 0.971717
Reading from 23741: heap size 358 MB, throughput 0.995532
Reading from 23741: heap size 358 MB, throughput 0.998508
Reading from 23740: heap size 1306 MB, throughput 0.981193
Equal recommendation: 3705 MB each
Reading from 23741: heap size 366 MB, throughput 0.998375
Reading from 23741: heap size 367 MB, throughput 0.998244
Reading from 23740: heap size 1309 MB, throughput 0.978176
Reading from 23741: heap size 374 MB, throughput 0.99858
Reading from 23740: heap size 1312 MB, throughput 0.978412
Reading from 23741: heap size 374 MB, throughput 0.997598
Equal recommendation: 3705 MB each
Reading from 23741: heap size 381 MB, throughput 0.998476
Reading from 23740: heap size 1318 MB, throughput 0.978037
Reading from 23741: heap size 381 MB, throughput 0.997906
Reading from 23741: heap size 389 MB, throughput 0.996619
Reading from 23740: heap size 1325 MB, throughput 0.978285
Reading from 23741: heap size 389 MB, throughput 0.998524
Reading from 23741: heap size 396 MB, throughput 0.997891
Equal recommendation: 3705 MB each
Reading from 23740: heap size 1331 MB, throughput 0.976042
Reading from 23741: heap size 397 MB, throughput 0.997739
Reading from 23741: heap size 405 MB, throughput 0.997563
Reading from 23740: heap size 1337 MB, throughput 0.975841
Reading from 23741: heap size 405 MB, throughput 0.997946
Reading from 23741: heap size 414 MB, throughput 0.998152
Reading from 23740: heap size 1340 MB, throughput 0.976829
Equal recommendation: 3705 MB each
Reading from 23741: heap size 414 MB, throughput 0.994032
Reading from 23741: heap size 422 MB, throughput 0.998467
Reading from 23740: heap size 1345 MB, throughput 0.976271
Reading from 23741: heap size 423 MB, throughput 0.998316
Reading from 23741: heap size 431 MB, throughput 0.99833
Equal recommendation: 3705 MB each
Reading from 23741: heap size 432 MB, throughput 0.998206
Reading from 23741: heap size 442 MB, throughput 0.998464
Reading from 23741: heap size 442 MB, throughput 0.995129
Reading from 23740: heap size 1347 MB, throughput 0.986274
Reading from 23741: heap size 450 MB, throughput 0.99632
Equal recommendation: 3705 MB each
Reading from 23741: heap size 450 MB, throughput 0.998402
Reading from 23741: heap size 461 MB, throughput 0.99846
Reading from 23741: heap size 463 MB, throughput 0.998312
Reading from 23741: heap size 471 MB, throughput 0.998605
Reading from 23740: heap size 1337 MB, throughput 0.989993
Equal recommendation: 3705 MB each
Reading from 23741: heap size 472 MB, throughput 0.998286
Reading from 23741: heap size 481 MB, throughput 0.995716
Reading from 23741: heap size 481 MB, throughput 0.99833
Reading from 23740: heap size 1352 MB, throughput 0.981798
Reading from 23740: heap size 1345 MB, throughput 0.853611
Reading from 23740: heap size 1375 MB, throughput 0.248694
Reading from 23740: heap size 1449 MB, throughput 0.988073
Reading from 23740: heap size 1489 MB, throughput 0.991638
Equal recommendation: 3705 MB each
Reading from 23740: heap size 1507 MB, throughput 0.983673
Reading from 23741: heap size 493 MB, throughput 0.998186
Reading from 23740: heap size 1512 MB, throughput 0.967868
Reading from 23740: heap size 1517 MB, throughput 0.993411
Reading from 23740: heap size 1520 MB, throughput 0.993046
Reading from 23740: heap size 1507 MB, throughput 0.993811
Reading from 23740: heap size 1222 MB, throughput 0.991155
Reading from 23741: heap size 494 MB, throughput 0.998371
Reading from 23740: heap size 1491 MB, throughput 0.994825
Reading from 23741: heap size 503 MB, throughput 0.998307
Reading from 23741: heap size 504 MB, throughput 0.997637
Equal recommendation: 3705 MB each
Reading from 23740: heap size 1238 MB, throughput 0.990687
Reading from 23741: heap size 514 MB, throughput 0.996569
Reading from 23740: heap size 1464 MB, throughput 0.991091
Reading from 23741: heap size 514 MB, throughput 0.998088
Reading from 23740: heap size 1256 MB, throughput 0.99112
Reading from 23741: heap size 526 MB, throughput 0.998069
Equal recommendation: 3705 MB each
Reading from 23740: heap size 1436 MB, throughput 0.990718
Reading from 23741: heap size 527 MB, throughput 0.998477
Reading from 23740: heap size 1273 MB, throughput 0.987328
Reading from 23741: heap size 537 MB, throughput 0.998418
Reading from 23740: heap size 1408 MB, throughput 0.988748
Client 23741 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 23740: heap size 1290 MB, throughput 0.986922
Reading from 23740: heap size 1399 MB, throughput 0.984525
Reading from 23740: heap size 1307 MB, throughput 0.984461
Recommendation: one client; give it all the memory
Reading from 23740: heap size 1401 MB, throughput 0.984229
Reading from 23740: heap size 1403 MB, throughput 0.982234
Reading from 23740: heap size 1405 MB, throughput 0.982438
Recommendation: one client; give it all the memory
Reading from 23740: heap size 1407 MB, throughput 0.981048
Reading from 23740: heap size 1410 MB, throughput 0.981033
Recommendation: one client; give it all the memory
Reading from 23740: heap size 1414 MB, throughput 0.979192
Reading from 23740: heap size 1421 MB, throughput 0.979489
Reading from 23740: heap size 1424 MB, throughput 0.978925
Recommendation: one client; give it all the memory
Reading from 23740: heap size 1429 MB, throughput 0.981026
Reading from 23740: heap size 1431 MB, throughput 0.978411
Recommendation: one client; give it all the memory
Reading from 23740: heap size 1436 MB, throughput 0.980877
Reading from 23740: heap size 1436 MB, throughput 0.979843
Reading from 23740: heap size 1439 MB, throughput 0.97942
Recommendation: one client; give it all the memory
Reading from 23740: heap size 1440 MB, throughput 0.979405
Reading from 23740: heap size 1442 MB, throughput 0.979773
Recommendation: one client; give it all the memory
Reading from 23740: heap size 1443 MB, throughput 0.979442
Reading from 23740: heap size 1445 MB, throughput 0.979858
Reading from 23740: heap size 1446 MB, throughput 0.980643
Recommendation: one client; give it all the memory
Reading from 23740: heap size 1447 MB, throughput 0.978286
Reading from 23740: heap size 1449 MB, throughput 0.979399
Recommendation: one client; give it all the memory
Reading from 23740: heap size 1450 MB, throughput 0.803335
Reading from 23740: heap size 1489 MB, throughput 0.995614
Recommendation: one client; give it all the memory
Reading from 23740: heap size 1484 MB, throughput 0.994424
Reading from 23740: heap size 1494 MB, throughput 0.992833
Reading from 23740: heap size 1504 MB, throughput 0.991972
Recommendation: one client; give it all the memory
Reading from 23740: heap size 1505 MB, throughput 0.990533
Reading from 23740: heap size 1497 MB, throughput 0.990498
Recommendation: one client; give it all the memory
Reading from 23740: heap size 1380 MB, throughput 0.988742
Reading from 23740: heap size 1484 MB, throughput 0.987366
Recommendation: one client; give it all the memory
Reading from 23740: heap size 1401 MB, throughput 0.987023
Reading from 23740: heap size 1478 MB, throughput 0.98508
Recommendation: one client; give it all the memory
Reading from 23740: heap size 1483 MB, throughput 0.988004
Recommendation: one client; give it all the memory
Reading from 23740: heap size 1458 MB, throughput 0.990143
Recommendation: one client; give it all the memory
Reading from 23740: heap size 1487 MB, throughput 0.983463
Reading from 23740: heap size 1491 MB, throughput 0.846531
Reading from 23740: heap size 1518 MB, throughput 0.727608
Reading from 23740: heap size 1509 MB, throughput 0.665115
Reading from 23740: heap size 1553 MB, throughput 0.660453
Reading from 23740: heap size 1587 MB, throughput 0.692917
Reading from 23740: heap size 1603 MB, throughput 0.696723
Reading from 23740: heap size 1642 MB, throughput 0.740586
Reading from 23740: heap size 1642 MB, throughput 0.268935
Client 23740 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
