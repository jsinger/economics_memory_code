-*- truncate-lines:t -*-
BENCHMARKS                        MAX_HEAP   UNCONSTRAINED                      CGROUP_SIMPLE   CGROUP_FAIR                                 DAEMON_EQUAL                        DAEMON_SIMPLE   DAEMON_VENGEROV                            DAEMON_AVG_VENGEROV   DAEMON_MAJOR_ONLY   FASTEST
h2, huge, 2, 2|h2, large, 2, 13   1800       437.149632 +/- 6.253724711329018   -               316.56342233333334 +/- 11.917315280214194   481.143579 +/- 27.903868316592934   -               452.75022966666666 +/- 6.090198640563562   -                     -                   cgroupFair
