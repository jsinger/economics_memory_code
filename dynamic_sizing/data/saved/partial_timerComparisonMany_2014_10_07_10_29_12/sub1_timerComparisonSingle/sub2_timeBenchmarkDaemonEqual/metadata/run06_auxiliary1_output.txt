economemd
    total memory: 3759 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub1_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 27795: heap size 9 MB, throughput 0.982677
Clients: 1
Client 27795 has a minimum heap size of 30 MB
Reading from 27794: heap size 9 MB, throughput 0.985695
Clients: 2
Client 27794 has a minimum heap size of 1223 MB
Reading from 27795: heap size 9 MB, throughput 0.96722
Reading from 27795: heap size 11 MB, throughput 0.977843
Reading from 27795: heap size 11 MB, throughput 0.741291
Reading from 27795: heap size 15 MB, throughput 0.707651
Reading from 27794: heap size 9 MB, throughput 0.968256
Reading from 27795: heap size 15 MB, throughput 0.643091
Reading from 27795: heap size 24 MB, throughput 0.820513
Reading from 27795: heap size 24 MB, throughput 0.784689
Reading from 27795: heap size 40 MB, throughput 0.919925
Reading from 27795: heap size 40 MB, throughput 0.930537
Reading from 27795: heap size 60 MB, throughput 0.950673
Reading from 27795: heap size 60 MB, throughput 0.943551
Reading from 27795: heap size 91 MB, throughput 0.965917
Reading from 27795: heap size 91 MB, throughput 0.944094
Reading from 27794: heap size 11 MB, throughput 0.9051
Reading from 27795: heap size 140 MB, throughput 0.969966
Reading from 27794: heap size 11 MB, throughput 0.982172
Reading from 27794: heap size 15 MB, throughput 0.84645
Reading from 27794: heap size 18 MB, throughput 0.972026
Reading from 27794: heap size 23 MB, throughput 0.858952
Reading from 27794: heap size 28 MB, throughput 0.600493
Reading from 27794: heap size 39 MB, throughput 0.803907
Reading from 27795: heap size 140 MB, throughput 0.952886
Reading from 27794: heap size 40 MB, throughput 0.838018
Reading from 27794: heap size 42 MB, throughput 0.77906
Reading from 27794: heap size 44 MB, throughput 0.855422
Reading from 27794: heap size 49 MB, throughput 0.170246
Reading from 27794: heap size 65 MB, throughput 0.769726
Reading from 27794: heap size 70 MB, throughput 0.76413
Reading from 27795: heap size 224 MB, throughput 0.908245
Reading from 27794: heap size 72 MB, throughput 0.27897
Reading from 27794: heap size 104 MB, throughput 0.658855
Reading from 27794: heap size 104 MB, throughput 0.814861
Reading from 27795: heap size 243 MB, throughput 0.986189
Reading from 27794: heap size 107 MB, throughput 0.328869
Reading from 27794: heap size 137 MB, throughput 0.813958
Reading from 27794: heap size 141 MB, throughput 0.592839
Reading from 27794: heap size 144 MB, throughput 0.675387
Reading from 27794: heap size 153 MB, throughput 0.761965
Reading from 27795: heap size 282 MB, throughput 0.99403
Reading from 27794: heap size 156 MB, throughput 0.146896
Reading from 27794: heap size 191 MB, throughput 0.578871
Reading from 27794: heap size 199 MB, throughput 0.803389
Reading from 27794: heap size 202 MB, throughput 0.777136
Reading from 27794: heap size 204 MB, throughput 0.622665
Reading from 27794: heap size 207 MB, throughput 0.655788
Reading from 27795: heap size 292 MB, throughput 0.863866
Reading from 27794: heap size 216 MB, throughput 0.134825
Reading from 27794: heap size 261 MB, throughput 0.638657
Reading from 27794: heap size 268 MB, throughput 0.706636
Reading from 27794: heap size 272 MB, throughput 0.676386
Reading from 27794: heap size 280 MB, throughput 0.557723
Reading from 27794: heap size 284 MB, throughput 0.470678
Reading from 27795: heap size 329 MB, throughput 0.985075
Reading from 27794: heap size 294 MB, throughput 0.0803389
Reading from 27794: heap size 349 MB, throughput 0.548576
Reading from 27795: heap size 334 MB, throughput 0.988559
Reading from 27794: heap size 351 MB, throughput 0.130882
Reading from 27794: heap size 396 MB, throughput 0.416132
Reading from 27794: heap size 399 MB, throughput 0.593997
Reading from 27794: heap size 391 MB, throughput 0.644763
Reading from 27794: heap size 396 MB, throughput 0.556879
Reading from 27794: heap size 396 MB, throughput 0.716256
Reading from 27795: heap size 370 MB, throughput 0.907858
Equal recommendation: 1879 MB each
Reading from 27794: heap size 398 MB, throughput 0.584541
Reading from 27794: heap size 401 MB, throughput 0.634096
Reading from 27794: heap size 408 MB, throughput 0.641043
Reading from 27794: heap size 414 MB, throughput 0.546235
Reading from 27794: heap size 421 MB, throughput 0.594718
Reading from 27795: heap size 378 MB, throughput 0.99166
Reading from 27794: heap size 426 MB, throughput 0.0978125
Reading from 27794: heap size 485 MB, throughput 0.429972
Reading from 27794: heap size 496 MB, throughput 0.547174
Reading from 27794: heap size 499 MB, throughput 0.494867
Reading from 27794: heap size 506 MB, throughput 0.387832
Reading from 27795: heap size 416 MB, throughput 0.986781
Reading from 27794: heap size 511 MB, throughput 0.0890194
Reading from 27794: heap size 577 MB, throughput 0.441522
Reading from 27795: heap size 419 MB, throughput 0.977219
Reading from 27794: heap size 577 MB, throughput 0.457737
Reading from 27794: heap size 567 MB, throughput 0.103345
Reading from 27794: heap size 634 MB, throughput 0.493825
Reading from 27794: heap size 625 MB, throughput 0.605818
Reading from 27794: heap size 632 MB, throughput 0.548995
Reading from 27795: heap size 445 MB, throughput 0.986907
Reading from 27794: heap size 634 MB, throughput 0.571454
Reading from 27794: heap size 637 MB, throughput 0.52932
Reading from 27794: heap size 644 MB, throughput 0.668937
Reading from 27795: heap size 447 MB, throughput 0.986081
Reading from 27794: heap size 652 MB, throughput 0.863177
Reading from 27794: heap size 650 MB, throughput 0.829597
Reading from 27794: heap size 667 MB, throughput 0.793108
Reading from 27795: heap size 480 MB, throughput 0.971598
Equal recommendation: 1879 MB each
Reading from 27795: heap size 482 MB, throughput 0.985676
Reading from 27794: heap size 675 MB, throughput 0.0349219
Reading from 27794: heap size 754 MB, throughput 0.466487
Reading from 27794: heap size 765 MB, throughput 0.385357
Reading from 27794: heap size 768 MB, throughput 0.368817
Reading from 27794: heap size 763 MB, throughput 0.384897
Reading from 27795: heap size 520 MB, throughput 0.983184
Reading from 27794: heap size 675 MB, throughput 0.0543665
Reading from 27794: heap size 824 MB, throughput 0.31348
Reading from 27794: heap size 830 MB, throughput 0.870524
Reading from 27794: heap size 831 MB, throughput 0.607455
Reading from 27794: heap size 834 MB, throughput 0.637773
Reading from 27794: heap size 829 MB, throughput 0.59479
Reading from 27795: heap size 522 MB, throughput 0.988936
Reading from 27795: heap size 554 MB, throughput 0.980873
Reading from 27794: heap size 833 MB, throughput 0.117165
Reading from 27794: heap size 913 MB, throughput 0.568275
Reading from 27794: heap size 915 MB, throughput 0.865243
Reading from 27794: heap size 921 MB, throughput 0.89822
Reading from 27794: heap size 925 MB, throughput 0.863174
Reading from 27794: heap size 930 MB, throughput 0.92676
Reading from 27794: heap size 931 MB, throughput 0.915552
Reading from 27795: heap size 556 MB, throughput 0.990393
Reading from 27794: heap size 909 MB, throughput 0.769224
Reading from 27794: heap size 772 MB, throughput 0.735464
Reading from 27794: heap size 897 MB, throughput 0.719902
Reading from 27794: heap size 777 MB, throughput 0.729274
Reading from 27794: heap size 886 MB, throughput 0.779251
Reading from 27794: heap size 894 MB, throughput 0.781242
Reading from 27794: heap size 882 MB, throughput 0.820542
Reading from 27794: heap size 888 MB, throughput 0.827956
Reading from 27794: heap size 876 MB, throughput 0.834328
Reading from 27794: heap size 883 MB, throughput 0.798127
Reading from 27795: heap size 586 MB, throughput 0.984642
Equal recommendation: 1879 MB each
Reading from 27794: heap size 874 MB, throughput 0.573284
Reading from 27795: heap size 589 MB, throughput 0.98889
Reading from 27794: heap size 961 MB, throughput 0.849761
Reading from 27794: heap size 952 MB, throughput 0.809336
Reading from 27794: heap size 958 MB, throughput 0.815232
Reading from 27794: heap size 948 MB, throughput 0.80511
Reading from 27795: heap size 623 MB, throughput 0.991477
Reading from 27794: heap size 954 MB, throughput 0.817025
Reading from 27794: heap size 947 MB, throughput 0.859104
Reading from 27794: heap size 952 MB, throughput 0.855051
Reading from 27794: heap size 949 MB, throughput 0.817282
Reading from 27794: heap size 953 MB, throughput 0.890422
Reading from 27794: heap size 958 MB, throughput 0.903784
Reading from 27794: heap size 959 MB, throughput 0.885496
Reading from 27795: heap size 624 MB, throughput 0.981195
Reading from 27794: heap size 963 MB, throughput 0.756488
Reading from 27794: heap size 963 MB, throughput 0.666249
Reading from 27794: heap size 984 MB, throughput 0.697167
Reading from 27794: heap size 992 MB, throughput 0.666455
Reading from 27794: heap size 1012 MB, throughput 0.714341
Reading from 27794: heap size 1012 MB, throughput 0.650637
Reading from 27794: heap size 1025 MB, throughput 0.688684
Reading from 27794: heap size 1025 MB, throughput 0.655546
Reading from 27794: heap size 1042 MB, throughput 0.702186
Reading from 27795: heap size 650 MB, throughput 0.984195
Reading from 27794: heap size 1043 MB, throughput 0.69333
Reading from 27794: heap size 1062 MB, throughput 0.958487
Reading from 27795: heap size 653 MB, throughput 0.994779
Equal recommendation: 1879 MB each
Reading from 27795: heap size 679 MB, throughput 0.988498
Reading from 27794: heap size 1064 MB, throughput 0.971057
Reading from 27795: heap size 685 MB, throughput 0.988619
Reading from 27794: heap size 1075 MB, throughput 0.967049
Reading from 27795: heap size 712 MB, throughput 0.988919
Reading from 27795: heap size 712 MB, throughput 0.983516
Reading from 27794: heap size 1079 MB, throughput 0.967374
Equal recommendation: 1879 MB each
Reading from 27795: heap size 740 MB, throughput 0.995646
Reading from 27794: heap size 1077 MB, throughput 0.974289
Reading from 27795: heap size 741 MB, throughput 0.98984
Reading from 27794: heap size 1083 MB, throughput 0.972814
Reading from 27795: heap size 767 MB, throughput 0.99042
Reading from 27795: heap size 767 MB, throughput 0.989582
Reading from 27794: heap size 1077 MB, throughput 0.966246
Reading from 27795: heap size 796 MB, throughput 0.989489
Equal recommendation: 1879 MB each
Reading from 27794: heap size 1083 MB, throughput 0.969714
Reading from 27795: heap size 796 MB, throughput 0.99406
Reading from 27795: heap size 819 MB, throughput 0.981063
Reading from 27794: heap size 1086 MB, throughput 0.968498
Reading from 27795: heap size 822 MB, throughput 0.992411
Reading from 27794: heap size 1088 MB, throughput 0.969168
Equal recommendation: 1879 MB each
Reading from 27795: heap size 848 MB, throughput 0.992271
Reading from 27794: heap size 1094 MB, throughput 0.97322
Reading from 27795: heap size 851 MB, throughput 0.991889
Reading from 27794: heap size 1096 MB, throughput 0.970376
Reading from 27795: heap size 877 MB, throughput 0.992552
Reading from 27795: heap size 879 MB, throughput 0.991575
Equal recommendation: 1879 MB each
Reading from 27794: heap size 1101 MB, throughput 0.614928
Reading from 27795: heap size 905 MB, throughput 0.991469
Reading from 27794: heap size 1192 MB, throughput 0.956944
Reading from 27795: heap size 906 MB, throughput 0.990033
Reading from 27795: heap size 933 MB, throughput 0.991382
Reading from 27794: heap size 1201 MB, throughput 0.980219
Equal recommendation: 1879 MB each
Reading from 27795: heap size 934 MB, throughput 0.991758
Reading from 27794: heap size 1202 MB, throughput 0.98324
Reading from 27795: heap size 965 MB, throughput 0.990455
Reading from 27794: heap size 1200 MB, throughput 0.978506
Reading from 27795: heap size 965 MB, throughput 0.992265
Reading from 27794: heap size 1203 MB, throughput 0.978688
Equal recommendation: 1879 MB each
Reading from 27795: heap size 994 MB, throughput 0.993309
Reading from 27794: heap size 1195 MB, throughput 0.980361
Reading from 27795: heap size 996 MB, throughput 0.995987
Reading from 27795: heap size 1019 MB, throughput 0.990472
Reading from 27794: heap size 1200 MB, throughput 0.973815
Equal recommendation: 1879 MB each
Reading from 27795: heap size 1022 MB, throughput 0.990777
Reading from 27794: heap size 1202 MB, throughput 0.976882
Reading from 27795: heap size 1051 MB, throughput 0.993737
Reading from 27794: heap size 1202 MB, throughput 0.975423
Reading from 27795: heap size 1051 MB, throughput 0.992338
Reading from 27794: heap size 1205 MB, throughput 0.977521
Equal recommendation: 1879 MB each
Reading from 27795: heap size 1080 MB, throughput 0.989881
Reading from 27794: heap size 1208 MB, throughput 0.972498
Reading from 27795: heap size 1081 MB, throughput 0.991567
Reading from 27794: heap size 1213 MB, throughput 0.97422
Reading from 27795: heap size 1113 MB, throughput 0.992845
Equal recommendation: 1879 MB each
Reading from 27794: heap size 1218 MB, throughput 0.972506
Reading from 27795: heap size 1114 MB, throughput 0.991281
Reading from 27794: heap size 1223 MB, throughput 0.976028
Reading from 27795: heap size 1146 MB, throughput 0.989356
Reading from 27794: heap size 1229 MB, throughput 0.969385
Reading from 27795: heap size 1146 MB, throughput 0.991614
Equal recommendation: 1879 MB each
Reading from 27794: heap size 1234 MB, throughput 0.965135
Reading from 27795: heap size 1184 MB, throughput 0.99016
Reading from 27794: heap size 1241 MB, throughput 0.966271
Reading from 27795: heap size 1186 MB, throughput 0.990367
Equal recommendation: 1879 MB each
Reading from 27794: heap size 1248 MB, throughput 0.972078
Reading from 27795: heap size 1226 MB, throughput 0.99304
Reading from 27794: heap size 1252 MB, throughput 0.971361
Reading from 27795: heap size 1228 MB, throughput 0.988432
Reading from 27794: heap size 1259 MB, throughput 0.970873
Reading from 27795: heap size 1239 MB, throughput 0.990375
Equal recommendation: 1879 MB each
Reading from 27795: heap size 1237 MB, throughput 0.982384
Reading from 27794: heap size 1261 MB, throughput 0.644073
Reading from 27795: heap size 1227 MB, throughput 0.99038
Reading from 27794: heap size 1371 MB, throughput 0.956399
Equal recommendation: 1879 MB each
Reading from 27795: heap size 1228 MB, throughput 0.987833
Reading from 27794: heap size 1372 MB, throughput 0.978048
Reading from 27795: heap size 1218 MB, throughput 0.989531
Reading from 27794: heap size 1379 MB, throughput 0.980531
Reading from 27795: heap size 1207 MB, throughput 0.992342
Equal recommendation: 1879 MB each
Reading from 27794: heap size 1380 MB, throughput 0.97878
Reading from 27795: heap size 1221 MB, throughput 0.986092
Reading from 27794: heap size 1378 MB, throughput 0.977365
Reading from 27795: heap size 1212 MB, throughput 0.990823
Equal recommendation: 1879 MB each
Reading from 27794: heap size 1381 MB, throughput 0.979581
Reading from 27795: heap size 1214 MB, throughput 0.990853
Reading from 27794: heap size 1372 MB, throughput 0.976502
Reading from 27795: heap size 1183 MB, throughput 0.963454
Reading from 27795: heap size 1234 MB, throughput 0.996175
Equal recommendation: 1879 MB each
Reading from 27795: heap size 1231 MB, throughput 0.989265
Reading from 27795: heap size 1235 MB, throughput 0.990593
Reading from 27795: heap size 1235 MB, throughput 0.992428
Equal recommendation: 1879 MB each
Reading from 27795: heap size 1236 MB, throughput 0.987891
Reading from 27794: heap size 1378 MB, throughput 0.991679
Reading from 27795: heap size 1236 MB, throughput 0.992472
Reading from 27795: heap size 1239 MB, throughput 0.989604
Equal recommendation: 1879 MB each
Reading from 27795: heap size 1238 MB, throughput 0.991298
Reading from 27795: heap size 1243 MB, throughput 0.992649
Reading from 27794: heap size 1361 MB, throughput 0.910242
Reading from 27795: heap size 1242 MB, throughput 0.988892
Equal recommendation: 1879 MB each
Reading from 27794: heap size 1472 MB, throughput 0.996582
Reading from 27794: heap size 1517 MB, throughput 0.925377
Reading from 27794: heap size 1390 MB, throughput 0.890221
Reading from 27794: heap size 1515 MB, throughput 0.881945
Reading from 27794: heap size 1519 MB, throughput 0.898022
Reading from 27794: heap size 1515 MB, throughput 0.914569
Reading from 27795: heap size 1247 MB, throughput 0.992875
Reading from 27794: heap size 1522 MB, throughput 0.984324
Reading from 27795: heap size 1247 MB, throughput 0.99003
Reading from 27794: heap size 1530 MB, throughput 0.994156
Equal recommendation: 1879 MB each
Reading from 27795: heap size 1250 MB, throughput 0.993106
Reading from 27794: heap size 1536 MB, throughput 0.99244
Reading from 27795: heap size 1249 MB, throughput 0.991096
Reading from 27794: heap size 1546 MB, throughput 0.990124
Reading from 27795: heap size 1253 MB, throughput 0.992604
Equal recommendation: 1879 MB each
Reading from 27794: heap size 1551 MB, throughput 0.98823
Reading from 27795: heap size 1253 MB, throughput 1
Reading from 27795: heap size 1253 MB, throughput 1
Reading from 27795: heap size 1253 MB, throughput 0.015745
Client 27795 died
Clients: 1
Reading from 27794: heap size 1541 MB, throughput 0.991356
Recommendation: one client; give it all the memory
Reading from 27794: heap size 1550 MB, throughput 0.990508
Reading from 27794: heap size 1527 MB, throughput 0.989807
Recommendation: one client; give it all the memory
Reading from 27794: heap size 1539 MB, throughput 0.988882
Reading from 27794: heap size 1535 MB, throughput 0.987882
Recommendation: one client; give it all the memory
Reading from 27794: heap size 1538 MB, throughput 0.986067
Reading from 27794: heap size 1541 MB, throughput 0.985855
Recommendation: one client; give it all the memory
Reading from 27794: heap size 1546 MB, throughput 0.983443
Recommendation: one client; give it all the memory
Reading from 27794: heap size 1550 MB, throughput 0.984538
Reading from 27794: heap size 1559 MB, throughput 0.981501
Recommendation: one client; give it all the memory
Reading from 27794: heap size 1565 MB, throughput 0.982436
Reading from 27794: heap size 1577 MB, throughput 0.979012
Recommendation: one client; give it all the memory
Reading from 27794: heap size 1586 MB, throughput 0.979521
Reading from 27794: heap size 1597 MB, throughput 0.9794
Recommendation: one client; give it all the memory
Reading from 27794: heap size 1606 MB, throughput 0.980894
Reading from 27794: heap size 1612 MB, throughput 0.979719
Recommendation: one client; give it all the memory
Reading from 27794: heap size 1622 MB, throughput 0.979319
Reading from 27794: heap size 1625 MB, throughput 0.979738
Recommendation: one client; give it all the memory
Reading from 27794: heap size 1634 MB, throughput 0.979245
Reading from 27794: heap size 1635 MB, throughput 0.979364
Recommendation: one client; give it all the memory
Reading from 27794: heap size 1641 MB, throughput 0.980163
Recommendation: one client; give it all the memory
Reading from 27794: heap size 1642 MB, throughput 0.980039
Reading from 27794: heap size 1645 MB, throughput 0.828663
Recommendation: one client; give it all the memory
Reading from 27794: heap size 1699 MB, throughput 0.996104
Reading from 27794: heap size 1692 MB, throughput 0.994715
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 27794: heap size 1706 MB, throughput 0.995687
Recommendation: one client; give it all the memory
Reading from 27794: heap size 1711 MB, throughput 0.994209
Recommendation: one client; give it all the memory
Reading from 27794: heap size 1715 MB, throughput 0.988794
Reading from 27794: heap size 1710 MB, throughput 0.853687
Reading from 27794: heap size 1715 MB, throughput 0.776123
Reading from 27794: heap size 1742 MB, throughput 0.818701
Reading from 27794: heap size 1760 MB, throughput 0.798488
Client 27794 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
