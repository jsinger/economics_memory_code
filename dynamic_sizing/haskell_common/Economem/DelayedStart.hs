module Economem.DelayedStart (DelayedStart(..))
       where

import Control.Applicative
import Economem.Common
import Economem.Files
import Economem.Process


delayedStart :: IO FilePath
delayedStart = (</> "delayed_start.sh") <$> measurementScriptsDir

data DelayedStart = DelayedStart Integer

instance ArgsModifier DelayedStart where
  modifyArgsInternal (DelayedStart t) (cmd, args) = do
    delayCmd <- delayedStart
    return (delayCmd, [show t, cmd] ++ args)
