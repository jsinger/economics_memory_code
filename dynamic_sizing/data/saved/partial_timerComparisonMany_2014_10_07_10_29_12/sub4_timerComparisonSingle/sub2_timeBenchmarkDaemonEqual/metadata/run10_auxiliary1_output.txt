economemd
    total memory: 7518 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub4_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run10_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 2382: heap size 9 MB, throughput 0.990636
Clients: 1
Client 2382 has a minimum heap size of 30 MB
Reading from 2380: heap size 9 MB, throughput 0.985989
Clients: 2
Client 2380 has a minimum heap size of 1223 MB
Reading from 2382: heap size 9 MB, throughput 0.97725
Reading from 2382: heap size 9 MB, throughput 0.966008
Reading from 2380: heap size 9 MB, throughput 0.978417
Reading from 2382: heap size 9 MB, throughput 0.962064
Reading from 2382: heap size 11 MB, throughput 0.691148
Reading from 2382: heap size 11 MB, throughput 0.573679
Reading from 2382: heap size 16 MB, throughput 0.68703
Reading from 2382: heap size 16 MB, throughput 0.702885
Reading from 2382: heap size 24 MB, throughput 0.846409
Reading from 2382: heap size 24 MB, throughput 0.873047
Reading from 2382: heap size 34 MB, throughput 0.906051
Reading from 2382: heap size 34 MB, throughput 0.909777
Reading from 2382: heap size 50 MB, throughput 0.935398
Reading from 2382: heap size 50 MB, throughput 0.935712
Reading from 2382: heap size 76 MB, throughput 0.947894
Reading from 2380: heap size 11 MB, throughput 0.949487
Reading from 2382: heap size 76 MB, throughput 0.946162
Reading from 2382: heap size 116 MB, throughput 0.952962
Reading from 2382: heap size 116 MB, throughput 0.918799
Reading from 2380: heap size 11 MB, throughput 0.973023
Reading from 2380: heap size 15 MB, throughput 0.854476
Reading from 2380: heap size 18 MB, throughput 0.969283
Reading from 2380: heap size 24 MB, throughput 0.859879
Reading from 2380: heap size 28 MB, throughput 0.531052
Reading from 2380: heap size 41 MB, throughput 0.923072
Reading from 2380: heap size 42 MB, throughput 0.868011
Reading from 2380: heap size 44 MB, throughput 0.780848
Reading from 2382: heap size 159 MB, throughput 0.947649
Reading from 2380: heap size 46 MB, throughput 0.178298
Reading from 2380: heap size 63 MB, throughput 0.87175
Reading from 2380: heap size 66 MB, throughput 0.729123
Reading from 2382: heap size 166 MB, throughput 0.912315
Reading from 2380: heap size 69 MB, throughput 0.180917
Reading from 2380: heap size 89 MB, throughput 0.795874
Reading from 2380: heap size 95 MB, throughput 0.695261
Reading from 2380: heap size 97 MB, throughput 0.712855
Reading from 2380: heap size 100 MB, throughput 0.630597
Reading from 2382: heap size 217 MB, throughput 0.98446
Reading from 2380: heap size 105 MB, throughput 0.688967
Reading from 2380: heap size 109 MB, throughput 0.236715
Reading from 2380: heap size 135 MB, throughput 0.664171
Reading from 2380: heap size 142 MB, throughput 0.681125
Reading from 2380: heap size 144 MB, throughput 0.685549
Reading from 2382: heap size 237 MB, throughput 0.943847
Reading from 2380: heap size 149 MB, throughput 0.561771
Reading from 2380: heap size 154 MB, throughput 0.710303
Reading from 2380: heap size 159 MB, throughput 0.622094
Reading from 2380: heap size 166 MB, throughput 0.115169
Reading from 2380: heap size 196 MB, throughput 0.526269
Reading from 2380: heap size 201 MB, throughput 0.497455
Reading from 2380: heap size 205 MB, throughput 0.145303
Reading from 2382: heap size 261 MB, throughput 0.9963
Reading from 2380: heap size 242 MB, throughput 0.56667
Reading from 2380: heap size 236 MB, throughput 0.69963
Reading from 2380: heap size 241 MB, throughput 0.619182
Reading from 2380: heap size 242 MB, throughput 0.592611
Reading from 2380: heap size 244 MB, throughput 0.636613
Reading from 2382: heap size 260 MB, throughput 0.9791
Reading from 2380: heap size 248 MB, throughput 0.119669
Reading from 2380: heap size 289 MB, throughput 0.490785
Reading from 2380: heap size 299 MB, throughput 0.669128
Reading from 2380: heap size 300 MB, throughput 0.610301
Reading from 2382: heap size 290 MB, throughput 0.910683
Reading from 2380: heap size 303 MB, throughput 0.515509
Reading from 2380: heap size 309 MB, throughput 0.480314
Reading from 2380: heap size 314 MB, throughput 0.575929
Reading from 2382: heap size 307 MB, throughput 0.986478
Reading from 2380: heap size 324 MB, throughput 0.116947
Reading from 2380: heap size 375 MB, throughput 0.516005
Reading from 2380: heap size 379 MB, throughput 0.614145
Reading from 2382: heap size 330 MB, throughput 0.969726
Reading from 2380: heap size 379 MB, throughput 0.10061
Equal recommendation: 3759 MB each
Reading from 2380: heap size 428 MB, throughput 0.485655
Reading from 2380: heap size 421 MB, throughput 0.632482
Reading from 2380: heap size 427 MB, throughput 0.689878
Reading from 2380: heap size 429 MB, throughput 0.664203
Reading from 2380: heap size 432 MB, throughput 0.632352
Reading from 2382: heap size 330 MB, throughput 0.966817
Reading from 2380: heap size 434 MB, throughput 0.595773
Reading from 2380: heap size 443 MB, throughput 0.563347
Reading from 2380: heap size 446 MB, throughput 0.556163
Reading from 2380: heap size 459 MB, throughput 0.492969
Reading from 2380: heap size 467 MB, throughput 0.088053
Reading from 2382: heap size 360 MB, throughput 0.991783
Reading from 2380: heap size 524 MB, throughput 0.343078
Reading from 2380: heap size 532 MB, throughput 0.552562
Reading from 2380: heap size 533 MB, throughput 0.487189
Reading from 2380: heap size 537 MB, throughput 0.547113
Reading from 2380: heap size 538 MB, throughput 0.499577
Reading from 2382: heap size 362 MB, throughput 0.985388
Reading from 2380: heap size 543 MB, throughput 0.0915254
Reading from 2380: heap size 602 MB, throughput 0.472729
Reading from 2382: heap size 391 MB, throughput 0.908403
Reading from 2380: heap size 594 MB, throughput 0.570012
Reading from 2380: heap size 601 MB, throughput 0.613537
Reading from 2380: heap size 601 MB, throughput 0.114288
Reading from 2380: heap size 667 MB, throughput 0.344654
Reading from 2382: heap size 401 MB, throughput 0.984301
Reading from 2380: heap size 660 MB, throughput 0.622866
Reading from 2380: heap size 666 MB, throughput 0.673727
Reading from 2382: heap size 431 MB, throughput 0.988559
Reading from 2380: heap size 671 MB, throughput 0.834872
Reading from 2380: heap size 672 MB, throughput 0.862308
Reading from 2382: heap size 432 MB, throughput 0.975742
Equal recommendation: 3759 MB each
Reading from 2382: heap size 462 MB, throughput 0.993678
Reading from 2380: heap size 670 MB, throughput 0.184151
Reading from 2380: heap size 756 MB, throughput 0.320902
Reading from 2380: heap size 767 MB, throughput 0.629765
Reading from 2380: heap size 768 MB, throughput 0.367587
Reading from 2380: heap size 766 MB, throughput 0.471258
Reading from 2380: heap size 769 MB, throughput 0.477056
Reading from 2380: heap size 761 MB, throughput 0.840664
Reading from 2380: heap size 766 MB, throughput 0.45241
Reading from 2382: heap size 463 MB, throughput 0.987808
Reading from 2382: heap size 492 MB, throughput 0.985005
Reading from 2380: heap size 765 MB, throughput 0.0428423
Reading from 2380: heap size 838 MB, throughput 0.4423
Reading from 2380: heap size 845 MB, throughput 0.849634
Reading from 2380: heap size 847 MB, throughput 0.806248
Reading from 2380: heap size 854 MB, throughput 0.635182
Reading from 2382: heap size 493 MB, throughput 0.988147
Reading from 2380: heap size 856 MB, throughput 0.250942
Reading from 2380: heap size 945 MB, throughput 0.726791
Reading from 2382: heap size 525 MB, throughput 0.993053
Reading from 2380: heap size 948 MB, throughput 0.907308
Reading from 2380: heap size 955 MB, throughput 0.835863
Reading from 2380: heap size 958 MB, throughput 0.817468
Reading from 2380: heap size 954 MB, throughput 0.842516
Reading from 2380: heap size 958 MB, throughput 0.805126
Reading from 2380: heap size 950 MB, throughput 0.854918
Reading from 2380: heap size 955 MB, throughput 0.838767
Reading from 2382: heap size 526 MB, throughput 0.984313
Reading from 2380: heap size 947 MB, throughput 0.866495
Reading from 2380: heap size 952 MB, throughput 0.857405
Reading from 2380: heap size 943 MB, throughput 0.981659
Reading from 2382: heap size 552 MB, throughput 0.995102
Equal recommendation: 3759 MB each
Reading from 2380: heap size 948 MB, throughput 0.967251
Reading from 2380: heap size 942 MB, throughput 0.803571
Reading from 2380: heap size 948 MB, throughput 0.802021
Reading from 2382: heap size 555 MB, throughput 0.984855
Reading from 2380: heap size 941 MB, throughput 0.823982
Reading from 2380: heap size 945 MB, throughput 0.825767
Reading from 2380: heap size 938 MB, throughput 0.829069
Reading from 2380: heap size 943 MB, throughput 0.840072
Reading from 2380: heap size 938 MB, throughput 0.860305
Reading from 2380: heap size 942 MB, throughput 0.883212
Reading from 2382: heap size 579 MB, throughput 0.990354
Reading from 2380: heap size 942 MB, throughput 0.893484
Reading from 2380: heap size 945 MB, throughput 0.857699
Reading from 2380: heap size 947 MB, throughput 0.793227
Reading from 2380: heap size 948 MB, throughput 0.648313
Reading from 2380: heap size 968 MB, throughput 0.689145
Reading from 2382: heap size 581 MB, throughput 0.987258
Reading from 2380: heap size 978 MB, throughput 0.06512
Reading from 2380: heap size 1086 MB, throughput 0.580188
Reading from 2382: heap size 607 MB, throughput 0.977319
Reading from 2380: heap size 1089 MB, throughput 0.734769
Reading from 2380: heap size 1090 MB, throughput 0.738543
Reading from 2380: heap size 1094 MB, throughput 0.732011
Reading from 2380: heap size 1094 MB, throughput 0.749604
Reading from 2380: heap size 1100 MB, throughput 0.89496
Reading from 2382: heap size 608 MB, throughput 0.984273
Equal recommendation: 3759 MB each
Reading from 2382: heap size 646 MB, throughput 0.995058
Reading from 2380: heap size 1107 MB, throughput 0.977114
Reading from 2382: heap size 646 MB, throughput 0.987078
Reading from 2380: heap size 1113 MB, throughput 0.976735
Reading from 2382: heap size 677 MB, throughput 0.987973
Reading from 2382: heap size 680 MB, throughput 0.992589
Reading from 2380: heap size 1126 MB, throughput 0.968639
Reading from 2382: heap size 710 MB, throughput 0.991189
Equal recommendation: 3759 MB each
Reading from 2380: heap size 1128 MB, throughput 0.979742
Reading from 2382: heap size 713 MB, throughput 0.988815
Reading from 2382: heap size 743 MB, throughput 0.992507
Reading from 2380: heap size 1126 MB, throughput 0.971496
Reading from 2382: heap size 745 MB, throughput 0.988598
Reading from 2380: heap size 1131 MB, throughput 0.972559
Reading from 2382: heap size 775 MB, throughput 0.99051
Equal recommendation: 3759 MB each
Reading from 2380: heap size 1120 MB, throughput 0.973925
Reading from 2382: heap size 776 MB, throughput 0.992025
Reading from 2382: heap size 806 MB, throughput 0.996013
Reading from 2380: heap size 1127 MB, throughput 0.974047
Reading from 2382: heap size 808 MB, throughput 0.989665
Reading from 2380: heap size 1128 MB, throughput 0.969165
Reading from 2382: heap size 835 MB, throughput 0.992026
Equal recommendation: 3759 MB each
Reading from 2380: heap size 1129 MB, throughput 0.972088
Reading from 2382: heap size 836 MB, throughput 0.990318
Reading from 2382: heap size 867 MB, throughput 0.992113
Reading from 2380: heap size 1133 MB, throughput 0.970839
Reading from 2382: heap size 868 MB, throughput 0.989965
Reading from 2380: heap size 1136 MB, throughput 0.971847
Equal recommendation: 3759 MB each
Reading from 2382: heap size 902 MB, throughput 0.991907
Reading from 2380: heap size 1140 MB, throughput 0.97239
Reading from 2382: heap size 902 MB, throughput 0.991264
Reading from 2380: heap size 1143 MB, throughput 0.971014
Reading from 2382: heap size 937 MB, throughput 0.992704
Reading from 2380: heap size 1147 MB, throughput 0.969985
Reading from 2382: heap size 937 MB, throughput 0.990245
Equal recommendation: 3759 MB each
Reading from 2380: heap size 1152 MB, throughput 0.966175
Reading from 2382: heap size 971 MB, throughput 0.989512
Reading from 2382: heap size 972 MB, throughput 0.991954
Reading from 2380: heap size 1158 MB, throughput 0.966045
Reading from 2382: heap size 1009 MB, throughput 0.989768
Reading from 2380: heap size 1162 MB, throughput 0.96518
Equal recommendation: 3759 MB each
Reading from 2382: heap size 1011 MB, throughput 0.992196
Reading from 2380: heap size 1167 MB, throughput 0.966766
Reading from 2382: heap size 1050 MB, throughput 0.993043
Reading from 2380: heap size 1169 MB, throughput 0.604619
Reading from 2382: heap size 1051 MB, throughput 0.988348
Equal recommendation: 3759 MB each
Reading from 2380: heap size 1270 MB, throughput 0.94935
Reading from 2382: heap size 1091 MB, throughput 0.990217
Reading from 2382: heap size 1091 MB, throughput 0.992068
Reading from 2380: heap size 1271 MB, throughput 0.985313
Reading from 2382: heap size 1134 MB, throughput 0.993303
Reading from 2380: heap size 1280 MB, throughput 0.981997
Equal recommendation: 3759 MB each
Reading from 2380: heap size 1282 MB, throughput 0.979127
Reading from 2382: heap size 1135 MB, throughput 0.989917
Reading from 2380: heap size 1279 MB, throughput 0.982178
Reading from 2382: heap size 1176 MB, throughput 0.992757
Equal recommendation: 3759 MB each
Reading from 2380: heap size 1282 MB, throughput 0.981817
Reading from 2382: heap size 1178 MB, throughput 0.99347
Reading from 2380: heap size 1270 MB, throughput 0.981681
Reading from 2382: heap size 1217 MB, throughput 0.990174
Reading from 2380: heap size 1203 MB, throughput 0.978184
Reading from 2382: heap size 1220 MB, throughput 0.993667
Equal recommendation: 3759 MB each
Reading from 2380: heap size 1264 MB, throughput 0.974186
Reading from 2382: heap size 1264 MB, throughput 0.993364
Reading from 2380: heap size 1269 MB, throughput 0.975158
Reading from 2382: heap size 1267 MB, throughput 0.991508
Equal recommendation: 3759 MB each
Reading from 2380: heap size 1272 MB, throughput 0.973592
Reading from 2382: heap size 1310 MB, throughput 0.99036
Reading from 2380: heap size 1272 MB, throughput 0.972012
Reading from 2382: heap size 1312 MB, throughput 0.991193
Reading from 2380: heap size 1275 MB, throughput 0.972468
Equal recommendation: 3759 MB each
Reading from 2382: heap size 1362 MB, throughput 0.992006
Reading from 2380: heap size 1279 MB, throughput 0.971079
Reading from 2382: heap size 1364 MB, throughput 0.991888
Reading from 2380: heap size 1283 MB, throughput 0.968471
Reading from 2382: heap size 1417 MB, throughput 0.989802
Equal recommendation: 3759 MB each
Reading from 2380: heap size 1289 MB, throughput 0.962174
Reading from 2382: heap size 1418 MB, throughput 0.987817
Reading from 2380: heap size 1296 MB, throughput 0.962214
Reading from 2380: heap size 1301 MB, throughput 0.971051
Reading from 2382: heap size 1475 MB, throughput 0.991961
Equal recommendation: 3759 MB each
Reading from 2380: heap size 1308 MB, throughput 0.97179
Reading from 2382: heap size 1480 MB, throughput 0.989418
Reading from 2380: heap size 1312 MB, throughput 0.967791
Reading from 2382: heap size 1538 MB, throughput 0.989751
Equal recommendation: 3759 MB each
Reading from 2380: heap size 1319 MB, throughput 0.963875
Reading from 2382: heap size 1542 MB, throughput 0.989396
Reading from 2380: heap size 1321 MB, throughput 0.969896
Reading from 2382: heap size 1607 MB, throughput 0.990237
Equal recommendation: 3759 MB each
Reading from 2382: heap size 1611 MB, throughput 0.992269
Reading from 2382: heap size 1668 MB, throughput 0.991337
Equal recommendation: 3759 MB each
Reading from 2382: heap size 1673 MB, throughput 0.992165
Reading from 2382: heap size 1735 MB, throughput 0.991268
Equal recommendation: 3759 MB each
Reading from 2382: heap size 1739 MB, throughput 0.991067
Reading from 2380: heap size 1328 MB, throughput 0.96534
Reading from 2382: heap size 1808 MB, throughput 0.99059
Equal recommendation: 3759 MB each
Reading from 2380: heap size 1389 MB, throughput 0.990708
Reading from 2382: heap size 1808 MB, throughput 0.993325
Reading from 2380: heap size 1406 MB, throughput 0.946925
Reading from 2380: heap size 1407 MB, throughput 0.822691
Reading from 2380: heap size 1402 MB, throughput 0.780961
Reading from 2380: heap size 1413 MB, throughput 0.77438
Reading from 2380: heap size 1436 MB, throughput 0.795717
Reading from 2380: heap size 1443 MB, throughput 0.775319
Reading from 2380: heap size 1472 MB, throughput 0.909263
Equal recommendation: 3759 MB each
Reading from 2382: heap size 1874 MB, throughput 0.967988
Reading from 2380: heap size 1475 MB, throughput 0.988647
Reading from 2382: heap size 1903 MB, throughput 0.992512
Reading from 2380: heap size 1493 MB, throughput 0.985225
Equal recommendation: 3759 MB each
Reading from 2380: heap size 1499 MB, throughput 0.981834
Reading from 2382: heap size 1976 MB, throughput 0.992315
Client 2382 died
Clients: 1
Reading from 2380: heap size 1499 MB, throughput 0.988548
Recommendation: one client; give it all the memory
Reading from 2380: heap size 1509 MB, throughput 0.987731
Reading from 2380: heap size 1496 MB, throughput 0.987493
Recommendation: one client; give it all the memory
Reading from 2380: heap size 1508 MB, throughput 0.986835
Recommendation: one client; give it all the memory
Reading from 2380: heap size 1507 MB, throughput 0.985588
Reading from 2380: heap size 1511 MB, throughput 0.987083
Recommendation: one client; give it all the memory
Reading from 2380: heap size 1507 MB, throughput 0.987433
Reading from 2380: heap size 1513 MB, throughput 0.986199
Recommendation: one client; give it all the memory
Reading from 2380: heap size 1511 MB, throughput 0.984875
Recommendation: one client; give it all the memory
Reading from 2380: heap size 1513 MB, throughput 0.98402
Reading from 2380: heap size 1517 MB, throughput 0.983899
Recommendation: one client; give it all the memory
Reading from 2380: heap size 1523 MB, throughput 0.980745
Reading from 2380: heap size 1528 MB, throughput 0.980626
Recommendation: one client; give it all the memory
Reading from 2380: heap size 1539 MB, throughput 0.979384
Recommendation: one client; give it all the memory
Reading from 2380: heap size 1547 MB, throughput 0.98046
Reading from 2380: heap size 1555 MB, throughput 0.979562
Recommendation: one client; give it all the memory
Reading from 2380: heap size 1564 MB, throughput 0.847348
Reading from 2380: heap size 1688 MB, throughput 0.995514
Recommendation: one client; give it all the memory
Reading from 2380: heap size 1686 MB, throughput 0.99416
Recommendation: one client; give it all the memory
Reading from 2380: heap size 1700 MB, throughput 0.992571
Reading from 2380: heap size 1713 MB, throughput 0.991696
Recommendation: one client; give it all the memory
Reading from 2380: heap size 1714 MB, throughput 0.990633
Reading from 2380: heap size 1701 MB, throughput 0.989694
Recommendation: one client; give it all the memory
Reading from 2380: heap size 1546 MB, throughput 0.988329
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 2380: heap size 1684 MB, throughput 0.996426
Recommendation: one client; give it all the memory
Reading from 2380: heap size 1579 MB, throughput 0.987536
Reading from 2380: heap size 1670 MB, throughput 0.873757
Reading from 2380: heap size 1689 MB, throughput 0.76765
Reading from 2380: heap size 1718 MB, throughput 0.800596
Reading from 2380: heap size 1743 MB, throughput 0.782832
Client 2380 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
