economemd
    total memory: 5012 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub23_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 30489: heap size 9 MB, throughput 0.989712
Clients: 1
Client 30489 has a minimum heap size of 30 MB
Reading from 30488: heap size 9 MB, throughput 0.991384
Clients: 2
Client 30488 has a minimum heap size of 1223 MB
Reading from 30489: heap size 9 MB, throughput 0.960001
Reading from 30489: heap size 9 MB, throughput 0.966911
Reading from 30488: heap size 9 MB, throughput 0.91183
Reading from 30489: heap size 9 MB, throughput 0.970803
Reading from 30489: heap size 11 MB, throughput 0.734344
Reading from 30488: heap size 9 MB, throughput 0.956665
Reading from 30489: heap size 11 MB, throughput 0.650919
Reading from 30489: heap size 16 MB, throughput 0.783029
Reading from 30489: heap size 16 MB, throughput 0.655063
Reading from 30489: heap size 24 MB, throughput 0.867394
Reading from 30489: heap size 24 MB, throughput 0.854273
Reading from 30489: heap size 34 MB, throughput 0.908819
Reading from 30489: heap size 34 MB, throughput 0.890355
Reading from 30488: heap size 9 MB, throughput 0.955107
Reading from 30489: heap size 50 MB, throughput 0.947271
Reading from 30489: heap size 50 MB, throughput 0.915648
Reading from 30489: heap size 75 MB, throughput 0.962432
Reading from 30489: heap size 75 MB, throughput 0.954456
Reading from 30489: heap size 116 MB, throughput 0.963523
Reading from 30489: heap size 116 MB, throughput 0.962259
Reading from 30488: heap size 11 MB, throughput 0.985654
Reading from 30488: heap size 11 MB, throughput 0.945473
Reading from 30488: heap size 17 MB, throughput 0.906357
Reading from 30488: heap size 17 MB, throughput 0.516791
Reading from 30488: heap size 30 MB, throughput 0.935583
Reading from 30488: heap size 31 MB, throughput 0.852918
Reading from 30488: heap size 34 MB, throughput 0.458549
Reading from 30488: heap size 44 MB, throughput 0.731605
Reading from 30489: heap size 159 MB, throughput 0.949556
Reading from 30488: heap size 49 MB, throughput 0.727525
Reading from 30488: heap size 49 MB, throughput 0.286314
Reading from 30488: heap size 68 MB, throughput 0.848974
Reading from 30489: heap size 165 MB, throughput 0.860265
Reading from 30488: heap size 70 MB, throughput 0.237663
Reading from 30488: heap size 94 MB, throughput 0.830473
Reading from 30488: heap size 96 MB, throughput 0.783502
Reading from 30488: heap size 98 MB, throughput 0.76242
Reading from 30488: heap size 101 MB, throughput 0.633238
Reading from 30489: heap size 220 MB, throughput 0.989588
Reading from 30488: heap size 104 MB, throughput 0.154796
Reading from 30488: heap size 135 MB, throughput 0.64545
Reading from 30488: heap size 139 MB, throughput 0.704729
Reading from 30488: heap size 142 MB, throughput 0.673288
Reading from 30488: heap size 145 MB, throughput 0.607118
Reading from 30489: heap size 238 MB, throughput 0.918651
Reading from 30488: heap size 151 MB, throughput 0.500868
Reading from 30488: heap size 154 MB, throughput 0.0988059
Reading from 30488: heap size 190 MB, throughput 0.467874
Reading from 30488: heap size 197 MB, throughput 0.712855
Reading from 30488: heap size 198 MB, throughput 0.686635
Reading from 30488: heap size 201 MB, throughput 0.558765
Reading from 30489: heap size 255 MB, throughput 0.995197
Reading from 30488: heap size 205 MB, throughput 0.390517
Reading from 30488: heap size 208 MB, throughput 0.540986
Reading from 30488: heap size 216 MB, throughput 0.599254
Reading from 30489: heap size 254 MB, throughput 0.963466
Reading from 30488: heap size 222 MB, throughput 0.0933721
Reading from 30488: heap size 263 MB, throughput 0.557161
Reading from 30488: heap size 271 MB, throughput 0.527012
Reading from 30488: heap size 271 MB, throughput 0.743924
Reading from 30488: heap size 275 MB, throughput 0.517841
Reading from 30489: heap size 276 MB, throughput 0.986049
Reading from 30488: heap size 275 MB, throughput 0.116815
Reading from 30488: heap size 319 MB, throughput 0.59582
Reading from 30488: heap size 321 MB, throughput 0.645937
Reading from 30489: heap size 280 MB, throughput 0.994021
Reading from 30488: heap size 320 MB, throughput 0.656156
Reading from 30488: heap size 325 MB, throughput 0.605041
Reading from 30488: heap size 327 MB, throughput 0.544799
Reading from 30488: heap size 336 MB, throughput 0.567189
Reading from 30488: heap size 341 MB, throughput 0.429344
Reading from 30489: heap size 296 MB, throughput 0.833146
Reading from 30488: heap size 348 MB, throughput 0.0951043
Equal recommendation: 2506 MB each
Reading from 30488: heap size 401 MB, throughput 0.395498
Reading from 30488: heap size 403 MB, throughput 0.644645
Reading from 30488: heap size 400 MB, throughput 0.636192
Reading from 30489: heap size 318 MB, throughput 0.980775
Reading from 30488: heap size 402 MB, throughput 0.083629
Reading from 30488: heap size 451 MB, throughput 0.451884
Reading from 30488: heap size 454 MB, throughput 0.648569
Reading from 30489: heap size 341 MB, throughput 0.985641
Reading from 30488: heap size 455 MB, throughput 0.606439
Reading from 30488: heap size 455 MB, throughput 0.580734
Reading from 30488: heap size 456 MB, throughput 0.543538
Reading from 30488: heap size 463 MB, throughput 0.52881
Reading from 30488: heap size 466 MB, throughput 0.435992
Reading from 30488: heap size 477 MB, throughput 0.425201
Reading from 30489: heap size 342 MB, throughput 0.97141
Reading from 30488: heap size 483 MB, throughput 0.0891408
Reading from 30488: heap size 547 MB, throughput 0.351924
Reading from 30488: heap size 546 MB, throughput 0.509112
Reading from 30489: heap size 358 MB, throughput 0.977729
Reading from 30488: heap size 551 MB, throughput 0.37653
Reading from 30488: heap size 546 MB, throughput 0.0842235
Reading from 30489: heap size 362 MB, throughput 0.975007
Reading from 30488: heap size 607 MB, throughput 0.423014
Reading from 30488: heap size 601 MB, throughput 0.642866
Reading from 30488: heap size 605 MB, throughput 0.554961
Reading from 30488: heap size 606 MB, throughput 0.556063
Reading from 30488: heap size 606 MB, throughput 0.549219
Reading from 30489: heap size 387 MB, throughput 0.979368
Reading from 30488: heap size 611 MB, throughput 0.114109
Reading from 30488: heap size 682 MB, throughput 0.432188
Reading from 30489: heap size 388 MB, throughput 0.983837
Reading from 30488: heap size 685 MB, throughput 0.880148
Reading from 30488: heap size 688 MB, throughput 0.754911
Reading from 30489: heap size 413 MB, throughput 0.90334
Reading from 30488: heap size 685 MB, throughput 0.869153
Equal recommendation: 2506 MB each
Reading from 30488: heap size 701 MB, throughput 0.48172
Reading from 30488: heap size 708 MB, throughput 0.667755
Reading from 30488: heap size 716 MB, throughput 0.3229
Reading from 30489: heap size 427 MB, throughput 0.988759
Reading from 30488: heap size 725 MB, throughput 0.291226
Reading from 30488: heap size 727 MB, throughput 0.376171
Reading from 30489: heap size 455 MB, throughput 0.985738
Reading from 30488: heap size 725 MB, throughput 0.0319815
Reading from 30489: heap size 456 MB, throughput 0.982223
Reading from 30488: heap size 800 MB, throughput 0.748892
Reading from 30488: heap size 798 MB, throughput 0.5069
Reading from 30488: heap size 801 MB, throughput 0.558559
Reading from 30488: heap size 808 MB, throughput 0.103006
Reading from 30489: heap size 478 MB, throughput 0.984286
Reading from 30488: heap size 890 MB, throughput 0.716674
Reading from 30488: heap size 894 MB, throughput 0.927756
Reading from 30488: heap size 899 MB, throughput 0.745543
Reading from 30488: heap size 908 MB, throughput 0.92713
Reading from 30489: heap size 480 MB, throughput 0.983343
Reading from 30488: heap size 909 MB, throughput 0.876799
Reading from 30488: heap size 914 MB, throughput 0.92683
Reading from 30488: heap size 786 MB, throughput 0.80844
Reading from 30488: heap size 893 MB, throughput 0.833039
Reading from 30488: heap size 792 MB, throughput 0.78366
Reading from 30488: heap size 884 MB, throughput 0.773691
Reading from 30489: heap size 507 MB, throughput 0.985579
Reading from 30488: heap size 892 MB, throughput 0.795368
Reading from 30488: heap size 878 MB, throughput 0.158167
Reading from 30489: heap size 509 MB, throughput 0.987577
Reading from 30488: heap size 976 MB, throughput 0.582395
Reading from 30488: heap size 978 MB, throughput 0.793996
Equal recommendation: 2506 MB each
Reading from 30488: heap size 979 MB, throughput 0.851263
Reading from 30488: heap size 983 MB, throughput 0.940768
Reading from 30489: heap size 533 MB, throughput 0.985612
Reading from 30489: heap size 534 MB, throughput 0.986091
Reading from 30488: heap size 985 MB, throughput 0.954788
Reading from 30488: heap size 980 MB, throughput 0.720137
Reading from 30488: heap size 987 MB, throughput 0.700155
Reading from 30488: heap size 997 MB, throughput 0.763863
Reading from 30488: heap size 1001 MB, throughput 0.67995
Reading from 30488: heap size 997 MB, throughput 0.727137
Reading from 30488: heap size 1005 MB, throughput 0.762697
Reading from 30489: heap size 559 MB, throughput 0.981819
Reading from 30488: heap size 994 MB, throughput 0.8155
Reading from 30488: heap size 1001 MB, throughput 0.838065
Reading from 30488: heap size 993 MB, throughput 0.905122
Reading from 30488: heap size 1000 MB, throughput 0.892192
Reading from 30488: heap size 997 MB, throughput 0.790548
Reading from 30489: heap size 560 MB, throughput 0.989228
Reading from 30488: heap size 1003 MB, throughput 0.750302
Reading from 30488: heap size 993 MB, throughput 0.648835
Reading from 30488: heap size 1011 MB, throughput 0.672511
Reading from 30489: heap size 584 MB, throughput 0.980383
Reading from 30488: heap size 1024 MB, throughput 0.0695774
Reading from 30488: heap size 1137 MB, throughput 0.519806
Reading from 30488: heap size 1144 MB, throughput 0.781126
Reading from 30488: heap size 1147 MB, throughput 0.743085
Reading from 30488: heap size 1161 MB, throughput 0.711586
Reading from 30488: heap size 1162 MB, throughput 0.788357
Reading from 30489: heap size 587 MB, throughput 0.985479
Reading from 30488: heap size 1170 MB, throughput 0.664009
Equal recommendation: 2506 MB each
Reading from 30488: heap size 1173 MB, throughput 0.887648
Reading from 30489: heap size 616 MB, throughput 0.986696
Reading from 30489: heap size 618 MB, throughput 0.97898
Reading from 30488: heap size 1176 MB, throughput 0.970203
Reading from 30489: heap size 643 MB, throughput 0.988851
Reading from 30488: heap size 1182 MB, throughput 0.966215
Reading from 30489: heap size 645 MB, throughput 0.993418
Reading from 30489: heap size 668 MB, throughput 0.990846
Reading from 30488: heap size 1178 MB, throughput 0.976415
Equal recommendation: 2506 MB each
Reading from 30489: heap size 671 MB, throughput 0.98733
Reading from 30488: heap size 1185 MB, throughput 0.967481
Reading from 30489: heap size 693 MB, throughput 0.996171
Reading from 30489: heap size 694 MB, throughput 0.988706
Reading from 30488: heap size 1194 MB, throughput 0.973551
Reading from 30489: heap size 718 MB, throughput 0.989944
Reading from 30488: heap size 1195 MB, throughput 0.97411
Reading from 30489: heap size 718 MB, throughput 0.989268
Equal recommendation: 2506 MB each
Reading from 30489: heap size 741 MB, throughput 0.99349
Reading from 30488: heap size 1190 MB, throughput 0.979134
Reading from 30489: heap size 742 MB, throughput 0.994451
Reading from 30488: heap size 1195 MB, throughput 0.975484
Reading from 30489: heap size 761 MB, throughput 0.989827
Reading from 30489: heap size 763 MB, throughput 0.988832
Equal recommendation: 2506 MB each
Reading from 30488: heap size 1198 MB, throughput 0.970439
Reading from 30489: heap size 787 MB, throughput 0.990141
Reading from 30488: heap size 1199 MB, throughput 0.970513
Reading from 30489: heap size 786 MB, throughput 0.989765
Reading from 30489: heap size 812 MB, throughput 0.989428
Reading from 30488: heap size 1204 MB, throughput 0.972716
Reading from 30489: heap size 812 MB, throughput 0.991134
Equal recommendation: 2506 MB each
Reading from 30488: heap size 1207 MB, throughput 0.976221
Reading from 30489: heap size 837 MB, throughput 0.99343
Reading from 30488: heap size 1212 MB, throughput 0.974086
Reading from 30489: heap size 838 MB, throughput 0.991191
Reading from 30489: heap size 863 MB, throughput 0.99193
Reading from 30488: heap size 1216 MB, throughput 0.975095
Equal recommendation: 2506 MB each
Reading from 30489: heap size 863 MB, throughput 0.990784
Reading from 30488: heap size 1221 MB, throughput 0.973545
Reading from 30489: heap size 889 MB, throughput 0.99264
Reading from 30488: heap size 1226 MB, throughput 0.97372
Reading from 30489: heap size 889 MB, throughput 0.992766
Reading from 30489: heap size 913 MB, throughput 0.992274
Reading from 30488: heap size 1231 MB, throughput 0.971985
Equal recommendation: 2506 MB each
Reading from 30489: heap size 914 MB, throughput 0.991043
Reading from 30488: heap size 1236 MB, throughput 0.971607
Reading from 30489: heap size 939 MB, throughput 0.990181
Reading from 30488: heap size 1241 MB, throughput 0.969615
Reading from 30489: heap size 939 MB, throughput 0.9896
Equal recommendation: 2506 MB each
Reading from 30488: heap size 1244 MB, throughput 0.973655
Reading from 30489: heap size 970 MB, throughput 0.992825
Reading from 30488: heap size 1250 MB, throughput 0.968641
Reading from 30489: heap size 970 MB, throughput 0.989824
Reading from 30489: heap size 999 MB, throughput 0.992633
Equal recommendation: 2506 MB each
Reading from 30488: heap size 1251 MB, throughput 0.660388
Reading from 30489: heap size 1001 MB, throughput 0.991923
Reading from 30488: heap size 1322 MB, throughput 0.994747
Reading from 30489: heap size 1031 MB, throughput 0.990561
Reading from 30488: heap size 1323 MB, throughput 0.992846
Reading from 30489: heap size 1032 MB, throughput 0.990591
Equal recommendation: 2506 MB each
Reading from 30488: heap size 1333 MB, throughput 0.9907
Reading from 30489: heap size 1064 MB, throughput 0.991973
Reading from 30489: heap size 1065 MB, throughput 0.992465
Reading from 30488: heap size 1338 MB, throughput 0.985228
Reading from 30489: heap size 1095 MB, throughput 0.993635
Reading from 30488: heap size 1338 MB, throughput 0.987748
Equal recommendation: 2506 MB each
Reading from 30489: heap size 1096 MB, throughput 0.987517
Reading from 30488: heap size 1217 MB, throughput 0.984157
Reading from 30489: heap size 1123 MB, throughput 0.991611
Reading from 30488: heap size 1327 MB, throughput 0.986412
Reading from 30489: heap size 1128 MB, throughput 0.990815
Equal recommendation: 2506 MB each
Reading from 30488: heap size 1237 MB, throughput 0.984788
Reading from 30489: heap size 1166 MB, throughput 0.989348
Reading from 30488: heap size 1313 MB, throughput 0.980768
Reading from 30489: heap size 1167 MB, throughput 0.990478
Reading from 30488: heap size 1256 MB, throughput 0.979144
Equal recommendation: 2506 MB each
Reading from 30489: heap size 1208 MB, throughput 0.993047
Reading from 30488: heap size 1319 MB, throughput 0.977309
Reading from 30489: heap size 1210 MB, throughput 0.988096
Reading from 30488: heap size 1320 MB, throughput 0.974215
Reading from 30489: heap size 1244 MB, throughput 0.993738
Equal recommendation: 2506 MB each
Reading from 30488: heap size 1322 MB, throughput 0.974825
Reading from 30489: heap size 1248 MB, throughput 0.9919
Reading from 30488: heap size 1326 MB, throughput 0.972964
Reading from 30489: heap size 1285 MB, throughput 0.991785
Reading from 30488: heap size 1329 MB, throughput 0.972071
Equal recommendation: 2506 MB each
Reading from 30489: heap size 1286 MB, throughput 0.989866
Reading from 30488: heap size 1336 MB, throughput 0.970632
Reading from 30489: heap size 1323 MB, throughput 0.993241
Reading from 30488: heap size 1344 MB, throughput 0.970298
Equal recommendation: 2506 MB each
Reading from 30489: heap size 1324 MB, throughput 0.988593
Reading from 30488: heap size 1350 MB, throughput 0.971284
Reading from 30489: heap size 1361 MB, throughput 0.993719
Reading from 30488: heap size 1357 MB, throughput 0.973502
Reading from 30489: heap size 1363 MB, throughput 0.98923
Equal recommendation: 2506 MB each
Reading from 30488: heap size 1361 MB, throughput 0.9691
Reading from 30489: heap size 1402 MB, throughput 0.968517
Reading from 30489: heap size 1414 MB, throughput 0.993721
Equal recommendation: 2506 MB each
Reading from 30489: heap size 1460 MB, throughput 0.988721
Reading from 30489: heap size 1459 MB, throughput 0.99065
Equal recommendation: 2506 MB each
Reading from 30488: heap size 1368 MB, throughput 0.859759
Reading from 30489: heap size 1524 MB, throughput 0.990384
Reading from 30489: heap size 1525 MB, throughput 0.987047
Equal recommendation: 2506 MB each
Reading from 30489: heap size 1585 MB, throughput 0.988958
Reading from 30489: heap size 1591 MB, throughput 0.985789
Reading from 30488: heap size 1506 MB, throughput 0.985689
Equal recommendation: 2506 MB each
Reading from 30489: heap size 1646 MB, throughput 0.987525
Reading from 30488: heap size 1536 MB, throughput 0.985631
Reading from 30488: heap size 1542 MB, throughput 0.87033
Reading from 30488: heap size 1542 MB, throughput 0.769102
Reading from 30488: heap size 1550 MB, throughput 0.721226
Reading from 30488: heap size 1576 MB, throughput 0.729525
Reading from 30488: heap size 1596 MB, throughput 0.706546
Reading from 30489: heap size 1655 MB, throughput 0.986336
Reading from 30488: heap size 1629 MB, throughput 0.75546
Reading from 30488: heap size 1640 MB, throughput 0.79234
Equal recommendation: 2506 MB each
Reading from 30488: heap size 1664 MB, throughput 0.98073
Reading from 30489: heap size 1616 MB, throughput 0.988636
Reading from 30488: heap size 1673 MB, throughput 0.982985
Reading from 30489: heap size 1622 MB, throughput 0.986867
Client 30489 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 30488: heap size 1670 MB, throughput 0.986088
Reading from 30488: heap size 1684 MB, throughput 0.985779
Recommendation: one client; give it all the memory
Reading from 30488: heap size 1672 MB, throughput 0.986296
Reading from 30488: heap size 1686 MB, throughput 0.985177
Recommendation: one client; give it all the memory
Reading from 30488: heap size 1678 MB, throughput 0.985561
Reading from 30488: heap size 1687 MB, throughput 0.989248
Recommendation: one client; give it all the memory
Reading from 30488: heap size 1698 MB, throughput 0.989085
Recommendation: one client; give it all the memory
Reading from 30488: heap size 1701 MB, throughput 0.988174
Reading from 30488: heap size 1693 MB, throughput 0.987556
Recommendation: one client; give it all the memory
Reading from 30488: heap size 1701 MB, throughput 0.987032
Recommendation: one client; give it all the memory
Reading from 30488: heap size 1683 MB, throughput 0.986679
Reading from 30488: heap size 1692 MB, throughput 0.986119
Recommendation: one client; give it all the memory
Reading from 30488: heap size 1695 MB, throughput 0.984365
Reading from 30488: heap size 1696 MB, throughput 0.982688
Recommendation: one client; give it all the memory
Reading from 30488: heap size 1703 MB, throughput 0.865969
Recommendation: one client; give it all the memory
Reading from 30488: heap size 1672 MB, throughput 0.996751
Recommendation: one client; give it all the memory
Reading from 30488: heap size 1670 MB, throughput 0.995222
Reading from 30488: heap size 1684 MB, throughput 0.994098
Recommendation: one client; give it all the memory
Reading from 30488: heap size 1696 MB, throughput 0.992746
Reading from 30488: heap size 1697 MB, throughput 0.991811
Recommendation: one client; give it all the memory
Reading from 30488: heap size 1685 MB, throughput 0.990662
Recommendation: one client; give it all the memory
Reading from 30488: heap size 1539 MB, throughput 0.990001
Reading from 30488: heap size 1665 MB, throughput 0.989391
Recommendation: one client; give it all the memory
Reading from 30488: heap size 1570 MB, throughput 0.987981
Recommendation: one client; give it all the memory
Reading from 30488: heap size 1668 MB, throughput 0.986833
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 30488: heap size 1671 MB, throughput 0.99582
Reading from 30488: heap size 1654 MB, throughput 0.985591
Reading from 30488: heap size 1679 MB, throughput 0.828088
Reading from 30488: heap size 1658 MB, throughput 0.80388
Reading from 30488: heap size 1696 MB, throughput 0.785396
Reading from 30488: heap size 1733 MB, throughput 0.811885
Client 30488 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
