#ifndef SIMPLEHEAP_HPP
#define SIMPLEHEAP_HPP

#include <DaemonProxy.hpp>
#include <Messages.hpp>
#include "Heap.hpp"

struct timeval;

class SimpleHeap : public Heap
{
public:
    SimpleHeap(unsigned long long capacityMB, unsigned long long minHeapMB, economem::DaemonProxy& proxy);
    virtual void alloc(unsigned long long bytes);
    virtual unsigned long long getCapacityMB() const;

private:

    bool allocInternal(unsigned long long bytes);
    void garbageCollect();
    void resizeHeap();

    economem::Recommendation m_Capacity;
    unsigned long long m_UsedB;
    unsigned long long m_MinHeapB;
    economem::DaemonProxy& m_Proxy;
};

#endif
