#ifndef UNBOUNDEDCACHE_HPP
#define UNBOUNDEDCACHE_HPP

#include <map>
#include "CacheReading.hpp"
#include "ReadingCache.hpp"

class UnboundedCache : public ReadingCache
{
public:
    static const char* NAME;

    UnboundedCache() {}
    virtual ~UnboundedCache() {}
    virtual std::string getName() const;
    virtual bool isValid() const;
    virtual void addReading(unsigned long long heapSizeMB, double throughput);
    virtual ReadingIterator* newIterator() const;
    virtual void dumpTo(FILE* fd) const;

private:
    class Iterator;

    std::map<unsigned long long, CacheReading> m_Cache;
    static const size_t MIN_READINGS_VALID = 2;
};

#endif
