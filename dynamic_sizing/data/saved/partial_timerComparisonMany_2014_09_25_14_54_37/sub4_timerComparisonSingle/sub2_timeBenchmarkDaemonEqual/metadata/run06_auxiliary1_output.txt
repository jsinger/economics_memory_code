economemd
    total memory: 5948 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub4_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 7156: heap size 9 MB, throughput 0.991166
Clients: 1
Client 7156 has a minimum heap size of 1211 MB
Reading from 7157: heap size 9 MB, throughput 0.964974
Clients: 2
Client 7157 has a minimum heap size of 276 MB
Reading from 7156: heap size 9 MB, throughput 0.978479
Reading from 7157: heap size 9 MB, throughput 0.974814
Reading from 7156: heap size 9 MB, throughput 0.966826
Reading from 7156: heap size 9 MB, throughput 0.961232
Reading from 7157: heap size 11 MB, throughput 0.975659
Reading from 7156: heap size 11 MB, throughput 0.972098
Reading from 7157: heap size 11 MB, throughput 0.987025
Reading from 7156: heap size 11 MB, throughput 0.980045
Reading from 7156: heap size 17 MB, throughput 0.888267
Reading from 7157: heap size 15 MB, throughput 0.896754
Reading from 7156: heap size 17 MB, throughput 0.606292
Reading from 7157: heap size 18 MB, throughput 0.945563
Reading from 7156: heap size 30 MB, throughput 0.958106
Reading from 7157: heap size 24 MB, throughput 0.945828
Reading from 7157: heap size 28 MB, throughput 0.906426
Reading from 7156: heap size 31 MB, throughput 0.965419
Reading from 7157: heap size 31 MB, throughput 0.518317
Reading from 7156: heap size 34 MB, throughput 0.493994
Reading from 7157: heap size 40 MB, throughput 0.859773
Reading from 7156: heap size 47 MB, throughput 0.807255
Reading from 7157: heap size 46 MB, throughput 0.926202
Reading from 7156: heap size 51 MB, throughput 0.894136
Reading from 7157: heap size 48 MB, throughput 0.331589
Reading from 7156: heap size 52 MB, throughput 0.399657
Reading from 7157: heap size 64 MB, throughput 0.73854
Reading from 7156: heap size 71 MB, throughput 0.803934
Reading from 7157: heap size 67 MB, throughput 0.830658
Reading from 7156: heap size 73 MB, throughput 0.834755
Reading from 7156: heap size 78 MB, throughput 0.826418
Reading from 7157: heap size 70 MB, throughput 0.221774
Reading from 7157: heap size 91 MB, throughput 0.449366
Reading from 7157: heap size 98 MB, throughput 0.810206
Reading from 7156: heap size 80 MB, throughput 0.213807
Reading from 7157: heap size 100 MB, throughput 0.6768
Reading from 7156: heap size 113 MB, throughput 0.723906
Reading from 7157: heap size 104 MB, throughput 0.801058
Reading from 7156: heap size 113 MB, throughput 0.76598
Reading from 7157: heap size 109 MB, throughput 0.719234
Reading from 7156: heap size 117 MB, throughput 0.76738
Reading from 7156: heap size 121 MB, throughput 0.746201
Reading from 7156: heap size 125 MB, throughput 0.72815
Reading from 7157: heap size 113 MB, throughput 0.183873
Reading from 7157: heap size 140 MB, throughput 0.645717
Reading from 7157: heap size 147 MB, throughput 0.645105
Reading from 7157: heap size 151 MB, throughput 0.580974
Reading from 7156: heap size 130 MB, throughput 0.152566
Reading from 7156: heap size 172 MB, throughput 0.539378
Reading from 7156: heap size 174 MB, throughput 0.67457
Reading from 7157: heap size 158 MB, throughput 0.183321
Reading from 7156: heap size 177 MB, throughput 0.765864
Reading from 7157: heap size 191 MB, throughput 0.65778
Reading from 7156: heap size 181 MB, throughput 0.714613
Reading from 7157: heap size 196 MB, throughput 0.741521
Reading from 7156: heap size 185 MB, throughput 0.663759
Reading from 7157: heap size 198 MB, throughput 0.665272
Reading from 7157: heap size 201 MB, throughput 0.875569
Reading from 7156: heap size 194 MB, throughput 0.155056
Reading from 7156: heap size 232 MB, throughput 0.559909
Reading from 7156: heap size 239 MB, throughput 0.533553
Reading from 7156: heap size 241 MB, throughput 0.501223
Reading from 7157: heap size 206 MB, throughput 0.86743
Reading from 7157: heap size 209 MB, throughput 0.718219
Reading from 7157: heap size 219 MB, throughput 0.565167
Reading from 7157: heap size 226 MB, throughput 0.732823
Reading from 7156: heap size 245 MB, throughput 0.158137
Reading from 7156: heap size 293 MB, throughput 0.585373
Reading from 7156: heap size 293 MB, throughput 0.740533
Reading from 7157: heap size 233 MB, throughput 0.0866932
Reading from 7156: heap size 292 MB, throughput 0.695228
Reading from 7157: heap size 275 MB, throughput 0.70861
Reading from 7156: heap size 292 MB, throughput 0.620161
Reading from 7157: heap size 275 MB, throughput 0.725877
Reading from 7157: heap size 266 MB, throughput 0.704504
Reading from 7156: heap size 294 MB, throughput 0.630531
Reading from 7156: heap size 300 MB, throughput 0.535807
Reading from 7157: heap size 270 MB, throughput 0.217939
Reading from 7157: heap size 304 MB, throughput 0.427383
Reading from 7157: heap size 308 MB, throughput 0.60051
Reading from 7156: heap size 305 MB, throughput 0.122753
Reading from 7157: heap size 310 MB, throughput 0.808397
Reading from 7157: heap size 311 MB, throughput 0.746627
Reading from 7156: heap size 357 MB, throughput 0.486801
Reading from 7156: heap size 367 MB, throughput 0.683724
Reading from 7157: heap size 310 MB, throughput 0.897745
Reading from 7156: heap size 369 MB, throughput 0.616464
Reading from 7157: heap size 312 MB, throughput 0.916775
Reading from 7157: heap size 308 MB, throughput 0.647072
Reading from 7157: heap size 311 MB, throughput 0.756829
Reading from 7157: heap size 312 MB, throughput 0.853537
Reading from 7156: heap size 374 MB, throughput 0.128676
Reading from 7156: heap size 423 MB, throughput 0.536789
Reading from 7157: heap size 313 MB, throughput 0.758552
Reading from 7156: heap size 425 MB, throughput 0.656475
Reading from 7157: heap size 316 MB, throughput 0.834929
Reading from 7157: heap size 317 MB, throughput 0.677711
Reading from 7156: heap size 428 MB, throughput 0.686858
Reading from 7157: heap size 322 MB, throughput 0.720287
Reading from 7156: heap size 431 MB, throughput 0.675065
Reading from 7157: heap size 322 MB, throughput 0.724552
Reading from 7156: heap size 437 MB, throughput 0.570578
Reading from 7156: heap size 440 MB, throughput 0.532559
Reading from 7156: heap size 451 MB, throughput 0.473666
Equal recommendation: 2974 MB each
Reading from 7156: heap size 459 MB, throughput 0.550301
Reading from 7157: heap size 330 MB, throughput 0.0918933
Reading from 7157: heap size 374 MB, throughput 0.562093
Reading from 7157: heap size 379 MB, throughput 0.793797
Reading from 7156: heap size 470 MB, throughput 0.0891406
Reading from 7156: heap size 534 MB, throughput 0.429101
Reading from 7156: heap size 540 MB, throughput 0.482412
Reading from 7156: heap size 540 MB, throughput 0.504528
Reading from 7157: heap size 380 MB, throughput 0.970557
Reading from 7156: heap size 541 MB, throughput 0.0922338
Reading from 7156: heap size 598 MB, throughput 0.40434
Reading from 7156: heap size 533 MB, throughput 0.607729
Reading from 7156: heap size 598 MB, throughput 0.637029
Reading from 7156: heap size 600 MB, throughput 0.639985
Reading from 7157: heap size 383 MB, throughput 0.981859
Reading from 7156: heap size 602 MB, throughput 0.677017
Reading from 7156: heap size 609 MB, throughput 0.560708
Reading from 7156: heap size 612 MB, throughput 0.491979
Reading from 7157: heap size 386 MB, throughput 0.961279
Reading from 7156: heap size 624 MB, throughput 0.0969949
Reading from 7156: heap size 699 MB, throughput 0.799718
Reading from 7156: heap size 707 MB, throughput 0.837589
Reading from 7157: heap size 385 MB, throughput 0.976741
Reading from 7156: heap size 707 MB, throughput 0.851185
Reading from 7156: heap size 711 MB, throughput 0.805138
Reading from 7156: heap size 717 MB, throughput 0.506298
Reading from 7156: heap size 725 MB, throughput 0.534993
Reading from 7157: heap size 388 MB, throughput 0.979875
Reading from 7156: heap size 732 MB, throughput 0.389384
Reading from 7157: heap size 387 MB, throughput 0.947271
Reading from 7156: heap size 734 MB, throughput 0.0236814
Equal recommendation: 2974 MB each
Reading from 7156: heap size 790 MB, throughput 0.238531
Reading from 7156: heap size 798 MB, throughput 0.399039
Reading from 7157: heap size 389 MB, throughput 0.957996
Reading from 7156: heap size 797 MB, throughput 0.217977
Reading from 7156: heap size 881 MB, throughput 0.598984
Reading from 7156: heap size 886 MB, throughput 0.648317
Reading from 7156: heap size 888 MB, throughput 0.6468
Reading from 7157: heap size 392 MB, throughput 0.972111
Reading from 7156: heap size 891 MB, throughput 0.854366
Reading from 7156: heap size 892 MB, throughput 0.713497
Reading from 7156: heap size 890 MB, throughput 0.839368
Reading from 7156: heap size 892 MB, throughput 0.0821677
Reading from 7157: heap size 393 MB, throughput 0.968481
Reading from 7156: heap size 988 MB, throughput 0.723726
Reading from 7156: heap size 990 MB, throughput 0.941369
Reading from 7156: heap size 998 MB, throughput 0.912886
Reading from 7156: heap size 831 MB, throughput 0.74143
Reading from 7156: heap size 979 MB, throughput 0.718723
Reading from 7156: heap size 837 MB, throughput 0.81249
Reading from 7157: heap size 396 MB, throughput 0.970846
Reading from 7156: heap size 969 MB, throughput 0.824989
Reading from 7156: heap size 843 MB, throughput 0.797946
Reading from 7156: heap size 961 MB, throughput 0.804044
Reading from 7156: heap size 968 MB, throughput 0.828803
Reading from 7156: heap size 955 MB, throughput 0.841701
Reading from 7156: heap size 961 MB, throughput 0.814941
Reading from 7156: heap size 949 MB, throughput 0.972399
Reading from 7157: heap size 398 MB, throughput 0.976547
Reading from 7157: heap size 396 MB, throughput 0.974774
Reading from 7156: heap size 955 MB, throughput 0.966858
Reading from 7156: heap size 958 MB, throughput 0.814014
Reading from 7156: heap size 960 MB, throughput 0.786009
Reading from 7156: heap size 961 MB, throughput 0.791643
Reading from 7156: heap size 963 MB, throughput 0.783402
Reading from 7156: heap size 961 MB, throughput 0.813403
Reading from 7156: heap size 964 MB, throughput 0.816584
Reading from 7156: heap size 963 MB, throughput 0.756324
Equal recommendation: 2974 MB each
Reading from 7157: heap size 398 MB, throughput 0.973274
Reading from 7156: heap size 966 MB, throughput 0.788768
Reading from 7156: heap size 967 MB, throughput 0.83907
Reading from 7156: heap size 970 MB, throughput 0.882374
Reading from 7156: heap size 971 MB, throughput 0.777231
Reading from 7156: heap size 974 MB, throughput 0.666677
Reading from 7157: heap size 396 MB, throughput 0.978809
Reading from 7156: heap size 967 MB, throughput 0.65959
Reading from 7157: heap size 401 MB, throughput 0.847242
Reading from 7156: heap size 986 MB, throughput 0.631635
Reading from 7157: heap size 398 MB, throughput 0.67069
Reading from 7157: heap size 402 MB, throughput 0.654371
Reading from 7156: heap size 999 MB, throughput 0.640993
Reading from 7157: heap size 412 MB, throughput 0.841308
Reading from 7156: heap size 1006 MB, throughput 0.694021
Reading from 7156: heap size 1010 MB, throughput 0.697742
Reading from 7156: heap size 1013 MB, throughput 0.664764
Reading from 7156: heap size 1024 MB, throughput 0.698832
Reading from 7156: heap size 1025 MB, throughput 0.686081
Reading from 7156: heap size 1040 MB, throughput 0.73228
Reading from 7157: heap size 413 MB, throughput 0.973355
Reading from 7156: heap size 1041 MB, throughput 0.943579
Reading from 7157: heap size 420 MB, throughput 0.981602
Reading from 7156: heap size 1059 MB, throughput 0.950468
Reading from 7157: heap size 421 MB, throughput 0.985517
Reading from 7157: heap size 424 MB, throughput 0.967829
Equal recommendation: 2974 MB each
Reading from 7156: heap size 1061 MB, throughput 0.423863
Reading from 7157: heap size 426 MB, throughput 0.985827
Reading from 7156: heap size 1160 MB, throughput 0.923561
Reading from 7157: heap size 426 MB, throughput 0.756713
Reading from 7156: heap size 1164 MB, throughput 0.983636
Reading from 7157: heap size 450 MB, throughput 0.993706
Reading from 7156: heap size 1177 MB, throughput 0.980261
Reading from 7157: heap size 449 MB, throughput 0.9919
Reading from 7157: heap size 453 MB, throughput 0.989496
Reading from 7156: heap size 1179 MB, throughput 0.978506
Reading from 7157: heap size 456 MB, throughput 0.989272
Reading from 7156: heap size 1176 MB, throughput 0.978311
Equal recommendation: 2974 MB each
Reading from 7156: heap size 1181 MB, throughput 0.970261
Reading from 7157: heap size 457 MB, throughput 0.988917
Reading from 7157: heap size 452 MB, throughput 0.9235
Reading from 7157: heap size 456 MB, throughput 0.819363
Reading from 7157: heap size 460 MB, throughput 0.865529
Reading from 7157: heap size 463 MB, throughput 0.974842
Reading from 7156: heap size 1168 MB, throughput 0.968587
Reading from 7157: heap size 467 MB, throughput 0.988238
Reading from 7156: heap size 1096 MB, throughput 0.968619
Reading from 7157: heap size 470 MB, throughput 0.988001
Reading from 7156: heap size 1163 MB, throughput 0.970555
Reading from 7157: heap size 468 MB, throughput 0.977658
Reading from 7156: heap size 1168 MB, throughput 0.954842
Equal recommendation: 2974 MB each
Reading from 7157: heap size 471 MB, throughput 0.985032
Reading from 7156: heap size 1171 MB, throughput 0.971055
Reading from 7157: heap size 469 MB, throughput 0.98256
Reading from 7156: heap size 1172 MB, throughput 0.957302
Reading from 7157: heap size 472 MB, throughput 0.984883
Reading from 7156: heap size 1175 MB, throughput 0.960083
Reading from 7157: heap size 475 MB, throughput 0.983246
Reading from 7156: heap size 1180 MB, throughput 0.934019
Reading from 7157: heap size 475 MB, throughput 0.980949
Equal recommendation: 2974 MB each
Reading from 7156: heap size 1184 MB, throughput 0.953016
Reading from 7157: heap size 479 MB, throughput 0.988544
Reading from 7157: heap size 480 MB, throughput 0.902298
Reading from 7157: heap size 480 MB, throughput 0.830907
Reading from 7156: heap size 1191 MB, throughput 0.941086
Reading from 7157: heap size 482 MB, throughput 0.959736
Reading from 7156: heap size 1199 MB, throughput 0.955802
Reading from 7157: heap size 490 MB, throughput 0.990334
Reading from 7156: heap size 1206 MB, throughput 0.960459
Reading from 7157: heap size 491 MB, throughput 0.990175
Reading from 7156: heap size 1214 MB, throughput 0.953584
Reading from 7157: heap size 494 MB, throughput 0.987622
Equal recommendation: 2974 MB each
Reading from 7156: heap size 1217 MB, throughput 0.94905
Reading from 7157: heap size 496 MB, throughput 0.989447
Reading from 7156: heap size 1224 MB, throughput 0.95036
Reading from 7157: heap size 494 MB, throughput 0.987528
Reading from 7156: heap size 1226 MB, throughput 0.951102
Reading from 7157: heap size 497 MB, throughput 0.988505
Reading from 7156: heap size 1233 MB, throughput 0.963271
Equal recommendation: 2974 MB each
Reading from 7157: heap size 498 MB, throughput 0.985203
Reading from 7156: heap size 1234 MB, throughput 0.949566
Reading from 7157: heap size 499 MB, throughput 0.990246
Reading from 7157: heap size 504 MB, throughput 0.892446
Reading from 7157: heap size 504 MB, throughput 0.887243
Reading from 7156: heap size 1242 MB, throughput 0.951211
Reading from 7157: heap size 511 MB, throughput 0.990174
Reading from 7156: heap size 1242 MB, throughput 0.950942
Reading from 7157: heap size 513 MB, throughput 0.989504
Reading from 7157: heap size 517 MB, throughput 0.977977
Equal recommendation: 2974 MB each
Reading from 7156: heap size 1249 MB, throughput 0.512384
Reading from 7157: heap size 519 MB, throughput 0.989831
Reading from 7156: heap size 1348 MB, throughput 0.932943
Reading from 7157: heap size 518 MB, throughput 0.988881
Reading from 7156: heap size 1351 MB, throughput 0.977567
Reading from 7157: heap size 521 MB, throughput 0.988051
Reading from 7156: heap size 1355 MB, throughput 0.973947
Reading from 7157: heap size 524 MB, throughput 0.985296
Equal recommendation: 2974 MB each
Reading from 7156: heap size 1361 MB, throughput 0.972541
Reading from 7157: heap size 524 MB, throughput 0.981658
Reading from 7157: heap size 528 MB, throughput 0.895642
Reading from 7157: heap size 530 MB, throughput 0.966935
Reading from 7156: heap size 1362 MB, throughput 0.969795
Reading from 7157: heap size 538 MB, throughput 0.993017
Reading from 7156: heap size 1356 MB, throughput 0.979241
Reading from 7157: heap size 539 MB, throughput 0.990016
Reading from 7156: heap size 1361 MB, throughput 0.967722
Equal recommendation: 2974 MB each
Reading from 7156: heap size 1348 MB, throughput 0.963312
Reading from 7157: heap size 541 MB, throughput 0.991407
Reading from 7156: heap size 1355 MB, throughput 0.966154
Reading from 7157: heap size 543 MB, throughput 0.990239
Reading from 7156: heap size 1351 MB, throughput 0.963954
Reading from 7157: heap size 541 MB, throughput 0.987002
Reading from 7156: heap size 1354 MB, throughput 0.962254
Equal recommendation: 2974 MB each
Reading from 7157: heap size 543 MB, throughput 0.992997
Reading from 7157: heap size 546 MB, throughput 0.978203
Reading from 7157: heap size 547 MB, throughput 0.893742
Reading from 7156: heap size 1360 MB, throughput 0.95906
Reading from 7157: heap size 552 MB, throughput 0.986483
Reading from 7157: heap size 553 MB, throughput 0.992176
Reading from 7157: heap size 556 MB, throughput 0.991298
Equal recommendation: 2974 MB each
Reading from 7157: heap size 558 MB, throughput 0.988932
Reading from 7157: heap size 558 MB, throughput 0.988724
Reading from 7157: heap size 560 MB, throughput 0.985763
Equal recommendation: 2974 MB each
Reading from 7156: heap size 1362 MB, throughput 0.852556
Reading from 7157: heap size 562 MB, throughput 0.987105
Reading from 7157: heap size 563 MB, throughput 0.913472
Reading from 7157: heap size 568 MB, throughput 0.984473
Reading from 7157: heap size 570 MB, throughput 0.993606
Reading from 7157: heap size 573 MB, throughput 0.991262
Equal recommendation: 2974 MB each
Reading from 7156: heap size 1464 MB, throughput 0.975734
Reading from 7157: heap size 575 MB, throughput 0.990504
Reading from 7156: heap size 1500 MB, throughput 0.978634
Reading from 7156: heap size 1503 MB, throughput 0.806851
Reading from 7156: heap size 1511 MB, throughput 0.741058
Reading from 7156: heap size 1530 MB, throughput 0.692052
Reading from 7157: heap size 573 MB, throughput 0.989187
Reading from 7156: heap size 1546 MB, throughput 0.723807
Reading from 7156: heap size 1576 MB, throughput 0.787251
Reading from 7156: heap size 1585 MB, throughput 0.791575
Reading from 7156: heap size 1614 MB, throughput 0.966282
Reading from 7157: heap size 576 MB, throughput 0.989123
Equal recommendation: 2974 MB each
Reading from 7157: heap size 577 MB, throughput 0.983987
Reading from 7157: heap size 579 MB, throughput 0.916784
Reading from 7156: heap size 1618 MB, throughput 0.975235
Reading from 7157: heap size 584 MB, throughput 0.991477
Reading from 7156: heap size 1625 MB, throughput 0.976885
Reading from 7157: heap size 585 MB, throughput 0.993152
Reading from 7156: heap size 1635 MB, throughput 0.967931
Equal recommendation: 2974 MB each
Reading from 7157: heap size 589 MB, throughput 0.991895
Reading from 7156: heap size 1630 MB, throughput 0.970272
Reading from 7157: heap size 590 MB, throughput 0.991413
Reading from 7156: heap size 1641 MB, throughput 0.96891
Reading from 7157: heap size 589 MB, throughput 0.989691
Reading from 7156: heap size 1631 MB, throughput 0.968217
Equal recommendation: 2974 MB each
Reading from 7157: heap size 591 MB, throughput 0.99321
Reading from 7157: heap size 594 MB, throughput 0.927174
Reading from 7156: heap size 1642 MB, throughput 0.965237
Reading from 7157: heap size 595 MB, throughput 0.986813
Reading from 7156: heap size 1647 MB, throughput 0.971469
Reading from 7157: heap size 602 MB, throughput 0.993026
Reading from 7156: heap size 1650 MB, throughput 0.972658
Reading from 7157: heap size 603 MB, throughput 0.989969
Equal recommendation: 2974 MB each
Reading from 7156: heap size 1646 MB, throughput 0.969554
Reading from 7157: heap size 601 MB, throughput 0.991572
Reading from 7156: heap size 1654 MB, throughput 0.967538
Reading from 7157: heap size 604 MB, throughput 0.990195
Equal recommendation: 2974 MB each
Reading from 7157: heap size 607 MB, throughput 0.992813
Reading from 7157: heap size 607 MB, throughput 0.934128
Reading from 7156: heap size 1663 MB, throughput 0.970605
Reading from 7157: heap size 608 MB, throughput 0.987153
Reading from 7156: heap size 1663 MB, throughput 0.962596
Reading from 7157: heap size 611 MB, throughput 0.992826
Reading from 7156: heap size 1673 MB, throughput 0.962291
Reading from 7157: heap size 612 MB, throughput 0.992364
Equal recommendation: 2974 MB each
Reading from 7156: heap size 1680 MB, throughput 0.960511
Reading from 7157: heap size 614 MB, throughput 0.991656
Reading from 7156: heap size 1694 MB, throughput 0.962362
Reading from 7157: heap size 615 MB, throughput 0.990741
Reading from 7157: heap size 616 MB, throughput 0.989853
Equal recommendation: 2974 MB each
Reading from 7157: heap size 619 MB, throughput 0.931301
Reading from 7156: heap size 1704 MB, throughput 0.964017
Reading from 7157: heap size 621 MB, throughput 0.99111
Reading from 7156: heap size 1720 MB, throughput 0.958065
Reading from 7157: heap size 627 MB, throughput 0.99311
Equal recommendation: 2974 MB each
Reading from 7157: heap size 629 MB, throughput 0.975524
Reading from 7156: heap size 1728 MB, throughput 0.732164
Reading from 7157: heap size 630 MB, throughput 0.991825
Reading from 7156: heap size 1731 MB, throughput 0.990959
Reading from 7157: heap size 632 MB, throughput 0.994522
Reading from 7156: heap size 1737 MB, throughput 0.987147
Reading from 7157: heap size 635 MB, throughput 0.976576
Equal recommendation: 2974 MB each
Reading from 7157: heap size 636 MB, throughput 0.97795
Reading from 7156: heap size 1762 MB, throughput 0.984446
Reading from 7157: heap size 643 MB, throughput 0.994028
Reading from 7156: heap size 1768 MB, throughput 0.98282
Reading from 7157: heap size 643 MB, throughput 0.993039
Equal recommendation: 2974 MB each
Reading from 7156: heap size 1771 MB, throughput 0.980215
Reading from 7157: heap size 642 MB, throughput 0.99268
Reading from 7157: heap size 644 MB, throughput 0.991021
Reading from 7157: heap size 646 MB, throughput 0.987895
Client 7157 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 7156: heap size 1776 MB, throughput 0.989701
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 7156: heap size 1773 MB, throughput 0.99424
Reading from 7156: heap size 1781 MB, throughput 0.887493
Reading from 7156: heap size 1765 MB, throughput 0.825714
Reading from 7156: heap size 1794 MB, throughput 0.805787
Reading from 7156: heap size 1831 MB, throughput 0.833452
Client 7156 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
