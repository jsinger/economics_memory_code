# A reduced version of staticutility, without dependencies on numpy, scipy and matplotlib,
# so that the remote worker machines don't have to have those libraries installed

import datetime
import errno
import getpass
import glob
import math
import operator
import os.path
import platform
import re
import shutil
import subprocess
import sys
import time


class UtilityException(Exception):
    def __init__(self, message, cause=None):
        self.message = message
        self.cause = cause

    def __str__(self):
        s = self.message
        if self.cause:
            s += "\n" + str(self.cause)
        return s


class Dir:
    def __init__(self, d):
        self.d = d

    def join(self, *s):
        return os.path.realpath(os.path.join(self.d, *s))

    def makedir(self):
        try:
            os.makedirs(self.d)
        except OSError as e:
            if e.errno == errno.EEXIST and os.path.isdir(self.d):
                pass
            else:
                raise UtilityException("Cannot create directory", e)

    def __str__(self):
        return self.d


thisDir = Dir(os.path.dirname(os.path.abspath(__file__)))
inputDir = Dir(thisDir.join("input"))
inputDir.makedir()
outputDir = Dir(thisDir.join("data"))
outputDir.makedir()

class BenchmarkId:
    def __init__(self, name, time, inputSize, threads):
        self.name = name
        self.time = time
        self.inputSize = inputSize
        self.threads = str(threads)

    def __str__(self):
        return "%s_%s_%s_%s" % (self.name, self.time, self.inputSize, self.threads)

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        if type(other) is type(self):
            return self.__dict__ == other.__dict__
        else:
            return False

    def __ne__(self, other):
        return not self.__eq__(other)

    def pretty(self):
        return "%s %s %s %s" % (self.name, self.time, self.inputSize, self.threads)

    def minHeap(self):
        with open(inputDir.join("minHeap.txt")) as f:
            for line in f:
                (name, inputSize, threads, minHeap) = line.strip().split()
                if name == self.name and inputSize == self.inputSize and threads == self.threads:
                    return int(minHeap)
        raise UtilityException("Can't find min heap for " + str(self))

def bidFromStr(s):
    """For the use of shell scripts that pass an already-constructed bid string"""
    return BenchmarkId(*s.split("_"))


class UtilitySpaceId:
    def __init__(self, bids, time):
        self.bids = bids
        self.time = time

    def __str__(self):
        return self.time + "_" + "_".join(map(str, self.bids))


class MeasureConfigLine:
    def __init__(self, benchmark, inputSize, threads, time, minHeap, maxHeap, inc):
        self.b = BenchmarkId(benchmark, time, inputSize, threads)
        self.minHeap = int(minHeap)
        self.maxHeap = int(maxHeap)
        self.inc = int(inc)

    def __str__(self):
        return "%s %s %s %s %d %d %d" % (self.b.name, self.b.inputSize, self.b.threads, self.b.time, self.minHeap, self.maxHeap, self.inc)

    def genInputLines(self):
        l = []
        i = self.minHeap
        while i <= self.maxHeap + self.inc:
            l.append(MeasureInputLine(self.b, i, self.maxHeap + self.inc))
            i += self.inc
        return l

def measureConfigLineFromStr(s):
    return MeasureConfigLine(*s.strip("\n").split())


class MeasureInputLine:
    def __init__(self, bid, heap, maxHeap):
        self.b = bid
        self.heap = int(heap)
        self.maxHeap = int(maxHeap)

    def __str__(self):
        return "%s %d %d" % (self.b.pretty(), self.heap, self.maxHeap)

def measureInputLineFromStr(s):
    l = s.strip("\n").split()
    return MeasureInputLine(BenchmarkId(l[0], l[1], l[2], l[3]), l[4], l[5])


class MeasureResult:
    def __init__(self, h, mean, stddev, itersPerSec, stddevItersPerSec, median, medianItersPerSec):
        self.h = int(h)
        self.mean = float(mean)
        self.stddev = float(stddev)
        self.itersPerSec = float(itersPerSec)
        self.stddevItersPerSec = float(stddevItersPerSec)
        self.median = float(median)
        self.medianItersPerSec = float(medianItersPerSec)

    def __str__(self):
        return "%d %g %g %g %g %g %g" % (self.h, self.mean, self.stddev, self.itersPerSec, self.stddevItersPerSec, self.median, self.medianItersPerSec)

    def append(self, filename):
        with open(filename, "a") as f:
            f.write(str(self) + "\n")

def measureResultFromStr(s):
    return MeasureResult(*s.strip("\n").split())

def loadMeasureResultsFile(filename):
    return map(measureResultFromStr, fileToList(filename))


class MeasureResultsColumns:
    def __init__(self, l):
        self.hs = map(lambda r: r.h, l)
        self.means = map(lambda r: r.mean, l)
        self.stddevs = map(lambda r: r.stddev, l)
        self.itersPerSecs = map(lambda r: r.itersPerSec, l)
        self.stddevItersPerSecs = map(lambda r: r.stddevItersPerSec, l)
        self.medians = map(lambda r: r.median, l)
        self.medianItersPerSecs = map(lambda r: r.medianItersPerSec, l)

    def __str__(self):
        return "\n".join(map(str, [self.hs, self.means, self.stddevs, self.itersPerSecs, self.stddevItersPerSecs, self.medians, self.medianItersPerSecs]))


class UtilityRawResult:
    def __init__(self, hs, means, stddevs, itersPerSecs, stddevItersPerSecs, medians, medianItersPerSecs):
        if not allSameLength(hs, means, stddevs, itersPerSecs, stddevItersPerSecs, medians, medianItersPerSecs):
            raise UtilityException("All args to UtilityRawResult must be the same length")
        self.hs = map(int, hs)
        self.means = map(float, means)
        self.stddevs = map(float, stddevs)
        self.itersPerSecs = map(float, itersPerSecs)
        self.stddevItersPerSecs = map(float, stddevItersPerSecs)
        self.medians = map(float, medians)
        self.medianItersPerSecs = map(float, medianItersPerSecs)

    def __str__(self):
        def join(l):
            return " ".join(map(str, l))

        return "%s %s %s %s %s %s %s" % (join(self.hs), join(self.means), join(self.stddevs), join(self.itersPerSecs), join(self.stddevItersPerSecs), join(self.medians), join(self.medianItersPerSecs))

    def numBenchmarks(self):
        return len(self.hs)

    def append(self, filename):
        with open(filename, "a") as f:
            f.write(str(self) + "\n")

    def isEqualSplit(self, mem):
        return self.hs == evenSplit(mem, len(self.hs))

def utilityRawResultFromStr(s):
    l = s.strip("\n").split()
    fields = 7
    if len(l) % fields != 0:
        raise UtilityException("Not enough fields in string to construct utility raw result")

    size = len(l) / fields
    return UtilityRawResult(*[l[i*size:(i+1)*size] for i in range(fields)])


class UtilityFullResult:
    def __init__(self, raw, measuredU, stddevU, predictedU, differenceU):
        self.raw = raw
        self.measuredU = float(measuredU)
        self.stddevU = float(stddevU)
        self.predictedU = float(predictedU)
        self.differenceU = float(differenceU)

    def __str__(self):
        return "%s %g %g %g %g" % (str(self.raw), self.measuredU, self.stddevU, self.predictedU, self.differenceU)

    def append(self, f):
        f.write(str(self) + "\n")

    def isEqualSplit(self, mem):
        return self.raw.isEqualSplit(mem)

def utilityFullResultFromStr(s):
    l = s.strip("\n").split()
    return UtilityFullResult(utilityRawResultFromStr(" ".join(l[:-4])), *l[-4:])

def loadUtilityFullResultsFile(filename):
    return map(utilityFullResultFromStr, fileToList(filename))


class UtilityResultsColumns:
    def __init__(self, l):
        self.hs = map(list, zip(*map(lambda r: r.raw.hs, l)))
        self.means = map(list, zip(*map(lambda r: r.raw.means, l)))
        self.stddevs = map(list, zip(*map(lambda r: r.raw.stddevs, l)))
        self.itersPerSecs = map(list, zip(*map(lambda r: r.raw.itersPerSecs, l)))
        self.stddevItersPerSecs = map(list, zip(*map(lambda r: r.raw.stddevItersPerSecs, l)))
        self.medians = map(list, zip(*map(lambda r: r.raw.medians, l)))
        self.medianItersPerSecs = map(list, zip(*map(lambda r: r.raw.medianItersPerSecs, l)))
        self.measuredU = map(lambda r: r.measuredU, l)
        self.stddevU = map(lambda r: r.stddevU, l)
        self.predictedU = map(lambda r: r.predictedU, l)
        self.differenceU = map(lambda r: r.differenceU, l)

    def __str__(self):
        return "\n".join(map(str, [self.hs, self.means, self.stddevs, self.itersPerSecs, self.stddevItersPerSecs, self.medians, self.medianItersPerSecs, self.measuredU, self.stddevU, self.predictedU, self.differenceU]))


class DefaultUtilityRawResult:
    def __init__(self, means, stddevs, itersPerSecs, stddevItersPerSecs, medians, medianItersPerSecs):
        if not allSameLength(means, stddevs, itersPerSecs, stddevItersPerSecs, medians, medianItersPerSecs):
            raise UtilityException("All args to DefaultUtilityRawResult must be the same length")
        self.means = map(float, means)
        self.stddevs = map(float, stddevs)
        self.itersPerSecs = map(float, itersPerSecs)
        self.stddevItersPerSecs = map(float, stddevItersPerSecs)
        self.medians = map(float, medians)
        self.medianItersPerSecs = map(float, medianItersPerSecs)

    def __str__(self):
        def join(l):
            return " ".join(map(str, l))

        return "%s %s %s %s %s %s" % (join(self.means), join(self.stddevs), join(self.itersPerSecs), join(self.stddevItersPerSecs), join(self.medians), join(self.medianItersPerSecs))

    def numBenchmarks(self):
        return len(self.means)

    def append(self, filename):
        with open(filename, "a") as f:
            f.write(str(self) + "\n")

def defaultUtilityRawResultFromStr(s):
    l = s.strip("\n").split()
    fields = 6
    if len(l) % fields != 0:
        raise UtilityException("Not enough fields in string to construct default utility raw result")

    size = len(l) / fields
    return DefaultUtilityRawResult(*[l[i*size:(i+1)*size] for i in range(fields)])


class DefaultUtilityFullResult:
    def __init__(self, raw, U, stddevU):
        self.raw = raw
        self.U = float(U)
        self.stddevU = float(stddevU)

    def __str__(self):
        return "%s %g %g" % (str(self.raw), self.U, self.stddevU)

    def append(self, f):
        f.write(str(self) + "\n")

def defaultUtilityFullResultFromStr(s):
    l = s.strip("\n").split()
    return DefaultUtilityFullResult(defaultUtilityRawResultFromStr(" ".join(l[:-2])), *l[-2:])


class ValStddev:
    def __init__(self, val, stddev):
        self.val = float(val)
        self.stddev = float(stddev)

    def __str__(self):
        return "%g %g" % (self.val, self.stddev)

    def pretty(self):
        return "(%g +- %g)" % (self.val, self.stddev)

    def writeTo(self, filename):
        writeFile(filename, self)

    def __div__(self, other):
        """Divide the vals and propagate the stddevs"""
        val = self.val / other.val
        stddev = val * math.sqrt(((self.stddev / self.val) ** 2) + ((other.stddev / other.val) ** 2))
        return ValStddev(val, stddev)


def valStddevFromStr(s):
    l = s.strip("\n").split()
    return ValStddev(*l)

def readMaybeValStddev(filename):
    if os.path.exists(filename):
        return valStddevFromStr(firstLineOfFile(filename))
    else:
        return None


class UtilitySummary:
    def __init__(self, bids, totalMem, model, predictionAccuracy, euclideanDistance, practicalAccuracy, practicalVsEqual, defaultRatioPredicted, defaultRatioEqual, defaultRatioMaxMeasured, defaultRatioMaxPredictedMeasured):
        self.bids = bids
        self.totalMem = int(totalMem)
        self.model = model
        self.predictionAccuracy = predictionAccuracy
        self.euclideanDistance = float(euclideanDistance)
        self.practicalAccuracy = practicalAccuracy
        self.practicalVsEqual = practicalVsEqual
        self.defaultRatioPredicted = defaultRatioPredicted
        self.defaultRatioEqual = defaultRatioEqual
        self.defaultRatioMaxMeasured = defaultRatioMaxMeasured
        self.defaultRatioMaxPredictedMeasured = defaultRatioMaxPredictedMeasured

    def toFields(self):
        def maybe(d):
            if d:
                return d.pretty()
            else:
                return "(No data)"

        return [", ".join(map(lambda b: b.pretty(), self.bids)), str(self.totalMem), self.model, self.predictionAccuracy.pretty(), str(self.euclideanDistance), self.practicalAccuracy.pretty(), maybe(self.practicalVsEqual), maybe(self.defaultRatioPredicted), maybe(self.defaultRatioEqual), maybe(self.defaultRatioMaxMeasured), maybe(self.defaultRatioMaxPredictedMeasured)]

    def allBidsEqual(self):
        for b in self.bids[1:]:
            if b != self.bids[0]:
                return False
        return True


class UtilityAggregatedResults:
    def __init__(self, summaries):
        self.model = summaries[0].model
        self.summaries = summaries
        self.predictionAccuracy = self.averagePropagate(map(lambda s: s.predictionAccuracy, self.summaries))
        self.euclideanDistance = self.averageRaw(map(lambda s: s.euclideanDistance, self.summaries))
        self.practicalAccuracy = self.averagePropagate(map(lambda s: s.practicalAccuracy, self.summaries))
        self.practicalVsEqual = self.averagePropagate(map(lambda s: s.practicalVsEqual, self.summaries))
        self.defaultRatioPredicted = self.averagePropagate(map(lambda s: s.defaultRatioPredicted, self.summaries))
        self.defaultRatioEqual = self.averagePropagate(map(lambda s: s.defaultRatioEqual, self.summaries))
        self.defaultRatioMaxMeasured = self.averagePropagate(map(lambda s: s.defaultRatioMaxMeasured, self.summaries))
        self.defaultRatioMaxPredictedMeasured = self.averagePropagate(map(lambda s: s.defaultRatioMaxPredictedMeasured, self.summaries))

    def averageRaw(self, l):
        return ValStddev(average(l), stddev(l))

    def averagePropagate(self, l):
        l = filter(lambda s: s is not None, l)
        if len(l) > 0:
            val = average(map(lambda s: s.val, l))
            stddev = (math.sqrt(sum(map(lambda s: s.stddev**2, l))))/float(len(l))
            return ValStddev(val, stddev)
        else:
            return None


class ResultsDir(Dir):
    def benchmarksFile(self):
        return self.join("benchmarks.txt")

    def runsFile(self):
        return self.join("runs.txt")

    def timestampsFile(self):
        return self.join("timestamps.txt")

    def paramsFile(self, bid, modelName, statistic="mean"):
        return self.join(str(bid) + "_fit_" + modelName + "_" + statistic + ".txt")

    def parseBenchmarksFile(self):
        """Return the whole benchmarks file as a list of bids"""
        l = []
        with open(fileMustExist(self.benchmarksFile())) as f:
            for line in f:
                l.append(BenchmarkId(*line.split()))
        return l

    def writeBenchmarksFile(self, bids):
        with open(self.benchmarksFile(), "w") as f:
            for b in bids:
                f.write(b.pretty() + "\n")

    def runs(self):
        return int(firstLineOfFile(self.runsFile()))

    def writeRuns(self, runs):
        writeFile(self.runsFile(), runs)

    def machineInfo(self):
        if subprocess.call([thisDir.join("..", "machineInfo.sh"), str(self)]) != 0:
            raise UtilityException("Can't get machine info")

    def prettyHeap(self, heap, maxHeap):
        return "%0*d" % (len(str(maxHeap)), heap)

    def prettyRun(self, run, runs):
        return "%0*d" % (len(str(runs - 1)), run)

    def timestampStartLocal(self, *args):
        with open(self.timestampsFile(), "w") as f:
            f.write("Local run on %s\n" % me())
            f.write("Started: %s\n" % nowPretty())
            e = estimateRuntime(*args)
            f.write(e + "\n")
            print e
        return nowStamp()

    def timestampEndLocal(self, starttime):
        endtime = nowStamp()
        with open(self.timestampsFile(), "a") as f:
            f.write("Completed: %s\n" % nowPretty())
            f.write("Runtime: %d s\n" % (endtime - starttime))

    def timestampStartRemote(self, *args):
        with open(self.timestampsFile(), "w") as f:
            f.write("Remote run started by %s\n" % me())
            f.write("Started: %s\n" % nowPretty())
            e = estimateRuntime(*args)
            f.write(e + "\n")
            print e


class MeasureDir(ResultsDir):

    def configFile(self):
        return self.join("config.txt")

    def pullInConfigFile(self, filename):
        shutil.copy(filename, self.configFile())

    def inputFile(self):
        return self.join("input.txt")

    def pointIterationsFile(self, pointID):
        return self.join(pointID + "_iterations.txt")

    def pointRawFile(self, pointID, run, runs):
        return self.join(pointID + "_run" + self.prettyRun(run, runs) + ".txt")

    def hostResultsFileName(self, s1, s2):
        return self.join(s1 + "_" + s2 + "_results.txt")

    def hostResultsFile(self, bid, host):
        return self.hostResultsFileName(str(bid), host)

    def resultsFile(self, bid):
        return self.join(str(bid) + "_results.txt")

    def individualIterationsGraphName(self, s):
        return self.join(s + "_iterations.pdf")

    def individualIterationsGraph(self, bid):
        return self.individualIterationsGraphName(str(bid))

    def overallIterationsGraph(self):
        return self.join("iterations.pdf")

    def globIterationsGraphs(self):
        return glob.glob(self.individualIterationsGraphName("*"))

    def individualThroughputGraphName(self, s1, s2):
        return self.join(s1 + "_throughput_" + s2 + ".pdf")

    def individualThroughputGraph(self, bid, modelName):
        return self.individualThroughputGraphName(str(bid), modelName)

    def cleanedIndividualThroughputGraphName(self, s1, s2):
        return self.join(s1 + "_throughputCleaned_" + s2 + ".pdf")

    def cleanedIndividualThroughputGraph(self, bid, modelName):
        return self.cleanedIndividualThroughputGraphName(str(bid), modelName)

    def overallThroughputGraph(self):
        return self.join("throughput.pdf")

    def globThroughputGraphs(self):
        return glob.glob(self.individualThroughputGraphName("*", "*"))

    def overallCleanedThroughputGraph(self):
        return self.join("throughputCleaned.pdf")

    def globCleanedThroughputGraphs(self):
        return glob.glob(self.cleanedIndividualThroughputGraphName("*", "*"))

    def pointID(self, bid, heap, maxHeap):
        return str(bid) + "_" + self.prettyHeap(heap, maxHeap)

    def genInputFromConfig(self):
        """Returns inputLines, and also writes the input and benchmarks files"""
        bids = []
        inputLines = []

        with open(self.configFile()) as f:
            for line in f:
                c = measureConfigLineFromStr(line)
                bids.append(c.b)
                inputLines += c.genInputLines()

        self.writeBenchmarksFile(bids)

        with open(self.inputFile(), "w") as f:
            for line in inputLines:
                f.write(str(line) + "\n")

        return inputLines

    def gatherResults(self, bid):
        f = self.resultsFile(bid)
        catAndSort(glob.glob(self.hostResultsFileName(str(bid), "*")), f)
        return f

    def readHeapSizes(self, bid):
        """Read the heap sizes from an individual benchmark's results (the first column)"""
        return MeasureResultsColumns(loadMeasureResultsFile(self.resultsFile(bid))).hs

def measureDirName():
    return "measure_%s_%s" % (me(), now())


class UtilitySpaceDir(ResultsDir):

    def measurepointsFile(self):
        return self.join("input.txt")

    def totalMemFile(self):
        return self.join("maxMemory.txt")

    def runTimeFile(self):
        return self.join("time.txt")

    def maxCalculatedFile(self, modelName):
        return self.join("maxCalculated_" + modelName + ".txt")

    def maxMeasuredFile(self, modelName):
        return self.join("maxMeasured_" + modelName + ".txt")

    def maxPredictedFile(self, modelName):
        return self.join("maxPredicted_" + modelName + ".txt")

    def pointRawFile(self, pointID, run, runs, bnum):
        return self.join(pointID + "_b" + str(bnum) + "_run" + self.prettyRun(run, runs) + ".txt")

    def pointIterationsFile(self, pointID, bnum):
        return self.join(pointID + "_iterations_b" + str(bnum) + ".txt")

    def hostRawFile(self, host):
        return self.join("rawResults_" + host + ".txt")

    def rawResultsFile(self):
        return self.join("rawResults.txt")

    def resultsFile(self, modelName):
        return self.join("results_" + modelName + ".txt")

    def euclideanFile(self, modelName):
        return self.join("euclidean_" + modelName + ".txt")

    def predictionAccuracyFile(self, modelName):
        return self.join("predictionAccuracy_" + modelName + ".txt")

    def practicalAccuracyFile(self, modelName):
        return self.join("practicalAccuracy_" + modelName + ".txt")

    def practicalVsEqualFile(self, modelName):
        return self.join("practicalVsEqual_" + modelName + ".txt")

    def defaultRatioPredictedFile(self, modelName):
        return self.join("defaultRatio_predicted_" + modelName + ".txt")

    def defaultRatioEqualSplitFile(self, modelName):
        return self.join("defaultRatio_equal_" + modelName + ".txt")

    def defaultRatioMaxMeasuredFile(self, modelName):
        return self.join("defaultRatio_maxMeasured_" + modelName + ".txt")

    def defaultRatioMaxPredictedMeasuredFile(self, modelName):
        return self.join("defaultRatio_maxPredictedMeasured_" + modelName + ".txt")

    def individualGraphName(self, modelName, name, cleaned=False):
        if cleaned:
            return self.join("utilitySpaceCleaned_%s_%s.pdf" % (modelName, name))
        else:
            return self.join("utilitySpace_%s_%s.pdf" % (modelName, name))

    def measuredGraph(self, modelName, cleaned=False):
        return self.individualGraphName(modelName, "measured", cleaned)

    def measuredGraphTopDown(self, modelName, cleaned=False):
        return self.individualGraphName(modelName, "measuredTopDown", cleaned)

    def predictedGraph(self, modelName, cleaned=False):
        return self.individualGraphName(modelName, "predicted", cleaned)

    def predictedGraphTopDown(self, modelName, cleaned=False):
        return self.individualGraphName(modelName, "predictedTopDown", cleaned)

    def differenceGraph(self, modelName, cleaned=False):
        return self.individualGraphName(modelName, "difference", cleaned)

    def differenceGraphTopDown(self, modelName, cleaned=False):
        return self.individualGraphName(modelName, "differenceTopDown", cleaned)

    def modelGraph(self, modelName, cleaned=False):
        if cleaned:
            return self.join("utilitySpaceCleaned_results_" + modelName + ".pdf")
        else:
            return self.join("utilitySpace_results_" + modelName + ".pdf")

    def resultsGraph(self, cleaned=False):
        if cleaned:
            return self.join("utilitySpaceCleaned_results.pdf")
        else:
            return self.join("utilitySpace_results.pdf")

    def globModelGraphs(self, modelName, cleaned=False):
        return glob.glob(self.individualGraphName(modelName, "*", cleaned))

    def globModelResultsGraphs(self, cleaned=False):
        return glob.glob(self.modelGraph("*", cleaned))

    def pointID(self, heaps, maxHeaps):
        return "_".join(map(lambda (h, m): self.prettyHeap(h, m), zip(heaps, maxHeaps)))

    def totalMem(self):
        return int(firstLineOfFile(self.totalMemFile()))

    def writeTotalMem(self, totalMem):
        writeFile(self.totalMemFile(), totalMem)

    def runTime(self):
        return firstLineOfFile(self.runTimeFile())

    def writeRunTime(self, runTime):
        writeFile(self.runTimeFile(), runTime)

    def gatherResults(self):
        f = self.rawResultsFile()
        catAndSort(glob.glob(self.hostRawFile("*")), f)
        return f

    def writeMaxNFile(self, filename, r):
        """r is a UtilityFullResult"""
        with open(filename, "w") as f:
            r.append(f)

    def writeMaxMeasuredFile(self, modelName, r):
        self.writeMaxNFile(self.maxMeasuredFile(modelName), r)

    def writeMaxPredictedFile(self, modelName, r):
        self.writeMaxNFile(self.maxPredictedFile(modelName), r)

    def readEuclideanDistance(self, modelName):
        return float(firstLineOfFile(self.euclideanFile(modelName)))

    def writeEuclideanFile(self, modelName, p1, p2):
        writeFile(self.euclideanFile(modelName), euclideanDistance(p1, p2))

    def readPredictionAccuracy(self, modelName):
        return valStddevFromStr(firstLineOfFile(self.predictionAccuracyFile(modelName)))

    def writePredictionAccuracyFile(self, modelName, maxMeasured, maxMeasuredStddev, maxPredicted):
        ValStddev(float(maxMeasured) / float(maxPredicted), float(maxMeasuredStddev) / float(maxPredicted)).writeTo(self.predictionAccuracyFile(modelName))

    def readPracticalAccuracy(self, modelName):
        return valStddevFromStr(firstLineOfFile(self.practicalAccuracyFile(modelName)))

    def writePracticalAccuracyFile(self, modelName, maxPredicted_measured, maxPredicted_measuredStddev, maxMeasured, maxMeasuredStddev):
        (ValStddev(maxPredicted_measured, maxPredicted_measuredStddev) / ValStddev(maxMeasured, maxMeasuredStddev)).writeTo(self.practicalAccuracyFile(modelName))

    def readPracticalVsEqual(self, modelName):
        return readMaybeValStddev(self.practicalVsEqualFile(modelName))

    def writePracticalVsEqual(self, modelName, measured, measuredStddev, equal, equalStddev):
        (ValStddev(measured, measuredStddev) / ValStddev(equal, equalStddev)).writeTo(self.practicalVsEqualFile(modelName))

    def writeDefaultRatio(self, modelName, outfile, U, stddevU):
        d = DefaultUtilityDir(str(self))
        if os.path.exists(d.resultsFile(modelName)):
            r = defaultUtilityFullResultFromStr(firstLineOfFile(d.resultsFile(modelName)))
            (ValStddev(U, stddevU) / ValStddev(r.U, r.stddevU)).writeTo(outfile)

    def readDefaultRatioPredicted(self, modelName):
        return readMaybeValStddev(self.defaultRatioPredictedFile(modelName))

    def writeDefaultRatioPredicted(self, modelName, U):
        self.writeDefaultRatio(modelName, self.defaultRatioPredictedFile(modelName), U, 0)

    def readDefaultRatioEqualSplit(self, modelName):
        return readMaybeValStddev(self.defaultRatioEqualSplitFile(modelName))

    def writeDefaultRatioEqualSplit(self, modelName, U, stddevU):
        self.writeDefaultRatio(modelName, self.defaultRatioEqualSplitFile(modelName), U, stddevU)

    def readDefaultRatioMaxMeasured(self, modelName):
        return readMaybeValStddev(self.defaultRatioMaxMeasuredFile(modelName))

    def writeDefaultRatioMaxMeasured(self, modelName, U, stddevU):
        self.writeDefaultRatio(modelName, self.defaultRatioMaxMeasuredFile(modelName), U, stddevU)

    def readDefaultRatioMaxPredictedMeasured(self, modelName):
        return readMaybeValStddev(self.defaultRatioMaxPredictedMeasuredFile(modelName))

    def writeDefaultRatioMaxPredictedMeasured(self, modelName, U, stddevU):
        self.writeDefaultRatio(modelName, self.defaultRatioMaxPredictedMeasuredFile(modelName), U, stddevU)

    def checkCanUseUtilityModel(self, modelName, bids, dirs):
        """Whether parameters for the model are available in the given measure runs to be copied into this utility space run"""

        for (b, d) in zip(bids, dirs):
            if not os.path.exists(d.paramsFile(b, modelName)):
                raise UtilityException("Benchmark '%s' in '%s' has no parameters for the '%s' utility model" % (str(b), str(d), modelName))

    def pullInParamsFiles(self, modelName, bids, dirs):
        for (b, d) in zip(bids, dirs):
            shutil.copy(d.paramsFile(b, modelName), self.paramsFile(b, modelName))

    def haveParamsForUtilityModel(self, modelName, bids):
        """Whether parameters for this model are available in the utility dir"""
        for b in bids:
            if not os.path.exists(self.paramsFile(b, modelName)):
                return False
        return True

    def maxHeaps(self, measurepoints):
        return map(max, zip(*measurepoints))

    def summarise(self, model):
        return UtilitySummary(self.parseBenchmarksFile(),
                              self.totalMem(),
                              model,
                              self.readPredictionAccuracy(model),
                              self.readEuclideanDistance(model),
                              self.readPracticalAccuracy(model),
                              self.readPracticalVsEqual(model),
                              self.readDefaultRatioPredicted(model),
                              self.readDefaultRatioEqualSplit(model),
                              self.readDefaultRatioMaxMeasured(model),
                              self.readDefaultRatioMaxPredictedMeasured(model))


def utilitySpaceDirName(bids, time):
    return "utilitySpace_%s_%s_%s" % (UtilitySpaceId(bids, time), me(), now())

def globUtilitySpaceDirs(d):
    return filter(os.path.isdir, glob.glob(d.join("utilitySpace_*")))



class DefaultUtilityDir(ResultsDir):

    def totalMemFile(self):
        return self.join("maxMemory.txt")

    def writeTotalMem(self, totalMem):
        writeFile(self.totalMemFile(), totalMem)

    def rawFile(self, run, runs, bnum):
        return self.join("defaultUtility_raw_b" + str(bnum) + "_run" + self.prettyRun(run, runs) + ".txt")

    def iterationsFile(self, bnum):
        return self.join("defaultUtility_iterations_b" + str(bnum) + ".txt")

    def rawResultsFile(self):
        return self.join("defaultUtility_rawResults.txt")

    def resultsFile(self, model):
        return self.join("defaultUtility_results_%s.txt" % model)

    def globResultsFiles(self):
        return glob.glob(self.resultsFile("*"))


def defaultUtilityDirName(bids, time):
    return "defaultUtility_%s_%s_%s" % (UtilitySpaceId(bids, time), me(), now())

def globDefaultUtilityDirs(d):
    return filter(os.path.isdir, glob.glob(d.join("defaultUtility_*")))

def allDefaultUtilityDirs():
    return map(DefaultUtilityDir, globDefaultUtilityDirs(outputDir))


class UtilityAggregateDir(ResultsDir):
    def __init__(self, d, dirs):
        Dir.__init__(self, d)
        self.dirs = sorted(dirs, key=str)

    def resultsGraph(self, cleaned=False):
        if cleaned:
            return self.join("utilitySpaceCleaned_results.pdf")
        else:
            return self.join("utilitySpace_results.pdf")

    def utilityResultsTable(self):
        return self.join("utilitySpace_results.txt")

    def improvementGraphModel(self, model):
        return self.join("utilitySpace_improvement_" + model + ".pdf")

    def improvementGraph(self):
        return self.join("utilitySpace_improvement.pdf")

    def mergeImprovementGraphs(self):
        mergePdfs(glob.glob(self.improvementGraphModel("*")), self.improvementGraph())

    def allUtilityResultsGraphs(self, cleaned=False):
        return filter(os.path.exists, map(lambda d: d.resultsGraph(cleaned), self.dirs))

    def gatherResults(self):
        mergePdfs(self.allUtilityResultsGraphs(), self.resultsGraph())
        mergePdfs(self.allUtilityResultsGraphs(cleaned=True), self.resultsGraph(cleaned=True))


def fileMustExist(f):
    f = os.path.realpath(f)
    if not os.path.exists(f):
        sys.stderr.write("File '" + f + "' does not exist.\n")
        sys.exit(1)
    else:
        return f

def dirMustExist(d):
    d = os.path.realpath(d)
    if not os.path.isdir(d):
        sys.stderr.write("Directory '" + d + "' does not exist.\n")
        sys.exit(1)
    else:
        return d

def firstLineOfFile(filename):
    with open(fileMustExist(filename)) as f:
        return f.readline().strip()

def fileToList(filename, conv=None):
    l = []
    with open(fileMustExist(filename)) as f:
        for line in f:
            l.append(line.strip("\n"))
    if conv:
        return map(conv, l)
    else:
        return l

def writeFile(filename, s):
    with open(filename, "w") as f:
        f.write(str(s) + "\n")

def catAndSort(files, outfile, key=lambda s: int(s.split()[0])):
    l = []
    for filename in files:
        with open(filename) as f:
            for line in f:
                l.append(line)
    l.sort(key=key)

    with open(outfile, "w") as f:
        for line in l:
            f.write(line)

def mergePdfs(files, outfile):
    if len(files) > 1:
        subprocess.call(["pdfunite"] + sorted(files) + [outfile])
    elif len(files) == 1:
        shutil.copy(files[0], outfile)


def emailConfigFile():
    return thisDir.join("..", "email.conf")

def me():
    return getpass.getuser() + "@" + platform.node()

def now():
    return datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

def nowPretty():
    return datetime.datetime.now().strftime("%Y %m %d, %H:%M:%S")

def nowStamp():
    return int(time.time())

def euclideanDistance(p1, p2):
    return math.sqrt(sum(map(lambda (a, b): (a - b)**2, zip(p1, p2))))

def estimateRuntime(machines, time, *multipliers):
    s = ""
    seconds = time * reduce(operator.mul, multipliers, 1)

    if machines == 1:
        length = datetime.timedelta(seconds=seconds)
        completiontime = datetime.datetime.now() + length

        s += "Estimated runtime: %s (%d seconds)\n" % (str(length), seconds)
        s += "Estimated completion at: %s\n" % str(completiontime)
    else:
        runtime = seconds/machines
        length = datetime.timedelta(seconds=runtime)
        completiontime = datetime.datetime.now() + length

        s += "Estimated runtime: %s (%d seconds) using %d machines (would take %s (%d seconds) on one machine)\n" % (length, runtime, machines, str(datetime.timedelta(seconds=seconds)), seconds)
        s += "Estimated completion at: %s" % str(completiontime)

    return s


def allSameLength(*ls):
    if len(ls) == 0 or len(ls) == 1:
        return True
    else:
        i = len(ls[0])
        for l in ls[1:]:
            if len(l) != i:
                return False
        return True

def timeToSeconds(s):
    """Convert a sleep-compatible time format to seconds"""
    multiplier = 1
    time = 0

    if not s[-1].isalpha():
        s += "s"

    if s.endswith("s"):
        multiplier = 1
    elif s.endswith("m"):
        multiplier = 60
    elif s.endswith("h"):
        multiplier = 60 * 60
    elif s.endswith("d"):
        multiplier = 60 * 60 * 24

    t = int(s[:-1])
    return t * multiplier

def dacapo():
    return thisDir.join("..", "dacapo-9.12-bach.jar")

def checkDaCapo():
    if subprocess.call([thisDir.join("..", "checkDacapo.sh")]) != 0:
        raise UtilityException("Can't find DaCapo")

def benchmarkIterations(filename):
    i = 0
    with open(filename) as f:
        for line in f:
            if re.search("[0-9]+ msec", line):
                i += 1
    return i


def evenSplit(totalMem, num):
    val = int(round(float(totalMem)/float(num)))
    val = [val] * num
    if sum(val) != totalMem:
        val[-1] += (totalMem - sum(val))
    return val

def average(l):
    return float(sum(l)) / float(len(l))

def stddev(l):
    avg = average(l)
    l2 = map(lambda i: (i - avg) ** 2, l)
    return math.sqrt(float(sum(l2))/float(len(l2)))

def median(l):
    l = sorted(l)
    m = len(l)/2

    if len(l) % 2 == 0:
        # Even list, take average of middle two
        return (l[m - 1] + l[m]) / 2.0
    else:
        # Odd list, take middle element
        return float(l[m])

def fitStraightLine(xdata, ydata):
    """Returns (a, b) such that y = a + bx"""
    l = zip(xdata, ydata)

    xbar = 0.0
    ybar = 0.0
    xybar = 0.0
    x2bar = 0.0

    for (x, y) in l:
        xbar += x
        ybar += y
        xybar += (x * y)
        x2bar += (x * x)

    xbar = xbar / len(l)
    ybar = ybar / len(l)
    xybar = xybar / len(l)
    x2bar = x2bar / len(l)

    b = (xybar - (xbar * ybar)) / (x2bar - (xbar * xbar))
    a = ybar - (b * xbar)

    return (a, b)
