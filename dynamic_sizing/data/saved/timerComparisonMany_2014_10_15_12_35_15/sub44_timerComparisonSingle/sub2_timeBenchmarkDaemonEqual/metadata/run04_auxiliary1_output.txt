economemd
    total memory: 2760 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub44_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 20271: heap size 9 MB, throughput 0.991776
Clients: 1
Client 20271 has a minimum heap size of 276 MB
Reading from 20270: heap size 9 MB, throughput 0.992196
Clients: 2
Client 20270 has a minimum heap size of 276 MB
Reading from 20271: heap size 9 MB, throughput 0.974308
Reading from 20270: heap size 9 MB, throughput 0.97469
Reading from 20271: heap size 9 MB, throughput 0.967779
Reading from 20270: heap size 9 MB, throughput 0.965637
Reading from 20270: heap size 9 MB, throughput 0.944825
Reading from 20271: heap size 9 MB, throughput 0.95876
Reading from 20271: heap size 11 MB, throughput 0.955536
Reading from 20270: heap size 11 MB, throughput 0.974074
Reading from 20270: heap size 11 MB, throughput 0.980569
Reading from 20271: heap size 11 MB, throughput 0.982814
Reading from 20271: heap size 17 MB, throughput 0.956264
Reading from 20270: heap size 17 MB, throughput 0.875889
Reading from 20270: heap size 17 MB, throughput 0.613532
Reading from 20271: heap size 17 MB, throughput 0.574936
Reading from 20270: heap size 30 MB, throughput 0.939385
Reading from 20271: heap size 30 MB, throughput 0.944285
Reading from 20270: heap size 31 MB, throughput 0.949072
Reading from 20271: heap size 31 MB, throughput 0.776308
Reading from 20270: heap size 35 MB, throughput 0.364939
Reading from 20271: heap size 33 MB, throughput 0.353421
Reading from 20270: heap size 48 MB, throughput 0.921706
Reading from 20271: heap size 46 MB, throughput 0.802217
Reading from 20271: heap size 47 MB, throughput 0.877983
Reading from 20270: heap size 50 MB, throughput 0.410284
Reading from 20270: heap size 65 MB, throughput 0.82993
Reading from 20270: heap size 72 MB, throughput 0.811807
Reading from 20271: heap size 50 MB, throughput 0.358723
Reading from 20271: heap size 70 MB, throughput 0.816592
Reading from 20270: heap size 73 MB, throughput 0.211876
Reading from 20270: heap size 101 MB, throughput 0.73661
Reading from 20271: heap size 71 MB, throughput 0.294381
Reading from 20271: heap size 95 MB, throughput 0.772787
Reading from 20271: heap size 95 MB, throughput 0.695038
Reading from 20270: heap size 102 MB, throughput 0.311175
Reading from 20270: heap size 132 MB, throughput 0.779124
Reading from 20270: heap size 132 MB, throughput 0.778005
Reading from 20271: heap size 97 MB, throughput 0.342088
Reading from 20270: heap size 134 MB, throughput 0.845462
Reading from 20271: heap size 124 MB, throughput 0.779075
Reading from 20271: heap size 128 MB, throughput 0.746257
Reading from 20271: heap size 130 MB, throughput 0.691323
Reading from 20271: heap size 136 MB, throughput 0.762962
Reading from 20270: heap size 139 MB, throughput 0.183647
Reading from 20270: heap size 169 MB, throughput 0.532984
Reading from 20270: heap size 174 MB, throughput 0.750983
Reading from 20270: heap size 176 MB, throughput 0.642997
Reading from 20271: heap size 139 MB, throughput 0.158433
Reading from 20271: heap size 177 MB, throughput 0.611563
Reading from 20271: heap size 180 MB, throughput 0.727521
Reading from 20270: heap size 178 MB, throughput 0.218962
Reading from 20271: heap size 182 MB, throughput 0.76635
Reading from 20271: heap size 189 MB, throughput 0.659087
Reading from 20270: heap size 219 MB, throughput 0.772913
Reading from 20271: heap size 192 MB, throughput 0.902577
Reading from 20270: heap size 223 MB, throughput 0.937826
Reading from 20270: heap size 225 MB, throughput 0.873378
Reading from 20270: heap size 233 MB, throughput 0.625073
Reading from 20270: heap size 238 MB, throughput 0.783625
Reading from 20270: heap size 238 MB, throughput 0.75265
Reading from 20270: heap size 245 MB, throughput 0.749198
Reading from 20270: heap size 245 MB, throughput 0.759175
Reading from 20271: heap size 197 MB, throughput 0.415099
Reading from 20270: heap size 247 MB, throughput 0.701524
Reading from 20271: heap size 247 MB, throughput 0.704613
Reading from 20270: heap size 252 MB, throughput 0.703397
Reading from 20271: heap size 251 MB, throughput 0.673847
Reading from 20270: heap size 259 MB, throughput 0.757542
Reading from 20271: heap size 257 MB, throughput 0.719912
Reading from 20270: heap size 262 MB, throughput 0.622412
Reading from 20271: heap size 258 MB, throughput 0.775834
Reading from 20271: heap size 264 MB, throughput 0.751015
Reading from 20271: heap size 266 MB, throughput 0.799716
Reading from 20270: heap size 269 MB, throughput 0.912214
Reading from 20271: heap size 270 MB, throughput 0.811346
Reading from 20270: heap size 270 MB, throughput 0.798643
Reading from 20271: heap size 272 MB, throughput 0.52368
Reading from 20271: heap size 279 MB, throughput 0.578249
Reading from 20270: heap size 274 MB, throughput 0.610452
Reading from 20271: heap size 282 MB, throughput 0.691683
Reading from 20271: heap size 289 MB, throughput 0.616538
Reading from 20270: heap size 277 MB, throughput 0.095052
Reading from 20270: heap size 317 MB, throughput 0.601641
Reading from 20271: heap size 290 MB, throughput 0.473646
Reading from 20270: heap size 320 MB, throughput 0.906087
Reading from 20271: heap size 326 MB, throughput 0.611557
Reading from 20270: heap size 324 MB, throughput 0.902364
Reading from 20271: heap size 331 MB, throughput 0.782702
Reading from 20270: heap size 325 MB, throughput 0.81826
Reading from 20271: heap size 337 MB, throughput 0.85153
Reading from 20270: heap size 326 MB, throughput 0.732067
Reading from 20271: heap size 337 MB, throughput 0.72174
Reading from 20270: heap size 327 MB, throughput 0.570514
Reading from 20270: heap size 334 MB, throughput 0.642126
Reading from 20270: heap size 334 MB, throughput 0.631588
Reading from 20271: heap size 344 MB, throughput 0.85985
Reading from 20271: heap size 344 MB, throughput 0.690069
Reading from 20271: heap size 345 MB, throughput 0.761164
Reading from 20271: heap size 348 MB, throughput 0.691486
Reading from 20271: heap size 351 MB, throughput 0.707598
Reading from 20271: heap size 353 MB, throughput 0.662625
Equal recommendation: 1380 MB each
Reading from 20270: heap size 342 MB, throughput 0.956496
Reading from 20271: heap size 360 MB, throughput 0.940709
Reading from 20271: heap size 360 MB, throughput 0.966874
Reading from 20270: heap size 343 MB, throughput 0.967049
Reading from 20270: heap size 348 MB, throughput 0.97675
Reading from 20271: heap size 364 MB, throughput 0.978884
Reading from 20270: heap size 350 MB, throughput 0.966929
Reading from 20271: heap size 367 MB, throughput 0.978444
Reading from 20270: heap size 354 MB, throughput 0.982167
Reading from 20271: heap size 364 MB, throughput 0.976893
Reading from 20270: heap size 355 MB, throughput 0.976413
Reading from 20271: heap size 369 MB, throughput 0.975662
Reading from 20270: heap size 354 MB, throughput 0.97496
Reading from 20271: heap size 369 MB, throughput 0.975364
Equal recommendation: 1380 MB each
Reading from 20270: heap size 357 MB, throughput 0.977062
Reading from 20271: heap size 371 MB, throughput 0.974066
Reading from 20270: heap size 357 MB, throughput 0.967861
Reading from 20271: heap size 371 MB, throughput 0.971289
Reading from 20270: heap size 358 MB, throughput 0.967905
Reading from 20271: heap size 372 MB, throughput 0.977716
Reading from 20270: heap size 361 MB, throughput 0.975198
Reading from 20271: heap size 376 MB, throughput 0.974276
Reading from 20270: heap size 362 MB, throughput 0.963155
Reading from 20271: heap size 377 MB, throughput 0.714772
Reading from 20270: heap size 366 MB, throughput 0.971914
Reading from 20271: heap size 423 MB, throughput 0.96957
Equal recommendation: 1380 MB each
Reading from 20270: heap size 367 MB, throughput 0.695164
Reading from 20270: heap size 411 MB, throughput 0.827163
Reading from 20271: heap size 424 MB, throughput 0.985655
Reading from 20270: heap size 411 MB, throughput 0.898064
Reading from 20271: heap size 426 MB, throughput 0.877799
Reading from 20270: heap size 412 MB, throughput 0.893214
Reading from 20271: heap size 426 MB, throughput 0.837012
Reading from 20271: heap size 432 MB, throughput 0.857667
Reading from 20270: heap size 413 MB, throughput 0.948854
Reading from 20271: heap size 433 MB, throughput 0.985094
Reading from 20270: heap size 416 MB, throughput 0.991074
Reading from 20271: heap size 439 MB, throughput 0.986625
Reading from 20270: heap size 418 MB, throughput 0.9912
Reading from 20271: heap size 441 MB, throughput 0.987472
Reading from 20270: heap size 422 MB, throughput 0.990749
Reading from 20271: heap size 442 MB, throughput 0.98734
Reading from 20270: heap size 423 MB, throughput 0.989042
Reading from 20271: heap size 445 MB, throughput 0.986816
Equal recommendation: 1380 MB each
Reading from 20270: heap size 421 MB, throughput 0.986717
Reading from 20271: heap size 443 MB, throughput 0.983491
Reading from 20270: heap size 423 MB, throughput 0.986074
Reading from 20271: heap size 445 MB, throughput 0.987589
Reading from 20270: heap size 424 MB, throughput 0.982795
Reading from 20271: heap size 445 MB, throughput 0.985276
Reading from 20270: heap size 424 MB, throughput 0.982273
Reading from 20271: heap size 446 MB, throughput 0.984913
Reading from 20270: heap size 426 MB, throughput 0.984071
Reading from 20271: heap size 448 MB, throughput 0.986081
Equal recommendation: 1380 MB each
Reading from 20270: heap size 428 MB, throughput 0.99262
Reading from 20271: heap size 449 MB, throughput 0.990505
Reading from 20271: heap size 452 MB, throughput 0.901335
Reading from 20270: heap size 428 MB, throughput 0.974055
Reading from 20271: heap size 453 MB, throughput 0.862107
Reading from 20270: heap size 431 MB, throughput 0.858369
Reading from 20270: heap size 427 MB, throughput 0.860421
Reading from 20271: heap size 459 MB, throughput 0.95754
Reading from 20270: heap size 431 MB, throughput 0.899843
Reading from 20271: heap size 460 MB, throughput 0.988624
Reading from 20270: heap size 441 MB, throughput 0.990183
Reading from 20271: heap size 465 MB, throughput 0.98846
Reading from 20270: heap size 441 MB, throughput 0.987067
Reading from 20271: heap size 466 MB, throughput 0.98928
Reading from 20270: heap size 442 MB, throughput 0.987815
Equal recommendation: 1380 MB each
Reading from 20271: heap size 467 MB, throughput 0.988354
Reading from 20270: heap size 444 MB, throughput 0.989647
Reading from 20271: heap size 469 MB, throughput 0.988465
Reading from 20270: heap size 442 MB, throughput 0.986689
Reading from 20271: heap size 467 MB, throughput 0.984531
Reading from 20270: heap size 445 MB, throughput 0.98571
Reading from 20270: heap size 446 MB, throughput 0.984924
Reading from 20271: heap size 469 MB, throughput 0.986385
Reading from 20270: heap size 447 MB, throughput 0.982102
Reading from 20271: heap size 472 MB, throughput 0.984254
Equal recommendation: 1380 MB each
Reading from 20271: heap size 472 MB, throughput 0.990172
Reading from 20270: heap size 450 MB, throughput 0.990649
Reading from 20271: heap size 475 MB, throughput 0.908434
Reading from 20271: heap size 476 MB, throughput 0.875848
Reading from 20270: heap size 450 MB, throughput 0.956971
Reading from 20270: heap size 450 MB, throughput 0.858551
Reading from 20270: heap size 452 MB, throughput 0.872998
Reading from 20271: heap size 482 MB, throughput 0.988492
Reading from 20270: heap size 459 MB, throughput 0.989227
Reading from 20271: heap size 483 MB, throughput 0.992063
Reading from 20270: heap size 460 MB, throughput 0.990366
Reading from 20271: heap size 487 MB, throughput 0.989492
Reading from 20270: heap size 466 MB, throughput 0.991254
Equal recommendation: 1380 MB each
Reading from 20271: heap size 489 MB, throughput 0.98887
Reading from 20270: heap size 467 MB, throughput 0.99139
Reading from 20271: heap size 488 MB, throughput 0.989268
Reading from 20270: heap size 468 MB, throughput 0.991464
Reading from 20271: heap size 491 MB, throughput 0.988149
Reading from 20270: heap size 470 MB, throughput 0.986571
Reading from 20271: heap size 492 MB, throughput 0.987245
Reading from 20270: heap size 470 MB, throughput 0.987073
Equal recommendation: 1380 MB each
Reading from 20271: heap size 493 MB, throughput 0.990205
Reading from 20270: heap size 471 MB, throughput 0.984344
Reading from 20271: heap size 493 MB, throughput 0.956343
Reading from 20271: heap size 495 MB, throughput 0.883439
Reading from 20270: heap size 472 MB, throughput 0.975525
Reading from 20271: heap size 500 MB, throughput 0.984954
Reading from 20270: heap size 474 MB, throughput 0.884128
Reading from 20270: heap size 478 MB, throughput 0.901454
Reading from 20270: heap size 479 MB, throughput 0.991861
Reading from 20271: heap size 502 MB, throughput 0.991504
Reading from 20270: heap size 486 MB, throughput 0.993813
Reading from 20271: heap size 507 MB, throughput 0.99236
Reading from 20270: heap size 487 MB, throughput 0.992385
Reading from 20271: heap size 508 MB, throughput 0.992502
Equal recommendation: 1380 MB each
Reading from 20271: heap size 507 MB, throughput 0.989671
Reading from 20270: heap size 489 MB, throughput 0.99207
Reading from 20271: heap size 510 MB, throughput 0.988377
Reading from 20270: heap size 491 MB, throughput 0.989127
Reading from 20271: heap size 511 MB, throughput 0.986916
Reading from 20270: heap size 490 MB, throughput 0.988838
Equal recommendation: 1380 MB each
Reading from 20271: heap size 512 MB, throughput 0.992087
Reading from 20271: heap size 516 MB, throughput 0.925795
Reading from 20271: heap size 516 MB, throughput 0.928769
Reading from 20270: heap size 492 MB, throughput 0.986878
Reading from 20270: heap size 494 MB, throughput 0.987341
Reading from 20270: heap size 495 MB, throughput 0.909051
Reading from 20270: heap size 495 MB, throughput 0.950767
Reading from 20271: heap size 524 MB, throughput 0.993083
Reading from 20270: heap size 496 MB, throughput 0.993739
Reading from 20271: heap size 524 MB, throughput 0.991888
Reading from 20270: heap size 502 MB, throughput 0.992944
Reading from 20271: heap size 526 MB, throughput 0.993167
Equal recommendation: 1380 MB each
Reading from 20270: heap size 503 MB, throughput 0.9938
Reading from 20271: heap size 528 MB, throughput 0.992504
Reading from 20270: heap size 503 MB, throughput 0.990226
Reading from 20271: heap size 529 MB, throughput 0.988892
Reading from 20270: heap size 505 MB, throughput 0.989069
Reading from 20271: heap size 530 MB, throughput 0.987471
Reading from 20270: heap size 505 MB, throughput 0.98795
Equal recommendation: 1380 MB each
Reading from 20271: heap size 533 MB, throughput 0.990123
Reading from 20271: heap size 534 MB, throughput 0.915485
Reading from 20271: heap size 538 MB, throughput 0.981927
Reading from 20270: heap size 506 MB, throughput 0.992235
Reading from 20270: heap size 510 MB, throughput 0.931817
Reading from 20270: heap size 510 MB, throughput 0.883045
Reading from 20271: heap size 540 MB, throughput 0.993288
Reading from 20270: heap size 515 MB, throughput 0.991684
Reading from 20271: heap size 543 MB, throughput 0.992932
Reading from 20270: heap size 517 MB, throughput 0.992772
Equal recommendation: 1380 MB each
Reading from 20271: heap size 545 MB, throughput 0.990991
Reading from 20270: heap size 520 MB, throughput 0.990224
Reading from 20270: heap size 522 MB, throughput 0.992132
Reading from 20271: heap size 543 MB, throughput 0.991536
Reading from 20270: heap size 521 MB, throughput 0.990126
Reading from 20271: heap size 545 MB, throughput 0.989552
Equal recommendation: 1380 MB each
Reading from 20270: heap size 524 MB, throughput 0.987538
Reading from 20271: heap size 548 MB, throughput 0.992849
Reading from 20271: heap size 548 MB, throughput 0.930266
Reading from 20271: heap size 549 MB, throughput 0.968118
Reading from 20270: heap size 527 MB, throughput 0.988764
Reading from 20270: heap size 528 MB, throughput 0.900468
Reading from 20270: heap size 532 MB, throughput 0.985271
Reading from 20271: heap size 551 MB, throughput 0.994286
Reading from 20270: heap size 533 MB, throughput 0.993587
Reading from 20271: heap size 555 MB, throughput 0.992698
Equal recommendation: 1380 MB each
Reading from 20270: heap size 537 MB, throughput 0.994131
Reading from 20271: heap size 556 MB, throughput 0.990719
Reading from 20270: heap size 539 MB, throughput 0.990807
Reading from 20271: heap size 555 MB, throughput 0.991908
Reading from 20270: heap size 537 MB, throughput 0.989424
Reading from 20271: heap size 557 MB, throughput 0.989401
Equal recommendation: 1380 MB each
Reading from 20270: heap size 540 MB, throughput 0.990017
Reading from 20271: heap size 560 MB, throughput 0.994433
Reading from 20271: heap size 561 MB, throughput 0.91106
Reading from 20271: heap size 561 MB, throughput 0.983197
Reading from 20270: heap size 541 MB, throughput 0.993097
Reading from 20270: heap size 542 MB, throughput 0.920271
Reading from 20270: heap size 543 MB, throughput 0.981308
Reading from 20271: heap size 564 MB, throughput 0.99399
Reading from 20270: heap size 545 MB, throughput 0.993676
Reading from 20271: heap size 566 MB, throughput 0.992121
Equal recommendation: 1380 MB each
Reading from 20270: heap size 548 MB, throughput 0.992015
Reading from 20271: heap size 568 MB, throughput 0.991281
Reading from 20270: heap size 550 MB, throughput 0.994134
Reading from 20271: heap size 568 MB, throughput 0.991272
Reading from 20270: heap size 548 MB, throughput 0.991029
Reading from 20271: heap size 570 MB, throughput 0.986844
Equal recommendation: 1380 MB each
Reading from 20271: heap size 570 MB, throughput 0.981206
Reading from 20271: heap size 574 MB, throughput 0.921716
Reading from 20270: heap size 550 MB, throughput 0.989012
Reading from 20271: heap size 578 MB, throughput 0.983608
Reading from 20270: heap size 553 MB, throughput 0.985236
Reading from 20270: heap size 554 MB, throughput 0.915131
Reading from 20270: heap size 558 MB, throughput 0.988411
Reading from 20271: heap size 580 MB, throughput 0.994797
Reading from 20270: heap size 560 MB, throughput 0.99195
Equal recommendation: 1380 MB each
Reading from 20271: heap size 581 MB, throughput 0.9912
Reading from 20270: heap size 562 MB, throughput 0.992666
Reading from 20271: heap size 584 MB, throughput 0.993716
Reading from 20270: heap size 564 MB, throughput 0.992112
Reading from 20271: heap size 584 MB, throughput 0.990491
Equal recommendation: 1380 MB each
Reading from 20271: heap size 586 MB, throughput 0.99029
Reading from 20270: heap size 563 MB, throughput 0.991075
Reading from 20271: heap size 589 MB, throughput 0.933288
Reading from 20271: heap size 590 MB, throughput 0.991407
Reading from 20270: heap size 565 MB, throughput 0.994839
Reading from 20270: heap size 568 MB, throughput 0.958693
Reading from 20270: heap size 569 MB, throughput 0.968659
Reading from 20271: heap size 597 MB, throughput 0.993576
Equal recommendation: 1380 MB each
Reading from 20270: heap size 576 MB, throughput 0.994601
Reading from 20271: heap size 598 MB, throughput 0.993162
Reading from 20270: heap size 576 MB, throughput 0.992257
Reading from 20271: heap size 598 MB, throughput 0.991348
Reading from 20270: heap size 575 MB, throughput 0.990676
Reading from 20271: heap size 600 MB, throughput 0.990769
Equal recommendation: 1380 MB each
Reading from 20271: heap size 602 MB, throughput 0.991278
Reading from 20271: heap size 603 MB, throughput 0.931549
Reading from 20270: heap size 577 MB, throughput 0.990529
Reading from 20271: heap size 607 MB, throughput 0.991503
Reading from 20270: heap size 579 MB, throughput 0.992994
Reading from 20270: heap size 580 MB, throughput 0.971361
Reading from 20270: heap size 579 MB, throughput 0.970186
Reading from 20271: heap size 608 MB, throughput 0.993537
Equal recommendation: 1380 MB each
Reading from 20270: heap size 581 MB, throughput 0.995401
Reading from 20271: heap size 611 MB, throughput 0.992921
Reading from 20270: heap size 583 MB, throughput 0.993847
Reading from 20271: heap size 612 MB, throughput 0.992608
Reading from 20270: heap size 585 MB, throughput 0.991491
Reading from 20271: heap size 613 MB, throughput 0.986607
Equal recommendation: 1380 MB each
Reading from 20271: heap size 614 MB, throughput 0.98347
Reading from 20271: heap size 622 MB, throughput 0.97239
Reading from 20270: heap size 585 MB, throughput 0.989243
Reading from 20270: heap size 586 MB, throughput 0.994844
Reading from 20271: heap size 623 MB, throughput 0.995116
Reading from 20270: heap size 588 MB, throughput 0.9441
Reading from 20270: heap size 589 MB, throughput 0.980756
Equal recommendation: 1380 MB each
Reading from 20271: heap size 626 MB, throughput 0.993473
Reading from 20270: heap size 597 MB, throughput 0.992941
Reading from 20271: heap size 627 MB, throughput 0.991533
Reading from 20270: heap size 597 MB, throughput 0.936231
Reading from 20271: heap size 625 MB, throughput 0.990816
Reading from 20270: heap size 610 MB, throughput 0.995796
Equal recommendation: 1380 MB each
Reading from 20271: heap size 627 MB, throughput 0.99169
Reading from 20271: heap size 629 MB, throughput 0.940065
Client 20271 died
Clients: 1
Reading from 20270: heap size 613 MB, throughput 0.996143
Reading from 20270: heap size 618 MB, throughput 0.992764
Reading from 20270: heap size 619 MB, throughput 0.924672
Client 20270 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
