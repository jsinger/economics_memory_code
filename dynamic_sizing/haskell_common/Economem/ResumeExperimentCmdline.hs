{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, DeriveDataTypeable #-}
{-# OPTIONS_GHC -fno-warn-missing-fields -fno-warn-missing-methods #-}

module Economem.ResumeExperimentCmdline (resume) where

import Economem.ArgHelper
import Economem.Experiment
import Economem.Signals
import System.Console.CmdLib

data Args = Args { inputDir :: FilePath }
          deriving (Typeable, Data, Eq, Show)

instance Attributes Args where
  attributes _ = group "Options" [
    inputDir %> [ Positional 0,
                  Required True,
                  Help "Directory containing an incomplete experiment to resume",
                  ArgHelp "INPUT_DIR" ]]

instance RecordCommand Args where
  mode_summary _ = "Resume an incomplete experiment."

resume :: (FilePath -> IO Experiment) -> IO ()
resume f = getArgs >>= executeR Args {} >>= \opts -> do
  d <- existingAbsoluteDir (inputDir opts)
  e <- f d
  h <- makeSignalHandler
  runExperiment e h
