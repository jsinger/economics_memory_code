#include <cstring>
#include <chrono>
#include <random>
#include <Utilities.hpp>
#include "ProgramSpec.hpp"
#include "lex.ProgramSpec.h"
#include "ProgramSpec.tab.hpp"

Program* g_BisonParsedProgram = NULL; // ARGH! Globals! Bison is ugly...

std::default_random_engine Phase::s_Generator(std::chrono::system_clock::now().time_since_epoch().count());

Program::Program() : m_CurrentPhase(NULL), m_MinHeapMB(1)
{
}

Program::~Program()
{
    for (std::pair<std::string, Phase*> p : m_Phases)
        delete p.second;
}

void Program::addPhase(Phase* p)
{
    m_Phases[p->getName()] = p;
    if (m_CurrentPhase == NULL)
        m_CurrentPhase = p;
}

bool Program::validate()
{
    if (m_Phases.empty())
        return false;

    for (std::pair<std::string, Phase*> p : m_Phases)
    {
        if (!p.second->validate(m_Phases))
            return false;
    }
    return true;
}

unsigned long long Program::getMinHeapMB() const
{
    return m_MinHeapMB;
}

void Program::setMinHeapMB(unsigned long long minHeapMB)
{
    m_MinHeapMB = minHeapMB;
}

const Phase* Program::getCurrentPhase() const
{
    return m_CurrentPhase;
}

void Program::changePhase()
{
    m_CurrentPhase = m_Phases[m_CurrentPhase->pickTransition()];
}

Program* Program::parseFile(const char* filename)
{
    FILE* f;
    if ((f = fopen(filename, "r")) != NULL)
    {
        ProgramSpec_in = f;
        ProgramSpec_parse();
        fclose(f);

        if (g_BisonParsedProgram != NULL && !g_BisonParsedProgram->validate())
        {
            economem::warning("Failed to validate program input");
            delete g_BisonParsedProgram;
            g_BisonParsedProgram = NULL;
        }

        return g_BisonParsedProgram;
    }
    else
        return NULL;
}

void Program::print() const
{
    printf("Program: min heap %llu MB\n", m_MinHeapMB);
    for (auto p : m_Phases)
        p.second->print();
}

Phase::Phase(const std::string& name, int allocRateB, double lengthSeconds, int lenPercentError) : m_Name(name), m_AllocRateB(allocRateB), m_LengthSeconds(lengthSeconds), m_LenPercentError(lenPercentError), m_TransitionProbability(std::vector<Transition*>(100, NULL))
{
}

Phase::~Phase()
{
    for (Transition* t : m_Transitions)
        delete t;
}

const std::string& Phase::getName() const
{
    return m_Name;
}

int Phase::getAllocRateBytes() const
{
    return m_AllocRateB;
}

double Phase::getAllocRateMB() const
{
    return m_AllocRateB / (1024.0 * 1024.0);
}

double Phase::getLengthSeconds() const
{
    return m_LengthSeconds;
}

double Phase::getLengthWithRandomError() const
{
    double length = m_LengthSeconds;

    if (m_LenPercentError != 0)
    {
        std::uniform_int_distribution<int> d(-m_LenPercentError, m_LenPercentError);
        int i = d(s_Generator);

        length = length + (length * (i / 100.0));
    }

    return length;
}

void Phase::addTransitions(const std::list<Transition*>& l)
{
    for (Transition* t : l)
        m_Transitions.push_back(t);
}

bool Phase::validate(const std::map<std::string, Phase*>& phases)
{
    unsigned int i = 0;

    for (Transition* t : m_Transitions)
    {
        unsigned int j = t->getProbability();
        if (i + j > 100)
        {
            economem::warning("Failed to validate program phase '%s': transitions have a total probability over 100 (at least %u).", m_Name.c_str(), i + j);
            return false;
        }

        while (j > 0)
        {
            m_TransitionProbability[i] = t;
            i++;
            j--;
        }
    }

    if (i != 100)
    {
        economem::warning("Failed to validate program phase '%s': transitions have a total probability that is not 100 (total: %u).", m_Name.c_str(), i);
        return false;
    }

    for (Transition* t : m_Transitions)
    {
        if (phases.find(t->getTarget()) == phases.end())
        {
            economem::warning("The phase '%s' does not exist.", t->getTarget().c_str());
            return false;
        }
    }

    return true;
}

const std::string& Phase::pickTransition() const
{
    std::uniform_int_distribution<int> d(0, 99);
    int i = d(s_Generator);
    return m_TransitionProbability[i]->getTarget();
}

void Phase::print() const
{
    printf("   Phase '%s', allocRate %u B/s, length %g s, error %u\n", m_Name.c_str(), m_AllocRateB, m_LengthSeconds, m_LenPercentError);
    for (Transition* p : m_Transitions)
        p->print();
}

const std::string& Transition::getTarget() const
{
    return m_Target;
}

unsigned int Transition::getProbability() const
{
    return m_Probability;
}

void Transition::print() const
{
    printf("        to %s, probability %u\n", m_Target.c_str(), m_Probability);
}
