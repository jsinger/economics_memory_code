Using scaled threading model. 4 processors detected, 4 threads used to drive the workload, in a possible range of [1,64000]
===== DaCapo 9.12 h2 starting warmup 1 =====
................................
Completed 32000 transactions
	Stock level .............  1277 ( 4.0%)
	Order status by name ....   765 ( 2.4%)
	Order status by ID ......   518 ( 1.6%)
	Payment by name .........  8262 (25.8%)
	Payment by ID ...........  5489 (17.2%)
	Delivery schedule .......  1299 ( 4.1%)
	New order ............... 14241 (44.5%)
	New order rollback ......   149 ( 0.5%)
Resetting database to initial state
===== DaCapo 9.12 h2 completed warmup 1 in 30113 msec =====
===== DaCapo 9.12 h2 starting warmup 2 =====
................................
Completed 32000 transactions
	Stock level .............  1277 ( 4.0%)
	Order status by name ....   765 ( 2.4%)
	Order status by ID ......   518 ( 1.6%)
	Payment by name .........  8262 (25.8%)
	Payment by ID ...........  5489 (17.2%)
	Delivery schedule .......  1299 ( 4.1%)
	New order ............... 14241 (44.5%)
	New order rollback ......   149 ( 0.5%)
Resetting database to initial state
===== DaCapo 9.12 h2 completed warmup 2 in 31523 msec =====
===== DaCapo 9.12 h2 starting =====
................................
Completed 32000 transactions
	Stock level .............  1277 ( 4.0%)
	Order status by name ....   765 ( 2.4%)
	Order status by ID ......   518 ( 1.6%)
	Payment by name .........  8262 (25.8%)
	Payment by ID ...........  5489 (17.2%)
	Delivery schedule .......  1299 ( 4.1%)
	New order ............... 14241 (44.5%)
	New order rollback ......   149 ( 0.5%)
Resetting database to initial state
===== DaCapo 9.12 h2 PASSED in 31432 msec =====
