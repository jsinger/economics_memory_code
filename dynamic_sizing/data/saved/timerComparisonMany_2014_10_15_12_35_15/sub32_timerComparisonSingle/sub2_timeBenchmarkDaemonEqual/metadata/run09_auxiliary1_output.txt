economemd
    total memory: 3705 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub32_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run09_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 14395: heap size 9 MB, throughput 0.987966
Clients: 1
Client 14395 has a minimum heap size of 12 MB
Reading from 14394: heap size 9 MB, throughput 0.986878
Clients: 2
Client 14394 has a minimum heap size of 1223 MB
Reading from 14394: heap size 9 MB, throughput 0.981341
Reading from 14395: heap size 9 MB, throughput 0.986688
Reading from 14394: heap size 11 MB, throughput 0.978027
Reading from 14395: heap size 11 MB, throughput 0.973948
Reading from 14395: heap size 11 MB, throughput 0.962574
Reading from 14394: heap size 11 MB, throughput 0.98453
Reading from 14394: heap size 15 MB, throughput 0.859948
Reading from 14395: heap size 15 MB, throughput 0.987329
Reading from 14394: heap size 19 MB, throughput 0.96509
Reading from 14394: heap size 24 MB, throughput 0.942413
Reading from 14395: heap size 15 MB, throughput 0.979457
Reading from 14394: heap size 29 MB, throughput 0.913677
Reading from 14394: heap size 30 MB, throughput 0.394644
Reading from 14394: heap size 40 MB, throughput 0.795086
Reading from 14394: heap size 44 MB, throughput 0.765349
Reading from 14394: heap size 46 MB, throughput 0.700204
Reading from 14395: heap size 24 MB, throughput 0.945315
Reading from 14394: heap size 49 MB, throughput 0.315379
Reading from 14395: heap size 29 MB, throughput 0.988002
Reading from 14394: heap size 65 MB, throughput 0.764127
Reading from 14394: heap size 70 MB, throughput 0.737713
Reading from 14394: heap size 72 MB, throughput 0.273868
Reading from 14395: heap size 37 MB, throughput 0.988122
Reading from 14394: heap size 97 MB, throughput 0.7318
Reading from 14394: heap size 99 MB, throughput 0.815528
Reading from 14394: heap size 103 MB, throughput 0.684194
Reading from 14395: heap size 44 MB, throughput 0.987491
Reading from 14394: heap size 106 MB, throughput 0.628081
Reading from 14394: heap size 114 MB, throughput 0.738507
Reading from 14395: heap size 45 MB, throughput 0.987725
Reading from 14395: heap size 45 MB, throughput 0.936345
Reading from 14394: heap size 117 MB, throughput 0.124159
Reading from 14395: heap size 53 MB, throughput 0.964979
Reading from 14394: heap size 147 MB, throughput 0.654046
Reading from 14395: heap size 53 MB, throughput 0.980813
Reading from 14394: heap size 150 MB, throughput 0.715388
Reading from 14394: heap size 153 MB, throughput 0.538645
Reading from 14395: heap size 66 MB, throughput 0.982184
Reading from 14394: heap size 158 MB, throughput 0.667468
Reading from 14395: heap size 66 MB, throughput 0.761697
Reading from 14394: heap size 164 MB, throughput 0.138764
Reading from 14395: heap size 85 MB, throughput 0.992968
Reading from 14394: heap size 197 MB, throughput 0.622832
Reading from 14395: heap size 86 MB, throughput 0.996171
Reading from 14394: heap size 207 MB, throughput 0.173668
Reading from 14394: heap size 241 MB, throughput 0.59071
Reading from 14394: heap size 241 MB, throughput 0.716267
Reading from 14394: heap size 244 MB, throughput 0.645661
Reading from 14394: heap size 247 MB, throughput 0.674086
Reading from 14394: heap size 251 MB, throughput 0.573617
Reading from 14395: heap size 94 MB, throughput 0.997965
Reading from 14394: heap size 254 MB, throughput 0.705343
Reading from 14394: heap size 263 MB, throughput 0.498856
Reading from 14394: heap size 267 MB, throughput 0.528971
Reading from 14394: heap size 279 MB, throughput 0.519972
Reading from 14394: heap size 285 MB, throughput 0.451969
Reading from 14395: heap size 96 MB, throughput 0.997363
Reading from 14394: heap size 294 MB, throughput 0.0793242
Reading from 14394: heap size 338 MB, throughput 0.468071
Reading from 14395: heap size 103 MB, throughput 0.996362
Reading from 14394: heap size 344 MB, throughput 0.0792083
Reading from 14395: heap size 104 MB, throughput 0.996269
Reading from 14394: heap size 387 MB, throughput 0.38074
Reading from 14394: heap size 309 MB, throughput 0.618422
Reading from 14394: heap size 376 MB, throughput 0.57335
Reading from 14394: heap size 382 MB, throughput 0.578168
Reading from 14394: heap size 381 MB, throughput 0.510238
Reading from 14394: heap size 382 MB, throughput 0.570322
Reading from 14395: heap size 110 MB, throughput 0.997578
Reading from 14394: heap size 385 MB, throughput 0.55451
Reading from 14395: heap size 111 MB, throughput 0.990767
Reading from 14394: heap size 390 MB, throughput 0.0854428
Reading from 14394: heap size 446 MB, throughput 0.42586
Reading from 14394: heap size 449 MB, throughput 0.490234
Equal recommendation: 1852 MB each
Reading from 14394: heap size 451 MB, throughput 0.557285
Reading from 14395: heap size 118 MB, throughput 0.988671
Reading from 14394: heap size 452 MB, throughput 0.498348
Reading from 14394: heap size 457 MB, throughput 0.529712
Reading from 14394: heap size 459 MB, throughput 0.554267
Reading from 14394: heap size 464 MB, throughput 0.526126
Reading from 14395: heap size 118 MB, throughput 0.98921
Reading from 14394: heap size 469 MB, throughput 0.108988
Reading from 14394: heap size 524 MB, throughput 0.519418
Reading from 14395: heap size 126 MB, throughput 0.996734
Reading from 14394: heap size 530 MB, throughput 0.553095
Reading from 14394: heap size 534 MB, throughput 0.485155
Reading from 14394: heap size 537 MB, throughput 0.493819
Reading from 14394: heap size 542 MB, throughput 0.537417
Reading from 14395: heap size 126 MB, throughput 0.997162
Reading from 14395: heap size 131 MB, throughput 0.9966
Reading from 14394: heap size 546 MB, throughput 0.0921074
Reading from 14394: heap size 608 MB, throughput 0.510703
Reading from 14394: heap size 612 MB, throughput 0.619161
Reading from 14394: heap size 614 MB, throughput 0.62392
Reading from 14394: heap size 616 MB, throughput 0.575047
Reading from 14395: heap size 132 MB, throughput 0.996283
Reading from 14394: heap size 621 MB, throughput 0.567438
Reading from 14395: heap size 136 MB, throughput 0.99568
Reading from 14395: heap size 137 MB, throughput 0.995236
Reading from 14394: heap size 627 MB, throughput 0.139144
Reading from 14395: heap size 143 MB, throughput 0.997247
Reading from 14394: heap size 688 MB, throughput 0.813495
Reading from 14394: heap size 693 MB, throughput 0.837701
Reading from 14395: heap size 143 MB, throughput 0.997289
Reading from 14394: heap size 694 MB, throughput 0.862934
Reading from 14394: heap size 706 MB, throughput 0.436194
Reading from 14394: heap size 716 MB, throughput 0.566528
Reading from 14395: heap size 147 MB, throughput 0.99796
Reading from 14395: heap size 147 MB, throughput 0.998277
Equal recommendation: 1852 MB each
Reading from 14394: heap size 720 MB, throughput 0.0250935
Reading from 14395: heap size 150 MB, throughput 0.997658
Reading from 14394: heap size 792 MB, throughput 0.303923
Reading from 14394: heap size 704 MB, throughput 0.385101
Reading from 14394: heap size 781 MB, throughput 0.463119
Reading from 14394: heap size 786 MB, throughput 0.829423
Reading from 14395: heap size 151 MB, throughput 0.997863
Reading from 14395: heap size 154 MB, throughput 0.998007
Reading from 14394: heap size 781 MB, throughput 0.101969
Reading from 14394: heap size 861 MB, throughput 0.3978
Reading from 14394: heap size 861 MB, throughput 0.511602
Reading from 14395: heap size 154 MB, throughput 0.99671
Reading from 14394: heap size 863 MB, throughput 0.757334
Reading from 14394: heap size 870 MB, throughput 0.674938
Reading from 14395: heap size 156 MB, throughput 0.984689
Reading from 14394: heap size 870 MB, throughput 0.818048
Reading from 14395: heap size 157 MB, throughput 0.994087
Reading from 14394: heap size 876 MB, throughput 0.0713899
Reading from 14395: heap size 163 MB, throughput 0.997664
Reading from 14394: heap size 969 MB, throughput 0.823016
Reading from 14394: heap size 977 MB, throughput 0.8941
Reading from 14394: heap size 979 MB, throughput 0.926729
Reading from 14395: heap size 164 MB, throughput 0.997363
Reading from 14394: heap size 965 MB, throughput 0.850486
Reading from 14394: heap size 825 MB, throughput 0.852886
Reading from 14394: heap size 954 MB, throughput 0.828806
Reading from 14394: heap size 830 MB, throughput 0.856934
Reading from 14394: heap size 944 MB, throughput 0.856892
Reading from 14394: heap size 827 MB, throughput 0.871895
Reading from 14395: heap size 169 MB, throughput 0.997885
Reading from 14394: heap size 937 MB, throughput 0.85926
Reading from 14394: heap size 832 MB, throughput 0.874783
Reading from 14394: heap size 932 MB, throughput 0.884999
Reading from 14394: heap size 938 MB, throughput 0.860477
Reading from 14395: heap size 169 MB, throughput 0.997445
Equal recommendation: 1852 MB each
Reading from 14395: heap size 174 MB, throughput 0.997777
Reading from 14394: heap size 927 MB, throughput 0.983112
Reading from 14394: heap size 838 MB, throughput 0.925821
Reading from 14395: heap size 174 MB, throughput 0.991113
Reading from 14394: heap size 925 MB, throughput 0.788504
Reading from 14394: heap size 928 MB, throughput 0.79621
Reading from 14394: heap size 920 MB, throughput 0.826591
Reading from 14394: heap size 924 MB, throughput 0.82752
Reading from 14394: heap size 919 MB, throughput 0.838953
Reading from 14394: heap size 923 MB, throughput 0.841003
Reading from 14395: heap size 178 MB, throughput 0.99777
Reading from 14394: heap size 920 MB, throughput 0.842639
Reading from 14394: heap size 923 MB, throughput 0.919338
Reading from 14394: heap size 926 MB, throughput 0.89527
Reading from 14394: heap size 926 MB, throughput 0.884638
Reading from 14395: heap size 178 MB, throughput 0.997396
Reading from 14394: heap size 929 MB, throughput 0.789643
Reading from 14394: heap size 931 MB, throughput 0.707688
Reading from 14395: heap size 182 MB, throughput 0.997293
Reading from 14394: heap size 919 MB, throughput 0.0747383
Reading from 14394: heap size 1050 MB, throughput 0.556653
Reading from 14395: heap size 182 MB, throughput 0.997276
Reading from 14394: heap size 1045 MB, throughput 0.772448
Reading from 14394: heap size 1050 MB, throughput 0.719466
Reading from 14394: heap size 1051 MB, throughput 0.755197
Reading from 14394: heap size 1053 MB, throughput 0.710187
Reading from 14394: heap size 1061 MB, throughput 0.742364
Reading from 14394: heap size 1062 MB, throughput 0.722401
Reading from 14395: heap size 186 MB, throughput 0.996455
Reading from 14394: heap size 1075 MB, throughput 0.725417
Reading from 14394: heap size 1076 MB, throughput 0.705953
Reading from 14394: heap size 1091 MB, throughput 0.904386
Reading from 14395: heap size 186 MB, throughput 0.997428
Reading from 14395: heap size 189 MB, throughput 0.997748
Equal recommendation: 1852 MB each
Reading from 14395: heap size 190 MB, throughput 0.998178
Reading from 14394: heap size 1093 MB, throughput 0.981211
Reading from 14395: heap size 193 MB, throughput 0.998004
Reading from 14395: heap size 193 MB, throughput 0.988525
Reading from 14395: heap size 196 MB, throughput 0.995905
Reading from 14394: heap size 1110 MB, throughput 0.982658
Reading from 14395: heap size 197 MB, throughput 0.997632
Reading from 14395: heap size 202 MB, throughput 0.997552
Reading from 14395: heap size 202 MB, throughput 0.997876
Reading from 14394: heap size 1113 MB, throughput 0.980758
Reading from 14395: heap size 206 MB, throughput 0.997168
Reading from 14395: heap size 206 MB, throughput 0.997571
Equal recommendation: 1852 MB each
Reading from 14394: heap size 1118 MB, throughput 0.981201
Reading from 14395: heap size 210 MB, throughput 0.998221
Reading from 14395: heap size 210 MB, throughput 0.997982
Reading from 14395: heap size 214 MB, throughput 0.9971
Reading from 14394: heap size 1122 MB, throughput 0.978075
Reading from 14395: heap size 214 MB, throughput 0.996835
Reading from 14395: heap size 217 MB, throughput 0.996844
Reading from 14394: heap size 1115 MB, throughput 0.979403
Reading from 14395: heap size 218 MB, throughput 0.997903
Reading from 14395: heap size 222 MB, throughput 0.996515
Equal recommendation: 1852 MB each
Reading from 14395: heap size 222 MB, throughput 0.990037
Reading from 14394: heap size 1121 MB, throughput 0.973641
Reading from 14395: heap size 225 MB, throughput 0.995992
Reading from 14395: heap size 226 MB, throughput 0.99712
Reading from 14394: heap size 1115 MB, throughput 0.97766
Reading from 14395: heap size 231 MB, throughput 0.997911
Reading from 14395: heap size 232 MB, throughput 0.997355
Reading from 14395: heap size 220 MB, throughput 0.99819
Reading from 14394: heap size 1119 MB, throughput 0.97894
Reading from 14395: heap size 210 MB, throughput 0.997198
Reading from 14395: heap size 201 MB, throughput 0.997873
Equal recommendation: 1852 MB each
Reading from 14394: heap size 1122 MB, throughput 0.976919
Reading from 14395: heap size 193 MB, throughput 0.997221
Reading from 14395: heap size 185 MB, throughput 0.997042
Reading from 14395: heap size 177 MB, throughput 0.997192
Reading from 14394: heap size 1122 MB, throughput 0.977844
Reading from 14395: heap size 169 MB, throughput 0.996942
Reading from 14395: heap size 162 MB, throughput 0.997127
Reading from 14395: heap size 156 MB, throughput 0.997229
Reading from 14395: heap size 149 MB, throughput 0.993458
Reading from 14395: heap size 143 MB, throughput 0.992017
Reading from 14394: heap size 1126 MB, throughput 0.966725
Reading from 14395: heap size 148 MB, throughput 0.778808
Reading from 14395: heap size 156 MB, throughput 0.993909
Reading from 14395: heap size 162 MB, throughput 0.997966
Equal recommendation: 1852 MB each
Reading from 14395: heap size 167 MB, throughput 0.998301
Reading from 14394: heap size 1128 MB, throughput 0.977059
Reading from 14395: heap size 173 MB, throughput 0.998269
Reading from 14395: heap size 178 MB, throughput 0.998521
Reading from 14395: heap size 182 MB, throughput 0.998145
Reading from 14394: heap size 1131 MB, throughput 0.978623
Reading from 14395: heap size 187 MB, throughput 0.99875
Reading from 14395: heap size 191 MB, throughput 0.997764
Reading from 14395: heap size 195 MB, throughput 0.998438
Reading from 14394: heap size 1135 MB, throughput 0.979223
Reading from 14395: heap size 199 MB, throughput 0.998598
Equal recommendation: 1852 MB each
Reading from 14395: heap size 202 MB, throughput 0.998106
Reading from 14395: heap size 205 MB, throughput 0.998055
Reading from 14394: heap size 1137 MB, throughput 0.979518
Reading from 14395: heap size 209 MB, throughput 0.998729
Reading from 14395: heap size 209 MB, throughput 0.998577
Reading from 14395: heap size 212 MB, throughput 0.997453
Reading from 14394: heap size 1142 MB, throughput 0.972476
Reading from 14395: heap size 212 MB, throughput 0.992097
Reading from 14395: heap size 215 MB, throughput 0.996542
Reading from 14395: heap size 216 MB, throughput 0.998233
Reading from 14394: heap size 1146 MB, throughput 0.975982
Reading from 14395: heap size 222 MB, throughput 0.998404
Equal recommendation: 1852 MB each
Reading from 14395: heap size 222 MB, throughput 0.997144
Reading from 14395: heap size 227 MB, throughput 0.99841
Reading from 14394: heap size 1149 MB, throughput 0.977339
Reading from 14395: heap size 227 MB, throughput 0.997772
Reading from 14395: heap size 232 MB, throughput 0.99804
Reading from 14394: heap size 1153 MB, throughput 0.977929
Reading from 14395: heap size 232 MB, throughput 0.997945
Reading from 14395: heap size 237 MB, throughput 0.997365
Reading from 14395: heap size 237 MB, throughput 0.997538
Equal recommendation: 1852 MB each
Reading from 14394: heap size 1154 MB, throughput 0.977217
Reading from 14395: heap size 242 MB, throughput 0.998185
Reading from 14395: heap size 242 MB, throughput 0.996415
Reading from 14395: heap size 247 MB, throughput 0.990222
Reading from 14394: heap size 1158 MB, throughput 0.977683
Reading from 14395: heap size 247 MB, throughput 0.996373
Reading from 14395: heap size 257 MB, throughput 0.998573
Reading from 14395: heap size 257 MB, throughput 0.9977
Reading from 14394: heap size 1158 MB, throughput 0.978948
Reading from 14395: heap size 263 MB, throughput 0.997847
Equal recommendation: 1852 MB each
Reading from 14395: heap size 264 MB, throughput 0.997321
Reading from 14394: heap size 1162 MB, throughput 0.978867
Reading from 14395: heap size 271 MB, throughput 0.998265
Reading from 14395: heap size 271 MB, throughput 0.997925
Reading from 14395: heap size 277 MB, throughput 0.995361
Reading from 14394: heap size 1162 MB, throughput 0.681469
Reading from 14395: heap size 277 MB, throughput 0.99762
Equal recommendation: 1852 MB each
Reading from 14395: heap size 284 MB, throughput 0.998183
Reading from 14395: heap size 285 MB, throughput 0.992427
Reading from 14394: heap size 1264 MB, throughput 0.969967
Reading from 14395: heap size 290 MB, throughput 0.997283
Reading from 14395: heap size 291 MB, throughput 0.99804
Reading from 14394: heap size 1264 MB, throughput 0.989775
Reading from 14395: heap size 299 MB, throughput 0.99787
Reading from 14395: heap size 300 MB, throughput 0.997644
Reading from 14394: heap size 1272 MB, throughput 0.99064
Equal recommendation: 1852 MB each
Reading from 14395: heap size 307 MB, throughput 0.998421
Reading from 14395: heap size 307 MB, throughput 0.998382
Reading from 14394: heap size 1277 MB, throughput 0.988425
Reading from 14395: heap size 313 MB, throughput 0.998467
Reading from 14395: heap size 314 MB, throughput 0.998265
Reading from 14394: heap size 1280 MB, throughput 0.989524
Reading from 14395: heap size 319 MB, throughput 0.997874
Reading from 14395: heap size 319 MB, throughput 0.994139
Equal recommendation: 1852 MB each
Reading from 14394: heap size 1176 MB, throughput 0.986841
Reading from 14395: heap size 326 MB, throughput 0.99854
Reading from 14395: heap size 326 MB, throughput 0.998106
Reading from 14394: heap size 1271 MB, throughput 0.983129
Reading from 14395: heap size 332 MB, throughput 0.998471
Reading from 14395: heap size 333 MB, throughput 0.997708
Reading from 14394: heap size 1192 MB, throughput 0.984382
Reading from 14395: heap size 340 MB, throughput 0.997568
Equal recommendation: 1852 MB each
Reading from 14395: heap size 340 MB, throughput 0.998173
Reading from 14394: heap size 1258 MB, throughput 0.982605
Reading from 14395: heap size 347 MB, throughput 0.998525
Reading from 14395: heap size 347 MB, throughput 0.995714
Reading from 14394: heap size 1208 MB, throughput 0.968648
Reading from 14395: heap size 353 MB, throughput 0.997477
Reading from 14395: heap size 354 MB, throughput 0.998042
Reading from 14394: heap size 1262 MB, throughput 0.979723
Equal recommendation: 1852 MB each
Reading from 14395: heap size 361 MB, throughput 0.997978
Reading from 14395: heap size 362 MB, throughput 0.998105
Reading from 14394: heap size 1263 MB, throughput 0.983649
Reading from 14395: heap size 369 MB, throughput 0.997978
Reading from 14394: heap size 1265 MB, throughput 0.978718
Reading from 14395: heap size 369 MB, throughput 0.997888
Equal recommendation: 1852 MB each
Reading from 14395: heap size 376 MB, throughput 0.998146
Reading from 14394: heap size 1268 MB, throughput 0.968681
Reading from 14395: heap size 376 MB, throughput 0.932037
Reading from 14395: heap size 390 MB, throughput 0.998826
Reading from 14394: heap size 1270 MB, throughput 0.975437
Reading from 14395: heap size 391 MB, throughput 0.998188
Equal recommendation: 1852 MB each
Reading from 14395: heap size 401 MB, throughput 0.998692
Reading from 14394: heap size 1275 MB, throughput 0.976734
Reading from 14395: heap size 402 MB, throughput 0.999049
Reading from 14394: heap size 1281 MB, throughput 0.976686
Reading from 14395: heap size 410 MB, throughput 0.998585
Reading from 14395: heap size 411 MB, throughput 0.998921
Reading from 14394: heap size 1286 MB, throughput 0.977164
Equal recommendation: 1852 MB each
Reading from 14395: heap size 419 MB, throughput 0.996983
Reading from 14395: heap size 419 MB, throughput 0.997897
Reading from 14394: heap size 1291 MB, throughput 0.976157
Reading from 14395: heap size 431 MB, throughput 0.998329
Reading from 14394: heap size 1294 MB, throughput 0.978204
Reading from 14395: heap size 431 MB, throughput 0.998488
Equal recommendation: 1852 MB each
Reading from 14395: heap size 440 MB, throughput 0.998608
Reading from 14394: heap size 1298 MB, throughput 0.975132
Reading from 14395: heap size 441 MB, throughput 0.998458
Reading from 14394: heap size 1299 MB, throughput 0.970368
Reading from 14395: heap size 450 MB, throughput 0.998531
Reading from 14395: heap size 450 MB, throughput 0.994833
Equal recommendation: 1852 MB each
Reading from 14395: heap size 458 MB, throughput 0.998721
Reading from 14395: heap size 459 MB, throughput 0.998596
Reading from 14395: heap size 469 MB, throughput 0.998504
Reading from 14395: heap size 470 MB, throughput 0.998275
Equal recommendation: 1852 MB each
Reading from 14395: heap size 479 MB, throughput 0.998635
Reading from 14395: heap size 480 MB, throughput 0.996947
Reading from 14394: heap size 1304 MB, throughput 0.86977
Reading from 14395: heap size 489 MB, throughput 0.998244
Reading from 14395: heap size 490 MB, throughput 0.998521
Equal recommendation: 1852 MB each
Reading from 14395: heap size 500 MB, throughput 0.998736
Reading from 14395: heap size 501 MB, throughput 0.998498
Reading from 14394: heap size 1494 MB, throughput 0.988475
Reading from 14395: heap size 510 MB, throughput 0.998621
Equal recommendation: 1852 MB each
Reading from 14395: heap size 510 MB, throughput 0.99593
Reading from 14395: heap size 519 MB, throughput 0.998577
Reading from 14394: heap size 1537 MB, throughput 0.986215
Reading from 14394: heap size 1452 MB, throughput 0.887182
Reading from 14394: heap size 1537 MB, throughput 0.808153
Reading from 14394: heap size 1537 MB, throughput 0.714172
Reading from 14394: heap size 1560 MB, throughput 0.769461
Reading from 14394: heap size 1574 MB, throughput 0.753625
Reading from 14394: heap size 1601 MB, throughput 0.780313
Reading from 14394: heap size 1609 MB, throughput 0.76105
Reading from 14395: heap size 520 MB, throughput 0.998417
Reading from 14394: heap size 1639 MB, throughput 0.967016
Equal recommendation: 1852 MB each
Reading from 14395: heap size 531 MB, throughput 0.998658
Reading from 14394: heap size 1643 MB, throughput 0.98851
Reading from 14395: heap size 532 MB, throughput 0.998093
Client 14395 died
Clients: 1
Reading from 14394: heap size 1650 MB, throughput 0.988244
Recommendation: one client; give it all the memory
Reading from 14394: heap size 1659 MB, throughput 0.987341
Reading from 14394: heap size 1652 MB, throughput 0.98656
Recommendation: one client; give it all the memory
Reading from 14394: heap size 1663 MB, throughput 0.986504
Reading from 14394: heap size 1655 MB, throughput 0.985223
Recommendation: one client; give it all the memory
Reading from 14394: heap size 1663 MB, throughput 0.988792
Reading from 14394: heap size 1670 MB, throughput 0.98901
Recommendation: one client; give it all the memory
Reading from 14394: heap size 1672 MB, throughput 0.987984
Recommendation: one client; give it all the memory
Reading from 14394: heap size 1663 MB, throughput 0.987736
Reading from 14394: heap size 1565 MB, throughput 0.987062
Recommendation: one client; give it all the memory
Reading from 14394: heap size 1652 MB, throughput 0.986288
Reading from 14394: heap size 1661 MB, throughput 0.98499
Recommendation: one client; give it all the memory
Reading from 14394: heap size 1663 MB, throughput 0.984229
Reading from 14394: heap size 1663 MB, throughput 0.983552
Recommendation: one client; give it all the memory
Reading from 14394: heap size 1667 MB, throughput 0.982106
Recommendation: one client; give it all the memory
Reading from 14394: heap size 1672 MB, throughput 0.981747
Reading from 14394: heap size 1679 MB, throughput 0.855012
Recommendation: one client; give it all the memory
Reading from 14394: heap size 1601 MB, throughput 0.996608
Reading from 14394: heap size 1598 MB, throughput 0.994236
Recommendation: one client; give it all the memory
Reading from 14394: heap size 1610 MB, throughput 0.993474
Reading from 14394: heap size 1620 MB, throughput 0.992887
Recommendation: one client; give it all the memory
Reading from 14394: heap size 1621 MB, throughput 0.991712
Recommendation: one client; give it all the memory
Reading from 14394: heap size 1610 MB, throughput 0.990123
Reading from 14394: heap size 1469 MB, throughput 0.988813
Recommendation: one client; give it all the memory
Reading from 14394: heap size 1592 MB, throughput 0.988692
Reading from 14394: heap size 1497 MB, throughput 0.987224
Recommendation: one client; give it all the memory
Reading from 14394: heap size 1589 MB, throughput 0.987327
Recommendation: one client; give it all the memory
Reading from 14394: heap size 1593 MB, throughput 0.985768
Recommendation: one client; give it all the memory
Reading from 14394: heap size 1596 MB, throughput 0.990646
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 14394: heap size 1599 MB, throughput 0.991337
Reading from 14394: heap size 1619 MB, throughput 0.867608
Reading from 14394: heap size 1647 MB, throughput 0.751415
Reading from 14394: heap size 1637 MB, throughput 0.714299
Reading from 14394: heap size 1686 MB, throughput 0.716959
Reading from 14394: heap size 1726 MB, throughput 0.743632
Reading from 14394: heap size 1745 MB, throughput 0.784036
Client 14394 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
TimerThread: finished
CommandThread: finished
ReadingThread: finished
Main: All threads killed, cleaning up
