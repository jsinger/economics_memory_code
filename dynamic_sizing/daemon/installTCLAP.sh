#!/bin/bash
# Install the TCLAP library's header files

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ ! -e "${DIR}/tclap" ]; then
    TEMPDIR="$(mktemp -d)" &&
    cd "${TEMPDIR}" &&
    wget -O download.tgz http://sourceforge.net/projects/tclap/files/tclap-1.2.1.tar.gz/download &&
    tar -xvf download.tgz &&
    cd tclap-1.2.1 &&
    mkdir -p "${DIR}/tclap" &&
    ./configure --prefix="${DIR}/tclap" &&
    make &&
    make install &&
    cd &&
    rm -rf "${TEMPDIR}"
fi
