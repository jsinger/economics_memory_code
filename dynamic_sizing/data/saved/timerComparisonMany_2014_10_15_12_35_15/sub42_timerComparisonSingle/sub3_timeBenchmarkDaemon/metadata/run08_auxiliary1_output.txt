economemd
    total memory: 1656 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub42_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 15591: heap size 9 MB, throughput 0.992121
Clients: 1
Client 15591 has a minimum heap size of 276 MB
Reading from 15590: heap size 9 MB, throughput 0.990697
Clients: 2
Client 15590 has a minimum heap size of 276 MB
Reading from 15591: heap size 9 MB, throughput 0.988515
Reading from 15590: heap size 9 MB, throughput 0.986724
Reading from 15591: heap size 9 MB, throughput 0.983205
Reading from 15590: heap size 9 MB, throughput 0.980018
Reading from 15590: heap size 9 MB, throughput 0.972933
Reading from 15591: heap size 9 MB, throughput 0.977132
Reading from 15590: heap size 11 MB, throughput 0.979922
Reading from 15591: heap size 11 MB, throughput 0.98543
Reading from 15591: heap size 11 MB, throughput 0.984854
Reading from 15590: heap size 11 MB, throughput 0.985676
Reading from 15590: heap size 17 MB, throughput 0.977378
Reading from 15591: heap size 17 MB, throughput 0.966038
Reading from 15591: heap size 17 MB, throughput 0.890498
Reading from 15590: heap size 17 MB, throughput 0.910801
Reading from 15590: heap size 30 MB, throughput 0.844204
Reading from 15591: heap size 30 MB, throughput 0.808912
Reading from 15591: heap size 31 MB, throughput 0.540186
Reading from 15590: heap size 31 MB, throughput 0.646163
Reading from 15590: heap size 34 MB, throughput 0.769592
Reading from 15591: heap size 36 MB, throughput 0.718093
Reading from 15590: heap size 47 MB, throughput 0.617841
Reading from 15591: heap size 47 MB, throughput 0.486427
Reading from 15590: heap size 49 MB, throughput 0.559867
Reading from 15591: heap size 51 MB, throughput 0.420192
Reading from 15591: heap size 66 MB, throughput 0.442558
Reading from 15590: heap size 51 MB, throughput 0.474845
Reading from 15591: heap size 69 MB, throughput 0.428284
Reading from 15590: heap size 74 MB, throughput 0.371915
Reading from 15590: heap size 74 MB, throughput 0.427763
Reading from 15590: heap size 96 MB, throughput 0.39875
Reading from 15591: heap size 71 MB, throughput 0.410017
Reading from 15590: heap size 99 MB, throughput 0.498775
Reading from 15591: heap size 99 MB, throughput 0.452206
Reading from 15590: heap size 101 MB, throughput 0.345572
Reading from 15591: heap size 99 MB, throughput 0.254989
Reading from 15590: heap size 103 MB, throughput 0.41287
Reading from 15591: heap size 131 MB, throughput 0.669952
Reading from 15590: heap size 134 MB, throughput 0.345647
Reading from 15591: heap size 131 MB, throughput 0.585905
Reading from 15590: heap size 136 MB, throughput 0.258026
Reading from 15591: heap size 132 MB, throughput 0.32581
Reading from 15590: heap size 139 MB, throughput 0.720042
Reading from 15591: heap size 135 MB, throughput 0.332518
Reading from 15590: heap size 144 MB, throughput 0.749085
Reading from 15590: heap size 148 MB, throughput 0.664148
Reading from 15591: heap size 138 MB, throughput 0.464399
Reading from 15591: heap size 178 MB, throughput 0.453705
Reading from 15591: heap size 182 MB, throughput 0.403337
Reading from 15590: heap size 152 MB, throughput 0.39156
Reading from 15591: heap size 184 MB, throughput 0.19753
Reading from 15590: heap size 197 MB, throughput 0.675531
Reading from 15591: heap size 190 MB, throughput 0.137105
Reading from 15590: heap size 199 MB, throughput 0.70394
Reading from 15590: heap size 202 MB, throughput 0.766151
Reading from 15591: heap size 193 MB, throughput 0.411294
Reading from 15590: heap size 209 MB, throughput 0.862551
Reading from 15590: heap size 215 MB, throughput 0.848064
Reading from 15590: heap size 222 MB, throughput 0.788053
Reading from 15591: heap size 243 MB, throughput 0.301543
Reading from 15590: heap size 228 MB, throughput 0.785677
Reading from 15590: heap size 233 MB, throughput 0.725382
Reading from 15591: heap size 246 MB, throughput 0.881295
Reading from 15590: heap size 237 MB, throughput 0.714731
Reading from 15591: heap size 250 MB, throughput 0.824333
Reading from 15590: heap size 241 MB, throughput 0.694479
Reading from 15590: heap size 246 MB, throughput 0.657455
Reading from 15591: heap size 254 MB, throughput 0.870374
Reading from 15590: heap size 248 MB, throughput 0.648065
Reading from 15591: heap size 258 MB, throughput 0.839797
Reading from 15590: heap size 252 MB, throughput 0.645854
Reading from 15591: heap size 261 MB, throughput 0.860979
Reading from 15591: heap size 265 MB, throughput 0.827608
Reading from 15591: heap size 267 MB, throughput 0.795199
Reading from 15591: heap size 268 MB, throughput 0.812899
Reading from 15590: heap size 254 MB, throughput 0.648269
Reading from 15591: heap size 273 MB, throughput 0.778906
Reading from 15590: heap size 293 MB, throughput 0.607279
Reading from 15591: heap size 278 MB, throughput 0.763616
Reading from 15590: heap size 295 MB, throughput 0.650185
Reading from 15591: heap size 281 MB, throughput 0.749979
Reading from 15590: heap size 290 MB, throughput 0.679943
Reading from 15591: heap size 286 MB, throughput 0.818882
Reading from 15590: heap size 293 MB, throughput 0.453962
Reading from 15591: heap size 288 MB, throughput 0.912815
Reading from 15591: heap size 285 MB, throughput 0.875072
Reading from 15590: heap size 338 MB, throughput 0.8514
Reading from 15590: heap size 339 MB, throughput 0.808101
Reading from 15590: heap size 338 MB, throughput 0.796924
Reading from 15590: heap size 340 MB, throughput 0.780069
Reading from 15590: heap size 339 MB, throughput 0.806894
Reading from 15591: heap size 287 MB, throughput 0.726747
Reading from 15591: heap size 335 MB, throughput 0.720465
Reading from 15590: heap size 342 MB, throughput 0.883204
Reading from 15591: heap size 336 MB, throughput 0.782966
Reading from 15591: heap size 341 MB, throughput 0.729673
Reading from 15590: heap size 341 MB, throughput 0.856032
Reading from 15591: heap size 341 MB, throughput 0.779955
Reading from 15590: heap size 343 MB, throughput 0.799776
Reading from 15591: heap size 342 MB, throughput 0.762803
Reading from 15590: heap size 346 MB, throughput 0.749635
Reading from 15591: heap size 344 MB, throughput 0.750102
Reading from 15590: heap size 347 MB, throughput 0.725755
Reading from 15591: heap size 347 MB, throughput 0.746988
Numeric result:
Recommendation: 2 clients, utility 0.403477:
    h1: 1380 MB (U(h) = 0.576845*h^0.00796522)
    h2: 276 MB (U(h) = 0.656613*h^0.001)
Recommendation: 2 clients, utility 0.403477:
    h1: 1380 MB (U(h) = 0.576845*h^0.00796522)
    h2: 276 MB (U(h) = 0.656613*h^0.001)
Reading from 15590: heap size 346 MB, throughput 0.732928
Reading from 15591: heap size 348 MB, throughput 0.678607
Reading from 15591: heap size 354 MB, throughput 0.594764
Reading from 15590: heap size 319 MB, throughput 0.693387
Reading from 15590: heap size 341 MB, throughput 0.744272
Reading from 15591: heap size 354 MB, throughput 0.59852
Reading from 15590: heap size 300 MB, throughput 0.928305
Reading from 15591: heap size 363 MB, throughput 0.953946
Reading from 15590: heap size 340 MB, throughput 0.942422
Reading from 15591: heap size 363 MB, throughput 0.970752
Reading from 15590: heap size 301 MB, throughput 0.951901
Reading from 15591: heap size 365 MB, throughput 0.960608
Reading from 15590: heap size 337 MB, throughput 0.958934
Reading from 15591: heap size 368 MB, throughput 0.96988
Reading from 15590: heap size 294 MB, throughput 0.96501
Reading from 15590: heap size 331 MB, throughput 0.962758
Reading from 15591: heap size 370 MB, throughput 0.975382
Reading from 15590: heap size 296 MB, throughput 0.963738
Reading from 15591: heap size 372 MB, throughput 0.974888
Reading from 15590: heap size 324 MB, throughput 0.965762
Reading from 15591: heap size 368 MB, throughput 0.975689
Reading from 15590: heap size 298 MB, throughput 0.962015
Numeric result:
Recommendation: 2 clients, utility 0.510481:
    h1: 882.171 MB (U(h) = 0.48498*h^0.0517541)
    h2: 773.829 MB (U(h) = 0.547859*h^0.0453995)
Recommendation: 2 clients, utility 0.510481:
    h1: 882.158 MB (U(h) = 0.48498*h^0.0517541)
    h2: 773.842 MB (U(h) = 0.547859*h^0.0453995)
Reading from 15590: heap size 318 MB, throughput 0.963927
Reading from 15591: heap size 372 MB, throughput 0.975802
Reading from 15590: heap size 321 MB, throughput 0.962388
Reading from 15591: heap size 368 MB, throughput 0.975685
Reading from 15590: heap size 318 MB, throughput 0.967712
Reading from 15591: heap size 371 MB, throughput 0.591097
Reading from 15590: heap size 320 MB, throughput 0.967902
Reading from 15590: heap size 318 MB, throughput 0.970399
Reading from 15591: heap size 372 MB, throughput 0.413257
Reading from 15590: heap size 319 MB, throughput 0.431067
Reading from 15590: heap size 319 MB, throughput 0.619384
Reading from 15591: heap size 372 MB, throughput 0.388511
Reading from 15590: heap size 320 MB, throughput 0.569393
Reading from 15591: heap size 374 MB, throughput 0.972365
Reading from 15590: heap size 320 MB, throughput 0.918457
Reading from 15591: heap size 375 MB, throughput 0.974707
Reading from 15590: heap size 361 MB, throughput 0.958976
Numeric result:
Recommendation: 2 clients, utility 0.531871:
    h1: 787.756 MB (U(h) = 0.482009*h^0.0532649)
    h2: 868.244 MB (U(h) = 0.519953*h^0.0587044)
Recommendation: 2 clients, utility 0.531871:
    h1: 787.775 MB (U(h) = 0.482009*h^0.0532649)
    h2: 868.225 MB (U(h) = 0.519953*h^0.0587044)
Reading from 15591: heap size 377 MB, throughput 0.977704
Reading from 15590: heap size 359 MB, throughput 0.975039
Reading from 15591: heap size 378 MB, throughput 0.979643
Reading from 15590: heap size 363 MB, throughput 0.971022
Reading from 15591: heap size 379 MB, throughput 0.958306
Reading from 15591: heap size 381 MB, throughput 0.935762
Reading from 15591: heap size 386 MB, throughput 0.908703
Reading from 15591: heap size 390 MB, throughput 0.879825
Reading from 15590: heap size 366 MB, throughput 0.970853
Reading from 15590: heap size 366 MB, throughput 0.954665
Reading from 15590: heap size 361 MB, throughput 0.908653
Reading from 15590: heap size 366 MB, throughput 0.879168
Reading from 15590: heap size 373 MB, throughput 0.857902
Reading from 15590: heap size 376 MB, throughput 0.831138
Reading from 15590: heap size 384 MB, throughput 0.765753
Reading from 15591: heap size 397 MB, throughput 0.937655
Reading from 15590: heap size 386 MB, throughput 0.953471
Reading from 15591: heap size 398 MB, throughput 0.973935
Reading from 15590: heap size 388 MB, throughput 0.976546
Reading from 15591: heap size 400 MB, throughput 0.982412
Reading from 15590: heap size 391 MB, throughput 0.983568
Reading from 15590: heap size 390 MB, throughput 0.984952
Reading from 15591: heap size 402 MB, throughput 0.98394
Reading from 15590: heap size 393 MB, throughput 0.983696
Numeric result:
Recommendation: 2 clients, utility 0.655364:
    h1: 869.951 MB (U(h) = 0.39867*h^0.100238)
    h2: 786.049 MB (U(h) = 0.456016*h^0.0905699)
Recommendation: 2 clients, utility 0.655364:
    h1: 869.955 MB (U(h) = 0.39867*h^0.100238)
    h2: 786.045 MB (U(h) = 0.456016*h^0.0905699)
Reading from 15591: heap size 401 MB, throughput 0.983634
Reading from 15590: heap size 391 MB, throughput 0.981734
Reading from 15591: heap size 403 MB, throughput 0.98377
Reading from 15590: heap size 394 MB, throughput 0.982868
Reading from 15591: heap size 400 MB, throughput 0.982358
Reading from 15590: heap size 392 MB, throughput 0.981274
Reading from 15590: heap size 394 MB, throughput 0.983647
Reading from 15591: heap size 403 MB, throughput 0.983237
Reading from 15590: heap size 394 MB, throughput 0.983937
Reading from 15591: heap size 399 MB, throughput 0.982076
Reading from 15590: heap size 395 MB, throughput 0.981655
Reading from 15591: heap size 401 MB, throughput 0.984491
Reading from 15590: heap size 394 MB, throughput 0.980399
Numeric result:
Recommendation: 2 clients, utility 0.69019:
    h1: 877.169 MB (U(h) = 0.38051*h^0.111627)
    h2: 778.831 MB (U(h) = 0.440034*h^0.0991157)
Recommendation: 2 clients, utility 0.69019:
    h1: 877.157 MB (U(h) = 0.38051*h^0.111627)
    h2: 778.843 MB (U(h) = 0.440034*h^0.0991157)
Reading from 15591: heap size 399 MB, throughput 0.982905
Reading from 15590: heap size 395 MB, throughput 0.97794
Reading from 15590: heap size 397 MB, throughput 0.978103
Reading from 15591: heap size 401 MB, throughput 0.708291
Reading from 15591: heap size 400 MB, throughput 0.984106
Reading from 15590: heap size 398 MB, throughput 0.985031
Reading from 15591: heap size 401 MB, throughput 0.693555
Reading from 15591: heap size 401 MB, throughput 0.958071
Reading from 15591: heap size 403 MB, throughput 0.92815
Reading from 15590: heap size 400 MB, throughput 0.981064
Reading from 15590: heap size 401 MB, throughput 0.96753
Reading from 15590: heap size 397 MB, throughput 0.945778
Reading from 15591: heap size 408 MB, throughput 0.959874
Reading from 15590: heap size 402 MB, throughput 0.915337
Reading from 15590: heap size 409 MB, throughput 0.909378
Reading from 15590: heap size 411 MB, throughput 0.975789
Reading from 15591: heap size 409 MB, throughput 0.983848
Reading from 15590: heap size 414 MB, throughput 0.983267
Reading from 15591: heap size 414 MB, throughput 0.985986
Reading from 15590: heap size 416 MB, throughput 0.986131
Numeric result:
Recommendation: 2 clients, utility 0.737317:
    h1: 835.63 MB (U(h) = 0.368124*h^0.119618)
    h2: 820.37 MB (U(h) = 0.407306*h^0.117437)
Recommendation: 2 clients, utility 0.737317:
    h1: 835.619 MB (U(h) = 0.368124*h^0.119618)
    h2: 820.381 MB (U(h) = 0.407306*h^0.117437)
Reading from 15591: heap size 415 MB, throughput 0.986734
Reading from 15590: heap size 416 MB, throughput 0.986216
Reading from 15591: heap size 416 MB, throughput 0.985346
Reading from 15590: heap size 419 MB, throughput 0.985559
Reading from 15591: heap size 417 MB, throughput 0.98507
Reading from 15590: heap size 418 MB, throughput 0.985805
Reading from 15591: heap size 415 MB, throughput 0.987104
Reading from 15590: heap size 420 MB, throughput 0.98647
Reading from 15591: heap size 417 MB, throughput 0.895929
Reading from 15590: heap size 419 MB, throughput 0.985949
Reading from 15591: heap size 416 MB, throughput 0.918305
Numeric result:
Recommendation: 2 clients, utility 0.763488:
    h1: 843.574 MB (U(h) = 0.355803*h^0.127811)
    h2: 812.426 MB (U(h) = 0.397576*h^0.123095)
Recommendation: 2 clients, utility 0.763488:
    h1: 843.564 MB (U(h) = 0.355803*h^0.127811)
    h2: 812.436 MB (U(h) = 0.397576*h^0.123095)
Reading from 15590: heap size 420 MB, throughput 0.986081
Reading from 15591: heap size 417 MB, throughput 0.930669
Reading from 15590: heap size 422 MB, throughput 0.983239
Reading from 15590: heap size 422 MB, throughput 0.981638
Reading from 15591: heap size 418 MB, throughput 0.920069
Reading from 15591: heap size 418 MB, throughput 0.884253
Reading from 15591: heap size 417 MB, throughput 0.741152
Reading from 15591: heap size 419 MB, throughput 0.947037
Reading from 15591: heap size 425 MB, throughput 0.970123
Reading from 15590: heap size 424 MB, throughput 0.984948
Reading from 15590: heap size 425 MB, throughput 0.980602
Reading from 15590: heap size 424 MB, throughput 0.962379
Reading from 15590: heap size 426 MB, throughput 0.942957
Reading from 15590: heap size 432 MB, throughput 0.932869
Reading from 15591: heap size 426 MB, throughput 0.983509
Reading from 15590: heap size 435 MB, throughput 0.981016
Reading from 15591: heap size 430 MB, throughput 0.985934
Numeric result:
Recommendation: 2 clients, utility 0.799925:
    h1: 848.491 MB (U(h) = 0.340639*h^0.138198)
    h2: 807.509 MB (U(h) = 0.383415*h^0.131521)
Recommendation: 2 clients, utility 0.799925:
    h1: 848.497 MB (U(h) = 0.340639*h^0.138198)
    h2: 807.503 MB (U(h) = 0.383415*h^0.131521)
Reading from 15590: heap size 441 MB, throughput 0.98807
Reading from 15591: heap size 431 MB, throughput 0.988182
Reading from 15590: heap size 442 MB, throughput 0.985568
Reading from 15591: heap size 431 MB, throughput 0.989865
Reading from 15590: heap size 444 MB, throughput 0.987563
Reading from 15591: heap size 433 MB, throughput 0.812253
Reading from 15590: heap size 446 MB, throughput 0.987516
Reading from 15591: heap size 429 MB, throughput 0.952322
Reading from 15590: heap size 445 MB, throughput 0.98791
Numeric result:
Recommendation: 2 clients, utility 0.822534:
    h1: 840.766 MB (U(h) = 0.334073*h^0.142816)
    h2: 815.234 MB (U(h) = 0.371938*h^0.138479)
Recommendation: 2 clients, utility 0.822534:
    h1: 840.764 MB (U(h) = 0.334073*h^0.142816)
    h2: 815.236 MB (U(h) = 0.371938*h^0.138479)
Reading from 15591: heap size 432 MB, throughput 0.959099
Reading from 15590: heap size 447 MB, throughput 0.986502
Reading from 15591: heap size 433 MB, throughput 0.957699
Reading from 15590: heap size 448 MB, throughput 0.985851
Reading from 15590: heap size 449 MB, throughput 0.985421
Reading from 15591: heap size 433 MB, throughput 0.960517
Reading from 15591: heap size 436 MB, throughput 0.939908
Reading from 15591: heap size 436 MB, throughput 0.969218
Reading from 15591: heap size 477 MB, throughput 0.972347
Reading from 15590: heap size 451 MB, throughput 0.989132
Reading from 15591: heap size 479 MB, throughput 0.988081
Reading from 15590: heap size 452 MB, throughput 0.981887
Reading from 15590: heap size 452 MB, throughput 0.974342
Reading from 15590: heap size 455 MB, throughput 0.958344
Reading from 15590: heap size 461 MB, throughput 0.96992
Numeric result:
Recommendation: 2 clients, utility 0.946984:
    h1: 646.172 MB (U(h) = 0.321193*h^0.151951)
    h2: 1009.83 MB (U(h) = 0.21337*h^0.237466)
Recommendation: 2 clients, utility 0.946984:
    h1: 646.172 MB (U(h) = 0.321193*h^0.151951)
    h2: 1009.83 MB (U(h) = 0.21337*h^0.237466)
Reading from 15591: heap size 486 MB, throughput 0.992197
Reading from 15590: heap size 462 MB, throughput 0.985477
Reading from 15591: heap size 488 MB, throughput 0.992503
Reading from 15590: heap size 466 MB, throughput 0.989097
Reading from 15591: heap size 489 MB, throughput 0.991898
Reading from 15590: heap size 468 MB, throughput 0.989217
Reading from 15591: heap size 490 MB, throughput 0.988594
Reading from 15590: heap size 467 MB, throughput 0.98924
Reading from 15591: heap size 484 MB, throughput 0.988184
Reading from 15590: heap size 469 MB, throughput 0.988383
Numeric result:
Recommendation: 2 clients, utility 1.30526:
    h1: 417.808 MB (U(h) = 0.307755*h^0.161616)
    h2: 1238.19 MB (U(h) = 0.0527893*h^0.47897)
Recommendation: 2 clients, utility 1.30526:
    h1: 417.8 MB (U(h) = 0.307755*h^0.161616)
    h2: 1238.2 MB (U(h) = 0.0527893*h^0.47897)
Reading from 15591: heap size 488 MB, throughput 0.989241
Reading from 15590: heap size 467 MB, throughput 0.989424
Reading from 15591: heap size 478 MB, throughput 0.986803
Reading from 15590: heap size 469 MB, throughput 0.987607
Reading from 15590: heap size 472 MB, throughput 0.985817
Reading from 15591: heap size 467 MB, throughput 0.988547
Reading from 15591: heap size 473 MB, throughput 0.980826
Reading from 15591: heap size 474 MB, throughput 0.965958
Reading from 15591: heap size 475 MB, throughput 0.95055
Reading from 15590: heap size 472 MB, throughput 0.988235
Reading from 15590: heap size 476 MB, throughput 0.981254
Reading from 15590: heap size 476 MB, throughput 0.967566
Reading from 15591: heap size 445 MB, throughput 0.97902
Reading from 15590: heap size 482 MB, throughput 0.97343
Numeric result:
Recommendation: 2 clients, utility 1.48598:
    h1: 380.52 MB (U(h) = 0.296638*h^0.170068)
    h2: 1275.48 MB (U(h) = 0.0309442*h^0.570043)
Recommendation: 2 clients, utility 1.48598:
    h1: 380.528 MB (U(h) = 0.296638*h^0.170068)
    h2: 1275.47 MB (U(h) = 0.0309442*h^0.570043)
Reading from 15591: heap size 463 MB, throughput 0.986225
Reading from 15590: heap size 484 MB, throughput 0.986363
Reading from 15591: heap size 441 MB, throughput 0.985811
Reading from 15590: heap size 487 MB, throughput 0.988279
Reading from 15591: heap size 455 MB, throughput 0.986107
Reading from 15591: heap size 438 MB, throughput 0.985294
Reading from 15590: heap size 489 MB, throughput 0.990002
Reading from 15591: heap size 449 MB, throughput 0.983216
Reading from 15590: heap size 488 MB, throughput 0.989869
Numeric result:
Recommendation: 2 clients, utility 1.44899:
    h1: 405.558 MB (U(h) = 0.2889*h^0.176264)
    h2: 1250.44 MB (U(h) = 0.0360949*h^0.543471)
Recommendation: 2 clients, utility 1.44899:
    h1: 405.557 MB (U(h) = 0.2889*h^0.176264)
    h2: 1250.44 MB (U(h) = 0.0360949*h^0.543471)
Reading from 15591: heap size 434 MB, throughput 0.981956
Reading from 15590: heap size 490 MB, throughput 0.988337
Reading from 15591: heap size 444 MB, throughput 0.98109
Reading from 15591: heap size 430 MB, throughput 0.980063
Reading from 15590: heap size 491 MB, throughput 0.987212
Reading from 15591: heap size 440 MB, throughput 0.977928
Reading from 15590: heap size 491 MB, throughput 0.985886
Reading from 15591: heap size 427 MB, throughput 0.984734
Reading from 15591: heap size 436 MB, throughput 0.975644
Reading from 15591: heap size 425 MB, throughput 0.962335
Reading from 15591: heap size 434 MB, throughput 0.942807
Reading from 15591: heap size 421 MB, throughput 0.928296
Reading from 15590: heap size 493 MB, throughput 0.988548
Reading from 15590: heap size 495 MB, throughput 0.978248
Reading from 15590: heap size 495 MB, throughput 0.967718
Reading from 15591: heap size 431 MB, throughput 0.974939
Numeric result:
Recommendation: 2 clients, utility 1.61342:
    h1: 828.092 MB (U(h) = 0.0532054*h^0.467811)
    h2: 827.908 MB (U(h) = 0.05648*h^0.467716)
Recommendation: 2 clients, utility 1.61342:
    h1: 828.084 MB (U(h) = 0.0532054*h^0.467811)
    h2: 827.916 MB (U(h) = 0.05648*h^0.467716)
Reading from 15590: heap size 497 MB, throughput 0.983421
Reading from 15591: heap size 396 MB, throughput 0.982952
Reading from 15591: heap size 431 MB, throughput 0.98508
Reading from 15590: heap size 505 MB, throughput 0.989564
Reading from 15591: heap size 431 MB, throughput 0.984286
Reading from 15591: heap size 431 MB, throughput 0.984194
Reading from 15590: heap size 506 MB, throughput 0.990381
Reading from 15591: heap size 432 MB, throughput 0.98626
Reading from 15591: heap size 430 MB, throughput 0.985664
Reading from 15590: heap size 507 MB, throughput 0.990774
Reading from 15591: heap size 432 MB, throughput 0.983526
Numeric result:
Recommendation: 2 clients, utility 1.67548:
    h1: 939.553 MB (U(h) = 0.0334235*h^0.547215)
    h2: 716.447 MB (U(h) = 0.076185*h^0.417273)
Recommendation: 2 clients, utility 1.67548:
    h1: 939.553 MB (U(h) = 0.0334235*h^0.547215)
    h2: 716.447 MB (U(h) = 0.076185*h^0.417273)
Reading from 15590: heap size 509 MB, throughput 0.991289
Reading from 15591: heap size 430 MB, throughput 0.983023
Reading from 15591: heap size 431 MB, throughput 0.982442
Reading from 15590: heap size 508 MB, throughput 0.98962
Reading from 15591: heap size 433 MB, throughput 0.981449
Reading from 15591: heap size 433 MB, throughput 0.981901
Reading from 15591: heap size 435 MB, throughput 0.966569
Reading from 15591: heap size 436 MB, throughput 0.945435
Reading from 15591: heap size 440 MB, throughput 0.914538
Reading from 15590: heap size 510 MB, throughput 0.981102
Reading from 15591: heap size 442 MB, throughput 0.910625
Reading from 15591: heap size 452 MB, throughput 0.977689
Reading from 15590: heap size 521 MB, throughput 0.991019
Numeric result:
Recommendation: 2 clients, utility 1.98212:
    h1: 975.328 MB (U(h) = 0.0143883*h^0.689996)
    h2: 680.672 MB (U(h) = 0.0515728*h^0.481549)
Recommendation: 2 clients, utility 1.98212:
    h1: 975.322 MB (U(h) = 0.0143883*h^0.689996)
    h2: 680.678 MB (U(h) = 0.0515728*h^0.481549)
Reading from 15590: heap size 522 MB, throughput 0.983488
Reading from 15590: heap size 525 MB, throughput 0.975079
Reading from 15591: heap size 453 MB, throughput 0.985282
Reading from 15590: heap size 526 MB, throughput 0.987898
Reading from 15591: heap size 457 MB, throughput 0.988992
Reading from 15591: heap size 459 MB, throughput 0.988484
Reading from 15590: heap size 534 MB, throughput 0.990943
Reading from 15591: heap size 458 MB, throughput 0.987111
Reading from 15590: heap size 535 MB, throughput 0.991122
Reading from 15591: heap size 461 MB, throughput 0.986946
Numeric result:
Recommendation: 2 clients, utility 2.40687:
    h1: 1100.12 MB (U(h) = 0.00415051*h^0.899005)
    h2: 555.881 MB (U(h) = 0.0605528*h^0.454265)
Recommendation: 2 clients, utility 2.40687:
    h1: 1100.11 MB (U(h) = 0.00415051*h^0.899005)
    h2: 555.885 MB (U(h) = 0.0605528*h^0.454265)
Reading from 15590: heap size 537 MB, throughput 0.990792
Reading from 15591: heap size 459 MB, throughput 0.986021
Reading from 15591: heap size 461 MB, throughput 0.986276
Reading from 15590: heap size 539 MB, throughput 0.989538
Reading from 15591: heap size 463 MB, throughput 0.985046
Reading from 15590: heap size 541 MB, throughput 0.989762
Reading from 15591: heap size 464 MB, throughput 0.989057
Reading from 15591: heap size 467 MB, throughput 0.982831
Reading from 15591: heap size 467 MB, throughput 0.970305
Reading from 15591: heap size 471 MB, throughput 0.955008
Reading from 15591: heap size 473 MB, throughput 0.980989
Reading from 15590: heap size 542 MB, throughput 0.990531
Numeric result:
Recommendation: 2 clients, utility 2.4562:
    h1: 1217.81 MB (U(h) = 0.00332409*h^0.935799)
    h2: 438.195 MB (U(h) = 0.123481*h^0.336723)
Recommendation: 2 clients, utility 2.4562:
    h1: 1217.8 MB (U(h) = 0.00332409*h^0.935799)
    h2: 438.195 MB (U(h) = 0.123481*h^0.336723)
Reading from 15590: heap size 543 MB, throughput 0.98525
Reading from 15590: heap size 536 MB, throughput 0.975281
Reading from 15591: heap size 481 MB, throughput 0.988211
Reading from 15590: heap size 533 MB, throughput 0.988974
Reading from 15591: heap size 481 MB, throughput 0.989201
Reading from 15590: heap size 504 MB, throughput 0.991216
Reading from 15591: heap size 482 MB, throughput 0.98972
Reading from 15591: heap size 484 MB, throughput 0.98867
Reading from 15590: heap size 517 MB, throughput 0.991054
Numeric result:
Recommendation: 2 clients, utility 2.47486:
    h1: 1333.07 MB (U(h) = 0.00348176*h^0.927722)
    h2: 322.934 MB (U(h) = 0.244833*h^0.224736)
Recommendation: 2 clients, utility 2.47486:
    h1: 1333.07 MB (U(h) = 0.00348176*h^0.927722)
    h2: 322.93 MB (U(h) = 0.244833*h^0.224736)
Reading from 15591: heap size 482 MB, throughput 0.987153
Reading from 15590: heap size 499 MB, throughput 0.989314
Reading from 15591: heap size 484 MB, throughput 0.987593
Reading from 15590: heap size 500 MB, throughput 0.988813
Reading from 15591: heap size 486 MB, throughput 0.985877
Reading from 15590: heap size 490 MB, throughput 0.987819
Reading from 15591: heap size 486 MB, throughput 0.989842
Reading from 15591: heap size 489 MB, throughput 0.984177
Reading from 15591: heap size 489 MB, throughput 0.970805
Reading from 15590: heap size 490 MB, throughput 0.986051
Reading from 15591: heap size 494 MB, throughput 0.96328
Numeric result:
Recommendation: 2 clients, utility 2.13201:
    h1: 1326.72 MB (U(h) = 0.00779601*h^0.793368)
    h2: 329.282 MB (U(h) = 0.290847*h^0.196912)
Recommendation: 2 clients, utility 2.13201:
    h1: 1326.71 MB (U(h) = 0.00779601*h^0.793368)
    h2: 329.287 MB (U(h) = 0.290847*h^0.196912)
Reading from 15590: heap size 482 MB, throughput 0.988077
Reading from 15590: heap size 480 MB, throughput 0.978174
Reading from 15591: heap size 496 MB, throughput 0.980698
Reading from 15590: heap size 475 MB, throughput 0.956694
Reading from 15590: heap size 477 MB, throughput 0.972286
Reading from 15591: heap size 500 MB, throughput 0.987975
Reading from 15590: heap size 444 MB, throughput 0.986823
Reading from 15591: heap size 502 MB, throughput 0.990214
Reading from 15590: heap size 465 MB, throughput 0.988731
Reading from 15591: heap size 502 MB, throughput 0.989135
Reading from 15590: heap size 438 MB, throughput 0.988989
Numeric result:
Recommendation: 2 clients, utility 1.35412:
    h1: 1142.52 MB (U(h) = 0.0949108*h^0.378494)
    h2: 513.479 MB (U(h) = 0.34347*h^0.170103)
Recommendation: 2 clients, utility 1.35412:
    h1: 1142.53 MB (U(h) = 0.0949108*h^0.378494)
    h2: 513.475 MB (U(h) = 0.34347*h^0.170103)
Reading from 15590: heap size 455 MB, throughput 0.987442
Reading from 15591: heap size 504 MB, throughput 0.989558
Reading from 15590: heap size 460 MB, throughput 0.988084
Reading from 15591: heap size 504 MB, throughput 0.988104
Reading from 15590: heap size 459 MB, throughput 0.988613
Reading from 15591: heap size 505 MB, throughput 0.987655
Reading from 15590: heap size 461 MB, throughput 0.986098
Reading from 15591: heap size 506 MB, throughput 0.990814
Reading from 15591: heap size 508 MB, throughput 0.978063
Reading from 15590: heap size 463 MB, throughput 0.982948
Reading from 15591: heap size 506 MB, throughput 0.969285
Reading from 15591: heap size 509 MB, throughput 0.98133
Numeric result:
Recommendation: 2 clients, utility 1.46511:
    h1: 1161.46 MB (U(h) = 0.0584842*h^0.458015)
    h2: 494.536 MB (U(h) = 0.29484*h^0.195022)
Recommendation: 2 clients, utility 1.46511:
    h1: 1161.45 MB (U(h) = 0.0584842*h^0.458015)
    h2: 494.546 MB (U(h) = 0.29484*h^0.195022)
Reading from 15590: heap size 463 MB, throughput 0.988418
Reading from 15590: heap size 465 MB, throughput 0.984365
Reading from 15590: heap size 466 MB, throughput 0.974685
Reading from 15590: heap size 471 MB, throughput 0.95983
Reading from 15591: heap size 516 MB, throughput 0.988461
Reading from 15590: heap size 472 MB, throughput 0.977323
Reading from 15590: heap size 481 MB, throughput 0.987997
Reading from 15591: heap size 518 MB, throughput 0.990196
Reading from 15590: heap size 482 MB, throughput 0.98996
Reading from 15591: heap size 519 MB, throughput 0.99138
Reading from 15590: heap size 485 MB, throughput 0.990849
Numeric result:
Recommendation: 2 clients, utility 1.51406:
    h1: 1107.34 MB (U(h) = 0.0483694*h^0.488971)
    h2: 548.664 MB (U(h) = 0.220466*h^0.242276)
Recommendation: 2 clients, utility 1.51406:
    h1: 1107.34 MB (U(h) = 0.0483694*h^0.488971)
    h2: 548.664 MB (U(h) = 0.220466*h^0.242276)
Reading from 15591: heap size 521 MB, throughput 0.990036
Reading from 15590: heap size 487 MB, throughput 0.990334
Reading from 15591: heap size 521 MB, throughput 0.988937
Reading from 15590: heap size 486 MB, throughput 0.990079
Reading from 15591: heap size 522 MB, throughput 0.988124
Reading from 15590: heap size 488 MB, throughput 0.988438
Reading from 15591: heap size 525 MB, throughput 0.98955
Reading from 15591: heap size 526 MB, throughput 0.984357
Reading from 15591: heap size 526 MB, throughput 0.979358
Reading from 15590: heap size 489 MB, throughput 0.988363
Numeric result:
Recommendation: 2 clients, utility 1.51535:
    h1: 1083.59 MB (U(h) = 0.0480253*h^0.489701)
    h2: 572.412 MB (U(h) = 0.199289*h^0.258687)
Recommendation: 2 clients, utility 1.51535:
    h1: 1083.59 MB (U(h) = 0.0480253*h^0.489701)
    h2: 572.412 MB (U(h) = 0.199289*h^0.258687)
Reading from 15591: heap size 529 MB, throughput 0.987986
Reading from 15590: heap size 490 MB, throughput 0.990103
Reading from 15590: heap size 491 MB, throughput 0.985297
Reading from 15590: heap size 493 MB, throughput 0.971668
Reading from 15590: heap size 497 MB, throughput 0.9712
Reading from 15591: heap size 532 MB, throughput 0.991973
Reading from 15590: heap size 499 MB, throughput 0.990174
Reading from 15591: heap size 533 MB, throughput 0.991624
Reading from 15590: heap size 505 MB, throughput 0.990814
Reading from 15591: heap size 532 MB, throughput 0.990185
Numeric result:
Recommendation: 2 clients, utility 1.57998:
    h1: 1120.16 MB (U(h) = 0.0354204*h^0.539164)
    h2: 535.838 MB (U(h) = 0.200209*h^0.257913)
Recommendation: 2 clients, utility 1.57998:
    h1: 1120.16 MB (U(h) = 0.0354204*h^0.539164)
    h2: 535.837 MB (U(h) = 0.200209*h^0.257913)
Reading from 15590: heap size 507 MB, throughput 0.99064
Reading from 15591: heap size 534 MB, throughput 0.989878
Reading from 15590: heap size 508 MB, throughput 0.988727
Reading from 15591: heap size 535 MB, throughput 0.989472
Reading from 15590: heap size 510 MB, throughput 0.988837
Reading from 15590: heap size 511 MB, throughput 0.98837
Reading from 15591: heap size 536 MB, throughput 0.99083
Reading from 15591: heap size 538 MB, throughput 0.986175
Reading from 15591: heap size 539 MB, throughput 0.976014
Numeric result:
Recommendation: 2 clients, utility 1.43098:
    h1: 993.293 MB (U(h) = 0.0742268*h^0.418031)
    h2: 662.707 MB (U(h) = 0.175923*h^0.278904)
Recommendation: 2 clients, utility 1.43098:
    h1: 993.292 MB (U(h) = 0.0742268*h^0.418031)
    h2: 662.708 MB (U(h) = 0.175923*h^0.278904)
Reading from 15590: heap size 512 MB, throughput 0.986935
Reading from 15591: heap size 544 MB, throughput 0.986715
Reading from 15590: heap size 515 MB, throughput 0.986054
Reading from 15590: heap size 516 MB, throughput 0.978954
Reading from 15590: heap size 521 MB, throughput 0.967215
Reading from 15591: heap size 546 MB, throughput 0.990936
Reading from 15590: heap size 522 MB, throughput 0.987311
Reading from 15591: heap size 548 MB, throughput 0.99181
Reading from 15590: heap size 529 MB, throughput 0.989956
Numeric result:
Recommendation: 2 clients, utility 1.22097:
    h1: 1026.35 MB (U(h) = 0.194837*h^0.261127)
    h2: 629.649 MB (U(h) = 0.364999*h^0.160199)
Recommendation: 2 clients, utility 1.22097:
    h1: 1026.35 MB (U(h) = 0.194837*h^0.261127)
    h2: 629.655 MB (U(h) = 0.364999*h^0.160199)
Reading from 15591: heap size 550 MB, throughput 0.991149
Reading from 15590: heap size 531 MB, throughput 0.990782
Reading from 15591: heap size 548 MB, throughput 0.990415
Reading from 15590: heap size 533 MB, throughput 0.991346
Reading from 15591: heap size 551 MB, throughput 0.990097
Reading from 15590: heap size 536 MB, throughput 0.988986
Reading from 15591: heap size 553 MB, throughput 0.991263
Reading from 15591: heap size 554 MB, throughput 0.985641
Reading from 15590: heap size 539 MB, throughput 0.988998
Numeric result:
Recommendation: 2 clients, utility 1.24572:
    h1: 1067.89 MB (U(h) = 0.162984*h^0.2897)
    h2: 588.115 MB (U(h) = 0.366487*h^0.159545)
Recommendation: 2 clients, utility 1.24572:
    h1: 1067.89 MB (U(h) = 0.162984*h^0.2897)
    h2: 588.111 MB (U(h) = 0.366487*h^0.159545)
Client 15591 died
Clients: 1
Reading from 15590: heap size 539 MB, throughput 0.99164
Reading from 15590: heap size 541 MB, throughput 0.98748
Reading from 15590: heap size 543 MB, throughput 0.977939
Client 15590 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
