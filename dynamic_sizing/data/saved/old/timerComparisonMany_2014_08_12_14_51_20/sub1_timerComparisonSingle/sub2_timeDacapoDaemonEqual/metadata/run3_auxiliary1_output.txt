economemd
    total memory: 200 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_08_12_14_51_20/sub1_timerComparisonSingle/sub2_timeDacapoDaemonEqual/daemon_run3_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 9047: heap size 9 MB, throughput 0.979573
Clients: 1
Reading from 9046: heap size 9 MB, throughput 0.97654
Clients: 2
Reading from 9047: heap size 9 MB, throughput 0.951308
Reading from 9046: heap size 9 MB, throughput 0.962361
Reading from 9047: heap size 11 MB, throughput 0.965933
Reading from 9046: heap size 11 MB, throughput 0.974012
Reading from 9047: heap size 11 MB, throughput 0.955404
Reading from 9046: heap size 11 MB, throughput 0.974565
Reading from 9046: heap size 15 MB, throughput 0.755584
Reading from 9047: heap size 15 MB, throughput 0.700372
Reading from 9047: heap size 23 MB, throughput 0.998746
Reading from 9046: heap size 23 MB, throughput 0.988076
Reading from 9046: heap size 26 MB, throughput 0.997226
Reading from 9047: heap size 26 MB, throughput 0.997843
Reading from 9046: heap size 28 MB, throughput 0.997676
Reading from 9047: heap size 28 MB, throughput 0.997751
Reading from 9046: heap size 30 MB, throughput 0.99848
Reading from 9047: heap size 32 MB, throughput 0.998456
Reading from 9046: heap size 31 MB, throughput 0.997818
Reading from 9047: heap size 32 MB, throughput 0.997407
Reading from 9046: heap size 31 MB, throughput 0.997757
Equal recommendation: 100 MB each
Reading from 9047: heap size 33 MB, throughput 0.997812
Reading from 9046: heap size 32 MB, throughput 0.997297
Reading from 9047: heap size 33 MB, throughput 0.996098
Reading from 9046: heap size 32 MB, throughput 0.997072
Reading from 9047: heap size 34 MB, throughput 0.996771
Reading from 9046: heap size 32 MB, throughput 0.993745
Reading from 9046: heap size 32 MB, throughput 0.996377
Reading from 9047: heap size 34 MB, throughput 0.99665
Reading from 9046: heap size 33 MB, throughput 0.993864
Reading from 9047: heap size 35 MB, throughput 0.994878
Equal recommendation: 100 MB each
Reading from 9046: heap size 33 MB, throughput 0.995825
Reading from 9047: heap size 35 MB, throughput 0.994295
Reading from 9046: heap size 34 MB, throughput 0.995682
Reading from 9047: heap size 35 MB, throughput 0.995894
Reading from 9046: heap size 35 MB, throughput 0.994601
Reading from 9047: heap size 36 MB, throughput 0.990622
Reading from 9046: heap size 35 MB, throughput 0.995371
Reading from 9047: heap size 36 MB, throughput 0.995079
Reading from 9046: heap size 35 MB, throughput 0.99387
Reading from 9046: heap size 36 MB, throughput 0.997385
Reading from 9047: heap size 37 MB, throughput 0.995153
Equal recommendation: 100 MB each
Reading from 9046: heap size 35 MB, throughput 0.99248
Reading from 9047: heap size 37 MB, throughput 0.990934
Reading from 9046: heap size 36 MB, throughput 0.996503
Reading from 9047: heap size 38 MB, throughput 0.997493
Reading from 9046: heap size 37 MB, throughput 0.996702
Reading from 9047: heap size 40 MB, throughput 0.99691
Reading from 9046: heap size 38 MB, throughput 0.995934
Reading from 9047: heap size 41 MB, throughput 0.997137
Reading from 9046: heap size 39 MB, throughput 0.997566
Reading from 9046: heap size 39 MB, throughput 0.996968
Reading from 9047: heap size 41 MB, throughput 0.997609
Reading from 9046: heap size 40 MB, throughput 0.996006
Equal recommendation: 100 MB each
Reading from 9047: heap size 42 MB, throughput 0.997836
Reading from 9046: heap size 40 MB, throughput 0.997592
Reading from 9047: heap size 42 MB, throughput 0.998155
Reading from 9046: heap size 40 MB, throughput 0.997365
Reading from 9047: heap size 42 MB, throughput 0.997188
Reading from 9046: heap size 40 MB, throughput 0.994577
Reading from 9047: heap size 42 MB, throughput 0.997693
Reading from 9046: heap size 40 MB, throughput 0.998276
Reading from 9046: heap size 40 MB, throughput 0.99776
Reading from 9047: heap size 36 MB, throughput 0.994005
Equal recommendation: 100 MB each
Reading from 9046: heap size 39 MB, throughput 0.997433
Reading from 9047: heap size 42 MB, throughput 0.998293
Reading from 9046: heap size 40 MB, throughput 0.997995
Reading from 9047: heap size 42 MB, throughput 0.998145
Reading from 9046: heap size 40 MB, throughput 0.978703
Reading from 9047: heap size 41 MB, throughput 0.998273
Reading from 9046: heap size 42 MB, throughput 0.998554
Reading from 9047: heap size 41 MB, throughput 0.99615
Reading from 9046: heap size 43 MB, throughput 0.997492
Reading from 9047: heap size 41 MB, throughput 0.998826
Reading from 9046: heap size 43 MB, throughput 0.997759
Equal recommendation: 100 MB each
Reading from 9047: heap size 41 MB, throughput 0.997055
Reading from 9046: heap size 42 MB, throughput 0.998368
Reading from 9047: heap size 41 MB, throughput 0.998605
Reading from 9046: heap size 42 MB, throughput 0.997491
Reading from 9046: heap size 42 MB, throughput 0.998465
Reading from 9047: heap size 41 MB, throughput 0.998235
Reading from 9046: heap size 42 MB, throughput 0.99858
Reading from 9047: heap size 41 MB, throughput 0.998551
Reading from 9046: heap size 42 MB, throughput 0.997773
Equal recommendation: 100 MB each
Reading from 9047: heap size 41 MB, throughput 0.997856
Reading from 9046: heap size 42 MB, throughput 0.998088
Reading from 9047: heap size 41 MB, throughput 0.998769
Reading from 9046: heap size 42 MB, throughput 0.998001
Reading from 9047: heap size 41 MB, throughput 0.996527
Reading from 9046: heap size 42 MB, throughput 0.997792
Reading from 9047: heap size 41 MB, throughput 0.998388
Reading from 9046: heap size 42 MB, throughput 0.996979
Equal recommendation: 100 MB each
Reading from 9047: heap size 41 MB, throughput 0.998349
Reading from 9046: heap size 42 MB, throughput 0.99769
Reading from 9047: heap size 41 MB, throughput 0.995218
Reading from 9046: heap size 43 MB, throughput 0.99791
Reading from 9047: heap size 41 MB, throughput 0.997317
Reading from 9046: heap size 43 MB, throughput 0.997829
Reading from 9047: heap size 42 MB, throughput 0.997228
Reading from 9046: heap size 43 MB, throughput 0.998572
Reading from 9047: heap size 42 MB, throughput 0.998209
Equal recommendation: 100 MB each
Reading from 9046: heap size 43 MB, throughput 0.998149
Reading from 9046: heap size 43 MB, throughput 0.996691
Reading from 9047: heap size 41 MB, throughput 0.99883
Reading from 9046: heap size 43 MB, throughput 0.997805
Reading from 9047: heap size 42 MB, throughput 0.998666
Reading from 9046: heap size 43 MB, throughput 0.99845
Reading from 9047: heap size 42 MB, throughput 0.998639
Equal recommendation: 100 MB each
Reading from 9046: heap size 43 MB, throughput 0.99829
Reading from 9047: heap size 42 MB, throughput 0.998668
Reading from 9046: heap size 44 MB, throughput 0.99847
Reading from 9047: heap size 43 MB, throughput 0.998456
Reading from 9046: heap size 44 MB, throughput 0.998743
Reading from 9047: heap size 43 MB, throughput 0.995456
Reading from 9046: heap size 44 MB, throughput 0.998351
Reading from 9047: heap size 42 MB, throughput 0.998606
Equal recommendation: 100 MB each
Reading from 9046: heap size 44 MB, throughput 0.993698
Reading from 9047: heap size 43 MB, throughput 0.998768
Reading from 9046: heap size 45 MB, throughput 0.99827
Reading from 9047: heap size 43 MB, throughput 0.998865
Reading from 9046: heap size 45 MB, throughput 0.998485
Reading from 9047: heap size 42 MB, throughput 0.998607
Reading from 9046: heap size 45 MB, throughput 0.998863
Equal recommendation: 100 MB each
Reading from 9047: heap size 42 MB, throughput 0.998551
Reading from 9046: heap size 45 MB, throughput 0.99521
Reading from 9047: heap size 41 MB, throughput 0.995735
Reading from 9046: heap size 45 MB, throughput 0.998592
Reading from 9047: heap size 41 MB, throughput 0.998638
Reading from 9046: heap size 45 MB, throughput 0.998522
Reading from 9047: heap size 40 MB, throughput 0.998442
Reading from 9046: heap size 46 MB, throughput 0.998205
Equal recommendation: 100 MB each
Reading from 9047: heap size 40 MB, throughput 0.998153
Reading from 9046: heap size 46 MB, throughput 0.998928
Reading from 9047: heap size 39 MB, throughput 0.997668
Reading from 9046: heap size 46 MB, throughput 0.997972
Reading from 9047: heap size 39 MB, throughput 0.998026
Reading from 9047: heap size 38 MB, throughput 0.993169
Reading from 9046: heap size 46 MB, throughput 0.998495
Equal recommendation: 100 MB each
Reading from 9047: heap size 38 MB, throughput 0.998133
Reading from 9046: heap size 47 MB, throughput 0.998737
Reading from 9047: heap size 37 MB, throughput 0.998374
Reading from 9046: heap size 47 MB, throughput 0.99899
Reading from 9047: heap size 37 MB, throughput 0.997609
Reading from 9047: heap size 36 MB, throughput 0.998379
Reading from 9046: heap size 47 MB, throughput 0.998355
Reading from 9047: heap size 35 MB, throughput 0.998068
Equal recommendation: 100 MB each
Reading from 9046: heap size 47 MB, throughput 0.998808
Reading from 9047: heap size 35 MB, throughput 0.998495
Reading from 9047: heap size 35 MB, throughput 0.998337
Reading from 9046: heap size 48 MB, throughput 0.999127
Reading from 9047: heap size 34 MB, throughput 0.997773
Reading from 9047: heap size 34 MB, throughput 0.998286
Reading from 9046: heap size 48 MB, throughput 0.999106
Reading from 9047: heap size 33 MB, throughput 0.996338
Reading from 9047: heap size 34 MB, throughput 0.997582
Reading from 9046: heap size 48 MB, throughput 0.99792
Equal recommendation: 100 MB each
Reading from 9047: heap size 34 MB, throughput 0.998197
Reading from 9046: heap size 48 MB, throughput 0.998744
Reading from 9047: heap size 33 MB, throughput 0.997853
Reading from 9047: heap size 33 MB, throughput 0.932007
Reading from 9047: heap size 34 MB, throughput 0.999619
Reading from 9046: heap size 49 MB, throughput 0.999236
Reading from 9047: heap size 36 MB, throughput 0.999021
Reading from 9047: heap size 37 MB, throughput 0.992952
Reading from 9046: heap size 49 MB, throughput 0.999006
Equal recommendation: 100 MB each
Reading from 9047: heap size 38 MB, throughput 0.997955
Reading from 9047: heap size 38 MB, throughput 0.998312
Reading from 9046: heap size 49 MB, throughput 0.996362
Reading from 9047: heap size 38 MB, throughput 0.99461
Reading from 9047: heap size 38 MB, throughput 0.995369
Reading from 9046: heap size 49 MB, throughput 0.998777
Reading from 9047: heap size 38 MB, throughput 0.997752
Reading from 9047: heap size 38 MB, throughput 0.995928
Reading from 9047: heap size 38 MB, throughput 0.995432
Reading from 9046: heap size 50 MB, throughput 0.998982
Equal recommendation: 100 MB each
Reading from 9047: heap size 37 MB, throughput 0.997133
Reading from 9047: heap size 38 MB, throughput 0.995344
Reading from 9046: heap size 50 MB, throughput 0.999137
Reading from 9047: heap size 37 MB, throughput 0.996837
Reading from 9047: heap size 38 MB, throughput 0.994968
Reading from 9046: heap size 50 MB, throughput 0.999201
Reading from 9047: heap size 37 MB, throughput 0.994021
Reading from 9046: heap size 50 MB, throughput 0.999029
Reading from 9047: heap size 38 MB, throughput 0.994505
Equal recommendation: 100 MB each
Reading from 9047: heap size 38 MB, throughput 0.991675
Reading from 9046: heap size 50 MB, throughput 0.996501
Reading from 9047: heap size 38 MB, throughput 0.996367
Reading from 9047: heap size 38 MB, throughput 0.995753
Reading from 9046: heap size 50 MB, throughput 0.99922
Reading from 9047: heap size 40 MB, throughput 0.995342
Reading from 9047: heap size 40 MB, throughput 0.996953
Reading from 9047: heap size 41 MB, throughput 0.995228
Reading from 9046: heap size 51 MB, throughput 0.999026
Equal recommendation: 100 MB each
Reading from 9047: heap size 42 MB, throughput 0.995887
Reading from 9047: heap size 42 MB, throughput 0.995458
Reading from 9046: heap size 51 MB, throughput 0.999022
Reading from 9047: heap size 43 MB, throughput 0.994979
Reading from 9047: heap size 43 MB, throughput 0.995141
Reading from 9046: heap size 51 MB, throughput 0.99923
Reading from 9047: heap size 44 MB, throughput 0.995034
Reading from 9047: heap size 44 MB, throughput 0.99328
Equal recommendation: 100 MB each
Reading from 9046: heap size 51 MB, throughput 0.996196
Reading from 9047: heap size 45 MB, throughput 0.994617
Reading from 9047: heap size 45 MB, throughput 0.994469
Reading from 9046: heap size 52 MB, throughput 0.998756
Reading from 9047: heap size 45 MB, throughput 0.99286
Reading from 9047: heap size 45 MB, throughput 0.994101
Reading from 9047: heap size 45 MB, throughput 0.961464
Reading from 9046: heap size 52 MB, throughput 0.997029
Equal recommendation: 100 MB each
Reading from 9047: heap size 49 MB, throughput 0.999306
Reading from 9046: heap size 52 MB, throughput 0.999035
Reading from 9047: heap size 49 MB, throughput 0.999354
Reading from 9047: heap size 50 MB, throughput 0.999288
Reading from 9046: heap size 52 MB, throughput 0.998644
Reading from 9047: heap size 50 MB, throughput 0.999355
Reading from 9047: heap size 51 MB, throughput 0.99893
Reading from 9046: heap size 53 MB, throughput 0.997059
Equal recommendation: 100 MB each
Reading from 9047: heap size 51 MB, throughput 0.998977
Reading from 9047: heap size 51 MB, throughput 0.998746
Reading from 9046: heap size 53 MB, throughput 0.999248
Reading from 9047: heap size 50 MB, throughput 0.998544
Reading from 9047: heap size 50 MB, throughput 0.996803
Reading from 9046: heap size 53 MB, throughput 0.999234
Reading from 9047: heap size 49 MB, throughput 0.998911
Equal recommendation: 100 MB each
Reading from 9047: heap size 41 MB, throughput 0.998915
Reading from 9046: heap size 53 MB, throughput 0.999211
Reading from 9047: heap size 48 MB, throughput 0.991732
Reading from 9047: heap size 49 MB, throughput 0.998922
Reading from 9046: heap size 54 MB, throughput 0.999263
Reading from 9047: heap size 48 MB, throughput 0.995392
Reading from 9047: heap size 48 MB, throughput 0.997651
Reading from 9046: heap size 54 MB, throughput 0.999184
Equal recommendation: 100 MB each
Reading from 9047: heap size 50 MB, throughput 0.997089
Reading from 9047: heap size 51 MB, throughput 0.99804
Reading from 9046: heap size 54 MB, throughput 0.999379
Reading from 9047: heap size 51 MB, throughput 0.995843
Reading from 9046: heap size 54 MB, throughput 0.999118
Reading from 9047: heap size 52 MB, throughput 0.997566
Reading from 9047: heap size 53 MB, throughput 0.997681
Equal recommendation: 100 MB each
Reading from 9046: heap size 55 MB, throughput 0.999018
Reading from 9047: heap size 53 MB, throughput 0.995817
Reading from 9047: heap size 53 MB, throughput 0.997292
Reading from 9046: heap size 55 MB, throughput 0.999302
Reading from 9047: heap size 53 MB, throughput 0.99753
Reading from 9047: heap size 52 MB, throughput 0.997584
Reading from 9046: heap size 55 MB, throughput 0.998673
Equal recommendation: 100 MB each
Reading from 9047: heap size 52 MB, throughput 0.997431
Reading from 9047: heap size 51 MB, throughput 0.996863
Reading from 9046: heap size 55 MB, throughput 0.999128
Reading from 9047: heap size 52 MB, throughput 0.997127
Reading from 9046: heap size 56 MB, throughput 0.998947
Reading from 9047: heap size 52 MB, throughput 0.996705
Equal recommendation: 100 MB each
Reading from 9047: heap size 53 MB, throughput 0.995388
Reading from 9046: heap size 56 MB, throughput 0.998914
Reading from 9047: heap size 54 MB, throughput 0.998033
Reading from 9047: heap size 54 MB, throughput 0.998657
Reading from 9046: heap size 56 MB, throughput 0.999076
Reading from 9047: heap size 54 MB, throughput 0.998412
Equal recommendation: 100 MB each
Reading from 9047: heap size 54 MB, throughput 0.998487
Reading from 9046: heap size 56 MB, throughput 0.999071
Reading from 9047: heap size 53 MB, throughput 0.998635
Reading from 9046: heap size 57 MB, throughput 0.999629
Reading from 9047: heap size 54 MB, throughput 0.991935
Reading from 9047: heap size 53 MB, throughput 0.998648
Reading from 9046: heap size 57 MB, throughput 0.999093
Equal recommendation: 100 MB each
Reading from 9047: heap size 54 MB, throughput 0.998659
Reading from 9047: heap size 54 MB, throughput 0.998701
Reading from 9046: heap size 58 MB, throughput 0.999118
Reading from 9047: heap size 54 MB, throughput 0.998849
Reading from 9046: heap size 58 MB, throughput 0.999008
Reading from 9047: heap size 53 MB, throughput 0.998855
Equal recommendation: 100 MB each
Reading from 9047: heap size 53 MB, throughput 0.998115
Reading from 9046: heap size 58 MB, throughput 0.999418
Reading from 9047: heap size 53 MB, throughput 0.998778
Reading from 9047: heap size 53 MB, throughput 0.999045
Reading from 9046: heap size 58 MB, throughput 0.999264
Reading from 9047: heap size 53 MB, throughput 0.998646
Equal recommendation: 100 MB each
Client 9046 died
Clients: 1
Reading from 9047: heap size 53 MB, throughput 0.998599
Reading from 9047: heap size 53 MB, throughput 0.99813
Client 9047 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
