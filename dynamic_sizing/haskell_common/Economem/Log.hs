module Economem.Log (Log(..),
                     putLog) where

import Economem.Common
import Economem.Files

data Log = Log { logName :: String,
                 logFile :: FilePath,
                 logParent :: Maybe Log }

instance Path Log where
  getPath = logFile

putLog :: Log -> String -> IO ()
putLog l message = do
  timestamp <- nowPretty
  putLogInternal l (timestamp ++ "    " ++ logName l ++ ": " ++ message)


putLogInternal :: Log -> String -> IO ()
putLogInternal l message = do
  appendFile (logFile l) (message ++ "\n")
  case logParent l of
    Nothing -> putStrLn message
    Just parent -> putLogInternal parent message
