economemd
    total memory: 3759 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub1_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 629: heap size 9 MB, throughput 0.989946
Clients: 1
Client 629 has a minimum heap size of 30 MB
Reading from 628: heap size 9 MB, throughput 0.966649
Clients: 2
Client 628 has a minimum heap size of 1223 MB
Reading from 629: heap size 9 MB, throughput 0.985324
Reading from 629: heap size 9 MB, throughput 0.980746
Reading from 628: heap size 9 MB, throughput 0.969224
Reading from 629: heap size 9 MB, throughput 0.970546
Reading from 629: heap size 11 MB, throughput 0.942275
Reading from 629: heap size 11 MB, throughput 0.901446
Reading from 629: heap size 16 MB, throughput 0.796016
Reading from 629: heap size 16 MB, throughput 0.763643
Reading from 629: heap size 24 MB, throughput 0.805349
Reading from 629: heap size 24 MB, throughput 0.807192
Reading from 629: heap size 34 MB, throughput 0.863899
Reading from 629: heap size 34 MB, throughput 0.877123
Reading from 629: heap size 50 MB, throughput 0.925959
Reading from 629: heap size 50 MB, throughput 0.931157
Reading from 628: heap size 11 MB, throughput 0.971178
Reading from 629: heap size 75 MB, throughput 0.94909
Reading from 629: heap size 75 MB, throughput 0.951609
Reading from 629: heap size 116 MB, throughput 0.862268
Reading from 628: heap size 11 MB, throughput 0.977874
Reading from 629: heap size 116 MB, throughput 0.97599
Reading from 628: heap size 15 MB, throughput 0.91715
Reading from 628: heap size 18 MB, throughput 0.907308
Reading from 628: heap size 23 MB, throughput 0.824703
Reading from 628: heap size 28 MB, throughput 0.692953
Reading from 628: heap size 37 MB, throughput 0.7961
Reading from 628: heap size 39 MB, throughput 0.735183
Reading from 628: heap size 40 MB, throughput 0.456687
Reading from 628: heap size 41 MB, throughput 0.833026
Reading from 628: heap size 43 MB, throughput 0.577254
Reading from 628: heap size 58 MB, throughput 0.760613
Reading from 629: heap size 159 MB, throughput 0.957658
Reading from 628: heap size 62 MB, throughput 0.751256
Reading from 628: heap size 64 MB, throughput 0.743166
Reading from 628: heap size 69 MB, throughput 0.66837
Reading from 628: heap size 71 MB, throughput 0.576564
Reading from 629: heap size 171 MB, throughput 0.835909
Reading from 628: heap size 76 MB, throughput 0.363585
Reading from 628: heap size 100 MB, throughput 0.626728
Reading from 628: heap size 103 MB, throughput 0.660219
Reading from 628: heap size 105 MB, throughput 0.635442
Reading from 629: heap size 177 MB, throughput 0.967369
Reading from 628: heap size 109 MB, throughput 0.39063
Reading from 628: heap size 135 MB, throughput 0.687088
Reading from 628: heap size 144 MB, throughput 0.705737
Reading from 628: heap size 145 MB, throughput 0.661936
Reading from 628: heap size 147 MB, throughput 0.65256
Reading from 629: heap size 211 MB, throughput 0.854112
Reading from 628: heap size 153 MB, throughput 0.686145
Reading from 628: heap size 158 MB, throughput 0.599969
Reading from 628: heap size 164 MB, throughput 0.61345
Reading from 628: heap size 170 MB, throughput 0.39621
Reading from 628: heap size 202 MB, throughput 0.501286
Reading from 629: heap size 240 MB, throughput 0.952276
Reading from 628: heap size 210 MB, throughput 0.225821
Reading from 628: heap size 246 MB, throughput 0.511297
Reading from 628: heap size 246 MB, throughput 0.560541
Reading from 628: heap size 249 MB, throughput 0.582561
Reading from 628: heap size 251 MB, throughput 0.581351
Reading from 628: heap size 254 MB, throughput 0.587101
Reading from 629: heap size 243 MB, throughput 0.958557
Reading from 628: heap size 258 MB, throughput 0.656903
Reading from 628: heap size 265 MB, throughput 0.627744
Reading from 628: heap size 268 MB, throughput 0.595146
Reading from 629: heap size 257 MB, throughput 0.952463
Reading from 628: heap size 275 MB, throughput 0.403917
Reading from 628: heap size 318 MB, throughput 0.568883
Reading from 628: heap size 323 MB, throughput 0.602803
Reading from 629: heap size 289 MB, throughput 0.954469
Reading from 628: heap size 326 MB, throughput 0.566013
Reading from 628: heap size 327 MB, throughput 0.321435
Reading from 628: heap size 373 MB, throughput 0.538557
Reading from 628: heap size 315 MB, throughput 0.575977
Reading from 628: heap size 367 MB, throughput 0.582676
Reading from 629: heap size 294 MB, throughput 0.907366
Numeric result:
Recommendation: 2 clients, utility 1.26684:
    h1: 2536 MB (U(h) = 0.840238*h^0.0177853)
    h2: 1223 MB (U(h) = 1.30223*h^0.001)
Recommendation: 2 clients, utility 1.26684:
    h1: 2536 MB (U(h) = 0.840238*h^0.0177853)
    h2: 1223 MB (U(h) = 1.30223*h^0.001)
Reading from 628: heap size 370 MB, throughput 0.239466
Reading from 628: heap size 420 MB, throughput 0.597324
Reading from 629: heap size 321 MB, throughput 0.960346
Reading from 628: heap size 421 MB, throughput 0.596761
Reading from 628: heap size 421 MB, throughput 0.570993
Reading from 628: heap size 421 MB, throughput 0.533705
Reading from 628: heap size 423 MB, throughput 0.530272
Reading from 628: heap size 431 MB, throughput 0.526947
Reading from 628: heap size 435 MB, throughput 0.543231
Reading from 629: heap size 326 MB, throughput 0.988346
Reading from 628: heap size 444 MB, throughput 0.521497
Reading from 628: heap size 449 MB, throughput 0.529457
Reading from 628: heap size 459 MB, throughput 0.516329
Reading from 629: heap size 354 MB, throughput 0.989022
Reading from 628: heap size 464 MB, throughput 0.36127
Reading from 628: heap size 520 MB, throughput 0.415651
Reading from 628: heap size 532 MB, throughput 0.452674
Reading from 628: heap size 532 MB, throughput 0.432047
Reading from 629: heap size 356 MB, throughput 0.981385
Reading from 628: heap size 538 MB, throughput 0.212597
Reading from 628: heap size 604 MB, throughput 0.47165
Reading from 628: heap size 591 MB, throughput 0.541642
Reading from 629: heap size 383 MB, throughput 0.982687
Reading from 628: heap size 598 MB, throughput 0.582583
Reading from 628: heap size 599 MB, throughput 0.593779
Reading from 628: heap size 601 MB, throughput 0.523781
Reading from 628: heap size 604 MB, throughput 0.547574
Reading from 629: heap size 387 MB, throughput 0.97497
Reading from 628: heap size 613 MB, throughput 0.331543
Reading from 628: heap size 687 MB, throughput 0.615832
Reading from 629: heap size 416 MB, throughput 0.97409
Reading from 628: heap size 694 MB, throughput 0.768722
Reading from 628: heap size 694 MB, throughput 0.794594
Reading from 629: heap size 426 MB, throughput 0.983546
Numeric result:
Recommendation: 2 clients, utility 1.2632:
    h1: 2536 MB (U(h) = 0.80904*h^0.0285992)
    h2: 1223 MB (U(h) = 1.23896*h^0.001)
Recommendation: 2 clients, utility 1.2632:
    h1: 2536 MB (U(h) = 0.80904*h^0.0285992)
    h2: 1223 MB (U(h) = 1.23896*h^0.001)
Reading from 628: heap size 698 MB, throughput 0.831395
Reading from 628: heap size 702 MB, throughput 0.766696
Reading from 628: heap size 712 MB, throughput 0.701441
Reading from 629: heap size 458 MB, throughput 0.989261
Reading from 628: heap size 718 MB, throughput 0.480775
Reading from 628: heap size 788 MB, throughput 0.449771
Reading from 629: heap size 458 MB, throughput 0.983777
Reading from 628: heap size 777 MB, throughput 0.391464
Reading from 628: heap size 784 MB, throughput 0.378394
Reading from 629: heap size 483 MB, throughput 0.983307
Reading from 628: heap size 781 MB, throughput 0.260816
Reading from 628: heap size 861 MB, throughput 0.60203
Reading from 628: heap size 865 MB, throughput 0.613042
Reading from 628: heap size 867 MB, throughput 0.576941
Reading from 628: heap size 874 MB, throughput 0.727804
Reading from 629: heap size 486 MB, throughput 0.979473
Reading from 628: heap size 875 MB, throughput 0.714678
Reading from 628: heap size 878 MB, throughput 0.799324
Reading from 628: heap size 879 MB, throughput 0.469241
Reading from 629: heap size 517 MB, throughput 0.986805
Reading from 628: heap size 974 MB, throughput 0.723185
Reading from 628: heap size 976 MB, throughput 0.84815
Reading from 628: heap size 977 MB, throughput 0.838288
Reading from 628: heap size 829 MB, throughput 0.808118
Reading from 628: heap size 965 MB, throughput 0.820869
Reading from 628: heap size 835 MB, throughput 0.806867
Reading from 628: heap size 956 MB, throughput 0.818749
Reading from 628: heap size 841 MB, throughput 0.813294
Reading from 629: heap size 520 MB, throughput 0.985096
Reading from 628: heap size 952 MB, throughput 0.82912
Reading from 628: heap size 957 MB, throughput 0.833077
Reading from 628: heap size 947 MB, throughput 0.843839
Reading from 628: heap size 953 MB, throughput 0.845338
Numeric result:
Recommendation: 2 clients, utility 0.827425:
    h1: 2536 MB (U(h) = 0.801755*h^0.03097)
    h2: 1223 MB (U(h) = 0.803846*h^0.001)
Recommendation: 2 clients, utility 0.827425:
    h1: 2536 MB (U(h) = 0.801755*h^0.03097)
    h2: 1223 MB (U(h) = 0.803846*h^0.001)
Reading from 629: heap size 552 MB, throughput 0.989969
Reading from 628: heap size 944 MB, throughput 0.968943
Reading from 628: heap size 949 MB, throughput 0.956346
Reading from 628: heap size 945 MB, throughput 0.91665
Reading from 629: heap size 554 MB, throughput 0.98699
Reading from 628: heap size 948 MB, throughput 0.888686
Reading from 628: heap size 947 MB, throughput 0.857962
Reading from 628: heap size 950 MB, throughput 0.829076
Reading from 628: heap size 950 MB, throughput 0.826264
Reading from 628: heap size 953 MB, throughput 0.82351
Reading from 628: heap size 954 MB, throughput 0.818752
Reading from 628: heap size 957 MB, throughput 0.873246
Reading from 629: heap size 581 MB, throughput 0.988425
Reading from 628: heap size 957 MB, throughput 0.884559
Reading from 628: heap size 960 MB, throughput 0.872754
Reading from 628: heap size 962 MB, throughput 0.819697
Reading from 628: heap size 964 MB, throughput 0.757047
Reading from 628: heap size 985 MB, throughput 0.73186
Reading from 629: heap size 584 MB, throughput 0.988306
Reading from 628: heap size 995 MB, throughput 0.719662
Reading from 628: heap size 1108 MB, throughput 0.567521
Reading from 628: heap size 1113 MB, throughput 0.606961
Reading from 629: heap size 613 MB, throughput 0.984288
Reading from 628: heap size 1118 MB, throughput 0.668762
Reading from 628: heap size 1121 MB, throughput 0.679719
Reading from 628: heap size 1125 MB, throughput 0.688336
Reading from 628: heap size 1129 MB, throughput 0.684562
Reading from 629: heap size 614 MB, throughput 0.987657
Numeric result:
Recommendation: 2 clients, utility 0.697402:
    h1: 1030.97 MB (U(h) = 0.797326*h^0.0323603)
    h2: 2728.03 MB (U(h) = 0.355003*h^0.0855993)
Recommendation: 2 clients, utility 0.697402:
    h1: 1031.22 MB (U(h) = 0.797326*h^0.0323603)
    h2: 2727.78 MB (U(h) = 0.355003*h^0.0855993)
Reading from 629: heap size 645 MB, throughput 0.987464
Reading from 628: heap size 1145 MB, throughput 0.936351
Reading from 629: heap size 649 MB, throughput 0.987609
Reading from 628: heap size 1145 MB, throughput 0.958332
Reading from 629: heap size 683 MB, throughput 0.987219
Reading from 629: heap size 684 MB, throughput 0.989531
Reading from 628: heap size 1160 MB, throughput 0.963784
Numeric result:
Recommendation: 2 clients, utility 0.747535:
    h1: 783.642 MB (U(h) = 0.796306*h^0.032669)
    h2: 2975.36 MB (U(h) = 0.279997*h^0.124038)
Recommendation: 2 clients, utility 0.747535:
    h1: 783.646 MB (U(h) = 0.796306*h^0.032669)
    h2: 2975.35 MB (U(h) = 0.279997*h^0.124038)
Reading from 629: heap size 715 MB, throughput 0.992319
Reading from 628: heap size 1162 MB, throughput 0.965759
Reading from 629: heap size 719 MB, throughput 0.990817
Reading from 629: heap size 751 MB, throughput 0.990342
Reading from 628: heap size 1176 MB, throughput 0.966197
Reading from 629: heap size 752 MB, throughput 0.989057
Reading from 628: heap size 1181 MB, throughput 0.963377
Reading from 629: heap size 788 MB, throughput 0.989716
Numeric result:
Recommendation: 2 clients, utility 0.82781:
    h1: 578.689 MB (U(h) = 0.795711*h^0.0328456)
    h2: 3180.31 MB (U(h) = 0.196905*h^0.180497)
Recommendation: 2 clients, utility 0.82781:
    h1: 578.725 MB (U(h) = 0.795711*h^0.0328456)
    h2: 3180.28 MB (U(h) = 0.196905*h^0.180497)
Reading from 628: heap size 1195 MB, throughput 0.96753
Reading from 629: heap size 746 MB, throughput 0.983204
Reading from 629: heap size 698 MB, throughput 0.991888
Reading from 628: heap size 1197 MB, throughput 0.970217
Reading from 629: heap size 670 MB, throughput 0.990567
Reading from 629: heap size 648 MB, throughput 0.986846
Reading from 628: heap size 1194 MB, throughput 0.969785
Numeric result:
Recommendation: 2 clients, utility 0.884726:
    h1: 497.495 MB (U(h) = 0.795416*h^0.0329382)
    h2: 3261.5 MB (U(h) = 0.157939*h^0.216)
Recommendation: 2 clients, utility 0.884726:
    h1: 497.371 MB (U(h) = 0.795416*h^0.0329382)
    h2: 3261.63 MB (U(h) = 0.157939*h^0.216)
Reading from 629: heap size 627 MB, throughput 0.986493
Reading from 629: heap size 581 MB, throughput 0.989084
Reading from 628: heap size 1199 MB, throughput 0.97099
Reading from 629: heap size 568 MB, throughput 0.989468
Reading from 629: heap size 559 MB, throughput 0.988074
Reading from 628: heap size 1196 MB, throughput 0.973893
Reading from 629: heap size 535 MB, throughput 0.97172
Reading from 629: heap size 523 MB, throughput 0.993635
Reading from 629: heap size 523 MB, throughput 0.993635
Reading from 629: heap size 523 MB, throughput 0.979754
Reading from 629: heap size 487 MB, throughput 0.967154
Numeric result:
Recommendation: 2 clients, utility 0.922581:
    h1: 458.219 MB (U(h) = 0.794908*h^0.0330993)
    h2: 3300.78 MB (U(h) = 0.137291*h^0.238436)
Recommendation: 2 clients, utility 0.922581:
    h1: 458.21 MB (U(h) = 0.794908*h^0.0330993)
    h2: 3300.79 MB (U(h) = 0.137291*h^0.238436)
Reading from 628: heap size 1199 MB, throughput 0.97191
Reading from 629: heap size 578 MB, throughput 0.990495
Reading from 629: heap size 545 MB, throughput 0.986333
Reading from 629: heap size 534 MB, throughput 0.989386
Reading from 629: heap size 502 MB, throughput 0.97936
Reading from 628: heap size 1203 MB, throughput 0.925076
Reading from 629: heap size 495 MB, throughput 0.985641
Reading from 629: heap size 467 MB, throughput 0.983462
Reading from 629: heap size 465 MB, throughput 0.987658
Reading from 628: heap size 1273 MB, throughput 0.983056
Numeric result:
Recommendation: 2 clients, utility 0.993332:
    h1: 396.913 MB (U(h) = 0.793818*h^0.0335131)
    h2: 3362.09 MB (U(h) = 0.102143*h^0.283863)
Recommendation: 2 clients, utility 0.993332:
    h1: 396.93 MB (U(h) = 0.793818*h^0.0335131)
    h2: 3362.07 MB (U(h) = 0.102143*h^0.283863)
Reading from 629: heap size 431 MB, throughput 0.988056
Reading from 629: heap size 436 MB, throughput 0.988458
Reading from 629: heap size 406 MB, throughput 0.991174
Reading from 628: heap size 1271 MB, throughput 0.987353
Reading from 629: heap size 410 MB, throughput 0.985564
Reading from 629: heap size 391 MB, throughput 0.989543
Reading from 629: heap size 412 MB, throughput 0.987407
Reading from 628: heap size 1281 MB, throughput 0.98821
Reading from 629: heap size 370 MB, throughput 0.988594
Reading from 629: heap size 400 MB, throughput 0.990711
Reading from 629: heap size 359 MB, throughput 0.986023
Numeric result:
Recommendation: 2 clients, utility 1.07684:
    h1: 347.238 MB (U(h) = 0.79372*h^0.0339345)
    h2: 3411.76 MB (U(h) = 0.0738489*h^0.333408)
Recommendation: 2 clients, utility 1.07684:
    h1: 347.25 MB (U(h) = 0.79372*h^0.0339345)
    h2: 3411.75 MB (U(h) = 0.0738489*h^0.333408)
Reading from 629: heap size 389 MB, throughput 0.987044
Reading from 628: heap size 1289 MB, throughput 0.988104
Reading from 629: heap size 351 MB, throughput 0.990033
Reading from 629: heap size 353 MB, throughput 0.990833
Reading from 629: heap size 328 MB, throughput 0.990675
Reading from 629: heap size 356 MB, throughput 0.991087
Reading from 628: heap size 1291 MB, throughput 0.987227
Reading from 629: heap size 323 MB, throughput 0.986601
Reading from 629: heap size 346 MB, throughput 0.987469
Reading from 629: heap size 345 MB, throughput 0.990115
Reading from 629: heap size 357 MB, throughput 0.986913
Reading from 628: heap size 1286 MB, throughput 0.985027
Reading from 629: heap size 333 MB, throughput 0.98779
Reading from 629: heap size 349 MB, throughput 0.99021
Numeric result:
Recommendation: 2 clients, utility 1.22823:
    h1: 283.092 MB (U(h) = 0.795453*h^0.0340208)
    h2: 3475.91 MB (U(h) = 0.0422847*h^0.417689)
Recommendation: 2 clients, utility 1.22823:
    h1: 283.111 MB (U(h) = 0.795453*h^0.0340208)
    h2: 3475.89 MB (U(h) = 0.0422847*h^0.417689)
Reading from 629: heap size 324 MB, throughput 0.991777
Reading from 629: heap size 317 MB, throughput 0.98231
Reading from 629: heap size 317 MB, throughput 0.984976
Reading from 628: heap size 1201 MB, throughput 0.984292
Reading from 629: heap size 300 MB, throughput 0.988577
Reading from 629: heap size 288 MB, throughput 0.983088
Reading from 629: heap size 287 MB, throughput 0.983126
Reading from 629: heap size 275 MB, throughput 0.982217
Reading from 629: heap size 289 MB, throughput 0.98481
Reading from 628: heap size 1274 MB, throughput 0.983489
Reading from 629: heap size 269 MB, throughput 0.983897
Reading from 629: heap size 282 MB, throughput 0.984631
Reading from 629: heap size 281 MB, throughput 0.987422
Reading from 629: heap size 294 MB, throughput 0.988729
Reading from 629: heap size 277 MB, throughput 0.985912
Reading from 629: heap size 288 MB, throughput 0.983746
Numeric result:
Recommendation: 2 clients, utility 1.28226:
    h1: 264.342 MB (U(h) = 0.800293*h^0.0334927)
    h2: 3494.66 MB (U(h) = 0.0358641*h^0.442778)
Recommendation: 2 clients, utility 1.28226:
    h1: 264.344 MB (U(h) = 0.800293*h^0.0334927)
    h2: 3494.66 MB (U(h) = 0.0358641*h^0.442778)
Reading from 628: heap size 1222 MB, throughput 0.98097
Reading from 629: heap size 275 MB, throughput 0.987614
Reading from 629: heap size 266 MB, throughput 0.9884
Reading from 629: heap size 259 MB, throughput 0.988811
Reading from 629: heap size 268 MB, throughput 0.98307
Reading from 629: heap size 256 MB, throughput 0.982211
Reading from 629: heap size 262 MB, throughput 0.984826
Reading from 628: heap size 1279 MB, throughput 0.979394
Reading from 629: heap size 262 MB, throughput 0.985731
Reading from 629: heap size 275 MB, throughput 0.981574
Reading from 629: heap size 265 MB, throughput 0.982476
Reading from 629: heap size 257 MB, throughput 0.986418
Reading from 629: heap size 266 MB, throughput 0.987455
Reading from 629: heap size 254 MB, throughput 0.987627
Reading from 628: heap size 1280 MB, throughput 0.979084
Reading from 629: heap size 271 MB, throughput 0.982196
Reading from 629: heap size 262 MB, throughput 0.98151
Reading from 629: heap size 270 MB, throughput 0.984656
Reading from 629: heap size 259 MB, throughput 0.984472
Numeric result:
Recommendation: 2 clients, utility 1.31044:
    h1: 254.228 MB (U(h) = 0.804804*h^0.0328711)
    h2: 3504.77 MB (U(h) = 0.0336092*h^0.453135)
Recommendation: 2 clients, utility 1.31044:
    h1: 254.24 MB (U(h) = 0.804804*h^0.0328711)
    h2: 3504.76 MB (U(h) = 0.0336092*h^0.453135)
Reading from 629: heap size 275 MB, throughput 0.980392
Reading from 629: heap size 265 MB, throughput 0.98089
Reading from 628: heap size 1282 MB, throughput 0.979634
Reading from 629: heap size 258 MB, throughput 0.961032
Reading from 629: heap size 248 MB, throughput 0.966639
Reading from 629: heap size 262 MB, throughput 0.978456
Reading from 629: heap size 248 MB, throughput 0.983564
Reading from 629: heap size 262 MB, throughput 0.97911
Reading from 629: heap size 255 MB, throughput 0.979409
Reading from 629: heap size 250 MB, throughput 0.934668
Reading from 628: heap size 1286 MB, throughput 0.977046
Reading from 629: heap size 253 MB, throughput 0.949815
Reading from 629: heap size 265 MB, throughput 0.95072
Reading from 629: heap size 259 MB, throughput 0.963493
Reading from 629: heap size 251 MB, throughput 0.984945
Reading from 629: heap size 234 MB, throughput 0.986003
Reading from 629: heap size 251 MB, throughput 0.983889
Reading from 628: heap size 1289 MB, throughput 0.975725
Reading from 629: heap size 271 MB, throughput 0.982009
Numeric result:
Recommendation: 2 clients, utility 1.35069:
    h1: 230.347 MB (U(h) = 0.814427*h^0.0309166)
    h2: 3528.65 MB (U(h) = 0.0292838*h^0.47357)
Recommendation: 2 clients, utility 1.35069:
    h1: 230.364 MB (U(h) = 0.814427*h^0.0309166)
    h2: 3528.64 MB (U(h) = 0.0292838*h^0.47357)
Reading from 629: heap size 255 MB, throughput 0.985091
Reading from 629: heap size 246 MB, throughput 0.986865
Reading from 629: heap size 238 MB, throughput 0.982832
Reading from 629: heap size 235 MB, throughput 0.982433
Reading from 629: heap size 226 MB, throughput 0.9915
Reading from 629: heap size 226 MB, throughput 0.991501
Reading from 629: heap size 226 MB, throughput 0.970298
Reading from 629: heap size 235 MB, throughput 0.931746
Reading from 628: heap size 1296 MB, throughput 0.971935
Reading from 629: heap size 220 MB, throughput 0.979882
Reading from 629: heap size 228 MB, throughput 0.87375
Reading from 629: heap size 228 MB, throughput 0.954923
Reading from 629: heap size 256 MB, throughput 0.95218
Reading from 629: heap size 242 MB, throughput 0.977261
Reading from 629: heap size 238 MB, throughput 0.978827
Reading from 628: heap size 1300 MB, throughput 0.972424
Reading from 629: heap size 228 MB, throughput 0.969784
Reading from 629: heap size 235 MB, throughput 0.642227
Reading from 629: heap size 229 MB, throughput 0.874908
Reading from 629: heap size 234 MB, throughput 0.976088
Reading from 629: heap size 221 MB, throughput 0.835842
Numeric result:
Recommendation: 2 clients, utility 1.45395:
    h1: 204.183 MB (U(h) = 0.816763*h^0.030271)
    h2: 3554.82 MB (U(h) = 0.0203794*h^0.527015)
Recommendation: 2 clients, utility 1.45395:
    h1: 204.184 MB (U(h) = 0.816763*h^0.030271)
    h2: 3554.82 MB (U(h) = 0.0203794*h^0.527015)
Reading from 629: heap size 239 MB, throughput 0.837008
Reading from 629: heap size 217 MB, throughput 0.688179
Reading from 628: heap size 1310 MB, throughput 0.969254
Reading from 629: heap size 221 MB, throughput 0.453361
Reading from 629: heap size 204 MB, throughput 0.961047
Reading from 629: heap size 204 MB, throughput 0.668391
Reading from 629: heap size 186 MB, throughput 0.811456
Reading from 629: heap size 205 MB, throughput 0.901141
Reading from 629: heap size 185 MB, throughput 0.948599
Reading from 629: heap size 198 MB, throughput 0.940979
Reading from 629: heap size 199 MB, throughput 0.97708
Reading from 628: heap size 1318 MB, throughput 0.967969
Reading from 629: heap size 213 MB, throughput 0.500101
Reading from 629: heap size 190 MB, throughput 0.652903
Reading from 629: heap size 209 MB, throughput 0.967051
Reading from 629: heap size 189 MB, throughput 0.804741
Reading from 629: heap size 201 MB, throughput 0.911673
Reading from 629: heap size 198 MB, throughput 0.961527
Reading from 629: heap size 204 MB, throughput 0.979837
Reading from 628: heap size 1323 MB, throughput 0.967886
Reading from 629: heap size 208 MB, throughput 0.985828
Reading from 629: heap size 190 MB, throughput 0.974663
Reading from 629: heap size 207 MB, throughput 0.976122
Numeric result:
Recommendation: 2 clients, utility 1.56003:
    h1: 481.571 MB (U(h) = 0.573799*h^0.0874439)
    h2: 3277.43 MB (U(h) = 0.0128091*h^0.595151)
Recommendation: 2 clients, utility 1.56003:
    h1: 481.547 MB (U(h) = 0.573799*h^0.0874439)
    h2: 3277.45 MB (U(h) = 0.0128091*h^0.595151)
Reading from 629: heap size 200 MB, throughput 0.976206
Reading from 629: heap size 209 MB, throughput 0.978869
Reading from 629: heap size 220 MB, throughput 0.980674
Reading from 629: heap size 221 MB, throughput 0.975069
Reading from 629: heap size 233 MB, throughput 0.974915
Reading from 628: heap size 1330 MB, throughput 0.969021
Reading from 629: heap size 235 MB, throughput 0.978691
Reading from 629: heap size 250 MB, throughput 0.982616
Reading from 629: heap size 250 MB, throughput 0.985047
Reading from 629: heap size 263 MB, throughput 0.977487
Reading from 629: heap size 264 MB, throughput 0.978866
Reading from 629: heap size 281 MB, throughput 0.982785
Reading from 628: heap size 1333 MB, throughput 0.970693
Reading from 629: heap size 282 MB, throughput 0.987336
Reading from 629: heap size 293 MB, throughput 0.979451
Reading from 629: heap size 296 MB, throughput 0.982887
Reading from 629: heap size 313 MB, throughput 0.987782
Numeric result:
Recommendation: 2 clients, utility 1.67273:
    h1: 432.944 MB (U(h) = 0.590445*h^0.0840411)
    h2: 3326.06 MB (U(h) = 0.00905203*h^0.64565)
Recommendation: 2 clients, utility 1.67273:
    h1: 432.937 MB (U(h) = 0.590445*h^0.0840411)
    h2: 3326.06 MB (U(h) = 0.00905203*h^0.64565)
Reading from 629: heap size 315 MB, throughput 0.985897
Reading from 628: heap size 1340 MB, throughput 0.970654
Reading from 629: heap size 329 MB, throughput 0.96639
Reading from 629: heap size 330 MB, throughput 0.986997
Reading from 629: heap size 344 MB, throughput 0.985066
Reading from 629: heap size 345 MB, throughput 0.986851
Reading from 628: heap size 1341 MB, throughput 0.969448
Reading from 629: heap size 361 MB, throughput 0.990277
Reading from 629: heap size 362 MB, throughput 0.982312
Reading from 629: heap size 376 MB, throughput 0.986748
Reading from 629: heap size 378 MB, throughput 0.990405
Reading from 628: heap size 1347 MB, throughput 0.96809
Reading from 629: heap size 393 MB, throughput 0.984514
Numeric result:
Recommendation: 2 clients, utility 1.78041:
    h1: 546.783 MB (U(h) = 0.494551*h^0.11592)
    h2: 3212.22 MB (U(h) = 0.00709368*h^0.680982)
Recommendation: 2 clients, utility 1.78041:
    h1: 546.796 MB (U(h) = 0.494551*h^0.11592)
    h2: 3212.2 MB (U(h) = 0.00709368*h^0.680982)
Reading from 629: heap size 395 MB, throughput 0.988563
Reading from 629: heap size 415 MB, throughput 0.983373
Reading from 629: heap size 417 MB, throughput 0.988389
Reading from 628: heap size 1347 MB, throughput 0.967153
Reading from 629: heap size 438 MB, throughput 0.989101
Reading from 629: heap size 441 MB, throughput 0.990016
Reading from 629: heap size 459 MB, throughput 0.98897
Reading from 628: heap size 1352 MB, throughput 0.96815
Reading from 629: heap size 461 MB, throughput 0.989316
Numeric result:
Recommendation: 2 clients, utility 1.89285:
    h1: 600.583 MB (U(h) = 0.439827*h^0.13693)
    h2: 3158.42 MB (U(h) = 0.00541167*h^0.720118)
Recommendation: 2 clients, utility 1.89285:
    h1: 600.573 MB (U(h) = 0.439827*h^0.13693)
    h2: 3158.43 MB (U(h) = 0.00541167*h^0.720118)
Reading from 629: heap size 481 MB, throughput 0.988906
Reading from 629: heap size 482 MB, throughput 0.990159
Reading from 629: heap size 502 MB, throughput 0.97622
Reading from 628: heap size 1353 MB, throughput 0.956471
Reading from 629: heap size 504 MB, throughput 0.9841
Reading from 629: heap size 531 MB, throughput 0.98497
Reading from 629: heap size 534 MB, throughput 0.987942
Reading from 629: heap size 563 MB, throughput 0.978516
Numeric result:
Recommendation: 2 clients, utility 1.94188:
    h1: 536.222 MB (U(h) = 0.470224*h^0.124969)
    h2: 3222.78 MB (U(h) = 0.00436339*h^0.751094)
Recommendation: 2 clients, utility 1.94188:
    h1: 536.214 MB (U(h) = 0.470224*h^0.124969)
    h2: 3222.79 MB (U(h) = 0.00436339*h^0.751094)
Reading from 629: heap size 565 MB, throughput 0.987692
Reading from 629: heap size 533 MB, throughput 0.968278
Reading from 629: heap size 559 MB, throughput 0.972674
Reading from 629: heap size 546 MB, throughput 0.989659
Reading from 629: heap size 524 MB, throughput 0.984086
Reading from 629: heap size 558 MB, throughput 0.993302
Numeric result:
Recommendation: 2 clients, utility 1.91826:
    h1: 466.435 MB (U(h) = 0.521334*h^0.106404)
    h2: 3292.56 MB (U(h) = 0.00436339*h^0.751094)
Recommendation: 2 clients, utility 1.91826:
    h1: 466.44 MB (U(h) = 0.521334*h^0.106404)
    h2: 3292.56 MB (U(h) = 0.00436339*h^0.751094)
Reading from 629: heap size 531 MB, throughput 0.993469
Reading from 628: heap size 1475 MB, throughput 0.983272
Reading from 629: heap size 517 MB, throughput 0.989694
Reading from 629: heap size 505 MB, throughput 0.992005
Reading from 629: heap size 477 MB, throughput 0.99091
Reading from 629: heap size 463 MB, throughput 0.99247
Reading from 629: heap size 492 MB, throughput 0.988231
Reading from 629: heap size 466 MB, throughput 0.988964
Reading from 629: heap size 476 MB, throughput 0.986371
Numeric result:
Recommendation: 2 clients, utility 1.82866:
    h1: 463.743 MB (U(h) = 0.537925*h^0.100539)
    h2: 3295.26 MB (U(h) = 0.00562449*h^0.714424)
Recommendation: 2 clients, utility 1.82866:
    h1: 463.734 MB (U(h) = 0.537925*h^0.100539)
    h2: 3295.27 MB (U(h) = 0.00562449*h^0.714424)
Reading from 629: heap size 459 MB, throughput 0.989843
Reading from 629: heap size 469 MB, throughput 0.987499
Reading from 629: heap size 446 MB, throughput 0.989771
Reading from 629: heap size 460 MB, throughput 0.988468
Reading from 629: heap size 461 MB, throughput 0.989106
Reading from 628: heap size 1476 MB, throughput 0.986351
Reading from 629: heap size 482 MB, throughput 0.988426
Reading from 629: heap size 453 MB, throughput 0.950088
Reading from 629: heap size 467 MB, throughput 0.988888
Reading from 628: heap size 1461 MB, throughput 0.98397
Reading from 629: heap size 438 MB, throughput 0.99022
Numeric result:
Recommendation: 2 clients, utility 1.91209:
    h1: 428.124 MB (U(h) = 0.546608*h^0.0974175)
    h2: 3330.88 MB (U(h) = 0.00414463*h^0.757968)
Recommendation: 2 clients, utility 1.91209:
    h1: 428.102 MB (U(h) = 0.546608*h^0.0974175)
    h2: 3330.9 MB (U(h) = 0.00414463*h^0.757968)
Reading from 628: heap size 1486 MB, throughput 0.970696
Reading from 628: heap size 1462 MB, throughput 0.952487
Reading from 628: heap size 1490 MB, throughput 0.91851
Reading from 629: heap size 455 MB, throughput 0.991996
Reading from 628: heap size 1516 MB, throughput 0.8822
Reading from 628: heap size 1528 MB, throughput 0.82538
Reading from 628: heap size 1560 MB, throughput 0.7924
Reading from 629: heap size 424 MB, throughput 0.98413
Reading from 629: heap size 436 MB, throughput 0.990736
Reading from 628: heap size 1566 MB, throughput 0.901384
Reading from 629: heap size 408 MB, throughput 0.984716
Reading from 629: heap size 424 MB, throughput 0.988468
Reading from 629: heap size 425 MB, throughput 0.987755
Reading from 628: heap size 1588 MB, throughput 0.964414
Reading from 629: heap size 447 MB, throughput 0.989687
Reading from 629: heap size 417 MB, throughput 0.991516
Numeric result:
Recommendation: 2 clients, utility 1.98881:
    h1: 409.671 MB (U(h) = 0.539046*h^0.0995415)
    h2: 3349.33 MB (U(h) = 0.00274324*h^0.813814)
Recommendation: 2 clients, utility 1.98881:
    h1: 409.672 MB (U(h) = 0.539046*h^0.0995415)
    h2: 3349.33 MB (U(h) = 0.00274324*h^0.813814)
Reading from 629: heap size 433 MB, throughput 0.986657
Reading from 629: heap size 415 MB, throughput 0.990335
Reading from 628: heap size 1596 MB, throughput 0.969777
Reading from 629: heap size 390 MB, throughput 0.988655
Reading from 629: heap size 411 MB, throughput 0.989233
Reading from 629: heap size 386 MB, throughput 0.988508
Reading from 629: heap size 417 MB, throughput 0.985202
Reading from 629: heap size 401 MB, throughput 0.98979
Reading from 628: heap size 1595 MB, throughput 0.974303
Reading from 629: heap size 413 MB, throughput 0.988566
Reading from 629: heap size 386 MB, throughput 0.989353
Numeric result:
Recommendation: 2 clients, utility 1.79461:
    h1: 479.416 MB (U(h) = 0.520605*h^0.105176)
    h2: 3279.58 MB (U(h) = 0.00531807*h^0.719537)
Recommendation: 2 clients, utility 1.79461:
    h1: 479.389 MB (U(h) = 0.520605*h^0.105176)
    h2: 3279.61 MB (U(h) = 0.00531807*h^0.719537)
Reading from 629: heap size 413 MB, throughput 0.991469
Reading from 629: heap size 420 MB, throughput 0.98525
Reading from 628: heap size 1607 MB, throughput 0.972446
Reading from 629: heap size 420 MB, throughput 0.988934
Reading from 629: heap size 443 MB, throughput 0.984589
Reading from 629: heap size 445 MB, throughput 0.988234
Client 629 died
Clients: 1
Reading from 628: heap size 1598 MB, throughput 0.978063
Recommendation: one client; give it all the memory
Reading from 628: heap size 1610 MB, throughput 0.963604
Reading from 628: heap size 1560 MB, throughput 0.988449
Recommendation: one client; give it all the memory
Reading from 628: heap size 1565 MB, throughput 0.991681
Recommendation: one client; give it all the memory
Reading from 628: heap size 1590 MB, throughput 0.992631
Reading from 628: heap size 1598 MB, throughput 0.99268
Recommendation: one client; give it all the memory
Reading from 628: heap size 1605 MB, throughput 0.992274
Reading from 628: heap size 1609 MB, throughput 0.991478
Recommendation: one client; give it all the memory
Reading from 628: heap size 1592 MB, throughput 0.990926
Recommendation: one client; give it all the memory
Reading from 628: heap size 1445 MB, throughput 0.990021
Reading from 628: heap size 1574 MB, throughput 0.988915
Recommendation: one client; give it all the memory
Reading from 628: heap size 1476 MB, throughput 0.987712
Recommendation: one client; give it all the memory
Reading from 628: heap size 1577 MB, throughput 0.986899
Reading from 628: heap size 1581 MB, throughput 0.986138
Recommendation: one client; give it all the memory
Reading from 628: heap size 1584 MB, throughput 0.984734
Reading from 628: heap size 1588 MB, throughput 0.983221
Recommendation: one client; give it all the memory
Reading from 628: heap size 1594 MB, throughput 0.982818
Recommendation: one client; give it all the memory
Reading from 628: heap size 1603 MB, throughput 0.981504
Reading from 628: heap size 1614 MB, throughput 0.981001
Recommendation: one client; give it all the memory
Reading from 628: heap size 1623 MB, throughput 0.980876
Reading from 628: heap size 1634 MB, throughput 0.980946
Recommendation: one client; give it all the memory
Reading from 628: heap size 1639 MB, throughput 0.980594
Recommendation: one client; give it all the memory
Reading from 628: heap size 1650 MB, throughput 0.980781
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 628: heap size 1653 MB, throughput 0.993086
Recommendation: one client; give it all the memory
Reading from 628: heap size 1643 MB, throughput 0.991142
Reading from 628: heap size 1654 MB, throughput 0.982031
Reading from 628: heap size 1645 MB, throughput 0.968432
Reading from 628: heap size 1671 MB, throughput 0.944422
Reading from 628: heap size 1712 MB, throughput 0.916123
Client 628 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
