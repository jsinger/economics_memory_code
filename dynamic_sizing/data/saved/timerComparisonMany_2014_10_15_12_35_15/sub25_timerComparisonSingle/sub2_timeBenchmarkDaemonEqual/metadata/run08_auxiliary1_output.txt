economemd
    total memory: 7518 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub25_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 25070: heap size 9 MB, throughput 0.990088
Clients: 1
Client 25070 has a minimum heap size of 30 MB
Reading from 25069: heap size 9 MB, throughput 0.989524
Clients: 2
Client 25069 has a minimum heap size of 1223 MB
Reading from 25070: heap size 9 MB, throughput 0.977338
Reading from 25070: heap size 9 MB, throughput 0.967343
Reading from 25069: heap size 9 MB, throughput 0.968443
Reading from 25070: heap size 9 MB, throughput 0.914822
Reading from 25070: heap size 11 MB, throughput 0.764083
Reading from 25070: heap size 11 MB, throughput 0.416716
Reading from 25070: heap size 16 MB, throughput 0.694348
Reading from 25070: heap size 16 MB, throughput 0.6783
Reading from 25070: heap size 24 MB, throughput 0.837787
Reading from 25070: heap size 24 MB, throughput 0.85342
Reading from 25069: heap size 9 MB, throughput 0.959957
Reading from 25070: heap size 34 MB, throughput 0.882885
Reading from 25070: heap size 34 MB, throughput 0.920291
Reading from 25070: heap size 50 MB, throughput 0.926227
Reading from 25070: heap size 50 MB, throughput 0.910849
Reading from 25069: heap size 9 MB, throughput 0.951728
Reading from 25070: heap size 75 MB, throughput 0.954692
Reading from 25070: heap size 75 MB, throughput 0.942012
Reading from 25070: heap size 116 MB, throughput 0.968561
Reading from 25070: heap size 116 MB, throughput 0.942072
Reading from 25069: heap size 11 MB, throughput 0.986382
Reading from 25069: heap size 11 MB, throughput 0.974055
Reading from 25069: heap size 17 MB, throughput 0.913899
Reading from 25069: heap size 17 MB, throughput 0.510014
Reading from 25069: heap size 30 MB, throughput 0.945016
Reading from 25069: heap size 31 MB, throughput 0.891151
Reading from 25069: heap size 34 MB, throughput 0.329157
Reading from 25069: heap size 46 MB, throughput 0.874858
Reading from 25069: heap size 49 MB, throughput 0.886435
Reading from 25070: heap size 159 MB, throughput 0.954523
Reading from 25069: heap size 51 MB, throughput 0.212497
Reading from 25069: heap size 75 MB, throughput 0.873967
Reading from 25070: heap size 164 MB, throughput 0.863946
Reading from 25069: heap size 75 MB, throughput 0.253044
Reading from 25069: heap size 98 MB, throughput 0.735466
Reading from 25069: heap size 101 MB, throughput 0.811212
Reading from 25069: heap size 102 MB, throughput 0.852205
Reading from 25070: heap size 220 MB, throughput 0.988619
Reading from 25069: heap size 104 MB, throughput 0.219274
Reading from 25069: heap size 134 MB, throughput 0.661829
Reading from 25069: heap size 136 MB, throughput 0.742059
Reading from 25069: heap size 139 MB, throughput 0.542514
Reading from 25069: heap size 143 MB, throughput 0.665681
Reading from 25069: heap size 147 MB, throughput 0.67382
Reading from 25070: heap size 238 MB, throughput 0.992015
Reading from 25069: heap size 152 MB, throughput 0.110205
Reading from 25069: heap size 194 MB, throughput 0.576514
Reading from 25069: heap size 196 MB, throughput 0.65166
Reading from 25069: heap size 199 MB, throughput 0.789838
Reading from 25069: heap size 206 MB, throughput 0.670747
Reading from 25069: heap size 210 MB, throughput 0.598961
Reading from 25069: heap size 220 MB, throughput 0.492256
Reading from 25069: heap size 223 MB, throughput 0.395834
Reading from 25070: heap size 252 MB, throughput 0.991816
Reading from 25069: heap size 234 MB, throughput 0.11062
Reading from 25069: heap size 280 MB, throughput 0.545083
Reading from 25070: heap size 253 MB, throughput 0.842244
Reading from 25069: heap size 225 MB, throughput 0.601641
Reading from 25069: heap size 272 MB, throughput 0.12992
Reading from 25070: heap size 284 MB, throughput 0.985754
Reading from 25069: heap size 269 MB, throughput 0.501907
Reading from 25069: heap size 312 MB, throughput 0.579011
Reading from 25069: heap size 315 MB, throughput 0.62506
Reading from 25069: heap size 310 MB, throughput 0.64122
Reading from 25069: heap size 313 MB, throughput 0.492076
Reading from 25069: heap size 312 MB, throughput 0.587595
Reading from 25070: heap size 288 MB, throughput 0.990721
Reading from 25069: heap size 317 MB, throughput 0.570928
Reading from 25069: heap size 322 MB, throughput 0.576262
Reading from 25069: heap size 326 MB, throughput 0.592641
Reading from 25069: heap size 329 MB, throughput 0.572385
Reading from 25069: heap size 334 MB, throughput 0.582618
Reading from 25070: heap size 303 MB, throughput 0.980571
Reading from 25069: heap size 339 MB, throughput 0.0644709
Reading from 25069: heap size 385 MB, throughput 0.41478
Equal recommendation: 3759 MB each
Reading from 25069: heap size 395 MB, throughput 0.620451
Reading from 25069: heap size 395 MB, throughput 0.62044
Reading from 25070: heap size 305 MB, throughput 0.968613
Reading from 25069: heap size 399 MB, throughput 0.0789709
Reading from 25069: heap size 447 MB, throughput 0.381893
Reading from 25070: heap size 325 MB, throughput 0.989457
Reading from 25069: heap size 445 MB, throughput 0.543854
Reading from 25069: heap size 448 MB, throughput 0.524451
Reading from 25069: heap size 450 MB, throughput 0.506089
Reading from 25069: heap size 454 MB, throughput 0.497502
Reading from 25069: heap size 460 MB, throughput 0.487401
Reading from 25069: heap size 466 MB, throughput 0.419898
Reading from 25070: heap size 327 MB, throughput 0.98035
Reading from 25069: heap size 472 MB, throughput 0.0734894
Reading from 25070: heap size 345 MB, throughput 0.984358
Reading from 25069: heap size 531 MB, throughput 0.458222
Reading from 25069: heap size 534 MB, throughput 0.521167
Reading from 25069: heap size 536 MB, throughput 0.500171
Reading from 25069: heap size 541 MB, throughput 0.557233
Reading from 25070: heap size 348 MB, throughput 0.974759
Reading from 25069: heap size 541 MB, throughput 0.116625
Reading from 25069: heap size 606 MB, throughput 0.357482
Reading from 25069: heap size 611 MB, throughput 0.502579
Reading from 25069: heap size 612 MB, throughput 0.502716
Reading from 25070: heap size 371 MB, throughput 0.981609
Reading from 25069: heap size 614 MB, throughput 0.491475
Reading from 25069: heap size 620 MB, throughput 0.547393
Reading from 25070: heap size 373 MB, throughput 0.897731
Reading from 25069: heap size 623 MB, throughput 0.130903
Reading from 25070: heap size 400 MB, throughput 0.997026
Reading from 25069: heap size 694 MB, throughput 0.731234
Equal recommendation: 3759 MB each
Reading from 25070: heap size 403 MB, throughput 0.989494
Reading from 25069: heap size 698 MB, throughput 0.845897
Reading from 25069: heap size 696 MB, throughput 0.858802
Reading from 25069: heap size 711 MB, throughput 0.585033
Reading from 25070: heap size 416 MB, throughput 0.978656
Reading from 25069: heap size 720 MB, throughput 0.626566
Reading from 25069: heap size 730 MB, throughput 0.327386
Reading from 25069: heap size 740 MB, throughput 0.331114
Reading from 25070: heap size 419 MB, throughput 0.981789
Reading from 25069: heap size 741 MB, throughput 0.0247522
Reading from 25069: heap size 800 MB, throughput 0.262351
Reading from 25070: heap size 437 MB, throughput 0.994882
Reading from 25069: heap size 806 MB, throughput 0.857354
Reading from 25069: heap size 803 MB, throughput 0.462702
Reading from 25069: heap size 806 MB, throughput 0.551178
Reading from 25069: heap size 806 MB, throughput 0.612031
Reading from 25070: heap size 439 MB, throughput 0.974436
Reading from 25069: heap size 808 MB, throughput 0.178667
Reading from 25069: heap size 889 MB, throughput 0.637747
Reading from 25070: heap size 456 MB, throughput 0.988719
Reading from 25069: heap size 890 MB, throughput 0.698085
Reading from 25069: heap size 896 MB, throughput 0.246517
Reading from 25070: heap size 458 MB, throughput 0.989513
Reading from 25069: heap size 992 MB, throughput 0.650998
Reading from 25069: heap size 998 MB, throughput 0.936764
Reading from 25069: heap size 835 MB, throughput 0.774728
Reading from 25069: heap size 992 MB, throughput 0.848098
Reading from 25070: heap size 471 MB, throughput 0.993192
Reading from 25069: heap size 841 MB, throughput 0.858308
Reading from 25069: heap size 982 MB, throughput 0.862582
Equal recommendation: 3759 MB each
Reading from 25069: heap size 846 MB, throughput 0.884281
Reading from 25069: heap size 973 MB, throughput 0.869871
Reading from 25069: heap size 849 MB, throughput 0.87305
Reading from 25069: heap size 966 MB, throughput 0.836639
Reading from 25069: heap size 854 MB, throughput 0.878505
Reading from 25070: heap size 474 MB, throughput 0.988683
Reading from 25069: heap size 959 MB, throughput 0.874622
Reading from 25070: heap size 490 MB, throughput 0.994572
Reading from 25069: heap size 859 MB, throughput 0.98475
Reading from 25069: heap size 955 MB, throughput 0.939964
Reading from 25069: heap size 960 MB, throughput 0.787105
Reading from 25069: heap size 960 MB, throughput 0.759345
Reading from 25070: heap size 491 MB, throughput 0.985564
Reading from 25069: heap size 961 MB, throughput 0.829248
Reading from 25069: heap size 960 MB, throughput 0.762333
Reading from 25069: heap size 963 MB, throughput 0.802098
Reading from 25069: heap size 963 MB, throughput 0.792156
Reading from 25069: heap size 965 MB, throughput 0.819509
Reading from 25070: heap size 505 MB, throughput 0.985613
Reading from 25069: heap size 966 MB, throughput 0.791436
Reading from 25069: heap size 968 MB, throughput 0.845701
Reading from 25069: heap size 968 MB, throughput 0.905407
Reading from 25069: heap size 971 MB, throughput 0.72224
Reading from 25069: heap size 975 MB, throughput 0.654092
Reading from 25069: heap size 976 MB, throughput 0.60082
Reading from 25070: heap size 507 MB, throughput 0.984617
Reading from 25069: heap size 1005 MB, throughput 0.736858
Reading from 25069: heap size 1007 MB, throughput 0.71085
Reading from 25069: heap size 1015 MB, throughput 0.701522
Reading from 25069: heap size 1017 MB, throughput 0.695062
Reading from 25069: heap size 1031 MB, throughput 0.725114
Reading from 25069: heap size 1031 MB, throughput 0.678123
Reading from 25069: heap size 1048 MB, throughput 0.731261
Reading from 25070: heap size 524 MB, throughput 0.99536
Reading from 25069: heap size 1049 MB, throughput 0.912767
Equal recommendation: 3759 MB each
Reading from 25070: heap size 526 MB, throughput 0.987693
Reading from 25070: heap size 542 MB, throughput 0.989533
Reading from 25069: heap size 1066 MB, throughput 0.96716
Reading from 25070: heap size 544 MB, throughput 0.992748
Reading from 25070: heap size 558 MB, throughput 0.987835
Reading from 25069: heap size 1069 MB, throughput 0.96918
Reading from 25070: heap size 560 MB, throughput 0.994708
Reading from 25070: heap size 575 MB, throughput 0.972777
Reading from 25069: heap size 1076 MB, throughput 0.970441
Equal recommendation: 3759 MB each
Reading from 25070: heap size 578 MB, throughput 0.986584
Reading from 25069: heap size 1080 MB, throughput 0.971343
Reading from 25070: heap size 602 MB, throughput 0.994978
Reading from 25070: heap size 604 MB, throughput 0.987524
Reading from 25069: heap size 1077 MB, throughput 0.969781
Reading from 25070: heap size 624 MB, throughput 0.990547
Reading from 25070: heap size 625 MB, throughput 0.993432
Reading from 25069: heap size 1083 MB, throughput 0.968792
Reading from 25070: heap size 641 MB, throughput 0.995003
Equal recommendation: 3759 MB each
Reading from 25070: heap size 643 MB, throughput 0.978423
Reading from 25069: heap size 1074 MB, throughput 0.967215
Reading from 25070: heap size 660 MB, throughput 0.994302
Reading from 25069: heap size 1080 MB, throughput 0.969919
Reading from 25070: heap size 661 MB, throughput 0.994072
Reading from 25070: heap size 676 MB, throughput 0.989527
Reading from 25069: heap size 1081 MB, throughput 0.974957
Equal recommendation: 3759 MB each
Reading from 25070: heap size 679 MB, throughput 0.98995
Reading from 25070: heap size 702 MB, throughput 0.995134
Reading from 25069: heap size 1083 MB, throughput 0.621902
Reading from 25070: heap size 702 MB, throughput 0.988423
Reading from 25070: heap size 720 MB, throughput 0.990214
Reading from 25069: heap size 1181 MB, throughput 0.961407
Reading from 25070: heap size 722 MB, throughput 0.988723
Equal recommendation: 3759 MB each
Reading from 25069: heap size 1182 MB, throughput 0.980259
Reading from 25070: heap size 744 MB, throughput 0.990758
Reading from 25070: heap size 746 MB, throughput 0.991454
Reading from 25069: heap size 1190 MB, throughput 0.982475
Reading from 25070: heap size 767 MB, throughput 0.994323
Reading from 25069: heap size 1190 MB, throughput 0.984165
Reading from 25070: heap size 769 MB, throughput 0.990403
Equal recommendation: 3759 MB each
Reading from 25070: heap size 790 MB, throughput 0.990505
Reading from 25069: heap size 1185 MB, throughput 0.984943
Reading from 25070: heap size 790 MB, throughput 0.989523
Reading from 25069: heap size 1138 MB, throughput 0.979755
Reading from 25070: heap size 813 MB, throughput 0.991091
Reading from 25070: heap size 815 MB, throughput 0.990589
Equal recommendation: 3759 MB each
Reading from 25069: heap size 1182 MB, throughput 0.980273
Reading from 25070: heap size 840 MB, throughput 0.990759
Reading from 25069: heap size 1185 MB, throughput 0.979211
Reading from 25070: heap size 840 MB, throughput 0.988297
Reading from 25070: heap size 867 MB, throughput 0.989505
Reading from 25069: heap size 1186 MB, throughput 0.980831
Equal recommendation: 3759 MB each
Reading from 25070: heap size 867 MB, throughput 0.990617
Reading from 25069: heap size 1188 MB, throughput 0.97385
Reading from 25070: heap size 894 MB, throughput 0.991319
Reading from 25069: heap size 1190 MB, throughput 0.968302
Reading from 25070: heap size 895 MB, throughput 0.989349
Reading from 25070: heap size 923 MB, throughput 0.992777
Reading from 25069: heap size 1194 MB, throughput 0.97171
Equal recommendation: 3759 MB each
Reading from 25070: heap size 923 MB, throughput 0.991573
Reading from 25069: heap size 1197 MB, throughput 0.973411
Reading from 25070: heap size 950 MB, throughput 0.9912
Reading from 25069: heap size 1203 MB, throughput 0.967293
Reading from 25070: heap size 951 MB, throughput 0.9948
Equal recommendation: 3759 MB each
Reading from 25069: heap size 1207 MB, throughput 0.967338
Reading from 25070: heap size 975 MB, throughput 0.991607
Reading from 25070: heap size 978 MB, throughput 0.991561
Reading from 25069: heap size 1214 MB, throughput 0.967665
Reading from 25070: heap size 1005 MB, throughput 0.989974
Reading from 25069: heap size 1220 MB, throughput 0.972113
Equal recommendation: 3759 MB each
Reading from 25070: heap size 1005 MB, throughput 0.991299
Reading from 25069: heap size 1226 MB, throughput 0.965876
Reading from 25070: heap size 1033 MB, throughput 0.99134
Reading from 25069: heap size 1232 MB, throughput 0.965483
Reading from 25070: heap size 1034 MB, throughput 0.995448
Reading from 25069: heap size 1235 MB, throughput 0.964156
Equal recommendation: 3759 MB each
Reading from 25070: heap size 1059 MB, throughput 0.995011
Reading from 25069: heap size 1242 MB, throughput 0.968355
Reading from 25070: heap size 1062 MB, throughput 0.990159
Reading from 25069: heap size 1243 MB, throughput 0.966616
Reading from 25070: heap size 1089 MB, throughput 0.992214
Equal recommendation: 3759 MB each
Reading from 25070: heap size 1089 MB, throughput 0.995276
Reading from 25069: heap size 1248 MB, throughput 0.628545
Reading from 25070: heap size 1114 MB, throughput 0.993905
Reading from 25069: heap size 1357 MB, throughput 0.952582
Reading from 25070: heap size 1117 MB, throughput 0.9898
Equal recommendation: 3759 MB each
Reading from 25069: heap size 1358 MB, throughput 0.982035
Reading from 25070: heap size 1142 MB, throughput 0.992537
Reading from 25069: heap size 1362 MB, throughput 0.98087
Reading from 25070: heap size 1144 MB, throughput 0.992182
Equal recommendation: 3759 MB each
Reading from 25069: heap size 1366 MB, throughput 0.979564
Reading from 25070: heap size 1175 MB, throughput 0.99001
Reading from 25070: heap size 1175 MB, throughput 0.991052
Reading from 25069: heap size 1367 MB, throughput 0.975864
Reading from 25070: heap size 1211 MB, throughput 0.996026
Reading from 25069: heap size 1362 MB, throughput 0.970829
Equal recommendation: 3759 MB each
Reading from 25070: heap size 1213 MB, throughput 0.987933
Reading from 25069: heap size 1366 MB, throughput 0.975389
Reading from 25070: heap size 1237 MB, throughput 0.995229
Reading from 25069: heap size 1354 MB, throughput 0.976494
Reading from 25070: heap size 1243 MB, throughput 0.987366
Equal recommendation: 3759 MB each
Reading from 25069: heap size 1295 MB, throughput 0.976687
Reading from 25070: heap size 1270 MB, throughput 0.99378
Reading from 25070: heap size 1275 MB, throughput 0.952459
Equal recommendation: 3759 MB each
Reading from 25070: heap size 1324 MB, throughput 0.993936
Reading from 25070: heap size 1332 MB, throughput 0.986579
Reading from 25070: heap size 1375 MB, throughput 0.991741
Equal recommendation: 3759 MB each
Reading from 25070: heap size 1380 MB, throughput 0.986705
Reading from 25070: heap size 1432 MB, throughput 0.989783
Equal recommendation: 3759 MB each
Reading from 25070: heap size 1435 MB, throughput 0.990375
Reading from 25069: heap size 1357 MB, throughput 0.996278
Reading from 25070: heap size 1492 MB, throughput 0.989039
Reading from 25069: heap size 1358 MB, throughput 0.981009
Reading from 25070: heap size 1494 MB, throughput 0.989644
Reading from 25069: heap size 1346 MB, throughput 0.857517
Equal recommendation: 3759 MB each
Reading from 25069: heap size 1380 MB, throughput 0.23589
Reading from 25069: heap size 1436 MB, throughput 0.994007
Reading from 25069: heap size 1475 MB, throughput 0.989216
Reading from 25069: heap size 1495 MB, throughput 0.990508
Reading from 25069: heap size 1499 MB, throughput 0.982345
Reading from 25069: heap size 1504 MB, throughput 0.990743
Reading from 25069: heap size 1508 MB, throughput 0.98414
Reading from 25070: heap size 1561 MB, throughput 0.992637
Reading from 25069: heap size 1495 MB, throughput 0.992723
Reading from 25070: heap size 1561 MB, throughput 0.991777
Reading from 25069: heap size 1265 MB, throughput 0.992719
Equal recommendation: 3759 MB each
Client 25070 died
Clients: 1
Reading from 25069: heap size 1480 MB, throughput 0.993219
Reading from 25069: heap size 1288 MB, throughput 0.991601
Recommendation: one client; give it all the memory
Reading from 25069: heap size 1458 MB, throughput 0.990933
Reading from 25069: heap size 1308 MB, throughput 0.989021
Reading from 25069: heap size 1432 MB, throughput 0.988937
Recommendation: one client; give it all the memory
Reading from 25069: heap size 1328 MB, throughput 0.987788
Reading from 25069: heap size 1428 MB, throughput 0.986854
Recommendation: one client; give it all the memory
Reading from 25069: heap size 1434 MB, throughput 0.986191
Reading from 25069: heap size 1434 MB, throughput 0.984097
Reading from 25069: heap size 1435 MB, throughput 0.984722
Recommendation: one client; give it all the memory
Reading from 25069: heap size 1437 MB, throughput 0.983269
Reading from 25069: heap size 1441 MB, throughput 0.982761
Recommendation: one client; give it all the memory
Reading from 25069: heap size 1444 MB, throughput 0.980902
Reading from 25069: heap size 1451 MB, throughput 0.980382
Recommendation: one client; give it all the memory
Reading from 25069: heap size 1457 MB, throughput 0.980782
Reading from 25069: heap size 1464 MB, throughput 0.98
Reading from 25069: heap size 1471 MB, throughput 0.979662
Recommendation: one client; give it all the memory
Reading from 25069: heap size 1474 MB, throughput 0.979672
Reading from 25069: heap size 1480 MB, throughput 0.979359
Recommendation: one client; give it all the memory
Reading from 25069: heap size 1481 MB, throughput 0.978979
Reading from 25069: heap size 1487 MB, throughput 0.980357
Recommendation: one client; give it all the memory
Reading from 25069: heap size 1487 MB, throughput 0.980264
Reading from 25069: heap size 1490 MB, throughput 0.980329
Recommendation: one client; give it all the memory
Reading from 25069: heap size 1491 MB, throughput 0.97808
Reading from 25069: heap size 1493 MB, throughput 0.97886
Recommendation: one client; give it all the memory
Reading from 25069: heap size 1494 MB, throughput 0.979144
Reading from 25069: heap size 1495 MB, throughput 0.97988
Reading from 25069: heap size 1497 MB, throughput 0.980182
Recommendation: one client; give it all the memory
Reading from 25069: heap size 1497 MB, throughput 0.978856
Recommendation: one client; give it all the memory
Reading from 25069: heap size 1499 MB, throughput 0.813739
Reading from 25069: heap size 1548 MB, throughput 0.9961
Recommendation: one client; give it all the memory
Reading from 25069: heap size 1550 MB, throughput 0.99469
Reading from 25069: heap size 1563 MB, throughput 0.993661
Recommendation: one client; give it all the memory
Reading from 25069: heap size 1569 MB, throughput 0.992237
Reading from 25069: heap size 1571 MB, throughput 0.990439
Recommendation: one client; give it all the memory
Reading from 25069: heap size 1424 MB, throughput 0.988349
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 25069: heap size 1559 MB, throughput 0.997248
Recommendation: one client; give it all the memory
Reading from 25069: heap size 1566 MB, throughput 0.987903
Reading from 25069: heap size 1549 MB, throughput 0.881501
Reading from 25069: heap size 1566 MB, throughput 0.741478
Reading from 25069: heap size 1583 MB, throughput 0.763441
Reading from 25069: heap size 1612 MB, throughput 0.738293
Reading from 25069: heap size 1648 MB, throughput 0.773276
Reading from 25069: heap size 1667 MB, throughput 0.754632
Client 25069 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
