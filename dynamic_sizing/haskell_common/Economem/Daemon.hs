module Economem.Daemon (Daemon(..),
                        RecommendationType(..),
                        ThroughputType(..),
                        simpleThroughput,
                        vengerov,
                        avgVengerov,
                        majorOnly,
                        simpleSmoothing,
                        proportional,
                        availableThroughputs,
                        validThroughputNames,
                        makeDaemon,
                        daemonProcessLauncher,
                        throughputFromName,
                        plotBehaviour) where

import Control.Applicative
import Economem.Common
import Economem.Files
import qualified Economem.Process as P
import Economem.Records
import Economem.ResourceUsageFile
import System.Process

data ThroughputType = ThroughputType { throughputSocketName :: String } deriving (Read, Show, Eq)
instance Fileable ThroughputType

simpleThroughput :: ThroughputType
simpleThroughput = ThroughputType "simple"

vengerov :: ThroughputType
vengerov = ThroughputType "vengerov"

avgVengerov :: ThroughputType
avgVengerov = ThroughputType "avgVengerov"

majorOnly :: ThroughputType
majorOnly = ThroughputType "majorOnly"

simpleSmoothing :: ThroughputType
simpleSmoothing = ThroughputType "simpleSmoothing"

proportional :: ThroughputType
proportional = ThroughputType "proportional"

availableThroughputs :: [ThroughputType]
availableThroughputs = [simpleThroughput, vengerov, avgVengerov, majorOnly, simpleSmoothing, proportional]

validThroughputNames :: [String]
validThroughputNames = map throughputSocketName availableThroughputs

throughputFromName :: String -> IO ThroughputType
throughputFromName s = case lookup s (zip validThroughputNames availableThroughputs) of
  Just throughput -> return throughput
  Nothing -> fail $ "Unrecognised throughput type: " ++ s


data RecommendationType = Normal | Equal deriving Eq

data Daemon = Daemon { totalMem :: DataSize,
                       timerInterval :: Maybe Int,
                       startPaused :: Bool,
                       utilityModel :: Maybe String,
                       readingCaches :: [String],
                       forceFunctionValidity :: Bool,
                       alwaysNumericOptimisation :: Bool,
                       recommendOnTimer :: Bool,
                       recommendationType :: RecommendationType,
                       readingRecv :: Maybe String,
                       commandRecv :: Maybe String,
                       dumpFile :: Maybe String,
                       logFile :: Maybe String,
                       logReadings :: Bool,
                       logFunctions :: Bool,
                       verbose :: Bool }

makeDaemon :: DataSize -> Daemon
makeDaemon mem =
  Daemon { totalMem = mem,
           timerInterval = Nothing,
           startPaused = False,
           utilityModel = Nothing,
           readingCaches = [],
           forceFunctionValidity = True,
           alwaysNumericOptimisation = False,
           recommendOnTimer = True,
           recommendationType = Normal,
           readingRecv = Nothing,
           commandRecv = Nothing,
           dumpFile = Nothing,
           logFile = Nothing,
           logReadings = False,
           logFunctions = False,
           verbose = False }

daemonToArgs :: Daemon -> [String]
daemonToArgs d =
  intArg "--timer-interval" timerInterval ++
  ifTrue startPaused "--start-paused" ++
  arg "--utility-model" utilityModel ++
  listArg "--cache" readingCaches ++
  ifFalse forceFunctionValidity "--dont-force-function-validity" ++
  ifTrue alwaysNumericOptimisation "--always-numeric" ++
  ifFalse recommendOnTimer "--dont-recommend" ++
  ifTrue (\daemon -> recommendationType daemon == Equal) "--equal-recommendations" ++
  arg "--reading-recv" readingRecv ++
  arg "--command-recv" commandRecv ++
  arg "--dump-file" dumpFile ++
  arg "--log-file" logFile ++
  ifTrue logReadings "--log-readings" ++
  ifTrue logFunctions "--log-functions" ++
  ifTrue verbose "--verbose" ++
  [show (sizeToInt (toMB (totalMem d)))]

  where
    arg :: String -> (Daemon -> Maybe String) -> [String]
    arg s f = case f d of
      (Just a) -> [s, a]
      Nothing -> []

    intArg :: String -> (Daemon -> Maybe Int) -> [String]
    intArg s f = case f d of
      (Just a) -> [s, show a]
      Nothing -> []

    listArg :: String -> (Daemon -> [String]) -> [String]
    listArg s f = concatMap (\a -> [s, a]) (f d)

    ifTrue :: (Daemon -> Bool) -> String -> [String]
    ifTrue f s = if f d then [s] else []

    ifFalse :: (Daemon -> Bool) -> String -> [String]
    ifFalse f s = if f d then [] else [s]

daemonExecutable :: IO FilePath
daemonExecutable = (</> "daemon" </> "economemd") <$> economemRoot

daemonProcessLauncher :: Daemon -> FilePath -> FilePath -> Maybe ResourceUsageFile -> IO P.ProcessLauncher
daemonProcessLauncher d outfile workingDir res = do
  exe <- daemonExecutable
  (realCmd, realArgs) <- P.modifyArgs res (exe, daemonToArgs d)
  return P.ProcessLauncher { P.procCmd = realCmd,
                             P.procArgs = realArgs,
                             P.procWorkingDir = workingDir,
                             P.procExtraEnvironment = [],
                             P.procInfile = Just devnull,
                             P.procOutfile = Just outfile,
                             P.procSetupActions = [],
                             P.procStartedActions = [],
                             P.procCleanupActions = [] }

plotBehaviour :: String -> ThroughputType -> FilePath -> FilePath -> FilePath -> Maybe (CsvFile PidName) -> IO ()
plotBehaviour title throughput inFile outFile highestMemFile pidNamesFile = do
  behaviourPlotter <- (</> "behaviour_plotter" </> "plot_behaviour_batch.py") <$> measurementScriptsDir
  h <- runProcess
       behaviourPlotter
       (["--dontDrawRs",
         "--title",
         title,
         "--highestUsedFile",
         highestMemFile] ++
        (case pidNamesFile of
          Nothing -> []
          Just f -> ["--pidNamesFile", getPath f]) ++
        [throughputSocketName throughput,
         inFile,
         outFile])
       Nothing
       Nothing
       Nothing
       Nothing
       Nothing
  _ <- waitForProcess h
  return ()
