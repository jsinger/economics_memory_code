economemd
    total memory: 7435 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub4_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 2898: heap size 9 MB, throughput 0.992426
Clients: 1
Client 2898 has a minimum heap size of 276 MB
Reading from 2897: heap size 9 MB, throughput 0.988643
Clients: 2
Client 2897 has a minimum heap size of 1211 MB
Reading from 2897: heap size 9 MB, throughput 0.977617
Reading from 2898: heap size 9 MB, throughput 0.974297
Reading from 2898: heap size 9 MB, throughput 0.956967
Reading from 2897: heap size 11 MB, throughput 0.971666
Reading from 2898: heap size 9 MB, throughput 0.931926
Reading from 2897: heap size 11 MB, throughput 0.959024
Reading from 2898: heap size 11 MB, throughput 0.952653
Reading from 2898: heap size 11 MB, throughput 0.986496
Reading from 2897: heap size 15 MB, throughput 0.89578
Reading from 2898: heap size 17 MB, throughput 0.959795
Reading from 2897: heap size 18 MB, throughput 0.82304
Reading from 2898: heap size 17 MB, throughput 0.598743
Reading from 2897: heap size 25 MB, throughput 0.932545
Reading from 2898: heap size 30 MB, throughput 0.933701
Reading from 2898: heap size 31 MB, throughput 0.928515
Reading from 2897: heap size 28 MB, throughput 0.484925
Reading from 2898: heap size 34 MB, throughput 0.611229
Reading from 2898: heap size 47 MB, throughput 0.856816
Reading from 2897: heap size 43 MB, throughput 0.969526
Reading from 2898: heap size 50 MB, throughput 0.806843
Reading from 2897: heap size 44 MB, throughput 0.888571
Reading from 2898: heap size 51 MB, throughput 0.213488
Reading from 2897: heap size 48 MB, throughput 0.264454
Reading from 2898: heap size 69 MB, throughput 0.803239
Reading from 2897: heap size 67 MB, throughput 0.77168
Reading from 2898: heap size 72 MB, throughput 0.874452
Reading from 2897: heap size 72 MB, throughput 0.728261
Reading from 2898: heap size 76 MB, throughput 0.227542
Reading from 2897: heap size 73 MB, throughput 0.265109
Reading from 2898: heap size 96 MB, throughput 0.697164
Reading from 2897: heap size 100 MB, throughput 0.70062
Reading from 2898: heap size 104 MB, throughput 0.823963
Reading from 2897: heap size 102 MB, throughput 0.818374
Reading from 2897: heap size 107 MB, throughput 0.66228
Reading from 2897: heap size 111 MB, throughput 0.488555
Reading from 2898: heap size 105 MB, throughput 0.243793
Reading from 2898: heap size 137 MB, throughput 0.803522
Reading from 2898: heap size 138 MB, throughput 0.812377
Reading from 2898: heap size 141 MB, throughput 0.719722
Reading from 2898: heap size 146 MB, throughput 0.68371
Reading from 2897: heap size 116 MB, throughput 0.161314
Reading from 2898: heap size 149 MB, throughput 0.754868
Reading from 2897: heap size 141 MB, throughput 0.28662
Reading from 2897: heap size 166 MB, throughput 0.319705
Reading from 2897: heap size 185 MB, throughput 0.944849
Reading from 2898: heap size 154 MB, throughput 0.107076
Reading from 2897: heap size 190 MB, throughput 0.853003
Reading from 2898: heap size 200 MB, throughput 0.69603
Reading from 2897: heap size 194 MB, throughput 0.806113
Reading from 2898: heap size 202 MB, throughput 0.715095
Reading from 2897: heap size 194 MB, throughput 0.791307
Reading from 2897: heap size 196 MB, throughput 0.675218
Reading from 2898: heap size 205 MB, throughput 0.792587
Reading from 2897: heap size 202 MB, throughput 0.604359
Reading from 2897: heap size 212 MB, throughput 0.713918
Reading from 2897: heap size 215 MB, throughput 0.589536
Reading from 2897: heap size 217 MB, throughput 0.627489
Reading from 2898: heap size 212 MB, throughput 0.899176
Reading from 2897: heap size 225 MB, throughput 0.639173
Reading from 2898: heap size 214 MB, throughput 0.797389
Reading from 2897: heap size 229 MB, throughput 0.652358
Reading from 2898: heap size 226 MB, throughput 0.548845
Reading from 2898: heap size 231 MB, throughput 0.530246
Reading from 2897: heap size 239 MB, throughput 0.55341
Reading from 2897: heap size 243 MB, throughput 0.571822
Reading from 2897: heap size 255 MB, throughput 0.458351
Reading from 2898: heap size 234 MB, throughput 0.178855
Reading from 2898: heap size 277 MB, throughput 0.522061
Reading from 2898: heap size 278 MB, throughput 0.771384
Reading from 2898: heap size 271 MB, throughput 0.804495
Reading from 2897: heap size 259 MB, throughput 0.0511884
Reading from 2898: heap size 275 MB, throughput 0.68866
Reading from 2897: heap size 305 MB, throughput 0.340831
Reading from 2897: heap size 313 MB, throughput 0.400858
Reading from 2898: heap size 270 MB, throughput 0.840356
Reading from 2898: heap size 273 MB, throughput 0.687142
Reading from 2898: heap size 277 MB, throughput 0.766846
Reading from 2898: heap size 277 MB, throughput 0.503756
Reading from 2898: heap size 278 MB, throughput 0.694328
Reading from 2897: heap size 316 MB, throughput 0.0520919
Reading from 2897: heap size 359 MB, throughput 0.439361
Reading from 2897: heap size 282 MB, throughput 0.643171
Reading from 2897: heap size 346 MB, throughput 0.613451
Reading from 2897: heap size 296 MB, throughput 0.603468
Reading from 2898: heap size 280 MB, throughput 0.899314
Reading from 2897: heap size 347 MB, throughput 0.534034
Reading from 2897: heap size 349 MB, throughput 0.49643
Reading from 2898: heap size 280 MB, throughput 0.212594
Reading from 2898: heap size 325 MB, throughput 0.729596
Reading from 2898: heap size 331 MB, throughput 0.756529
Reading from 2898: heap size 331 MB, throughput 0.833638
Reading from 2897: heap size 352 MB, throughput 0.0844521
Reading from 2898: heap size 336 MB, throughput 0.803283
Reading from 2897: heap size 404 MB, throughput 0.440037
Reading from 2897: heap size 396 MB, throughput 0.484238
Reading from 2898: heap size 337 MB, throughput 0.823217
Reading from 2897: heap size 402 MB, throughput 0.535758
Reading from 2898: heap size 339 MB, throughput 0.655452
Reading from 2898: heap size 340 MB, throughput 0.489947
Reading from 2897: heap size 403 MB, throughput 0.549928
Reading from 2898: heap size 348 MB, throughput 0.655668
Reading from 2897: heap size 403 MB, throughput 0.569761
Reading from 2898: heap size 349 MB, throughput 0.750824
Reading from 2897: heap size 406 MB, throughput 0.632957
Reading from 2898: heap size 360 MB, throughput 0.749188
Reading from 2897: heap size 409 MB, throughput 0.544034
Reading from 2897: heap size 413 MB, throughput 0.511028
Reading from 2897: heap size 417 MB, throughput 0.399534
Reading from 2897: heap size 422 MB, throughput 0.473591
Equal recommendation: 3717 MB each
Reading from 2898: heap size 361 MB, throughput 0.942545
Reading from 2897: heap size 429 MB, throughput 0.0725401
Reading from 2897: heap size 482 MB, throughput 0.299764
Reading from 2897: heap size 488 MB, throughput 0.525906
Reading from 2897: heap size 492 MB, throughput 0.470495
Reading from 2897: heap size 493 MB, throughput 0.492945
Reading from 2897: heap size 498 MB, throughput 0.586437
Reading from 2898: heap size 368 MB, throughput 0.967194
Reading from 2897: heap size 501 MB, throughput 0.0914007
Reading from 2897: heap size 561 MB, throughput 0.326974
Reading from 2897: heap size 563 MB, throughput 0.347582
Reading from 2898: heap size 371 MB, throughput 0.95487
Reading from 2897: heap size 566 MB, throughput 0.620031
Reading from 2897: heap size 569 MB, throughput 0.541177
Reading from 2897: heap size 574 MB, throughput 0.100847
Reading from 2897: heap size 637 MB, throughput 0.496172
Reading from 2897: heap size 639 MB, throughput 0.641621
Reading from 2898: heap size 371 MB, throughput 0.978555
Reading from 2897: heap size 642 MB, throughput 0.575433
Reading from 2897: heap size 646 MB, throughput 0.760468
Reading from 2897: heap size 650 MB, throughput 0.850884
Reading from 2898: heap size 374 MB, throughput 0.976607
Reading from 2897: heap size 650 MB, throughput 0.827254
Reading from 2897: heap size 661 MB, throughput 0.818256
Reading from 2897: heap size 665 MB, throughput 0.401679
Reading from 2898: heap size 377 MB, throughput 0.952479
Reading from 2897: heap size 680 MB, throughput 0.0401784
Reading from 2897: heap size 752 MB, throughput 0.467242
Reading from 2898: heap size 379 MB, throughput 0.960765
Reading from 2897: heap size 756 MB, throughput 0.271088
Reading from 2897: heap size 751 MB, throughput 0.344051
Equal recommendation: 3717 MB each
Reading from 2897: heap size 670 MB, throughput 0.0449814
Reading from 2897: heap size 824 MB, throughput 0.273333
Reading from 2897: heap size 827 MB, throughput 0.843475
Reading from 2897: heap size 832 MB, throughput 0.766092
Reading from 2898: heap size 378 MB, throughput 0.983219
Reading from 2897: heap size 834 MB, throughput 0.637087
Reading from 2897: heap size 833 MB, throughput 0.609171
Reading from 2897: heap size 836 MB, throughput 0.631159
Reading from 2898: heap size 381 MB, throughput 0.965039
Reading from 2897: heap size 838 MB, throughput 0.114336
Reading from 2897: heap size 922 MB, throughput 0.523823
Reading from 2897: heap size 924 MB, throughput 0.903541
Reading from 2897: heap size 929 MB, throughput 0.932725
Reading from 2897: heap size 932 MB, throughput 0.893742
Reading from 2897: heap size 934 MB, throughput 0.909676
Reading from 2897: heap size 924 MB, throughput 0.913616
Reading from 2898: heap size 382 MB, throughput 0.981072
Reading from 2897: heap size 787 MB, throughput 0.814655
Reading from 2897: heap size 899 MB, throughput 0.760599
Reading from 2897: heap size 791 MB, throughput 0.755391
Reading from 2897: heap size 886 MB, throughput 0.741004
Reading from 2897: heap size 789 MB, throughput 0.782846
Reading from 2897: heap size 880 MB, throughput 0.796618
Reading from 2897: heap size 795 MB, throughput 0.760603
Reading from 2897: heap size 875 MB, throughput 0.792598
Reading from 2897: heap size 881 MB, throughput 0.826144
Reading from 2897: heap size 871 MB, throughput 0.80156
Reading from 2898: heap size 383 MB, throughput 0.973331
Reading from 2897: heap size 877 MB, throughput 0.972135
Reading from 2898: heap size 387 MB, throughput 0.969478
Reading from 2897: heap size 876 MB, throughput 0.968426
Reading from 2897: heap size 879 MB, throughput 0.77392
Reading from 2897: heap size 884 MB, throughput 0.731478
Reading from 2897: heap size 884 MB, throughput 0.782466
Reading from 2897: heap size 887 MB, throughput 0.791057
Reading from 2897: heap size 889 MB, throughput 0.782012
Reading from 2897: heap size 892 MB, throughput 0.784323
Reading from 2897: heap size 894 MB, throughput 0.742751
Reading from 2898: heap size 388 MB, throughput 0.971791
Equal recommendation: 3717 MB each
Reading from 2897: heap size 898 MB, throughput 0.818366
Reading from 2897: heap size 899 MB, throughput 0.902966
Reading from 2897: heap size 902 MB, throughput 0.843368
Reading from 2898: heap size 389 MB, throughput 0.971498
Reading from 2898: heap size 391 MB, throughput 0.758551
Reading from 2897: heap size 904 MB, throughput 0.860756
Reading from 2898: heap size 388 MB, throughput 0.812937
Reading from 2897: heap size 906 MB, throughput 0.749422
Reading from 2898: heap size 393 MB, throughput 0.816018
Reading from 2897: heap size 907 MB, throughput 0.660653
Reading from 2897: heap size 919 MB, throughput 0.631353
Reading from 2898: heap size 402 MB, throughput 0.967311
Reading from 2897: heap size 931 MB, throughput 0.0656296
Reading from 2897: heap size 1060 MB, throughput 0.593203
Reading from 2897: heap size 1061 MB, throughput 0.680349
Reading from 2897: heap size 1068 MB, throughput 0.71358
Reading from 2898: heap size 404 MB, throughput 0.985123
Reading from 2897: heap size 1070 MB, throughput 0.668138
Reading from 2897: heap size 1084 MB, throughput 0.68189
Reading from 2897: heap size 1084 MB, throughput 0.679967
Reading from 2897: heap size 1101 MB, throughput 0.750346
Reading from 2898: heap size 407 MB, throughput 0.976829
Reading from 2897: heap size 1102 MB, throughput 0.955228
Reading from 2898: heap size 409 MB, throughput 0.757453
Reading from 2897: heap size 1122 MB, throughput 0.954447
Equal recommendation: 3717 MB each
Reading from 2897: heap size 1125 MB, throughput 0.950469
Reading from 2898: heap size 441 MB, throughput 0.995532
Reading from 2897: heap size 1133 MB, throughput 0.963762
Reading from 2898: heap size 444 MB, throughput 0.992859
Reading from 2897: heap size 1136 MB, throughput 0.957514
Reading from 2898: heap size 452 MB, throughput 0.99125
Reading from 2897: heap size 1131 MB, throughput 0.953613
Reading from 2898: heap size 453 MB, throughput 0.988195
Reading from 2897: heap size 1138 MB, throughput 0.955698
Reading from 2898: heap size 453 MB, throughput 0.989178
Reading from 2897: heap size 1128 MB, throughput 0.959739
Equal recommendation: 3717 MB each
Reading from 2898: heap size 455 MB, throughput 0.986173
Reading from 2897: heap size 1135 MB, throughput 0.952364
Reading from 2898: heap size 450 MB, throughput 0.987672
Reading from 2898: heap size 453 MB, throughput 0.880078
Reading from 2898: heap size 450 MB, throughput 0.851093
Reading from 2898: heap size 455 MB, throughput 0.898448
Reading from 2897: heap size 1135 MB, throughput 0.95502
Reading from 2898: heap size 464 MB, throughput 0.988252
Reading from 2897: heap size 1137 MB, throughput 0.953066
Reading from 2898: heap size 465 MB, throughput 0.988721
Reading from 2897: heap size 1143 MB, throughput 0.95058
Reading from 2898: heap size 464 MB, throughput 0.98682
Reading from 2897: heap size 1144 MB, throughput 0.953766
Equal recommendation: 3717 MB each
Reading from 2898: heap size 467 MB, throughput 0.987854
Reading from 2897: heap size 1150 MB, throughput 0.954095
Reading from 2898: heap size 468 MB, throughput 0.98554
Reading from 2897: heap size 1153 MB, throughput 0.952476
Reading from 2898: heap size 470 MB, throughput 0.983949
Reading from 2897: heap size 1159 MB, throughput 0.954595
Reading from 2898: heap size 474 MB, throughput 0.983566
Reading from 2897: heap size 1163 MB, throughput 0.950238
Reading from 2898: heap size 475 MB, throughput 0.984258
Reading from 2897: heap size 1171 MB, throughput 0.950476
Equal recommendation: 3717 MB each
Reading from 2898: heap size 478 MB, throughput 0.989436
Reading from 2898: heap size 479 MB, throughput 0.886833
Reading from 2898: heap size 479 MB, throughput 0.877318
Reading from 2897: heap size 1174 MB, throughput 0.958824
Reading from 2898: heap size 481 MB, throughput 0.980467
Reading from 2897: heap size 1181 MB, throughput 0.957874
Reading from 2898: heap size 489 MB, throughput 0.992028
Reading from 2897: heap size 1183 MB, throughput 0.955321
Reading from 2898: heap size 490 MB, throughput 0.991778
Reading from 2897: heap size 1191 MB, throughput 0.954184
Equal recommendation: 3717 MB each
Reading from 2898: heap size 492 MB, throughput 0.990102
Reading from 2897: heap size 1192 MB, throughput 0.952526
Reading from 2898: heap size 494 MB, throughput 0.987427
Reading from 2897: heap size 1198 MB, throughput 0.528529
Reading from 2898: heap size 493 MB, throughput 0.990043
Reading from 2897: heap size 1295 MB, throughput 0.930766
Reading from 2898: heap size 495 MB, throughput 0.987047
Reading from 2897: heap size 1299 MB, throughput 0.972056
Equal recommendation: 3717 MB each
Reading from 2898: heap size 497 MB, throughput 0.984493
Reading from 2898: heap size 498 MB, throughput 0.979826
Reading from 2898: heap size 505 MB, throughput 0.88937
Reading from 2897: heap size 1303 MB, throughput 0.969961
Reading from 2898: heap size 506 MB, throughput 0.963832
Reading from 2897: heap size 1310 MB, throughput 0.968797
Reading from 2898: heap size 514 MB, throughput 0.993229
Reading from 2897: heap size 1311 MB, throughput 0.971944
Reading from 2898: heap size 514 MB, throughput 0.990966
Reading from 2897: heap size 1307 MB, throughput 0.967996
Equal recommendation: 3717 MB each
Reading from 2898: heap size 516 MB, throughput 0.991422
Reading from 2897: heap size 1311 MB, throughput 0.96361
Reading from 2898: heap size 518 MB, throughput 0.989036
Reading from 2897: heap size 1300 MB, throughput 0.964101
Reading from 2898: heap size 516 MB, throughput 0.989181
Reading from 2897: heap size 1306 MB, throughput 0.961947
Reading from 2898: heap size 519 MB, throughput 0.986535
Equal recommendation: 3717 MB each
Reading from 2897: heap size 1306 MB, throughput 0.960982
Reading from 2898: heap size 521 MB, throughput 0.99248
Reading from 2898: heap size 522 MB, throughput 0.921922
Reading from 2898: heap size 522 MB, throughput 0.905952
Reading from 2897: heap size 1307 MB, throughput 0.958821
Reading from 2898: heap size 524 MB, throughput 0.989614
Reading from 2897: heap size 1313 MB, throughput 0.969019
Reading from 2898: heap size 532 MB, throughput 0.992093
Reading from 2897: heap size 1315 MB, throughput 0.956363
Reading from 2898: heap size 533 MB, throughput 0.989993
Equal recommendation: 3717 MB each
Reading from 2897: heap size 1321 MB, throughput 0.958353
Reading from 2898: heap size 534 MB, throughput 0.989884
Reading from 2897: heap size 1327 MB, throughput 0.956476
Reading from 2898: heap size 536 MB, throughput 0.988642
Reading from 2897: heap size 1334 MB, throughput 0.952117
Reading from 2898: heap size 538 MB, throughput 0.98818
Reading from 2897: heap size 1342 MB, throughput 0.951033
Equal recommendation: 3717 MB each
Reading from 2898: heap size 539 MB, throughput 0.992284
Reading from 2898: heap size 541 MB, throughput 0.923997
Reading from 2898: heap size 542 MB, throughput 0.978
Reading from 2898: heap size 551 MB, throughput 0.993171
Reading from 2898: heap size 551 MB, throughput 0.990989
Equal recommendation: 3717 MB each
Reading from 2898: heap size 552 MB, throughput 0.990585
Reading from 2898: heap size 554 MB, throughput 0.990086
Reading from 2897: heap size 1351 MB, throughput 0.838182
Reading from 2898: heap size 555 MB, throughput 0.989949
Reading from 2898: heap size 556 MB, throughput 0.990915
Equal recommendation: 3717 MB each
Reading from 2898: heap size 561 MB, throughput 0.927818
Reading from 2898: heap size 561 MB, throughput 0.985325
Reading from 2898: heap size 568 MB, throughput 0.993625
Reading from 2898: heap size 569 MB, throughput 0.9911
Equal recommendation: 3717 MB each
Reading from 2898: heap size 569 MB, throughput 0.991538
Reading from 2897: heap size 1504 MB, throughput 0.93074
Reading from 2898: heap size 571 MB, throughput 0.990043
Reading from 2897: heap size 1468 MB, throughput 0.994757
Reading from 2897: heap size 1473 MB, throughput 0.922658
Reading from 2897: heap size 1481 MB, throughput 0.890729
Reading from 2897: heap size 1486 MB, throughput 0.893909
Reading from 2897: heap size 1487 MB, throughput 0.875018
Reading from 2898: heap size 574 MB, throughput 0.988653
Reading from 2897: heap size 1491 MB, throughput 0.899921
Reading from 2898: heap size 574 MB, throughput 0.977184
Equal recommendation: 3717 MB each
Reading from 2898: heap size 575 MB, throughput 0.925532
Reading from 2897: heap size 1498 MB, throughput 0.980093
Reading from 2898: heap size 578 MB, throughput 0.992601
Reading from 2897: heap size 1503 MB, throughput 0.988882
Reading from 2897: heap size 1529 MB, throughput 0.984131
Reading from 2898: heap size 584 MB, throughput 0.993205
Reading from 2897: heap size 1533 MB, throughput 0.983189
Equal recommendation: 3717 MB each
Reading from 2898: heap size 586 MB, throughput 0.992792
Reading from 2897: heap size 1538 MB, throughput 0.983874
Reading from 2898: heap size 585 MB, throughput 0.990705
Reading from 2897: heap size 1543 MB, throughput 0.97881
Reading from 2898: heap size 588 MB, throughput 0.989219
Reading from 2898: heap size 591 MB, throughput 0.988818
Reading from 2898: heap size 592 MB, throughput 0.864484
Reading from 2897: heap size 1533 MB, throughput 0.966783
Equal recommendation: 3717 MB each
Reading from 2898: heap size 596 MB, throughput 0.992327
Reading from 2897: heap size 1541 MB, throughput 0.973977
Reading from 2898: heap size 598 MB, throughput 0.993371
Reading from 2897: heap size 1548 MB, throughput 0.973327
Reading from 2898: heap size 601 MB, throughput 0.992791
Equal recommendation: 3717 MB each
Reading from 2897: heap size 1548 MB, throughput 0.970485
Reading from 2897: heap size 1558 MB, throughput 0.968843
Reading from 2898: heap size 604 MB, throughput 0.991442
Reading from 2897: heap size 1564 MB, throughput 0.966284
Reading from 2898: heap size 606 MB, throughput 0.989476
Reading from 2898: heap size 608 MB, throughput 0.984177
Reading from 2898: heap size 614 MB, throughput 0.946281
Equal recommendation: 3717 MB each
Reading from 2897: heap size 1574 MB, throughput 0.962987
Reading from 2898: heap size 615 MB, throughput 0.994481
Reading from 2897: heap size 1585 MB, throughput 0.964517
Reading from 2898: heap size 620 MB, throughput 0.993099
Reading from 2897: heap size 1596 MB, throughput 0.966904
Equal recommendation: 3717 MB each
Reading from 2898: heap size 621 MB, throughput 0.99139
Reading from 2897: heap size 1611 MB, throughput 0.956142
Reading from 2898: heap size 621 MB, throughput 0.991245
Reading from 2897: heap size 1624 MB, throughput 0.959218
Reading from 2898: heap size 623 MB, throughput 0.994182
Reading from 2898: heap size 627 MB, throughput 0.932152
Reading from 2897: heap size 1639 MB, throughput 0.960286
Equal recommendation: 3717 MB each
Reading from 2898: heap size 627 MB, throughput 0.993058
Reading from 2897: heap size 1656 MB, throughput 0.963253
Reading from 2898: heap size 634 MB, throughput 0.993598
Reading from 2897: heap size 1666 MB, throughput 0.956258
Reading from 2898: heap size 636 MB, throughput 0.992492
Equal recommendation: 3717 MB each
Reading from 2897: heap size 1683 MB, throughput 0.957848
Reading from 2898: heap size 636 MB, throughput 0.992413
Reading from 2897: heap size 1688 MB, throughput 0.957396
Reading from 2897: heap size 1706 MB, throughput 0.958966
Reading from 2898: heap size 638 MB, throughput 0.995365
Reading from 2898: heap size 641 MB, throughput 0.951578
Equal recommendation: 3717 MB each
Reading from 2898: heap size 642 MB, throughput 0.990407
Reading from 2897: heap size 1709 MB, throughput 0.951271
Reading from 2898: heap size 649 MB, throughput 0.994246
Reading from 2897: heap size 1726 MB, throughput 0.960583
Reading from 2898: heap size 650 MB, throughput 0.993184
Equal recommendation: 3717 MB each
Reading from 2897: heap size 1728 MB, throughput 0.734472
Reading from 2898: heap size 650 MB, throughput 0.992511
Reading from 2897: heap size 1826 MB, throughput 0.991889
Reading from 2898: heap size 652 MB, throughput 0.994723
Reading from 2898: heap size 653 MB, throughput 0.946876
Client 2898 died
Clients: 1
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 2897: heap size 1826 MB, throughput 0.995732
Reading from 2897: heap size 1843 MB, throughput 0.984936
Reading from 2897: heap size 1850 MB, throughput 0.849335
Reading from 2897: heap size 1850 MB, throughput 0.853213
Reading from 2897: heap size 1861 MB, throughput 0.839927
Client 2897 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
