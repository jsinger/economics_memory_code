Static Heap Sizing
==================

This folder contains scripts to run the experiments reported in the paper, and also the data from our runs of those experiments.


Dependencies
------------

If you are on a recent Ubuntu-based system (13.10 or 14.04 recommended), just run `./packages.sh` and then install [argtypes](https://github.com/CallumCameron/argtypes). Otherwise, install:

 - Python 2.7
 - NumPy
 - SciPy
 - MatPlotLib
 - [Argtypes](https://github.com/CallumCameron/argtypes) (NOTE: not installed by packages.sh!)
 - An OpenJDK installation accessible on your path (the paper used OpenJDK 7)
 - pdfunite
 - the cgroups libraries and commands (cgcreate, cgexec)
 - wget
 - Ruby (only for the strace parsing scripts)

If you want to run experiments remotely on many machines (as was done for the paper), you'll need the [Distributor](https://bitbucket.org/CallumCameron/distributor) scripts too, and will need to make sure this repo is cloned to `economics_memory_code` in your home directory. If you just want to run things locally, you don't need these scripts, and you can clone to anywhere you like.


Measurement Scripts
-------------------

These scripts profile the DaCapo benchmarks and generate individual throughput functions for them.

Running measurements:
 - measureLocal.py: run measurements locally. See usage string for details.
 - measureRemote.py: run measurements remotely using Distributor. See usage string for details.

Processing the data:
 - measurePostprocess.py: generate graphs from raw data

Misc:
 - measureFromMinHeap.sh: generate a config file for use with measureLocal or measureRemote, from all the benchmarks in a min heap file

For internal use:
 - measureBenchmark.py
 - measureRemoteHarness.py


Utility Space Scripts
---------------------

These scripts use the throughput functions produced by the measurement scripts to generate combined utility space maps for combinations of benchmarks, by measuring performance at various heap sizes.

Running measurements:
 - utilitySpaceLocal.py: run locally; see usage string for details.
 - utilitySpaceRemote.py: run on remote machines; see usage string for details.

Processing the data:
 - utilitySpaceImportDefaultUtility.py: attempt to pull in default utility measurements for all existing datasets; check the results manually!
 - utilitySpacePostprocess.py: generate graphs for an existing dataset
 - utilitySpaceTabulateResults.py: generate a table summarising existing results
 - utilitySpaceMergePdfs.py: pull together graphs from all existing datasets into one file
 - utilitySpaceImprovementGraph.py: generate the 'improvement' graph (Figure 9 in paper) from the average of all existing datasets.
 - utilitySpacePostprocessAll.py: all of the above, in one go
 - utilitySpaceLatexTable.py: generate comparison table (Table 1 in paper) from all existing datasets.

Visualising the data interactively:
 - utilitySpaceViewDifference.py
 - utilitySpaceViewMeasured.py
 - utilitySpaceViewPredicted.py

For internal use:
 - utilitySpacePoint.py: run a single combination (but it's up to the caller to set things up)
 - utilitySpaceRemoteHarness.py


Default Utility Scripts
-----------------------

These scripts measure the performance of benchmark combinations without fixing the heap size, i.e. when HotSpot ergonomics is left to its own devices. Cgroups memory limiting is used to impose a budget for fair comparison. A better name might have been 'utility under default conditions'.

If you generate default utility data for a combination for which you also have a utility space dataset, copy defaultUtility_results*.txt to the utility space folder, and they will be automatically picked up by the postprocessing scripts. The script utilitySpaceImportDefaultUtility.py attempts to do this, but it isn't a completely reliable process, and should be verified manually!

Setup:
 - defaultUtilityCreateCgroup.sh: sets up the necessary cgroup, ready for running

Running:
 - defaultUtilityCombination.py: measure a single combination
 - defaultUtilityFromUtilitySpaceDirs.py: measure for the combinations in the specified directories

Processing the data:
 - defaultUtilityPostprocess.py: process the data for an existing dataset
 - defaultUtilityPostprocessAll.py: process the data for all existing datasets


Strace Scripts
--------------

In the paper, strace was used to investigate whether identical benchmark pairs with equal memory partitioning interfere with each other. To generate data for this, run utilitySpacePoint.py with the --strace option. The scripts straceParse.rb and straceCountSyscalls.rb summarise the data produced.


Miscellaneous Scripts
---------------------

 - numericalOptimisationSanityCheck.py: compares the results of the analytical and numerical methods of calculating the best partition, for all existing utility space results.
 - numericalOptimisationRuntimeTest.py: tests whether varying the number of dimensions has any effect on the runtime of numerical optimisation; used to back up our claim that reducing the number of dimensions makes things run faster.
