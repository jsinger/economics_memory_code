economemd
    total memory: 2470 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub31_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Reading from 10815: heap size 9 MB, throughput 0.988569
Clients: 1
Client 10815 has a minimum heap size of 12 MB
Recommendation: one client; give it all the memory
Reading from 10814: heap size 9 MB, throughput 0.989949
Clients: 2
Client 10814 has a minimum heap size of 1223 MB
Reading from 10815: heap size 9 MB, throughput 0.987138
Reading from 10814: heap size 9 MB, throughput 0.978616
Reading from 10814: heap size 9 MB, throughput 0.97189
Reading from 10814: heap size 9 MB, throughput 0.94908
Reading from 10815: heap size 11 MB, throughput 0.978503
Reading from 10815: heap size 11 MB, throughput 0.974342
Reading from 10814: heap size 11 MB, throughput 0.92101
Reading from 10815: heap size 15 MB, throughput 0.985359
Reading from 10814: heap size 11 MB, throughput 0.976784
Reading from 10815: heap size 15 MB, throughput 0.985849
Reading from 10814: heap size 17 MB, throughput 0.950715
Reading from 10814: heap size 17 MB, throughput 0.527161
Reading from 10814: heap size 30 MB, throughput 0.949918
Reading from 10814: heap size 31 MB, throughput 0.915983
Reading from 10815: heap size 24 MB, throughput 0.87464
Reading from 10814: heap size 34 MB, throughput 0.34601
Reading from 10814: heap size 48 MB, throughput 0.919836
Reading from 10814: heap size 49 MB, throughput 0.930859
Reading from 10815: heap size 29 MB, throughput 0.989814
Reading from 10814: heap size 51 MB, throughput 0.222574
Reading from 10815: heap size 33 MB, throughput 0.935195
Reading from 10814: heap size 73 MB, throughput 0.752598
Reading from 10815: heap size 38 MB, throughput 0.981928
Reading from 10814: heap size 73 MB, throughput 0.268919
Reading from 10814: heap size 100 MB, throughput 0.883883
Reading from 10814: heap size 100 MB, throughput 0.828306
Reading from 10815: heap size 40 MB, throughput 0.99312
Reading from 10814: heap size 102 MB, throughput 0.844329
Reading from 10814: heap size 104 MB, throughput 0.788924
Reading from 10815: heap size 44 MB, throughput 0.981657
Reading from 10815: heap size 48 MB, throughput 0.981353
Reading from 10814: heap size 106 MB, throughput 0.179865
Reading from 10815: heap size 48 MB, throughput 0.983752
Reading from 10814: heap size 136 MB, throughput 0.616404
Reading from 10814: heap size 139 MB, throughput 0.594472
Reading from 10815: heap size 52 MB, throughput 0.977582
Reading from 10814: heap size 142 MB, throughput 0.735009
Reading from 10815: heap size 53 MB, throughput 0.977763
Reading from 10814: heap size 151 MB, throughput 0.659853
Reading from 10815: heap size 59 MB, throughput 0.947324
Reading from 10815: heap size 59 MB, throughput 0.390952
Reading from 10814: heap size 152 MB, throughput 0.170698
Reading from 10815: heap size 70 MB, throughput 0.989393
Reading from 10814: heap size 185 MB, throughput 0.616808
Reading from 10814: heap size 191 MB, throughput 0.722864
Reading from 10815: heap size 70 MB, throughput 0.993792
Reading from 10814: heap size 194 MB, throughput 0.708852
Reading from 10814: heap size 197 MB, throughput 0.64434
Reading from 10814: heap size 201 MB, throughput 0.486046
Reading from 10814: heap size 208 MB, throughput 0.605962
Reading from 10814: heap size 215 MB, throughput 0.630315
Reading from 10815: heap size 77 MB, throughput 0.997107
Reading from 10814: heap size 222 MB, throughput 0.123177
Reading from 10814: heap size 260 MB, throughput 0.604917
Reading from 10815: heap size 78 MB, throughput 0.994143
Reading from 10814: heap size 266 MB, throughput 0.62397
Reading from 10814: heap size 268 MB, throughput 0.588271
Reading from 10814: heap size 271 MB, throughput 0.549355
Reading from 10815: heap size 84 MB, throughput 0.996439
Reading from 10814: heap size 276 MB, throughput 0.117612
Reading from 10814: heap size 318 MB, throughput 0.534898
Reading from 10814: heap size 313 MB, throughput 0.664903
Reading from 10814: heap size 319 MB, throughput 0.63567
Reading from 10815: heap size 85 MB, throughput 0.995517
Reading from 10814: heap size 319 MB, throughput 0.680624
Reading from 10814: heap size 322 MB, throughput 0.572565
Reading from 10814: heap size 325 MB, throughput 0.629777
Reading from 10814: heap size 332 MB, throughput 0.5949
Reading from 10815: heap size 89 MB, throughput 0.9908
Reading from 10814: heap size 338 MB, throughput 0.117328
Reading from 10814: heap size 389 MB, throughput 0.541391
Reading from 10815: heap size 91 MB, throughput 0.995117
Reading from 10814: heap size 398 MB, throughput 0.635391
Reading from 10814: heap size 398 MB, throughput 0.492025
Reading from 10814: heap size 400 MB, throughput 0.613603
Reading from 10814: heap size 405 MB, throughput 0.530978
Reading from 10814: heap size 410 MB, throughput 0.535404
Reading from 10815: heap size 94 MB, throughput 0.995949
Reading from 10814: heap size 415 MB, throughput 0.517519
Reading from 10815: heap size 95 MB, throughput 0.995681
Reading from 10814: heap size 422 MB, throughput 0.0677882
Reading from 10815: heap size 97 MB, throughput 0.984707
Equal recommendation: 1235 MB each
Reading from 10814: heap size 482 MB, throughput 0.4209
Reading from 10814: heap size 473 MB, throughput 0.553469
Reading from 10814: heap size 479 MB, throughput 0.502506
Reading from 10814: heap size 482 MB, throughput 0.557215
Reading from 10815: heap size 98 MB, throughput 0.993214
Reading from 10814: heap size 482 MB, throughput 0.540141
Reading from 10815: heap size 101 MB, throughput 0.996041
Reading from 10814: heap size 488 MB, throughput 0.112319
Reading from 10814: heap size 547 MB, throughput 0.473012
Reading from 10815: heap size 102 MB, throughput 0.995776
Reading from 10814: heap size 544 MB, throughput 0.618686
Reading from 10814: heap size 549 MB, throughput 0.546369
Reading from 10814: heap size 554 MB, throughput 0.589516
Reading from 10814: heap size 556 MB, throughput 0.621436
Reading from 10815: heap size 104 MB, throughput 0.996884
Reading from 10815: heap size 105 MB, throughput 0.996145
Reading from 10814: heap size 561 MB, throughput 0.113117
Reading from 10814: heap size 628 MB, throughput 0.497868
Reading from 10815: heap size 107 MB, throughput 0.989249
Reading from 10814: heap size 639 MB, throughput 0.654785
Reading from 10814: heap size 642 MB, throughput 0.620576
Reading from 10814: heap size 644 MB, throughput 0.822694
Reading from 10815: heap size 107 MB, throughput 0.995773
Reading from 10814: heap size 655 MB, throughput 0.851276
Reading from 10815: heap size 110 MB, throughput 0.997608
Reading from 10814: heap size 658 MB, throughput 0.812404
Reading from 10815: heap size 110 MB, throughput 0.997623
Reading from 10814: heap size 675 MB, throughput 0.857136
Reading from 10814: heap size 682 MB, throughput 0.515284
Reading from 10815: heap size 113 MB, throughput 0.998228
Reading from 10815: heap size 113 MB, throughput 0.997539
Reading from 10815: heap size 115 MB, throughput 0.998031
Reading from 10814: heap size 698 MB, throughput 0.0486611
Reading from 10814: heap size 772 MB, throughput 0.457889
Reading from 10814: heap size 776 MB, throughput 0.296236
Reading from 10814: heap size 760 MB, throughput 0.344785
Reading from 10815: heap size 115 MB, throughput 0.998172
Reading from 10815: heap size 115 MB, throughput 0.998193
Reading from 10814: heap size 668 MB, throughput 0.0354758
Equal recommendation: 1235 MB each
Reading from 10814: heap size 841 MB, throughput 0.295758
Reading from 10814: heap size 843 MB, throughput 0.494229
Reading from 10815: heap size 116 MB, throughput 0.99788
Reading from 10814: heap size 847 MB, throughput 0.885612
Reading from 10814: heap size 848 MB, throughput 0.595306
Reading from 10814: heap size 845 MB, throughput 0.616065
Reading from 10814: heap size 848 MB, throughput 0.600945
Reading from 10815: heap size 117 MB, throughput 0.998446
Reading from 10815: heap size 117 MB, throughput 0.997442
Reading from 10815: heap size 119 MB, throughput 0.998043
Reading from 10814: heap size 851 MB, throughput 0.165378
Reading from 10814: heap size 941 MB, throughput 0.648459
Reading from 10814: heap size 945 MB, throughput 0.95187
Reading from 10814: heap size 949 MB, throughput 0.780497
Reading from 10815: heap size 119 MB, throughput 0.997281
Reading from 10814: heap size 949 MB, throughput 0.933339
Reading from 10815: heap size 119 MB, throughput 0.982061
Reading from 10814: heap size 951 MB, throughput 0.825943
Reading from 10815: heap size 120 MB, throughput 0.990385
Reading from 10814: heap size 940 MB, throughput 0.852721
Reading from 10815: heap size 123 MB, throughput 0.98306
Reading from 10814: heap size 772 MB, throughput 0.876482
Reading from 10814: heap size 914 MB, throughput 0.840332
Reading from 10814: heap size 802 MB, throughput 0.834988
Reading from 10814: heap size 898 MB, throughput 0.82655
Reading from 10815: heap size 123 MB, throughput 0.989208
Reading from 10814: heap size 806 MB, throughput 0.820538
Reading from 10814: heap size 889 MB, throughput 0.850446
Reading from 10814: heap size 802 MB, throughput 0.865102
Reading from 10814: heap size 882 MB, throughput 0.873991
Reading from 10815: heap size 128 MB, throughput 0.997771
Reading from 10814: heap size 807 MB, throughput 0.871171
Reading from 10814: heap size 878 MB, throughput 0.872115
Reading from 10814: heap size 882 MB, throughput 0.886253
Reading from 10815: heap size 128 MB, throughput 0.996381
Reading from 10814: heap size 875 MB, throughput 0.984697
Reading from 10815: heap size 131 MB, throughput 0.997273
Reading from 10815: heap size 132 MB, throughput 0.996975
Reading from 10814: heap size 879 MB, throughput 0.971542
Reading from 10814: heap size 876 MB, throughput 0.879895
Reading from 10815: heap size 135 MB, throughput 0.996269
Reading from 10814: heap size 881 MB, throughput 0.770872
Reading from 10814: heap size 886 MB, throughput 0.821132
Reading from 10814: heap size 885 MB, throughput 0.825069
Reading from 10814: heap size 888 MB, throughput 0.752882
Reading from 10814: heap size 889 MB, throughput 0.835517
Reading from 10815: heap size 135 MB, throughput 0.996891
Reading from 10814: heap size 892 MB, throughput 0.833022
Equal recommendation: 1235 MB each
Reading from 10814: heap size 894 MB, throughput 0.839832
Reading from 10814: heap size 896 MB, throughput 0.848983
Reading from 10815: heap size 138 MB, throughput 0.997256
Reading from 10814: heap size 898 MB, throughput 0.890314
Reading from 10814: heap size 899 MB, throughput 0.885227
Reading from 10814: heap size 901 MB, throughput 0.846833
Reading from 10815: heap size 138 MB, throughput 0.996847
Reading from 10815: heap size 138 MB, throughput 0.99493
Reading from 10815: heap size 139 MB, throughput 0.997333
Reading from 10814: heap size 904 MB, throughput 0.100463
Reading from 10814: heap size 998 MB, throughput 0.522037
Reading from 10814: heap size 1022 MB, throughput 0.634555
Reading from 10814: heap size 1024 MB, throughput 0.722421
Reading from 10814: heap size 1027 MB, throughput 0.733901
Reading from 10815: heap size 141 MB, throughput 0.997055
Reading from 10814: heap size 1030 MB, throughput 0.716297
Reading from 10814: heap size 1033 MB, throughput 0.730592
Reading from 10814: heap size 1036 MB, throughput 0.696691
Reading from 10814: heap size 1043 MB, throughput 0.740962
Reading from 10815: heap size 141 MB, throughput 0.997484
Reading from 10814: heap size 1045 MB, throughput 0.712529
Reading from 10814: heap size 1060 MB, throughput 0.723597
Reading from 10815: heap size 143 MB, throughput 0.996442
Reading from 10815: heap size 143 MB, throughput 0.997375
Reading from 10815: heap size 145 MB, throughput 0.998731
Reading from 10814: heap size 1060 MB, throughput 0.973003
Reading from 10815: heap size 145 MB, throughput 0.998445
Reading from 10815: heap size 147 MB, throughput 0.998171
Equal recommendation: 1235 MB each
Reading from 10815: heap size 147 MB, throughput 0.998717
Reading from 10815: heap size 148 MB, throughput 0.998378
Reading from 10814: heap size 1079 MB, throughput 0.974617
Reading from 10815: heap size 148 MB, throughput 0.992603
Reading from 10815: heap size 149 MB, throughput 0.993889
Reading from 10815: heap size 149 MB, throughput 0.992113
Reading from 10815: heap size 152 MB, throughput 0.996994
Reading from 10815: heap size 152 MB, throughput 0.996063
Reading from 10814: heap size 1080 MB, throughput 0.973937
Reading from 10815: heap size 153 MB, throughput 0.996844
Reading from 10815: heap size 154 MB, throughput 0.99754
Reading from 10815: heap size 157 MB, throughput 0.997035
Reading from 10814: heap size 1093 MB, throughput 0.973098
Reading from 10815: heap size 157 MB, throughput 0.997596
Equal recommendation: 1235 MB each
Reading from 10815: heap size 157 MB, throughput 0.997092
Reading from 10815: heap size 158 MB, throughput 0.997952
Reading from 10815: heap size 160 MB, throughput 0.99749
Reading from 10814: heap size 1099 MB, throughput 0.788542
Reading from 10815: heap size 160 MB, throughput 0.996953
Reading from 10815: heap size 162 MB, throughput 0.995966
Reading from 10815: heap size 162 MB, throughput 0.996974
Reading from 10815: heap size 164 MB, throughput 0.99701
Reading from 10814: heap size 1200 MB, throughput 0.995834
Reading from 10815: heap size 164 MB, throughput 0.997929
Reading from 10815: heap size 166 MB, throughput 0.998476
Reading from 10815: heap size 166 MB, throughput 0.998423
Reading from 10814: heap size 1203 MB, throughput 0.99465
Reading from 10815: heap size 167 MB, throughput 0.996649
Equal recommendation: 1235 MB each
Reading from 10815: heap size 168 MB, throughput 0.990281
Reading from 10815: heap size 170 MB, throughput 0.993801
Reading from 10815: heap size 170 MB, throughput 0.997704
Reading from 10815: heap size 173 MB, throughput 0.99704
Reading from 10814: heap size 1217 MB, throughput 0.992613
Reading from 10815: heap size 173 MB, throughput 0.997545
Reading from 10815: heap size 174 MB, throughput 0.997903
Reading from 10815: heap size 175 MB, throughput 0.997368
Reading from 10814: heap size 1221 MB, throughput 0.990037
Reading from 10815: heap size 178 MB, throughput 0.997956
Reading from 10815: heap size 178 MB, throughput 0.997662
Reading from 10815: heap size 179 MB, throughput 0.996713
Equal recommendation: 1235 MB each
Reading from 10814: heap size 1223 MB, throughput 0.989531
Reading from 10815: heap size 179 MB, throughput 0.997088
Reading from 10815: heap size 181 MB, throughput 0.996979
Reading from 10815: heap size 181 MB, throughput 0.997211
Reading from 10814: heap size 1225 MB, throughput 0.989952
Reading from 10815: heap size 184 MB, throughput 0.997647
Reading from 10815: heap size 184 MB, throughput 0.91908
Reading from 10815: heap size 189 MB, throughput 0.998613
Reading from 10815: heap size 189 MB, throughput 0.996301
Reading from 10814: heap size 1215 MB, throughput 0.979413
Reading from 10815: heap size 192 MB, throughput 0.993331
Reading from 10815: heap size 192 MB, throughput 0.995546
Equal recommendation: 1235 MB each
Reading from 10815: heap size 197 MB, throughput 0.998052
Reading from 10815: heap size 197 MB, throughput 0.99759
Reading from 10814: heap size 1146 MB, throughput 0.986228
Reading from 10815: heap size 199 MB, throughput 0.998148
Reading from 10815: heap size 200 MB, throughput 0.997834
Reading from 10815: heap size 202 MB, throughput 0.998022
Reading from 10814: heap size 1206 MB, throughput 0.986933
Reading from 10815: heap size 203 MB, throughput 0.997324
Reading from 10815: heap size 206 MB, throughput 0.997977
Reading from 10815: heap size 206 MB, throughput 0.99424
Reading from 10814: heap size 1212 MB, throughput 0.984868
Equal recommendation: 1235 MB each
Reading from 10815: heap size 208 MB, throughput 0.995956
Reading from 10815: heap size 208 MB, throughput 0.997693
Reading from 10815: heap size 212 MB, throughput 0.997766
Reading from 10814: heap size 1213 MB, throughput 0.985553
Reading from 10815: heap size 212 MB, throughput 0.996549
Reading from 10815: heap size 216 MB, throughput 0.996826
Reading from 10815: heap size 216 MB, throughput 0.99096
Reading from 10815: heap size 219 MB, throughput 0.997487
Reading from 10814: heap size 1214 MB, throughput 0.979896
Reading from 10815: heap size 220 MB, throughput 0.997961
Reading from 10815: heap size 224 MB, throughput 0.997949
Equal recommendation: 1235 MB each
Reading from 10815: heap size 224 MB, throughput 0.997656
Reading from 10814: heap size 1215 MB, throughput 0.98242
Reading from 10815: heap size 228 MB, throughput 0.998672
Reading from 10815: heap size 228 MB, throughput 0.997583
Reading from 10814: heap size 1219 MB, throughput 0.980323
Reading from 10815: heap size 231 MB, throughput 0.998282
Reading from 10815: heap size 231 MB, throughput 0.997185
Reading from 10815: heap size 235 MB, throughput 0.99808
Reading from 10814: heap size 1221 MB, throughput 0.978037
Reading from 10815: heap size 235 MB, throughput 0.998077
Equal recommendation: 1235 MB each
Reading from 10815: heap size 237 MB, throughput 0.997904
Reading from 10815: heap size 238 MB, throughput 0.997454
Reading from 10814: heap size 1229 MB, throughput 0.975633
Reading from 10815: heap size 241 MB, throughput 0.992497
Reading from 10815: heap size 241 MB, throughput 0.995427
Reading from 10815: heap size 246 MB, throughput 0.998073
Reading from 10814: heap size 1234 MB, throughput 0.977494
Reading from 10815: heap size 246 MB, throughput 0.996805
Reading from 10815: heap size 251 MB, throughput 0.998335
Equal recommendation: 1235 MB each
Reading from 10814: heap size 1240 MB, throughput 0.977607
Reading from 10815: heap size 251 MB, throughput 0.99751
Reading from 10815: heap size 254 MB, throughput 0.997788
Reading from 10815: heap size 255 MB, throughput 0.997863
Reading from 10814: heap size 1242 MB, throughput 0.976698
Reading from 10815: heap size 259 MB, throughput 0.998257
Reading from 10815: heap size 259 MB, throughput 0.998167
Reading from 10814: heap size 1244 MB, throughput 0.976969
Reading from 10815: heap size 262 MB, throughput 0.99801
Equal recommendation: 1235 MB each
Reading from 10815: heap size 262 MB, throughput 0.996273
Reading from 10815: heap size 265 MB, throughput 0.988581
Reading from 10814: heap size 1244 MB, throughput 0.976349
Reading from 10815: heap size 265 MB, throughput 0.99764
Reading from 10815: heap size 271 MB, throughput 0.997801
Reading from 10814: heap size 1193 MB, throughput 0.977248
Reading from 10815: heap size 272 MB, throughput 0.997564
Reading from 10815: heap size 276 MB, throughput 0.998006
Reading from 10814: heap size 1243 MB, throughput 0.977268
Reading from 10815: heap size 277 MB, throughput 0.998157
Equal recommendation: 1235 MB each
Reading from 10815: heap size 281 MB, throughput 0.997744
Reading from 10815: heap size 281 MB, throughput 0.996776
Reading from 10814: heap size 1190 MB, throughput 0.977181
Reading from 10815: heap size 286 MB, throughput 0.99772
Reading from 10815: heap size 286 MB, throughput 0.997592
Reading from 10814: heap size 1241 MB, throughput 0.977227
Reading from 10815: heap size 290 MB, throughput 0.995542
Reading from 10815: heap size 290 MB, throughput 0.997072
Equal recommendation: 1235 MB each
Reading from 10814: heap size 1188 MB, throughput 0.974727
Reading from 10815: heap size 295 MB, throughput 0.998502
Reading from 10815: heap size 296 MB, throughput 0.997848
Reading from 10814: heap size 1238 MB, throughput 0.977
Reading from 10815: heap size 300 MB, throughput 0.998178
Reading from 10815: heap size 300 MB, throughput 0.99784
Reading from 10814: heap size 1186 MB, throughput 0.977357
Reading from 10815: heap size 304 MB, throughput 0.998493
Equal recommendation: 1235 MB each
Reading from 10815: heap size 304 MB, throughput 0.998168
Reading from 10814: heap size 1233 MB, throughput 0.977765
Reading from 10815: heap size 308 MB, throughput 0.998511
Reading from 10815: heap size 308 MB, throughput 0.994724
Reading from 10815: heap size 311 MB, throughput 0.992419
Reading from 10814: heap size 1232 MB, throughput 0.975006
Reading from 10815: heap size 311 MB, throughput 0.997675
Reading from 10815: heap size 317 MB, throughput 0.998374
Equal recommendation: 1235 MB each
Reading from 10815: heap size 318 MB, throughput 0.997979
Reading from 10814: heap size 1227 MB, throughput 0.666507
Reading from 10815: heap size 324 MB, throughput 0.998514
Reading from 10815: heap size 324 MB, throughput 0.997932
Reading from 10814: heap size 1348 MB, throughput 0.97268
Reading from 10815: heap size 327 MB, throughput 0.998493
Reading from 10814: heap size 1339 MB, throughput 0.992699
Reading from 10815: heap size 328 MB, throughput 0.998443
Equal recommendation: 1235 MB each
Reading from 10815: heap size 331 MB, throughput 0.997924
Reading from 10815: heap size 332 MB, throughput 0.992208
Reading from 10814: heap size 1348 MB, throughput 0.990268
Reading from 10815: heap size 335 MB, throughput 0.998294
Reading from 10814: heap size 1351 MB, throughput 0.988361
Reading from 10815: heap size 336 MB, throughput 0.998207
Reading from 10815: heap size 340 MB, throughput 0.998113
Equal recommendation: 1235 MB each
Reading from 10814: heap size 1223 MB, throughput 0.987306
Reading from 10815: heap size 341 MB, throughput 0.997882
Reading from 10814: heap size 1351 MB, throughput 0.985796
Reading from 10815: heap size 346 MB, throughput 0.997889
Reading from 10815: heap size 346 MB, throughput 0.997074
Reading from 10814: heap size 1237 MB, throughput 0.987228
Reading from 10815: heap size 350 MB, throughput 0.998532
Reading from 10815: heap size 350 MB, throughput 0.928075
Equal recommendation: 1235 MB each
Reading from 10814: heap size 1330 MB, throughput 0.984137
Reading from 10815: heap size 362 MB, throughput 0.998508
Reading from 10815: heap size 362 MB, throughput 0.998963
Reading from 10814: heap size 1245 MB, throughput 0.984751
Reading from 10815: heap size 368 MB, throughput 0.99915
Reading from 10815: heap size 368 MB, throughput 0.998813
Reading from 10814: heap size 1309 MB, throughput 0.98275
Equal recommendation: 1235 MB each
Reading from 10815: heap size 373 MB, throughput 0.999259
Reading from 10814: heap size 1254 MB, throughput 0.978045
Reading from 10815: heap size 373 MB, throughput 0.998978
Reading from 10815: heap size 377 MB, throughput 0.999059
Reading from 10815: heap size 377 MB, throughput 0.996939
Reading from 10815: heap size 380 MB, throughput 0.997931
Equal recommendation: 1235 MB each
Reading from 10815: heap size 381 MB, throughput 0.998384
Reading from 10815: heap size 387 MB, throughput 0.998563
Reading from 10815: heap size 387 MB, throughput 0.998019
Reading from 10815: heap size 392 MB, throughput 0.997824
Reading from 10814: heap size 1304 MB, throughput 0.992296
Equal recommendation: 1235 MB each
Reading from 10815: heap size 392 MB, throughput 0.998219
Reading from 10815: heap size 396 MB, throughput 0.998591
Reading from 10815: heap size 397 MB, throughput 0.993406
Reading from 10815: heap size 401 MB, throughput 0.998463
Reading from 10815: heap size 402 MB, throughput 0.998378
Equal recommendation: 1235 MB each
Reading from 10815: heap size 407 MB, throughput 0.998577
Reading from 10814: heap size 1306 MB, throughput 0.993392
Reading from 10815: heap size 408 MB, throughput 0.998088
Reading from 10815: heap size 415 MB, throughput 0.998499
Reading from 10814: heap size 1314 MB, throughput 0.775552
Reading from 10815: heap size 415 MB, throughput 0.99826
Reading from 10815: heap size 420 MB, throughput 0.994578
Equal recommendation: 1235 MB each
Reading from 10814: heap size 1364 MB, throughput 0.995427
Reading from 10814: heap size 1412 MB, throughput 0.98067
Reading from 10814: heap size 1208 MB, throughput 0.905518
Reading from 10814: heap size 1409 MB, throughput 0.844015
Reading from 10815: heap size 420 MB, throughput 0.998361
Reading from 10814: heap size 1293 MB, throughput 0.849925
Reading from 10814: heap size 1381 MB, throughput 0.844048
Reading from 10814: heap size 1290 MB, throughput 0.846881
Reading from 10814: heap size 1365 MB, throughput 0.849044
Reading from 10814: heap size 1287 MB, throughput 0.848336
Reading from 10814: heap size 1350 MB, throughput 0.852354
Reading from 10814: heap size 1284 MB, throughput 0.949656
Reading from 10815: heap size 428 MB, throughput 0.998651
Reading from 10814: heap size 1346 MB, throughput 0.993649
Reading from 10815: heap size 429 MB, throughput 0.997926
Reading from 10814: heap size 1349 MB, throughput 0.992875
Equal recommendation: 1235 MB each
Reading from 10815: heap size 435 MB, throughput 0.998271
Reading from 10814: heap size 1350 MB, throughput 0.992396
Reading from 10815: heap size 435 MB, throughput 0.998409
Reading from 10814: heap size 1204 MB, throughput 0.989788
Reading from 10814: heap size 1351 MB, throughput 0.989703
Client 10815 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1209 MB, throughput 0.989178
Reading from 10814: heap size 1341 MB, throughput 0.988294
Reading from 10814: heap size 1227 MB, throughput 0.986097
Reading from 10814: heap size 1324 MB, throughput 0.986416
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1243 MB, throughput 0.985414
Reading from 10814: heap size 1315 MB, throughput 0.983379
Reading from 10814: heap size 1322 MB, throughput 0.983279
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1317 MB, throughput 0.981858
Reading from 10814: heap size 1320 MB, throughput 0.981624
Reading from 10814: heap size 1323 MB, throughput 0.979787
Reading from 10814: heap size 1324 MB, throughput 0.979321
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1329 MB, throughput 0.981549
Reading from 10814: heap size 1332 MB, throughput 0.978753
Reading from 10814: heap size 1337 MB, throughput 0.980802
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1338 MB, throughput 0.979788
Reading from 10814: heap size 1344 MB, throughput 0.979097
Reading from 10814: heap size 1344 MB, throughput 0.980687
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1349 MB, throughput 0.980219
Reading from 10814: heap size 1349 MB, throughput 0.980577
Reading from 10814: heap size 1354 MB, throughput 0.98009
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1354 MB, throughput 0.980027
Reading from 10814: heap size 1359 MB, throughput 0.980236
Reading from 10814: heap size 1359 MB, throughput 0.979367
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1363 MB, throughput 0.980049
Reading from 10814: heap size 1364 MB, throughput 0.979247
Reading from 10814: heap size 1369 MB, throughput 0.980668
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1369 MB, throughput 0.978951
Reading from 10814: heap size 1375 MB, throughput 0.980137
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1375 MB, throughput 0.981429
Reading from 10814: heap size 1380 MB, throughput 0.978451
Reading from 10814: heap size 1380 MB, throughput 0.979646
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1386 MB, throughput 0.979935
Reading from 10814: heap size 1386 MB, throughput 0.791536
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1401 MB, throughput 0.995794
Reading from 10814: heap size 1402 MB, throughput 0.994711
Reading from 10814: heap size 1413 MB, throughput 0.992949
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1419 MB, throughput 0.992271
Reading from 10814: heap size 1425 MB, throughput 0.99068
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1426 MB, throughput 0.990025
Reading from 10814: heap size 1417 MB, throughput 0.988993
Reading from 10814: heap size 1336 MB, throughput 0.98889
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1406 MB, throughput 0.98763
Reading from 10814: heap size 1413 MB, throughput 0.985807
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1414 MB, throughput 0.984913
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1414 MB, throughput 0.988656
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1377 MB, throughput 0.991996
Recommendation: one client; give it all the memory
Reading from 10814: heap size 1422 MB, throughput 0.983225
Reading from 10814: heap size 1426 MB, throughput 0.845119
Reading from 10814: heap size 1448 MB, throughput 0.268436
Reading from 10814: heap size 1444 MB, throughput 0.992129
Reading from 10814: heap size 1485 MB, throughput 0.990122
Reading from 10814: heap size 1499 MB, throughput 0.990802
Reading from 10814: heap size 1502 MB, throughput 0.990252
Reading from 10814: heap size 1512 MB, throughput 0.991295
Reading from 10814: heap size 1515 MB, throughput 0.99296
Reading from 10814: heap size 1504 MB, throughput 0.984954
Client 10814 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
