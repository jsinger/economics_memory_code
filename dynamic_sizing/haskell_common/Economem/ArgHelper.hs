module Economem.ArgHelper (absolutePath,
                           existingFile,
                           existingAbsoluteFile,
                           existingDir,
                           existingAbsoluteDir,
                           gt,
                           lt,
                           geq,
                           leq,
                           positive) where

import Economem.Files
import System.Directory
import Text.Printf

cmp :: Show a => (a -> a -> Bool) -> String -> a -> a -> IO a
cmp fn msg i j = if j `fn` i then return j
                 else fail (printf "'%s' is not %s %s" (show j) msg (show i))

gt :: (Show a, Ord a) => a -> a -> IO a
gt = cmp (>) "greater than"

lt :: (Show a, Ord a) => a -> a -> IO a
lt = cmp (<) "less than"

geq :: (Show a, Ord a) => a -> a -> IO a
geq = cmp (>=) "greater than or equal to"

leq :: (Show a, Ord a) => a -> a -> IO a
leq = cmp (<=) "less than or equal to"

positive :: (Show a, Ord a, Num a) => a -> IO a
positive = gt 0

absolutePath :: FilePath -> IO FilePath
absolutePath = absPath

checkExists :: (FilePath -> IO Bool) -> String -> FilePath -> IO FilePath
checkExists existenceCheck name p =
  do result <- existenceCheck p
     if result then return p
       else fail (name ++ " '" ++ p ++ "' does not exist")

existingFile :: FilePath -> IO FilePath
existingFile = checkExists doesFileExist "File"

existingAbsoluteFile :: FilePath -> IO FilePath
existingAbsoluteFile p = absolutePath p >>= existingFile

existingDir :: FilePath -> IO FilePath
existingDir = checkExists doesDirectoryExist "Directory"

existingAbsoluteDir :: FilePath -> IO FilePath
existingAbsoluteDir p = absolutePath p >>= existingDir
