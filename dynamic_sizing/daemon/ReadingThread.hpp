#ifndef READINGTHREAD_HPP
#define READINGTHREAD_HPP

#include <ReadingReceiver.hpp>
#include <ReceiverThread.hpp>
#include <Messages.hpp>

class ClientList;

class ReadingThread : public economem::ReceiverThread<economem::Reading>
{
public:
    ReadingThread(economem::ReadingReceiver& receiver, ClientList& clients);

private:
    virtual void received(const economem::Reading& reading);

    ClientList& m_Clients;
};

#endif
