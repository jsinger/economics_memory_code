#ifndef READINGITERATOR_HPP
#define READINGITERATOR_HPP

#include <sys/types.h>
#include <utility>

class ReadingIterator
{
public:
    ReadingIterator() {}
    virtual ~ReadingIterator() {}

    virtual bool hasNext() const = 0;
    virtual std::pair<unsigned long long, double> getNext() = 0;
};

#endif
