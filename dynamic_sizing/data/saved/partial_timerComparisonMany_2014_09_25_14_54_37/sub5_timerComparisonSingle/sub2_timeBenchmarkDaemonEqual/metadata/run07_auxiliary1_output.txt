economemd
    total memory: 7435 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub5_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 9633: heap size 9 MB, throughput 0.991096
Clients: 1
Client 9633 has a minimum heap size of 276 MB
Reading from 9632: heap size 9 MB, throughput 0.990705
Clients: 2
Client 9632 has a minimum heap size of 1211 MB
Reading from 9633: heap size 9 MB, throughput 0.952761
Reading from 9632: heap size 9 MB, throughput 0.961356
Reading from 9633: heap size 9 MB, throughput 0.966841
Reading from 9632: heap size 9 MB, throughput 0.963894
Reading from 9633: heap size 9 MB, throughput 0.951903
Reading from 9632: heap size 9 MB, throughput 0.934321
Reading from 9633: heap size 11 MB, throughput 0.986517
Reading from 9632: heap size 11 MB, throughput 0.982449
Reading from 9632: heap size 11 MB, throughput 0.9686
Reading from 9633: heap size 11 MB, throughput 0.978812
Reading from 9632: heap size 17 MB, throughput 0.799594
Reading from 9633: heap size 17 MB, throughput 0.968684
Reading from 9632: heap size 17 MB, throughput 0.692399
Reading from 9633: heap size 17 MB, throughput 0.639368
Reading from 9632: heap size 30 MB, throughput 0.930545
Reading from 9633: heap size 30 MB, throughput 0.933772
Reading from 9632: heap size 31 MB, throughput 0.916633
Reading from 9633: heap size 31 MB, throughput 0.883266
Reading from 9632: heap size 35 MB, throughput 0.474984
Reading from 9632: heap size 48 MB, throughput 0.801493
Reading from 9633: heap size 34 MB, throughput 0.371959
Reading from 9633: heap size 47 MB, throughput 0.789754
Reading from 9632: heap size 51 MB, throughput 0.85148
Reading from 9633: heap size 49 MB, throughput 0.925596
Reading from 9632: heap size 53 MB, throughput 0.317501
Reading from 9632: heap size 80 MB, throughput 0.780004
Reading from 9633: heap size 51 MB, throughput 0.207154
Reading from 9633: heap size 77 MB, throughput 0.776119
Reading from 9632: heap size 80 MB, throughput 0.315101
Reading from 9633: heap size 77 MB, throughput 0.2869
Reading from 9632: heap size 108 MB, throughput 0.600755
Reading from 9633: heap size 103 MB, throughput 0.694294
Reading from 9633: heap size 104 MB, throughput 0.704705
Reading from 9632: heap size 109 MB, throughput 0.787445
Reading from 9633: heap size 105 MB, throughput 0.827932
Reading from 9632: heap size 111 MB, throughput 0.824581
Reading from 9633: heap size 108 MB, throughput 0.808006
Reading from 9632: heap size 115 MB, throughput 0.286063
Reading from 9633: heap size 111 MB, throughput 0.225786
Reading from 9632: heap size 143 MB, throughput 0.781975
Reading from 9633: heap size 145 MB, throughput 0.733788
Reading from 9632: heap size 148 MB, throughput 0.80331
Reading from 9633: heap size 149 MB, throughput 0.808634
Reading from 9632: heap size 151 MB, throughput 0.753358
Reading from 9633: heap size 152 MB, throughput 0.756132
Reading from 9633: heap size 160 MB, throughput 0.74343
Reading from 9632: heap size 154 MB, throughput 0.144713
Reading from 9633: heap size 162 MB, throughput 0.141901
Reading from 9632: heap size 195 MB, throughput 0.721783
Reading from 9633: heap size 207 MB, throughput 0.759396
Reading from 9632: heap size 198 MB, throughput 0.738362
Reading from 9633: heap size 209 MB, throughput 0.792914
Reading from 9632: heap size 200 MB, throughput 0.664344
Reading from 9632: heap size 207 MB, throughput 0.655963
Reading from 9632: heap size 210 MB, throughput 0.669142
Reading from 9632: heap size 217 MB, throughput 0.525425
Reading from 9633: heap size 212 MB, throughput 0.890591
Reading from 9632: heap size 221 MB, throughput 0.527899
Reading from 9632: heap size 231 MB, throughput 0.128317
Reading from 9633: heap size 218 MB, throughput 0.30314
Reading from 9632: heap size 295 MB, throughput 0.668682
Reading from 9633: heap size 272 MB, throughput 0.762205
Reading from 9633: heap size 275 MB, throughput 0.849821
Reading from 9633: heap size 270 MB, throughput 0.747918
Reading from 9632: heap size 297 MB, throughput 0.2779
Reading from 9633: heap size 273 MB, throughput 0.935073
Reading from 9632: heap size 342 MB, throughput 0.71459
Reading from 9633: heap size 270 MB, throughput 0.831583
Reading from 9632: heap size 345 MB, throughput 0.725082
Reading from 9632: heap size 345 MB, throughput 0.726443
Reading from 9633: heap size 272 MB, throughput 0.883418
Reading from 9633: heap size 272 MB, throughput 0.604755
Reading from 9632: heap size 346 MB, throughput 0.645655
Reading from 9633: heap size 275 MB, throughput 0.78048
Reading from 9632: heap size 349 MB, throughput 0.689187
Reading from 9633: heap size 281 MB, throughput 0.834471
Reading from 9633: heap size 282 MB, throughput 0.630418
Reading from 9632: heap size 356 MB, throughput 0.626428
Reading from 9632: heap size 360 MB, throughput 0.625937
Reading from 9632: heap size 372 MB, throughput 0.550274
Reading from 9632: heap size 376 MB, throughput 0.548335
Reading from 9632: heap size 389 MB, throughput 0.498448
Reading from 9633: heap size 289 MB, throughput 0.952253
Reading from 9632: heap size 395 MB, throughput 0.475599
Reading from 9633: heap size 289 MB, throughput 0.751288
Reading from 9633: heap size 293 MB, throughput 0.773972
Reading from 9633: heap size 294 MB, throughput 0.864123
Reading from 9633: heap size 295 MB, throughput 0.772233
Reading from 9632: heap size 404 MB, throughput 0.0672554
Reading from 9633: heap size 297 MB, throughput 0.933657
Reading from 9632: heap size 464 MB, throughput 0.325016
Reading from 9633: heap size 298 MB, throughput 0.88879
Reading from 9633: heap size 300 MB, throughput 0.591641
Reading from 9633: heap size 305 MB, throughput 0.663409
Reading from 9633: heap size 306 MB, throughput 0.666631
Reading from 9633: heap size 314 MB, throughput 0.649088
Reading from 9633: heap size 314 MB, throughput 0.692714
Reading from 9632: heap size 374 MB, throughput 0.0904291
Reading from 9632: heap size 517 MB, throughput 0.404812
Reading from 9632: heap size 522 MB, throughput 0.571719
Reading from 9632: heap size 516 MB, throughput 0.549872
Reading from 9632: heap size 429 MB, throughput 0.611763
Reading from 9632: heap size 506 MB, throughput 0.516443
Equal recommendation: 3717 MB each
Reading from 9632: heap size 512 MB, throughput 0.442595
Reading from 9633: heap size 323 MB, throughput 0.978034
Reading from 9632: heap size 506 MB, throughput 0.582535
Reading from 9632: heap size 509 MB, throughput 0.511981
Reading from 9632: heap size 512 MB, throughput 0.60264
Reading from 9632: heap size 512 MB, throughput 0.670382
Reading from 9632: heap size 514 MB, throughput 0.535166
Reading from 9632: heap size 519 MB, throughput 0.58948
Reading from 9633: heap size 324 MB, throughput 0.977417
Reading from 9632: heap size 520 MB, throughput 0.0872333
Reading from 9632: heap size 587 MB, throughput 0.396044
Reading from 9632: heap size 599 MB, throughput 0.446819
Reading from 9632: heap size 603 MB, throughput 0.468868
Reading from 9632: heap size 608 MB, throughput 0.559426
Reading from 9633: heap size 329 MB, throughput 0.959105
Reading from 9632: heap size 615 MB, throughput 0.0719562
Reading from 9632: heap size 689 MB, throughput 0.448087
Reading from 9632: heap size 619 MB, throughput 0.838756
Reading from 9632: heap size 687 MB, throughput 0.820484
Reading from 9633: heap size 331 MB, throughput 0.638112
Reading from 9632: heap size 688 MB, throughput 0.854014
Reading from 9632: heap size 686 MB, throughput 0.748337
Reading from 9632: heap size 702 MB, throughput 0.388061
Reading from 9632: heap size 709 MB, throughput 0.640733
Reading from 9632: heap size 720 MB, throughput 0.343321
Reading from 9633: heap size 375 MB, throughput 0.956707
Reading from 9632: heap size 730 MB, throughput 0.022953
Reading from 9632: heap size 806 MB, throughput 0.198551
Reading from 9633: heap size 377 MB, throughput 0.978746
Reading from 9632: heap size 796 MB, throughput 0.400339
Reading from 9632: heap size 802 MB, throughput 0.42789
Equal recommendation: 3717 MB each
Reading from 9633: heap size 380 MB, throughput 0.972728
Reading from 9632: heap size 803 MB, throughput 0.2233
Reading from 9632: heap size 890 MB, throughput 0.435431
Reading from 9632: heap size 892 MB, throughput 0.6837
Reading from 9632: heap size 894 MB, throughput 0.653816
Reading from 9632: heap size 895 MB, throughput 0.899806
Reading from 9632: heap size 897 MB, throughput 0.768965
Reading from 9632: heap size 893 MB, throughput 0.84898
Reading from 9633: heap size 382 MB, throughput 0.980684
Reading from 9632: heap size 896 MB, throughput 0.847219
Reading from 9632: heap size 899 MB, throughput 0.916534
Reading from 9632: heap size 900 MB, throughput 0.860952
Reading from 9632: heap size 892 MB, throughput 0.915373
Reading from 9632: heap size 768 MB, throughput 0.810832
Reading from 9632: heap size 872 MB, throughput 0.760815
Reading from 9632: heap size 785 MB, throughput 0.730607
Reading from 9632: heap size 862 MB, throughput 0.662748
Reading from 9633: heap size 379 MB, throughput 0.980957
Reading from 9632: heap size 790 MB, throughput 0.110105
Reading from 9632: heap size 953 MB, throughput 0.52733
Reading from 9633: heap size 382 MB, throughput 0.961986
Reading from 9632: heap size 958 MB, throughput 0.796622
Reading from 9632: heap size 964 MB, throughput 0.767447
Reading from 9632: heap size 965 MB, throughput 0.826055
Reading from 9632: heap size 965 MB, throughput 0.829049
Reading from 9632: heap size 968 MB, throughput 0.807645
Reading from 9633: heap size 384 MB, throughput 0.979725
Reading from 9632: heap size 965 MB, throughput 0.976886
Reading from 9633: heap size 384 MB, throughput 0.960954
Reading from 9632: heap size 968 MB, throughput 0.855865
Reading from 9632: heap size 968 MB, throughput 0.77552
Reading from 9632: heap size 978 MB, throughput 0.746525
Reading from 9632: heap size 986 MB, throughput 0.789191
Reading from 9632: heap size 990 MB, throughput 0.800585
Reading from 9632: heap size 986 MB, throughput 0.773319
Reading from 9632: heap size 990 MB, throughput 0.791711
Reading from 9633: heap size 386 MB, throughput 0.975152
Reading from 9632: heap size 988 MB, throughput 0.823556
Reading from 9632: heap size 992 MB, throughput 0.813302
Equal recommendation: 3717 MB each
Reading from 9632: heap size 990 MB, throughput 0.835681
Reading from 9632: heap size 994 MB, throughput 0.882292
Reading from 9632: heap size 994 MB, throughput 0.766541
Reading from 9632: heap size 997 MB, throughput 0.62722
Reading from 9633: heap size 388 MB, throughput 0.972788
Reading from 9633: heap size 390 MB, throughput 0.933444
Reading from 9633: heap size 393 MB, throughput 0.680259
Reading from 9633: heap size 395 MB, throughput 0.760792
Reading from 9633: heap size 400 MB, throughput 0.771349
Reading from 9632: heap size 985 MB, throughput 0.0679542
Reading from 9632: heap size 1115 MB, throughput 0.512532
Reading from 9633: heap size 410 MB, throughput 0.944538
Reading from 9632: heap size 1122 MB, throughput 0.781148
Reading from 9632: heap size 1125 MB, throughput 0.764793
Reading from 9632: heap size 1136 MB, throughput 0.677589
Reading from 9632: heap size 1137 MB, throughput 0.779822
Reading from 9632: heap size 1143 MB, throughput 0.713334
Reading from 9632: heap size 1146 MB, throughput 0.665496
Reading from 9632: heap size 1150 MB, throughput 0.743802
Reading from 9633: heap size 412 MB, throughput 0.981909
Reading from 9632: heap size 1154 MB, throughput 0.896326
Reading from 9633: heap size 414 MB, throughput 0.986373
Reading from 9632: heap size 1154 MB, throughput 0.953048
Reading from 9633: heap size 417 MB, throughput 0.986077
Reading from 9632: heap size 1161 MB, throughput 0.944376
Equal recommendation: 3717 MB each
Reading from 9633: heap size 417 MB, throughput 0.986015
Reading from 9632: heap size 1164 MB, throughput 0.951291
Reading from 9633: heap size 420 MB, throughput 0.975135
Reading from 9632: heap size 1168 MB, throughput 0.943891
Reading from 9633: heap size 418 MB, throughput 0.984091
Reading from 9632: heap size 1176 MB, throughput 0.950696
Reading from 9633: heap size 420 MB, throughput 0.984663
Reading from 9632: heap size 1179 MB, throughput 0.958654
Reading from 9633: heap size 418 MB, throughput 0.983745
Reading from 9632: heap size 1181 MB, throughput 0.947222
Equal recommendation: 3717 MB each
Reading from 9633: heap size 420 MB, throughput 0.984218
Reading from 9632: heap size 1184 MB, throughput 0.952008
Reading from 9633: heap size 421 MB, throughput 0.98374
Reading from 9632: heap size 1192 MB, throughput 0.957996
Reading from 9633: heap size 422 MB, throughput 0.979899
Reading from 9633: heap size 426 MB, throughput 0.87061
Reading from 9633: heap size 427 MB, throughput 0.836155
Reading from 9633: heap size 432 MB, throughput 0.903356
Reading from 9632: heap size 1194 MB, throughput 0.963012
Reading from 9633: heap size 433 MB, throughput 0.990547
Reading from 9632: heap size 1202 MB, throughput 0.951443
Reading from 9633: heap size 438 MB, throughput 0.991444
Reading from 9632: heap size 1205 MB, throughput 0.961907
Equal recommendation: 3717 MB each
Reading from 9633: heap size 440 MB, throughput 0.988514
Reading from 9632: heap size 1213 MB, throughput 0.948918
Reading from 9633: heap size 441 MB, throughput 0.986428
Reading from 9632: heap size 1217 MB, throughput 0.946121
Reading from 9632: heap size 1224 MB, throughput 0.960247
Reading from 9633: heap size 443 MB, throughput 0.987844
Reading from 9632: heap size 1230 MB, throughput 0.948447
Reading from 9633: heap size 440 MB, throughput 0.986258
Reading from 9632: heap size 1238 MB, throughput 0.961077
Reading from 9633: heap size 443 MB, throughput 0.986482
Equal recommendation: 3717 MB each
Reading from 9632: heap size 1243 MB, throughput 0.950968
Reading from 9633: heap size 444 MB, throughput 0.985838
Reading from 9632: heap size 1250 MB, throughput 0.951953
Reading from 9633: heap size 444 MB, throughput 0.990604
Reading from 9633: heap size 445 MB, throughput 0.935873
Reading from 9633: heap size 446 MB, throughput 0.870506
Reading from 9633: heap size 450 MB, throughput 0.860647
Reading from 9632: heap size 1253 MB, throughput 0.953844
Reading from 9633: heap size 452 MB, throughput 0.989084
Reading from 9632: heap size 1261 MB, throughput 0.958158
Reading from 9633: heap size 460 MB, throughput 0.992035
Equal recommendation: 3717 MB each
Reading from 9632: heap size 1262 MB, throughput 0.957376
Reading from 9633: heap size 461 MB, throughput 0.990804
Reading from 9633: heap size 462 MB, throughput 0.986806
Reading from 9632: heap size 1270 MB, throughput 0.55129
Reading from 9632: heap size 1328 MB, throughput 0.99329
Reading from 9633: heap size 464 MB, throughput 0.988374
Reading from 9632: heap size 1327 MB, throughput 0.986435
Equal recommendation: 3717 MB each
Reading from 9633: heap size 462 MB, throughput 0.987869
Reading from 9632: heap size 1336 MB, throughput 0.98251
Reading from 9633: heap size 465 MB, throughput 0.98808
Reading from 9632: heap size 1346 MB, throughput 0.980436
Reading from 9633: heap size 467 MB, throughput 0.991721
Reading from 9633: heap size 468 MB, throughput 0.970475
Reading from 9633: heap size 468 MB, throughput 0.881806
Reading from 9632: heap size 1346 MB, throughput 0.976709
Reading from 9633: heap size 470 MB, throughput 0.919757
Reading from 9632: heap size 1340 MB, throughput 0.973934
Reading from 9633: heap size 478 MB, throughput 0.991422
Equal recommendation: 3717 MB each
Reading from 9632: heap size 1234 MB, throughput 0.981265
Reading from 9633: heap size 478 MB, throughput 0.991645
Reading from 9632: heap size 1328 MB, throughput 0.973563
Reading from 9633: heap size 481 MB, throughput 0.990484
Reading from 9632: heap size 1256 MB, throughput 0.970522
Reading from 9633: heap size 482 MB, throughput 0.988887
Reading from 9632: heap size 1326 MB, throughput 0.960042
Equal recommendation: 3717 MB each
Reading from 9633: heap size 480 MB, throughput 0.989342
Reading from 9632: heap size 1330 MB, throughput 0.961524
Reading from 9633: heap size 483 MB, throughput 0.989725
Reading from 9632: heap size 1334 MB, throughput 0.96407
Reading from 9633: heap size 485 MB, throughput 0.984087
Reading from 9632: heap size 1336 MB, throughput 0.960703
Reading from 9633: heap size 486 MB, throughput 0.975369
Reading from 9633: heap size 489 MB, throughput 0.874104
Reading from 9633: heap size 494 MB, throughput 0.916636
Equal recommendation: 3717 MB each
Reading from 9632: heap size 1341 MB, throughput 0.957106
Reading from 9633: heap size 505 MB, throughput 0.990352
Reading from 9632: heap size 1347 MB, throughput 0.953235
Reading from 9633: heap size 505 MB, throughput 0.990057
Reading from 9632: heap size 1354 MB, throughput 0.963516
Reading from 9633: heap size 503 MB, throughput 0.988186
Reading from 9632: heap size 1363 MB, throughput 0.960982
Equal recommendation: 3717 MB each
Reading from 9633: heap size 506 MB, throughput 0.988884
Reading from 9632: heap size 1372 MB, throughput 0.954631
Reading from 9633: heap size 510 MB, throughput 0.987446
Reading from 9633: heap size 510 MB, throughput 0.986733
Reading from 9633: heap size 514 MB, throughput 0.988903
Reading from 9633: heap size 516 MB, throughput 0.897113
Reading from 9633: heap size 516 MB, throughput 0.912384
Equal recommendation: 3717 MB each
Reading from 9633: heap size 518 MB, throughput 0.991354
Reading from 9632: heap size 1378 MB, throughput 0.991589
Reading from 9633: heap size 522 MB, throughput 0.919867
Reading from 9633: heap size 551 MB, throughput 0.995419
Equal recommendation: 3717 MB each
Reading from 9633: heap size 553 MB, throughput 0.993665
Reading from 9633: heap size 556 MB, throughput 0.994479
Reading from 9633: heap size 557 MB, throughput 0.990341
Equal recommendation: 3717 MB each
Reading from 9633: heap size 559 MB, throughput 0.971637
Reading from 9632: heap size 1380 MB, throughput 0.991212
Reading from 9633: heap size 556 MB, throughput 0.90682
Reading from 9633: heap size 560 MB, throughput 0.983811
Reading from 9633: heap size 565 MB, throughput 0.992497
Reading from 9632: heap size 1390 MB, throughput 0.977765
Reading from 9633: heap size 568 MB, throughput 0.984705
Reading from 9632: heap size 1385 MB, throughput 0.803714
Equal recommendation: 3717 MB each
Reading from 9632: heap size 1418 MB, throughput 0.23375
Reading from 9632: heap size 1485 MB, throughput 0.99351
Reading from 9632: heap size 1526 MB, throughput 0.993069
Reading from 9632: heap size 1547 MB, throughput 0.990829
Reading from 9632: heap size 1551 MB, throughput 0.99058
Reading from 9632: heap size 1554 MB, throughput 0.990562
Reading from 9632: heap size 1559 MB, throughput 0.978044
Reading from 9633: heap size 566 MB, throughput 0.990869
Reading from 9632: heap size 1542 MB, throughput 0.989533
Reading from 9633: heap size 569 MB, throughput 0.990282
Reading from 9632: heap size 1264 MB, throughput 0.990766
Reading from 9632: heap size 1522 MB, throughput 0.988978
Reading from 9633: heap size 572 MB, throughput 0.989499
Equal recommendation: 3717 MB each
Reading from 9632: heap size 1287 MB, throughput 0.980335
Reading from 9633: heap size 573 MB, throughput 0.991596
Reading from 9633: heap size 573 MB, throughput 0.904769
Reading from 9632: heap size 1497 MB, throughput 0.978043
Reading from 9633: heap size 576 MB, throughput 0.977895
Reading from 9632: heap size 1309 MB, throughput 0.977931
Reading from 9633: heap size 584 MB, throughput 0.99323
Reading from 9632: heap size 1471 MB, throughput 0.976275
Equal recommendation: 3717 MB each
Reading from 9632: heap size 1331 MB, throughput 0.974019
Reading from 9633: heap size 586 MB, throughput 0.991699
Reading from 9632: heap size 1457 MB, throughput 0.968729
Reading from 9633: heap size 585 MB, throughput 0.9918
Reading from 9632: heap size 1467 MB, throughput 0.970273
Reading from 9632: heap size 1462 MB, throughput 0.965472
Reading from 9633: heap size 588 MB, throughput 0.991401
Equal recommendation: 3717 MB each
Reading from 9632: heap size 1465 MB, throughput 0.963375
Reading from 9633: heap size 589 MB, throughput 0.987522
Reading from 9633: heap size 590 MB, throughput 0.9793
Reading from 9632: heap size 1470 MB, throughput 0.962279
Reading from 9633: heap size 596 MB, throughput 0.905634
Reading from 9633: heap size 597 MB, throughput 0.991168
Reading from 9632: heap size 1472 MB, throughput 0.96628
Equal recommendation: 3717 MB each
Reading from 9632: heap size 1480 MB, throughput 0.95588
Reading from 9633: heap size 605 MB, throughput 0.993781
Reading from 9632: heap size 1485 MB, throughput 0.951909
Reading from 9633: heap size 606 MB, throughput 0.992822
Reading from 9632: heap size 1497 MB, throughput 0.958746
Reading from 9632: heap size 1502 MB, throughput 0.960387
Reading from 9633: heap size 606 MB, throughput 0.991781
Equal recommendation: 3717 MB each
Reading from 9632: heap size 1515 MB, throughput 0.958628
Reading from 9633: heap size 609 MB, throughput 0.990335
Reading from 9632: heap size 1518 MB, throughput 0.952095
Reading from 9633: heap size 610 MB, throughput 0.989817
Reading from 9633: heap size 611 MB, throughput 0.911299
Reading from 9632: heap size 1530 MB, throughput 0.954842
Reading from 9633: heap size 617 MB, throughput 0.991469
Equal recommendation: 3717 MB each
Reading from 9632: heap size 1531 MB, throughput 0.957515
Reading from 9633: heap size 617 MB, throughput 0.993178
Reading from 9632: heap size 1544 MB, throughput 0.959931
Reading from 9633: heap size 621 MB, throughput 0.992964
Reading from 9632: heap size 1545 MB, throughput 0.956826
Equal recommendation: 3717 MB each
Reading from 9632: heap size 1558 MB, throughput 0.960107
Reading from 9633: heap size 623 MB, throughput 0.992617
Reading from 9632: heap size 1558 MB, throughput 0.956943
Reading from 9633: heap size 624 MB, throughput 0.995156
Reading from 9633: heap size 626 MB, throughput 0.97758
Reading from 9633: heap size 628 MB, throughput 0.96169
Reading from 9632: heap size 1571 MB, throughput 0.960376
Equal recommendation: 3717 MB each
Reading from 9633: heap size 630 MB, throughput 0.992572
Reading from 9632: heap size 1571 MB, throughput 0.713625
Reading from 9633: heap size 633 MB, throughput 0.993788
Reading from 9632: heap size 1631 MB, throughput 0.989147
Reading from 9632: heap size 1631 MB, throughput 0.985777
Reading from 9633: heap size 635 MB, throughput 0.992661
Equal recommendation: 3717 MB each
Reading from 9632: heap size 1652 MB, throughput 0.986454
Reading from 9633: heap size 635 MB, throughput 0.992287
Reading from 9632: heap size 1656 MB, throughput 0.982868
Reading from 9633: heap size 636 MB, throughput 0.99142
Reading from 9633: heap size 639 MB, throughput 0.915419
Equal recommendation: 3717 MB each
Reading from 9632: heap size 1655 MB, throughput 0.9783
Reading from 9633: heap size 641 MB, throughput 0.993497
Reading from 9632: heap size 1661 MB, throughput 0.9758
Reading from 9633: heap size 648 MB, throughput 0.994213
Reading from 9633: heap size 650 MB, throughput 0.992842
Equal recommendation: 3717 MB each
Reading from 9633: heap size 649 MB, throughput 0.992632
Reading from 9633: heap size 652 MB, throughput 0.99464
Reading from 9633: heap size 653 MB, throughput 0.943085
Client 9633 died
Clients: 1
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 9632: heap size 1645 MB, throughput 0.997248
Recommendation: one client; give it all the memory
Reading from 9632: heap size 1655 MB, throughput 0.987847
Reading from 9632: heap size 1640 MB, throughput 0.877689
Reading from 9632: heap size 1660 MB, throughput 0.767898
Reading from 9632: heap size 1690 MB, throughput 0.801594
Reading from 9632: heap size 1716 MB, throughput 0.781732
Reading from 9632: heap size 1757 MB, throughput 0.8537
Client 9632 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
TimerThread: finished
ReadingThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
