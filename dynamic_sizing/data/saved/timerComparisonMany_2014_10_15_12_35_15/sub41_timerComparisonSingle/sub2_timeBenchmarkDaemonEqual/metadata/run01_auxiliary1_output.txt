economemd
    total memory: 1104 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub41_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 10135: heap size 9 MB, throughput 0.991635
Clients: 1
Client 10135 has a minimum heap size of 276 MB
Reading from 10137: heap size 9 MB, throughput 0.975802
Clients: 2
Client 10137 has a minimum heap size of 276 MB
Reading from 10137: heap size 9 MB, throughput 0.977556
Reading from 10135: heap size 9 MB, throughput 0.957247
Reading from 10135: heap size 9 MB, throughput 0.967259
Reading from 10135: heap size 9 MB, throughput 0.957783
Reading from 10137: heap size 11 MB, throughput 0.961273
Reading from 10135: heap size 11 MB, throughput 0.986128
Reading from 10137: heap size 11 MB, throughput 0.827067
Reading from 10135: heap size 11 MB, throughput 0.969959
Reading from 10135: heap size 17 MB, throughput 0.963688
Reading from 10137: heap size 15 MB, throughput 0.88116
Reading from 10137: heap size 18 MB, throughput 0.913451
Reading from 10135: heap size 17 MB, throughput 0.469401
Reading from 10137: heap size 24 MB, throughput 0.53019
Reading from 10135: heap size 29 MB, throughput 0.954673
Reading from 10137: heap size 36 MB, throughput 0.977896
Reading from 10135: heap size 30 MB, throughput 0.885321
Reading from 10137: heap size 41 MB, throughput 0.870935
Reading from 10137: heap size 42 MB, throughput 0.784715
Reading from 10135: heap size 35 MB, throughput 0.345282
Reading from 10135: heap size 47 MB, throughput 0.773291
Reading from 10137: heap size 45 MB, throughput 0.298523
Reading from 10135: heap size 49 MB, throughput 0.244196
Reading from 10137: heap size 58 MB, throughput 0.698706
Reading from 10135: heap size 64 MB, throughput 0.698387
Reading from 10137: heap size 65 MB, throughput 0.853141
Reading from 10135: heap size 70 MB, throughput 0.302741
Reading from 10135: heap size 88 MB, throughput 0.758948
Reading from 10137: heap size 66 MB, throughput 0.191452
Reading from 10135: heap size 96 MB, throughput 0.778638
Reading from 10137: heap size 90 MB, throughput 0.788658
Reading from 10135: heap size 96 MB, throughput 0.207998
Reading from 10137: heap size 92 MB, throughput 0.180616
Reading from 10137: heap size 124 MB, throughput 0.644944
Reading from 10135: heap size 131 MB, throughput 0.808391
Reading from 10137: heap size 124 MB, throughput 0.788949
Reading from 10137: heap size 126 MB, throughput 0.734326
Reading from 10137: heap size 130 MB, throughput 0.644314
Reading from 10135: heap size 131 MB, throughput 0.252184
Reading from 10135: heap size 169 MB, throughput 0.841694
Reading from 10135: heap size 169 MB, throughput 0.8697
Reading from 10135: heap size 170 MB, throughput 0.678546
Reading from 10137: heap size 133 MB, throughput 0.169907
Reading from 10135: heap size 173 MB, throughput 0.803594
Reading from 10137: heap size 168 MB, throughput 0.690922
Reading from 10137: heap size 174 MB, throughput 0.651568
Reading from 10137: heap size 176 MB, throughput 0.54746
Reading from 10137: heap size 181 MB, throughput 0.606311
Reading from 10137: heap size 188 MB, throughput 0.490216
Reading from 10135: heap size 176 MB, throughput 0.145415
Reading from 10135: heap size 219 MB, throughput 0.629997
Reading from 10137: heap size 198 MB, throughput 0.245345
Reading from 10135: heap size 221 MB, throughput 0.942603
Reading from 10135: heap size 225 MB, throughput 0.848135
Reading from 10137: heap size 238 MB, throughput 0.876299
Reading from 10135: heap size 237 MB, throughput 0.906976
Reading from 10137: heap size 244 MB, throughput 0.712247
Reading from 10137: heap size 249 MB, throughput 0.582146
Reading from 10135: heap size 238 MB, throughput 0.850064
Reading from 10135: heap size 241 MB, throughput 0.781518
Reading from 10137: heap size 254 MB, throughput 0.800384
Reading from 10135: heap size 242 MB, throughput 0.807701
Reading from 10137: heap size 257 MB, throughput 0.799758
Reading from 10135: heap size 246 MB, throughput 0.780093
Reading from 10137: heap size 263 MB, throughput 0.747248
Reading from 10135: heap size 247 MB, throughput 0.681413
Reading from 10137: heap size 265 MB, throughput 0.793386
Reading from 10135: heap size 252 MB, throughput 0.786852
Reading from 10135: heap size 256 MB, throughput 0.726099
Reading from 10137: heap size 269 MB, throughput 0.819978
Reading from 10135: heap size 261 MB, throughput 0.635227
Reading from 10137: heap size 271 MB, throughput 0.562658
Reading from 10137: heap size 277 MB, throughput 0.64291
Reading from 10137: heap size 281 MB, throughput 0.654518
Reading from 10137: heap size 281 MB, throughput 0.711011
Reading from 10135: heap size 264 MB, throughput 0.938928
Reading from 10135: heap size 270 MB, throughput 0.822151
Reading from 10135: heap size 271 MB, throughput 0.626121
Reading from 10137: heap size 284 MB, throughput 0.0978295
Reading from 10135: heap size 277 MB, throughput 0.104434
Reading from 10137: heap size 321 MB, throughput 0.891207
Reading from 10137: heap size 324 MB, throughput 0.635468
Reading from 10135: heap size 320 MB, throughput 0.577869
Reading from 10137: heap size 321 MB, throughput 0.740334
Reading from 10135: heap size 323 MB, throughput 0.904518
Reading from 10137: heap size 323 MB, throughput 0.705112
Reading from 10135: heap size 324 MB, throughput 0.845623
Reading from 10137: heap size 326 MB, throughput 0.753101
Reading from 10135: heap size 330 MB, throughput 0.803642
Reading from 10137: heap size 328 MB, throughput 0.753597
Reading from 10135: heap size 330 MB, throughput 0.63611
Reading from 10135: heap size 334 MB, throughput 0.766177
Reading from 10137: heap size 334 MB, throughput 0.805345
Reading from 10135: heap size 334 MB, throughput 0.702691
Reading from 10137: heap size 334 MB, throughput 0.670568
Reading from 10135: heap size 341 MB, throughput 0.755223
Reading from 10137: heap size 337 MB, throughput 0.643211
Equal recommendation: 552 MB each
Reading from 10135: heap size 341 MB, throughput 0.951106
Reading from 10137: heap size 339 MB, throughput 0.0897506
Reading from 10137: heap size 385 MB, throughput 0.61062
Reading from 10137: heap size 387 MB, throughput 0.722888
Reading from 10135: heap size 346 MB, throughput 0.971224
Reading from 10137: heap size 398 MB, throughput 0.980908
Reading from 10135: heap size 348 MB, throughput 0.983256
Reading from 10137: heap size 398 MB, throughput 0.954299
Reading from 10135: heap size 347 MB, throughput 0.960751
Reading from 10137: heap size 400 MB, throughput 0.978196
Reading from 10135: heap size 350 MB, throughput 0.97404
Reading from 10137: heap size 403 MB, throughput 0.968773
Reading from 10135: heap size 349 MB, throughput 0.969743
Reading from 10137: heap size 401 MB, throughput 0.973928
Reading from 10135: heap size 352 MB, throughput 0.97415
Equal recommendation: 552 MB each
Reading from 10137: heap size 404 MB, throughput 0.969791
Reading from 10135: heap size 349 MB, throughput 0.980282
Reading from 10137: heap size 402 MB, throughput 0.973299
Reading from 10135: heap size 351 MB, throughput 0.973531
Reading from 10137: heap size 404 MB, throughput 0.965456
Reading from 10135: heap size 354 MB, throughput 0.975855
Reading from 10137: heap size 408 MB, throughput 0.973154
Reading from 10135: heap size 354 MB, throughput 0.974322
Reading from 10135: heap size 356 MB, throughput 0.963033
Reading from 10137: heap size 408 MB, throughput 0.960158
Reading from 10135: heap size 357 MB, throughput 0.977111
Reading from 10137: heap size 409 MB, throughput 0.978545
Reading from 10135: heap size 360 MB, throughput 0.971189
Reading from 10137: heap size 411 MB, throughput 0.974002
Equal recommendation: 552 MB each
Reading from 10135: heap size 361 MB, throughput 0.977543
Reading from 10135: heap size 362 MB, throughput 0.872518
Reading from 10135: heap size 364 MB, throughput 0.777725
Reading from 10135: heap size 369 MB, throughput 0.754427
Reading from 10135: heap size 372 MB, throughput 0.764603
Reading from 10137: heap size 412 MB, throughput 0.985582
Reading from 10137: heap size 413 MB, throughput 0.878233
Reading from 10135: heap size 381 MB, throughput 0.970599
Reading from 10137: heap size 410 MB, throughput 0.664774
Reading from 10137: heap size 414 MB, throughput 0.772416
Reading from 10137: heap size 423 MB, throughput 0.836269
Reading from 10137: heap size 425 MB, throughput 0.978755
Reading from 10135: heap size 382 MB, throughput 0.734059
Reading from 10137: heap size 432 MB, throughput 0.991644
Reading from 10135: heap size 420 MB, throughput 0.980123
Reading from 10137: heap size 434 MB, throughput 0.985873
Reading from 10135: heap size 423 MB, throughput 0.992599
Equal recommendation: 552 MB each
Reading from 10135: heap size 431 MB, throughput 0.984878
Reading from 10137: heap size 436 MB, throughput 0.984637
Reading from 10135: heap size 432 MB, throughput 0.992446
Reading from 10137: heap size 438 MB, throughput 0.984252
Reading from 10135: heap size 432 MB, throughput 0.987551
Reading from 10137: heap size 438 MB, throughput 0.980656
Reading from 10135: heap size 434 MB, throughput 0.987694
Reading from 10137: heap size 440 MB, throughput 0.98309
Reading from 10135: heap size 428 MB, throughput 0.989979
Reading from 10137: heap size 438 MB, throughput 0.982098
Reading from 10135: heap size 398 MB, throughput 0.984419
Equal recommendation: 552 MB each
Reading from 10137: heap size 440 MB, throughput 0.980529
Reading from 10135: heap size 427 MB, throughput 0.98147
Reading from 10137: heap size 438 MB, throughput 0.981899
Reading from 10135: heap size 429 MB, throughput 0.986126
Reading from 10135: heap size 431 MB, throughput 0.873785
Reading from 10135: heap size 431 MB, throughput 0.818856
Reading from 10135: heap size 437 MB, throughput 0.845337
Reading from 10137: heap size 440 MB, throughput 0.988633
Reading from 10137: heap size 443 MB, throughput 0.88001
Reading from 10137: heap size 444 MB, throughput 0.859161
Reading from 10137: heap size 449 MB, throughput 0.905332
Reading from 10135: heap size 441 MB, throughput 0.98339
Reading from 10135: heap size 446 MB, throughput 0.987736
Reading from 10137: heap size 449 MB, throughput 0.990987
Reading from 10135: heap size 449 MB, throughput 0.989974
Reading from 10137: heap size 454 MB, throughput 0.988874
Equal recommendation: 552 MB each
Reading from 10135: heap size 446 MB, throughput 0.986824
Reading from 10137: heap size 455 MB, throughput 0.988344
Reading from 10135: heap size 449 MB, throughput 0.986635
Reading from 10137: heap size 456 MB, throughput 0.987958
Reading from 10135: heap size 447 MB, throughput 0.989104
Reading from 10137: heap size 458 MB, throughput 0.987185
Reading from 10135: heap size 449 MB, throughput 0.982434
Reading from 10137: heap size 455 MB, throughput 0.984145
Reading from 10135: heap size 450 MB, throughput 0.982612
Reading from 10137: heap size 457 MB, throughput 0.988929
Equal recommendation: 552 MB each
Reading from 10135: heap size 450 MB, throughput 0.985575
Reading from 10137: heap size 456 MB, throughput 0.984403
Reading from 10135: heap size 452 MB, throughput 0.990545
Reading from 10135: heap size 453 MB, throughput 0.908712
Reading from 10135: heap size 453 MB, throughput 0.865525
Reading from 10135: heap size 455 MB, throughput 0.911613
Reading from 10137: heap size 457 MB, throughput 0.990653
Reading from 10137: heap size 457 MB, throughput 0.916526
Reading from 10137: heap size 459 MB, throughput 0.892622
Reading from 10137: heap size 464 MB, throughput 0.950717
Reading from 10135: heap size 462 MB, throughput 0.990915
Reading from 10137: heap size 465 MB, throughput 0.991863
Reading from 10135: heap size 463 MB, throughput 0.991112
Equal recommendation: 552 MB each
Reading from 10137: heap size 468 MB, throughput 0.987417
Reading from 10135: heap size 465 MB, throughput 0.987684
Reading from 10137: heap size 470 MB, throughput 0.988229
Reading from 10135: heap size 467 MB, throughput 0.987794
Reading from 10135: heap size 465 MB, throughput 0.987975
Reading from 10137: heap size 469 MB, throughput 0.988822
Reading from 10135: heap size 468 MB, throughput 0.988014
Reading from 10137: heap size 471 MB, throughput 0.987007
Reading from 10135: heap size 467 MB, throughput 0.986379
Equal recommendation: 552 MB each
Reading from 10137: heap size 468 MB, throughput 0.824922
Reading from 10135: heap size 468 MB, throughput 0.98007
Reading from 10137: heap size 492 MB, throughput 0.994666
Reading from 10135: heap size 468 MB, throughput 0.981686
Reading from 10135: heap size 472 MB, throughput 0.883825
Reading from 10135: heap size 476 MB, throughput 0.877193
Reading from 10137: heap size 492 MB, throughput 0.991825
Reading from 10135: heap size 476 MB, throughput 0.986409
Reading from 10137: heap size 495 MB, throughput 0.912681
Reading from 10137: heap size 494 MB, throughput 0.898794
Reading from 10137: heap size 496 MB, throughput 0.972951
Reading from 10135: heap size 484 MB, throughput 0.993792
Reading from 10137: heap size 503 MB, throughput 0.992084
Equal recommendation: 552 MB each
Reading from 10135: heap size 485 MB, throughput 0.990832
Reading from 10137: heap size 504 MB, throughput 0.991661
Reading from 10135: heap size 487 MB, throughput 0.990024
Reading from 10137: heap size 503 MB, throughput 0.990986
Reading from 10135: heap size 488 MB, throughput 0.990096
Reading from 10137: heap size 506 MB, throughput 0.988928
Reading from 10135: heap size 485 MB, throughput 0.987247
Reading from 10137: heap size 505 MB, throughput 0.988316
Equal recommendation: 552 MB each
Reading from 10135: heap size 488 MB, throughput 0.988088
Reading from 10137: heap size 506 MB, throughput 0.987231
Reading from 10135: heap size 490 MB, throughput 0.994844
Reading from 10137: heap size 509 MB, throughput 0.989105
Reading from 10135: heap size 490 MB, throughput 0.920406
Reading from 10135: heap size 491 MB, throughput 0.878213
Reading from 10137: heap size 510 MB, throughput 0.976437
Reading from 10137: heap size 513 MB, throughput 0.8691
Reading from 10137: heap size 516 MB, throughput 0.934468
Reading from 10135: heap size 493 MB, throughput 0.988106
Reading from 10135: heap size 500 MB, throughput 0.99252
Reading from 10137: heap size 526 MB, throughput 0.993633
Equal recommendation: 552 MB each
Reading from 10135: heap size 501 MB, throughput 0.992126
Reading from 10137: heap size 526 MB, throughput 0.991647
Reading from 10135: heap size 502 MB, throughput 0.989815
Reading from 10137: heap size 526 MB, throughput 0.991218
Reading from 10135: heap size 504 MB, throughput 0.989923
Reading from 10137: heap size 529 MB, throughput 0.99031
Equal recommendation: 552 MB each
Reading from 10135: heap size 503 MB, throughput 0.98868
Reading from 10137: heap size 527 MB, throughput 0.991511
Reading from 10135: heap size 504 MB, throughput 0.985068
Reading from 10137: heap size 529 MB, throughput 0.988002
Reading from 10135: heap size 506 MB, throughput 0.989177
Reading from 10135: heap size 507 MB, throughput 0.903037
Reading from 10135: heap size 508 MB, throughput 0.91093
Reading from 10137: heap size 531 MB, throughput 0.993294
Reading from 10137: heap size 532 MB, throughput 0.942591
Reading from 10137: heap size 532 MB, throughput 0.899563
Reading from 10135: heap size 510 MB, throughput 0.992759
Reading from 10137: heap size 534 MB, throughput 0.990278
Equal recommendation: 552 MB each
Reading from 10135: heap size 515 MB, throughput 0.992819
Reading from 10137: heap size 541 MB, throughput 0.992064
Reading from 10135: heap size 517 MB, throughput 0.991658
Reading from 10137: heap size 542 MB, throughput 0.991982
Reading from 10135: heap size 516 MB, throughput 0.990654
Reading from 10137: heap size 542 MB, throughput 0.993076
Equal recommendation: 552 MB each
Reading from 10135: heap size 519 MB, throughput 0.990118
Reading from 10137: heap size 544 MB, throughput 0.990732
Reading from 10135: heap size 521 MB, throughput 0.98642
Reading from 10137: heap size 544 MB, throughput 0.982276
Reading from 10135: heap size 521 MB, throughput 0.989196
Reading from 10135: heap size 524 MB, throughput 0.908797
Reading from 10137: heap size 545 MB, throughput 0.993083
Reading from 10135: heap size 526 MB, throughput 0.975346
Reading from 10137: heap size 546 MB, throughput 0.923861
Reading from 10137: heap size 548 MB, throughput 0.930683
Equal recommendation: 552 MB each
Reading from 10137: heap size 555 MB, throughput 0.994034
Reading from 10135: heap size 533 MB, throughput 0.992894
Reading from 10137: heap size 521 MB, throughput 0.994178
Reading from 10135: heap size 533 MB, throughput 0.992865
Reading from 10137: heap size 549 MB, throughput 0.990945
Reading from 10135: heap size 533 MB, throughput 0.990325
Equal recommendation: 552 MB each
Reading from 10137: heap size 548 MB, throughput 0.991294
Reading from 10135: heap size 534 MB, throughput 0.991073
Reading from 10137: heap size 548 MB, throughput 0.990176
Reading from 10135: heap size 533 MB, throughput 0.988209
Reading from 10137: heap size 549 MB, throughput 0.984757
Reading from 10135: heap size 534 MB, throughput 0.99398
Reading from 10137: heap size 549 MB, throughput 0.968263
Reading from 10135: heap size 536 MB, throughput 0.865882
Reading from 10137: heap size 544 MB, throughput 0.826063
Reading from 10135: heap size 537 MB, throughput 0.852158
Reading from 10137: heap size 550 MB, throughput 0.98219
Equal recommendation: 552 MB each
Reading from 10135: heap size 537 MB, throughput 0.991984
Reading from 10137: heap size 551 MB, throughput 0.992884
Reading from 10135: heap size 510 MB, throughput 0.992893
Reading from 10137: heap size 550 MB, throughput 0.990597
Reading from 10135: heap size 538 MB, throughput 0.991498
Reading from 10137: heap size 551 MB, throughput 0.992842
Equal recommendation: 552 MB each
Reading from 10135: heap size 517 MB, throughput 0.99096
Reading from 10137: heap size 550 MB, throughput 0.988809
Reading from 10135: heap size 536 MB, throughput 0.989966
Reading from 10137: heap size 550 MB, throughput 0.989543
Reading from 10135: heap size 536 MB, throughput 0.993438
Reading from 10137: heap size 553 MB, throughput 0.994231
Reading from 10137: heap size 524 MB, throughput 0.928791
Reading from 10137: heap size 544 MB, throughput 0.911321
Reading from 10135: heap size 538 MB, throughput 0.975497
Reading from 10135: heap size 536 MB, throughput 0.920757
Equal recommendation: 552 MB each
Reading from 10137: heap size 546 MB, throughput 0.991319
Reading from 10135: heap size 535 MB, throughput 0.994362
Reading from 10137: heap size 553 MB, throughput 0.991973
Reading from 10135: heap size 537 MB, throughput 0.99311
Reading from 10137: heap size 520 MB, throughput 0.991963
Reading from 10135: heap size 535 MB, throughput 0.991292
Equal recommendation: 552 MB each
Reading from 10137: heap size 547 MB, throughput 0.988838
Reading from 10135: heap size 536 MB, throughput 0.990168
Reading from 10137: heap size 546 MB, throughput 0.99172
Reading from 10135: heap size 536 MB, throughput 0.991378
Reading from 10137: heap size 546 MB, throughput 0.989368
Reading from 10135: heap size 537 MB, throughput 0.98742
Reading from 10137: heap size 547 MB, throughput 0.989301
Reading from 10137: heap size 550 MB, throughput 0.912743
Reading from 10135: heap size 538 MB, throughput 0.985884
Reading from 10135: heap size 533 MB, throughput 0.912816
Reading from 10137: heap size 552 MB, throughput 0.983993
Equal recommendation: 552 MB each
Reading from 10135: heap size 533 MB, throughput 0.991441
Reading from 10137: heap size 551 MB, throughput 0.992864
Reading from 10135: heap size 535 MB, throughput 0.992981
Reading from 10137: heap size 552 MB, throughput 0.992263
Reading from 10135: heap size 534 MB, throughput 0.991318
Reading from 10137: heap size 538 MB, throughput 0.992341
Equal recommendation: 552 MB each
Reading from 10135: heap size 535 MB, throughput 0.990464
Reading from 10137: heap size 545 MB, throughput 0.990054
Reading from 10135: heap size 534 MB, throughput 0.989364
Reading from 10137: heap size 544 MB, throughput 0.986302
Reading from 10135: heap size 534 MB, throughput 0.989623
Reading from 10137: heap size 545 MB, throughput 0.993277
Reading from 10137: heap size 545 MB, throughput 0.97502
Reading from 10137: heap size 547 MB, throughput 0.91355
Reading from 10135: heap size 536 MB, throughput 0.990836
Reading from 10135: heap size 536 MB, throughput 0.923033
Reading from 10135: heap size 534 MB, throughput 0.976098
Equal recommendation: 552 MB each
Reading from 10137: heap size 551 MB, throughput 0.992484
Reading from 10135: heap size 537 MB, throughput 0.993977
Reading from 10137: heap size 553 MB, throughput 0.992507
Reading from 10135: heap size 536 MB, throughput 0.992456
Reading from 10137: heap size 540 MB, throughput 0.993316
Equal recommendation: 552 MB each
Reading from 10137: heap size 547 MB, throughput 0.987143
Reading from 10135: heap size 536 MB, throughput 0.987097
Reading from 10135: heap size 534 MB, throughput 0.988007
Reading from 10137: heap size 545 MB, throughput 0.990555
Reading from 10137: heap size 547 MB, throughput 0.986945
Reading from 10135: heap size 535 MB, throughput 0.990406
Reading from 10137: heap size 548 MB, throughput 0.990446
Reading from 10137: heap size 549 MB, throughput 0.911807
Reading from 10137: heap size 550 MB, throughput 0.94605
Equal recommendation: 552 MB each
Reading from 10135: heap size 538 MB, throughput 0.99433
Reading from 10135: heap size 539 MB, throughput 0.936246
Reading from 10135: heap size 535 MB, throughput 0.912049
Reading from 10137: heap size 552 MB, throughput 0.995492
Reading from 10135: heap size 538 MB, throughput 0.992426
Reading from 10137: heap size 538 MB, throughput 0.992028
Reading from 10135: heap size 536 MB, throughput 0.991641
Reading from 10137: heap size 546 MB, throughput 0.992884
Equal recommendation: 552 MB each
Reading from 10135: heap size 537 MB, throughput 0.991806
Reading from 10137: heap size 545 MB, throughput 0.992473
Reading from 10135: heap size 536 MB, throughput 0.990841
Reading from 10137: heap size 547 MB, throughput 0.989995
Reading from 10135: heap size 537 MB, throughput 0.922288
Reading from 10137: heap size 549 MB, throughput 0.99149
Reading from 10137: heap size 550 MB, throughput 0.97336
Reading from 10137: heap size 552 MB, throughput 0.905265
Client 10137 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 10135: heap size 560 MB, throughput 0.995962
Reading from 10135: heap size 561 MB, throughput 0.980149
Reading from 10135: heap size 557 MB, throughput 0.93582
Client 10135 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
