economemd
    total memory: 4461 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub2_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 30440: heap size 9 MB, throughput 0.989007
Clients: 1
Client 30440 has a minimum heap size of 276 MB
Reading from 30439: heap size 9 MB, throughput 0.990695
Clients: 2
Client 30439 has a minimum heap size of 1211 MB
Reading from 30439: heap size 9 MB, throughput 0.974683
Reading from 30440: heap size 9 MB, throughput 0.905688
Reading from 30439: heap size 9 MB, throughput 0.961409
Reading from 30439: heap size 9 MB, throughput 0.939812
Reading from 30440: heap size 11 MB, throughput 0.974977
Reading from 30439: heap size 11 MB, throughput 0.985472
Reading from 30440: heap size 11 MB, throughput 0.973118
Reading from 30439: heap size 11 MB, throughput 0.973872
Reading from 30440: heap size 15 MB, throughput 0.748663
Reading from 30439: heap size 17 MB, throughput 0.831027
Reading from 30440: heap size 18 MB, throughput 0.899759
Reading from 30439: heap size 17 MB, throughput 0.593707
Reading from 30440: heap size 23 MB, throughput 0.946588
Reading from 30440: heap size 28 MB, throughput 0.717205
Reading from 30439: heap size 30 MB, throughput 0.966644
Reading from 30439: heap size 31 MB, throughput 0.908615
Reading from 30440: heap size 28 MB, throughput 0.424336
Reading from 30440: heap size 38 MB, throughput 0.755451
Reading from 30440: heap size 43 MB, throughput 0.812716
Reading from 30439: heap size 34 MB, throughput 0.453946
Reading from 30440: heap size 44 MB, throughput 0.704325
Reading from 30439: heap size 43 MB, throughput 0.903311
Reading from 30439: heap size 47 MB, throughput 0.821905
Reading from 30440: heap size 48 MB, throughput 0.297933
Reading from 30440: heap size 61 MB, throughput 0.794868
Reading from 30440: heap size 68 MB, throughput 0.738855
Reading from 30439: heap size 48 MB, throughput 0.21059
Reading from 30439: heap size 69 MB, throughput 0.637539
Reading from 30439: heap size 70 MB, throughput 0.775421
Reading from 30440: heap size 69 MB, throughput 0.284335
Reading from 30440: heap size 94 MB, throughput 0.706666
Reading from 30440: heap size 96 MB, throughput 0.712904
Reading from 30439: heap size 76 MB, throughput 0.349157
Reading from 30440: heap size 99 MB, throughput 0.822323
Reading from 30439: heap size 95 MB, throughput 0.565647
Reading from 30440: heap size 103 MB, throughput 0.662849
Reading from 30439: heap size 103 MB, throughput 0.804309
Reading from 30439: heap size 103 MB, throughput 0.656544
Reading from 30440: heap size 106 MB, throughput 0.758116
Reading from 30439: heap size 106 MB, throughput 0.178385
Reading from 30440: heap size 111 MB, throughput 0.115662
Reading from 30439: heap size 133 MB, throughput 0.715423
Reading from 30440: heap size 146 MB, throughput 0.504112
Reading from 30439: heap size 138 MB, throughput 0.789959
Reading from 30440: heap size 150 MB, throughput 0.771475
Reading from 30439: heap size 142 MB, throughput 0.746999
Reading from 30440: heap size 153 MB, throughput 0.696413
Reading from 30439: heap size 151 MB, throughput 0.774079
Reading from 30440: heap size 157 MB, throughput 0.696865
Reading from 30439: heap size 153 MB, throughput 0.647008
Reading from 30440: heap size 160 MB, throughput 0.568238
Reading from 30439: heap size 156 MB, throughput 0.590299
Reading from 30440: heap size 168 MB, throughput 0.503763
Reading from 30439: heap size 163 MB, throughput 0.117704
Reading from 30440: heap size 174 MB, throughput 0.0928807
Reading from 30439: heap size 209 MB, throughput 0.691735
Reading from 30440: heap size 211 MB, throughput 0.588411
Reading from 30439: heap size 214 MB, throughput 0.6732
Reading from 30439: heap size 217 MB, throughput 0.582591
Reading from 30440: heap size 217 MB, throughput 0.830613
Reading from 30439: heap size 222 MB, throughput 0.613697
Reading from 30440: heap size 218 MB, throughput 0.859542
Reading from 30440: heap size 218 MB, throughput 0.525945
Reading from 30439: heap size 230 MB, throughput 0.108815
Reading from 30440: heap size 225 MB, throughput 0.564009
Reading from 30439: heap size 270 MB, throughput 0.51183
Reading from 30439: heap size 278 MB, throughput 0.617823
Reading from 30439: heap size 279 MB, throughput 0.647554
Reading from 30440: heap size 230 MB, throughput 0.0777184
Reading from 30439: heap size 281 MB, throughput 0.640304
Reading from 30440: heap size 268 MB, throughput 0.738396
Reading from 30440: heap size 262 MB, throughput 0.576218
Reading from 30439: heap size 286 MB, throughput 0.562286
Reading from 30439: heap size 292 MB, throughput 0.110288
Reading from 30440: heap size 267 MB, throughput 0.102763
Reading from 30440: heap size 302 MB, throughput 0.58306
Reading from 30439: heap size 334 MB, throughput 0.519683
Reading from 30440: heap size 304 MB, throughput 0.920682
Reading from 30439: heap size 341 MB, throughput 0.714577
Reading from 30440: heap size 307 MB, throughput 0.898766
Reading from 30439: heap size 342 MB, throughput 0.654255
Reading from 30440: heap size 308 MB, throughput 0.725014
Reading from 30440: heap size 301 MB, throughput 0.807181
Reading from 30439: heap size 345 MB, throughput 0.705958
Reading from 30440: heap size 305 MB, throughput 0.85138
Reading from 30440: heap size 300 MB, throughput 0.611041
Reading from 30439: heap size 349 MB, throughput 0.604067
Reading from 30440: heap size 303 MB, throughput 0.834059
Reading from 30439: heap size 352 MB, throughput 0.599328
Reading from 30440: heap size 301 MB, throughput 0.952319
Reading from 30440: heap size 303 MB, throughput 0.961854
Reading from 30439: heap size 361 MB, throughput 0.122616
Reading from 30440: heap size 301 MB, throughput 0.852806
Reading from 30440: heap size 303 MB, throughput 0.718024
Reading from 30439: heap size 410 MB, throughput 0.501323
Reading from 30440: heap size 301 MB, throughput 0.875669
Reading from 30439: heap size 418 MB, throughput 0.619185
Reading from 30440: heap size 303 MB, throughput 0.876241
Reading from 30439: heap size 420 MB, throughput 0.515278
Reading from 30439: heap size 424 MB, throughput 0.495925
Reading from 30440: heap size 300 MB, throughput 0.895964
Reading from 30440: heap size 302 MB, throughput 0.725723
Reading from 30440: heap size 301 MB, throughput 0.635005
Reading from 30440: heap size 302 MB, throughput 0.58009
Reading from 30440: heap size 307 MB, throughput 0.62262
Reading from 30440: heap size 308 MB, throughput 0.570268
Equal recommendation: 2230 MB each
Reading from 30440: heap size 315 MB, throughput 0.689603
Reading from 30439: heap size 431 MB, throughput 0.10829
Reading from 30440: heap size 315 MB, throughput 0.760057
Reading from 30439: heap size 485 MB, throughput 0.388265
Reading from 30439: heap size 478 MB, throughput 0.639689
Reading from 30439: heap size 417 MB, throughput 0.0983466
Reading from 30439: heap size 530 MB, throughput 0.538377
Reading from 30439: heap size 535 MB, throughput 0.583472
Reading from 30440: heap size 321 MB, throughput 0.96554
Reading from 30439: heap size 528 MB, throughput 0.574381
Reading from 30439: heap size 532 MB, throughput 0.593019
Reading from 30439: heap size 533 MB, throughput 0.629898
Reading from 30439: heap size 538 MB, throughput 0.521769
Reading from 30439: heap size 543 MB, throughput 0.531266
Reading from 30439: heap size 549 MB, throughput 0.556208
Reading from 30439: heap size 555 MB, throughput 0.0826769
Reading from 30440: heap size 323 MB, throughput 0.609111
Reading from 30439: heap size 617 MB, throughput 0.545117
Reading from 30439: heap size 628 MB, throughput 0.585004
Reading from 30439: heap size 630 MB, throughput 0.593668
Reading from 30439: heap size 634 MB, throughput 0.643548
Reading from 30439: heap size 639 MB, throughput 0.787796
Reading from 30440: heap size 364 MB, throughput 0.965871
Reading from 30439: heap size 643 MB, throughput 0.836019
Reading from 30440: heap size 365 MB, throughput 0.972879
Reading from 30439: heap size 650 MB, throughput 0.273938
Reading from 30440: heap size 370 MB, throughput 0.977044
Reading from 30439: heap size 715 MB, throughput 0.779165
Reading from 30439: heap size 729 MB, throughput 0.476566
Reading from 30439: heap size 739 MB, throughput 0.629118
Reading from 30439: heap size 740 MB, throughput 0.645728
Reading from 30440: heap size 371 MB, throughput 0.978867
Equal recommendation: 2230 MB each
Reading from 30439: heap size 733 MB, throughput 0.0266208
Reading from 30440: heap size 371 MB, throughput 0.983668
Reading from 30439: heap size 720 MB, throughput 0.288281
Reading from 30439: heap size 796 MB, throughput 0.450581
Reading from 30439: heap size 801 MB, throughput 0.560332
Reading from 30439: heap size 800 MB, throughput 0.828744
Reading from 30439: heap size 802 MB, throughput 0.532582
Reading from 30440: heap size 373 MB, throughput 0.960825
Reading from 30439: heap size 800 MB, throughput 0.0525654
Reading from 30439: heap size 878 MB, throughput 0.424946
Reading from 30440: heap size 368 MB, throughput 0.98392
Reading from 30439: heap size 882 MB, throughput 0.893315
Reading from 30439: heap size 884 MB, throughput 0.827939
Reading from 30440: heap size 371 MB, throughput 0.96333
Reading from 30439: heap size 889 MB, throughput 0.127101
Reading from 30439: heap size 981 MB, throughput 0.711441
Reading from 30439: heap size 985 MB, throughput 0.952778
Reading from 30439: heap size 990 MB, throughput 0.925023
Reading from 30440: heap size 367 MB, throughput 0.980265
Reading from 30439: heap size 984 MB, throughput 0.877834
Reading from 30439: heap size 818 MB, throughput 0.7818
Reading from 30439: heap size 968 MB, throughput 0.823245
Reading from 30439: heap size 822 MB, throughput 0.825151
Reading from 30439: heap size 955 MB, throughput 0.825036
Reading from 30439: heap size 827 MB, throughput 0.827948
Reading from 30439: heap size 946 MB, throughput 0.832323
Reading from 30440: heap size 369 MB, throughput 0.975912
Reading from 30439: heap size 833 MB, throughput 0.840333
Reading from 30439: heap size 937 MB, throughput 0.838263
Reading from 30439: heap size 838 MB, throughput 0.844804
Reading from 30439: heap size 929 MB, throughput 0.840203
Reading from 30440: heap size 371 MB, throughput 0.976466
Reading from 30439: heap size 936 MB, throughput 0.980639
Reading from 30440: heap size 372 MB, throughput 0.972955
Reading from 30439: heap size 928 MB, throughput 0.95538
Equal recommendation: 2230 MB each
Reading from 30439: heap size 932 MB, throughput 0.746702
Reading from 30439: heap size 935 MB, throughput 0.796489
Reading from 30439: heap size 935 MB, throughput 0.79986
Reading from 30439: heap size 936 MB, throughput 0.80297
Reading from 30440: heap size 373 MB, throughput 0.970405
Reading from 30439: heap size 938 MB, throughput 0.811714
Reading from 30439: heap size 940 MB, throughput 0.804577
Reading from 30439: heap size 941 MB, throughput 0.805539
Reading from 30439: heap size 942 MB, throughput 0.828284
Reading from 30439: heap size 945 MB, throughput 0.81901
Reading from 30439: heap size 946 MB, throughput 0.870625
Reading from 30439: heap size 948 MB, throughput 0.828635
Reading from 30440: heap size 375 MB, throughput 0.982314
Reading from 30439: heap size 950 MB, throughput 0.731579
Reading from 30440: heap size 378 MB, throughput 0.884776
Reading from 30439: heap size 952 MB, throughput 0.609963
Reading from 30440: heap size 379 MB, throughput 0.71299
Reading from 30440: heap size 385 MB, throughput 0.796559
Reading from 30439: heap size 982 MB, throughput 0.721294
Reading from 30440: heap size 390 MB, throughput 0.791408
Reading from 30440: heap size 397 MB, throughput 0.744117
Reading from 30440: heap size 400 MB, throughput 0.976492
Reading from 30439: heap size 982 MB, throughput 0.0685844
Reading from 30439: heap size 1080 MB, throughput 0.494658
Reading from 30439: heap size 1083 MB, throughput 0.775928
Reading from 30439: heap size 1085 MB, throughput 0.711341
Reading from 30439: heap size 1089 MB, throughput 0.774267
Reading from 30439: heap size 1095 MB, throughput 0.637182
Reading from 30440: heap size 398 MB, throughput 0.974307
Reading from 30439: heap size 1097 MB, throughput 0.700507
Reading from 30439: heap size 1113 MB, throughput 0.944562
Reading from 30440: heap size 402 MB, throughput 0.981851
Reading from 30439: heap size 1113 MB, throughput 0.954174
Reading from 30440: heap size 400 MB, throughput 0.982698
Equal recommendation: 2230 MB each
Reading from 30440: heap size 403 MB, throughput 0.984562
Reading from 30439: heap size 1128 MB, throughput 0.939616
Reading from 30440: heap size 399 MB, throughput 0.980127
Reading from 30439: heap size 1131 MB, throughput 0.955468
Reading from 30440: heap size 402 MB, throughput 0.964744
Reading from 30439: heap size 1144 MB, throughput 0.954204
Reading from 30440: heap size 400 MB, throughput 0.97843
Reading from 30440: heap size 402 MB, throughput 0.970474
Reading from 30439: heap size 1149 MB, throughput 0.673071
Reading from 30440: heap size 403 MB, throughput 0.981217
Equal recommendation: 2230 MB each
Reading from 30439: heap size 1230 MB, throughput 0.990019
Reading from 30440: heap size 403 MB, throughput 0.977808
Reading from 30439: heap size 1230 MB, throughput 0.985139
Reading from 30440: heap size 403 MB, throughput 0.99016
Reading from 30440: heap size 403 MB, throughput 0.977019
Reading from 30439: heap size 1244 MB, throughput 0.982725
Reading from 30440: heap size 405 MB, throughput 0.861831
Reading from 30440: heap size 405 MB, throughput 0.833258
Reading from 30440: heap size 410 MB, throughput 0.823696
Reading from 30440: heap size 413 MB, throughput 0.977951
Reading from 30439: heap size 1248 MB, throughput 0.980595
Reading from 30440: heap size 419 MB, throughput 0.986583
Reading from 30439: heap size 1250 MB, throughput 0.984345
Reading from 30440: heap size 421 MB, throughput 0.988458
Equal recommendation: 2230 MB each
Reading from 30439: heap size 1253 MB, throughput 0.977279
Reading from 30440: heap size 421 MB, throughput 0.984775
Reading from 30439: heap size 1244 MB, throughput 0.977112
Reading from 30440: heap size 423 MB, throughput 0.985498
Reading from 30440: heap size 422 MB, throughput 0.983656
Reading from 30439: heap size 1164 MB, throughput 0.97089
Reading from 30440: heap size 424 MB, throughput 0.983445
Reading from 30439: heap size 1234 MB, throughput 0.971585
Reading from 30440: heap size 422 MB, throughput 0.983218
Reading from 30439: heap size 1241 MB, throughput 0.969119
Reading from 30440: heap size 423 MB, throughput 0.981947
Equal recommendation: 2230 MB each
Reading from 30439: heap size 1242 MB, throughput 0.969377
Reading from 30440: heap size 425 MB, throughput 0.982841
Reading from 30439: heap size 1242 MB, throughput 0.961814
Reading from 30440: heap size 425 MB, throughput 0.989993
Reading from 30440: heap size 427 MB, throughput 0.912136
Reading from 30440: heap size 428 MB, throughput 0.859562
Reading from 30440: heap size 432 MB, throughput 0.86375
Reading from 30439: heap size 1246 MB, throughput 0.969862
Reading from 30440: heap size 433 MB, throughput 0.987535
Reading from 30439: heap size 1249 MB, throughput 0.958324
Reading from 30440: heap size 439 MB, throughput 0.988699
Equal recommendation: 2230 MB each
Reading from 30440: heap size 440 MB, throughput 0.98759
Reading from 30439: heap size 1254 MB, throughput 0.958544
Reading from 30440: heap size 442 MB, throughput 0.987314
Reading from 30439: heap size 1261 MB, throughput 0.944539
Reading from 30440: heap size 443 MB, throughput 0.986413
Reading from 30439: heap size 1268 MB, throughput 0.962632
Reading from 30440: heap size 441 MB, throughput 0.984014
Reading from 30439: heap size 1275 MB, throughput 0.947601
Reading from 30440: heap size 443 MB, throughput 0.985247
Reading from 30439: heap size 1283 MB, throughput 0.953612
Equal recommendation: 2230 MB each
Reading from 30440: heap size 443 MB, throughput 0.984466
Reading from 30439: heap size 1286 MB, throughput 0.956407
Reading from 30440: heap size 444 MB, throughput 0.98121
Reading from 30440: heap size 443 MB, throughput 0.983218
Reading from 30439: heap size 1295 MB, throughput 0.94762
Reading from 30440: heap size 447 MB, throughput 0.891235
Reading from 30440: heap size 447 MB, throughput 0.880695
Reading from 30440: heap size 448 MB, throughput 0.982873
Reading from 30439: heap size 1297 MB, throughput 0.956855
Reading from 30440: heap size 454 MB, throughput 0.989203
Reading from 30439: heap size 1306 MB, throughput 0.954891
Reading from 30440: heap size 455 MB, throughput 0.990373
Equal recommendation: 2230 MB each
Reading from 30440: heap size 455 MB, throughput 0.987562
Reading from 30439: heap size 1307 MB, throughput 0.953227
Reading from 30440: heap size 457 MB, throughput 0.987647
Reading from 30439: heap size 1315 MB, throughput 0.957085
Reading from 30440: heap size 456 MB, throughput 0.985084
Reading from 30439: heap size 1315 MB, throughput 0.958542
Reading from 30440: heap size 457 MB, throughput 0.984799
Equal recommendation: 2230 MB each
Reading from 30439: heap size 1322 MB, throughput 0.95904
Reading from 30440: heap size 458 MB, throughput 0.983044
Reading from 30440: heap size 458 MB, throughput 0.984967
Reading from 30439: heap size 1323 MB, throughput 0.961083
Reading from 30440: heap size 460 MB, throughput 0.96214
Reading from 30440: heap size 461 MB, throughput 0.880102
Reading from 30440: heap size 464 MB, throughput 0.9151
Reading from 30439: heap size 1328 MB, throughput 0.959513
Reading from 30440: heap size 466 MB, throughput 0.990217
Reading from 30440: heap size 471 MB, throughput 0.990513
Reading from 30439: heap size 1329 MB, throughput 0.957536
Equal recommendation: 2230 MB each
Reading from 30440: heap size 472 MB, throughput 0.98729
Reading from 30440: heap size 470 MB, throughput 0.989859
Reading from 30439: heap size 1336 MB, throughput 0.561189
Reading from 30440: heap size 473 MB, throughput 0.987297
Reading from 30439: heap size 1456 MB, throughput 0.946421
Reading from 30440: heap size 472 MB, throughput 0.98533
Reading from 30439: heap size 1454 MB, throughput 0.981084
Equal recommendation: 2230 MB each
Reading from 30440: heap size 473 MB, throughput 0.984259
Reading from 30439: heap size 1464 MB, throughput 0.978333
Reading from 30440: heap size 475 MB, throughput 0.989245
Reading from 30440: heap size 476 MB, throughput 0.913039
Reading from 30440: heap size 475 MB, throughput 0.888813
Reading from 30440: heap size 477 MB, throughput 0.982721
Reading from 30439: heap size 1476 MB, throughput 0.983431
Reading from 30440: heap size 484 MB, throughput 0.992184
Reading from 30439: heap size 1477 MB, throughput 0.973377
Reading from 30440: heap size 485 MB, throughput 0.990126
Equal recommendation: 2230 MB each
Reading from 30440: heap size 486 MB, throughput 0.988718
Reading from 30440: heap size 487 MB, throughput 0.988518
Reading from 30440: heap size 485 MB, throughput 0.851908
Equal recommendation: 2230 MB each
Reading from 30440: heap size 508 MB, throughput 0.995032
Reading from 30440: heap size 507 MB, throughput 0.991556
Reading from 30440: heap size 510 MB, throughput 0.906388
Reading from 30440: heap size 509 MB, throughput 0.903788
Reading from 30440: heap size 511 MB, throughput 0.989795
Reading from 30440: heap size 517 MB, throughput 0.993117
Equal recommendation: 2230 MB each
Reading from 30440: heap size 519 MB, throughput 0.990818
Reading from 30440: heap size 518 MB, throughput 0.989382
Reading from 30439: heap size 1474 MB, throughput 0.996935
Reading from 30440: heap size 521 MB, throughput 0.988472
Reading from 30440: heap size 522 MB, throughput 0.988137
Equal recommendation: 2230 MB each
Reading from 30440: heap size 523 MB, throughput 0.983052
Reading from 30440: heap size 522 MB, throughput 0.979309
Reading from 30440: heap size 527 MB, throughput 0.900082
Reading from 30440: heap size 531 MB, throughput 0.980677
Reading from 30439: heap size 1478 MB, throughput 0.984058
Reading from 30439: heap size 1460 MB, throughput 0.893336
Reading from 30440: heap size 533 MB, throughput 0.985003
Reading from 30439: heap size 1489 MB, throughput 0.719023
Reading from 30439: heap size 1473 MB, throughput 0.676595
Reading from 30439: heap size 1515 MB, throughput 0.656682
Reading from 30439: heap size 1543 MB, throughput 0.679888
Reading from 30439: heap size 1562 MB, throughput 0.678133
Reading from 30439: heap size 1596 MB, throughput 0.706502
Reading from 30440: heap size 534 MB, throughput 0.98858
Equal recommendation: 2230 MB each
Reading from 30439: heap size 1606 MB, throughput 0.929981
Reading from 30440: heap size 536 MB, throughput 0.991082
Reading from 30439: heap size 1610 MB, throughput 0.967428
Reading from 30440: heap size 534 MB, throughput 0.989572
Reading from 30439: heap size 1628 MB, throughput 0.959892
Reading from 30440: heap size 537 MB, throughput 0.989284
Reading from 30439: heap size 1606 MB, throughput 0.970685
Equal recommendation: 2230 MB each
Reading from 30440: heap size 539 MB, throughput 0.986607
Reading from 30439: heap size 1624 MB, throughput 0.962046
Reading from 30440: heap size 539 MB, throughput 0.983945
Reading from 30440: heap size 544 MB, throughput 0.905886
Reading from 30440: heap size 545 MB, throughput 0.986332
Reading from 30439: heap size 1605 MB, throughput 0.962457
Reading from 30440: heap size 552 MB, throughput 0.992445
Reading from 30439: heap size 1620 MB, throughput 0.755391
Reading from 30440: heap size 553 MB, throughput 0.991498
Equal recommendation: 2230 MB each
Reading from 30440: heap size 553 MB, throughput 0.984262
Reading from 30439: heap size 1578 MB, throughput 0.991322
Reading from 30440: heap size 555 MB, throughput 0.989764
Reading from 30439: heap size 1584 MB, throughput 0.988304
Reading from 30440: heap size 553 MB, throughput 0.989842
Reading from 30439: heap size 1609 MB, throughput 0.985614
Equal recommendation: 2230 MB each
Reading from 30440: heap size 555 MB, throughput 0.992786
Reading from 30440: heap size 555 MB, throughput 0.92018
Reading from 30440: heap size 557 MB, throughput 0.975254
Reading from 30439: heap size 1616 MB, throughput 0.987402
Reading from 30440: heap size 564 MB, throughput 0.993597
Reading from 30439: heap size 1619 MB, throughput 0.980088
Reading from 30440: heap size 565 MB, throughput 0.991982
Equal recommendation: 2230 MB each
Reading from 30439: heap size 1625 MB, throughput 0.979957
Reading from 30440: heap size 565 MB, throughput 0.991547
Reading from 30439: heap size 1608 MB, throughput 0.97692
Reading from 30440: heap size 567 MB, throughput 0.990542
Reading from 30439: heap size 1442 MB, throughput 0.973853
Reading from 30440: heap size 568 MB, throughput 0.989645
Equal recommendation: 2230 MB each
Reading from 30440: heap size 569 MB, throughput 0.993705
Reading from 30440: heap size 572 MB, throughput 0.92539
Reading from 30439: heap size 1590 MB, throughput 0.976693
Reading from 30440: heap size 572 MB, throughput 0.98662
Reading from 30439: heap size 1604 MB, throughput 0.976423
Reading from 30440: heap size 580 MB, throughput 0.993597
Reading from 30440: heap size 580 MB, throughput 0.992145
Equal recommendation: 2230 MB each
Reading from 30439: heap size 1592 MB, throughput 0.967512
Reading from 30440: heap size 580 MB, throughput 0.992773
Reading from 30439: heap size 1598 MB, throughput 0.967234
Reading from 30440: heap size 582 MB, throughput 0.991356
Reading from 30439: heap size 1606 MB, throughput 0.970136
Equal recommendation: 2230 MB each
Reading from 30440: heap size 583 MB, throughput 0.988429
Reading from 30440: heap size 584 MB, throughput 0.982654
Reading from 30440: heap size 590 MB, throughput 0.92532
Reading from 30439: heap size 1609 MB, throughput 0.969574
Reading from 30440: heap size 591 MB, throughput 0.992802
Reading from 30439: heap size 1619 MB, throughput 0.963613
Reading from 30440: heap size 597 MB, throughput 0.993845
Equal recommendation: 2230 MB each
Reading from 30439: heap size 1628 MB, throughput 0.95865
Reading from 30440: heap size 598 MB, throughput 0.991809
Reading from 30439: heap size 1644 MB, throughput 0.961075
Reading from 30440: heap size 597 MB, throughput 0.99109
Reading from 30440: heap size 599 MB, throughput 0.990279
Reading from 30439: heap size 1653 MB, throughput 0.95734
Equal recommendation: 2230 MB each
Reading from 30440: heap size 601 MB, throughput 0.989622
Reading from 30440: heap size 602 MB, throughput 0.92691
Client 30440 died
Clients: 1
Reading from 30439: heap size 1670 MB, throughput 0.970297
Reading from 30439: heap size 1675 MB, throughput 0.969468
Recommendation: one client; give it all the memory
Reading from 30439: heap size 1692 MB, throughput 0.970655
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 30439: heap size 1695 MB, throughput 0.995799
Recommendation: one client; give it all the memory
Reading from 30439: heap size 1710 MB, throughput 0.985788
Reading from 30439: heap size 1713 MB, throughput 0.820619
Reading from 30439: heap size 1697 MB, throughput 0.808532
Reading from 30439: heap size 1722 MB, throughput 0.799143
Client 30439 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
CommandThread: finished
ReadingThread: finished
TimerThread: finished
Main: All threads killed, cleaning up
