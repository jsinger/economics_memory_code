economemd
    total memory: 7314 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub10_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
ReadingThread: starting
TimerThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 30807: heap size 9 MB, throughput 0.990774
Clients: 1
Client 30807 has a minimum heap size of 8 MB
Reading from 30805: heap size 9 MB, throughput 0.991668
Clients: 2
Client 30805 has a minimum heap size of 1211 MB
Reading from 30805: heap size 9 MB, throughput 0.976613
Reading from 30807: heap size 9 MB, throughput 0.983981
Reading from 30805: heap size 9 MB, throughput 0.963225
Reading from 30807: heap size 9 MB, throughput 0.955341
Reading from 30807: heap size 9 MB, throughput 0.899092
Reading from 30805: heap size 9 MB, throughput 0.938765
Reading from 30807: heap size 11 MB, throughput 0.922299
Reading from 30807: heap size 11 MB, throughput 0.974922
Reading from 30805: heap size 11 MB, throughput 0.963406
Reading from 30807: heap size 17 MB, throughput 0.973426
Reading from 30805: heap size 11 MB, throughput 0.977525
Reading from 30807: heap size 17 MB, throughput 0.96995
Reading from 30805: heap size 17 MB, throughput 0.966652
Reading from 30807: heap size 25 MB, throughput 0.720063
Reading from 30807: heap size 29 MB, throughput 0.936378
Reading from 30807: heap size 29 MB, throughput 0.979422
Reading from 30805: heap size 17 MB, throughput 0.651181
Reading from 30807: heap size 34 MB, throughput 0.991943
Reading from 30805: heap size 30 MB, throughput 0.892544
Reading from 30807: heap size 37 MB, throughput 0.989269
Reading from 30807: heap size 37 MB, throughput 0.974378
Reading from 30807: heap size 41 MB, throughput 0.967667
Reading from 30805: heap size 31 MB, throughput 0.926975
Reading from 30807: heap size 41 MB, throughput 0.958829
Reading from 30807: heap size 46 MB, throughput 0.98559
Reading from 30807: heap size 46 MB, throughput 0.97937
Reading from 30805: heap size 34 MB, throughput 0.420286
Reading from 30807: heap size 50 MB, throughput 0.977027
Reading from 30805: heap size 48 MB, throughput 0.837231
Reading from 30807: heap size 50 MB, throughput 0.978252
Reading from 30805: heap size 49 MB, throughput 0.79255
Reading from 30807: heap size 55 MB, throughput 0.985096
Reading from 30807: heap size 55 MB, throughput 0.986819
Reading from 30807: heap size 59 MB, throughput 0.983132
Reading from 30805: heap size 51 MB, throughput 0.220857
Reading from 30807: heap size 60 MB, throughput 0.988854
Reading from 30805: heap size 73 MB, throughput 0.643526
Reading from 30807: heap size 64 MB, throughput 0.981483
Reading from 30807: heap size 64 MB, throughput 0.976412
Reading from 30807: heap size 68 MB, throughput 0.985166
Reading from 30805: heap size 73 MB, throughput 0.244671
Reading from 30805: heap size 100 MB, throughput 0.794987
Reading from 30807: heap size 68 MB, throughput 0.991638
Reading from 30805: heap size 100 MB, throughput 0.80682
Reading from 30807: heap size 72 MB, throughput 0.990497
Reading from 30805: heap size 103 MB, throughput 0.742997
Reading from 30807: heap size 72 MB, throughput 0.938384
Reading from 30807: heap size 76 MB, throughput 0.991303
Reading from 30805: heap size 105 MB, throughput 0.201075
Reading from 30807: heap size 76 MB, throughput 0.986194
Reading from 30807: heap size 81 MB, throughput 0.986067
Reading from 30805: heap size 132 MB, throughput 0.64133
Reading from 30805: heap size 137 MB, throughput 0.795364
Reading from 30807: heap size 82 MB, throughput 0.989146
Reading from 30805: heap size 140 MB, throughput 0.695402
Reading from 30807: heap size 86 MB, throughput 0.992599
Reading from 30807: heap size 86 MB, throughput 0.989006
Reading from 30807: heap size 90 MB, throughput 0.992089
Reading from 30805: heap size 141 MB, throughput 0.143449
Reading from 30807: heap size 90 MB, throughput 0.994284
Reading from 30805: heap size 181 MB, throughput 0.61405
Reading from 30805: heap size 183 MB, throughput 0.640159
Reading from 30807: heap size 93 MB, throughput 0.995027
Reading from 30805: heap size 185 MB, throughput 0.719904
Reading from 30807: heap size 93 MB, throughput 0.993316
Reading from 30805: heap size 191 MB, throughput 0.714742
Reading from 30807: heap size 96 MB, throughput 0.992632
Reading from 30805: heap size 195 MB, throughput 0.681031
Reading from 30807: heap size 96 MB, throughput 0.991438
Reading from 30807: heap size 99 MB, throughput 0.995032
Reading from 30807: heap size 99 MB, throughput 0.986835
Reading from 30805: heap size 201 MB, throughput 0.13327
Reading from 30807: heap size 101 MB, throughput 0.994309
Reading from 30807: heap size 101 MB, throughput 0.990384
Reading from 30805: heap size 248 MB, throughput 0.57212
Reading from 30807: heap size 104 MB, throughput 0.991027
Reading from 30805: heap size 252 MB, throughput 0.639218
Reading from 30805: heap size 255 MB, throughput 0.708488
Reading from 30807: heap size 104 MB, throughput 0.993038
Reading from 30805: heap size 262 MB, throughput 0.67145
Reading from 30807: heap size 107 MB, throughput 0.99621
Reading from 30805: heap size 265 MB, throughput 0.581754
Reading from 30807: heap size 107 MB, throughput 0.993038
Reading from 30805: heap size 275 MB, throughput 0.536182
Reading from 30805: heap size 279 MB, throughput 0.42167
Reading from 30807: heap size 109 MB, throughput 0.99539
Reading from 30805: heap size 290 MB, throughput 0.447194
Reading from 30807: heap size 109 MB, throughput 0.990782
Reading from 30807: heap size 112 MB, throughput 0.995915
Reading from 30807: heap size 112 MB, throughput 0.990528
Reading from 30807: heap size 114 MB, throughput 0.99265
Reading from 30805: heap size 299 MB, throughput 0.0887384
Reading from 30807: heap size 114 MB, throughput 0.990927
Reading from 30805: heap size 349 MB, throughput 0.390729
Reading from 30807: heap size 117 MB, throughput 0.993185
Reading from 30807: heap size 117 MB, throughput 0.992517
Reading from 30805: heap size 342 MB, throughput 0.100182
Reading from 30807: heap size 120 MB, throughput 0.991825
Reading from 30805: heap size 340 MB, throughput 0.540896
Reading from 30805: heap size 392 MB, throughput 0.639649
Reading from 30807: heap size 120 MB, throughput 0.974415
Reading from 30805: heap size 396 MB, throughput 0.723594
Reading from 30805: heap size 386 MB, throughput 0.600033
Reading from 30807: heap size 123 MB, throughput 0.987558
Reading from 30805: heap size 391 MB, throughput 0.601965
Reading from 30805: heap size 391 MB, throughput 0.600308
Reading from 30807: heap size 123 MB, throughput 0.99197
Reading from 30805: heap size 393 MB, throughput 0.562084
Reading from 30805: heap size 396 MB, throughput 0.517867
Reading from 30807: heap size 127 MB, throughput 0.995782
Reading from 30805: heap size 403 MB, throughput 0.4713
Reading from 30807: heap size 128 MB, throughput 0.992992
Reading from 30805: heap size 408 MB, throughput 0.505875
Reading from 30805: heap size 416 MB, throughput 0.386991
Reading from 30807: heap size 131 MB, throughput 0.996714
Reading from 30805: heap size 420 MB, throughput 0.50647
Reading from 30805: heap size 428 MB, throughput 0.429962
Reading from 30807: heap size 131 MB, throughput 0.996516
Reading from 30807: heap size 134 MB, throughput 0.994806
Reading from 30807: heap size 134 MB, throughput 0.992771
Reading from 30807: heap size 137 MB, throughput 0.993688
Reading from 30807: heap size 137 MB, throughput 0.994999
Reading from 30805: heap size 435 MB, throughput 0.0590115
Reading from 30807: heap size 140 MB, throughput 0.997087
Reading from 30805: heap size 493 MB, throughput 0.279065
Reading from 30807: heap size 140 MB, throughput 0.995509
Equal recommendation: 3657 MB each
Reading from 30805: heap size 483 MB, throughput 0.414137
Reading from 30807: heap size 143 MB, throughput 0.995293
Reading from 30807: heap size 143 MB, throughput 0.995535
Reading from 30807: heap size 145 MB, throughput 0.996622
Reading from 30807: heap size 145 MB, throughput 0.994044
Reading from 30805: heap size 428 MB, throughput 0.0677897
Reading from 30807: heap size 148 MB, throughput 0.993328
Reading from 30805: heap size 544 MB, throughput 0.368435
Reading from 30805: heap size 548 MB, throughput 0.500485
Reading from 30807: heap size 148 MB, throughput 0.996083
Reading from 30805: heap size 544 MB, throughput 0.520796
Reading from 30807: heap size 150 MB, throughput 0.994211
Reading from 30805: heap size 548 MB, throughput 0.552376
Reading from 30805: heap size 539 MB, throughput 0.548022
Reading from 30807: heap size 150 MB, throughput 0.996378
Reading from 30805: heap size 544 MB, throughput 0.541895
Reading from 30807: heap size 153 MB, throughput 0.950281
Reading from 30805: heap size 544 MB, throughput 0.628139
Reading from 30807: heap size 155 MB, throughput 0.992536
Reading from 30807: heap size 158 MB, throughput 0.994403
Reading from 30807: heap size 158 MB, throughput 0.995858
Reading from 30805: heap size 546 MB, throughput 0.146289
Reading from 30807: heap size 163 MB, throughput 0.996662
Reading from 30805: heap size 612 MB, throughput 0.463614
Reading from 30807: heap size 163 MB, throughput 0.994971
Reading from 30805: heap size 617 MB, throughput 0.570316
Reading from 30805: heap size 617 MB, throughput 0.495909
Reading from 30807: heap size 168 MB, throughput 0.995944
Reading from 30805: heap size 622 MB, throughput 0.445406
Reading from 30807: heap size 168 MB, throughput 0.994259
Reading from 30805: heap size 626 MB, throughput 0.436098
Reading from 30807: heap size 172 MB, throughput 0.996602
Reading from 30807: heap size 172 MB, throughput 0.995506
Reading from 30805: heap size 636 MB, throughput 0.771182
Reading from 30807: heap size 176 MB, throughput 0.995964
Reading from 30807: heap size 177 MB, throughput 0.990786
Reading from 30805: heap size 640 MB, throughput 0.763011
Reading from 30807: heap size 181 MB, throughput 0.996736
Reading from 30807: heap size 181 MB, throughput 0.9801
Reading from 30805: heap size 652 MB, throughput 0.750312
Reading from 30807: heap size 187 MB, throughput 0.994158
Reading from 30807: heap size 187 MB, throughput 0.992197
Reading from 30805: heap size 653 MB, throughput 0.783184
Reading from 30807: heap size 195 MB, throughput 0.992609
Reading from 30805: heap size 670 MB, throughput 0.530338
Reading from 30805: heap size 680 MB, throughput 0.379862
Reading from 30807: heap size 195 MB, throughput 0.9969
Reading from 30807: heap size 202 MB, throughput 0.997971
Reading from 30807: heap size 203 MB, throughput 0.995856
Reading from 30807: heap size 209 MB, throughput 0.996382
Reading from 30805: heap size 690 MB, throughput 0.0325743
Reading from 30807: heap size 209 MB, throughput 0.997172
Reading from 30805: heap size 762 MB, throughput 0.3416
Reading from 30807: heap size 214 MB, throughput 0.994563
Reading from 30805: heap size 670 MB, throughput 0.66837
Reading from 30805: heap size 742 MB, throughput 0.323803
Reading from 30805: heap size 661 MB, throughput 0.271581
Reading from 30807: heap size 214 MB, throughput 0.996993
Equal recommendation: 3657 MB each
Reading from 30807: heap size 219 MB, throughput 0.997965
Reading from 30807: heap size 220 MB, throughput 0.9953
Reading from 30807: heap size 225 MB, throughput 0.997588
Reading from 30805: heap size 738 MB, throughput 0.0268328
Reading from 30805: heap size 823 MB, throughput 0.192753
Reading from 30805: heap size 825 MB, throughput 0.40036
Reading from 30807: heap size 225 MB, throughput 0.996614
Reading from 30805: heap size 827 MB, throughput 0.431473
Reading from 30807: heap size 230 MB, throughput 0.998307
Reading from 30807: heap size 230 MB, throughput 0.996828
Reading from 30807: heap size 234 MB, throughput 0.998468
Reading from 30807: heap size 234 MB, throughput 0.996825
Reading from 30807: heap size 238 MB, throughput 0.997093
Reading from 30805: heap size 831 MB, throughput 0.037544
Reading from 30807: heap size 238 MB, throughput 0.995451
Reading from 30805: heap size 933 MB, throughput 0.749562
Reading from 30805: heap size 943 MB, throughput 0.696754
Reading from 30807: heap size 241 MB, throughput 0.997429
Reading from 30805: heap size 945 MB, throughput 0.649095
Reading from 30805: heap size 941 MB, throughput 0.676459
Reading from 30805: heap size 739 MB, throughput 0.653936
Reading from 30805: heap size 933 MB, throughput 0.685609
Reading from 30807: heap size 242 MB, throughput 0.99635
Reading from 30805: heap size 743 MB, throughput 0.899191
Reading from 30805: heap size 921 MB, throughput 0.812323
Reading from 30805: heap size 746 MB, throughput 0.849517
Reading from 30807: heap size 247 MB, throughput 0.997117
Reading from 30805: heap size 910 MB, throughput 0.913917
Reading from 30805: heap size 752 MB, throughput 0.693427
Reading from 30807: heap size 247 MB, throughput 0.997057
Reading from 30805: heap size 895 MB, throughput 0.913007
Reading from 30805: heap size 761 MB, throughput 0.744547
Reading from 30805: heap size 880 MB, throughput 0.862097
Reading from 30807: heap size 252 MB, throughput 0.997487
Reading from 30805: heap size 770 MB, throughput 0.83439
Reading from 30805: heap size 883 MB, throughput 0.618773
Reading from 30807: heap size 252 MB, throughput 0.996993
Reading from 30805: heap size 883 MB, throughput 0.67939
Reading from 30805: heap size 877 MB, throughput 0.684962
Reading from 30805: heap size 881 MB, throughput 0.638848
Reading from 30807: heap size 257 MB, throughput 0.998162
Reading from 30805: heap size 875 MB, throughput 0.685701
Reading from 30805: heap size 879 MB, throughput 0.669868
Reading from 30805: heap size 875 MB, throughput 0.708273
Reading from 30807: heap size 257 MB, throughput 0.996047
Reading from 30805: heap size 878 MB, throughput 0.716715
Reading from 30805: heap size 870 MB, throughput 0.732396
Reading from 30807: heap size 262 MB, throughput 0.996559
Reading from 30805: heap size 875 MB, throughput 0.730541
Reading from 30805: heap size 866 MB, throughput 0.7477
Reading from 30807: heap size 262 MB, throughput 0.996063
Reading from 30805: heap size 871 MB, throughput 0.722576
Reading from 30807: heap size 267 MB, throughput 0.997884
Reading from 30805: heap size 866 MB, throughput 0.969546
Reading from 30807: heap size 267 MB, throughput 0.997162
Reading from 30807: heap size 272 MB, throughput 0.997269
Reading from 30807: heap size 272 MB, throughput 0.997423
Reading from 30805: heap size 870 MB, throughput 0.963318
Reading from 30805: heap size 876 MB, throughput 0.785899
Equal recommendation: 3657 MB each
Reading from 30807: heap size 277 MB, throughput 0.997063
Reading from 30805: heap size 879 MB, throughput 0.697629
Reading from 30805: heap size 886 MB, throughput 0.63595
Reading from 30807: heap size 277 MB, throughput 0.997599
Reading from 30805: heap size 890 MB, throughput 0.697027
Reading from 30805: heap size 897 MB, throughput 0.654587
Reading from 30805: heap size 900 MB, throughput 0.664018
Reading from 30807: heap size 282 MB, throughput 0.998056
Reading from 30805: heap size 909 MB, throughput 0.708578
Reading from 30807: heap size 282 MB, throughput 0.993364
Reading from 30805: heap size 909 MB, throughput 0.746466
Reading from 30805: heap size 917 MB, throughput 0.724395
Reading from 30807: heap size 287 MB, throughput 0.995836
Reading from 30805: heap size 918 MB, throughput 0.783043
Reading from 30805: heap size 925 MB, throughput 0.853779
Reading from 30807: heap size 287 MB, throughput 0.996177
Reading from 30805: heap size 926 MB, throughput 0.841454
Reading from 30807: heap size 294 MB, throughput 0.998019
Reading from 30805: heap size 929 MB, throughput 0.70735
Reading from 30805: heap size 930 MB, throughput 0.614134
Reading from 30807: heap size 294 MB, throughput 0.997404
Reading from 30807: heap size 300 MB, throughput 0.99744
Reading from 30807: heap size 300 MB, throughput 0.987852
Reading from 30807: heap size 306 MB, throughput 0.995087
Reading from 30807: heap size 306 MB, throughput 0.997935
Reading from 30805: heap size 917 MB, throughput 0.0438658
Reading from 30805: heap size 1034 MB, throughput 0.44346
Reading from 30805: heap size 1033 MB, throughput 0.510697
Reading from 30807: heap size 315 MB, throughput 0.99659
Reading from 30805: heap size 1038 MB, throughput 0.60843
Reading from 30805: heap size 1038 MB, throughput 0.500556
Reading from 30807: heap size 316 MB, throughput 0.987407
Reading from 30805: heap size 1042 MB, throughput 0.638526
Reading from 30805: heap size 1046 MB, throughput 0.488377
Reading from 30807: heap size 324 MB, throughput 0.995168
Reading from 30805: heap size 1049 MB, throughput 0.517715
Reading from 30805: heap size 1063 MB, throughput 0.539862
Reading from 30807: heap size 324 MB, throughput 0.995878
Reading from 30805: heap size 1063 MB, throughput 0.471177
Reading from 30807: heap size 335 MB, throughput 0.995849
Reading from 30805: heap size 1081 MB, throughput 0.571863
Reading from 30807: heap size 336 MB, throughput 0.996402
Reading from 30807: heap size 345 MB, throughput 0.997456
Reading from 30807: heap size 346 MB, throughput 0.99681
Equal recommendation: 3657 MB each
Reading from 30805: heap size 1082 MB, throughput 0.941352
Reading from 30807: heap size 355 MB, throughput 0.997919
Reading from 30807: heap size 355 MB, throughput 0.997184
Reading from 30807: heap size 362 MB, throughput 0.997182
Reading from 30805: heap size 1104 MB, throughput 0.918945
Reading from 30807: heap size 362 MB, throughput 0.996213
Reading from 30807: heap size 370 MB, throughput 0.997576
Reading from 30807: heap size 370 MB, throughput 0.998307
Reading from 30805: heap size 1105 MB, throughput 0.924585
Reading from 30807: heap size 375 MB, throughput 0.998552
Reading from 30807: heap size 376 MB, throughput 0.997129
Reading from 30807: heap size 382 MB, throughput 0.996878
Reading from 30807: heap size 382 MB, throughput 0.991856
Reading from 30807: heap size 388 MB, throughput 0.997861
Reading from 30805: heap size 1121 MB, throughput 0.586765
Reading from 30807: heap size 389 MB, throughput 0.997148
Reading from 30807: heap size 397 MB, throughput 0.996332
Reading from 30805: heap size 1174 MB, throughput 0.985219
Reading from 30807: heap size 397 MB, throughput 0.996124
Reading from 30807: heap size 407 MB, throughput 0.997243
Reading from 30807: heap size 407 MB, throughput 0.997317
Equal recommendation: 3657 MB each
Reading from 30805: heap size 1181 MB, throughput 0.979788
Reading from 30807: heap size 415 MB, throughput 0.996984
Reading from 30807: heap size 415 MB, throughput 0.997327
Reading from 30807: heap size 424 MB, throughput 0.998058
Reading from 30805: heap size 1190 MB, throughput 0.97549
Reading from 30807: heap size 424 MB, throughput 0.996592
Reading from 30807: heap size 432 MB, throughput 0.998057
Reading from 30807: heap size 432 MB, throughput 0.99628
Reading from 30805: heap size 1204 MB, throughput 0.974637
Reading from 30807: heap size 439 MB, throughput 0.976754
Reading from 30807: heap size 442 MB, throughput 0.996462
Reading from 30805: heap size 1205 MB, throughput 0.964862
Reading from 30807: heap size 451 MB, throughput 0.99738
Reading from 30807: heap size 451 MB, throughput 0.997432
Reading from 30807: heap size 462 MB, throughput 0.997967
Reading from 30805: heap size 1204 MB, throughput 0.964089
Reading from 30807: heap size 462 MB, throughput 0.997201
Reading from 30807: heap size 474 MB, throughput 0.996578
Reading from 30807: heap size 474 MB, throughput 0.997138
Reading from 30805: heap size 1208 MB, throughput 0.961192
Reading from 30807: heap size 485 MB, throughput 0.998009
Equal recommendation: 3657 MB each
Reading from 30807: heap size 486 MB, throughput 0.99732
Reading from 30805: heap size 1198 MB, throughput 0.953501
Reading from 30807: heap size 498 MB, throughput 0.997395
Reading from 30807: heap size 498 MB, throughput 0.99728
Reading from 30807: heap size 510 MB, throughput 0.99751
Reading from 30805: heap size 1204 MB, throughput 0.94997
Reading from 30807: heap size 510 MB, throughput 0.997651
Reading from 30807: heap size 523 MB, throughput 0.997177
Reading from 30807: heap size 523 MB, throughput 0.997111
Reading from 30805: heap size 1200 MB, throughput 0.955011
Reading from 30807: heap size 535 MB, throughput 0.997978
Reading from 30807: heap size 535 MB, throughput 0.997848
Reading from 30805: heap size 1203 MB, throughput 0.944755
Reading from 30807: heap size 548 MB, throughput 0.998232
Reading from 30807: heap size 548 MB, throughput 0.99735
Reading from 30805: heap size 1207 MB, throughput 0.941557
Reading from 30807: heap size 560 MB, throughput 0.998164
Equal recommendation: 3657 MB each
Reading from 30807: heap size 560 MB, throughput 0.998066
Reading from 30807: heap size 570 MB, throughput 0.997901
Reading from 30805: heap size 1210 MB, throughput 0.941428
Reading from 30807: heap size 571 MB, throughput 0.997784
Reading from 30807: heap size 582 MB, throughput 0.997929
Reading from 30805: heap size 1216 MB, throughput 0.945568
Reading from 30807: heap size 582 MB, throughput 0.997955
Reading from 30807: heap size 594 MB, throughput 0.998051
Reading from 30807: heap size 594 MB, throughput 0.997098
Reading from 30805: heap size 1222 MB, throughput 0.943966
Reading from 30807: heap size 606 MB, throughput 0.998226
Reading from 30807: heap size 606 MB, throughput 0.99793
Reading from 30805: heap size 1228 MB, throughput 0.933573
Reading from 30807: heap size 619 MB, throughput 0.99851
Reading from 30807: heap size 619 MB, throughput 0.998369
Equal recommendation: 3657 MB each
Reading from 30805: heap size 1236 MB, throughput 0.936017
Reading from 30807: heap size 630 MB, throughput 0.998306
Reading from 30807: heap size 630 MB, throughput 0.997817
Reading from 30805: heap size 1246 MB, throughput 0.940713
Reading from 30807: heap size 642 MB, throughput 0.998753
Reading from 30807: heap size 642 MB, throughput 0.998177
Reading from 30807: heap size 653 MB, throughput 0.998042
Reading from 30805: heap size 1251 MB, throughput 0.926856
Reading from 30807: heap size 653 MB, throughput 0.998442
Reading from 30807: heap size 664 MB, throughput 0.998195
Reading from 30805: heap size 1260 MB, throughput 0.929329
Reading from 30807: heap size 664 MB, throughput 0.998321
Reading from 30807: heap size 675 MB, throughput 0.998732
Reading from 30805: heap size 1264 MB, throughput 0.935687
Reading from 30807: heap size 675 MB, throughput 0.998598
Equal recommendation: 3657 MB each
Reading from 30807: heap size 686 MB, throughput 0.998154
Reading from 30805: heap size 1273 MB, throughput 0.930864
Reading from 30807: heap size 686 MB, throughput 0.99843
Reading from 30807: heap size 697 MB, throughput 0.998749
Reading from 30807: heap size 697 MB, throughput 0.997177
Reading from 30805: heap size 1275 MB, throughput 0.943201
Reading from 30807: heap size 707 MB, throughput 0.998918
Reading from 30807: heap size 707 MB, throughput 0.998489
Reading from 30805: heap size 1285 MB, throughput 0.9478
Reading from 30807: heap size 719 MB, throughput 0.998497
Reading from 30807: heap size 719 MB, throughput 0.99874
Reading from 30805: heap size 1286 MB, throughput 0.938541
Reading from 30807: heap size 730 MB, throughput 0.998646
Equal recommendation: 3657 MB each
Reading from 30807: heap size 730 MB, throughput 0.998379
Reading from 30805: heap size 1296 MB, throughput 0.937979
Reading from 30807: heap size 741 MB, throughput 0.998704
Reading from 30807: heap size 741 MB, throughput 0.998382
Reading from 30805: heap size 1296 MB, throughput 0.93687
Reading from 30807: heap size 752 MB, throughput 0.998605
Reading from 30807: heap size 752 MB, throughput 0.998699
Reading from 30807: heap size 763 MB, throughput 0.998293
Reading from 30805: heap size 1305 MB, throughput 0.939963
Reading from 30807: heap size 763 MB, throughput 0.998901
Reading from 30807: heap size 774 MB, throughput 0.998642
Reading from 30805: heap size 1305 MB, throughput 0.945142
Equal recommendation: 3657 MB each
Reading from 30807: heap size 774 MB, throughput 0.998468
Reading from 30807: heap size 785 MB, throughput 0.99854
Reading from 30807: heap size 785 MB, throughput 0.997717
Reading from 30807: heap size 796 MB, throughput 0.998774
Reading from 30805: heap size 1314 MB, throughput 0.502549
Reading from 30807: heap size 796 MB, throughput 0.998843
Reading from 30807: heap size 808 MB, throughput 0.998312
Reading from 30805: heap size 1425 MB, throughput 0.930592
Reading from 30807: heap size 809 MB, throughput 0.998766
Reading from 30807: heap size 820 MB, throughput 0.998789
Equal recommendation: 3657 MB each
Reading from 30805: heap size 1426 MB, throughput 0.971287
Reading from 30807: heap size 820 MB, throughput 0.998302
Reading from 30807: heap size 832 MB, throughput 0.998939
Reading from 30805: heap size 1434 MB, throughput 0.968537
Reading from 30807: heap size 832 MB, throughput 0.998797
Reading from 30807: heap size 843 MB, throughput 0.998774
Reading from 30805: heap size 1446 MB, throughput 0.967064
Reading from 30807: heap size 843 MB, throughput 0.998564
Reading from 30807: heap size 854 MB, throughput 0.998945
Reading from 30805: heap size 1447 MB, throughput 0.963644
Reading from 30807: heap size 854 MB, throughput 0.998721
Equal recommendation: 3657 MB each
Reading from 30807: heap size 865 MB, throughput 0.998793
Reading from 30805: heap size 1443 MB, throughput 0.96752
Reading from 30807: heap size 865 MB, throughput 0.998816
Reading from 30807: heap size 876 MB, throughput 0.998771
Reading from 30807: heap size 876 MB, throughput 0.998738
Reading from 30805: heap size 1448 MB, throughput 0.956894
Reading from 30807: heap size 887 MB, throughput 0.998662
Reading from 30807: heap size 887 MB, throughput 0.998847
Reading from 30805: heap size 1435 MB, throughput 0.963458
Reading from 30807: heap size 898 MB, throughput 0.998904
Reading from 30807: heap size 898 MB, throughput 0.998739
Equal recommendation: 3657 MB each
Reading from 30807: heap size 909 MB, throughput 0.998816
Reading from 30807: heap size 909 MB, throughput 0.998933
Reading from 30807: heap size 920 MB, throughput 0.998732
Reading from 30807: heap size 920 MB, throughput 0.998872
Reading from 30807: heap size 931 MB, throughput 0.999011
Reading from 30807: heap size 931 MB, throughput 0.999071
Reading from 30807: heap size 942 MB, throughput 0.998604
Equal recommendation: 3657 MB each
Reading from 30807: heap size 942 MB, throughput 0.998792
Reading from 30807: heap size 953 MB, throughput 0.998955
Reading from 30807: heap size 953 MB, throughput 0.998888
Reading from 30807: heap size 964 MB, throughput 0.998813
Reading from 30807: heap size 964 MB, throughput 0.998989
Reading from 30807: heap size 975 MB, throughput 0.998694
Reading from 30805: heap size 1443 MB, throughput 0.992024
Equal recommendation: 3657 MB each
Reading from 30807: heap size 975 MB, throughput 0.999082
Reading from 30807: heap size 986 MB, throughput 0.998808
Reading from 30807: heap size 986 MB, throughput 0.999034
Reading from 30807: heap size 997 MB, throughput 0.999082
Reading from 30807: heap size 998 MB, throughput 0.999051
Reading from 30805: heap size 1425 MB, throughput 0.97989
Reading from 30805: heap size 1448 MB, throughput 0.771596
Reading from 30805: heap size 1456 MB, throughput 0.649816
Reading from 30805: heap size 1485 MB, throughput 0.658371
Reading from 30807: heap size 1008 MB, throughput 0.999106
Reading from 30805: heap size 1525 MB, throughput 0.671486
Reading from 30805: heap size 1550 MB, throughput 0.614848
Reading from 30805: heap size 1584 MB, throughput 0.667468
Reading from 30807: heap size 1008 MB, throughput 0.998924
Equal recommendation: 3657 MB each
Reading from 30805: heap size 1599 MB, throughput 0.801188
Reading from 30807: heap size 1018 MB, throughput 0.998939
Reading from 30807: heap size 1018 MB, throughput 0.998996
Reading from 30805: heap size 1637 MB, throughput 0.943264
Reading from 30807: heap size 1029 MB, throughput 0.999011
Reading from 30805: heap size 1643 MB, throughput 0.93555
Reading from 30807: heap size 1029 MB, throughput 0.998904
Reading from 30807: heap size 1040 MB, throughput 0.999108
Reading from 30805: heap size 1675 MB, throughput 0.944377
Reading from 30807: heap size 1040 MB, throughput 0.998915
Equal recommendation: 3657 MB each
Reading from 30807: heap size 1051 MB, throughput 0.998961
Reading from 30805: heap size 1678 MB, throughput 0.947115
Reading from 30807: heap size 1051 MB, throughput 0.999147
Reading from 30807: heap size 1062 MB, throughput 0.999077
Reading from 30805: heap size 1704 MB, throughput 0.944819
Reading from 30807: heap size 1062 MB, throughput 0.998964
Reading from 30807: heap size 1073 MB, throughput 0.998888
Reading from 30805: heap size 1706 MB, throughput 0.94759
Reading from 30807: heap size 1073 MB, throughput 0.999136
Equal recommendation: 3657 MB each
Reading from 30807: heap size 1084 MB, throughput 0.998988
Reading from 30807: heap size 1084 MB, throughput 0.999188
Reading from 30805: heap size 1726 MB, throughput 0.722016
Reading from 30807: heap size 1095 MB, throughput 0.999184
Reading from 30807: heap size 1095 MB, throughput 0.999083
Reading from 30805: heap size 1718 MB, throughput 0.987344
Reading from 30807: heap size 1105 MB, throughput 0.999092
Reading from 30807: heap size 1105 MB, throughput 0.999159
Equal recommendation: 3657 MB each
Reading from 30805: heap size 1705 MB, throughput 0.981092
Reading from 30807: heap size 1116 MB, throughput 0.999114
Reading from 30807: heap size 1116 MB, throughput 0.999195
Reading from 30805: heap size 1722 MB, throughput 0.977749
Reading from 30807: heap size 1126 MB, throughput 0.998967
Reading from 30807: heap size 1126 MB, throughput 0.999241
Reading from 30805: heap size 1746 MB, throughput 0.971715
Reading from 30807: heap size 1136 MB, throughput 0.999125
Equal recommendation: 3657 MB each
Reading from 30807: heap size 1136 MB, throughput 0.998929
Reading from 30805: heap size 1747 MB, throughput 0.972475
Reading from 30807: heap size 1147 MB, throughput 0.999264
Reading from 30807: heap size 1147 MB, throughput 0.998994
Reading from 30805: heap size 1736 MB, throughput 0.975819
Reading from 30807: heap size 1157 MB, throughput 0.999231
Reading from 30807: heap size 1157 MB, throughput 0.999116
Reading from 30805: heap size 1746 MB, throughput 0.963192
Reading from 30807: heap size 1168 MB, throughput 0.999216
Equal recommendation: 3657 MB each
Reading from 30807: heap size 1168 MB, throughput 0.999114
Reading from 30805: heap size 1717 MB, throughput 0.966931
Reading from 30807: heap size 1178 MB, throughput 0.998915
Reading from 30807: heap size 1178 MB, throughput 0.999274
Reading from 30805: heap size 1495 MB, throughput 0.963922
Reading from 30807: heap size 1189 MB, throughput 0.999237
Reading from 30807: heap size 1190 MB, throughput 0.999122
Equal recommendation: 3657 MB each
Reading from 30807: heap size 1200 MB, throughput 0.99914
Reading from 30805: heap size 1692 MB, throughput 0.959116
Reading from 30807: heap size 1200 MB, throughput 0.999195
Reading from 30807: heap size 1210 MB, throughput 0.999251
Reading from 30805: heap size 1534 MB, throughput 0.963103
Reading from 30807: heap size 1210 MB, throughput 0.999112
Reading from 30807: heap size 1221 MB, throughput 0.99924
Reading from 30805: heap size 1687 MB, throughput 0.956854
Reading from 30807: heap size 1221 MB, throughput 0.998902
Equal recommendation: 3657 MB each
Reading from 30807: heap size 1231 MB, throughput 0.999001
Reading from 30805: heap size 1698 MB, throughput 0.957099
Reading from 30807: heap size 1231 MB, throughput 0.999142
Reading from 30807: heap size 1243 MB, throughput 0.999366
Reading from 30805: heap size 1708 MB, throughput 0.956069
Reading from 30807: heap size 1243 MB, throughput 0.99926
Reading from 30807: heap size 1254 MB, throughput 0.999182
Equal recommendation: 3657 MB each
Reading from 30807: heap size 1254 MB, throughput 0.999277
Reading from 30805: heap size 1708 MB, throughput 0.948605
Reading from 30807: heap size 1264 MB, throughput 0.99915
Reading from 30807: heap size 1264 MB, throughput 0.999224
Reading from 30805: heap size 1721 MB, throughput 0.952423
Reading from 30807: heap size 1275 MB, throughput 0.999348
Reading from 30807: heap size 1275 MB, throughput 0.999067
Equal recommendation: 3657 MB each
Reading from 30805: heap size 1728 MB, throughput 0.946908
Reading from 30807: heap size 1285 MB, throughput 0.999059
Reading from 30807: heap size 1285 MB, throughput 0.999287
Reading from 30805: heap size 1748 MB, throughput 0.946628
Reading from 30807: heap size 1296 MB, throughput 0.999018
Reading from 30807: heap size 1296 MB, throughput 0.999128
Reading from 30807: heap size 1307 MB, throughput 0.999261
Equal recommendation: 3657 MB each
Reading from 30807: heap size 1307 MB, throughput 0.999112
Reading from 30807: heap size 1319 MB, throughput 0.999233
Reading from 30807: heap size 1319 MB, throughput 0.999134
Reading from 30807: heap size 1330 MB, throughput 0.997943
Reading from 30805: heap size 1752 MB, throughput 0.977207
Reading from 30807: heap size 1330 MB, throughput 0.999119
Equal recommendation: 3657 MB each
Reading from 30807: heap size 1345 MB, throughput 0.998871
Reading from 30807: heap size 1346 MB, throughput 0.999217
Reading from 30807: heap size 1361 MB, throughput 0.999106
Reading from 30807: heap size 1361 MB, throughput 0.999242
Equal recommendation: 3657 MB each
Reading from 30807: heap size 1374 MB, throughput 0.999316
Reading from 30807: heap size 1375 MB, throughput 0.999292
Reading from 30807: heap size 1387 MB, throughput 0.999243
Reading from 30807: heap size 1387 MB, throughput 0.99921
Reading from 30805: heap size 1760 MB, throughput 0.990666
Reading from 30805: heap size 1776 MB, throughput 0.82959
Reading from 30805: heap size 1767 MB, throughput 0.729006
Reading from 30807: heap size 1399 MB, throughput 0.998807
Reading from 30805: heap size 1789 MB, throughput 0.755405
Reading from 30805: heap size 1833 MB, throughput 0.797447
Client 30805 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 30807: heap size 1399 MB, throughput 0.999209
Reading from 30807: heap size 1412 MB, throughput 0.999056
Reading from 30807: heap size 1412 MB, throughput 0.99885
Reading from 30807: heap size 1425 MB, throughput 0.999036
Reading from 30807: heap size 1425 MB, throughput 0.998956
Recommendation: one client; give it all the memory
Reading from 30807: heap size 1439 MB, throughput 0.998999
Reading from 30807: heap size 1439 MB, throughput 0.99899
Client 30807 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
