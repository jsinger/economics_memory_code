	Command being timed: "cgexec -g memory:economem /home/callum/economics_memory_code/dynamic_sizing/hotspot8/openjdk8/jdk8/build/linux-x86_64-normal-server-release/jdk/bin/java -XX:+EconomemSimpleThroughput -XX:-EconomemVengerovThroughput -XX:EconomemMinHeap=22 -Xms10m -Xmx88m -XX:+DisableExplicitGC -XX:+UseAdaptiveSizePolicyWithSystemGC -jar /home/callum/economics_memory_code/dynamic_sizing/haskell_common/dacapo-9.12-bach.jar --no-pre-iteration-gc --scratch-directory /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub45_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/scratch2 -t 1 -n 28 -s large jython"
	User time (seconds): 593.03
	System time (seconds): 17.05
	Percent of CPU this job got: 38%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 26:22.96
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 145812
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 155491
	Minor (reclaiming a frame) page faults: 800076
	Voluntary context switches: 545321
	Involuntary context switches: 177447
	Swaps: 0
	File system inputs: 4811712
	File system outputs: 105040
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
