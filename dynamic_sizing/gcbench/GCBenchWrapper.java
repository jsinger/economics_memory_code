// The existing gcbench classes only do one iteration; here, we do many.
public class GCBenchWrapper
{
    public static void main(String[] args)
    {
        int iters = Integer.parseInt(args[0]);
        int threads = Integer.parseInt(args[1]);

        System.out.println("GCBench: " + Integer.toString(threads) + " threads, " + Integer.toString(iters) + " iters\n");
        for (int i = 0; i < iters; ++i)
        {
            System.out.println("\n\n##########\nGCBench iteration " + Integer.toString(i + 1) + "\n##########\n");
            GCBenchMT gcb = new GCBenchMT(threads, false);
            gcb.start();
        }
    }
}
