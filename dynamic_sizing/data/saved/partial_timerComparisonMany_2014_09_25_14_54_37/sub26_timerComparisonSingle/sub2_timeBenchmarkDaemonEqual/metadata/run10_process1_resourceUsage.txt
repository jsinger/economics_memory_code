	Command being timed: "cgexec -g memory:economem /home/callum/economics_memory_code/dynamic_sizing/hotspot8/openjdk8/jdk8/build/linux-x86_64-normal-server-release/jdk/bin/java -XX:+EconomemSimpleThroughput -XX:-EconomemVengerovThroughput -XX:EconomemMinHeap=1223 -Xms10m -Xmx2506m -XX:+DisableExplicitGC -XX:+UseAdaptiveSizePolicyWithSystemGC -jar /home/callum/economics_memory_code/dynamic_sizing/haskell_common/dacapo-9.12-bach.jar --no-pre-iteration-gc --scratch-directory /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub26_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/scratch1 -t 2 -n 2 -s huge h2"
	User time (seconds): 630.02
	System time (seconds): 7.38
	Percent of CPU this job got: 113%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 9:19.32
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 1908644
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 27999
	Minor (reclaiming a frame) page faults: 518189
	Voluntary context switches: 490670
	Involuntary context switches: 135900
	Swaps: 0
	File system inputs: 1799296
	File system outputs: 16184
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
