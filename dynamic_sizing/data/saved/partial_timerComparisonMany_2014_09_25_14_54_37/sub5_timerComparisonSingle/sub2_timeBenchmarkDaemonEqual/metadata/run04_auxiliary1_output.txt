economemd
    total memory: 7435 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub5_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 9385: heap size 9 MB, throughput 0.988815
Clients: 1
Reading from 9386: heap size 9 MB, throughput 0.988538
Clients: 2
Client 9386 has a minimum heap size of 276 MB
Client 9385 has a minimum heap size of 1211 MB
Reading from 9385: heap size 9 MB, throughput 0.978822
Reading from 9386: heap size 9 MB, throughput 0.977525
Reading from 9386: heap size 11 MB, throughput 0.971964
Reading from 9385: heap size 11 MB, throughput 0.975887
Reading from 9385: heap size 11 MB, throughput 0.968023
Reading from 9386: heap size 11 MB, throughput 0.982584
Reading from 9386: heap size 15 MB, throughput 0.836544
Reading from 9385: heap size 15 MB, throughput 0.757337
Reading from 9386: heap size 19 MB, throughput 0.952376
Reading from 9385: heap size 19 MB, throughput 0.96892
Reading from 9386: heap size 23 MB, throughput 0.953306
Reading from 9386: heap size 27 MB, throughput 0.912581
Reading from 9385: heap size 23 MB, throughput 0.87981
Reading from 9385: heap size 27 MB, throughput 0.901804
Reading from 9386: heap size 27 MB, throughput 0.336411
Reading from 9386: heap size 38 MB, throughput 0.709063
Reading from 9385: heap size 27 MB, throughput 0.42755
Reading from 9385: heap size 38 MB, throughput 0.853815
Reading from 9386: heap size 42 MB, throughput 0.971117
Reading from 9385: heap size 42 MB, throughput 0.857518
Reading from 9386: heap size 43 MB, throughput 0.853336
Reading from 9385: heap size 43 MB, throughput 0.775196
Reading from 9386: heap size 47 MB, throughput 0.211355
Reading from 9385: heap size 46 MB, throughput 0.223283
Reading from 9386: heap size 61 MB, throughput 0.836723
Reading from 9385: heap size 62 MB, throughput 0.785518
Reading from 9386: heap size 68 MB, throughput 0.885654
Reading from 9385: heap size 68 MB, throughput 0.86171
Reading from 9386: heap size 70 MB, throughput 0.739254
Reading from 9385: heap size 69 MB, throughput 0.789775
Reading from 9385: heap size 74 MB, throughput 0.866371
Reading from 9386: heap size 74 MB, throughput 0.145074
Reading from 9386: heap size 96 MB, throughput 0.660936
Reading from 9386: heap size 100 MB, throughput 0.656587
Reading from 9385: heap size 76 MB, throughput 0.120522
Reading from 9385: heap size 107 MB, throughput 0.645119
Reading from 9385: heap size 108 MB, throughput 0.738303
Reading from 9385: heap size 111 MB, throughput 0.695288
Reading from 9386: heap size 103 MB, throughput 0.208202
Reading from 9386: heap size 134 MB, throughput 0.724693
Reading from 9386: heap size 136 MB, throughput 0.773469
Reading from 9386: heap size 138 MB, throughput 0.665618
Reading from 9385: heap size 113 MB, throughput 0.225248
Reading from 9386: heap size 142 MB, throughput 0.749557
Reading from 9385: heap size 145 MB, throughput 0.65594
Reading from 9386: heap size 145 MB, throughput 0.578152
Reading from 9386: heap size 152 MB, throughput 0.551134
Reading from 9385: heap size 148 MB, throughput 0.60045
Reading from 9386: heap size 156 MB, throughput 0.686917
Reading from 9385: heap size 152 MB, throughput 0.815143
Reading from 9386: heap size 164 MB, throughput 0.577959
Reading from 9385: heap size 156 MB, throughput 0.742848
Reading from 9386: heap size 171 MB, throughput 0.494837
Reading from 9385: heap size 159 MB, throughput 0.526177
Reading from 9386: heap size 177 MB, throughput 0.095239
Reading from 9385: heap size 166 MB, throughput 0.136272
Reading from 9386: heap size 211 MB, throughput 0.43718
Reading from 9385: heap size 202 MB, throughput 0.600009
Reading from 9385: heap size 207 MB, throughput 0.126022
Reading from 9385: heap size 247 MB, throughput 0.616113
Reading from 9385: heap size 248 MB, throughput 0.747422
Reading from 9386: heap size 215 MB, throughput 0.113716
Reading from 9385: heap size 249 MB, throughput 0.709278
Reading from 9385: heap size 250 MB, throughput 0.674879
Reading from 9385: heap size 252 MB, throughput 0.470768
Reading from 9386: heap size 246 MB, throughput 0.88276
Reading from 9385: heap size 258 MB, throughput 0.556318
Reading from 9385: heap size 261 MB, throughput 0.447208
Reading from 9386: heap size 249 MB, throughput 0.937588
Reading from 9386: heap size 246 MB, throughput 0.614122
Reading from 9385: heap size 271 MB, throughput 0.513138
Reading from 9386: heap size 248 MB, throughput 0.561572
Reading from 9385: heap size 276 MB, throughput 0.557674
Reading from 9386: heap size 248 MB, throughput 0.751636
Reading from 9386: heap size 249 MB, throughput 0.435917
Reading from 9386: heap size 253 MB, throughput 0.745905
Reading from 9386: heap size 253 MB, throughput 0.690176
Reading from 9385: heap size 285 MB, throughput 0.110638
Reading from 9386: heap size 257 MB, throughput 0.86185
Reading from 9385: heap size 341 MB, throughput 0.559797
Reading from 9386: heap size 257 MB, throughput 0.754828
Reading from 9386: heap size 260 MB, throughput 0.577293
Reading from 9385: heap size 344 MB, throughput 0.11306
Reading from 9385: heap size 390 MB, throughput 0.41843
Reading from 9386: heap size 261 MB, throughput 0.132215
Reading from 9385: heap size 393 MB, throughput 0.688621
Reading from 9386: heap size 298 MB, throughput 0.648708
Reading from 9385: heap size 384 MB, throughput 0.706072
Reading from 9386: heap size 299 MB, throughput 0.828265
Reading from 9385: heap size 389 MB, throughput 0.59372
Reading from 9385: heap size 389 MB, throughput 0.601414
Reading from 9385: heap size 392 MB, throughput 0.562845
Reading from 9385: heap size 395 MB, throughput 0.510738
Reading from 9386: heap size 304 MB, throughput 0.947072
Reading from 9385: heap size 403 MB, throughput 0.531391
Reading from 9386: heap size 304 MB, throughput 0.692938
Reading from 9386: heap size 306 MB, throughput 0.715893
Reading from 9385: heap size 406 MB, throughput 0.421807
Reading from 9386: heap size 306 MB, throughput 0.84364
Reading from 9385: heap size 417 MB, throughput 0.522939
Reading from 9386: heap size 307 MB, throughput 0.857386
Reading from 9385: heap size 424 MB, throughput 0.327174
Reading from 9386: heap size 309 MB, throughput 0.909556
Reading from 9386: heap size 311 MB, throughput 0.749152
Reading from 9386: heap size 312 MB, throughput 0.559062
Equal recommendation: 3717 MB each
Reading from 9386: heap size 318 MB, throughput 0.446449
Reading from 9385: heap size 431 MB, throughput 0.0821245
Reading from 9385: heap size 490 MB, throughput 0.404355
Reading from 9385: heap size 494 MB, throughput 0.452213
Reading from 9386: heap size 318 MB, throughput 0.0986935
Reading from 9386: heap size 363 MB, throughput 0.553168
Reading from 9386: heap size 364 MB, throughput 0.692437
Reading from 9385: heap size 490 MB, throughput 0.0660909
Reading from 9385: heap size 485 MB, throughput 0.417461
Reading from 9385: heap size 545 MB, throughput 0.534538
Reading from 9385: heap size 547 MB, throughput 0.546249
Reading from 9385: heap size 538 MB, throughput 0.556888
Reading from 9385: heap size 543 MB, throughput 0.549927
Reading from 9386: heap size 375 MB, throughput 0.971871
Reading from 9385: heap size 541 MB, throughput 0.521439
Reading from 9385: heap size 543 MB, throughput 0.103704
Reading from 9385: heap size 610 MB, throughput 0.473582
Reading from 9385: heap size 612 MB, throughput 0.574372
Reading from 9386: heap size 375 MB, throughput 0.984301
Reading from 9385: heap size 613 MB, throughput 0.593438
Reading from 9385: heap size 614 MB, throughput 0.550676
Reading from 9385: heap size 618 MB, throughput 0.499487
Reading from 9385: heap size 625 MB, throughput 0.490311
Reading from 9386: heap size 379 MB, throughput 0.958437
Reading from 9385: heap size 631 MB, throughput 0.864773
Reading from 9385: heap size 640 MB, throughput 0.802709
Reading from 9385: heap size 640 MB, throughput 0.737068
Reading from 9386: heap size 381 MB, throughput 0.954747
Reading from 9385: heap size 659 MB, throughput 0.73157
Reading from 9386: heap size 383 MB, throughput 0.969425
Reading from 9385: heap size 664 MB, throughput 0.0327289
Reading from 9385: heap size 744 MB, throughput 0.283907
Equal recommendation: 3717 MB each
Reading from 9385: heap size 753 MB, throughput 0.667838
Reading from 9386: heap size 385 MB, throughput 0.948654
Reading from 9385: heap size 754 MB, throughput 0.508241
Reading from 9385: heap size 748 MB, throughput 0.344889
Reading from 9385: heap size 668 MB, throughput 0.299969
Reading from 9385: heap size 736 MB, throughput 0.0291956
Reading from 9386: heap size 384 MB, throughput 0.962186
Reading from 9385: heap size 821 MB, throughput 0.181586
Reading from 9385: heap size 824 MB, throughput 0.661404
Reading from 9385: heap size 825 MB, throughput 0.893792
Reading from 9385: heap size 832 MB, throughput 0.696946
Reading from 9385: heap size 833 MB, throughput 0.637451
Reading from 9386: heap size 387 MB, throughput 0.959029
Reading from 9385: heap size 833 MB, throughput 0.0307362
Reading from 9385: heap size 925 MB, throughput 0.42755
Reading from 9385: heap size 928 MB, throughput 0.910237
Reading from 9386: heap size 390 MB, throughput 0.969678
Reading from 9385: heap size 931 MB, throughput 0.832334
Reading from 9385: heap size 929 MB, throughput 0.93039
Reading from 9385: heap size 747 MB, throughput 0.814319
Reading from 9385: heap size 920 MB, throughput 0.94326
Reading from 9385: heap size 756 MB, throughput 0.826924
Reading from 9385: heap size 907 MB, throughput 0.901934
Reading from 9385: heap size 785 MB, throughput 0.820085
Reading from 9385: heap size 894 MB, throughput 0.722668
Reading from 9385: heap size 806 MB, throughput 0.692487
Reading from 9385: heap size 887 MB, throughput 0.75644
Reading from 9385: heap size 796 MB, throughput 0.804751
Reading from 9385: heap size 875 MB, throughput 0.709169
Reading from 9386: heap size 391 MB, throughput 0.967022
Reading from 9385: heap size 801 MB, throughput 0.776844
Reading from 9385: heap size 869 MB, throughput 0.788814
Reading from 9385: heap size 875 MB, throughput 0.791474
Reading from 9385: heap size 866 MB, throughput 0.811011
Reading from 9385: heap size 871 MB, throughput 0.808329
Reading from 9385: heap size 865 MB, throughput 0.824358
Reading from 9386: heap size 392 MB, throughput 0.975638
Reading from 9385: heap size 869 MB, throughput 0.980457
Reading from 9385: heap size 870 MB, throughput 0.957996
Reading from 9385: heap size 871 MB, throughput 0.760114
Equal recommendation: 3717 MB each
Reading from 9385: heap size 876 MB, throughput 0.723648
Reading from 9386: heap size 394 MB, throughput 0.974294
Reading from 9385: heap size 878 MB, throughput 0.784253
Reading from 9385: heap size 885 MB, throughput 0.763012
Reading from 9385: heap size 886 MB, throughput 0.739384
Reading from 9385: heap size 892 MB, throughput 0.823703
Reading from 9385: heap size 893 MB, throughput 0.784533
Reading from 9385: heap size 899 MB, throughput 0.797692
Reading from 9385: heap size 900 MB, throughput 0.893999
Reading from 9386: heap size 396 MB, throughput 0.958582
Reading from 9385: heap size 902 MB, throughput 0.864169
Reading from 9385: heap size 905 MB, throughput 0.904743
Reading from 9385: heap size 906 MB, throughput 0.777492
Reading from 9386: heap size 397 MB, throughput 0.983072
Reading from 9385: heap size 908 MB, throughput 0.072833
Reading from 9386: heap size 401 MB, throughput 0.902055
Reading from 9386: heap size 401 MB, throughput 0.657265
Reading from 9385: heap size 984 MB, throughput 0.421787
Reading from 9386: heap size 408 MB, throughput 0.748954
Reading from 9385: heap size 1006 MB, throughput 0.609949
Reading from 9385: heap size 1013 MB, throughput 0.621061
Reading from 9386: heap size 410 MB, throughput 0.921663
Reading from 9385: heap size 1013 MB, throughput 0.603822
Reading from 9385: heap size 1017 MB, throughput 0.6943
Reading from 9385: heap size 1020 MB, throughput 0.688241
Reading from 9385: heap size 1023 MB, throughput 0.68362
Reading from 9385: heap size 1026 MB, throughput 0.579904
Reading from 9385: heap size 1031 MB, throughput 0.69478
Reading from 9385: heap size 1034 MB, throughput 0.693215
Reading from 9386: heap size 419 MB, throughput 0.988035
Reading from 9385: heap size 1046 MB, throughput 0.777599
Reading from 9385: heap size 1048 MB, throughput 0.962993
Reading from 9386: heap size 420 MB, throughput 0.983038
Equal recommendation: 3717 MB each
Reading from 9385: heap size 1062 MB, throughput 0.934954
Reading from 9386: heap size 424 MB, throughput 0.984302
Reading from 9385: heap size 1064 MB, throughput 0.924023
Reading from 9386: heap size 426 MB, throughput 0.985405
Reading from 9386: heap size 425 MB, throughput 0.971246
Reading from 9385: heap size 1076 MB, throughput 0.613442
Reading from 9385: heap size 1148 MB, throughput 0.988728
Reading from 9386: heap size 427 MB, throughput 0.984052
Reading from 9385: heap size 1152 MB, throughput 0.985806
Reading from 9386: heap size 424 MB, throughput 0.982243
Equal recommendation: 3717 MB each
Reading from 9385: heap size 1159 MB, throughput 0.981277
Reading from 9386: heap size 427 MB, throughput 0.76078
Reading from 9385: heap size 1170 MB, throughput 0.976513
Reading from 9386: heap size 455 MB, throughput 0.994733
Reading from 9385: heap size 1171 MB, throughput 0.974949
Reading from 9385: heap size 1168 MB, throughput 0.972481
Reading from 9386: heap size 456 MB, throughput 0.991171
Reading from 9386: heap size 462 MB, throughput 0.961485
Reading from 9386: heap size 462 MB, throughput 0.865677
Reading from 9386: heap size 466 MB, throughput 0.869566
Reading from 9385: heap size 1172 MB, throughput 0.96994
Reading from 9386: heap size 467 MB, throughput 0.985515
Reading from 9385: heap size 1162 MB, throughput 0.970827
Equal recommendation: 3717 MB each
Reading from 9386: heap size 473 MB, throughput 0.990334
Reading from 9385: heap size 1168 MB, throughput 0.965157
Reading from 9386: heap size 474 MB, throughput 0.989394
Reading from 9385: heap size 1167 MB, throughput 0.966379
Reading from 9385: heap size 1169 MB, throughput 0.96383
Reading from 9386: heap size 475 MB, throughput 0.986622
Reading from 9385: heap size 1173 MB, throughput 0.956886
Reading from 9386: heap size 477 MB, throughput 0.986673
Reading from 9385: heap size 1176 MB, throughput 0.956032
Reading from 9386: heap size 474 MB, throughput 0.986939
Equal recommendation: 3717 MB each
Reading from 9385: heap size 1180 MB, throughput 0.953698
Reading from 9386: heap size 477 MB, throughput 0.987419
Reading from 9385: heap size 1187 MB, throughput 0.947753
Reading from 9386: heap size 479 MB, throughput 0.982584
Reading from 9385: heap size 1192 MB, throughput 0.947532
Reading from 9385: heap size 1200 MB, throughput 0.947943
Reading from 9386: heap size 479 MB, throughput 0.992248
Reading from 9386: heap size 481 MB, throughput 0.974775
Reading from 9386: heap size 482 MB, throughput 0.860761
Reading from 9386: heap size 486 MB, throughput 0.870609
Reading from 9385: heap size 1209 MB, throughput 0.946538
Reading from 9386: heap size 488 MB, throughput 0.98911
Equal recommendation: 3717 MB each
Reading from 9385: heap size 1214 MB, throughput 0.959682
Reading from 9386: heap size 495 MB, throughput 0.991636
Reading from 9385: heap size 1221 MB, throughput 0.949652
Reading from 9386: heap size 496 MB, throughput 0.989862
Reading from 9385: heap size 1225 MB, throughput 0.957476
Reading from 9385: heap size 1232 MB, throughput 0.961328
Reading from 9386: heap size 497 MB, throughput 0.99
Reading from 9385: heap size 1234 MB, throughput 0.94052
Reading from 9386: heap size 500 MB, throughput 0.988256
Equal recommendation: 3717 MB each
Reading from 9385: heap size 1241 MB, throughput 0.951744
Reading from 9386: heap size 498 MB, throughput 0.987711
Reading from 9385: heap size 1241 MB, throughput 0.948949
Reading from 9386: heap size 500 MB, throughput 0.986402
Reading from 9385: heap size 1249 MB, throughput 0.953315
Reading from 9386: heap size 502 MB, throughput 0.992004
Reading from 9385: heap size 1249 MB, throughput 0.947257
Reading from 9386: heap size 503 MB, throughput 0.972459
Reading from 9386: heap size 503 MB, throughput 0.888623
Reading from 9386: heap size 505 MB, throughput 0.970128
Equal recommendation: 3717 MB each
Reading from 9385: heap size 1255 MB, throughput 0.955119
Reading from 9386: heap size 513 MB, throughput 0.991746
Reading from 9385: heap size 1255 MB, throughput 0.950499
Reading from 9386: heap size 513 MB, throughput 0.99165
Reading from 9385: heap size 1261 MB, throughput 0.950329
Reading from 9386: heap size 514 MB, throughput 0.991041
Reading from 9385: heap size 1262 MB, throughput 0.524651
Equal recommendation: 3717 MB each
Reading from 9386: heap size 516 MB, throughput 0.991006
Reading from 9385: heap size 1377 MB, throughput 0.956651
Reading from 9386: heap size 515 MB, throughput 0.989231
Reading from 9385: heap size 1377 MB, throughput 0.977835
Reading from 9386: heap size 517 MB, throughput 0.98651
Reading from 9385: heap size 1390 MB, throughput 0.976299
Reading from 9386: heap size 519 MB, throughput 0.993213
Reading from 9386: heap size 519 MB, throughput 0.926461
Reading from 9386: heap size 519 MB, throughput 0.890145
Equal recommendation: 3717 MB each
Reading from 9385: heap size 1394 MB, throughput 0.97316
Reading from 9386: heap size 522 MB, throughput 0.989387
Reading from 9385: heap size 1397 MB, throughput 0.971654
Reading from 9386: heap size 528 MB, throughput 0.992771
Reading from 9385: heap size 1399 MB, throughput 0.968717
Reading from 9386: heap size 530 MB, throughput 0.99096
Reading from 9385: heap size 1390 MB, throughput 0.968168
Equal recommendation: 3717 MB each
Reading from 9386: heap size 531 MB, throughput 0.99021
Reading from 9385: heap size 1396 MB, throughput 0.966853
Reading from 9386: heap size 533 MB, throughput 0.988742
Reading from 9385: heap size 1384 MB, throughput 0.962948
Reading from 9386: heap size 533 MB, throughput 0.988696
Reading from 9386: heap size 534 MB, throughput 0.99204
Equal recommendation: 3717 MB each
Reading from 9386: heap size 535 MB, throughput 0.977025
Reading from 9386: heap size 537 MB, throughput 0.894643
Reading from 9386: heap size 542 MB, throughput 0.987985
Reading from 9386: heap size 543 MB, throughput 0.989594
Reading from 9385: heap size 1391 MB, throughput 0.989265
Reading from 9386: heap size 546 MB, throughput 0.991825
Equal recommendation: 3717 MB each
Reading from 9386: heap size 548 MB, throughput 0.989957
Reading from 9386: heap size 547 MB, throughput 0.989638
Reading from 9386: heap size 549 MB, throughput 0.98871
Reading from 9385: heap size 1363 MB, throughput 0.990038
Equal recommendation: 3717 MB each
Reading from 9386: heap size 551 MB, throughput 0.99231
Reading from 9386: heap size 552 MB, throughput 0.920255
Reading from 9386: heap size 552 MB, throughput 0.960851
Reading from 9386: heap size 554 MB, throughput 0.993457
Reading from 9385: heap size 1396 MB, throughput 0.979185
Reading from 9385: heap size 1398 MB, throughput 0.825236
Reading from 9385: heap size 1425 MB, throughput 0.237143
Reading from 9385: heap size 1453 MB, throughput 0.989366
Reading from 9385: heap size 1496 MB, throughput 0.989615
Reading from 9385: heap size 1515 MB, throughput 0.993874
Reading from 9385: heap size 1517 MB, throughput 0.98943
Reading from 9385: heap size 1517 MB, throughput 0.99073
Reading from 9386: heap size 557 MB, throughput 0.993566
Reading from 9385: heap size 1523 MB, throughput 0.983439
Equal recommendation: 3717 MB each
Reading from 9385: heap size 1505 MB, throughput 0.992479
Reading from 9386: heap size 559 MB, throughput 0.99103
Reading from 9385: heap size 1253 MB, throughput 0.986969
Reading from 9385: heap size 1488 MB, throughput 0.984219
Reading from 9386: heap size 558 MB, throughput 0.990832
Reading from 9385: heap size 1275 MB, throughput 0.98356
Reading from 9386: heap size 560 MB, throughput 0.988829
Reading from 9385: heap size 1464 MB, throughput 0.982316
Equal recommendation: 3717 MB each
Reading from 9385: heap size 1297 MB, throughput 0.975114
Reading from 9386: heap size 562 MB, throughput 0.995546
Reading from 9386: heap size 563 MB, throughput 0.931279
Reading from 9386: heap size 563 MB, throughput 0.950893
Reading from 9385: heap size 1438 MB, throughput 0.971621
Reading from 9386: heap size 566 MB, throughput 0.992257
Reading from 9385: heap size 1318 MB, throughput 0.970077
Reading from 9385: heap size 1433 MB, throughput 0.968325
Reading from 9386: heap size 568 MB, throughput 0.993086
Equal recommendation: 3717 MB each
Reading from 9385: heap size 1440 MB, throughput 0.965223
Reading from 9386: heap size 570 MB, throughput 0.990987
Reading from 9385: heap size 1440 MB, throughput 0.964417
Reading from 9386: heap size 570 MB, throughput 0.990205
Reading from 9385: heap size 1441 MB, throughput 0.962327
Reading from 9385: heap size 1447 MB, throughput 0.96064
Reading from 9386: heap size 572 MB, throughput 0.989351
Equal recommendation: 3717 MB each
Reading from 9385: heap size 1450 MB, throughput 0.959879
Reading from 9386: heap size 575 MB, throughput 0.99359
Reading from 9386: heap size 576 MB, throughput 0.926748
Reading from 9386: heap size 576 MB, throughput 0.985675
Reading from 9385: heap size 1458 MB, throughput 0.956804
Reading from 9385: heap size 1464 MB, throughput 0.956006
Reading from 9386: heap size 579 MB, throughput 0.992851
Reading from 9385: heap size 1476 MB, throughput 0.955489
Equal recommendation: 3717 MB each
Reading from 9386: heap size 580 MB, throughput 0.991539
Reading from 9385: heap size 1482 MB, throughput 0.961151
Reading from 9386: heap size 582 MB, throughput 0.991736
Reading from 9385: heap size 1495 MB, throughput 0.957392
Reading from 9385: heap size 1498 MB, throughput 0.952423
Reading from 9386: heap size 581 MB, throughput 0.990787
Equal recommendation: 3717 MB each
Reading from 9385: heap size 1510 MB, throughput 0.956912
Reading from 9386: heap size 583 MB, throughput 0.993948
Reading from 9386: heap size 584 MB, throughput 0.975858
Reading from 9386: heap size 586 MB, throughput 0.898165
Reading from 9385: heap size 1512 MB, throughput 0.958119
Reading from 9385: heap size 1525 MB, throughput 0.95791
Reading from 9386: heap size 593 MB, throughput 0.992666
Equal recommendation: 3717 MB each
Reading from 9385: heap size 1526 MB, throughput 0.957738
Reading from 9386: heap size 592 MB, throughput 0.992297
Reading from 9385: heap size 1539 MB, throughput 0.953873
Reading from 9386: heap size 593 MB, throughput 0.991459
Reading from 9385: heap size 1539 MB, throughput 0.96088
Reading from 9386: heap size 596 MB, throughput 0.990811
Reading from 9385: heap size 1553 MB, throughput 0.957718
Equal recommendation: 3717 MB each
Reading from 9386: heap size 599 MB, throughput 0.989634
Reading from 9385: heap size 1553 MB, throughput 0.95914
Reading from 9386: heap size 600 MB, throughput 0.987632
Reading from 9386: heap size 603 MB, throughput 0.925542
Reading from 9385: heap size 1565 MB, throughput 0.961041
Reading from 9386: heap size 606 MB, throughput 0.991618
Equal recommendation: 3717 MB each
Reading from 9385: heap size 1566 MB, throughput 0.708642
Reading from 9386: heap size 612 MB, throughput 0.993782
Reading from 9385: heap size 1627 MB, throughput 0.99091
Reading from 9386: heap size 613 MB, throughput 0.991918
Reading from 9385: heap size 1627 MB, throughput 0.987057
Reading from 9386: heap size 613 MB, throughput 0.991528
Equal recommendation: 3717 MB each
Reading from 9385: heap size 1647 MB, throughput 0.98579
Reading from 9386: heap size 615 MB, throughput 0.995126
Reading from 9385: heap size 1652 MB, throughput 0.982526
Reading from 9386: heap size 617 MB, throughput 0.979329
Reading from 9386: heap size 618 MB, throughput 0.970769
Reading from 9385: heap size 1654 MB, throughput 0.986132
Equal recommendation: 3717 MB each
Reading from 9386: heap size 623 MB, throughput 0.994291
Reading from 9386: heap size 624 MB, throughput 0.99241
Reading from 9386: heap size 623 MB, throughput 0.992051
Equal recommendation: 3717 MB each
Reading from 9386: heap size 625 MB, throughput 0.992122
Reading from 9386: heap size 626 MB, throughput 0.994101
Reading from 9386: heap size 627 MB, throughput 0.93048
Client 9386 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 9385: heap size 1658 MB, throughput 0.996928
Recommendation: one client; give it all the memory
Reading from 9385: heap size 1648 MB, throughput 0.989343
Reading from 9385: heap size 1655 MB, throughput 0.852116
Reading from 9385: heap size 1646 MB, throughput 0.795503
Reading from 9385: heap size 1681 MB, throughput 0.779114
Reading from 9385: heap size 1718 MB, throughput 0.804472
Reading from 9385: heap size 1738 MB, throughput 0.851351
Client 9385 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
