#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <unistd.h>
#include <Utilities.hpp>
#include "Args.hpp"

#define DEFAULT_INIT_HEAP_SIZE_MB 100

const char* Args::s_ProgramFile = "";
unsigned long long Args::s_InitialHeapSizeMB = DEFAULT_INIT_HEAP_SIZE_MB;
const char* Args::s_ReceiverName = economem::NAME_DEFAULT;
#define USAGE_STRING "Usage: fakevm [-h] [-v] [-r RECEIVER_NAME] program_file [init_heap_size_MB]\n"

void Args::usage()
{
    fprintf(stderr, USAGE_STRING);
    exit(1);
}

void Args::help()
{
    printf(USAGE_STRING);
    printf("    -h                 help: print this message and exit\n");
    printf("    -v                 verbose\n");
    printf("    -r RECEIVER_NAME   set the name of the socket used to receive readings\n");
    printf("    program_file       read program specification from this file\n");
    printf("    init_heap_size_MB  optional; defaults to %d MB\n", DEFAULT_INIT_HEAP_SIZE_MB);
    exit(0);
}

const char* Args::getProgramFile()
{
    return s_ProgramFile;
}

unsigned long long Args::getInitialHeapSizeMB()
{
    return s_InitialHeapSizeMB;
}

const char* Args::getReceiverName()
{
    return s_ReceiverName;
}

void Args::parse(int argc, char** argv)
{
    int opt = 0;
    opterr = 0;

    while ((opt = getopt(argc, argv, "hvr:")) != -1)
    {
        switch (opt)
        {
        case 'h':
            help();
            break;
        case 'v':
            economem::makeInfoVerbose();
            break;
        case 'r':
            s_ReceiverName = optarg;
            if (s_ReceiverName == NULL || s_ReceiverName[0] == '\0')
                economem::die("Argument to '-r' must not be empty");
            break;
        default:
            usage();
            break;
        }
    }

    argv = argv + optind;
    argc = argc - optind;

    if (argc < 1)
        usage();

    s_ProgramFile = argv[0];

    if (argc >= 2 && ((s_InitialHeapSizeMB = atoi(argv[1])) <= 0))
        economem::die("Initial heap size must be a positive integer number of megabytes");
}
