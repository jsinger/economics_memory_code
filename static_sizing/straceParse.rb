#!/usr/bin/ruby -w

# Jeremy Singer
# 31 Jan 14

if ARGV.empty?
  puts "Usage: parse_strace.rb STRACE_FILE"
  exit 1
end

file = ARGV[0]

#puts "parsing #{file}"

# cumulative time spent in syscalls
total = 0.0

File.readlines(file).each do |line|
  # when run with strace -ttt then each syscall dump line
  # has timestamp of form NNNN.NNNN
  if line =~ /[0-9]+\.[0-9]+/ then
    # final word on line is time spent in syscall
    time = line.split(/ /).last[1..-3]
    # strip '<' and '>' chars ^^^^^^^^^
    # sometimes the word says "<unfinished>..."
    if time =~ /[0-9]+\.[0-9]+/ then
      #puts "time: #{time}"
      total = total + time.to_f
    end
  end
end

puts "total time in syscalls: #{total}"
