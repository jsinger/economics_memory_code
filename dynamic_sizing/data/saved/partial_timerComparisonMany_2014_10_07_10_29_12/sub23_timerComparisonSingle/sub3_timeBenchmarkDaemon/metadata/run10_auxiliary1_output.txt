economemd
    total memory: 2760 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub23_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run10_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
TimerThread: starting
ReadingThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 31327: heap size 9 MB, throughput 0.990798
Clients: 1
Client 31327 has a minimum heap size of 276 MB
Reading from 31328: heap size 9 MB, throughput 0.985558
Clients: 2
Client 31328 has a minimum heap size of 276 MB
Reading from 31327: heap size 9 MB, throughput 0.987589
Reading from 31328: heap size 9 MB, throughput 0.982878
Reading from 31327: heap size 9 MB, throughput 0.98122
Reading from 31327: heap size 9 MB, throughput 0.972793
Reading from 31328: heap size 11 MB, throughput 0.980946
Reading from 31327: heap size 11 MB, throughput 0.971859
Reading from 31328: heap size 11 MB, throughput 0.917235
Reading from 31327: heap size 11 MB, throughput 0.978324
Reading from 31327: heap size 17 MB, throughput 0.96202
Reading from 31328: heap size 15 MB, throughput 0.909521
Reading from 31327: heap size 17 MB, throughput 0.900394
Reading from 31328: heap size 19 MB, throughput 0.909947
Reading from 31328: heap size 25 MB, throughput 0.812365
Reading from 31327: heap size 30 MB, throughput 0.855388
Reading from 31327: heap size 31 MB, throughput 0.560917
Reading from 31328: heap size 29 MB, throughput 0.705347
Reading from 31328: heap size 42 MB, throughput 0.660845
Reading from 31328: heap size 43 MB, throughput 0.409481
Reading from 31327: heap size 34 MB, throughput 0.663043
Reading from 31327: heap size 46 MB, throughput 0.417723
Reading from 31327: heap size 48 MB, throughput 0.332583
Reading from 31328: heap size 47 MB, throughput 0.574367
Reading from 31328: heap size 60 MB, throughput 0.876695
Reading from 31328: heap size 69 MB, throughput 0.822979
Reading from 31327: heap size 50 MB, throughput 0.499175
Reading from 31327: heap size 74 MB, throughput 0.489406
Reading from 31328: heap size 71 MB, throughput 0.567038
Reading from 31328: heap size 93 MB, throughput 0.847619
Reading from 31327: heap size 74 MB, throughput 0.351132
Reading from 31327: heap size 101 MB, throughput 0.618967
Reading from 31328: heap size 98 MB, throughput 0.824809
Reading from 31327: heap size 101 MB, throughput 0.547838
Reading from 31327: heap size 103 MB, throughput 0.330281
Reading from 31328: heap size 104 MB, throughput 0.446635
Reading from 31327: heap size 106 MB, throughput 0.541981
Reading from 31328: heap size 128 MB, throughput 0.722387
Reading from 31327: heap size 132 MB, throughput 0.253915
Reading from 31327: heap size 136 MB, throughput 0.355879
Reading from 31328: heap size 136 MB, throughput 0.769964
Reading from 31327: heap size 138 MB, throughput 0.198144
Reading from 31328: heap size 138 MB, throughput 0.715252
Reading from 31327: heap size 140 MB, throughput 0.708942
Reading from 31327: heap size 148 MB, throughput 0.715254
Reading from 31327: heap size 150 MB, throughput 0.397479
Reading from 31328: heap size 142 MB, throughput 0.334744
Reading from 31327: heap size 182 MB, throughput 0.678774
Reading from 31328: heap size 178 MB, throughput 0.675508
Reading from 31327: heap size 189 MB, throughput 0.735765
Reading from 31328: heap size 187 MB, throughput 0.691794
Reading from 31327: heap size 190 MB, throughput 0.74485
Reading from 31328: heap size 191 MB, throughput 0.675342
Reading from 31328: heap size 196 MB, throughput 0.756267
Reading from 31327: heap size 195 MB, throughput 0.825087
Reading from 31327: heap size 195 MB, throughput 0.602706
Reading from 31327: heap size 241 MB, throughput 0.81313
Reading from 31328: heap size 205 MB, throughput 0.484888
Reading from 31327: heap size 248 MB, throughput 0.739985
Reading from 31327: heap size 249 MB, throughput 0.684468
Reading from 31327: heap size 253 MB, throughput 0.735586
Reading from 31328: heap size 250 MB, throughput 0.867365
Reading from 31327: heap size 253 MB, throughput 0.648132
Reading from 31328: heap size 257 MB, throughput 0.847278
Reading from 31327: heap size 257 MB, throughput 0.745133
Reading from 31328: heap size 263 MB, throughput 0.812463
Reading from 31327: heap size 258 MB, throughput 0.766234
Reading from 31328: heap size 267 MB, throughput 0.778358
Reading from 31327: heap size 259 MB, throughput 0.805358
Reading from 31327: heap size 260 MB, throughput 0.776773
Reading from 31328: heap size 273 MB, throughput 0.808624
Reading from 31327: heap size 266 MB, throughput 0.765324
Reading from 31327: heap size 268 MB, throughput 0.626099
Reading from 31328: heap size 276 MB, throughput 0.751087
Reading from 31327: heap size 273 MB, throughput 0.616346
Reading from 31328: heap size 278 MB, throughput 0.676138
Reading from 31327: heap size 274 MB, throughput 0.684258
Reading from 31328: heap size 284 MB, throughput 0.654105
Reading from 31328: heap size 291 MB, throughput 0.669561
Reading from 31328: heap size 294 MB, throughput 0.687556
Reading from 31328: heap size 290 MB, throughput 0.724521
Reading from 31327: heap size 274 MB, throughput 0.628504
Reading from 31327: heap size 319 MB, throughput 0.786592
Reading from 31327: heap size 323 MB, throughput 0.823925
Reading from 31327: heap size 323 MB, throughput 0.807834
Reading from 31328: heap size 295 MB, throughput 0.649581
Reading from 31327: heap size 327 MB, throughput 0.849277
Reading from 31328: heap size 326 MB, throughput 0.835477
Reading from 31328: heap size 331 MB, throughput 0.79701
Reading from 31327: heap size 328 MB, throughput 0.843349
Reading from 31327: heap size 328 MB, throughput 0.85407
Reading from 31327: heap size 330 MB, throughput 0.812214
Reading from 31327: heap size 332 MB, throughput 0.759551
Reading from 31328: heap size 335 MB, throughput 0.381988
Reading from 31327: heap size 334 MB, throughput 0.736572
Reading from 31327: heap size 341 MB, throughput 0.673067
Reading from 31328: heap size 379 MB, throughput 0.756285
Reading from 31327: heap size 343 MB, throughput 0.617038
Numeric result:
Recommendation: 2 clients, utility 0.615751:
    h1: 2484 MB (U(h) = 0.554805*h^0.0255604)
    h2: 276 MB (U(h) = 0.903739*h^0.001)
Recommendation: 2 clients, utility 0.615751:
    h1: 2484 MB (U(h) = 0.554805*h^0.0255604)
    h2: 276 MB (U(h) = 0.903739*h^0.001)
Reading from 31327: heap size 352 MB, throughput 0.650932
Reading from 31328: heap size 386 MB, throughput 0.80412
Reading from 31328: heap size 301 MB, throughput 0.827563
Reading from 31328: heap size 375 MB, throughput 0.780397
Reading from 31328: heap size 321 MB, throughput 0.70289
Reading from 31328: heap size 364 MB, throughput 0.645011
Reading from 31328: heap size 321 MB, throughput 0.658036
Reading from 31328: heap size 355 MB, throughput 0.599298
Reading from 31328: heap size 322 MB, throughput 0.891774
Reading from 31327: heap size 353 MB, throughput 0.943968
Reading from 31328: heap size 354 MB, throughput 0.951855
Reading from 31327: heap size 356 MB, throughput 0.960604
Reading from 31328: heap size 304 MB, throughput 0.958382
Reading from 31327: heap size 359 MB, throughput 0.949426
Reading from 31328: heap size 349 MB, throughput 0.962882
Reading from 31328: heap size 296 MB, throughput 0.963205
Reading from 31327: heap size 363 MB, throughput 0.963838
Reading from 31328: heap size 347 MB, throughput 0.963435
Reading from 31327: heap size 364 MB, throughput 0.967855
Reading from 31328: heap size 299 MB, throughput 0.964937
Reading from 31328: heap size 341 MB, throughput 0.957368
Reading from 31327: heap size 365 MB, throughput 0.973923
Reading from 31328: heap size 301 MB, throughput 0.95837
Numeric result:
Recommendation: 2 clients, utility 0.60962:
    h1: 2484 MB (U(h) = 0.46869*h^0.0681395)
    h2: 276 MB (U(h) = 0.759256*h^0.001)
Recommendation: 2 clients, utility 0.60962:
    h1: 2484 MB (U(h) = 0.46869*h^0.0681395)
    h2: 276 MB (U(h) = 0.759256*h^0.001)
Reading from 31328: heap size 336 MB, throughput 0.960205
Reading from 31327: heap size 367 MB, throughput 0.975011
Reading from 31328: heap size 302 MB, throughput 0.962232
Reading from 31328: heap size 330 MB, throughput 0.963142
Reading from 31327: heap size 364 MB, throughput 0.973625
Reading from 31328: heap size 301 MB, throughput 0.9635
Reading from 31328: heap size 323 MB, throughput 0.972529
Reading from 31327: heap size 367 MB, throughput 0.977085
Reading from 31328: heap size 301 MB, throughput 0.973237
Reading from 31328: heap size 319 MB, throughput 0.971904
Reading from 31327: heap size 368 MB, throughput 0.976636
Reading from 31328: heap size 301 MB, throughput 0.969134
Reading from 31328: heap size 314 MB, throughput 0.96553
Reading from 31327: heap size 369 MB, throughput 0.974877
Reading from 31328: heap size 300 MB, throughput 0.518947
Reading from 31327: heap size 372 MB, throughput 0.9743
Reading from 31328: heap size 311 MB, throughput 0.675585
Reading from 31328: heap size 298 MB, throughput 0.779328
Reading from 31327: heap size 373 MB, throughput 0.972295
Reading from 31328: heap size 308 MB, throughput 0.807178
Numeric result:
Recommendation: 2 clients, utility 0.666613:
    h1: 2484 MB (U(h) = 0.422871*h^0.0938834)
    h2: 276 MB (U(h) = 0.717228*h^0.00952948)
Recommendation: 2 clients, utility 0.666613:
    h1: 2484 MB (U(h) = 0.422871*h^0.0938834)
    h2: 276 MB (U(h) = 0.717228*h^0.00952948)
Reading from 31328: heap size 297 MB, throughput 0.827117
Reading from 31327: heap size 375 MB, throughput 0.979101
Reading from 31327: heap size 377 MB, throughput 0.968032
Reading from 31327: heap size 373 MB, throughput 0.942497
Reading from 31328: heap size 306 MB, throughput 0.892019
Reading from 31327: heap size 378 MB, throughput 0.918009
Reading from 31327: heap size 387 MB, throughput 0.929037
Reading from 31328: heap size 300 MB, throughput 0.88608
Reading from 31328: heap size 304 MB, throughput 0.752002
Reading from 31327: heap size 430 MB, throughput 0.868558
Reading from 31328: heap size 306 MB, throughput 0.930009
Reading from 31328: heap size 331 MB, throughput 0.927694
Reading from 31328: heap size 339 MB, throughput 0.936923
Reading from 31328: heap size 339 MB, throughput 0.918477
Reading from 31328: heap size 342 MB, throughput 0.894082
Reading from 31328: heap size 343 MB, throughput 0.926397
Reading from 31328: heap size 344 MB, throughput 0.949551
Reading from 31328: heap size 343 MB, throughput 0.933443
Reading from 31328: heap size 286 MB, throughput 0.954423
Reading from 31328: heap size 340 MB, throughput 0.960725
Reading from 31328: heap size 288 MB, throughput 0.986268
Reading from 31327: heap size 438 MB, throughput 0.959983
Reading from 31328: heap size 335 MB, throughput 0.992899
Reading from 31328: heap size 288 MB, throughput 0.990911
Reading from 31328: heap size 329 MB, throughput 0.985628
Reading from 31328: heap size 291 MB, throughput 0.986601
Reading from 31327: heap size 440 MB, throughput 0.978795
Reading from 31328: heap size 324 MB, throughput 0.987831
Reading from 31328: heap size 294 MB, throughput 0.979722
Reading from 31328: heap size 317 MB, throughput 0.977754
Reading from 31327: heap size 445 MB, throughput 0.984038
Reading from 31328: heap size 296 MB, throughput 0.975335
Reading from 31328: heap size 315 MB, throughput 0.973475
Numeric result:
Recommendation: 2 clients, utility 0.798817:
    h1: 2038.5 MB (U(h) = 0.37884*h^0.120555)
    h2: 721.495 MB (U(h) = 0.635448*h^0.0426686)
Recommendation: 2 clients, utility 0.798817:
    h1: 2038.5 MB (U(h) = 0.37884*h^0.120555)
    h2: 721.497 MB (U(h) = 0.635448*h^0.0426686)
Reading from 31328: heap size 299 MB, throughput 0.974958
Reading from 31328: heap size 316 MB, throughput 0.970694
Reading from 31327: heap size 447 MB, throughput 0.98259
Reading from 31328: heap size 316 MB, throughput 0.969883
Reading from 31328: heap size 316 MB, throughput 0.962861
Reading from 31328: heap size 317 MB, throughput 0.968225
Reading from 31327: heap size 446 MB, throughput 0.983722
Reading from 31328: heap size 318 MB, throughput 0.876798
Reading from 31328: heap size 319 MB, throughput 0.74958
Reading from 31328: heap size 321 MB, throughput 0.653864
Reading from 31327: heap size 449 MB, throughput 0.984168
Reading from 31328: heap size 321 MB, throughput 0.642412
Reading from 31328: heap size 323 MB, throughput 0.655637
Reading from 31328: heap size 323 MB, throughput 0.660341
Reading from 31327: heap size 446 MB, throughput 0.981377
Reading from 31328: heap size 324 MB, throughput 0.691223
Reading from 31328: heap size 324 MB, throughput 0.668806
Reading from 31328: heap size 326 MB, throughput 0.701875
Reading from 31327: heap size 448 MB, throughput 0.978932
Reading from 31328: heap size 326 MB, throughput 0.676158
Numeric result:
Recommendation: 2 clients, utility 0.82109:
    h1: 2148.8 MB (U(h) = 0.35986*h^0.132659)
    h2: 611.2 MB (U(h) = 0.647226*h^0.0377414)
Recommendation: 2 clients, utility 0.82109:
    h1: 2148.7 MB (U(h) = 0.35986*h^0.132659)
    h2: 611.302 MB (U(h) = 0.647226*h^0.0377414)
Reading from 31328: heap size 327 MB, throughput 0.674532
Reading from 31328: heap size 327 MB, throughput 0.66922
Reading from 31327: heap size 451 MB, throughput 0.98097
Reading from 31328: heap size 329 MB, throughput 0.684184
Reading from 31328: heap size 329 MB, throughput 0.668351
Reading from 31328: heap size 329 MB, throughput 0.699729
Reading from 31327: heap size 452 MB, throughput 0.976688
Reading from 31328: heap size 330 MB, throughput 0.621631
Reading from 31327: heap size 451 MB, throughput 0.97468
Reading from 31327: heap size 458 MB, throughput 0.964523
Reading from 31327: heap size 458 MB, throughput 0.94628
Reading from 31327: heap size 460 MB, throughput 0.922517
Reading from 31328: heap size 326 MB, throughput 0.963114
Reading from 31328: heap size 331 MB, throughput 0.953078
Reading from 31328: heap size 337 MB, throughput 0.940005
Reading from 31328: heap size 338 MB, throughput 0.893801
Reading from 31328: heap size 335 MB, throughput 0.841923
Reading from 31328: heap size 341 MB, throughput 0.781647
Reading from 31328: heap size 347 MB, throughput 0.762938
Reading from 31328: heap size 350 MB, throughput 0.955813
Reading from 31328: heap size 354 MB, throughput 0.834596
Reading from 31328: heap size 356 MB, throughput 0.899927
Reading from 31328: heap size 363 MB, throughput 0.939666
Reading from 31327: heap size 469 MB, throughput 0.974637
Reading from 31328: heap size 364 MB, throughput 0.983935
Reading from 31328: heap size 367 MB, throughput 0.991234
Reading from 31328: heap size 368 MB, throughput 0.989635
Reading from 31327: heap size 470 MB, throughput 0.986492
Reading from 31328: heap size 366 MB, throughput 0.988787
Numeric result:
Recommendation: 2 clients, utility 0.874289:
    h1: 2100.11 MB (U(h) = 0.340451*h^0.14561)
    h2: 659.886 MB (U(h) = 0.626401*h^0.0457523)
Recommendation: 2 clients, utility 0.874289:
    h1: 2100.12 MB (U(h) = 0.340451*h^0.14561)
    h2: 659.882 MB (U(h) = 0.626401*h^0.0457523)
Reading from 31328: heap size 368 MB, throughput 0.989343
Reading from 31327: heap size 472 MB, throughput 0.987439
Reading from 31328: heap size 362 MB, throughput 0.988625
Reading from 31328: heap size 320 MB, throughput 0.985721
Reading from 31327: heap size 475 MB, throughput 0.985733
Reading from 31328: heap size 358 MB, throughput 0.98611
Reading from 31328: heap size 325 MB, throughput 0.98068
Reading from 31328: heap size 355 MB, throughput 0.979036
Reading from 31327: heap size 473 MB, throughput 0.985215
Reading from 31328: heap size 357 MB, throughput 0.97632
Reading from 31328: heap size 355 MB, throughput 0.977787
Reading from 31327: heap size 476 MB, throughput 0.985748
Reading from 31328: heap size 356 MB, throughput 0.976169
Reading from 31328: heap size 358 MB, throughput 0.974872
Reading from 31327: heap size 473 MB, throughput 0.985149
Reading from 31328: heap size 358 MB, throughput 0.970823
Numeric result:
Recommendation: 2 clients, utility 0.920952:
    h1: 2026.57 MB (U(h) = 0.329085*h^0.153466)
    h2: 733.433 MB (U(h) = 0.602979*h^0.0555438)
Recommendation: 2 clients, utility 0.920952:
    h1: 2026.54 MB (U(h) = 0.329085*h^0.153466)
    h2: 733.464 MB (U(h) = 0.602979*h^0.0555438)
Reading from 31328: heap size 360 MB, throughput 0.973492
Reading from 31327: heap size 476 MB, throughput 0.984289
Reading from 31328: heap size 361 MB, throughput 0.973559
Reading from 31328: heap size 364 MB, throughput 0.97461
Reading from 31328: heap size 365 MB, throughput 0.971461
Reading from 31327: heap size 475 MB, throughput 0.983124
Reading from 31328: heap size 367 MB, throughput 0.970693
Reading from 31327: heap size 477 MB, throughput 0.98688
Reading from 31327: heap size 479 MB, throughput 0.978958
Reading from 31327: heap size 480 MB, throughput 0.964683
Reading from 31328: heap size 368 MB, throughput 0.967953
Reading from 31327: heap size 486 MB, throughput 0.973398
Reading from 31328: heap size 366 MB, throughput 0.975189
Reading from 31328: heap size 371 MB, throughput 0.963141
Reading from 31328: heap size 370 MB, throughput 0.922845
Reading from 31328: heap size 371 MB, throughput 0.873846
Reading from 31328: heap size 378 MB, throughput 0.843499
Reading from 31328: heap size 382 MB, throughput 0.803304
Reading from 31328: heap size 392 MB, throughput 0.81525
Reading from 31328: heap size 393 MB, throughput 0.945689
Reading from 31327: heap size 486 MB, throughput 0.986754
Numeric result:
Recommendation: 2 clients, utility 1.0071:
    h1: 1656.5 MB (U(h) = 0.320321*h^0.159683)
    h2: 1103.5 MB (U(h) = 0.456879*h^0.106363)
Recommendation: 2 clients, utility 1.0071:
    h1: 1656.57 MB (U(h) = 0.320321*h^0.159683)
    h2: 1103.43 MB (U(h) = 0.456879*h^0.106363)
Reading from 31328: heap size 400 MB, throughput 0.975787
Reading from 31328: heap size 402 MB, throughput 0.979428
Reading from 31327: heap size 491 MB, throughput 0.987145
Reading from 31328: heap size 403 MB, throughput 0.98088
Reading from 31327: heap size 493 MB, throughput 0.987017
Reading from 31328: heap size 406 MB, throughput 0.970163
Reading from 31328: heap size 405 MB, throughput 0.978947
Reading from 31327: heap size 493 MB, throughput 0.987387
Reading from 31328: heap size 408 MB, throughput 0.979922
Reading from 31328: heap size 405 MB, throughput 0.979785
Reading from 31327: heap size 495 MB, throughput 0.987981
Numeric result:
Recommendation: 2 clients, utility 1.22437:
    h1: 1176.87 MB (U(h) = 0.314232*h^0.164065)
    h2: 1583.13 MB (U(h) = 0.240286*h^0.2207)
Recommendation: 2 clients, utility 1.22437:
    h1: 1176.87 MB (U(h) = 0.314232*h^0.164065)
    h2: 1583.13 MB (U(h) = 0.240286*h^0.2207)
Reading from 31328: heap size 408 MB, throughput 0.981123
Reading from 31327: heap size 494 MB, throughput 0.988027
Reading from 31328: heap size 406 MB, throughput 0.982403
Reading from 31328: heap size 408 MB, throughput 0.981359
Reading from 31327: heap size 496 MB, throughput 0.987097
Reading from 31328: heap size 407 MB, throughput 0.979801
Reading from 31328: heap size 408 MB, throughput 0.978359
Reading from 31327: heap size 499 MB, throughput 0.987641
Reading from 31327: heap size 499 MB, throughput 0.98018
Reading from 31327: heap size 501 MB, throughput 0.972118
Reading from 31328: heap size 410 MB, throughput 0.981398
Reading from 31327: heap size 502 MB, throughput 0.985053
Reading from 31328: heap size 411 MB, throughput 0.981706
Reading from 31328: heap size 415 MB, throughput 0.969678
Reading from 31328: heap size 416 MB, throughput 0.945767
Reading from 31328: heap size 422 MB, throughput 0.929008
Reading from 31328: heap size 426 MB, throughput 0.909056
Numeric result:
Recommendation: 2 clients, utility 1.77952:
    h1: 783.874 MB (U(h) = 0.305995*h^0.170109)
    h2: 1976.13 MB (U(h) = 0.072268*h^0.428818)
Recommendation: 2 clients, utility 1.77952:
    h1: 783.903 MB (U(h) = 0.305995*h^0.170109)
    h2: 1976.1 MB (U(h) = 0.072268*h^0.428818)
Reading from 31327: heap size 509 MB, throughput 0.990032
Reading from 31328: heap size 437 MB, throughput 0.977822
Reading from 31328: heap size 437 MB, throughput 0.985433
Reading from 31327: heap size 510 MB, throughput 0.99063
Reading from 31328: heap size 438 MB, throughput 0.985098
Reading from 31327: heap size 511 MB, throughput 0.990422
Reading from 31328: heap size 441 MB, throughput 0.985694
Reading from 31328: heap size 441 MB, throughput 0.985351
Reading from 31327: heap size 513 MB, throughput 0.989099
Numeric result:
Recommendation: 2 clients, utility 2.35203:
    h1: 638.767 MB (U(h) = 0.300158*h^0.174454)
    h2: 2121.23 MB (U(h) = 0.030029*h^0.579317)
Recommendation: 2 clients, utility 2.35203:
    h1: 638.779 MB (U(h) = 0.300158*h^0.174454)
    h2: 2121.22 MB (U(h) = 0.030029*h^0.579317)
Reading from 31328: heap size 444 MB, throughput 0.972857
Reading from 31327: heap size 513 MB, throughput 0.988948
Reading from 31328: heap size 437 MB, throughput 0.990254
Reading from 31327: heap size 514 MB, throughput 0.98893
Reading from 31328: heap size 439 MB, throughput 0.991148
Reading from 31328: heap size 445 MB, throughput 0.989786
Reading from 31327: heap size 517 MB, throughput 0.989958
Reading from 31327: heap size 518 MB, throughput 0.983294
Reading from 31327: heap size 518 MB, throughput 0.976138
Reading from 31328: heap size 446 MB, throughput 0.988929
Numeric result:
Recommendation: 2 clients, utility 1.95134:
    h1: 752.826 MB (U(h) = 0.296486*h^0.17722)
    h2: 2007.17 MB (U(h) = 0.0559827*h^0.472499)
Recommendation: 2 clients, utility 1.95134:
    h1: 752.828 MB (U(h) = 0.296486*h^0.17722)
    h2: 2007.17 MB (U(h) = 0.0559827*h^0.472499)
Reading from 31327: heap size 520 MB, throughput 0.987595
Reading from 31328: heap size 445 MB, throughput 0.987204
Reading from 31328: heap size 447 MB, throughput 0.976667
Reading from 31328: heap size 446 MB, throughput 0.958537
Reading from 31328: heap size 450 MB, throughput 0.964974
Reading from 31327: heap size 524 MB, throughput 0.990527
Reading from 31328: heap size 459 MB, throughput 0.984931
Reading from 31328: heap size 462 MB, throughput 0.988003
Reading from 31327: heap size 526 MB, throughput 0.990183
Reading from 31328: heap size 462 MB, throughput 0.989959
Reading from 31327: heap size 527 MB, throughput 0.989855
Numeric result:
Recommendation: 2 clients, utility 1.89808:
    h1: 948.219 MB (U(h) = 0.226625*h^0.22422)
    h2: 1811.78 MB (U(h) = 0.0723898*h^0.428422)
Recommendation: 2 clients, utility 1.89808:
    h1: 948.219 MB (U(h) = 0.226625*h^0.22422)
    h2: 1811.78 MB (U(h) = 0.0723898*h^0.428422)
Reading from 31328: heap size 465 MB, throughput 0.987971
Reading from 31327: heap size 529 MB, throughput 0.990285
Reading from 31328: heap size 466 MB, throughput 0.988431
Reading from 31327: heap size 531 MB, throughput 0.988732
Reading from 31328: heap size 468 MB, throughput 0.987328
Reading from 31327: heap size 532 MB, throughput 0.988637
Reading from 31328: heap size 472 MB, throughput 0.984258
Reading from 31327: heap size 536 MB, throughput 0.983324
Reading from 31327: heap size 536 MB, throughput 0.97794
Reading from 31328: heap size 473 MB, throughput 0.982624
Numeric result:
Recommendation: 2 clients, utility 2.28084:
    h1: 1444.64 MB (U(h) = 0.0704456*h^0.423443)
    h2: 1315.36 MB (U(h) = 0.0932657*h^0.385548)
Recommendation: 2 clients, utility 2.28084:
    h1: 1444.64 MB (U(h) = 0.0704456*h^0.423443)
    h2: 1315.36 MB (U(h) = 0.0932657*h^0.385548)
Reading from 31327: heap size 545 MB, throughput 0.98897
Reading from 31328: heap size 477 MB, throughput 0.986272
Reading from 31328: heap size 479 MB, throughput 0.975845
Reading from 31328: heap size 480 MB, throughput 0.963069
Reading from 31328: heap size 483 MB, throughput 0.974882
Reading from 31327: heap size 545 MB, throughput 0.989386
Reading from 31328: heap size 492 MB, throughput 0.986597
Reading from 31327: heap size 547 MB, throughput 0.990561
Reading from 31328: heap size 494 MB, throughput 0.987882
Numeric result:
Recommendation: 2 clients, utility 2.3292:
    h1: 1703.8 MB (U(h) = 0.045759*h^0.496053)
    h2: 1056.2 MB (U(h) = 0.149263*h^0.307507)
Recommendation: 2 clients, utility 2.3292:
    h1: 1703.8 MB (U(h) = 0.045759*h^0.496053)
    h2: 1056.2 MB (U(h) = 0.149263*h^0.307507)
Reading from 31327: heap size 549 MB, throughput 0.990538
Reading from 31328: heap size 497 MB, throughput 0.988522
Reading from 31327: heap size 550 MB, throughput 0.9913
Reading from 31328: heap size 500 MB, throughput 0.988606
Reading from 31328: heap size 500 MB, throughput 0.988553
Reading from 31327: heap size 551 MB, throughput 0.993103
Reading from 31327: heap size 553 MB, throughput 0.989607
Reading from 31327: heap size 554 MB, throughput 0.981432
Reading from 31328: heap size 502 MB, throughput 0.98734
Numeric result:
Recommendation: 2 clients, utility 2.53724:
    h1: 1784.51 MB (U(h) = 0.0312771*h^0.55903)
    h2: 975.486 MB (U(h) = 0.15066*h^0.305578)
Recommendation: 2 clients, utility 2.53724:
    h1: 1784.53 MB (U(h) = 0.0312771*h^0.55903)
    h2: 975.465 MB (U(h) = 0.15066*h^0.305578)
Reading from 31327: heap size 559 MB, throughput 0.987549
Reading from 31328: heap size 507 MB, throughput 0.989156
Reading from 31328: heap size 507 MB, throughput 0.983026
Reading from 31328: heap size 509 MB, throughput 0.976788
Reading from 31327: heap size 561 MB, throughput 0.990428
Reading from 31328: heap size 511 MB, throughput 0.982879
Reading from 31328: heap size 520 MB, throughput 0.990669
Reading from 31327: heap size 563 MB, throughput 0.991367
Numeric result:
Recommendation: 2 clients, utility 2.29103:
    h1: 1716.88 MB (U(h) = 0.0464468*h^0.493416)
    h2: 1043.12 MB (U(h) = 0.155652*h^0.299783)
Recommendation: 2 clients, utility 2.29103:
    h1: 1716.88 MB (U(h) = 0.0464468*h^0.493416)
    h2: 1043.12 MB (U(h) = 0.155652*h^0.299783)
Reading from 31328: heap size 521 MB, throughput 0.991507
Reading from 31327: heap size 565 MB, throughput 0.990992
Reading from 31328: heap size 524 MB, throughput 0.990762
Reading from 31327: heap size 566 MB, throughput 0.991278
Reading from 31328: heap size 526 MB, throughput 0.990002
Reading from 31327: heap size 567 MB, throughput 0.99172
Reading from 31328: heap size 526 MB, throughput 0.989566
Reading from 31327: heap size 570 MB, throughput 0.987798
Reading from 31327: heap size 571 MB, throughput 0.980054
Numeric result:
Recommendation: 2 clients, utility 1.92922:
    h1: 1614.01 MB (U(h) = 0.0887849*h^0.38632)
    h2: 1145.99 MB (U(h) = 0.1814*h^0.274298)
Recommendation: 2 clients, utility 1.92922:
    h1: 1614.01 MB (U(h) = 0.0887849*h^0.38632)
    h2: 1145.99 MB (U(h) = 0.1814*h^0.274298)
Reading from 31327: heap size 576 MB, throughput 0.988678
Reading from 31328: heap size 528 MB, throughput 0.992835
Reading from 31328: heap size 531 MB, throughput 0.989471
Reading from 31328: heap size 532 MB, throughput 0.979821
Reading from 31328: heap size 537 MB, throughput 0.986434
Reading from 31327: heap size 578 MB, throughput 0.9917
Reading from 31328: heap size 539 MB, throughput 0.989359
Reading from 31327: heap size 580 MB, throughput 0.991964
Numeric result:
Recommendation: 2 clients, utility 1.93112:
    h1: 1724.46 MB (U(h) = 0.0764786*h^0.410342)
    h2: 1035.54 MB (U(h) = 0.21437*h^0.246412)
Recommendation: 2 clients, utility 1.93112:
    h1: 1724.46 MB (U(h) = 0.0764786*h^0.410342)
    h2: 1035.54 MB (U(h) = 0.21437*h^0.246412)
Reading from 31328: heap size 544 MB, throughput 0.990701
Reading from 31327: heap size 582 MB, throughput 0.990657
Reading from 31328: heap size 546 MB, throughput 0.991633
Reading from 31327: heap size 583 MB, throughput 0.990693
Reading from 31328: heap size 546 MB, throughput 0.990843
Reading from 31327: heap size 584 MB, throughput 0.991907
Reading from 31327: heap size 588 MB, throughput 0.987683
Numeric result:
Recommendation: 2 clients, utility 1.89241:
    h1: 1705.85 MB (U(h) = 0.0822757*h^0.398031)
    h2: 1054.15 MB (U(h) = 0.214678*h^0.24597)
Recommendation: 2 clients, utility 1.89241:
    h1: 1705.85 MB (U(h) = 0.0822757*h^0.398031)
    h2: 1054.15 MB (U(h) = 0.214678*h^0.24597)
Reading from 31328: heap size 548 MB, throughput 0.989097
Reading from 31327: heap size 588 MB, throughput 0.987132
Reading from 31328: heap size 552 MB, throughput 0.990702
Reading from 31328: heap size 553 MB, throughput 0.985382
Reading from 31327: heap size 594 MB, throughput 0.992853
Reading from 31328: heap size 555 MB, throughput 0.986263
Reading from 31328: heap size 558 MB, throughput 0.99134
Reading from 31327: heap size 596 MB, throughput 0.99277
Numeric result:
Recommendation: 2 clients, utility 1.88889:
    h1: 1675.97 MB (U(h) = 0.0843049*h^0.393844)
    h2: 1084.03 MB (U(h) = 0.202932*h^0.25474)
Recommendation: 2 clients, utility 1.88889:
    h1: 1675.97 MB (U(h) = 0.0843049*h^0.393844)
    h2: 1084.03 MB (U(h) = 0.202932*h^0.25474)
Reading from 31328: heap size 561 MB, throughput 0.991647
Reading from 31327: heap size 595 MB, throughput 0.992313
Reading from 31328: heap size 563 MB, throughput 0.9925
Reading from 31327: heap size 597 MB, throughput 0.985852
Reading from 31328: heap size 563 MB, throughput 0.992409
Numeric result:
Recommendation: 2 clients, utility 1.92415:
    h1: 1646.04 MB (U(h) = 0.0810494*h^0.399935)
    h2: 1113.96 MB (U(h) = 0.183853*h^0.270655)
Recommendation: 2 clients, utility 1.92415:
    h1: 1646.04 MB (U(h) = 0.0810494*h^0.399935)
    h2: 1113.96 MB (U(h) = 0.183853*h^0.270655)
Reading from 31327: heap size 610 MB, throughput 0.993504
Reading from 31327: heap size 611 MB, throughput 0.989746
Reading from 31328: heap size 565 MB, throughput 0.9906
Reading from 31327: heap size 613 MB, throughput 0.988493
Reading from 31328: heap size 569 MB, throughput 0.99059
Reading from 31328: heap size 570 MB, throughput 0.982587
Reading from 31327: heap size 614 MB, throughput 0.991912
Reading from 31328: heap size 575 MB, throughput 0.986362
Numeric result:
Recommendation: 2 clients, utility 1.73926:
    h1: 1396.7 MB (U(h) = 0.152725*h^0.297517)
    h2: 1363.3 MB (U(h) = 0.162342*h^0.290405)
Recommendation: 2 clients, utility 1.73926:
    h1: 1396.69 MB (U(h) = 0.152725*h^0.297517)
    h2: 1363.31 MB (U(h) = 0.162342*h^0.290405)
Reading from 31328: heap size 577 MB, throughput 0.990039
Reading from 31327: heap size 619 MB, throughput 0.992853
Reading from 31328: heap size 583 MB, throughput 0.993225
Reading from 31327: heap size 620 MB, throughput 0.992694
Reading from 31328: heap size 585 MB, throughput 0.991815
Reading from 31327: heap size 621 MB, throughput 0.993332
Numeric result:
Recommendation: 2 clients, utility 1.60393:
    h1: 1471.92 MB (U(h) = 0.17785*h^0.272867)
    h2: 1288.08 MB (U(h) = 0.222901*h^0.238786)
Recommendation: 2 clients, utility 1.60393:
    h1: 1471.92 MB (U(h) = 0.17785*h^0.272867)
    h2: 1288.08 MB (U(h) = 0.222901*h^0.238786)
Reading from 31328: heap size 587 MB, throughput 0.991399
Reading from 31327: heap size 622 MB, throughput 0.993544
Reading from 31327: heap size 626 MB, throughput 0.989305
Reading from 31327: heap size 627 MB, throughput 0.989792
Reading from 31328: heap size 588 MB, throughput 0.992575
Reading from 31328: heap size 591 MB, throughput 0.987957
Reading from 31328: heap size 593 MB, throughput 0.988184
Reading from 31327: heap size 634 MB, throughput 0.992658
Numeric result:
Recommendation: 2 clients, utility 1.38589:
    h1: 1582.02 MB (U(h) = 0.263018*h^0.210137)
    h2: 1177.98 MB (U(h) = 0.370623*h^0.156469)
Recommendation: 2 clients, utility 1.38589:
    h1: 1582.02 MB (U(h) = 0.263018*h^0.210137)
    h2: 1177.98 MB (U(h) = 0.370623*h^0.156469)
Reading from 31328: heap size 602 MB, throughput 0.991992
Reading from 31327: heap size 636 MB, throughput 0.993592
Reading from 31328: heap size 603 MB, throughput 0.992152
Reading from 31327: heap size 638 MB, throughput 0.993251
Numeric result:
Recommendation: 2 clients, utility 1.24861:
    h1: 1237.5 MB (U(h) = 0.4719*h^0.117053)
    h2: 1522.5 MB (U(h) = 0.400237*h^0.143994)
Recommendation: 2 clients, utility 1.24861:
    h1: 1237.58 MB (U(h) = 0.4719*h^0.117053)
    h2: 1522.42 MB (U(h) = 0.400237*h^0.143994)
Reading from 31328: heap size 604 MB, throughput 0.993267
Reading from 31327: heap size 640 MB, throughput 0.990382
Reading from 31327: heap size 640 MB, throughput 0.987652
Reading from 31328: heap size 607 MB, throughput 0.992112
Reading from 31327: heap size 646 MB, throughput 0.983788
Reading from 31328: heap size 611 MB, throughput 0.99223
Reading from 31328: heap size 612 MB, throughput 0.987244
Reading from 31327: heap size 653 MB, throughput 0.992048
Numeric result:
Recommendation: 2 clients, utility 1.15576:
    h1: 1016.33 MB (U(h) = 0.653961*h^0.0651547)
    h2: 1743.67 MB (U(h) = 0.488726*h^0.111779)
Recommendation: 2 clients, utility 1.15576:
    h1: 1016.35 MB (U(h) = 0.653961*h^0.0651547)
    h2: 1743.65 MB (U(h) = 0.488726*h^0.111779)
Reading from 31328: heap size 618 MB, throughput 0.990573
Reading from 31327: heap size 653 MB, throughput 0.992606
Reading from 31328: heap size 620 MB, throughput 0.992102
Reading from 31327: heap size 653 MB, throughput 0.992568
Reading from 31328: heap size 624 MB, throughput 0.992508
Numeric result:
Recommendation: 2 clients, utility 1.13615:
    h1: 1104.2 MB (U(h) = 0.653809*h^0.0651926)
    h2: 1655.8 MB (U(h) = 0.533225*h^0.0977604)
Recommendation: 2 clients, utility 1.13615:
    h1: 1104.19 MB (U(h) = 0.653809*h^0.0651926)
    h2: 1655.81 MB (U(h) = 0.533225*h^0.0977604)
Reading from 31327: heap size 656 MB, throughput 0.991917
Reading from 31328: heap size 626 MB, throughput 0.992083
Reading from 31327: heap size 660 MB, throughput 0.991817
Reading from 31327: heap size 661 MB, throughput 0.986483
Client 31327 died
Clients: 1
Reading from 31328: heap size 630 MB, throughput 0.993304
Reading from 31328: heap size 630 MB, throughput 0.990386
Client 31328 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
