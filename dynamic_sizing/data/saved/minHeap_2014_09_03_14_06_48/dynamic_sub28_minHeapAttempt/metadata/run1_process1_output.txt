GCBench: 2 threads, 3 iters



##########
GCBench iteration 1
##########

[0] Garbage Collector Test
[0]  Stretching memory with a binary tree of depth 22
[1] Garbage Collector Test
[1]  Stretching memory with a binary tree of depth 22
 Total memory available=2574778368 bytes Total memory available=2574778368 bytes  Free memory=2507564392 bytes
  Free memory=2507564392 bytes
[0]  Creating a long-lived binary tree of depth 22
[1]  Creating a long-lived binary tree of depth 22
[0]  Creating a long-lived array of 700000 doubles
[1]  Creating a long-lived array of 700000 doubles
 Total memory available=2574778368 bytes  Free memory=2007879840 bytes
Creating 8196 trees of depth 10
 Total memory available=2574778368 bytes  Free memory=1998066608 bytes
Creating 8196 trees of depth 10
	Top down construction took 533msecs
	Top down construction took 534msecs
	Bottom up construction took 161msecs
Creating 2048 trees of depth 12
	Bottom up construction took 160msecs
Creating 2048 trees of depth 12
	Top down construction took 283msecs
	Top down construction took 285msecs
	Bottom up construction took 124msecs
Creating 512 trees of depth 14
	Bottom up construction took 127msecs
Creating 512 trees of depth 14
	Top down construction took 135msecs
	Top down construction took 136msecs
	Bottom up construction took 130msecs
	Bottom up construction took 130msecs
Creating 128 trees of depth 16
Creating 128 trees of depth 16
	Top down construction took 134msecs
	Top down construction took 137msecs
	Bottom up construction took 137msecs
Creating 32 trees of depth 18
	Bottom up construction took 137msecs
Creating 32 trees of depth 18
	Top down construction took 132msecs
	Top down construction took 133msecs
	Bottom up construction took 138msecs
Creating 8 trees of depth 20
	Bottom up construction took 139msecs
Creating 8 trees of depth 20
	Top down construction took 146msecs
	Top down construction took 146msecs
	Bottom up construction took 181msecs
Creating 2 trees of depth 22
	Bottom up construction took 181msecs
Creating 2 trees of depth 22
	Top down construction took 131msecs
	Top down construction took 131msecs
	Bottom up construction took 205msecs
 Total memory available=2545942528 bytes  Free memory=1474065600 bytes
[1] Completed in 2767ms.
	Bottom up construction took 204msecs
 Total memory available=2545942528 bytes  Free memory=1455823200 bytes
[0] Completed in 2770ms.
[harness] Finished all threads


##########
GCBench iteration 2
##########

[0] Garbage Collector Test
[0]  Stretching memory with a binary tree of depth 22
 Total memory available=2545942528 bytes  Free memory=1443533880 bytes
[1] Garbage Collector Test
[1]  Stretching memory with a binary tree of depth 22
 Total memory available=2545942528 bytes  Free memory=1437389232 bytes
[0]  Creating a long-lived binary tree of depth 22
[1]  Creating a long-lived binary tree of depth 22
[0]  Creating a long-lived array of 700000 doubles
 Total memory available=2371878912 bytes  Free memory=1206613168 bytes
Creating 8196 trees of depth 10
[1]  Creating a long-lived array of 700000 doubles
 Total memory available=2320498688 bytes  Free memory=1133888112 bytes
Creating 8196 trees of depth 10
	Top down construction took 573msecs
	Top down construction took 293msecs
	Bottom up construction took 135msecs
Creating 2048 trees of depth 12
	Bottom up construction took 140msecs
Creating 2048 trees of depth 12
	Top down construction took 133msecs
	Top down construction took 130msecs
	Bottom up construction took 130msecs
Creating 512 trees of depth 14
	Bottom up construction took 128msecs
Creating 512 trees of depth 14
	Top down construction took 127msecs
	Top down construction took 128msecs
	Bottom up construction took 128msecs
Creating 128 trees of depth 16
	Bottom up construction took 126msecs
Creating 128 trees of depth 16
	Top down construction took 129msecs
	Top down construction took 130msecs
	Bottom up construction took 128msecs
Creating 32 trees of depth 18
	Bottom up construction took 129msecs
Creating 32 trees of depth 18
	Top down construction took 129msecs
	Top down construction took 133msecs
	Bottom up construction took 141msecs
Creating 8 trees of depth 20
	Bottom up construction took 135msecs
Creating 8 trees of depth 20
	Top down construction took 135msecs
	Top down construction took 135msecs
	Bottom up construction took 139msecs
Creating 2 trees of depth 22
	Bottom up construction took 140msecs
Creating 2 trees of depth 22
	Top down construction took 288msecs
	Top down construction took 286msecs
	Bottom up construction took 2119msecs
 Total memory available=2359296000 bytes  Free memory=1280320040 bytes
[0] Completed in 4712ms.
	Bottom up construction took 2117msecs
 Total memory available=2359296000 bytes  Free memory=1268284232 bytes
[1] Completed in 4714ms.
[harness] Finished all threads


##########
GCBench iteration 3
##########

[0] Garbage Collector Test
[1] Garbage Collector Test
[1]  Stretching memory with a binary tree of depth 22
 Total memory available=2359296000 bytes  Free memory=1260364656 bytes
[0]  Stretching memory with a binary tree of depth 22
 Total memory available=2359296000 bytes  Free memory=1255433680 bytes
[1]  Creating a long-lived binary tree of depth 22
[0]  Creating a long-lived binary tree of depth 22
[1]  Creating a long-lived array of 700000 doubles
[0]  Creating a long-lived array of 700000 doubles
 Total memory available=2388131840 bytes  Free memory=748388760 bytes
Creating 8196 trees of depth 10
 Total memory available=2388131840 bytes  Free memory=742558136 bytes
Creating 8196 trees of depth 10
	Top down construction took 1308msecs
	Top down construction took 1307msecs
	Bottom up construction took 126msecs
Creating 2048 trees of depth 12
	Bottom up construction took 129msecs
Creating 2048 trees of depth 12
	Top down construction took 134msecs
	Top down construction took 136msecs
	Bottom up construction took 130msecs
Creating 512 trees of depth 14
	Bottom up construction took 128msecs
Creating 512 trees of depth 14
	Top down construction took 127msecs
	Top down construction took 127msecs
	Bottom up construction took 129msecs
Creating 128 trees of depth 16
	Bottom up construction took 129msecs
Creating 128 trees of depth 16
	Top down construction took 126msecs
	Top down construction took 128msecs
	Bottom up construction took 125msecs
Creating 32 trees of depth 18
	Bottom up construction took 130msecs
Creating 32 trees of depth 18
	Top down construction took 129msecs
	Top down construction took 130msecs
	Bottom up construction took 143msecs
Creating 8 trees of depth 20
	Bottom up construction took 142msecs
Creating 8 trees of depth 20
	Top down construction took 149msecs
	Top down construction took 146msecs
	Bottom up construction took 142msecs
Creating 2 trees of depth 22
	Bottom up construction took 142msecs
Creating 2 trees of depth 22
	Top down construction took 305msecs
	Top down construction took 305msecs
	Bottom up construction took 460msecs
 Total memory available=2372927488 bytes  Free memory=910270560 bytes
[1] Completed in 3894ms.
	Bottom up construction took 462msecs
 Total memory available=2372927488 bytes  Free memory=880774240 bytes
[0] Completed in 3900ms.
[harness] Finished all threads
