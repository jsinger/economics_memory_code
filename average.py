import sys

factor = 1.0
if len(sys.argv) > 1:
    factor = float(sys.argv[1])

i = 0
n = 0

for line in sys.stdin:
    i += int(line)
    n += 1

# Factor allows for conversion between units
print (float(i) / float(n)) / factor
