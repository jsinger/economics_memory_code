#include "Client.hpp"
#include "ClientList.hpp"
#include "ReadingThread.hpp"

ReadingThread::ReadingThread(economem::ReadingReceiver& receiver, ClientList& clients)
    : ReceiverThread(receiver, "ReadingThread"), m_Clients(clients)
{

}

void ReadingThread::received(const economem::Reading& reading)
{
    switch (reading.type)
    {
    case economem::READINGTYPE_READING:
    {
        m_Clients.lock();
        reading.print();
        size_t i = m_Clients.count();
        m_Clients.addReading(reading);
        if (m_Clients.count() != i)
            printf("Clients: %zu\n", m_Clients.count());
        m_Clients.unlock();
    }
    break;
    case economem::READINGTYPE_DEATH:
    {
        m_Clients.lock();
        printf("Client %d died\n", reading.sender);
        m_Clients.clientDied(reading.sender);
        printf("Clients: %zu\n", m_Clients.count());
        m_Clients.unlock();
    }
    break;
    case economem::READINGTYPE_MINHEAP:
    {
        m_Clients.lock();
        Client* c = m_Clients.getClientByPid(reading.sender);
        if (c == NULL)
            printf("Received minimum heap notification for unknown client pid %d\n", reading.sender);
        else
        {
            c->setMinHeapMB(reading.heapSizeMB);
            printf("Client %d has a minimum heap size of %llu MB\n", reading.sender, reading.heapSizeMB);
        }
        m_Clients.unlock();
    }
    break;
    default:
        economem::warning("Unknown reading type %u", (unsigned int)reading.type);
        break;
    }
}
