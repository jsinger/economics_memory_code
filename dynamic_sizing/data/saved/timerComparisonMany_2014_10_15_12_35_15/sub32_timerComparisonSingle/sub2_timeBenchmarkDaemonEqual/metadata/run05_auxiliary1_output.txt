economemd
    total memory: 3705 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub32_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run05_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 13977: heap size 9 MB, throughput 0.989373
Clients: 1
Client 13977 has a minimum heap size of 12 MB
Reading from 13976: heap size 9 MB, throughput 0.992606
Clients: 2
Client 13976 has a minimum heap size of 1223 MB
Reading from 13976: heap size 9 MB, throughput 0.976152
Reading from 13977: heap size 9 MB, throughput 0.985392
Reading from 13976: heap size 9 MB, throughput 0.935009
Reading from 13977: heap size 9 MB, throughput 0.973047
Reading from 13976: heap size 9 MB, throughput 0.927223
Reading from 13977: heap size 9 MB, throughput 0.97756
Reading from 13976: heap size 11 MB, throughput 0.981389
Reading from 13977: heap size 11 MB, throughput 0.97737
Reading from 13977: heap size 11 MB, throughput 0.983222
Reading from 13976: heap size 11 MB, throughput 0.981009
Reading from 13976: heap size 17 MB, throughput 0.955331
Reading from 13977: heap size 16 MB, throughput 0.964658
Reading from 13976: heap size 17 MB, throughput 0.491195
Reading from 13977: heap size 16 MB, throughput 0.975601
Reading from 13976: heap size 30 MB, throughput 0.901813
Reading from 13976: heap size 31 MB, throughput 0.819138
Reading from 13977: heap size 24 MB, throughput 0.986473
Reading from 13976: heap size 34 MB, throughput 0.40613
Reading from 13976: heap size 46 MB, throughput 0.911058
Reading from 13976: heap size 49 MB, throughput 0.875713
Reading from 13977: heap size 25 MB, throughput 0.881678
Reading from 13976: heap size 50 MB, throughput 0.200904
Reading from 13976: heap size 74 MB, throughput 0.826984
Reading from 13977: heap size 40 MB, throughput 0.990503
Reading from 13976: heap size 74 MB, throughput 0.292314
Reading from 13976: heap size 100 MB, throughput 0.808289
Reading from 13976: heap size 101 MB, throughput 0.742575
Reading from 13977: heap size 41 MB, throughput 0.98916
Reading from 13976: heap size 103 MB, throughput 0.755011
Reading from 13977: heap size 46 MB, throughput 0.990663
Reading from 13976: heap size 106 MB, throughput 0.250346
Reading from 13977: heap size 47 MB, throughput 0.983792
Reading from 13976: heap size 132 MB, throughput 0.720595
Reading from 13976: heap size 136 MB, throughput 0.752408
Reading from 13977: heap size 53 MB, throughput 0.986709
Reading from 13976: heap size 138 MB, throughput 0.757371
Reading from 13977: heap size 53 MB, throughput 0.97772
Reading from 13977: heap size 62 MB, throughput 0.977004
Reading from 13977: heap size 62 MB, throughput 0.947798
Reading from 13976: heap size 140 MB, throughput 0.101223
Reading from 13977: heap size 69 MB, throughput 0.939888
Reading from 13976: heap size 178 MB, throughput 0.667494
Reading from 13976: heap size 181 MB, throughput 0.7044
Reading from 13976: heap size 183 MB, throughput 0.697912
Reading from 13977: heap size 70 MB, throughput 0.983132
Reading from 13976: heap size 189 MB, throughput 0.721978
Reading from 13976: heap size 192 MB, throughput 0.721102
Reading from 13976: heap size 198 MB, throughput 0.632415
Reading from 13976: heap size 201 MB, throughput 0.67496
Reading from 13976: heap size 210 MB, throughput 0.585776
Reading from 13976: heap size 215 MB, throughput 0.614722
Reading from 13976: heap size 225 MB, throughput 0.551943
Reading from 13976: heap size 228 MB, throughput 0.442313
Reading from 13977: heap size 81 MB, throughput 0.985902
Reading from 13976: heap size 238 MB, throughput 0.0575996
Reading from 13977: heap size 82 MB, throughput 0.992591
Reading from 13976: heap size 278 MB, throughput 0.566647
Reading from 13976: heap size 282 MB, throughput 0.0842954
Reading from 13976: heap size 316 MB, throughput 0.361932
Reading from 13976: heap size 245 MB, throughput 0.657613
Reading from 13977: heap size 92 MB, throughput 0.996383
Reading from 13976: heap size 312 MB, throughput 0.623671
Reading from 13976: heap size 258 MB, throughput 0.649015
Reading from 13976: heap size 305 MB, throughput 0.627899
Reading from 13976: heap size 309 MB, throughput 0.680674
Reading from 13976: heap size 309 MB, throughput 0.542398
Reading from 13976: heap size 311 MB, throughput 0.43362
Reading from 13976: heap size 315 MB, throughput 0.53149
Reading from 13977: heap size 94 MB, throughput 0.994327
Reading from 13976: heap size 317 MB, throughput 0.082739
Reading from 13977: heap size 104 MB, throughput 0.996278
Reading from 13976: heap size 367 MB, throughput 0.42743
Reading from 13976: heap size 368 MB, throughput 0.479825
Reading from 13976: heap size 365 MB, throughput 0.587287
Reading from 13976: heap size 367 MB, throughput 0.529073
Reading from 13976: heap size 368 MB, throughput 0.559833
Reading from 13976: heap size 371 MB, throughput 0.502675
Reading from 13977: heap size 105 MB, throughput 0.996019
Reading from 13976: heap size 375 MB, throughput 0.482391
Reading from 13976: heap size 379 MB, throughput 0.491271
Reading from 13977: heap size 112 MB, throughput 0.995823
Reading from 13976: heap size 385 MB, throughput 0.0811968
Equal recommendation: 1852 MB each
Reading from 13976: heap size 435 MB, throughput 0.471926
Reading from 13976: heap size 434 MB, throughput 0.561519
Reading from 13976: heap size 438 MB, throughput 0.473025
Reading from 13977: heap size 113 MB, throughput 0.995739
Reading from 13976: heap size 441 MB, throughput 0.547465
Reading from 13976: heap size 443 MB, throughput 0.480925
Reading from 13977: heap size 117 MB, throughput 0.994712
Reading from 13976: heap size 449 MB, throughput 0.104228
Reading from 13976: heap size 506 MB, throughput 0.512604
Reading from 13976: heap size 511 MB, throughput 0.492017
Reading from 13977: heap size 119 MB, throughput 0.995494
Reading from 13976: heap size 512 MB, throughput 0.547901
Reading from 13976: heap size 517 MB, throughput 0.583694
Reading from 13976: heap size 520 MB, throughput 0.586555
Reading from 13976: heap size 526 MB, throughput 0.569437
Reading from 13977: heap size 124 MB, throughput 0.996621
Reading from 13976: heap size 530 MB, throughput 0.109405
Reading from 13977: heap size 125 MB, throughput 0.994715
Reading from 13976: heap size 592 MB, throughput 0.445656
Reading from 13976: heap size 600 MB, throughput 0.523723
Reading from 13976: heap size 602 MB, throughput 0.448882
Reading from 13976: heap size 607 MB, throughput 0.502155
Reading from 13977: heap size 130 MB, throughput 0.996182
Reading from 13976: heap size 613 MB, throughput 0.490377
Reading from 13977: heap size 130 MB, throughput 0.995827
Reading from 13976: heap size 618 MB, throughput 0.0834384
Reading from 13977: heap size 135 MB, throughput 0.995423
Reading from 13976: heap size 687 MB, throughput 0.788728
Reading from 13977: heap size 135 MB, throughput 0.995952
Reading from 13976: heap size 690 MB, throughput 0.858683
Reading from 13976: heap size 691 MB, throughput 0.898383
Reading from 13977: heap size 139 MB, throughput 0.997301
Reading from 13976: heap size 694 MB, throughput 0.645896
Reading from 13976: heap size 701 MB, throughput 0.630971
Reading from 13976: heap size 709 MB, throughput 0.606703
Reading from 13977: heap size 139 MB, throughput 0.996925
Equal recommendation: 1852 MB each
Reading from 13977: heap size 143 MB, throughput 0.997605
Reading from 13976: heap size 719 MB, throughput 0.0263306
Reading from 13976: heap size 792 MB, throughput 0.301645
Reading from 13976: heap size 783 MB, throughput 0.436037
Reading from 13977: heap size 143 MB, throughput 0.998144
Reading from 13976: heap size 790 MB, throughput 0.856123
Reading from 13976: heap size 780 MB, throughput 0.611556
Reading from 13976: heap size 786 MB, throughput 0.568344
Reading from 13977: heap size 145 MB, throughput 0.998684
Reading from 13977: heap size 146 MB, throughput 0.998312
Reading from 13976: heap size 786 MB, throughput 0.0691326
Reading from 13976: heap size 863 MB, throughput 0.697313
Reading from 13976: heap size 868 MB, throughput 0.663993
Reading from 13977: heap size 148 MB, throughput 0.996579
Reading from 13977: heap size 149 MB, throughput 0.485059
Reading from 13976: heap size 870 MB, throughput 0.687177
Reading from 13976: heap size 876 MB, throughput 0.851856
Reading from 13977: heap size 155 MB, throughput 0.995181
Reading from 13977: heap size 155 MB, throughput 0.995369
Reading from 13976: heap size 878 MB, throughput 0.133497
Reading from 13976: heap size 972 MB, throughput 0.784653
Reading from 13976: heap size 975 MB, throughput 0.852013
Reading from 13977: heap size 166 MB, throughput 0.998328
Reading from 13976: heap size 972 MB, throughput 0.857774
Reading from 13976: heap size 976 MB, throughput 0.865554
Reading from 13976: heap size 967 MB, throughput 0.873007
Reading from 13976: heap size 972 MB, throughput 0.87338
Reading from 13976: heap size 961 MB, throughput 0.879446
Reading from 13977: heap size 166 MB, throughput 0.997975
Reading from 13976: heap size 967 MB, throughput 0.858692
Reading from 13976: heap size 958 MB, throughput 0.891472
Reading from 13976: heap size 963 MB, throughput 0.885566
Reading from 13976: heap size 951 MB, throughput 0.896537
Reading from 13977: heap size 173 MB, throughput 0.998172
Reading from 13977: heap size 175 MB, throughput 0.997955
Reading from 13976: heap size 957 MB, throughput 0.986411
Equal recommendation: 1852 MB each
Reading from 13976: heap size 947 MB, throughput 0.93941
Reading from 13976: heap size 953 MB, throughput 0.813407
Reading from 13976: heap size 950 MB, throughput 0.819113
Reading from 13977: heap size 181 MB, throughput 0.998176
Reading from 13976: heap size 953 MB, throughput 0.838098
Reading from 13976: heap size 950 MB, throughput 0.830864
Reading from 13976: heap size 953 MB, throughput 0.834484
Reading from 13976: heap size 951 MB, throughput 0.846457
Reading from 13976: heap size 954 MB, throughput 0.814943
Reading from 13977: heap size 182 MB, throughput 0.997871
Reading from 13976: heap size 954 MB, throughput 0.897273
Reading from 13976: heap size 956 MB, throughput 0.89054
Reading from 13976: heap size 957 MB, throughput 0.905006
Reading from 13976: heap size 960 MB, throughput 0.734095
Reading from 13977: heap size 187 MB, throughput 0.998191
Reading from 13976: heap size 947 MB, throughput 0.667842
Reading from 13976: heap size 967 MB, throughput 0.675428
Reading from 13977: heap size 188 MB, throughput 0.992624
Reading from 13977: heap size 193 MB, throughput 0.998267
Reading from 13976: heap size 977 MB, throughput 0.0604678
Reading from 13976: heap size 1088 MB, throughput 0.501839
Reading from 13976: heap size 1089 MB, throughput 0.734073
Reading from 13976: heap size 1091 MB, throughput 0.702748
Reading from 13976: heap size 1096 MB, throughput 0.703445
Reading from 13976: heap size 1099 MB, throughput 0.722669
Reading from 13976: heap size 1101 MB, throughput 0.730995
Reading from 13977: heap size 193 MB, throughput 0.998192
Reading from 13976: heap size 1105 MB, throughput 0.72805
Reading from 13977: heap size 199 MB, throughput 0.997681
Reading from 13976: heap size 1109 MB, throughput 0.897099
Reading from 13977: heap size 200 MB, throughput 0.997976
Equal recommendation: 1852 MB each
Reading from 13977: heap size 206 MB, throughput 0.997863
Reading from 13976: heap size 1113 MB, throughput 0.974581
Reading from 13977: heap size 206 MB, throughput 0.996325
Reading from 13977: heap size 210 MB, throughput 0.989758
Reading from 13977: heap size 211 MB, throughput 0.996058
Reading from 13976: heap size 1121 MB, throughput 0.973446
Reading from 13977: heap size 221 MB, throughput 0.997765
Reading from 13977: heap size 222 MB, throughput 0.997891
Reading from 13976: heap size 1124 MB, throughput 0.976636
Reading from 13977: heap size 229 MB, throughput 0.997138
Reading from 13977: heap size 230 MB, throughput 0.997168
Equal recommendation: 1852 MB each
Reading from 13976: heap size 1134 MB, throughput 0.963144
Reading from 13977: heap size 237 MB, throughput 0.997654
Reading from 13977: heap size 237 MB, throughput 0.997994
Reading from 13976: heap size 1138 MB, throughput 0.982625
Reading from 13977: heap size 243 MB, throughput 0.998346
Reading from 13977: heap size 244 MB, throughput 0.998058
Reading from 13976: heap size 1144 MB, throughput 0.978438
Reading from 13977: heap size 250 MB, throughput 0.997599
Reading from 13977: heap size 251 MB, throughput 0.997314
Equal recommendation: 1852 MB each
Reading from 13977: heap size 257 MB, throughput 0.995324
Reading from 13976: heap size 1146 MB, throughput 0.973516
Reading from 13977: heap size 257 MB, throughput 0.994332
Reading from 13977: heap size 266 MB, throughput 0.997502
Reading from 13976: heap size 1143 MB, throughput 0.977898
Reading from 13977: heap size 266 MB, throughput 0.997469
Reading from 13977: heap size 274 MB, throughput 0.997997
Reading from 13976: heap size 1147 MB, throughput 0.979567
Reading from 13977: heap size 275 MB, throughput 0.998433
Equal recommendation: 1852 MB each
Reading from 13977: heap size 282 MB, throughput 0.997693
Reading from 13976: heap size 1144 MB, throughput 0.978867
Reading from 13977: heap size 283 MB, throughput 0.99805
Reading from 13977: heap size 289 MB, throughput 0.998103
Reading from 13976: heap size 1146 MB, throughput 0.978663
Reading from 13977: heap size 290 MB, throughput 0.997878
Reading from 13977: heap size 297 MB, throughput 0.996779
Reading from 13977: heap size 297 MB, throughput 0.994405
Reading from 13976: heap size 1149 MB, throughput 0.978514
Equal recommendation: 1852 MB each
Reading from 13977: heap size 305 MB, throughput 0.99659
Reading from 13976: heap size 1150 MB, throughput 0.975965
Reading from 13977: heap size 306 MB, throughput 0.995784
Reading from 13977: heap size 316 MB, throughput 0.997697
Reading from 13976: heap size 1153 MB, throughput 0.979224
Reading from 13977: heap size 316 MB, throughput 0.997826
Reading from 13977: heap size 325 MB, throughput 0.998147
Reading from 13976: heap size 1156 MB, throughput 0.9787
Equal recommendation: 1852 MB each
Reading from 13977: heap size 326 MB, throughput 0.998159
Reading from 13977: heap size 334 MB, throughput 0.998675
Reading from 13976: heap size 1158 MB, throughput 0.977659
Reading from 13977: heap size 335 MB, throughput 0.99725
Reading from 13977: heap size 343 MB, throughput 0.995298
Reading from 13976: heap size 1163 MB, throughput 0.693581
Reading from 13977: heap size 343 MB, throughput 0.998004
Equal recommendation: 1852 MB each
Reading from 13977: heap size 354 MB, throughput 0.997723
Reading from 13976: heap size 1237 MB, throughput 0.995525
Reading from 13977: heap size 355 MB, throughput 0.99782
Reading from 13976: heap size 1239 MB, throughput 0.993415
Reading from 13977: heap size 364 MB, throughput 0.998348
Reading from 13977: heap size 364 MB, throughput 0.997435
Reading from 13976: heap size 1247 MB, throughput 0.992807
Equal recommendation: 1852 MB each
Reading from 13977: heap size 373 MB, throughput 0.997645
Reading from 13977: heap size 373 MB, throughput 0.997276
Reading from 13976: heap size 1252 MB, throughput 0.991459
Reading from 13977: heap size 383 MB, throughput 0.996781
Reading from 13977: heap size 383 MB, throughput 0.998304
Reading from 13976: heap size 1254 MB, throughput 0.990099
Reading from 13977: heap size 393 MB, throughput 0.99808
Equal recommendation: 1852 MB each
Reading from 13976: heap size 1152 MB, throughput 0.986138
Reading from 13977: heap size 394 MB, throughput 0.998304
Reading from 13977: heap size 402 MB, throughput 0.99812
Reading from 13976: heap size 1244 MB, throughput 0.986618
Reading from 13977: heap size 403 MB, throughput 0.997289
Reading from 13976: heap size 1169 MB, throughput 0.986269
Equal recommendation: 1852 MB each
Reading from 13977: heap size 412 MB, throughput 0.997923
Reading from 13977: heap size 412 MB, throughput 0.937425
Reading from 13976: heap size 1233 MB, throughput 0.983427
Reading from 13977: heap size 428 MB, throughput 0.999293
Reading from 13976: heap size 1239 MB, throughput 0.983568
Reading from 13977: heap size 429 MB, throughput 0.999014
Equal recommendation: 1852 MB each
Reading from 13976: heap size 1238 MB, throughput 0.983849
Reading from 13977: heap size 441 MB, throughput 0.999089
Reading from 13977: heap size 442 MB, throughput 0.999065
Reading from 13976: heap size 1239 MB, throughput 0.98312
Reading from 13977: heap size 452 MB, throughput 0.999221
Reading from 13977: heap size 453 MB, throughput 0.994916
Reading from 13976: heap size 1240 MB, throughput 0.965865
Equal recommendation: 1852 MB each
Reading from 13977: heap size 462 MB, throughput 0.998862
Reading from 13976: heap size 1244 MB, throughput 0.977409
Reading from 13977: heap size 463 MB, throughput 0.998801
Reading from 13976: heap size 1247 MB, throughput 0.977316
Reading from 13977: heap size 474 MB, throughput 0.998584
Reading from 13976: heap size 1253 MB, throughput 0.97597
Equal recommendation: 1852 MB each
Reading from 13977: heap size 476 MB, throughput 0.99869
Reading from 13976: heap size 1259 MB, throughput 0.974201
Reading from 13977: heap size 487 MB, throughput 0.998182
Reading from 13977: heap size 488 MB, throughput 0.995834
Reading from 13976: heap size 1265 MB, throughput 0.973327
Reading from 13977: heap size 499 MB, throughput 0.998354
Equal recommendation: 1852 MB each
Reading from 13976: heap size 1270 MB, throughput 0.976611
Reading from 13977: heap size 500 MB, throughput 0.998238
Reading from 13976: heap size 1273 MB, throughput 0.974589
Reading from 13977: heap size 514 MB, throughput 0.998552
Reading from 13976: heap size 1279 MB, throughput 0.976526
Reading from 13977: heap size 515 MB, throughput 0.99862
Equal recommendation: 1852 MB each
Reading from 13977: heap size 528 MB, throughput 0.998392
Reading from 13976: heap size 1280 MB, throughput 0.96866
Reading from 13977: heap size 529 MB, throughput 0.996368
Reading from 13976: heap size 1285 MB, throughput 0.977542
Reading from 13977: heap size 543 MB, throughput 0.998526
Equal recommendation: 1852 MB each
Reading from 13976: heap size 1285 MB, throughput 0.972487
Reading from 13977: heap size 544 MB, throughput 0.998482
Reading from 13976: heap size 1288 MB, throughput 0.978761
Reading from 13977: heap size 558 MB, throughput 0.998558
Reading from 13976: heap size 1289 MB, throughput 0.977861
Reading from 13977: heap size 559 MB, throughput 0.997621
Equal recommendation: 1852 MB each
Reading from 13977: heap size 574 MB, throughput 0.997202
Reading from 13976: heap size 1290 MB, throughput 0.977804
Reading from 13977: heap size 574 MB, throughput 0.998013
Reading from 13976: heap size 1291 MB, throughput 0.977074
Reading from 13977: heap size 592 MB, throughput 0.998465
Equal recommendation: 1852 MB each
Reading from 13977: heap size 593 MB, throughput 0.998069
Reading from 13976: heap size 1291 MB, throughput 0.63129
Reading from 13977: heap size 610 MB, throughput 0.998038
Reading from 13977: heap size 611 MB, throughput 0.997867
Equal recommendation: 1852 MB each
Reading from 13977: heap size 629 MB, throughput 0.998743
Reading from 13977: heap size 629 MB, throughput 0.998599
Equal recommendation: 1852 MB each
Reading from 13976: heap size 1412 MB, throughput 0.987167
Reading from 13977: heap size 644 MB, throughput 0.998666
Reading from 13977: heap size 646 MB, throughput 0.996728
Reading from 13977: heap size 661 MB, throughput 0.998667
Equal recommendation: 1852 MB each
Reading from 13977: heap size 662 MB, throughput 0.998765
Reading from 13976: heap size 1400 MB, throughput 0.994281
Reading from 13977: heap size 678 MB, throughput 0.998664
Equal recommendation: 1852 MB each
Reading from 13976: heap size 1416 MB, throughput 0.987022
Reading from 13977: heap size 680 MB, throughput 0.997538
Reading from 13976: heap size 1420 MB, throughput 0.865599
Reading from 13976: heap size 1428 MB, throughput 0.768148
Reading from 13976: heap size 1410 MB, throughput 0.714132
Reading from 13976: heap size 1442 MB, throughput 0.712989
Reading from 13976: heap size 1465 MB, throughput 0.707743
Reading from 13976: heap size 1474 MB, throughput 0.716982
Reading from 13976: heap size 1502 MB, throughput 0.755351
Reading from 13976: heap size 1507 MB, throughput 0.792475
Reading from 13977: heap size 696 MB, throughput 0.998533
Reading from 13976: heap size 1522 MB, throughput 0.979939
Reading from 13977: heap size 697 MB, throughput 0.969564
Equal recommendation: 1852 MB each
Reading from 13976: heap size 1530 MB, throughput 0.982905
Reading from 13977: heap size 715 MB, throughput 0.999218
Reading from 13976: heap size 1528 MB, throughput 0.984313
Client 13977 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 13976: heap size 1540 MB, throughput 0.982284
Reading from 13976: heap size 1532 MB, throughput 0.859036
Recommendation: one client; give it all the memory
Reading from 13976: heap size 1477 MB, throughput 0.996234
Reading from 13976: heap size 1480 MB, throughput 0.994729
Reading from 13976: heap size 1495 MB, throughput 0.993791
Recommendation: one client; give it all the memory
Reading from 13976: heap size 1514 MB, throughput 0.992459
Reading from 13976: heap size 1517 MB, throughput 0.991063
Recommendation: one client; give it all the memory
Reading from 13976: heap size 1513 MB, throughput 0.990351
Reading from 13976: heap size 1519 MB, throughput 0.989616
Recommendation: one client; give it all the memory
Reading from 13976: heap size 1499 MB, throughput 0.98853
Reading from 13976: heap size 1369 MB, throughput 0.988466
Recommendation: one client; give it all the memory
Reading from 13976: heap size 1480 MB, throughput 0.987691
Reading from 13976: heap size 1396 MB, throughput 0.986727
Recommendation: one client; give it all the memory
Reading from 13976: heap size 1487 MB, throughput 0.985155
Reading from 13976: heap size 1490 MB, throughput 0.983923
Recommendation: one client; give it all the memory
Reading from 13976: heap size 1493 MB, throughput 0.983581
Recommendation: one client; give it all the memory
Reading from 13976: heap size 1497 MB, throughput 0.982715
Reading from 13976: heap size 1502 MB, throughput 0.981517
Recommendation: one client; give it all the memory
Reading from 13976: heap size 1510 MB, throughput 0.980376
Reading from 13976: heap size 1520 MB, throughput 0.981037
Recommendation: one client; give it all the memory
Reading from 13976: heap size 1524 MB, throughput 0.980953
Reading from 13976: heap size 1533 MB, throughput 0.98107
Recommendation: one client; give it all the memory
Reading from 13976: heap size 1536 MB, throughput 0.98019
Reading from 13976: heap size 1546 MB, throughput 0.980382
Recommendation: one client; give it all the memory
Reading from 13976: heap size 1547 MB, throughput 0.980936
Reading from 13976: heap size 1556 MB, throughput 0.98148
Recommendation: one client; give it all the memory
Reading from 13976: heap size 1556 MB, throughput 0.982237
Recommendation: one client; give it all the memory
Reading from 13976: heap size 1564 MB, throughput 0.981481
Recommendation: one client; give it all the memory
Reading from 13976: heap size 1564 MB, throughput 0.989894
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 13976: heap size 1556 MB, throughput 0.990511
Reading from 13976: heap size 1573 MB, throughput 0.980993
Reading from 13976: heap size 1589 MB, throughput 0.830414
Reading from 13976: heap size 1592 MB, throughput 0.747132
Reading from 13976: heap size 1621 MB, throughput 0.788246
Reading from 13976: heap size 1638 MB, throughput 0.775968
Client 13976 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
