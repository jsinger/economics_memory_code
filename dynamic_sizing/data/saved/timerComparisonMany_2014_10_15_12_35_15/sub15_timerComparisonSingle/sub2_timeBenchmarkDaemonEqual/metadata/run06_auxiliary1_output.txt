economemd
    total memory: 7338 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub15_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 15204: heap size 9 MB, throughput 0.989263
Clients: 1
Client 15204 has a minimum heap size of 12 MB
Reading from 15203: heap size 9 MB, throughput 0.985925
Clients: 2
Client 15203 has a minimum heap size of 1211 MB
Reading from 15204: heap size 9 MB, throughput 0.987697
Reading from 15203: heap size 9 MB, throughput 0.96571
Reading from 15204: heap size 9 MB, throughput 0.948034
Reading from 15204: heap size 9 MB, throughput 0.959645
Reading from 15203: heap size 11 MB, throughput 0.980644
Reading from 15204: heap size 11 MB, throughput 0.95885
Reading from 15203: heap size 11 MB, throughput 0.972428
Reading from 15204: heap size 11 MB, throughput 0.974208
Reading from 15203: heap size 15 MB, throughput 0.844351
Reading from 15204: heap size 16 MB, throughput 0.983298
Reading from 15203: heap size 18 MB, throughput 0.966217
Reading from 15203: heap size 24 MB, throughput 0.956964
Reading from 15203: heap size 28 MB, throughput 0.941185
Reading from 15204: heap size 16 MB, throughput 0.983224
Reading from 15203: heap size 31 MB, throughput 0.277424
Reading from 15203: heap size 41 MB, throughput 0.868742
Reading from 15203: heap size 45 MB, throughput 0.854723
Reading from 15204: heap size 24 MB, throughput 0.985559
Reading from 15203: heap size 47 MB, throughput 0.342552
Reading from 15203: heap size 62 MB, throughput 0.866077
Reading from 15204: heap size 25 MB, throughput 0.88867
Reading from 15203: heap size 65 MB, throughput 0.798463
Reading from 15203: heap size 67 MB, throughput 0.823874
Reading from 15203: heap size 70 MB, throughput 0.792987
Reading from 15204: heap size 39 MB, throughput 0.966905
Reading from 15204: heap size 40 MB, throughput 0.962571
Reading from 15203: heap size 74 MB, throughput 0.107867
Reading from 15203: heap size 101 MB, throughput 0.729158
Reading from 15203: heap size 105 MB, throughput 0.69533
Reading from 15203: heap size 107 MB, throughput 0.601131
Reading from 15204: heap size 49 MB, throughput 0.989189
Reading from 15203: heap size 110 MB, throughput 0.59747
Reading from 15204: heap size 49 MB, throughput 0.990024
Reading from 15204: heap size 60 MB, throughput 0.989095
Reading from 15203: heap size 115 MB, throughput 0.190081
Reading from 15203: heap size 145 MB, throughput 0.672591
Reading from 15204: heap size 60 MB, throughput 0.991303
Reading from 15203: heap size 148 MB, throughput 0.642825
Reading from 15204: heap size 71 MB, throughput 0.987209
Reading from 15203: heap size 150 MB, throughput 0.833492
Reading from 15203: heap size 154 MB, throughput 0.564955
Reading from 15204: heap size 72 MB, throughput 0.975775
Reading from 15203: heap size 156 MB, throughput 0.641951
Reading from 15203: heap size 162 MB, throughput 0.488476
Reading from 15203: heap size 165 MB, throughput 0.383591
Reading from 15203: heap size 173 MB, throughput 0.50246
Reading from 15204: heap size 83 MB, throughput 0.979323
Reading from 15203: heap size 180 MB, throughput 0.127239
Reading from 15203: heap size 215 MB, throughput 0.498858
Reading from 15203: heap size 214 MB, throughput 0.726293
Reading from 15204: heap size 83 MB, throughput 0.997525
Reading from 15203: heap size 217 MB, throughput 0.140878
Reading from 15203: heap size 249 MB, throughput 0.548849
Reading from 15203: heap size 251 MB, throughput 0.689142
Reading from 15203: heap size 246 MB, throughput 0.638492
Reading from 15203: heap size 249 MB, throughput 0.679539
Reading from 15203: heap size 250 MB, throughput 0.50968
Reading from 15204: heap size 93 MB, throughput 0.996806
Reading from 15203: heap size 253 MB, throughput 0.559363
Reading from 15203: heap size 255 MB, throughput 0.518703
Reading from 15203: heap size 261 MB, throughput 0.589376
Reading from 15203: heap size 265 MB, throughput 0.551551
Reading from 15203: heap size 271 MB, throughput 0.492283
Reading from 15204: heap size 95 MB, throughput 0.997344
Reading from 15203: heap size 275 MB, throughput 0.519277
Reading from 15203: heap size 281 MB, throughput 0.085718
Reading from 15203: heap size 325 MB, throughput 0.524057
Reading from 15203: heap size 329 MB, throughput 0.675183
Reading from 15204: heap size 104 MB, throughput 0.993928
Reading from 15203: heap size 327 MB, throughput 0.0948005
Reading from 15203: heap size 372 MB, throughput 0.527113
Reading from 15204: heap size 106 MB, throughput 0.996541
Reading from 15203: heap size 364 MB, throughput 0.556683
Reading from 15203: heap size 369 MB, throughput 0.568975
Reading from 15203: heap size 370 MB, throughput 0.566186
Reading from 15203: heap size 372 MB, throughput 0.58949
Reading from 15203: heap size 374 MB, throughput 0.630019
Reading from 15203: heap size 380 MB, throughput 0.545876
Reading from 15203: heap size 386 MB, throughput 0.542123
Reading from 15204: heap size 114 MB, throughput 0.996422
Reading from 15203: heap size 392 MB, throughput 0.538382
Reading from 15203: heap size 396 MB, throughput 0.55209
Equal recommendation: 3669 MB each
Reading from 15204: heap size 116 MB, throughput 0.991859
Reading from 15203: heap size 401 MB, throughput 0.064921
Reading from 15203: heap size 454 MB, throughput 0.375754
Reading from 15203: heap size 456 MB, throughput 0.379593
Reading from 15203: heap size 459 MB, throughput 0.428095
Reading from 15204: heap size 124 MB, throughput 0.997845
Reading from 15203: heap size 459 MB, throughput 0.471067
Reading from 15204: heap size 124 MB, throughput 0.995606
Reading from 15203: heap size 463 MB, throughput 0.0898665
Reading from 15203: heap size 519 MB, throughput 0.347785
Reading from 15203: heap size 512 MB, throughput 0.57986
Reading from 15203: heap size 516 MB, throughput 0.53318
Reading from 15204: heap size 131 MB, throughput 0.996115
Reading from 15203: heap size 510 MB, throughput 0.58559
Reading from 15204: heap size 131 MB, throughput 0.995182
Reading from 15203: heap size 514 MB, throughput 0.109493
Reading from 15203: heap size 575 MB, throughput 0.516832
Reading from 15203: heap size 576 MB, throughput 0.693583
Reading from 15203: heap size 577 MB, throughput 0.615633
Reading from 15203: heap size 582 MB, throughput 0.593007
Reading from 15203: heap size 584 MB, throughput 0.608193
Reading from 15204: heap size 138 MB, throughput 0.997365
Reading from 15203: heap size 596 MB, throughput 0.586203
Reading from 15203: heap size 599 MB, throughput 0.511807
Reading from 15203: heap size 610 MB, throughput 0.550152
Reading from 15203: heap size 617 MB, throughput 0.827582
Reading from 15204: heap size 138 MB, throughput 0.997113
Reading from 15203: heap size 626 MB, throughput 0.802639
Reading from 15204: heap size 145 MB, throughput 0.994529
Reading from 15204: heap size 145 MB, throughput 0.996074
Reading from 15203: heap size 628 MB, throughput 0.20425
Reading from 15203: heap size 703 MB, throughput 0.720494
Reading from 15204: heap size 152 MB, throughput 0.99693
Reading from 15203: heap size 711 MB, throughput 0.795712
Reading from 15203: heap size 718 MB, throughput 0.474272
Reading from 15203: heap size 725 MB, throughput 0.403164
Reading from 15204: heap size 152 MB, throughput 0.997601
Equal recommendation: 3669 MB each
Reading from 15204: heap size 157 MB, throughput 0.997731
Reading from 15203: heap size 726 MB, throughput 0.0636272
Reading from 15203: heap size 785 MB, throughput 0.363267
Reading from 15203: heap size 793 MB, throughput 0.352626
Reading from 15203: heap size 790 MB, throughput 0.392188
Reading from 15203: heap size 791 MB, throughput 0.40971
Reading from 15204: heap size 158 MB, throughput 0.997737
Reading from 15203: heap size 795 MB, throughput 0.0402521
Reading from 15203: heap size 878 MB, throughput 0.415803
Reading from 15203: heap size 883 MB, throughput 0.923835
Reading from 15203: heap size 886 MB, throughput 0.635399
Reading from 15204: heap size 163 MB, throughput 0.998731
Reading from 15203: heap size 882 MB, throughput 0.629323
Reading from 15203: heap size 885 MB, throughput 0.671914
Reading from 15203: heap size 885 MB, throughput 0.719734
Reading from 15203: heap size 886 MB, throughput 0.935228
Reading from 15203: heap size 889 MB, throughput 0.885137
Reading from 15203: heap size 890 MB, throughput 0.879565
Reading from 15203: heap size 882 MB, throughput 0.672159
Reading from 15204: heap size 163 MB, throughput 0.99738
Reading from 15204: heap size 168 MB, throughput 0.593675
Reading from 15203: heap size 887 MB, throughput 0.905867
Reading from 15203: heap size 875 MB, throughput 0.833881
Reading from 15204: heap size 170 MB, throughput 0.992612
Reading from 15203: heap size 710 MB, throughput 0.80007
Reading from 15203: heap size 855 MB, throughput 0.737332
Reading from 15203: heap size 760 MB, throughput 0.641425
Reading from 15203: heap size 849 MB, throughput 0.734014
Reading from 15204: heap size 180 MB, throughput 0.996029
Reading from 15204: heap size 181 MB, throughput 0.996807
Reading from 15203: heap size 765 MB, throughput 0.0610679
Reading from 15203: heap size 925 MB, throughput 0.566197
Reading from 15203: heap size 934 MB, throughput 0.705965
Reading from 15203: heap size 929 MB, throughput 0.730062
Reading from 15203: heap size 932 MB, throughput 0.750949
Reading from 15203: heap size 926 MB, throughput 0.695462
Reading from 15204: heap size 192 MB, throughput 0.996676
Reading from 15203: heap size 930 MB, throughput 0.649098
Reading from 15203: heap size 925 MB, throughput 0.761501
Reading from 15203: heap size 929 MB, throughput 0.940514
Reading from 15204: heap size 194 MB, throughput 0.998178
Equal recommendation: 3669 MB each
Reading from 15203: heap size 921 MB, throughput 0.964751
Reading from 15203: heap size 928 MB, throughput 0.759248
Reading from 15204: heap size 204 MB, throughput 0.99836
Reading from 15203: heap size 951 MB, throughput 0.81851
Reading from 15203: heap size 952 MB, throughput 0.799399
Reading from 15203: heap size 959 MB, throughput 0.830369
Reading from 15203: heap size 959 MB, throughput 0.823909
Reading from 15203: heap size 963 MB, throughput 0.834266
Reading from 15203: heap size 965 MB, throughput 0.781452
Reading from 15203: heap size 969 MB, throughput 0.817757
Reading from 15204: heap size 206 MB, throughput 0.998173
Reading from 15203: heap size 970 MB, throughput 0.890394
Reading from 15203: heap size 969 MB, throughput 0.907567
Reading from 15203: heap size 973 MB, throughput 0.852665
Reading from 15203: heap size 975 MB, throughput 0.726276
Reading from 15204: heap size 215 MB, throughput 0.995695
Reading from 15203: heap size 978 MB, throughput 0.0951526
Reading from 15204: heap size 216 MB, throughput 0.996241
Reading from 15203: heap size 1111 MB, throughput 0.583591
Reading from 15203: heap size 1114 MB, throughput 0.788507
Reading from 15203: heap size 1125 MB, throughput 0.814325
Reading from 15203: heap size 1125 MB, throughput 0.773054
Reading from 15203: heap size 1130 MB, throughput 0.757284
Reading from 15203: heap size 1133 MB, throughput 0.792161
Reading from 15203: heap size 1135 MB, throughput 0.77286
Reading from 15203: heap size 1139 MB, throughput 0.79053
Reading from 15204: heap size 227 MB, throughput 0.998413
Reading from 15204: heap size 227 MB, throughput 0.998111
Reading from 15203: heap size 1141 MB, throughput 0.959208
Reading from 15204: heap size 235 MB, throughput 0.998111
Equal recommendation: 3669 MB each
Reading from 15203: heap size 1146 MB, throughput 0.959644
Reading from 15204: heap size 236 MB, throughput 0.99832
Reading from 15203: heap size 1147 MB, throughput 0.934915
Reading from 15204: heap size 244 MB, throughput 0.996731
Reading from 15204: heap size 245 MB, throughput 0.982959
Reading from 15203: heap size 1151 MB, throughput 0.953458
Reading from 15204: heap size 253 MB, throughput 0.998075
Reading from 15203: heap size 1161 MB, throughput 0.955151
Reading from 15204: heap size 255 MB, throughput 0.998066
Reading from 15203: heap size 1164 MB, throughput 0.951493
Reading from 15204: heap size 267 MB, throughput 0.998045
Reading from 15203: heap size 1168 MB, throughput 0.965404
Equal recommendation: 3669 MB each
Reading from 15204: heap size 269 MB, throughput 0.998069
Reading from 15203: heap size 1174 MB, throughput 0.956937
Reading from 15204: heap size 280 MB, throughput 0.998271
Reading from 15204: heap size 281 MB, throughput 0.997803
Reading from 15203: heap size 1178 MB, throughput 0.962095
Reading from 15204: heap size 291 MB, throughput 0.998248
Reading from 15203: heap size 1179 MB, throughput 0.960959
Reading from 15204: heap size 292 MB, throughput 0.998168
Reading from 15203: heap size 1184 MB, throughput 0.960402
Reading from 15204: heap size 301 MB, throughput 0.99837
Equal recommendation: 3669 MB each
Reading from 15203: heap size 1187 MB, throughput 0.936689
Reading from 15204: heap size 302 MB, throughput 0.91747
Reading from 15203: heap size 1193 MB, throughput 0.961097
Reading from 15204: heap size 318 MB, throughput 0.998883
Reading from 15204: heap size 301 MB, throughput 0.998743
Reading from 15203: heap size 1196 MB, throughput 0.957264
Reading from 15204: heap size 287 MB, throughput 0.99907
Reading from 15203: heap size 1203 MB, throughput 0.95934
Reading from 15204: heap size 275 MB, throughput 0.99894
Reading from 15203: heap size 1207 MB, throughput 0.95708
Reading from 15204: heap size 263 MB, throughput 0.999048
Equal recommendation: 3669 MB each
Reading from 15203: heap size 1214 MB, throughput 0.960501
Reading from 15204: heap size 256 MB, throughput 0.998697
Reading from 15204: heap size 240 MB, throughput 0.998703
Reading from 15203: heap size 1219 MB, throughput 0.961368
Reading from 15204: heap size 230 MB, throughput 0.998503
Reading from 15203: heap size 1226 MB, throughput 0.958144
Reading from 15204: heap size 220 MB, throughput 0.998794
Reading from 15204: heap size 214 MB, throughput 0.998455
Reading from 15204: heap size 201 MB, throughput 0.994377
Reading from 15204: heap size 196 MB, throughput 0.9931
Reading from 15204: heap size 207 MB, throughput 0.995806
Reading from 15203: heap size 1229 MB, throughput 0.489836
Equal recommendation: 3669 MB each
Reading from 15204: heap size 218 MB, throughput 0.997878
Reading from 15203: heap size 1308 MB, throughput 0.98909
Reading from 15204: heap size 230 MB, throughput 0.997994
Reading from 15204: heap size 242 MB, throughput 0.998051
Reading from 15203: heap size 1309 MB, throughput 0.989343
Reading from 15204: heap size 231 MB, throughput 0.998295
Reading from 15203: heap size 1321 MB, throughput 0.985562
Reading from 15204: heap size 221 MB, throughput 0.998114
Reading from 15204: heap size 212 MB, throughput 0.997952
Reading from 15203: heap size 1325 MB, throughput 0.983582
Reading from 15204: heap size 203 MB, throughput 0.997915
Reading from 15204: heap size 194 MB, throughput 0.997698
Equal recommendation: 3669 MB each
Reading from 15203: heap size 1325 MB, throughput 0.982308
Reading from 15204: heap size 186 MB, throughput 0.997648
Reading from 15204: heap size 179 MB, throughput 0.997451
Reading from 15203: heap size 1328 MB, throughput 0.981376
Reading from 15204: heap size 172 MB, throughput 0.997462
Reading from 15204: heap size 165 MB, throughput 0.997341
Reading from 15204: heap size 158 MB, throughput 0.989383
Reading from 15204: heap size 169 MB, throughput 0.982629
Reading from 15203: heap size 1316 MB, throughput 0.97804
Reading from 15204: heap size 184 MB, throughput 0.997246
Reading from 15204: heap size 196 MB, throughput 0.998426
Reading from 15203: heap size 1232 MB, throughput 0.971692
Reading from 15204: heap size 211 MB, throughput 0.99849
Reading from 15204: heap size 224 MB, throughput 0.998537
Reading from 15203: heap size 1306 MB, throughput 0.970931
Equal recommendation: 3669 MB each
Reading from 15204: heap size 237 MB, throughput 0.998588
Reading from 15204: heap size 227 MB, throughput 0.998436
Reading from 15203: heap size 1313 MB, throughput 0.968868
Reading from 15204: heap size 218 MB, throughput 0.998382
Reading from 15204: heap size 209 MB, throughput 0.998266
Reading from 15203: heap size 1315 MB, throughput 0.969107
Reading from 15204: heap size 200 MB, throughput 0.998104
Reading from 15204: heap size 192 MB, throughput 0.997466
Reading from 15203: heap size 1315 MB, throughput 0.968587
Reading from 15204: heap size 184 MB, throughput 0.997945
Reading from 15204: heap size 176 MB, throughput 0.997865
Reading from 15203: heap size 1318 MB, throughput 0.966611
Reading from 15204: heap size 169 MB, throughput 0.997557
Equal recommendation: 3669 MB each
Reading from 15204: heap size 163 MB, throughput 0.995347
Reading from 15204: heap size 158 MB, throughput 0.989288
Reading from 15204: heap size 168 MB, throughput 0.991733
Reading from 15203: heap size 1323 MB, throughput 0.958858
Reading from 15204: heap size 182 MB, throughput 0.997956
Reading from 15204: heap size 192 MB, throughput 0.997812
Reading from 15203: heap size 1328 MB, throughput 0.962752
Reading from 15204: heap size 204 MB, throughput 0.998078
Reading from 15204: heap size 216 MB, throughput 0.997977
Reading from 15203: heap size 1335 MB, throughput 0.95735
Reading from 15204: heap size 227 MB, throughput 0.998222
Reading from 15204: heap size 217 MB, throughput 0.998033
Reading from 15203: heap size 1344 MB, throughput 0.959707
Reading from 15204: heap size 208 MB, throughput 0.997771
Equal recommendation: 3669 MB each
Reading from 15204: heap size 200 MB, throughput 0.997521
Reading from 15203: heap size 1351 MB, throughput 0.957389
Reading from 15204: heap size 192 MB, throughput 0.997529
Reading from 15204: heap size 184 MB, throughput 0.997437
Reading from 15204: heap size 176 MB, throughput 0.997177
Reading from 15203: heap size 1359 MB, throughput 0.962106
Reading from 15204: heap size 169 MB, throughput 0.996944
Reading from 15204: heap size 163 MB, throughput 0.996925
Reading from 15204: heap size 156 MB, throughput 0.996189
Reading from 15203: heap size 1363 MB, throughput 0.945945
Reading from 15204: heap size 150 MB, throughput 0.991482
Reading from 15204: heap size 161 MB, throughput 0.989459
Reading from 15204: heap size 173 MB, throughput 0.918449
Reading from 15204: heap size 183 MB, throughput 0.998699
Reading from 15203: heap size 1371 MB, throughput 0.959153
Equal recommendation: 3669 MB each
Reading from 15204: heap size 175 MB, throughput 0.998639
Reading from 15204: heap size 168 MB, throughput 0.998577
Reading from 15204: heap size 166 MB, throughput 0.998471
Reading from 15203: heap size 1373 MB, throughput 0.960651
Reading from 15204: heap size 156 MB, throughput 0.998673
Reading from 15204: heap size 154 MB, throughput 0.998112
Reading from 15204: heap size 144 MB, throughput 0.998293
Reading from 15204: heap size 143 MB, throughput 0.99812
Reading from 15204: heap size 134 MB, throughput 0.998295
Reading from 15204: heap size 133 MB, throughput 0.997915
Reading from 15204: heap size 124 MB, throughput 0.998046
Reading from 15204: heap size 123 MB, throughput 0.997996
Reading from 15204: heap size 116 MB, throughput 0.997954
Reading from 15204: heap size 115 MB, throughput 0.997664
Equal recommendation: 3669 MB each
Reading from 15204: heap size 108 MB, throughput 0.997808
Reading from 15204: heap size 107 MB, throughput 0.9976
Reading from 15204: heap size 101 MB, throughput 0.997748
Reading from 15204: heap size 100 MB, throughput 0.997441
Reading from 15204: heap size 95 MB, throughput 0.997711
Reading from 15204: heap size 94 MB, throughput 0.997081
Reading from 15204: heap size 89 MB, throughput 0.99701
Reading from 15204: heap size 88 MB, throughput 0.992792
Reading from 15204: heap size 85 MB, throughput 0.983144
Reading from 15204: heap size 92 MB, throughput 0.985206
Reading from 15204: heap size 101 MB, throughput 0.98378
Reading from 15204: heap size 112 MB, throughput 0.988946
Reading from 15204: heap size 122 MB, throughput 0.996611
Reading from 15204: heap size 135 MB, throughput 0.99698
Reading from 15204: heap size 147 MB, throughput 0.997385
Reading from 15204: heap size 160 MB, throughput 0.997553
Reading from 15204: heap size 168 MB, throughput 0.99754
Reading from 15204: heap size 159 MB, throughput 0.997409
Equal recommendation: 3669 MB each
Reading from 15204: heap size 153 MB, throughput 0.997364
Reading from 15204: heap size 148 MB, throughput 0.997119
Reading from 15204: heap size 142 MB, throughput 0.997035
Reading from 15204: heap size 137 MB, throughput 0.996451
Reading from 15204: heap size 132 MB, throughput 0.996296
Reading from 15204: heap size 127 MB, throughput 0.996497
Reading from 15204: heap size 122 MB, throughput 0.996727
Reading from 15204: heap size 118 MB, throughput 0.996837
Reading from 15204: heap size 114 MB, throughput 0.997548
Reading from 15204: heap size 109 MB, throughput 0.997666
Reading from 15204: heap size 106 MB, throughput 0.998054
Reading from 15204: heap size 106 MB, throughput 0.997655
Reading from 15204: heap size 99 MB, throughput 0.995831
Reading from 15203: heap size 1382 MB, throughput 0.994671
Reading from 15204: heap size 99 MB, throughput 0.997362
Reading from 15204: heap size 93 MB, throughput 0.997803
Reading from 15204: heap size 93 MB, throughput 0.997102
Reading from 15204: heap size 87 MB, throughput 0.997454
Equal recommendation: 3669 MB each
Reading from 15204: heap size 88 MB, throughput 0.992705
Reading from 15204: heap size 84 MB, throughput 0.983676
Reading from 15204: heap size 90 MB, throughput 0.985767
Reading from 15204: heap size 99 MB, throughput 0.985099
Reading from 15204: heap size 108 MB, throughput 0.986577
Reading from 15204: heap size 120 MB, throughput 0.996498
Reading from 15204: heap size 130 MB, throughput 0.996751
Reading from 15204: heap size 133 MB, throughput 0.996946
Reading from 15204: heap size 133 MB, throughput 0.996086
Reading from 15204: heap size 144 MB, throughput 0.996534
Reading from 15204: heap size 144 MB, throughput 0.996747
Reading from 15204: heap size 153 MB, throughput 0.997304
Reading from 15204: heap size 147 MB, throughput 0.997464
Reading from 15203: heap size 1382 MB, throughput 0.894097
Reading from 15204: heap size 141 MB, throughput 0.99743
Reading from 15203: heap size 1439 MB, throughput 0.995396
Reading from 15203: heap size 1472 MB, throughput 0.936088
Reading from 15204: heap size 136 MB, throughput 0.997217
Reading from 15203: heap size 1475 MB, throughput 0.900817
Reading from 15203: heap size 1481 MB, throughput 0.903258
Reading from 15204: heap size 131 MB, throughput 0.997027
Reading from 15203: heap size 1472 MB, throughput 0.907956
Reading from 15203: heap size 1478 MB, throughput 0.908663
Reading from 15203: heap size 1471 MB, throughput 0.914141
Reading from 15204: heap size 127 MB, throughput 0.997106
Equal recommendation: 3669 MB each
Reading from 15204: heap size 122 MB, throughput 0.996749
Reading from 15204: heap size 118 MB, throughput 0.996727
Reading from 15203: heap size 1478 MB, throughput 0.988399
Reading from 15204: heap size 113 MB, throughput 0.997213
Reading from 15204: heap size 109 MB, throughput 0.997452
Reading from 15204: heap size 105 MB, throughput 0.997757
Reading from 15204: heap size 105 MB, throughput 0.99735
Reading from 15204: heap size 98 MB, throughput 0.997508
Reading from 15203: heap size 1481 MB, throughput 0.991187
Reading from 15204: heap size 98 MB, throughput 0.997001
Reading from 15204: heap size 92 MB, throughput 0.997372
Reading from 15204: heap size 92 MB, throughput 0.997244
Reading from 15204: heap size 87 MB, throughput 0.99727
Reading from 15204: heap size 87 MB, throughput 0.996777
Reading from 15204: heap size 82 MB, throughput 0.996963
Reading from 15203: heap size 1488 MB, throughput 0.981709
Reading from 15204: heap size 82 MB, throughput 0.994004
Reading from 15204: heap size 79 MB, throughput 0.979027
Reading from 15204: heap size 85 MB, throughput 0.986655
Reading from 15204: heap size 94 MB, throughput 0.97784
Reading from 15204: heap size 104 MB, throughput 0.985147
Reading from 15204: heap size 114 MB, throughput 0.994761
Reading from 15204: heap size 121 MB, throughput 0.995789
Reading from 15204: heap size 128 MB, throughput 0.996537
Reading from 15203: heap size 1495 MB, throughput 0.985089
Reading from 15204: heap size 128 MB, throughput 0.996591
Equal recommendation: 3669 MB each
Reading from 15204: heap size 138 MB, throughput 0.997589
Reading from 15204: heap size 139 MB, throughput 0.996994
Reading from 15204: heap size 149 MB, throughput 0.998158
Reading from 15203: heap size 1499 MB, throughput 0.98495
Reading from 15204: heap size 141 MB, throughput 0.997432
Reading from 15204: heap size 136 MB, throughput 0.997762
Reading from 15204: heap size 134 MB, throughput 0.997408
Reading from 15204: heap size 126 MB, throughput 0.997726
Reading from 15203: heap size 1487 MB, throughput 0.981253
Reading from 15204: heap size 122 MB, throughput 0.9973
Reading from 15204: heap size 118 MB, throughput 0.997418
Reading from 15204: heap size 116 MB, throughput 0.99706
Reading from 15204: heap size 110 MB, throughput 0.995373
Reading from 15203: heap size 1365 MB, throughput 0.97961
Reading from 15204: heap size 106 MB, throughput 0.997539
Reading from 15204: heap size 102 MB, throughput 0.997572
Reading from 15204: heap size 101 MB, throughput 0.997195
Reading from 15204: heap size 96 MB, throughput 0.9975
Equal recommendation: 3669 MB each
Reading from 15204: heap size 95 MB, throughput 0.996957
Reading from 15204: heap size 90 MB, throughput 0.997407
Reading from 15203: heap size 1473 MB, throughput 0.978326
Reading from 15204: heap size 89 MB, throughput 0.99701
Reading from 15204: heap size 85 MB, throughput 0.997111
Reading from 15204: heap size 84 MB, throughput 0.996532
Reading from 15204: heap size 80 MB, throughput 0.996491
Reading from 15204: heap size 80 MB, throughput 0.995999
Reading from 15204: heap size 76 MB, throughput 0.99622
Reading from 15204: heap size 75 MB, throughput 0.994966
Reading from 15204: heap size 72 MB, throughput 0.984834
Reading from 15204: heap size 79 MB, throughput 0.98163
Reading from 15204: heap size 85 MB, throughput 0.981515
Reading from 15204: heap size 95 MB, throughput 0.983861
Reading from 15204: heap size 104 MB, throughput 0.974654
Reading from 15203: heap size 1391 MB, throughput 0.959183
Reading from 15204: heap size 117 MB, throughput 0.996186
Reading from 15204: heap size 118 MB, throughput 0.996326
Reading from 15204: heap size 119 MB, throughput 0.995479
Reading from 15204: heap size 131 MB, throughput 0.993321
Reading from 15203: heap size 1478 MB, throughput 0.974786
Reading from 15204: heap size 132 MB, throughput 0.907146
Reading from 15204: heap size 143 MB, throughput 0.998337
Reading from 15204: heap size 143 MB, throughput 0.99776
Reading from 15204: heap size 135 MB, throughput 0.997939
Equal recommendation: 3669 MB each
Reading from 15203: heap size 1481 MB, throughput 0.972736
Reading from 15204: heap size 133 MB, throughput 0.997605
Reading from 15204: heap size 125 MB, throughput 0.998149
Reading from 15204: heap size 124 MB, throughput 0.997952
Reading from 15204: heap size 117 MB, throughput 0.998066
Reading from 15204: heap size 116 MB, throughput 0.997564
Reading from 15203: heap size 1486 MB, throughput 0.97114
Reading from 15204: heap size 109 MB, throughput 0.997469
Reading from 15204: heap size 109 MB, throughput 0.997577
Reading from 15204: heap size 102 MB, throughput 0.997647
Reading from 15204: heap size 102 MB, throughput 0.997502
Reading from 15204: heap size 96 MB, throughput 0.997552
Reading from 15204: heap size 96 MB, throughput 0.99725
Reading from 15203: heap size 1490 MB, throughput 0.971021
Reading from 15204: heap size 90 MB, throughput 0.997426
Reading from 15204: heap size 90 MB, throughput 0.997216
Reading from 15204: heap size 85 MB, throughput 0.997138
Reading from 15204: heap size 85 MB, throughput 0.996686
Reading from 15204: heap size 81 MB, throughput 0.996992
Reading from 15204: heap size 80 MB, throughput 0.996313
Reading from 15204: heap size 77 MB, throughput 0.996752
Reading from 15204: heap size 76 MB, throughput 0.996035
Reading from 15203: heap size 1496 MB, throughput 0.967455
Reading from 15204: heap size 73 MB, throughput 0.996596
Reading from 15204: heap size 72 MB, throughput 0.993871
Reading from 15204: heap size 70 MB, throughput 0.982143
Equal recommendation: 3669 MB each
Reading from 15204: heap size 77 MB, throughput 0.98404
Reading from 15204: heap size 83 MB, throughput 0.984773
Reading from 15204: heap size 93 MB, throughput 0.985907
Reading from 15204: heap size 102 MB, throughput 0.985337
Reading from 15204: heap size 115 MB, throughput 0.996008
Reading from 15204: heap size 127 MB, throughput 0.996895
Reading from 15204: heap size 128 MB, throughput 0.995969
Reading from 15203: heap size 1504 MB, throughput 0.966069
Reading from 15204: heap size 140 MB, throughput 0.997455
Reading from 15204: heap size 141 MB, throughput 0.996945
Reading from 15204: heap size 133 MB, throughput 0.997396
Reading from 15204: heap size 128 MB, throughput 0.997667
Reading from 15203: heap size 1513 MB, throughput 0.963483
Reading from 15204: heap size 123 MB, throughput 0.99803
Reading from 15204: heap size 122 MB, throughput 0.997765
Reading from 15204: heap size 115 MB, throughput 0.997844
Reading from 15204: heap size 114 MB, throughput 0.99791
Reading from 15204: heap size 108 MB, throughput 0.997146
Reading from 15203: heap size 1523 MB, throughput 0.962838
Reading from 15204: heap size 107 MB, throughput 0.99768
Reading from 15204: heap size 101 MB, throughput 0.997752
Equal recommendation: 3669 MB each
Reading from 15204: heap size 100 MB, throughput 0.997189
Reading from 15204: heap size 95 MB, throughput 0.997279
Reading from 15204: heap size 94 MB, throughput 0.997251
Reading from 15204: heap size 89 MB, throughput 0.997387
Reading from 15204: heap size 89 MB, throughput 0.997145
Reading from 15203: heap size 1536 MB, throughput 0.960222
Reading from 15204: heap size 84 MB, throughput 0.99731
Reading from 15204: heap size 84 MB, throughput 0.996382
Reading from 15204: heap size 80 MB, throughput 0.996744
Reading from 15204: heap size 79 MB, throughput 0.996518
Reading from 15204: heap size 76 MB, throughput 0.996412
Reading from 15204: heap size 75 MB, throughput 0.9963
Reading from 15204: heap size 72 MB, throughput 0.996438
Reading from 15204: heap size 71 MB, throughput 0.996024
Reading from 15204: heap size 69 MB, throughput 0.995883
Reading from 15204: heap size 68 MB, throughput 0.995359
Reading from 15203: heap size 1544 MB, throughput 0.96414
Reading from 15204: heap size 66 MB, throughput 0.996199
Reading from 15204: heap size 65 MB, throughput 0.995419
Reading from 15204: heap size 63 MB, throughput 0.99423
Reading from 15204: heap size 62 MB, throughput 0.975387
Reading from 15204: heap size 68 MB, throughput 0.973591
Reading from 15204: heap size 75 MB, throughput 0.982617
Reading from 15204: heap size 82 MB, throughput 0.9842
Reading from 15204: heap size 93 MB, throughput 0.985878
Reading from 15204: heap size 104 MB, throughput 0.989828
Reading from 15204: heap size 117 MB, throughput 0.996852
Reading from 15204: heap size 122 MB, throughput 0.997083
Reading from 15203: heap size 1555 MB, throughput 0.963426
Reading from 15204: heap size 124 MB, throughput 0.996723
Reading from 15204: heap size 136 MB, throughput 0.997218
Equal recommendation: 3669 MB each
Reading from 15204: heap size 137 MB, throughput 0.997415
Reading from 15204: heap size 129 MB, throughput 0.997737
Reading from 15204: heap size 127 MB, throughput 0.997504
Reading from 15203: heap size 1560 MB, throughput 0.963096
Reading from 15204: heap size 120 MB, throughput 0.997558
Reading from 15204: heap size 118 MB, throughput 0.99732
Reading from 15204: heap size 112 MB, throughput 0.997955
Reading from 15204: heap size 111 MB, throughput 0.99733
Reading from 15204: heap size 105 MB, throughput 0.997638
Reading from 15204: heap size 104 MB, throughput 0.997289
Reading from 15203: heap size 1572 MB, throughput 0.964371
Reading from 15204: heap size 98 MB, throughput 0.99679
Reading from 15204: heap size 97 MB, throughput 0.997067
Reading from 15204: heap size 92 MB, throughput 0.997308
Reading from 15204: heap size 92 MB, throughput 0.997034
Reading from 15204: heap size 87 MB, throughput 0.996962
Reading from 15204: heap size 86 MB, throughput 0.996527
Reading from 15204: heap size 82 MB, throughput 0.996829
Reading from 15204: heap size 82 MB, throughput 0.996856
Reading from 15203: heap size 1575 MB, throughput 0.963375
Reading from 15204: heap size 78 MB, throughput 0.996409
Reading from 15204: heap size 78 MB, throughput 0.996216
Equal recommendation: 3669 MB each
Reading from 15204: heap size 74 MB, throughput 0.996271
Reading from 15204: heap size 73 MB, throughput 0.995823
Reading from 15204: heap size 71 MB, throughput 0.996059
Reading from 15204: heap size 70 MB, throughput 0.995655
Reading from 15204: heap size 68 MB, throughput 0.995751
Reading from 15204: heap size 67 MB, throughput 0.995301
Reading from 15204: heap size 65 MB, throughput 0.995696
Reading from 15204: heap size 64 MB, throughput 0.994985
Reading from 15204: heap size 62 MB, throughput 0.99552
Reading from 15203: heap size 1587 MB, throughput 0.965136
Reading from 15204: heap size 61 MB, throughput 0.995478
Reading from 15204: heap size 60 MB, throughput 0.985495
Reading from 15204: heap size 65 MB, throughput 0.979986
Reading from 15204: heap size 71 MB, throughput 0.976203
Reading from 15204: heap size 79 MB, throughput 0.982478
Reading from 15204: heap size 87 MB, throughput 0.980419
Reading from 15204: heap size 99 MB, throughput 0.982421
Reading from 15204: heap size 110 MB, throughput 0.995228
Reading from 15204: heap size 123 MB, throughput 0.996109
Reading from 15204: heap size 125 MB, throughput 0.99627
Reading from 15204: heap size 125 MB, throughput 0.90011
Reading from 15203: heap size 1588 MB, throughput 0.963968
Reading from 15204: heap size 139 MB, throughput 0.998129
Reading from 15204: heap size 132 MB, throughput 0.997653
Reading from 15204: heap size 128 MB, throughput 0.998149
Reading from 15204: heap size 127 MB, throughput 0.997916
Reading from 15204: heap size 120 MB, throughput 0.997935
Reading from 15203: heap size 1599 MB, throughput 0.965368
Reading from 15204: heap size 119 MB, throughput 0.997851
Equal recommendation: 3669 MB each
Reading from 15204: heap size 112 MB, throughput 0.997915
Reading from 15204: heap size 112 MB, throughput 0.997411
Reading from 15204: heap size 105 MB, throughput 0.997715
Reading from 15204: heap size 105 MB, throughput 0.9972
Reading from 15204: heap size 99 MB, throughput 0.997478
Reading from 15204: heap size 99 MB, throughput 0.997303
Reading from 15203: heap size 1600 MB, throughput 0.962893
Reading from 15204: heap size 94 MB, throughput 0.997697
Reading from 15204: heap size 94 MB, throughput 0.997044
Reading from 15204: heap size 89 MB, throughput 0.997248
Reading from 15204: heap size 88 MB, throughput 0.996898
Reading from 15204: heap size 84 MB, throughput 0.996911
Reading from 15204: heap size 84 MB, throughput 0.996804
Reading from 15204: heap size 80 MB, throughput 0.996493
Reading from 15204: heap size 80 MB, throughput 0.996254
Reading from 15204: heap size 76 MB, throughput 0.996415
Reading from 15204: heap size 76 MB, throughput 0.995796
Reading from 15204: heap size 73 MB, throughput 0.995381
Reading from 15204: heap size 73 MB, throughput 0.99471
Reading from 15204: heap size 70 MB, throughput 0.995129
Reading from 15204: heap size 70 MB, throughput 0.994463
Reading from 15203: heap size 1610 MB, throughput 0.736298
Reading from 15204: heap size 67 MB, throughput 0.99652
Reading from 15204: heap size 67 MB, throughput 0.995709
Reading from 15204: heap size 65 MB, throughput 0.995712
Equal recommendation: 3669 MB each
Client 15204 died
Clients: 1
Reading from 15203: heap size 1718 MB, throughput 0.993351
Reading from 15203: heap size 1717 MB, throughput 0.990811
Reading from 15203: heap size 1730 MB, throughput 0.988662
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 15203: heap size 1744 MB, throughput 0.99629
Reading from 15203: heap size 1603 MB, throughput 0.988431
Reading from 15203: heap size 1734 MB, throughput 0.869187
Reading from 15203: heap size 1740 MB, throughput 0.780748
Reading from 15203: heap size 1765 MB, throughput 0.816802
Recommendation: one client; give it all the memory
Reading from 15203: heap size 1785 MB, throughput 0.802266
Client 15203 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
