#!/bin/bash
# This is a horrible, non-general hack done in the rush for the paper!

function usage()
{
    echo "Usage: $(basename "${0}") timer_comparison_many_dir"
    exit 1
}

test -z "${1}" && usage
test ! -d "${1}" && usage

DIR="$(readlink -f "${1}")"

i='1'

while true; do
    SUBDIR="${DIR}/sub${i}_timerComparisonSingle"
    test ! -d "${SUBDIR}" && break

    SUBDIR="${SUBDIR}/sub2_timeBenchmarkStaticEqual"
    test ! -d "${SUBDIR}" && break

    cd "${SUBDIR}"
    if grep 'Exit status: [1-9]' metadata/run*_process*_resourceUsage.txt &>/dev/null; then
        echo "true"
    else
        echo
    fi

    i=$(( i + 1 ))
done
