#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

PID='-1'
THROUGHPUT_MODEL='unknown'

function usage()
{
    echo "Usage: $(basename "${0}") readings_file [throughput_model] [pause_on_func]"
    exit 1
}

test -z "${1}" && usage
READINGS_FILE="${1}"

if [ ! -z "${2}" ]; then
    THROUGHPUT_MODEL="${2}"
fi

if [ ! -z "${3}" ]; then
    PAUSE_ON_FUNC='--pause_on_func'
else
    PAUSE_ON_FUNC=''
fi

python "${DIR}/plot_readings_live.py" ${PAUSE_ON_FUNC} "${THROUGHPUT_MODEL}" < "${READINGS_FILE}"
