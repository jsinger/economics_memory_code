# Main python library for all the static utility measurement scripts, both for individual
# benchmarks, and utility space sampling

import itertools
import utilitymodel
import warnings
import numpy as np
import matplotlib.cm
import matplotlib.colors
import matplotlib.pyplot as plt
import scipy.interpolate
from mpl_toolkits.mplot3d import Axes3D
import utilitySpacePostprocess

# This is a bit of namespace hackery to stop numpy, scipy, and matplotlib having to be installed on
# remote worker machines
import staticutility_core as core
from staticutility_core import *

warnings.filterwarnings("error")
np.seterr(all="warn")

black = "#000000"
red = "#FF0000"
green = "#00FF00"
blue = "#0000FF"
cyan = "#00FFFF"
magenta = "#FF00FF"

class MeasureDir(core.MeasureDir):

    def plotIterations(self, heap, meanIterations, meanStddevs, medianIterations, bid, runs):
        fontsize="small"
        plt.errorbar(heap, meanIterations, meanStddevs)
        plt.plot(heap, medianIterations, "m--")
        plt.xlabel("Heap size (MB)", fontsize=fontsize)
        plt.ylabel("Completed iterations", fontsize=fontsize)
        plt.legend(["Mean", "Median"], fontsize=fontsize, frameon=False, loc=4)
        plt.title("Completed iterations of '%s %s (%s thread(s))' in %s (%d runs)\n" % (bid.name, bid.inputSize, bid.threads, bid.time, runs), fontsize=fontsize);

        plt.savefig(self.individualIterationsGraph(bid))
        plt.clf()

    def plotThroughput(self, heap, meanThroughputs, meanThroughputStddevs, medianThroughputs, meanFit, medianFit, bid, runs, modelName, cleaned=False):
        fitx = np.linspace(min(heap), max(heap), 100)
        fityMean = meanFit.calculate(fitx)
        fityMedian = medianFit.calculate(fitx)

        plt.errorbar(heap, meanThroughputs, meanThroughputStddevs)

        if cleaned:
            plt.plot(fitx, fityMean, 'k--')
            fontsize="medium"
        else:
            plt.plot(heap, medianThroughputs, 'm--')
            plt.plot(fitx, fityMean, 'k')
            plt.plot(fitx, fityMedian, 'r--')
            fontsize="small"

        plt.xlabel("Heap size (MB)", fontsize=fontsize)
        plt.ylabel("Throughput (Iterations per second)", fontsize=fontsize)

        if cleaned:
            plt.legend(["Mean values", "Throughput function: " + meanFit.pretty()], fontsize=fontsize, frameon=False, loc=4)
            plt.savefig(self.cleanedIndividualThroughputGraph(bid, modelName))
        else:
            plt.legend(["Mean", "Median", "Fitted mean: " + str(meanFit), "Fitted median: " + str(medianFit)], fontsize=fontsize, frameon=False, loc=4)
            plt.title("Throughput of '%s %s (%s thread(s))' in %s (%d runs)\n('%s' throughput model)\n" % (bid.name, bid.inputSize, bid.threads, bid.time, runs, modelName), fontsize=fontsize)
            plt.savefig(self.individualThroughputGraph(bid, modelName))

        plt.clf()

        meanFit.save(self.paramsFile(bid, modelName, "mean"))
        medianFit.save(self.paramsFile(bid, modelName, "median"))


class UtilitySpaceDir(core.UtilitySpaceDir):

    def genStandardMeasurePoints(self, bids, srcDirs, totalMem, minHeaps, ignoreCrossProduct=False):
        """Return a list of (x, y, ...) points"""
        l = []
        heaps = map(lambda (b, d): d.readHeapSizes(b), zip(bids, srcDirs))

        # Generate points from the cross product of the measurements
        if not ignoreCrossProduct:
            for i in itertools.product(*heaps):
                if sum(i) <= totalMem:
                    l.append(i)

        # Generate the even split point
        l.append(tuple(evenSplit(totalMem, len(bids))))

        # Generate points that are actually on the budget line
        def budgetPoints(mem, inc, minHeaps):
            if len(minHeaps) <= 1:
                if mem < minHeaps[0]:
                    return []
                else:
                    return [(mem,)]
            else:
                l = []
                i = minHeaps[0]
                while i <= mem - sum(minHeaps[1:]):
                    l += [i]
                    i += inc

                p = []
                for i in l:
                    p += map(lambda t: (i,) + t, budgetPoints(mem - i, inc, minHeaps[1:]))

                return p

        if len(heaps[0]) > 1:
            inc = heaps[0][1] - heaps[0][0]
        else:
            inc = 100

        l += budgetPoints(totalMem, inc, minHeaps)

        return l

    def bestPoints(self, bids, srcDirs, minHeaps, totalMem):
        l = []
        for model in utilitymodel.models:
            self.checkCanUseUtilityModel(model, bids, srcDirs)
            self.pullInParamsFiles(model, bids, srcDirs)
            params = map(lambda b: utilitymodel.get(model).loadParams(self.paramsFile(b, model)), bids)

            (hs, U) = utilitymodel.get(model).calculateBest(totalMem, params, minHeaps)
            l.append(tuple(map(lambda h: int(round(h)), hs)))
        return l

    def writeMeasurepoints(self, l):
        with open(self.measurepointsFile(), "w") as f:
            for point in l:
                f.write(" ".join(map(str, point)) + "\n")

    def pointsFromMeasure(self, bids, srcDirs, minHeaps, totalMem, ignoreCrossProduct=False):
        l = self.genStandardMeasurePoints(bids, srcDirs, totalMem, minHeaps, ignoreCrossProduct)
        l += self.bestPoints(bids, srcDirs, minHeaps, totalMem)
        l = sorted(list(set(l)))

        self.writeMeasurepoints(l)

        return l

    def pointsFromFile(self, bids, srcDirs, minHeaps, totalMem, filename):
        """Load points from an existing file"""
        l = []

        with open(filename) as f:
            for line in f:
                s = line.strip("\n").split()
                if s:
                    l.append(tuple(map(lambda h: int(round(float(h))), s)))

        l += self.bestPoints(bids, srcDirs, minHeaps, totalMem)
        l = sorted(list(set(l)))

        self.writeMeasurepoints(l)

        return l


    # Plotting functions

    def canPlotVMs(self, n):
        return n == 2 or n == 3

    def plot(self, hs, measured, predicted, difference, bids, totalMem, runs, modelName, cleaned=False):
        if not self.canPlotVMs(len(bids)):
            return False

        self.plotMeasured(hs, measured, predicted, bids, totalMem, runs, modelName, cleaned)
        plt.savefig(self.measuredGraph(modelName, cleaned))
        plt.clf()

        self.plotPredicted(hs, predicted, bids, totalMem, runs, modelName, cleaned)
        plt.savefig(self.predictedGraph(modelName, cleaned))
        plt.clf()

        self.plotDifference(hs, difference, bids, totalMem, runs, modelName, cleaned)
        plt.savefig(self.differenceGraph(modelName, cleaned))
        plt.clf()

        if self.plotMeasuredTopDown(hs, measured, predicted, bids, totalMem, runs, modelName, cleaned):
            plt.savefig(self.measuredGraphTopDown(modelName, cleaned))
            plt.clf()

        if self.plotPredictedTopDown(hs, predicted, bids, totalMem, runs, modelName, cleaned):
            plt.savefig(self.predictedGraphTopDown(modelName, cleaned))
            plt.clf()

        if self.plotDifferenceTopDown(hs, difference, bids, totalMem, runs, modelName, cleaned):
            plt.savefig(self.differenceGraphTopDown(modelName, cleaned))
            plt.clf()

        return True


    def viewMeasured(self, hs, measured, predicted, bids, totalMem, runs, modelName):
        if not self.canPlotVMs(len(bids)):
            return False

        self.plotMeasured(hs, measured, predicted, bids, totalMem, runs, modelName)
        plt.show()
        plt.clf()

        return True


    def viewPredicted(self, hs, predicted, bids, totalMem, runs, modelName):
        if not self.canPlotVMs(len(bids)):
            return False

        self.plotPredicted(hs, predicted, bids, totalMem, runs, modelName)
        plt.show()
        plt.clf()

        return True


    def viewDifference(self, hs, difference, bids, totalMem, runs, modelName):
        if not self.canPlotVMs(len(bids)):
            return False

        self.plotDifference(hs, difference, bids, totalMem, runs, modelName)
        plt.show()
        plt.clf()

        return True


    def bestPoint(self, hs, U):
        i = np.argmax(U)
        return (tuple(map(lambda h: h[i], hs) + [max(U)]), i)


    def plotMeasured(self, hs, measured, predicted, bids, totalMem, runs, modelName, cleaned=False):
        (maxMeasured, maxMeasured_i) = self.bestPoint(hs, measured)
        (maxPredicted, maxPredicted_i) = self.bestPoint(hs, predicted)

        if len(bids) == 2:
            maxMeasured_predicted = (maxMeasured[0], maxMeasured[1], predicted[maxMeasured_i])
            maxPredicted_measured = (maxPredicted[0], maxPredicted[1], measured[maxPredicted_i])

            # Filter out duplicates of special points from the general dataset, so it's easier to see things on the plot
            (hs, measured) = self.measuredFilter(hs, measured, [maxMeasured, maxMeasured_predicted, maxPredicted, maxPredicted_measured])

            if cleaned:
                msg=u"\u25C7 = best measured throughput, \u25BD = best predicted throughput\n\u25A1 = observed throughput"
                extraPoints=[(maxMeasured, red, "D"),
                             (maxPredicted, cyan, "v"),
                             (maxPredicted_measured, green, "s")]
            else:
                msg=u"\u25C7 = best measured point, \u25A1 = predicted value at best measured point\n\u25BD = best predicted point, \u25B3 = measured value at best predicted point"
                extraPoints=[(maxMeasured, red, "D"),
                             (maxMeasured_predicted, green, "s"),
                             (maxPredicted, cyan, "v"),
                             (maxPredicted_measured, magenta, "^")]


            self.plotInternal2(hs, measured, bids, totalMem, runs, modelName, "Measured",
                               threeD=True,
                               msg=msg,
                               extraPoints=extraPoints,
                               cleaned=cleaned)

        elif len(bids) == 3:
            (hs, measured) = self.measuredFilter(hs, measured, [maxMeasured, maxPredicted])
            self.plotInternal3(hs, measured, bids, totalMem, runs, modelName, "Measured",
                               msg=u"\u25C7 = best measured throughput, \u25BD = best predicted throughput",
                               extraPoints=[(maxMeasured, red, "D"),
                                            (maxPredicted, cyan, "v")],
                               cleaned=cleaned)


    def plotMeasuredTopDown(self, hs, measured, predicted, bids, totalMem, runs, modelName, cleaned=False):
        if len(bids) != 2:
            return False

        maxMeasured = self.bestPoint(hs, measured)[0]
        (maxPredicted, maxPredicted_i) = self.bestPoint(hs, predicted)
        maxPredicted_measured = (maxPredicted[0], maxPredicted[1], measured[maxPredicted_i])

        # Filter out duplicates of special points from the general dataset, so it's easier to see things on the plot
        (hs, measured) = self.measuredFilter(hs, measured, [maxMeasured, maxPredicted, maxPredicted_measured])

        self.plotInternal2(hs, measured, bids, totalMem, runs, modelName, "Measured",
                           msg=u"+ = best measured throughput, \u2715 = best predicted throughput",
                           extraPoints=[(maxMeasured, red, "+"),
                                        (maxPredicted, black, "x")],
                           cleaned=cleaned)

        return True


    def measuredFilter(self, hs, data, specialPoints):
        newHs = []
        newData = []

        for (p, U) in zip(zip(*hs), data):
            if not self.measuredMatch(list(p) + [U], specialPoints):
                newHs.append(p)
                newData.append(U)

        return (map(list, zip(*newHs)), newData)


    def measuredMatch(self, point, specialPoints):
        """Because we can only visualise three dimensions, we only care if points are the same in their first three coordinates"""
        for p in specialPoints:
            if tuple(p)[:3] == tuple(point)[:3]:
                return True
        return False


    def plotPredicted(self, hs, predicted, bids, totalMem, runs, modelName, cleaned=False):
        if len(bids) == 2:
            self.plotInternal2(hs, predicted, bids, totalMem, runs, modelName, "Predicted", threeD=True, cleaned=cleaned)
        elif len(bids) == 3:
            self.plotInternal3(hs, predicted, bids, totalMem, runs, modelName, "Predicted", cleaned=cleaned)


    def plotPredictedTopDown(self, hs, predicted, bids, totalMem, runs, modelName, cleaned=False):
        if len(bids) != 2:
            return False

        self.plotInternal2(hs, predicted, bids, totalMem, runs, modelName, "Predicted", cleaned=cleaned)
        return True


    def plotDifference(self, hs, difference, bids, totalMem, runs, modelName, cleaned=False):
        if len(bids) == 2:
            self.plotInternal2(hs, difference, bids, totalMem, runs, modelName, "Difference in", threeD=True, zLabel="Difference in throughput", cleaned=cleaned)
        elif len(bids) == 3:
            self.plotInternal3(hs, difference, bids, totalMem, runs, modelName, "Difference in", cleaned=cleaned)


    def plotDifferenceTopDown(self, hs, difference, bids, totalMem, runs, modelName, cleaned=False):
        if len(bids) != 2:
            return False

        self.plotInternal2(hs, difference, bids, totalMem, runs, modelName, "Difference in", cleaned=cleaned)
        return True


    def contour(self, x, y, z, ax):
        xi = np.linspace(min(x), max(x))
        yi = np.linspace(min(y), max(y))
        try:
            zi = scipy.interpolate.griddata((x, y), z, (xi[None,:], yi[:,None]))
        except:
            pass
        else:
            ax.contour(xi, yi, zi, zdir="z", offset=min(z), cmap=matplotlib.cm.coolwarm)


    def plotInternal2(self, hs, U, bids, totalMem, runs, modelName, name, threeD=False, zLabel="Throughput", extraPoints=[], msg="", cleaned=False):
        """extraPoints should be [((x,y,z),colour,marker)]"""
        defaultMarker = "o"

        if threeD:
            ax = plt.subplot(111, projection='3d')
            ax.scatter(hs[0], hs[1], U, c=blue, marker=defaultMarker)
            fontsize="small"
        else:
            ax = plt.subplot(111)
            ax.scatter(hs[0], hs[1], c=blue, marker=defaultMarker)
            fontsize="medium"

        ax.set_xlabel("%s heap size (MB)" % bids[0].name, fontsize=fontsize)
        ax.set_ylabel("%s heap size (MB)" % bids[1].name, fontsize=fontsize)

        if threeD:
            ax.set_zlabel(zLabel, fontsize=fontsize)

        if cleaned:
            ax.set_title(msg, fontsize=fontsize)
        else:
            if msg:
                msg = "\n" + msg
            ax.set_title("%s throughput: %s\n('%s' throughput model)%s" % (name, self.graphTitle(totalMem, runs, bids), modelName, msg), fontsize=fontsize)

        for ((x, y, z), c, m) in extraPoints:
            if threeD:
                ax.scatter(x, y, z, c=c, marker=m, s=[40])
            else:
                ax.scatter(x, y, c=c, marker=m, s=[80])

        self.contour(hs[0], hs[1], U, ax)


    def plotInternal3(self, hs, U, bids, totalMem, runs, modelName, name, extraPoints=[], msg="", cleaned=False):
        defaultMarker = "o"
        fontsize="small"

        ax = plt.subplot(111, projection='3d')
        ax.view_init(azim=20)
        ax.scatter(hs[0], hs[1], hs[2], marker=defaultMarker, c=U, cmap=matplotlib.colors.LinearSegmentedColormap.from_list("throughput", ["k", "w"]))
        ax.set_xlabel("%s heap (MB)" % bids[0].name, fontsize=fontsize)
        ax.set_ylabel("%s heap (MB)" % bids[1].name, fontsize=fontsize)
        ax.set_zlabel("%s heap (MB)" % bids[2].name, fontsize=fontsize)

        if msg:
            msg = "\n" + msg

        if cleaned:
            ax.set_title("lighter = higher throughput%s" % msg, fontsize=fontsize)
        else:
            ax.set_title("%s throughput: %s\n('%s' throughput model)%s\nwhiter = higher throughput" % (name, self.graphTitle(totalMem, runs, bids), modelName, msg), fontsize=fontsize)

        for ((x, y, z, U), c, m) in extraPoints:
            ax.scatter(x, y, z, c=c, marker=m, s=[40])

        ax.set_xlim(0, ax.get_xlim()[1])
        ax.set_ylim(0, ax.get_ylim()[1])
        ax.set_zlim(0, ax.get_zlim()[1])


    def graphTitle(self, totalMem, runs, bids):
        return "%s: %d MB, %d runs" % (", ".join(map(lambda b: b.pretty(), bids)), totalMem, runs)

def allUtilitySpaceDirs():
    return map(UtilitySpaceDir, globUtilitySpaceDirs(outputDir))


class UtilityAggregateDir(core.UtilityAggregateDir):
    def postprocessAll(self):
        for d in self.dirs:
            utilitySpacePostprocess.postprocess(d)

    def summarise(self, models=utilitymodel.models):
        s = []
        for d in self.dirs:
            for model in utilitymodel.models:
                if model in models:
                    s.append(d.summarise(model))
        return s

    def aggregateSummaries(self, s, models=utilitymodel.models):
        r = []
        for model in utilitymodel.models:
            if model in models:
                lines = filter(lambda l: l.model == model, s)
                if lines:
                    r.append(UtilityAggregatedResults(lines))
        return r

    def tabulateResults(self):
        s = self.summarise()
        allLines = self.aggregateSummaries(s)
        same = self.aggregateSummaries(filter(lambda x: x.allBidsEqual(), s))
        diff = self.aggregateSummaries(filter(lambda x: not x.allBidsEqual(), s))

        headings = ["BENCHMARKS", "BUDGET", "MODEL", "PREDICTION ACCURACY", "EUCLIDEAN DISTANCE", "PRACTICAL ACCURACY", "PRACTICAL VS EQUAL", "RATIO PREDICTED / DEFAULT", "RATIO EQUAL / DEFAULT", "RATIO MAX MEASURED / DEFAULT", "RATIO MAX PREDICTED MEASURED / DEFAULT"]
        emptyLine = [""]*len(headings)

        t = [headings, emptyLine]

        t += map(lambda s: s.toFields(), s)
        t.append(emptyLine)

        def averageLine(a, msg):
            def maybe(d):
                if d is not None:
                    return d.pretty()
                else:
                    return "(No data)"

            if msg:
                msg = ", " + msg

            for line in a:
                t.append(["Average" + msg + " (" + str(len(line.summaries)) + " combinations)", "", line.model, line.predictionAccuracy.pretty(), line.euclideanDistance.pretty(), line.practicalAccuracy.pretty(), maybe(line.practicalVsEqual),  maybe(line.defaultRatioPredicted), maybe(line.defaultRatioEqual), maybe(line.defaultRatioMaxMeasured), maybe(line.defaultRatioMaxPredictedMeasured)])
            t.append(emptyLine)

        averageLine(allLines, "")
        averageLine(same, "same-benchmark combinations")
        averageLine(diff, "different-benchmark combinations")

        maxLens = map(lambda l: max(map(len, l)), zip(*t))

        template = "   ".join(map(lambda (x, y): "{%d:%d}" % (x, y), zip(range(len(maxLens)), maxLens)))
        return "# -*- truncate-lines:t -*-\n\n" + "\n".join(map(lambda l: template.format(*l), t))

    def tabulateAndSave(self):
        writeFile(self.utilityResultsTable(), self.tabulateResults())

    def plotImprovementGraph(self):
        for model in utilitymodel.models:
            s = self.summarise(models=[model])
            allLines = self.aggregateSummaries(s, models=[model])[0]
            same = self.aggregateSummaries(filter(lambda x: x.allBidsEqual(), s), models=[model])[0]
            diff = self.aggregateSummaries(filter(lambda x: not x.allBidsEqual(), s), models=[model])[0]
            groups = [allLines, same, diff]

            bars = []
            bars.append((zip(*map(lambda s: (s.defaultRatioEqual.val, s.defaultRatioEqual.stddev), groups)), "Equal", "r", "/"))
            bars.append((zip(*map(lambda s: (s.defaultRatioPredicted.val, s.defaultRatioPredicted.stddev), groups)), "Predicted", "g", "\\"))
            bars.append((zip(*map(lambda s: (s.defaultRatioMaxPredictedMeasured.val, s.defaultRatioMaxPredictedMeasured.stddev), groups)), "Measured", "b", "+"))

            width = 1.0 / (len(groups) + 1)
            leftPos = np.arange(len(groups))

            ax = plt.subplot(111)

            def labelBar(rects):
                for rect in rects:
                    height = rect.get_height()
                    ax.text(rect.get_x() + rect.get_width() / 2.0, 1.05 * height, "%.2f" % height, ha="center", va="bottom")

            i = 0
            for ((means, stddevs), title, colour, hatch) in bars:
                r = ax.bar(leftPos + i*width, means, width, color="w", yerr=stddevs, ecolor="k", edgecolor=colour, hatch=hatch)
                labelBar(r)
                i += 1

            ax.set_ylabel("Throughput relative to default heap sizing")
            ax.set_xticks(leftPos + 1.5*width)
            ax.xaxis.set_tick_params(size=0)
            ax.set_xticklabels(("All", "Same", "Different"))
            ax.legend(("Equal partition", "Predicted", "Observed"), loc=4)

            ax.hlines(1, 0, len(groups), linestyles='dashed')

            plt.savefig(self.improvementGraphModel(model))
            plt.clf()

        self.mergeImprovementGraphs()


def validateModel(m):
    if m not in utilitymodel.models:
        raise UtilityException("'" + m + "' is not a valid utility model")
    else:
        return m
