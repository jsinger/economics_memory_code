economemd
    total memory: 3657 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub9_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run09_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 28384: heap size 9 MB, throughput 0.989945
Clients: 1
Client 28384 has a minimum heap size of 8 MB
Reading from 28383: heap size 9 MB, throughput 0.99316
Clients: 2
Client 28383 has a minimum heap size of 1211 MB
Reading from 28383: heap size 9 MB, throughput 0.977804
Reading from 28384: heap size 9 MB, throughput 0.96879
Reading from 28383: heap size 9 MB, throughput 0.96915
Reading from 28384: heap size 9 MB, throughput 0.929738
Reading from 28383: heap size 9 MB, throughput 0.900345
Reading from 28384: heap size 9 MB, throughput 0.896592
Reading from 28384: heap size 11 MB, throughput 0.914348
Reading from 28384: heap size 11 MB, throughput 0.967296
Reading from 28383: heap size 11 MB, throughput 0.986994
Reading from 28384: heap size 17 MB, throughput 0.943924
Reading from 28383: heap size 11 MB, throughput 0.985907
Reading from 28384: heap size 18 MB, throughput 0.970943
Reading from 28384: heap size 25 MB, throughput 0.711726
Reading from 28383: heap size 17 MB, throughput 0.934171
Reading from 28384: heap size 29 MB, throughput 0.975485
Reading from 28384: heap size 30 MB, throughput 0.969672
Reading from 28384: heap size 34 MB, throughput 0.97204
Reading from 28383: heap size 17 MB, throughput 0.391018
Reading from 28384: heap size 40 MB, throughput 0.975966
Reading from 28384: heap size 40 MB, throughput 0.970892
Reading from 28383: heap size 30 MB, throughput 0.919035
Reading from 28384: heap size 48 MB, throughput 0.984362
Reading from 28383: heap size 31 MB, throughput 0.697926
Reading from 28384: heap size 48 MB, throughput 0.981745
Reading from 28384: heap size 56 MB, throughput 0.970042
Reading from 28384: heap size 57 MB, throughput 0.978632
Reading from 28383: heap size 34 MB, throughput 0.452928
Reading from 28384: heap size 63 MB, throughput 0.980007
Reading from 28383: heap size 44 MB, throughput 0.933304
Reading from 28384: heap size 64 MB, throughput 0.989466
Reading from 28383: heap size 50 MB, throughput 0.916869
Reading from 28384: heap size 70 MB, throughput 0.990203
Reading from 28384: heap size 71 MB, throughput 0.989538
Reading from 28384: heap size 78 MB, throughput 0.986945
Reading from 28383: heap size 51 MB, throughput 0.278696
Reading from 28384: heap size 79 MB, throughput 0.990432
Reading from 28383: heap size 68 MB, throughput 0.236685
Reading from 28384: heap size 85 MB, throughput 0.995385
Reading from 28383: heap size 88 MB, throughput 0.673313
Reading from 28383: heap size 92 MB, throughput 0.718668
Reading from 28384: heap size 86 MB, throughput 0.984827
Reading from 28384: heap size 92 MB, throughput 0.993927
Reading from 28383: heap size 93 MB, throughput 0.188386
Reading from 28384: heap size 92 MB, throughput 0.988052
Reading from 28383: heap size 124 MB, throughput 0.768483
Reading from 28384: heap size 98 MB, throughput 0.991708
Reading from 28383: heap size 124 MB, throughput 0.827071
Reading from 28383: heap size 126 MB, throughput 0.752046
Reading from 28384: heap size 98 MB, throughput 0.989803
Reading from 28384: heap size 104 MB, throughput 0.995812
Reading from 28384: heap size 104 MB, throughput 0.992189
Reading from 28383: heap size 130 MB, throughput 0.157934
Reading from 28383: heap size 172 MB, throughput 0.705
Reading from 28384: heap size 108 MB, throughput 0.993563
Reading from 28383: heap size 172 MB, throughput 0.69902
Reading from 28384: heap size 109 MB, throughput 0.9919
Reading from 28383: heap size 175 MB, throughput 0.742741
Reading from 28383: heap size 179 MB, throughput 0.640591
Reading from 28384: heap size 114 MB, throughput 0.99448
Reading from 28383: heap size 187 MB, throughput 0.706794
Reading from 28384: heap size 114 MB, throughput 0.991818
Reading from 28383: heap size 191 MB, throughput 0.666388
Reading from 28384: heap size 118 MB, throughput 0.991851
Reading from 28384: heap size 118 MB, throughput 0.991394
Reading from 28383: heap size 195 MB, throughput 0.157555
Reading from 28384: heap size 123 MB, throughput 0.995224
Reading from 28383: heap size 241 MB, throughput 0.603776
Reading from 28384: heap size 123 MB, throughput 0.992824
Reading from 28383: heap size 246 MB, throughput 0.615371
Reading from 28384: heap size 128 MB, throughput 0.992287
Reading from 28383: heap size 251 MB, throughput 0.622228
Reading from 28383: heap size 255 MB, throughput 0.570182
Reading from 28384: heap size 128 MB, throughput 0.99261
Reading from 28383: heap size 263 MB, throughput 0.547544
Reading from 28384: heap size 133 MB, throughput 0.992304
Reading from 28383: heap size 269 MB, throughput 0.575702
Reading from 28383: heap size 278 MB, throughput 0.471363
Reading from 28384: heap size 133 MB, throughput 0.994419
Reading from 28384: heap size 138 MB, throughput 0.993998
Reading from 28384: heap size 138 MB, throughput 0.993742
Reading from 28384: heap size 142 MB, throughput 0.997417
Reading from 28383: heap size 285 MB, throughput 0.078062
Reading from 28383: heap size 337 MB, throughput 0.417368
Reading from 28384: heap size 142 MB, throughput 0.995352
Reading from 28384: heap size 146 MB, throughput 0.996452
Reading from 28384: heap size 146 MB, throughput 0.997112
Reading from 28383: heap size 330 MB, throughput 0.124106
Reading from 28383: heap size 388 MB, throughput 0.468942
Reading from 28384: heap size 149 MB, throughput 0.995485
Reading from 28383: heap size 382 MB, throughput 0.679424
Reading from 28383: heap size 386 MB, throughput 0.652774
Reading from 28384: heap size 150 MB, throughput 0.993823
Reading from 28383: heap size 382 MB, throughput 0.610983
Reading from 28383: heap size 384 MB, throughput 0.52453
Reading from 28384: heap size 153 MB, throughput 0.996727
Reading from 28383: heap size 385 MB, throughput 0.505482
Reading from 28383: heap size 390 MB, throughput 0.503797
Reading from 28384: heap size 153 MB, throughput 0.996766
Reading from 28383: heap size 396 MB, throughput 0.503382
Reading from 28384: heap size 156 MB, throughput 0.996925
Reading from 28383: heap size 403 MB, throughput 0.54302
Reading from 28383: heap size 409 MB, throughput 0.473669
Reading from 28384: heap size 156 MB, throughput 0.997031
Reading from 28383: heap size 415 MB, throughput 0.465617
Reading from 28384: heap size 159 MB, throughput 0.995387
Reading from 28384: heap size 160 MB, throughput 0.993595
Reading from 28384: heap size 162 MB, throughput 0.996193
Reading from 28384: heap size 162 MB, throughput 0.997504
Reading from 28383: heap size 419 MB, throughput 0.0762771
Reading from 28384: heap size 166 MB, throughput 0.995821
Reading from 28383: heap size 480 MB, throughput 0.454571
Equal recommendation: 1828 MB each
Reading from 28383: heap size 485 MB, throughput 0.553589
Reading from 28384: heap size 166 MB, throughput 0.997258
Reading from 28383: heap size 488 MB, throughput 0.556498
Reading from 28384: heap size 169 MB, throughput 0.993522
Reading from 28384: heap size 169 MB, throughput 0.995736
Reading from 28384: heap size 172 MB, throughput 0.997026
Reading from 28383: heap size 489 MB, throughput 0.103925
Reading from 28384: heap size 172 MB, throughput 0.994258
Reading from 28383: heap size 548 MB, throughput 0.417857
Reading from 28383: heap size 542 MB, throughput 0.564575
Reading from 28384: heap size 176 MB, throughput 0.984702
Reading from 28383: heap size 548 MB, throughput 0.589013
Reading from 28383: heap size 549 MB, throughput 0.537012
Reading from 28384: heap size 176 MB, throughput 0.995225
Reading from 28383: heap size 553 MB, throughput 0.467619
Reading from 28384: heap size 181 MB, throughput 0.996009
Reading from 28383: heap size 558 MB, throughput 0.526181
Reading from 28384: heap size 182 MB, throughput 0.930753
Reading from 28383: heap size 567 MB, throughput 0.509703
Reading from 28384: heap size 187 MB, throughput 0.993927
Reading from 28384: heap size 187 MB, throughput 0.996368
Reading from 28384: heap size 197 MB, throughput 0.995712
Reading from 28383: heap size 575 MB, throughput 0.0924478
Reading from 28384: heap size 199 MB, throughput 0.99481
Reading from 28383: heap size 644 MB, throughput 0.338689
Reading from 28383: heap size 655 MB, throughput 0.474958
Reading from 28384: heap size 207 MB, throughput 0.997119
Reading from 28383: heap size 656 MB, throughput 0.465288
Reading from 28384: heap size 207 MB, throughput 0.994478
Reading from 28384: heap size 214 MB, throughput 0.99478
Reading from 28383: heap size 663 MB, throughput 0.782966
Reading from 28384: heap size 215 MB, throughput 0.996482
Reading from 28384: heap size 221 MB, throughput 0.994492
Reading from 28383: heap size 665 MB, throughput 0.795537
Reading from 28384: heap size 221 MB, throughput 0.995037
Reading from 28384: heap size 227 MB, throughput 0.995262
Reading from 28383: heap size 665 MB, throughput 0.85577
Reading from 28383: heap size 678 MB, throughput 0.410955
Reading from 28384: heap size 228 MB, throughput 0.996736
Reading from 28384: heap size 233 MB, throughput 0.997708
Reading from 28384: heap size 233 MB, throughput 0.99708
Reading from 28384: heap size 238 MB, throughput 0.996248
Reading from 28383: heap size 687 MB, throughput 0.0320677
Reading from 28384: heap size 238 MB, throughput 0.996842
Reading from 28383: heap size 766 MB, throughput 0.405061
Reading from 28383: heap size 775 MB, throughput 0.259746
Reading from 28383: heap size 777 MB, throughput 0.273524
Reading from 28384: heap size 242 MB, throughput 0.995676
Reading from 28384: heap size 243 MB, throughput 0.997079
Equal recommendation: 1828 MB each
Reading from 28384: heap size 247 MB, throughput 0.997179
Reading from 28384: heap size 247 MB, throughput 0.996014
Reading from 28383: heap size 762 MB, throughput 0.0305218
Reading from 28383: heap size 849 MB, throughput 0.337773
Reading from 28384: heap size 251 MB, throughput 0.9976
Reading from 28384: heap size 251 MB, throughput 0.995948
Reading from 28383: heap size 851 MB, throughput 0.842915
Reading from 28383: heap size 853 MB, throughput 0.520615
Reading from 28383: heap size 853 MB, throughput 0.534206
Reading from 28384: heap size 256 MB, throughput 0.99538
Reading from 28383: heap size 855 MB, throughput 0.590111
Reading from 28383: heap size 849 MB, throughput 0.766518
Reading from 28384: heap size 256 MB, throughput 0.997396
Reading from 28383: heap size 854 MB, throughput 0.742204
Reading from 28384: heap size 260 MB, throughput 0.99563
Reading from 28384: heap size 260 MB, throughput 0.956336
Reading from 28384: heap size 265 MB, throughput 0.996832
Reading from 28384: heap size 266 MB, throughput 0.995358
Reading from 28383: heap size 854 MB, throughput 0.0431381
Reading from 28384: heap size 275 MB, throughput 0.99847
Reading from 28383: heap size 947 MB, throughput 0.635109
Reading from 28383: heap size 954 MB, throughput 0.849177
Reading from 28384: heap size 275 MB, throughput 0.997298
Reading from 28383: heap size 956 MB, throughput 0.886791
Reading from 28384: heap size 283 MB, throughput 0.997194
Reading from 28383: heap size 943 MB, throughput 0.721263
Reading from 28383: heap size 815 MB, throughput 0.699548
Reading from 28383: heap size 934 MB, throughput 0.686368
Reading from 28384: heap size 283 MB, throughput 0.997372
Reading from 28383: heap size 822 MB, throughput 0.738247
Reading from 28383: heap size 926 MB, throughput 0.764714
Reading from 28384: heap size 291 MB, throughput 0.996746
Reading from 28383: heap size 934 MB, throughput 0.736448
Reading from 28383: heap size 925 MB, throughput 0.76525
Reading from 28384: heap size 291 MB, throughput 0.996027
Reading from 28383: heap size 930 MB, throughput 0.789612
Reading from 28383: heap size 922 MB, throughput 0.775023
Reading from 28384: heap size 300 MB, throughput 0.997823
Reading from 28384: heap size 300 MB, throughput 0.996944
Reading from 28384: heap size 307 MB, throughput 0.997665
Reading from 28383: heap size 927 MB, throughput 0.963765
Reading from 28384: heap size 308 MB, throughput 0.997259
Reading from 28383: heap size 924 MB, throughput 0.921373
Reading from 28384: heap size 316 MB, throughput 0.998197
Reading from 28383: heap size 928 MB, throughput 0.731306
Reading from 28383: heap size 928 MB, throughput 0.738253
Equal recommendation: 1828 MB each
Reading from 28384: heap size 316 MB, throughput 0.997489
Reading from 28383: heap size 930 MB, throughput 0.728439
Reading from 28383: heap size 926 MB, throughput 0.746249
Reading from 28384: heap size 324 MB, throughput 0.998186
Reading from 28383: heap size 930 MB, throughput 0.763978
Reading from 28383: heap size 929 MB, throughput 0.786469
Reading from 28384: heap size 324 MB, throughput 0.996751
Reading from 28383: heap size 933 MB, throughput 0.764567
Reading from 28384: heap size 331 MB, throughput 0.998086
Reading from 28383: heap size 934 MB, throughput 0.867687
Reading from 28384: heap size 331 MB, throughput 0.998222
Reading from 28383: heap size 938 MB, throughput 0.871958
Reading from 28383: heap size 944 MB, throughput 0.671539
Reading from 28384: heap size 338 MB, throughput 0.998641
Reading from 28383: heap size 945 MB, throughput 0.622052
Reading from 28383: heap size 965 MB, throughput 0.625734
Reading from 28384: heap size 338 MB, throughput 0.998252
Reading from 28383: heap size 975 MB, throughput 0.592561
Reading from 28384: heap size 345 MB, throughput 0.995701
Reading from 28384: heap size 345 MB, throughput 0.996176
Reading from 28384: heap size 354 MB, throughput 0.996496
Reading from 28384: heap size 354 MB, throughput 0.995645
Reading from 28383: heap size 991 MB, throughput 0.0581225
Reading from 28383: heap size 1092 MB, throughput 0.50325
Reading from 28384: heap size 365 MB, throughput 0.99745
Reading from 28383: heap size 1094 MB, throughput 0.64324
Reading from 28383: heap size 1100 MB, throughput 0.629186
Reading from 28384: heap size 365 MB, throughput 0.997033
Reading from 28383: heap size 1113 MB, throughput 0.636006
Reading from 28383: heap size 1113 MB, throughput 0.617834
Reading from 28384: heap size 376 MB, throughput 0.994588
Reading from 28384: heap size 376 MB, throughput 0.997021
Reading from 28384: heap size 387 MB, throughput 0.99798
Reading from 28384: heap size 388 MB, throughput 0.996361
Reading from 28383: heap size 1129 MB, throughput 0.951407
Equal recommendation: 1828 MB each
Reading from 28384: heap size 400 MB, throughput 0.997999
Reading from 28384: heap size 400 MB, throughput 0.997079
Reading from 28383: heap size 1133 MB, throughput 0.946922
Reading from 28384: heap size 411 MB, throughput 0.997063
Reading from 28384: heap size 411 MB, throughput 0.996793
Reading from 28384: heap size 423 MB, throughput 0.997802
Reading from 28384: heap size 423 MB, throughput 0.99755
Reading from 28383: heap size 1148 MB, throughput 0.943895
Reading from 28384: heap size 433 MB, throughput 0.996805
Reading from 28384: heap size 434 MB, throughput 0.997098
Reading from 28383: heap size 1151 MB, throughput 0.922457
Reading from 28384: heap size 446 MB, throughput 0.99777
Reading from 28384: heap size 446 MB, throughput 0.99715
Reading from 28384: heap size 457 MB, throughput 0.997137
Reading from 28383: heap size 1153 MB, throughput 0.946343
Reading from 28384: heap size 458 MB, throughput 0.993549
Reading from 28384: heap size 470 MB, throughput 0.997894
Reading from 28384: heap size 470 MB, throughput 0.997839
Reading from 28383: heap size 1158 MB, throughput 0.938368
Reading from 28384: heap size 482 MB, throughput 0.997917
Equal recommendation: 1828 MB each
Reading from 28384: heap size 483 MB, throughput 0.997316
Reading from 28384: heap size 496 MB, throughput 0.997202
Reading from 28383: heap size 1151 MB, throughput 0.938208
Reading from 28384: heap size 496 MB, throughput 0.997391
Reading from 28384: heap size 509 MB, throughput 0.997965
Reading from 28383: heap size 1159 MB, throughput 0.958523
Reading from 28384: heap size 509 MB, throughput 0.99748
Reading from 28384: heap size 522 MB, throughput 0.997804
Reading from 28384: heap size 522 MB, throughput 0.997795
Reading from 28383: heap size 1157 MB, throughput 0.944486
Reading from 28384: heap size 534 MB, throughput 0.998257
Reading from 28384: heap size 534 MB, throughput 0.997447
Reading from 28384: heap size 546 MB, throughput 0.997419
Reading from 28383: heap size 1161 MB, throughput 0.934092
Reading from 28384: heap size 546 MB, throughput 0.998011
Reading from 28384: heap size 558 MB, throughput 0.998243
Equal recommendation: 1828 MB each
Reading from 28383: heap size 1169 MB, throughput 0.937466
Reading from 28384: heap size 559 MB, throughput 0.997926
Reading from 28384: heap size 571 MB, throughput 0.997441
Reading from 28384: heap size 571 MB, throughput 0.997955
Reading from 28383: heap size 1169 MB, throughput 0.93732
Reading from 28384: heap size 583 MB, throughput 0.997946
Reading from 28384: heap size 583 MB, throughput 0.997258
Reading from 28383: heap size 1178 MB, throughput 0.927757
Reading from 28384: heap size 596 MB, throughput 0.998808
Reading from 28384: heap size 596 MB, throughput 0.99806
Reading from 28384: heap size 607 MB, throughput 0.997972
Reading from 28383: heap size 1180 MB, throughput 0.927394
Reading from 28384: heap size 607 MB, throughput 0.99825
Reading from 28384: heap size 619 MB, throughput 0.998485
Reading from 28383: heap size 1189 MB, throughput 0.937422
Reading from 28384: heap size 619 MB, throughput 0.998403
Equal recommendation: 1828 MB each
Reading from 28384: heap size 631 MB, throughput 0.997982
Reading from 28384: heap size 631 MB, throughput 0.998098
Reading from 28383: heap size 1193 MB, throughput 0.923583
Reading from 28384: heap size 643 MB, throughput 0.998158
Reading from 28384: heap size 643 MB, throughput 0.998066
Reading from 28383: heap size 1202 MB, throughput 0.945692
Reading from 28384: heap size 655 MB, throughput 0.998633
Reading from 28384: heap size 655 MB, throughput 0.99838
Reading from 28384: heap size 666 MB, throughput 0.997985
Reading from 28383: heap size 1208 MB, throughput 0.936156
Reading from 28384: heap size 666 MB, throughput 0.998334
Reading from 28384: heap size 678 MB, throughput 0.99829
Reading from 28383: heap size 1217 MB, throughput 0.938906
Equal recommendation: 1828 MB each
Reading from 28384: heap size 678 MB, throughput 0.998292
Reading from 28384: heap size 690 MB, throughput 0.997814
Reading from 28383: heap size 1221 MB, throughput 0.929078
Reading from 28384: heap size 690 MB, throughput 0.998717
Reading from 28384: heap size 702 MB, throughput 0.998024
Reading from 28384: heap size 703 MB, throughput 0.998494
Reading from 28383: heap size 1231 MB, throughput 0.936639
Reading from 28384: heap size 715 MB, throughput 0.998887
Reading from 28384: heap size 715 MB, throughput 0.997905
Reading from 28384: heap size 727 MB, throughput 0.998577
Reading from 28383: heap size 1233 MB, throughput 0.492943
Reading from 28384: heap size 727 MB, throughput 0.998952
Equal recommendation: 1828 MB each
Reading from 28384: heap size 738 MB, throughput 0.998363
Reading from 28384: heap size 739 MB, throughput 0.997576
Reading from 28383: heap size 1333 MB, throughput 0.930871
Reading from 28384: heap size 750 MB, throughput 0.998833
Reading from 28384: heap size 750 MB, throughput 0.998198
Reading from 28383: heap size 1335 MB, throughput 0.958189
Reading from 28384: heap size 763 MB, throughput 0.998965
Reading from 28384: heap size 763 MB, throughput 0.998324
Reading from 28383: heap size 1348 MB, throughput 0.96547
Reading from 28384: heap size 775 MB, throughput 0.998884
Reading from 28384: heap size 775 MB, throughput 0.998708
Equal recommendation: 1828 MB each
Reading from 28383: heap size 1349 MB, throughput 0.9603
Reading from 28384: heap size 787 MB, throughput 0.998575
Reading from 28384: heap size 787 MB, throughput 0.998629
Reading from 28384: heap size 798 MB, throughput 0.998562
Reading from 28383: heap size 1349 MB, throughput 0.958065
Reading from 28384: heap size 798 MB, throughput 0.998727
Reading from 28384: heap size 810 MB, throughput 0.998746
Reading from 28383: heap size 1353 MB, throughput 0.960154
Reading from 28384: heap size 810 MB, throughput 0.998343
Reading from 28384: heap size 821 MB, throughput 0.998938
Reading from 28383: heap size 1346 MB, throughput 0.9593
Reading from 28384: heap size 821 MB, throughput 0.998694
Equal recommendation: 1828 MB each
Reading from 28384: heap size 833 MB, throughput 0.998808
Reading from 28383: heap size 1351 MB, throughput 0.957548
Reading from 28384: heap size 833 MB, throughput 0.998961
Reading from 28384: heap size 844 MB, throughput 0.99828
Reading from 28384: heap size 844 MB, throughput 0.998716
Reading from 28383: heap size 1346 MB, throughput 0.95652
Reading from 28384: heap size 856 MB, throughput 0.998888
Reading from 28384: heap size 856 MB, throughput 0.998536
Reading from 28383: heap size 1350 MB, throughput 0.957696
Reading from 28384: heap size 867 MB, throughput 0.998817
Equal recommendation: 1828 MB each
Reading from 28384: heap size 867 MB, throughput 0.998608
Reading from 28383: heap size 1356 MB, throughput 0.950612
Reading from 28384: heap size 879 MB, throughput 0.998848
Reading from 28384: heap size 879 MB, throughput 0.998373
Reading from 28384: heap size 891 MB, throughput 0.998883
Reading from 28383: heap size 1358 MB, throughput 0.941529
Reading from 28384: heap size 891 MB, throughput 0.99874
Reading from 28384: heap size 903 MB, throughput 0.998633
Reading from 28383: heap size 1366 MB, throughput 0.946534
Reading from 28384: heap size 903 MB, throughput 0.998891
Reading from 28384: heap size 915 MB, throughput 0.998845
Equal recommendation: 1828 MB each
Reading from 28383: heap size 1371 MB, throughput 0.947379
Reading from 28384: heap size 916 MB, throughput 0.99889
Reading from 28384: heap size 927 MB, throughput 0.999013
Reading from 28383: heap size 1380 MB, throughput 0.938701
Reading from 28384: heap size 927 MB, throughput 0.998908
Reading from 28384: heap size 938 MB, throughput 0.998599
Reading from 28384: heap size 939 MB, throughput 0.998952
Reading from 28384: heap size 950 MB, throughput 0.998898
Equal recommendation: 1828 MB each
Reading from 28384: heap size 950 MB, throughput 0.998852
Reading from 28384: heap size 962 MB, throughput 0.999038
Reading from 28384: heap size 962 MB, throughput 0.998649
Reading from 28384: heap size 973 MB, throughput 0.999121
Reading from 28384: heap size 973 MB, throughput 0.998677
Reading from 28384: heap size 985 MB, throughput 0.99913
Reading from 28384: heap size 985 MB, throughput 0.998776
Equal recommendation: 1828 MB each
Reading from 28384: heap size 996 MB, throughput 0.99873
Reading from 28384: heap size 996 MB, throughput 0.998672
Reading from 28384: heap size 1008 MB, throughput 0.99885
Reading from 28384: heap size 1008 MB, throughput 0.998765
Reading from 28384: heap size 1021 MB, throughput 0.999024
Reading from 28384: heap size 1021 MB, throughput 0.998958
Equal recommendation: 1828 MB each
Reading from 28384: heap size 1032 MB, throughput 0.998953
Reading from 28383: heap size 1389 MB, throughput 0.993713
Reading from 28384: heap size 1033 MB, throughput 0.999044
Reading from 28384: heap size 1044 MB, throughput 0.998982
Reading from 28384: heap size 1044 MB, throughput 0.99882
Reading from 28384: heap size 1055 MB, throughput 0.998957
Reading from 28384: heap size 1055 MB, throughput 0.99882
Reading from 28383: heap size 1387 MB, throughput 0.867443
Equal recommendation: 1828 MB each
Reading from 28383: heap size 1464 MB, throughput 0.963474
Reading from 28383: heap size 1505 MB, throughput 0.885713
Reading from 28383: heap size 1511 MB, throughput 0.875986
Reading from 28384: heap size 1067 MB, throughput 0.999178
Reading from 28383: heap size 1510 MB, throughput 0.881238
Reading from 28383: heap size 1514 MB, throughput 0.888647
Reading from 28384: heap size 1067 MB, throughput 0.998952
Reading from 28383: heap size 1515 MB, throughput 0.987925
Reading from 28384: heap size 1080 MB, throughput 0.999098
Reading from 28383: heap size 1521 MB, throughput 0.986476
Reading from 28384: heap size 1080 MB, throughput 0.998763
Reading from 28384: heap size 1091 MB, throughput 0.999002
Reading from 28383: heap size 1544 MB, throughput 0.982799
Reading from 28384: heap size 1091 MB, throughput 0.999068
Equal recommendation: 1828 MB each
Reading from 28384: heap size 1103 MB, throughput 0.998979
Reading from 28383: heap size 1546 MB, throughput 0.978662
Reading from 28384: heap size 1103 MB, throughput 0.998947
Reading from 28384: heap size 1115 MB, throughput 0.99901
Reading from 28383: heap size 1541 MB, throughput 0.97844
Reading from 28384: heap size 1115 MB, throughput 0.999013
Reading from 28384: heap size 1127 MB, throughput 0.999074
Reading from 28383: heap size 1549 MB, throughput 0.974703
Reading from 28384: heap size 1127 MB, throughput 0.999054
Equal recommendation: 1828 MB each
Reading from 28384: heap size 1139 MB, throughput 0.998945
Reading from 28383: heap size 1529 MB, throughput 0.972918
Reading from 28384: heap size 1139 MB, throughput 0.99907
Reading from 28384: heap size 1151 MB, throughput 0.99904
Reading from 28383: heap size 1420 MB, throughput 0.96734
Reading from 28384: heap size 1151 MB, throughput 0.998932
Reading from 28384: heap size 1163 MB, throughput 0.999092
Equal recommendation: 1828 MB each
Reading from 28383: heap size 1527 MB, throughput 0.966132
Reading from 28384: heap size 1163 MB, throughput 0.998956
Reading from 28384: heap size 1174 MB, throughput 0.999254
Reading from 28383: heap size 1533 MB, throughput 0.965826
Reading from 28384: heap size 1175 MB, throughput 0.998986
Reading from 28384: heap size 1185 MB, throughput 0.999292
Reading from 28383: heap size 1540 MB, throughput 0.961891
Reading from 28384: heap size 1186 MB, throughput 0.999173
Reading from 28384: heap size 1196 MB, throughput 0.998919
Equal recommendation: 1828 MB each
Reading from 28383: heap size 1542 MB, throughput 0.953369
Reading from 28384: heap size 1197 MB, throughput 0.999269
Reading from 28384: heap size 1208 MB, throughput 0.998995
Reading from 28383: heap size 1551 MB, throughput 0.956581
Reading from 28384: heap size 1208 MB, throughput 0.999171
Reading from 28384: heap size 1219 MB, throughput 0.999249
Reading from 28383: heap size 1560 MB, throughput 0.952506
Reading from 28384: heap size 1220 MB, throughput 0.999254
Equal recommendation: 1828 MB each
Reading from 28384: heap size 1226 MB, throughput 0.999299
Reading from 28383: heap size 1572 MB, throughput 0.946321
Reading from 28384: heap size 1226 MB, throughput 0.999453
Reading from 28384: heap size 1227 MB, throughput 0.999415
Reading from 28383: heap size 1585 MB, throughput 0.944707
Reading from 28384: heap size 1227 MB, throughput 0.999439
Reading from 28384: heap size 1227 MB, throughput 0.999371
Equal recommendation: 1828 MB each
Reading from 28383: heap size 1599 MB, throughput 0.945156
Reading from 28384: heap size 1227 MB, throughput 0.999375
Reading from 28384: heap size 1227 MB, throughput 0.999404
Reading from 28383: heap size 1614 MB, throughput 0.938109
Reading from 28384: heap size 1227 MB, throughput 0.999442
Reading from 28384: heap size 1167 MB, throughput 0.999128
Reading from 28384: heap size 1110 MB, throughput 0.988273
Reading from 28383: heap size 1633 MB, throughput 0.947897
Equal recommendation: 1828 MB each
Reading from 28384: heap size 1057 MB, throughput 0.999448
Reading from 28384: heap size 1069 MB, throughput 0.999423
Reading from 28383: heap size 1642 MB, throughput 0.943174
Reading from 28384: heap size 1080 MB, throughput 0.999464
Reading from 28384: heap size 1090 MB, throughput 0.999434
Reading from 28383: heap size 1661 MB, throughput 0.952864
Reading from 28384: heap size 1100 MB, throughput 0.999442
Reading from 28384: heap size 1110 MB, throughput 0.999196
Equal recommendation: 1828 MB each
Reading from 28384: heap size 1124 MB, throughput 0.999067
Reading from 28383: heap size 1667 MB, throughput 0.942809
Reading from 28384: heap size 1136 MB, throughput 0.999101
Reading from 28384: heap size 1149 MB, throughput 0.999398
Reading from 28383: heap size 1685 MB, throughput 0.947162
Reading from 28384: heap size 1161 MB, throughput 0.999195
Reading from 28384: heap size 1174 MB, throughput 0.999166
Reading from 28383: heap size 1688 MB, throughput 0.949992
Reading from 28384: heap size 1188 MB, throughput 0.999288
Equal recommendation: 1828 MB each
Reading from 28384: heap size 1201 MB, throughput 0.999249
Reading from 28384: heap size 1207 MB, throughput 0.998589
Reading from 28383: heap size 1707 MB, throughput 0.709907
Reading from 28384: heap size 1217 MB, throughput 0.999438
Reading from 28384: heap size 1217 MB, throughput 0.999183
Reading from 28384: heap size 1228 MB, throughput 0.999074
Equal recommendation: 1828 MB each
Reading from 28384: heap size 1228 MB, throughput 0.999266
Reading from 28383: heap size 1839 MB, throughput 0.967921
Reading from 28384: heap size 1229 MB, throughput 0.999448
Reading from 28384: heap size 1229 MB, throughput 0.999468
Reading from 28384: heap size 1229 MB, throughput 0.999546
Reading from 28384: heap size 1227 MB, throughput 0.999413
Equal recommendation: 1828 MB each
Reading from 28384: heap size 1229 MB, throughput 0.999352
Reading from 28384: heap size 1229 MB, throughput 0.999384
Reading from 28384: heap size 1230 MB, throughput 0.999417
Reading from 28384: heap size 1230 MB, throughput 0.999526
Reading from 28384: heap size 1230 MB, throughput 0.999415
Equal recommendation: 1828 MB each
Reading from 28384: heap size 1230 MB, throughput 0.999382
Reading from 28384: heap size 1230 MB, throughput 0.99941
Reading from 28384: heap size 1230 MB, throughput 0.999468
Reading from 28383: heap size 1820 MB, throughput 0.989095
Reading from 28384: heap size 1230 MB, throughput 0.999524
Reading from 28384: heap size 1230 MB, throughput 0.999406
Reading from 28383: heap size 1839 MB, throughput 0.966568
Reading from 28383: heap size 1810 MB, throughput 0.761787
Equal recommendation: 1828 MB each
Reading from 28383: heap size 1832 MB, throughput 0.765335
Reading from 28384: heap size 1230 MB, throughput 0.999244
Reading from 28383: heap size 1801 MB, throughput 0.767222
Client 28383 died
Clients: 1
Reading from 28384: heap size 1230 MB, throughput 0.999418
Reading from 28384: heap size 1230 MB, throughput 0.99918
Reading from 28384: heap size 1229 MB, throughput 0.999381
Reading from 28384: heap size 1229 MB, throughput 0.999209
Reading from 28384: heap size 1229 MB, throughput 0.998997
Recommendation: one client; give it all the memory
Reading from 28384: heap size 1230 MB, throughput 0.999142
Reading from 28384: heap size 1230 MB, throughput 0.999253
Client 28384 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
