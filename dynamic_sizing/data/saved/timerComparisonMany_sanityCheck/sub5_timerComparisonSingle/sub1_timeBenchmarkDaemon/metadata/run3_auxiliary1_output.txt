economemd
    total memory: 1049 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    always use numeric optimisation: yes
    make recommendations: yes
    equal recommendations: no
    random recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2015_02_13_14_49_56/sub5_timerComparisonSingle/sub1_timeBenchmarkDaemon/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 8775: heap size 9 MB, throughput 0.991682
Clients: 1
Client 8775 has a minimum heap size of 276 MB
Reading from 8776: heap size 9 MB, throughput 0.99326
Clients: 2
Client 8776 has a minimum heap size of 276 MB
Reading from 8775: heap size 9 MB, throughput 0.983941
Reading from 8776: heap size 9 MB, throughput 0.989643
Reading from 8775: heap size 9 MB, throughput 0.979282
Reading from 8776: heap size 9 MB, throughput 0.983219
Reading from 8775: heap size 9 MB, throughput 0.970237
Reading from 8776: heap size 9 MB, throughput 0.973877
Reading from 8775: heap size 11 MB, throughput 0.981012
Reading from 8776: heap size 11 MB, throughput 0.983119
Reading from 8775: heap size 11 MB, throughput 0.974482
Reading from 8776: heap size 11 MB, throughput 0.977854
Reading from 8775: heap size 17 MB, throughput 0.964228
Reading from 8776: heap size 17 MB, throughput 0.960276
Reading from 8775: heap size 17 MB, throughput 0.857171
Reading from 8776: heap size 17 MB, throughput 0.89078
Reading from 8775: heap size 30 MB, throughput 0.697851
Reading from 8776: heap size 30 MB, throughput 0.789653
Reading from 8775: heap size 31 MB, throughput 0.268162
Reading from 8776: heap size 31 MB, throughput 0.303367
Reading from 8775: heap size 34 MB, throughput 0.619451
Reading from 8776: heap size 33 MB, throughput 0.63073
Reading from 8776: heap size 44 MB, throughput 0.344662
Reading from 8775: heap size 44 MB, throughput 0.180223
Reading from 8775: heap size 46 MB, throughput 0.17038
Reading from 8776: heap size 46 MB, throughput 0.510607
Reading from 8776: heap size 61 MB, throughput 0.45607
Reading from 8776: heap size 67 MB, throughput 0.422446
Reading from 8775: heap size 49 MB, throughput 0.576638
Reading from 8775: heap size 66 MB, throughput 0.126944
Reading from 8775: heap size 68 MB, throughput 0.304258
Reading from 8776: heap size 68 MB, throughput 0.400188
Reading from 8775: heap size 93 MB, throughput 0.367973
Reading from 8776: heap size 95 MB, throughput 0.405123
Reading from 8775: heap size 94 MB, throughput 0.233189
Reading from 8775: heap size 96 MB, throughput 0.720648
Reading from 8775: heap size 99 MB, throughput 0.657984
Reading from 8776: heap size 96 MB, throughput 0.373194
Reading from 8776: heap size 127 MB, throughput 0.599868
Reading from 8776: heap size 127 MB, throughput 0.502666
Reading from 8776: heap size 129 MB, throughput 0.341228
Reading from 8775: heap size 101 MB, throughput 0.413344
Reading from 8776: heap size 133 MB, throughput 0.322915
Reading from 8775: heap size 131 MB, throughput 0.695296
Reading from 8775: heap size 134 MB, throughput 0.675489
Reading from 8775: heap size 137 MB, throughput 0.685452
Reading from 8775: heap size 142 MB, throughput 0.686145
Reading from 8776: heap size 136 MB, throughput 0.447312
Reading from 8775: heap size 147 MB, throughput 0.616076
Reading from 8776: heap size 172 MB, throughput 0.332006
Reading from 8776: heap size 176 MB, throughput 0.163801
Reading from 8776: heap size 179 MB, throughput 0.645605
Reading from 8776: heap size 188 MB, throughput 0.690587
Reading from 8775: heap size 155 MB, throughput 0.446287
Reading from 8775: heap size 189 MB, throughput 0.503738
Reading from 8775: heap size 196 MB, throughput 0.550696
Reading from 8775: heap size 197 MB, throughput 0.552547
Reading from 8776: heap size 191 MB, throughput 0.536249
Reading from 8775: heap size 203 MB, throughput 0.747501
Reading from 8776: heap size 229 MB, throughput 0.878907
Reading from 8776: heap size 238 MB, throughput 0.828391
Reading from 8775: heap size 206 MB, throughput 0.848178
Reading from 8776: heap size 243 MB, throughput 0.81405
Reading from 8776: heap size 244 MB, throughput 0.769608
Reading from 8775: heap size 206 MB, throughput 0.818907
Reading from 8776: heap size 249 MB, throughput 0.769258
Reading from 8775: heap size 215 MB, throughput 0.746971
Reading from 8775: heap size 220 MB, throughput 0.706229
Reading from 8776: heap size 250 MB, throughput 0.767653
Reading from 8776: heap size 255 MB, throughput 0.715083
Reading from 8776: heap size 256 MB, throughput 0.701901
Reading from 8776: heap size 263 MB, throughput 0.690976
Reading from 8775: heap size 226 MB, throughput 0.595419
Reading from 8776: heap size 267 MB, throughput 0.634607
Reading from 8775: heap size 266 MB, throughput 0.5826
Reading from 8776: heap size 274 MB, throughput 0.673902
Reading from 8775: heap size 268 MB, throughput 0.634978
Reading from 8775: heap size 259 MB, throughput 0.725167
Reading from 8775: heap size 264 MB, throughput 0.758306
Reading from 8775: heap size 265 MB, throughput 0.753169
Reading from 8775: heap size 266 MB, throughput 0.722962
Reading from 8776: heap size 276 MB, throughput 0.88776
Reading from 8775: heap size 271 MB, throughput 0.720336
Reading from 8776: heap size 282 MB, throughput 0.862852
Reading from 8775: heap size 271 MB, throughput 0.725061
Reading from 8776: heap size 283 MB, throughput 0.767029
Reading from 8775: heap size 269 MB, throughput 0.502239
Reading from 8775: heap size 310 MB, throughput 0.827334
Reading from 8776: heap size 288 MB, throughput 0.645229
Reading from 8775: heap size 311 MB, throughput 0.836562
Reading from 8776: heap size 328 MB, throughput 0.794307
Reading from 8775: heap size 313 MB, throughput 0.811409
Reading from 8775: heap size 314 MB, throughput 0.795655
Reading from 8776: heap size 333 MB, throughput 0.86874
Reading from 8775: heap size 316 MB, throughput 0.782926
Reading from 8776: heap size 334 MB, throughput 0.86089
Reading from 8776: heap size 334 MB, throughput 0.78008
Reading from 8775: heap size 318 MB, throughput 0.823193
Reading from 8776: heap size 338 MB, throughput 0.745884
Reading from 8775: heap size 319 MB, throughput 0.783433
Reading from 8776: heap size 344 MB, throughput 0.731412
Reading from 8775: heap size 324 MB, throughput 0.73605
Reading from 8776: heap size 344 MB, throughput 0.686994
Reading from 8775: heap size 325 MB, throughput 0.69551
Reading from 8775: heap size 333 MB, throughput 0.650268
Reading from 8775: heap size 334 MB, throughput 0.607482
Reading from 8775: heap size 338 MB, throughput 0.637947
Analytic result (unused):
Recommendation: 2 clients, utility 0.450864:
    h1: 756.204 MB (U(h) = 0.315406*h^0.125422)
    h2: 292.796 MB (U(h) = 0.472441*h^0.0485625)
Recommendation: 2 clients, utility 0.450864:
    h1: 756.209 MB (U(h) = 0.315406*h^0.125422)
    h2: 292.791 MB (U(h) = 0.472441*h^0.0485625)
Reading from 8775: heap size 340 MB, throughput 0.890047
Reading from 8776: heap size 354 MB, throughput 0.937482
Reading from 8776: heap size 315 MB, throughput 0.960332
Reading from 8775: heap size 348 MB, throughput 0.805372
Reading from 8776: heap size 347 MB, throughput 0.966051
Reading from 8775: heap size 392 MB, throughput 0.973038
Reading from 8776: heap size 318 MB, throughput 0.974388
Reading from 8775: heap size 401 MB, throughput 0.984231
Reading from 8776: heap size 339 MB, throughput 0.973275
Reading from 8775: heap size 404 MB, throughput 0.989134
Reading from 8776: heap size 312 MB, throughput 0.976819
Reading from 8775: heap size 409 MB, throughput 0.989084
Reading from 8776: heap size 334 MB, throughput 0.976717
Reading from 8775: heap size 411 MB, throughput 0.987967
Reading from 8776: heap size 315 MB, throughput 0.97376
Analytic result (unused):
Recommendation: 2 clients, utility 0.555592:
    h1: 666.868 MB (U(h) = 0.262376*h^0.170486)
    h2: 382.132 MB (U(h) = 0.390941*h^0.0976923)
Recommendation: 2 clients, utility 0.555592:
    h1: 666.868 MB (U(h) = 0.262376*h^0.170486)
    h2: 382.132 MB (U(h) = 0.390941*h^0.0976923)
Reading from 8775: heap size 410 MB, throughput 0.98688
Reading from 8776: heap size 328 MB, throughput 0.967091
Reading from 8776: heap size 331 MB, throughput 0.966631
Reading from 8775: heap size 413 MB, throughput 0.985312
Reading from 8776: heap size 334 MB, throughput 0.965109
Reading from 8775: heap size 407 MB, throughput 0.983898
Reading from 8776: heap size 334 MB, throughput 0.965209
Reading from 8775: heap size 411 MB, throughput 0.982241
Reading from 8776: heap size 337 MB, throughput 0.965284
Reading from 8776: heap size 338 MB, throughput 0.964878
Reading from 8775: heap size 409 MB, throughput 0.981654
Reading from 8776: heap size 340 MB, throughput 0.966856
Reading from 8775: heap size 410 MB, throughput 0.969977
Reading from 8776: heap size 341 MB, throughput 0.977476
Analytic result (unused):
Recommendation: 2 clients, utility 0.609758:
    h1: 627.94 MB (U(h) = 0.246188*h^0.185886)
    h2: 421.06 MB (U(h) = 0.352115*h^0.124645)
Recommendation: 2 clients, utility 0.609758:
    h1: 627.926 MB (U(h) = 0.246188*h^0.185886)
    h2: 421.074 MB (U(h) = 0.352115*h^0.124645)
Reading from 8776: heap size 341 MB, throughput 0.970297
Reading from 8776: heap size 343 MB, throughput 0.938583
Reading from 8775: heap size 412 MB, throughput 0.97165
Reading from 8775: heap size 413 MB, throughput 0.951181
Reading from 8775: heap size 409 MB, throughput 0.918805
Reading from 8776: heap size 340 MB, throughput 0.934422
Reading from 8775: heap size 418 MB, throughput 0.892627
Reading from 8776: heap size 381 MB, throughput 0.782952
Reading from 8775: heap size 428 MB, throughput 0.872793
Reading from 8776: heap size 388 MB, throughput 0.802068
Reading from 8776: heap size 390 MB, throughput 0.942479
Reading from 8775: heap size 433 MB, throughput 0.972661
Reading from 8776: heap size 395 MB, throughput 0.973123
Reading from 8775: heap size 432 MB, throughput 0.980513
Reading from 8776: heap size 397 MB, throughput 0.9763
Reading from 8775: heap size 436 MB, throughput 0.98195
Reading from 8776: heap size 397 MB, throughput 0.977859
Reading from 8776: heap size 400 MB, throughput 0.976407
Reading from 8775: heap size 433 MB, throughput 0.982122
Reading from 8776: heap size 399 MB, throughput 0.977964
Reading from 8775: heap size 437 MB, throughput 0.981985
Analytic result (unused):
Recommendation: 2 clients, utility 0.675044:
    h1: 609.627 MB (U(h) = 0.224049*h^0.208423)
    h2: 439.373 MB (U(h) = 0.31734*h^0.150215)
Recommendation: 2 clients, utility 0.675044:
    h1: 609.627 MB (U(h) = 0.224049*h^0.208423)
    h2: 439.373 MB (U(h) = 0.31734*h^0.150215)
Reading from 8776: heap size 401 MB, throughput 0.975369
Reading from 8775: heap size 434 MB, throughput 0.982938
Reading from 8776: heap size 404 MB, throughput 0.970103
Reading from 8775: heap size 437 MB, throughput 0.981723
Reading from 8776: heap size 405 MB, throughput 0.969326
Reading from 8775: heap size 437 MB, throughput 0.979203
Reading from 8776: heap size 408 MB, throughput 0.969282
Reading from 8775: heap size 439 MB, throughput 0.979816
Reading from 8776: heap size 411 MB, throughput 0.969721
Reading from 8775: heap size 441 MB, throughput 0.976054
Reading from 8776: heap size 416 MB, throughput 0.971336
Reading from 8775: heap size 442 MB, throughput 0.979604
Reading from 8775: heap size 447 MB, throughput 0.969107
Analytic result (unused):
Recommendation: 2 clients, utility 0.719819:
    h1: 597.683 MB (U(h) = 0.211545*h^0.222058)
    h2: 451.317 MB (U(h) = 0.295245*h^0.167678)
Recommendation: 2 clients, utility 0.719819:
    h1: 597.686 MB (U(h) = 0.211545*h^0.222058)
    h2: 451.314 MB (U(h) = 0.295245*h^0.167678)
Reading from 8775: heap size 448 MB, throughput 0.950379
Reading from 8775: heap size 454 MB, throughput 0.949546
Reading from 8776: heap size 418 MB, throughput 0.977595
Reading from 8776: heap size 415 MB, throughput 0.969917
Reading from 8776: heap size 417 MB, throughput 0.953962
Reading from 8776: heap size 422 MB, throughput 0.925872
Reading from 8776: heap size 424 MB, throughput 0.898274
Reading from 8775: heap size 454 MB, throughput 0.984512
Reading from 8776: heap size 433 MB, throughput 0.971074
Reading from 8776: heap size 434 MB, throughput 0.979665
Reading from 8775: heap size 460 MB, throughput 0.989838
Reading from 8776: heap size 439 MB, throughput 0.980503
Reading from 8775: heap size 461 MB, throughput 0.986586
Reading from 8776: heap size 441 MB, throughput 0.983904
Reading from 8775: heap size 461 MB, throughput 0.987163
Analytic result (unused):
Recommendation: 2 clients, utility 0.757308:
    h1: 582.346 MB (U(h) = 0.20409*h^0.230491)
    h2: 466.654 MB (U(h) = 0.274877*h^0.1847)
Recommendation: 2 clients, utility 0.757308:
    h1: 582.337 MB (U(h) = 0.20409*h^0.230491)
    h2: 466.663 MB (U(h) = 0.274877*h^0.1847)
Reading from 8776: heap size 443 MB, throughput 0.986726
Reading from 8775: heap size 464 MB, throughput 0.986602
Reading from 8776: heap size 445 MB, throughput 0.988827
Reading from 8775: heap size 462 MB, throughput 0.986398
Reading from 8776: heap size 444 MB, throughput 0.986192
Reading from 8775: heap size 464 MB, throughput 0.986102
Reading from 8776: heap size 446 MB, throughput 0.984242
Reading from 8775: heap size 467 MB, throughput 0.985535
Reading from 8776: heap size 444 MB, throughput 0.982508
Reading from 8775: heap size 467 MB, throughput 0.986809
Reading from 8775: heap size 470 MB, throughput 0.97871
Analytic result (unused):
Recommendation: 2 clients, utility 0.77906:
    h1: 581.219 MB (U(h) = 0.197696*h^0.237927)
    h2: 467.781 MB (U(h) = 0.26704*h^0.19149)
Recommendation: 2 clients, utility 0.77906:
    h1: 581.215 MB (U(h) = 0.197696*h^0.237927)
    h2: 467.785 MB (U(h) = 0.26704*h^0.19149)
Reading from 8775: heap size 470 MB, throughput 0.964032
Reading from 8776: heap size 446 MB, throughput 0.987112
Reading from 8776: heap size 448 MB, throughput 0.983046
Reading from 8776: heap size 448 MB, throughput 0.971253
Reading from 8775: heap size 477 MB, throughput 0.981219
Reading from 8776: heap size 453 MB, throughput 0.955249
Reading from 8776: heap size 453 MB, throughput 0.975543
Reading from 8775: heap size 478 MB, throughput 0.988142
Reading from 8776: heap size 460 MB, throughput 0.985448
Reading from 8775: heap size 482 MB, throughput 0.988839
Reading from 8776: heap size 461 MB, throughput 0.987446
Reading from 8775: heap size 484 MB, throughput 0.988944
Reading from 8776: heap size 464 MB, throughput 0.990532
Analytic result (unused):
Recommendation: 2 clients, utility 0.798873:
    h1: 578.805 MB (U(h) = 0.192506*h^0.244084)
    h2: 470.195 MB (U(h) = 0.259346*h^0.198283)
Recommendation: 2 clients, utility 0.798873:
    h1: 578.809 MB (U(h) = 0.192506*h^0.244084)
    h2: 470.191 MB (U(h) = 0.259346*h^0.198283)
Reading from 8775: heap size 483 MB, throughput 0.99012
Reading from 8776: heap size 465 MB, throughput 0.988182
Reading from 8776: heap size 464 MB, throughput 0.989311
Reading from 8775: heap size 485 MB, throughput 0.989423
Reading from 8776: heap size 466 MB, throughput 0.989472
Reading from 8775: heap size 486 MB, throughput 0.987793
Reading from 8776: heap size 466 MB, throughput 0.987645
Reading from 8775: heap size 487 MB, throughput 0.988158
Analytic result (unused):
Recommendation: 2 clients, utility 0.811225:
    h1: 581.175 MB (U(h) = 0.188106*h^0.249417)
    h2: 467.825 MB (U(h) = 0.256565*h^0.200772)
Recommendation: 2 clients, utility 0.811225:
    h1: 581.173 MB (U(h) = 0.188106*h^0.249417)
    h2: 467.827 MB (U(h) = 0.256565*h^0.200772)
Reading from 8775: heap size 489 MB, throughput 0.982653
Reading from 8775: heap size 490 MB, throughput 0.971666
Reading from 8775: heap size 494 MB, throughput 0.975904
Reading from 8776: heap size 467 MB, throughput 0.988768
Reading from 8776: heap size 466 MB, throughput 0.983054
Reading from 8776: heap size 468 MB, throughput 0.971066
Reading from 8776: heap size 473 MB, throughput 0.963251
Reading from 8775: heap size 496 MB, throughput 0.987977
Reading from 8776: heap size 439 MB, throughput 0.986243
Reading from 8775: heap size 500 MB, throughput 0.989794
Reading from 8776: heap size 471 MB, throughput 0.988606
Reading from 8775: heap size 502 MB, throughput 0.990356
Reading from 8776: heap size 440 MB, throughput 0.988589
Analytic result (unused):
Recommendation: 2 clients, utility 0.828896:
    h1: 581.009 MB (U(h) = 0.183136*h^0.255553)
    h2: 467.991 MB (U(h) = 0.251006*h^0.205842)
Recommendation: 2 clients, utility 0.828896:
    h1: 581.01 MB (U(h) = 0.183136*h^0.255553)
    h2: 467.99 MB (U(h) = 0.251006*h^0.205842)
Reading from 8776: heap size 467 MB, throughput 0.988551
Reading from 8775: heap size 500 MB, throughput 0.991367
Reading from 8776: heap size 466 MB, throughput 0.987112
Reading from 8775: heap size 503 MB, throughput 0.988565
Reading from 8776: heap size 465 MB, throughput 0.977371
Reading from 8775: heap size 505 MB, throughput 0.987143
Reading from 8776: heap size 484 MB, throughput 0.991611
Reading from 8775: heap size 505 MB, throughput 0.988349
Reading from 8775: heap size 508 MB, throughput 0.981227
Analytic result (unused):
Recommendation: 2 clients, utility 0.834573:
    h1: 582.441 MB (U(h) = 0.181047*h^0.258165)
    h2: 466.559 MB (U(h) = 0.24995*h^0.2068)
Recommendation: 2 clients, utility 0.834573:
    h1: 582.439 MB (U(h) = 0.181047*h^0.258165)
    h2: 466.561 MB (U(h) = 0.24995*h^0.2068)
Reading from 8775: heap size 509 MB, throughput 0.969146
Reading from 8776: heap size 478 MB, throughput 0.992329
Reading from 8775: heap size 516 MB, throughput 0.985562
Reading from 8776: heap size 447 MB, throughput 0.989333
Reading from 8776: heap size 476 MB, throughput 0.980557
Reading from 8776: heap size 464 MB, throughput 0.963434
Reading from 8776: heap size 476 MB, throughput 0.98418
Reading from 8775: heap size 518 MB, throughput 0.989541
Reading from 8776: heap size 439 MB, throughput 0.98879
Reading from 8775: heap size 517 MB, throughput 0.989909
Reading from 8776: heap size 475 MB, throughput 0.988508
Analytic result (unused):
Recommendation: 2 clients, utility 0.844653:
    h1: 580.951 MB (U(h) = 0.17874*h^0.261063)
    h2: 468.049 MB (U(h) = 0.246151*h^0.210328)
Recommendation: 2 clients, utility 0.844653:
    h1: 580.952 MB (U(h) = 0.17874*h^0.261063)
    h2: 468.048 MB (U(h) = 0.246151*h^0.210328)
Reading from 8776: heap size 443 MB, throughput 0.988136
Reading from 8775: heap size 518 MB, throughput 0.990718
Reading from 8776: heap size 471 MB, throughput 0.987262
Reading from 8775: heap size 516 MB, throughput 0.990299
Reading from 8776: heap size 447 MB, throughput 0.984664
Reading from 8775: heap size 518 MB, throughput 0.982807
Reading from 8776: heap size 465 MB, throughput 0.978784
Analytic result (unused):
Recommendation: 2 clients, utility 0.844585:
    h1: 580.991 MB (U(h) = 0.178746*h^0.261057)
    h2: 468.009 MB (U(h) = 0.246191*h^0.210291)
Recommendation: 2 clients, utility 0.844585:
    h1: 580.993 MB (U(h) = 0.178746*h^0.261057)
    h2: 468.007 MB (U(h) = 0.246191*h^0.210291)
Reading from 8776: heap size 465 MB, throughput 0.981577
Reading from 8775: heap size 522 MB, throughput 0.987308
Reading from 8775: heap size 522 MB, throughput 0.981279
Reading from 8775: heap size 518 MB, throughput 0.972784
Reading from 8776: heap size 468 MB, throughput 0.985861
Reading from 8775: heap size 520 MB, throughput 0.986725
Reading from 8776: heap size 444 MB, throughput 0.980894
Reading from 8776: heap size 461 MB, throughput 0.970917
Reading from 8776: heap size 465 MB, throughput 0.959283
Reading from 8776: heap size 474 MB, throughput 0.980042
Reading from 8775: heap size 519 MB, throughput 0.989117
Reading from 8776: heap size 438 MB, throughput 0.985818
Reading from 8776: heap size 473 MB, throughput 0.987948
Reading from 8775: heap size 520 MB, throughput 0.991789
Analytic result (unused):
Recommendation: 2 clients, utility 0.969916:
    h1: 716.544 MB (U(h) = 0.0575828*h^0.456643)
    h2: 332.456 MB (U(h) = 0.244532*h^0.211869)
Recommendation: 2 clients, utility 0.969916:
    h1: 716.543 MB (U(h) = 0.0575828*h^0.456643)
    h2: 332.457 MB (U(h) = 0.244532*h^0.211869)
Reading from 8776: heap size 440 MB, throughput 0.988119
Reading from 8775: heap size 519 MB, throughput 0.992025
Reading from 8776: heap size 456 MB, throughput 0.986987
Reading from 8776: heap size 436 MB, throughput 0.986983
Reading from 8775: heap size 519 MB, throughput 0.990253
Reading from 8776: heap size 445 MB, throughput 0.986053
Reading from 8775: heap size 522 MB, throughput 0.989553
Reading from 8776: heap size 432 MB, throughput 0.985176
Analytic result (unused):
Recommendation: 2 clients, utility 0.979726:
    h1: 663.445 MB (U(h) = 0.0575697*h^0.45669)
    h2: 385.555 MB (U(h) = 0.180248*h^0.265401)
Recommendation: 2 clients, utility 0.979726:
    h1: 663.446 MB (U(h) = 0.0575697*h^0.45669)
    h2: 385.554 MB (U(h) = 0.180248*h^0.265401)
Reading from 8776: heap size 439 MB, throughput 0.982069
Reading from 8775: heap size 522 MB, throughput 0.989174
Reading from 8775: heap size 518 MB, throughput 0.983073
Reading from 8775: heap size 512 MB, throughput 0.978823
Reading from 8776: heap size 429 MB, throughput 0.986795
Reading from 8776: heap size 434 MB, throughput 0.982286
Reading from 8776: heap size 424 MB, throughput 0.971449
Reading from 8776: heap size 432 MB, throughput 0.957433
Reading from 8776: heap size 423 MB, throughput 0.950688
Reading from 8775: heap size 518 MB, throughput 0.988558
Reading from 8776: heap size 429 MB, throughput 0.978082
Reading from 8776: heap size 398 MB, throughput 0.983181
Reading from 8775: heap size 519 MB, throughput 0.990372
Reading from 8776: heap size 424 MB, throughput 0.985629
Reading from 8776: heap size 397 MB, throughput 0.983342
Analytic result (unused):
Recommendation: 2 clients, utility 1.06453:
    h1: 549.388 MB (U(h) = 0.0439126*h^0.502745)
    h2: 499.612 MB (U(h) = 0.0593341*h^0.457195)
Recommendation: 2 clients, utility 1.06453:
    h1: 549.388 MB (U(h) = 0.0439126*h^0.502745)
    h2: 499.612 MB (U(h) = 0.0593341*h^0.457195)
Reading from 8775: heap size 517 MB, throughput 0.990573
Reading from 8776: heap size 417 MB, throughput 0.983202
Reading from 8776: heap size 420 MB, throughput 0.986194
Reading from 8775: heap size 518 MB, throughput 0.989791
Reading from 8776: heap size 418 MB, throughput 0.981812
Reading from 8775: heap size 517 MB, throughput 0.988923
Reading from 8776: heap size 420 MB, throughput 0.982371
Reading from 8776: heap size 422 MB, throughput 0.982316
Reading from 8775: heap size 518 MB, throughput 0.98744
Reading from 8776: heap size 422 MB, throughput 0.980862
Analytic result (unused):
Recommendation: 2 clients, utility 1.06794:
    h1: 546.409 MB (U(h) = 0.0439068*h^0.502772)
    h2: 502.591 MB (U(h) = 0.0576076*h^0.462454)
Recommendation: 2 clients, utility 1.06794:
    h1: 546.409 MB (U(h) = 0.0439068*h^0.502772)
    h2: 502.591 MB (U(h) = 0.0576076*h^0.462454)
Reading from 8775: heap size 521 MB, throughput 0.986757
Reading from 8775: heap size 515 MB, throughput 0.979068
Reading from 8776: heap size 425 MB, throughput 0.980002
Reading from 8775: heap size 517 MB, throughput 0.978495
Reading from 8776: heap size 425 MB, throughput 0.986487
Reading from 8776: heap size 428 MB, throughput 0.978413
Reading from 8776: heap size 429 MB, throughput 0.966519
Reading from 8776: heap size 435 MB, throughput 0.946663
Reading from 8776: heap size 437 MB, throughput 0.939162
Reading from 8775: heap size 487 MB, throughput 0.986625
Reading from 8776: heap size 447 MB, throughput 0.979976
Reading from 8775: heap size 519 MB, throughput 0.989034
Reading from 8776: heap size 448 MB, throughput 0.985763
Reading from 8776: heap size 452 MB, throughput 0.986528
Analytic result (unused):
Recommendation: 2 clients, utility 1.17209:
    h1: 484.321 MB (U(h) = 0.0315333*h^0.558753)
    h2: 564.679 MB (U(h) = 0.0189311*h^0.651461)
Recommendation: 2 clients, utility 1.17209:
    h1: 484.324 MB (U(h) = 0.0315333*h^0.558753)
    h2: 564.676 MB (U(h) = 0.0189311*h^0.651461)
Reading from 8775: heap size 493 MB, throughput 0.990031
Reading from 8776: heap size 454 MB, throughput 0.987053
Reading from 8775: heap size 501 MB, throughput 0.987786
Reading from 8776: heap size 454 MB, throughput 0.985855
Reading from 8776: heap size 456 MB, throughput 0.987089
Reading from 8775: heap size 487 MB, throughput 0.988664
Reading from 8776: heap size 456 MB, throughput 0.986761
Reading from 8775: heap size 493 MB, throughput 0.987375
Reading from 8776: heap size 458 MB, throughput 0.98517
Analytic result (unused):
Recommendation: 2 clients, utility 1.18306:
    h1: 438.849 MB (U(h) = 0.0436298*h^0.50389)
    h2: 610.151 MB (U(h) = 0.0141373*h^0.70058)
Recommendation: 2 clients, utility 1.18306:
    h1: 438.85 MB (U(h) = 0.0436298*h^0.50389)
    h2: 610.15 MB (U(h) = 0.0141373*h^0.70058)
Reading from 8775: heap size 483 MB, throughput 0.988187
Reading from 8775: heap size 482 MB, throughput 0.978786
Reading from 8776: heap size 461 MB, throughput 0.978565
Reading from 8775: heap size 476 MB, throughput 0.96741
Reading from 8776: heap size 462 MB, throughput 0.984117
Reading from 8775: heap size 479 MB, throughput 0.982
Reading from 8776: heap size 466 MB, throughput 0.976244
Reading from 8776: heap size 467 MB, throughput 0.957816
Reading from 8776: heap size 475 MB, throughput 0.967301
Reading from 8775: heap size 447 MB, throughput 0.98591
Reading from 8776: heap size 476 MB, throughput 0.987453
Reading from 8775: heap size 467 MB, throughput 0.987743
Reading from 8776: heap size 481 MB, throughput 0.989182
Reading from 8775: heap size 443 MB, throughput 0.987391
Analytic result (unused):
Recommendation: 2 clients, utility 1.19862:
    h1: 407.354 MB (U(h) = 0.05457*h^0.466688)
    h2: 641.646 MB (U(h) = 0.0114822*h^0.735106)
Recommendation: 2 clients, utility 1.19862:
    h1: 407.354 MB (U(h) = 0.05457*h^0.466688)
    h2: 641.646 MB (U(h) = 0.0114822*h^0.735106)
Reading from 8776: heap size 484 MB, throughput 0.989121
Reading from 8775: heap size 456 MB, throughput 0.987926
Reading from 8776: heap size 485 MB, throughput 0.989916
Reading from 8775: heap size 440 MB, throughput 0.986818
Reading from 8776: heap size 487 MB, throughput 0.987951
Reading from 8775: heap size 447 MB, throughput 0.985803
Reading from 8776: heap size 488 MB, throughput 0.984084
Reading from 8775: heap size 437 MB, throughput 0.983337
Reading from 8776: heap size 490 MB, throughput 0.98426
Analytic result (unused):
Recommendation: 2 clients, utility 1.12207:
    h1: 421.165 MB (U(h) = 0.0797022*h^0.403808)
    h2: 627.835 MB (U(h) = 0.0253853*h^0.601961)
Recommendation: 2 clients, utility 1.12207:
    h1: 421.167 MB (U(h) = 0.0797022*h^0.403808)
    h2: 627.833 MB (U(h) = 0.0253853*h^0.601961)
Reading from 8775: heap size 442 MB, throughput 0.9863
Reading from 8775: heap size 421 MB, throughput 0.978998
Reading from 8775: heap size 438 MB, throughput 0.966999
Reading from 8775: heap size 429 MB, throughput 0.951282
Reading from 8776: heap size 494 MB, throughput 0.988778
Reading from 8775: heap size 434 MB, throughput 0.979357
Reading from 8776: heap size 495 MB, throughput 0.974447
Reading from 8776: heap size 497 MB, throughput 0.965207
Reading from 8776: heap size 499 MB, throughput 0.980555
Reading from 8775: heap size 402 MB, throughput 0.986649
Reading from 8775: heap size 434 MB, throughput 0.987793
Reading from 8776: heap size 510 MB, throughput 0.990232
Reading from 8775: heap size 404 MB, throughput 0.987861
Reading from 8776: heap size 511 MB, throughput 0.990656
Analytic result (unused):
Recommendation: 2 clients, utility 1.06492:
    h1: 559.535 MB (U(h) = 0.0542333*h^0.46819)
    h2: 489.465 MB (U(h) = 0.0803431*h^0.409558)
Recommendation: 2 clients, utility 1.06492:
    h1: 559.537 MB (U(h) = 0.0542333*h^0.46819)
    h2: 489.463 MB (U(h) = 0.0803431*h^0.409558)
Reading from 8775: heap size 431 MB, throughput 0.98621
Reading from 8776: heap size 513 MB, throughput 0.990393
Reading from 8775: heap size 429 MB, throughput 0.986107
Reading from 8775: heap size 427 MB, throughput 0.985014
Reading from 8776: heap size 491 MB, throughput 0.98951
Reading from 8775: heap size 429 MB, throughput 0.987126
Reading from 8776: heap size 499 MB, throughput 0.990323
Reading from 8775: heap size 431 MB, throughput 0.985336
Reading from 8776: heap size 488 MB, throughput 0.988652
Analytic result (unused):
Recommendation: 2 clients, utility 1.08266:
    h1: 581.885 MB (U(h) = 0.0401834*h^0.518078)
    h2: 467.115 MB (U(h) = 0.0772414*h^0.415893)
Recommendation: 2 clients, utility 1.08266:
    h1: 581.886 MB (U(h) = 0.0401834*h^0.518078)
    h2: 467.114 MB (U(h) = 0.0772414*h^0.415893)
Reading from 8775: heap size 432 MB, throughput 0.988162
Reading from 8775: heap size 435 MB, throughput 0.983076
Reading from 8775: heap size 458 MB, throughput 0.983382
Reading from 8775: heap size 460 MB, throughput 0.984832
Reading from 8776: heap size 499 MB, throughput 0.990187
Reading from 8776: heap size 472 MB, throughput 0.986227
Reading from 8775: heap size 463 MB, throughput 0.988884
Reading from 8776: heap size 490 MB, throughput 0.976548
Reading from 8776: heap size 481 MB, throughput 0.977997
Reading from 8775: heap size 467 MB, throughput 0.99287
Reading from 8776: heap size 483 MB, throughput 0.989917
Reading from 8775: heap size 468 MB, throughput 0.989968
Reading from 8776: heap size 454 MB, throughput 0.989496
Reading from 8775: heap size 467 MB, throughput 0.989839
Analytic result (unused):
Recommendation: 2 clients, utility 1.0895:
    h1: 590.365 MB (U(h) = 0.0366607*h^0.533556)
    h2: 458.635 MB (U(h) = 0.077857*h^0.414501)
Recommendation: 2 clients, utility 1.0895:
    h1: 590.363 MB (U(h) = 0.0366607*h^0.533556)
    h2: 458.637 MB (U(h) = 0.077857*h^0.414501)
Reading from 8775: heap size 468 MB, throughput 0.989061
Reading from 8776: heap size 483 MB, throughput 0.98908
Reading from 8775: heap size 461 MB, throughput 0.988809
Reading from 8776: heap size 456 MB, throughput 0.989844
Reading from 8775: heap size 465 MB, throughput 0.987194
Reading from 8776: heap size 479 MB, throughput 0.987787
Reading from 8775: heap size 465 MB, throughput 0.984188
Reading from 8776: heap size 459 MB, throughput 0.985998
Reading from 8775: heap size 465 MB, throughput 0.983479
Reading from 8776: heap size 466 MB, throughput 0.98478
Analytic result (unused):
Recommendation: 2 clients, utility 1.0843:
    h1: 604.984 MB (U(h) = 0.0394632*h^0.521478)
    h2: 444.016 MB (U(h) = 0.0944285*h^0.382728)
Recommendation: 2 clients, utility 1.0843:
    h1: 604.984 MB (U(h) = 0.0394632*h^0.521478)
    h2: 444.016 MB (U(h) = 0.0944285*h^0.382728)
Reading from 8775: heap size 466 MB, throughput 0.986847
Reading from 8775: heap size 441 MB, throughput 0.980844
Reading from 8775: heap size 461 MB, throughput 0.966284
Reading from 8776: heap size 457 MB, throughput 0.988697
Reading from 8775: heap size 469 MB, throughput 0.940184
Reading from 8775: heap size 478 MB, throughput 0.954984
Reading from 8776: heap size 459 MB, throughput 0.985068
Reading from 8776: heap size 455 MB, throughput 0.973797
Reading from 8776: heap size 460 MB, throughput 0.960242
Reading from 8776: heap size 449 MB, throughput 0.972252
Reading from 8775: heap size 483 MB, throughput 0.980676
Reading from 8776: heap size 455 MB, throughput 0.984261
Reading from 8775: heap size 482 MB, throughput 0.987509
Reading from 8776: heap size 423 MB, throughput 0.98538
Reading from 8775: heap size 486 MB, throughput 0.986769
Reading from 8776: heap size 455 MB, throughput 0.986416
Analytic result (unused):
Recommendation: 2 clients, utility 1.09742:
    h1: 693.62 MB (U(h) = 0.0381828*h^0.526786)
    h2: 355.38 MB (U(h) = 0.187674*h^0.269902)
Recommendation: 2 clients, utility 1.09742:
    h1: 693.62 MB (U(h) = 0.0381828*h^0.526786)
    h2: 355.38 MB (U(h) = 0.187674*h^0.269902)
Reading from 8775: heap size 487 MB, throughput 0.98592
Reading from 8776: heap size 426 MB, throughput 0.981
Reading from 8776: heap size 440 MB, throughput 0.983554
Reading from 8775: heap size 489 MB, throughput 0.985051
Reading from 8776: heap size 423 MB, throughput 0.982962
Reading from 8775: heap size 492 MB, throughput 0.986096
Reading from 8776: heap size 432 MB, throughput 0.981299
Reading from 8775: heap size 493 MB, throughput 0.986839
Reading from 8776: heap size 421 MB, throughput 0.982079
Reading from 8776: heap size 428 MB, throughput 0.980515
Analytic result (unused):
Recommendation: 2 clients, utility 1.11553:
    h1: 702.896 MB (U(h) = 0.029969*h^0.566507)
    h2: 346.104 MB (U(h) = 0.177714*h^0.278947)
Recommendation: 2 clients, utility 1.11553:
    h1: 702.893 MB (U(h) = 0.029969*h^0.566507)
    h2: 346.107 MB (U(h) = 0.177714*h^0.278947)
Reading from 8775: heap size 498 MB, throughput 0.984105
Reading from 8775: heap size 498 MB, throughput 0.982708
Reading from 8775: heap size 505 MB, throughput 0.972306
Reading from 8776: heap size 419 MB, throughput 0.986979
Reading from 8775: heap size 506 MB, throughput 0.958049
Client 8775 died
Clients: 1
Reading from 8776: heap size 421 MB, throughput 0.982115
Reading from 8776: heap size 418 MB, throughput 0.972149
Reading from 8776: heap size 422 MB, throughput 0.956856
Reading from 8776: heap size 413 MB, throughput 0.937671
Client 8776 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
