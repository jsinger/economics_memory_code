economemd
    total memory: 2446 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub11_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
ReadingThread: starting
TimerThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 2829: heap size 9 MB, throughput 0.988719
Clients: 1
Client 2829 has a minimum heap size of 12 MB
Reading from 2828: heap size 9 MB, throughput 0.986997
Clients: 2
Client 2828 has a minimum heap size of 1211 MB
Reading from 2829: heap size 9 MB, throughput 0.989235
Reading from 2828: heap size 9 MB, throughput 0.983426
Reading from 2829: heap size 11 MB, throughput 0.98161
Reading from 2828: heap size 11 MB, throughput 0.962414
Reading from 2829: heap size 11 MB, throughput 0.964329
Reading from 2828: heap size 11 MB, throughput 0.982714
Reading from 2828: heap size 15 MB, throughput 0.86074
Reading from 2829: heap size 15 MB, throughput 0.984559
Reading from 2828: heap size 18 MB, throughput 0.974667
Reading from 2829: heap size 15 MB, throughput 0.96518
Reading from 2828: heap size 23 MB, throughput 0.953968
Reading from 2828: heap size 28 MB, throughput 0.362774
Reading from 2828: heap size 37 MB, throughput 0.767609
Reading from 2828: heap size 39 MB, throughput 0.865042
Reading from 2829: heap size 24 MB, throughput 0.921049
Reading from 2828: heap size 42 MB, throughput 0.922691
Reading from 2828: heap size 43 MB, throughput 0.670616
Reading from 2828: heap size 46 MB, throughput 0.290523
Reading from 2829: heap size 30 MB, throughput 0.979493
Reading from 2828: heap size 60 MB, throughput 0.715544
Reading from 2828: heap size 65 MB, throughput 0.691595
Reading from 2828: heap size 67 MB, throughput 0.786064
Reading from 2829: heap size 37 MB, throughput 0.987015
Reading from 2828: heap size 75 MB, throughput 0.157851
Reading from 2828: heap size 94 MB, throughput 0.565145
Reading from 2829: heap size 44 MB, throughput 0.983785
Reading from 2828: heap size 100 MB, throughput 0.826321
Reading from 2828: heap size 101 MB, throughput 0.590402
Reading from 2829: heap size 45 MB, throughput 0.988422
Reading from 2828: heap size 104 MB, throughput 0.179103
Reading from 2829: heap size 46 MB, throughput 0.981515
Reading from 2828: heap size 132 MB, throughput 0.654548
Reading from 2829: heap size 52 MB, throughput 0.979572
Reading from 2828: heap size 138 MB, throughput 0.750914
Reading from 2828: heap size 140 MB, throughput 0.645875
Reading from 2829: heap size 52 MB, throughput 0.9736
Reading from 2828: heap size 144 MB, throughput 0.553186
Reading from 2829: heap size 61 MB, throughput 0.976209
Reading from 2828: heap size 150 MB, throughput 0.676753
Reading from 2829: heap size 61 MB, throughput 0.928797
Reading from 2829: heap size 72 MB, throughput 0.763697
Reading from 2829: heap size 76 MB, throughput 0.991298
Reading from 2828: heap size 156 MB, throughput 0.114881
Reading from 2828: heap size 189 MB, throughput 0.493708
Reading from 2828: heap size 195 MB, throughput 0.668392
Reading from 2828: heap size 196 MB, throughput 0.616635
Reading from 2829: heap size 89 MB, throughput 0.997416
Reading from 2828: heap size 199 MB, throughput 0.148829
Reading from 2828: heap size 238 MB, throughput 0.489384
Reading from 2828: heap size 247 MB, throughput 0.650865
Reading from 2828: heap size 247 MB, throughput 0.547958
Reading from 2829: heap size 89 MB, throughput 0.996759
Reading from 2828: heap size 250 MB, throughput 0.608779
Reading from 2828: heap size 255 MB, throughput 0.501103
Reading from 2828: heap size 261 MB, throughput 0.552131
Reading from 2829: heap size 100 MB, throughput 0.996262
Reading from 2828: heap size 268 MB, throughput 0.110325
Reading from 2828: heap size 312 MB, throughput 0.503313
Reading from 2828: heap size 318 MB, throughput 0.635258
Reading from 2829: heap size 102 MB, throughput 0.996752
Reading from 2828: heap size 321 MB, throughput 0.542302
Reading from 2828: heap size 321 MB, throughput 0.134814
Reading from 2829: heap size 111 MB, throughput 0.997304
Reading from 2828: heap size 369 MB, throughput 0.531557
Reading from 2828: heap size 371 MB, throughput 0.63092
Reading from 2828: heap size 373 MB, throughput 0.656963
Reading from 2828: heap size 376 MB, throughput 0.56642
Reading from 2828: heap size 379 MB, throughput 0.499846
Reading from 2829: heap size 113 MB, throughput 0.996848
Reading from 2828: heap size 388 MB, throughput 0.520721
Reading from 2828: heap size 393 MB, throughput 0.558707
Reading from 2829: heap size 120 MB, throughput 0.99664
Reading from 2828: heap size 401 MB, throughput 0.104762
Reading from 2828: heap size 451 MB, throughput 0.43116
Equal recommendation: 1223 MB each
Reading from 2828: heap size 458 MB, throughput 0.446901
Reading from 2829: heap size 122 MB, throughput 0.997162
Reading from 2828: heap size 459 MB, throughput 0.0922575
Reading from 2829: heap size 126 MB, throughput 0.995855
Reading from 2828: heap size 512 MB, throughput 0.436555
Reading from 2828: heap size 505 MB, throughput 0.566043
Reading from 2828: heap size 512 MB, throughput 0.575234
Reading from 2828: heap size 513 MB, throughput 0.572165
Reading from 2829: heap size 127 MB, throughput 0.995816
Reading from 2828: heap size 514 MB, throughput 0.530447
Reading from 2829: heap size 133 MB, throughput 0.996459
Reading from 2828: heap size 519 MB, throughput 0.125065
Reading from 2828: heap size 583 MB, throughput 0.386327
Reading from 2828: heap size 593 MB, throughput 0.639519
Reading from 2828: heap size 595 MB, throughput 0.57663
Reading from 2828: heap size 598 MB, throughput 0.561351
Reading from 2829: heap size 133 MB, throughput 0.997325
Reading from 2828: heap size 608 MB, throughput 0.567746
Reading from 2828: heap size 614 MB, throughput 0.565324
Reading from 2828: heap size 623 MB, throughput 0.56762
Reading from 2829: heap size 137 MB, throughput 0.995085
Reading from 2829: heap size 138 MB, throughput 0.996009
Reading from 2828: heap size 629 MB, throughput 0.21082
Reading from 2829: heap size 143 MB, throughput 0.996582
Reading from 2828: heap size 698 MB, throughput 0.804415
Reading from 2828: heap size 704 MB, throughput 0.840413
Reading from 2829: heap size 143 MB, throughput 0.997583
Reading from 2828: heap size 713 MB, throughput 0.813172
Reading from 2828: heap size 719 MB, throughput 0.418236
Reading from 2829: heap size 146 MB, throughput 0.997872
Reading from 2829: heap size 147 MB, throughput 0.997336
Reading from 2828: heap size 728 MB, throughput 0.0736809
Equal recommendation: 1223 MB each
Reading from 2828: heap size 805 MB, throughput 0.269632
Reading from 2828: heap size 707 MB, throughput 0.330039
Reading from 2828: heap size 784 MB, throughput 0.378192
Reading from 2829: heap size 149 MB, throughput 0.99845
Reading from 2828: heap size 792 MB, throughput 0.0575679
Reading from 2829: heap size 150 MB, throughput 0.998232
Reading from 2828: heap size 873 MB, throughput 0.729735
Reading from 2828: heap size 874 MB, throughput 0.792644
Reading from 2828: heap size 881 MB, throughput 0.677277
Reading from 2828: heap size 882 MB, throughput 0.679571
Reading from 2829: heap size 152 MB, throughput 0.998548
Reading from 2828: heap size 884 MB, throughput 0.829748
Reading from 2828: heap size 886 MB, throughput 0.751294
Reading from 2829: heap size 153 MB, throughput 0.997077
Reading from 2829: heap size 155 MB, throughput 0.993943
Reading from 2828: heap size 888 MB, throughput 0.107579
Reading from 2829: heap size 155 MB, throughput 0.986522
Reading from 2828: heap size 979 MB, throughput 0.680969
Reading from 2828: heap size 980 MB, throughput 0.873896
Reading from 2828: heap size 985 MB, throughput 0.939757
Reading from 2829: heap size 160 MB, throughput 0.99669
Reading from 2828: heap size 985 MB, throughput 0.918996
Reading from 2828: heap size 812 MB, throughput 0.794019
Reading from 2828: heap size 968 MB, throughput 0.832301
Reading from 2828: heap size 817 MB, throughput 0.830733
Reading from 2828: heap size 955 MB, throughput 0.845077
Reading from 2828: heap size 822 MB, throughput 0.846748
Reading from 2828: heap size 945 MB, throughput 0.85723
Reading from 2829: heap size 160 MB, throughput 0.997446
Reading from 2828: heap size 827 MB, throughput 0.857003
Reading from 2828: heap size 935 MB, throughput 0.860745
Reading from 2828: heap size 833 MB, throughput 0.865056
Reading from 2828: heap size 927 MB, throughput 0.983975
Reading from 2829: heap size 165 MB, throughput 0.99789
Reading from 2829: heap size 166 MB, throughput 0.997391
Reading from 2828: heap size 934 MB, throughput 0.970748
Reading from 2828: heap size 935 MB, throughput 0.79463
Reading from 2828: heap size 937 MB, throughput 0.799444
Reading from 2828: heap size 935 MB, throughput 0.817952
Equal recommendation: 1223 MB each
Reading from 2829: heap size 170 MB, throughput 0.997956
Reading from 2828: heap size 937 MB, throughput 0.817279
Reading from 2828: heap size 936 MB, throughput 0.826454
Reading from 2828: heap size 939 MB, throughput 0.789942
Reading from 2828: heap size 937 MB, throughput 0.841691
Reading from 2828: heap size 940 MB, throughput 0.833836
Reading from 2829: heap size 170 MB, throughput 0.997373
Reading from 2828: heap size 939 MB, throughput 0.896987
Reading from 2828: heap size 942 MB, throughput 0.887552
Reading from 2828: heap size 942 MB, throughput 0.887887
Reading from 2828: heap size 944 MB, throughput 0.740129
Reading from 2829: heap size 174 MB, throughput 0.997612
Reading from 2828: heap size 930 MB, throughput 0.646499
Reading from 2828: heap size 950 MB, throughput 0.700465
Reading from 2829: heap size 174 MB, throughput 0.992653
Reading from 2829: heap size 178 MB, throughput 0.997735
Reading from 2828: heap size 960 MB, throughput 0.0610634
Reading from 2828: heap size 1064 MB, throughput 0.587731
Reading from 2828: heap size 1060 MB, throughput 0.777124
Reading from 2828: heap size 1065 MB, throughput 0.756983
Reading from 2828: heap size 1064 MB, throughput 0.746131
Reading from 2828: heap size 1068 MB, throughput 0.756664
Reading from 2828: heap size 1074 MB, throughput 0.757461
Reading from 2829: heap size 178 MB, throughput 0.997203
Reading from 2828: heap size 1076 MB, throughput 0.72315
Reading from 2828: heap size 1089 MB, throughput 0.815148
Reading from 2829: heap size 182 MB, throughput 0.995339
Reading from 2828: heap size 1089 MB, throughput 0.956662
Reading from 2829: heap size 182 MB, throughput 0.997174
Reading from 2828: heap size 1106 MB, throughput 0.969184
Reading from 2829: heap size 186 MB, throughput 0.997409
Equal recommendation: 1223 MB each
Reading from 2829: heap size 186 MB, throughput 0.99757
Reading from 2828: heap size 1109 MB, throughput 0.971057
Reading from 2829: heap size 190 MB, throughput 0.997962
Reading from 2829: heap size 190 MB, throughput 0.994876
Reading from 2828: heap size 1117 MB, throughput 0.939061
Reading from 2829: heap size 194 MB, throughput 0.993722
Reading from 2829: heap size 194 MB, throughput 0.996389
Reading from 2828: heap size 1120 MB, throughput 0.968892
Reading from 2829: heap size 199 MB, throughput 0.997603
Reading from 2828: heap size 1116 MB, throughput 0.966711
Reading from 2829: heap size 199 MB, throughput 0.997536
Reading from 2829: heap size 202 MB, throughput 0.997875
Reading from 2828: heap size 1121 MB, throughput 0.961906
Reading from 2829: heap size 203 MB, throughput 0.997812
Reading from 2828: heap size 1113 MB, throughput 0.956711
Equal recommendation: 1223 MB each
Reading from 2829: heap size 207 MB, throughput 0.997966
Reading from 2828: heap size 1118 MB, throughput 0.959643
Reading from 2829: heap size 207 MB, throughput 0.997419
Reading from 2829: heap size 211 MB, throughput 0.997861
Reading from 2828: heap size 1119 MB, throughput 0.958734
Reading from 2829: heap size 211 MB, throughput 0.997398
Reading from 2828: heap size 1121 MB, throughput 0.960064
Reading from 2829: heap size 214 MB, throughput 0.997858
Reading from 2829: heap size 214 MB, throughput 0.997546
Reading from 2828: heap size 1125 MB, throughput 0.958833
Reading from 2829: heap size 217 MB, throughput 0.997785
Reading from 2828: heap size 1127 MB, throughput 0.960228
Reading from 2829: heap size 217 MB, throughput 0.99685
Equal recommendation: 1223 MB each
Reading from 2829: heap size 221 MB, throughput 0.992281
Reading from 2828: heap size 1132 MB, throughput 0.959335
Reading from 2829: heap size 221 MB, throughput 0.995817
Reading from 2829: heap size 227 MB, throughput 0.998032
Reading from 2828: heap size 1135 MB, throughput 0.961329
Reading from 2829: heap size 227 MB, throughput 0.997784
Reading from 2828: heap size 1139 MB, throughput 0.959162
Reading from 2829: heap size 232 MB, throughput 0.998183
Reading from 2829: heap size 220 MB, throughput 0.997379
Reading from 2828: heap size 1143 MB, throughput 0.954939
Reading from 2829: heap size 211 MB, throughput 0.997928
Reading from 2828: heap size 1150 MB, throughput 0.956832
Reading from 2829: heap size 202 MB, throughput 0.997688
Equal recommendation: 1223 MB each
Reading from 2829: heap size 193 MB, throughput 0.997481
Reading from 2828: heap size 1153 MB, throughput 0.957544
Reading from 2829: heap size 185 MB, throughput 0.997523
Reading from 2829: heap size 177 MB, throughput 0.997183
Reading from 2828: heap size 1159 MB, throughput 0.959431
Reading from 2829: heap size 170 MB, throughput 0.997338
Reading from 2829: heap size 162 MB, throughput 0.99717
Reading from 2828: heap size 1161 MB, throughput 0.958228
Reading from 2829: heap size 156 MB, throughput 0.996354
Reading from 2829: heap size 149 MB, throughput 0.99604
Reading from 2829: heap size 143 MB, throughput 0.986177
Reading from 2828: heap size 1166 MB, throughput 0.949414
Reading from 2829: heap size 149 MB, throughput 0.786721
Reading from 2829: heap size 158 MB, throughput 0.997389
Reading from 2829: heap size 163 MB, throughput 0.998378
Reading from 2828: heap size 1167 MB, throughput 0.959383
Reading from 2829: heap size 169 MB, throughput 0.998195
Equal recommendation: 1223 MB each
Reading from 2829: heap size 174 MB, throughput 0.998378
Reading from 2828: heap size 1173 MB, throughput 0.959893
Reading from 2829: heap size 179 MB, throughput 0.998322
Reading from 2829: heap size 184 MB, throughput 0.998387
Reading from 2828: heap size 1174 MB, throughput 0.956947
Reading from 2829: heap size 189 MB, throughput 0.998397
Reading from 2829: heap size 193 MB, throughput 0.998174
Reading from 2829: heap size 200 MB, throughput 0.998538
Reading from 2828: heap size 1179 MB, throughput 0.543616
Reading from 2829: heap size 203 MB, throughput 0.998621
Reading from 2829: heap size 203 MB, throughput 0.998491
Reading from 2828: heap size 1278 MB, throughput 0.946526
Equal recommendation: 1223 MB each
Reading from 2829: heap size 206 MB, throughput 0.998606
Reading from 2829: heap size 207 MB, throughput 0.998343
Reading from 2828: heap size 1275 MB, throughput 0.979394
Reading from 2829: heap size 209 MB, throughput 0.998666
Reading from 2829: heap size 210 MB, throughput 0.994473
Reading from 2829: heap size 213 MB, throughput 0.989727
Reading from 2828: heap size 1281 MB, throughput 0.979596
Reading from 2829: heap size 213 MB, throughput 0.997617
Reading from 2828: heap size 1281 MB, throughput 0.975946
Reading from 2829: heap size 220 MB, throughput 0.998107
Reading from 2829: heap size 222 MB, throughput 0.997677
Reading from 2828: heap size 1170 MB, throughput 0.974617
Reading from 2829: heap size 228 MB, throughput 0.99813
Equal recommendation: 1223 MB each
Reading from 2829: heap size 228 MB, throughput 0.997622
Reading from 2828: heap size 1280 MB, throughput 0.974869
Reading from 2829: heap size 234 MB, throughput 0.997786
Reading from 2828: heap size 1184 MB, throughput 0.96943
Reading from 2829: heap size 234 MB, throughput 0.997648
Reading from 2829: heap size 239 MB, throughput 0.998006
Reading from 2828: heap size 1268 MB, throughput 0.968315
Reading from 2829: heap size 239 MB, throughput 0.997502
Reading from 2828: heap size 1198 MB, throughput 0.97034
Reading from 2829: heap size 245 MB, throughput 0.997957
Reading from 2829: heap size 245 MB, throughput 0.997577
Equal recommendation: 1223 MB each
Reading from 2828: heap size 1262 MB, throughput 0.954365
Reading from 2829: heap size 249 MB, throughput 0.99312
Reading from 2829: heap size 250 MB, throughput 0.994186
Reading from 2828: heap size 1213 MB, throughput 0.966375
Reading from 2829: heap size 258 MB, throughput 0.997839
Reading from 2829: heap size 259 MB, throughput 0.99781
Reading from 2828: heap size 1256 MB, throughput 0.962535
Reading from 2829: heap size 265 MB, throughput 0.998329
Reading from 2828: heap size 1254 MB, throughput 0.957317
Reading from 2829: heap size 266 MB, throughput 0.997993
Reading from 2828: heap size 1253 MB, throughput 0.957709
Reading from 2829: heap size 271 MB, throughput 0.997987
Equal recommendation: 1223 MB each
Reading from 2829: heap size 272 MB, throughput 0.997874
Reading from 2828: heap size 1254 MB, throughput 0.956806
Reading from 2829: heap size 277 MB, throughput 0.998116
Reading from 2828: heap size 1253 MB, throughput 0.955501
Reading from 2829: heap size 278 MB, throughput 0.99791
Reading from 2828: heap size 1257 MB, throughput 0.954272
Reading from 2829: heap size 284 MB, throughput 0.998133
Reading from 2829: heap size 284 MB, throughput 0.994211
Reading from 2828: heap size 1259 MB, throughput 0.956272
Reading from 2829: heap size 289 MB, throughput 0.996616
Reading from 2828: heap size 1259 MB, throughput 0.954698
Reading from 2829: heap size 290 MB, throughput 0.997697
Equal recommendation: 1223 MB each
Reading from 2829: heap size 298 MB, throughput 0.997894
Reading from 2828: heap size 1258 MB, throughput 0.955032
Reading from 2829: heap size 298 MB, throughput 0.997862
Reading from 2828: heap size 1213 MB, throughput 0.951105
Reading from 2829: heap size 305 MB, throughput 0.998306
Reading from 2829: heap size 305 MB, throughput 0.9981
Reading from 2828: heap size 1258 MB, throughput 0.97723
Reading from 2829: heap size 311 MB, throughput 0.998239
Equal recommendation: 1223 MB each
Reading from 2829: heap size 312 MB, throughput 0.9983
Reading from 2829: heap size 316 MB, throughput 0.998046
Reading from 2829: heap size 317 MB, throughput 0.988633
Reading from 2829: heap size 322 MB, throughput 0.99833
Reading from 2829: heap size 323 MB, throughput 0.99813
Equal recommendation: 1223 MB each
Reading from 2829: heap size 331 MB, throughput 0.997914
Reading from 2829: heap size 333 MB, throughput 0.992253
Reading from 2828: heap size 1255 MB, throughput 0.825477
Reading from 2829: heap size 341 MB, throughput 0.99812
Reading from 2829: heap size 341 MB, throughput 0.998004
Reading from 2829: heap size 350 MB, throughput 0.998273
Reading from 2829: heap size 351 MB, throughput 0.996179
Equal recommendation: 1223 MB each
Reading from 2828: heap size 1340 MB, throughput 0.98392
Reading from 2829: heap size 360 MB, throughput 0.997332
Reading from 2829: heap size 361 MB, throughput 0.998061
Reading from 2829: heap size 370 MB, throughput 0.998229
Reading from 2828: heap size 1301 MB, throughput 0.986624
Reading from 2828: heap size 1357 MB, throughput 0.860087
Reading from 2828: heap size 1300 MB, throughput 0.77051
Reading from 2828: heap size 1337 MB, throughput 0.655075
Reading from 2828: heap size 1353 MB, throughput 0.643219
Reading from 2829: heap size 370 MB, throughput 0.998078
Reading from 2828: heap size 1360 MB, throughput 0.6504
Reading from 2828: heap size 1375 MB, throughput 0.644582
Reading from 2828: heap size 1378 MB, throughput 0.655816
Reading from 2828: heap size 1386 MB, throughput 0.645029
Reading from 2828: heap size 1388 MB, throughput 0.637949
Equal recommendation: 1223 MB each
Reading from 2828: heap size 1290 MB, throughput 0.130858
Reading from 2828: heap size 1366 MB, throughput 0.989293
Reading from 2828: heap size 1366 MB, throughput 0.989864
Reading from 2828: heap size 1376 MB, throughput 0.979178
Reading from 2829: heap size 378 MB, throughput 0.998365
Reading from 2828: heap size 1115 MB, throughput 0.987851
Reading from 2828: heap size 1379 MB, throughput 0.990804
Reading from 2829: heap size 379 MB, throughput 0.998061
Reading from 2828: heap size 1113 MB, throughput 0.987051
Reading from 2828: heap size 1381 MB, throughput 0.985063
Reading from 2829: heap size 386 MB, throughput 0.998341
Reading from 2828: heap size 1124 MB, throughput 0.963088
Reading from 2829: heap size 386 MB, throughput 0.922248
Reading from 2828: heap size 1364 MB, throughput 0.978801
Reading from 2828: heap size 1139 MB, throughput 0.976089
Reading from 2829: heap size 400 MB, throughput 0.999137
Equal recommendation: 1223 MB each
Reading from 2828: heap size 1343 MB, throughput 0.974945
Reading from 2828: heap size 1152 MB, throughput 0.973042
Reading from 2829: heap size 401 MB, throughput 0.998989
Reading from 2828: heap size 1320 MB, throughput 0.968944
Reading from 2828: heap size 1165 MB, throughput 0.968931
Reading from 2829: heap size 411 MB, throughput 0.999025
Reading from 2828: heap size 1299 MB, throughput 0.964456
Reading from 2829: heap size 412 MB, throughput 0.999005
Reading from 2828: heap size 1178 MB, throughput 0.965222
Reading from 2828: heap size 1284 MB, throughput 0.966766
Equal recommendation: 1223 MB each
Reading from 2829: heap size 420 MB, throughput 0.99908
Reading from 2828: heap size 1192 MB, throughput 0.963344
Reading from 2828: heap size 1280 MB, throughput 0.955059
Reading from 2829: heap size 421 MB, throughput 0.998925
Reading from 2828: heap size 1204 MB, throughput 0.949129
Reading from 2829: heap size 428 MB, throughput 0.995092
Reading from 2828: heap size 1278 MB, throughput 0.959088
Reading from 2829: heap size 428 MB, throughput 0.998544
Reading from 2828: heap size 1208 MB, throughput 0.957987
Reading from 2828: heap size 1274 MB, throughput 0.959852
Reading from 2829: heap size 440 MB, throughput 0.998647
Equal recommendation: 1223 MB each
Reading from 2828: heap size 1212 MB, throughput 0.960428
Reading from 2828: heap size 1269 MB, throughput 0.960446
Reading from 2829: heap size 441 MB, throughput 0.998463
Reading from 2828: heap size 1215 MB, throughput 0.957704
Reading from 2829: heap size 450 MB, throughput 0.998467
Reading from 2828: heap size 1265 MB, throughput 0.960545
Reading from 2828: heap size 1217 MB, throughput 0.960048
Reading from 2829: heap size 451 MB, throughput 0.998455
Reading from 2828: heap size 1262 MB, throughput 0.960898
Reading from 2829: heap size 460 MB, throughput 0.997586
Equal recommendation: 1223 MB each
Reading from 2828: heap size 1219 MB, throughput 0.952795
Reading from 2828: heap size 1259 MB, throughput 0.960411
Reading from 2829: heap size 460 MB, throughput 0.997113
Reading from 2828: heap size 1220 MB, throughput 0.956788
Reading from 2829: heap size 470 MB, throughput 0.998383
Reading from 2828: heap size 1257 MB, throughput 0.957398
Reading from 2828: heap size 1222 MB, throughput 0.954358
Reading from 2829: heap size 471 MB, throughput 0.998411
Reading from 2828: heap size 1254 MB, throughput 0.95805
Equal recommendation: 1223 MB each
Reading from 2828: heap size 1222 MB, throughput 0.957493
Reading from 2829: heap size 482 MB, throughput 0.998571
Reading from 2828: heap size 1252 MB, throughput 0.958355
Reading from 2828: heap size 1223 MB, throughput 0.954611
Reading from 2829: heap size 482 MB, throughput 0.998435
Reading from 2828: heap size 1245 MB, throughput 0.955607
Reading from 2829: heap size 492 MB, throughput 0.997829
Reading from 2828: heap size 1221 MB, throughput 0.954868
Reading from 2829: heap size 492 MB, throughput 0.997123
Reading from 2828: heap size 1246 MB, throughput 0.95659
Equal recommendation: 1223 MB each
Reading from 2828: heap size 1221 MB, throughput 0.957237
Reading from 2829: heap size 503 MB, throughput 0.99832
Reading from 2828: heap size 1244 MB, throughput 0.954135
Reading from 2828: heap size 1222 MB, throughput 0.952638
Reading from 2829: heap size 504 MB, throughput 0.998219
Reading from 2828: heap size 1242 MB, throughput 0.954327
Reading from 2828: heap size 1221 MB, throughput 0.958829
Reading from 2829: heap size 514 MB, throughput 0.998588
Equal recommendation: 1223 MB each
Reading from 2828: heap size 1239 MB, throughput 0.595592
Reading from 2829: heap size 515 MB, throughput 0.99849
Reading from 2828: heap size 1196 MB, throughput 0.990647
Reading from 2828: heap size 1201 MB, throughput 0.975428
Reading from 2829: heap size 526 MB, throughput 0.996403
Reading from 2828: heap size 1211 MB, throughput 0.985219
Reading from 2828: heap size 1221 MB, throughput 0.981726
Reading from 2829: heap size 526 MB, throughput 0.998397
Reading from 2828: heap size 1224 MB, throughput 0.981047
Reading from 2828: heap size 1216 MB, throughput 0.97989
Equal recommendation: 1223 MB each
Reading from 2829: heap size 538 MB, throughput 0.998582
Reading from 2828: heap size 1223 MB, throughput 0.973964
Reading from 2828: heap size 1206 MB, throughput 0.974508
Reading from 2829: heap size 540 MB, throughput 0.998255
Reading from 2828: heap size 1216 MB, throughput 0.972712
Reading from 2828: heap size 1207 MB, throughput 0.970092
Reading from 2829: heap size 550 MB, throughput 0.998635
Reading from 2828: heap size 1212 MB, throughput 0.965261
Reading from 2828: heap size 1211 MB, throughput 0.968894
Equal recommendation: 1223 MB each
Reading from 2828: heap size 1212 MB, throughput 0.964709
Client 2829 died
Clients: 1
Reading from 2828: heap size 1218 MB, throughput 0.964913
Reading from 2828: heap size 1219 MB, throughput 0.962703
Reading from 2828: heap size 1225 MB, throughput 0.960586
Reading from 2828: heap size 1228 MB, throughput 0.960096
Reading from 2828: heap size 1228 MB, throughput 0.959875
Recommendation: one client; give it all the memory
Reading from 2828: heap size 1230 MB, throughput 0.959347
Reading from 2828: heap size 1231 MB, throughput 0.959761
Reading from 2828: heap size 1232 MB, throughput 0.957441
Reading from 2828: heap size 1241 MB, throughput 0.959759
Reading from 2828: heap size 1241 MB, throughput 0.959394
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 2828: heap size 1250 MB, throughput 0.877041
Reading from 2828: heap size 1410 MB, throughput 0.988019
Recommendation: one client; give it all the memory
Reading from 2828: heap size 1458 MB, throughput 0.989176
Reading from 2828: heap size 1459 MB, throughput 0.915333
Reading from 2828: heap size 1462 MB, throughput 0.852435
Reading from 2828: heap size 1469 MB, throughput 0.759692
Reading from 2828: heap size 1459 MB, throughput 0.723717
Reading from 2828: heap size 1485 MB, throughput 0.69402
Reading from 2828: heap size 1512 MB, throughput 0.73083
Reading from 2828: heap size 1524 MB, throughput 0.709836
Reading from 2828: heap size 1555 MB, throughput 0.755594
Reading from 2828: heap size 1561 MB, throughput 0.197225
Reading from 2828: heap size 1478 MB, throughput 0.991913
Client 2828 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
