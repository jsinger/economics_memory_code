#!/bin/bash
# Download and build OpenJDK 7

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
VM_DIR="${DIR}/icedtea7"

mkdir -p "${VM_DIR}" || exit 1

function download()
{
    if [ -e "${VM_DIR}/download.done" ]; then
        return 0
    else
        cd "${VM_DIR}" &&
        wget 'http://icedtea.classpath.org/download/source/icedtea-2.4.3.tar.gz' &&
        tar -xvf 'icedtea-2.4.3.tar.gz' &&
        touch "${VM_DIR}/download.done" &&
        return 0

        return 1
    fi
}

function configure()
{
    if [ -e "${VM_DIR}/configure.done" ]; then
        return 0
    else
        cd "${VM_DIR}" &&
        mkdir -p "${VM_DIR}/icedtea-build" &&
        cd "${VM_DIR}/icedtea-2.4.3" &&
        ./autogen.sh &&
        cd ../icedtea-build &&
        ../icedtea-2.4.3/configure "--prefix=${VM_DIR}/openjdk7-install" --disable-system-kerberos --disable-system-lcms &&
        make clean &&
        touch "${VM_DIR}/configure.done" &&
        return 0

        return 1
    fi
}

function build()
{
    if [ -e "${VM_DIR}/install.done" ]; then
        return 0
    else
        cd "${VM_DIR}/icedtea-build" &&
        make &&
        touch "${VM_DIR}/install.done" &&
        return 0

        return 1
    fi
}

download || exit 1
configure || exit 1
build || exit 1
exit 0
