#!/usr/bin/env python
# Generate a list of points to measure overall throughput at, to compare against predictions
# Args: dir total_mem_to_use
# Outputs: benchmark1_heap_size benchmark2_heap_size

import sys

if len(sys.argv) < 3:
    print "Usage: python " + sys.argv[0] + " dir total_mem_to_use"
    exit(1)

dir = sys.argv[1] + "/"
total = int(sys.argv[2])

def firstTwoLines(filename):
    f = open(filename)
    lines = f.readlines()
    return (lines[0].strip(), lines[1].strip())

def readFileInts(filename):
    l = []
    for line in open(filename):
        l.append(int(line))
    return l

def heapSizesFile(b):
    return dir + b + "_heapSizes.txt"

(b1, b2) = firstTwoLines(dir + "benchmarks.txt")
b1_heap = readFileInts(heapSizesFile(b1))
b2_heap = readFileInts(heapSizesFile(b2))

l = []

for h1 in b1_heap:
    for h2 in b2_heap:
        if h1 + h2 <= total:
            l.append((h1, h2))

for t in l:
    print str(t[0]) + " " + str(t[1])
