((nil
  (flycheck-clang-include-path . ("common"
                                  "openjdk8/jdk8/hotspot/src/share/vm/prims"
                                  "openjdk8/jdk8/hotspot/src/share/vm"
                                  "openjdk8/jdk8/hotspot/src/share/vm/precompiled"
                                  "openjdk8/jdk8/hotspot/src/cpu/x86/vm"
                                  "openjdk8/jdk8/hotspot/src/os_cpu/linux_x86/vm"
                                  "openjdk8/jdk8/hotspot/src/os/linux/vm"
                                  "openjdk8/jdk8/hotspot/src/os/posix/vm"))
  (flycheck-ghc-search-path . ("../haskell_common"))))
