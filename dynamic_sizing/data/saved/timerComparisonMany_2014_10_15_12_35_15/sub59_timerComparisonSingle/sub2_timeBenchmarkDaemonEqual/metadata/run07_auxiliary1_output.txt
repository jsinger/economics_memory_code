economemd
    total memory: 1440 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub59_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 16112: heap size 9 MB, throughput 0.980863
Clients: 1
Client 16112 has a minimum heap size of 12 MB
Reading from 16110: heap size 9 MB, throughput 0.992054
Clients: 2
Client 16110 has a minimum heap size of 276 MB
Reading from 16112: heap size 9 MB, throughput 0.986199
Reading from 16110: heap size 9 MB, throughput 0.982364
Reading from 16110: heap size 9 MB, throughput 0.966872
Reading from 16112: heap size 11 MB, throughput 0.97702
Reading from 16110: heap size 9 MB, throughput 0.966538
Reading from 16112: heap size 11 MB, throughput 0.973051
Reading from 16110: heap size 11 MB, throughput 0.985544
Reading from 16110: heap size 11 MB, throughput 0.97256
Reading from 16112: heap size 15 MB, throughput 0.991709
Reading from 16110: heap size 17 MB, throughput 0.961223
Reading from 16112: heap size 15 MB, throughput 0.976651
Reading from 16110: heap size 17 MB, throughput 0.63305
Reading from 16110: heap size 30 MB, throughput 0.916483
Reading from 16110: heap size 31 MB, throughput 0.914606
Reading from 16110: heap size 35 MB, throughput 0.390294
Reading from 16112: heap size 24 MB, throughput 0.93917
Reading from 16110: heap size 44 MB, throughput 0.892659
Reading from 16110: heap size 50 MB, throughput 0.888732
Reading from 16112: heap size 29 MB, throughput 0.990298
Reading from 16110: heap size 51 MB, throughput 0.278137
Reading from 16110: heap size 69 MB, throughput 0.740676
Reading from 16112: heap size 36 MB, throughput 0.989599
Reading from 16110: heap size 71 MB, throughput 0.250946
Reading from 16110: heap size 97 MB, throughput 0.674714
Reading from 16110: heap size 98 MB, throughput 0.650224
Reading from 16112: heap size 41 MB, throughput 0.987119
Reading from 16112: heap size 42 MB, throughput 0.983309
Reading from 16110: heap size 99 MB, throughput 0.176824
Reading from 16110: heap size 128 MB, throughput 0.705544
Reading from 16112: heap size 45 MB, throughput 0.953735
Reading from 16110: heap size 133 MB, throughput 0.746113
Reading from 16110: heap size 134 MB, throughput 0.588964
Reading from 16112: heap size 52 MB, throughput 0.955443
Reading from 16110: heap size 138 MB, throughput 0.660108
Reading from 16110: heap size 143 MB, throughput 0.618558
Reading from 16112: heap size 52 MB, throughput 0.967088
Reading from 16110: heap size 152 MB, throughput 0.530207
Reading from 16110: heap size 156 MB, throughput 0.656117
Reading from 16112: heap size 63 MB, throughput 0.980429
Reading from 16112: heap size 63 MB, throughput 0.943348
Reading from 16112: heap size 77 MB, throughput 0.655322
Reading from 16110: heap size 160 MB, throughput 0.098627
Reading from 16112: heap size 81 MB, throughput 0.989427
Reading from 16110: heap size 194 MB, throughput 0.597116
Reading from 16110: heap size 203 MB, throughput 0.559433
Reading from 16110: heap size 206 MB, throughput 0.841379
Reading from 16112: heap size 93 MB, throughput 0.99562
Reading from 16110: heap size 211 MB, throughput 0.928335
Reading from 16110: heap size 215 MB, throughput 0.76308
Reading from 16110: heap size 221 MB, throughput 0.509261
Reading from 16110: heap size 227 MB, throughput 0.779992
Reading from 16112: heap size 94 MB, throughput 0.997055
Reading from 16110: heap size 232 MB, throughput 0.413492
Reading from 16112: heap size 105 MB, throughput 0.997474
Reading from 16110: heap size 236 MB, throughput 0.0886063
Reading from 16110: heap size 275 MB, throughput 0.600644
Reading from 16110: heap size 220 MB, throughput 0.658535
Reading from 16110: heap size 267 MB, throughput 0.807849
Reading from 16110: heap size 271 MB, throughput 0.572312
Reading from 16110: heap size 269 MB, throughput 0.593581
Reading from 16112: heap size 107 MB, throughput 0.996869
Reading from 16110: heap size 271 MB, throughput 0.13959
Reading from 16110: heap size 305 MB, throughput 0.602794
Reading from 16110: heap size 308 MB, throughput 0.837636
Reading from 16112: heap size 116 MB, throughput 0.99782
Reading from 16110: heap size 313 MB, throughput 0.97071
Reading from 16110: heap size 313 MB, throughput 0.730928
Reading from 16110: heap size 316 MB, throughput 0.881688
Reading from 16110: heap size 316 MB, throughput 0.85396
Reading from 16110: heap size 318 MB, throughput 0.859431
Reading from 16110: heap size 319 MB, throughput 0.902396
Reading from 16110: heap size 320 MB, throughput 0.859796
Reading from 16112: heap size 118 MB, throughput 0.996691
Reading from 16110: heap size 321 MB, throughput 0.719288
Reading from 16110: heap size 317 MB, throughput 0.748082
Reading from 16110: heap size 320 MB, throughput 0.694633
Reading from 16110: heap size 321 MB, throughput 0.692803
Reading from 16110: heap size 322 MB, throughput 0.719725
Equal recommendation: 720 MB each
Reading from 16112: heap size 126 MB, throughput 0.994298
Reading from 16110: heap size 327 MB, throughput 0.972274
Reading from 16112: heap size 127 MB, throughput 0.997327
Reading from 16112: heap size 132 MB, throughput 0.997615
Reading from 16110: heap size 328 MB, throughput 0.977583
Reading from 16112: heap size 133 MB, throughput 0.995898
Reading from 16110: heap size 332 MB, throughput 0.979857
Reading from 16112: heap size 138 MB, throughput 0.997781
Reading from 16112: heap size 139 MB, throughput 0.997214
Reading from 16110: heap size 334 MB, throughput 0.656593
Reading from 16112: heap size 143 MB, throughput 0.997399
Reading from 16112: heap size 143 MB, throughput 0.996711
Reading from 16110: heap size 376 MB, throughput 0.976795
Reading from 16112: heap size 148 MB, throughput 0.994224
Reading from 16110: heap size 377 MB, throughput 0.985252
Reading from 16112: heap size 148 MB, throughput 0.993728
Reading from 16112: heap size 152 MB, throughput 0.997868
Reading from 16110: heap size 380 MB, throughput 0.984721
Equal recommendation: 720 MB each
Reading from 16112: heap size 152 MB, throughput 0.998268
Reading from 16110: heap size 381 MB, throughput 0.985678
Reading from 16112: heap size 155 MB, throughput 0.99819
Reading from 16112: heap size 156 MB, throughput 0.998488
Reading from 16110: heap size 378 MB, throughput 0.984109
Reading from 16112: heap size 159 MB, throughput 0.997105
Reading from 16112: heap size 159 MB, throughput 0.994634
Reading from 16110: heap size 381 MB, throughput 0.968596
Reading from 16112: heap size 162 MB, throughput 0.9929
Reading from 16112: heap size 163 MB, throughput 0.996955
Reading from 16110: heap size 378 MB, throughput 0.977014
Reading from 16112: heap size 168 MB, throughput 0.996455
Reading from 16110: heap size 380 MB, throughput 0.980449
Reading from 16112: heap size 168 MB, throughput 0.996841
Reading from 16112: heap size 172 MB, throughput 0.997148
Reading from 16110: heap size 382 MB, throughput 0.984646
Reading from 16112: heap size 173 MB, throughput 0.997073
Equal recommendation: 720 MB each
Reading from 16110: heap size 382 MB, throughput 0.982055
Reading from 16112: heap size 177 MB, throughput 0.997642
Reading from 16112: heap size 177 MB, throughput 0.997068
Reading from 16110: heap size 384 MB, throughput 0.988318
Reading from 16110: heap size 385 MB, throughput 0.896915
Reading from 16110: heap size 381 MB, throughput 0.83564
Reading from 16110: heap size 386 MB, throughput 0.802423
Reading from 16110: heap size 392 MB, throughput 0.845572
Reading from 16112: heap size 181 MB, throughput 0.997724
Reading from 16110: heap size 395 MB, throughput 0.984358
Reading from 16112: heap size 181 MB, throughput 0.996846
Reading from 16112: heap size 185 MB, throughput 0.997785
Reading from 16110: heap size 400 MB, throughput 0.990428
Reading from 16112: heap size 185 MB, throughput 0.997234
Reading from 16110: heap size 402 MB, throughput 0.989826
Reading from 16112: heap size 188 MB, throughput 0.996767
Reading from 16112: heap size 188 MB, throughput 0.997489
Reading from 16110: heap size 402 MB, throughput 0.988789
Equal recommendation: 720 MB each
Reading from 16112: heap size 191 MB, throughput 0.996818
Reading from 16110: heap size 404 MB, throughput 0.98956
Reading from 16112: heap size 192 MB, throughput 0.995754
Reading from 16112: heap size 196 MB, throughput 0.994531
Reading from 16112: heap size 196 MB, throughput 0.995596
Reading from 16110: heap size 401 MB, throughput 0.984375
Reading from 16112: heap size 201 MB, throughput 0.997867
Reading from 16110: heap size 404 MB, throughput 0.987732
Reading from 16112: heap size 201 MB, throughput 0.996967
Reading from 16112: heap size 204 MB, throughput 0.998227
Reading from 16110: heap size 400 MB, throughput 0.988737
Reading from 16112: heap size 205 MB, throughput 0.997794
Reading from 16110: heap size 402 MB, throughput 0.985855
Reading from 16112: heap size 209 MB, throughput 0.998211
Equal recommendation: 720 MB each
Reading from 16112: heap size 209 MB, throughput 0.997184
Reading from 16110: heap size 400 MB, throughput 0.979489
Reading from 16112: heap size 213 MB, throughput 0.997891
Reading from 16110: heap size 402 MB, throughput 0.981872
Reading from 16112: heap size 213 MB, throughput 0.997506
Reading from 16110: heap size 401 MB, throughput 0.988407
Reading from 16112: heap size 217 MB, throughput 0.99806
Reading from 16110: heap size 402 MB, throughput 0.980654
Reading from 16110: heap size 402 MB, throughput 0.895759
Reading from 16110: heap size 403 MB, throughput 0.873039
Reading from 16110: heap size 407 MB, throughput 0.921836
Reading from 16112: heap size 217 MB, throughput 0.997626
Reading from 16110: heap size 409 MB, throughput 0.990245
Reading from 16112: heap size 220 MB, throughput 0.997456
Reading from 16112: heap size 220 MB, throughput 0.998012
Reading from 16110: heap size 414 MB, throughput 0.985804
Reading from 16112: heap size 223 MB, throughput 0.881726
Equal recommendation: 720 MB each
Reading from 16112: heap size 229 MB, throughput 0.99682
Reading from 16110: heap size 415 MB, throughput 0.99056
Reading from 16112: heap size 235 MB, throughput 0.99874
Reading from 16110: heap size 415 MB, throughput 0.991057
Reading from 16112: heap size 235 MB, throughput 0.998825
Reading from 16112: heap size 239 MB, throughput 0.998794
Reading from 16110: heap size 417 MB, throughput 0.98922
Reading from 16112: heap size 240 MB, throughput 0.998776
Reading from 16110: heap size 414 MB, throughput 0.990106
Reading from 16112: heap size 244 MB, throughput 0.998714
Reading from 16112: heap size 244 MB, throughput 0.997859
Equal recommendation: 720 MB each
Reading from 16110: heap size 416 MB, throughput 0.988869
Reading from 16112: heap size 248 MB, throughput 0.998911
Reading from 16110: heap size 415 MB, throughput 0.988457
Reading from 16112: heap size 248 MB, throughput 0.998479
Reading from 16110: heap size 416 MB, throughput 0.989672
Reading from 16112: heap size 251 MB, throughput 0.998082
Reading from 16112: heap size 252 MB, throughput 0.998723
Reading from 16110: heap size 418 MB, throughput 0.977243
Reading from 16112: heap size 255 MB, throughput 0.996189
Reading from 16112: heap size 256 MB, throughput 0.993862
Reading from 16110: heap size 418 MB, throughput 0.980146
Reading from 16110: heap size 422 MB, throughput 0.872754
Reading from 16110: heap size 424 MB, throughput 0.839224
Reading from 16110: heap size 432 MB, throughput 0.950542
Reading from 16112: heap size 262 MB, throughput 0.99811
Equal recommendation: 720 MB each
Reading from 16110: heap size 435 MB, throughput 0.991584
Reading from 16112: heap size 262 MB, throughput 0.9979
Reading from 16112: heap size 268 MB, throughput 0.997924
Reading from 16110: heap size 434 MB, throughput 0.989571
Reading from 16112: heap size 269 MB, throughput 0.998313
Reading from 16110: heap size 437 MB, throughput 0.989749
Reading from 16112: heap size 273 MB, throughput 0.997666
Reading from 16112: heap size 274 MB, throughput 0.996474
Reading from 16110: heap size 436 MB, throughput 0.991153
Equal recommendation: 720 MB each
Reading from 16112: heap size 280 MB, throughput 0.998402
Reading from 16110: heap size 438 MB, throughput 0.990328
Reading from 16112: heap size 280 MB, throughput 0.997185
Reading from 16112: heap size 286 MB, throughput 0.99725
Reading from 16110: heap size 439 MB, throughput 0.863833
Reading from 16112: heap size 286 MB, throughput 0.995012
Reading from 16112: heap size 293 MB, throughput 0.996362
Reading from 16110: heap size 470 MB, throughput 0.997047
Reading from 16112: heap size 293 MB, throughput 0.998194
Reading from 16110: heap size 469 MB, throughput 0.994651
Reading from 16112: heap size 300 MB, throughput 0.998127
Equal recommendation: 720 MB each
Reading from 16110: heap size 473 MB, throughput 0.989037
Reading from 16110: heap size 473 MB, throughput 0.954232
Reading from 16110: heap size 475 MB, throughput 0.86462
Reading from 16112: heap size 301 MB, throughput 0.998149
Reading from 16110: heap size 480 MB, throughput 0.861438
Reading from 16112: heap size 308 MB, throughput 0.99735
Reading from 16110: heap size 481 MB, throughput 0.992403
Reading from 16112: heap size 308 MB, throughput 0.998323
Reading from 16110: heap size 487 MB, throughput 0.993249
Reading from 16112: heap size 314 MB, throughput 0.998402
Reading from 16110: heap size 489 MB, throughput 0.992494
Reading from 16112: heap size 315 MB, throughput 0.998465
Equal recommendation: 720 MB each
Reading from 16112: heap size 320 MB, throughput 0.998181
Reading from 16110: heap size 489 MB, throughput 0.986569
Reading from 16112: heap size 321 MB, throughput 0.993894
Reading from 16110: heap size 492 MB, throughput 0.990777
Reading from 16112: heap size 325 MB, throughput 0.998559
Reading from 16112: heap size 326 MB, throughput 0.997132
Reading from 16110: heap size 490 MB, throughput 0.990567
Reading from 16112: heap size 334 MB, throughput 0.998515
Reading from 16110: heap size 492 MB, throughput 0.987983
Equal recommendation: 720 MB each
Reading from 16112: heap size 334 MB, throughput 0.99793
Reading from 16110: heap size 496 MB, throughput 0.987094
Reading from 16112: heap size 341 MB, throughput 0.998366
Reading from 16110: heap size 496 MB, throughput 0.983357
Reading from 16110: heap size 502 MB, throughput 0.910246
Reading from 16110: heap size 504 MB, throughput 0.906205
Reading from 16112: heap size 341 MB, throughput 0.998365
Reading from 16110: heap size 511 MB, throughput 0.994196
Reading from 16112: heap size 347 MB, throughput 0.997754
Reading from 16112: heap size 348 MB, throughput 0.997221
Reading from 16110: heap size 511 MB, throughput 0.994192
Equal recommendation: 720 MB each
Reading from 16112: heap size 355 MB, throughput 0.997153
Reading from 16110: heap size 515 MB, throughput 0.993818
Reading from 16112: heap size 355 MB, throughput 0.997075
Reading from 16110: heap size 517 MB, throughput 0.992456
Reading from 16112: heap size 363 MB, throughput 0.998407
Reading from 16112: heap size 363 MB, throughput 0.998182
Reading from 16110: heap size 514 MB, throughput 0.991863
Equal recommendation: 720 MB each
Reading from 16112: heap size 369 MB, throughput 0.998428
Reading from 16110: heap size 517 MB, throughput 0.991133
Reading from 16112: heap size 370 MB, throughput 0.99817
Reading from 16110: heap size 517 MB, throughput 0.989573
Reading from 16112: heap size 377 MB, throughput 0.998509
Reading from 16110: heap size 518 MB, throughput 0.989543
Reading from 16110: heap size 520 MB, throughput 0.925817
Reading from 16110: heap size 520 MB, throughput 0.903662
Reading from 16112: heap size 377 MB, throughput 0.995965
Reading from 16112: heap size 383 MB, throughput 0.997141
Reading from 16110: heap size 526 MB, throughput 0.995517
Equal recommendation: 720 MB each
Reading from 16112: heap size 384 MB, throughput 0.998542
Reading from 16110: heap size 527 MB, throughput 0.993448
Reading from 16112: heap size 392 MB, throughput 0.998621
Reading from 16110: heap size 530 MB, throughput 0.994466
Reading from 16112: heap size 392 MB, throughput 0.998394
Reading from 16110: heap size 532 MB, throughput 0.992942
Reading from 16112: heap size 400 MB, throughput 0.998576
Equal recommendation: 720 MB each
Reading from 16112: heap size 400 MB, throughput 0.998069
Reading from 16110: heap size 530 MB, throughput 0.992761
Reading from 16112: heap size 406 MB, throughput 0.99605
Reading from 16110: heap size 533 MB, throughput 0.991012
Reading from 16112: heap size 406 MB, throughput 0.995931
Reading from 16110: heap size 535 MB, throughput 0.994688
Reading from 16112: heap size 415 MB, throughput 0.998557
Reading from 16110: heap size 536 MB, throughput 0.932441
Reading from 16110: heap size 535 MB, throughput 0.939878
Equal recommendation: 720 MB each
Reading from 16112: heap size 416 MB, throughput 0.998209
Reading from 16110: heap size 538 MB, throughput 0.994738
Reading from 16112: heap size 424 MB, throughput 0.998792
Reading from 16110: heap size 541 MB, throughput 0.994172
Reading from 16112: heap size 424 MB, throughput 0.998146
Reading from 16110: heap size 543 MB, throughput 0.993251
Reading from 16112: heap size 432 MB, throughput 0.998437
Equal recommendation: 720 MB each
Reading from 16112: heap size 432 MB, throughput 0.951279
Reading from 16110: heap size 543 MB, throughput 0.988953
Reading from 16112: heap size 443 MB, throughput 0.99889
Reading from 16110: heap size 545 MB, throughput 0.992339
Reading from 16112: heap size 444 MB, throughput 0.999215
Reading from 16110: heap size 548 MB, throughput 0.990448
Reading from 16112: heap size 452 MB, throughput 0.999311
Reading from 16110: heap size 548 MB, throughput 0.988121
Reading from 16110: heap size 551 MB, throughput 0.913957
Equal recommendation: 720 MB each
Reading from 16110: heap size 553 MB, throughput 0.993017
Reading from 16112: heap size 453 MB, throughput 0.998462
Reading from 16110: heap size 560 MB, throughput 0.995561
Reading from 16112: heap size 461 MB, throughput 0.999232
Reading from 16112: heap size 461 MB, throughput 0.997281
Reading from 16110: heap size 561 MB, throughput 0.993194
Reading from 16112: heap size 468 MB, throughput 0.997135
Equal recommendation: 720 MB each
Reading from 16110: heap size 561 MB, throughput 0.993921
Reading from 16112: heap size 468 MB, throughput 0.99734
Reading from 16112: heap size 481 MB, throughput 0.998679
Reading from 16110: heap size 563 MB, throughput 0.993694
Reading from 16112: heap size 481 MB, throughput 0.998519
Reading from 16110: heap size 564 MB, throughput 0.992259
Equal recommendation: 720 MB each
Reading from 16110: heap size 565 MB, throughput 0.991002
Reading from 16110: heap size 566 MB, throughput 0.938742
Reading from 16112: heap size 492 MB, throughput 0.99865
Reading from 16110: heap size 568 MB, throughput 0.989472
Reading from 16112: heap size 492 MB, throughput 0.998557
Reading from 16112: heap size 497 MB, throughput 0.996706
Reading from 16110: heap size 574 MB, throughput 0.994947
Equal recommendation: 720 MB each
Reading from 16112: heap size 497 MB, throughput 0.998445
Reading from 16110: heap size 575 MB, throughput 0.994863
Reading from 16112: heap size 497 MB, throughput 0.998605
Reading from 16110: heap size 575 MB, throughput 0.993153
Reading from 16112: heap size 497 MB, throughput 0.998718
Reading from 16110: heap size 577 MB, throughput 0.99289
Equal recommendation: 720 MB each
Reading from 16112: heap size 496 MB, throughput 0.998817
Reading from 16110: heap size 576 MB, throughput 0.986795
Reading from 16112: heap size 497 MB, throughput 0.997285
Reading from 16110: heap size 577 MB, throughput 0.984918
Reading from 16110: heap size 583 MB, throughput 0.938516
Reading from 16112: heap size 497 MB, throughput 0.998264
Reading from 16110: heap size 584 MB, throughput 0.995483
Reading from 16112: heap size 497 MB, throughput 0.998672
Equal recommendation: 720 MB each
Reading from 16110: heap size 590 MB, throughput 0.994241
Reading from 16112: heap size 497 MB, throughput 0.998608
Reading from 16112: heap size 494 MB, throughput 0.998678
Reading from 16110: heap size 591 MB, throughput 0.994468
Reading from 16112: heap size 497 MB, throughput 0.998949
Reading from 16110: heap size 590 MB, throughput 0.991295
Reading from 16112: heap size 497 MB, throughput 0.995871
Equal recommendation: 720 MB each
Reading from 16112: heap size 496 MB, throughput 0.998756
Reading from 16110: heap size 592 MB, throughput 0.993451
Reading from 16112: heap size 497 MB, throughput 0.998655
Reading from 16110: heap size 593 MB, throughput 0.995029
Reading from 16110: heap size 594 MB, throughput 0.930744
Reading from 16110: heap size 595 MB, throughput 0.992783
Reading from 16112: heap size 496 MB, throughput 0.998679
Equal recommendation: 720 MB each
Reading from 16110: heap size 597 MB, throughput 0.995553
Reading from 16112: heap size 496 MB, throughput 0.998688
Reading from 16110: heap size 598 MB, throughput 0.995759
Client 16112 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 16110: heap size 600 MB, throughput 0.9939
Reading from 16110: heap size 599 MB, throughput 0.992865
Reading from 16110: heap size 601 MB, throughput 0.995453
Reading from 16110: heap size 604 MB, throughput 0.946936
Reading from 16110: heap size 604 MB, throughput 0.992956
Recommendation: one client; give it all the memory
Reading from 16110: heap size 611 MB, throughput 0.995427
Reading from 16110: heap size 611 MB, throughput 0.995231
Recommendation: one client; give it all the memory
Reading from 16110: heap size 609 MB, throughput 0.993839
Reading from 16110: heap size 612 MB, throughput 0.993181
Reading from 16110: heap size 614 MB, throughput 0.993254
Reading from 16110: heap size 615 MB, throughput 0.935776
Client 16110 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
