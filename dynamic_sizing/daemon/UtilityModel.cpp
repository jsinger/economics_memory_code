#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sstream>
#include <Utilities.hpp>
#include "UtilityModel.hpp"
#include "LogUtilityModel.hpp"
#include "PyOptimiser.hpp"
#include "RootUtilityModel.hpp"
#include "ReadingIterator.hpp"
#include "UtilityFunction.hpp"

const std::string UtilityModel::DEFAULT_UTILITY_MODEL = RootUtilityModel::NAME;

UtilityModel* UtilityModel::fromName(const std::string& modelName, bool alwaysNumericOptimisation)
{
    if (modelName == RootUtilityModel::NAME)
        return new RootUtilityModel(alwaysNumericOptimisation);
    else if (modelName == LogUtilityModel::NAME)
        return new LogUtilityModel;
    else
        return NULL;
}

std::vector<std::string> UtilityModel::allModelNames()
{
    return { RootUtilityModel::NAME, LogUtilityModel::NAME };
}

double UtilityModel::combineUtilities(const std::vector<const UtilityFunction*>& functions, const std::vector<double>& heapSizes) const
{
    assert(functions.size() == heapSizes.size());
    size_t minSize = functions.size();
    std::vector<double> v(minSize, 0.0f);
    for (size_t i = 0; i < minSize; ++i)
        v[i] =  functions[i]->evaluate(heapSizes[i]);
    return combineRawUtilities(v);
}

std::pair<double, double> UtilityModel::fitStraightLine(ReadingIterator& it,
                                                        std::function<double(double)> xfilter,
                                                        std::function<double(double)> yfilter)
{
    // Simple linear regression
    size_t size = 0;
    double xbar = 0.0f;
    double ybar = 0.0f;
    double xybar = 0.0f;
    double x2bar = 0.0f;

    while (it.hasNext())
    {
        std::pair<size_t, double> p = it.getNext();
        double x = xfilter(p.first);
        double y = yfilter(p.second);
        xbar += x;
        ybar += y;
        xybar += (x * y);
        x2bar += (x * x);

        ++size;
    }

    xbar = xbar / size;
    ybar = ybar / size;
    xybar = xybar / size;
    x2bar = x2bar / size;

    double b = (xybar - (xbar * ybar)) / (x2bar - (xbar * xbar));
    double a = ybar - (b * xbar);

    return std::pair<double, double>(a, b);
}

PartitionResult* UtilityModel::partitionMemoryNumeric(unsigned long long totalMem, const std::vector<const UtilityFunction*>& functions, const std::vector<unsigned long long>& minHeaps) const
{
    return PyOptimiser::partitionMemoryNumeric(totalMem,
                                               minHeaps,
                                               [&] (const std::vector<double>& heapSizes) { return combineUtilities(functions, heapSizes); });
}

void PartitionResult::print(const std::vector<const UtilityFunction*>& functions) const
{
    printf("Recommendation: %zu clients, utility %g:\n", heapSizes.size(), utility);
    for (size_t i = 0; i < heapSizes.size(); ++i)
        printf("    h%zu: %g MB (%s)\n", i + 1, heapSizes[i], functions[i]->toString().c_str());
}
