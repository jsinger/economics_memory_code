economemd
    total memory: 3657 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub7_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 14828: heap size 9 MB, throughput 0.988641
Clients: 1
Client 14828 has a minimum heap size of 8 MB
Reading from 14827: heap size 9 MB, throughput 0.990756
Clients: 2
Client 14827 has a minimum heap size of 1211 MB
Reading from 14827: heap size 9 MB, throughput 0.974378
Reading from 14828: heap size 9 MB, throughput 0.972774
Reading from 14827: heap size 9 MB, throughput 0.955973
Reading from 14827: heap size 9 MB, throughput 0.879851
Reading from 14828: heap size 11 MB, throughput 0.80966
Reading from 14828: heap size 15 MB, throughput 0.932532
Reading from 14827: heap size 11 MB, throughput 0.988308
Reading from 14828: heap size 17 MB, throughput 0.982792
Reading from 14827: heap size 11 MB, throughput 0.985754
Reading from 14828: heap size 19 MB, throughput 0.98993
Reading from 14827: heap size 17 MB, throughput 0.927377
Reading from 14828: heap size 26 MB, throughput 0.972428
Reading from 14828: heap size 25 MB, throughput 0.827641
Reading from 14827: heap size 17 MB, throughput 0.506884
Reading from 14828: heap size 29 MB, throughput 0.968883
Reading from 14828: heap size 29 MB, throughput 0.965287
Reading from 14828: heap size 33 MB, throughput 0.915771
Reading from 14828: heap size 33 MB, throughput 0.975717
Reading from 14827: heap size 30 MB, throughput 0.966257
Reading from 14828: heap size 40 MB, throughput 0.97711
Reading from 14828: heap size 40 MB, throughput 0.930973
Reading from 14827: heap size 31 MB, throughput 0.958114
Reading from 14828: heap size 49 MB, throughput 0.988685
Reading from 14828: heap size 49 MB, throughput 0.965901
Reading from 14827: heap size 36 MB, throughput 0.283413
Reading from 14828: heap size 59 MB, throughput 0.984642
Reading from 14827: heap size 48 MB, throughput 0.834069
Reading from 14828: heap size 59 MB, throughput 0.980005
Reading from 14828: heap size 67 MB, throughput 0.980648
Reading from 14828: heap size 68 MB, throughput 0.98797
Reading from 14827: heap size 50 MB, throughput 0.518314
Reading from 14828: heap size 74 MB, throughput 0.994487
Reading from 14827: heap size 66 MB, throughput 0.823535
Reading from 14828: heap size 76 MB, throughput 0.981146
Reading from 14827: heap size 72 MB, throughput 0.799733
Reading from 14828: heap size 85 MB, throughput 0.984489
Reading from 14827: heap size 72 MB, throughput 0.300675
Reading from 14828: heap size 85 MB, throughput 0.992137
Reading from 14827: heap size 102 MB, throughput 0.84865
Reading from 14828: heap size 92 MB, throughput 0.990867
Reading from 14828: heap size 93 MB, throughput 0.987657
Reading from 14827: heap size 102 MB, throughput 0.301271
Reading from 14827: heap size 134 MB, throughput 0.885805
Reading from 14828: heap size 99 MB, throughput 0.993669
Reading from 14827: heap size 134 MB, throughput 0.859281
Reading from 14828: heap size 99 MB, throughput 0.990279
Reading from 14827: heap size 135 MB, throughput 0.792164
Reading from 14828: heap size 105 MB, throughput 0.991774
Reading from 14827: heap size 138 MB, throughput 0.724692
Reading from 14828: heap size 105 MB, throughput 0.990906
Reading from 14828: heap size 111 MB, throughput 0.992334
Reading from 14827: heap size 141 MB, throughput 0.145424
Reading from 14828: heap size 111 MB, throughput 0.991389
Reading from 14827: heap size 178 MB, throughput 0.683861
Reading from 14827: heap size 182 MB, throughput 0.689511
Reading from 14827: heap size 185 MB, throughput 0.760283
Reading from 14828: heap size 118 MB, throughput 0.993493
Reading from 14827: heap size 195 MB, throughput 0.752815
Reading from 14828: heap size 118 MB, throughput 0.994377
Reading from 14828: heap size 122 MB, throughput 0.995366
Reading from 14828: heap size 123 MB, throughput 0.991205
Reading from 14827: heap size 197 MB, throughput 0.182793
Reading from 14827: heap size 237 MB, throughput 0.599621
Reading from 14828: heap size 127 MB, throughput 0.994949
Reading from 14827: heap size 245 MB, throughput 0.709976
Reading from 14828: heap size 127 MB, throughput 0.977112
Reading from 14827: heap size 247 MB, throughput 0.585488
Reading from 14828: heap size 132 MB, throughput 0.994948
Reading from 14827: heap size 251 MB, throughput 0.524308
Reading from 14828: heap size 132 MB, throughput 0.988898
Reading from 14827: heap size 256 MB, throughput 0.552786
Reading from 14827: heap size 265 MB, throughput 0.553561
Reading from 14828: heap size 138 MB, throughput 0.994969
Reading from 14828: heap size 138 MB, throughput 0.994303
Reading from 14828: heap size 143 MB, throughput 0.993643
Reading from 14828: heap size 143 MB, throughput 0.994123
Reading from 14827: heap size 271 MB, throughput 0.113758
Reading from 14827: heap size 322 MB, throughput 0.459429
Reading from 14828: heap size 148 MB, throughput 0.995414
Reading from 14828: heap size 148 MB, throughput 0.994433
Reading from 14827: heap size 321 MB, throughput 0.143508
Reading from 14828: heap size 152 MB, throughput 0.996502
Reading from 14827: heap size 378 MB, throughput 0.503572
Reading from 14828: heap size 153 MB, throughput 0.99512
Reading from 14827: heap size 376 MB, throughput 0.673776
Reading from 14827: heap size 379 MB, throughput 0.60854
Reading from 14828: heap size 156 MB, throughput 0.99663
Reading from 14827: heap size 380 MB, throughput 0.625702
Reading from 14828: heap size 157 MB, throughput 0.993376
Reading from 14827: heap size 381 MB, throughput 0.571911
Reading from 14827: heap size 383 MB, throughput 0.541191
Reading from 14828: heap size 160 MB, throughput 0.996656
Reading from 14827: heap size 391 MB, throughput 0.505294
Reading from 14828: heap size 161 MB, throughput 0.994391
Reading from 14827: heap size 397 MB, throughput 0.470486
Reading from 14827: heap size 407 MB, throughput 0.492582
Reading from 14828: heap size 164 MB, throughput 0.996774
Reading from 14827: heap size 412 MB, throughput 0.468772
Reading from 14828: heap size 164 MB, throughput 0.994882
Reading from 14828: heap size 168 MB, throughput 0.995698
Reading from 14828: heap size 168 MB, throughput 0.99153
Reading from 14828: heap size 172 MB, throughput 0.994846
Reading from 14827: heap size 422 MB, throughput 0.0713517
Reading from 14827: heap size 489 MB, throughput 0.447771
Reading from 14828: heap size 172 MB, throughput 0.997384
Reading from 14827: heap size 493 MB, throughput 0.533993
Reading from 14828: heap size 176 MB, throughput 0.993228
Equal recommendation: 1828 MB each
Reading from 14828: heap size 176 MB, throughput 0.995607
Reading from 14828: heap size 180 MB, throughput 0.997957
Reading from 14827: heap size 484 MB, throughput 0.105918
Reading from 14827: heap size 554 MB, throughput 0.464009
Reading from 14828: heap size 180 MB, throughput 0.997417
Reading from 14827: heap size 545 MB, throughput 0.578508
Reading from 14828: heap size 184 MB, throughput 0.995196
Reading from 14827: heap size 550 MB, throughput 0.611416
Reading from 14827: heap size 550 MB, throughput 0.568908
Reading from 14828: heap size 184 MB, throughput 0.995925
Reading from 14827: heap size 551 MB, throughput 0.52834
Reading from 14827: heap size 553 MB, throughput 0.527762
Reading from 14828: heap size 187 MB, throughput 0.965442
Reading from 14827: heap size 561 MB, throughput 0.591116
Reading from 14828: heap size 190 MB, throughput 0.995155
Reading from 14827: heap size 568 MB, throughput 0.547916
Reading from 14828: heap size 195 MB, throughput 0.992092
Reading from 14827: heap size 577 MB, throughput 0.549832
Reading from 14828: heap size 195 MB, throughput 0.992781
Reading from 14828: heap size 203 MB, throughput 0.994598
Reading from 14828: heap size 203 MB, throughput 0.994385
Reading from 14827: heap size 583 MB, throughput 0.069366
Reading from 14828: heap size 212 MB, throughput 0.997198
Reading from 14827: heap size 656 MB, throughput 0.445479
Reading from 14827: heap size 666 MB, throughput 0.492705
Reading from 14828: heap size 212 MB, throughput 0.996343
Reading from 14827: heap size 668 MB, throughput 0.646234
Reading from 14828: heap size 219 MB, throughput 0.994616
Reading from 14828: heap size 219 MB, throughput 0.996851
Reading from 14827: heap size 672 MB, throughput 0.831909
Reading from 14828: heap size 227 MB, throughput 0.996862
Reading from 14828: heap size 227 MB, throughput 0.995541
Reading from 14827: heap size 679 MB, throughput 0.780324
Reading from 14828: heap size 235 MB, throughput 0.996893
Reading from 14828: heap size 235 MB, throughput 0.996237
Reading from 14828: heap size 243 MB, throughput 0.993972
Reading from 14827: heap size 678 MB, throughput 0.858095
Reading from 14827: heap size 693 MB, throughput 0.470515
Reading from 14828: heap size 243 MB, throughput 0.977074
Reading from 14827: heap size 705 MB, throughput 0.446583
Reading from 14827: heap size 715 MB, throughput 0.469321
Reading from 14828: heap size 252 MB, throughput 0.997823
Reading from 14828: heap size 252 MB, throughput 0.996765
Reading from 14828: heap size 264 MB, throughput 0.998165
Reading from 14828: heap size 266 MB, throughput 0.996541
Reading from 14827: heap size 722 MB, throughput 0.0251842
Reading from 14827: heap size 791 MB, throughput 0.178808
Reading from 14828: heap size 276 MB, throughput 0.997652
Reading from 14827: heap size 779 MB, throughput 0.322065
Reading from 14828: heap size 276 MB, throughput 0.996465
Equal recommendation: 1828 MB each
Reading from 14828: heap size 285 MB, throughput 0.997416
Reading from 14827: heap size 662 MB, throughput 0.0358448
Reading from 14827: heap size 866 MB, throughput 0.233591
Reading from 14827: heap size 867 MB, throughput 0.421573
Reading from 14828: heap size 286 MB, throughput 0.997821
Reading from 14827: heap size 871 MB, throughput 0.852087
Reading from 14827: heap size 872 MB, throughput 0.466382
Reading from 14828: heap size 294 MB, throughput 0.998197
Reading from 14827: heap size 864 MB, throughput 0.514491
Reading from 14827: heap size 868 MB, throughput 0.506008
Reading from 14828: heap size 295 MB, throughput 0.99643
Reading from 14828: heap size 302 MB, throughput 0.998009
Reading from 14828: heap size 302 MB, throughput 0.995976
Reading from 14827: heap size 864 MB, throughput 0.0586267
Reading from 14827: heap size 958 MB, throughput 0.689297
Reading from 14828: heap size 309 MB, throughput 0.997128
Reading from 14827: heap size 963 MB, throughput 0.930279
Reading from 14827: heap size 967 MB, throughput 0.934548
Reading from 14828: heap size 310 MB, throughput 0.996063
Reading from 14827: heap size 967 MB, throughput 0.927195
Reading from 14827: heap size 766 MB, throughput 0.870682
Reading from 14827: heap size 959 MB, throughput 0.879085
Reading from 14828: heap size 318 MB, throughput 0.995622
Reading from 14827: heap size 769 MB, throughput 0.881938
Reading from 14827: heap size 933 MB, throughput 0.72477
Reading from 14828: heap size 319 MB, throughput 0.996136
Reading from 14827: heap size 817 MB, throughput 0.744636
Reading from 14827: heap size 917 MB, throughput 0.736906
Reading from 14827: heap size 822 MB, throughput 0.737492
Reading from 14828: heap size 328 MB, throughput 0.99436
Reading from 14827: heap size 905 MB, throughput 0.70613
Reading from 14827: heap size 828 MB, throughput 0.736406
Reading from 14827: heap size 897 MB, throughput 0.740594
Reading from 14828: heap size 329 MB, throughput 0.996077
Reading from 14827: heap size 904 MB, throughput 0.770673
Reading from 14827: heap size 891 MB, throughput 0.76572
Reading from 14828: heap size 340 MB, throughput 0.995055
Reading from 14827: heap size 898 MB, throughput 0.781549
Reading from 14827: heap size 890 MB, throughput 0.797721
Reading from 14828: heap size 340 MB, throughput 0.996028
Reading from 14828: heap size 353 MB, throughput 0.997124
Reading from 14828: heap size 353 MB, throughput 0.996132
Reading from 14827: heap size 895 MB, throughput 0.972794
Reading from 14828: heap size 365 MB, throughput 0.997671
Reading from 14827: heap size 891 MB, throughput 0.872142
Reading from 14827: heap size 895 MB, throughput 0.747022
Reading from 14828: heap size 365 MB, throughput 0.99653
Reading from 14827: heap size 903 MB, throughput 0.757298
Reading from 14827: heap size 905 MB, throughput 0.729987
Reading from 14827: heap size 913 MB, throughput 0.758595
Reading from 14828: heap size 377 MB, throughput 0.997168
Reading from 14827: heap size 914 MB, throughput 0.726998
Reading from 14827: heap size 923 MB, throughput 0.747418
Reading from 14828: heap size 377 MB, throughput 0.995502
Reading from 14827: heap size 923 MB, throughput 0.73149
Reading from 14827: heap size 932 MB, throughput 0.736708
Equal recommendation: 1828 MB each
Reading from 14828: heap size 388 MB, throughput 0.997619
Reading from 14827: heap size 932 MB, throughput 0.841807
Reading from 14828: heap size 388 MB, throughput 0.998215
Reading from 14827: heap size 935 MB, throughput 0.85499
Reading from 14827: heap size 939 MB, throughput 0.721856
Reading from 14828: heap size 397 MB, throughput 0.998086
Reading from 14827: heap size 948 MB, throughput 0.638358
Reading from 14828: heap size 398 MB, throughput 0.996923
Reading from 14828: heap size 408 MB, throughput 0.994793
Reading from 14828: heap size 408 MB, throughput 0.997781
Reading from 14827: heap size 947 MB, throughput 0.0613019
Reading from 14827: heap size 1079 MB, throughput 0.515342
Reading from 14827: heap size 1083 MB, throughput 0.678619
Reading from 14828: heap size 420 MB, throughput 0.995369
Reading from 14827: heap size 1087 MB, throughput 0.702009
Reading from 14827: heap size 1090 MB, throughput 0.65634
Reading from 14827: heap size 1096 MB, throughput 0.656264
Reading from 14828: heap size 421 MB, throughput 0.995302
Reading from 14827: heap size 1099 MB, throughput 0.693843
Reading from 14827: heap size 1114 MB, throughput 0.63767
Reading from 14828: heap size 434 MB, throughput 0.992834
Reading from 14828: heap size 434 MB, throughput 0.995655
Reading from 14827: heap size 1115 MB, throughput 0.901512
Reading from 14828: heap size 450 MB, throughput 0.996937
Reading from 14828: heap size 451 MB, throughput 0.997237
Reading from 14827: heap size 1132 MB, throughput 0.957784
Reading from 14828: heap size 464 MB, throughput 0.997799
Reading from 14828: heap size 466 MB, throughput 0.996916
Equal recommendation: 1828 MB each
Reading from 14828: heap size 480 MB, throughput 0.996748
Reading from 14827: heap size 1136 MB, throughput 0.932554
Reading from 14828: heap size 480 MB, throughput 0.997411
Reading from 14828: heap size 492 MB, throughput 0.997275
Reading from 14828: heap size 493 MB, throughput 0.997084
Reading from 14827: heap size 1148 MB, throughput 0.939376
Reading from 14828: heap size 506 MB, throughput 0.997612
Reading from 14828: heap size 506 MB, throughput 0.997664
Reading from 14828: heap size 518 MB, throughput 0.998114
Reading from 14827: heap size 1151 MB, throughput 0.937901
Reading from 14828: heap size 519 MB, throughput 0.997235
Reading from 14828: heap size 531 MB, throughput 0.997695
Reading from 14828: heap size 531 MB, throughput 0.998025
Reading from 14827: heap size 1153 MB, throughput 0.936097
Reading from 14828: heap size 542 MB, throughput 0.997883
Reading from 14828: heap size 542 MB, throughput 0.997886
Reading from 14827: heap size 1158 MB, throughput 0.952568
Reading from 14828: heap size 553 MB, throughput 0.997885
Equal recommendation: 1828 MB each
Reading from 14828: heap size 554 MB, throughput 0.99794
Reading from 14828: heap size 565 MB, throughput 0.997986
Reading from 14827: heap size 1163 MB, throughput 0.94466
Reading from 14828: heap size 565 MB, throughput 0.997515
Reading from 14828: heap size 577 MB, throughput 0.998097
Reading from 14828: heap size 577 MB, throughput 0.997467
Reading from 14827: heap size 1164 MB, throughput 0.945382
Reading from 14828: heap size 588 MB, throughput 0.997819
Reading from 14828: heap size 588 MB, throughput 0.997843
Reading from 14827: heap size 1173 MB, throughput 0.928711
Reading from 14828: heap size 601 MB, throughput 0.998326
Reading from 14828: heap size 601 MB, throughput 0.998285
Reading from 14828: heap size 612 MB, throughput 0.996522
Reading from 14827: heap size 1176 MB, throughput 0.947322
Reading from 14828: heap size 612 MB, throughput 0.9981
Equal recommendation: 1828 MB each
Reading from 14828: heap size 625 MB, throughput 0.998358
Reading from 14827: heap size 1184 MB, throughput 0.945091
Reading from 14828: heap size 625 MB, throughput 0.998291
Reading from 14828: heap size 637 MB, throughput 0.998558
Reading from 14827: heap size 1189 MB, throughput 0.941748
Reading from 14828: heap size 637 MB, throughput 0.998072
Reading from 14828: heap size 649 MB, throughput 0.998355
Reading from 14828: heap size 649 MB, throughput 0.997627
Reading from 14827: heap size 1197 MB, throughput 0.936943
Reading from 14828: heap size 660 MB, throughput 0.998281
Reading from 14828: heap size 660 MB, throughput 0.998376
Reading from 14827: heap size 1202 MB, throughput 0.940937
Reading from 14828: heap size 671 MB, throughput 0.997998
Reading from 14828: heap size 672 MB, throughput 0.998374
Reading from 14828: heap size 684 MB, throughput 0.998186
Reading from 14827: heap size 1210 MB, throughput 0.947063
Equal recommendation: 1828 MB each
Reading from 14828: heap size 684 MB, throughput 0.998033
Reading from 14828: heap size 696 MB, throughput 0.998467
Reading from 14827: heap size 1217 MB, throughput 0.936051
Reading from 14828: heap size 696 MB, throughput 0.998396
Reading from 14828: heap size 708 MB, throughput 0.998452
Reading from 14827: heap size 1226 MB, throughput 0.936503
Reading from 14828: heap size 708 MB, throughput 0.998601
Reading from 14828: heap size 718 MB, throughput 0.998796
Reading from 14828: heap size 719 MB, throughput 0.998344
Reading from 14827: heap size 1231 MB, throughput 0.932135
Reading from 14828: heap size 729 MB, throughput 0.998542
Reading from 14828: heap size 729 MB, throughput 0.998118
Reading from 14827: heap size 1240 MB, throughput 0.941112
Equal recommendation: 1828 MB each
Reading from 14828: heap size 740 MB, throughput 0.998446
Reading from 14828: heap size 740 MB, throughput 0.998547
Reading from 14827: heap size 1244 MB, throughput 0.942143
Reading from 14828: heap size 751 MB, throughput 0.998657
Reading from 14828: heap size 752 MB, throughput 0.99864
Reading from 14827: heap size 1253 MB, throughput 0.948679
Reading from 14828: heap size 762 MB, throughput 0.998842
Reading from 14828: heap size 762 MB, throughput 0.998295
Reading from 14828: heap size 773 MB, throughput 0.998516
Reading from 14827: heap size 1255 MB, throughput 0.947687
Reading from 14828: heap size 773 MB, throughput 0.998642
Reading from 14828: heap size 784 MB, throughput 0.998504
Equal recommendation: 1828 MB each
Reading from 14827: heap size 1263 MB, throughput 0.939349
Reading from 14828: heap size 784 MB, throughput 0.998604
Reading from 14828: heap size 795 MB, throughput 0.998589
Reading from 14828: heap size 795 MB, throughput 0.998641
Reading from 14827: heap size 1264 MB, throughput 0.515438
Reading from 14828: heap size 807 MB, throughput 0.99921
Reading from 14828: heap size 807 MB, throughput 0.998425
Reading from 14828: heap size 817 MB, throughput 0.998923
Reading from 14827: heap size 1370 MB, throughput 0.937178
Reading from 14828: heap size 817 MB, throughput 0.998538
Equal recommendation: 1828 MB each
Reading from 14828: heap size 827 MB, throughput 0.998872
Reading from 14827: heap size 1371 MB, throughput 0.968093
Reading from 14828: heap size 827 MB, throughput 0.998953
Reading from 14828: heap size 837 MB, throughput 0.998682
Reading from 14827: heap size 1386 MB, throughput 0.97106
Reading from 14828: heap size 838 MB, throughput 0.998742
Reading from 14828: heap size 848 MB, throughput 0.998864
Reading from 14827: heap size 1389 MB, throughput 0.969027
Reading from 14828: heap size 848 MB, throughput 0.998649
Reading from 14828: heap size 858 MB, throughput 0.998733
Reading from 14828: heap size 858 MB, throughput 0.998698
Reading from 14827: heap size 1393 MB, throughput 0.965489
Equal recommendation: 1828 MB each
Reading from 14828: heap size 869 MB, throughput 0.998955
Reading from 14828: heap size 869 MB, throughput 0.998488
Reading from 14827: heap size 1396 MB, throughput 0.964903
Reading from 14828: heap size 880 MB, throughput 0.998889
Reading from 14828: heap size 880 MB, throughput 0.998876
Reading from 14827: heap size 1385 MB, throughput 0.962364
Reading from 14828: heap size 891 MB, throughput 0.998843
Reading from 14828: heap size 891 MB, throughput 0.999026
Reading from 14827: heap size 1392 MB, throughput 0.957109
Reading from 14828: heap size 901 MB, throughput 0.99878
Reading from 14828: heap size 902 MB, throughput 0.998724
Equal recommendation: 1828 MB each
Reading from 14828: heap size 912 MB, throughput 0.998583
Reading from 14827: heap size 1380 MB, throughput 0.959808
Reading from 14828: heap size 912 MB, throughput 0.999006
Reading from 14828: heap size 923 MB, throughput 0.998715
Reading from 14827: heap size 1386 MB, throughput 0.953847
Reading from 14828: heap size 923 MB, throughput 0.998819
Reading from 14828: heap size 934 MB, throughput 0.998467
Reading from 14827: heap size 1391 MB, throughput 0.955465
Reading from 14828: heap size 934 MB, throughput 0.998811
Reading from 14828: heap size 946 MB, throughput 0.998654
Equal recommendation: 1828 MB each
Reading from 14827: heap size 1391 MB, throughput 0.951868
Reading from 14828: heap size 900 MB, throughput 0.999233
Reading from 14828: heap size 854 MB, throughput 0.999373
Reading from 14828: heap size 813 MB, throughput 0.999089
Reading from 14827: heap size 1397 MB, throughput 0.944348
Reading from 14828: heap size 773 MB, throughput 0.999125
Reading from 14828: heap size 736 MB, throughput 0.998755
Reading from 14828: heap size 703 MB, throughput 0.998537
Reading from 14827: heap size 1403 MB, throughput 0.941662
Reading from 14828: heap size 669 MB, throughput 0.986123
Reading from 14828: heap size 639 MB, throughput 0.99886
Reading from 14828: heap size 648 MB, throughput 0.99878
Equal recommendation: 1828 MB each
Reading from 14828: heap size 658 MB, throughput 0.998984
Reading from 14828: heap size 668 MB, throughput 0.999049
Reading from 14828: heap size 678 MB, throughput 0.998897
Reading from 14828: heap size 688 MB, throughput 0.998835
Reading from 14828: heap size 698 MB, throughput 0.998991
Reading from 14828: heap size 708 MB, throughput 0.998471
Reading from 14828: heap size 721 MB, throughput 0.998443
Reading from 14828: heap size 734 MB, throughput 0.998864
Reading from 14828: heap size 745 MB, throughput 0.998421
Equal recommendation: 1828 MB each
Reading from 14828: heap size 758 MB, throughput 0.998984
Reading from 14828: heap size 771 MB, throughput 0.998828
Reading from 14828: heap size 784 MB, throughput 0.998508
Reading from 14828: heap size 798 MB, throughput 0.998802
Reading from 14828: heap size 812 MB, throughput 0.998591
Reading from 14827: heap size 1411 MB, throughput 0.988809
Reading from 14828: heap size 826 MB, throughput 0.998905
Reading from 14828: heap size 840 MB, throughput 0.99901
Reading from 14828: heap size 853 MB, throughput 0.998678
Reading from 14828: heap size 867 MB, throughput 0.999043
Equal recommendation: 1828 MB each
Reading from 14828: heap size 880 MB, throughput 0.999064
Reading from 14828: heap size 895 MB, throughput 0.999142
Reading from 14828: heap size 896 MB, throughput 0.998991
Reading from 14828: heap size 896 MB, throughput 0.998807
Reading from 14828: heap size 909 MB, throughput 0.998767
Reading from 14828: heap size 909 MB, throughput 0.998614
Reading from 14828: heap size 923 MB, throughput 0.998831
Equal recommendation: 1828 MB each
Reading from 14828: heap size 923 MB, throughput 0.998642
Reading from 14827: heap size 1419 MB, throughput 0.987207
Reading from 14828: heap size 939 MB, throughput 0.998723
Reading from 14828: heap size 939 MB, throughput 0.998353
Reading from 14827: heap size 1424 MB, throughput 0.365732
Reading from 14827: heap size 1465 MB, throughput 0.90242
Reading from 14827: heap size 1483 MB, throughput 0.862211
Reading from 14827: heap size 1488 MB, throughput 0.869104
Reading from 14827: heap size 1490 MB, throughput 0.8851
Reading from 14828: heap size 955 MB, throughput 0.998777
Reading from 14827: heap size 1495 MB, throughput 0.885181
Reading from 14828: heap size 955 MB, throughput 0.998829
Reading from 14827: heap size 1497 MB, throughput 0.983041
Reading from 14828: heap size 972 MB, throughput 0.998847
Reading from 14828: heap size 973 MB, throughput 0.998916
Equal recommendation: 1828 MB each
Reading from 14827: heap size 1504 MB, throughput 0.983677
Reading from 14828: heap size 989 MB, throughput 0.998639
Reading from 14828: heap size 990 MB, throughput 0.998781
Reading from 14827: heap size 1527 MB, throughput 0.983821
Reading from 14828: heap size 1007 MB, throughput 0.999103
Reading from 14828: heap size 1007 MB, throughput 0.998776
Reading from 14827: heap size 1529 MB, throughput 0.978919
Reading from 14828: heap size 1024 MB, throughput 0.998992
Reading from 14828: heap size 1024 MB, throughput 0.999012
Equal recommendation: 1828 MB each
Reading from 14828: heap size 1040 MB, throughput 0.998827
Reading from 14827: heap size 1524 MB, throughput 0.97853
Reading from 14828: heap size 1040 MB, throughput 0.998937
Reading from 14828: heap size 1056 MB, throughput 0.999087
Reading from 14827: heap size 1532 MB, throughput 0.968222
Reading from 14828: heap size 1057 MB, throughput 0.998938
Reading from 14828: heap size 1073 MB, throughput 0.999044
Reading from 14827: heap size 1513 MB, throughput 0.967116
Reading from 14828: heap size 1073 MB, throughput 0.998897
Equal recommendation: 1828 MB each
Reading from 14828: heap size 1089 MB, throughput 0.998866
Reading from 14827: heap size 1524 MB, throughput 0.962457
Reading from 14828: heap size 1089 MB, throughput 0.998801
Reading from 14828: heap size 1106 MB, throughput 0.998994
Reading from 14828: heap size 1106 MB, throughput 0.998897
Reading from 14827: heap size 1511 MB, throughput 0.962969
Reading from 14828: heap size 1124 MB, throughput 0.999193
Reading from 14828: heap size 1124 MB, throughput 0.998854
Reading from 14827: heap size 1518 MB, throughput 0.960211
Reading from 14828: heap size 1140 MB, throughput 0.999099
Equal recommendation: 1828 MB each
Reading from 14828: heap size 1140 MB, throughput 0.998857
Reading from 14827: heap size 1527 MB, throughput 0.957738
Reading from 14828: heap size 1157 MB, throughput 0.999267
Reading from 14828: heap size 1157 MB, throughput 0.99898
Reading from 14827: heap size 1529 MB, throughput 0.955114
Reading from 14828: heap size 1173 MB, throughput 0.999293
Reading from 14828: heap size 1173 MB, throughput 0.999089
Equal recommendation: 1828 MB each
Reading from 14827: heap size 1540 MB, throughput 0.947715
Reading from 14828: heap size 1189 MB, throughput 0.999147
Reading from 14828: heap size 1189 MB, throughput 0.99918
Reading from 14828: heap size 1205 MB, throughput 0.998951
Reading from 14827: heap size 1548 MB, throughput 0.950078
Reading from 14828: heap size 1205 MB, throughput 0.999137
Reading from 14828: heap size 1221 MB, throughput 0.998932
Reading from 14827: heap size 1561 MB, throughput 0.942102
Reading from 14828: heap size 1221 MB, throughput 0.998949
Equal recommendation: 1828 MB each
Reading from 14828: heap size 1233 MB, throughput 0.999223
Reading from 14827: heap size 1573 MB, throughput 0.941481
Reading from 14828: heap size 1233 MB, throughput 0.999192
Reading from 14828: heap size 1233 MB, throughput 0.999441
Reading from 14827: heap size 1591 MB, throughput 0.952496
Reading from 14828: heap size 1233 MB, throughput 0.99931
Reading from 14828: heap size 1233 MB, throughput 0.999309
Reading from 14827: heap size 1603 MB, throughput 0.945674
Equal recommendation: 1828 MB each
Reading from 14828: heap size 1233 MB, throughput 0.999423
Reading from 14828: heap size 1233 MB, throughput 0.999366
Reading from 14827: heap size 1622 MB, throughput 0.946616
Reading from 14828: heap size 1233 MB, throughput 0.999441
Reading from 14828: heap size 1233 MB, throughput 0.999273
Reading from 14827: heap size 1630 MB, throughput 0.949465
Reading from 14828: heap size 1233 MB, throughput 0.999322
Reading from 14828: heap size 1233 MB, throughput 0.999334
Equal recommendation: 1828 MB each
Reading from 14828: heap size 1233 MB, throughput 0.999408
Reading from 14827: heap size 1649 MB, throughput 0.954197
Reading from 14828: heap size 1233 MB, throughput 0.999457
Reading from 14828: heap size 1233 MB, throughput 0.999327
Reading from 14827: heap size 1654 MB, throughput 0.944349
Reading from 14828: heap size 1233 MB, throughput 0.99926
Reading from 14828: heap size 1233 MB, throughput 0.999383
Equal recommendation: 1828 MB each
Reading from 14827: heap size 1673 MB, throughput 0.950154
Reading from 14828: heap size 1233 MB, throughput 0.999366
Reading from 14828: heap size 1233 MB, throughput 0.999385
Reading from 14827: heap size 1675 MB, throughput 0.945606
Reading from 14828: heap size 1233 MB, throughput 0.999354
Reading from 14828: heap size 1233 MB, throughput 0.999242
Reading from 14828: heap size 1233 MB, throughput 0.999318
Equal recommendation: 1828 MB each
Reading from 14828: heap size 1233 MB, throughput 0.999468
Reading from 14827: heap size 1694 MB, throughput 0.688917
Reading from 14828: heap size 1233 MB, throughput 0.999481
Reading from 14828: heap size 1233 MB, throughput 0.999343
Reading from 14827: heap size 1832 MB, throughput 0.949879
Reading from 14828: heap size 1233 MB, throughput 0.999291
Reading from 14828: heap size 1233 MB, throughput 0.999363
Reading from 14828: heap size 1233 MB, throughput 0.999358
Equal recommendation: 1828 MB each
Reading from 14828: heap size 1233 MB, throughput 0.99937
Reading from 14828: heap size 1233 MB, throughput 0.999442
Reading from 14828: heap size 1233 MB, throughput 0.999238
Reading from 14828: heap size 1233 MB, throughput 0.9994
Reading from 14828: heap size 1233 MB, throughput 0.999334
Equal recommendation: 1828 MB each
Reading from 14828: heap size 1233 MB, throughput 0.999229
Reading from 14828: heap size 1233 MB, throughput 0.999266
Reading from 14828: heap size 1233 MB, throughput 0.999279
Reading from 14828: heap size 1233 MB, throughput 0.999333
Reading from 14828: heap size 1233 MB, throughput 0.999421
Reading from 14828: heap size 1233 MB, throughput 0.999365
Equal recommendation: 1828 MB each
Reading from 14828: heap size 1233 MB, throughput 0.999341
Client 14828 died
Clients: 1
Reading from 14827: heap size 1817 MB, throughput 0.996518
Recommendation: one client; give it all the memory
Reading from 14827: heap size 1835 MB, throughput 0.987712
Reading from 14827: heap size 1814 MB, throughput 0.872409
Reading from 14827: heap size 1828 MB, throughput 0.82543
Reading from 14827: heap size 1856 MB, throughput 0.852584
Client 14827 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
