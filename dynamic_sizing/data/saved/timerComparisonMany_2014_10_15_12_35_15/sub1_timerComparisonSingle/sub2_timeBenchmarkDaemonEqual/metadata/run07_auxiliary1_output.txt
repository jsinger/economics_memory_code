economemd
    total memory: 2974 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub1_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
TimerThread: starting
ReadingThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 28078: heap size 9 MB, throughput 0.992041
Clients: 1
Client 28078 has a minimum heap size of 1211 MB
Reading from 28080: heap size 9 MB, throughput 0.99163
Clients: 2
Client 28080 has a minimum heap size of 276 MB
Reading from 28078: heap size 9 MB, throughput 0.983471
Reading from 28080: heap size 9 MB, throughput 0.978893
Reading from 28080: heap size 9 MB, throughput 0.962915
Reading from 28078: heap size 9 MB, throughput 0.969193
Reading from 28078: heap size 9 MB, throughput 0.945452
Reading from 28080: heap size 9 MB, throughput 0.931271
Reading from 28080: heap size 11 MB, throughput 0.974033
Reading from 28078: heap size 11 MB, throughput 0.839433
Reading from 28080: heap size 11 MB, throughput 0.97949
Reading from 28078: heap size 11 MB, throughput 0.989929
Reading from 28080: heap size 17 MB, throughput 0.915378
Reading from 28078: heap size 17 MB, throughput 0.927585
Reading from 28080: heap size 17 MB, throughput 0.576572
Reading from 28078: heap size 17 MB, throughput 0.614028
Reading from 28080: heap size 30 MB, throughput 0.970852
Reading from 28078: heap size 30 MB, throughput 0.962706
Reading from 28080: heap size 31 MB, throughput 0.894362
Reading from 28078: heap size 31 MB, throughput 0.665868
Reading from 28078: heap size 35 MB, throughput 0.280257
Reading from 28080: heap size 33 MB, throughput 0.315749
Reading from 28080: heap size 47 MB, throughput 0.9046
Reading from 28078: heap size 44 MB, throughput 0.874837
Reading from 28080: heap size 48 MB, throughput 0.814388
Reading from 28078: heap size 49 MB, throughput 0.818758
Reading from 28080: heap size 49 MB, throughput 0.281327
Reading from 28080: heap size 68 MB, throughput 0.780182
Reading from 28078: heap size 51 MB, throughput 0.173803
Reading from 28080: heap size 69 MB, throughput 0.876561
Reading from 28078: heap size 69 MB, throughput 0.831703
Reading from 28080: heap size 71 MB, throughput 0.693127
Reading from 28080: heap size 74 MB, throughput 0.666771
Reading from 28078: heap size 71 MB, throughput 0.280038
Reading from 28078: heap size 95 MB, throughput 0.610417
Reading from 28080: heap size 76 MB, throughput 0.166176
Reading from 28078: heap size 97 MB, throughput 0.641928
Reading from 28080: heap size 102 MB, throughput 0.516113
Reading from 28080: heap size 107 MB, throughput 0.832432
Reading from 28080: heap size 107 MB, throughput 0.767689
Reading from 28078: heap size 100 MB, throughput 0.21042
Reading from 28080: heap size 111 MB, throughput 0.750591
Reading from 28078: heap size 126 MB, throughput 0.75616
Reading from 28080: heap size 115 MB, throughput 0.583613
Reading from 28078: heap size 132 MB, throughput 0.707621
Reading from 28078: heap size 134 MB, throughput 0.558503
Reading from 28078: heap size 138 MB, throughput 0.691125
Reading from 28080: heap size 119 MB, throughput 0.151888
Reading from 28078: heap size 144 MB, throughput 0.550736
Reading from 28080: heap size 147 MB, throughput 0.58966
Reading from 28080: heap size 153 MB, throughput 0.596138
Reading from 28078: heap size 148 MB, throughput 0.563303
Reading from 28080: heap size 155 MB, throughput 0.601791
Reading from 28080: heap size 160 MB, throughput 0.123441
Reading from 28078: heap size 154 MB, throughput 0.10928
Reading from 28080: heap size 193 MB, throughput 0.628706
Reading from 28078: heap size 187 MB, throughput 0.640965
Reading from 28080: heap size 197 MB, throughput 0.839906
Reading from 28080: heap size 199 MB, throughput 0.561596
Reading from 28078: heap size 192 MB, throughput 0.619567
Reading from 28078: heap size 195 MB, throughput 0.642788
Reading from 28078: heap size 200 MB, throughput 0.511555
Reading from 28080: heap size 202 MB, throughput 0.904655
Reading from 28078: heap size 207 MB, throughput 0.122175
Reading from 28080: heap size 206 MB, throughput 0.873966
Reading from 28078: heap size 245 MB, throughput 0.579849
Reading from 28080: heap size 210 MB, throughput 0.603957
Reading from 28078: heap size 253 MB, throughput 0.539915
Reading from 28080: heap size 218 MB, throughput 0.450774
Reading from 28078: heap size 254 MB, throughput 0.615425
Reading from 28080: heap size 223 MB, throughput 0.834912
Reading from 28078: heap size 259 MB, throughput 0.685611
Reading from 28080: heap size 228 MB, throughput 0.435713
Reading from 28078: heap size 262 MB, throughput 0.125549
Reading from 28080: heap size 233 MB, throughput 0.18922
Reading from 28078: heap size 303 MB, throughput 0.524662
Reading from 28080: heap size 268 MB, throughput 0.508208
Reading from 28078: heap size 306 MB, throughput 0.598503
Reading from 28080: heap size 263 MB, throughput 0.72391
Reading from 28078: heap size 307 MB, throughput 0.585603
Reading from 28080: heap size 267 MB, throughput 0.737594
Reading from 28078: heap size 310 MB, throughput 0.565159
Reading from 28080: heap size 267 MB, throughput 0.710495
Reading from 28078: heap size 312 MB, throughput 0.559679
Reading from 28080: heap size 267 MB, throughput 0.632924
Reading from 28080: heap size 269 MB, throughput 0.7185
Reading from 28078: heap size 321 MB, throughput 0.516992
Reading from 28080: heap size 270 MB, throughput 0.788472
Reading from 28080: heap size 268 MB, throughput 0.115434
Reading from 28078: heap size 327 MB, throughput 0.102251
Reading from 28078: heap size 373 MB, throughput 0.495276
Reading from 28078: heap size 383 MB, throughput 0.590483
Reading from 28078: heap size 384 MB, throughput 0.565468
Reading from 28080: heap size 308 MB, throughput 0.929838
Reading from 28078: heap size 386 MB, throughput 0.597389
Reading from 28080: heap size 308 MB, throughput 0.789249
Reading from 28080: heap size 310 MB, throughput 0.756864
Reading from 28078: heap size 393 MB, throughput 0.510184
Reading from 28080: heap size 312 MB, throughput 0.702825
Reading from 28078: heap size 399 MB, throughput 0.533074
Reading from 28080: heap size 313 MB, throughput 0.847555
Reading from 28080: heap size 314 MB, throughput 0.811881
Reading from 28080: heap size 316 MB, throughput 0.843176
Reading from 28080: heap size 319 MB, throughput 0.821481
Reading from 28078: heap size 403 MB, throughput 0.0877273
Reading from 28080: heap size 320 MB, throughput 0.688466
Reading from 28078: heap size 450 MB, throughput 0.465847
Reading from 28080: heap size 325 MB, throughput 0.554716
Reading from 28080: heap size 327 MB, throughput 0.606235
Equal recommendation: 1487 MB each
Reading from 28078: heap size 454 MB, throughput 0.602536
Reading from 28080: heap size 329 MB, throughput 0.720663
Reading from 28078: heap size 456 MB, throughput 0.589032
Reading from 28080: heap size 331 MB, throughput 0.697186
Reading from 28078: heap size 456 MB, throughput 0.086408
Reading from 28078: heap size 511 MB, throughput 0.388502
Reading from 28078: heap size 512 MB, throughput 0.567945
Reading from 28078: heap size 514 MB, throughput 0.567687
Reading from 28078: heap size 514 MB, throughput 0.557266
Reading from 28080: heap size 337 MB, throughput 0.958714
Reading from 28078: heap size 518 MB, throughput 0.587856
Reading from 28078: heap size 521 MB, throughput 0.506075
Reading from 28080: heap size 338 MB, throughput 0.962577
Reading from 28078: heap size 526 MB, throughput 0.092695
Reading from 28078: heap size 584 MB, throughput 0.440771
Reading from 28078: heap size 597 MB, throughput 0.612539
Reading from 28078: heap size 599 MB, throughput 0.586448
Reading from 28078: heap size 605 MB, throughput 0.530934
Reading from 28080: heap size 342 MB, throughput 0.647333
Reading from 28078: heap size 609 MB, throughput 0.0845169
Reading from 28078: heap size 673 MB, throughput 0.539241
Reading from 28078: heap size 678 MB, throughput 0.83928
Reading from 28080: heap size 386 MB, throughput 0.996642
Reading from 28078: heap size 680 MB, throughput 0.86929
Reading from 28078: heap size 685 MB, throughput 0.862452
Reading from 28080: heap size 391 MB, throughput 0.985815
Reading from 28078: heap size 687 MB, throughput 0.567575
Reading from 28080: heap size 393 MB, throughput 0.98548
Equal recommendation: 1487 MB each
Reading from 28078: heap size 702 MB, throughput 0.124019
Reading from 28078: heap size 775 MB, throughput 0.412266
Reading from 28078: heap size 780 MB, throughput 0.363716
Reading from 28080: heap size 397 MB, throughput 0.984181
Reading from 28078: heap size 771 MB, throughput 0.373381
Reading from 28078: heap size 692 MB, throughput 0.439607
Reading from 28080: heap size 399 MB, throughput 0.977406
Reading from 28078: heap size 765 MB, throughput 0.203346
Reading from 28078: heap size 840 MB, throughput 0.371675
Reading from 28078: heap size 839 MB, throughput 0.465045
Reading from 28080: heap size 397 MB, throughput 0.977877
Reading from 28078: heap size 840 MB, throughput 0.579
Reading from 28078: heap size 846 MB, throughput 0.855085
Reading from 28078: heap size 848 MB, throughput 0.785128
Reading from 28080: heap size 399 MB, throughput 0.973425
Reading from 28078: heap size 854 MB, throughput 0.230055
Reading from 28078: heap size 932 MB, throughput 0.731993
Reading from 28078: heap size 934 MB, throughput 0.886995
Reading from 28078: heap size 938 MB, throughput 0.924717
Reading from 28078: heap size 938 MB, throughput 0.840597
Reading from 28080: heap size 393 MB, throughput 0.98561
Reading from 28078: heap size 781 MB, throughput 0.858664
Reading from 28078: heap size 929 MB, throughput 0.820631
Reading from 28078: heap size 786 MB, throughput 0.854755
Reading from 28078: heap size 921 MB, throughput 0.878251
Reading from 28078: heap size 791 MB, throughput 0.841021
Reading from 28078: heap size 916 MB, throughput 0.85597
Reading from 28078: heap size 922 MB, throughput 0.871848
Reading from 28078: heap size 909 MB, throughput 0.873883
Reading from 28080: heap size 397 MB, throughput 0.984418
Reading from 28078: heap size 915 MB, throughput 0.89884
Reading from 28080: heap size 395 MB, throughput 0.982466
Equal recommendation: 1487 MB each
Reading from 28078: heap size 902 MB, throughput 0.981328
Reading from 28078: heap size 797 MB, throughput 0.917012
Reading from 28078: heap size 895 MB, throughput 0.791362
Reading from 28078: heap size 825 MB, throughput 0.758265
Reading from 28078: heap size 889 MB, throughput 0.78598
Reading from 28078: heap size 895 MB, throughput 0.806532
Reading from 28080: heap size 396 MB, throughput 0.986736
Reading from 28078: heap size 888 MB, throughput 0.777668
Reading from 28078: heap size 893 MB, throughput 0.82134
Reading from 28078: heap size 887 MB, throughput 0.803462
Reading from 28078: heap size 892 MB, throughput 0.912715
Reading from 28080: heap size 398 MB, throughput 0.936722
Reading from 28080: heap size 398 MB, throughput 0.743589
Reading from 28080: heap size 395 MB, throughput 0.792375
Reading from 28078: heap size 894 MB, throughput 0.910832
Reading from 28080: heap size 404 MB, throughput 0.817636
Reading from 28080: heap size 412 MB, throughput 0.828008
Reading from 28078: heap size 896 MB, throughput 0.915231
Reading from 28080: heap size 417 MB, throughput 0.981306
Reading from 28078: heap size 898 MB, throughput 0.0794044
Reading from 28078: heap size 992 MB, throughput 0.39229
Reading from 28080: heap size 414 MB, throughput 0.977982
Reading from 28078: heap size 1012 MB, throughput 0.678549
Reading from 28078: heap size 1019 MB, throughput 0.654526
Reading from 28078: heap size 1029 MB, throughput 0.734855
Reading from 28078: heap size 1033 MB, throughput 0.729716
Reading from 28078: heap size 1033 MB, throughput 0.729678
Reading from 28078: heap size 1038 MB, throughput 0.713453
Reading from 28078: heap size 1047 MB, throughput 0.712516
Reading from 28078: heap size 1048 MB, throughput 0.694226
Reading from 28080: heap size 418 MB, throughput 0.984605
Reading from 28078: heap size 1064 MB, throughput 0.924466
Reading from 28080: heap size 415 MB, throughput 0.977887
Equal recommendation: 1487 MB each
Reading from 28078: heap size 1065 MB, throughput 0.959134
Reading from 28080: heap size 419 MB, throughput 0.985037
Reading from 28078: heap size 1085 MB, throughput 0.953849
Reading from 28080: heap size 415 MB, throughput 0.983697
Reading from 28078: heap size 1087 MB, throughput 0.95404
Reading from 28080: heap size 418 MB, throughput 0.982429
Reading from 28080: heap size 417 MB, throughput 0.982522
Reading from 28078: heap size 1093 MB, throughput 0.958042
Reading from 28080: heap size 419 MB, throughput 0.984063
Reading from 28078: heap size 1097 MB, throughput 0.953207
Reading from 28080: heap size 420 MB, throughput 0.982292
Equal recommendation: 1487 MB each
Reading from 28078: heap size 1091 MB, throughput 0.958803
Reading from 28080: heap size 420 MB, throughput 0.978846
Reading from 28078: heap size 1098 MB, throughput 0.950135
Reading from 28080: heap size 422 MB, throughput 0.989541
Reading from 28080: heap size 422 MB, throughput 0.91633
Reading from 28080: heap size 423 MB, throughput 0.838024
Reading from 28080: heap size 425 MB, throughput 0.843796
Reading from 28078: heap size 1095 MB, throughput 0.946079
Reading from 28080: heap size 431 MB, throughput 0.959036
Reading from 28080: heap size 433 MB, throughput 0.970273
Reading from 28078: heap size 1098 MB, throughput 0.949044
Reading from 28080: heap size 438 MB, throughput 0.988896
Reading from 28078: heap size 1105 MB, throughput 0.950194
Equal recommendation: 1487 MB each
Reading from 28080: heap size 439 MB, throughput 0.987517
Reading from 28078: heap size 1106 MB, throughput 0.956843
Reading from 28080: heap size 439 MB, throughput 0.988502
Reading from 28078: heap size 1113 MB, throughput 0.955255
Reading from 28080: heap size 442 MB, throughput 0.985399
Reading from 28078: heap size 1116 MB, throughput 0.950702
Reading from 28080: heap size 439 MB, throughput 0.984197
Reading from 28078: heap size 1122 MB, throughput 0.953051
Reading from 28080: heap size 442 MB, throughput 0.985767
Reading from 28078: heap size 1126 MB, throughput 0.953148
Reading from 28080: heap size 442 MB, throughput 0.985853
Equal recommendation: 1487 MB each
Reading from 28078: heap size 1133 MB, throughput 0.95429
Reading from 28080: heap size 443 MB, throughput 0.980973
Reading from 28080: heap size 443 MB, throughput 0.986719
Reading from 28080: heap size 447 MB, throughput 0.896796
Reading from 28080: heap size 448 MB, throughput 0.78288
Reading from 28080: heap size 449 MB, throughput 0.94552
Reading from 28078: heap size 1138 MB, throughput 0.523851
Reading from 28080: heap size 458 MB, throughput 0.991897
Reading from 28078: heap size 1241 MB, throughput 0.927908
Reading from 28080: heap size 459 MB, throughput 0.990664
Equal recommendation: 1487 MB each
Reading from 28078: heap size 1245 MB, throughput 0.963076
Reading from 28080: heap size 460 MB, throughput 0.990429
Reading from 28078: heap size 1255 MB, throughput 0.970292
Reading from 28080: heap size 463 MB, throughput 0.98805
Reading from 28078: heap size 1255 MB, throughput 0.969083
Reading from 28080: heap size 461 MB, throughput 0.988106
Reading from 28078: heap size 1253 MB, throughput 0.964689
Reading from 28080: heap size 464 MB, throughput 0.986169
Reading from 28078: heap size 1257 MB, throughput 0.965816
Equal recommendation: 1487 MB each
Reading from 28080: heap size 465 MB, throughput 0.987934
Reading from 28078: heap size 1249 MB, throughput 0.964339
Reading from 28080: heap size 465 MB, throughput 0.982841
Reading from 28080: heap size 466 MB, throughput 0.984689
Reading from 28080: heap size 469 MB, throughput 0.893066
Reading from 28080: heap size 469 MB, throughput 0.887267
Reading from 28078: heap size 1254 MB, throughput 0.965567
Reading from 28080: heap size 471 MB, throughput 0.988034
Reading from 28078: heap size 1252 MB, throughput 0.959905
Reading from 28080: heap size 477 MB, throughput 0.990937
Reading from 28078: heap size 1254 MB, throughput 0.961736
Equal recommendation: 1487 MB each
Reading from 28080: heap size 478 MB, throughput 0.990908
Reading from 28078: heap size 1260 MB, throughput 0.961952
Reading from 28080: heap size 480 MB, throughput 0.988339
Reading from 28080: heap size 482 MB, throughput 0.988062
Reading from 28078: heap size 1261 MB, throughput 0.959742
Reading from 28080: heap size 479 MB, throughput 0.987625
Reading from 28078: heap size 1267 MB, throughput 0.966084
Reading from 28080: heap size 482 MB, throughput 0.987821
Equal recommendation: 1487 MB each
Reading from 28078: heap size 1272 MB, throughput 0.955707
Reading from 28080: heap size 484 MB, throughput 0.992121
Reading from 28078: heap size 1278 MB, throughput 0.955454
Reading from 28080: heap size 484 MB, throughput 0.975865
Reading from 28080: heap size 484 MB, throughput 0.885411
Reading from 28080: heap size 486 MB, throughput 0.9154
Reading from 28078: heap size 1286 MB, throughput 0.958044
Reading from 28080: heap size 493 MB, throughput 0.992576
Reading from 28078: heap size 1296 MB, throughput 0.950774
Reading from 28080: heap size 494 MB, throughput 0.991544
Equal recommendation: 1487 MB each
Reading from 28078: heap size 1302 MB, throughput 0.956105
Reading from 28080: heap size 496 MB, throughput 0.990576
Reading from 28080: heap size 498 MB, throughput 0.988159
Reading from 28078: heap size 1312 MB, throughput 0.944348
Reading from 28080: heap size 496 MB, throughput 0.985465
Reading from 28078: heap size 1316 MB, throughput 0.959108
Reading from 28080: heap size 498 MB, throughput 0.988061
Equal recommendation: 1487 MB each
Reading from 28078: heap size 1327 MB, throughput 0.545495
Reading from 28080: heap size 501 MB, throughput 0.994058
Reading from 28080: heap size 502 MB, throughput 0.963426
Reading from 28080: heap size 501 MB, throughput 0.903449
Reading from 28078: heap size 1441 MB, throughput 0.944495
Reading from 28080: heap size 504 MB, throughput 0.990851
Reading from 28080: heap size 510 MB, throughput 0.993029
Reading from 28078: heap size 1450 MB, throughput 0.970565
Equal recommendation: 1487 MB each
Reading from 28080: heap size 511 MB, throughput 0.99084
Reading from 28080: heap size 512 MB, throughput 0.990823
Reading from 28080: heap size 513 MB, throughput 0.988831
Reading from 28080: heap size 512 MB, throughput 0.98886
Equal recommendation: 1487 MB each
Reading from 28080: heap size 514 MB, throughput 0.986333
Reading from 28080: heap size 516 MB, throughput 0.98707
Reading from 28080: heap size 517 MB, throughput 0.898821
Reading from 28080: heap size 521 MB, throughput 0.983157
Reading from 28080: heap size 523 MB, throughput 0.993069
Equal recommendation: 1487 MB each
Reading from 28080: heap size 526 MB, throughput 0.992097
Reading from 28080: heap size 527 MB, throughput 0.991089
Reading from 28080: heap size 526 MB, throughput 0.990269
Reading from 28078: heap size 1452 MB, throughput 0.994778
Reading from 28080: heap size 528 MB, throughput 0.988739
Equal recommendation: 1487 MB each
Reading from 28080: heap size 530 MB, throughput 0.99242
Reading from 28080: heap size 530 MB, throughput 0.974095
Reading from 28080: heap size 529 MB, throughput 0.914067
Reading from 28078: heap size 1454 MB, throughput 0.983141
Reading from 28080: heap size 532 MB, throughput 0.989503
Reading from 28078: heap size 1460 MB, throughput 0.820936
Reading from 28078: heap size 1460 MB, throughput 0.722376
Reading from 28078: heap size 1487 MB, throughput 0.685587
Reading from 28078: heap size 1524 MB, throughput 0.713782
Reading from 28078: heap size 1541 MB, throughput 0.711679
Reading from 28078: heap size 1545 MB, throughput 0.746789
Reading from 28080: heap size 537 MB, throughput 0.992545
Reading from 28078: heap size 1554 MB, throughput 0.919753
Equal recommendation: 1487 MB each
Reading from 28080: heap size 538 MB, throughput 0.982518
Reading from 28078: heap size 1535 MB, throughput 0.958414
Reading from 28078: heap size 1375 MB, throughput 0.956895
Reading from 28080: heap size 538 MB, throughput 0.991055
Reading from 28078: heap size 1546 MB, throughput 0.965647
Reading from 28080: heap size 540 MB, throughput 0.989121
Reading from 28078: heap size 1391 MB, throughput 0.966599
Equal recommendation: 1487 MB each
Reading from 28080: heap size 541 MB, throughput 0.988815
Reading from 28078: heap size 1528 MB, throughput 0.955279
Reading from 28080: heap size 542 MB, throughput 0.98893
Reading from 28080: heap size 543 MB, throughput 0.918209
Reading from 28080: heap size 546 MB, throughput 0.98459
Reading from 28078: heap size 1402 MB, throughput 0.952954
Reading from 28080: heap size 552 MB, throughput 0.988549
Reading from 28078: heap size 1514 MB, throughput 0.739911
Equal recommendation: 1487 MB each
Reading from 28078: heap size 1336 MB, throughput 0.990942
Reading from 28080: heap size 553 MB, throughput 0.991687
Reading from 28078: heap size 1465 MB, throughput 0.991302
Reading from 28080: heap size 553 MB, throughput 0.923566
Reading from 28078: heap size 1480 MB, throughput 0.982549
Reading from 28080: heap size 574 MB, throughput 0.995505
Reading from 28078: heap size 1497 MB, throughput 0.987447
Equal recommendation: 1487 MB each
Reading from 28080: heap size 574 MB, throughput 0.99372
Reading from 28078: heap size 1259 MB, throughput 0.979365
Reading from 28080: heap size 576 MB, throughput 0.986976
Reading from 28080: heap size 577 MB, throughput 0.916411
Reading from 28078: heap size 1496 MB, throughput 0.976767
Reading from 28080: heap size 580 MB, throughput 0.989684
Reading from 28078: heap size 1278 MB, throughput 0.973374
Reading from 28080: heap size 585 MB, throughput 0.993133
Equal recommendation: 1487 MB each
Reading from 28078: heap size 1477 MB, throughput 0.978374
Reading from 28080: heap size 587 MB, throughput 0.993099
Reading from 28078: heap size 1305 MB, throughput 0.976616
Reading from 28080: heap size 586 MB, throughput 0.990771
Reading from 28078: heap size 1451 MB, throughput 0.967967
Reading from 28078: heap size 1331 MB, throughput 0.966972
Reading from 28080: heap size 588 MB, throughput 0.989515
Equal recommendation: 1487 MB each
Reading from 28078: heap size 1442 MB, throughput 0.965386
Reading from 28080: heap size 592 MB, throughput 0.989278
Reading from 28080: heap size 593 MB, throughput 0.957595
Reading from 28080: heap size 596 MB, throughput 0.937011
Reading from 28078: heap size 1451 MB, throughput 0.963446
Reading from 28080: heap size 599 MB, throughput 0.992983
Reading from 28078: heap size 1456 MB, throughput 0.966592
Equal recommendation: 1487 MB each
Reading from 28080: heap size 601 MB, throughput 0.992338
Reading from 28078: heap size 1456 MB, throughput 0.961223
Reading from 28080: heap size 603 MB, throughput 0.982483
Reading from 28078: heap size 1465 MB, throughput 0.954404
Reading from 28078: heap size 1469 MB, throughput 0.963499
Reading from 28080: heap size 606 MB, throughput 0.990235
Equal recommendation: 1487 MB each
Reading from 28078: heap size 1483 MB, throughput 0.957707
Reading from 28080: heap size 607 MB, throughput 0.98798
Reading from 28080: heap size 610 MB, throughput 0.985227
Reading from 28080: heap size 612 MB, throughput 0.906006
Reading from 28078: heap size 1485 MB, throughput 0.964745
Reading from 28080: heap size 618 MB, throughput 0.994236
Reading from 28078: heap size 1499 MB, throughput 0.96664
Equal recommendation: 1487 MB each
Reading from 28078: heap size 1429 MB, throughput 0.957347
Reading from 28080: heap size 619 MB, throughput 0.993108
Reading from 28078: heap size 1496 MB, throughput 0.954671
Reading from 28080: heap size 621 MB, throughput 0.992261
Reading from 28078: heap size 1427 MB, throughput 0.954003
Reading from 28080: heap size 624 MB, throughput 0.991121
Equal recommendation: 1487 MB each
Reading from 28078: heap size 1492 MB, throughput 0.955584
Reading from 28080: heap size 625 MB, throughput 0.994488
Reading from 28080: heap size 626 MB, throughput 0.97421
Client 28080 died
Clients: 1
Reading from 28078: heap size 1424 MB, throughput 0.966775
Reading from 28078: heap size 1488 MB, throughput 0.966076
Recommendation: one client; give it all the memory
Reading from 28078: heap size 1422 MB, throughput 0.967394
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 28078: heap size 1482 MB, throughput 0.99654
Recommendation: one client; give it all the memory
Reading from 28078: heap size 1482 MB, throughput 0.985637
Reading from 28078: heap size 1474 MB, throughput 0.419855
Reading from 28078: heap size 1554 MB, throughput 0.994334
Reading from 28078: heap size 1589 MB, throughput 0.994626
Reading from 28078: heap size 1605 MB, throughput 0.994404
Reading from 28078: heap size 1624 MB, throughput 0.994724
Client 28078 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
