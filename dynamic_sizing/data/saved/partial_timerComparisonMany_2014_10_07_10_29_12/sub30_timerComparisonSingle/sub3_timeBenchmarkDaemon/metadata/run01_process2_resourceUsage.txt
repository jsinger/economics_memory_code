	Command being timed: "cgexec -g memory:economem /home/callum/economics_memory_code/dynamic_sizing/hotspot8/openjdk8/jdk8/build/linux-x86_64-normal-server-release/jdk/bin/java -XX:+EconomemVengerovThroughput -XX:EconomemMinHeap=8 -Xms10m -Xmx568m -XX:+DisableExplicitGC -XX:+UseAdaptiveSizePolicyWithSystemGC -jar /home/callum/economics_memory_code/dynamic_sizing/haskell_common/dacapo-9.12-bach.jar --no-pre-iteration-gc --scratch-directory /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub30_timerComparisonSingle/sub3_timeBenchmarkDaemon/scratch2 -t 2 -n 32 -s large sunflow"
	User time (seconds): 618.20
	System time (seconds): 10.41
	Percent of CPU this job got: 133%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 7:49.57
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 250752
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 22428
	Minor (reclaiming a frame) page faults: 233659
	Voluntary context switches: 69758
	Involuntary context switches: 73676
	Swaps: 0
	File system inputs: 1344776
	File system outputs: 18568
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
