#!/usr/bin/env python

import os.path
import staticutility as su
import shutil
import defaultUtilityPostprocessAll

defaultUtilityPostprocessAll.postprocessAll()

defaultUtilityDirs = su.allDefaultUtilityDirs()

utilitySpaceDirs = []
for d in su.allUtilitySpaceDirs():
    utilitySpaceDirs.append((d, d.parseBenchmarksFile()))


for d in defaultUtilityDirs:
    bms = d.parseBenchmarksFile()
    match = None
    for (ud, ubms) in utilitySpaceDirs:
        if ubms == bms:
            match = (ud, ubms)
            break

    if match:
        print "%s (%s) matches %s (%s)" % (os.path.basename(str(d)), bms, os.path.basename(str(match[0])), match[1])
        for f in d.globResultsFiles():
            shutil.copy(f, str(match[0]))
            print "Imported %s" % f
        print
    else:
        print "%s (%s) NO MATCH\n" % (os.path.basename(str(d)), bms)
