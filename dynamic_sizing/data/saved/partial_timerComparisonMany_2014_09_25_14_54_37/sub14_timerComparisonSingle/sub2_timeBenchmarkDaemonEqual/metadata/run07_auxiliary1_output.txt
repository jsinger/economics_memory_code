economemd
    total memory: 2446 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub14_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 23549: heap size 9 MB, throughput 0.988748
Clients: 1
Client 23549 has a minimum heap size of 12 MB
Reading from 23548: heap size 9 MB, throughput 0.98662
Clients: 2
Client 23548 has a minimum heap size of 1211 MB
Reading from 23548: heap size 9 MB, throughput 0.957773
Reading from 23549: heap size 9 MB, throughput 0.991463
Reading from 23548: heap size 11 MB, throughput 0.975089
Reading from 23549: heap size 9 MB, throughput 0.951642
Reading from 23549: heap size 9 MB, throughput 0.974484
Reading from 23548: heap size 11 MB, throughput 0.979456
Reading from 23549: heap size 11 MB, throughput 0.98335
Reading from 23549: heap size 11 MB, throughput 0.977374
Reading from 23548: heap size 15 MB, throughput 0.84826
Reading from 23549: heap size 16 MB, throughput 0.983219
Reading from 23548: heap size 18 MB, throughput 0.939468
Reading from 23548: heap size 23 MB, throughput 0.94094
Reading from 23548: heap size 27 MB, throughput 0.914092
Reading from 23549: heap size 16 MB, throughput 0.988302
Reading from 23548: heap size 27 MB, throughput 0.227167
Reading from 23548: heap size 37 MB, throughput 0.661268
Reading from 23548: heap size 40 MB, throughput 0.86533
Reading from 23548: heap size 42 MB, throughput 0.779858
Reading from 23549: heap size 23 MB, throughput 0.920653
Reading from 23548: heap size 45 MB, throughput 0.202176
Reading from 23548: heap size 59 MB, throughput 0.82327
Reading from 23548: heap size 66 MB, throughput 0.73488
Reading from 23549: heap size 29 MB, throughput 0.991883
Reading from 23548: heap size 67 MB, throughput 0.648812
Reading from 23548: heap size 71 MB, throughput 0.775678
Reading from 23548: heap size 75 MB, throughput 0.657538
Reading from 23549: heap size 33 MB, throughput 0.985689
Reading from 23549: heap size 38 MB, throughput 0.991303
Reading from 23548: heap size 79 MB, throughput 0.166977
Reading from 23548: heap size 99 MB, throughput 0.654862
Reading from 23549: heap size 38 MB, throughput 0.988971
Reading from 23548: heap size 104 MB, throughput 0.757982
Reading from 23548: heap size 108 MB, throughput 0.540394
Reading from 23549: heap size 38 MB, throughput 0.925568
Reading from 23548: heap size 112 MB, throughput 0.695294
Reading from 23549: heap size 44 MB, throughput 0.988218
Reading from 23549: heap size 43 MB, throughput 0.990576
Reading from 23548: heap size 118 MB, throughput 0.116537
Reading from 23548: heap size 144 MB, throughput 0.400108
Reading from 23549: heap size 51 MB, throughput 0.989437
Reading from 23548: heap size 148 MB, throughput 0.66373
Reading from 23549: heap size 52 MB, throughput 0.992986
Reading from 23549: heap size 57 MB, throughput 0.965587
Reading from 23548: heap size 151 MB, throughput 0.125179
Reading from 23549: heap size 58 MB, throughput 0.491102
Reading from 23548: heap size 177 MB, throughput 0.615096
Reading from 23548: heap size 181 MB, throughput 0.674407
Reading from 23549: heap size 71 MB, throughput 0.993583
Reading from 23548: heap size 182 MB, throughput 0.471081
Reading from 23549: heap size 70 MB, throughput 0.985071
Reading from 23548: heap size 184 MB, throughput 0.592308
Reading from 23548: heap size 189 MB, throughput 0.520277
Reading from 23549: heap size 80 MB, throughput 0.995996
Reading from 23548: heap size 195 MB, throughput 0.144902
Reading from 23548: heap size 228 MB, throughput 0.525428
Reading from 23548: heap size 235 MB, throughput 0.731319
Reading from 23548: heap size 237 MB, throughput 0.626603
Reading from 23548: heap size 240 MB, throughput 0.667729
Reading from 23548: heap size 245 MB, throughput 0.613358
Reading from 23549: heap size 81 MB, throughput 0.995522
Reading from 23548: heap size 248 MB, throughput 0.668163
Reading from 23548: heap size 257 MB, throughput 0.605488
Reading from 23548: heap size 260 MB, throughput 0.548636
Reading from 23549: heap size 89 MB, throughput 0.997607
Reading from 23548: heap size 267 MB, throughput 0.086718
Reading from 23548: heap size 305 MB, throughput 0.521302
Reading from 23548: heap size 309 MB, throughput 0.517803
Reading from 23549: heap size 90 MB, throughput 0.99539
Reading from 23548: heap size 307 MB, throughput 0.0869016
Reading from 23548: heap size 348 MB, throughput 0.547845
Reading from 23548: heap size 341 MB, throughput 0.613195
Reading from 23548: heap size 346 MB, throughput 0.584378
Reading from 23549: heap size 96 MB, throughput 0.996962
Reading from 23548: heap size 348 MB, throughput 0.521838
Reading from 23548: heap size 349 MB, throughput 0.575662
Reading from 23548: heap size 355 MB, throughput 0.565147
Reading from 23549: heap size 97 MB, throughput 0.995111
Reading from 23548: heap size 357 MB, throughput 0.105123
Reading from 23548: heap size 404 MB, throughput 0.592012
Reading from 23548: heap size 406 MB, throughput 0.608441
Reading from 23549: heap size 102 MB, throughput 0.994239
Reading from 23548: heap size 409 MB, throughput 0.674356
Reading from 23548: heap size 409 MB, throughput 0.638991
Reading from 23548: heap size 414 MB, throughput 0.650821
Reading from 23548: heap size 416 MB, throughput 0.509468
Equal recommendation: 1223 MB each
Reading from 23548: heap size 420 MB, throughput 0.546341
Reading from 23549: heap size 103 MB, throughput 0.996637
Reading from 23549: heap size 107 MB, throughput 0.995344
Reading from 23548: heap size 426 MB, throughput 0.0955057
Reading from 23548: heap size 478 MB, throughput 0.477805
Reading from 23548: heap size 484 MB, throughput 0.473571
Reading from 23548: heap size 489 MB, throughput 0.515982
Reading from 23549: heap size 108 MB, throughput 0.996503
Reading from 23548: heap size 493 MB, throughput 0.410391
Reading from 23549: heap size 112 MB, throughput 0.996547
Reading from 23548: heap size 498 MB, throughput 0.0827216
Reading from 23548: heap size 555 MB, throughput 0.498323
Reading from 23549: heap size 113 MB, throughput 0.996491
Reading from 23548: heap size 555 MB, throughput 0.545399
Reading from 23548: heap size 559 MB, throughput 0.576228
Reading from 23549: heap size 116 MB, throughput 0.995165
Reading from 23548: heap size 562 MB, throughput 0.11581
Reading from 23549: heap size 117 MB, throughput 0.995106
Reading from 23548: heap size 623 MB, throughput 0.540495
Reading from 23548: heap size 621 MB, throughput 0.579431
Reading from 23548: heap size 626 MB, throughput 0.613144
Reading from 23548: heap size 628 MB, throughput 0.585981
Reading from 23548: heap size 634 MB, throughput 0.592856
Reading from 23549: heap size 121 MB, throughput 0.996643
Reading from 23548: heap size 640 MB, throughput 0.887063
Reading from 23549: heap size 121 MB, throughput 0.997754
Reading from 23549: heap size 125 MB, throughput 0.997836
Reading from 23549: heap size 125 MB, throughput 0.997909
Reading from 23548: heap size 645 MB, throughput 0.275967
Reading from 23549: heap size 128 MB, throughput 0.998068
Reading from 23548: heap size 712 MB, throughput 0.81597
Reading from 23548: heap size 726 MB, throughput 0.522881
Reading from 23548: heap size 734 MB, throughput 0.625223
Reading from 23549: heap size 128 MB, throughput 0.997716
Reading from 23548: heap size 739 MB, throughput 0.436858
Reading from 23548: heap size 745 MB, throughput 0.410061
Reading from 23548: heap size 745 MB, throughput 0.418005
Equal recommendation: 1223 MB each
Reading from 23549: heap size 131 MB, throughput 0.998221
Reading from 23549: heap size 131 MB, throughput 0.997418
Reading from 23549: heap size 134 MB, throughput 0.997631
Reading from 23548: heap size 734 MB, throughput 0.0223746
Reading from 23549: heap size 134 MB, throughput 0.998297
Reading from 23548: heap size 817 MB, throughput 0.791498
Reading from 23548: heap size 816 MB, throughput 0.582569
Reading from 23548: heap size 819 MB, throughput 0.601717
Reading from 23548: heap size 818 MB, throughput 0.625795
Reading from 23548: heap size 820 MB, throughput 0.823381
Reading from 23549: heap size 136 MB, throughput 0.998165
Reading from 23549: heap size 137 MB, throughput 0.991478
Reading from 23549: heap size 138 MB, throughput 0.991745
Reading from 23549: heap size 139 MB, throughput 0.991358
Reading from 23548: heap size 826 MB, throughput 0.142751
Reading from 23548: heap size 911 MB, throughput 0.44071
Reading from 23548: heap size 913 MB, throughput 0.914675
Reading from 23549: heap size 144 MB, throughput 0.997055
Reading from 23548: heap size 919 MB, throughput 0.915905
Reading from 23548: heap size 925 MB, throughput 0.926157
Reading from 23548: heap size 930 MB, throughput 0.852742
Reading from 23548: heap size 920 MB, throughput 0.858888
Reading from 23549: heap size 145 MB, throughput 0.997008
Reading from 23548: heap size 777 MB, throughput 0.85913
Reading from 23548: heap size 911 MB, throughput 0.861418
Reading from 23548: heap size 781 MB, throughput 0.862028
Reading from 23548: heap size 903 MB, throughput 0.835435
Reading from 23549: heap size 150 MB, throughput 0.996901
Reading from 23548: heap size 910 MB, throughput 0.876073
Reading from 23548: heap size 898 MB, throughput 0.883522
Reading from 23548: heap size 786 MB, throughput 0.875219
Reading from 23548: heap size 892 MB, throughput 0.888973
Reading from 23549: heap size 150 MB, throughput 0.996925
Equal recommendation: 1223 MB each
Reading from 23549: heap size 154 MB, throughput 0.997483
Reading from 23548: heap size 790 MB, throughput 0.983295
Reading from 23549: heap size 155 MB, throughput 0.995832
Reading from 23548: heap size 885 MB, throughput 0.922521
Reading from 23549: heap size 159 MB, throughput 0.995277
Reading from 23548: heap size 818 MB, throughput 0.1164
Reading from 23548: heap size 967 MB, throughput 0.540199
Reading from 23548: heap size 973 MB, throughput 0.772551
Reading from 23549: heap size 159 MB, throughput 0.997073
Reading from 23548: heap size 967 MB, throughput 0.78784
Reading from 23548: heap size 971 MB, throughput 0.80945
Reading from 23548: heap size 966 MB, throughput 0.809172
Reading from 23548: heap size 970 MB, throughput 0.811371
Reading from 23549: heap size 164 MB, throughput 0.995124
Reading from 23548: heap size 967 MB, throughput 0.834058
Reading from 23548: heap size 971 MB, throughput 0.829006
Reading from 23548: heap size 976 MB, throughput 0.871332
Reading from 23548: heap size 976 MB, throughput 0.695986
Reading from 23549: heap size 164 MB, throughput 0.996825
Reading from 23548: heap size 976 MB, throughput 0.67962
Reading from 23549: heap size 168 MB, throughput 0.996772
Reading from 23548: heap size 984 MB, throughput 0.0798696
Reading from 23549: heap size 168 MB, throughput 0.996258
Reading from 23548: heap size 1132 MB, throughput 0.606782
Reading from 23548: heap size 1136 MB, throughput 0.808464
Reading from 23548: heap size 1149 MB, throughput 0.787433
Reading from 23548: heap size 1150 MB, throughput 0.748603
Reading from 23548: heap size 1158 MB, throughput 0.795425
Reading from 23548: heap size 1160 MB, throughput 0.776654
Reading from 23549: heap size 172 MB, throughput 0.997544
Reading from 23548: heap size 1163 MB, throughput 0.724311
Reading from 23548: heap size 1167 MB, throughput 0.72756
Reading from 23549: heap size 172 MB, throughput 0.997987
Equal recommendation: 1223 MB each
Reading from 23549: heap size 176 MB, throughput 0.99847
Reading from 23548: heap size 1167 MB, throughput 0.969486
Reading from 23549: heap size 176 MB, throughput 0.998529
Reading from 23549: heap size 180 MB, throughput 0.994324
Reading from 23548: heap size 1173 MB, throughput 0.936956
Reading from 23549: heap size 180 MB, throughput 0.993083
Reading from 23549: heap size 184 MB, throughput 0.997159
Reading from 23548: heap size 1175 MB, throughput 0.963378
Reading from 23549: heap size 185 MB, throughput 0.997458
Reading from 23549: heap size 189 MB, throughput 0.998201
Reading from 23548: heap size 1180 MB, throughput 0.950518
Reading from 23549: heap size 190 MB, throughput 0.997509
Reading from 23549: heap size 194 MB, throughput 0.996905
Reading from 23548: heap size 1191 MB, throughput 0.946047
Equal recommendation: 1223 MB each
Reading from 23549: heap size 194 MB, throughput 0.99725
Reading from 23548: heap size 1193 MB, throughput 0.9523
Reading from 23549: heap size 198 MB, throughput 0.997892
Reading from 23549: heap size 198 MB, throughput 0.997423
Reading from 23548: heap size 1198 MB, throughput 0.963125
Reading from 23549: heap size 203 MB, throughput 0.997789
Reading from 23549: heap size 203 MB, throughput 0.997322
Reading from 23548: heap size 1203 MB, throughput 0.961529
Reading from 23549: heap size 206 MB, throughput 0.997732
Reading from 23548: heap size 1205 MB, throughput 0.962435
Reading from 23549: heap size 196 MB, throughput 0.997629
Reading from 23549: heap size 187 MB, throughput 0.997674
Reading from 23548: heap size 1207 MB, throughput 0.961958
Reading from 23549: heap size 178 MB, throughput 0.997817
Equal recommendation: 1223 MB each
Reading from 23549: heap size 172 MB, throughput 0.806682
Reading from 23549: heap size 183 MB, throughput 0.992143
Reading from 23548: heap size 1214 MB, throughput 0.961722
Reading from 23549: heap size 189 MB, throughput 0.998287
Reading from 23549: heap size 196 MB, throughput 0.998298
Reading from 23548: heap size 1216 MB, throughput 0.959013
Reading from 23549: heap size 200 MB, throughput 0.998321
Reading from 23549: heap size 200 MB, throughput 0.998051
Reading from 23548: heap size 1222 MB, throughput 0.960386
Reading from 23549: heap size 206 MB, throughput 0.998315
Reading from 23549: heap size 206 MB, throughput 0.998054
Reading from 23548: heap size 1225 MB, throughput 0.962156
Reading from 23549: heap size 211 MB, throughput 0.99826
Equal recommendation: 1223 MB each
Reading from 23549: heap size 211 MB, throughput 0.997817
Reading from 23548: heap size 1226 MB, throughput 0.96147
Reading from 23549: heap size 216 MB, throughput 0.998009
Reading from 23548: heap size 1228 MB, throughput 0.959786
Reading from 23549: heap size 216 MB, throughput 0.997865
Reading from 23549: heap size 220 MB, throughput 0.998252
Reading from 23548: heap size 1229 MB, throughput 0.960677
Reading from 23549: heap size 221 MB, throughput 0.997843
Reading from 23549: heap size 226 MB, throughput 0.996896
Reading from 23549: heap size 226 MB, throughput 0.98934
Reading from 23548: heap size 1231 MB, throughput 0.950508
Reading from 23549: heap size 231 MB, throughput 0.996704
Reading from 23548: heap size 1232 MB, throughput 0.960532
Equal recommendation: 1223 MB each
Reading from 23549: heap size 232 MB, throughput 0.997878
Reading from 23549: heap size 239 MB, throughput 0.997989
Reading from 23548: heap size 1199 MB, throughput 0.95947
Reading from 23549: heap size 240 MB, throughput 0.997837
Reading from 23548: heap size 1233 MB, throughput 0.958952
Reading from 23549: heap size 247 MB, throughput 0.998046
Reading from 23549: heap size 247 MB, throughput 0.997539
Reading from 23548: heap size 1195 MB, throughput 0.95895
Reading from 23549: heap size 254 MB, throughput 0.99793
Reading from 23549: heap size 254 MB, throughput 0.997598
Equal recommendation: 1223 MB each
Reading from 23548: heap size 1233 MB, throughput 0.558287
Reading from 23549: heap size 260 MB, throughput 0.998038
Reading from 23549: heap size 260 MB, throughput 0.997729
Reading from 23548: heap size 1245 MB, throughput 0.992736
Reading from 23549: heap size 267 MB, throughput 0.997846
Reading from 23549: heap size 267 MB, throughput 0.991239
Reading from 23548: heap size 1271 MB, throughput 0.989825
Reading from 23549: heap size 273 MB, throughput 0.997491
Reading from 23548: heap size 1280 MB, throughput 0.987069
Reading from 23549: heap size 273 MB, throughput 0.997777
Reading from 23548: heap size 1284 MB, throughput 0.984591
Reading from 23549: heap size 282 MB, throughput 0.997923
Equal recommendation: 1223 MB each
Reading from 23548: heap size 1147 MB, throughput 0.980543
Reading from 23549: heap size 282 MB, throughput 0.997862
Reading from 23548: heap size 1283 MB, throughput 0.978878
Reading from 23549: heap size 289 MB, throughput 0.99835
Reading from 23549: heap size 290 MB, throughput 0.997978
Reading from 23548: heap size 1162 MB, throughput 0.977177
Reading from 23549: heap size 297 MB, throughput 0.998276
Reading from 23548: heap size 1270 MB, throughput 0.975305
Reading from 23549: heap size 297 MB, throughput 0.998005
Reading from 23548: heap size 1177 MB, throughput 0.972497
Equal recommendation: 1223 MB each
Reading from 23549: heap size 304 MB, throughput 0.998206
Reading from 23549: heap size 304 MB, throughput 0.992744
Reading from 23548: heap size 1255 MB, throughput 0.964699
Reading from 23549: heap size 310 MB, throughput 0.997499
Reading from 23548: heap size 1192 MB, throughput 0.970784
Reading from 23549: heap size 311 MB, throughput 0.997934
Reading from 23548: heap size 1255 MB, throughput 0.96849
Reading from 23549: heap size 320 MB, throughput 0.9981
Reading from 23548: heap size 1206 MB, throughput 0.965285
Reading from 23549: heap size 321 MB, throughput 0.9977
Reading from 23548: heap size 1248 MB, throughput 0.960973
Equal recommendation: 1223 MB each
Reading from 23549: heap size 328 MB, throughput 0.998104
Reading from 23548: heap size 1249 MB, throughput 0.963035
Reading from 23549: heap size 329 MB, throughput 0.998262
Reading from 23548: heap size 1247 MB, throughput 0.958755
Reading from 23549: heap size 336 MB, throughput 0.998292
Reading from 23548: heap size 1250 MB, throughput 0.961849
Reading from 23549: heap size 337 MB, throughput 0.998255
Reading from 23549: heap size 344 MB, throughput 0.91365
Reading from 23548: heap size 1252 MB, throughput 0.959284
Reading from 23549: heap size 351 MB, throughput 0.998675
Reading from 23548: heap size 1207 MB, throughput 0.958749
Equal recommendation: 1223 MB each
Reading from 23549: heap size 363 MB, throughput 0.998344
Reading from 23548: heap size 1253 MB, throughput 0.958802
Reading from 23548: heap size 1205 MB, throughput 0.95964
Reading from 23549: heap size 363 MB, throughput 0.998927
Reading from 23548: heap size 1252 MB, throughput 0.960892
Reading from 23549: heap size 372 MB, throughput 0.99897
Reading from 23548: heap size 1202 MB, throughput 0.959595
Reading from 23549: heap size 373 MB, throughput 0.998956
Equal recommendation: 1223 MB each
Reading from 23548: heap size 1249 MB, throughput 0.959111
Reading from 23549: heap size 381 MB, throughput 0.998966
Reading from 23549: heap size 382 MB, throughput 0.998997
Reading from 23548: heap size 1202 MB, throughput 0.969068
Reading from 23549: heap size 389 MB, throughput 0.994304
Reading from 23549: heap size 389 MB, throughput 0.998439
Reading from 23549: heap size 402 MB, throughput 0.998476
Equal recommendation: 1223 MB each
Reading from 23549: heap size 403 MB, throughput 0.998507
Reading from 23549: heap size 413 MB, throughput 0.998591
Reading from 23549: heap size 413 MB, throughput 0.998026
Reading from 23549: heap size 423 MB, throughput 0.997264
Equal recommendation: 1223 MB each
Reading from 23549: heap size 423 MB, throughput 0.992468
Reading from 23548: heap size 1242 MB, throughput 0.846882
Reading from 23549: heap size 433 MB, throughput 0.998359
Reading from 23549: heap size 435 MB, throughput 0.99829
Reading from 23549: heap size 448 MB, throughput 0.998356
Equal recommendation: 1223 MB each
Reading from 23549: heap size 450 MB, throughput 0.998435
Reading from 23548: heap size 1390 MB, throughput 0.987513
Reading from 23549: heap size 462 MB, throughput 0.998295
Reading from 23549: heap size 463 MB, throughput 0.998025
Reading from 23549: heap size 474 MB, throughput 0.996801
Reading from 23548: heap size 1410 MB, throughput 0.989139
Equal recommendation: 1223 MB each
Reading from 23548: heap size 1367 MB, throughput 0.899045
Reading from 23548: heap size 1407 MB, throughput 0.779813
Reading from 23548: heap size 1410 MB, throughput 0.684849
Reading from 23548: heap size 1426 MB, throughput 0.671726
Reading from 23548: heap size 1435 MB, throughput 0.711986
Reading from 23548: heap size 1437 MB, throughput 0.704691
Reading from 23548: heap size 1442 MB, throughput 0.70986
Reading from 23549: heap size 475 MB, throughput 0.995212
Reading from 23548: heap size 1443 MB, throughput 0.701164
Reading from 23548: heap size 1379 MB, throughput 0.715898
Reading from 23548: heap size 1438 MB, throughput 0.711055
Reading from 23548: heap size 1359 MB, throughput 0.184806
Reading from 23548: heap size 1269 MB, throughput 0.986895
Reading from 23549: heap size 491 MB, throughput 0.998647
Reading from 23548: heap size 1283 MB, throughput 0.988737
Reading from 23548: heap size 1288 MB, throughput 0.985775
Reading from 23549: heap size 490 MB, throughput 0.998429
Reading from 23548: heap size 1063 MB, throughput 0.984284
Equal recommendation: 1223 MB each
Reading from 23548: heap size 1294 MB, throughput 0.98273
Reading from 23549: heap size 503 MB, throughput 0.998601
Reading from 23548: heap size 1073 MB, throughput 0.980222
Reading from 23548: heap size 1289 MB, throughput 0.980189
Reading from 23549: heap size 505 MB, throughput 0.997863
Reading from 23548: heap size 1089 MB, throughput 0.976038
Reading from 23549: heap size 518 MB, throughput 0.997826
Reading from 23548: heap size 1272 MB, throughput 0.974577
Reading from 23548: heap size 1105 MB, throughput 0.973104
Equal recommendation: 1223 MB each
Reading from 23549: heap size 519 MB, throughput 0.998154
Reading from 23548: heap size 1254 MB, throughput 0.970882
Reading from 23548: heap size 1121 MB, throughput 0.970065
Reading from 23549: heap size 533 MB, throughput 0.998385
Reading from 23548: heap size 1238 MB, throughput 0.969831
Reading from 23548: heap size 1136 MB, throughput 0.96668
Reading from 23549: heap size 533 MB, throughput 0.99846
Reading from 23548: heap size 1229 MB, throughput 0.966891
Reading from 23548: heap size 1151 MB, throughput 0.965565
Equal recommendation: 1223 MB each
Reading from 23549: heap size 544 MB, throughput 0.998798
Reading from 23548: heap size 1226 MB, throughput 0.964441
Reading from 23549: heap size 546 MB, throughput 0.994871
Reading from 23548: heap size 1155 MB, throughput 0.962728
Reading from 23548: heap size 1222 MB, throughput 0.964338
Reading from 23549: heap size 557 MB, throughput 0.998467
Reading from 23548: heap size 1218 MB, throughput 0.962749
Reading from 23548: heap size 1216 MB, throughput 0.966321
Reading from 23549: heap size 558 MB, throughput 0.998368
Equal recommendation: 1223 MB each
Reading from 23548: heap size 1219 MB, throughput 0.96335
Reading from 23548: heap size 1220 MB, throughput 0.9611
Reading from 23549: heap size 573 MB, throughput 0.998381
Reading from 23548: heap size 1222 MB, throughput 0.966323
Reading from 23548: heap size 1226 MB, throughput 0.966018
Reading from 23549: heap size 574 MB, throughput 0.99846
Reading from 23548: heap size 1187 MB, throughput 0.967082
Reading from 23549: heap size 587 MB, throughput 0.997005
Equal recommendation: 1223 MB each
Reading from 23548: heap size 1225 MB, throughput 0.959876
Reading from 23548: heap size 1188 MB, throughput 0.96267
Reading from 23549: heap size 588 MB, throughput 0.998543
Reading from 23548: heap size 1222 MB, throughput 0.965432
Reading from 23549: heap size 604 MB, throughput 0.998181
Reading from 23548: heap size 1222 MB, throughput 0.95804
Reading from 23548: heap size 1228 MB, throughput 0.96331
Equal recommendation: 1223 MB each
Reading from 23549: heap size 605 MB, throughput 0.998392
Reading from 23548: heap size 1198 MB, throughput 0.96077
Reading from 23548: heap size 1227 MB, throughput 0.962962
Reading from 23549: heap size 620 MB, throughput 0.99827
Reading from 23548: heap size 1198 MB, throughput 0.962339
Reading from 23549: heap size 620 MB, throughput 0.997641
Reading from 23548: heap size 1225 MB, throughput 0.959969
Reading from 23548: heap size 1197 MB, throughput 0.957605
Equal recommendation: 1223 MB each
Reading from 23549: heap size 635 MB, throughput 0.998657
Reading from 23548: heap size 1223 MB, throughput 0.960868
Reading from 23549: heap size 636 MB, throughput 0.937059
Reading from 23548: heap size 1197 MB, throughput 0.959954
Reading from 23548: heap size 1221 MB, throughput 0.957914
Reading from 23549: heap size 652 MB, throughput 0.999287
Reading from 23548: heap size 1221 MB, throughput 0.638595
Equal recommendation: 1223 MB each
Reading from 23548: heap size 1299 MB, throughput 0.992691
Client 23549 died
Clients: 1
Reading from 23548: heap size 1297 MB, throughput 0.990093
Reading from 23548: heap size 1302 MB, throughput 0.987174
Reading from 23548: heap size 1306 MB, throughput 0.985449
Reading from 23548: heap size 1305 MB, throughput 0.982929
Recommendation: one client; give it all the memory
Reading from 23548: heap size 1202 MB, throughput 0.980695
Reading from 23548: heap size 1306 MB, throughput 0.979479
Reading from 23548: heap size 1304 MB, throughput 0.976832
Reading from 23548: heap size 1293 MB, throughput 0.97691
Reading from 23548: heap size 1299 MB, throughput 0.973557
Recommendation: one client; give it all the memory
Reading from 23548: heap size 1294 MB, throughput 0.973766
Reading from 23548: heap size 1297 MB, throughput 0.971531
Reading from 23548: heap size 1303 MB, throughput 0.970958
Reading from 23548: heap size 1304 MB, throughput 0.968585
Reading from 23548: heap size 1311 MB, throughput 0.967221
Recommendation: one client; give it all the memory
Reading from 23548: heap size 1315 MB, throughput 0.964173
Recommendation: one client; give it all the memory
Reading from 23548: heap size 1326 MB, throughput 0.891044
Recommendation: one client; give it all the memory
Reading from 23548: heap size 1479 MB, throughput 0.987698
Recommendation: one client; give it all the memory
Reading from 23548: heap size 1523 MB, throughput 0.98917
Reading from 23548: heap size 1529 MB, throughput 0.892967
Reading from 23548: heap size 1536 MB, throughput 0.815659
Reading from 23548: heap size 1535 MB, throughput 0.709416
Reading from 23548: heap size 1566 MB, throughput 0.768684
Reading from 23548: heap size 1581 MB, throughput 0.747429
Reading from 23548: heap size 1618 MB, throughput 0.796787
Reading from 23548: heap size 1625 MB, throughput 0.777029
Client 23548 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
