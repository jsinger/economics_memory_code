economemd
    total memory: 5948 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub4_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
ReadingThread: starting
TimerThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 6827: heap size 9 MB, throughput 0.99194
Clients: 1
Client 6827 has a minimum heap size of 1211 MB
Reading from 6828: heap size 9 MB, throughput 0.98871
Clients: 2
Client 6828 has a minimum heap size of 276 MB
Reading from 6827: heap size 9 MB, throughput 0.980553
Reading from 6828: heap size 9 MB, throughput 0.894273
Reading from 6827: heap size 9 MB, throughput 0.964405
Reading from 6827: heap size 9 MB, throughput 0.95126
Reading from 6828: heap size 11 MB, throughput 0.978737
Reading from 6827: heap size 11 MB, throughput 0.921244
Reading from 6828: heap size 11 MB, throughput 0.985239
Reading from 6827: heap size 11 MB, throughput 0.97302
Reading from 6827: heap size 17 MB, throughput 0.824211
Reading from 6828: heap size 15 MB, throughput 0.843699
Reading from 6827: heap size 17 MB, throughput 0.590956
Reading from 6828: heap size 18 MB, throughput 0.951262
Reading from 6828: heap size 24 MB, throughput 0.782229
Reading from 6827: heap size 30 MB, throughput 0.949058
Reading from 6828: heap size 28 MB, throughput 0.809819
Reading from 6827: heap size 31 MB, throughput 0.926431
Reading from 6828: heap size 32 MB, throughput 0.33058
Reading from 6827: heap size 35 MB, throughput 0.40983
Reading from 6828: heap size 41 MB, throughput 0.903862
Reading from 6827: heap size 45 MB, throughput 0.82263
Reading from 6828: heap size 48 MB, throughput 0.897464
Reading from 6827: heap size 50 MB, throughput 0.460661
Reading from 6828: heap size 50 MB, throughput 0.238509
Reading from 6827: heap size 63 MB, throughput 0.638979
Reading from 6827: heap size 69 MB, throughput 0.818643
Reading from 6828: heap size 68 MB, throughput 0.39892
Reading from 6828: heap size 85 MB, throughput 0.786521
Reading from 6827: heap size 70 MB, throughput 0.247003
Reading from 6828: heap size 92 MB, throughput 0.793387
Reading from 6828: heap size 94 MB, throughput 0.515227
Reading from 6827: heap size 95 MB, throughput 0.697296
Reading from 6827: heap size 97 MB, throughput 0.252874
Reading from 6827: heap size 128 MB, throughput 0.76584
Reading from 6828: heap size 103 MB, throughput 0.286103
Reading from 6827: heap size 128 MB, throughput 0.745029
Reading from 6828: heap size 124 MB, throughput 0.681425
Reading from 6827: heap size 132 MB, throughput 0.794058
Reading from 6828: heap size 134 MB, throughput 0.854327
Reading from 6827: heap size 135 MB, throughput 0.771575
Reading from 6827: heap size 142 MB, throughput 0.174077
Reading from 6827: heap size 178 MB, throughput 0.496299
Reading from 6828: heap size 137 MB, throughput 0.131923
Reading from 6827: heap size 184 MB, throughput 0.780398
Reading from 6828: heap size 175 MB, throughput 0.729272
Reading from 6827: heap size 187 MB, throughput 0.683952
Reading from 6828: heap size 178 MB, throughput 0.734572
Reading from 6827: heap size 189 MB, throughput 0.155955
Reading from 6828: heap size 181 MB, throughput 0.180031
Reading from 6827: heap size 235 MB, throughput 0.673239
Reading from 6828: heap size 224 MB, throughput 0.732994
Reading from 6827: heap size 243 MB, throughput 0.727977
Reading from 6827: heap size 244 MB, throughput 0.657825
Reading from 6827: heap size 247 MB, throughput 0.600216
Reading from 6828: heap size 232 MB, throughput 0.918179
Reading from 6827: heap size 253 MB, throughput 0.645876
Reading from 6827: heap size 257 MB, throughput 0.595115
Reading from 6827: heap size 267 MB, throughput 0.570181
Reading from 6828: heap size 235 MB, throughput 0.918926
Reading from 6827: heap size 273 MB, throughput 0.557608
Reading from 6828: heap size 239 MB, throughput 0.550243
Reading from 6827: heap size 283 MB, throughput 0.497719
Reading from 6828: heap size 248 MB, throughput 0.798946
Reading from 6828: heap size 254 MB, throughput 0.527556
Reading from 6828: heap size 261 MB, throughput 0.70403
Reading from 6828: heap size 267 MB, throughput 0.518567
Reading from 6827: heap size 287 MB, throughput 0.155056
Reading from 6828: heap size 271 MB, throughput 0.77019
Reading from 6827: heap size 335 MB, throughput 0.378435
Reading from 6828: heap size 276 MB, throughput 0.646037
Reading from 6828: heap size 282 MB, throughput 0.591256
Reading from 6828: heap size 289 MB, throughput 0.5022
Reading from 6828: heap size 293 MB, throughput 0.560184
Reading from 6827: heap size 336 MB, throughput 0.111107
Reading from 6827: heap size 388 MB, throughput 0.57507
Reading from 6827: heap size 381 MB, throughput 0.666548
Reading from 6828: heap size 293 MB, throughput 0.86462
Reading from 6827: heap size 385 MB, throughput 0.708798
Reading from 6827: heap size 373 MB, throughput 0.726976
Reading from 6827: heap size 379 MB, throughput 0.584999
Reading from 6827: heap size 379 MB, throughput 0.535971
Reading from 6827: heap size 381 MB, throughput 0.637277
Reading from 6828: heap size 296 MB, throughput 0.287012
Reading from 6827: heap size 387 MB, throughput 0.707563
Reading from 6828: heap size 325 MB, throughput 0.491887
Reading from 6827: heap size 391 MB, throughput 0.560236
Reading from 6827: heap size 394 MB, throughput 0.466852
Reading from 6827: heap size 402 MB, throughput 0.479376
Reading from 6828: heap size 330 MB, throughput 0.198124
Reading from 6828: heap size 379 MB, throughput 0.592556
Reading from 6828: heap size 379 MB, throughput 0.868754
Reading from 6828: heap size 388 MB, throughput 0.840851
Reading from 6827: heap size 406 MB, throughput 0.0805099
Reading from 6828: heap size 388 MB, throughput 0.770817
Reading from 6828: heap size 384 MB, throughput 0.66563
Reading from 6827: heap size 462 MB, throughput 0.450598
Equal recommendation: 2974 MB each
Reading from 6828: heap size 389 MB, throughput 0.755671
Reading from 6827: heap size 476 MB, throughput 0.506805
Reading from 6828: heap size 382 MB, throughput 0.61327
Reading from 6828: heap size 387 MB, throughput 0.524493
Reading from 6827: heap size 477 MB, throughput 0.504856
Reading from 6828: heap size 384 MB, throughput 0.763196
Reading from 6827: heap size 482 MB, throughput 0.0643704
Reading from 6827: heap size 541 MB, throughput 0.398566
Reading from 6827: heap size 530 MB, throughput 0.550646
Reading from 6827: heap size 537 MB, throughput 0.553894
Reading from 6828: heap size 388 MB, throughput 0.960969
Reading from 6827: heap size 538 MB, throughput 0.453587
Reading from 6827: heap size 541 MB, throughput 0.468672
Reading from 6828: heap size 391 MB, throughput 0.95581
Reading from 6827: heap size 544 MB, throughput 0.0940338
Reading from 6827: heap size 612 MB, throughput 0.576541
Reading from 6827: heap size 623 MB, throughput 0.607077
Reading from 6827: heap size 623 MB, throughput 0.634886
Reading from 6827: heap size 625 MB, throughput 0.616108
Reading from 6827: heap size 633 MB, throughput 0.556038
Reading from 6827: heap size 638 MB, throughput 0.741112
Reading from 6828: heap size 394 MB, throughput 0.946069
Reading from 6827: heap size 650 MB, throughput 0.826018
Reading from 6827: heap size 652 MB, throughput 0.806983
Reading from 6828: heap size 392 MB, throughput 0.964242
Reading from 6828: heap size 396 MB, throughput 0.965607
Reading from 6827: heap size 666 MB, throughput 0.274071
Reading from 6827: heap size 735 MB, throughput 0.437406
Reading from 6827: heap size 750 MB, throughput 0.376232
Reading from 6827: heap size 750 MB, throughput 0.441709
Reading from 6827: heap size 662 MB, throughput 0.594023
Equal recommendation: 2974 MB each
Reading from 6827: heap size 737 MB, throughput 0.0389399
Reading from 6827: heap size 719 MB, throughput 0.214655
Reading from 6827: heap size 812 MB, throughput 0.376488
Reading from 6827: heap size 813 MB, throughput 0.404582
Reading from 6828: heap size 392 MB, throughput 0.965563
Reading from 6827: heap size 817 MB, throughput 0.0520256
Reading from 6827: heap size 899 MB, throughput 0.771395
Reading from 6827: heap size 904 MB, throughput 0.684579
Reading from 6827: heap size 906 MB, throughput 0.72218
Reading from 6827: heap size 907 MB, throughput 0.555056
Reading from 6828: heap size 396 MB, throughput 0.975095
Reading from 6827: heap size 909 MB, throughput 0.627954
Reading from 6827: heap size 908 MB, throughput 0.882328
Reading from 6827: heap size 910 MB, throughput 0.848883
Reading from 6827: heap size 909 MB, throughput 0.905068
Reading from 6827: heap size 912 MB, throughput 0.892719
Reading from 6827: heap size 902 MB, throughput 0.930018
Reading from 6827: heap size 719 MB, throughput 0.909782
Reading from 6828: heap size 392 MB, throughput 0.972229
Reading from 6827: heap size 892 MB, throughput 0.900635
Reading from 6827: heap size 765 MB, throughput 0.700449
Reading from 6827: heap size 871 MB, throughput 0.755932
Reading from 6827: heap size 770 MB, throughput 0.757146
Reading from 6828: heap size 395 MB, throughput 0.947828
Reading from 6827: heap size 859 MB, throughput 0.0829717
Reading from 6827: heap size 851 MB, throughput 0.471179
Reading from 6827: heap size 943 MB, throughput 0.675638
Reading from 6827: heap size 947 MB, throughput 0.743616
Reading from 6827: heap size 942 MB, throughput 0.748233
Reading from 6827: heap size 946 MB, throughput 0.766489
Reading from 6828: heap size 395 MB, throughput 0.969024
Reading from 6827: heap size 940 MB, throughput 0.773106
Reading from 6827: heap size 944 MB, throughput 0.717835
Reading from 6827: heap size 937 MB, throughput 0.959021
Reading from 6828: heap size 396 MB, throughput 0.97691
Reading from 6827: heap size 942 MB, throughput 0.88433
Reading from 6827: heap size 938 MB, throughput 0.672434
Reading from 6827: heap size 952 MB, throughput 0.657532
Reading from 6827: heap size 967 MB, throughput 0.776392
Reading from 6827: heap size 969 MB, throughput 0.764901
Reading from 6827: heap size 968 MB, throughput 0.731646
Reading from 6828: heap size 399 MB, throughput 0.955096
Reading from 6827: heap size 971 MB, throughput 0.795556
Equal recommendation: 2974 MB each
Reading from 6827: heap size 971 MB, throughput 0.787294
Reading from 6827: heap size 974 MB, throughput 0.79556
Reading from 6827: heap size 973 MB, throughput 0.864797
Reading from 6827: heap size 977 MB, throughput 0.850289
Reading from 6827: heap size 978 MB, throughput 0.826571
Reading from 6827: heap size 981 MB, throughput 0.708535
Reading from 6828: heap size 400 MB, throughput 0.970421
Reading from 6828: heap size 399 MB, throughput 0.959539
Reading from 6828: heap size 401 MB, throughput 0.786427
Reading from 6828: heap size 397 MB, throughput 0.710353
Reading from 6827: heap size 968 MB, throughput 0.0601706
Reading from 6828: heap size 402 MB, throughput 0.803593
Reading from 6827: heap size 1100 MB, throughput 0.480299
Reading from 6827: heap size 1108 MB, throughput 0.730182
Reading from 6827: heap size 1108 MB, throughput 0.6249
Reading from 6828: heap size 411 MB, throughput 0.959159
Reading from 6827: heap size 1118 MB, throughput 0.649686
Reading from 6827: heap size 1119 MB, throughput 0.689571
Reading from 6827: heap size 1124 MB, throughput 0.769224
Reading from 6827: heap size 1127 MB, throughput 0.695373
Reading from 6827: heap size 1130 MB, throughput 0.639752
Reading from 6827: heap size 1134 MB, throughput 0.661544
Reading from 6828: heap size 413 MB, throughput 0.976815
Reading from 6827: heap size 1144 MB, throughput 0.936479
Reading from 6828: heap size 415 MB, throughput 0.987456
Reading from 6827: heap size 1146 MB, throughput 0.943634
Reading from 6828: heap size 417 MB, throughput 0.985726
Equal recommendation: 2974 MB each
Reading from 6827: heap size 1155 MB, throughput 0.961193
Reading from 6828: heap size 415 MB, throughput 0.985073
Reading from 6827: heap size 1158 MB, throughput 0.939776
Reading from 6828: heap size 418 MB, throughput 0.983621
Reading from 6827: heap size 1170 MB, throughput 0.951016
Reading from 6828: heap size 414 MB, throughput 0.986801
Reading from 6827: heap size 1174 MB, throughput 0.956478
Reading from 6828: heap size 417 MB, throughput 0.981909
Reading from 6827: heap size 1172 MB, throughput 0.944292
Reading from 6828: heap size 414 MB, throughput 0.980244
Reading from 6827: heap size 1177 MB, throughput 0.954517
Equal recommendation: 2974 MB each
Reading from 6828: heap size 416 MB, throughput 0.980725
Reading from 6827: heap size 1183 MB, throughput 0.955055
Reading from 6827: heap size 1184 MB, throughput 0.953919
Reading from 6828: heap size 416 MB, throughput 0.993055
Reading from 6828: heap size 417 MB, throughput 0.979068
Reading from 6828: heap size 418 MB, throughput 0.882025
Reading from 6828: heap size 419 MB, throughput 0.856314
Reading from 6828: heap size 423 MB, throughput 0.884666
Reading from 6827: heap size 1192 MB, throughput 0.963864
Reading from 6828: heap size 424 MB, throughput 0.989058
Reading from 6827: heap size 1194 MB, throughput 0.949071
Reading from 6828: heap size 428 MB, throughput 0.988108
Reading from 6827: heap size 1201 MB, throughput 0.954242
Equal recommendation: 2974 MB each
Reading from 6828: heap size 429 MB, throughput 0.987989
Reading from 6827: heap size 1205 MB, throughput 0.953804
Reading from 6828: heap size 429 MB, throughput 0.986241
Reading from 6827: heap size 1211 MB, throughput 0.962486
Reading from 6828: heap size 431 MB, throughput 0.986577
Reading from 6827: heap size 1217 MB, throughput 0.953327
Reading from 6828: heap size 427 MB, throughput 0.986415
Reading from 6827: heap size 1224 MB, throughput 0.963369
Reading from 6828: heap size 430 MB, throughput 0.985696
Reading from 6827: heap size 1229 MB, throughput 0.955958
Equal recommendation: 2974 MB each
Reading from 6828: heap size 429 MB, throughput 0.984933
Reading from 6827: heap size 1236 MB, throughput 0.946869
Reading from 6828: heap size 430 MB, throughput 0.981627
Reading from 6827: heap size 1240 MB, throughput 0.952031
Reading from 6828: heap size 432 MB, throughput 0.988007
Reading from 6828: heap size 433 MB, throughput 0.87782
Reading from 6828: heap size 433 MB, throughput 0.86023
Reading from 6828: heap size 435 MB, throughput 0.965905
Reading from 6827: heap size 1247 MB, throughput 0.521013
Reading from 6828: heap size 441 MB, throughput 0.99111
Reading from 6827: heap size 1311 MB, throughput 0.992605
Equal recommendation: 2974 MB each
Reading from 6828: heap size 442 MB, throughput 0.991034
Reading from 6827: heap size 1310 MB, throughput 0.981685
Reading from 6828: heap size 442 MB, throughput 0.986699
Reading from 6827: heap size 1319 MB, throughput 0.984723
Reading from 6828: heap size 444 MB, throughput 0.987939
Reading from 6827: heap size 1328 MB, throughput 0.985402
Reading from 6828: heap size 441 MB, throughput 0.983956
Reading from 6827: heap size 1329 MB, throughput 0.977506
Reading from 6828: heap size 443 MB, throughput 0.986405
Equal recommendation: 2974 MB each
Reading from 6827: heap size 1323 MB, throughput 0.976924
Reading from 6828: heap size 443 MB, throughput 0.986414
Reading from 6827: heap size 1221 MB, throughput 0.97968
Reading from 6828: heap size 444 MB, throughput 0.984717
Reading from 6827: heap size 1311 MB, throughput 0.969292
Reading from 6828: heap size 445 MB, throughput 0.989006
Reading from 6828: heap size 446 MB, throughput 0.898281
Reading from 6828: heap size 446 MB, throughput 0.88437
Reading from 6828: heap size 448 MB, throughput 0.976025
Reading from 6827: heap size 1242 MB, throughput 0.967933
Reading from 6828: heap size 454 MB, throughput 0.989845
Reading from 6827: heap size 1310 MB, throughput 0.967124
Equal recommendation: 2974 MB each
Reading from 6828: heap size 455 MB, throughput 0.989468
Reading from 6827: heap size 1313 MB, throughput 0.964065
Reading from 6828: heap size 454 MB, throughput 0.989174
Reading from 6827: heap size 1317 MB, throughput 0.960704
Reading from 6828: heap size 456 MB, throughput 0.974309
Reading from 6827: heap size 1319 MB, throughput 0.957888
Reading from 6827: heap size 1324 MB, throughput 0.955183
Reading from 6828: heap size 455 MB, throughput 0.829925
Equal recommendation: 2974 MB each
Reading from 6827: heap size 1330 MB, throughput 0.954914
Reading from 6828: heap size 480 MB, throughput 0.995062
Reading from 6827: heap size 1336 MB, throughput 0.953078
Reading from 6828: heap size 479 MB, throughput 0.993194
Reading from 6827: heap size 1345 MB, throughput 0.951369
Reading from 6828: heap size 482 MB, throughput 0.990245
Reading from 6828: heap size 481 MB, throughput 0.920297
Reading from 6828: heap size 483 MB, throughput 0.865565
Reading from 6828: heap size 487 MB, throughput 0.977035
Reading from 6827: heap size 1355 MB, throughput 0.953975
Equal recommendation: 2974 MB each
Reading from 6828: heap size 490 MB, throughput 0.990983
Reading from 6827: heap size 1360 MB, throughput 0.955441
Reading from 6828: heap size 490 MB, throughput 0.990559
Reading from 6827: heap size 1370 MB, throughput 0.952795
Reading from 6828: heap size 492 MB, throughput 0.988619
Reading from 6827: heap size 1372 MB, throughput 0.954931
Reading from 6828: heap size 490 MB, throughput 0.986456
Equal recommendation: 2974 MB each
Reading from 6828: heap size 492 MB, throughput 0.986093
Reading from 6828: heap size 494 MB, throughput 0.985817
Reading from 6828: heap size 495 MB, throughput 0.982551
Reading from 6828: heap size 496 MB, throughput 0.983399
Reading from 6828: heap size 500 MB, throughput 0.850999
Reading from 6828: heap size 504 MB, throughput 0.887986
Reading from 6828: heap size 506 MB, throughput 0.988325
Equal recommendation: 2974 MB each
Reading from 6828: heap size 512 MB, throughput 0.981104
Reading from 6827: heap size 1382 MB, throughput 0.819065
Reading from 6828: heap size 514 MB, throughput 0.989592
Reading from 6828: heap size 513 MB, throughput 0.989391
Reading from 6828: heap size 516 MB, throughput 0.987651
Equal recommendation: 2974 MB each
Reading from 6828: heap size 514 MB, throughput 0.983474
Reading from 6827: heap size 1531 MB, throughput 0.981695
Reading from 6828: heap size 516 MB, throughput 0.98361
Reading from 6828: heap size 516 MB, throughput 0.983101
Reading from 6828: heap size 519 MB, throughput 0.880814
Reading from 6828: heap size 523 MB, throughput 0.957728
Reading from 6827: heap size 1582 MB, throughput 0.983885
Equal recommendation: 2974 MB each
Reading from 6827: heap size 1583 MB, throughput 0.815716
Reading from 6827: heap size 1571 MB, throughput 0.707975
Reading from 6828: heap size 524 MB, throughput 0.992479
Reading from 6827: heap size 1590 MB, throughput 0.712764
Reading from 6827: heap size 1616 MB, throughput 0.732115
Reading from 6827: heap size 1627 MB, throughput 0.739117
Reading from 6827: heap size 1660 MB, throughput 0.809025
Reading from 6828: heap size 527 MB, throughput 0.98778
Reading from 6827: heap size 1665 MB, throughput 0.963109
Reading from 6828: heap size 529 MB, throughput 0.989827
Reading from 6827: heap size 1672 MB, throughput 0.972736
Reading from 6828: heap size 527 MB, throughput 0.989744
Reading from 6827: heap size 1682 MB, throughput 0.970616
Equal recommendation: 2974 MB each
Reading from 6828: heap size 530 MB, throughput 0.988718
Reading from 6827: heap size 1679 MB, throughput 0.979038
Reading from 6828: heap size 529 MB, throughput 0.988313
Reading from 6827: heap size 1691 MB, throughput 0.967517
Reading from 6828: heap size 530 MB, throughput 0.991582
Reading from 6828: heap size 532 MB, throughput 0.908872
Reading from 6828: heap size 533 MB, throughput 0.934301
Reading from 6827: heap size 1680 MB, throughput 0.973561
Equal recommendation: 2974 MB each
Reading from 6828: heap size 540 MB, throughput 0.995304
Reading from 6827: heap size 1692 MB, throughput 0.965077
Reading from 6828: heap size 540 MB, throughput 0.99195
Reading from 6827: heap size 1685 MB, throughput 0.966855
Reading from 6828: heap size 540 MB, throughput 0.990032
Reading from 6827: heap size 1693 MB, throughput 0.972473
Equal recommendation: 2974 MB each
Reading from 6828: heap size 542 MB, throughput 0.98941
Reading from 6828: heap size 541 MB, throughput 0.981961
Reading from 6827: heap size 1700 MB, throughput 0.966712
Reading from 6828: heap size 543 MB, throughput 0.989474
Reading from 6827: heap size 1704 MB, throughput 0.96903
Reading from 6828: heap size 543 MB, throughput 0.981352
Reading from 6828: heap size 546 MB, throughput 0.910455
Reading from 6828: heap size 550 MB, throughput 0.98613
Equal recommendation: 2974 MB each
Reading from 6827: heap size 1698 MB, throughput 0.967984
Reading from 6828: heap size 551 MB, throughput 0.991664
Reading from 6827: heap size 1705 MB, throughput 0.968589
Reading from 6828: heap size 554 MB, throughput 0.991526
Reading from 6827: heap size 1707 MB, throughput 0.96531
Reading from 6828: heap size 555 MB, throughput 0.990572
Equal recommendation: 2974 MB each
Reading from 6828: heap size 553 MB, throughput 0.989922
Reading from 6827: heap size 1710 MB, throughput 0.962219
Reading from 6828: heap size 555 MB, throughput 0.988952
Reading from 6827: heap size 1719 MB, throughput 0.961551
Reading from 6828: heap size 556 MB, throughput 0.989597
Reading from 6828: heap size 557 MB, throughput 0.895443
Reading from 6828: heap size 560 MB, throughput 0.985607
Equal recommendation: 2974 MB each
Reading from 6827: heap size 1725 MB, throughput 0.958446
Reading from 6828: heap size 561 MB, throughput 0.992748
Reading from 6827: heap size 1739 MB, throughput 0.95464
Reading from 6828: heap size 564 MB, throughput 0.991716
Reading from 6828: heap size 566 MB, throughput 0.982711
Reading from 6827: heap size 1746 MB, throughput 0.731224
Equal recommendation: 2974 MB each
Reading from 6828: heap size 564 MB, throughput 0.990281
Reading from 6827: heap size 1730 MB, throughput 0.994084
Reading from 6828: heap size 566 MB, throughput 0.989156
Reading from 6827: heap size 1736 MB, throughput 0.988587
Reading from 6828: heap size 569 MB, throughput 0.988716
Reading from 6828: heap size 569 MB, throughput 0.911371
Reading from 6828: heap size 573 MB, throughput 0.988767
Equal recommendation: 2974 MB each
Reading from 6827: heap size 1758 MB, throughput 0.986617
Reading from 6828: heap size 574 MB, throughput 0.992974
Reading from 6827: heap size 1766 MB, throughput 0.985134
Reading from 6828: heap size 577 MB, throughput 0.992865
Equal recommendation: 2974 MB each
Reading from 6827: heap size 1769 MB, throughput 0.980812
Reading from 6828: heap size 578 MB, throughput 0.990948
Reading from 6828: heap size 576 MB, throughput 0.98971
Reading from 6827: heap size 1774 MB, throughput 0.978304
Reading from 6828: heap size 578 MB, throughput 0.991927
Reading from 6828: heap size 578 MB, throughput 0.956322
Reading from 6828: heap size 580 MB, throughput 0.939683
Equal recommendation: 2974 MB each
Reading from 6828: heap size 587 MB, throughput 0.994092
Reading from 6828: heap size 587 MB, throughput 0.99207
Reading from 6827: heap size 1759 MB, throughput 0.989083
Reading from 6828: heap size 587 MB, throughput 0.991841
Equal recommendation: 2974 MB each
Reading from 6828: heap size 589 MB, throughput 0.989217
Reading from 6828: heap size 591 MB, throughput 0.990005
Reading from 6828: heap size 591 MB, throughput 0.985247
Reading from 6828: heap size 594 MB, throughput 0.926368
Client 6828 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 6827: heap size 1769 MB, throughput 0.994477
Reading from 6827: heap size 1781 MB, throughput 0.903301
Reading from 6827: heap size 1788 MB, throughput 0.791864
Reading from 6827: heap size 1813 MB, throughput 0.830127
Reading from 6827: heap size 1839 MB, throughput 0.868615
Client 6827 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
