economemd
    total memory: 8922 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub6_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 12374: heap size 9 MB, throughput 0.991824
Clients: 1
Client 12374 has a minimum heap size of 276 MB
Reading from 12373: heap size 9 MB, throughput 0.985365
Clients: 2
Client 12373 has a minimum heap size of 1211 MB
Reading from 12374: heap size 9 MB, throughput 0.982351
Reading from 12373: heap size 9 MB, throughput 0.976261
Reading from 12374: heap size 9 MB, throughput 0.965623
Reading from 12374: heap size 9 MB, throughput 0.949047
Reading from 12373: heap size 11 MB, throughput 0.975344
Reading from 12374: heap size 11 MB, throughput 0.970215
Reading from 12373: heap size 11 MB, throughput 0.981347
Reading from 12374: heap size 11 MB, throughput 0.985913
Reading from 12373: heap size 15 MB, throughput 0.909718
Reading from 12374: heap size 17 MB, throughput 0.931496
Reading from 12373: heap size 19 MB, throughput 0.976797
Reading from 12374: heap size 17 MB, throughput 0.43941
Reading from 12373: heap size 24 MB, throughput 0.935948
Reading from 12374: heap size 30 MB, throughput 0.87529
Reading from 12374: heap size 31 MB, throughput 0.903985
Reading from 12373: heap size 29 MB, throughput 0.60304
Reading from 12373: heap size 39 MB, throughput 0.96386
Reading from 12373: heap size 41 MB, throughput 0.941035
Reading from 12374: heap size 35 MB, throughput 0.481167
Reading from 12373: heap size 41 MB, throughput 0.890839
Reading from 12373: heap size 43 MB, throughput 0.680236
Reading from 12374: heap size 48 MB, throughput 0.87437
Reading from 12373: heap size 45 MB, throughput 0.286028
Reading from 12374: heap size 51 MB, throughput 0.495263
Reading from 12373: heap size 59 MB, throughput 0.761835
Reading from 12373: heap size 64 MB, throughput 0.818279
Reading from 12374: heap size 64 MB, throughput 0.894958
Reading from 12374: heap size 71 MB, throughput 0.793884
Reading from 12373: heap size 66 MB, throughput 0.23151
Reading from 12373: heap size 87 MB, throughput 0.652292
Reading from 12374: heap size 71 MB, throughput 0.2804
Reading from 12373: heap size 90 MB, throughput 0.558078
Reading from 12374: heap size 102 MB, throughput 0.823281
Reading from 12373: heap size 93 MB, throughput 0.752745
Reading from 12373: heap size 95 MB, throughput 0.669667
Reading from 12374: heap size 102 MB, throughput 0.787997
Reading from 12374: heap size 104 MB, throughput 0.20846
Reading from 12373: heap size 100 MB, throughput 0.153214
Reading from 12374: heap size 130 MB, throughput 0.633035
Reading from 12373: heap size 130 MB, throughput 0.65243
Reading from 12374: heap size 136 MB, throughput 0.809176
Reading from 12374: heap size 137 MB, throughput 0.803517
Reading from 12374: heap size 144 MB, throughput 0.664352
Reading from 12373: heap size 133 MB, throughput 0.189835
Reading from 12373: heap size 165 MB, throughput 0.752286
Reading from 12373: heap size 166 MB, throughput 0.710716
Reading from 12373: heap size 168 MB, throughput 0.717957
Reading from 12373: heap size 170 MB, throughput 0.656614
Reading from 12374: heap size 147 MB, throughput 0.140208
Reading from 12373: heap size 175 MB, throughput 0.780571
Reading from 12374: heap size 189 MB, throughput 0.687302
Reading from 12374: heap size 192 MB, throughput 0.704346
Reading from 12373: heap size 177 MB, throughput 0.177876
Reading from 12373: heap size 219 MB, throughput 0.617966
Reading from 12373: heap size 223 MB, throughput 0.621644
Reading from 12374: heap size 194 MB, throughput 0.177168
Reading from 12373: heap size 226 MB, throughput 0.617566
Reading from 12373: heap size 229 MB, throughput 0.461979
Reading from 12373: heap size 236 MB, throughput 0.465964
Reading from 12374: heap size 236 MB, throughput 0.903324
Reading from 12373: heap size 241 MB, throughput 0.613493
Reading from 12374: heap size 242 MB, throughput 0.917697
Reading from 12374: heap size 245 MB, throughput 0.73387
Reading from 12373: heap size 249 MB, throughput 0.0868567
Reading from 12374: heap size 250 MB, throughput 0.855614
Reading from 12374: heap size 253 MB, throughput 0.495415
Reading from 12373: heap size 300 MB, throughput 0.567344
Reading from 12373: heap size 300 MB, throughput 0.732312
Reading from 12374: heap size 258 MB, throughput 0.848973
Reading from 12373: heap size 300 MB, throughput 0.643995
Reading from 12374: heap size 261 MB, throughput 0.772825
Reading from 12373: heap size 301 MB, throughput 0.591543
Reading from 12373: heap size 303 MB, throughput 0.661069
Reading from 12374: heap size 266 MB, throughput 0.802685
Reading from 12374: heap size 268 MB, throughput 0.596381
Reading from 12374: heap size 274 MB, throughput 0.661893
Reading from 12374: heap size 278 MB, throughput 0.48413
Reading from 12374: heap size 284 MB, throughput 0.675316
Reading from 12373: heap size 309 MB, throughput 0.139986
Reading from 12373: heap size 361 MB, throughput 0.518329
Reading from 12373: heap size 367 MB, throughput 0.682851
Reading from 12374: heap size 287 MB, throughput 0.929366
Reading from 12373: heap size 368 MB, throughput 0.679302
Reading from 12373: heap size 371 MB, throughput 0.5901
Reading from 12374: heap size 282 MB, throughput 0.885698
Reading from 12373: heap size 374 MB, throughput 0.604906
Reading from 12373: heap size 382 MB, throughput 0.417188
Reading from 12373: heap size 388 MB, throughput 0.446736
Reading from 12374: heap size 288 MB, throughput 0.0948554
Reading from 12374: heap size 321 MB, throughput 0.456754
Reading from 12374: heap size 324 MB, throughput 0.911811
Reading from 12373: heap size 397 MB, throughput 0.0832022
Reading from 12374: heap size 329 MB, throughput 0.837878
Reading from 12373: heap size 456 MB, throughput 0.337664
Reading from 12373: heap size 459 MB, throughput 0.495721
Reading from 12374: heap size 330 MB, throughput 0.814675
Reading from 12374: heap size 332 MB, throughput 0.768603
Reading from 12374: heap size 333 MB, throughput 0.598933
Reading from 12374: heap size 339 MB, throughput 0.77371
Reading from 12374: heap size 338 MB, throughput 0.625184
Reading from 12374: heap size 345 MB, throughput 0.681825
Equal recommendation: 4461 MB each
Reading from 12373: heap size 446 MB, throughput 0.136701
Reading from 12374: heap size 345 MB, throughput 0.78288
Reading from 12373: heap size 513 MB, throughput 0.360008
Reading from 12373: heap size 510 MB, throughput 0.641396
Reading from 12373: heap size 512 MB, throughput 0.625099
Reading from 12373: heap size 508 MB, throughput 0.599366
Reading from 12374: heap size 353 MB, throughput 0.974059
Reading from 12373: heap size 511 MB, throughput 0.0892721
Reading from 12373: heap size 567 MB, throughput 0.476598
Reading from 12373: heap size 570 MB, throughput 0.656827
Reading from 12373: heap size 570 MB, throughput 0.611771
Reading from 12373: heap size 572 MB, throughput 0.626857
Reading from 12373: heap size 574 MB, throughput 0.703306
Reading from 12373: heap size 585 MB, throughput 0.563666
Reading from 12373: heap size 595 MB, throughput 0.65303
Reading from 12373: heap size 604 MB, throughput 0.574351
Reading from 12373: heap size 608 MB, throughput 0.549357
Reading from 12374: heap size 354 MB, throughput 0.962312
Reading from 12373: heap size 622 MB, throughput 0.543366
Reading from 12373: heap size 630 MB, throughput 0.808357
Reading from 12373: heap size 643 MB, throughput 0.797188
Reading from 12374: heap size 354 MB, throughput 0.977673
Reading from 12373: heap size 644 MB, throughput 0.761832
Reading from 12373: heap size 661 MB, throughput 0.780938
Reading from 12373: heap size 666 MB, throughput 0.064504
Reading from 12373: heap size 745 MB, throughput 0.214611
Reading from 12373: heap size 754 MB, throughput 0.387117
Reading from 12374: heap size 357 MB, throughput 0.601294
Reading from 12373: heap size 643 MB, throughput 0.513879
Reading from 12373: heap size 729 MB, throughput 0.0342762
Reading from 12373: heap size 690 MB, throughput 0.141314
Reading from 12373: heap size 803 MB, throughput 0.661092
Reading from 12373: heap size 805 MB, throughput 0.390369
Reading from 12374: heap size 402 MB, throughput 0.960091
Reading from 12373: heap size 803 MB, throughput 0.0273502
Equal recommendation: 4461 MB each
Reading from 12373: heap size 885 MB, throughput 0.158963
Reading from 12373: heap size 884 MB, throughput 0.554644
Reading from 12373: heap size 886 MB, throughput 0.540651
Reading from 12373: heap size 883 MB, throughput 0.494977
Reading from 12373: heap size 886 MB, throughput 0.584927
Reading from 12373: heap size 884 MB, throughput 0.663804
Reading from 12373: heap size 886 MB, throughput 0.915721
Reading from 12373: heap size 879 MB, throughput 0.926615
Reading from 12373: heap size 684 MB, throughput 0.699769
Reading from 12373: heap size 862 MB, throughput 0.665882
Reading from 12373: heap size 687 MB, throughput 0.738908
Reading from 12373: heap size 848 MB, throughput 0.692371
Reading from 12373: heap size 690 MB, throughput 0.753803
Reading from 12373: heap size 837 MB, throughput 0.918669
Reading from 12373: heap size 678 MB, throughput 0.930315
Reading from 12373: heap size 825 MB, throughput 0.810704
Reading from 12374: heap size 403 MB, throughput 0.99307
Reading from 12373: heap size 687 MB, throughput 0.929763
Reading from 12373: heap size 813 MB, throughput 0.723462
Reading from 12373: heap size 693 MB, throughput 0.915836
Reading from 12373: heap size 801 MB, throughput 0.892327
Reading from 12373: heap size 701 MB, throughput 0.839156
Reading from 12373: heap size 789 MB, throughput 0.894869
Reading from 12373: heap size 705 MB, throughput 0.888061
Reading from 12373: heap size 790 MB, throughput 0.765925
Reading from 12373: heap size 792 MB, throughput 0.699007
Reading from 12373: heap size 796 MB, throughput 0.756733
Reading from 12373: heap size 796 MB, throughput 0.677826
Reading from 12374: heap size 411 MB, throughput 0.990784
Reading from 12373: heap size 799 MB, throughput 0.756278
Reading from 12373: heap size 799 MB, throughput 0.752805
Reading from 12373: heap size 797 MB, throughput 0.721464
Reading from 12373: heap size 800 MB, throughput 0.754798
Reading from 12373: heap size 797 MB, throughput 0.784016
Reading from 12373: heap size 800 MB, throughput 0.7735
Reading from 12374: heap size 411 MB, throughput 0.982174
Reading from 12373: heap size 793 MB, throughput 0.0601238
Reading from 12373: heap size 872 MB, throughput 0.493382
Reading from 12374: heap size 411 MB, throughput 0.986431
Reading from 12373: heap size 871 MB, throughput 0.745911
Reading from 12373: heap size 872 MB, throughput 0.670269
Reading from 12373: heap size 877 MB, throughput 0.945425
Reading from 12374: heap size 413 MB, throughput 0.984971
Reading from 12373: heap size 877 MB, throughput 0.939374
Reading from 12373: heap size 872 MB, throughput 0.712066
Reading from 12373: heap size 880 MB, throughput 0.731216
Reading from 12373: heap size 888 MB, throughput 0.718382
Reading from 12373: heap size 893 MB, throughput 0.743843
Reading from 12373: heap size 901 MB, throughput 0.712976
Reading from 12374: heap size 408 MB, throughput 0.98453
Reading from 12373: heap size 904 MB, throughput 0.736284
Reading from 12373: heap size 913 MB, throughput 0.717576
Reading from 12373: heap size 914 MB, throughput 0.744519
Reading from 12373: heap size 923 MB, throughput 0.807328
Reading from 12373: heap size 923 MB, throughput 0.757295
Equal recommendation: 4461 MB each
Reading from 12373: heap size 930 MB, throughput 0.85297
Reading from 12373: heap size 931 MB, throughput 0.856374
Reading from 12374: heap size 377 MB, throughput 0.98421
Reading from 12373: heap size 930 MB, throughput 0.890856
Reading from 12373: heap size 933 MB, throughput 0.134868
Reading from 12373: heap size 994 MB, throughput 0.835909
Reading from 12373: heap size 997 MB, throughput 0.736677
Reading from 12373: heap size 1006 MB, throughput 0.752804
Reading from 12373: heap size 1010 MB, throughput 0.694677
Reading from 12373: heap size 1021 MB, throughput 0.719436
Reading from 12373: heap size 1024 MB, throughput 0.670469
Reading from 12374: heap size 406 MB, throughput 0.980011
Reading from 12373: heap size 1037 MB, throughput 0.70099
Reading from 12373: heap size 1040 MB, throughput 0.694851
Reading from 12373: heap size 1054 MB, throughput 0.709496
Reading from 12373: heap size 1057 MB, throughput 0.589645
Reading from 12373: heap size 1072 MB, throughput 0.667934
Reading from 12373: heap size 1075 MB, throughput 0.632318
Reading from 12374: heap size 408 MB, throughput 0.976279
Reading from 12374: heap size 411 MB, throughput 0.655055
Reading from 12374: heap size 414 MB, throughput 0.603399
Reading from 12374: heap size 423 MB, throughput 0.729743
Reading from 12374: heap size 429 MB, throughput 0.836295
Reading from 12373: heap size 1093 MB, throughput 0.901269
Reading from 12373: heap size 1096 MB, throughput 0.93981
Reading from 12374: heap size 437 MB, throughput 0.985626
Reading from 12373: heap size 1113 MB, throughput 0.949791
Reading from 12374: heap size 441 MB, throughput 0.984596
Equal recommendation: 4461 MB each
Reading from 12373: heap size 1117 MB, throughput 0.653026
Reading from 12374: heap size 438 MB, throughput 0.986215
Reading from 12373: heap size 1211 MB, throughput 0.992225
Reading from 12374: heap size 443 MB, throughput 0.985972
Reading from 12373: heap size 1212 MB, throughput 0.986073
Reading from 12374: heap size 441 MB, throughput 0.984705
Reading from 12373: heap size 1229 MB, throughput 0.974594
Reading from 12374: heap size 445 MB, throughput 0.982166
Reading from 12373: heap size 1232 MB, throughput 0.976465
Reading from 12374: heap size 443 MB, throughput 0.981545
Reading from 12373: heap size 1233 MB, throughput 0.983039
Equal recommendation: 4461 MB each
Reading from 12374: heap size 446 MB, throughput 0.982493
Reading from 12373: heap size 1238 MB, throughput 0.971254
Reading from 12373: heap size 1226 MB, throughput 0.978671
Reading from 12374: heap size 447 MB, throughput 0.984109
Reading from 12373: heap size 1126 MB, throughput 0.970044
Reading from 12374: heap size 448 MB, throughput 0.984884
Reading from 12374: heap size 446 MB, throughput 0.962187
Reading from 12374: heap size 448 MB, throughput 0.863403
Reading from 12374: heap size 452 MB, throughput 0.849001
Reading from 12373: heap size 1215 MB, throughput 0.968048
Reading from 12374: heap size 452 MB, throughput 0.988418
Reading from 12373: heap size 1145 MB, throughput 0.964939
Equal recommendation: 4461 MB each
Reading from 12374: heap size 459 MB, throughput 0.989086
Reading from 12373: heap size 1215 MB, throughput 0.968283
Reading from 12374: heap size 460 MB, throughput 0.972651
Reading from 12373: heap size 1219 MB, throughput 0.960479
Reading from 12373: heap size 1222 MB, throughput 0.956475
Reading from 12374: heap size 465 MB, throughput 0.987821
Reading from 12373: heap size 1223 MB, throughput 0.957992
Reading from 12374: heap size 466 MB, throughput 0.987526
Reading from 12373: heap size 1229 MB, throughput 0.955644
Reading from 12374: heap size 465 MB, throughput 0.988325
Equal recommendation: 4461 MB each
Reading from 12373: heap size 1233 MB, throughput 0.950525
Reading from 12374: heap size 467 MB, throughput 0.985072
Reading from 12373: heap size 1242 MB, throughput 0.949643
Reading from 12374: heap size 468 MB, throughput 0.984262
Reading from 12373: heap size 1246 MB, throughput 0.952098
Reading from 12374: heap size 468 MB, throughput 0.992804
Reading from 12374: heap size 470 MB, throughput 0.931027
Reading from 12374: heap size 471 MB, throughput 0.77444
Reading from 12373: heap size 1254 MB, throughput 0.954628
Reading from 12374: heap size 475 MB, throughput 0.965914
Reading from 12373: heap size 1257 MB, throughput 0.953472
Reading from 12374: heap size 477 MB, throughput 0.991707
Equal recommendation: 4461 MB each
Reading from 12373: heap size 1265 MB, throughput 0.959616
Reading from 12374: heap size 483 MB, throughput 0.990938
Reading from 12373: heap size 1267 MB, throughput 0.947159
Reading from 12374: heap size 484 MB, throughput 0.991006
Reading from 12373: heap size 1275 MB, throughput 0.953654
Reading from 12374: heap size 484 MB, throughput 0.989087
Reading from 12373: heap size 1276 MB, throughput 0.9529
Reading from 12374: heap size 487 MB, throughput 0.98507
Equal recommendation: 4461 MB each
Reading from 12373: heap size 1284 MB, throughput 0.955125
Reading from 12374: heap size 487 MB, throughput 0.986461
Reading from 12373: heap size 1285 MB, throughput 0.963779
Reading from 12374: heap size 488 MB, throughput 0.986803
Reading from 12373: heap size 1293 MB, throughput 0.954302
Reading from 12374: heap size 491 MB, throughput 0.987145
Reading from 12374: heap size 492 MB, throughput 0.893943
Reading from 12374: heap size 494 MB, throughput 0.92528
Reading from 12373: heap size 1294 MB, throughput 0.959758
Equal recommendation: 4461 MB each
Reading from 12374: heap size 495 MB, throughput 0.993102
Reading from 12373: heap size 1302 MB, throughput 0.953584
Reading from 12374: heap size 500 MB, throughput 0.991436
Reading from 12373: heap size 1302 MB, throughput 0.954029
Reading from 12374: heap size 501 MB, throughput 0.990419
Reading from 12373: heap size 1311 MB, throughput 0.947734
Reading from 12374: heap size 500 MB, throughput 0.989602
Reading from 12373: heap size 1311 MB, throughput 0.952778
Equal recommendation: 4461 MB each
Reading from 12374: heap size 503 MB, throughput 0.989132
Reading from 12373: heap size 1321 MB, throughput 0.542579
Reading from 12374: heap size 504 MB, throughput 0.985057
Reading from 12373: heap size 1434 MB, throughput 0.938368
Reading from 12374: heap size 504 MB, throughput 0.992766
Reading from 12374: heap size 504 MB, throughput 0.970412
Reading from 12374: heap size 506 MB, throughput 0.884041
Reading from 12373: heap size 1435 MB, throughput 0.979689
Equal recommendation: 4461 MB each
Reading from 12374: heap size 511 MB, throughput 0.987694
Reading from 12373: heap size 1444 MB, throughput 0.975919
Reading from 12374: heap size 512 MB, throughput 0.991479
Reading from 12373: heap size 1458 MB, throughput 0.973456
Reading from 12374: heap size 516 MB, throughput 0.990297
Reading from 12373: heap size 1458 MB, throughput 0.972105
Reading from 12374: heap size 517 MB, throughput 0.990398
Equal recommendation: 4461 MB each
Reading from 12373: heap size 1455 MB, throughput 0.980879
Reading from 12374: heap size 516 MB, throughput 0.989229
Reading from 12374: heap size 518 MB, throughput 0.987422
Reading from 12374: heap size 520 MB, throughput 0.993976
Reading from 12374: heap size 521 MB, throughput 0.97388
Reading from 12374: heap size 521 MB, throughput 0.911746
Equal recommendation: 4461 MB each
Reading from 12374: heap size 523 MB, throughput 0.988539
Reading from 12374: heap size 529 MB, throughput 0.99234
Reading from 12374: heap size 530 MB, throughput 0.988553
Reading from 12373: heap size 1460 MB, throughput 0.984478
Reading from 12374: heap size 530 MB, throughput 0.991063
Equal recommendation: 4461 MB each
Reading from 12374: heap size 532 MB, throughput 0.98937
Reading from 12374: heap size 533 MB, throughput 0.988589
Reading from 12374: heap size 533 MB, throughput 0.993125
Reading from 12374: heap size 536 MB, throughput 0.916507
Reading from 12374: heap size 536 MB, throughput 0.963828
Equal recommendation: 4461 MB each
Reading from 12373: heap size 1425 MB, throughput 0.989494
Reading from 12374: heap size 544 MB, throughput 0.992593
Reading from 12373: heap size 1462 MB, throughput 0.978391
Reading from 12373: heap size 1477 MB, throughput 0.804876
Reading from 12374: heap size 544 MB, throughput 0.990443
Reading from 12373: heap size 1495 MB, throughput 0.651023
Reading from 12373: heap size 1516 MB, throughput 0.672752
Reading from 12373: heap size 1544 MB, throughput 0.662965
Reading from 12373: heap size 1577 MB, throughput 0.684986
Reading from 12373: heap size 1594 MB, throughput 0.641631
Reading from 12373: heap size 1632 MB, throughput 0.805153
Reading from 12374: heap size 545 MB, throughput 0.988123
Equal recommendation: 4461 MB each
Reading from 12373: heap size 1639 MB, throughput 0.962679
Reading from 12374: heap size 547 MB, throughput 0.987759
Reading from 12373: heap size 1630 MB, throughput 0.968611
Reading from 12374: heap size 547 MB, throughput 0.99039
Reading from 12373: heap size 1645 MB, throughput 0.96977
Reading from 12374: heap size 548 MB, throughput 0.993626
Reading from 12374: heap size 551 MB, throughput 0.92849
Reading from 12373: heap size 1633 MB, throughput 0.964567
Equal recommendation: 4461 MB each
Reading from 12374: heap size 552 MB, throughput 0.971324
Reading from 12373: heap size 1649 MB, throughput 0.965509
Reading from 12374: heap size 560 MB, throughput 0.993331
Reading from 12373: heap size 1631 MB, throughput 0.754018
Reading from 12374: heap size 560 MB, throughput 0.992201
Equal recommendation: 4461 MB each
Reading from 12373: heap size 1620 MB, throughput 0.990467
Reading from 12374: heap size 560 MB, throughput 0.992654
Reading from 12373: heap size 1624 MB, throughput 0.98677
Reading from 12374: heap size 562 MB, throughput 0.990427
Reading from 12373: heap size 1634 MB, throughput 0.984186
Reading from 12374: heap size 564 MB, throughput 0.989902
Equal recommendation: 4461 MB each
Reading from 12373: heap size 1647 MB, throughput 0.98153
Reading from 12374: heap size 564 MB, throughput 0.988202
Reading from 12374: heap size 566 MB, throughput 0.919012
Reading from 12374: heap size 569 MB, throughput 0.992041
Reading from 12373: heap size 1652 MB, throughput 0.983024
Reading from 12374: heap size 574 MB, throughput 0.992837
Reading from 12373: heap size 1639 MB, throughput 0.976915
Equal recommendation: 4461 MB each
Reading from 12374: heap size 575 MB, throughput 0.991704
Reading from 12373: heap size 1650 MB, throughput 0.974941
Reading from 12374: heap size 575 MB, throughput 0.990374
Reading from 12373: heap size 1622 MB, throughput 0.972367
Reading from 12374: heap size 577 MB, throughput 0.990885
Reading from 12373: heap size 1479 MB, throughput 0.971505
Equal recommendation: 4461 MB each
Reading from 12374: heap size 579 MB, throughput 0.992928
Reading from 12374: heap size 579 MB, throughput 0.927931
Reading from 12374: heap size 580 MB, throughput 0.990242
Reading from 12373: heap size 1615 MB, throughput 0.970984
Reading from 12374: heap size 582 MB, throughput 0.993041
Reading from 12373: heap size 1625 MB, throughput 0.967684
Equal recommendation: 4461 MB each
Reading from 12374: heap size 584 MB, throughput 0.992086
Reading from 12373: heap size 1633 MB, throughput 0.966737
Reading from 12374: heap size 585 MB, throughput 0.991952
Reading from 12373: heap size 1634 MB, throughput 0.963008
Reading from 12374: heap size 586 MB, throughput 0.989504
Reading from 12373: heap size 1646 MB, throughput 0.961959
Equal recommendation: 4461 MB each
Reading from 12374: heap size 587 MB, throughput 0.993324
Reading from 12374: heap size 590 MB, throughput 0.930592
Reading from 12374: heap size 590 MB, throughput 0.975998
Reading from 12373: heap size 1653 MB, throughput 0.959548
Reading from 12374: heap size 598 MB, throughput 0.993426
Reading from 12373: heap size 1667 MB, throughput 0.959567
Equal recommendation: 4461 MB each
Reading from 12374: heap size 598 MB, throughput 0.928616
Reading from 12373: heap size 1679 MB, throughput 0.957022
Reading from 12374: heap size 610 MB, throughput 0.995469
Reading from 12373: heap size 1700 MB, throughput 0.960204
Reading from 12374: heap size 612 MB, throughput 0.993894
Equal recommendation: 4461 MB each
Reading from 12373: heap size 1707 MB, throughput 0.95789
Reading from 12374: heap size 618 MB, throughput 0.990519
Reading from 12374: heap size 618 MB, throughput 0.917798
Reading from 12373: heap size 1729 MB, throughput 0.956501
Reading from 12374: heap size 623 MB, throughput 0.993105
Reading from 12374: heap size 626 MB, throughput 0.993153
Equal recommendation: 4461 MB each
Reading from 12374: heap size 624 MB, throughput 0.99206
Reading from 12374: heap size 628 MB, throughput 0.99022
Equal recommendation: 4461 MB each
Reading from 12374: heap size 631 MB, throughput 0.99464
Reading from 12374: heap size 633 MB, throughput 0.936502
Client 12374 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 12373: heap size 1734 MB, throughput 0.996746
Recommendation: one client; give it all the memory
Reading from 12373: heap size 1726 MB, throughput 0.986553
Reading from 12373: heap size 1743 MB, throughput 0.827921
Reading from 12373: heap size 1731 MB, throughput 0.812005
Reading from 12373: heap size 1758 MB, throughput 0.801181
Client 12373 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
