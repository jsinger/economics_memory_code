#include <unistd.h>
#include "ReadingSender.hpp"
#include "Utilities.hpp"

economem::ReadingSender::ReadingSender(const char* daemonName) : SendSock(), m_DaemonName(daemonName) {}

bool economem::ReadingSender::sendReading(unsigned long long heapSizeMB, double throughput)
{
    return sendMsg(economem::READINGTYPE_READING, heapSizeMB, throughput);
}

bool economem::ReadingSender::sendMinHeapMB(unsigned long long minHeapMB)
{
    return sendMsg(economem::READINGTYPE_MINHEAP, minHeapMB, 0.0);
}

bool economem::ReadingSender::sendDeathNotification()
{
    return sendMsg(economem::READINGTYPE_DEATH, 0, 0.0);
}

bool economem::ReadingSender::forwardExistingReading(const economem::Reading& reading)
{
    return sendToDaemon(economem::TYPE_READING, m_DaemonName, &reading, sizeof(reading));
}

bool economem::ReadingSender::sendMsg(economem::ReadingType t, unsigned long long heapSizeMB, double throughput)
{
    economem::Reading r(t, getpid(), heapSizeMB, throughput);
    return sendToDaemon(economem::TYPE_READING, m_DaemonName, &r, sizeof(r));
}
