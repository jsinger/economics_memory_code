{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, DeriveDataTypeable #-}
{-# OPTIONS_GHC -fno-warn-missing-fields -fno-warn-missing-methods #-}

import Control.Applicative
import Economem.ArgHelper
import Economem.Experiment
import Economem.Files
import Economem.Java
import Economem.MinHeap
import Economem.Records
import Economem.Signals
import System.Console.CmdLib

data Args = Args { inputFile :: FilePath,
                   iterations :: Int,
                   java :: String,
                   auxBenchmarksFile :: FilePath }
          deriving (Typeable, Data, Eq, Show)

instance Attributes Args where
  attributes _ = group "Options" [
    inputFile %> [ Positional 0,
                   Required True,
                   Help "Specifies which benchmarks to run",
                   ArgHelp "INPUT_FILE" ],
    iterations %> [ Short ['i'],
              Long ["iterations"],
              Default (fromIntegral defaultIterations::Int),
              Help "How many iterations of the benchmarks to run",
              ArgHelp "ITERATIONS" ],
    java %> [ Short ['j'],
              Long ["java"],
              Default (javaName defaultJava),
              Help ("Java version to use; available options: " ++ show validJavaNames),
              ArgHelp "JAVA" ],
    auxBenchmarksFile %> [ Short ['a'],
              Long ["aux-benchmarks"],
              Default "",
              Help ("Other benchmarks to run concurrently to the one being measured (e.g. if it needs network comms)"),
              ArgHelp "AUX_BENCHMARKS_FILE" ]]


instance RecordCommand Args where
  mode_summary _ = "Find the smallest heap size at which benchmarks run without crashing"

main :: IO ()
main = getArgs >>= executeR Args {} >>= \opts -> do
  (bms, aux, iters, j) <- validate opts
  e <- newMinHeap bms iters j aux Nothing
  h <- makeSignalHandler
  runExperiment e h

  where
    validate :: Args -> IO (Input BenchmarkThreads, Maybe (Input BenchmarkThreads), Integer, Java)
    validate a = do
      bms <- fromFile <$> existingAbsoluteFile (inputFile a)
      let aux = if auxBenchmarksFile a /= "" then Just (fromFile (auxBenchmarksFile a)) else Nothing
      iters <- fromIntegral <$> positive (iterations a)
      j <- javaFromName (java a)
      return (bms, aux, iters, j)
