economemd
    total memory: 2760 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub44_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 19954: heap size 9 MB, throughput 0.991867
Clients: 1
Client 19954 has a minimum heap size of 276 MB
Reading from 19955: heap size 9 MB, throughput 0.990517
Clients: 2
Client 19955 has a minimum heap size of 276 MB
Reading from 19955: heap size 9 MB, throughput 0.978097
Reading from 19954: heap size 9 MB, throughput 0.978005
Reading from 19955: heap size 9 MB, throughput 0.964001
Reading from 19954: heap size 9 MB, throughput 0.965385
Reading from 19955: heap size 9 MB, throughput 0.948996
Reading from 19954: heap size 9 MB, throughput 0.952763
Reading from 19955: heap size 11 MB, throughput 0.977603
Reading from 19954: heap size 11 MB, throughput 0.981947
Reading from 19955: heap size 11 MB, throughput 0.985825
Reading from 19954: heap size 11 MB, throughput 0.987947
Reading from 19955: heap size 17 MB, throughput 0.949057
Reading from 19954: heap size 17 MB, throughput 0.969062
Reading from 19955: heap size 17 MB, throughput 0.539555
Reading from 19954: heap size 17 MB, throughput 0.527673
Reading from 19955: heap size 30 MB, throughput 0.967608
Reading from 19955: heap size 31 MB, throughput 0.767951
Reading from 19954: heap size 30 MB, throughput 0.978789
Reading from 19954: heap size 31 MB, throughput 0.973668
Reading from 19955: heap size 34 MB, throughput 0.497604
Reading from 19955: heap size 47 MB, throughput 0.83248
Reading from 19954: heap size 33 MB, throughput 0.453034
Reading from 19954: heap size 47 MB, throughput 0.838332
Reading from 19955: heap size 48 MB, throughput 0.827117
Reading from 19954: heap size 49 MB, throughput 0.79333
Reading from 19954: heap size 50 MB, throughput 0.765092
Reading from 19955: heap size 51 MB, throughput 0.190811
Reading from 19955: heap size 76 MB, throughput 0.833922
Reading from 19954: heap size 54 MB, throughput 0.173433
Reading from 19954: heap size 76 MB, throughput 0.640391
Reading from 19955: heap size 76 MB, throughput 0.283015
Reading from 19954: heap size 78 MB, throughput 0.779269
Reading from 19955: heap size 103 MB, throughput 0.720311
Reading from 19955: heap size 104 MB, throughput 0.700552
Reading from 19955: heap size 105 MB, throughput 0.703685
Reading from 19954: heap size 80 MB, throughput 0.187925
Reading from 19955: heap size 108 MB, throughput 0.659703
Reading from 19954: heap size 108 MB, throughput 0.658419
Reading from 19954: heap size 108 MB, throughput 0.762686
Reading from 19954: heap size 110 MB, throughput 0.811879
Reading from 19955: heap size 111 MB, throughput 0.172487
Reading from 19955: heap size 147 MB, throughput 0.683572
Reading from 19954: heap size 113 MB, throughput 0.188982
Reading from 19955: heap size 152 MB, throughput 0.878688
Reading from 19954: heap size 140 MB, throughput 0.774493
Reading from 19955: heap size 154 MB, throughput 0.758018
Reading from 19954: heap size 144 MB, throughput 0.838165
Reading from 19955: heap size 163 MB, throughput 0.802185
Reading from 19954: heap size 146 MB, throughput 0.762289
Reading from 19955: heap size 165 MB, throughput 0.151476
Reading from 19954: heap size 149 MB, throughput 0.152233
Reading from 19955: heap size 208 MB, throughput 0.741222
Reading from 19954: heap size 186 MB, throughput 0.712406
Reading from 19954: heap size 188 MB, throughput 0.650123
Reading from 19955: heap size 211 MB, throughput 0.846597
Reading from 19954: heap size 189 MB, throughput 0.681816
Reading from 19954: heap size 193 MB, throughput 0.629074
Reading from 19954: heap size 201 MB, throughput 0.915719
Reading from 19955: heap size 213 MB, throughput 0.482842
Reading from 19954: heap size 205 MB, throughput 0.303891
Reading from 19955: heap size 264 MB, throughput 0.768876
Reading from 19954: heap size 252 MB, throughput 0.696267
Reading from 19955: heap size 267 MB, throughput 0.788175
Reading from 19954: heap size 254 MB, throughput 0.839453
Reading from 19955: heap size 269 MB, throughput 0.898593
Reading from 19954: heap size 258 MB, throughput 0.770174
Reading from 19955: heap size 270 MB, throughput 0.855217
Reading from 19954: heap size 258 MB, throughput 0.696691
Reading from 19955: heap size 271 MB, throughput 0.751999
Reading from 19954: heap size 262 MB, throughput 0.794359
Reading from 19955: heap size 271 MB, throughput 0.874555
Reading from 19954: heap size 262 MB, throughput 0.83638
Reading from 19955: heap size 272 MB, throughput 0.771892
Reading from 19954: heap size 266 MB, throughput 0.764924
Reading from 19955: heap size 277 MB, throughput 0.677996
Reading from 19954: heap size 267 MB, throughput 0.630124
Reading from 19955: heap size 280 MB, throughput 0.64668
Reading from 19954: heap size 273 MB, throughput 0.656286
Reading from 19955: heap size 285 MB, throughput 0.854659
Reading from 19954: heap size 276 MB, throughput 0.744201
Reading from 19955: heap size 287 MB, throughput 0.915573
Reading from 19954: heap size 282 MB, throughput 0.727152
Reading from 19955: heap size 288 MB, throughput 0.929822
Reading from 19954: heap size 283 MB, throughput 0.908385
Reading from 19955: heap size 291 MB, throughput 0.648115
Reading from 19954: heap size 287 MB, throughput 0.689421
Reading from 19955: heap size 297 MB, throughput 0.727061
Reading from 19955: heap size 298 MB, throughput 0.751483
Reading from 19955: heap size 300 MB, throughput 0.935917
Reading from 19955: heap size 301 MB, throughput 0.896097
Reading from 19955: heap size 305 MB, throughput 0.856632
Reading from 19954: heap size 287 MB, throughput 0.102073
Reading from 19955: heap size 306 MB, throughput 0.720147
Reading from 19954: heap size 333 MB, throughput 0.748932
Reading from 19955: heap size 312 MB, throughput 0.756221
Reading from 19954: heap size 333 MB, throughput 0.90222
Reading from 19955: heap size 312 MB, throughput 0.61604
Reading from 19955: heap size 319 MB, throughput 0.670093
Reading from 19954: heap size 338 MB, throughput 0.900402
Reading from 19954: heap size 339 MB, throughput 0.885792
Reading from 19954: heap size 337 MB, throughput 0.764882
Reading from 19955: heap size 319 MB, throughput 0.900343
Reading from 19954: heap size 340 MB, throughput 0.629959
Reading from 19954: heap size 345 MB, throughput 0.785324
Reading from 19954: heap size 346 MB, throughput 0.589932
Reading from 19954: heap size 353 MB, throughput 0.615378
Equal recommendation: 1380 MB each
Reading from 19954: heap size 355 MB, throughput 0.937994
Reading from 19955: heap size 327 MB, throughput 0.97344
Reading from 19954: heap size 360 MB, throughput 0.983066
Reading from 19955: heap size 328 MB, throughput 0.967295
Reading from 19954: heap size 363 MB, throughput 0.973621
Reading from 19955: heap size 328 MB, throughput 0.963604
Reading from 19954: heap size 362 MB, throughput 0.976001
Reading from 19955: heap size 331 MB, throughput 0.666479
Reading from 19954: heap size 365 MB, throughput 0.975691
Reading from 19955: heap size 375 MB, throughput 0.970692
Reading from 19954: heap size 368 MB, throughput 0.973532
Reading from 19955: heap size 377 MB, throughput 0.982801
Reading from 19954: heap size 369 MB, throughput 0.972857
Equal recommendation: 1380 MB each
Reading from 19955: heap size 378 MB, throughput 0.969498
Reading from 19954: heap size 367 MB, throughput 0.949926
Reading from 19954: heap size 370 MB, throughput 0.969525
Reading from 19955: heap size 380 MB, throughput 0.98051
Reading from 19954: heap size 367 MB, throughput 0.97045
Reading from 19955: heap size 378 MB, throughput 0.984243
Reading from 19954: heap size 369 MB, throughput 0.971088
Reading from 19955: heap size 380 MB, throughput 0.980629
Reading from 19954: heap size 372 MB, throughput 0.981031
Reading from 19955: heap size 380 MB, throughput 0.979942
Reading from 19954: heap size 372 MB, throughput 0.971778
Reading from 19955: heap size 381 MB, throughput 0.967706
Reading from 19954: heap size 374 MB, throughput 0.969797
Reading from 19955: heap size 383 MB, throughput 0.967542
Equal recommendation: 1380 MB each
Reading from 19954: heap size 375 MB, throughput 0.982646
Reading from 19954: heap size 376 MB, throughput 0.964548
Reading from 19955: heap size 384 MB, throughput 0.980434
Reading from 19954: heap size 377 MB, throughput 0.716878
Reading from 19954: heap size 373 MB, throughput 0.797368
Reading from 19955: heap size 388 MB, throughput 0.876138
Reading from 19954: heap size 378 MB, throughput 0.716698
Reading from 19955: heap size 388 MB, throughput 0.806018
Reading from 19955: heap size 395 MB, throughput 0.858297
Reading from 19954: heap size 386 MB, throughput 0.939665
Reading from 19955: heap size 398 MB, throughput 0.701386
Reading from 19955: heap size 406 MB, throughput 0.96717
Reading from 19954: heap size 388 MB, throughput 0.986503
Reading from 19955: heap size 408 MB, throughput 0.983225
Reading from 19954: heap size 392 MB, throughput 0.98458
Reading from 19955: heap size 410 MB, throughput 0.986991
Reading from 19954: heap size 394 MB, throughput 0.986144
Reading from 19955: heap size 412 MB, throughput 0.983411
Reading from 19954: heap size 394 MB, throughput 0.984727
Reading from 19955: heap size 411 MB, throughput 0.986123
Equal recommendation: 1380 MB each
Reading from 19954: heap size 396 MB, throughput 0.984587
Reading from 19955: heap size 414 MB, throughput 0.985691
Reading from 19954: heap size 395 MB, throughput 0.977211
Reading from 19955: heap size 411 MB, throughput 0.984123
Reading from 19954: heap size 397 MB, throughput 0.97747
Reading from 19955: heap size 414 MB, throughput 0.982567
Reading from 19954: heap size 394 MB, throughput 0.795021
Reading from 19955: heap size 413 MB, throughput 0.982027
Reading from 19954: heap size 433 MB, throughput 0.994745
Reading from 19955: heap size 414 MB, throughput 0.985339
Reading from 19954: heap size 431 MB, throughput 0.991838
Reading from 19955: heap size 416 MB, throughput 0.969053
Equal recommendation: 1380 MB each
Reading from 19955: heap size 417 MB, throughput 0.987369
Reading from 19955: heap size 417 MB, throughput 0.885473
Reading from 19954: heap size 434 MB, throughput 0.990792
Reading from 19955: heap size 418 MB, throughput 0.857275
Reading from 19955: heap size 424 MB, throughput 0.844719
Reading from 19954: heap size 436 MB, throughput 0.983697
Reading from 19954: heap size 437 MB, throughput 0.891152
Reading from 19954: heap size 435 MB, throughput 0.883908
Reading from 19954: heap size 438 MB, throughput 0.861096
Reading from 19955: heap size 425 MB, throughput 0.984373
Reading from 19954: heap size 444 MB, throughput 0.987656
Reading from 19955: heap size 433 MB, throughput 0.9929
Reading from 19954: heap size 446 MB, throughput 0.992528
Reading from 19955: heap size 434 MB, throughput 0.98727
Reading from 19954: heap size 445 MB, throughput 0.988184
Reading from 19955: heap size 436 MB, throughput 0.987596
Equal recommendation: 1380 MB each
Reading from 19954: heap size 448 MB, throughput 0.983335
Reading from 19955: heap size 438 MB, throughput 0.986351
Reading from 19954: heap size 444 MB, throughput 0.987967
Reading from 19955: heap size 437 MB, throughput 0.9892
Reading from 19954: heap size 447 MB, throughput 0.985394
Reading from 19955: heap size 439 MB, throughput 0.985544
Reading from 19954: heap size 446 MB, throughput 0.984645
Reading from 19955: heap size 438 MB, throughput 0.985275
Reading from 19954: heap size 448 MB, throughput 0.979237
Reading from 19954: heap size 450 MB, throughput 0.981169
Equal recommendation: 1380 MB each
Reading from 19955: heap size 439 MB, throughput 0.987399
Reading from 19954: heap size 451 MB, throughput 0.979845
Reading from 19955: heap size 441 MB, throughput 0.989179
Reading from 19955: heap size 441 MB, throughput 0.909282
Reading from 19955: heap size 441 MB, throughput 0.884775
Reading from 19955: heap size 443 MB, throughput 0.913081
Reading from 19954: heap size 453 MB, throughput 0.991333
Reading from 19954: heap size 454 MB, throughput 0.896946
Reading from 19954: heap size 455 MB, throughput 0.859437
Reading from 19954: heap size 457 MB, throughput 0.917898
Reading from 19955: heap size 450 MB, throughput 0.991102
Reading from 19954: heap size 465 MB, throughput 0.990018
Reading from 19955: heap size 451 MB, throughput 0.991099
Reading from 19954: heap size 465 MB, throughput 0.990013
Reading from 19955: heap size 453 MB, throughput 0.99116
Equal recommendation: 1380 MB each
Reading from 19954: heap size 467 MB, throughput 0.991619
Reading from 19955: heap size 455 MB, throughput 0.988615
Reading from 19954: heap size 469 MB, throughput 0.989828
Reading from 19955: heap size 454 MB, throughput 0.988116
Reading from 19954: heap size 468 MB, throughput 0.988109
Reading from 19955: heap size 456 MB, throughput 0.986951
Reading from 19954: heap size 470 MB, throughput 0.984993
Reading from 19955: heap size 457 MB, throughput 0.984902
Reading from 19954: heap size 470 MB, throughput 0.986314
Equal recommendation: 1380 MB each
Reading from 19955: heap size 457 MB, throughput 0.980073
Reading from 19954: heap size 471 MB, throughput 0.984414
Reading from 19955: heap size 456 MB, throughput 0.972885
Reading from 19955: heap size 461 MB, throughput 0.848965
Reading from 19955: heap size 467 MB, throughput 0.856868
Reading from 19954: heap size 474 MB, throughput 0.991463
Reading from 19955: heap size 470 MB, throughput 0.983129
Reading from 19954: heap size 474 MB, throughput 0.943038
Reading from 19954: heap size 474 MB, throughput 0.890838
Reading from 19954: heap size 477 MB, throughput 0.926326
Reading from 19955: heap size 475 MB, throughput 0.991638
Reading from 19954: heap size 485 MB, throughput 0.991053
Reading from 19955: heap size 478 MB, throughput 0.988682
Reading from 19954: heap size 485 MB, throughput 0.991013
Equal recommendation: 1380 MB each
Reading from 19955: heap size 477 MB, throughput 0.990304
Reading from 19954: heap size 487 MB, throughput 0.991204
Reading from 19955: heap size 479 MB, throughput 0.982339
Reading from 19954: heap size 489 MB, throughput 0.985264
Reading from 19955: heap size 482 MB, throughput 0.988647
Reading from 19954: heap size 487 MB, throughput 0.988173
Reading from 19955: heap size 483 MB, throughput 0.983691
Reading from 19954: heap size 489 MB, throughput 0.988458
Equal recommendation: 1380 MB each
Reading from 19955: heap size 486 MB, throughput 0.989978
Reading from 19954: heap size 491 MB, throughput 0.987405
Reading from 19955: heap size 486 MB, throughput 0.730397
Reading from 19955: heap size 531 MB, throughput 0.995351
Reading from 19955: heap size 532 MB, throughput 0.992068
Reading from 19954: heap size 491 MB, throughput 0.994537
Reading from 19954: heap size 494 MB, throughput 0.943734
Reading from 19954: heap size 495 MB, throughput 0.863363
Reading from 19955: heap size 537 MB, throughput 0.995444
Reading from 19954: heap size 500 MB, throughput 0.979194
Reading from 19955: heap size 541 MB, throughput 0.995954
Reading from 19954: heap size 502 MB, throughput 0.992197
Equal recommendation: 1380 MB each
Reading from 19955: heap size 542 MB, throughput 0.994391
Reading from 19954: heap size 505 MB, throughput 0.993257
Reading from 19955: heap size 543 MB, throughput 0.990803
Reading from 19954: heap size 507 MB, throughput 0.990629
Reading from 19955: heap size 536 MB, throughput 0.986823
Reading from 19954: heap size 504 MB, throughput 0.990835
Reading from 19955: heap size 507 MB, throughput 0.986194
Reading from 19954: heap size 507 MB, throughput 0.988656
Equal recommendation: 1380 MB each
Reading from 19955: heap size 539 MB, throughput 0.992355
Reading from 19954: heap size 510 MB, throughput 0.987881
Reading from 19955: heap size 539 MB, throughput 0.973145
Reading from 19955: heap size 534 MB, throughput 0.886938
Reading from 19955: heap size 539 MB, throughput 0.918314
Reading from 19954: heap size 510 MB, throughput 0.99145
Reading from 19954: heap size 512 MB, throughput 0.97127
Reading from 19954: heap size 513 MB, throughput 0.893838
Reading from 19955: heap size 548 MB, throughput 0.990938
Reading from 19954: heap size 517 MB, throughput 0.986421
Reading from 19955: heap size 549 MB, throughput 0.992009
Reading from 19954: heap size 519 MB, throughput 0.992124
Equal recommendation: 1380 MB each
Reading from 19955: heap size 548 MB, throughput 0.988522
Reading from 19954: heap size 522 MB, throughput 0.994019
Reading from 19955: heap size 551 MB, throughput 0.992014
Reading from 19954: heap size 523 MB, throughput 0.990951
Reading from 19955: heap size 555 MB, throughput 0.987497
Reading from 19954: heap size 521 MB, throughput 0.987977
Reading from 19955: heap size 555 MB, throughput 0.985138
Equal recommendation: 1380 MB each
Reading from 19954: heap size 524 MB, throughput 0.989354
Reading from 19955: heap size 559 MB, throughput 0.991285
Reading from 19955: heap size 561 MB, throughput 0.912399
Reading from 19955: heap size 560 MB, throughput 0.8873
Reading from 19954: heap size 526 MB, throughput 0.987786
Reading from 19955: heap size 562 MB, throughput 0.989619
Reading from 19954: heap size 526 MB, throughput 0.987522
Reading from 19954: heap size 528 MB, throughput 0.899279
Reading from 19954: heap size 530 MB, throughput 0.976431
Reading from 19955: heap size 569 MB, throughput 0.99182
Reading from 19954: heap size 538 MB, throughput 0.992142
Equal recommendation: 1380 MB each
Reading from 19955: heap size 571 MB, throughput 0.991191
Reading from 19954: heap size 538 MB, throughput 0.992913
Reading from 19955: heap size 572 MB, throughput 0.990528
Reading from 19954: heap size 540 MB, throughput 0.992022
Reading from 19955: heap size 575 MB, throughput 0.991507
Reading from 19954: heap size 541 MB, throughput 0.990931
Reading from 19955: heap size 572 MB, throughput 0.989742
Equal recommendation: 1380 MB each
Reading from 19954: heap size 540 MB, throughput 0.988651
Reading from 19955: heap size 575 MB, throughput 0.991284
Reading from 19955: heap size 576 MB, throughput 0.924625
Reading from 19955: heap size 578 MB, throughput 0.967755
Reading from 19954: heap size 542 MB, throughput 0.986059
Reading from 19954: heap size 542 MB, throughput 0.979483
Reading from 19954: heap size 545 MB, throughput 0.912788
Reading from 19955: heap size 586 MB, throughput 0.993691
Reading from 19954: heap size 548 MB, throughput 0.990554
Reading from 19955: heap size 586 MB, throughput 0.991343
Equal recommendation: 1380 MB each
Reading from 19954: heap size 550 MB, throughput 0.991878
Reading from 19955: heap size 587 MB, throughput 0.993793
Reading from 19954: heap size 552 MB, throughput 0.991849
Reading from 19955: heap size 589 MB, throughput 0.991384
Reading from 19954: heap size 554 MB, throughput 0.993719
Reading from 19955: heap size 589 MB, throughput 0.989916
Equal recommendation: 1380 MB each
Reading from 19954: heap size 552 MB, throughput 0.987428
Reading from 19955: heap size 590 MB, throughput 0.992321
Reading from 19955: heap size 594 MB, throughput 0.931442
Reading from 19954: heap size 554 MB, throughput 0.98998
Reading from 19955: heap size 594 MB, throughput 0.981198
Reading from 19954: heap size 556 MB, throughput 0.992714
Reading from 19954: heap size 557 MB, throughput 0.937243
Reading from 19954: heap size 558 MB, throughput 0.982431
Reading from 19955: heap size 601 MB, throughput 0.995406
Equal recommendation: 1380 MB each
Reading from 19954: heap size 560 MB, throughput 0.992871
Reading from 19955: heap size 602 MB, throughput 0.991889
Reading from 19954: heap size 561 MB, throughput 0.991864
Reading from 19955: heap size 602 MB, throughput 0.991609
Reading from 19954: heap size 563 MB, throughput 0.993208
Reading from 19955: heap size 604 MB, throughput 0.990748
Equal recommendation: 1380 MB each
Reading from 19954: heap size 561 MB, throughput 0.991245
Reading from 19955: heap size 605 MB, throughput 0.986127
Reading from 19955: heap size 606 MB, throughput 0.980936
Reading from 19954: heap size 563 MB, throughput 0.989186
Reading from 19955: heap size 612 MB, throughput 0.94171
Reading from 19955: heap size 614 MB, throughput 0.988629
Reading from 19954: heap size 565 MB, throughput 0.993393
Reading from 19954: heap size 566 MB, throughput 0.928301
Reading from 19954: heap size 565 MB, throughput 0.984724
Reading from 19955: heap size 619 MB, throughput 0.993292
Equal recommendation: 1380 MB each
Reading from 19954: heap size 568 MB, throughput 0.992614
Reading from 19955: heap size 621 MB, throughput 0.992423
Reading from 19954: heap size 570 MB, throughput 0.990461
Reading from 19955: heap size 621 MB, throughput 0.986683
Reading from 19954: heap size 571 MB, throughput 0.991445
Equal recommendation: 1380 MB each
Reading from 19955: heap size 623 MB, throughput 0.989211
Reading from 19954: heap size 570 MB, throughput 0.991139
Reading from 19955: heap size 625 MB, throughput 0.989685
Reading from 19955: heap size 626 MB, throughput 0.927715
Reading from 19954: heap size 572 MB, throughput 0.991788
Reading from 19955: heap size 630 MB, throughput 0.992795
Reading from 19954: heap size 573 MB, throughput 0.990395
Reading from 19954: heap size 575 MB, throughput 0.921937
Reading from 19954: heap size 578 MB, throughput 0.988821
Reading from 19955: heap size 631 MB, throughput 0.995073
Equal recommendation: 1380 MB each
Reading from 19954: heap size 580 MB, throughput 0.992788
Reading from 19955: heap size 634 MB, throughput 0.992673
Reading from 19954: heap size 582 MB, throughput 0.991951
Reading from 19955: heap size 636 MB, throughput 0.99169
Reading from 19954: heap size 584 MB, throughput 0.990735
Equal recommendation: 1380 MB each
Reading from 19955: heap size 636 MB, throughput 0.991228
Reading from 19954: heap size 582 MB, throughput 0.990988
Reading from 19955: heap size 637 MB, throughput 0.986378
Reading from 19955: heap size 640 MB, throughput 0.933629
Reading from 19954: heap size 584 MB, throughput 0.993758
Reading from 19955: heap size 642 MB, throughput 0.993198
Reading from 19954: heap size 586 MB, throughput 0.931946
Reading from 19954: heap size 587 MB, throughput 0.962393
Equal recommendation: 1380 MB each
Reading from 19955: heap size 648 MB, throughput 0.993648
Reading from 19954: heap size 596 MB, throughput 0.993979
Reading from 19954: heap size 596 MB, throughput 0.986362
Reading from 19955: heap size 649 MB, throughput 0.988847
Reading from 19954: heap size 596 MB, throughput 0.988935
Reading from 19955: heap size 650 MB, throughput 0.99212
Equal recommendation: 1380 MB each
Reading from 19954: heap size 598 MB, throughput 0.989562
Reading from 19955: heap size 652 MB, throughput 0.996042
Reading from 19955: heap size 653 MB, throughput 0.957133
Client 19955 died
Clients: 1
Reading from 19954: heap size 601 MB, throughput 0.995685
Reading from 19954: heap size 601 MB, throughput 0.979774
Client 19954 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
