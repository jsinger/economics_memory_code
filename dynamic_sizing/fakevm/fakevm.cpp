#include <DaemonProxy.hpp>
#include <ReadingSender.hpp>
#include <SignalHandler.hpp>
#include <Thread.hpp>
#include <Timer.hpp>
#include <Utilities.hpp>
#include <unistd.h>
#include "Args.hpp"
#include "ProgramSpec.hpp"
#include "SimpleHeap.hpp"
#include "FakeVMSimpleThroughput.hpp"

class MutatorThread : public economem::Thread
{
public:
    MutatorThread(Program& p, Heap& h) : Thread(), m_Program(p), m_Heap(h) {}
private:
    virtual void threadFunc();
    Program& m_Program;
    Heap& m_Heap;
};


int main(int argc, char** argv)
{
    Args::parse(argc, argv);

    Program* p = Program::parseFile(Args::getProgramFile());
    if (p == NULL)
        economem::die("Failed to parse program");
    if (economem::isInfoVerbose())
        p->print();

    economem::DaemonProxy proxy;
    SimpleHeap h(Args::getInitialHeapSizeMB(), p->getMinHeapMB(), proxy);
    MutatorThread t(*p, h);

    economem::SignalHandler::init([&] () { t.kill(); });
    proxy.addTarget(new FakeVMSimpleThroughput([&] () { return h.getCapacityMB(); }), new economem::ReadingSender(Args::getReceiverName()));

    if (!t.start())
        economem::die("Failed to start mutator thread");

    t.wait();

    delete p;
    return 0;
}

void MutatorThread::threadFunc()
{
    const Phase* phase = m_Program.getCurrentPhase();
    double length = phase->getLengthWithRandomError();

    economem::info("MutatorThread", "starting");

    economem::Timer timer(length);
    printf("Starting in phase '%s'; allocation rate %u B/s (%g MB/s). Will transition in %g s\n", phase->getName().c_str(), phase->getAllocRateBytes(), phase->getAllocRateMB(), length);

    while (isAlive())
    {
        sleep(1);
        m_Heap.alloc(phase->getAllocRateBytes());
        if (timer.hasElapsed())
        {
            m_Program.changePhase();
            phase = m_Program.getCurrentPhase();
            length = phase->getLengthWithRandomError();
            timer.setTimer(length);
            printf("Phase transitioned to '%s'; allocation rate %u B/s (%g MB/s). Will transition in %g s\n", phase->getName().c_str(), phase->getAllocRateBytes(), phase->getAllocRateMB(), length);
        }
    }

    economem::info("MutatorThread", "finished");
}
