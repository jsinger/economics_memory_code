economemd
    total memory: 5948 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub3_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 417: heap size 9 MB, throughput 0.992588
Clients: 1
Client 417 has a minimum heap size of 276 MB
Reading from 416: heap size 9 MB, throughput 0.992027
Clients: 2
Client 416 has a minimum heap size of 1211 MB
Reading from 416: heap size 9 MB, throughput 0.977723
Reading from 417: heap size 9 MB, throughput 0.980762
Reading from 417: heap size 9 MB, throughput 0.962647
Reading from 416: heap size 9 MB, throughput 0.965035
Reading from 417: heap size 9 MB, throughput 0.960393
Reading from 416: heap size 9 MB, throughput 0.956268
Reading from 417: heap size 11 MB, throughput 0.985372
Reading from 416: heap size 11 MB, throughput 0.986225
Reading from 417: heap size 11 MB, throughput 0.98664
Reading from 416: heap size 11 MB, throughput 0.987205
Reading from 417: heap size 17 MB, throughput 0.95883
Reading from 416: heap size 17 MB, throughput 0.95096
Reading from 417: heap size 17 MB, throughput 0.40001
Reading from 416: heap size 17 MB, throughput 0.597429
Reading from 417: heap size 30 MB, throughput 0.840472
Reading from 416: heap size 30 MB, throughput 0.861982
Reading from 417: heap size 31 MB, throughput 0.87708
Reading from 416: heap size 31 MB, throughput 0.965356
Reading from 416: heap size 35 MB, throughput 0.463631
Reading from 417: heap size 35 MB, throughput 0.49188
Reading from 417: heap size 48 MB, throughput 0.84263
Reading from 416: heap size 47 MB, throughput 0.878685
Reading from 416: heap size 50 MB, throughput 0.915719
Reading from 417: heap size 52 MB, throughput 0.316177
Reading from 417: heap size 65 MB, throughput 0.7688
Reading from 416: heap size 51 MB, throughput 0.285968
Reading from 417: heap size 72 MB, throughput 0.618954
Reading from 416: heap size 75 MB, throughput 0.781406
Reading from 417: heap size 74 MB, throughput 0.234002
Reading from 416: heap size 75 MB, throughput 0.271502
Reading from 417: heap size 106 MB, throughput 0.801518
Reading from 416: heap size 102 MB, throughput 0.841374
Reading from 416: heap size 103 MB, throughput 0.766627
Reading from 416: heap size 104 MB, throughput 0.691962
Reading from 416: heap size 107 MB, throughput 0.769858
Reading from 417: heap size 106 MB, throughput 0.286837
Reading from 417: heap size 138 MB, throughput 0.660999
Reading from 417: heap size 138 MB, throughput 0.759557
Reading from 416: heap size 110 MB, throughput 0.217129
Reading from 416: heap size 143 MB, throughput 0.630348
Reading from 416: heap size 147 MB, throughput 0.807411
Reading from 416: heap size 150 MB, throughput 0.677103
Reading from 417: heap size 142 MB, throughput 0.216069
Reading from 416: heap size 159 MB, throughput 0.766988
Reading from 417: heap size 176 MB, throughput 0.621889
Reading from 416: heap size 161 MB, throughput 0.671909
Reading from 417: heap size 183 MB, throughput 0.774411
Reading from 417: heap size 185 MB, throughput 0.756581
Reading from 417: heap size 195 MB, throughput 0.772753
Reading from 416: heap size 164 MB, throughput 0.151252
Reading from 416: heap size 207 MB, throughput 0.666209
Reading from 416: heap size 211 MB, throughput 0.688707
Reading from 416: heap size 216 MB, throughput 0.66016
Reading from 416: heap size 219 MB, throughput 0.498234
Reading from 416: heap size 228 MB, throughput 0.669106
Reading from 416: heap size 235 MB, throughput 0.523676
Reading from 417: heap size 198 MB, throughput 0.459484
Reading from 417: heap size 243 MB, throughput 0.898522
Reading from 416: heap size 243 MB, throughput 0.0803408
Reading from 417: heap size 250 MB, throughput 0.630174
Reading from 416: heap size 293 MB, throughput 0.543375
Reading from 417: heap size 256 MB, throughput 0.77197
Reading from 417: heap size 260 MB, throughput 0.696789
Reading from 416: heap size 294 MB, throughput 0.1408
Reading from 417: heap size 267 MB, throughput 0.863665
Reading from 416: heap size 337 MB, throughput 0.65582
Reading from 417: heap size 270 MB, throughput 0.776303
Reading from 416: heap size 340 MB, throughput 0.621616
Reading from 417: heap size 273 MB, throughput 0.716678
Reading from 416: heap size 334 MB, throughput 0.762245
Reading from 417: heap size 279 MB, throughput 0.727915
Reading from 416: heap size 338 MB, throughput 0.705826
Reading from 417: heap size 286 MB, throughput 0.749576
Reading from 417: heap size 290 MB, throughput 0.608727
Reading from 416: heap size 340 MB, throughput 0.550642
Reading from 416: heap size 344 MB, throughput 0.653015
Reading from 416: heap size 347 MB, throughput 0.625574
Reading from 416: heap size 357 MB, throughput 0.597414
Reading from 416: heap size 360 MB, throughput 0.541243
Reading from 417: heap size 297 MB, throughput 0.918302
Reading from 416: heap size 372 MB, throughput 0.488788
Reading from 417: heap size 299 MB, throughput 0.625562
Reading from 416: heap size 378 MB, throughput 0.424017
Reading from 417: heap size 306 MB, throughput 0.619968
Reading from 417: heap size 309 MB, throughput 0.590447
Reading from 416: heap size 387 MB, throughput 0.457211
Reading from 416: heap size 392 MB, throughput 0.0453675
Reading from 417: heap size 308 MB, throughput 0.161117
Reading from 416: heap size 445 MB, throughput 0.316468
Reading from 417: heap size 359 MB, throughput 0.765035
Reading from 417: heap size 365 MB, throughput 0.800401
Reading from 417: heap size 366 MB, throughput 0.514473
Reading from 417: heap size 363 MB, throughput 0.775843
Reading from 416: heap size 435 MB, throughput 0.0833609
Equal recommendation: 2974 MB each
Reading from 417: heap size 366 MB, throughput 0.526503
Reading from 416: heap size 426 MB, throughput 0.303511
Reading from 417: heap size 367 MB, throughput 0.765526
Reading from 416: heap size 494 MB, throughput 0.500287
Reading from 416: heap size 496 MB, throughput 0.421644
Reading from 416: heap size 485 MB, throughput 0.615797
Reading from 416: heap size 491 MB, throughput 0.51508
Reading from 417: heap size 369 MB, throughput 0.936598
Reading from 416: heap size 481 MB, throughput 0.0893975
Reading from 416: heap size 547 MB, throughput 0.54896
Reading from 416: heap size 541 MB, throughput 0.502374
Reading from 416: heap size 544 MB, throughput 0.57009
Reading from 416: heap size 542 MB, throughput 0.56355
Reading from 416: heap size 544 MB, throughput 0.511221
Reading from 416: heap size 546 MB, throughput 0.593153
Reading from 417: heap size 376 MB, throughput 0.987077
Reading from 416: heap size 551 MB, throughput 0.58385
Reading from 416: heap size 556 MB, throughput 0.510447
Reading from 416: heap size 567 MB, throughput 0.576254
Reading from 416: heap size 571 MB, throughput 0.500844
Reading from 416: heap size 583 MB, throughput 0.473428
Reading from 416: heap size 589 MB, throughput 0.47069
Reading from 416: heap size 598 MB, throughput 0.431117
Reading from 417: heap size 377 MB, throughput 0.955044
Reading from 416: heap size 603 MB, throughput 0.054306
Reading from 416: heap size 669 MB, throughput 0.394155
Reading from 416: heap size 662 MB, throughput 0.7935
Reading from 417: heap size 380 MB, throughput 0.966433
Reading from 416: heap size 596 MB, throughput 0.78716
Reading from 416: heap size 668 MB, throughput 0.827312
Reading from 416: heap size 668 MB, throughput 0.796771
Reading from 417: heap size 382 MB, throughput 0.942779
Reading from 416: heap size 670 MB, throughput 0.21044
Reading from 416: heap size 746 MB, throughput 0.312951
Reading from 417: heap size 384 MB, throughput 0.964771
Reading from 416: heap size 737 MB, throughput 0.382755
Reading from 416: heap size 663 MB, throughput 0.681906
Reading from 416: heap size 733 MB, throughput 0.371011
Equal recommendation: 2974 MB each
Reading from 416: heap size 739 MB, throughput 0.0440604
Reading from 416: heap size 812 MB, throughput 0.33662
Reading from 416: heap size 814 MB, throughput 0.49585
Reading from 416: heap size 818 MB, throughput 0.552294
Reading from 416: heap size 819 MB, throughput 0.881991
Reading from 417: heap size 385 MB, throughput 0.969895
Reading from 416: heap size 817 MB, throughput 0.701852
Reading from 416: heap size 821 MB, throughput 0.614266
Reading from 416: heap size 819 MB, throughput 0.569738
Reading from 416: heap size 821 MB, throughput 0.661115
Reading from 417: heap size 382 MB, throughput 0.957604
Reading from 416: heap size 825 MB, throughput 0.169157
Reading from 416: heap size 910 MB, throughput 0.453412
Reading from 416: heap size 915 MB, throughput 0.929495
Reading from 416: heap size 918 MB, throughput 0.90862
Reading from 416: heap size 915 MB, throughput 0.924854
Reading from 416: heap size 770 MB, throughput 0.882863
Reading from 417: heap size 386 MB, throughput 0.975931
Reading from 416: heap size 908 MB, throughput 0.903839
Reading from 416: heap size 816 MB, throughput 0.750322
Reading from 416: heap size 899 MB, throughput 0.740508
Reading from 416: heap size 821 MB, throughput 0.772166
Reading from 416: heap size 894 MB, throughput 0.746079
Reading from 416: heap size 817 MB, throughput 0.817897
Reading from 416: heap size 883 MB, throughput 0.766655
Reading from 416: heap size 890 MB, throughput 0.807825
Reading from 417: heap size 386 MB, throughput 0.972234
Reading from 416: heap size 878 MB, throughput 0.783321
Reading from 416: heap size 884 MB, throughput 0.795024
Reading from 416: heap size 879 MB, throughput 0.837496
Reading from 416: heap size 882 MB, throughput 0.836001
Reading from 417: heap size 387 MB, throughput 0.974746
Reading from 416: heap size 880 MB, throughput 0.978745
Reading from 416: heap size 883 MB, throughput 0.93181
Reading from 416: heap size 887 MB, throughput 0.808247
Reading from 416: heap size 889 MB, throughput 0.783539
Reading from 416: heap size 896 MB, throughput 0.797233
Reading from 417: heap size 391 MB, throughput 0.977285
Reading from 416: heap size 896 MB, throughput 0.781461
Reading from 416: heap size 904 MB, throughput 0.796372
Reading from 416: heap size 904 MB, throughput 0.802363
Equal recommendation: 2974 MB each
Reading from 416: heap size 910 MB, throughput 0.849344
Reading from 416: heap size 911 MB, throughput 0.733622
Reading from 416: heap size 918 MB, throughput 0.865244
Reading from 416: heap size 918 MB, throughput 0.882418
Reading from 416: heap size 923 MB, throughput 0.854394
Reading from 416: heap size 924 MB, throughput 0.68911
Reading from 417: heap size 392 MB, throughput 0.983792
Reading from 417: heap size 391 MB, throughput 0.9436
Reading from 417: heap size 393 MB, throughput 0.729453
Reading from 417: heap size 392 MB, throughput 0.791463
Reading from 417: heap size 397 MB, throughput 0.647139
Reading from 416: heap size 911 MB, throughput 0.0648804
Reading from 416: heap size 1044 MB, throughput 0.480216
Reading from 416: heap size 1043 MB, throughput 0.781373
Reading from 416: heap size 1047 MB, throughput 0.675293
Reading from 416: heap size 1050 MB, throughput 0.725019
Reading from 417: heap size 406 MB, throughput 0.954051
Reading from 416: heap size 1053 MB, throughput 0.686076
Reading from 416: heap size 1059 MB, throughput 0.776834
Reading from 416: heap size 1061 MB, throughput 0.703305
Reading from 416: heap size 1073 MB, throughput 0.701228
Reading from 416: heap size 1073 MB, throughput 0.681851
Reading from 417: heap size 408 MB, throughput 0.984822
Reading from 416: heap size 1086 MB, throughput 0.931399
Reading from 417: heap size 412 MB, throughput 0.970536
Reading from 416: heap size 1090 MB, throughput 0.933762
Equal recommendation: 2974 MB each
Reading from 416: heap size 1106 MB, throughput 0.965881
Reading from 417: heap size 414 MB, throughput 0.986503
Reading from 416: heap size 1108 MB, throughput 0.969186
Reading from 417: heap size 415 MB, throughput 0.987048
Reading from 416: heap size 1111 MB, throughput 0.955592
Reading from 417: heap size 417 MB, throughput 0.985335
Reading from 416: heap size 1115 MB, throughput 0.952394
Reading from 417: heap size 415 MB, throughput 0.982955
Reading from 416: heap size 1112 MB, throughput 0.957543
Reading from 417: heap size 418 MB, throughput 0.978205
Reading from 416: heap size 1117 MB, throughput 0.951097
Reading from 417: heap size 417 MB, throughput 0.982478
Equal recommendation: 2974 MB each
Reading from 416: heap size 1123 MB, throughput 0.951963
Reading from 417: heap size 418 MB, throughput 0.980513
Reading from 416: heap size 1123 MB, throughput 0.943181
Reading from 417: heap size 416 MB, throughput 0.985896
Reading from 417: heap size 420 MB, throughput 0.899143
Reading from 417: heap size 423 MB, throughput 0.879994
Reading from 417: heap size 423 MB, throughput 0.92146
Reading from 416: heap size 1130 MB, throughput 0.948532
Reading from 417: heap size 428 MB, throughput 0.990079
Reading from 416: heap size 1132 MB, throughput 0.963938
Reading from 417: heap size 429 MB, throughput 0.989617
Reading from 416: heap size 1138 MB, throughput 0.959514
Equal recommendation: 2974 MB each
Reading from 417: heap size 432 MB, throughput 0.990225
Reading from 416: heap size 1142 MB, throughput 0.945694
Reading from 416: heap size 1148 MB, throughput 0.945239
Reading from 417: heap size 433 MB, throughput 0.976336
Reading from 416: heap size 1152 MB, throughput 0.949395
Reading from 417: heap size 431 MB, throughput 0.987841
Reading from 416: heap size 1158 MB, throughput 0.951513
Reading from 417: heap size 433 MB, throughput 0.817483
Reading from 416: heap size 1164 MB, throughput 0.952334
Reading from 417: heap size 463 MB, throughput 0.980584
Equal recommendation: 2974 MB each
Reading from 416: heap size 1171 MB, throughput 0.946737
Reading from 417: heap size 465 MB, throughput 0.99035
Reading from 416: heap size 1175 MB, throughput 0.945172
Reading from 417: heap size 470 MB, throughput 0.989954
Reading from 417: heap size 471 MB, throughput 0.916325
Reading from 417: heap size 468 MB, throughput 0.875538
Reading from 416: heap size 1182 MB, throughput 0.945228
Reading from 417: heap size 471 MB, throughput 0.910908
Reading from 416: heap size 1184 MB, throughput 0.963148
Reading from 417: heap size 479 MB, throughput 0.991835
Reading from 416: heap size 1192 MB, throughput 0.960578
Equal recommendation: 2974 MB each
Reading from 417: heap size 480 MB, throughput 0.990552
Reading from 416: heap size 1193 MB, throughput 0.964822
Reading from 417: heap size 479 MB, throughput 0.98969
Reading from 417: heap size 482 MB, throughput 0.976972
Reading from 416: heap size 1200 MB, throughput 0.493312
Reading from 417: heap size 482 MB, throughput 0.986151
Reading from 416: heap size 1298 MB, throughput 0.93374
Reading from 417: heap size 484 MB, throughput 0.987223
Equal recommendation: 2974 MB each
Reading from 416: heap size 1295 MB, throughput 0.981646
Reading from 417: heap size 487 MB, throughput 0.98517
Reading from 416: heap size 1304 MB, throughput 0.980007
Reading from 417: heap size 488 MB, throughput 0.990679
Reading from 417: heap size 489 MB, throughput 0.919557
Reading from 417: heap size 491 MB, throughput 0.87628
Reading from 416: heap size 1314 MB, throughput 0.960198
Reading from 417: heap size 498 MB, throughput 0.985498
Reading from 416: heap size 1315 MB, throughput 0.975667
Reading from 417: heap size 500 MB, throughput 0.991544
Equal recommendation: 2974 MB each
Reading from 416: heap size 1309 MB, throughput 0.970287
Reading from 417: heap size 503 MB, throughput 0.990739
Reading from 416: heap size 1314 MB, throughput 0.970275
Reading from 417: heap size 505 MB, throughput 0.990632
Reading from 416: heap size 1301 MB, throughput 0.963422
Reading from 417: heap size 503 MB, throughput 0.988047
Reading from 416: heap size 1236 MB, throughput 0.972031
Equal recommendation: 2974 MB each
Reading from 416: heap size 1301 MB, throughput 0.964775
Reading from 417: heap size 506 MB, throughput 0.988343
Reading from 416: heap size 1304 MB, throughput 0.96353
Reading from 417: heap size 508 MB, throughput 0.986786
Reading from 416: heap size 1307 MB, throughput 0.961974
Reading from 417: heap size 508 MB, throughput 0.99156
Reading from 417: heap size 511 MB, throughput 0.917467
Reading from 417: heap size 512 MB, throughput 0.89646
Reading from 416: heap size 1309 MB, throughput 0.958986
Reading from 417: heap size 518 MB, throughput 0.991281
Equal recommendation: 2974 MB each
Reading from 416: heap size 1314 MB, throughput 0.959208
Reading from 417: heap size 520 MB, throughput 0.992291
Reading from 416: heap size 1320 MB, throughput 0.948667
Reading from 417: heap size 522 MB, throughput 0.990457
Reading from 416: heap size 1325 MB, throughput 0.959054
Reading from 417: heap size 524 MB, throughput 0.990115
Reading from 416: heap size 1334 MB, throughput 0.947924
Equal recommendation: 2974 MB each
Reading from 417: heap size 522 MB, throughput 0.989849
Reading from 416: heap size 1342 MB, throughput 0.951298
Reading from 417: heap size 525 MB, throughput 0.98795
Reading from 416: heap size 1347 MB, throughput 0.950687
Reading from 417: heap size 527 MB, throughput 0.993261
Reading from 417: heap size 528 MB, throughput 0.935734
Reading from 417: heap size 529 MB, throughput 0.904418
Reading from 417: heap size 531 MB, throughput 0.990102
Equal recommendation: 2974 MB each
Reading from 417: heap size 538 MB, throughput 0.992481
Reading from 417: heap size 539 MB, throughput 0.992313
Reading from 417: heap size 539 MB, throughput 0.990757
Reading from 416: heap size 1357 MB, throughput 0.990129
Equal recommendation: 2974 MB each
Reading from 417: heap size 541 MB, throughput 0.989763
Reading from 417: heap size 542 MB, throughput 0.988189
Reading from 417: heap size 543 MB, throughput 0.986889
Reading from 417: heap size 545 MB, throughput 0.908052
Reading from 417: heap size 547 MB, throughput 0.98155
Equal recommendation: 2974 MB each
Reading from 417: heap size 555 MB, throughput 0.992043
Reading from 416: heap size 1360 MB, throughput 0.988961
Reading from 417: heap size 555 MB, throughput 0.991991
Reading from 417: heap size 556 MB, throughput 0.991446
Equal recommendation: 2974 MB each
Reading from 416: heap size 1378 MB, throughput 0.977341
Reading from 417: heap size 558 MB, throughput 0.98959
Reading from 416: heap size 1398 MB, throughput 0.356016
Reading from 416: heap size 1422 MB, throughput 0.919942
Reading from 416: heap size 1438 MB, throughput 0.886951
Reading from 416: heap size 1438 MB, throughput 0.89411
Reading from 416: heap size 1443 MB, throughput 0.893246
Reading from 416: heap size 1442 MB, throughput 0.901965
Reading from 416: heap size 1448 MB, throughput 0.898197
Reading from 417: heap size 558 MB, throughput 0.988923
Reading from 416: heap size 1443 MB, throughput 0.979248
Reading from 417: heap size 559 MB, throughput 0.984596
Reading from 417: heap size 560 MB, throughput 0.909874
Reading from 417: heap size 563 MB, throughput 0.987041
Reading from 416: heap size 1451 MB, throughput 0.98
Equal recommendation: 2974 MB each
Reading from 417: heap size 569 MB, throughput 0.993031
Reading from 416: heap size 1435 MB, throughput 0.977741
Reading from 416: heap size 1448 MB, throughput 0.969291
Reading from 417: heap size 570 MB, throughput 0.993351
Reading from 416: heap size 1441 MB, throughput 0.976006
Reading from 417: heap size 571 MB, throughput 0.991357
Equal recommendation: 2974 MB each
Reading from 416: heap size 1447 MB, throughput 0.970117
Reading from 417: heap size 573 MB, throughput 0.990377
Reading from 416: heap size 1455 MB, throughput 0.970005
Reading from 417: heap size 573 MB, throughput 0.994276
Reading from 416: heap size 1458 MB, throughput 0.968809
Reading from 417: heap size 574 MB, throughput 0.97459
Reading from 417: heap size 575 MB, throughput 0.92596
Reading from 416: heap size 1467 MB, throughput 0.971461
Equal recommendation: 2974 MB each
Reading from 417: heap size 577 MB, throughput 0.993152
Reading from 416: heap size 1474 MB, throughput 0.739315
Reading from 417: heap size 583 MB, throughput 0.993283
Reading from 416: heap size 1572 MB, throughput 0.993191
Reading from 417: heap size 584 MB, throughput 0.992602
Equal recommendation: 2974 MB each
Reading from 416: heap size 1576 MB, throughput 0.987079
Reading from 417: heap size 583 MB, throughput 0.991357
Reading from 416: heap size 1593 MB, throughput 0.988719
Reading from 417: heap size 586 MB, throughput 0.990342
Reading from 416: heap size 1597 MB, throughput 0.981544
Reading from 417: heap size 587 MB, throughput 0.98934
Reading from 417: heap size 588 MB, throughput 0.920754
Equal recommendation: 2974 MB each
Reading from 416: heap size 1596 MB, throughput 0.98023
Reading from 417: heap size 592 MB, throughput 0.991599
Reading from 416: heap size 1600 MB, throughput 0.97683
Reading from 417: heap size 593 MB, throughput 0.992103
Reading from 416: heap size 1584 MB, throughput 0.977694
Reading from 417: heap size 596 MB, throughput 0.992569
Equal recommendation: 2974 MB each
Reading from 416: heap size 1463 MB, throughput 0.973661
Reading from 417: heap size 598 MB, throughput 0.9921
Reading from 416: heap size 1573 MB, throughput 0.977203
Reading from 417: heap size 598 MB, throughput 0.990842
Reading from 416: heap size 1582 MB, throughput 0.971089
Reading from 417: heap size 599 MB, throughput 0.98906
Reading from 417: heap size 602 MB, throughput 0.931789
Equal recommendation: 2974 MB each
Reading from 417: heap size 603 MB, throughput 0.991446
Reading from 416: heap size 1587 MB, throughput 0.968128
Reading from 416: heap size 1587 MB, throughput 0.970444
Reading from 417: heap size 610 MB, throughput 0.994037
Reading from 416: heap size 1596 MB, throughput 0.96527
Equal recommendation: 2974 MB each
Reading from 417: heap size 610 MB, throughput 0.993009
Reading from 416: heap size 1602 MB, throughput 0.958215
Reading from 417: heap size 610 MB, throughput 0.991704
Reading from 416: heap size 1613 MB, throughput 0.96275
Reading from 417: heap size 612 MB, throughput 0.98815
Reading from 417: heap size 609 MB, throughput 0.979659
Reading from 417: heap size 614 MB, throughput 0.94096
Equal recommendation: 2974 MB each
Reading from 416: heap size 1624 MB, throughput 0.958026
Reading from 417: heap size 621 MB, throughput 0.993673
Reading from 416: heap size 1641 MB, throughput 0.956821
Reading from 417: heap size 620 MB, throughput 0.992978
Reading from 416: heap size 1650 MB, throughput 0.953097
Equal recommendation: 2974 MB each
Reading from 417: heap size 620 MB, throughput 0.991838
Reading from 416: heap size 1669 MB, throughput 0.956239
Reading from 417: heap size 622 MB, throughput 0.991269
Reading from 417: heap size 625 MB, throughput 0.993357
Equal recommendation: 2974 MB each
Reading from 417: heap size 625 MB, throughput 0.924456
Client 417 died
Clients: 1
Reading from 416: heap size 1674 MB, throughput 0.988754
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 416: heap size 1668 MB, throughput 0.991049
Reading from 416: heap size 1697 MB, throughput 0.946766
Reading from 416: heap size 1714 MB, throughput 0.822404
Reading from 416: heap size 1719 MB, throughput 0.776421
Reading from 416: heap size 1752 MB, throughput 0.808927
Reading from 416: heap size 1767 MB, throughput 0.850251
Client 416 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
