economemd
    total memory: 2506 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub26_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
ReadingThread: starting
TimerThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 12034: heap size 9 MB, throughput 0.999046
Clients: 1
Client 12034 has a minimum heap size of 30 MB
Reading from 12033: heap size 9 MB, throughput 0.999257
Clients: 2
Client 12033 has a minimum heap size of 1223 MB
Reading from 12034: heap size 9 MB, throughput 0.995713
Reading from 12034: heap size 9 MB, throughput 0.967773
Reading from 12033: heap size 9 MB, throughput 0.996729
Reading from 12034: heap size 9 MB, throughput 0.995785
Reading from 12034: heap size 9 MB, throughput 0.578206
Reading from 12034: heap size 9 MB, throughput 0.402205
Reading from 12034: heap size 12 MB, throughput 0.561699
Reading from 12034: heap size 12 MB, throughput 0.561433
Reading from 12034: heap size 16 MB, throughput 0.810849
Reading from 12034: heap size 16 MB, throughput 0.682784
Reading from 12034: heap size 21 MB, throughput 0.833865
Reading from 12034: heap size 21 MB, throughput 0.826482
Reading from 12034: heap size 29 MB, throughput 0.881478
Reading from 12034: heap size 29 MB, throughput 0.871668
Reading from 12034: heap size 42 MB, throughput 0.927379
Reading from 12034: heap size 42 MB, throughput 0.923088
Reading from 12034: heap size 62 MB, throughput 0.940006
Reading from 12034: heap size 62 MB, throughput 0.915759
Reading from 12034: heap size 84 MB, throughput 0.955866
Reading from 12033: heap size 9 MB, throughput 0.98803
Reading from 12034: heap size 84 MB, throughput 0.92234
Reading from 12034: heap size 115 MB, throughput 0.952164
Reading from 12033: heap size 9 MB, throughput 0.974866
Reading from 12033: heap size 9 MB, throughput 0.996152
Reading from 12033: heap size 9 MB, throughput 0.977715
Reading from 12033: heap size 10 MB, throughput 0.964765
Reading from 12033: heap size 10 MB, throughput 0.910942
Reading from 12033: heap size 12 MB, throughput 0.917856
Reading from 12033: heap size 12 MB, throughput 0.939393
Reading from 12033: heap size 15 MB, throughput 0.402915
Reading from 12033: heap size 19 MB, throughput 0.947494
Reading from 12033: heap size 22 MB, throughput 0.925356
Reading from 12033: heap size 23 MB, throughput 0.328221
Reading from 12034: heap size 115 MB, throughput 0.962417
Reading from 12033: heap size 34 MB, throughput 0.923588
Reading from 12033: heap size 34 MB, throughput 0.972088
Reading from 12033: heap size 36 MB, throughput 0.269114
Reading from 12033: heap size 47 MB, throughput 0.785399
Reading from 12033: heap size 49 MB, throughput 0.896381
Reading from 12033: heap size 50 MB, throughput 0.858073
Reading from 12034: heap size 162 MB, throughput 0.876408
Reading from 12033: heap size 54 MB, throughput 0.338574
Reading from 12033: heap size 75 MB, throughput 0.84936
Reading from 12033: heap size 77 MB, throughput 0.793775
Reading from 12033: heap size 78 MB, throughput 0.769275
Reading from 12033: heap size 82 MB, throughput 0.85525
Reading from 12033: heap size 84 MB, throughput 0.246205
Reading from 12033: heap size 112 MB, throughput 0.639132
Reading from 12033: heap size 113 MB, throughput 0.79596
Reading from 12034: heap size 176 MB, throughput 0.993018
Reading from 12033: heap size 114 MB, throughput 0.744246
Reading from 12033: heap size 117 MB, throughput 0.590409
Reading from 12033: heap size 119 MB, throughput 0.699893
Reading from 12033: heap size 122 MB, throughput 0.22709
Reading from 12034: heap size 205 MB, throughput 0.987528
Reading from 12033: heap size 151 MB, throughput 0.519561
Reading from 12033: heap size 155 MB, throughput 0.527622
Reading from 12033: heap size 157 MB, throughput 0.551988
Reading from 12033: heap size 159 MB, throughput 0.0943113
Reading from 12033: heap size 192 MB, throughput 0.485402
Reading from 12034: heap size 225 MB, throughput 0.927343
Reading from 12033: heap size 193 MB, throughput 0.837972
Reading from 12033: heap size 193 MB, throughput 0.766407
Reading from 12033: heap size 194 MB, throughput 0.670879
Reading from 12033: heap size 195 MB, throughput 0.613286
Reading from 12033: heap size 199 MB, throughput 0.647341
Reading from 12033: heap size 202 MB, throughput 0.560344
Reading from 12033: heap size 208 MB, throughput 0.58113
Reading from 12033: heap size 213 MB, throughput 0.545583
Reading from 12033: heap size 220 MB, throughput 0.0985345
Reading from 12034: heap size 233 MB, throughput 0.995374
Reading from 12033: heap size 257 MB, throughput 0.380571
Reading from 12033: heap size 261 MB, throughput 0.624421
Reading from 12033: heap size 254 MB, throughput 0.135325
Reading from 12034: heap size 233 MB, throughput 0.816123
Reading from 12033: heap size 295 MB, throughput 0.538176
Reading from 12033: heap size 291 MB, throughput 0.637388
Reading from 12033: heap size 260 MB, throughput 0.727411
Reading from 12033: heap size 292 MB, throughput 0.590256
Reading from 12033: heap size 292 MB, throughput 0.492035
Reading from 12033: heap size 293 MB, throughput 0.519828
Reading from 12033: heap size 298 MB, throughput 0.519596
Reading from 12034: heap size 265 MB, throughput 0.983473
Reading from 12033: heap size 302 MB, throughput 0.544058
Reading from 12033: heap size 306 MB, throughput 0.48293
Equal recommendation: 1253 MB each
Reading from 12034: heap size 269 MB, throughput 0.959027
Reading from 12033: heap size 308 MB, throughput 0.0820547
Reading from 12033: heap size 352 MB, throughput 0.544725
Reading from 12033: heap size 354 MB, throughput 0.654768
Reading from 12033: heap size 357 MB, throughput 0.589786
Reading from 12033: heap size 360 MB, throughput 0.637508
Reading from 12033: heap size 360 MB, throughput 0.619415
Reading from 12034: heap size 287 MB, throughput 0.967533
Reading from 12033: heap size 363 MB, throughput 0.0618968
Reading from 12033: heap size 407 MB, throughput 0.422852
Reading from 12033: heap size 408 MB, throughput 0.673969
Reading from 12033: heap size 410 MB, throughput 0.550904
Reading from 12034: heap size 287 MB, throughput 0.968526
Reading from 12033: heap size 411 MB, throughput 0.565433
Reading from 12033: heap size 416 MB, throughput 0.485582
Reading from 12033: heap size 418 MB, throughput 0.472494
Reading from 12033: heap size 427 MB, throughput 0.366988
Reading from 12033: heap size 432 MB, throughput 0.5124
Reading from 12034: heap size 310 MB, throughput 0.999998
Reading from 12034: heap size 310 MB, throughput 1
Reading from 12034: heap size 310 MB, throughput 0.0505097
Reading from 12033: heap size 439 MB, throughput 0.058598
Reading from 12033: heap size 497 MB, throughput 0.448025
Reading from 12033: heap size 497 MB, throughput 0.513435
Reading from 12033: heap size 493 MB, throughput 0.514222
Reading from 12034: heap size 314 MB, throughput 0.992311
Reading from 12034: heap size 334 MB, throughput 0.97673
Reading from 12033: heap size 496 MB, throughput 0.0627949
Reading from 12033: heap size 548 MB, throughput 0.430816
Reading from 12033: heap size 551 MB, throughput 0.586852
Reading from 12033: heap size 545 MB, throughput 0.474799
Reading from 12033: heap size 549 MB, throughput 0.568923
Reading from 12033: heap size 551 MB, throughput 0.551668
Reading from 12034: heap size 336 MB, throughput 0.988992
Reading from 12033: heap size 556 MB, throughput 0.565564
Reading from 12033: heap size 559 MB, throughput 0.5484
Reading from 12033: heap size 569 MB, throughput 0.467952
Reading from 12033: heap size 576 MB, throughput 0.0829869
Reading from 12033: heap size 641 MB, throughput 0.371225
Reading from 12034: heap size 360 MB, throughput 0.986694
Reading from 12033: heap size 646 MB, throughput 0.568839
Reading from 12033: heap size 649 MB, throughput 0.671385
Reading from 12033: heap size 653 MB, throughput 0.869542
Reading from 12034: heap size 361 MB, throughput 0.98538
Reading from 12033: heap size 654 MB, throughput 0.833144
Equal recommendation: 1253 MB each
Reading from 12034: heap size 385 MB, throughput 0.976139
Reading from 12033: heap size 655 MB, throughput 0.848321
Reading from 12033: heap size 664 MB, throughput 0.618498
Reading from 12034: heap size 386 MB, throughput 0.982619
Reading from 12033: heap size 674 MB, throughput 0.0207036
Reading from 12034: heap size 408 MB, throughput 0.992696
Reading from 12033: heap size 743 MB, throughput 0.458498
Reading from 12033: heap size 743 MB, throughput 0.35792
Reading from 12033: heap size 673 MB, throughput 0.295442
Reading from 12033: heap size 734 MB, throughput 0.387463
Reading from 12033: heap size 739 MB, throughput 0.0548498
Reading from 12033: heap size 807 MB, throughput 0.223905
Reading from 12034: heap size 412 MB, throughput 0.98998
Reading from 12033: heap size 808 MB, throughput 0.884718
Reading from 12033: heap size 811 MB, throughput 0.59036
Reading from 12033: heap size 813 MB, throughput 0.580812
Reading from 12033: heap size 810 MB, throughput 0.618048
Reading from 12033: heap size 813 MB, throughput 0.676192
Reading from 12034: heap size 431 MB, throughput 0.978467
Reading from 12034: heap size 432 MB, throughput 0.984589
Reading from 12033: heap size 815 MB, throughput 0.126554
Reading from 12033: heap size 899 MB, throughput 0.656988
Reading from 12033: heap size 901 MB, throughput 0.914411
Reading from 12034: heap size 454 MB, throughput 0.990661
Reading from 12033: heap size 907 MB, throughput 0.951515
Reading from 12033: heap size 912 MB, throughput 0.914997
Reading from 12033: heap size 913 MB, throughput 0.906994
Reading from 12033: heap size 906 MB, throughput 0.902646
Reading from 12033: heap size 795 MB, throughput 0.812667
Reading from 12034: heap size 455 MB, throughput 0.986177
Reading from 12033: heap size 881 MB, throughput 0.762509
Reading from 12033: heap size 799 MB, throughput 0.765806
Equal recommendation: 1253 MB each
Reading from 12033: heap size 870 MB, throughput 0.823932
Reading from 12033: heap size 797 MB, throughput 0.822125
Reading from 12033: heap size 866 MB, throughput 0.864457
Reading from 12033: heap size 802 MB, throughput 0.838772
Reading from 12033: heap size 861 MB, throughput 0.856841
Reading from 12033: heap size 866 MB, throughput 0.764273
Reading from 12034: heap size 475 MB, throughput 0.969255
Reading from 12033: heap size 860 MB, throughput 0.806198
Reading from 12033: heap size 863 MB, throughput 0.869505
Reading from 12033: heap size 859 MB, throughput 0.981703
Reading from 12034: heap size 476 MB, throughput 0.98832
Reading from 12034: heap size 501 MB, throughput 0.990429
Reading from 12033: heap size 863 MB, throughput 0.968517
Reading from 12033: heap size 867 MB, throughput 0.721622
Reading from 12033: heap size 870 MB, throughput 0.778155
Reading from 12033: heap size 876 MB, throughput 0.796691
Reading from 12033: heap size 877 MB, throughput 0.770017
Reading from 12033: heap size 884 MB, throughput 0.796561
Reading from 12033: heap size 884 MB, throughput 0.780619
Reading from 12033: heap size 890 MB, throughput 0.733797
Reading from 12034: heap size 504 MB, throughput 0.988698
Reading from 12033: heap size 890 MB, throughput 0.829951
Reading from 12033: heap size 895 MB, throughput 0.814743
Reading from 12033: heap size 896 MB, throughput 0.879019
Reading from 12033: heap size 898 MB, throughput 0.870033
Reading from 12033: heap size 901 MB, throughput 0.839464
Reading from 12034: heap size 524 MB, throughput 0.993065
Reading from 12034: heap size 526 MB, throughput 0.986859
Reading from 12033: heap size 905 MB, throughput 0.0794813
Reading from 12033: heap size 995 MB, throughput 0.413
Reading from 12033: heap size 1022 MB, throughput 0.74593
Reading from 12033: heap size 1023 MB, throughput 0.688585
Equal recommendation: 1253 MB each
Reading from 12033: heap size 1028 MB, throughput 0.730244
Reading from 12033: heap size 1030 MB, throughput 0.694863
Reading from 12034: heap size 545 MB, throughput 0.995641
Reading from 12033: heap size 1032 MB, throughput 0.740999
Reading from 12033: heap size 1036 MB, throughput 0.727151
Reading from 12033: heap size 1039 MB, throughput 0.734033
Reading from 12033: heap size 1043 MB, throughput 0.634582
Reading from 12033: heap size 1057 MB, throughput 0.751107
Reading from 12034: heap size 548 MB, throughput 0.981347
Reading from 12034: heap size 567 MB, throughput 0.992063
Reading from 12033: heap size 1057 MB, throughput 0.963934
Reading from 12034: heap size 568 MB, throughput 0.990325
Reading from 12033: heap size 1075 MB, throughput 0.956554
Reading from 12034: heap size 587 MB, throughput 0.977006
Reading from 12034: heap size 589 MB, throughput 0.994772
Equal recommendation: 1253 MB each
Reading from 12033: heap size 1076 MB, throughput 0.96235
Reading from 12034: heap size 609 MB, throughput 0.989746
Reading from 12034: heap size 613 MB, throughput 0.979094
Reading from 12033: heap size 1089 MB, throughput 0.957948
Reading from 12034: heap size 637 MB, throughput 0.996683
Reading from 12034: heap size 639 MB, throughput 0.983605
Reading from 12033: heap size 1094 MB, throughput 0.757621
Reading from 12034: heap size 663 MB, throughput 0.98931
Equal recommendation: 1253 MB each
Reading from 12033: heap size 1193 MB, throughput 0.994387
Reading from 12034: heap size 663 MB, throughput 0.991762
Reading from 12034: heap size 686 MB, throughput 0.99529
Reading from 12033: heap size 1196 MB, throughput 0.99164
Reading from 12034: heap size 689 MB, throughput 0.988953
Reading from 12033: heap size 1210 MB, throughput 0.990037
Reading from 12034: heap size 711 MB, throughput 0.986671
Equal recommendation: 1253 MB each
Reading from 12034: heap size 712 MB, throughput 0.988062
Reading from 12033: heap size 1213 MB, throughput 0.988862
Reading from 12034: heap size 739 MB, throughput 0.992673
Reading from 12033: heap size 1215 MB, throughput 0.984232
Reading from 12034: heap size 740 MB, throughput 0.994708
Reading from 12034: heap size 759 MB, throughput 0.995393
Reading from 12033: heap size 1218 MB, throughput 0.987066
Equal recommendation: 1253 MB each
Reading from 12034: heap size 763 MB, throughput 0.990687
Reading from 12033: heap size 1207 MB, throughput 0.983968
Reading from 12034: heap size 783 MB, throughput 0.989495
Reading from 12033: heap size 1143 MB, throughput 0.981827
Reading from 12034: heap size 784 MB, throughput 0.988188
Reading from 12034: heap size 808 MB, throughput 0.991344
Reading from 12033: heap size 1202 MB, throughput 0.981423
Equal recommendation: 1253 MB each
Reading from 12034: heap size 810 MB, throughput 0.991572
Reading from 12033: heap size 1207 MB, throughput 0.978263
Reading from 12034: heap size 835 MB, throughput 0.992962
Reading from 12033: heap size 1208 MB, throughput 0.98003
Reading from 12034: heap size 836 MB, throughput 0.992092
Reading from 12034: heap size 842 MB, throughput 0.992478
Reading from 12033: heap size 1209 MB, throughput 0.978773
Equal recommendation: 1253 MB each
Reading from 12034: heap size 842 MB, throughput 0.995753
Reading from 12033: heap size 1211 MB, throughput 0.973329
Reading from 12034: heap size 844 MB, throughput 0.991896
Reading from 12033: heap size 1216 MB, throughput 0.972957
Reading from 12034: heap size 844 MB, throughput 0.991651
Reading from 12034: heap size 844 MB, throughput 0.991951
Reading from 12033: heap size 1219 MB, throughput 0.974516
Equal recommendation: 1253 MB each
Reading from 12034: heap size 837 MB, throughput 0.99115
Reading from 12033: heap size 1226 MB, throughput 0.972081
Reading from 12034: heap size 844 MB, throughput 0.991764
Reading from 12033: heap size 1232 MB, throughput 0.968086
Reading from 12034: heap size 836 MB, throughput 0.992278
Reading from 12034: heap size 844 MB, throughput 0.992082
Equal recommendation: 1253 MB each
Reading from 12033: heap size 1238 MB, throughput 0.971389
Reading from 12034: heap size 843 MB, throughput 0.990373
Reading from 12033: heap size 1245 MB, throughput 0.970473
Reading from 12034: heap size 843 MB, throughput 0.99015
Reading from 12033: heap size 1248 MB, throughput 0.970331
Reading from 12034: heap size 843 MB, throughput 0.992005
Equal recommendation: 1253 MB each
Reading from 12034: heap size 842 MB, throughput 0.993067
Reading from 12033: heap size 1254 MB, throughput 0.967902
Reading from 12034: heap size 843 MB, throughput 0.991587
Reading from 12033: heap size 1204 MB, throughput 0.968169
Reading from 12034: heap size 841 MB, throughput 0.989552
Reading from 12033: heap size 1254 MB, throughput 0.957596
Reading from 12034: heap size 842 MB, throughput 0.995291
Equal recommendation: 1253 MB each
Reading from 12034: heap size 843 MB, throughput 0.991593
Reading from 12033: heap size 1201 MB, throughput 0.967311
Reading from 12034: heap size 843 MB, throughput 0.991367
Reading from 12033: heap size 1253 MB, throughput 0.972393
Reading from 12034: heap size 842 MB, throughput 0.993073
Reading from 12033: heap size 1252 MB, throughput 0.963498
Reading from 12034: heap size 843 MB, throughput 0.98635
Equal recommendation: 1253 MB each
Reading from 12034: heap size 843 MB, throughput 0.991632
Reading from 12033: heap size 1253 MB, throughput 0.969799
Reading from 12034: heap size 843 MB, throughput 0.992499
Reading from 12033: heap size 1204 MB, throughput 0.968022
Reading from 12034: heap size 843 MB, throughput 0.999999
Reading from 12034: heap size 843 MB, throughput 1
Reading from 12034: heap size 843 MB, throughput 0.208874
Equal recommendation: 1253 MB each
Reading from 12034: heap size 843 MB, throughput 0.996506
Reading from 12033: heap size 1253 MB, throughput 0.971999
Reading from 12034: heap size 843 MB, throughput 0.991462
Equal recommendation: 1253 MB each
Equal recommendation: 1253 MB each
Reading from 12034: heap size 843 MB, throughput 0.913727
Reading from 12033: heap size 1251 MB, throughput 0.156739
Equal recommendation: 1253 MB each
Equal recommendation: 1253 MB each
Reading from 12034: heap size 843 MB, throughput 0.999552
Reading from 12034: heap size 825 MB, throughput 0.994413
Reading from 12034: heap size 842 MB, throughput 0.994789
Equal recommendation: 1253 MB each
Reading from 12034: heap size 843 MB, throughput 0.99372
Reading from 12034: heap size 843 MB, throughput 0.995625
Reading from 12034: heap size 834 MB, throughput 0.993414
Reading from 12033: heap size 1361 MB, throughput 0.979183
Reading from 12034: heap size 842 MB, throughput 0.991939
Equal recommendation: 1253 MB each
Reading from 12033: heap size 1360 MB, throughput 0.959516
Reading from 12034: heap size 838 MB, throughput 0.984755
Reading from 12034: heap size 836 MB, throughput 0.990093
Reading from 12033: heap size 1364 MB, throughput 0.98016
Reading from 12034: heap size 834 MB, throughput 0.990122
Reading from 12033: heap size 1232 MB, throughput 0.977736
Reading from 12034: heap size 833 MB, throughput 0.989356
Equal recommendation: 1253 MB each
Reading from 12034: heap size 827 MB, throughput 0.990377
Reading from 12033: heap size 1370 MB, throughput 0.974724
Reading from 12034: heap size 830 MB, throughput 0.942294
Reading from 12033: heap size 1244 MB, throughput 0.981773
Reading from 12034: heap size 832 MB, throughput 0.992626
Reading from 12033: heap size 1361 MB, throughput 0.980098
Reading from 12034: heap size 831 MB, throughput 0.9926
Equal recommendation: 1253 MB each
Reading from 12034: heap size 832 MB, throughput 0.996903
Reading from 12033: heap size 1260 MB, throughput 0.932511
Reading from 12034: heap size 832 MB, throughput 0.99076
Reading from 12033: heap size 1333 MB, throughput 0.978598
Reading from 12034: heap size 815 MB, throughput 0.991966
Reading from 12034: heap size 834 MB, throughput 0.991062
Equal recommendation: 1253 MB each
Reading from 12033: heap size 1267 MB, throughput 0.962905
Reading from 12034: heap size 832 MB, throughput 0.995957
Reading from 12034: heap size 832 MB, throughput 0.989357
Reading from 12034: heap size 832 MB, throughput 0.989038
Reading from 12034: heap size 836 MB, throughput 0.987616
Equal recommendation: 1253 MB each
Reading from 12034: heap size 837 MB, throughput 0.990733
Reading from 12034: heap size 835 MB, throughput 0.990031
Reading from 12034: heap size 836 MB, throughput 0.994499
Reading from 12033: heap size 1321 MB, throughput 0.985758
Reading from 12034: heap size 839 MB, throughput 0.990791
Equal recommendation: 1253 MB each
Reading from 12034: heap size 840 MB, throughput 0.991393
Reading from 12034: heap size 838 MB, throughput 0.992622
Reading from 12034: heap size 839 MB, throughput 0.991299
Reading from 12034: heap size 841 MB, throughput 0.991547
Equal recommendation: 1253 MB each
Reading from 12034: heap size 841 MB, throughput 0.995881
Reading from 12034: heap size 841 MB, throughput 0.990557
Reading from 12033: heap size 1325 MB, throughput 0.992304
Reading from 12034: heap size 841 MB, throughput 0.987456
Reading from 12034: heap size 841 MB, throughput 0.990333
Equal recommendation: 1253 MB each
Reading from 12034: heap size 830 MB, throughput 0.992322
Reading from 12033: heap size 1317 MB, throughput 0.689761
Client 12034 died
Clients: 1
Reading from 12033: heap size 1359 MB, throughput 0.99514
Reading from 12033: heap size 1407 MB, throughput 0.976197
Reading from 12033: heap size 1219 MB, throughput 0.896768
Reading from 12033: heap size 1409 MB, throughput 0.852365
Reading from 12033: heap size 1303 MB, throughput 0.8519
Reading from 12033: heap size 1384 MB, throughput 0.854412
Reading from 12033: heap size 1299 MB, throughput 0.853286
Reading from 12033: heap size 1369 MB, throughput 0.855516
Reading from 12033: heap size 1295 MB, throughput 0.854795
Reading from 12033: heap size 1356 MB, throughput 0.952968
Reading from 12033: heap size 1360 MB, throughput 0.992977
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1362 MB, throughput 0.992801
Reading from 12033: heap size 1364 MB, throughput 0.991147
Reading from 12033: heap size 1365 MB, throughput 0.989948
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1369 MB, throughput 0.988696
Reading from 12033: heap size 1357 MB, throughput 0.987845
Reading from 12033: heap size 1364 MB, throughput 0.986955
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1350 MB, throughput 0.987351
Reading from 12033: heap size 1260 MB, throughput 0.984787
Reading from 12033: heap size 1338 MB, throughput 0.984627
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1278 MB, throughput 0.984291
Reading from 12033: heap size 1339 MB, throughput 0.983808
Reading from 12033: heap size 1342 MB, throughput 0.98193
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1345 MB, throughput 0.9806
Reading from 12033: heap size 1346 MB, throughput 0.980742
Reading from 12033: heap size 1349 MB, throughput 0.978879
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1354 MB, throughput 0.97825
Reading from 12033: heap size 1360 MB, throughput 0.975503
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1363 MB, throughput 0.979211
Reading from 12033: heap size 1369 MB, throughput 0.979102
Reading from 12033: heap size 1371 MB, throughput 0.978793
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1376 MB, throughput 0.980296
Reading from 12033: heap size 1377 MB, throughput 0.979288
Reading from 12033: heap size 1383 MB, throughput 0.979406
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1383 MB, throughput 0.977868
Reading from 12033: heap size 1387 MB, throughput 0.979233
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1387 MB, throughput 0.978289
Reading from 12033: heap size 1391 MB, throughput 0.979211
Reading from 12033: heap size 1392 MB, throughput 0.977865
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1396 MB, throughput 0.980145
Reading from 12033: heap size 1396 MB, throughput 0.977505
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1400 MB, throughput 0.978697
Reading from 12033: heap size 1401 MB, throughput 0.790387
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1422 MB, throughput 0.995454
Reading from 12033: heap size 1422 MB, throughput 0.993068
Reading from 12033: heap size 1433 MB, throughput 0.992605
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1439 MB, throughput 0.990883
Reading from 12033: heap size 1444 MB, throughput 0.990031
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1445 MB, throughput 0.988727
Reading from 12033: heap size 1434 MB, throughput 0.987557
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1343 MB, throughput 0.986518
Reading from 12033: heap size 1421 MB, throughput 0.986393
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1429 MB, throughput 0.984751
Reading from 12033: heap size 1428 MB, throughput 0.983024
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1429 MB, throughput 0.989184
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1427 MB, throughput 0.995713
Reading from 12033: heap size 1436 MB, throughput 0.987155
Reading from 12033: heap size 1425 MB, throughput 0.884639
Reading from 12033: heap size 1449 MB, throughput 0.773374
Recommendation: one client; give it all the memory
Reading from 12033: heap size 1434 MB, throughput 0.723193
Reading from 12033: heap size 1475 MB, throughput 0.711492
Reading from 12033: heap size 1503 MB, throughput 0.742626
Reading from 12033: heap size 1516 MB, throughput 0.733255
Reading from 12033: heap size 1536 MB, throughput 0.756293
Reading from 12033: heap size 1540 MB, throughput 0.788776
Client 12033 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
