economemd
    total memory: 3657 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub9_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 27088: heap size 9 MB, throughput 0.984586
Clients: 1
Client 27088 has a minimum heap size of 8 MB
Reading from 27087: heap size 9 MB, throughput 0.991543
Clients: 2
Client 27087 has a minimum heap size of 1211 MB
Reading from 27088: heap size 9 MB, throughput 0.981352
Reading from 27087: heap size 9 MB, throughput 0.978242
Reading from 27087: heap size 9 MB, throughput 0.94801
Reading from 27088: heap size 11 MB, throughput 0.971386
Reading from 27088: heap size 11 MB, throughput 0.931075
Reading from 27087: heap size 9 MB, throughput 0.943243
Reading from 27087: heap size 11 MB, throughput 0.987975
Reading from 27088: heap size 15 MB, throughput 0.915214
Reading from 27087: heap size 11 MB, throughput 0.987368
Reading from 27088: heap size 20 MB, throughput 0.979238
Reading from 27087: heap size 17 MB, throughput 0.955878
Reading from 27088: heap size 24 MB, throughput 0.983692
Reading from 27088: heap size 29 MB, throughput 0.975214
Reading from 27088: heap size 32 MB, throughput 0.974832
Reading from 27087: heap size 17 MB, throughput 0.474378
Reading from 27088: heap size 32 MB, throughput 0.896707
Reading from 27088: heap size 37 MB, throughput 0.966817
Reading from 27088: heap size 37 MB, throughput 0.971081
Reading from 27087: heap size 30 MB, throughput 0.96794
Reading from 27088: heap size 44 MB, throughput 0.987415
Reading from 27087: heap size 31 MB, throughput 0.922366
Reading from 27088: heap size 44 MB, throughput 0.973783
Reading from 27088: heap size 53 MB, throughput 0.987281
Reading from 27088: heap size 53 MB, throughput 0.975875
Reading from 27087: heap size 34 MB, throughput 0.384137
Reading from 27087: heap size 46 MB, throughput 0.91508
Reading from 27088: heap size 63 MB, throughput 0.986883
Reading from 27088: heap size 63 MB, throughput 0.979384
Reading from 27087: heap size 49 MB, throughput 0.886642
Reading from 27088: heap size 72 MB, throughput 0.992186
Reading from 27088: heap size 72 MB, throughput 0.960916
Reading from 27087: heap size 50 MB, throughput 0.212017
Reading from 27087: heap size 69 MB, throughput 0.715174
Reading from 27088: heap size 81 MB, throughput 0.987098
Reading from 27087: heap size 71 MB, throughput 0.787366
Reading from 27088: heap size 81 MB, throughput 0.986489
Reading from 27087: heap size 76 MB, throughput 0.386422
Reading from 27088: heap size 90 MB, throughput 0.958895
Reading from 27087: heap size 96 MB, throughput 0.697781
Reading from 27088: heap size 91 MB, throughput 0.990813
Reading from 27087: heap size 103 MB, throughput 0.762654
Reading from 27087: heap size 103 MB, throughput 0.682091
Reading from 27088: heap size 102 MB, throughput 0.993058
Reading from 27088: heap size 104 MB, throughput 0.993245
Reading from 27088: heap size 112 MB, throughput 0.99444
Reading from 27087: heap size 106 MB, throughput 0.164896
Reading from 27087: heap size 134 MB, throughput 0.670191
Reading from 27088: heap size 113 MB, throughput 0.992467
Reading from 27087: heap size 142 MB, throughput 0.677833
Reading from 27087: heap size 144 MB, throughput 0.689068
Reading from 27088: heap size 120 MB, throughput 0.995988
Reading from 27088: heap size 121 MB, throughput 0.994773
Reading from 27088: heap size 127 MB, throughput 0.99621
Reading from 27087: heap size 149 MB, throughput 0.127911
Reading from 27087: heap size 185 MB, throughput 0.600603
Reading from 27088: heap size 127 MB, throughput 0.995457
Reading from 27087: heap size 190 MB, throughput 0.683169
Reading from 27087: heap size 194 MB, throughput 0.707719
Reading from 27088: heap size 133 MB, throughput 0.996382
Reading from 27087: heap size 198 MB, throughput 0.603498
Reading from 27088: heap size 133 MB, throughput 0.993275
Reading from 27087: heap size 207 MB, throughput 0.57059
Reading from 27088: heap size 138 MB, throughput 0.995299
Reading from 27088: heap size 138 MB, throughput 0.991935
Reading from 27087: heap size 210 MB, throughput 0.14823
Reading from 27088: heap size 143 MB, throughput 0.995716
Reading from 27087: heap size 253 MB, throughput 0.537657
Reading from 27088: heap size 143 MB, throughput 0.990257
Reading from 27087: heap size 264 MB, throughput 0.645305
Reading from 27087: heap size 265 MB, throughput 0.691295
Reading from 27088: heap size 149 MB, throughput 0.995542
Reading from 27087: heap size 268 MB, throughput 0.61948
Reading from 27087: heap size 274 MB, throughput 0.575444
Reading from 27088: heap size 149 MB, throughput 0.995877
Reading from 27088: heap size 154 MB, throughput 0.992495
Reading from 27088: heap size 154 MB, throughput 0.985252
Reading from 27087: heap size 280 MB, throughput 0.123768
Reading from 27087: heap size 326 MB, throughput 0.463244
Reading from 27088: heap size 160 MB, throughput 0.994545
Reading from 27087: heap size 334 MB, throughput 0.596302
Reading from 27087: heap size 335 MB, throughput 0.498184
Reading from 27088: heap size 160 MB, throughput 0.975851
Reading from 27087: heap size 341 MB, throughput 0.637429
Reading from 27087: heap size 345 MB, throughput 0.587743
Reading from 27088: heap size 166 MB, throughput 0.997352
Reading from 27088: heap size 166 MB, throughput 0.995984
Reading from 27088: heap size 173 MB, throughput 0.998208
Reading from 27087: heap size 350 MB, throughput 0.0987607
Reading from 27087: heap size 397 MB, throughput 0.447352
Reading from 27088: heap size 174 MB, throughput 0.996523
Reading from 27087: heap size 400 MB, throughput 0.540442
Reading from 27087: heap size 404 MB, throughput 0.462286
Reading from 27088: heap size 180 MB, throughput 0.997259
Reading from 27088: heap size 180 MB, throughput 0.988035
Reading from 27088: heap size 185 MB, throughput 0.99675
Reading from 27088: heap size 185 MB, throughput 0.997442
Reading from 27087: heap size 407 MB, throughput 0.10407
Reading from 27088: heap size 190 MB, throughput 0.996012
Reading from 27087: heap size 458 MB, throughput 0.454539
Reading from 27087: heap size 456 MB, throughput 0.616239
Reading from 27088: heap size 191 MB, throughput 0.996567
Reading from 27087: heap size 461 MB, throughput 0.587893
Reading from 27088: heap size 195 MB, throughput 0.993451
Reading from 27087: heap size 463 MB, throughput 0.563807
Equal recommendation: 1828 MB each
Reading from 27087: heap size 468 MB, throughput 0.475338
Reading from 27088: heap size 195 MB, throughput 0.997472
Reading from 27088: heap size 200 MB, throughput 0.994775
Reading from 27088: heap size 201 MB, throughput 0.997417
Reading from 27087: heap size 476 MB, throughput 0.106274
Reading from 27088: heap size 205 MB, throughput 0.997915
Reading from 27087: heap size 541 MB, throughput 0.424184
Reading from 27088: heap size 205 MB, throughput 0.996561
Reading from 27087: heap size 550 MB, throughput 0.537689
Reading from 27087: heap size 556 MB, throughput 0.494014
Reading from 27088: heap size 209 MB, throughput 0.996594
Reading from 27087: heap size 562 MB, throughput 0.513068
Reading from 27088: heap size 209 MB, throughput 0.992932
Reading from 27087: heap size 572 MB, throughput 0.550807
Reading from 27088: heap size 214 MB, throughput 0.994149
Reading from 27088: heap size 214 MB, throughput 0.994867
Reading from 27087: heap size 580 MB, throughput 0.0960604
Reading from 27088: heap size 220 MB, throughput 0.994691
Reading from 27087: heap size 650 MB, throughput 0.433384
Reading from 27087: heap size 652 MB, throughput 0.51848
Reading from 27088: heap size 220 MB, throughput 0.996142
Reading from 27088: heap size 225 MB, throughput 0.99577
Reading from 27088: heap size 225 MB, throughput 0.995959
Reading from 27087: heap size 659 MB, throughput 0.0943615
Reading from 27088: heap size 231 MB, throughput 0.995643
Reading from 27087: heap size 723 MB, throughput 0.546271
Reading from 27088: heap size 231 MB, throughput 0.993864
Reading from 27088: heap size 236 MB, throughput 0.995631
Reading from 27087: heap size 648 MB, throughput 0.867851
Reading from 27088: heap size 236 MB, throughput 0.99677
Reading from 27088: heap size 242 MB, throughput 0.996292
Reading from 27087: heap size 722 MB, throughput 0.870814
Reading from 27088: heap size 242 MB, throughput 0.997348
Reading from 27087: heap size 726 MB, throughput 0.722996
Reading from 27088: heap size 247 MB, throughput 0.998029
Reading from 27087: heap size 733 MB, throughput 0.493751
Reading from 27087: heap size 745 MB, throughput 0.316365
Reading from 27088: heap size 247 MB, throughput 0.99652
Reading from 27087: heap size 758 MB, throughput 0.343315
Reading from 27088: heap size 252 MB, throughput 0.997009
Reading from 27088: heap size 240 MB, throughput 0.998002
Reading from 27088: heap size 229 MB, throughput 0.996744
Equal recommendation: 1828 MB each
Reading from 27087: heap size 763 MB, throughput 0.0317095
Reading from 27088: heap size 218 MB, throughput 0.997623
Reading from 27088: heap size 208 MB, throughput 0.997148
Reading from 27087: heap size 829 MB, throughput 0.69586
Reading from 27088: heap size 199 MB, throughput 0.996509
Reading from 27087: heap size 836 MB, throughput 0.509042
Reading from 27088: heap size 190 MB, throughput 0.996042
Reading from 27087: heap size 831 MB, throughput 0.555677
Reading from 27087: heap size 835 MB, throughput 0.546589
Reading from 27088: heap size 181 MB, throughput 0.995901
Reading from 27088: heap size 173 MB, throughput 0.994141
Reading from 27088: heap size 166 MB, throughput 0.995866
Reading from 27088: heap size 158 MB, throughput 0.992634
Reading from 27088: heap size 162 MB, throughput 0.996883
Reading from 27088: heap size 155 MB, throughput 0.995621
Reading from 27088: heap size 149 MB, throughput 0.995877
Reading from 27087: heap size 836 MB, throughput 0.176336
Reading from 27088: heap size 142 MB, throughput 0.958379
Reading from 27087: heap size 918 MB, throughput 0.632053
Reading from 27088: heap size 140 MB, throughput 0.984157
Reading from 27088: heap size 147 MB, throughput 0.995244
Reading from 27087: heap size 924 MB, throughput 0.792851
Reading from 27088: heap size 156 MB, throughput 0.995904
Reading from 27087: heap size 926 MB, throughput 0.757382
Reading from 27088: heap size 164 MB, throughput 0.994931
Reading from 27088: heap size 172 MB, throughput 0.996537
Reading from 27087: heap size 934 MB, throughput 0.865237
Reading from 27088: heap size 180 MB, throughput 0.996852
Reading from 27087: heap size 935 MB, throughput 0.71038
Reading from 27088: heap size 187 MB, throughput 0.993443
Reading from 27087: heap size 931 MB, throughput 0.783476
Reading from 27087: heap size 933 MB, throughput 0.74664
Reading from 27088: heap size 196 MB, throughput 0.99696
Reading from 27087: heap size 930 MB, throughput 0.79758
Reading from 27088: heap size 204 MB, throughput 0.997442
Reading from 27087: heap size 934 MB, throughput 0.768368
Reading from 27087: heap size 928 MB, throughput 0.79368
Reading from 27088: heap size 212 MB, throughput 0.997888
Reading from 27087: heap size 933 MB, throughput 0.784702
Reading from 27088: heap size 219 MB, throughput 0.996905
Reading from 27087: heap size 931 MB, throughput 0.806469
Reading from 27088: heap size 227 MB, throughput 0.99612
Reading from 27088: heap size 234 MB, throughput 0.996394
Reading from 27088: heap size 239 MB, throughput 0.996363
Reading from 27088: heap size 240 MB, throughput 0.995967
Reading from 27087: heap size 934 MB, throughput 0.971645
Reading from 27088: heap size 249 MB, throughput 0.997777
Reading from 27088: heap size 249 MB, throughput 0.997403
Reading from 27087: heap size 931 MB, throughput 0.916408
Reading from 27088: heap size 257 MB, throughput 0.997572
Reading from 27088: heap size 257 MB, throughput 0.996789
Equal recommendation: 1828 MB each
Reading from 27087: heap size 935 MB, throughput 0.141775
Reading from 27088: heap size 264 MB, throughput 0.998651
Reading from 27087: heap size 1008 MB, throughput 0.550397
Reading from 27088: heap size 264 MB, throughput 0.998163
Reading from 27087: heap size 1015 MB, throughput 0.781326
Reading from 27087: heap size 1013 MB, throughput 0.829848
Reading from 27088: heap size 270 MB, throughput 0.996816
Reading from 27087: heap size 1017 MB, throughput 0.814155
Reading from 27088: heap size 271 MB, throughput 0.995991
Reading from 27087: heap size 1017 MB, throughput 0.802556
Reading from 27088: heap size 278 MB, throughput 0.997374
Reading from 27087: heap size 1020 MB, throughput 0.814729
Reading from 27088: heap size 278 MB, throughput 0.997837
Reading from 27087: heap size 1021 MB, throughput 0.817659
Reading from 27088: heap size 285 MB, throughput 0.99754
Reading from 27087: heap size 1025 MB, throughput 0.654815
Reading from 27088: heap size 286 MB, throughput 0.994192
Reading from 27087: heap size 1008 MB, throughput 0.661724
Reading from 27087: heap size 1032 MB, throughput 0.578164
Reading from 27088: heap size 294 MB, throughput 0.997247
Reading from 27087: heap size 1049 MB, throughput 0.604807
Reading from 27087: heap size 1060 MB, throughput 0.54786
Reading from 27088: heap size 294 MB, throughput 0.99644
Reading from 27088: heap size 302 MB, throughput 0.995731
Reading from 27088: heap size 303 MB, throughput 0.99616
Reading from 27088: heap size 314 MB, throughput 0.995745
Reading from 27087: heap size 1079 MB, throughput 0.0700856
Reading from 27088: heap size 314 MB, throughput 0.995628
Reading from 27087: heap size 1189 MB, throughput 0.470626
Reading from 27087: heap size 1201 MB, throughput 0.604926
Reading from 27088: heap size 326 MB, throughput 0.995989
Reading from 27087: heap size 1203 MB, throughput 0.613158
Reading from 27087: heap size 1217 MB, throughput 0.6386
Reading from 27088: heap size 326 MB, throughput 0.994721
Reading from 27088: heap size 339 MB, throughput 0.997144
Reading from 27088: heap size 339 MB, throughput 0.996191
Reading from 27088: heap size 352 MB, throughput 0.997902
Reading from 27088: heap size 352 MB, throughput 0.97635
Reading from 27087: heap size 1220 MB, throughput 0.954587
Equal recommendation: 1828 MB each
Reading from 27088: heap size 365 MB, throughput 0.996637
Reading from 27088: heap size 365 MB, throughput 0.997693
Reading from 27088: heap size 384 MB, throughput 0.997647
Reading from 27087: heap size 1222 MB, throughput 0.94447
Reading from 27088: heap size 386 MB, throughput 0.997237
Reading from 27088: heap size 403 MB, throughput 0.997982
Reading from 27088: heap size 403 MB, throughput 0.998074
Reading from 27088: heap size 416 MB, throughput 0.998366
Reading from 27087: heap size 1230 MB, throughput 0.941615
Reading from 27088: heap size 417 MB, throughput 0.997044
Reading from 27088: heap size 430 MB, throughput 0.998092
Reading from 27088: heap size 430 MB, throughput 0.996986
Reading from 27087: heap size 1223 MB, throughput 0.936404
Reading from 27088: heap size 443 MB, throughput 0.998173
Reading from 27088: heap size 443 MB, throughput 0.997579
Reading from 27088: heap size 455 MB, throughput 0.996765
Reading from 27087: heap size 1233 MB, throughput 0.928575
Reading from 27088: heap size 455 MB, throughput 0.996612
Reading from 27088: heap size 468 MB, throughput 0.997579
Equal recommendation: 1828 MB each
Reading from 27088: heap size 468 MB, throughput 0.997754
Reading from 27087: heap size 1238 MB, throughput 0.934089
Reading from 27088: heap size 481 MB, throughput 0.998106
Reading from 27088: heap size 481 MB, throughput 0.995434
Reading from 27088: heap size 494 MB, throughput 0.99743
Reading from 27087: heap size 1241 MB, throughput 0.9432
Reading from 27088: heap size 494 MB, throughput 0.997107
Reading from 27088: heap size 508 MB, throughput 0.997306
Reading from 27088: heap size 508 MB, throughput 0.996961
Reading from 27087: heap size 1244 MB, throughput 0.944964
Reading from 27088: heap size 523 MB, throughput 0.997807
Reading from 27088: heap size 523 MB, throughput 0.997937
Reading from 27088: heap size 536 MB, throughput 0.99765
Reading from 27087: heap size 1249 MB, throughput 0.939224
Reading from 27088: heap size 537 MB, throughput 0.997766
Reading from 27088: heap size 551 MB, throughput 0.997711
Reading from 27088: heap size 551 MB, throughput 0.998236
Equal recommendation: 1828 MB each
Reading from 27087: heap size 1245 MB, throughput 0.953971
Reading from 27088: heap size 563 MB, throughput 0.99803
Reading from 27088: heap size 565 MB, throughput 0.997157
Reading from 27088: heap size 578 MB, throughput 0.998122
Reading from 27087: heap size 1250 MB, throughput 0.942333
Reading from 27088: heap size 578 MB, throughput 0.997911
Reading from 27088: heap size 592 MB, throughput 0.99782
Reading from 27088: heap size 592 MB, throughput 0.998238
Reading from 27087: heap size 1259 MB, throughput 0.94542
Reading from 27088: heap size 605 MB, throughput 0.997903
Reading from 27088: heap size 605 MB, throughput 0.997669
Reading from 27088: heap size 619 MB, throughput 0.998102
Reading from 27087: heap size 1260 MB, throughput 0.938803
Reading from 27088: heap size 619 MB, throughput 0.998349
Reading from 27088: heap size 632 MB, throughput 0.998709
Equal recommendation: 1828 MB each
Reading from 27087: heap size 1269 MB, throughput 0.942489
Reading from 27088: heap size 632 MB, throughput 0.998007
Reading from 27088: heap size 645 MB, throughput 0.998152
Reading from 27088: heap size 645 MB, throughput 0.998328
Reading from 27087: heap size 1273 MB, throughput 0.944483
Reading from 27088: heap size 658 MB, throughput 0.998673
Reading from 27088: heap size 658 MB, throughput 0.998396
Reading from 27087: heap size 1282 MB, throughput 0.940656
Reading from 27088: heap size 671 MB, throughput 0.998624
Reading from 27088: heap size 671 MB, throughput 0.998221
Reading from 27088: heap size 683 MB, throughput 0.998877
Reading from 27087: heap size 1288 MB, throughput 0.93184
Reading from 27088: heap size 683 MB, throughput 0.998791
Equal recommendation: 1828 MB each
Reading from 27088: heap size 693 MB, throughput 0.998586
Reading from 27088: heap size 694 MB, throughput 0.997206
Reading from 27088: heap size 705 MB, throughput 0.998857
Reading from 27087: heap size 1299 MB, throughput 0.545263
Reading from 27088: heap size 705 MB, throughput 0.998532
Reading from 27088: heap size 717 MB, throughput 0.99873
Reading from 27087: heap size 1379 MB, throughput 0.98352
Reading from 27088: heap size 718 MB, throughput 0.998451
Reading from 27088: heap size 730 MB, throughput 0.99812
Reading from 27088: heap size 730 MB, throughput 0.998693
Reading from 27087: heap size 1384 MB, throughput 0.982187
Reading from 27088: heap size 743 MB, throughput 0.998602
Equal recommendation: 1828 MB each
Reading from 27088: heap size 743 MB, throughput 0.998127
Reading from 27087: heap size 1393 MB, throughput 0.977804
Reading from 27088: heap size 756 MB, throughput 0.998589
Reading from 27088: heap size 756 MB, throughput 0.998603
Reading from 27088: heap size 769 MB, throughput 0.998444
Reading from 27087: heap size 1405 MB, throughput 0.972777
Reading from 27088: heap size 769 MB, throughput 0.998502
Reading from 27088: heap size 782 MB, throughput 0.998324
Reading from 27087: heap size 1406 MB, throughput 0.970361
Reading from 27088: heap size 782 MB, throughput 0.998502
Reading from 27088: heap size 796 MB, throughput 0.998739
Equal recommendation: 1828 MB each
Reading from 27087: heap size 1400 MB, throughput 0.967207
Reading from 27088: heap size 796 MB, throughput 0.998361
Reading from 27088: heap size 810 MB, throughput 0.998666
Reading from 27088: heap size 810 MB, throughput 0.997854
Reading from 27087: heap size 1405 MB, throughput 0.964968
Reading from 27088: heap size 824 MB, throughput 0.998571
Reading from 27088: heap size 824 MB, throughput 0.998645
Reading from 27087: heap size 1389 MB, throughput 0.958013
Reading from 27088: heap size 838 MB, throughput 0.998673
Reading from 27088: heap size 839 MB, throughput 0.998771
Reading from 27087: heap size 1397 MB, throughput 0.961807
Reading from 27088: heap size 853 MB, throughput 0.998936
Equal recommendation: 1828 MB each
Reading from 27088: heap size 853 MB, throughput 0.998462
Reading from 27087: heap size 1396 MB, throughput 0.953319
Reading from 27088: heap size 866 MB, throughput 0.998953
Reading from 27088: heap size 866 MB, throughput 0.998583
Reading from 27088: heap size 879 MB, throughput 0.998659
Reading from 27087: heap size 1398 MB, throughput 0.957712
Reading from 27088: heap size 879 MB, throughput 0.998517
Reading from 27088: heap size 893 MB, throughput 0.998762
Reading from 27087: heap size 1405 MB, throughput 0.95366
Reading from 27088: heap size 893 MB, throughput 0.99877
Equal recommendation: 1828 MB each
Reading from 27088: heap size 906 MB, throughput 0.998951
Reading from 27087: heap size 1410 MB, throughput 0.949686
Reading from 27088: heap size 906 MB, throughput 0.998588
Reading from 27088: heap size 920 MB, throughput 0.99884
Reading from 27087: heap size 1418 MB, throughput 0.939256
Reading from 27088: heap size 920 MB, throughput 0.998832
Reading from 27088: heap size 933 MB, throughput 0.998432
Reading from 27088: heap size 933 MB, throughput 0.998922
Reading from 27087: heap size 1427 MB, throughput 0.946936
Reading from 27088: heap size 947 MB, throughput 0.998709
Equal recommendation: 1828 MB each
Reading from 27088: heap size 948 MB, throughput 0.998846
Reading from 27087: heap size 1437 MB, throughput 0.943835
Reading from 27088: heap size 961 MB, throughput 0.9986
Reading from 27088: heap size 961 MB, throughput 0.998846
Reading from 27087: heap size 1449 MB, throughput 0.931985
Reading from 27088: heap size 975 MB, throughput 0.998723
Reading from 27088: heap size 975 MB, throughput 0.998697
Reading from 27088: heap size 990 MB, throughput 0.999121
Equal recommendation: 1828 MB each
Reading from 27088: heap size 990 MB, throughput 0.998721
Reading from 27088: heap size 1003 MB, throughput 0.998772
Reading from 27088: heap size 1003 MB, throughput 0.999022
Reading from 27088: heap size 1017 MB, throughput 0.99893
Reading from 27088: heap size 1017 MB, throughput 0.999043
Reading from 27088: heap size 1030 MB, throughput 0.999092
Reading from 27088: heap size 1030 MB, throughput 0.998925
Equal recommendation: 1828 MB each
Reading from 27087: heap size 1462 MB, throughput 0.982929
Reading from 27088: heap size 1043 MB, throughput 0.998933
Reading from 27088: heap size 1043 MB, throughput 0.998927
Reading from 27088: heap size 1056 MB, throughput 0.999064
Reading from 27088: heap size 1056 MB, throughput 0.998872
Reading from 27088: heap size 1069 MB, throughput 0.999154
Reading from 27088: heap size 1069 MB, throughput 0.999069
Equal recommendation: 1828 MB each
Reading from 27088: heap size 1081 MB, throughput 0.999017
Reading from 27088: heap size 1081 MB, throughput 0.999006
Reading from 27088: heap size 1094 MB, throughput 0.998903
Reading from 27087: heap size 1470 MB, throughput 0.982979
Reading from 27088: heap size 1094 MB, throughput 0.99915
Reading from 27088: heap size 1106 MB, throughput 0.999064
Reading from 27088: heap size 1107 MB, throughput 0.999002
Equal recommendation: 1828 MB each
Reading from 27088: heap size 1119 MB, throughput 0.998437
Reading from 27087: heap size 1474 MB, throughput 0.973353
Reading from 27087: heap size 1513 MB, throughput 0.698462
Reading from 27087: heap size 1506 MB, throughput 0.615175
Reading from 27087: heap size 1544 MB, throughput 0.578709
Reading from 27088: heap size 1119 MB, throughput 0.992734
Reading from 27087: heap size 1579 MB, throughput 0.17355
Reading from 27087: heap size 1690 MB, throughput 0.991649
Reading from 27087: heap size 1737 MB, throughput 0.983207
Reading from 27088: heap size 1134 MB, throughput 0.999032
Reading from 27088: heap size 1134 MB, throughput 0.999132
Reading from 27087: heap size 1737 MB, throughput 0.986327
Reading from 27088: heap size 1158 MB, throughput 0.998933
Equal recommendation: 1828 MB each
Reading from 27088: heap size 1162 MB, throughput 0.998961
Reading from 27087: heap size 1759 MB, throughput 0.98284
Reading from 27088: heap size 1181 MB, throughput 0.999178
Reading from 27087: heap size 1764 MB, throughput 0.978067
Reading from 27088: heap size 1181 MB, throughput 0.999003
Reading from 27088: heap size 1198 MB, throughput 0.999101
Reading from 27087: heap size 1768 MB, throughput 0.98075
Reading from 27088: heap size 1200 MB, throughput 0.99914
Equal recommendation: 1828 MB each
Reading from 27088: heap size 1215 MB, throughput 0.99914
Reading from 27087: heap size 1774 MB, throughput 0.970596
Reading from 27088: heap size 1215 MB, throughput 0.998939
Reading from 27088: heap size 1230 MB, throughput 0.999202
Reading from 27087: heap size 1742 MB, throughput 0.970799
Reading from 27088: heap size 1230 MB, throughput 0.999426
Reading from 27088: heap size 1230 MB, throughput 0.999232
Reading from 27087: heap size 1456 MB, throughput 0.966744
Reading from 27088: heap size 1230 MB, throughput 0.999271
Equal recommendation: 1828 MB each
Reading from 27088: heap size 1230 MB, throughput 0.999424
Reading from 27087: heap size 1713 MB, throughput 0.96839
Reading from 27088: heap size 1230 MB, throughput 0.999283
Reading from 27088: heap size 1230 MB, throughput 0.999401
Reading from 27087: heap size 1493 MB, throughput 0.964691
Reading from 27088: heap size 1230 MB, throughput 0.999293
Reading from 27088: heap size 1230 MB, throughput 0.999347
Reading from 27087: heap size 1686 MB, throughput 0.955988
Equal recommendation: 1828 MB each
Reading from 27088: heap size 1230 MB, throughput 0.999313
Reading from 27088: heap size 1230 MB, throughput 0.99928
Reading from 27087: heap size 1707 MB, throughput 0.955131
Reading from 27088: heap size 1170 MB, throughput 0.992635
Reading from 27088: heap size 1112 MB, throughput 0.999337
Reading from 27087: heap size 1691 MB, throughput 0.949523
Reading from 27088: heap size 1126 MB, throughput 0.999347
Equal recommendation: 1828 MB each
Reading from 27088: heap size 1141 MB, throughput 0.999264
Reading from 27087: heap size 1699 MB, throughput 0.951931
Reading from 27088: heap size 1155 MB, throughput 0.999039
Reading from 27088: heap size 1167 MB, throughput 0.999306
Reading from 27087: heap size 1712 MB, throughput 0.953822
Reading from 27088: heap size 1183 MB, throughput 0.999234
Reading from 27088: heap size 1197 MB, throughput 0.999299
Reading from 27088: heap size 1212 MB, throughput 0.998873
Reading from 27087: heap size 1712 MB, throughput 0.95069
Equal recommendation: 1828 MB each
Reading from 27088: heap size 1219 MB, throughput 0.999228
Reading from 27088: heap size 1220 MB, throughput 0.998539
Reading from 27087: heap size 1727 MB, throughput 0.946377
Reading from 27088: heap size 1230 MB, throughput 0.998921
Reading from 27088: heap size 1230 MB, throughput 0.999114
Reading from 27087: heap size 1733 MB, throughput 0.948323
Reading from 27088: heap size 1230 MB, throughput 0.998974
Equal recommendation: 1828 MB each
Reading from 27088: heap size 1229 MB, throughput 0.999124
Reading from 27087: heap size 1752 MB, throughput 0.954579
Reading from 27088: heap size 1228 MB, throughput 0.999023
Reading from 27088: heap size 1229 MB, throughput 0.999148
Reading from 27088: heap size 1229 MB, throughput 0.998231
Reading from 27087: heap size 1757 MB, throughput 0.951085
Reading from 27088: heap size 1229 MB, throughput 0.999033
Equal recommendation: 1828 MB each
Reading from 27088: heap size 1228 MB, throughput 0.999244
Reading from 27087: heap size 1777 MB, throughput 0.952792
Reading from 27088: heap size 1228 MB, throughput 0.999273
Reading from 27088: heap size 1229 MB, throughput 0.999311
Reading from 27087: heap size 1780 MB, throughput 0.948571
Reading from 27088: heap size 1227 MB, throughput 0.999315
Reading from 27088: heap size 1229 MB, throughput 0.99916
Reading from 27088: heap size 1229 MB, throughput 0.999381
Equal recommendation: 1828 MB each
Reading from 27087: heap size 1800 MB, throughput 0.946888
Reading from 27088: heap size 1230 MB, throughput 0.999276
Reading from 27088: heap size 1230 MB, throughput 0.999426
Reading from 27087: heap size 1802 MB, throughput 0.947935
Reading from 27088: heap size 1229 MB, throughput 0.999375
Reading from 27088: heap size 1229 MB, throughput 0.999365
Reading from 27088: heap size 1230 MB, throughput 0.999288
Equal recommendation: 1828 MB each
Reading from 27088: heap size 1230 MB, throughput 0.999012
Reading from 27087: heap size 1824 MB, throughput 0.727371
Reading from 27088: heap size 1230 MB, throughput 0.999414
Reading from 27088: heap size 1228 MB, throughput 0.999348
Reading from 27087: heap size 1899 MB, throughput 0.98622
Reading from 27088: heap size 1230 MB, throughput 0.999287
Reading from 27088: heap size 1230 MB, throughput 0.999378
Equal recommendation: 1828 MB each
Reading from 27088: heap size 1231 MB, throughput 0.999377
Reading from 27088: heap size 1231 MB, throughput 0.999449
Reading from 27088: heap size 1231 MB, throughput 0.999281
Reading from 27088: heap size 1229 MB, throughput 0.999322
Reading from 27088: heap size 1230 MB, throughput 0.99943
Equal recommendation: 1828 MB each
Reading from 27088: heap size 1230 MB, throughput 0.999301
Reading from 27088: heap size 1231 MB, throughput 0.999418
Reading from 27088: heap size 1231 MB, throughput 0.999223
Reading from 27088: heap size 1231 MB, throughput 0.999231
Reading from 27088: heap size 1231 MB, throughput 0.999412
Reading from 27088: heap size 1231 MB, throughput 0.999304
Equal recommendation: 1828 MB each
Reading from 27088: heap size 1230 MB, throughput 0.999406
Reading from 27088: heap size 1231 MB, throughput 0.999442
Reading from 27088: heap size 1231 MB, throughput 0.999307
Reading from 27088: heap size 1232 MB, throughput 0.999321
Reading from 27087: heap size 1885 MB, throughput 0.996293
Reading from 27088: heap size 1232 MB, throughput 0.999409
Equal recommendation: 1828 MB each
Reading from 27088: heap size 1232 MB, throughput 0.999413
Reading from 27088: heap size 1232 MB, throughput 0.999401
Client 27088 died
Clients: 1
Reading from 27087: heap size 1679 MB, throughput 0.991079
Reading from 27087: heap size 1890 MB, throughput 0.881265
Reading from 27087: heap size 1881 MB, throughput 0.848285
Reading from 27087: heap size 1885 MB, throughput 0.851049
Client 27087 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
