#!/bin/bash
# The 'time' command doesn't propagate signals to its children, making it hard to reliably clean up after it.
# This script makes sure all the child processes get the appropriate signals

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function cleanup()
{
    test ! -z "${PID}" && pkill -P "${PID}" &>/dev/null
    wait
    exit
}

trap "cleanup" SIGINT SIGHUP SIGTERM

"$(which time)" "--verbose" "--append" "-o" "${1}" "${DIR}/save_pid.sh" "${@}" &
PID="${!}"

wait
cleanup
