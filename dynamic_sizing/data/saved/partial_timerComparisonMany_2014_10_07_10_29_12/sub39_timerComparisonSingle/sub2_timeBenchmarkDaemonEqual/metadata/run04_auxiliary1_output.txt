economemd
    total memory: 1728 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub39_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 29113: heap size 9 MB, throughput 0.972135
Clients: 1
Client 29113 has a minimum heap size of 12 MB
Reading from 29112: heap size 9 MB, throughput 0.986395
Clients: 2
Client 29112 has a minimum heap size of 276 MB
Reading from 29112: heap size 9 MB, throughput 0.968999
Reading from 29113: heap size 9 MB, throughput 0.94577
Reading from 29112: heap size 11 MB, throughput 0.973291
Reading from 29113: heap size 11 MB, throughput 0.985026
Reading from 29112: heap size 11 MB, throughput 0.985475
Reading from 29113: heap size 11 MB, throughput 0.980575
Reading from 29112: heap size 15 MB, throughput 0.765982
Reading from 29113: heap size 15 MB, throughput 0.988078
Reading from 29112: heap size 20 MB, throughput 0.97176
Reading from 29112: heap size 24 MB, throughput 0.874619
Reading from 29112: heap size 28 MB, throughput 0.92383
Reading from 29113: heap size 15 MB, throughput 0.982113
Reading from 29112: heap size 28 MB, throughput 0.775357
Reading from 29112: heap size 30 MB, throughput 0.220008
Reading from 29112: heap size 43 MB, throughput 0.847017
Reading from 29112: heap size 44 MB, throughput 0.83305
Reading from 29112: heap size 49 MB, throughput 0.796282
Reading from 29113: heap size 25 MB, throughput 0.931759
Reading from 29112: heap size 50 MB, throughput 0.255784
Reading from 29113: heap size 30 MB, throughput 0.990462
Reading from 29112: heap size 74 MB, throughput 0.309247
Reading from 29112: heap size 93 MB, throughput 0.7738
Reading from 29112: heap size 96 MB, throughput 0.840329
Reading from 29112: heap size 98 MB, throughput 0.797486
Reading from 29113: heap size 38 MB, throughput 0.987518
Reading from 29112: heap size 103 MB, throughput 0.71363
Reading from 29113: heap size 44 MB, throughput 0.988296
Reading from 29112: heap size 104 MB, throughput 0.23923
Reading from 29112: heap size 139 MB, throughput 0.692287
Reading from 29113: heap size 45 MB, throughput 0.98275
Reading from 29113: heap size 46 MB, throughput 0.98195
Reading from 29112: heap size 140 MB, throughput 0.170106
Reading from 29113: heap size 54 MB, throughput 0.949921
Reading from 29112: heap size 182 MB, throughput 0.704975
Reading from 29113: heap size 54 MB, throughput 0.970572
Reading from 29112: heap size 182 MB, throughput 0.632306
Reading from 29113: heap size 65 MB, throughput 0.940278
Reading from 29112: heap size 185 MB, throughput 0.716894
Reading from 29112: heap size 188 MB, throughput 0.710693
Reading from 29113: heap size 66 MB, throughput 0.794913
Reading from 29113: heap size 87 MB, throughput 0.993905
Reading from 29112: heap size 192 MB, throughput 0.157007
Reading from 29113: heap size 87 MB, throughput 0.994578
Reading from 29112: heap size 239 MB, throughput 0.887098
Reading from 29112: heap size 243 MB, throughput 0.918092
Reading from 29112: heap size 246 MB, throughput 0.674775
Reading from 29113: heap size 100 MB, throughput 0.997791
Reading from 29112: heap size 252 MB, throughput 0.845263
Reading from 29112: heap size 255 MB, throughput 0.72126
Reading from 29112: heap size 262 MB, throughput 0.864606
Reading from 29112: heap size 264 MB, throughput 0.854579
Reading from 29113: heap size 101 MB, throughput 0.99742
Reading from 29112: heap size 269 MB, throughput 0.854363
Reading from 29112: heap size 271 MB, throughput 0.586435
Reading from 29112: heap size 277 MB, throughput 0.626677
Reading from 29112: heap size 281 MB, throughput 0.629253
Reading from 29112: heap size 288 MB, throughput 0.744727
Reading from 29113: heap size 112 MB, throughput 0.996979
Reading from 29112: heap size 290 MB, throughput 0.912029
Reading from 29112: heap size 297 MB, throughput 0.624268
Reading from 29112: heap size 299 MB, throughput 0.798014
Reading from 29113: heap size 114 MB, throughput 0.995573
Reading from 29112: heap size 296 MB, throughput 0.095566
Reading from 29113: heap size 124 MB, throughput 0.987315
Reading from 29112: heap size 345 MB, throughput 0.644433
Reading from 29112: heap size 350 MB, throughput 0.931211
Reading from 29112: heap size 350 MB, throughput 0.824959
Reading from 29112: heap size 348 MB, throughput 0.700091
Reading from 29112: heap size 351 MB, throughput 0.674197
Reading from 29112: heap size 353 MB, throughput 0.721616
Reading from 29112: heap size 354 MB, throughput 0.656335
Reading from 29113: heap size 126 MB, throughput 0.99755
Reading from 29112: heap size 361 MB, throughput 0.696646
Equal recommendation: 864 MB each
Reading from 29112: heap size 361 MB, throughput 0.968475
Reading from 29113: heap size 136 MB, throughput 0.996256
Reading from 29113: heap size 138 MB, throughput 0.996368
Reading from 29112: heap size 367 MB, throughput 0.977572
Reading from 29113: heap size 145 MB, throughput 0.998026
Reading from 29113: heap size 146 MB, throughput 0.99744
Reading from 29112: heap size 369 MB, throughput 0.984444
Reading from 29113: heap size 152 MB, throughput 0.996656
Reading from 29112: heap size 369 MB, throughput 0.977059
Reading from 29113: heap size 153 MB, throughput 0.996335
Reading from 29113: heap size 159 MB, throughput 0.99753
Reading from 29112: heap size 372 MB, throughput 0.977464
Reading from 29113: heap size 159 MB, throughput 0.997573
Reading from 29112: heap size 375 MB, throughput 0.982291
Reading from 29113: heap size 165 MB, throughput 0.997564
Equal recommendation: 864 MB each
Reading from 29113: heap size 165 MB, throughput 0.998209
Reading from 29112: heap size 377 MB, throughput 0.973627
Reading from 29113: heap size 169 MB, throughput 0.998576
Reading from 29112: heap size 374 MB, throughput 0.980069
Reading from 29113: heap size 170 MB, throughput 0.997328
Reading from 29113: heap size 173 MB, throughput 0.997619
Reading from 29112: heap size 377 MB, throughput 0.960688
Reading from 29113: heap size 173 MB, throughput 0.99017
Reading from 29113: heap size 177 MB, throughput 0.993461
Reading from 29112: heap size 374 MB, throughput 0.979581
Reading from 29113: heap size 178 MB, throughput 0.997482
Reading from 29112: heap size 376 MB, throughput 0.980909
Reading from 29113: heap size 185 MB, throughput 0.997827
Reading from 29113: heap size 186 MB, throughput 0.99759
Reading from 29112: heap size 377 MB, throughput 0.982546
Reading from 29113: heap size 191 MB, throughput 0.997541
Equal recommendation: 864 MB each
Reading from 29112: heap size 378 MB, throughput 0.975595
Reading from 29113: heap size 191 MB, throughput 0.997698
Reading from 29113: heap size 196 MB, throughput 0.998146
Reading from 29112: heap size 380 MB, throughput 0.988754
Reading from 29113: heap size 196 MB, throughput 0.99714
Reading from 29112: heap size 381 MB, throughput 0.978668
Reading from 29112: heap size 379 MB, throughput 0.855886
Reading from 29112: heap size 380 MB, throughput 0.816058
Reading from 29112: heap size 387 MB, throughput 0.850857
Reading from 29112: heap size 390 MB, throughput 0.89376
Reading from 29113: heap size 201 MB, throughput 0.997914
Reading from 29112: heap size 397 MB, throughput 0.986168
Reading from 29113: heap size 201 MB, throughput 0.997173
Reading from 29113: heap size 205 MB, throughput 0.99764
Reading from 29112: heap size 398 MB, throughput 0.990267
Reading from 29113: heap size 205 MB, throughput 0.996831
Reading from 29112: heap size 399 MB, throughput 0.988075
Reading from 29113: heap size 210 MB, throughput 0.997732
Equal recommendation: 864 MB each
Reading from 29113: heap size 210 MB, throughput 0.997597
Reading from 29112: heap size 401 MB, throughput 0.987897
Reading from 29113: heap size 214 MB, throughput 0.996145
Reading from 29113: heap size 214 MB, throughput 0.988655
Reading from 29112: heap size 399 MB, throughput 0.987076
Reading from 29113: heap size 219 MB, throughput 0.997841
Reading from 29112: heap size 401 MB, throughput 0.986095
Reading from 29113: heap size 220 MB, throughput 0.99818
Reading from 29113: heap size 225 MB, throughput 0.998093
Reading from 29112: heap size 398 MB, throughput 0.813019
Reading from 29113: heap size 226 MB, throughput 0.997935
Equal recommendation: 864 MB each
Reading from 29113: heap size 230 MB, throughput 0.996878
Reading from 29112: heap size 437 MB, throughput 0.996082
Reading from 29113: heap size 231 MB, throughput 0.997525
Reading from 29112: heap size 434 MB, throughput 0.993811
Reading from 29113: heap size 236 MB, throughput 0.998146
Reading from 29112: heap size 438 MB, throughput 0.993375
Reading from 29113: heap size 224 MB, throughput 0.997977
Reading from 29113: heap size 213 MB, throughput 0.998051
Reading from 29112: heap size 442 MB, throughput 0.991524
Reading from 29112: heap size 409 MB, throughput 0.974264
Reading from 29112: heap size 438 MB, throughput 0.874312
Reading from 29112: heap size 441 MB, throughput 0.871637
Reading from 29112: heap size 446 MB, throughput 0.888241
Reading from 29113: heap size 204 MB, throughput 0.998052
Reading from 29113: heap size 195 MB, throughput 0.997866
Reading from 29112: heap size 448 MB, throughput 0.991985
Reading from 29113: heap size 187 MB, throughput 0.997514
Equal recommendation: 864 MB each
Reading from 29113: heap size 179 MB, throughput 0.991408
Reading from 29113: heap size 173 MB, throughput 0.801052
Reading from 29112: heap size 452 MB, throughput 0.992718
Reading from 29113: heap size 182 MB, throughput 0.998094
Reading from 29113: heap size 189 MB, throughput 0.998606
Reading from 29112: heap size 454 MB, throughput 0.989673
Reading from 29113: heap size 196 MB, throughput 0.998279
Reading from 29112: heap size 451 MB, throughput 0.990992
Reading from 29113: heap size 208 MB, throughput 0.998974
Reading from 29113: heap size 209 MB, throughput 0.998999
Reading from 29112: heap size 454 MB, throughput 0.990057
Reading from 29113: heap size 217 MB, throughput 0.998957
Reading from 29113: heap size 220 MB, throughput 0.998869
Reading from 29112: heap size 451 MB, throughput 0.990659
Equal recommendation: 864 MB each
Reading from 29113: heap size 220 MB, throughput 0.99872
Reading from 29112: heap size 453 MB, throughput 0.989656
Reading from 29113: heap size 224 MB, throughput 0.998944
Reading from 29113: heap size 224 MB, throughput 0.998961
Reading from 29112: heap size 454 MB, throughput 0.989493
Reading from 29113: heap size 228 MB, throughput 0.998991
Reading from 29113: heap size 228 MB, throughput 0.998748
Reading from 29112: heap size 454 MB, throughput 0.989233
Reading from 29113: heap size 231 MB, throughput 0.997361
Reading from 29113: heap size 231 MB, throughput 0.992268
Reading from 29112: heap size 457 MB, throughput 0.992083
Reading from 29113: heap size 235 MB, throughput 0.998013
Reading from 29112: heap size 457 MB, throughput 0.975605
Equal recommendation: 864 MB each
Reading from 29112: heap size 456 MB, throughput 0.897689
Reading from 29112: heap size 458 MB, throughput 0.890119
Reading from 29113: heap size 236 MB, throughput 0.997509
Reading from 29112: heap size 462 MB, throughput 0.981857
Reading from 29113: heap size 242 MB, throughput 0.998397
Reading from 29112: heap size 463 MB, throughput 0.993784
Reading from 29113: heap size 243 MB, throughput 0.998129
Reading from 29113: heap size 249 MB, throughput 0.998151
Reading from 29112: heap size 466 MB, throughput 0.989272
Reading from 29113: heap size 249 MB, throughput 0.997886
Reading from 29112: heap size 468 MB, throughput 0.991698
Reading from 29113: heap size 254 MB, throughput 0.997248
Reading from 29112: heap size 467 MB, throughput 0.990389
Equal recommendation: 864 MB each
Reading from 29113: heap size 255 MB, throughput 0.996867
Reading from 29113: heap size 261 MB, throughput 0.998148
Reading from 29112: heap size 469 MB, throughput 0.992405
Reading from 29113: heap size 261 MB, throughput 0.996411
Reading from 29113: heap size 268 MB, throughput 0.997097
Reading from 29112: heap size 466 MB, throughput 0.985238
Reading from 29113: heap size 268 MB, throughput 0.992808
Reading from 29113: heap size 276 MB, throughput 0.998083
Reading from 29112: heap size 468 MB, throughput 0.989782
Reading from 29113: heap size 277 MB, throughput 0.996951
Reading from 29112: heap size 469 MB, throughput 0.987623
Equal recommendation: 864 MB each
Reading from 29113: heap size 285 MB, throughput 0.998307
Reading from 29112: heap size 469 MB, throughput 0.992161
Reading from 29112: heap size 470 MB, throughput 0.930613
Reading from 29112: heap size 471 MB, throughput 0.880072
Reading from 29112: heap size 477 MB, throughput 0.963795
Reading from 29113: heap size 286 MB, throughput 0.99841
Reading from 29113: heap size 292 MB, throughput 0.997595
Reading from 29112: heap size 478 MB, throughput 0.994649
Reading from 29113: heap size 293 MB, throughput 0.998057
Reading from 29112: heap size 482 MB, throughput 0.993895
Reading from 29113: heap size 301 MB, throughput 0.998069
Equal recommendation: 864 MB each
Reading from 29112: heap size 483 MB, throughput 0.991829
Reading from 29113: heap size 301 MB, throughput 0.997531
Reading from 29113: heap size 309 MB, throughput 0.997895
Reading from 29112: heap size 482 MB, throughput 0.989258
Reading from 29113: heap size 309 MB, throughput 0.992803
Reading from 29113: heap size 316 MB, throughput 0.99827
Reading from 29112: heap size 484 MB, throughput 0.990994
Reading from 29113: heap size 317 MB, throughput 0.998131
Reading from 29112: heap size 484 MB, throughput 0.992268
Reading from 29113: heap size 324 MB, throughput 0.998393
Equal recommendation: 864 MB each
Reading from 29112: heap size 485 MB, throughput 0.987546
Reading from 29113: heap size 326 MB, throughput 0.997936
Reading from 29112: heap size 487 MB, throughput 0.988276
Reading from 29113: heap size 334 MB, throughput 0.998343
Reading from 29112: heap size 487 MB, throughput 0.964606
Reading from 29112: heap size 489 MB, throughput 0.917382
Reading from 29112: heap size 491 MB, throughput 0.867262
Reading from 29113: heap size 334 MB, throughput 0.997903
Reading from 29112: heap size 497 MB, throughput 0.994517
Reading from 29113: heap size 341 MB, throughput 0.998615
Reading from 29113: heap size 342 MB, throughput 0.997094
Reading from 29112: heap size 497 MB, throughput 0.993008
Equal recommendation: 864 MB each
Reading from 29113: heap size 349 MB, throughput 0.99315
Reading from 29112: heap size 501 MB, throughput 0.993255
Reading from 29113: heap size 349 MB, throughput 0.997704
Reading from 29113: heap size 361 MB, throughput 0.997927
Reading from 29112: heap size 502 MB, throughput 0.991708
Reading from 29113: heap size 362 MB, throughput 0.998317
Reading from 29112: heap size 500 MB, throughput 0.992645
Equal recommendation: 864 MB each
Reading from 29113: heap size 370 MB, throughput 0.998241
Reading from 29112: heap size 503 MB, throughput 0.991035
Reading from 29113: heap size 371 MB, throughput 0.99827
Reading from 29112: heap size 505 MB, throughput 0.989869
Reading from 29113: heap size 379 MB, throughput 0.998421
Reading from 29112: heap size 505 MB, throughput 0.991264
Reading from 29112: heap size 506 MB, throughput 0.885538
Reading from 29113: heap size 380 MB, throughput 0.996913
Reading from 29112: heap size 508 MB, throughput 0.899086
Reading from 29113: heap size 389 MB, throughput 0.997548
Reading from 29112: heap size 514 MB, throughput 0.993537
Equal recommendation: 864 MB each
Reading from 29113: heap size 389 MB, throughput 0.998353
Reading from 29112: heap size 516 MB, throughput 0.994385
Reading from 29113: heap size 398 MB, throughput 0.998085
Reading from 29112: heap size 519 MB, throughput 0.994059
Reading from 29113: heap size 399 MB, throughput 0.998365
Reading from 29112: heap size 521 MB, throughput 0.993192
Reading from 29113: heap size 407 MB, throughput 0.998501
Equal recommendation: 864 MB each
Reading from 29113: heap size 408 MB, throughput 0.997891
Reading from 29112: heap size 520 MB, throughput 0.992841
Reading from 29113: heap size 416 MB, throughput 0.997798
Reading from 29112: heap size 522 MB, throughput 0.990078
Reading from 29113: heap size 416 MB, throughput 0.996858
Reading from 29113: heap size 425 MB, throughput 0.998556
Reading from 29112: heap size 525 MB, throughput 0.993668
Reading from 29112: heap size 525 MB, throughput 0.956438
Reading from 29112: heap size 525 MB, throughput 0.929123
Equal recommendation: 864 MB each
Reading from 29112: heap size 527 MB, throughput 0.992031
Reading from 29113: heap size 426 MB, throughput 0.998657
Reading from 29112: heap size 534 MB, throughput 0.994544
Reading from 29113: heap size 433 MB, throughput 0.998008
Reading from 29113: heap size 434 MB, throughput 0.998403
Reading from 29112: heap size 534 MB, throughput 0.993926
Reading from 29113: heap size 443 MB, throughput 0.998548
Equal recommendation: 864 MB each
Reading from 29112: heap size 535 MB, throughput 0.990721
Reading from 29113: heap size 443 MB, throughput 0.995764
Reading from 29113: heap size 451 MB, throughput 0.618423
Reading from 29112: heap size 536 MB, throughput 0.992795
Reading from 29113: heap size 453 MB, throughput 0.999128
Reading from 29112: heap size 536 MB, throughput 0.989938
Reading from 29113: heap size 483 MB, throughput 0.999336
Equal recommendation: 864 MB each
Reading from 29112: heap size 537 MB, throughput 0.995308
Reading from 29112: heap size 540 MB, throughput 0.952406
Reading from 29113: heap size 483 MB, throughput 0.999324
Reading from 29112: heap size 540 MB, throughput 0.910927
Reading from 29112: heap size 544 MB, throughput 0.993695
Reading from 29113: heap size 502 MB, throughput 0.999366
Reading from 29112: heap size 546 MB, throughput 0.99518
Reading from 29113: heap size 507 MB, throughput 0.998957
Reading from 29113: heap size 521 MB, throughput 0.997275
Equal recommendation: 864 MB each
Reading from 29112: heap size 548 MB, throughput 0.994829
Reading from 29113: heap size 524 MB, throughput 0.99897
Reading from 29112: heap size 550 MB, throughput 0.994007
Reading from 29113: heap size 538 MB, throughput 0.998285
Reading from 29112: heap size 548 MB, throughput 0.993036
Reading from 29113: heap size 541 MB, throughput 0.998895
Equal recommendation: 864 MB each
Reading from 29112: heap size 550 MB, throughput 0.992253
Reading from 29113: heap size 554 MB, throughput 0.998928
Reading from 29112: heap size 552 MB, throughput 0.991943
Reading from 29112: heap size 553 MB, throughput 0.926901
Reading from 29112: heap size 556 MB, throughput 0.987103
Reading from 29113: heap size 556 MB, throughput 0.996379
Reading from 29112: heap size 558 MB, throughput 0.995687
Reading from 29113: heap size 568 MB, throughput 0.998842
Equal recommendation: 864 MB each
Reading from 29112: heap size 561 MB, throughput 0.99435
Reading from 29113: heap size 569 MB, throughput 0.998457
Reading from 29112: heap size 562 MB, throughput 0.99419
Reading from 29113: heap size 583 MB, throughput 0.998627
Reading from 29112: heap size 559 MB, throughput 0.992426
Equal recommendation: 864 MB each
Reading from 29113: heap size 584 MB, throughput 0.998781
Reading from 29112: heap size 562 MB, throughput 0.991205
Reading from 29113: heap size 590 MB, throughput 0.99735
Reading from 29112: heap size 563 MB, throughput 0.994581
Reading from 29112: heap size 564 MB, throughput 0.936204
Reading from 29113: heap size 591 MB, throughput 0.998848
Reading from 29112: heap size 563 MB, throughput 0.988431
Equal recommendation: 864 MB each
Reading from 29112: heap size 565 MB, throughput 0.995123
Reading from 29113: heap size 590 MB, throughput 0.998681
Reading from 29112: heap size 566 MB, throughput 0.994631
Reading from 29113: heap size 590 MB, throughput 0.998732
Reading from 29112: heap size 568 MB, throughput 0.993633
Reading from 29113: heap size 590 MB, throughput 0.998531
Equal recommendation: 864 MB each
Reading from 29113: heap size 590 MB, throughput 0.997298
Reading from 29112: heap size 567 MB, throughput 0.993638
Reading from 29113: heap size 589 MB, throughput 0.998683
Reading from 29112: heap size 568 MB, throughput 0.992525
Reading from 29112: heap size 570 MB, throughput 0.992168
Reading from 29112: heap size 571 MB, throughput 0.928977
Reading from 29113: heap size 590 MB, throughput 0.998022
Equal recommendation: 864 MB each
Reading from 29112: heap size 574 MB, throughput 0.99253
Reading from 29113: heap size 589 MB, throughput 0.998752
Reading from 29112: heap size 576 MB, throughput 0.995265
Reading from 29112: heap size 577 MB, throughput 0.994959
Client 29113 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 29112: heap size 579 MB, throughput 0.993607
Reading from 29112: heap size 577 MB, throughput 0.993351
Reading from 29112: heap size 579 MB, throughput 0.995542
Reading from 29112: heap size 580 MB, throughput 0.98162
Reading from 29112: heap size 581 MB, throughput 0.928342
Recommendation: one client; give it all the memory
Reading from 29112: heap size 585 MB, throughput 0.994692
Reading from 29112: heap size 586 MB, throughput 0.994678
Reading from 29112: heap size 587 MB, throughput 0.993959
Recommendation: one client; give it all the memory
Reading from 29112: heap size 589 MB, throughput 0.993671
Reading from 29112: heap size 588 MB, throughput 0.99277
Reading from 29112: heap size 590 MB, throughput 0.99156
Reading from 29112: heap size 591 MB, throughput 0.938237
Client 29112 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
TimerThread: finished
ReadingThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
