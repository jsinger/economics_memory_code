#ifndef COMMANDSENDER_HPP
#define COMMANDSENDER_HPP

#include "Messages.hpp"
#include "SendSock.hpp"
#include "Utilities.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
    //! Send commands from a management program to a daemon.
    class CommandSender : public SendSock
    {
    public:
        CommandSender(const char* daemonName=economem::NAME_DEFAULT);

        bool sendCommand(economem::CommandType t, unsigned long long i=0, unsigned long long j=0);

    private:
        const char* m_DaemonName;
    };
}

/*! @} */

#endif
