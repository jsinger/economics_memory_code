#!/bin/bash
# Save information about the machine this script is running on

function usage()
{
    echo "Usage: $(basename "${0}") outdir"
    exit 1
}

test -z "${1}" && usage
test ! -d "${1}" && usage

OUTDIR="$(readlink -f "${1}")"
ME="$(whoami)@$(hostname)"

cat '/proc/cpuinfo' > "${OUTDIR}/cpu_${ME}.txt"

MEMORY_FILE="${OUTDIR}/memory_${ME}.txt"

free > "${MEMORY_FILE}"

# The lab machines don't support free -h
if free -h &>/dev/null; then
    echo >> "${MEMORY_FILE}"
    free -h >> "${MEMORY_FILE}"
fi

java -version &> "${OUTDIR}/java_${ME}.txt"
w > "${OUTDIR}/w_${ME}.txt"
ps -elf > "${OUTDIR}/ps_${ME}.txt"
