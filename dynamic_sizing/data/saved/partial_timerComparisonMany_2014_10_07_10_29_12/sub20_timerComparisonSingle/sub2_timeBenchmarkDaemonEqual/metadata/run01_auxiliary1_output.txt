economemd
    total memory: 1104 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub20_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
ReadingThread: starting
TimerThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 19947: heap size 9 MB, throughput 0.992227
Clients: 1
Reading from 19945: heap size 9 MB, throughput 0.992168
Clients: 2
Client 19945 has a minimum heap size of 276 MB
Client 19947 has a minimum heap size of 276 MB
Reading from 19945: heap size 9 MB, throughput 0.982067
Reading from 19947: heap size 9 MB, throughput 0.979946
Reading from 19945: heap size 9 MB, throughput 0.973641
Reading from 19947: heap size 9 MB, throughput 0.969336
Reading from 19947: heap size 9 MB, throughput 0.966088
Reading from 19945: heap size 9 MB, throughput 0.955964
Reading from 19947: heap size 11 MB, throughput 0.974384
Reading from 19945: heap size 11 MB, throughput 0.974161
Reading from 19945: heap size 11 MB, throughput 0.968609
Reading from 19947: heap size 11 MB, throughput 0.978343
Reading from 19945: heap size 17 MB, throughput 0.94758
Reading from 19947: heap size 17 MB, throughput 0.918769
Reading from 19945: heap size 17 MB, throughput 0.474203
Reading from 19947: heap size 17 MB, throughput 0.56615
Reading from 19945: heap size 30 MB, throughput 0.949796
Reading from 19947: heap size 30 MB, throughput 0.84477
Reading from 19945: heap size 31 MB, throughput 0.934558
Reading from 19947: heap size 31 MB, throughput 0.949865
Reading from 19945: heap size 34 MB, throughput 0.544334
Reading from 19945: heap size 46 MB, throughput 0.847248
Reading from 19947: heap size 35 MB, throughput 0.237208
Reading from 19947: heap size 48 MB, throughput 0.76999
Reading from 19945: heap size 49 MB, throughput 0.810926
Reading from 19947: heap size 50 MB, throughput 0.743634
Reading from 19945: heap size 50 MB, throughput 0.26626
Reading from 19945: heap size 70 MB, throughput 0.723328
Reading from 19947: heap size 52 MB, throughput 0.282432
Reading from 19945: heap size 71 MB, throughput 0.820543
Reading from 19947: heap size 75 MB, throughput 0.838706
Reading from 19945: heap size 75 MB, throughput 0.258864
Reading from 19947: heap size 75 MB, throughput 0.254814
Reading from 19945: heap size 97 MB, throughput 0.776807
Reading from 19945: heap size 100 MB, throughput 0.757073
Reading from 19947: heap size 103 MB, throughput 0.850124
Reading from 19945: heap size 103 MB, throughput 0.701114
Reading from 19947: heap size 103 MB, throughput 0.734887
Reading from 19945: heap size 109 MB, throughput 0.741655
Reading from 19947: heap size 104 MB, throughput 0.772601
Reading from 19945: heap size 111 MB, throughput 0.726035
Reading from 19947: heap size 108 MB, throughput 0.230339
Reading from 19947: heap size 134 MB, throughput 0.771702
Reading from 19945: heap size 116 MB, throughput 0.111375
Reading from 19947: heap size 138 MB, throughput 0.818577
Reading from 19947: heap size 140 MB, throughput 0.682949
Reading from 19945: heap size 152 MB, throughput 0.663304
Reading from 19945: heap size 160 MB, throughput 0.667983
Reading from 19945: heap size 161 MB, throughput 0.673916
Reading from 19945: heap size 165 MB, throughput 0.585358
Reading from 19947: heap size 142 MB, throughput 0.152085
Reading from 19945: heap size 171 MB, throughput 0.599528
Reading from 19947: heap size 183 MB, throughput 0.701529
Reading from 19947: heap size 184 MB, throughput 0.742517
Reading from 19947: heap size 186 MB, throughput 0.584216
Reading from 19945: heap size 177 MB, throughput 0.147122
Reading from 19947: heap size 192 MB, throughput 0.68888
Reading from 19945: heap size 213 MB, throughput 0.578823
Reading from 19947: heap size 195 MB, throughput 0.906203
Reading from 19945: heap size 223 MB, throughput 0.494572
Reading from 19947: heap size 202 MB, throughput 0.30696
Reading from 19945: heap size 260 MB, throughput 0.744009
Reading from 19947: heap size 252 MB, throughput 0.7114
Reading from 19945: heap size 263 MB, throughput 0.798776
Reading from 19947: heap size 254 MB, throughput 0.718146
Reading from 19945: heap size 266 MB, throughput 0.829561
Reading from 19947: heap size 258 MB, throughput 0.607017
Reading from 19945: heap size 268 MB, throughput 0.770773
Reading from 19947: heap size 260 MB, throughput 0.718869
Reading from 19945: heap size 268 MB, throughput 0.876185
Reading from 19947: heap size 265 MB, throughput 0.871219
Reading from 19945: heap size 270 MB, throughput 0.883536
Reading from 19947: heap size 266 MB, throughput 0.846522
Reading from 19945: heap size 271 MB, throughput 0.840167
Reading from 19947: heap size 269 MB, throughput 0.778127
Reading from 19945: heap size 272 MB, throughput 0.777729
Reading from 19947: heap size 272 MB, throughput 0.729164
Reading from 19945: heap size 275 MB, throughput 0.771692
Reading from 19947: heap size 278 MB, throughput 0.76066
Reading from 19945: heap size 281 MB, throughput 0.780981
Reading from 19947: heap size 281 MB, throughput 0.759904
Reading from 19945: heap size 283 MB, throughput 0.807721
Reading from 19947: heap size 287 MB, throughput 0.760794
Reading from 19945: heap size 281 MB, throughput 0.946556
Reading from 19947: heap size 288 MB, throughput 0.933249
Reading from 19945: heap size 284 MB, throughput 0.787134
Reading from 19947: heap size 286 MB, throughput 0.7233
Reading from 19945: heap size 281 MB, throughput 0.806965
Reading from 19945: heap size 284 MB, throughput 0.10168
Reading from 19947: heap size 289 MB, throughput 0.0765014
Reading from 19945: heap size 330 MB, throughput 0.746086
Reading from 19947: heap size 334 MB, throughput 0.528055
Reading from 19947: heap size 335 MB, throughput 0.797566
Reading from 19945: heap size 331 MB, throughput 0.834084
Reading from 19945: heap size 335 MB, throughput 0.738929
Reading from 19947: heap size 338 MB, throughput 0.764226
Reading from 19945: heap size 336 MB, throughput 0.583576
Reading from 19945: heap size 342 MB, throughput 0.682086
Reading from 19947: heap size 339 MB, throughput 0.836501
Reading from 19945: heap size 343 MB, throughput 0.691257
Reading from 19947: heap size 340 MB, throughput 0.787797
Reading from 19945: heap size 351 MB, throughput 0.728333
Equal recommendation: 552 MB each
Reading from 19947: heap size 342 MB, throughput 0.717374
Reading from 19945: heap size 352 MB, throughput 0.774864
Reading from 19947: heap size 349 MB, throughput 0.755032
Reading from 19947: heap size 349 MB, throughput 0.618163
Reading from 19947: heap size 356 MB, throughput 0.559126
Reading from 19947: heap size 356 MB, throughput 0.905345
Reading from 19945: heap size 360 MB, throughput 0.968684
Reading from 19947: heap size 362 MB, throughput 0.969343
Reading from 19945: heap size 361 MB, throughput 0.980596
Reading from 19947: heap size 364 MB, throughput 0.975982
Reading from 19945: heap size 363 MB, throughput 0.963607
Reading from 19947: heap size 363 MB, throughput 0.97991
Reading from 19945: heap size 366 MB, throughput 0.972276
Reading from 19947: heap size 367 MB, throughput 0.98243
Reading from 19945: heap size 370 MB, throughput 0.979618
Reading from 19947: heap size 370 MB, throughput 0.978337
Reading from 19945: heap size 370 MB, throughput 0.977294
Equal recommendation: 552 MB each
Reading from 19947: heap size 372 MB, throughput 0.975439
Reading from 19945: heap size 371 MB, throughput 0.975857
Reading from 19947: heap size 369 MB, throughput 0.976173
Reading from 19945: heap size 373 MB, throughput 0.979516
Reading from 19947: heap size 372 MB, throughput 0.972983
Reading from 19945: heap size 370 MB, throughput 0.970536
Reading from 19947: heap size 367 MB, throughput 0.969815
Reading from 19945: heap size 373 MB, throughput 0.976575
Reading from 19947: heap size 370 MB, throughput 0.959286
Reading from 19945: heap size 373 MB, throughput 0.973899
Reading from 19947: heap size 371 MB, throughput 0.978253
Reading from 19945: heap size 374 MB, throughput 0.974057
Reading from 19947: heap size 371 MB, throughput 0.974549
Equal recommendation: 552 MB each
Reading from 19945: heap size 376 MB, throughput 0.970538
Reading from 19947: heap size 373 MB, throughput 0.97336
Reading from 19945: heap size 377 MB, throughput 0.981835
Reading from 19945: heap size 377 MB, throughput 0.820012
Reading from 19947: heap size 374 MB, throughput 0.976313
Reading from 19947: heap size 374 MB, throughput 0.801306
Reading from 19947: heap size 376 MB, throughput 0.694006
Reading from 19947: heap size 382 MB, throughput 0.792852
Reading from 19947: heap size 385 MB, throughput 0.735745
Reading from 19945: heap size 379 MB, throughput 0.1336
Reading from 19945: heap size 426 MB, throughput 0.801649
Reading from 19945: heap size 429 MB, throughput 0.883764
Reading from 19945: heap size 437 MB, throughput 0.967503
Reading from 19947: heap size 395 MB, throughput 0.686061
Reading from 19947: heap size 424 MB, throughput 0.993198
Reading from 19945: heap size 439 MB, throughput 0.990222
Reading from 19945: heap size 442 MB, throughput 0.989404
Reading from 19947: heap size 429 MB, throughput 0.991157
Reading from 19947: heap size 433 MB, throughput 0.988821
Reading from 19945: heap size 444 MB, throughput 0.987903
Equal recommendation: 552 MB each
Reading from 19947: heap size 439 MB, throughput 0.990432
Reading from 19945: heap size 441 MB, throughput 0.986379
Reading from 19947: heap size 440 MB, throughput 0.98673
Reading from 19945: heap size 444 MB, throughput 0.984405
Reading from 19947: heap size 438 MB, throughput 0.987025
Reading from 19945: heap size 441 MB, throughput 0.984712
Reading from 19947: heap size 441 MB, throughput 0.985883
Reading from 19945: heap size 443 MB, throughput 0.982864
Reading from 19947: heap size 435 MB, throughput 0.989887
Reading from 19945: heap size 445 MB, throughput 0.981603
Equal recommendation: 552 MB each
Reading from 19947: heap size 439 MB, throughput 0.979376
Reading from 19945: heap size 447 MB, throughput 0.9737
Reading from 19947: heap size 439 MB, throughput 0.988621
Reading from 19947: heap size 439 MB, throughput 0.961029
Reading from 19947: heap size 436 MB, throughput 0.850069
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Reading from 19947: heap size 439 MB, throughput 0.00478011
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Reading from 19945: heap size 448 MB, throughput 0.103595
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Reading from 19945: heap size 452 MB, throughput 0.940451
Reading from 19945: heap size 466 MB, throughput 0.369268
Reading from 19945: heap size 468 MB, throughput 0.927554
Reading from 19945: heap size 488 MB, throughput 0.985932
Equal recommendation: 552 MB each
Reading from 19945: heap size 489 MB, throughput 0.97515
Reading from 19945: heap size 498 MB, throughput 0.909149
Reading from 19945: heap size 501 MB, throughput 0.978434
Reading from 19945: heap size 505 MB, throughput 0.975535
Equal recommendation: 552 MB each
Reading from 19945: heap size 510 MB, throughput 0.9346
Reading from 19947: heap size 446 MB, throughput 0.93587
Reading from 19945: heap size 515 MB, throughput 0.990938
Reading from 19947: heap size 450 MB, throughput 0.949551
Reading from 19945: heap size 517 MB, throughput 0.988393
Equal recommendation: 552 MB each
Reading from 19945: heap size 522 MB, throughput 0.97716
Reading from 19945: heap size 524 MB, throughput 0.887415
Reading from 19945: heap size 532 MB, throughput 0.979607
Reading from 19947: heap size 454 MB, throughput 0.889457
Reading from 19947: heap size 459 MB, throughput 0.920593
Reading from 19945: heap size 533 MB, throughput 0.991963
Reading from 19947: heap size 463 MB, throughput 0.903777
Reading from 19945: heap size 538 MB, throughput 0.994096
Reading from 19947: heap size 466 MB, throughput 0.978053
Equal recommendation: 552 MB each
Reading from 19945: heap size 540 MB, throughput 0.988293
Reading from 19947: heap size 472 MB, throughput 0.955174
Reading from 19945: heap size 537 MB, throughput 0.990212
Reading from 19947: heap size 475 MB, throughput 0.983808
Reading from 19945: heap size 540 MB, throughput 0.989693
Reading from 19947: heap size 482 MB, throughput 0.989836
Equal recommendation: 552 MB each
Reading from 19947: heap size 484 MB, throughput 0.984335
Reading from 19945: heap size 541 MB, throughput 0.985026
Reading from 19947: heap size 490 MB, throughput 0.818307
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Reading from 19947: heap size 491 MB, throughput 0.0072823
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Reading from 19945: heap size 542 MB, throughput 0.914546
Reading from 19945: heap size 541 MB, throughput 0.880092
Equal recommendation: 552 MB each
Reading from 19945: heap size 527 MB, throughput 0.973381
Reading from 19947: heap size 503 MB, throughput 0.978807
Reading from 19945: heap size 542 MB, throughput 0.991594
Reading from 19947: heap size 503 MB, throughput 0.912672
Reading from 19945: heap size 508 MB, throughput 0.989732
Reading from 19947: heap size 521 MB, throughput 0.946038
Equal recommendation: 552 MB each
Reading from 19945: heap size 543 MB, throughput 0.992536
Reading from 19947: heap size 523 MB, throughput 0.993429
Reading from 19945: heap size 516 MB, throughput 0.99099
Reading from 19947: heap size 531 MB, throughput 0.934447
Reading from 19945: heap size 542 MB, throughput 0.987709
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Reading from 19947: heap size 534 MB, throughput 0.0981994
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Reading from 19945: heap size 541 MB, throughput 0.966281
Reading from 19945: heap size 544 MB, throughput 0.866319
Reading from 19945: heap size 544 MB, throughput 0.529748
Equal recommendation: 552 MB each
Reading from 19947: heap size 539 MB, throughput 0.991909
Reading from 19945: heap size 544 MB, throughput 0.992349
Reading from 19947: heap size 539 MB, throughput 0.902019
Reading from 19947: heap size 536 MB, throughput 0.964036
Reading from 19945: heap size 545 MB, throughput 0.994227
Reading from 19947: heap size 538 MB, throughput 0.983262
Reading from 19945: heap size 543 MB, throughput 0.991083
Equal recommendation: 552 MB each
Reading from 19947: heap size 537 MB, throughput 0.991632
Reading from 19945: heap size 544 MB, throughput 0.988806
Reading from 19947: heap size 537 MB, throughput 0.993684
Reading from 19945: heap size 542 MB, throughput 0.988889
Reading from 19947: heap size 535 MB, throughput 0.96968
Reading from 19945: heap size 544 MB, throughput 0.989371
Equal recommendation: 552 MB each
Reading from 19945: heap size 549 MB, throughput 0.992547
Equal recommendation: 552 MB each
Reading from 19945: heap size 549 MB, throughput 0.0164396
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Reading from 19947: heap size 536 MB, throughput 0.921227
Equal recommendation: 552 MB each
Reading from 19945: heap size 546 MB, throughput 0.935477
Reading from 19945: heap size 548 MB, throughput 0.948513
Reading from 19947: heap size 541 MB, throughput 0.946207
Reading from 19947: heap size 533 MB, throughput 0.798629
Equal recommendation: 552 MB each
Reading from 19947: heap size 536 MB, throughput 0.915258
Reading from 19945: heap size 547 MB, throughput 0.965817
Reading from 19947: heap size 529 MB, throughput 0.974074
Reading from 19945: heap size 547 MB, throughput 0.990358
Reading from 19947: heap size 537 MB, throughput 0.99482
Reading from 19945: heap size 545 MB, throughput 0.977938
Equal recommendation: 552 MB each
Reading from 19947: heap size 510 MB, throughput 0.986151
Reading from 19945: heap size 546 MB, throughput 0.962829
Reading from 19947: heap size 537 MB, throughput 0.989313
Reading from 19945: heap size 551 MB, throughput 0.981885
Reading from 19945: heap size 552 MB, throughput 0.931882
Reading from 19945: heap size 547 MB, throughput 0.57996
Reading from 19947: heap size 518 MB, throughput 0.969166
Equal recommendation: 552 MB each
Reading from 19945: heap size 542 MB, throughput 0.988979
Reading from 19947: heap size 535 MB, throughput 0.987685
Reading from 19947: heap size 535 MB, throughput 0.989663
Reading from 19945: heap size 548 MB, throughput 0.990031
Reading from 19947: heap size 536 MB, throughput 0.89839
Reading from 19947: heap size 528 MB, throughput 0.987522
Reading from 19945: heap size 522 MB, throughput 0.986427
Reading from 19947: heap size 536 MB, throughput 0.992679
Equal recommendation: 552 MB each
Reading from 19945: heap size 548 MB, throughput 0.993154
Reading from 19947: heap size 536 MB, throughput 0.99397
Reading from 19945: heap size 547 MB, throughput 0.990693
Reading from 19947: heap size 534 MB, throughput 0.990354
Reading from 19945: heap size 547 MB, throughput 0.982091
Equal recommendation: 552 MB each
Reading from 19945: heap size 547 MB, throughput 0.987907
Reading from 19945: heap size 547 MB, throughput 0.917248
Reading from 19947: heap size 535 MB, throughput 0.989949
Reading from 19945: heap size 540 MB, throughput 0.983456
Reading from 19947: heap size 535 MB, throughput 0.989092
Reading from 19945: heap size 546 MB, throughput 0.993041
Reading from 19947: heap size 536 MB, throughput 0.993051
Reading from 19947: heap size 539 MB, throughput 0.924866
Reading from 19947: heap size 532 MB, throughput 0.973335
Reading from 19945: heap size 547 MB, throughput 0.989041
Equal recommendation: 552 MB each
Reading from 19947: heap size 537 MB, throughput 0.993593
Reading from 19945: heap size 544 MB, throughput 0.990848
Reading from 19945: heap size 545 MB, throughput 0.988229
Reading from 19947: heap size 510 MB, throughput 0.993459
Reading from 19945: heap size 545 MB, throughput 0.991658
Reading from 19947: heap size 538 MB, throughput 0.993496
Equal recommendation: 552 MB each
Reading from 19945: heap size 547 MB, throughput 0.99404
Reading from 19945: heap size 550 MB, throughput 0.762055
Reading from 19945: heap size 544 MB, throughput 0.955966
Reading from 19947: heap size 518 MB, throughput 0.993891
Reading from 19945: heap size 549 MB, throughput 0.992202
Reading from 19947: heap size 537 MB, throughput 0.989804
Reading from 19945: heap size 521 MB, throughput 0.993058
Equal recommendation: 552 MB each
Reading from 19947: heap size 537 MB, throughput 0.993491
Reading from 19947: heap size 536 MB, throughput 0.830509
Reading from 19947: heap size 538 MB, throughput 0.917125
Reading from 19945: heap size 549 MB, throughput 0.990427
Reading from 19947: heap size 537 MB, throughput 0.992796
Reading from 19945: heap size 529 MB, throughput 0.991095
Reading from 19947: heap size 539 MB, throughput 0.992861
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Reading from 19945: heap size 548 MB, throughput 0.144854
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Reading from 19945: heap size 555 MB, throughput 0.965462
Reading from 19945: heap size 539 MB, throughput 0.835336
Reading from 19945: heap size 550 MB, throughput 0.209125
Reading from 19945: heap size 557 MB, throughput 0.968571
Equal recommendation: 552 MB each
Reading from 19945: heap size 522 MB, throughput 0.950094
Reading from 19947: heap size 538 MB, throughput 0.944065
Reading from 19945: heap size 553 MB, throughput 0.993745
Reading from 19945: heap size 524 MB, throughput 0.967274
Reading from 19947: heap size 538 MB, throughput 0.961147
Equal recommendation: 552 MB each
Reading from 19945: heap size 546 MB, throughput 0.984124
Reading from 19947: heap size 537 MB, throughput 0.96324
Reading from 19945: heap size 545 MB, throughput 0.991032
Reading from 19945: heap size 557 MB, throughput 0.990828
Reading from 19945: heap size 534 MB, throughput 0.922685
Reading from 19945: heap size 548 MB, throughput 0.962921
Reading from 19947: heap size 538 MB, throughput 0.971493
Equal recommendation: 552 MB each
Reading from 19945: heap size 551 MB, throughput 0.975233
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Reading from 19947: heap size 532 MB, throughput 0.0439851
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Equal recommendation: 552 MB each
Reading from 19945: heap size 556 MB, throughput 0.906999
Reading from 19947: heap size 563 MB, throughput 0.995905
Reading from 19947: heap size 539 MB, throughput 0.986427
Reading from 19945: heap size 531 MB, throughput 0.960342
Reading from 19947: heap size 489 MB, throughput 0.922304
Equal recommendation: 552 MB each
Reading from 19945: heap size 549 MB, throughput 0.942863
Reading from 19947: heap size 549 MB, throughput 0.987705
Reading from 19945: heap size 549 MB, throughput 0.9458
Reading from 19947: heap size 497 MB, throughput 0.854824
Reading from 19947: heap size 551 MB, throughput 0.930258
Reading from 19945: heap size 559 MB, throughput 0.851331
Equal recommendation: 552 MB each
Reading from 19945: heap size 542 MB, throughput 0.901909
Reading from 19945: heap size 551 MB, throughput 0.812566
Reading from 19947: heap size 548 MB, throughput 0.927601
Reading from 19945: heap size 554 MB, throughput 0.986089
Reading from 19947: heap size 549 MB, throughput 0.944104
Reading from 19945: heap size 545 MB, throughput 0.988767
Reading from 19947: heap size 548 MB, throughput 0.957278
Reading from 19947: heap size 545 MB, throughput 0.811137
Reading from 19947: heap size 533 MB, throughput 0.937868
Equal recommendation: 552 MB each
Reading from 19945: heap size 550 MB, throughput 0.989438
Reading from 19947: heap size 549 MB, throughput 0.991131
Reading from 19945: heap size 552 MB, throughput 0.986075
Reading from 19947: heap size 511 MB, throughput 0.991557
Reading from 19945: heap size 530 MB, throughput 0.97298
Reading from 19947: heap size 545 MB, throughput 0.989625
Equal recommendation: 552 MB each
Reading from 19945: heap size 547 MB, throughput 0.989024
Reading from 19947: heap size 543 MB, throughput 0.988724
Reading from 19945: heap size 546 MB, throughput 0.98433
Reading from 19945: heap size 551 MB, throughput 0.755974
Client 19945 died
Clients: 1
Reading from 19947: heap size 548 MB, throughput 0.979176
Reading from 19947: heap size 549 MB, throughput 0.985726
Recommendation: one client; give it all the memory
Reading from 19947: heap size 550 MB, throughput 0.979706
Reading from 19947: heap size 552 MB, throughput 0.908242
Reading from 19947: heap size 550 MB, throughput 0.891279
Reading from 19947: heap size 552 MB, throughput 0.979428
Reading from 19947: heap size 549 MB, throughput 0.984307
Recommendation: one client; give it all the memory
Reading from 19947: heap size 551 MB, throughput 0.98944
Reading from 19947: heap size 549 MB, throughput 0.98999
Reading from 19947: heap size 552 MB, throughput 0.99231
Recommendation: one client; give it all the memory
Reading from 19947: heap size 551 MB, throughput 0.974236
Reading from 19947: heap size 553 MB, throughput 0.993619
Reading from 19947: heap size 556 MB, throughput 0.920942
Reading from 19947: heap size 557 MB, throughput 0.933305
Reading from 19947: heap size 560 MB, throughput 0.995491
Recommendation: one client; give it all the memory
Reading from 19947: heap size 528 MB, throughput 0.994205
Reading from 19947: heap size 561 MB, throughput 0.986283
Reading from 19947: heap size 536 MB, throughput 0.988614
Recommendation: one client; give it all the memory
Reading from 19947: heap size 560 MB, throughput 0.993291
Reading from 19947: heap size 559 MB, throughput 0.978573
Reading from 19947: heap size 558 MB, throughput 0.984997
Reading from 19947: heap size 551 MB, throughput 0.903029
Client 19947 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
