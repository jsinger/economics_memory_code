#!/bin/bash
# Measure isoquant points
#
# Usage: isoquantLocal.sh measurepoints total_mem [runs] [iterations]
#

function usage()
{
    echo "Usage: $(basename "${0}") measurepoints total_mem [runs] [iterations]"
    exit 1
}

test -z "${1}" && usage
test ! -f "${1}" && usage
test -z "${2}" && usage

MEASUREPOINTS="$(readlink -f "${1}")"
TOTAL="${2}"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

"${DIR}/../checkDacapo.sh" || exit 1

B1='pmd'
B2='xalan'

if [ -z "${3}" ]; then
    RUNS='5'
else
    RUNS="${3}"
fi

if [ -z "${4}" ]; then
    ITERATIONS='10'
else
    ITERATIONS="${4}"
fi

MAXHEAP1="$(cut '--delimiter= ' -f 1 "${MEASUREPOINTS}" | sort -nr | head -n 1)"
MAXHEAP2="$(cut '--delimiter= ' -f 2 "${MEASUREPOINTS}" | sort -nr | head -n 1)"
ME="$(whoami)@$(hostname)"

OUTDIR="${DIR}/isoquant_${ME}_$(date '+%Y_%m_%d_%H_%M_%S')"

BENCHMARKS="${OUTDIR}/benchmarks.txt"

mkdir -p "${OUTDIR}"

echo "${B1}" >> "${BENCHMARKS}"
echo "${B2}" >> "${BENCHMARKS}"
echo "${ITERATIONS}" > "${OUTDIR}/iterations.txt"
echo "${RUNS}" > "${OUTDIR}/runs.txt"
echo "${TOTAL}" > "${OUTDIR}/maxMemory.txt"

cp "${MEASUREPOINTS}" "${OUTDIR}/input.txt"

AVG_TIMES_FILE="${OUTDIR}/avgTimes_${ME}.txt"
TIMESTAMPS_FILE="${OUTDIR}/timestamps.txt"

"${DIR}/../machineInfo.sh" "${OUTDIR}"

echo "Local run on ${ME}" >> "${TIMESTAMPS_FILE}"
echo "Started: $(date '+%Y %m %d, %H:%M:%S')" >> "${TIMESTAMPS_FILE}"
STARTTIME="$(date '+%s')"

while read HEAP1 HEAP2; do
    "${DIR}/isoquant_point.sh" "${B1}" "${B2}" "${HEAP1}" "${HEAP2}" "${MAXHEAP1}" "${MAXHEAP2}" "${RUNS}" "${ITERATIONS}" "${OUTDIR}" "${AVG_TIMES_FILE}"
done < "${MEASUREPOINTS}"

"${DIR}/isoquant_postprocess.sh" "${OUTDIR}"

echo "Completed: $(date '+%Y %m %d, %H:%M:%S')" >> "${TIMESTAMPS_FILE}"
ENDTIME="$(date '+%s')"

RUNTIME=$(( ENDTIME - STARTTIME ))
echo "Runtime: ${RUNTIME} s" >> "${TIMESTAMPS_FILE}"
