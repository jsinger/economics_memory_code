economemd
    total memory: 2760 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub23_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 30689: heap size 9 MB, throughput 0.98746
Clients: 1
Reading from 30690: heap size 9 MB, throughput 0.992651
Clients: 2
Client 30689 has a minimum heap size of 276 MB
Client 30690 has a minimum heap size of 276 MB
Reading from 30689: heap size 9 MB, throughput 0.984823
Reading from 30690: heap size 9 MB, throughput 0.988261
Reading from 30690: heap size 9 MB, throughput 0.981204
Reading from 30689: heap size 11 MB, throughput 0.98073
Reading from 30690: heap size 9 MB, throughput 0.974216
Reading from 30689: heap size 11 MB, throughput 0.981364
Reading from 30690: heap size 11 MB, throughput 0.968921
Reading from 30690: heap size 11 MB, throughput 0.979309
Reading from 30689: heap size 15 MB, throughput 0.890932
Reading from 30690: heap size 17 MB, throughput 0.969566
Reading from 30689: heap size 19 MB, throughput 0.914439
Reading from 30690: heap size 17 MB, throughput 0.908507
Reading from 30689: heap size 23 MB, throughput 0.81234
Reading from 30690: heap size 30 MB, throughput 0.781133
Reading from 30689: heap size 27 MB, throughput 0.720331
Reading from 30690: heap size 31 MB, throughput 0.45251
Reading from 30689: heap size 37 MB, throughput 0.818655
Reading from 30689: heap size 40 MB, throughput 0.844408
Reading from 30689: heap size 40 MB, throughput 0.709793
Reading from 30689: heap size 41 MB, throughput 0.516665
Reading from 30689: heap size 43 MB, throughput 0.229363
Reading from 30690: heap size 34 MB, throughput 0.720514
Reading from 30690: heap size 47 MB, throughput 0.672843
Reading from 30689: heap size 45 MB, throughput 0.596846
Reading from 30690: heap size 51 MB, throughput 0.375342
Reading from 30689: heap size 63 MB, throughput 0.190459
Reading from 30689: heap size 65 MB, throughput 0.692306
Reading from 30689: heap size 67 MB, throughput 0.706805
Reading from 30689: heap size 70 MB, throughput 0.768572
Reading from 30690: heap size 51 MB, throughput 0.496283
Reading from 30690: heap size 72 MB, throughput 0.334167
Reading from 30690: heap size 73 MB, throughput 0.782108
Reading from 30689: heap size 75 MB, throughput 0.488406
Reading from 30690: heap size 79 MB, throughput 0.818859
Reading from 30689: heap size 96 MB, throughput 0.670988
Reading from 30689: heap size 98 MB, throughput 0.695155
Reading from 30689: heap size 100 MB, throughput 0.750689
Reading from 30690: heap size 81 MB, throughput 0.483891
Reading from 30689: heap size 104 MB, throughput 0.480728
Reading from 30690: heap size 115 MB, throughput 0.215477
Reading from 30689: heap size 129 MB, throughput 0.0194646
Reading from 30690: heap size 116 MB, throughput 0.791011
Reading from 30689: heap size 134 MB, throughput 0.761984
Reading from 30690: heap size 119 MB, throughput 0.794467
Reading from 30689: heap size 136 MB, throughput 0.742465
Reading from 30690: heap size 123 MB, throughput 0.745554
Reading from 30689: heap size 141 MB, throughput 0.75878
Reading from 30690: heap size 127 MB, throughput 0.741259
Reading from 30689: heap size 144 MB, throughput 0.7125
Reading from 30690: heap size 131 MB, throughput 0.458259
Reading from 30689: heap size 146 MB, throughput 0.428124
Reading from 30690: heap size 173 MB, throughput 0.71681
Reading from 30689: heap size 184 MB, throughput 0.68523
Reading from 30690: heap size 176 MB, throughput 0.736542
Reading from 30689: heap size 191 MB, throughput 0.734659
Reading from 30690: heap size 178 MB, throughput 0.731788
Reading from 30689: heap size 193 MB, throughput 0.721678
Reading from 30690: heap size 182 MB, throughput 0.664006
Reading from 30689: heap size 196 MB, throughput 0.807353
Reading from 30690: heap size 185 MB, throughput 0.653142
Reading from 30689: heap size 202 MB, throughput 0.882154
Reading from 30690: heap size 194 MB, throughput 0.538494
Reading from 30689: heap size 202 MB, throughput 0.551392
Reading from 30689: heap size 251 MB, throughput 0.81151
Reading from 30689: heap size 253 MB, throughput 0.69931
Reading from 30690: heap size 231 MB, throughput 0.847408
Reading from 30689: heap size 255 MB, throughput 0.696714
Reading from 30690: heap size 239 MB, throughput 0.793584
Reading from 30689: heap size 253 MB, throughput 0.627981
Reading from 30690: heap size 244 MB, throughput 0.719038
Reading from 30689: heap size 256 MB, throughput 0.648079
Reading from 30690: heap size 248 MB, throughput 0.720355
Reading from 30690: heap size 254 MB, throughput 0.62246
Reading from 30689: heap size 255 MB, throughput 0.394413
Reading from 30690: heap size 255 MB, throughput 0.478914
Reading from 30689: heap size 301 MB, throughput 0.730318
Reading from 30690: heap size 290 MB, throughput 0.688874
Reading from 30689: heap size 304 MB, throughput 0.847211
Reading from 30690: heap size 293 MB, throughput 0.798317
Reading from 30689: heap size 305 MB, throughput 0.866543
Reading from 30689: heap size 303 MB, throughput 0.882837
Reading from 30690: heap size 294 MB, throughput 0.837809
Reading from 30689: heap size 306 MB, throughput 0.886972
Reading from 30690: heap size 296 MB, throughput 0.831179
Reading from 30690: heap size 292 MB, throughput 0.810666
Reading from 30689: heap size 304 MB, throughput 0.869163
Reading from 30690: heap size 295 MB, throughput 0.823222
Reading from 30689: heap size 306 MB, throughput 0.846569
Reading from 30690: heap size 292 MB, throughput 0.839276
Reading from 30689: heap size 303 MB, throughput 0.949594
Reading from 30689: heap size 305 MB, throughput 0.94024
Reading from 30690: heap size 294 MB, throughput 0.942751
Reading from 30689: heap size 298 MB, throughput 0.916056
Reading from 30690: heap size 290 MB, throughput 0.904503
Reading from 30689: heap size 302 MB, throughput 0.895095
Reading from 30690: heap size 293 MB, throughput 0.88671
Reading from 30689: heap size 297 MB, throughput 0.880973
Reading from 30690: heap size 293 MB, throughput 0.877251
Reading from 30689: heap size 300 MB, throughput 0.872391
Reading from 30690: heap size 295 MB, throughput 0.844862
Reading from 30689: heap size 296 MB, throughput 0.412585
Reading from 30689: heap size 298 MB, throughput 0.6154
Reading from 30690: heap size 295 MB, throughput 0.905688
Reading from 30689: heap size 301 MB, throughput 0.444828
Reading from 30690: heap size 297 MB, throughput 0.903794
Reading from 30689: heap size 301 MB, throughput 0.313277
Reading from 30690: heap size 296 MB, throughput 0.840003
Reading from 30689: heap size 305 MB, throughput 0.627737
Reading from 30689: heap size 308 MB, throughput 0.609453
Reading from 30689: heap size 315 MB, throughput 0.478154
Reading from 30689: heap size 318 MB, throughput 0.473157
Reading from 30689: heap size 326 MB, throughput 0.545133
Reading from 30690: heap size 298 MB, throughput 0.709317
Numeric result:
Recommendation: 2 clients, utility 0.509409:
    h1: 1378.25 MB (U(h) = 0.73617*h^0.001)
    h2: 1381.75 MB (U(h) = 0.682038*h^0.001)
Recommendation: 2 clients, utility 0.509409:
    h1: 1380 MB (U(h) = 0.73617*h^0.001)
    h2: 1380 MB (U(h) = 0.682038*h^0.001)
Reading from 30690: heap size 343 MB, throughput 0.64603
Reading from 30690: heap size 343 MB, throughput 0.640028
Reading from 30690: heap size 346 MB, throughput 0.671688
Reading from 30689: heap size 328 MB, throughput 0.926736
Reading from 30690: heap size 347 MB, throughput 0.930274
Reading from 30689: heap size 335 MB, throughput 0.961941
Reading from 30690: heap size 351 MB, throughput 0.967844
Reading from 30689: heap size 336 MB, throughput 0.963705
Reading from 30690: heap size 353 MB, throughput 0.965025
Reading from 30689: heap size 336 MB, throughput 0.965427
Reading from 30689: heap size 339 MB, throughput 0.971972
Reading from 30690: heap size 359 MB, throughput 0.968988
Reading from 30689: heap size 337 MB, throughput 0.975644
Reading from 30690: heap size 360 MB, throughput 0.967553
Reading from 30689: heap size 340 MB, throughput 0.976007
Reading from 30690: heap size 362 MB, throughput 0.968101
Reading from 30689: heap size 340 MB, throughput 0.347178
Numeric result:
Recommendation: 2 clients, utility 0.519736:
    h1: 364.158 MB (U(h) = 0.622008*h^0.00647682)
    h2: 2395.84 MB (U(h) = 0.577354*h^0.042597)
Recommendation: 2 clients, utility 0.519736:
    h1: 364.268 MB (U(h) = 0.622008*h^0.00647682)
    h2: 2395.73 MB (U(h) = 0.577354*h^0.042597)
Reading from 30690: heap size 364 MB, throughput 0.966519
Reading from 30689: heap size 341 MB, throughput 0.69039
Reading from 30690: heap size 368 MB, throughput 0.96631
Reading from 30689: heap size 344 MB, throughput 0.187478
Reading from 30690: heap size 370 MB, throughput 0.968822
Reading from 30689: heap size 344 MB, throughput 0.966183
Reading from 30690: heap size 374 MB, throughput 0.960186
Reading from 30689: heap size 348 MB, throughput 0.968676
Reading from 30690: heap size 376 MB, throughput 0.9685
Reading from 30689: heap size 348 MB, throughput 0.974291
Reading from 30690: heap size 375 MB, throughput 0.970122
Reading from 30689: heap size 351 MB, throughput 0.97238
Reading from 30690: heap size 377 MB, throughput 0.968942
Reading from 30689: heap size 352 MB, throughput 0.969257
Numeric result:
Recommendation: 2 clients, utility 0.611331:
    h1: 686.224 MB (U(h) = 0.579757*h^0.0246441)
    h2: 2073.78 MB (U(h) = 0.508259*h^0.0744832)
Recommendation: 2 clients, utility 0.611331:
    h1: 686.165 MB (U(h) = 0.579757*h^0.0246441)
    h2: 2073.84 MB (U(h) = 0.508259*h^0.0744832)
Reading from 30690: heap size 380 MB, throughput 0.97028
Reading from 30689: heap size 355 MB, throughput 0.972905
Reading from 30689: heap size 357 MB, throughput 0.970785
Reading from 30689: heap size 356 MB, throughput 0.956179
Reading from 30689: heap size 358 MB, throughput 0.92384
Reading from 30689: heap size 364 MB, throughput 0.862686
Reading from 30689: heap size 367 MB, throughput 0.80536
Reading from 30690: heap size 380 MB, throughput 0.98045
Reading from 30689: heap size 376 MB, throughput 0.942086
Reading from 30690: heap size 380 MB, throughput 0.940768
Reading from 30690: heap size 426 MB, throughput 0.967934
Reading from 30690: heap size 430 MB, throughput 0.971332
Reading from 30690: heap size 435 MB, throughput 0.975919
Reading from 30690: heap size 439 MB, throughput 0.985323
Reading from 30689: heap size 378 MB, throughput 0.96668
Reading from 30690: heap size 441 MB, throughput 0.993042
Reading from 30689: heap size 380 MB, throughput 0.979303
Reading from 30690: heap size 441 MB, throughput 0.994077
Reading from 30689: heap size 382 MB, throughput 0.980418
Reading from 30690: heap size 443 MB, throughput 0.993255
Reading from 30689: heap size 382 MB, throughput 0.983319
Numeric result:
Recommendation: 2 clients, utility 0.742539:
    h1: 1111.22 MB (U(h) = 0.493928*h^0.065457)
    h2: 1648.78 MB (U(h) = 0.462626*h^0.0971217)
Recommendation: 2 clients, utility 0.742539:
    h1: 1111.22 MB (U(h) = 0.493928*h^0.065457)
    h2: 1648.78 MB (U(h) = 0.462626*h^0.0971217)
Reading from 30690: heap size 437 MB, throughput 0.990743
Reading from 30689: heap size 384 MB, throughput 0.982368
Reading from 30690: heap size 394 MB, throughput 0.990853
Reading from 30689: heap size 381 MB, throughput 0.982275
Reading from 30690: heap size 431 MB, throughput 0.988113
Reading from 30689: heap size 384 MB, throughput 0.982007
Reading from 30690: heap size 402 MB, throughput 0.983773
Reading from 30689: heap size 380 MB, throughput 0.981259
Reading from 30690: heap size 432 MB, throughput 0.98404
Reading from 30689: heap size 383 MB, throughput 0.948041
Reading from 30690: heap size 433 MB, throughput 0.983583
Reading from 30689: heap size 423 MB, throughput 0.975734
Numeric result:
Recommendation: 2 clients, utility 0.809055:
    h1: 1151.51 MB (U(h) = 0.467377*h^0.0792306)
    h2: 1608.49 MB (U(h) = 0.437424*h^0.110672)
Recommendation: 2 clients, utility 0.809055:
    h1: 1151.52 MB (U(h) = 0.467377*h^0.0792306)
    h2: 1608.48 MB (U(h) = 0.437424*h^0.110672)
Reading from 30690: heap size 434 MB, throughput 0.981563
Reading from 30689: heap size 424 MB, throughput 0.979174
Reading from 30690: heap size 435 MB, throughput 0.979195
Reading from 30689: heap size 427 MB, throughput 0.981831
Reading from 30689: heap size 428 MB, throughput 0.971834
Reading from 30689: heap size 428 MB, throughput 0.955355
Reading from 30689: heap size 430 MB, throughput 0.933059
Reading from 30689: heap size 438 MB, throughput 0.964675
Reading from 30690: heap size 437 MB, throughput 0.984617
Reading from 30690: heap size 439 MB, throughput 0.971164
Reading from 30690: heap size 435 MB, throughput 0.954669
Reading from 30690: heap size 441 MB, throughput 0.923198
Reading from 30690: heap size 449 MB, throughput 0.890568
Reading from 30689: heap size 439 MB, throughput 0.982419
Reading from 30690: heap size 453 MB, throughput 0.965828
Reading from 30689: heap size 442 MB, throughput 0.989323
Reading from 30690: heap size 457 MB, throughput 0.981872
Reading from 30689: heap size 444 MB, throughput 0.988204
Reading from 30690: heap size 461 MB, throughput 0.986451
Numeric result:
Recommendation: 2 clients, utility 0.882051:
    h1: 1283.42 MB (U(h) = 0.425958*h^0.101684)
    h2: 1476.58 MB (U(h) = 0.425887*h^0.116987)
Recommendation: 2 clients, utility 0.882051:
    h1: 1283.43 MB (U(h) = 0.425958*h^0.101684)
    h2: 1476.57 MB (U(h) = 0.425887*h^0.116987)
Reading from 30689: heap size 442 MB, throughput 0.988402
Reading from 30690: heap size 459 MB, throughput 0.985559
Reading from 30689: heap size 445 MB, throughput 0.989252
Reading from 30690: heap size 463 MB, throughput 0.985722
Reading from 30690: heap size 459 MB, throughput 0.98493
Reading from 30689: heap size 442 MB, throughput 0.989306
Reading from 30690: heap size 463 MB, throughput 0.984799
Reading from 30689: heap size 444 MB, throughput 0.98672
Reading from 30690: heap size 461 MB, throughput 0.984694
Reading from 30689: heap size 446 MB, throughput 0.987101
Numeric result:
Recommendation: 2 clients, utility 0.9032:
    h1: 1298.11 MB (U(h) = 0.417422*h^0.106544)
    h2: 1461.89 MB (U(h) = 0.420462*h^0.119996)
Recommendation: 2 clients, utility 0.9032:
    h1: 1298.05 MB (U(h) = 0.417422*h^0.106544)
    h2: 1461.95 MB (U(h) = 0.420462*h^0.119996)
Reading from 30689: heap size 447 MB, throughput 0.982964
Reading from 30690: heap size 463 MB, throughput 0.984845
Reading from 30689: heap size 449 MB, throughput 0.986116
Reading from 30689: heap size 450 MB, throughput 0.976637
Reading from 30689: heap size 450 MB, throughput 0.96002
Reading from 30690: heap size 463 MB, throughput 0.984987
Reading from 30689: heap size 452 MB, throughput 0.940456
Reading from 30689: heap size 460 MB, throughput 0.978457
Reading from 30690: heap size 464 MB, throughput 0.988712
Reading from 30690: heap size 467 MB, throughput 0.983222
Reading from 30690: heap size 467 MB, throughput 0.96965
Reading from 30690: heap size 472 MB, throughput 0.953759
Reading from 30689: heap size 461 MB, throughput 0.987654
Reading from 30690: heap size 473 MB, throughput 0.980944
Reading from 30689: heap size 464 MB, throughput 0.988834
Reading from 30690: heap size 481 MB, throughput 0.989183
Numeric result:
Recommendation: 2 clients, utility 0.957548:
    h1: 1346.61 MB (U(h) = 0.394441*h^0.120056)
    h2: 1413.39 MB (U(h) = 0.40974*h^0.126017)
Recommendation: 2 clients, utility 0.957548:
    h1: 1346.57 MB (U(h) = 0.394441*h^0.120056)
    h2: 1413.43 MB (U(h) = 0.40974*h^0.126017)
Reading from 30689: heap size 467 MB, throughput 0.988369
Reading from 30690: heap size 482 MB, throughput 0.988307
Reading from 30689: heap size 466 MB, throughput 0.989943
Reading from 30690: heap size 483 MB, throughput 0.988802
Reading from 30689: heap size 469 MB, throughput 0.987015
Reading from 30690: heap size 485 MB, throughput 0.988229
Reading from 30689: heap size 467 MB, throughput 0.983932
Reading from 30690: heap size 484 MB, throughput 0.987468
Numeric result:
Recommendation: 2 clients, utility 0.985019:
    h1: 1351.8 MB (U(h) = 0.385933*h^0.125214)
    h2: 1408.2 MB (U(h) = 0.401981*h^0.130434)
Recommendation: 2 clients, utility 0.985019:
    h1: 1351.82 MB (U(h) = 0.385933*h^0.125214)
    h2: 1408.18 MB (U(h) = 0.401981*h^0.130434)
Reading from 30689: heap size 469 MB, throughput 0.984808
Reading from 30690: heap size 486 MB, throughput 0.985364
Reading from 30690: heap size 487 MB, throughput 0.984826
Reading from 30689: heap size 472 MB, throughput 0.987087
Reading from 30689: heap size 473 MB, throughput 0.982976
Reading from 30689: heap size 474 MB, throughput 0.974308
Reading from 30689: heap size 476 MB, throughput 0.958496
Reading from 30690: heap size 488 MB, throughput 0.988226
Reading from 30689: heap size 482 MB, throughput 0.980632
Reading from 30690: heap size 487 MB, throughput 0.984248
Reading from 30690: heap size 489 MB, throughput 0.972837
Reading from 30690: heap size 494 MB, throughput 0.964182
Reading from 30689: heap size 483 MB, throughput 0.989748
Reading from 30690: heap size 495 MB, throughput 0.987047
Numeric result:
Recommendation: 2 clients, utility 1.02428:
    h1: 1370.76 MB (U(h) = 0.372447*h^0.133575)
    h2: 1389.24 MB (U(h) = 0.393434*h^0.135379)
Recommendation: 2 clients, utility 1.02428:
    h1: 1370.75 MB (U(h) = 0.372447*h^0.133575)
    h2: 1389.25 MB (U(h) = 0.393434*h^0.135379)
Reading from 30690: heap size 502 MB, throughput 0.989928
Reading from 30689: heap size 487 MB, throughput 0.990773
Reading from 30689: heap size 489 MB, throughput 0.991215
Reading from 30690: heap size 503 MB, throughput 0.989787
Reading from 30689: heap size 488 MB, throughput 0.990521
Reading from 30690: heap size 504 MB, throughput 0.989687
Reading from 30689: heap size 490 MB, throughput 0.988595
Reading from 30690: heap size 506 MB, throughput 0.988657
Numeric result:
Recommendation: 2 clients, utility 1.29665:
    h1: 1814.91 MB (U(h) = 0.175996*h^0.265851)
    h2: 945.087 MB (U(h) = 0.388193*h^0.138432)
Recommendation: 2 clients, utility 1.29665:
    h1: 1814.94 MB (U(h) = 0.175996*h^0.265851)
    h2: 945.063 MB (U(h) = 0.388193*h^0.138432)
Reading from 30689: heap size 491 MB, throughput 0.986835
Reading from 30690: heap size 505 MB, throughput 0.988037
Reading from 30690: heap size 507 MB, throughput 0.987312
Reading from 30689: heap size 492 MB, throughput 0.989169
Reading from 30689: heap size 494 MB, throughput 0.9853
Reading from 30689: heap size 494 MB, throughput 0.975869
Reading from 30689: heap size 498 MB, throughput 0.971802
Reading from 30690: heap size 509 MB, throughput 0.989766
Reading from 30690: heap size 510 MB, throughput 0.984352
Reading from 30690: heap size 510 MB, throughput 0.974345
Reading from 30689: heap size 500 MB, throughput 0.988857
Reading from 30690: heap size 513 MB, throughput 0.982452
Numeric result:
Recommendation: 2 clients, utility 1.72647:
    h1: 2068.31 MB (U(h) = 0.070682*h^0.423099)
    h2: 691.693 MB (U(h) = 0.382992*h^0.141493)
Recommendation: 2 clients, utility 1.72647:
    h1: 2068.32 MB (U(h) = 0.070682*h^0.423099)
    h2: 691.684 MB (U(h) = 0.382992*h^0.141493)
Reading from 30689: heap size 503 MB, throughput 0.988903
Reading from 30690: heap size 520 MB, throughput 0.990677
Reading from 30689: heap size 505 MB, throughput 0.990644
Reading from 30690: heap size 521 MB, throughput 0.990372
Reading from 30689: heap size 505 MB, throughput 0.991606
Reading from 30690: heap size 523 MB, throughput 0.989928
Reading from 30689: heap size 507 MB, throughput 0.989966
Numeric result:
Recommendation: 2 clients, utility 1.62293:
    h1: 2011.33 MB (U(h) = 0.0887095*h^0.384697)
    h2: 748.673 MB (U(h) = 0.380094*h^0.1432)
Recommendation: 2 clients, utility 1.62293:
    h1: 2011.31 MB (U(h) = 0.0887095*h^0.384697)
    h2: 748.691 MB (U(h) = 0.380094*h^0.1432)
Reading from 30690: heap size 525 MB, throughput 0.990563
Reading from 30689: heap size 507 MB, throughput 0.988523
Reading from 30690: heap size 524 MB, throughput 0.989609
Reading from 30689: heap size 508 MB, throughput 0.990757
Reading from 30689: heap size 510 MB, throughput 0.987345
Reading from 30689: heap size 511 MB, throughput 0.976327
Reading from 30690: heap size 526 MB, throughput 0.989109
Reading from 30689: heap size 515 MB, throughput 0.978817
Reading from 30690: heap size 528 MB, throughput 0.989922
Reading from 30690: heap size 529 MB, throughput 0.983581
Reading from 30690: heap size 529 MB, throughput 0.975393
Reading from 30689: heap size 516 MB, throughput 0.989315
Numeric result:
Recommendation: 2 clients, utility 2.45026:
    h1: 1795.61 MB (U(h) = 0.0326328*h^0.553072)
    h2: 964.389 MB (U(h) = 0.154652*h^0.297022)
Recommendation: 2 clients, utility 2.45026:
    h1: 1795.66 MB (U(h) = 0.0326328*h^0.553072)
    h2: 964.341 MB (U(h) = 0.154652*h^0.297022)
Reading from 30690: heap size 531 MB, throughput 0.986058
Reading from 30689: heap size 520 MB, throughput 0.990505
Reading from 30690: heap size 539 MB, throughput 0.989274
Reading from 30689: heap size 521 MB, throughput 0.99162
Reading from 30690: heap size 540 MB, throughput 0.990305
Reading from 30689: heap size 520 MB, throughput 0.991073
Numeric result:
Recommendation: 2 clients, utility 3.02117:
    h1: 1786.81 MB (U(h) = 0.0176593*h^0.655613)
    h2: 973.194 MB (U(h) = 0.108162*h^0.35708)
Recommendation: 2 clients, utility 3.02117:
    h1: 1786.81 MB (U(h) = 0.0176593*h^0.655613)
    h2: 973.187 MB (U(h) = 0.108162*h^0.35708)
Reading from 30690: heap size 541 MB, throughput 0.990377
Reading from 30689: heap size 522 MB, throughput 0.990443
Reading from 30690: heap size 543 MB, throughput 0.99009
Reading from 30689: heap size 524 MB, throughput 0.988844
Reading from 30689: heap size 524 MB, throughput 0.989734
Reading from 30689: heap size 524 MB, throughput 0.985154
Reading from 30690: heap size 545 MB, throughput 0.98927
Reading from 30689: heap size 526 MB, throughput 0.979259
Reading from 30689: heap size 534 MB, throughput 0.987546
Reading from 30690: heap size 545 MB, throughput 0.991241
Reading from 30690: heap size 549 MB, throughput 0.986236
Numeric result:
Recommendation: 2 clients, utility 2.32801:
    h1: 1460.53 MB (U(h) = 0.0628609*h^0.44384)
    h2: 1299.47 MB (U(h) = 0.0859721*h^0.394927)
Recommendation: 2 clients, utility 2.32801:
    h1: 1460.48 MB (U(h) = 0.0628609*h^0.44384)
    h2: 1299.52 MB (U(h) = 0.0859721*h^0.394927)
Reading from 30690: heap size 549 MB, throughput 0.976579
Reading from 30689: heap size 534 MB, throughput 0.990222
Reading from 30690: heap size 556 MB, throughput 0.990184
Reading from 30689: heap size 536 MB, throughput 0.990518
Reading from 30690: heap size 558 MB, throughput 0.991435
Reading from 30689: heap size 537 MB, throughput 0.990279
Reading from 30690: heap size 560 MB, throughput 0.991656
Numeric result:
Recommendation: 2 clients, utility 2.44034:
    h1: 1610.29 MB (U(h) = 0.0426308*h^0.50808)
    h2: 1149.71 MB (U(h) = 0.104241*h^0.362782)
Recommendation: 2 clients, utility 2.44034:
    h1: 1610.24 MB (U(h) = 0.0426308*h^0.50808)
    h2: 1149.76 MB (U(h) = 0.104241*h^0.362782)
Reading from 30689: heap size 537 MB, throughput 0.990743
Reading from 30690: heap size 563 MB, throughput 0.990571
Reading from 30689: heap size 538 MB, throughput 0.989236
Reading from 30690: heap size 562 MB, throughput 0.989798
Reading from 30689: heap size 539 MB, throughput 0.987245
Reading from 30689: heap size 542 MB, throughput 0.980729
Reading from 30689: heap size 545 MB, throughput 0.983754
Reading from 30690: heap size 564 MB, throughput 0.988189
Numeric result:
Recommendation: 2 clients, utility 2.49018:
    h1: 1597.12 MB (U(h) = 0.0401075*h^0.517475)
    h2: 1162.88 MB (U(h) = 0.0955729*h^0.376777)
Recommendation: 2 clients, utility 2.49018:
    h1: 1597.12 MB (U(h) = 0.0401075*h^0.517475)
    h2: 1162.88 MB (U(h) = 0.0955729*h^0.376777)
Reading from 30690: heap size 566 MB, throughput 0.986872
Reading from 30689: heap size 547 MB, throughput 0.98987
Reading from 30690: heap size 569 MB, throughput 0.97852
Reading from 30690: heap size 574 MB, throughput 0.985933
Reading from 30689: heap size 550 MB, throughput 0.990557
Reading from 30690: heap size 575 MB, throughput 0.992228
Reading from 30689: heap size 551 MB, throughput 0.991599
Numeric result:
Recommendation: 2 clients, utility 2.7723:
    h1: 1651.52 MB (U(h) = 0.0256194*h^0.590623)
    h2: 1108.48 MB (U(h) = 0.0844759*h^0.396416)
Recommendation: 2 clients, utility 2.7723:
    h1: 1651.52 MB (U(h) = 0.0256194*h^0.590623)
    h2: 1108.48 MB (U(h) = 0.0844759*h^0.396416)
Reading from 30689: heap size 549 MB, throughput 0.990419
Reading from 30690: heap size 579 MB, throughput 0.993717
Reading from 30690: heap size 580 MB, throughput 0.992169
Reading from 30689: heap size 551 MB, throughput 0.990242
Reading from 30689: heap size 554 MB, throughput 0.992054
Reading from 30689: heap size 554 MB, throughput 0.98574
Reading from 30689: heap size 554 MB, throughput 0.977114
Reading from 30690: heap size 580 MB, throughput 0.988105
Numeric result:
Recommendation: 2 clients, utility 2.60282:
    h1: 1708.61 MB (U(h) = 0.0284001*h^0.573412)
    h2: 1051.39 MB (U(h) = 0.110205*h^0.352869)
Recommendation: 2 clients, utility 2.60282:
    h1: 1708.57 MB (U(h) = 0.0284001*h^0.573412)
    h2: 1051.43 MB (U(h) = 0.110205*h^0.352869)
Reading from 30689: heap size 557 MB, throughput 0.989319
Reading from 30690: heap size 582 MB, throughput 0.988266
Reading from 30690: heap size 581 MB, throughput 0.986334
Reading from 30690: heap size 586 MB, throughput 0.980247
Reading from 30689: heap size 562 MB, throughput 0.991389
Reading from 30690: heap size 591 MB, throughput 0.986304
Reading from 30689: heap size 563 MB, throughput 0.991408
Reading from 30690: heap size 591 MB, throughput 0.990943
Numeric result:
Recommendation: 2 clients, utility 2.4946:
    h1: 1697.16 MB (U(h) = 0.0324034*h^0.551208)
    h2: 1062.84 MB (U(h) = 0.115197*h^0.345195)
Recommendation: 2 clients, utility 2.4946:
    h1: 1697.15 MB (U(h) = 0.0324034*h^0.551208)
    h2: 1062.85 MB (U(h) = 0.115197*h^0.345195)
Reading from 30689: heap size 562 MB, throughput 0.991539
Reading from 30690: heap size 594 MB, throughput 0.992081
Reading from 30689: heap size 565 MB, throughput 0.990105
Reading from 30690: heap size 596 MB, throughput 0.991683
Reading from 30689: heap size 568 MB, throughput 0.992187
Reading from 30689: heap size 569 MB, throughput 0.986731
Reading from 30689: heap size 570 MB, throughput 0.985157
Reading from 30690: heap size 596 MB, throughput 0.990667
Numeric result:
Recommendation: 2 clients, utility 2.46745:
    h1: 1991.34 MB (U(h) = 0.0236653*h^0.601196)
    h2: 768.664 MB (U(h) = 0.231759*h^0.232061)
Recommendation: 2 clients, utility 2.46745:
    h1: 1991.34 MB (U(h) = 0.0236653*h^0.601196)
    h2: 768.655 MB (U(h) = 0.231759*h^0.232061)
Reading from 30689: heap size 573 MB, throughput 0.990873
Reading from 30690: heap size 598 MB, throughput 0.992416
Reading from 30690: heap size 598 MB, throughput 0.988116
Reading from 30690: heap size 600 MB, throughput 0.986985
Reading from 30689: heap size 575 MB, throughput 0.99199
Reading from 30689: heap size 577 MB, throughput 0.992195
Reading from 30690: heap size 607 MB, throughput 0.992185
Numeric result:
Recommendation: 2 clients, utility 1.99614:
    h1: 1891.29 MB (U(h) = 0.0567015*h^0.459365)
    h2: 868.713 MB (U(h) = 0.26381*h^0.210992)
Recommendation: 2 clients, utility 1.99614:
    h1: 1891.3 MB (U(h) = 0.0567015*h^0.459365)
    h2: 868.698 MB (U(h) = 0.26381*h^0.210992)
Reading from 30689: heap size 575 MB, throughput 0.991629
Reading from 30690: heap size 608 MB, throughput 0.992034
Reading from 30689: heap size 577 MB, throughput 0.990773
Reading from 30690: heap size 608 MB, throughput 0.992295
Reading from 30689: heap size 580 MB, throughput 0.989823
Reading from 30689: heap size 581 MB, throughput 0.984147
Numeric result:
Recommendation: 2 clients, utility 1.86903:
    h1: 1841.27 MB (U(h) = 0.0750524*h^0.413802)
    h2: 918.725 MB (U(h) = 0.271216*h^0.206475)
Recommendation: 2 clients, utility 1.86903:
    h1: 1841.26 MB (U(h) = 0.0750524*h^0.413802)
    h2: 918.737 MB (U(h) = 0.271216*h^0.206475)
Reading from 30690: heap size 610 MB, throughput 0.9928
Reading from 30689: heap size 585 MB, throughput 0.988538
Reading from 30689: heap size 587 MB, throughput 0.991518
Reading from 30690: heap size 612 MB, throughput 0.994004
Reading from 30690: heap size 613 MB, throughput 0.990718
Reading from 30690: heap size 613 MB, throughput 0.988209
Reading from 30689: heap size 588 MB, throughput 0.991806
Reading from 30690: heap size 615 MB, throughput 0.992362
Numeric result:
Recommendation: 2 clients, utility 1.39159:
    h1: 1601 MB (U(h) = 0.265351*h^0.210241)
    h2: 1159 MB (U(h) = 0.379867*h^0.152204)
Recommendation: 2 clients, utility 1.39159:
    h1: 1600.97 MB (U(h) = 0.265351*h^0.210241)
    h2: 1159.03 MB (U(h) = 0.379867*h^0.152204)
Reading from 30689: heap size 590 MB, throughput 0.991309
Reading from 30690: heap size 617 MB, throughput 0.992608
Reading from 30689: heap size 589 MB, throughput 0.991165
Reading from 30690: heap size 619 MB, throughput 0.992262
Reading from 30689: heap size 591 MB, throughput 0.99203
Reading from 30689: heap size 593 MB, throughput 0.987568
Numeric result:
Recommendation: 2 clients, utility 1.36622:
    h1: 2233.8 MB (U(h) = 0.229599*h^0.233179)
    h2: 526.203 MB (U(h) = 0.698485*h^0.0549277)
Recommendation: 2 clients, utility 1.36622:
    h1: 2233.8 MB (U(h) = 0.229599*h^0.233179)
    h2: 526.196 MB (U(h) = 0.698485*h^0.0549277)
Reading from 30689: heap size 594 MB, throughput 0.987658
Reading from 30690: heap size 619 MB, throughput 0.99191
Reading from 30689: heap size 600 MB, throughput 0.991174
Reading from 30690: heap size 601 MB, throughput 0.992663
Reading from 30690: heap size 591 MB, throughput 0.987939
Reading from 30690: heap size 588 MB, throughput 0.986907
Reading from 30689: heap size 601 MB, throughput 0.993225
Numeric result:
Recommendation: 2 clients, utility 1.22719:
    h1: 2101.91 MB (U(h) = 0.362918*h^0.159706)
    h2: 658.09 MB (U(h) = 0.720365*h^0.0499983)
Recommendation: 2 clients, utility 1.22719:
    h1: 2101.95 MB (U(h) = 0.362918*h^0.159706)
    h2: 658.048 MB (U(h) = 0.720365*h^0.0499983)
Reading from 30690: heap size 578 MB, throughput 0.991723
Reading from 30689: heap size 600 MB, throughput 0.992422
Reading from 30690: heap size 587 MB, throughput 0.991713
Reading from 30689: heap size 602 MB, throughput 0.99152
Reading from 30690: heap size 587 MB, throughput 0.991849
Reading from 30689: heap size 605 MB, throughput 0.99198
Numeric result:
Recommendation: 2 clients, utility 1.12391:
    h1: 1794.65 MB (U(h) = 0.548209*h^0.0936961)
    h2: 965.346 MB (U(h) = 0.718632*h^0.0503879)
Recommendation: 2 clients, utility 1.12391:
    h1: 1794.79 MB (U(h) = 0.548209*h^0.0936961)
    h2: 965.206 MB (U(h) = 0.718632*h^0.0503879)
Reading from 30689: heap size 605 MB, throughput 0.98731
Client 30689 died
Clients: 1
Reading from 30690: heap size 589 MB, throughput 0.992716
Reading from 30690: heap size 591 MB, throughput 0.992414
Reading from 30690: heap size 592 MB, throughput 0.991035
Reading from 30690: heap size 596 MB, throughput 0.986462
Client 30690 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
