#include <signal.h>
#include "Thread.hpp"
#include "Utilities.hpp"

const int economem::Thread::THREAD_INTERRUPT_SIG = SIGUSR1;

economem::Thread::Thread() : m_Lock(true), m_Alive(false) {}

economem::Thread::~Thread()
{
    kill();
    wait();
}

bool economem::Thread::start()
{
    bool retval = true;
    m_Lock.lock();

    if (!m_Alive)
    {
        if (pthread_create(&m_Thread, NULL, &economem::Thread::pthreadStub, (void*)this) == 0)
            m_Alive = true;
        else
            retval = false;
    }

    m_Lock.unlock();
    return retval;
}

bool economem::Thread::startAndWait()
{
    m_Lock.lock();

    if (!m_Alive)
    {
        if (pthread_create(&m_Thread, NULL, &economem::Thread::pthreadStub, (void*)this) == 0)
            m_Alive = true;
        else
        {
            m_Lock.unlock();
            return false;
        }
    }

    m_Lock.unlock();
    wait();
    return true;
}

bool economem::Thread::isAlive()
{
    bool retval;
    m_Lock.lock();
    retval = m_Alive;
    m_Lock.unlock();
    return retval;
}

void economem::Thread::kill()
{
    m_Lock.lock();
    if (m_Alive)
    {
        onKill();
        m_Alive = false;
    }
    m_Lock.unlock();
}

void economem::Thread::wait()
{
    pthread_join(m_Thread, NULL);
}

void economem::Thread::condWaitOnLock(pthread_cond_t* cond)
{
    m_Lock.condWaitOnLock(cond);
}

void economem::Thread::signal()
{
    pthread_kill(m_Thread, THREAD_INTERRUPT_SIG);
}

void* economem::Thread::pthreadStub(void* arg)
{
    ((economem::Thread*)arg)->threadFunc();
    return NULL;
}
