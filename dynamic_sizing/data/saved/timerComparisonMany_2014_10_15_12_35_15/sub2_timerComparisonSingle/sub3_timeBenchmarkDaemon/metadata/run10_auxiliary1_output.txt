economemd
    total memory: 4461 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub2_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run10_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 31500: heap size 9 MB, throughput 0.992061
Clients: 1
Client 31500 has a minimum heap size of 276 MB
Reading from 31499: heap size 9 MB, throughput 0.990849
Clients: 2
Client 31499 has a minimum heap size of 1211 MB
Reading from 31500: heap size 9 MB, throughput 0.988331
Reading from 31499: heap size 9 MB, throughput 0.987621
Reading from 31499: heap size 9 MB, throughput 0.981186
Reading from 31500: heap size 9 MB, throughput 0.983356
Reading from 31499: heap size 9 MB, throughput 0.970433
Reading from 31500: heap size 9 MB, throughput 0.974673
Reading from 31499: heap size 11 MB, throughput 0.982551
Reading from 31500: heap size 11 MB, throughput 0.983047
Reading from 31499: heap size 11 MB, throughput 0.974207
Reading from 31500: heap size 11 MB, throughput 0.977293
Reading from 31500: heap size 17 MB, throughput 0.959212
Reading from 31499: heap size 17 MB, throughput 0.969791
Reading from 31499: heap size 17 MB, throughput 0.906527
Reading from 31500: heap size 17 MB, throughput 0.904157
Reading from 31499: heap size 30 MB, throughput 0.818292
Reading from 31500: heap size 30 MB, throughput 0.784099
Reading from 31500: heap size 31 MB, throughput 0.440375
Reading from 31499: heap size 31 MB, throughput 0.387398
Reading from 31500: heap size 33 MB, throughput 0.558134
Reading from 31499: heap size 34 MB, throughput 0.67661
Reading from 31500: heap size 42 MB, throughput 0.0765575
Reading from 31499: heap size 46 MB, throughput 0.473823
Reading from 31499: heap size 48 MB, throughput 0.476346
Reading from 31500: heap size 47 MB, throughput 0.533036
Reading from 31500: heap size 60 MB, throughput 0.219443
Reading from 31499: heap size 50 MB, throughput 0.566783
Reading from 31500: heap size 68 MB, throughput 0.813669
Reading from 31499: heap size 75 MB, throughput 0.512708
Reading from 31499: heap size 75 MB, throughput 0.404877
Reading from 31500: heap size 69 MB, throughput 0.413678
Reading from 31499: heap size 102 MB, throughput 0.661736
Reading from 31500: heap size 94 MB, throughput 0.831593
Reading from 31499: heap size 103 MB, throughput 0.627785
Reading from 31499: heap size 104 MB, throughput 0.373954
Reading from 31499: heap size 107 MB, throughput 0.350838
Reading from 31500: heap size 96 MB, throughput 0.373323
Reading from 31500: heap size 127 MB, throughput 0.395472
Reading from 31500: heap size 127 MB, throughput 0.228481
Reading from 31499: heap size 109 MB, throughput 0.535946
Reading from 31500: heap size 129 MB, throughput 0.795906
Reading from 31499: heap size 142 MB, throughput 0.359733
Reading from 31500: heap size 134 MB, throughput 0.806431
Reading from 31499: heap size 145 MB, throughput 0.177544
Reading from 31500: heap size 137 MB, throughput 0.730616
Reading from 31499: heap size 148 MB, throughput 0.720002
Reading from 31499: heap size 156 MB, throughput 0.730823
Reading from 31499: heap size 158 MB, throughput 0.698903
Reading from 31500: heap size 142 MB, throughput 0.458801
Reading from 31500: heap size 182 MB, throughput 0.642946
Reading from 31499: heap size 161 MB, throughput 0.404946
Reading from 31500: heap size 185 MB, throughput 0.682216
Reading from 31499: heap size 205 MB, throughput 0.693717
Reading from 31500: heap size 187 MB, throughput 0.692231
Reading from 31499: heap size 211 MB, throughput 0.704728
Reading from 31500: heap size 195 MB, throughput 0.623175
Reading from 31499: heap size 213 MB, throughput 0.634157
Reading from 31499: heap size 217 MB, throughput 0.629644
Reading from 31499: heap size 224 MB, throughput 0.573117
Reading from 31500: heap size 199 MB, throughput 0.831721
Reading from 31499: heap size 232 MB, throughput 0.60993
Reading from 31500: heap size 209 MB, throughput 0.857453
Reading from 31500: heap size 213 MB, throughput 0.752288
Reading from 31499: heap size 238 MB, throughput 0.364409
Reading from 31500: heap size 225 MB, throughput 0.697518
Reading from 31500: heap size 231 MB, throughput 0.560761
Reading from 31499: heap size 283 MB, throughput 0.558933
Reading from 31500: heap size 239 MB, throughput 0.469893
Reading from 31499: heap size 287 MB, throughput 0.571396
Reading from 31499: heap size 289 MB, throughput 0.551926
Reading from 31500: heap size 246 MB, throughput 0.544915
Reading from 31500: heap size 217 MB, throughput 0.542485
Reading from 31499: heap size 291 MB, throughput 0.321736
Reading from 31499: heap size 340 MB, throughput 0.619914
Reading from 31500: heap size 274 MB, throughput 0.200224
Reading from 31499: heap size 341 MB, throughput 0.654543
Reading from 31500: heap size 324 MB, throughput 0.511454
Reading from 31499: heap size 344 MB, throughput 0.627863
Reading from 31500: heap size 324 MB, throughput 0.650925
Reading from 31500: heap size 325 MB, throughput 0.706241
Reading from 31499: heap size 345 MB, throughput 0.63726
Reading from 31500: heap size 323 MB, throughput 0.786544
Reading from 31499: heap size 349 MB, throughput 0.655008
Reading from 31500: heap size 325 MB, throughput 0.831934
Reading from 31500: heap size 315 MB, throughput 0.803552
Reading from 31500: heap size 241 MB, throughput 0.670563
Reading from 31500: heap size 311 MB, throughput 0.681779
Reading from 31500: heap size 315 MB, throughput 0.758825
Reading from 31500: heap size 309 MB, throughput 0.712858
Reading from 31500: heap size 312 MB, throughput 0.747451
Reading from 31500: heap size 305 MB, throughput 0.786192
Reading from 31499: heap size 355 MB, throughput 0.417361
Reading from 31499: heap size 408 MB, throughput 0.60152
Reading from 31500: heap size 309 MB, throughput 0.929817
Reading from 31499: heap size 414 MB, throughput 0.621333
Reading from 31500: heap size 300 MB, throughput 0.91562
Reading from 31499: heap size 416 MB, throughput 0.628966
Reading from 31500: heap size 255 MB, throughput 0.854219
Reading from 31500: heap size 297 MB, throughput 0.846524
Reading from 31499: heap size 422 MB, throughput 0.625246
Reading from 31500: heap size 301 MB, throughput 0.820931
Reading from 31500: heap size 295 MB, throughput 0.817689
Reading from 31499: heap size 425 MB, throughput 0.574576
Reading from 31500: heap size 298 MB, throughput 0.866462
Reading from 31499: heap size 435 MB, throughput 0.5447
Reading from 31500: heap size 294 MB, throughput 0.884159
Reading from 31499: heap size 444 MB, throughput 0.518534
Reading from 31500: heap size 297 MB, throughput 0.876133
Reading from 31500: heap size 297 MB, throughput 0.779019
Reading from 31500: heap size 299 MB, throughput 0.644301
Reading from 31500: heap size 302 MB, throughput 0.632728
Reading from 31500: heap size 304 MB, throughput 0.595088
Reading from 31500: heap size 310 MB, throughput 0.562404
Numeric result:
Recommendation: 2 clients, utility 0.617329:
    h1: 3250 MB (U(h) = 0.448792*h^0.065467)
    h2: 1211 MB (U(h) = 0.804406*h^0.001)
Recommendation: 2 clients, utility 0.617329:
    h1: 3250 MB (U(h) = 0.448792*h^0.065467)
    h2: 1211 MB (U(h) = 0.804406*h^0.001)
Reading from 31500: heap size 311 MB, throughput 0.602228
Reading from 31500: heap size 319 MB, throughput 0.606942
Reading from 31499: heap size 451 MB, throughput 0.298113
Reading from 31499: heap size 522 MB, throughput 0.495494
Reading from 31499: heap size 523 MB, throughput 0.479283
Reading from 31499: heap size 524 MB, throughput 0.504869
Reading from 31500: heap size 320 MB, throughput 0.946168
Reading from 31499: heap size 525 MB, throughput 0.250766
Reading from 31500: heap size 323 MB, throughput 0.960032
Reading from 31499: heap size 594 MB, throughput 0.475432
Reading from 31499: heap size 594 MB, throughput 0.559081
Reading from 31499: heap size 593 MB, throughput 0.621351
Reading from 31499: heap size 594 MB, throughput 0.602632
Reading from 31499: heap size 596 MB, throughput 0.59414
Reading from 31499: heap size 605 MB, throughput 0.576397
Reading from 31500: heap size 325 MB, throughput 0.948786
Reading from 31499: heap size 609 MB, throughput 0.543839
Reading from 31499: heap size 623 MB, throughput 0.524281
Reading from 31500: heap size 325 MB, throughput 0.958185
Reading from 31500: heap size 327 MB, throughput 0.939376
Reading from 31499: heap size 631 MB, throughput 0.397404
Reading from 31499: heap size 707 MB, throughput 0.75526
Reading from 31500: heap size 324 MB, throughput 0.952613
Reading from 31499: heap size 714 MB, throughput 0.77998
Reading from 31499: heap size 723 MB, throughput 0.797493
Reading from 31499: heap size 729 MB, throughput 0.696132
Reading from 31499: heap size 740 MB, throughput 0.61772
Reading from 31499: heap size 749 MB, throughput 0.620095
Reading from 31500: heap size 327 MB, throughput 0.966427
Reading from 31499: heap size 751 MB, throughput 0.547395
Reading from 31500: heap size 322 MB, throughput 0.965788
Numeric result:
Recommendation: 2 clients, utility 0.605336:
    h1: 3250 MB (U(h) = 0.408671*h^0.0895094)
    h2: 1211 MB (U(h) = 0.713171*h^0.001)
Recommendation: 2 clients, utility 0.605336:
    h1: 3250 MB (U(h) = 0.408671*h^0.0895094)
    h2: 1211 MB (U(h) = 0.713171*h^0.001)
Reading from 31499: heap size 747 MB, throughput 0.398394
Reading from 31499: heap size 712 MB, throughput 0.380882
Reading from 31499: heap size 810 MB, throughput 0.428531
Reading from 31500: heap size 325 MB, throughput 0.972151
Reading from 31500: heap size 325 MB, throughput 0.520782
Reading from 31499: heap size 814 MB, throughput 0.228511
Reading from 31499: heap size 895 MB, throughput 0.635583
Reading from 31499: heap size 897 MB, throughput 0.639931
Reading from 31499: heap size 899 MB, throughput 0.670789
Reading from 31499: heap size 900 MB, throughput 0.667965
Reading from 31500: heap size 326 MB, throughput 0.527053
Reading from 31499: heap size 904 MB, throughput 0.783501
Reading from 31500: heap size 329 MB, throughput 0.564698
Reading from 31499: heap size 904 MB, throughput 0.406168
Reading from 31499: heap size 1001 MB, throughput 0.683413
Reading from 31499: heap size 1002 MB, throughput 0.833379
Reading from 31499: heap size 1006 MB, throughput 0.868625
Reading from 31500: heap size 329 MB, throughput 0.966163
Reading from 31499: heap size 1008 MB, throughput 0.913482
Reading from 31499: heap size 987 MB, throughput 0.886101
Reading from 31499: heap size 824 MB, throughput 0.849014
Reading from 31499: heap size 968 MB, throughput 0.808708
Reading from 31499: heap size 829 MB, throughput 0.822972
Reading from 31499: heap size 954 MB, throughput 0.818881
Reading from 31499: heap size 832 MB, throughput 0.818647
Reading from 31499: heap size 943 MB, throughput 0.822187
Reading from 31500: heap size 331 MB, throughput 0.971104
Reading from 31499: heap size 838 MB, throughput 0.824543
Reading from 31499: heap size 934 MB, throughput 0.818808
Reading from 31499: heap size 843 MB, throughput 0.827804
Reading from 31499: heap size 927 MB, throughput 0.827922
Reading from 31500: heap size 332 MB, throughput 0.972521
Reading from 31499: heap size 933 MB, throughput 0.962801
Reading from 31500: heap size 334 MB, throughput 0.970623
Reading from 31499: heap size 925 MB, throughput 0.955612
Reading from 31499: heap size 929 MB, throughput 0.913854
Numeric result:
Recommendation: 2 clients, utility 0.541951:
    h1: 3250 MB (U(h) = 0.393555*h^0.0990296)
    h2: 1211 MB (U(h) = 0.49895*h^0.0302018)
Recommendation: 2 clients, utility 0.541951:
    h1: 3250 MB (U(h) = 0.393555*h^0.0990296)
    h2: 1211 MB (U(h) = 0.49895*h^0.0302018)
Reading from 31499: heap size 934 MB, throughput 0.854878
Reading from 31499: heap size 934 MB, throughput 0.824687
Reading from 31499: heap size 939 MB, throughput 0.80852
Reading from 31499: heap size 940 MB, throughput 0.80536
Reading from 31499: heap size 943 MB, throughput 0.775848
Reading from 31499: heap size 945 MB, throughput 0.714969
Reading from 31500: heap size 335 MB, throughput 0.915829
Reading from 31499: heap size 949 MB, throughput 0.792129
Reading from 31499: heap size 951 MB, throughput 0.799118
Reading from 31499: heap size 953 MB, throughput 0.816639
Reading from 31500: heap size 370 MB, throughput 0.967866
Reading from 31500: heap size 372 MB, throughput 0.95419
Reading from 31499: heap size 956 MB, throughput 0.799928
Reading from 31500: heap size 374 MB, throughput 0.9386
Reading from 31500: heap size 374 MB, throughput 0.874232
Reading from 31499: heap size 961 MB, throughput 0.768113
Reading from 31500: heap size 379 MB, throughput 0.873232
Reading from 31500: heap size 378 MB, throughput 0.864704
Reading from 31500: heap size 384 MB, throughput 0.976242
Reading from 31499: heap size 962 MB, throughput 0.702038
Reading from 31499: heap size 1092 MB, throughput 0.61589
Reading from 31499: heap size 1093 MB, throughput 0.640113
Reading from 31499: heap size 1102 MB, throughput 0.684285
Reading from 31499: heap size 1103 MB, throughput 0.645727
Reading from 31499: heap size 1112 MB, throughput 0.625345
Reading from 31500: heap size 385 MB, throughput 0.984433
Reading from 31499: heap size 1114 MB, throughput 0.656346
Reading from 31499: heap size 1121 MB, throughput 0.692618
Reading from 31499: heap size 1125 MB, throughput 0.708224
Reading from 31500: heap size 390 MB, throughput 0.98087
Reading from 31500: heap size 391 MB, throughput 0.983056
Reading from 31499: heap size 1141 MB, throughput 0.941236
Numeric result:
Recommendation: 2 clients, utility 0.785603:
    h1: 1818.59 MB (U(h) = 0.339147*h^0.135153)
    h2: 2642.41 MB (U(h) = 0.178832*h^0.19632)
Recommendation: 2 clients, utility 0.785603:
    h1: 1818.9 MB (U(h) = 0.339147*h^0.135153)
    h2: 2642.1 MB (U(h) = 0.178832*h^0.19632)
Reading from 31500: heap size 390 MB, throughput 0.986404
Reading from 31499: heap size 1141 MB, throughput 0.948258
Reading from 31500: heap size 392 MB, throughput 0.985693
Reading from 31499: heap size 1156 MB, throughput 0.950774
Reading from 31500: heap size 390 MB, throughput 0.98582
Reading from 31499: heap size 1159 MB, throughput 0.949864
Reading from 31500: heap size 392 MB, throughput 0.984181
Reading from 31500: heap size 393 MB, throughput 0.98285
Reading from 31499: heap size 1172 MB, throughput 0.951142
Reading from 31500: heap size 393 MB, throughput 0.983975
Reading from 31499: heap size 1178 MB, throughput 0.951084
Reading from 31500: heap size 396 MB, throughput 0.983353
Numeric result:
Recommendation: 2 clients, utility 0.878522:
    h1: 1660.18 MB (U(h) = 0.325457*h^0.144991)
    h2: 2800.82 MB (U(h) = 0.132151*h^0.244626)
Recommendation: 2 clients, utility 0.878522:
    h1: 1660.1 MB (U(h) = 0.325457*h^0.144991)
    h2: 2800.9 MB (U(h) = 0.132151*h^0.244626)
Reading from 31499: heap size 1188 MB, throughput 0.955727
Reading from 31500: heap size 397 MB, throughput 0.981016
Reading from 31499: heap size 1192 MB, throughput 0.956183
Reading from 31500: heap size 398 MB, throughput 0.985627
Reading from 31500: heap size 400 MB, throughput 0.975249
Reading from 31500: heap size 396 MB, throughput 0.960187
Reading from 31500: heap size 401 MB, throughput 0.936745
Reading from 31500: heap size 407 MB, throughput 0.922379
Reading from 31500: heap size 409 MB, throughput 0.975826
Reading from 31499: heap size 1191 MB, throughput 0.958094
Reading from 31500: heap size 411 MB, throughput 0.98456
Reading from 31499: heap size 1194 MB, throughput 0.953005
Reading from 31500: heap size 413 MB, throughput 0.987064
Numeric result:
Recommendation: 2 clients, utility 0.917931:
    h1: 1922.29 MB (U(h) = 0.299852*h^0.164372)
    h2: 2538.71 MB (U(h) = 0.161086*h^0.217081)
Recommendation: 2 clients, utility 0.917931:
    h1: 1922.29 MB (U(h) = 0.299852*h^0.164372)
    h2: 2538.71 MB (U(h) = 0.161086*h^0.217081)
Reading from 31499: heap size 1202 MB, throughput 0.952983
Reading from 31500: heap size 412 MB, throughput 0.986115
Reading from 31500: heap size 414 MB, throughput 0.98662
Reading from 31499: heap size 1203 MB, throughput 0.954449
Reading from 31500: heap size 412 MB, throughput 0.985924
Reading from 31499: heap size 1209 MB, throughput 0.955152
Reading from 31500: heap size 414 MB, throughput 0.983804
Reading from 31499: heap size 1213 MB, throughput 0.95525
Reading from 31500: heap size 413 MB, throughput 0.983729
Reading from 31500: heap size 414 MB, throughput 0.98403
Reading from 31499: heap size 1220 MB, throughput 0.954418
Numeric result:
Recommendation: 2 clients, utility 1.07115:
    h1: 1537.47 MB (U(h) = 0.294319*h^0.168742)
    h2: 2923.53 MB (U(h) = 0.0815585*h^0.320786)
Recommendation: 2 clients, utility 1.07115:
    h1: 1537.72 MB (U(h) = 0.294319*h^0.168742)
    h2: 2923.28 MB (U(h) = 0.0815585*h^0.320786)
Reading from 31500: heap size 416 MB, throughput 0.982749
Reading from 31499: heap size 1225 MB, throughput 0.954868
Reading from 31500: heap size 416 MB, throughput 0.983555
Reading from 31500: heap size 417 MB, throughput 0.982947
Reading from 31500: heap size 419 MB, throughput 0.971
Reading from 31500: heap size 419 MB, throughput 0.941938
Reading from 31500: heap size 421 MB, throughput 0.909254
Reading from 31499: heap size 1232 MB, throughput 0.902397
Reading from 31500: heap size 428 MB, throughput 0.977415
Reading from 31500: heap size 430 MB, throughput 0.983555
Reading from 31499: heap size 1314 MB, throughput 0.972334
Reading from 31500: heap size 434 MB, throughput 0.987051
Numeric result:
Recommendation: 2 clients, utility 1.22583:
    h1: 1397.6 MB (U(h) = 0.279591*h^0.180673)
    h2: 3063.4 MB (U(h) = 0.049332*h^0.395987)
Recommendation: 2 clients, utility 1.22583:
    h1: 1397.67 MB (U(h) = 0.279591*h^0.180673)
    h2: 3063.33 MB (U(h) = 0.049332*h^0.395987)
Reading from 31499: heap size 1315 MB, throughput 0.979139
Reading from 31500: heap size 435 MB, throughput 0.987736
Reading from 31499: heap size 1325 MB, throughput 0.981194
Reading from 31500: heap size 434 MB, throughput 0.986706
Reading from 31500: heap size 437 MB, throughput 0.985322
Reading from 31499: heap size 1335 MB, throughput 0.983616
Reading from 31500: heap size 435 MB, throughput 0.985174
Reading from 31499: heap size 1336 MB, throughput 0.98028
Reading from 31500: heap size 437 MB, throughput 0.984498
Numeric result:
Recommendation: 2 clients, utility 1.35102:
    h1: 1272.81 MB (U(h) = 0.275641*h^0.183944)
    h2: 3188.19 MB (U(h) = 0.0319815*h^0.460767)
Recommendation: 2 clients, utility 1.35102:
    h1: 1272.78 MB (U(h) = 0.275641*h^0.183944)
    h2: 3188.22 MB (U(h) = 0.0319815*h^0.460767)
Reading from 31499: heap size 1330 MB, throughput 0.977576
Reading from 31500: heap size 438 MB, throughput 0.984604
Reading from 31499: heap size 1231 MB, throughput 0.975404
Reading from 31500: heap size 439 MB, throughput 0.985077
Reading from 31500: heap size 441 MB, throughput 0.980161
Reading from 31500: heap size 442 MB, throughput 0.967685
Reading from 31500: heap size 445 MB, throughput 0.947355
Reading from 31499: heap size 1318 MB, throughput 0.973999
Reading from 31500: heap size 447 MB, throughput 0.977822
Reading from 31500: heap size 454 MB, throughput 0.985203
Reading from 31499: heap size 1253 MB, throughput 0.970991
Reading from 31500: heap size 454 MB, throughput 0.988693
Numeric result:
Recommendation: 2 clients, utility 1.47757:
    h1: 1227.95 MB (U(h) = 0.26474*h^0.19316)
    h2: 3233.05 MB (U(h) = 0.0231853*h^0.508549)
Recommendation: 2 clients, utility 1.47757:
    h1: 1227.98 MB (U(h) = 0.26474*h^0.19316)
    h2: 3233.02 MB (U(h) = 0.0231853*h^0.508549)
Reading from 31499: heap size 1321 MB, throughput 0.967245
Reading from 31500: heap size 456 MB, throughput 0.988344
Reading from 31499: heap size 1323 MB, throughput 0.967405
Reading from 31500: heap size 457 MB, throughput 0.988471
Reading from 31499: heap size 1326 MB, throughput 0.964249
Reading from 31500: heap size 455 MB, throughput 0.987173
Reading from 31500: heap size 457 MB, throughput 0.985565
Reading from 31499: heap size 1329 MB, throughput 0.962677
Reading from 31500: heap size 457 MB, throughput 0.985242
Numeric result:
Recommendation: 2 clients, utility 2.06821:
    h1: 1485.17 MB (U(h) = 0.131971*h^0.316056)
    h2: 2975.83 MB (U(h) = 0.00983932*h^0.633259)
Recommendation: 2 clients, utility 2.06821:
    h1: 1485.2 MB (U(h) = 0.131971*h^0.316056)
    h2: 2975.8 MB (U(h) = 0.00983932*h^0.633259)
Reading from 31499: heap size 1334 MB, throughput 0.959945
Reading from 31500: heap size 458 MB, throughput 0.988711
Reading from 31500: heap size 459 MB, throughput 0.984551
Reading from 31500: heap size 460 MB, throughput 0.973609
Reading from 31499: heap size 1341 MB, throughput 0.955846
Reading from 31500: heap size 462 MB, throughput 0.964761
Reading from 31500: heap size 464 MB, throughput 0.984841
Reading from 31499: heap size 1347 MB, throughput 0.952068
Reading from 31500: heap size 469 MB, throughput 0.989063
Reading from 31499: heap size 1357 MB, throughput 0.953657
Numeric result:
Recommendation: 2 clients, utility 2.8428:
    h1: 1724.5 MB (U(h) = 0.0595676*h^0.454714)
    h2: 2736.5 MB (U(h) = 0.00533147*h^0.721557)
Recommendation: 2 clients, utility 2.8428:
    h1: 1724.5 MB (U(h) = 0.0595676*h^0.454714)
    h2: 2736.5 MB (U(h) = 0.00533147*h^0.721557)
Reading from 31500: heap size 470 MB, throughput 0.989848
Reading from 31499: heap size 1366 MB, throughput 0.951076
Reading from 31500: heap size 469 MB, throughput 0.989174
Reading from 31499: heap size 1372 MB, throughput 0.950505
Reading from 31500: heap size 471 MB, throughput 0.988668
Reading from 31499: heap size 1382 MB, throughput 0.950143
Reading from 31500: heap size 471 MB, throughput 0.988588
Reading from 31500: heap size 472 MB, throughput 0.98684
Numeric result:
Recommendation: 2 clients, utility 3.17115:
    h1: 1627.49 MB (U(h) = 0.0551218*h^0.467936)
    h2: 2833.51 MB (U(h) = 0.00278305*h^0.814694)
Recommendation: 2 clients, utility 3.17115:
    h1: 1627.49 MB (U(h) = 0.0551218*h^0.467936)
    h2: 2833.51 MB (U(h) = 0.00278305*h^0.814694)
Reading from 31499: heap size 1385 MB, throughput 0.949376
Reading from 31500: heap size 473 MB, throughput 0.990201
Reading from 31499: heap size 1396 MB, throughput 0.951613
Reading from 31500: heap size 474 MB, throughput 0.984593
Reading from 31500: heap size 473 MB, throughput 0.975794
Reading from 31500: heap size 475 MB, throughput 0.968102
Reading from 31500: heap size 482 MB, throughput 0.987893
Reading from 31500: heap size 482 MB, throughput 0.989964
Numeric result:
Recommendation: 2 clients, utility 3.31362:
    h1: 1719.69 MB (U(h) = 0.0444085*h^0.504546)
    h2: 2741.31 MB (U(h) = 0.0029878*h^0.804272)
Recommendation: 2 clients, utility 3.31362:
    h1: 1719.7 MB (U(h) = 0.0444085*h^0.504546)
    h2: 2741.3 MB (U(h) = 0.0029878*h^0.804272)
Reading from 31500: heap size 483 MB, throughput 0.99002
Reading from 31500: heap size 485 MB, throughput 0.989646
Reading from 31500: heap size 483 MB, throughput 0.988815
Reading from 31500: heap size 485 MB, throughput 0.988072
Reading from 31500: heap size 487 MB, throughput 0.984347
Numeric result:
Recommendation: 2 clients, utility 3.76852:
    h1: 1883.34 MB (U(h) = 0.0270869*h^0.587649)
    h2: 2577.66 MB (U(h) = 0.0029878*h^0.804272)
Recommendation: 2 clients, utility 3.76852:
    h1: 1883.37 MB (U(h) = 0.0270869*h^0.587649)
    h2: 2577.63 MB (U(h) = 0.0029878*h^0.804272)
Reading from 31500: heap size 487 MB, throughput 0.985876
Reading from 31500: heap size 490 MB, throughput 0.976641
Reading from 31500: heap size 491 MB, throughput 0.965959
Reading from 31500: heap size 498 MB, throughput 0.986631
Reading from 31500: heap size 498 MB, throughput 0.990083
Reading from 31500: heap size 500 MB, throughput 0.9908
Numeric result:
Recommendation: 2 clients, utility 4.05632:
    h1: 1970.59 MB (U(h) = 0.0201626*h^0.636409)
    h2: 2490.41 MB (U(h) = 0.0029878*h^0.804272)
Recommendation: 2 clients, utility 4.05632:
    h1: 1970.61 MB (U(h) = 0.0201626*h^0.636409)
    h2: 2490.39 MB (U(h) = 0.0029878*h^0.804272)
Reading from 31499: heap size 1397 MB, throughput 0.991763
Reading from 31500: heap size 502 MB, throughput 0.989691
Reading from 31500: heap size 501 MB, throughput 0.989503
Reading from 31500: heap size 503 MB, throughput 0.988439
Numeric result:
Recommendation: 2 clients, utility 4.86707:
    h1: 2140.65 MB (U(h) = 0.0102861*h^0.747898)
    h2: 2320.35 MB (U(h) = 0.0028559*h^0.810674)
Recommendation: 2 clients, utility 4.86707:
    h1: 2140.66 MB (U(h) = 0.0102861*h^0.747898)
    h2: 2320.34 MB (U(h) = 0.0028559*h^0.810674)
Reading from 31499: heap size 1394 MB, throughput 0.966742
Reading from 31500: heap size 505 MB, throughput 0.990705
Reading from 31499: heap size 1450 MB, throughput 0.989038
Reading from 31500: heap size 506 MB, throughput 0.984831
Reading from 31500: heap size 504 MB, throughput 0.976676
Reading from 31499: heap size 1482 MB, throughput 0.985375
Reading from 31499: heap size 1491 MB, throughput 0.976937
Reading from 31499: heap size 1485 MB, throughput 0.966006
Reading from 31499: heap size 1489 MB, throughput 0.954904
Reading from 31499: heap size 1482 MB, throughput 0.938385
Reading from 31500: heap size 507 MB, throughput 0.983837
Reading from 31499: heap size 1489 MB, throughput 0.975462
Reading from 31500: heap size 514 MB, throughput 0.989549
Reading from 31499: heap size 1494 MB, throughput 0.988374
Reading from 31500: heap size 514 MB, throughput 0.99066
Numeric result:
Recommendation: 2 clients, utility 4.06258:
    h1: 2048.99 MB (U(h) = 0.0173363*h^0.660402)
    h2: 2412.01 MB (U(h) = 0.00357656*h^0.777391)
Recommendation: 2 clients, utility 4.06258:
    h1: 2049.01 MB (U(h) = 0.0173363*h^0.660402)
    h2: 2411.99 MB (U(h) = 0.00357656*h^0.777391)
Reading from 31499: heap size 1500 MB, throughput 0.987804
Reading from 31500: heap size 513 MB, throughput 0.991462
Reading from 31499: heap size 1504 MB, throughput 0.986093
Reading from 31500: heap size 516 MB, throughput 0.990692
Reading from 31499: heap size 1509 MB, throughput 0.984048
Reading from 31500: heap size 516 MB, throughput 0.989565
Numeric result:
Recommendation: 2 clients, utility 3.1054:
    h1: 1829.92 MB (U(h) = 0.0436469*h^0.508075)
    h2: 2631.08 MB (U(h) = 0.00496768*h^0.730513)
Recommendation: 2 clients, utility 3.1054:
    h1: 1829.93 MB (U(h) = 0.0436469*h^0.508075)
    h2: 2631.07 MB (U(h) = 0.00496768*h^0.730513)
Reading from 31499: heap size 1494 MB, throughput 0.982244
Reading from 31500: heap size 517 MB, throughput 0.990739
Reading from 31500: heap size 517 MB, throughput 0.986367
Reading from 31500: heap size 519 MB, throughput 0.976979
Reading from 31500: heap size 523 MB, throughput 0.984638
Reading from 31499: heap size 1362 MB, throughput 0.979642
Reading from 31500: heap size 525 MB, throughput 0.989043
Reading from 31499: heap size 1479 MB, throughput 0.979247
Reading from 31500: heap size 528 MB, throughput 0.98988
Numeric result:
Recommendation: 2 clients, utility 3.12258:
    h1: 1724.62 MB (U(h) = 0.0485865*h^0.489678)
    h2: 2736.38 MB (U(h) = 0.00356943*h^0.776934)
Recommendation: 2 clients, utility 3.12258:
    h1: 1724.64 MB (U(h) = 0.0485865*h^0.489678)
    h2: 2736.36 MB (U(h) = 0.00356943*h^0.776934)
Reading from 31499: heap size 1491 MB, throughput 0.976273
Reading from 31500: heap size 529 MB, throughput 0.989983
Reading from 31499: heap size 1485 MB, throughput 0.974361
Reading from 31500: heap size 527 MB, throughput 0.988618
Reading from 31499: heap size 1488 MB, throughput 0.976839
Reading from 31500: heap size 529 MB, throughput 0.988923
Reading from 31500: heap size 531 MB, throughput 0.989315
Reading from 31500: heap size 532 MB, throughput 0.983382
Numeric result:
Recommendation: 2 clients, utility 3.28997:
    h1: 1747.19 MB (U(h) = 0.0410366*h^0.516714)
    h2: 2713.81 MB (U(h) = 0.00297124*h^0.80258)
Recommendation: 2 clients, utility 3.28997:
    h1: 1747.19 MB (U(h) = 0.0410366*h^0.516714)
    h2: 2713.81 MB (U(h) = 0.00297124*h^0.80258)
Reading from 31499: heap size 1493 MB, throughput 0.9758
Reading from 31500: heap size 532 MB, throughput 0.983067
Reading from 31500: heap size 534 MB, throughput 0.989545
Reading from 31499: heap size 1497 MB, throughput 0.97292
Reading from 31500: heap size 538 MB, throughput 0.991376
Reading from 31499: heap size 1503 MB, throughput 0.970268
Reading from 31500: heap size 539 MB, throughput 0.990673
Numeric result:
Recommendation: 2 clients, utility 3.22231:
    h1: 1701.47 MB (U(h) = 0.0451659*h^0.500582)
    h2: 2759.53 MB (U(h) = 0.0027705*h^0.811869)
Recommendation: 2 clients, utility 3.22231:
    h1: 1701.47 MB (U(h) = 0.0451659*h^0.500582)
    h2: 2759.53 MB (U(h) = 0.0027705*h^0.811869)
Reading from 31499: heap size 1512 MB, throughput 0.964845
Reading from 31500: heap size 536 MB, throughput 0.990364
Reading from 31499: heap size 1521 MB, throughput 0.963121
Reading from 31500: heap size 539 MB, throughput 0.989984
Reading from 31499: heap size 1532 MB, throughput 0.960319
Reading from 31500: heap size 540 MB, throughput 0.99195
Reading from 31500: heap size 541 MB, throughput 0.986008
Reading from 31500: heap size 541 MB, throughput 0.982635
Numeric result:
Recommendation: 2 clients, utility 2.40375:
    h1: 1555.68 MB (U(h) = 0.105942*h^0.361382)
    h2: 2905.32 MB (U(h) = 0.00732637*h^0.674934)
Recommendation: 2 clients, utility 2.40375:
    h1: 1555.63 MB (U(h) = 0.105942*h^0.361382)
    h2: 2905.37 MB (U(h) = 0.00732637*h^0.674934)
Reading from 31499: heap size 1546 MB, throughput 0.96062
Reading from 31500: heap size 544 MB, throughput 0.990194
Reading from 31499: heap size 1555 MB, throughput 0.959287
Reading from 31500: heap size 545 MB, throughput 0.990731
Reading from 31499: heap size 1568 MB, throughput 0.960047
Reading from 31500: heap size 547 MB, throughput 0.989627
Numeric result:
Recommendation: 2 clients, utility 1.84264:
    h1: 1615.26 MB (U(h) = 0.181671*h^0.273495)
    h2: 2845.74 MB (U(h) = 0.029136*h^0.481816)
Recommendation: 2 clients, utility 1.84264:
    h1: 1615.31 MB (U(h) = 0.181671*h^0.273495)
    h2: 2845.69 MB (U(h) = 0.029136*h^0.481816)
Reading from 31499: heap size 1573 MB, throughput 0.959612
Reading from 31500: heap size 547 MB, throughput 0.990818
Reading from 31500: heap size 548 MB, throughput 0.984883
Reading from 31499: heap size 1587 MB, throughput 0.955507
Reading from 31500: heap size 550 MB, throughput 0.989202
Reading from 31500: heap size 551 MB, throughput 0.983391
Numeric result:
Recommendation: 2 clients, utility 1.74015:
    h1: 1606.91 MB (U(h) = 0.208013*h^0.251289)
    h2: 2854.09 MB (U(h) = 0.0375468*h^0.446334)
Recommendation: 2 clients, utility 1.74015:
    h1: 1606.89 MB (U(h) = 0.208013*h^0.251289)
    h2: 2854.11 MB (U(h) = 0.0375468*h^0.446334)
Reading from 31499: heap size 1590 MB, throughput 0.959451
Reading from 31500: heap size 551 MB, throughput 0.983931
Reading from 31499: heap size 1604 MB, throughput 0.958003
Reading from 31500: heap size 554 MB, throughput 0.984958
Reading from 31500: heap size 562 MB, throughput 0.993744
Reading from 31499: heap size 1605 MB, throughput 0.958424
Numeric result:
Recommendation: 2 clients, utility 1.61221:
    h1: 1642.72 MB (U(h) = 0.242759*h^0.226137)
    h2: 2818.28 MB (U(h) = 0.0570983*h^0.387963)
Recommendation: 2 clients, utility 1.61221:
    h1: 1642.72 MB (U(h) = 0.242759*h^0.226137)
    h2: 2818.28 MB (U(h) = 0.0570983*h^0.387963)
Reading from 31500: heap size 564 MB, throughput 0.994159
Reading from 31499: heap size 1618 MB, throughput 0.946794
Reading from 31500: heap size 568 MB, throughput 0.993485
Reading from 31499: heap size 1724 MB, throughput 0.975535
Reading from 31500: heap size 569 MB, throughput 0.992132
Numeric result:
Recommendation: 2 clients, utility 1.4063:
    h1: 886.172 MB (U(h) = 0.547814*h^0.0943951)
    h2: 3574.83 MB (U(h) = 0.0599581*h^0.38088)
Recommendation: 2 clients, utility 1.4063:
    h1: 886.005 MB (U(h) = 0.547814*h^0.0943951)
    h2: 3575 MB (U(h) = 0.0599581*h^0.38088)
Reading from 31500: heap size 568 MB, throughput 0.990229
Reading from 31500: heap size 570 MB, throughput 0.982179
Client 31500 died
Clients: 1
Reading from 31499: heap size 1726 MB, throughput 0.983652
Reading from 31499: heap size 1738 MB, throughput 0.986582
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 31499: heap size 1750 MB, throughput 0.995696
Reading from 31499: heap size 1752 MB, throughput 0.993804
Reading from 31499: heap size 1741 MB, throughput 0.987363
Recommendation: one client; give it all the memory
Reading from 31499: heap size 1747 MB, throughput 0.972284
Reading from 31499: heap size 1771 MB, throughput 0.954233
Reading from 31499: heap size 1790 MB, throughput 0.936172
Client 31499 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
