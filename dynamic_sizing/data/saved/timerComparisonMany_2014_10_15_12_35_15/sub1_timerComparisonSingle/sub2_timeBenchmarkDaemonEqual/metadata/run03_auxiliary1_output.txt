economemd
    total memory: 2974 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub1_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 27754: heap size 9 MB, throughput 0.990627
Clients: 1
Client 27754 has a minimum heap size of 276 MB
Reading from 27752: heap size 9 MB, throughput 0.987096
Clients: 2
Client 27752 has a minimum heap size of 1211 MB
Reading from 27754: heap size 9 MB, throughput 0.980118
Reading from 27752: heap size 9 MB, throughput 0.974516
Reading from 27754: heap size 9 MB, throughput 0.954648
Reading from 27754: heap size 9 MB, throughput 0.947463
Reading from 27752: heap size 11 MB, throughput 0.974264
Reading from 27754: heap size 11 MB, throughput 0.9858
Reading from 27752: heap size 11 MB, throughput 0.974251
Reading from 27754: heap size 11 MB, throughput 0.971053
Reading from 27752: heap size 15 MB, throughput 0.864877
Reading from 27754: heap size 17 MB, throughput 0.949529
Reading from 27752: heap size 18 MB, throughput 0.985174
Reading from 27754: heap size 17 MB, throughput 0.581015
Reading from 27752: heap size 23 MB, throughput 0.919781
Reading from 27754: heap size 30 MB, throughput 0.825941
Reading from 27752: heap size 28 MB, throughput 0.388269
Reading from 27754: heap size 31 MB, throughput 0.925175
Reading from 27752: heap size 37 MB, throughput 0.949761
Reading from 27752: heap size 38 MB, throughput 0.912039
Reading from 27752: heap size 39 MB, throughput 0.81067
Reading from 27752: heap size 41 MB, throughput 0.864598
Reading from 27754: heap size 35 MB, throughput 0.353815
Reading from 27754: heap size 44 MB, throughput 0.875844
Reading from 27752: heap size 43 MB, throughput 0.164434
Reading from 27752: heap size 58 MB, throughput 0.626063
Reading from 27754: heap size 49 MB, throughput 0.321555
Reading from 27752: heap size 62 MB, throughput 0.890985
Reading from 27754: heap size 63 MB, throughput 0.837128
Reading from 27752: heap size 63 MB, throughput 0.800388
Reading from 27754: heap size 69 MB, throughput 0.866338
Reading from 27752: heap size 67 MB, throughput 0.182818
Reading from 27754: heap size 70 MB, throughput 0.259131
Reading from 27752: heap size 87 MB, throughput 0.796788
Reading from 27754: heap size 93 MB, throughput 0.802569
Reading from 27752: heap size 91 MB, throughput 0.807497
Reading from 27754: heap size 95 MB, throughput 0.782871
Reading from 27752: heap size 94 MB, throughput 0.687341
Reading from 27754: heap size 97 MB, throughput 0.700494
Reading from 27752: heap size 99 MB, throughput 0.699169
Reading from 27754: heap size 101 MB, throughput 0.649634
Reading from 27752: heap size 102 MB, throughput 0.397403
Reading from 27754: heap size 104 MB, throughput 0.622366
Reading from 27754: heap size 107 MB, throughput 0.119622
Reading from 27752: heap size 106 MB, throughput 0.105594
Reading from 27752: heap size 134 MB, throughput 0.584912
Reading from 27754: heap size 143 MB, throughput 0.60602
Reading from 27752: heap size 142 MB, throughput 0.852162
Reading from 27754: heap size 146 MB, throughput 0.731132
Reading from 27752: heap size 143 MB, throughput 0.728577
Reading from 27754: heap size 149 MB, throughput 0.766344
Reading from 27752: heap size 145 MB, throughput 0.731147
Reading from 27754: heap size 155 MB, throughput 0.673283
Reading from 27752: heap size 150 MB, throughput 0.697719
Reading from 27754: heap size 158 MB, throughput 0.670602
Reading from 27752: heap size 152 MB, throughput 0.50406
Reading from 27754: heap size 166 MB, throughput 0.568305
Reading from 27754: heap size 169 MB, throughput 0.451041
Reading from 27752: heap size 160 MB, throughput 0.120501
Reading from 27754: heap size 178 MB, throughput 0.127754
Reading from 27752: heap size 193 MB, throughput 0.473826
Reading from 27754: heap size 216 MB, throughput 0.552829
Reading from 27752: heap size 200 MB, throughput 0.518991
Reading from 27754: heap size 220 MB, throughput 0.0869753
Reading from 27752: heap size 204 MB, throughput 0.122234
Reading from 27754: heap size 249 MB, throughput 0.814021
Reading from 27752: heap size 237 MB, throughput 0.356125
Reading from 27752: heap size 236 MB, throughput 0.566407
Reading from 27752: heap size 239 MB, throughput 0.647963
Reading from 27754: heap size 253 MB, throughput 0.868036
Reading from 27752: heap size 240 MB, throughput 0.586113
Reading from 27754: heap size 246 MB, throughput 0.737898
Reading from 27754: heap size 210 MB, throughput 0.497467
Reading from 27752: heap size 243 MB, throughput 0.587604
Reading from 27754: heap size 243 MB, throughput 0.678452
Reading from 27752: heap size 246 MB, throughput 0.414787
Reading from 27754: heap size 246 MB, throughput 0.81919
Reading from 27754: heap size 242 MB, throughput 0.525063
Reading from 27754: heap size 245 MB, throughput 0.739989
Reading from 27752: heap size 252 MB, throughput 0.419635
Reading from 27754: heap size 243 MB, throughput 0.596686
Reading from 27754: heap size 245 MB, throughput 0.748541
Reading from 27754: heap size 248 MB, throughput 0.515571
Reading from 27754: heap size 249 MB, throughput 0.480991
Reading from 27752: heap size 257 MB, throughput 0.106739
Reading from 27754: heap size 250 MB, throughput 0.688333
Reading from 27754: heap size 252 MB, throughput 0.509313
Reading from 27752: heap size 298 MB, throughput 0.47281
Reading from 27754: heap size 253 MB, throughput 0.763608
Reading from 27752: heap size 304 MB, throughput 0.593562
Reading from 27752: heap size 305 MB, throughput 0.644346
Reading from 27752: heap size 308 MB, throughput 0.624575
Reading from 27752: heap size 310 MB, throughput 0.092999
Reading from 27754: heap size 254 MB, throughput 0.362045
Reading from 27752: heap size 355 MB, throughput 0.489373
Reading from 27754: heap size 297 MB, throughput 0.509886
Reading from 27754: heap size 298 MB, throughput 0.826462
Reading from 27752: heap size 357 MB, throughput 0.649008
Reading from 27754: heap size 304 MB, throughput 0.871489
Reading from 27754: heap size 304 MB, throughput 0.577538
Reading from 27752: heap size 358 MB, throughput 0.612348
Reading from 27752: heap size 361 MB, throughput 0.452467
Reading from 27754: heap size 309 MB, throughput 0.890169
Reading from 27752: heap size 362 MB, throughput 0.477925
Reading from 27754: heap size 309 MB, throughput 0.924187
Reading from 27754: heap size 310 MB, throughput 0.668988
Reading from 27752: heap size 370 MB, throughput 0.535476
Reading from 27754: heap size 312 MB, throughput 0.605372
Reading from 27752: heap size 375 MB, throughput 0.53606
Reading from 27754: heap size 318 MB, throughput 0.710725
Reading from 27754: heap size 321 MB, throughput 0.504349
Reading from 27754: heap size 328 MB, throughput 0.770273
Reading from 27754: heap size 330 MB, throughput 0.517278
Reading from 27752: heap size 381 MB, throughput 0.0913396
Equal recommendation: 1487 MB each
Reading from 27752: heap size 428 MB, throughput 0.331898
Reading from 27752: heap size 433 MB, throughput 0.436062
Reading from 27754: heap size 338 MB, throughput 0.973067
Reading from 27752: heap size 434 MB, throughput 0.0913408
Reading from 27752: heap size 485 MB, throughput 0.344876
Reading from 27752: heap size 478 MB, throughput 0.544134
Reading from 27752: heap size 483 MB, throughput 0.525176
Reading from 27752: heap size 480 MB, throughput 0.555507
Reading from 27754: heap size 339 MB, throughput 0.954236
Reading from 27752: heap size 482 MB, throughput 0.111102
Reading from 27752: heap size 539 MB, throughput 0.463
Reading from 27752: heap size 540 MB, throughput 0.61659
Reading from 27752: heap size 541 MB, throughput 0.510475
Reading from 27752: heap size 543 MB, throughput 0.568672
Reading from 27754: heap size 339 MB, throughput 0.968947
Reading from 27752: heap size 547 MB, throughput 0.591968
Reading from 27752: heap size 557 MB, throughput 0.60062
Reading from 27752: heap size 563 MB, throughput 0.550082
Reading from 27752: heap size 574 MB, throughput 0.528425
Reading from 27754: heap size 342 MB, throughput 0.963001
Reading from 27752: heap size 579 MB, throughput 0.0840208
Reading from 27752: heap size 641 MB, throughput 0.443403
Reading from 27752: heap size 651 MB, throughput 0.67424
Reading from 27754: heap size 346 MB, throughput 0.951031
Reading from 27752: heap size 655 MB, throughput 0.858309
Reading from 27752: heap size 654 MB, throughput 0.785388
Reading from 27752: heap size 667 MB, throughput 0.76773
Reading from 27754: heap size 347 MB, throughput 0.974477
Reading from 27752: heap size 670 MB, throughput 0.794051
Reading from 27754: heap size 346 MB, throughput 0.961116
Equal recommendation: 1487 MB each
Reading from 27752: heap size 684 MB, throughput 0.0324929
Reading from 27752: heap size 757 MB, throughput 0.31721
Reading from 27754: heap size 349 MB, throughput 0.971
Reading from 27752: heap size 668 MB, throughput 0.511798
Reading from 27752: heap size 743 MB, throughput 0.504266
Reading from 27752: heap size 647 MB, throughput 0.046607
Reading from 27752: heap size 813 MB, throughput 0.261984
Reading from 27754: heap size 346 MB, throughput 0.957934
Reading from 27752: heap size 817 MB, throughput 0.400359
Reading from 27752: heap size 819 MB, throughput 0.483558
Reading from 27752: heap size 819 MB, throughput 0.406888
Reading from 27752: heap size 824 MB, throughput 0.81566
Reading from 27754: heap size 348 MB, throughput 0.953151
Reading from 27752: heap size 824 MB, throughput 0.139372
Reading from 27752: heap size 898 MB, throughput 0.410538
Reading from 27752: heap size 901 MB, throughput 0.656295
Reading from 27752: heap size 904 MB, throughput 0.765856
Reading from 27752: heap size 906 MB, throughput 0.822478
Reading from 27752: heap size 913 MB, throughput 0.925002
Reading from 27752: heap size 913 MB, throughput 0.937877
Reading from 27754: heap size 351 MB, throughput 0.680006
Reading from 27752: heap size 904 MB, throughput 0.93554
Reading from 27752: heap size 710 MB, throughput 0.795005
Reading from 27752: heap size 892 MB, throughput 0.934169
Reading from 27752: heap size 718 MB, throughput 0.852496
Reading from 27752: heap size 877 MB, throughput 0.914943
Reading from 27752: heap size 735 MB, throughput 0.853373
Reading from 27754: heap size 393 MB, throughput 0.974714
Reading from 27752: heap size 865 MB, throughput 0.777749
Reading from 27752: heap size 768 MB, throughput 0.742084
Reading from 27752: heap size 854 MB, throughput 0.736292
Reading from 27752: heap size 759 MB, throughput 0.777519
Reading from 27752: heap size 839 MB, throughput 0.784119
Reading from 27752: heap size 764 MB, throughput 0.773909
Reading from 27752: heap size 831 MB, throughput 0.797511
Reading from 27754: heap size 394 MB, throughput 0.976856
Reading from 27752: heap size 769 MB, throughput 0.769841
Reading from 27752: heap size 827 MB, throughput 0.806551
Reading from 27752: heap size 832 MB, throughput 0.794987
Reading from 27752: heap size 826 MB, throughput 0.820192
Reading from 27754: heap size 396 MB, throughput 0.986083
Reading from 27752: heap size 830 MB, throughput 0.214868
Equal recommendation: 1487 MB each
Reading from 27752: heap size 892 MB, throughput 0.983749
Reading from 27754: heap size 398 MB, throughput 0.983129
Reading from 27754: heap size 398 MB, throughput 0.90213
Reading from 27752: heap size 897 MB, throughput 0.919152
Reading from 27754: heap size 393 MB, throughput 0.829526
Reading from 27752: heap size 894 MB, throughput 0.772925
Reading from 27754: heap size 398 MB, throughput 0.822463
Reading from 27754: heap size 404 MB, throughput 0.7134
Reading from 27752: heap size 897 MB, throughput 0.749654
Reading from 27752: heap size 904 MB, throughput 0.789268
Reading from 27754: heap size 407 MB, throughput 0.943749
Reading from 27752: heap size 905 MB, throughput 0.683396
Reading from 27752: heap size 913 MB, throughput 0.756006
Reading from 27752: heap size 913 MB, throughput 0.787554
Reading from 27752: heap size 920 MB, throughput 0.812265
Reading from 27752: heap size 921 MB, throughput 0.769882
Reading from 27752: heap size 927 MB, throughput 0.844068
Reading from 27752: heap size 928 MB, throughput 0.912385
Reading from 27752: heap size 930 MB, throughput 0.863837
Reading from 27754: heap size 413 MB, throughput 0.988587
Reading from 27752: heap size 933 MB, throughput 0.851562
Reading from 27752: heap size 938 MB, throughput 0.747241
Reading from 27754: heap size 415 MB, throughput 0.986209
Reading from 27752: heap size 939 MB, throughput 0.0558199
Reading from 27752: heap size 1064 MB, throughput 0.499428
Reading from 27752: heap size 1068 MB, throughput 0.803468
Reading from 27752: heap size 1069 MB, throughput 0.824836
Reading from 27752: heap size 1072 MB, throughput 0.759308
Reading from 27752: heap size 1074 MB, throughput 0.762006
Reading from 27754: heap size 414 MB, throughput 0.981716
Reading from 27752: heap size 1077 MB, throughput 0.766193
Reading from 27752: heap size 1085 MB, throughput 0.757387
Reading from 27752: heap size 1087 MB, throughput 0.762827
Reading from 27752: heap size 1102 MB, throughput 0.766719
Reading from 27754: heap size 417 MB, throughput 0.977004
Reading from 27752: heap size 1102 MB, throughput 0.958165
Equal recommendation: 1487 MB each
Reading from 27754: heap size 414 MB, throughput 0.975578
Reading from 27752: heap size 1121 MB, throughput 0.963793
Reading from 27754: heap size 417 MB, throughput 0.982513
Reading from 27752: heap size 1123 MB, throughput 0.961923
Reading from 27754: heap size 417 MB, throughput 0.980494
Reading from 27752: heap size 1130 MB, throughput 0.957514
Reading from 27754: heap size 418 MB, throughput 0.981127
Reading from 27752: heap size 1134 MB, throughput 0.955144
Reading from 27754: heap size 420 MB, throughput 0.98343
Reading from 27752: heap size 1131 MB, throughput 0.956817
Reading from 27754: heap size 420 MB, throughput 0.980248
Equal recommendation: 1487 MB each
Reading from 27752: heap size 1136 MB, throughput 0.955444
Reading from 27754: heap size 422 MB, throughput 0.980087
Reading from 27754: heap size 422 MB, throughput 0.979456
Reading from 27754: heap size 426 MB, throughput 0.859436
Reading from 27754: heap size 428 MB, throughput 0.80765
Reading from 27754: heap size 434 MB, throughput 0.835462
Reading from 27752: heap size 1141 MB, throughput 0.968256
Reading from 27754: heap size 438 MB, throughput 0.982727
Reading from 27752: heap size 1142 MB, throughput 0.960901
Reading from 27754: heap size 444 MB, throughput 0.987695
Reading from 27752: heap size 1149 MB, throughput 0.953535
Reading from 27754: heap size 447 MB, throughput 0.98413
Reading from 27752: heap size 1151 MB, throughput 0.950899
Reading from 27754: heap size 447 MB, throughput 0.98692
Equal recommendation: 1487 MB each
Reading from 27752: heap size 1157 MB, throughput 0.95772
Reading from 27754: heap size 449 MB, throughput 0.984427
Reading from 27752: heap size 1161 MB, throughput 0.957261
Reading from 27754: heap size 448 MB, throughput 0.984946
Reading from 27752: heap size 1167 MB, throughput 0.956959
Reading from 27754: heap size 450 MB, throughput 0.981455
Reading from 27752: heap size 1172 MB, throughput 0.955693
Reading from 27754: heap size 448 MB, throughput 0.981334
Reading from 27752: heap size 1178 MB, throughput 0.954381
Reading from 27754: heap size 450 MB, throughput 0.980792
Equal recommendation: 1487 MB each
Reading from 27752: heap size 1184 MB, throughput 0.953241
Reading from 27754: heap size 452 MB, throughput 0.986395
Reading from 27754: heap size 452 MB, throughput 0.961451
Reading from 27754: heap size 452 MB, throughput 0.867577
Reading from 27754: heap size 453 MB, throughput 0.877077
Reading from 27752: heap size 1192 MB, throughput 0.953771
Reading from 27754: heap size 459 MB, throughput 0.98553
Reading from 27752: heap size 1196 MB, throughput 0.954345
Reading from 27754: heap size 459 MB, throughput 0.989495
Reading from 27752: heap size 1203 MB, throughput 0.953616
Reading from 27754: heap size 465 MB, throughput 0.988567
Reading from 27752: heap size 1206 MB, throughput 0.953148
Reading from 27754: heap size 466 MB, throughput 0.989371
Equal recommendation: 1487 MB each
Reading from 27752: heap size 1213 MB, throughput 0.95379
Reading from 27754: heap size 467 MB, throughput 0.987764
Reading from 27752: heap size 1214 MB, throughput 0.958002
Reading from 27754: heap size 469 MB, throughput 0.987121
Reading from 27752: heap size 1221 MB, throughput 0.955221
Reading from 27754: heap size 466 MB, throughput 0.98568
Reading from 27752: heap size 1222 MB, throughput 0.964575
Reading from 27754: heap size 469 MB, throughput 0.985597
Equal recommendation: 1487 MB each
Reading from 27752: heap size 1229 MB, throughput 0.955124
Reading from 27754: heap size 470 MB, throughput 0.992798
Reading from 27754: heap size 471 MB, throughput 0.922288
Reading from 27754: heap size 472 MB, throughput 0.879004
Reading from 27754: heap size 474 MB, throughput 0.963196
Reading from 27754: heap size 481 MB, throughput 0.986232
Reading from 27752: heap size 1229 MB, throughput 0.528168
Reading from 27754: heap size 482 MB, throughput 0.9911
Reading from 27752: heap size 1340 MB, throughput 0.956731
Reading from 27754: heap size 484 MB, throughput 0.98997
Reading from 27752: heap size 1340 MB, throughput 0.981352
Equal recommendation: 1487 MB each
Reading from 27754: heap size 485 MB, throughput 0.990247
Reading from 27752: heap size 1352 MB, throughput 0.978793
Reading from 27752: heap size 1356 MB, throughput 0.982023
Reading from 27754: heap size 483 MB, throughput 0.988776
Reading from 27754: heap size 485 MB, throughput 0.986542
Reading from 27752: heap size 1358 MB, throughput 0.975112
Reading from 27754: heap size 487 MB, throughput 0.985424
Reading from 27752: heap size 1361 MB, throughput 0.974421
Equal recommendation: 1487 MB each
Reading from 27754: heap size 487 MB, throughput 0.990147
Reading from 27754: heap size 491 MB, throughput 0.913784
Reading from 27754: heap size 491 MB, throughput 0.888656
Reading from 27752: heap size 1351 MB, throughput 0.972542
Reading from 27754: heap size 495 MB, throughput 0.990364
Reading from 27752: heap size 1271 MB, throughput 0.970899
Reading from 27754: heap size 497 MB, throughput 0.992199
Reading from 27752: heap size 1343 MB, throughput 0.975824
Reading from 27754: heap size 499 MB, throughput 0.991501
Equal recommendation: 1487 MB each
Reading from 27752: heap size 1349 MB, throughput 0.965742
Reading from 27754: heap size 501 MB, throughput 0.990064
Reading from 27752: heap size 1351 MB, throughput 0.963988
Reading from 27754: heap size 499 MB, throughput 0.987579
Reading from 27752: heap size 1351 MB, throughput 0.9636
Reading from 27754: heap size 502 MB, throughput 0.988787
Reading from 27752: heap size 1356 MB, throughput 0.961684
Reading from 27754: heap size 504 MB, throughput 0.986326
Equal recommendation: 1487 MB each
Reading from 27754: heap size 504 MB, throughput 0.988103
Reading from 27754: heap size 508 MB, throughput 0.825873
Reading from 27752: heap size 1360 MB, throughput 0.955566
Reading from 27754: heap size 508 MB, throughput 0.925738
Reading from 27752: heap size 1366 MB, throughput 0.963078
Reading from 27754: heap size 517 MB, throughput 0.993036
Reading from 27754: heap size 517 MB, throughput 0.992246
Equal recommendation: 1487 MB each
Reading from 27754: heap size 519 MB, throughput 0.988296
Reading from 27754: heap size 522 MB, throughput 0.988173
Reading from 27754: heap size 521 MB, throughput 0.988463
Reading from 27754: heap size 523 MB, throughput 0.985082
Equal recommendation: 1487 MB each
Reading from 27754: heap size 526 MB, throughput 0.982886
Reading from 27752: heap size 1373 MB, throughput 0.987468
Reading from 27754: heap size 527 MB, throughput 0.893846
Reading from 27754: heap size 527 MB, throughput 0.971121
Reading from 27754: heap size 529 MB, throughput 0.993742
Reading from 27754: heap size 533 MB, throughput 0.992035
Equal recommendation: 1487 MB each
Reading from 27754: heap size 534 MB, throughput 0.99186
Reading from 27754: heap size 532 MB, throughput 0.985269
Reading from 27752: heap size 1349 MB, throughput 0.941528
Reading from 27754: heap size 535 MB, throughput 0.990862
Equal recommendation: 1487 MB each
Reading from 27754: heap size 537 MB, throughput 0.993418
Reading from 27754: heap size 538 MB, throughput 0.969317
Reading from 27754: heap size 536 MB, throughput 0.916961
Reading from 27752: heap size 1424 MB, throughput 0.99297
Reading from 27752: heap size 1449 MB, throughput 0.95641
Reading from 27752: heap size 1450 MB, throughput 0.891774
Reading from 27752: heap size 1451 MB, throughput 0.838729
Reading from 27752: heap size 1454 MB, throughput 0.840348
Reading from 27752: heap size 1455 MB, throughput 0.847487
Reading from 27754: heap size 539 MB, throughput 0.991088
Reading from 27752: heap size 1459 MB, throughput 0.860971
Reading from 27752: heap size 1465 MB, throughput 0.922292
Reading from 27752: heap size 1470 MB, throughput 0.981857
Reading from 27754: heap size 545 MB, throughput 0.992944
Equal recommendation: 1487 MB each
Reading from 27752: heap size 1492 MB, throughput 0.986035
Reading from 27754: heap size 546 MB, throughput 0.99167
Reading from 27752: heap size 1337 MB, throughput 0.980589
Reading from 27754: heap size 546 MB, throughput 0.989257
Reading from 27752: heap size 1492 MB, throughput 0.979803
Reading from 27754: heap size 548 MB, throughput 0.989837
Reading from 27752: heap size 1356 MB, throughput 0.979791
Equal recommendation: 1487 MB each
Reading from 27754: heap size 549 MB, throughput 0.899033
Reading from 27752: heap size 1479 MB, throughput 0.977003
Reading from 27754: heap size 582 MB, throughput 0.983484
Reading from 27754: heap size 589 MB, throughput 0.933271
Reading from 27754: heap size 589 MB, throughput 0.993047
Reading from 27752: heap size 1475 MB, throughput 0.975392
Reading from 27754: heap size 593 MB, throughput 0.993623
Reading from 27752: heap size 1467 MB, throughput 0.979774
Equal recommendation: 1487 MB each
Reading from 27752: heap size 1472 MB, throughput 0.979719
Reading from 27754: heap size 594 MB, throughput 0.993235
Reading from 27752: heap size 1464 MB, throughput 0.967966
Reading from 27754: heap size 596 MB, throughput 0.991249
Reading from 27752: heap size 1469 MB, throughput 0.970921
Reading from 27754: heap size 598 MB, throughput 0.989432
Equal recommendation: 1487 MB each
Reading from 27752: heap size 1475 MB, throughput 0.970464
Reading from 27754: heap size 599 MB, throughput 0.994236
Reading from 27754: heap size 599 MB, throughput 0.918244
Reading from 27754: heap size 598 MB, throughput 0.959896
Reading from 27752: heap size 1478 MB, throughput 0.962771
Reading from 27754: heap size 601 MB, throughput 0.993845
Reading from 27752: heap size 1486 MB, throughput 0.96722
Equal recommendation: 1487 MB each
Reading from 27754: heap size 603 MB, throughput 0.992484
Reading from 27752: heap size 1494 MB, throughput 0.967145
Reading from 27754: heap size 606 MB, throughput 0.991922
Reading from 27752: heap size 1499 MB, throughput 0.960089
Reading from 27754: heap size 608 MB, throughput 0.990693
Reading from 27752: heap size 1505 MB, throughput 0.957442
Equal recommendation: 1487 MB each
Reading from 27754: heap size 609 MB, throughput 0.988183
Reading from 27752: heap size 1507 MB, throughput 0.956851
Reading from 27754: heap size 612 MB, throughput 0.98739
Reading from 27754: heap size 615 MB, throughput 0.904069
Reading from 27752: heap size 1510 MB, throughput 0.956335
Reading from 27754: heap size 620 MB, throughput 0.99133
Reading from 27752: heap size 1512 MB, throughput 0.966145
Reading from 27754: heap size 622 MB, throughput 0.991679
Equal recommendation: 1487 MB each
Reading from 27752: heap size 1453 MB, throughput 0.953897
Reading from 27754: heap size 623 MB, throughput 0.992015
Reading from 27752: heap size 1513 MB, throughput 0.958779
Reading from 27754: heap size 626 MB, throughput 0.990009
Reading from 27752: heap size 1446 MB, throughput 0.95236
Equal recommendation: 1487 MB each
Reading from 27754: heap size 627 MB, throughput 0.9899
Reading from 27752: heap size 1512 MB, throughput 0.955096
Reading from 27754: heap size 628 MB, throughput 0.985018
Reading from 27754: heap size 632 MB, throughput 0.930282
Reading from 27752: heap size 1441 MB, throughput 0.950454
Reading from 27754: heap size 634 MB, throughput 0.991799
Reading from 27752: heap size 1509 MB, throughput 0.682196
Equal recommendation: 1487 MB each
Reading from 27754: heap size 640 MB, throughput 0.993982
Reading from 27752: heap size 1527 MB, throughput 0.991759
Reading from 27754: heap size 641 MB, throughput 0.991247
Reading from 27752: heap size 1577 MB, throughput 0.985878
Reading from 27752: heap size 1588 MB, throughput 0.984862
Reading from 27754: heap size 641 MB, throughput 0.991581
Equal recommendation: 1487 MB each
Reading from 27752: heap size 1591 MB, throughput 0.981234
Reading from 27754: heap size 643 MB, throughput 0.993855
Reading from 27752: heap size 1389 MB, throughput 0.979438
Reading from 27754: heap size 644 MB, throughput 0.97519
Client 27754 died
Clients: 1
Reading from 27752: heap size 1591 MB, throughput 0.98234
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 27752: heap size 1409 MB, throughput 0.993676
Recommendation: one client; give it all the memory
Reading from 27752: heap size 1570 MB, throughput 0.991138
Reading from 27752: heap size 1576 MB, throughput 0.973291
Reading from 27752: heap size 1603 MB, throughput 0.832416
Reading from 27752: heap size 1604 MB, throughput 0.71834
Reading from 27752: heap size 1627 MB, throughput 0.764337
Recommendation: one client; give it all the memory
Reading from 27752: heap size 1650 MB, throughput 0.745408
Reading from 27752: heap size 1670 MB, throughput 0.765699
Reading from 27752: heap size 1676 MB, throughput 0.8127
Client 27752 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
