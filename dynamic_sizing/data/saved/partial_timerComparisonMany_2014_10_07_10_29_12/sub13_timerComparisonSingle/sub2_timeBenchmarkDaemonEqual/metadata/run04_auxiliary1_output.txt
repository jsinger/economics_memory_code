economemd
    total memory: 6175 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub13_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 29663: heap size 9 MB, throughput 0.982121
Clients: 1
Client 29663 has a minimum heap size of 12 MB
Reading from 29662: heap size 9 MB, throughput 0.988611
Clients: 2
Client 29662 has a minimum heap size of 1223 MB
Reading from 29663: heap size 9 MB, throughput 0.98539
Reading from 29662: heap size 9 MB, throughput 0.965196
Reading from 29663: heap size 11 MB, throughput 0.983821
Reading from 29662: heap size 11 MB, throughput 0.975942
Reading from 29663: heap size 11 MB, throughput 0.970537
Reading from 29662: heap size 11 MB, throughput 0.986168
Reading from 29663: heap size 15 MB, throughput 0.984011
Reading from 29662: heap size 15 MB, throughput 0.863087
Reading from 29663: heap size 15 MB, throughput 0.987575
Reading from 29662: heap size 19 MB, throughput 0.964238
Reading from 29662: heap size 24 MB, throughput 0.914254
Reading from 29662: heap size 29 MB, throughput 0.881374
Reading from 29663: heap size 24 MB, throughput 0.928277
Reading from 29662: heap size 30 MB, throughput 0.472124
Reading from 29662: heap size 40 MB, throughput 0.839252
Reading from 29662: heap size 46 MB, throughput 0.83928
Reading from 29663: heap size 30 MB, throughput 0.990354
Reading from 29662: heap size 47 MB, throughput 0.32576
Reading from 29662: heap size 64 MB, throughput 0.851006
Reading from 29662: heap size 67 MB, throughput 0.816303
Reading from 29662: heap size 70 MB, throughput 0.835488
Reading from 29663: heap size 36 MB, throughput 0.97964
Reading from 29662: heap size 72 MB, throughput 0.676003
Reading from 29663: heap size 41 MB, throughput 0.987951
Reading from 29662: heap size 77 MB, throughput 0.171078
Reading from 29662: heap size 105 MB, throughput 0.692688
Reading from 29662: heap size 110 MB, throughput 0.685312
Reading from 29663: heap size 43 MB, throughput 0.992973
Reading from 29662: heap size 111 MB, throughput 0.761942
Reading from 29663: heap size 46 MB, throughput 0.984826
Reading from 29663: heap size 52 MB, throughput 0.986772
Reading from 29662: heap size 118 MB, throughput 0.154358
Reading from 29663: heap size 52 MB, throughput 0.975891
Reading from 29662: heap size 143 MB, throughput 0.703602
Reading from 29662: heap size 152 MB, throughput 0.799878
Reading from 29663: heap size 59 MB, throughput 0.956677
Reading from 29662: heap size 153 MB, throughput 0.619545
Reading from 29662: heap size 158 MB, throughput 0.750952
Reading from 29663: heap size 59 MB, throughput 0.976988
Reading from 29663: heap size 70 MB, throughput 0.74953
Reading from 29662: heap size 164 MB, throughput 0.18929
Reading from 29663: heap size 74 MB, throughput 0.989403
Reading from 29662: heap size 198 MB, throughput 0.495478
Reading from 29662: heap size 204 MB, throughput 0.658153
Reading from 29662: heap size 208 MB, throughput 0.679441
Reading from 29663: heap size 82 MB, throughput 0.99721
Reading from 29662: heap size 214 MB, throughput 0.141825
Reading from 29662: heap size 252 MB, throughput 0.582228
Reading from 29662: heap size 257 MB, throughput 0.734064
Reading from 29662: heap size 260 MB, throughput 0.716603
Reading from 29662: heap size 264 MB, throughput 0.693589
Reading from 29663: heap size 83 MB, throughput 0.997239
Reading from 29662: heap size 267 MB, throughput 0.584031
Reading from 29662: heap size 275 MB, throughput 0.618626
Reading from 29663: heap size 89 MB, throughput 0.995715
Reading from 29662: heap size 281 MB, throughput 0.128366
Reading from 29662: heap size 325 MB, throughput 0.514756
Reading from 29662: heap size 334 MB, throughput 0.655821
Reading from 29662: heap size 337 MB, throughput 0.600804
Reading from 29663: heap size 90 MB, throughput 0.996931
Reading from 29662: heap size 340 MB, throughput 0.57427
Reading from 29662: heap size 347 MB, throughput 0.605227
Reading from 29662: heap size 352 MB, throughput 0.515191
Reading from 29663: heap size 96 MB, throughput 0.997104
Reading from 29662: heap size 357 MB, throughput 0.102561
Reading from 29662: heap size 415 MB, throughput 0.503637
Reading from 29662: heap size 415 MB, throughput 0.666908
Reading from 29663: heap size 97 MB, throughput 0.996881
Reading from 29662: heap size 411 MB, throughput 0.664916
Reading from 29663: heap size 102 MB, throughput 0.996932
Reading from 29662: heap size 414 MB, throughput 0.101253
Reading from 29662: heap size 466 MB, throughput 0.499864
Reading from 29662: heap size 466 MB, throughput 0.594119
Reading from 29663: heap size 102 MB, throughput 0.99717
Reading from 29662: heap size 466 MB, throughput 0.645055
Equal recommendation: 3087 MB each
Reading from 29662: heap size 466 MB, throughput 0.614105
Reading from 29662: heap size 468 MB, throughput 0.541262
Reading from 29663: heap size 104 MB, throughput 0.995251
Reading from 29662: heap size 475 MB, throughput 0.507146
Reading from 29662: heap size 481 MB, throughput 0.463051
Reading from 29662: heap size 491 MB, throughput 0.464776
Reading from 29663: heap size 105 MB, throughput 0.992915
Reading from 29663: heap size 109 MB, throughput 0.995418
Reading from 29662: heap size 496 MB, throughput 0.0962419
Reading from 29662: heap size 558 MB, throughput 0.355468
Reading from 29662: heap size 568 MB, throughput 0.474229
Reading from 29663: heap size 109 MB, throughput 0.9935
Reading from 29663: heap size 112 MB, throughput 0.995235
Reading from 29662: heap size 568 MB, throughput 0.105918
Reading from 29662: heap size 638 MB, throughput 0.462383
Reading from 29662: heap size 553 MB, throughput 0.729069
Reading from 29663: heap size 112 MB, throughput 0.996546
Reading from 29662: heap size 629 MB, throughput 0.652537
Reading from 29662: heap size 634 MB, throughput 0.659412
Reading from 29662: heap size 635 MB, throughput 0.616128
Reading from 29663: heap size 116 MB, throughput 0.996774
Reading from 29662: heap size 640 MB, throughput 0.792179
Reading from 29663: heap size 116 MB, throughput 0.996966
Reading from 29663: heap size 118 MB, throughput 0.996371
Reading from 29662: heap size 646 MB, throughput 0.337786
Reading from 29663: heap size 119 MB, throughput 0.997443
Reading from 29662: heap size 722 MB, throughput 0.810125
Reading from 29663: heap size 122 MB, throughput 0.997695
Reading from 29662: heap size 731 MB, throughput 0.823336
Reading from 29662: heap size 740 MB, throughput 0.48913
Reading from 29662: heap size 749 MB, throughput 0.668321
Reading from 29662: heap size 749 MB, throughput 0.420008
Reading from 29663: heap size 122 MB, throughput 0.997626
Reading from 29663: heap size 124 MB, throughput 0.998008
Reading from 29663: heap size 124 MB, throughput 0.997123
Equal recommendation: 3087 MB each
Reading from 29662: heap size 745 MB, throughput 0.034683
Reading from 29662: heap size 819 MB, throughput 0.36009
Reading from 29663: heap size 126 MB, throughput 0.997393
Reading from 29662: heap size 812 MB, throughput 0.871181
Reading from 29662: heap size 816 MB, throughput 0.579808
Reading from 29662: heap size 812 MB, throughput 0.593998
Reading from 29663: heap size 126 MB, throughput 0.99756
Reading from 29663: heap size 128 MB, throughput 0.997697
Reading from 29662: heap size 815 MB, throughput 0.179577
Reading from 29662: heap size 898 MB, throughput 0.654698
Reading from 29663: heap size 128 MB, throughput 0.99813
Reading from 29662: heap size 898 MB, throughput 0.835782
Reading from 29663: heap size 129 MB, throughput 0.991408
Reading from 29662: heap size 907 MB, throughput 0.828653
Reading from 29662: heap size 909 MB, throughput 0.791857
Reading from 29663: heap size 130 MB, throughput 0.991517
Reading from 29663: heap size 134 MB, throughput 0.968117
Reading from 29663: heap size 134 MB, throughput 0.994592
Reading from 29662: heap size 914 MB, throughput 0.260799
Reading from 29662: heap size 1006 MB, throughput 0.479221
Reading from 29662: heap size 1008 MB, throughput 0.763692
Reading from 29663: heap size 141 MB, throughput 0.996558
Reading from 29662: heap size 1008 MB, throughput 0.788976
Reading from 29662: heap size 1009 MB, throughput 0.769325
Reading from 29662: heap size 1011 MB, throughput 0.793097
Reading from 29663: heap size 142 MB, throughput 0.997069
Reading from 29662: heap size 1005 MB, throughput 0.822878
Reading from 29662: heap size 1010 MB, throughput 0.797223
Reading from 29662: heap size 1003 MB, throughput 0.818763
Reading from 29662: heap size 1008 MB, throughput 0.796335
Reading from 29663: heap size 148 MB, throughput 0.997539
Reading from 29663: heap size 148 MB, throughput 0.997172
Reading from 29662: heap size 1000 MB, throughput 0.966515
Reading from 29663: heap size 152 MB, throughput 0.997558
Equal recommendation: 3087 MB each
Reading from 29662: heap size 1005 MB, throughput 0.906066
Reading from 29662: heap size 1004 MB, throughput 0.792376
Reading from 29662: heap size 1010 MB, throughput 0.799324
Reading from 29663: heap size 153 MB, throughput 0.996949
Reading from 29662: heap size 1020 MB, throughput 0.756658
Reading from 29662: heap size 1022 MB, throughput 0.79583
Reading from 29662: heap size 1014 MB, throughput 0.814143
Reading from 29662: heap size 1023 MB, throughput 0.851234
Reading from 29663: heap size 157 MB, throughput 0.997524
Reading from 29662: heap size 1009 MB, throughput 0.855214
Reading from 29662: heap size 1016 MB, throughput 0.860518
Reading from 29662: heap size 1011 MB, throughput 0.915491
Reading from 29663: heap size 157 MB, throughput 0.996802
Reading from 29662: heap size 1015 MB, throughput 0.795311
Reading from 29662: heap size 1015 MB, throughput 0.705777
Reading from 29662: heap size 1021 MB, throughput 0.674295
Reading from 29662: heap size 1034 MB, throughput 0.661678
Reading from 29663: heap size 160 MB, throughput 0.995277
Reading from 29663: heap size 160 MB, throughput 0.994577
Reading from 29662: heap size 1045 MB, throughput 0.101448
Reading from 29662: heap size 1165 MB, throughput 0.596279
Reading from 29662: heap size 1171 MB, throughput 0.757389
Reading from 29662: heap size 1177 MB, throughput 0.718245
Reading from 29663: heap size 164 MB, throughput 0.997413
Reading from 29662: heap size 1180 MB, throughput 0.697298
Reading from 29662: heap size 1183 MB, throughput 0.753443
Reading from 29663: heap size 164 MB, throughput 0.990615
Reading from 29662: heap size 1188 MB, throughput 0.936801
Reading from 29663: heap size 167 MB, throughput 0.99749
Reading from 29663: heap size 167 MB, throughput 0.997651
Equal recommendation: 3087 MB each
Reading from 29663: heap size 171 MB, throughput 0.998576
Reading from 29662: heap size 1183 MB, throughput 0.981706
Reading from 29663: heap size 171 MB, throughput 0.998334
Reading from 29663: heap size 174 MB, throughput 0.996116
Reading from 29663: heap size 174 MB, throughput 0.992941
Reading from 29663: heap size 178 MB, throughput 0.997142
Reading from 29662: heap size 1192 MB, throughput 0.975268
Reading from 29663: heap size 178 MB, throughput 0.996908
Reading from 29663: heap size 182 MB, throughput 0.997544
Reading from 29663: heap size 182 MB, throughput 0.997233
Reading from 29662: heap size 1187 MB, throughput 0.978113
Reading from 29663: heap size 185 MB, throughput 0.994694
Reading from 29663: heap size 185 MB, throughput 0.997132
Equal recommendation: 3087 MB each
Reading from 29663: heap size 188 MB, throughput 0.997328
Reading from 29662: heap size 1194 MB, throughput 0.97868
Reading from 29663: heap size 189 MB, throughput 0.997196
Reading from 29663: heap size 192 MB, throughput 0.997329
Reading from 29663: heap size 192 MB, throughput 0.997202
Reading from 29662: heap size 1202 MB, throughput 0.976085
Reading from 29663: heap size 195 MB, throughput 0.998139
Reading from 29663: heap size 195 MB, throughput 0.996752
Reading from 29663: heap size 198 MB, throughput 0.996597
Reading from 29662: heap size 1203 MB, throughput 0.97858
Reading from 29663: heap size 198 MB, throughput 0.997586
Equal recommendation: 3087 MB each
Reading from 29663: heap size 202 MB, throughput 0.900247
Reading from 29663: heap size 205 MB, throughput 0.993607
Reading from 29663: heap size 209 MB, throughput 0.997575
Reading from 29662: heap size 1204 MB, throughput 0.982214
Reading from 29663: heap size 209 MB, throughput 0.99768
Reading from 29663: heap size 214 MB, throughput 0.998453
Reading from 29663: heap size 214 MB, throughput 0.998112
Reading from 29662: heap size 1207 MB, throughput 0.979016
Reading from 29663: heap size 218 MB, throughput 0.998298
Reading from 29663: heap size 219 MB, throughput 0.997569
Reading from 29662: heap size 1198 MB, throughput 0.977836
Equal recommendation: 3087 MB each
Reading from 29663: heap size 223 MB, throughput 0.997757
Reading from 29663: heap size 223 MB, throughput 0.997687
Reading from 29663: heap size 227 MB, throughput 0.998121
Reading from 29662: heap size 1204 MB, throughput 0.978513
Reading from 29663: heap size 227 MB, throughput 0.997874
Reading from 29663: heap size 231 MB, throughput 0.998213
Reading from 29663: heap size 231 MB, throughput 0.997808
Reading from 29662: heap size 1202 MB, throughput 0.972297
Reading from 29663: heap size 235 MB, throughput 0.992775
Reading from 29663: heap size 235 MB, throughput 0.990478
Equal recommendation: 3087 MB each
Reading from 29663: heap size 242 MB, throughput 0.997857
Reading from 29662: heap size 1203 MB, throughput 0.978507
Reading from 29663: heap size 243 MB, throughput 0.997961
Reading from 29663: heap size 248 MB, throughput 0.998182
Reading from 29663: heap size 249 MB, throughput 0.997833
Reading from 29662: heap size 1207 MB, throughput 0.977284
Reading from 29663: heap size 255 MB, throughput 0.998078
Reading from 29663: heap size 255 MB, throughput 0.997441
Reading from 29662: heap size 1207 MB, throughput 0.978828
Equal recommendation: 3087 MB each
Reading from 29663: heap size 259 MB, throughput 0.997712
Reading from 29663: heap size 260 MB, throughput 0.997871
Reading from 29663: heap size 266 MB, throughput 0.997761
Reading from 29662: heap size 1211 MB, throughput 0.979295
Reading from 29663: heap size 266 MB, throughput 0.996474
Reading from 29663: heap size 271 MB, throughput 0.992078
Reading from 29663: heap size 271 MB, throughput 0.995803
Reading from 29662: heap size 1214 MB, throughput 0.975265
Reading from 29663: heap size 281 MB, throughput 0.998338
Equal recommendation: 3087 MB each
Reading from 29663: heap size 281 MB, throughput 0.997475
Reading from 29663: heap size 288 MB, throughput 0.997702
Reading from 29662: heap size 1219 MB, throughput 0.717428
Reading from 29663: heap size 288 MB, throughput 0.998285
Reading from 29663: heap size 294 MB, throughput 0.998513
Reading from 29662: heap size 1293 MB, throughput 0.996003
Reading from 29663: heap size 294 MB, throughput 0.99784
Equal recommendation: 3087 MB each
Reading from 29663: heap size 300 MB, throughput 0.998244
Reading from 29662: heap size 1290 MB, throughput 0.99379
Reading from 29663: heap size 300 MB, throughput 0.998645
Reading from 29663: heap size 304 MB, throughput 0.991765
Reading from 29662: heap size 1299 MB, throughput 0.993067
Reading from 29663: heap size 305 MB, throughput 0.996346
Reading from 29663: heap size 314 MB, throughput 0.998063
Reading from 29662: heap size 1306 MB, throughput 0.989213
Reading from 29663: heap size 314 MB, throughput 0.99824
Equal recommendation: 3087 MB each
Reading from 29663: heap size 320 MB, throughput 0.998373
Reading from 29662: heap size 1308 MB, throughput 0.990553
Reading from 29663: heap size 321 MB, throughput 0.998071
Reading from 29663: heap size 327 MB, throughput 0.997599
Reading from 29662: heap size 1300 MB, throughput 0.989887
Reading from 29663: heap size 328 MB, throughput 0.998338
Reading from 29663: heap size 333 MB, throughput 0.998431
Equal recommendation: 3087 MB each
Reading from 29663: heap size 334 MB, throughput 0.905534
Reading from 29662: heap size 1201 MB, throughput 0.986782
Reading from 29663: heap size 347 MB, throughput 0.999094
Reading from 29662: heap size 1288 MB, throughput 0.985769
Reading from 29663: heap size 348 MB, throughput 0.998249
Reading from 29663: heap size 356 MB, throughput 0.998896
Reading from 29662: heap size 1219 MB, throughput 0.982125
Reading from 29663: heap size 357 MB, throughput 0.998935
Equal recommendation: 3087 MB each
Reading from 29663: heap size 363 MB, throughput 0.998895
Reading from 29662: heap size 1282 MB, throughput 0.984102
Reading from 29663: heap size 364 MB, throughput 0.998794
Reading from 29663: heap size 370 MB, throughput 0.998761
Reading from 29662: heap size 1286 MB, throughput 0.975469
Reading from 29663: heap size 370 MB, throughput 0.995631
Reading from 29663: heap size 375 MB, throughput 0.997646
Equal recommendation: 3087 MB each
Reading from 29662: heap size 1287 MB, throughput 0.981668
Reading from 29663: heap size 376 MB, throughput 0.998045
Reading from 29663: heap size 385 MB, throughput 0.998507
Reading from 29662: heap size 1289 MB, throughput 0.980417
Reading from 29663: heap size 385 MB, throughput 0.998332
Reading from 29662: heap size 1291 MB, throughput 0.978416
Reading from 29663: heap size 392 MB, throughput 0.998421
Equal recommendation: 3087 MB each
Reading from 29663: heap size 393 MB, throughput 0.996674
Reading from 29662: heap size 1296 MB, throughput 0.978757
Reading from 29663: heap size 401 MB, throughput 0.998695
Reading from 29663: heap size 401 MB, throughput 0.995368
Reading from 29662: heap size 1299 MB, throughput 0.977516
Reading from 29663: heap size 408 MB, throughput 0.998659
Equal recommendation: 3087 MB each
Reading from 29663: heap size 409 MB, throughput 0.998583
Reading from 29662: heap size 1307 MB, throughput 0.976876
Reading from 29663: heap size 416 MB, throughput 0.998187
Reading from 29662: heap size 1313 MB, throughput 0.977017
Reading from 29663: heap size 417 MB, throughput 0.997249
Reading from 29663: heap size 426 MB, throughput 0.998546
Reading from 29662: heap size 1317 MB, throughput 0.975592
Equal recommendation: 3087 MB each
Reading from 29663: heap size 426 MB, throughput 0.99847
Reading from 29663: heap size 434 MB, throughput 0.994858
Reading from 29662: heap size 1322 MB, throughput 0.977205
Reading from 29663: heap size 435 MB, throughput 0.998477
Reading from 29662: heap size 1325 MB, throughput 0.976224
Reading from 29663: heap size 445 MB, throughput 0.998364
Equal recommendation: 3087 MB each
Reading from 29663: heap size 446 MB, throughput 0.998008
Reading from 29662: heap size 1330 MB, throughput 0.979938
Reading from 29663: heap size 455 MB, throughput 0.998078
Reading from 29662: heap size 1330 MB, throughput 0.978811
Reading from 29663: heap size 456 MB, throughput 0.99838
Reading from 29663: heap size 465 MB, throughput 0.996189
Equal recommendation: 3087 MB each
Reading from 29662: heap size 1333 MB, throughput 0.978319
Reading from 29663: heap size 465 MB, throughput 0.997676
Reading from 29663: heap size 476 MB, throughput 0.998348
Reading from 29662: heap size 1334 MB, throughput 0.978389
Reading from 29663: heap size 477 MB, throughput 0.998694
Equal recommendation: 3087 MB each
Reading from 29663: heap size 486 MB, throughput 0.998502
Reading from 29663: heap size 487 MB, throughput 0.998495
Reading from 29663: heap size 496 MB, throughput 0.991996
Reading from 29663: heap size 496 MB, throughput 0.998171
Equal recommendation: 3087 MB each
Reading from 29662: heap size 1335 MB, throughput 0.992601
Reading from 29663: heap size 510 MB, throughput 0.998455
Reading from 29663: heap size 512 MB, throughput 0.998282
Reading from 29663: heap size 525 MB, throughput 0.998591
Equal recommendation: 3087 MB each
Reading from 29663: heap size 525 MB, throughput 0.998641
Reading from 29663: heap size 535 MB, throughput 0.996225
Reading from 29663: heap size 536 MB, throughput 0.998512
Reading from 29662: heap size 1336 MB, throughput 0.99244
Equal recommendation: 3087 MB each
Reading from 29663: heap size 548 MB, throughput 0.998531
Reading from 29663: heap size 549 MB, throughput 0.998407
Reading from 29662: heap size 1331 MB, throughput 0.903915
Reading from 29662: heap size 1382 MB, throughput 0.990675
Reading from 29662: heap size 1405 MB, throughput 0.928613
Reading from 29662: heap size 1280 MB, throughput 0.888976
Reading from 29663: heap size 559 MB, throughput 0.998788
Reading from 29662: heap size 1404 MB, throughput 0.896569
Reading from 29662: heap size 1408 MB, throughput 0.893267
Reading from 29662: heap size 1403 MB, throughput 0.895707
Reading from 29662: heap size 1409 MB, throughput 0.88671
Equal recommendation: 3087 MB each
Reading from 29663: heap size 560 MB, throughput 0.997072
Reading from 29662: heap size 1409 MB, throughput 0.992626
Reading from 29663: heap size 569 MB, throughput 0.998749
Reading from 29662: heap size 1414 MB, throughput 0.995777
Reading from 29663: heap size 570 MB, throughput 0.975638
Equal recommendation: 3087 MB each
Reading from 29662: heap size 1436 MB, throughput 0.994267
Reading from 29663: heap size 580 MB, throughput 0.99936
Reading from 29662: heap size 1439 MB, throughput 0.992354
Reading from 29663: heap size 582 MB, throughput 0.999449
Client 29663 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 29662: heap size 1437 MB, throughput 0.992291
Reading from 29662: heap size 1443 MB, throughput 0.990722
Reading from 29662: heap size 1427 MB, throughput 0.988928
Recommendation: one client; give it all the memory
Reading from 29662: heap size 1329 MB, throughput 0.988707
Reading from 29662: heap size 1417 MB, throughput 0.988091
Recommendation: one client; give it all the memory
Reading from 29662: heap size 1344 MB, throughput 0.98748
Reading from 29662: heap size 1417 MB, throughput 0.987194
Recommendation: one client; give it all the memory
Reading from 29662: heap size 1420 MB, throughput 0.985807
Reading from 29662: heap size 1422 MB, throughput 0.984489
Recommendation: one client; give it all the memory
Reading from 29662: heap size 1426 MB, throughput 0.983285
Reading from 29662: heap size 1429 MB, throughput 0.983399
Recommendation: one client; give it all the memory
Reading from 29662: heap size 1437 MB, throughput 0.980301
Reading from 29662: heap size 1442 MB, throughput 0.981631
Recommendation: one client; give it all the memory
Reading from 29662: heap size 1452 MB, throughput 0.980345
Reading from 29662: heap size 1460 MB, throughput 0.979671
Recommendation: one client; give it all the memory
Reading from 29662: heap size 1466 MB, throughput 0.980471
Reading from 29662: heap size 1474 MB, throughput 0.979342
Recommendation: one client; give it all the memory
Reading from 29662: heap size 1477 MB, throughput 0.981044
Reading from 29662: heap size 1484 MB, throughput 0.979324
Recommendation: one client; give it all the memory
Reading from 29662: heap size 1485 MB, throughput 0.979278
Reading from 29662: heap size 1492 MB, throughput 0.980166
Recommendation: one client; give it all the memory
Reading from 29662: heap size 1492 MB, throughput 0.979985
Recommendation: one client; give it all the memory
Reading from 29662: heap size 1497 MB, throughput 0.761851
Reading from 29662: heap size 1601 MB, throughput 0.996172
Recommendation: one client; give it all the memory
Reading from 29662: heap size 1594 MB, throughput 0.994488
Reading from 29662: heap size 1607 MB, throughput 0.993271
Recommendation: one client; give it all the memory
Reading from 29662: heap size 1618 MB, throughput 0.992504
Reading from 29662: heap size 1619 MB, throughput 0.991309
Recommendation: one client; give it all the memory
Reading from 29662: heap size 1609 MB, throughput 0.990893
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 29662: heap size 1459 MB, throughput 0.990314
Recommendation: one client; give it all the memory
Reading from 29662: heap size 1591 MB, throughput 0.991375
Reading from 29662: heap size 1612 MB, throughput 0.98059
Reading from 29662: heap size 1639 MB, throughput 0.834164
Reading from 29662: heap size 1640 MB, throughput 0.734056
Reading from 29662: heap size 1665 MB, throughput 0.780871
Reading from 29662: heap size 1688 MB, throughput 0.761288
Reading from 29662: heap size 1726 MB, throughput 0.841943
Recommendation: one client; give it all the memory
Client 29662 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
