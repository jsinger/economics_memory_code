economemd
    total memory: 3312 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub24_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
TimerThread: starting
ReadingThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 1035: heap size 9 MB, throughput 0.9851
Clients: 1
Reading from 1036: heap size 9 MB, throughput 0.984924
Clients: 2
Client 1035 has a minimum heap size of 276 MB
Client 1036 has a minimum heap size of 276 MB
Reading from 1035: heap size 9 MB, throughput 0.98321
Reading from 1036: heap size 9 MB, throughput 0.982711
Reading from 1035: heap size 11 MB, throughput 0.972924
Reading from 1036: heap size 11 MB, throughput 0.976744
Reading from 1035: heap size 11 MB, throughput 0.972529
Reading from 1036: heap size 11 MB, throughput 0.979345
Reading from 1036: heap size 15 MB, throughput 0.930034
Reading from 1035: heap size 15 MB, throughput 0.932467
Reading from 1036: heap size 19 MB, throughput 0.929378
Reading from 1035: heap size 19 MB, throughput 0.927721
Reading from 1036: heap size 24 MB, throughput 0.844387
Reading from 1035: heap size 25 MB, throughput 0.83403
Reading from 1035: heap size 29 MB, throughput 0.725217
Reading from 1036: heap size 29 MB, throughput 0.795233
Reading from 1036: heap size 39 MB, throughput 0.763614
Reading from 1035: heap size 33 MB, throughput 0.774547
Reading from 1036: heap size 40 MB, throughput 0.63774
Reading from 1035: heap size 42 MB, throughput 0.330395
Reading from 1036: heap size 44 MB, throughput 0.309744
Reading from 1035: heap size 49 MB, throughput 0.868916
Reading from 1036: heap size 45 MB, throughput 0.621105
Reading from 1036: heap size 62 MB, throughput 0.832991
Reading from 1035: heap size 50 MB, throughput 0.424754
Reading from 1036: heap size 65 MB, throughput 0.816268
Reading from 1035: heap size 68 MB, throughput 0.834268
Reading from 1036: heap size 69 MB, throughput 0.498726
Reading from 1036: heap size 88 MB, throughput 0.732559
Reading from 1035: heap size 71 MB, throughput 0.456583
Reading from 1036: heap size 96 MB, throughput 0.711855
Reading from 1035: heap size 97 MB, throughput 0.830918
Reading from 1035: heap size 98 MB, throughput 0.685243
Reading from 1036: heap size 98 MB, throughput 0.762807
Reading from 1036: heap size 105 MB, throughput 0.738521
Reading from 1035: heap size 103 MB, throughput 0.413755
Reading from 1035: heap size 129 MB, throughput 0.757904
Reading from 1036: heap size 107 MB, throughput 0.407295
Reading from 1035: heap size 136 MB, throughput 0.813335
Reading from 1036: heap size 136 MB, throughput 0.735442
Reading from 1035: heap size 138 MB, throughput 0.768856
Reading from 1036: heap size 140 MB, throughput 0.774956
Reading from 1035: heap size 142 MB, throughput 0.768374
Reading from 1036: heap size 144 MB, throughput 0.746917
Reading from 1035: heap size 149 MB, throughput 0.768511
Reading from 1036: heap size 148 MB, throughput 0.73216
Reading from 1035: heap size 153 MB, throughput 0.76695
Reading from 1036: heap size 154 MB, throughput 0.719795
Reading from 1035: heap size 159 MB, throughput 0.718868
Reading from 1035: heap size 164 MB, throughput 0.657288
Reading from 1036: heap size 161 MB, throughput 0.452645
Reading from 1036: heap size 194 MB, throughput 0.625309
Reading from 1036: heap size 203 MB, throughput 0.545284
Reading from 1035: heap size 173 MB, throughput 0.506832
Reading from 1036: heap size 206 MB, throughput 0.797176
Reading from 1035: heap size 205 MB, throughput 0.815192
Reading from 1035: heap size 215 MB, throughput 0.482934
Reading from 1036: heap size 213 MB, throughput 0.525517
Reading from 1036: heap size 252 MB, throughput 0.819274
Reading from 1035: heap size 253 MB, throughput 0.774219
Reading from 1036: heap size 257 MB, throughput 0.708717
Reading from 1035: heap size 257 MB, throughput 0.721671
Reading from 1035: heap size 262 MB, throughput 0.67561
Reading from 1036: heap size 261 MB, throughput 0.699666
Reading from 1035: heap size 263 MB, throughput 0.656055
Reading from 1036: heap size 262 MB, throughput 0.633992
Reading from 1035: heap size 268 MB, throughput 0.68163
Reading from 1035: heap size 269 MB, throughput 0.655004
Reading from 1035: heap size 262 MB, throughput 0.720267
Reading from 1035: heap size 267 MB, throughput 0.774786
Reading from 1036: heap size 258 MB, throughput 0.37785
Reading from 1035: heap size 265 MB, throughput 0.758806
Reading from 1035: heap size 267 MB, throughput 0.721894
Reading from 1036: heap size 301 MB, throughput 0.631209
Reading from 1036: heap size 301 MB, throughput 0.740334
Reading from 1036: heap size 302 MB, throughput 0.789558
Reading from 1036: heap size 298 MB, throughput 0.787735
Reading from 1036: heap size 302 MB, throughput 0.760473
Reading from 1035: heap size 267 MB, throughput 0.480344
Reading from 1036: heap size 297 MB, throughput 0.799712
Reading from 1035: heap size 306 MB, throughput 0.698527
Reading from 1036: heap size 300 MB, throughput 0.817268
Reading from 1035: heap size 307 MB, throughput 0.895382
Reading from 1036: heap size 298 MB, throughput 0.930894
Reading from 1035: heap size 309 MB, throughput 0.862101
Reading from 1035: heap size 308 MB, throughput 0.835481
Reading from 1036: heap size 300 MB, throughput 0.912061
Reading from 1035: heap size 310 MB, throughput 0.820965
Reading from 1036: heap size 299 MB, throughput 0.893992
Reading from 1035: heap size 313 MB, throughput 0.846561
Reading from 1036: heap size 301 MB, throughput 0.887583
Reading from 1036: heap size 302 MB, throughput 0.848713
Reading from 1035: heap size 314 MB, throughput 0.839269
Reading from 1035: heap size 320 MB, throughput 0.822687
Reading from 1036: heap size 303 MB, throughput 0.893273
Reading from 1035: heap size 320 MB, throughput 0.75581
Reading from 1036: heap size 305 MB, throughput 0.878228
Reading from 1036: heap size 306 MB, throughput 0.841332
Reading from 1036: heap size 309 MB, throughput 0.730652
Reading from 1036: heap size 310 MB, throughput 0.670931
Numeric result:
Recommendation: 2 clients, utility 0.686102:
    h1: 1659.98 MB (U(h) = 0.806521*h^0.001)
    h2: 1652.02 MB (U(h) = 0.838176*h^0.001)
Recommendation: 2 clients, utility 0.686102:
    h1: 1656 MB (U(h) = 0.806521*h^0.001)
    h2: 1656 MB (U(h) = 0.838176*h^0.001)
Reading from 1036: heap size 316 MB, throughput 0.637473
Reading from 1036: heap size 316 MB, throughput 0.651639
Reading from 1035: heap size 326 MB, throughput 0.587882
Reading from 1035: heap size 365 MB, throughput 0.631666
Reading from 1035: heap size 372 MB, throughput 0.68743
Reading from 1035: heap size 372 MB, throughput 0.679827
Reading from 1036: heap size 325 MB, throughput 0.831012
Reading from 1036: heap size 325 MB, throughput 0.951044
Reading from 1035: heap size 379 MB, throughput 0.954317
Reading from 1036: heap size 328 MB, throughput 0.958964
Reading from 1035: heap size 380 MB, throughput 0.959301
Reading from 1035: heap size 380 MB, throughput 0.95894
Reading from 1036: heap size 331 MB, throughput 0.833125
Reading from 1035: heap size 383 MB, throughput 0.964018
Reading from 1036: heap size 372 MB, throughput 0.966215
Reading from 1035: heap size 384 MB, throughput 0.966983
Reading from 1036: heap size 374 MB, throughput 0.973023
Reading from 1035: heap size 386 MB, throughput 0.971073
Numeric result:
Recommendation: 2 clients, utility 0.560074:
    h1: 1998.11 MB (U(h) = 0.729078*h^0.0015002)
    h2: 1313.89 MB (U(h) = 0.754052*h^0.001)
Recommendation: 2 clients, utility 0.560074:
    h1: 1987.3 MB (U(h) = 0.729078*h^0.0015002)
    h2: 1324.7 MB (U(h) = 0.754052*h^0.001)
Reading from 1036: heap size 375 MB, throughput 0.977535
Reading from 1035: heap size 387 MB, throughput 0.97425
Reading from 1036: heap size 378 MB, throughput 0.979432
Reading from 1035: heap size 388 MB, throughput 0.970525
Reading from 1036: heap size 374 MB, throughput 0.975712
Reading from 1035: heap size 392 MB, throughput 0.967887
Reading from 1036: heap size 377 MB, throughput 0.976531
Reading from 1036: heap size 378 MB, throughput 0.97075
Reading from 1035: heap size 393 MB, throughput 0.932688
Reading from 1035: heap size 440 MB, throughput 0.981005
Reading from 1036: heap size 379 MB, throughput 0.974888
Reading from 1035: heap size 441 MB, throughput 0.985606
Reading from 1036: heap size 382 MB, throughput 0.975553
Numeric result:
Recommendation: 2 clients, utility 0.635182:
    h1: 1982.99 MB (U(h) = 0.655168*h^0.028412)
    h2: 1329.01 MB (U(h) = 0.681388*h^0.0190384)
Recommendation: 2 clients, utility 0.635182:
    h1: 1983.14 MB (U(h) = 0.655168*h^0.028412)
    h2: 1328.86 MB (U(h) = 0.681388*h^0.0190384)
Reading from 1036: heap size 383 MB, throughput 0.97461
Reading from 1035: heap size 446 MB, throughput 0.985941
Reading from 1035: heap size 449 MB, throughput 0.982178
Reading from 1035: heap size 443 MB, throughput 0.968339
Reading from 1035: heap size 446 MB, throughput 0.943782
Reading from 1035: heap size 452 MB, throughput 0.9164
Reading from 1035: heap size 454 MB, throughput 0.952649
Reading from 1036: heap size 385 MB, throughput 0.978217
Reading from 1036: heap size 387 MB, throughput 0.968617
Reading from 1036: heap size 383 MB, throughput 0.94649
Reading from 1036: heap size 389 MB, throughput 0.922672
Reading from 1036: heap size 396 MB, throughput 0.893631
Reading from 1035: heap size 459 MB, throughput 0.981338
Reading from 1036: heap size 399 MB, throughput 0.960355
Reading from 1035: heap size 461 MB, throughput 0.985452
Reading from 1036: heap size 406 MB, throughput 0.980381
Reading from 1035: heap size 461 MB, throughput 0.98589
Reading from 1036: heap size 408 MB, throughput 0.984921
Numeric result:
Recommendation: 2 clients, utility 0.743372:
    h1: 1756.43 MB (U(h) = 0.600206*h^0.0498783)
    h2: 1555.57 MB (U(h) = 0.616684*h^0.0441763)
Recommendation: 2 clients, utility 0.743372:
    h1: 1756.39 MB (U(h) = 0.600206*h^0.0498783)
    h2: 1555.61 MB (U(h) = 0.616684*h^0.0441763)
Reading from 1035: heap size 464 MB, throughput 0.985681
Reading from 1036: heap size 408 MB, throughput 0.986441
Reading from 1035: heap size 460 MB, throughput 0.984913
Reading from 1036: heap size 411 MB, throughput 0.987711
Reading from 1035: heap size 463 MB, throughput 0.985861
Reading from 1036: heap size 408 MB, throughput 0.989058
Reading from 1035: heap size 464 MB, throughput 0.984948
Reading from 1036: heap size 411 MB, throughput 0.98603
Reading from 1035: heap size 465 MB, throughput 0.981831
Reading from 1036: heap size 409 MB, throughput 0.98498
Numeric result:
Recommendation: 2 clients, utility 0.783358:
    h1: 1806.93 MB (U(h) = 0.576128*h^0.0598422)
    h2: 1505.07 MB (U(h) = 0.602829*h^0.0498322)
Recommendation: 2 clients, utility 0.783358:
    h1: 1807.14 MB (U(h) = 0.576128*h^0.0598422)
    h2: 1504.86 MB (U(h) = 0.602829*h^0.0498322)
Reading from 1035: heap size 468 MB, throughput 0.98066
Reading from 1036: heap size 411 MB, throughput 0.9845
Reading from 1035: heap size 468 MB, throughput 0.98456
Reading from 1035: heap size 471 MB, throughput 0.977223
Reading from 1035: heap size 471 MB, throughput 0.963134
Reading from 1035: heap size 476 MB, throughput 0.950067
Reading from 1036: heap size 409 MB, throughput 0.984926
Reading from 1035: heap size 476 MB, throughput 0.983429
Reading from 1036: heap size 411 MB, throughput 0.982992
Reading from 1035: heap size 483 MB, throughput 0.988872
Reading from 1036: heap size 412 MB, throughput 0.986095
Reading from 1036: heap size 413 MB, throughput 0.977671
Reading from 1036: heap size 414 MB, throughput 0.964105
Reading from 1036: heap size 415 MB, throughput 0.942465
Reading from 1036: heap size 422 MB, throughput 0.98213
Reading from 1035: heap size 484 MB, throughput 0.988752
Numeric result:
Recommendation: 2 clients, utility 0.838341:
    h1: 1769.56 MB (U(h) = 0.553481*h^0.0695185)
    h2: 1542.44 MB (U(h) = 0.577217*h^0.0605956)
Recommendation: 2 clients, utility 0.838341:
    h1: 1769.56 MB (U(h) = 0.553481*h^0.0695185)
    h2: 1542.44 MB (U(h) = 0.577217*h^0.0605956)
Reading from 1036: heap size 423 MB, throughput 0.988287
Reading from 1035: heap size 487 MB, throughput 0.9892
Reading from 1036: heap size 427 MB, throughput 0.988891
Reading from 1035: heap size 488 MB, throughput 0.985512
Reading from 1036: heap size 429 MB, throughput 0.988597
Reading from 1035: heap size 487 MB, throughput 0.986148
Reading from 1036: heap size 428 MB, throughput 0.989314
Reading from 1035: heap size 489 MB, throughput 0.986643
Reading from 1035: heap size 488 MB, throughput 0.984085
Numeric result:
Recommendation: 2 clients, utility 0.874853:
    h1: 1726.48 MB (U(h) = 0.541894*h^0.0745857)
    h2: 1585.52 MB (U(h) = 0.558946*h^0.0684979)
Recommendation: 2 clients, utility 0.874853:
    h1: 1726.46 MB (U(h) = 0.541894*h^0.0745857)
    h2: 1585.54 MB (U(h) = 0.558946*h^0.0684979)
Reading from 1036: heap size 431 MB, throughput 0.988954
Reading from 1035: heap size 489 MB, throughput 0.987626
Reading from 1035: heap size 492 MB, throughput 0.980493
Reading from 1035: heap size 492 MB, throughput 0.96758
Reading from 1036: heap size 429 MB, throughput 0.98857
Reading from 1035: heap size 497 MB, throughput 0.967582
Reading from 1036: heap size 431 MB, throughput 0.985295
Reading from 1035: heap size 499 MB, throughput 0.986821
Reading from 1036: heap size 431 MB, throughput 0.98441
Reading from 1035: heap size 504 MB, throughput 0.990228
Reading from 1036: heap size 432 MB, throughput 0.988472
Numeric result:
Recommendation: 2 clients, utility 0.900802:
    h1: 1745.31 MB (U(h) = 0.529437*h^0.0801186)
    h2: 1566.69 MB (U(h) = 0.551184*h^0.0719203)
Recommendation: 2 clients, utility 0.900802:
    h1: 1745.29 MB (U(h) = 0.529437*h^0.0801186)
    h2: 1566.71 MB (U(h) = 0.551184*h^0.0719203)
Reading from 1036: heap size 435 MB, throughput 0.982491
Reading from 1036: heap size 435 MB, throughput 0.968281
Reading from 1035: heap size 505 MB, throughput 0.989943
Reading from 1036: heap size 439 MB, throughput 0.955054
Reading from 1036: heap size 441 MB, throughput 0.98477
Reading from 1035: heap size 505 MB, throughput 0.989055
Reading from 1035: heap size 507 MB, throughput 0.988059
Reading from 1036: heap size 446 MB, throughput 0.989917
Reading from 1036: heap size 447 MB, throughput 0.990293
Reading from 1035: heap size 506 MB, throughput 0.885405
Numeric result:
Recommendation: 2 clients, utility 0.929866:
    h1: 1695.62 MB (U(h) = 0.523351*h^0.0828553)
    h2: 1616.38 MB (U(h) = 0.535406*h^0.0789696)
Recommendation: 2 clients, utility 0.929866:
    h1: 1695.76 MB (U(h) = 0.523351*h^0.0828553)
    h2: 1616.24 MB (U(h) = 0.535406*h^0.0789696)
Reading from 1036: heap size 448 MB, throughput 0.987503
Reading from 1035: heap size 508 MB, throughput 0.872848
Reading from 1035: heap size 510 MB, throughput 0.987146
Reading from 1036: heap size 450 MB, throughput 0.982521
Reading from 1035: heap size 511 MB, throughput 0.973213
Reading from 1035: heap size 511 MB, throughput 0.96171
Reading from 1035: heap size 514 MB, throughput 0.982229
Reading from 1036: heap size 449 MB, throughput 0.984917
Reading from 1035: heap size 522 MB, throughput 0.989209
Reading from 1036: heap size 450 MB, throughput 0.986392
Numeric result:
Recommendation: 2 clients, utility 0.954076:
    h1: 1697.46 MB (U(h) = 0.514127*h^0.08704)
    h2: 1614.54 MB (U(h) = 0.527013*h^0.0827823)
Recommendation: 2 clients, utility 0.954076:
    h1: 1697.52 MB (U(h) = 0.514127*h^0.08704)
    h2: 1614.48 MB (U(h) = 0.527013*h^0.0827823)
Reading from 1036: heap size 453 MB, throughput 0.986502
Reading from 1035: heap size 523 MB, throughput 0.99092
Reading from 1036: heap size 454 MB, throughput 0.98852
Reading from 1036: heap size 457 MB, throughput 0.980971
Reading from 1036: heap size 457 MB, throughput 0.964053
Reading from 1035: heap size 524 MB, throughput 0.98773
Reading from 1036: heap size 464 MB, throughput 0.983515
Reading from 1035: heap size 527 MB, throughput 0.988778
Reading from 1036: heap size 465 MB, throughput 0.990504
Reading from 1035: heap size 528 MB, throughput 0.646105
Numeric result:
Recommendation: 2 clients, utility 0.97124:
    h1: 1645.55 MB (U(h) = 0.513984*h^0.0871118)
    h2: 1666.45 MB (U(h) = 0.515158*h^0.0882298)
Recommendation: 2 clients, utility 0.97124:
    h1: 1645.44 MB (U(h) = 0.513984*h^0.0871118)
    h2: 1666.56 MB (U(h) = 0.515158*h^0.0882298)
Reading from 1036: heap size 468 MB, throughput 0.991394
Reading from 1035: heap size 529 MB, throughput 0.581072
Reading from 1035: heap size 530 MB, throughput 0.987151
Reading from 1035: heap size 533 MB, throughput 0.981389
Reading from 1035: heap size 537 MB, throughput 0.980181
Reading from 1036: heap size 470 MB, throughput 0.991314
Reading from 1035: heap size 538 MB, throughput 0.989076
Reading from 1036: heap size 468 MB, throughput 0.990314
Reading from 1036: heap size 471 MB, throughput 0.988625
Reading from 1035: heap size 541 MB, throughput 0.992042
Numeric result:
Recommendation: 2 clients, utility 0.983241:
    h1: 1625.64 MB (U(h) = 0.512024*h^0.0879802)
    h2: 1686.36 MB (U(h) = 0.508657*h^0.0912435)
Recommendation: 2 clients, utility 0.983241:
    h1: 1625.85 MB (U(h) = 0.512024*h^0.0879802)
    h2: 1686.15 MB (U(h) = 0.508657*h^0.0912435)
Reading from 1036: heap size 473 MB, throughput 0.988834
Reading from 1035: heap size 543 MB, throughput 0.991669
Reading from 1035: heap size 541 MB, throughput 0.990569
Reading from 1036: heap size 473 MB, throughput 0.991879
Reading from 1036: heap size 475 MB, throughput 0.988251
Reading from 1036: heap size 476 MB, throughput 0.978263
Reading from 1036: heap size 481 MB, throughput 0.984512
Reading from 1035: heap size 543 MB, throughput 0.990231
Numeric result:
Recommendation: 2 clients, utility 0.996691:
    h1: 1603.75 MB (U(h) = 0.510069*h^0.0888629)
    h2: 1708.25 MB (U(h) = 0.501377*h^0.0946464)
Recommendation: 2 clients, utility 0.996691:
    h1: 1603.81 MB (U(h) = 0.510069*h^0.0888629)
    h2: 1708.19 MB (U(h) = 0.501377*h^0.0946464)
Reading from 1036: heap size 483 MB, throughput 0.987697
Reading from 1035: heap size 545 MB, throughput 0.98882
Reading from 1035: heap size 545 MB, throughput 0.988359
Reading from 1036: heap size 486 MB, throughput 0.990007
Reading from 1035: heap size 547 MB, throughput 0.982429
Reading from 1035: heap size 549 MB, throughput 0.982165
Reading from 1036: heap size 487 MB, throughput 0.990662
Reading from 1035: heap size 556 MB, throughput 0.989586
Reading from 1036: heap size 485 MB, throughput 0.991869
Numeric result:
Recommendation: 2 clients, utility 1.09185:
    h1: 1926.96 MB (U(h) = 0.38873*h^0.136067)
    h2: 1385.04 MB (U(h) = 0.494679*h^0.0977983)
Recommendation: 2 clients, utility 1.09185:
    h1: 1926.98 MB (U(h) = 0.38873*h^0.136067)
    h2: 1385.02 MB (U(h) = 0.494679*h^0.0977983)
Reading from 1035: heap size 556 MB, throughput 0.992724
Reading from 1036: heap size 488 MB, throughput 0.991163
Reading from 1035: heap size 556 MB, throughput 0.99226
Reading from 1036: heap size 490 MB, throughput 0.990178
Reading from 1035: heap size 558 MB, throughput 0.990871
Reading from 1036: heap size 490 MB, throughput 0.991063
Reading from 1036: heap size 493 MB, throughput 0.984262
Reading from 1036: heap size 493 MB, throughput 0.980004
Reading from 1035: heap size 556 MB, throughput 0.990588
Numeric result:
Recommendation: 2 clients, utility 1.28062:
    h1: 1612.02 MB (U(h) = 0.334576*h^0.1617)
    h2: 1699.98 MB (U(h) = 0.326164*h^0.17052)
Recommendation: 2 clients, utility 1.28062:
    h1: 1612.04 MB (U(h) = 0.334576*h^0.1617)
    h2: 1699.96 MB (U(h) = 0.326164*h^0.17052)
Reading from 1036: heap size 501 MB, throughput 0.989254
Reading from 1035: heap size 558 MB, throughput 0.992418
Reading from 1035: heap size 561 MB, throughput 0.987162
Reading from 1035: heap size 561 MB, throughput 0.975649
Reading from 1036: heap size 501 MB, throughput 0.990003
Reading from 1035: heap size 567 MB, throughput 0.986561
Reading from 1036: heap size 502 MB, throughput 0.990739
Reading from 1035: heap size 569 MB, throughput 0.991409
Numeric result:
Recommendation: 2 clients, utility 1.56876:
    h1: 1697.49 MB (U(h) = 0.213729*h^0.237516)
    h2: 1614.51 MB (U(h) = 0.236513*h^0.225903)
Recommendation: 2 clients, utility 1.56876:
    h1: 1697.5 MB (U(h) = 0.213729*h^0.237516)
    h2: 1614.5 MB (U(h) = 0.236513*h^0.225903)
Reading from 1036: heap size 504 MB, throughput 0.991441
Reading from 1035: heap size 571 MB, throughput 0.991605
Reading from 1036: heap size 503 MB, throughput 0.992171
Reading from 1035: heap size 574 MB, throughput 0.99163
Reading from 1036: heap size 505 MB, throughput 0.99114
Reading from 1035: heap size 574 MB, throughput 0.990799
Numeric result:
Recommendation: 2 clients, utility 1.74987:
    h1: 1505.63 MB (U(h) = 0.208344*h^0.241971)
    h2: 1806.37 MB (U(h) = 0.162124*h^0.290303)
Recommendation: 2 clients, utility 1.74987:
    h1: 1505.63 MB (U(h) = 0.208344*h^0.241971)
    h2: 1806.37 MB (U(h) = 0.162124*h^0.290303)
Reading from 1036: heap size 507 MB, throughput 0.99179
Reading from 1036: heap size 508 MB, throughput 0.985005
Reading from 1036: heap size 508 MB, throughput 0.979115
Reading from 1035: heap size 575 MB, throughput 0.992388
Reading from 1035: heap size 575 MB, throughput 0.720902
Reading from 1036: heap size 510 MB, throughput 0.989706
Reading from 1035: heap size 577 MB, throughput 0.981034
Reading from 1035: heap size 582 MB, throughput 0.988985
Reading from 1036: heap size 514 MB, throughput 0.992036
Numeric result:
Recommendation: 2 clients, utility 1.99163:
    h1: 1529.42 MB (U(h) = 0.157185*h^0.288479)
    h2: 1782.58 MB (U(h) = 0.123309*h^0.336241)
Recommendation: 2 clients, utility 1.99163:
    h1: 1529.39 MB (U(h) = 0.157185*h^0.288479)
    h2: 1782.61 MB (U(h) = 0.123309*h^0.336241)
Reading from 1035: heap size 584 MB, throughput 0.991891
Reading from 1036: heap size 515 MB, throughput 0.993127
Reading from 1035: heap size 586 MB, throughput 0.992174
Reading from 1036: heap size 513 MB, throughput 0.992229
Reading from 1035: heap size 588 MB, throughput 0.99208
Reading from 1036: heap size 516 MB, throughput 0.991322
Numeric result:
Recommendation: 2 clients, utility 2.17564:
    h1: 1501.84 MB (U(h) = 0.13614*h^0.312051)
    h2: 1810.16 MB (U(h) = 0.0970701*h^0.376106)
Recommendation: 2 clients, utility 2.17564:
    h1: 1501.86 MB (U(h) = 0.13614*h^0.312051)
    h2: 1810.14 MB (U(h) = 0.0970701*h^0.376106)
Reading from 1035: heap size 588 MB, throughput 0.991035
Reading from 1036: heap size 518 MB, throughput 0.992588
Reading from 1036: heap size 519 MB, throughput 0.989026
Reading from 1036: heap size 517 MB, throughput 0.982056
Reading from 1035: heap size 589 MB, throughput 0.992071
Reading from 1035: heap size 592 MB, throughput 0.987094
Reading from 1035: heap size 592 MB, throughput 0.985969
Reading from 1036: heap size 520 MB, throughput 0.987472
Reading from 1036: heap size 526 MB, throughput 0.987049
Reading from 1035: heap size 600 MB, throughput 0.987801
Numeric result:
Recommendation: 2 clients, utility 2.46866:
    h1: 1346.69 MB (U(h) = 0.13371*h^0.314797)
    h2: 1965.31 MB (U(h) = 0.0586418*h^0.459401)
Recommendation: 2 clients, utility 2.46866:
    h1: 1346.69 MB (U(h) = 0.13371*h^0.314797)
    h2: 1965.31 MB (U(h) = 0.0586418*h^0.459401)
Reading from 1036: heap size 527 MB, throughput 0.988924
Reading from 1035: heap size 600 MB, throughput 0.990838
Reading from 1036: heap size 527 MB, throughput 0.990867
Reading from 1035: heap size 600 MB, throughput 0.991472
Reading from 1035: heap size 602 MB, throughput 0.642898
Reading from 1036: heap size 529 MB, throughput 0.991586
Numeric result:
Recommendation: 2 clients, utility 2.70679:
    h1: 1258 MB (U(h) = 0.129668*h^0.31883)
    h2: 2054 MB (U(h) = 0.0404493*h^0.52057)
Recommendation: 2 clients, utility 2.70679:
    h1: 1258 MB (U(h) = 0.129668*h^0.31883)
    h2: 2054 MB (U(h) = 0.0404493*h^0.52057)
Reading from 1036: heap size 530 MB, throughput 0.986535
Reading from 1035: heap size 605 MB, throughput 0.988553
Reading from 1035: heap size 605 MB, throughput 0.985203
Reading from 1035: heap size 607 MB, throughput 0.98152
Reading from 1036: heap size 531 MB, throughput 0.986473
Reading from 1036: heap size 534 MB, throughput 0.980589
Reading from 1036: heap size 536 MB, throughput 0.986781
Reading from 1035: heap size 609 MB, throughput 0.992222
Numeric result:
Recommendation: 2 clients, utility 2.73431:
    h1: 1445.99 MB (U(h) = 0.0898129*h^0.378381)
    h2: 1866.01 MB (U(h) = 0.0490353*h^0.48832)
Recommendation: 2 clients, utility 2.73431:
    h1: 1445.94 MB (U(h) = 0.0898129*h^0.378381)
    h2: 1866.06 MB (U(h) = 0.0490353*h^0.48832)
Reading from 1035: heap size 612 MB, throughput 0.990138
Reading from 1036: heap size 543 MB, throughput 0.98453
Reading from 1035: heap size 614 MB, throughput 0.990423
Reading from 1036: heap size 565 MB, throughput 0.994376
Reading from 1035: heap size 614 MB, throughput 0.614594
Reading from 1036: heap size 566 MB, throughput 0.994209
Numeric result:
Recommendation: 2 clients, utility 2.72658:
    h1: 1500.65 MB (U(h) = 0.0804274*h^0.395692)
    h2: 1811.35 MB (U(h) = 0.0521498*h^0.477635)
Recommendation: 2 clients, utility 2.72658:
    h1: 1500.62 MB (U(h) = 0.0804274*h^0.395692)
    h2: 1811.38 MB (U(h) = 0.0521498*h^0.477635)
Reading from 1035: heap size 615 MB, throughput 0.99278
Reading from 1035: heap size 617 MB, throughput 0.42766
Reading from 1036: heap size 568 MB, throughput 0.993771
Reading from 1035: heap size 618 MB, throughput 0.98803
Reading from 1036: heap size 568 MB, throughput 0.994703
Reading from 1035: heap size 625 MB, throughput 0.992299
Reading from 1036: heap size 570 MB, throughput 0.990281
Reading from 1036: heap size 567 MB, throughput 0.983186
Numeric result:
Recommendation: 2 clients, utility 2.25631:
    h1: 1405.65 MB (U(h) = 0.135235*h^0.309831)
    h2: 1906.35 MB (U(h) = 0.073908*h^0.420189)
Recommendation: 2 clients, utility 2.25631:
    h1: 1405.66 MB (U(h) = 0.135235*h^0.309831)
    h2: 1906.34 MB (U(h) = 0.073908*h^0.420189)
Reading from 1035: heap size 626 MB, throughput 0.993545
Reading from 1036: heap size 572 MB, throughput 0.987662
Reading from 1035: heap size 624 MB, throughput 0.607091
Reading from 1036: heap size 576 MB, throughput 0.992188
Reading from 1035: heap size 627 MB, throughput 0.904975
Numeric result:
Recommendation: 2 clients, utility 1.75155:
    h1: 1505.25 MB (U(h) = 0.201693*h^0.244511)
    h2: 1806.75 MB (U(h) = 0.160665*h^0.293485)
Recommendation: 2 clients, utility 1.75155:
    h1: 1505.25 MB (U(h) = 0.201693*h^0.244511)
    h2: 1806.75 MB (U(h) = 0.160665*h^0.293485)
Reading from 1036: heap size 580 MB, throughput 0.991485
Reading from 1035: heap size 629 MB, throughput 0.99249
Reading from 1035: heap size 630 MB, throughput 0.389617
Reading from 1036: heap size 582 MB, throughput 0.992085
Reading from 1035: heap size 630 MB, throughput 0.988817
Reading from 1036: heap size 583 MB, throughput 0.99079
Reading from 1035: heap size 632 MB, throughput 0.991622
Numeric result:
Recommendation: 2 clients, utility 1.61918:
    h1: 1263.22 MB (U(h) = 0.30468*h^0.177911)
    h2: 2048.78 MB (U(h) = 0.165231*h^0.288563)
Recommendation: 2 clients, utility 1.61918:
    h1: 1263.18 MB (U(h) = 0.30468*h^0.177911)
    h2: 2048.82 MB (U(h) = 0.165231*h^0.288563)
Reading from 1036: heap size 586 MB, throughput 0.992272
Reading from 1036: heap size 589 MB, throughput 0.984503
Reading from 1035: heap size 634 MB, throughput 0.988591
Reading from 1036: heap size 588 MB, throughput 0.982131
Reading from 1036: heap size 592 MB, throughput 0.988774
Reading from 1035: heap size 636 MB, throughput 0.898735
Numeric result:
Recommendation: 2 clients, utility 1.45422:
    h1: 973.19 MB (U(h) = 0.477321*h^0.10627)
    h2: 2338.81 MB (U(h) = 0.202225*h^0.255396)
Recommendation: 2 clients, utility 1.45422:
    h1: 973.183 MB (U(h) = 0.477321*h^0.10627)
    h2: 2338.82 MB (U(h) = 0.202225*h^0.255396)
Reading from 1036: heap size 591 MB, throughput 0.990503
Reading from 1035: heap size 639 MB, throughput 0.391865
Reading from 1035: heap size 640 MB, throughput 0.986349
Client 1035 died
Clients: 1
Reading from 1036: heap size 595 MB, throughput 0.992089
Reading from 1036: heap size 596 MB, throughput 0.992741
Recommendation: one client; give it all the memory
Reading from 1036: heap size 598 MB, throughput 0.993775
Reading from 1036: heap size 599 MB, throughput 0.989417
Reading from 1036: heap size 601 MB, throughput 0.990926
Reading from 1036: heap size 608 MB, throughput 0.99388
Recommendation: one client; give it all the memory
Reading from 1036: heap size 608 MB, throughput 0.994314
Reading from 1036: heap size 607 MB, throughput 0.994445
Recommendation: one client; give it all the memory
Reading from 1036: heap size 610 MB, throughput 0.994084
Reading from 1036: heap size 611 MB, throughput 0.993925
Reading from 1036: heap size 612 MB, throughput 0.989878
Client 1036 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
