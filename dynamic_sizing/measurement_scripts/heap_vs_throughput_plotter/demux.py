import economem.reading_log
import sys

class Demux(object):
    def __init__(self, throughput_model, input_file=sys.stdin):
        self.throughput_model = throughput_model
        self.input_file = input_file
        self.cache = {}

    def cleanup(self):
        for pid in self.cache:
            self.cleanup_client(self.cache[pid])

    def new_client(self, pid):
        raise NotImplementedError

    def write_client(self, out, s):
        raise NotImplementedError

    def client_died(self, pid, out):
        pass

    def cleanup_client(self, out):
        pass

    def demux(self):
        while True:
            # Have to do it this way because 'for line in self.input_file' is
            # buffered and unresponsive in the interactive case
            line = self.input_file.readline()
            if not line:
                break

            event = economem.reading_log.event_from_str(line)

            if event.pid != 0:
                if event.pid in self.cache:
                    out = self.cache[event.pid]
                else:
                    out = self.new_client(event.pid)
                    self.cache[event.pid] = out
                    if not isinstance(event, economem.reading_log.Creation):
                        self.write_client(out, str(economem.reading_log.Creation(event.timestamp, event.pid)) + "\n")

                try:
                    self.write_client(out, str(event) + "\n")

                    if isinstance(event, economem.reading_log.Death):
                        self.client_died(event.pid, out)
                except IOError:
                    pass

        self.cleanup()
