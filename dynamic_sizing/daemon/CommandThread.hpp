#ifndef COMMANDTHREAD_HPP
#define COMMANDTHREAD_HPP

#include <string>
#include <CommandReceiver.hpp>
#include <CommandResultSender.hpp>
#include <Log.hpp>
#include <Messages.hpp>
#include <ReceiverThread.hpp>
#include <RecommendationSender.hpp>

class ClientList;
class ReadingThread;
class TimerThread;

class CommandThread : public economem::ReceiverThread<economem::Command>
{
public:
    CommandThread(ReadingThread& readingThread,
                  TimerThread& timerThread,
                  economem::CommandReceiver& receiver,
                  economem::CommandResultSender& resultSender,
                  economem::RecommendationSender& recommendationSender,
                  ClientList& clients,
                  economem::Log& log,
                  const std::string& dumpFileName);

private:
    virtual void received(const economem::Command& cmd);
    bool dump();

    ReadingThread& m_ReadingThread;
    TimerThread& m_TimerThread;
    economem::CommandResultSender& m_ResultSender;
    economem::RecommendationSender& m_RecommendationSender;
    ClientList& m_Clients;
    economem::Log& m_Log;
    const std::string& m_DumpFileName;
};

#endif
