import Economem.Files
import Economem.PostprocessCmdlineLauncher
import Economem.TimerComparisonMany

main :: IO ()
main = postprocess (asDir :: FilePath -> ComparisonManyDir)
