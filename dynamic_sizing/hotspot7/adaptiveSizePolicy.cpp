/*
 * Copyright (c) 2004, 2010, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 *
 */

#include "precompiled.hpp"
#include "gc_implementation/shared/adaptiveSizePolicy.hpp"
#include "gc_interface/gcCause.hpp"
#include "memory/collectorPolicy.hpp"
#include "runtime/timer.hpp"
#include "utilities/ostream.hpp"
#include "utilities/workgroup.hpp"
#include <jeconomem.hpp>
elapsedTimer AdaptiveSizePolicy::_minor_timer;
elapsedTimer AdaptiveSizePolicy::_major_timer;
bool AdaptiveSizePolicy::_debug_perturbation = false;

// The throughput goal is implemented as
//      _throughput_goal = 1 - ( 1 / (1 + gc_cost_ratio))
// gc_cost_ratio is the ratio
//      application cost / gc cost
// For example a gc_cost_ratio of 4 translates into a
// throughput goal of .80

AdaptiveSizePolicy::AdaptiveSizePolicy(size_t init_eden_size,
                                       size_t init_promo_size,
                                       size_t init_survivor_size,
                                       double gc_pause_goal_sec,
                                       uint gc_cost_ratio) :
    _eden_size(init_eden_size),
    _promo_size(init_promo_size),
    _survivor_size(init_survivor_size),
    _gc_pause_goal_sec(gc_pause_goal_sec),
    _throughput_goal(1.0 - double(1.0 / (1.0 + (double) gc_cost_ratio))),
    _gc_overhead_limit_exceeded(false),
    _print_gc_overhead_limit_would_be_exceeded(false),
    _gc_overhead_limit_count(0),
    _latest_minor_mutator_interval_seconds(0),
    _threshold_tolerance_percent(1.0 + ThresholdTolerance/100.0),
    _young_gen_change_for_minor_throughput(0),
    _old_gen_change_for_major_throughput(0) {
  assert(AdaptiveSizePolicyGCTimeLimitThreshold > 0,
    "No opportunity to clear SoftReferences before GC overhead limit");
  _avg_minor_pause    =
    new AdaptivePaddedAverage(AdaptiveTimeWeight, PausePadding);
  _avg_minor_interval = new AdaptiveWeightedAverage(AdaptiveTimeWeight);
  _avg_minor_gc_cost  = new AdaptiveWeightedAverage(AdaptiveTimeWeight);
  _avg_major_gc_cost  = new AdaptiveWeightedAverage(AdaptiveTimeWeight);

  _avg_young_live     = new AdaptiveWeightedAverage(AdaptiveSizePolicyWeight);
  _avg_old_live       = new AdaptiveWeightedAverage(AdaptiveSizePolicyWeight);
  _avg_eden_live      = new AdaptiveWeightedAverage(AdaptiveSizePolicyWeight);

  _avg_survived       = new AdaptivePaddedAverage(AdaptiveSizePolicyWeight,
                                                  SurvivorPadding);
  _avg_pretenured     = new AdaptivePaddedNoZeroDevAverage(
                                                  AdaptiveSizePolicyWeight,
                                                  SurvivorPadding);

  _minor_pause_old_estimator =
    new LinearLeastSquareFit(AdaptiveSizePolicyWeight);
  _minor_pause_young_estimator =
    new LinearLeastSquareFit(AdaptiveSizePolicyWeight);
  _minor_collection_estimator =
    new LinearLeastSquareFit(AdaptiveSizePolicyWeight);
  _major_collection_estimator =
    new LinearLeastSquareFit(AdaptiveSizePolicyWeight);

  // Start the timers
  _minor_timer.start();

  _young_gen_policy_is_ready = false;

  JEconoMem::asp_create(this);
}

//  If the number of GC threads was set on the command line,
// use it.
//  Else
//    Calculate the number of GC threads based on the number of Java threads.
//    Calculate the number of GC threads based on the size of the heap.
//    Use the larger.

int AdaptiveSizePolicy::calc_default_active_workers(uintx total_workers,
                                            const uintx min_workers,
                                            uintx active_workers,
                                            uintx application_workers) {
    return JEconoMem::asp_calc_default_active_workers(total_workers, min_workers, active_workers, application_workers);
}

int AdaptiveSizePolicy::calc_active_workers(uintx total_workers,
                                            uintx active_workers,
                                            uintx application_workers) {
    return JEconoMem::asp_calc_active_workers(total_workers, active_workers, application_workers);
}

int AdaptiveSizePolicy::calc_active_conc_workers(uintx total_workers,
                                                 uintx active_workers,
                                                 uintx application_workers) {
    return JEconoMem::asp_calc_active_conc_workers(total_workers, active_workers, application_workers);
}

bool AdaptiveSizePolicy::tenuring_threshold_change() const {
    return JEconoMem::asp_tenuring_threshold_change(this);
}

void AdaptiveSizePolicy::minor_collection_begin() {
    JEconoMem::asp_minor_collection_begin(this);
}

void AdaptiveSizePolicy::update_minor_pause_young_estimator(
    double minor_pause_in_ms) {
    JEconoMem::asp_update_minor_pause_young_estimator(this, minor_pause_in_ms);
}

void AdaptiveSizePolicy::minor_collection_end(GCCause::Cause gc_cause) {
    JEconoMem::asp_minor_collection_end(this, gc_cause);
}

size_t AdaptiveSizePolicy::eden_increment(size_t cur_eden,
                                            uint percent_change) {
    return JEconoMem::asp_eden_increment(this, cur_eden, percent_change);
}

size_t AdaptiveSizePolicy::eden_increment(size_t cur_eden) {
    return JEconoMem::asp_eden_increment(this, cur_eden);
}

size_t AdaptiveSizePolicy::eden_decrement(size_t cur_eden) {
    return JEconoMem::asp_eden_decrement(this, cur_eden);
}

size_t AdaptiveSizePolicy::promo_increment(size_t cur_promo,
                                             uint percent_change) {
    return JEconoMem::asp_promo_increment(this, cur_promo, percent_change);
}

size_t AdaptiveSizePolicy::promo_increment(size_t cur_promo) {
    return JEconoMem::asp_promo_increment(this, cur_promo);
}

size_t AdaptiveSizePolicy::promo_decrement(size_t cur_promo) {
    return JEconoMem::asp_promo_decrement(this, cur_promo);
}

double AdaptiveSizePolicy::time_since_major_gc() const {
    return JEconoMem::asp_time_since_major_gc(this);
}

// Linear decay of major gc cost
double AdaptiveSizePolicy::decaying_major_gc_cost() const {
    return JEconoMem::asp_decaying_major_gc_cost(this);
}

// Use a value of the major gc cost that has been decayed
// by the factor
//
//      average-interval-between-major-gc * AdaptiveSizeMajorGCDecayTimeScale /
//        time-since-last-major-gc
//
// if the average-interval-between-major-gc * AdaptiveSizeMajorGCDecayTimeScale
// is less than time-since-last-major-gc.
//
// In cases where there are initial major gc's that
// are of a relatively high cost but no later major
// gc's, the total gc cost can remain high because
// the major gc cost remains unchanged (since there are no major
// gc's).  In such a situation the value of the unchanging
// major gc cost can keep the mutator throughput below
// the goal when in fact the major gc cost is becoming diminishingly
// small.  Use the decaying gc cost only to decide whether to
// adjust for throughput.  Using it also to determine the adjustment
// to be made for throughput also seems reasonable but there is
// no test case to use to decide if it is the right thing to do
// don't do it yet.

double AdaptiveSizePolicy::decaying_gc_cost() const {
    return JEconoMem::asp_decaying_gc_cost(this);
}


void AdaptiveSizePolicy::clear_generation_free_space_flags() {
    JEconoMem::asp_clear_generation_free_space_flags(this);
}

void AdaptiveSizePolicy::check_gc_overhead_limit(
                                          size_t young_live,
                                          size_t eden_live,
                                          size_t max_old_gen_size,
                                          size_t max_eden_size,
                                          bool   is_full_gc,
                                          GCCause::Cause gc_cause,
                                          CollectorPolicy* collector_policy) {
    JEconoMem::asp_check_gc_overhead_limit(this, young_live, eden_live, max_old_gen_size, max_eden_size, is_full_gc, gc_cause, collector_policy);
}
// Printing

bool AdaptiveSizePolicy::print_adaptive_size_policy_on(outputStream* st) const {
    return JEconoMem::asp_print_adaptive_size_policy_on(this, st);
}

bool AdaptiveSizePolicy::print_adaptive_size_policy_on(
                                            outputStream* st,
                                            int tenuring_threshold_arg) const {
    return JEconoMem::asp_print_adaptive_size_policy_on(this, st, tenuring_threshold_arg);
}
