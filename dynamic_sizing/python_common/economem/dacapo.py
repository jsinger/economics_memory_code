import csv
import os.path
import psutil

class PidNames(object):
    def __init__(self, pid_names_file, guess_pids):
        super(PidNames, self).__init__()
        self.pid_names = None
        self.guess_cache = {}
        self.guess_pids = guess_pids
        if pid_names_file:
            with open(pid_names_file) as f:
                data = csv.DictReader(f)
                self.pid_names = {}
                for line in data:
                    self.pid_names[int(line["PID"])] = line["NAME"]

    def get_name(self, pid):
        if pid == 0:
            return "[Unknown]"
        elif self.pid_names:
            return self.pid_names[int(pid)]
        elif self.guess_pids:
            if int(pid) in self.guess_cache:
                return self.guess_cache[int(pid)]
            else:
                name = benchmark_from_pid(int(pid))
                self.guess_cache[int(pid)] = name
                return name
        else:
            return str(pid)


def benchmark_from_pid(pid):
    try:
        cmd = psutil.Process(pid).cmdline()
        exepath = cmd[0]

        if os.path.split(exepath)[-1] != "java":
            return str(pid)

        if "-jar" not in cmd:
            return str(pid)

        jar_index = cmd.index("-jar")
        if jar_index == len(cmd) - 1:
            return str(pid)

        jar = os.path.split(cmd[jar_index + 1])
        if jar[-1] != "dacapo-9.12-bach.jar":
            return str(pid)

        return cmd[-1]

    except psutil.Error:
        return str(pid)
