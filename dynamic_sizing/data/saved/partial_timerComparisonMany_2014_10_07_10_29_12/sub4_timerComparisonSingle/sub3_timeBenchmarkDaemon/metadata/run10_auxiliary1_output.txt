economemd
    total memory: 7518 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub4_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run10_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 6562: heap size 9 MB, throughput 0.987579
Clients: 1
Client 6562 has a minimum heap size of 30 MB
Reading from 6561: heap size 9 MB, throughput 0.990343
Clients: 2
Client 6561 has a minimum heap size of 1223 MB
Reading from 6562: heap size 9 MB, throughput 0.980587
Reading from 6561: heap size 9 MB, throughput 0.987755
Reading from 6562: heap size 11 MB, throughput 0.979854
Reading from 6562: heap size 11 MB, throughput 0.962803
Reading from 6562: heap size 15 MB, throughput 0.952824
Reading from 6562: heap size 15 MB, throughput 0.923425
Reading from 6562: heap size 24 MB, throughput 0.891552
Reading from 6562: heap size 24 MB, throughput 0.864955
Reading from 6561: heap size 9 MB, throughput 0.980718
Reading from 6562: heap size 40 MB, throughput 0.897873
Reading from 6562: heap size 40 MB, throughput 0.882672
Reading from 6562: heap size 60 MB, throughput 0.9212
Reading from 6562: heap size 60 MB, throughput 0.934798
Reading from 6561: heap size 9 MB, throughput 0.972295
Reading from 6562: heap size 91 MB, throughput 0.944218
Reading from 6562: heap size 91 MB, throughput 0.954381
Reading from 6562: heap size 140 MB, throughput 0.963341
Reading from 6561: heap size 11 MB, throughput 0.975657
Reading from 6561: heap size 11 MB, throughput 0.978239
Reading from 6561: heap size 17 MB, throughput 0.97591
Reading from 6561: heap size 17 MB, throughput 0.870169
Reading from 6561: heap size 30 MB, throughput 0.755262
Reading from 6561: heap size 31 MB, throughput 0.408072
Reading from 6562: heap size 140 MB, throughput 0.963095
Reading from 6561: heap size 34 MB, throughput 0.612566
Reading from 6561: heap size 46 MB, throughput 0.43035
Reading from 6561: heap size 46 MB, throughput 0.525002
Reading from 6561: heap size 49 MB, throughput 0.596
Reading from 6561: heap size 64 MB, throughput 0.458537
Reading from 6561: heap size 76 MB, throughput 0.771489
Reading from 6562: heap size 224 MB, throughput 0.918596
Reading from 6561: heap size 82 MB, throughput 0.667453
Reading from 6561: heap size 82 MB, throughput 0.451926
Reading from 6561: heap size 108 MB, throughput 0.711638
Reading from 6561: heap size 109 MB, throughput 0.59262
Reading from 6561: heap size 109 MB, throughput 0.487857
Reading from 6562: heap size 240 MB, throughput 0.995079
Reading from 6561: heap size 111 MB, throughput 0.456063
Reading from 6561: heap size 148 MB, throughput 0.650943
Reading from 6561: heap size 149 MB, throughput 0.660179
Reading from 6561: heap size 150 MB, throughput 0.453628
Reading from 6561: heap size 152 MB, throughput 0.280545
Reading from 6561: heap size 161 MB, throughput 0.338186
Reading from 6561: heap size 162 MB, throughput 0.408301
Reading from 6562: heap size 283 MB, throughput 0.994072
Reading from 6561: heap size 206 MB, throughput 0.371942
Reading from 6561: heap size 209 MB, throughput 0.440282
Reading from 6561: heap size 212 MB, throughput 0.156633
Reading from 6561: heap size 216 MB, throughput 0.676529
Reading from 6561: heap size 226 MB, throughput 0.706572
Reading from 6561: heap size 229 MB, throughput 0.644298
Reading from 6561: heap size 233 MB, throughput 0.594456
Reading from 6561: heap size 240 MB, throughput 0.62079
Reading from 6562: heap size 300 MB, throughput 0.951811
Reading from 6561: heap size 247 MB, throughput 0.433937
Reading from 6561: heap size 299 MB, throughput 0.520153
Reading from 6561: heap size 310 MB, throughput 0.563847
Reading from 6561: heap size 312 MB, throughput 0.586536
Reading from 6561: heap size 318 MB, throughput 0.563832
Reading from 6562: heap size 337 MB, throughput 0.990504
Reading from 6561: heap size 322 MB, throughput 0.284136
Reading from 6561: heap size 378 MB, throughput 0.530812
Reading from 6561: heap size 382 MB, throughput 0.598739
Reading from 6561: heap size 384 MB, throughput 0.623255
Reading from 6562: heap size 343 MB, throughput 0.992
Reading from 6561: heap size 385 MB, throughput 0.609928
Reading from 6561: heap size 387 MB, throughput 0.619768
Reading from 6561: heap size 395 MB, throughput 0.580183
Reading from 6561: heap size 398 MB, throughput 0.532884
Reading from 6561: heap size 409 MB, throughput 0.474578
Reading from 6561: heap size 415 MB, throughput 0.422375
Reading from 6562: heap size 376 MB, throughput 0.988021
Numeric result:
Recommendation: 2 clients, utility 1.0094:
    h1: 6295 MB (U(h) = 0.905385*h^0.011797)
    h2: 1223 MB (U(h) = 0.998449*h^0.001)
Recommendation: 2 clients, utility 1.0094:
    h1: 6295 MB (U(h) = 0.905385*h^0.011797)
    h2: 1223 MB (U(h) = 0.998449*h^0.001)
Reading from 6561: heap size 424 MB, throughput 0.360158
Reading from 6561: heap size 492 MB, throughput 0.451461
Reading from 6561: heap size 492 MB, throughput 0.498928
Reading from 6561: heap size 489 MB, throughput 0.531317
Reading from 6562: heap size 381 MB, throughput 0.970452
Reading from 6561: heap size 492 MB, throughput 0.269997
Reading from 6561: heap size 551 MB, throughput 0.537163
Reading from 6561: heap size 555 MB, throughput 0.569738
Reading from 6561: heap size 546 MB, throughput 0.576602
Reading from 6561: heap size 550 MB, throughput 0.576537
Reading from 6561: heap size 551 MB, throughput 0.507839
Reading from 6562: heap size 424 MB, throughput 0.987933
Reading from 6561: heap size 556 MB, throughput 0.54118
Reading from 6561: heap size 559 MB, throughput 0.569634
Reading from 6561: heap size 566 MB, throughput 0.588318
Reading from 6562: heap size 427 MB, throughput 0.980715
Reading from 6561: heap size 572 MB, throughput 0.373943
Reading from 6561: heap size 645 MB, throughput 0.532509
Reading from 6561: heap size 657 MB, throughput 0.587569
Reading from 6561: heap size 657 MB, throughput 0.592643
Reading from 6561: heap size 660 MB, throughput 0.726818
Reading from 6562: heap size 461 MB, throughput 0.985623
Reading from 6561: heap size 668 MB, throughput 0.790107
Reading from 6561: heap size 669 MB, throughput 0.786893
Reading from 6562: heap size 461 MB, throughput 0.986764
Reading from 6561: heap size 684 MB, throughput 0.783989
Reading from 6561: heap size 693 MB, throughput 0.673166
Reading from 6562: heap size 497 MB, throughput 0.983471
Reading from 6561: heap size 707 MB, throughput 0.46155
Reading from 6561: heap size 788 MB, throughput 0.496126
Reading from 6561: heap size 694 MB, throughput 0.44762
Reading from 6561: heap size 769 MB, throughput 0.403699
Numeric result:
Recommendation: 2 clients, utility 0.785461:
    h1: 6295 MB (U(h) = 0.900423*h^0.0133917)
    h2: 1223 MB (U(h) = 0.770399*h^0.001)
Recommendation: 2 clients, utility 0.785461:
    h1: 6295 MB (U(h) = 0.900423*h^0.0133917)
    h2: 1223 MB (U(h) = 0.770399*h^0.001)
Reading from 6562: heap size 499 MB, throughput 0.985513
Reading from 6561: heap size 673 MB, throughput 0.158
Reading from 6561: heap size 855 MB, throughput 0.313815
Reading from 6561: heap size 857 MB, throughput 0.65344
Reading from 6561: heap size 864 MB, throughput 0.721185
Reading from 6561: heap size 864 MB, throughput 0.689455
Reading from 6561: heap size 858 MB, throughput 0.635331
Reading from 6561: heap size 862 MB, throughput 0.587404
Reading from 6562: heap size 541 MB, throughput 0.977257
Reading from 6561: heap size 861 MB, throughput 0.399522
Reading from 6561: heap size 955 MB, throughput 0.640409
Reading from 6561: heap size 956 MB, throughput 0.79047
Reading from 6561: heap size 963 MB, throughput 0.869429
Reading from 6561: heap size 968 MB, throughput 0.897608
Reading from 6562: heap size 544 MB, throughput 0.985879
Reading from 6561: heap size 969 MB, throughput 0.916708
Reading from 6561: heap size 961 MB, throughput 0.909295
Reading from 6561: heap size 805 MB, throughput 0.863607
Reading from 6561: heap size 933 MB, throughput 0.799102
Reading from 6561: heap size 809 MB, throughput 0.813696
Reading from 6561: heap size 919 MB, throughput 0.799501
Reading from 6561: heap size 813 MB, throughput 0.811138
Reading from 6561: heap size 908 MB, throughput 0.821022
Reading from 6561: heap size 819 MB, throughput 0.778399
Reading from 6562: heap size 583 MB, throughput 0.986415
Reading from 6561: heap size 901 MB, throughput 0.789614
Reading from 6561: heap size 908 MB, throughput 0.801642
Reading from 6561: heap size 894 MB, throughput 0.821498
Reading from 6561: heap size 901 MB, throughput 0.805517
Reading from 6562: heap size 591 MB, throughput 0.98581
Reading from 6561: heap size 894 MB, throughput 0.965507
Reading from 6561: heap size 898 MB, throughput 0.949589
Reading from 6561: heap size 899 MB, throughput 0.909551
Reading from 6561: heap size 901 MB, throughput 0.875638
Reading from 6561: heap size 908 MB, throughput 0.847177
Reading from 6562: heap size 632 MB, throughput 0.990935
Reading from 6561: heap size 908 MB, throughput 0.820597
Numeric result:
Recommendation: 2 clients, utility 0.603918:
    h1: 3581.94 MB (U(h) = 0.897795*h^0.0141825)
    h2: 3936.06 MB (U(h) = 0.526426*h^0.0155928)
Recommendation: 2 clients, utility 0.603918:
    h1: 3580.96 MB (U(h) = 0.897795*h^0.0141825)
    h2: 3937.04 MB (U(h) = 0.526426*h^0.0155928)
Reading from 6561: heap size 916 MB, throughput 0.78192
Reading from 6561: heap size 916 MB, throughput 0.77173
Reading from 6561: heap size 922 MB, throughput 0.785299
Reading from 6561: heap size 923 MB, throughput 0.774212
Reading from 6561: heap size 930 MB, throughput 0.828081
Reading from 6561: heap size 931 MB, throughput 0.860144
Reading from 6562: heap size 635 MB, throughput 0.985903
Reading from 6561: heap size 934 MB, throughput 0.814693
Reading from 6561: heap size 936 MB, throughput 0.718095
Reading from 6562: heap size 669 MB, throughput 0.987868
Reading from 6561: heap size 925 MB, throughput 0.651308
Reading from 6561: heap size 1058 MB, throughput 0.578913
Reading from 6561: heap size 1056 MB, throughput 0.621773
Reading from 6561: heap size 1061 MB, throughput 0.651976
Reading from 6561: heap size 1062 MB, throughput 0.660696
Reading from 6561: heap size 1066 MB, throughput 0.647536
Reading from 6561: heap size 1077 MB, throughput 0.199483
Reading from 6561: heap size 1078 MB, throughput 0.370936
Reading from 6561: heap size 1095 MB, throughput 0.443565
Reading from 6562: heap size 671 MB, throughput 0.99066
Reading from 6561: heap size 1095 MB, throughput 0.659245
Reading from 6562: heap size 703 MB, throughput 0.99297
Reading from 6561: heap size 1113 MB, throughput 0.947471
Numeric result:
Recommendation: 2 clients, utility 0.813797:
    h1: 738.324 MB (U(h) = 0.896288*h^0.014613)
    h2: 6779.68 MB (U(h) = 0.252439*h^0.134161)
Recommendation: 2 clients, utility 0.813797:
    h1: 738.44 MB (U(h) = 0.896288*h^0.014613)
    h2: 6779.56 MB (U(h) = 0.252439*h^0.134161)
Reading from 6562: heap size 708 MB, throughput 0.991237
Reading from 6561: heap size 1116 MB, throughput 0.964561
Reading from 6562: heap size 743 MB, throughput 0.990671
Reading from 6562: heap size 705 MB, throughput 0.984835
Reading from 6561: heap size 1131 MB, throughput 0.968914
Reading from 6562: heap size 732 MB, throughput 0.992868
Reading from 6561: heap size 1134 MB, throughput 0.971809
Numeric result:
Recommendation: 2 clients, utility 0.916001:
    h1: 581.517 MB (U(h) = 0.895427*h^0.0148538)
    h2: 6936.48 MB (U(h) = 0.19412*h^0.177221)
Recommendation: 2 clients, utility 0.916001:
    h1: 581.39 MB (U(h) = 0.895427*h^0.0148538)
    h2: 6936.61 MB (U(h) = 0.19412*h^0.177221)
Reading from 6562: heap size 752 MB, throughput 0.991848
Reading from 6561: heap size 1137 MB, throughput 0.971411
Reading from 6562: heap size 700 MB, throughput 0.992343
Reading from 6562: heap size 665 MB, throughput 0.989954
Reading from 6561: heap size 1142 MB, throughput 0.760369
Reading from 6562: heap size 646 MB, throughput 0.987765
Reading from 6561: heap size 1138 MB, throughput 0.795221
Reading from 6562: heap size 627 MB, throughput 0.985202
Numeric result:
Recommendation: 2 clients, utility 1.09678:
    h1: 425.187 MB (U(h) = 0.894639*h^0.0150773)
    h2: 7092.81 MB (U(h) = 0.120366*h^0.251462)
Recommendation: 2 clients, utility 1.09678:
    h1: 425.27 MB (U(h) = 0.894639*h^0.0150773)
    h2: 7092.73 MB (U(h) = 0.120366*h^0.251462)
Reading from 6562: heap size 568 MB, throughput 0.990134
Reading from 6561: heap size 1143 MB, throughput 0.778487
Reading from 6562: heap size 545 MB, throughput 0.992374
Reading from 6562: heap size 518 MB, throughput 0.993471
Reading from 6562: heap size 495 MB, throughput 0.987237
Reading from 6561: heap size 1150 MB, throughput 0.748855
Reading from 6562: heap size 484 MB, throughput 0.990762
Reading from 6562: heap size 454 MB, throughput 0.988616
Reading from 6561: heap size 1150 MB, throughput 0.662945
Reading from 6562: heap size 447 MB, throughput 0.991392
Numeric result:
Recommendation: 2 clients, utility 1.16103:
    h1: 400.782 MB (U(h) = 0.89355*h^0.0155055)
    h2: 7117.22 MB (U(h) = 0.10292*h^0.275384)
Recommendation: 2 clients, utility 1.16103:
    h1: 400.738 MB (U(h) = 0.89355*h^0.0155055)
    h2: 7117.26 MB (U(h) = 0.10292*h^0.275384)
Reading from 6562: heap size 422 MB, throughput 0.988255
Reading from 6562: heap size 417 MB, throughput 0.990759
Reading from 6561: heap size 1156 MB, throughput 0.363171
Reading from 6562: heap size 394 MB, throughput 0.988769
Reading from 6562: heap size 418 MB, throughput 0.989605
Reading from 6562: heap size 400 MB, throughput 0.991989
Reading from 6561: heap size 1159 MB, throughput 0.968479
Reading from 6562: heap size 422 MB, throughput 0.987787
Reading from 6562: heap size 418 MB, throughput 0.990523
Reading from 6561: heap size 1164 MB, throughput 0.969699
Reading from 6562: heap size 392 MB, throughput 0.986837
Numeric result:
Recommendation: 2 clients, utility 1.29002:
    h1: 348.431 MB (U(h) = 0.893438*h^0.0157016)
    h2: 7169.57 MB (U(h) = 0.0748031*h^0.323096)
Recommendation: 2 clients, utility 1.29002:
    h1: 348.422 MB (U(h) = 0.893438*h^0.0157016)
    h2: 7169.58 MB (U(h) = 0.0748031*h^0.323096)
Reading from 6562: heap size 422 MB, throughput 0.989547
Reading from 6562: heap size 397 MB, throughput 0.991863
Reading from 6561: heap size 1168 MB, throughput 0.970891
Reading from 6562: heap size 377 MB, throughput 0.992039
Reading from 6562: heap size 368 MB, throughput 0.992664
Reading from 6562: heap size 355 MB, throughput 0.988323
Reading from 6562: heap size 352 MB, throughput 0.986614
Reading from 6561: heap size 1173 MB, throughput 0.96843
Reading from 6562: heap size 338 MB, throughput 0.989079
Reading from 6562: heap size 361 MB, throughput 0.983492
Reading from 6562: heap size 350 MB, throughput 0.987418
Reading from 6561: heap size 1178 MB, throughput 0.965788
Reading from 6562: heap size 329 MB, throughput 0.98842
Numeric result:
Recommendation: 2 clients, utility 1.29591:
    h1: 355.036 MB (U(h) = 0.894014*h^0.0158258)
    h2: 7162.96 MB (U(h) = 0.0776018*h^0.319317)
Recommendation: 2 clients, utility 1.29591:
    h1: 355.007 MB (U(h) = 0.894014*h^0.0158258)
    h2: 7162.99 MB (U(h) = 0.0776018*h^0.319317)
Reading from 6562: heap size 345 MB, throughput 0.983713
Reading from 6562: heap size 361 MB, throughput 0.988175
Reading from 6562: heap size 335 MB, throughput 0.990373
Reading from 6561: heap size 1184 MB, throughput 0.967539
Reading from 6562: heap size 352 MB, throughput 0.983462
Reading from 6562: heap size 352 MB, throughput 0.985771
Reading from 6562: heap size 373 MB, throughput 0.989763
Reading from 6561: heap size 1189 MB, throughput 0.967637
Reading from 6562: heap size 345 MB, throughput 0.978178
Reading from 6562: heap size 361 MB, throughput 0.985448
Reading from 6562: heap size 333 MB, throughput 0.988941
Reading from 6562: heap size 352 MB, throughput 0.983542
Reading from 6561: heap size 1194 MB, throughput 0.968451
Numeric result:
Recommendation: 2 clients, utility 1.20066:
    h1: 405.569 MB (U(h) = 0.89428*h^0.015848)
    h2: 7112.43 MB (U(h) = 0.103801*h^0.277884)
Recommendation: 2 clients, utility 1.20066:
    h1: 405.625 MB (U(h) = 0.89428*h^0.015848)
    h2: 7112.37 MB (U(h) = 0.103801*h^0.277884)
Reading from 6562: heap size 351 MB, throughput 0.985182
Reading from 6562: heap size 374 MB, throughput 0.985804
Reading from 6562: heap size 374 MB, throughput 0.986083
Reading from 6561: heap size 1198 MB, throughput 0.967543
Reading from 6562: heap size 394 MB, throughput 0.988491
Reading from 6562: heap size 396 MB, throughput 0.985091
Reading from 6562: heap size 420 MB, throughput 0.988293
Reading from 6561: heap size 1203 MB, throughput 0.970909
Reading from 6562: heap size 392 MB, throughput 0.988923
Reading from 6562: heap size 406 MB, throughput 0.988624
Reading from 6562: heap size 380 MB, throughput 0.991403
Numeric result:
Recommendation: 2 clients, utility 1.37204:
    h1: 338.592 MB (U(h) = 0.894286*h^0.0159164)
    h2: 7179.41 MB (U(h) = 0.0699053*h^0.337419)
Recommendation: 2 clients, utility 1.37204:
    h1: 338.658 MB (U(h) = 0.894286*h^0.0159164)
    h2: 7179.34 MB (U(h) = 0.0699053*h^0.337419)
Reading from 6561: heap size 1204 MB, throughput 0.970312
Reading from 6562: heap size 393 MB, throughput 0.986867
Reading from 6562: heap size 378 MB, throughput 0.989896
Reading from 6562: heap size 353 MB, throughput 0.991929
Reading from 6562: heap size 340 MB, throughput 0.986024
Reading from 6561: heap size 1210 MB, throughput 0.968384
Reading from 6562: heap size 341 MB, throughput 0.98595
Reading from 6562: heap size 320 MB, throughput 0.989395
Reading from 6562: heap size 337 MB, throughput 0.983552
Reading from 6562: heap size 343 MB, throughput 0.986192
Reading from 6561: heap size 1210 MB, throughput 0.967132
Reading from 6562: heap size 320 MB, throughput 0.98985
Reading from 6562: heap size 351 MB, throughput 0.981997
Numeric result:
Recommendation: 2 clients, utility 1.55903:
    h1: 291.733 MB (U(h) = 0.894531*h^0.0159405)
    h2: 7226.27 MB (U(h) = 0.0476495*h^0.394906)
Recommendation: 2 clients, utility 1.55903:
    h1: 291.692 MB (U(h) = 0.894531*h^0.0159405)
    h2: 7226.31 MB (U(h) = 0.0476495*h^0.394906)
Reading from 6562: heap size 338 MB, throughput 0.986271
Reading from 6562: heap size 315 MB, throughput 0.98824
Reading from 6562: heap size 308 MB, throughput 0.976128
Reading from 6562: heap size 305 MB, throughput 0.967595
Reading from 6562: heap size 291 MB, throughput 0.979357
Reading from 6561: heap size 1215 MB, throughput 0.938823
Reading from 6562: heap size 303 MB, throughput 0.981228
Reading from 6562: heap size 280 MB, throughput 0.983588
Reading from 6562: heap size 311 MB, throughput 0.982633
Reading from 6562: heap size 292 MB, throughput 0.984166
Reading from 6561: heap size 1318 MB, throughput 0.962607
Reading from 6562: heap size 302 MB, throughput 0.983269
Reading from 6562: heap size 283 MB, throughput 0.982874
Reading from 6562: heap size 311 MB, throughput 0.986986
Numeric result:
Recommendation: 2 clients, utility 1.71322:
    h1: 264.105 MB (U(h) = 0.894611*h^0.0159332)
    h2: 7253.9 MB (U(h) = 0.0358041*h^0.437671)
Recommendation: 2 clients, utility 1.71322:
    h1: 264.075 MB (U(h) = 0.894611*h^0.0159332)
    h2: 7253.92 MB (U(h) = 0.0358041*h^0.437671)
Reading from 6562: heap size 290 MB, throughput 0.985179
Reading from 6562: heap size 281 MB, throughput 0.985405
Reading from 6561: heap size 1314 MB, throughput 0.974726
Reading from 6562: heap size 273 MB, throughput 0.988541
Reading from 6562: heap size 268 MB, throughput 0.989793
Reading from 6562: heap size 259 MB, throughput 0.991227
Reading from 6562: heap size 267 MB, throughput 0.983651
Reading from 6562: heap size 254 MB, throughput 0.98747
Reading from 6561: heap size 1323 MB, throughput 0.980355
Reading from 6562: heap size 274 MB, throughput 0.989516
Reading from 6562: heap size 257 MB, throughput 0.989742
Reading from 6562: heap size 266 MB, throughput 0.936184
Reading from 6562: heap size 259 MB, throughput 0.934314
Reading from 6562: heap size 276 MB, throughput 0.951421
Reading from 6562: heap size 263 MB, throughput 0.963985
Reading from 6561: heap size 1331 MB, throughput 0.983068
Reading from 6562: heap size 274 MB, throughput 0.934836
Numeric result:
Recommendation: 2 clients, utility 1.97682:
    h1: 232.382 MB (U(h) = 0.893332*h^0.0160873)
    h2: 7285.62 MB (U(h) = 0.022835*h^0.504415)
Recommendation: 2 clients, utility 1.97682:
    h1: 232.361 MB (U(h) = 0.893332*h^0.0160873)
    h2: 7285.64 MB (U(h) = 0.022835*h^0.504415)
Reading from 6562: heap size 267 MB, throughput 0.978728
Reading from 6562: heap size 261 MB, throughput 0.915246
Reading from 6562: heap size 242 MB, throughput 0.979122
Reading from 6562: heap size 240 MB, throughput 0.980819
Reading from 6562: heap size 242 MB, throughput 0.985528
Reading from 6561: heap size 1332 MB, throughput 0.983029
Reading from 6562: heap size 238 MB, throughput 0.985374
Reading from 6562: heap size 239 MB, throughput 0.987217
Reading from 6562: heap size 212 MB, throughput 0.988756
Reading from 6562: heap size 239 MB, throughput 0.988015
Reading from 6562: heap size 211 MB, throughput 0.981217
Reading from 6562: heap size 232 MB, throughput 0.976329
Reading from 6562: heap size 214 MB, throughput 0.976626
Reading from 6561: heap size 1327 MB, throughput 0.979374
Reading from 6562: heap size 228 MB, throughput 0.977522
Reading from 6562: heap size 228 MB, throughput 0.980988
Reading from 6562: heap size 240 MB, throughput 0.968356
Reading from 6562: heap size 205 MB, throughput 0.97777
Reading from 6562: heap size 230 MB, throughput 0.989194
Reading from 6562: heap size 230 MB, throughput 0.989193
Reading from 6562: heap size 230 MB, throughput 0.963305
Reading from 6562: heap size 228 MB, throughput 0.923014
Reading from 6562: heap size 236 MB, throughput 0.981007
Numeric result:
Recommendation: 2 clients, utility 2.00067:
    h1: 206.803 MB (U(h) = 0.903545*h^0.0143518)
    h2: 7311.2 MB (U(h) = 0.0224627*h^0.507385)
Recommendation: 2 clients, utility 2.00067:
    h1: 206.803 MB (U(h) = 0.903545*h^0.0143518)
    h2: 7311.2 MB (U(h) = 0.0224627*h^0.507385)
Reading from 6561: heap size 1227 MB, throughput 0.978811
Reading from 6562: heap size 211 MB, throughput 0.984157
Reading from 6562: heap size 219 MB, throughput 0.990382
Reading from 6562: heap size 196 MB, throughput 0.985577
Reading from 6562: heap size 220 MB, throughput 0.971038
Reading from 6562: heap size 194 MB, throughput 0.34554
Reading from 6562: heap size 216 MB, throughput 0.615597
Reading from 6562: heap size 188 MB, throughput 0.822505
Reading from 6562: heap size 211 MB, throughput 0.85087
Reading from 6561: heap size 1316 MB, throughput 0.979818
Reading from 6562: heap size 194 MB, throughput 0.960967
Reading from 6562: heap size 217 MB, throughput 0.9411
Reading from 6562: heap size 194 MB, throughput 0.96968
Reading from 6562: heap size 212 MB, throughput 0.981815
Reading from 6562: heap size 191 MB, throughput 0.988033
Reading from 6562: heap size 206 MB, throughput 0.979382
Reading from 6562: heap size 206 MB, throughput 0.977628
Reading from 6562: heap size 219 MB, throughput 0.979884
Reading from 6561: heap size 1246 MB, throughput 0.979
Reading from 6562: heap size 210 MB, throughput 0.97979
Reading from 6562: heap size 211 MB, throughput 0.980726
Reading from 6562: heap size 200 MB, throughput 0.976952
Reading from 6562: heap size 210 MB, throughput 0.971888
Numeric result:
Recommendation: 2 clients, utility 2.29557:
    h1: 553.433 MB (U(h) = 0.746063*h^0.0458022)
    h2: 6964.57 MB (U(h) = 0.014036*h^0.576449)
Recommendation: 2 clients, utility 2.29557:
    h1: 553.379 MB (U(h) = 0.746063*h^0.0458022)
    h2: 6964.62 MB (U(h) = 0.014036*h^0.576449)
Reading from 6562: heap size 196 MB, throughput 0.977207
Reading from 6562: heap size 209 MB, throughput 0.752317
Reading from 6562: heap size 207 MB, throughput 0.890898
Reading from 6562: heap size 217 MB, throughput 0.953102
Reading from 6561: heap size 1310 MB, throughput 0.978482
Reading from 6562: heap size 218 MB, throughput 0.970372
Reading from 6562: heap size 230 MB, throughput 0.973162
Reading from 6562: heap size 230 MB, throughput 0.981702
Reading from 6562: heap size 241 MB, throughput 0.984096
Reading from 6562: heap size 242 MB, throughput 0.985325
Reading from 6562: heap size 254 MB, throughput 0.989758
Reading from 6562: heap size 255 MB, throughput 0.98102
Reading from 6561: heap size 1315 MB, throughput 0.977159
Reading from 6562: heap size 270 MB, throughput 0.978869
Reading from 6562: heap size 270 MB, throughput 0.983415
Reading from 6562: heap size 284 MB, throughput 0.988561
Reading from 6562: heap size 285 MB, throughput 0.98289
Reading from 6562: heap size 302 MB, throughput 0.989642
Reading from 6562: heap size 302 MB, throughput 0.988924
Numeric result:
Recommendation: 2 clients, utility 2.64918:
    h1: 689.019 MB (U(h) = 0.668391*h^0.0648527)
    h2: 6828.98 MB (U(h) = 0.00890371*h^0.642727)
Recommendation: 2 clients, utility 2.64918:
    h1: 689.057 MB (U(h) = 0.668391*h^0.0648527)
    h2: 6828.94 MB (U(h) = 0.00890371*h^0.642727)
Reading from 6561: heap size 1317 MB, throughput 0.976883
Reading from 6562: heap size 318 MB, throughput 0.989645
Reading from 6562: heap size 318 MB, throughput 0.986233
Reading from 6562: heap size 335 MB, throughput 0.989745
Reading from 6562: heap size 336 MB, throughput 0.983441
Reading from 6561: heap size 1318 MB, throughput 0.97431
Reading from 6562: heap size 352 MB, throughput 0.988088
Reading from 6562: heap size 353 MB, throughput 0.989842
Reading from 6562: heap size 370 MB, throughput 0.970671
Reading from 6562: heap size 372 MB, throughput 0.973586
Reading from 6561: heap size 1320 MB, throughput 0.972299
Reading from 6562: heap size 397 MB, throughput 0.979433
Reading from 6562: heap size 398 MB, throughput 0.980643
Numeric result:
Recommendation: 2 clients, utility 3.02938:
    h1: 780.572 MB (U(h) = 0.608569*h^0.0815201)
    h2: 6737.43 MB (U(h) = 0.00585352*h^0.703628)
Recommendation: 2 clients, utility 3.02938:
    h1: 780.576 MB (U(h) = 0.608569*h^0.0815201)
    h2: 6737.42 MB (U(h) = 0.00585352*h^0.703628)
Reading from 6562: heap size 423 MB, throughput 0.985985
Reading from 6561: heap size 1325 MB, throughput 0.971203
Reading from 6562: heap size 425 MB, throughput 0.951196
Reading from 6562: heap size 446 MB, throughput 0.987033
Reading from 6562: heap size 450 MB, throughput 0.980725
Reading from 6561: heap size 1330 MB, throughput 0.971643
Reading from 6562: heap size 474 MB, throughput 0.98429
Reading from 6562: heap size 479 MB, throughput 0.978246
Reading from 6562: heap size 507 MB, throughput 0.982491
Numeric result:
Recommendation: 2 clients, utility 3.19772:
    h1: 793.148 MB (U(h) = 0.5931*h^0.0860307)
    h2: 6724.85 MB (U(h) = 0.00490437*h^0.729344)
Recommendation: 2 clients, utility 3.19772:
    h1: 793.229 MB (U(h) = 0.5931*h^0.0860307)
    h2: 6724.77 MB (U(h) = 0.00490437*h^0.729344)
Reading from 6561: heap size 1337 MB, throughput 0.971699
Reading from 6562: heap size 514 MB, throughput 0.980948
Reading from 6562: heap size 556 MB, throughput 0.989517
Reading from 6561: heap size 1343 MB, throughput 0.970786
Reading from 6562: heap size 564 MB, throughput 0.990284
Reading from 6562: heap size 597 MB, throughput 0.989108
Reading from 6562: heap size 602 MB, throughput 0.990022
Reading from 6562: heap size 633 MB, throughput 0.987723
Numeric result:
Recommendation: 2 clients, utility 3.18892:
    h1: 658.017 MB (U(h) = 0.64682*h^0.0705589)
    h2: 6859.98 MB (U(h) = 0.00469977*h^0.735582)
Recommendation: 2 clients, utility 3.18892:
    h1: 658.026 MB (U(h) = 0.64682*h^0.0705589)
    h2: 6859.97 MB (U(h) = 0.00469977*h^0.735582)
Reading from 6562: heap size 637 MB, throughput 0.988781
Reading from 6562: heap size 674 MB, throughput 0.990522
Reading from 6562: heap size 623 MB, throughput 0.987883
Reading from 6562: heap size 652 MB, throughput 0.988776
Reading from 6562: heap size 652 MB, throughput 0.988698
Reading from 6562: heap size 690 MB, throughput 0.987191
Numeric result:
Recommendation: 2 clients, utility 3.1581:
    h1: 558.667 MB (U(h) = 0.689615*h^0.0590496)
    h2: 6959.33 MB (U(h) = 0.00469977*h^0.735582)
Recommendation: 2 clients, utility 3.1581:
    h1: 558.667 MB (U(h) = 0.689615*h^0.0590496)
    h2: 6959.33 MB (U(h) = 0.00469977*h^0.735582)
Reading from 6562: heap size 655 MB, throughput 0.979404
Reading from 6562: heap size 626 MB, throughput 0.977538
Reading from 6561: heap size 1348 MB, throughput 0.947718
Reading from 6562: heap size 589 MB, throughput 0.981279
Reading from 6562: heap size 580 MB, throughput 0.9851
Reading from 6562: heap size 547 MB, throughput 0.988554
Reading from 6562: heap size 594 MB, throughput 0.987482
Numeric result:
Recommendation: 2 clients, utility 3.03428:
    h1: 510.588 MB (U(h) = 0.715938*h^0.0522487)
    h2: 7007.41 MB (U(h) = 0.00534869*h^0.717046)
Recommendation: 2 clients, utility 3.03428:
    h1: 510.605 MB (U(h) = 0.715938*h^0.0522487)
    h2: 7007.39 MB (U(h) = 0.00534869*h^0.717046)
Reading from 6562: heap size 560 MB, throughput 0.991147
Reading from 6562: heap size 531 MB, throughput 0.987664
Reading from 6562: heap size 520 MB, throughput 0.988888
Reading from 6562: heap size 495 MB, throughput 0.990481
Reading from 6562: heap size 517 MB, throughput 0.989018
Reading from 6561: heap size 1456 MB, throughput 0.986063
Reading from 6562: heap size 476 MB, throughput 0.991266
Reading from 6562: heap size 524 MB, throughput 0.989627
Numeric result:
Recommendation: 2 clients, utility 3.09472:
    h1: 490.88 MB (U(h) = 0.720892*h^0.0508936)
    h2: 7027.12 MB (U(h) = 0.00493463*h^0.728543)
Recommendation: 2 clients, utility 3.09472:
    h1: 490.89 MB (U(h) = 0.720892*h^0.0508936)
    h2: 7027.11 MB (U(h) = 0.00493463*h^0.728543)
Reading from 6562: heap size 483 MB, throughput 0.991179
Reading from 6562: heap size 505 MB, throughput 0.989556
Reading from 6562: heap size 466 MB, throughput 0.991133
Reading from 6561: heap size 1401 MB, throughput 0.986874
Reading from 6561: heap size 1496 MB, throughput 0.978971
Reading from 6562: heap size 512 MB, throughput 0.984737
Reading from 6561: heap size 1498 MB, throughput 0.961791
Reading from 6561: heap size 1479 MB, throughput 0.934637
Reading from 6561: heap size 1507 MB, throughput 0.890653
Reading from 6562: heap size 475 MB, throughput 0.989671
Reading from 6561: heap size 1531 MB, throughput 0.832134
Reading from 6561: heap size 1545 MB, throughput 0.789737
Reading from 6561: heap size 1574 MB, throughput 0.759047
Reading from 6561: heap size 1582 MB, throughput 0.730555
Reading from 6562: heap size 495 MB, throughput 0.989203
Reading from 6561: heap size 1613 MB, throughput 0.912388
Reading from 6562: heap size 462 MB, throughput 0.991461
Reading from 6562: heap size 485 MB, throughput 0.989654
Numeric result:
Recommendation: 2 clients, utility 2.87389:
    h1: 512.518 MB (U(h) = 0.717976*h^0.0513907)
    h2: 7005.48 MB (U(h) = 0.00577931*h^0.70245)
Recommendation: 2 clients, utility 2.87389:
    h1: 512.515 MB (U(h) = 0.717976*h^0.0513907)
    h2: 7005.48 MB (U(h) = 0.00577931*h^0.70245)
Reading from 6562: heap size 505 MB, throughput 0.991911
Reading from 6561: heap size 1616 MB, throughput 0.965626
Reading from 6562: heap size 504 MB, throughput 0.988311
Reading from 6562: heap size 528 MB, throughput 0.991788
Reading from 6562: heap size 494 MB, throughput 0.992472
Reading from 6561: heap size 1630 MB, throughput 0.975817
Reading from 6562: heap size 512 MB, throughput 0.993611
Reading from 6562: heap size 512 MB, throughput 0.986205
Numeric result:
Recommendation: 2 clients, utility 3.18485:
    h1: 479.848 MB (U(h) = 0.715384*h^0.0519644)
    h2: 7038.15 MB (U(h) = 0.00377381*h^0.762178)
Recommendation: 2 clients, utility 3.18485:
    h1: 479.853 MB (U(h) = 0.715384*h^0.0519644)
    h2: 7038.15 MB (U(h) = 0.00377381*h^0.762178)
Reading from 6562: heap size 535 MB, throughput 0.99111
Reading from 6561: heap size 1635 MB, throughput 0.979219
Reading from 6562: heap size 499 MB, throughput 0.986616
Reading from 6562: heap size 492 MB, throughput 0.991026
Reading from 6562: heap size 464 MB, throughput 0.987222
Reading from 6561: heap size 1636 MB, throughput 0.979959
Reading from 6562: heap size 487 MB, throughput 0.991483
Client 6562 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 6561: heap size 1645 MB, throughput 0.952883
Reading from 6561: heap size 1543 MB, throughput 0.989105
Reading from 6561: heap size 1546 MB, throughput 0.99216
Recommendation: one client; give it all the memory
Reading from 6561: heap size 1564 MB, throughput 0.992895
Reading from 6561: heap size 1566 MB, throughput 0.992714
Recommendation: one client; give it all the memory
Reading from 6561: heap size 1556 MB, throughput 0.9923
Recommendation: one client; give it all the memory
Reading from 6561: heap size 1352 MB, throughput 0.991609
Reading from 6561: heap size 1538 MB, throughput 0.990805
Recommendation: one client; give it all the memory
Reading from 6561: heap size 1380 MB, throughput 0.989875
Reading from 6561: heap size 1516 MB, throughput 0.988938
Recommendation: one client; give it all the memory
Reading from 6561: heap size 1407 MB, throughput 0.988063
Reading from 6561: heap size 1514 MB, throughput 0.98693
Recommendation: one client; give it all the memory
Reading from 6561: heap size 1520 MB, throughput 0.986196
Reading from 6561: heap size 1523 MB, throughput 0.984778
Recommendation: one client; give it all the memory
Reading from 6561: heap size 1525 MB, throughput 0.984329
Reading from 6561: heap size 1528 MB, throughput 0.983424
Recommendation: one client; give it all the memory
Reading from 6561: heap size 1535 MB, throughput 0.981938
Recommendation: one client; give it all the memory
Reading from 6561: heap size 1544 MB, throughput 0.98126
Reading from 6561: heap size 1551 MB, throughput 0.980987
Recommendation: one client; give it all the memory
Reading from 6561: heap size 1559 MB, throughput 0.980965
Reading from 6561: heap size 1563 MB, throughput 0.980762
Recommendation: one client; give it all the memory
Reading from 6561: heap size 1572 MB, throughput 0.980473
Reading from 6561: heap size 1574 MB, throughput 0.980667
Recommendation: one client; give it all the memory
Reading from 6561: heap size 1583 MB, throughput 0.980358
Recommendation: one client; give it all the memory
Reading from 6561: heap size 1583 MB, throughput 0.981195
Reading from 6561: heap size 1589 MB, throughput 0.981534
Recommendation: one client; give it all the memory
Reading from 6561: heap size 1590 MB, throughput 0.981642
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 6561: heap size 1596 MB, throughput 0.992311
Recommendation: one client; give it all the memory
Reading from 6561: heap size 1598 MB, throughput 0.990225
Reading from 6561: heap size 1610 MB, throughput 0.982227
Reading from 6561: heap size 1610 MB, throughput 0.964805
Reading from 6561: heap size 1643 MB, throughput 0.943481
Reading from 6561: heap size 1660 MB, throughput 0.909232
Client 6561 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
