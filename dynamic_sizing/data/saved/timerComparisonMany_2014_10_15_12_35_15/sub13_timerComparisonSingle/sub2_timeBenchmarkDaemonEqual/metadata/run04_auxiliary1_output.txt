economemd
    total memory: 4892 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub13_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 9018: heap size 9 MB, throughput 0.991161
Clients: 1
Client 9018 has a minimum heap size of 12 MB
Reading from 9017: heap size 9 MB, throughput 0.987503
Clients: 2
Client 9017 has a minimum heap size of 1211 MB
Reading from 9018: heap size 9 MB, throughput 0.98995
Reading from 9017: heap size 9 MB, throughput 0.964806
Reading from 9018: heap size 9 MB, throughput 0.965762
Reading from 9018: heap size 9 MB, throughput 0.972344
Reading from 9017: heap size 11 MB, throughput 0.977115
Reading from 9018: heap size 11 MB, throughput 0.969799
Reading from 9017: heap size 11 MB, throughput 0.984171
Reading from 9018: heap size 11 MB, throughput 0.961686
Reading from 9018: heap size 16 MB, throughput 0.986527
Reading from 9017: heap size 15 MB, throughput 0.875129
Reading from 9017: heap size 19 MB, throughput 0.969151
Reading from 9018: heap size 16 MB, throughput 0.985154
Reading from 9017: heap size 24 MB, throughput 0.910686
Reading from 9017: heap size 29 MB, throughput 0.955824
Reading from 9017: heap size 31 MB, throughput 0.6882
Reading from 9018: heap size 25 MB, throughput 0.891092
Reading from 9017: heap size 41 MB, throughput 0.859114
Reading from 9017: heap size 46 MB, throughput 0.830707
Reading from 9017: heap size 47 MB, throughput 0.351415
Reading from 9018: heap size 30 MB, throughput 0.987031
Reading from 9017: heap size 65 MB, throughput 0.696179
Reading from 9017: heap size 68 MB, throughput 0.785284
Reading from 9017: heap size 72 MB, throughput 0.782826
Reading from 9018: heap size 33 MB, throughput 0.977208
Reading from 9018: heap size 38 MB, throughput 0.993241
Reading from 9017: heap size 75 MB, throughput 0.179358
Reading from 9017: heap size 98 MB, throughput 0.666909
Reading from 9018: heap size 39 MB, throughput 0.91884
Reading from 9017: heap size 102 MB, throughput 0.759778
Reading from 9018: heap size 39 MB, throughput 0.975205
Reading from 9017: heap size 104 MB, throughput 0.83397
Reading from 9018: heap size 44 MB, throughput 0.979201
Reading from 9018: heap size 44 MB, throughput 0.966417
Reading from 9017: heap size 107 MB, throughput 0.141831
Reading from 9018: heap size 51 MB, throughput 0.988225
Reading from 9017: heap size 138 MB, throughput 0.641001
Reading from 9018: heap size 52 MB, throughput 0.977469
Reading from 9017: heap size 141 MB, throughput 0.687455
Reading from 9017: heap size 144 MB, throughput 0.700041
Reading from 9018: heap size 57 MB, throughput 0.968787
Reading from 9017: heap size 148 MB, throughput 0.581659
Reading from 9018: heap size 57 MB, throughput 0.344969
Reading from 9017: heap size 154 MB, throughput 0.597113
Reading from 9018: heap size 67 MB, throughput 0.969485
Reading from 9018: heap size 67 MB, throughput 0.986709
Reading from 9017: heap size 161 MB, throughput 0.105482
Reading from 9017: heap size 194 MB, throughput 0.58151
Reading from 9018: heap size 77 MB, throughput 0.995421
Reading from 9017: heap size 200 MB, throughput 0.125455
Reading from 9017: heap size 239 MB, throughput 0.713018
Reading from 9017: heap size 239 MB, throughput 0.672067
Reading from 9017: heap size 239 MB, throughput 0.700843
Reading from 9018: heap size 78 MB, throughput 0.995616
Reading from 9017: heap size 240 MB, throughput 0.684397
Reading from 9017: heap size 243 MB, throughput 0.578899
Reading from 9017: heap size 248 MB, throughput 0.52965
Reading from 9017: heap size 252 MB, throughput 0.474439
Reading from 9017: heap size 261 MB, throughput 0.488057
Reading from 9018: heap size 87 MB, throughput 0.990726
Reading from 9017: heap size 268 MB, throughput 0.493049
Reading from 9018: heap size 88 MB, throughput 0.996155
Reading from 9017: heap size 276 MB, throughput 0.124507
Reading from 9017: heap size 321 MB, throughput 0.494982
Reading from 9017: heap size 327 MB, throughput 0.480485
Reading from 9017: heap size 331 MB, throughput 0.576224
Reading from 9018: heap size 95 MB, throughput 0.996936
Reading from 9017: heap size 334 MB, throughput 0.112475
Reading from 9017: heap size 382 MB, throughput 0.5583
Reading from 9017: heap size 321 MB, throughput 0.684719
Reading from 9017: heap size 378 MB, throughput 0.67797
Reading from 9018: heap size 96 MB, throughput 0.996472
Reading from 9017: heap size 379 MB, throughput 0.630295
Reading from 9017: heap size 381 MB, throughput 0.542201
Reading from 9017: heap size 385 MB, throughput 0.555224
Reading from 9017: heap size 392 MB, throughput 0.567667
Reading from 9017: heap size 397 MB, throughput 0.481757
Reading from 9018: heap size 100 MB, throughput 0.997148
Reading from 9018: heap size 101 MB, throughput 0.996082
Reading from 9017: heap size 401 MB, throughput 0.102747
Reading from 9017: heap size 452 MB, throughput 0.46029
Equal recommendation: 2446 MB each
Reading from 9017: heap size 459 MB, throughput 0.642853
Reading from 9017: heap size 461 MB, throughput 0.587545
Reading from 9017: heap size 465 MB, throughput 0.592739
Reading from 9018: heap size 105 MB, throughput 0.997196
Reading from 9017: heap size 469 MB, throughput 0.442641
Reading from 9017: heap size 472 MB, throughput 0.48759
Reading from 9018: heap size 106 MB, throughput 0.996216
Reading from 9018: heap size 109 MB, throughput 0.986325
Reading from 9018: heap size 110 MB, throughput 0.994358
Reading from 9017: heap size 476 MB, throughput 0.0531881
Reading from 9017: heap size 537 MB, throughput 0.493365
Reading from 9017: heap size 538 MB, throughput 0.532887
Reading from 9017: heap size 540 MB, throughput 0.535395
Reading from 9018: heap size 113 MB, throughput 0.996824
Reading from 9017: heap size 543 MB, throughput 0.442355
Reading from 9017: heap size 546 MB, throughput 0.50067
Reading from 9018: heap size 113 MB, throughput 0.995442
Reading from 9017: heap size 554 MB, throughput 0.087106
Reading from 9018: heap size 115 MB, throughput 0.996764
Reading from 9017: heap size 619 MB, throughput 0.422218
Reading from 9017: heap size 625 MB, throughput 0.504508
Reading from 9017: heap size 625 MB, throughput 0.477219
Reading from 9018: heap size 116 MB, throughput 0.996385
Reading from 9018: heap size 119 MB, throughput 0.997518
Reading from 9017: heap size 627 MB, throughput 0.0957757
Reading from 9017: heap size 693 MB, throughput 0.479056
Reading from 9018: heap size 119 MB, throughput 0.997093
Reading from 9017: heap size 694 MB, throughput 0.836596
Reading from 9018: heap size 121 MB, throughput 0.998398
Reading from 9017: heap size 692 MB, throughput 0.852486
Reading from 9018: heap size 122 MB, throughput 0.996653
Reading from 9017: heap size 695 MB, throughput 0.868975
Reading from 9017: heap size 699 MB, throughput 0.479419
Reading from 9018: heap size 123 MB, throughput 0.996426
Equal recommendation: 2446 MB each
Reading from 9018: heap size 123 MB, throughput 0.997348
Reading from 9017: heap size 709 MB, throughput 0.0926226
Reading from 9018: heap size 125 MB, throughput 0.997368
Reading from 9017: heap size 786 MB, throughput 0.387454
Reading from 9017: heap size 715 MB, throughput 0.408956
Reading from 9017: heap size 777 MB, throughput 0.412225
Reading from 9017: heap size 781 MB, throughput 0.774788
Reading from 9018: heap size 125 MB, throughput 0.998024
Reading from 9017: heap size 778 MB, throughput 0.820519
Reading from 9017: heap size 782 MB, throughput 0.560642
Reading from 9018: heap size 126 MB, throughput 0.997946
Reading from 9017: heap size 780 MB, throughput 0.0670614
Reading from 9018: heap size 127 MB, throughput 0.998275
Reading from 9017: heap size 856 MB, throughput 0.506619
Reading from 9018: heap size 129 MB, throughput 0.995105
Reading from 9017: heap size 862 MB, throughput 0.62488
Reading from 9017: heap size 863 MB, throughput 0.734853
Reading from 9018: heap size 129 MB, throughput 0.99401
Reading from 9017: heap size 871 MB, throughput 0.797367
Reading from 9018: heap size 130 MB, throughput 0.984991
Reading from 9018: heap size 131 MB, throughput 0.99698
Reading from 9017: heap size 872 MB, throughput 0.151786
Reading from 9017: heap size 964 MB, throughput 0.772417
Reading from 9017: heap size 968 MB, throughput 0.83227
Reading from 9018: heap size 135 MB, throughput 0.998006
Reading from 9017: heap size 973 MB, throughput 0.804877
Reading from 9017: heap size 975 MB, throughput 0.84863
Reading from 9017: heap size 973 MB, throughput 0.850125
Reading from 9017: heap size 977 MB, throughput 0.851721
Reading from 9017: heap size 971 MB, throughput 0.854813
Reading from 9018: heap size 136 MB, throughput 0.994096
Reading from 9017: heap size 975 MB, throughput 0.852039
Reading from 9017: heap size 970 MB, throughput 0.868736
Reading from 9018: heap size 139 MB, throughput 0.997414
Reading from 9017: heap size 974 MB, throughput 0.956905
Reading from 9018: heap size 139 MB, throughput 0.996613
Reading from 9017: heap size 959 MB, throughput 0.964023
Reading from 9018: heap size 142 MB, throughput 0.997374
Reading from 9017: heap size 968 MB, throughput 0.748573
Equal recommendation: 2446 MB each
Reading from 9017: heap size 976 MB, throughput 0.773902
Reading from 9017: heap size 983 MB, throughput 0.768764
Reading from 9017: heap size 993 MB, throughput 0.780552
Reading from 9018: heap size 142 MB, throughput 0.997549
Reading from 9017: heap size 996 MB, throughput 0.170028
Reading from 9018: heap size 144 MB, throughput 0.997283
Reading from 9017: heap size 1098 MB, throughput 0.62215
Reading from 9017: heap size 1104 MB, throughput 0.875323
Reading from 9017: heap size 1116 MB, throughput 0.876717
Reading from 9018: heap size 145 MB, throughput 0.9968
Reading from 9017: heap size 1118 MB, throughput 0.873489
Reading from 9017: heap size 1110 MB, throughput 0.874656
Reading from 9017: heap size 1118 MB, throughput 0.79685
Reading from 9018: heap size 147 MB, throughput 0.997367
Reading from 9017: heap size 1119 MB, throughput 0.71812
Reading from 9017: heap size 1124 MB, throughput 0.685806
Reading from 9017: heap size 1138 MB, throughput 0.712476
Reading from 9017: heap size 1141 MB, throughput 0.692593
Reading from 9018: heap size 147 MB, throughput 0.997024
Reading from 9017: heap size 1156 MB, throughput 0.735893
Reading from 9017: heap size 1158 MB, throughput 0.710627
Reading from 9017: heap size 1173 MB, throughput 0.728359
Reading from 9017: heap size 1173 MB, throughput 0.707862
Reading from 9018: heap size 149 MB, throughput 0.997395
Reading from 9017: heap size 1192 MB, throughput 0.783601
Reading from 9018: heap size 149 MB, throughput 0.996584
Reading from 9018: heap size 152 MB, throughput 0.997497
Reading from 9017: heap size 1192 MB, throughput 0.974122
Reading from 9018: heap size 152 MB, throughput 0.998221
Reading from 9018: heap size 153 MB, throughput 0.998548
Reading from 9017: heap size 1204 MB, throughput 0.964715
Equal recommendation: 2446 MB each
Reading from 9018: heap size 154 MB, throughput 0.99849
Reading from 9018: heap size 156 MB, throughput 0.99861
Reading from 9017: heap size 1209 MB, throughput 0.94565
Reading from 9018: heap size 156 MB, throughput 0.996572
Reading from 9018: heap size 157 MB, throughput 0.994044
Reading from 9018: heap size 157 MB, throughput 0.989711
Reading from 9018: heap size 160 MB, throughput 0.998275
Reading from 9017: heap size 1206 MB, throughput 0.961807
Reading from 9018: heap size 160 MB, throughput 0.997584
Reading from 9018: heap size 162 MB, throughput 0.997934
Reading from 9017: heap size 1214 MB, throughput 0.964109
Reading from 9018: heap size 163 MB, throughput 0.997554
Reading from 9018: heap size 165 MB, throughput 0.997828
Reading from 9017: heap size 1202 MB, throughput 0.961907
Reading from 9018: heap size 165 MB, throughput 0.995804
Reading from 9018: heap size 168 MB, throughput 0.997793
Equal recommendation: 2446 MB each
Reading from 9017: heap size 1211 MB, throughput 0.959399
Reading from 9018: heap size 168 MB, throughput 0.997743
Reading from 9018: heap size 170 MB, throughput 0.997577
Reading from 9017: heap size 1196 MB, throughput 0.961001
Reading from 9018: heap size 170 MB, throughput 0.997143
Reading from 9018: heap size 172 MB, throughput 0.998043
Reading from 9017: heap size 1205 MB, throughput 0.956603
Reading from 9018: heap size 172 MB, throughput 0.997334
Reading from 9018: heap size 175 MB, throughput 0.997618
Reading from 9017: heap size 1203 MB, throughput 0.949335
Reading from 9018: heap size 175 MB, throughput 0.930461
Reading from 9018: heap size 181 MB, throughput 0.998837
Reading from 9018: heap size 181 MB, throughput 0.99678
Reading from 9017: heap size 1206 MB, throughput 0.938212
Reading from 9018: heap size 184 MB, throughput 0.99441
Equal recommendation: 2446 MB each
Reading from 9018: heap size 184 MB, throughput 0.994398
Reading from 9018: heap size 188 MB, throughput 0.998116
Reading from 9017: heap size 1214 MB, throughput 0.961949
Reading from 9018: heap size 188 MB, throughput 0.997412
Reading from 9018: heap size 191 MB, throughput 0.998135
Reading from 9017: heap size 1214 MB, throughput 0.956807
Reading from 9018: heap size 192 MB, throughput 0.998048
Reading from 9018: heap size 194 MB, throughput 0.997937
Reading from 9017: heap size 1222 MB, throughput 0.963324
Reading from 9018: heap size 195 MB, throughput 0.99763
Reading from 9018: heap size 198 MB, throughput 0.997851
Reading from 9017: heap size 1225 MB, throughput 0.958283
Reading from 9018: heap size 198 MB, throughput 0.997822
Equal recommendation: 2446 MB each
Reading from 9018: heap size 202 MB, throughput 0.997847
Reading from 9017: heap size 1231 MB, throughput 0.961378
Reading from 9018: heap size 202 MB, throughput 0.997532
Reading from 9018: heap size 204 MB, throughput 0.997841
Reading from 9017: heap size 1236 MB, throughput 0.946695
Reading from 9018: heap size 205 MB, throughput 0.997677
Reading from 9018: heap size 208 MB, throughput 0.997819
Reading from 9017: heap size 1244 MB, throughput 0.954181
Reading from 9018: heap size 208 MB, throughput 0.992939
Reading from 9018: heap size 211 MB, throughput 0.993429
Reading from 9018: heap size 212 MB, throughput 0.997822
Reading from 9017: heap size 1248 MB, throughput 0.961786
Reading from 9018: heap size 216 MB, throughput 0.998381
Equal recommendation: 2446 MB each
Reading from 9018: heap size 217 MB, throughput 0.997799
Reading from 9017: heap size 1257 MB, throughput 0.962793
Reading from 9018: heap size 221 MB, throughput 0.997794
Reading from 9018: heap size 222 MB, throughput 0.997859
Reading from 9018: heap size 225 MB, throughput 0.993158
Reading from 9017: heap size 1259 MB, throughput 0.564501
Reading from 9018: heap size 226 MB, throughput 0.99786
Reading from 9018: heap size 230 MB, throughput 0.998193
Reading from 9017: heap size 1366 MB, throughput 0.952178
Reading from 9018: heap size 231 MB, throughput 0.997775
Equal recommendation: 2446 MB each
Reading from 9018: heap size 235 MB, throughput 0.998117
Reading from 9017: heap size 1367 MB, throughput 0.985397
Reading from 9018: heap size 235 MB, throughput 0.997861
Reading from 9018: heap size 238 MB, throughput 0.996539
Reading from 9017: heap size 1380 MB, throughput 0.974639
Reading from 9018: heap size 239 MB, throughput 0.990157
Reading from 9018: heap size 243 MB, throughput 0.998079
Reading from 9017: heap size 1383 MB, throughput 0.979985
Reading from 9018: heap size 244 MB, throughput 0.998072
Reading from 9018: heap size 248 MB, throughput 0.998463
Reading from 9017: heap size 1383 MB, throughput 0.978619
Reading from 9018: heap size 249 MB, throughput 0.998028
Equal recommendation: 2446 MB each
Reading from 9018: heap size 253 MB, throughput 0.998382
Reading from 9017: heap size 1387 MB, throughput 0.976619
Reading from 9018: heap size 254 MB, throughput 0.997999
Reading from 9017: heap size 1374 MB, throughput 0.975712
Reading from 9018: heap size 258 MB, throughput 0.998283
Reading from 9018: heap size 258 MB, throughput 0.997843
Reading from 9017: heap size 1292 MB, throughput 0.974521
Reading from 9018: heap size 262 MB, throughput 0.998242
Reading from 9018: heap size 262 MB, throughput 0.997889
Equal recommendation: 2446 MB each
Reading from 9018: heap size 266 MB, throughput 0.993947
Reading from 9017: heap size 1368 MB, throughput 0.968915
Reading from 9018: heap size 266 MB, throughput 0.996455
Reading from 9018: heap size 272 MB, throughput 0.998156
Reading from 9017: heap size 1374 MB, throughput 0.971568
Reading from 9018: heap size 272 MB, throughput 0.997948
Reading from 9017: heap size 1377 MB, throughput 0.969614
Reading from 9018: heap size 276 MB, throughput 0.998528
Reading from 9018: heap size 277 MB, throughput 0.998305
Reading from 9017: heap size 1378 MB, throughput 0.965946
Reading from 9018: heap size 281 MB, throughput 0.998465
Equal recommendation: 2446 MB each
Reading from 9018: heap size 281 MB, throughput 0.998176
Reading from 9017: heap size 1383 MB, throughput 0.959923
Reading from 9018: heap size 285 MB, throughput 0.998401
Reading from 9018: heap size 285 MB, throughput 0.998118
Reading from 9017: heap size 1389 MB, throughput 0.963949
Reading from 9018: heap size 289 MB, throughput 0.997688
Reading from 9018: heap size 289 MB, throughput 0.994105
Reading from 9017: heap size 1396 MB, throughput 0.964116
Reading from 9018: heap size 292 MB, throughput 0.998345
Equal recommendation: 2446 MB each
Reading from 9018: heap size 293 MB, throughput 0.997928
Reading from 9017: heap size 1404 MB, throughput 0.960314
Reading from 9018: heap size 297 MB, throughput 0.998131
Reading from 9018: heap size 297 MB, throughput 0.998012
Reading from 9018: heap size 302 MB, throughput 0.998413
Reading from 9018: heap size 302 MB, throughput 0.998263
Equal recommendation: 2446 MB each
Reading from 9018: heap size 307 MB, throughput 0.998132
Reading from 9018: heap size 307 MB, throughput 0.997751
Reading from 9018: heap size 311 MB, throughput 0.930763
Reading from 9018: heap size 318 MB, throughput 0.998072
Reading from 9018: heap size 324 MB, throughput 0.999094
Reading from 9018: heap size 324 MB, throughput 0.999017
Equal recommendation: 2446 MB each
Reading from 9018: heap size 329 MB, throughput 0.999108
Reading from 9018: heap size 330 MB, throughput 0.998843
Reading from 9018: heap size 334 MB, throughput 0.999029
Reading from 9017: heap size 1415 MB, throughput 0.994543
Reading from 9018: heap size 334 MB, throughput 0.998948
Reading from 9018: heap size 338 MB, throughput 0.999101
Equal recommendation: 2446 MB each
Reading from 9018: heap size 338 MB, throughput 0.995758
Reading from 9018: heap size 341 MB, throughput 0.997993
Reading from 9017: heap size 1422 MB, throughput 0.980059
Reading from 9018: heap size 342 MB, throughput 0.998351
Reading from 9017: heap size 1407 MB, throughput 0.444872
Reading from 9017: heap size 1471 MB, throughput 0.938237
Reading from 9017: heap size 1491 MB, throughput 0.914828
Reading from 9017: heap size 1495 MB, throughput 0.899521
Reading from 9017: heap size 1497 MB, throughput 0.918226
Reading from 9018: heap size 348 MB, throughput 0.998672
Reading from 9017: heap size 1501 MB, throughput 0.919019
Reading from 9017: heap size 1501 MB, throughput 0.988152
Reading from 9018: heap size 348 MB, throughput 0.99835
Equal recommendation: 2446 MB each
Reading from 9018: heap size 353 MB, throughput 0.998524
Reading from 9017: heap size 1508 MB, throughput 0.990656
Reading from 9018: heap size 354 MB, throughput 0.998267
Reading from 9017: heap size 1528 MB, throughput 0.987901
Reading from 9018: heap size 358 MB, throughput 0.998489
Reading from 9018: heap size 359 MB, throughput 0.997501
Reading from 9017: heap size 1530 MB, throughput 0.986038
Reading from 9018: heap size 364 MB, throughput 0.99622
Equal recommendation: 2446 MB each
Reading from 9018: heap size 364 MB, throughput 0.998226
Reading from 9017: heap size 1522 MB, throughput 0.98378
Reading from 9018: heap size 371 MB, throughput 0.998613
Reading from 9017: heap size 1531 MB, throughput 0.983083
Reading from 9018: heap size 372 MB, throughput 0.998361
Reading from 9017: heap size 1508 MB, throughput 0.980068
Reading from 9018: heap size 378 MB, throughput 0.998508
Reading from 9018: heap size 378 MB, throughput 0.998364
Equal recommendation: 2446 MB each
Reading from 9017: heap size 1384 MB, throughput 0.977963
Reading from 9018: heap size 383 MB, throughput 0.998404
Reading from 9018: heap size 384 MB, throughput 0.996583
Reading from 9017: heap size 1497 MB, throughput 0.977159
Reading from 9018: heap size 389 MB, throughput 0.997805
Reading from 9017: heap size 1507 MB, throughput 0.976015
Reading from 9018: heap size 390 MB, throughput 0.998366
Equal recommendation: 2446 MB each
Reading from 9018: heap size 397 MB, throughput 0.998649
Reading from 9017: heap size 1512 MB, throughput 0.972022
Reading from 9018: heap size 397 MB, throughput 0.99829
Reading from 9017: heap size 1512 MB, throughput 0.973075
Reading from 9018: heap size 403 MB, throughput 0.998611
Reading from 9018: heap size 403 MB, throughput 0.997118
Reading from 9017: heap size 1518 MB, throughput 0.971088
Reading from 9018: heap size 410 MB, throughput 0.997794
Equal recommendation: 2446 MB each
Reading from 9018: heap size 410 MB, throughput 0.995719
Reading from 9017: heap size 1525 MB, throughput 0.967685
Reading from 9018: heap size 417 MB, throughput 0.998658
Reading from 9017: heap size 1534 MB, throughput 0.96746
Reading from 9018: heap size 417 MB, throughput 0.998528
Reading from 9017: heap size 1544 MB, throughput 0.961129
Reading from 9018: heap size 424 MB, throughput 0.998608
Equal recommendation: 2446 MB each
Reading from 9018: heap size 425 MB, throughput 0.9985
Reading from 9017: heap size 1557 MB, throughput 0.964627
Reading from 9018: heap size 431 MB, throughput 0.998525
Reading from 9017: heap size 1568 MB, throughput 0.962404
Reading from 9018: heap size 432 MB, throughput 0.996442
Reading from 9018: heap size 438 MB, throughput 0.997577
Reading from 9017: heap size 1583 MB, throughput 0.962334
Equal recommendation: 2446 MB each
Reading from 9018: heap size 439 MB, throughput 0.99862
Reading from 9017: heap size 1590 MB, throughput 0.964128
Reading from 9018: heap size 446 MB, throughput 0.998756
Reading from 9017: heap size 1605 MB, throughput 0.962107
Reading from 9018: heap size 447 MB, throughput 0.99857
Reading from 9018: heap size 453 MB, throughput 0.998589
Equal recommendation: 2446 MB each
Reading from 9017: heap size 1609 MB, throughput 0.96331
Reading from 9018: heap size 454 MB, throughput 0.99862
Reading from 9018: heap size 460 MB, throughput 0.995736
Reading from 9017: heap size 1623 MB, throughput 0.965169
Reading from 9018: heap size 460 MB, throughput 0.998225
Reading from 9017: heap size 1625 MB, throughput 0.96347
Reading from 9018: heap size 469 MB, throughput 0.998733
Equal recommendation: 2446 MB each
Reading from 9017: heap size 1640 MB, throughput 0.965587
Reading from 9018: heap size 470 MB, throughput 0.998503
Reading from 9018: heap size 477 MB, throughput 0.998675
Reading from 9017: heap size 1641 MB, throughput 0.749857
Reading from 9018: heap size 478 MB, throughput 0.998424
Equal recommendation: 2446 MB each
Client 9018 died
Clients: 1
Reading from 9017: heap size 1795 MB, throughput 0.972571
Recommendation: one client; give it all the memory
Reading from 9017: heap size 1796 MB, throughput 0.98929
Recommendation: one client; give it all the memory
Reading from 9017: heap size 1798 MB, throughput 0.995058
Recommendation: one client; give it all the memory
Reading from 9017: heap size 1811 MB, throughput 0.893076
Reading from 9017: heap size 1796 MB, throughput 0.838932
Reading from 9017: heap size 1812 MB, throughput 0.823202
Reading from 9017: heap size 1848 MB, throughput 0.885794
Client 9017 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
