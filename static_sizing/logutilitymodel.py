# A utility model based on U(h) = a ln(bh)
# Combined utility for multiple VMs is found by multiplying the individual utilities

import math
import numpy as np
import operator
import staticutility as su
import utilitymodel

def name():
    return "log"

class LogUtilityModel(utilitymodel.UtilityModel):
    def modelName(self):
        return name()

    class Params:
        def __init__(self, a, b):
            self.a = a
            self.b = b

        def save(self, filename):
            with open(filename, "w") as f:
                f.write(str(self.a) + "\n")
                f.write(str(self.b) + "\n")

        def calculate(self, heap):
            """Heap can be a scalar or a sequence"""
            return self.a * np.log(self.b * heap)

        def __str__(self):
            return "y = %g ln(%gx)" % (self.a, self.b)

        def __repr__(self):
            return str(self)

        def pretty(self):
            return "T = %.3g ln(%.3gh)" % (self.a, self.b)


    def testingParams(self):
        return self.Params(0.5, 0.5)

    def curveFit(self, xdata, ydata):
        (c, a) = su.fitStraightLine(map(math.log, xdata), ydata)
        return self.Params(a, math.exp(c/a))

    def calculateBest(self, totalMem, params, minheaps):
        return self.calculateBestNumeric(totalMem, params, minheaps)

    def combineRawUtilities(self, *u):
        return reduce(operator.mul, u, 1)

    def propagateRawErrors(self, itersPerSecs, U, stddevItersPerSecs):
        return U * math.sqrt(sum(map(lambda (x, dx): (dx / x) ** 2, zip(itersPerSecs, stddevItersPerSecs))))
