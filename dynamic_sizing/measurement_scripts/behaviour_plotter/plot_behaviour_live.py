"""Plot readings from stdin as they arrive"""

import argparse
import matplotlib.pyplot as plt
import behaviour_plotter as p
import sys

class LivePlotter(p.BehaviourPlotter):
    def __init__(self, throughput_model):
        super(LivePlotter, self).__init__(throughput_model, sys.stdin, None, True, True, None, False)
        plt.ion()

    def begin(self):
        plt.show()

    def end_iteration(self):
        self.draw_clients()
        plt.draw()

    def end(self):
        plt.ioff()
        plt.show()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot readings from stdin as they arrive")
    parser.add_argument("throughput_model", help="The name of the throughput model used to generate the readings")

    args = parser.parse_args()

    LivePlotter(args.throughput_model).plot()
