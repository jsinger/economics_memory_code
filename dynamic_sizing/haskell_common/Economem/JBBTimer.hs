module Economem.JBBTimer (JBBTimerDef(..),
                          JBBTimerDir(..),
                          newJBBTimer,
                          resumeJBBTimer) where

import Control.Applicative
import qualified Data.ByteString.Char8 as B
import qualified Data.Csv as C
import Economem.Common
import Economem.Experiment
import Economem.Files
import Economem.Java
import Economem.Log
import Economem.Records
import qualified Economem.TimerComparisonMany as M
import System.Directory

name :: String
name = "jbbTimer"

data JBBTimerDef =
  JBBTimerDef { jbbGroups :: Int,
                jbbInjectorsPerGroup :: Int,
                jbbMaxHeap :: Int} deriving (Show, Read)
instance C.FromNamedRecord JBBTimerDef where
  parseNamedRecord r = JBBTimerDef <$>
                       r C..: B.pack "GROUPS" <*>
                       r C..: B.pack "INJECTORS" <*>
                       r C..: B.pack "SIZE_MB"
instance C.ToNamedRecord JBBTimerDef where
  toNamedRecord bm = C.namedRecord (getNamedRecordInput bm)
instance CsvFields JBBTimerDef where
  getCsvFieldNames _ = [B.pack "GROUPS", B.pack "INJECTORS", B.pack "SIZE_MB"]
  getNamedRecordInput (JBBTimerDef g i h) = [B.pack "GROUPS" C..= g,
                                             B.pack "INJECTORS" C..= i,
                                             B.pack "SIZE_MB" C..= h]


data JBBTimerDir =
  JBBTimerDir { dirPath :: FilePath,
                inputFile :: CsvFile JBBTimerDef,
                timersFile :: LinesFile TimerType,
                minHeapsFile :: CsvFile BenchmarkThreadsMB,
                runsFile :: IntFile,
                javaNameFile :: StringFile,
                resultsFile :: CsvFile M.TimerComparisonResult,
                prettyResultsFile :: LinesFile String }

instance Path JBBTimerDir where
  getPath = dirPath

instance Dir JBBTimerDir where
  asDir path =
    JBBTimerDir { dirPath = getPath path,
                  inputFile = CsvFile (path </> "input.txt"),
                  timersFile = LinesFile (path </> "timers.txt"),
                  minHeapsFile = CsvFile (path </> "min_heaps.csv"),
                  runsFile = WholeFile (path </> "runs.txt"),
                  javaNameFile = WholeFile (path </> "java_name.txt"),
                  resultsFile = CsvFile (path </> "results.csv"),
                  prettyResultsFile = LinesFile (path </> "results_pretty.txt") }

newJBBTimer :: Input JBBTimerDef ->
               [TimerType] ->
               Maybe (Input BenchmarkThreadsMB) ->
               Integer ->
               Java ->
               Maybe SubexpInfo ->
               IO Experiment
newJBBTimer input timers minHeaps runs j info = do
  (expDir, num, parentLog) <- newExperimentProperties name info
  makeJBBTimer expDir num input timers minHeaps runs j parentLog


resumeJBBTimer :: FilePath -> IO Experiment
resumeJBBTimer =
  resumeExperiment
  (\expDir -> do let jbbDir = asDir expDir
                 let input = FromFile (inputFile jbbDir)
                 timers <- get (timersFile jbbDir)
                 minHeaps <- maybeGet (minHeapsFile jbbDir)
                 runs <- get (runsFile jbbDir)
                 j <- javaFromName =<< get (javaNameFile jbbDir)
                 makeJBBTimer
                   expDir
                   topLevel
                   input
                   timers
                   minHeaps
                   runs
                   j
                   Nothing)


makeJBBTimer :: ExperimentDir ->
                ExperimentNum ->
                Input JBBTimerDef ->
                [TimerType] ->
                Maybe (Input BenchmarkThreadsMB) ->
                Integer ->
                Java ->
                Maybe Log ->
                IO Experiment
makeJBBTimer expDir expNum inputLoader timers minHeaps runs j parentLog = do
  let ex = makeExperiment name expDir expNum 0 parentLog
  let jbbDir = asDir expDir
  input <- loadInput inputLoader
  return (ex { saveAdditionalStaticMetadata = saveStatic jbbDir input,
               canResume = canResumeJBB jbbDir,
               createSubexperiments = createSubexps input,
               subexperimentCompleted = subexpCompleted jbbDir })

    where
      saveStatic :: JBBTimerDir -> [JBBTimerDef] -> IO ()
      saveStatic d input = do
        input |> inputFile d
        timers |> timersFile d
        maybePut (minHeapsFile d) minHeaps
        runs |> runsFile d
        javaName j |> javaNameFile d

      canResumeJBB :: JBBTimerDir -> IO Bool
      canResumeJBB d =
        and <$> mapM doesFileExist [getPath (inputFile d),
                                    getPath (timersFile d),
                                    getPath (runsFile d),
                                    getPath (javaNameFile d)]

      createSubexps :: [JBBTimerDef] -> Experiment -> IO [Experiment]
      createSubexps input e =
        pure <$> (M.newTimerComparisonMany
                  (Raw (map makeSubexp input))
                  timers
                  runs
                  j
                  minHeaps
                  False
                  False
                  (Just (SubexpInfo { subexpNum = makeNumAndMax 1 1,
                                      subexpParentDir = experimentDir e,
                                      subexpParentLog = experimentLog e,
                                      subexpType = Static }))
                  (map makeArgs input))

        where
          makeSubexp :: JBBTimerDef -> BenchmarkCombinationMB
          makeSubexp def =
            BenchmarkCombinationMB {
              bcmBenchmarks =
                 BenchmarkCombination
                 ([controller] ++
                  concatMap (makeGroup def) [1..jbbGroups def]),
              bcmMB = jbbMaxHeap def }

          controller :: BenchmarkThreadsIters
          controller =
            BenchmarkThreadsIters
            (BenchmarkThreads
             (Benchmark "jbb2013multiController" "default") 1) 1

          makeGroup :: JBBTimerDef -> Int -> [BenchmarkThreadsIters]
          makeGroup def i =
            [makeBM "jbb2013backend" i 0] ++
            map (makeBM "jbb2013txInjector" i) [1..jbbInjectorsPerGroup def]

          makeBM :: String -> Int -> Int -> BenchmarkThreadsIters
          makeBM n group vmID =
            BenchmarkThreadsIters
            (BenchmarkThreads (Benchmark (n ++ "-" ++ show group ++ "-" ++ show vmID) "default") 1) 1

          makeArgs :: JBBTimerDef -> [String]
          makeArgs def = ["-Dspecjbb.group.count=" ++ show (jbbGroups def),
                          "-Dspecjbb.txi.pergroup.count=" ++ show (jbbInjectorsPerGroup def)]



      subexpCompleted :: JBBTimerDir -> Experiment -> Experiment -> IO ()
      subexpCompleted d _ sub = do
        let subDir = asDir (experimentDir sub)
        copyFile (getPath (M.resultsFile subDir)) (getPath (resultsFile d))
        copyFile (getPath (M.prettyResultsFile subDir)) (getPath (prettyResultsFile d))
