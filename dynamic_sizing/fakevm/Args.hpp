#ifndef ARGS_HPP
#define ARGS_HPP

#include <cstddef>

class Args
{
private:
    static const char* s_ProgramFile;
    static unsigned long long s_InitialHeapSizeMB;
    static const char* s_ReceiverName;

    static void usage();
    static void help();

public:
    static const char* getProgramFile();
    static unsigned long long getInitialHeapSizeMB();
    static const char* getReceiverName();
    static void parse(int argc, char** argv);
};

#endif
