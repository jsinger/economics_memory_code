#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") total_mem [daemon_args...]"
    exit 1
}

test -z "${1}" && usage
TOTAL_MEM="${1}"

function cleanup()
{
    test ! -z "${SIMPLE}" && kill "${SIMPLE}" &>/dev/null
    test ! -z "${VENGEROV}" && kill "${VENGEROV}" &>/dev/null
    test ! -z "${AVG_VENGEROV}" && kill "${AVG_VENGEROV}" &>/dev/null
    test ! -z "${MAJOR_ONLY}" && kill "${MAJOR_ONLY}" &>/dev/null
    test ! -z "${SIMPLE_SMOOTHING}" && kill "${SIMPLE_SMOOTHING}" &>/dev/null
    test ! -z "${PROPORTIONAL}" && kill "${PROPORTIONAL}" &>/dev/null
    wait
    exit
}

trap "cleanup" SIGINT SIGHUP SIGTERM

"${DIR}/live_with_daemon.sh" 'simple' "${TOTAL_MEM}" "${@:2}" &
SIMPLE="${!}"

"${DIR}/live_with_daemon.sh" 'vengerov' "${TOTAL_MEM}" '--dont-recommend' "${@:2}" &
VENGEROV="${!}"

"${DIR}/live_with_daemon.sh" 'avgVengerov' "${TOTAL_MEM}" '--dont-recommend' "${@:2}" &
AVG_VENGEROV="${!}"

"${DIR}/live_with_daemon.sh" 'majorOnly' "${TOTAL_MEM}" '--dont-recommend' "${@:2}" &
MAJOR_ONLY="${!}"

"${DIR}/live_with_daemon.sh" 'simpleSmoothing' "${TOTAL_MEM}" '--dont-recommend' "${@:2}" &
SIMPLE_SMOOTHING="${!}"

"${DIR}/live_with_daemon.sh" 'proportional' "${TOTAL_MEM}" '--dont-recommend' "${@:2}" &
PROPORTIONAL="${!}"

wait
cleanup
