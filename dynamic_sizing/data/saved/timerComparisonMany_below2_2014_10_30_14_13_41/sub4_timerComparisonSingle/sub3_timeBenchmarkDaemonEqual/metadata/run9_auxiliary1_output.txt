economemd
    total memory: 938 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_30_14_13_41/sub4_timerComparisonSingle/sub3_timeBenchmarkDaemonEqual/daemon_run09_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 5146: heap size 9 MB, throughput 0.991921
Clients: 1
Client 5146 has a minimum heap size of 276 MB
Reading from 5147: heap size 9 MB, throughput 0.985622
Clients: 2
Client 5147 has a minimum heap size of 276 MB
Reading from 5147: heap size 9 MB, throughput 0.976245
Reading from 5146: heap size 9 MB, throughput 0.955688
Reading from 5146: heap size 9 MB, throughput 0.958064
Reading from 5147: heap size 11 MB, throughput 0.966579
Reading from 5146: heap size 9 MB, throughput 0.930248
Reading from 5147: heap size 11 MB, throughput 0.947186
Reading from 5146: heap size 11 MB, throughput 0.982683
Reading from 5146: heap size 11 MB, throughput 0.98062
Reading from 5147: heap size 15 MB, throughput 0.870911
Reading from 5146: heap size 17 MB, throughput 0.928368
Reading from 5147: heap size 19 MB, throughput 0.978284
Reading from 5146: heap size 17 MB, throughput 0.655681
Reading from 5147: heap size 25 MB, throughput 0.933527
Reading from 5146: heap size 30 MB, throughput 0.908992
Reading from 5146: heap size 31 MB, throughput 0.913056
Reading from 5147: heap size 29 MB, throughput 0.968517
Reading from 5146: heap size 35 MB, throughput 0.383887
Reading from 5147: heap size 32 MB, throughput 0.477395
Reading from 5146: heap size 48 MB, throughput 0.852843
Reading from 5147: heap size 42 MB, throughput 0.903536
Reading from 5146: heap size 49 MB, throughput 0.889551
Reading from 5147: heap size 46 MB, throughput 0.891795
Reading from 5147: heap size 48 MB, throughput 0.634985
Reading from 5146: heap size 51 MB, throughput 0.362384
Reading from 5146: heap size 74 MB, throughput 0.716315
Reading from 5147: heap size 51 MB, throughput 0.18342
Reading from 5147: heap size 67 MB, throughput 0.192442
Reading from 5146: heap size 74 MB, throughput 0.230406
Reading from 5147: heap size 91 MB, throughput 0.831044
Reading from 5146: heap size 101 MB, throughput 0.758265
Reading from 5147: heap size 93 MB, throughput 0.579136
Reading from 5146: heap size 102 MB, throughput 0.781761
Reading from 5147: heap size 96 MB, throughput 0.764683
Reading from 5146: heap size 104 MB, throughput 0.717819
Reading from 5147: heap size 98 MB, throughput 0.775239
Reading from 5147: heap size 104 MB, throughput 0.69902
Reading from 5146: heap size 107 MB, throughput 0.27908
Reading from 5146: heap size 134 MB, throughput 0.588158
Reading from 5146: heap size 138 MB, throughput 0.735105
Reading from 5146: heap size 141 MB, throughput 0.737885
Reading from 5147: heap size 107 MB, throughput 0.173742
Reading from 5147: heap size 140 MB, throughput 0.577163
Reading from 5147: heap size 142 MB, throughput 0.676655
Reading from 5146: heap size 144 MB, throughput 0.16254
Reading from 5147: heap size 146 MB, throughput 0.767532
Reading from 5147: heap size 151 MB, throughput 0.607237
Reading from 5146: heap size 185 MB, throughput 0.649728
Reading from 5147: heap size 157 MB, throughput 0.566769
Reading from 5146: heap size 187 MB, throughput 0.728594
Reading from 5147: heap size 164 MB, throughput 0.456388
Reading from 5146: heap size 189 MB, throughput 0.73211
Reading from 5146: heap size 195 MB, throughput 0.61589
Reading from 5147: heap size 171 MB, throughput 0.107526
Reading from 5147: heap size 209 MB, throughput 0.593505
Reading from 5147: heap size 216 MB, throughput 0.630809
Reading from 5146: heap size 198 MB, throughput 0.917138
Reading from 5147: heap size 217 MB, throughput 0.370318
Reading from 5146: heap size 204 MB, throughput 0.309681
Reading from 5146: heap size 260 MB, throughput 0.628211
Reading from 5146: heap size 262 MB, throughput 0.856178
Reading from 5147: heap size 257 MB, throughput 0.909031
Reading from 5146: heap size 266 MB, throughput 0.614213
Reading from 5147: heap size 258 MB, throughput 0.809251
Reading from 5146: heap size 267 MB, throughput 0.871016
Reading from 5147: heap size 262 MB, throughput 0.717321
Reading from 5147: heap size 263 MB, throughput 0.644759
Reading from 5146: heap size 273 MB, throughput 0.860566
Reading from 5147: heap size 267 MB, throughput 0.804657
Reading from 5146: heap size 273 MB, throughput 0.866739
Reading from 5147: heap size 268 MB, throughput 0.822773
Reading from 5146: heap size 275 MB, throughput 0.72447
Reading from 5146: heap size 279 MB, throughput 0.727633
Reading from 5147: heap size 272 MB, throughput 0.8342
Reading from 5146: heap size 286 MB, throughput 0.76593
Reading from 5147: heap size 273 MB, throughput 0.711531
Reading from 5146: heap size 288 MB, throughput 0.754331
Reading from 5147: heap size 279 MB, throughput 0.70752
Reading from 5147: heap size 282 MB, throughput 0.758046
Reading from 5147: heap size 284 MB, throughput 0.793063
Reading from 5146: heap size 296 MB, throughput 0.934156
Reading from 5146: heap size 297 MB, throughput 0.739433
Reading from 5147: heap size 286 MB, throughput 0.897299
Reading from 5146: heap size 298 MB, throughput 0.704709
Reading from 5146: heap size 300 MB, throughput 0.0848771
Reading from 5147: heap size 283 MB, throughput 0.207879
Reading from 5146: heap size 348 MB, throughput 0.781917
Reading from 5147: heap size 325 MB, throughput 0.614525
Reading from 5147: heap size 325 MB, throughput 0.7986
Reading from 5146: heap size 349 MB, throughput 0.878867
Reading from 5147: heap size 326 MB, throughput 0.784696
Reading from 5146: heap size 352 MB, throughput 0.673455
Reading from 5146: heap size 353 MB, throughput 0.56982
Reading from 5147: heap size 331 MB, throughput 0.915137
Reading from 5146: heap size 361 MB, throughput 0.656954
Reading from 5146: heap size 361 MB, throughput 0.678727
Reading from 5147: heap size 331 MB, throughput 0.884968
Reading from 5146: heap size 371 MB, throughput 0.582348
Reading from 5147: heap size 332 MB, throughput 0.6789
Reading from 5147: heap size 333 MB, throughput 0.614766
Reading from 5147: heap size 339 MB, throughput 0.676679
Equal recommendation: 469 MB each
Reading from 5147: heap size 338 MB, throughput 0.604074
Reading from 5147: heap size 346 MB, throughput 0.717095
Reading from 5146: heap size 371 MB, throughput 0.950113
Reading from 5147: heap size 346 MB, throughput 0.946779
Reading from 5146: heap size 378 MB, throughput 0.986316
Reading from 5147: heap size 353 MB, throughput 0.975719
Reading from 5147: heap size 354 MB, throughput 0.937173
Reading from 5146: heap size 380 MB, throughput 0.964777
Reading from 5147: heap size 356 MB, throughput 0.976093
Reading from 5146: heap size 381 MB, throughput 0.97093
Reading from 5146: heap size 385 MB, throughput 0.967088
Reading from 5147: heap size 359 MB, throughput 0.679727
Reading from 5147: heap size 409 MB, throughput 0.971358
Reading from 5146: heap size 388 MB, throughput 0.976386
Equal recommendation: 469 MB each
Reading from 5147: heap size 409 MB, throughput 0.989034
Reading from 5146: heap size 389 MB, throughput 0.978168
Reading from 5147: heap size 416 MB, throughput 0.991229
Reading from 5146: heap size 388 MB, throughput 0.976618
Reading from 5147: heap size 417 MB, throughput 0.987155
Reading from 5146: heap size 391 MB, throughput 0.976694
Reading from 5147: heap size 416 MB, throughput 0.985932
Reading from 5146: heap size 391 MB, throughput 0.973285
Reading from 5147: heap size 418 MB, throughput 0.983449
Reading from 5146: heap size 392 MB, throughput 0.975146
Reading from 5147: heap size 413 MB, throughput 0.982221
Reading from 5146: heap size 395 MB, throughput 0.973468
Equal recommendation: 469 MB each
Reading from 5147: heap size 384 MB, throughput 0.981504
Reading from 5146: heap size 396 MB, throughput 0.983256
Reading from 5146: heap size 400 MB, throughput 0.957139
Reading from 5146: heap size 400 MB, throughput 0.777209
Reading from 5147: heap size 412 MB, throughput 0.984341
Reading from 5146: heap size 404 MB, throughput 0.818058
Reading from 5146: heap size 407 MB, throughput 0.855763
Reading from 5147: heap size 414 MB, throughput 0.924642
Reading from 5147: heap size 412 MB, throughput 0.77806
Reading from 5147: heap size 418 MB, throughput 0.829302
Reading from 5147: heap size 426 MB, throughput 0.728353
Reading from 5147: heap size 429 MB, throughput 0.967603
Reading from 5146: heap size 417 MB, throughput 0.987657
Reading from 5147: heap size 434 MB, throughput 0.985222
Reading from 5146: heap size 417 MB, throughput 0.982675
Reading from 5147: heap size 438 MB, throughput 0.985144
Reading from 5146: heap size 419 MB, throughput 0.986035
Reading from 5147: heap size 435 MB, throughput 0.982483
Equal recommendation: 469 MB each
Reading from 5146: heap size 421 MB, throughput 0.986582
Reading from 5147: heap size 439 MB, throughput 0.983616
Reading from 5146: heap size 421 MB, throughput 0.985763
Reading from 5147: heap size 435 MB, throughput 0.986599
Reading from 5146: heap size 423 MB, throughput 0.984376
Reading from 5147: heap size 438 MB, throughput 0.981997
Reading from 5146: heap size 420 MB, throughput 0.982402
Reading from 5147: heap size 435 MB, throughput 0.982387
Reading from 5146: heap size 423 MB, throughput 0.983714
Reading from 5147: heap size 438 MB, throughput 0.982963
Reading from 5146: heap size 422 MB, throughput 0.982158
Equal recommendation: 469 MB each
Reading from 5147: heap size 437 MB, throughput 0.984416
Reading from 5146: heap size 423 MB, throughput 0.988967
Reading from 5146: heap size 422 MB, throughput 0.511389
Reading from 5147: heap size 439 MB, throughput 0.993993
Reading from 5146: heap size 464 MB, throughput 0.755711
Reading from 5146: heap size 463 MB, throughput 0.961284
Reading from 5147: heap size 440 MB, throughput 0.976119
Reading from 5147: heap size 441 MB, throughput 0.895941
Reading from 5146: heap size 467 MB, throughput 0.992142
Reading from 5147: heap size 441 MB, throughput 0.85786
Reading from 5147: heap size 442 MB, throughput 0.912378
Reading from 5147: heap size 449 MB, throughput 0.9895
Reading from 5146: heap size 474 MB, throughput 0.993805
Reading from 5147: heap size 450 MB, throughput 0.989268
Reading from 5146: heap size 422 MB, throughput 0.991992
Equal recommendation: 469 MB each
Reading from 5147: heap size 452 MB, throughput 0.990856
Reading from 5146: heap size 473 MB, throughput 0.992291
Reading from 5147: heap size 454 MB, throughput 0.988801
Reading from 5146: heap size 429 MB, throughput 0.990751
Reading from 5147: heap size 454 MB, throughput 0.987417
Reading from 5146: heap size 465 MB, throughput 0.988332
Reading from 5147: heap size 456 MB, throughput 0.983236
Reading from 5146: heap size 466 MB, throughput 0.984731
Reading from 5147: heap size 454 MB, throughput 0.986258
Reading from 5146: heap size 467 MB, throughput 0.984398
Equal recommendation: 469 MB each
Reading from 5147: heap size 456 MB, throughput 0.984811
Reading from 5146: heap size 467 MB, throughput 0.985472
Reading from 5146: heap size 469 MB, throughput 0.988958
Reading from 5147: heap size 458 MB, throughput 0.987903
Reading from 5146: heap size 471 MB, throughput 0.877906
Reading from 5146: heap size 467 MB, throughput 0.803146
Reading from 5147: heap size 459 MB, throughput 0.951847
Reading from 5146: heap size 474 MB, throughput 0.827317
Reading from 5147: heap size 459 MB, throughput 0.88774
Reading from 5147: heap size 461 MB, throughput 0.876077
Reading from 5146: heap size 475 MB, throughput 0.98533
Reading from 5147: heap size 467 MB, throughput 0.990183
Reading from 5146: heap size 436 MB, throughput 0.9845
Reading from 5147: heap size 468 MB, throughput 0.991385
Equal recommendation: 469 MB each
Reading from 5146: heap size 477 MB, throughput 0.987202
Reading from 5147: heap size 472 MB, throughput 0.992133
Reading from 5146: heap size 440 MB, throughput 0.986547
Reading from 5147: heap size 444 MB, throughput 0.989785
Reading from 5146: heap size 471 MB, throughput 0.983618
Reading from 5147: heap size 470 MB, throughput 0.985923
Reading from 5146: heap size 442 MB, throughput 0.982541
Reading from 5147: heap size 447 MB, throughput 0.98575
Reading from 5146: heap size 466 MB, throughput 0.979863
Equal recommendation: 469 MB each
Reading from 5147: heap size 464 MB, throughput 0.98664
Reading from 5146: heap size 464 MB, throughput 0.980754
Reading from 5147: heap size 463 MB, throughput 0.983763
Reading from 5146: heap size 464 MB, throughput 0.98217
Reading from 5147: heap size 465 MB, throughput 0.990569
Reading from 5147: heap size 466 MB, throughput 0.902704
Reading from 5146: heap size 466 MB, throughput 0.991039
Reading from 5147: heap size 466 MB, throughput 0.88666
Reading from 5146: heap size 468 MB, throughput 0.911161
Reading from 5146: heap size 468 MB, throughput 0.866727
Reading from 5146: heap size 474 MB, throughput 0.863365
Reading from 5147: heap size 468 MB, throughput 0.961785
Reading from 5146: heap size 458 MB, throughput 0.991691
Reading from 5147: heap size 476 MB, throughput 0.990688
Reading from 5146: heap size 470 MB, throughput 0.989123
Equal recommendation: 469 MB each
Reading from 5147: heap size 444 MB, throughput 0.991399
Reading from 5146: heap size 436 MB, throughput 0.988184
Reading from 5147: heap size 474 MB, throughput 0.989155
Reading from 5146: heap size 469 MB, throughput 0.987819
Reading from 5147: heap size 446 MB, throughput 0.989589
Reading from 5146: heap size 438 MB, throughput 0.987216
Reading from 5146: heap size 466 MB, throughput 0.967519
Reading from 5147: heap size 469 MB, throughput 0.975569
Reading from 5146: heap size 464 MB, throughput 0.983588
Reading from 5147: heap size 467 MB, throughput 0.985771
Equal recommendation: 469 MB each
Reading from 5146: heap size 463 MB, throughput 0.985968
Reading from 5147: heap size 469 MB, throughput 0.986192
Reading from 5146: heap size 465 MB, throughput 0.986021
Reading from 5147: heap size 469 MB, throughput 0.991187
Reading from 5147: heap size 457 MB, throughput 0.943904
Reading from 5147: heap size 465 MB, throughput 0.885373
Reading from 5147: heap size 469 MB, throughput 0.92057
Reading from 5146: heap size 467 MB, throughput 0.991386
Reading from 5146: heap size 468 MB, throughput 0.893438
Reading from 5146: heap size 470 MB, throughput 0.868392
Reading from 5146: heap size 458 MB, throughput 0.959764
Reading from 5147: heap size 471 MB, throughput 0.989643
Reading from 5146: heap size 469 MB, throughput 0.989303
Reading from 5147: heap size 457 MB, throughput 0.988777
Equal recommendation: 469 MB each
Reading from 5146: heap size 436 MB, throughput 0.989044
Reading from 5147: heap size 468 MB, throughput 0.988144
Reading from 5146: heap size 467 MB, throughput 0.989648
Reading from 5147: heap size 467 MB, throughput 0.988874
Reading from 5146: heap size 466 MB, throughput 0.992105
Reading from 5147: heap size 470 MB, throughput 0.989658
Reading from 5146: heap size 464 MB, throughput 0.98539
Reading from 5147: heap size 455 MB, throughput 0.984453
Reading from 5146: heap size 467 MB, throughput 0.983858
Equal recommendation: 469 MB each
Reading from 5146: heap size 467 MB, throughput 0.983412
Reading from 5147: heap size 463 MB, throughput 0.985854
Reading from 5146: heap size 468 MB, throughput 0.984961
Reading from 5147: heap size 465 MB, throughput 0.985086
Reading from 5147: heap size 466 MB, throughput 0.990176
Reading from 5147: heap size 470 MB, throughput 0.903185
Reading from 5147: heap size 460 MB, throughput 0.885666
Reading from 5146: heap size 470 MB, throughput 0.993229
Reading from 5146: heap size 444 MB, throughput 0.931693
Reading from 5146: heap size 465 MB, throughput 0.866041
Reading from 5146: heap size 468 MB, throughput 0.87492
Reading from 5147: heap size 466 MB, throughput 0.987746
Reading from 5146: heap size 475 MB, throughput 0.989345
Reading from 5147: heap size 468 MB, throughput 0.991414
Equal recommendation: 469 MB each
Reading from 5146: heap size 442 MB, throughput 0.990126
Reading from 5147: heap size 471 MB, throughput 0.989317
Reading from 5146: heap size 474 MB, throughput 0.989381
Reading from 5147: heap size 443 MB, throughput 0.9905
Reading from 5146: heap size 444 MB, throughput 0.988842
Reading from 5147: heap size 469 MB, throughput 0.984783
Reading from 5146: heap size 471 MB, throughput 0.986341
Reading from 5146: heap size 447 MB, throughput 0.97755
Reading from 5147: heap size 446 MB, throughput 0.980291
Equal recommendation: 469 MB each
Reading from 5147: heap size 463 MB, throughput 0.984938
Reading from 5146: heap size 465 MB, throughput 0.985982
Reading from 5147: heap size 463 MB, throughput 0.985644
Reading from 5146: heap size 464 MB, throughput 0.985235
Reading from 5146: heap size 467 MB, throughput 0.991168
Reading from 5147: heap size 465 MB, throughput 0.992146
Reading from 5147: heap size 466 MB, throughput 0.932243
Reading from 5147: heap size 465 MB, throughput 0.885276
Reading from 5146: heap size 468 MB, throughput 0.972816
Reading from 5147: heap size 468 MB, throughput 0.920216
Reading from 5146: heap size 468 MB, throughput 0.898265
Reading from 5146: heap size 470 MB, throughput 0.863636
Reading from 5146: heap size 463 MB, throughput 0.987069
Reading from 5147: heap size 476 MB, throughput 0.992656
Equal recommendation: 469 MB each
Reading from 5146: heap size 470 MB, throughput 0.992
Reading from 5147: heap size 443 MB, throughput 0.991285
Reading from 5146: heap size 457 MB, throughput 0.990466
Reading from 5147: heap size 473 MB, throughput 0.990728
Reading from 5146: heap size 467 MB, throughput 0.990631
Reading from 5147: heap size 445 MB, throughput 0.991259
Reading from 5146: heap size 467 MB, throughput 0.986608
Reading from 5147: heap size 468 MB, throughput 0.989312
Reading from 5146: heap size 469 MB, throughput 0.986277
Equal recommendation: 469 MB each
Reading from 5147: heap size 466 MB, throughput 0.987511
Reading from 5146: heap size 468 MB, throughput 0.9895
Reading from 5147: heap size 468 MB, throughput 0.985085
Reading from 5146: heap size 469 MB, throughput 0.985724
Reading from 5146: heap size 461 MB, throughput 0.97809
Reading from 5147: heap size 468 MB, throughput 0.987496
Reading from 5146: heap size 466 MB, throughput 0.990268
Reading from 5146: heap size 469 MB, throughput 0.895129
Reading from 5146: heap size 461 MB, throughput 0.861599
Reading from 5147: heap size 468 MB, throughput 0.980565
Reading from 5147: heap size 471 MB, throughput 0.883767
Reading from 5147: heap size 465 MB, throughput 0.888227
Reading from 5146: heap size 468 MB, throughput 0.978988
Reading from 5147: heap size 470 MB, throughput 0.988518
Equal recommendation: 469 MB each
Reading from 5146: heap size 469 MB, throughput 0.991027
Reading from 5147: heap size 458 MB, throughput 0.989806
Reading from 5146: heap size 456 MB, throughput 0.990382
Reading from 5147: heap size 468 MB, throughput 0.989911
Reading from 5146: heap size 466 MB, throughput 0.988591
Reading from 5147: heap size 469 MB, throughput 0.991675
Reading from 5146: heap size 465 MB, throughput 0.989622
Reading from 5147: heap size 471 MB, throughput 0.986145
Reading from 5146: heap size 468 MB, throughput 0.98978
Equal recommendation: 469 MB each
Reading from 5147: heap size 455 MB, throughput 0.986955
Reading from 5146: heap size 466 MB, throughput 0.989706
Reading from 5146: heap size 468 MB, throughput 0.985218
Reading from 5147: heap size 464 MB, throughput 0.989418
Reading from 5146: heap size 470 MB, throughput 0.984463
Reading from 5147: heap size 464 MB, throughput 0.983509
Reading from 5146: heap size 458 MB, throughput 0.99087
Reading from 5146: heap size 468 MB, throughput 0.915643
Reading from 5146: heap size 468 MB, throughput 0.859537
Reading from 5147: heap size 465 MB, throughput 0.992502
Reading from 5146: heap size 473 MB, throughput 0.913678
Reading from 5147: heap size 468 MB, throughput 0.927983
Reading from 5147: heap size 468 MB, throughput 0.863036
Reading from 5147: heap size 472 MB, throughput 0.903973
Equal recommendation: 469 MB each
Reading from 5146: heap size 442 MB, throughput 0.991504
Reading from 5147: heap size 438 MB, throughput 0.994196
Reading from 5146: heap size 472 MB, throughput 0.989882
Reading from 5147: heap size 470 MB, throughput 0.989876
Reading from 5146: heap size 442 MB, throughput 0.990641
Reading from 5147: heap size 439 MB, throughput 0.989902
Reading from 5146: heap size 469 MB, throughput 0.990432
Reading from 5147: heap size 467 MB, throughput 0.99184
Reading from 5146: heap size 445 MB, throughput 0.986182
Reading from 5147: heap size 466 MB, throughput 0.987332
Equal recommendation: 469 MB each
Reading from 5146: heap size 465 MB, throughput 0.988985
Reading from 5147: heap size 465 MB, throughput 0.989664
Reading from 5146: heap size 463 MB, throughput 0.984439
Reading from 5147: heap size 466 MB, throughput 0.986577
Reading from 5146: heap size 465 MB, throughput 0.985835
Reading from 5146: heap size 466 MB, throughput 0.989257
Reading from 5147: heap size 468 MB, throughput 0.984873
Reading from 5146: heap size 467 MB, throughput 0.948925
Reading from 5146: heap size 468 MB, throughput 0.862286
Reading from 5146: heap size 473 MB, throughput 0.883546
Reading from 5147: heap size 469 MB, throughput 0.976566
Reading from 5147: heap size 471 MB, throughput 0.884678
Reading from 5147: heap size 467 MB, throughput 0.843818
Equal recommendation: 469 MB each
Reading from 5146: heap size 462 MB, throughput 0.990285
Reading from 5147: heap size 469 MB, throughput 0.982204
Reading from 5146: heap size 472 MB, throughput 0.989504
Reading from 5147: heap size 437 MB, throughput 0.986601
Reading from 5146: heap size 441 MB, throughput 0.987534
Reading from 5147: heap size 470 MB, throughput 0.986058
Reading from 5146: heap size 470 MB, throughput 0.988952
Reading from 5147: heap size 441 MB, throughput 0.984074
Reading from 5146: heap size 444 MB, throughput 0.987628
Reading from 5147: heap size 464 MB, throughput 0.984412
Equal recommendation: 469 MB each
Reading from 5146: heap size 467 MB, throughput 0.985874
Reading from 5147: heap size 463 MB, throughput 0.870899
Reading from 5146: heap size 465 MB, throughput 0.985814
Reading from 5147: heap size 485 MB, throughput 0.995549
Reading from 5146: heap size 466 MB, throughput 0.985082
Reading from 5147: heap size 483 MB, throughput 0.992242
Reading from 5146: heap size 467 MB, throughput 0.993064
Reading from 5147: heap size 484 MB, throughput 0.991657
Reading from 5146: heap size 468 MB, throughput 0.973572
Reading from 5146: heap size 469 MB, throughput 0.873525
Reading from 5146: heap size 467 MB, throughput 0.878717
Reading from 5147: heap size 436 MB, throughput 0.992535
Reading from 5147: heap size 484 MB, throughput 0.914334
Reading from 5146: heap size 471 MB, throughput 0.988096
Equal recommendation: 469 MB each
Reading from 5147: heap size 463 MB, throughput 0.863822
Reading from 5147: heap size 481 MB, throughput 0.908762
Reading from 5146: heap size 459 MB, throughput 0.990944
Reading from 5147: heap size 439 MB, throughput 0.990024
Reading from 5146: heap size 469 MB, throughput 0.989994
Reading from 5147: heap size 479 MB, throughput 0.989421
Reading from 5146: heap size 455 MB, throughput 0.984533
Reading from 5147: heap size 441 MB, throughput 0.985074
Reading from 5147: heap size 475 MB, throughput 0.986679
Reading from 5146: heap size 465 MB, throughput 0.987077
Equal recommendation: 469 MB each
Reading from 5147: heap size 445 MB, throughput 0.9841
Reading from 5146: heap size 463 MB, throughput 0.98744
Reading from 5146: heap size 465 MB, throughput 0.976739
Reading from 5147: heap size 468 MB, throughput 0.974953
Reading from 5147: heap size 467 MB, throughput 0.984479
Reading from 5146: heap size 467 MB, throughput 0.87969
Reading from 5147: heap size 470 MB, throughput 0.982309
Reading from 5146: heap size 472 MB, throughput 0.995499
Reading from 5147: heap size 456 MB, throughput 0.980099
Reading from 5146: heap size 469 MB, throughput 0.989493
Reading from 5146: heap size 440 MB, throughput 0.89969
Reading from 5146: heap size 467 MB, throughput 0.880921
Reading from 5146: heap size 469 MB, throughput 0.94205
Reading from 5147: heap size 468 MB, throughput 0.989769
Equal recommendation: 469 MB each
Reading from 5147: heap size 469 MB, throughput 0.879055
Reading from 5147: heap size 469 MB, throughput 0.856599
Reading from 5147: heap size 455 MB, throughput 0.900495
Reading from 5146: heap size 479 MB, throughput 0.993926
Reading from 5147: heap size 470 MB, throughput 0.989418
Reading from 5146: heap size 445 MB, throughput 0.990994
Reading from 5147: heap size 433 MB, throughput 0.988849
Reading from 5146: heap size 477 MB, throughput 0.992345
Reading from 5147: heap size 468 MB, throughput 0.990449
Reading from 5147: heap size 467 MB, throughput 0.986182
Reading from 5146: heap size 448 MB, throughput 0.987915
Equal recommendation: 469 MB each
Reading from 5147: heap size 467 MB, throughput 0.987144
Reading from 5146: heap size 472 MB, throughput 0.990425
Reading from 5147: heap size 469 MB, throughput 0.985828
Reading from 5146: heap size 452 MB, throughput 0.985737
Reading from 5146: heap size 466 MB, throughput 0.986035
Reading from 5147: heap size 468 MB, throughput 0.988994
Reading from 5146: heap size 466 MB, throughput 0.98493
Reading from 5147: heap size 469 MB, throughput 0.982562
Reading from 5147: heap size 459 MB, throughput 0.983118
Reading from 5146: heap size 469 MB, throughput 0.992228
Reading from 5146: heap size 471 MB, throughput 0.919654
Reading from 5146: heap size 466 MB, throughput 0.869246
Reading from 5146: heap size 470 MB, throughput 0.868724
Client 5146 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 5147: heap size 466 MB, throughput 0.993211
Reading from 5147: heap size 467 MB, throughput 0.926529
Reading from 5147: heap size 469 MB, throughput 0.893903
Reading from 5147: heap size 475 MB, throughput 0.93652
Client 5147 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
