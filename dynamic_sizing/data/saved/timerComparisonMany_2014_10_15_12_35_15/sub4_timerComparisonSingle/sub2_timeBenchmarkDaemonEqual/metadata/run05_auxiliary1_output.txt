economemd
    total memory: 7435 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub4_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run05_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 2654: heap size 9 MB, throughput 0.991995
Clients: 1
Client 2654 has a minimum heap size of 1211 MB
Reading from 2655: heap size 9 MB, throughput 0.99127
Clients: 2
Client 2655 has a minimum heap size of 276 MB
Reading from 2655: heap size 9 MB, throughput 0.977228
Reading from 2654: heap size 9 MB, throughput 0.978582
Reading from 2655: heap size 9 MB, throughput 0.956517
Reading from 2654: heap size 9 MB, throughput 0.95665
Reading from 2655: heap size 9 MB, throughput 0.940862
Reading from 2654: heap size 9 MB, throughput 0.933552
Reading from 2655: heap size 11 MB, throughput 0.978119
Reading from 2654: heap size 11 MB, throughput 0.984962
Reading from 2655: heap size 11 MB, throughput 0.978099
Reading from 2654: heap size 11 MB, throughput 0.983971
Reading from 2655: heap size 17 MB, throughput 0.963147
Reading from 2654: heap size 17 MB, throughput 0.958161
Reading from 2655: heap size 17 MB, throughput 0.580935
Reading from 2654: heap size 17 MB, throughput 0.609425
Reading from 2655: heap size 30 MB, throughput 0.887036
Reading from 2654: heap size 30 MB, throughput 0.923578
Reading from 2655: heap size 31 MB, throughput 0.767699
Reading from 2654: heap size 31 MB, throughput 0.908923
Reading from 2655: heap size 35 MB, throughput 0.485532
Reading from 2654: heap size 34 MB, throughput 0.403025
Reading from 2655: heap size 47 MB, throughput 0.928456
Reading from 2654: heap size 48 MB, throughput 0.877496
Reading from 2655: heap size 50 MB, throughput 0.847137
Reading from 2654: heap size 50 MB, throughput 0.938172
Reading from 2655: heap size 51 MB, throughput 0.23886
Reading from 2654: heap size 52 MB, throughput 0.211157
Reading from 2655: heap size 75 MB, throughput 0.844917
Reading from 2654: heap size 74 MB, throughput 0.722756
Reading from 2655: heap size 76 MB, throughput 0.195328
Reading from 2654: heap size 74 MB, throughput 0.241203
Reading from 2655: heap size 97 MB, throughput 0.784975
Reading from 2654: heap size 100 MB, throughput 0.821331
Reading from 2655: heap size 100 MB, throughput 0.728811
Reading from 2654: heap size 101 MB, throughput 0.742141
Reading from 2654: heap size 103 MB, throughput 0.710624
Reading from 2655: heap size 102 MB, throughput 0.263567
Reading from 2654: heap size 106 MB, throughput 0.742289
Reading from 2655: heap size 130 MB, throughput 0.759568
Reading from 2655: heap size 134 MB, throughput 0.724444
Reading from 2654: heap size 109 MB, throughput 0.175562
Reading from 2654: heap size 143 MB, throughput 0.775899
Reading from 2655: heap size 136 MB, throughput 0.148451
Reading from 2654: heap size 148 MB, throughput 0.802633
Reading from 2654: heap size 150 MB, throughput 0.733039
Reading from 2655: heap size 176 MB, throughput 0.786721
Reading from 2654: heap size 159 MB, throughput 0.759109
Reading from 2655: heap size 177 MB, throughput 0.761054
Reading from 2654: heap size 161 MB, throughput 0.640291
Reading from 2655: heap size 180 MB, throughput 0.733603
Reading from 2655: heap size 183 MB, throughput 0.186983
Reading from 2654: heap size 164 MB, throughput 0.15418
Reading from 2655: heap size 233 MB, throughput 0.668129
Reading from 2654: heap size 206 MB, throughput 0.669088
Reading from 2654: heap size 210 MB, throughput 0.656717
Reading from 2654: heap size 215 MB, throughput 0.606817
Reading from 2654: heap size 218 MB, throughput 0.662848
Reading from 2654: heap size 227 MB, throughput 0.563645
Reading from 2654: heap size 231 MB, throughput 0.51758
Reading from 2655: heap size 234 MB, throughput 0.928484
Reading from 2655: heap size 233 MB, throughput 0.759843
Reading from 2655: heap size 240 MB, throughput 0.708561
Reading from 2655: heap size 246 MB, throughput 0.914103
Reading from 2654: heap size 241 MB, throughput 0.0954298
Reading from 2655: heap size 246 MB, throughput 0.875871
Reading from 2654: heap size 291 MB, throughput 0.493745
Reading from 2655: heap size 252 MB, throughput 0.872979
Reading from 2654: heap size 293 MB, throughput 0.696831
Reading from 2655: heap size 253 MB, throughput 0.769878
Reading from 2655: heap size 254 MB, throughput 0.663054
Reading from 2655: heap size 259 MB, throughput 0.682696
Reading from 2655: heap size 265 MB, throughput 0.593616
Reading from 2654: heap size 287 MB, throughput 0.136696
Reading from 2655: heap size 268 MB, throughput 0.825556
Reading from 2654: heap size 334 MB, throughput 0.64723
Reading from 2654: heap size 326 MB, throughput 0.695243
Reading from 2654: heap size 331 MB, throughput 0.707824
Reading from 2654: heap size 330 MB, throughput 0.698108
Reading from 2655: heap size 275 MB, throughput 0.939621
Reading from 2654: heap size 332 MB, throughput 0.657152
Reading from 2655: heap size 277 MB, throughput 0.765148
Reading from 2654: heap size 334 MB, throughput 0.644457
Reading from 2655: heap size 283 MB, throughput 0.739394
Reading from 2654: heap size 341 MB, throughput 0.567371
Reading from 2655: heap size 286 MB, throughput 0.680444
Reading from 2654: heap size 344 MB, throughput 0.499806
Reading from 2654: heap size 354 MB, throughput 0.473478
Reading from 2654: heap size 358 MB, throughput 0.434366
Reading from 2654: heap size 368 MB, throughput 0.450322
Reading from 2655: heap size 291 MB, throughput 0.0762898
Reading from 2654: heap size 373 MB, throughput 0.130666
Reading from 2655: heap size 336 MB, throughput 0.820954
Reading from 2654: heap size 423 MB, throughput 0.392966
Reading from 2654: heap size 418 MB, throughput 0.558198
Reading from 2655: heap size 338 MB, throughput 0.884993
Reading from 2655: heap size 340 MB, throughput 0.737663
Reading from 2655: heap size 339 MB, throughput 0.762154
Reading from 2655: heap size 341 MB, throughput 0.583591
Reading from 2655: heap size 346 MB, throughput 0.76887
Reading from 2654: heap size 357 MB, throughput 0.113826
Reading from 2655: heap size 347 MB, throughput 0.725959
Reading from 2654: heap size 463 MB, throughput 0.381968
Reading from 2654: heap size 468 MB, throughput 0.545859
Reading from 2654: heap size 464 MB, throughput 0.50953
Equal recommendation: 3717 MB each
Reading from 2654: heap size 467 MB, throughput 0.557009
Reading from 2654: heap size 461 MB, throughput 0.526249
Reading from 2654: heap size 465 MB, throughput 0.640667
Reading from 2655: heap size 353 MB, throughput 0.973719
Reading from 2654: heap size 459 MB, throughput 0.0941629
Reading from 2654: heap size 517 MB, throughput 0.470649
Reading from 2654: heap size 516 MB, throughput 0.629738
Reading from 2654: heap size 519 MB, throughput 0.541093
Reading from 2654: heap size 521 MB, throughput 0.563793
Reading from 2654: heap size 526 MB, throughput 0.522223
Reading from 2654: heap size 530 MB, throughput 0.400694
Reading from 2655: heap size 354 MB, throughput 0.963043
Reading from 2654: heap size 539 MB, throughput 0.566234
Reading from 2654: heap size 548 MB, throughput 0.1107
Reading from 2654: heap size 618 MB, throughput 0.387296
Reading from 2654: heap size 622 MB, throughput 0.518422
Reading from 2655: heap size 359 MB, throughput 0.964894
Reading from 2654: heap size 626 MB, throughput 0.619288
Reading from 2654: heap size 629 MB, throughput 0.630487
Reading from 2654: heap size 630 MB, throughput 0.100632
Reading from 2655: heap size 360 MB, throughput 0.973832
Reading from 2654: heap size 703 MB, throughput 0.822306
Reading from 2654: heap size 703 MB, throughput 0.877022
Reading from 2655: heap size 362 MB, throughput 0.977398
Reading from 2654: heap size 700 MB, throughput 0.874003
Reading from 2654: heap size 709 MB, throughput 0.588214
Reading from 2654: heap size 717 MB, throughput 0.633615
Reading from 2654: heap size 727 MB, throughput 0.363757
Reading from 2654: heap size 739 MB, throughput 0.367278
Reading from 2654: heap size 743 MB, throughput 0.373538
Reading from 2655: heap size 364 MB, throughput 0.9675
Reading from 2654: heap size 734 MB, throughput 0.0357425
Reading from 2655: heap size 363 MB, throughput 0.977182
Equal recommendation: 3717 MB each
Reading from 2654: heap size 817 MB, throughput 0.790194
Reading from 2654: heap size 812 MB, throughput 0.580243
Reading from 2654: heap size 816 MB, throughput 0.548378
Reading from 2655: heap size 365 MB, throughput 0.967372
Reading from 2654: heap size 819 MB, throughput 0.0640226
Reading from 2654: heap size 906 MB, throughput 0.794269
Reading from 2654: heap size 909 MB, throughput 0.913771
Reading from 2654: heap size 916 MB, throughput 0.916756
Reading from 2654: heap size 927 MB, throughput 0.918979
Reading from 2654: heap size 928 MB, throughput 0.814968
Reading from 2655: heap size 366 MB, throughput 0.977473
Reading from 2654: heap size 930 MB, throughput 0.935442
Reading from 2654: heap size 798 MB, throughput 0.843292
Reading from 2654: heap size 909 MB, throughput 0.833803
Reading from 2654: heap size 807 MB, throughput 0.859238
Reading from 2654: heap size 899 MB, throughput 0.817296
Reading from 2654: heap size 812 MB, throughput 0.767049
Reading from 2655: heap size 366 MB, throughput 0.949568
Reading from 2654: heap size 892 MB, throughput 0.869678
Reading from 2654: heap size 899 MB, throughput 0.833528
Reading from 2654: heap size 890 MB, throughput 0.849561
Reading from 2654: heap size 895 MB, throughput 0.864015
Reading from 2654: heap size 886 MB, throughput 0.848542
Reading from 2655: heap size 369 MB, throughput 0.973096
Reading from 2654: heap size 891 MB, throughput 0.985825
Reading from 2655: heap size 369 MB, throughput 0.97261
Reading from 2654: heap size 888 MB, throughput 0.926784
Reading from 2654: heap size 891 MB, throughput 0.771709
Reading from 2654: heap size 897 MB, throughput 0.749061
Reading from 2654: heap size 897 MB, throughput 0.838386
Reading from 2654: heap size 901 MB, throughput 0.804223
Reading from 2655: heap size 372 MB, throughput 0.970108
Reading from 2654: heap size 902 MB, throughput 0.823103
Reading from 2654: heap size 905 MB, throughput 0.809282
Reading from 2654: heap size 907 MB, throughput 0.800249
Reading from 2654: heap size 913 MB, throughput 0.85446
Equal recommendation: 3717 MB each
Reading from 2654: heap size 914 MB, throughput 0.853788
Reading from 2654: heap size 918 MB, throughput 0.866915
Reading from 2655: heap size 374 MB, throughput 0.969904
Reading from 2655: heap size 377 MB, throughput 0.948803
Reading from 2655: heap size 378 MB, throughput 0.883431
Reading from 2654: heap size 920 MB, throughput 0.113172
Reading from 2655: heap size 376 MB, throughput 0.816954
Reading from 2655: heap size 381 MB, throughput 0.663085
Reading from 2654: heap size 996 MB, throughput 0.462589
Reading from 2654: heap size 1017 MB, throughput 0.614456
Reading from 2654: heap size 1027 MB, throughput 0.640788
Reading from 2655: heap size 388 MB, throughput 0.967779
Reading from 2654: heap size 1028 MB, throughput 0.560225
Reading from 2654: heap size 1038 MB, throughput 0.597569
Reading from 2654: heap size 1039 MB, throughput 0.578989
Reading from 2654: heap size 1048 MB, throughput 0.620362
Reading from 2654: heap size 1051 MB, throughput 0.713867
Reading from 2654: heap size 1059 MB, throughput 0.56195
Reading from 2655: heap size 389 MB, throughput 0.989658
Reading from 2654: heap size 1063 MB, throughput 0.897005
Reading from 2655: heap size 393 MB, throughput 0.983956
Reading from 2654: heap size 1078 MB, throughput 0.959557
Reading from 2655: heap size 394 MB, throughput 0.980853
Reading from 2654: heap size 1081 MB, throughput 0.952457
Reading from 2655: heap size 395 MB, throughput 0.986009
Equal recommendation: 3717 MB each
Reading from 2654: heap size 1098 MB, throughput 0.947738
Reading from 2655: heap size 396 MB, throughput 0.984728
Reading from 2654: heap size 1102 MB, throughput 0.938514
Reading from 2655: heap size 393 MB, throughput 0.984407
Reading from 2655: heap size 396 MB, throughput 0.980317
Reading from 2654: heap size 1116 MB, throughput 0.67445
Reading from 2655: heap size 392 MB, throughput 0.984343
Reading from 2654: heap size 1212 MB, throughput 0.991305
Reading from 2655: heap size 394 MB, throughput 0.981497
Reading from 2654: heap size 1216 MB, throughput 0.987812
Equal recommendation: 3717 MB each
Reading from 2655: heap size 393 MB, throughput 0.980467
Reading from 2654: heap size 1224 MB, throughput 0.983949
Reading from 2655: heap size 394 MB, throughput 0.990993
Reading from 2655: heap size 395 MB, throughput 0.918173
Reading from 2655: heap size 396 MB, throughput 0.848162
Reading from 2655: heap size 399 MB, throughput 0.84917
Reading from 2655: heap size 400 MB, throughput 0.955571
Reading from 2654: heap size 1238 MB, throughput 0.978168
Reading from 2655: heap size 406 MB, throughput 0.990892
Reading from 2654: heap size 1239 MB, throughput 0.978565
Reading from 2655: heap size 407 MB, throughput 0.989797
Reading from 2654: heap size 1237 MB, throughput 0.977856
Reading from 2655: heap size 409 MB, throughput 0.986838
Equal recommendation: 3717 MB each
Reading from 2654: heap size 1241 MB, throughput 0.968162
Reading from 2655: heap size 411 MB, throughput 0.986587
Reading from 2654: heap size 1229 MB, throughput 0.967463
Reading from 2655: heap size 409 MB, throughput 0.985502
Reading from 2654: heap size 1236 MB, throughput 0.970815
Reading from 2655: heap size 411 MB, throughput 0.985419
Reading from 2654: heap size 1236 MB, throughput 0.96132
Reading from 2655: heap size 409 MB, throughput 0.774845
Reading from 2654: heap size 1237 MB, throughput 0.968589
Reading from 2655: heap size 446 MB, throughput 0.976794
Equal recommendation: 3717 MB each
Reading from 2655: heap size 446 MB, throughput 0.990288
Reading from 2654: heap size 1241 MB, throughput 0.964667
Reading from 2655: heap size 448 MB, throughput 0.988381
Reading from 2655: heap size 449 MB, throughput 0.905549
Reading from 2655: heap size 450 MB, throughput 0.843681
Reading from 2655: heap size 456 MB, throughput 0.917623
Reading from 2654: heap size 1245 MB, throughput 0.956563
Reading from 2655: heap size 458 MB, throughput 0.988495
Reading from 2654: heap size 1250 MB, throughput 0.958884
Reading from 2655: heap size 459 MB, throughput 0.991132
Reading from 2654: heap size 1258 MB, throughput 0.953394
Reading from 2655: heap size 462 MB, throughput 0.987243
Equal recommendation: 3717 MB each
Reading from 2654: heap size 1265 MB, throughput 0.955167
Reading from 2655: heap size 459 MB, throughput 0.987747
Reading from 2654: heap size 1274 MB, throughput 0.952288
Reading from 2655: heap size 462 MB, throughput 0.986128
Reading from 2655: heap size 463 MB, throughput 0.983699
Reading from 2654: heap size 1284 MB, throughput 0.957176
Reading from 2655: heap size 464 MB, throughput 0.984406
Reading from 2654: heap size 1290 MB, throughput 0.955114
Equal recommendation: 3717 MB each
Reading from 2655: heap size 466 MB, throughput 0.985361
Reading from 2654: heap size 1300 MB, throughput 0.956888
Reading from 2655: heap size 468 MB, throughput 0.98989
Reading from 2655: heap size 468 MB, throughput 0.923828
Reading from 2655: heap size 470 MB, throughput 0.849504
Reading from 2655: heap size 476 MB, throughput 0.966643
Reading from 2654: heap size 1303 MB, throughput 0.952931
Reading from 2655: heap size 478 MB, throughput 0.991071
Reading from 2654: heap size 1313 MB, throughput 0.956433
Reading from 2655: heap size 481 MB, throughput 0.989682
Reading from 2654: heap size 1315 MB, throughput 0.960902
Equal recommendation: 3717 MB each
Reading from 2655: heap size 483 MB, throughput 0.988645
Reading from 2654: heap size 1324 MB, throughput 0.956769
Reading from 2655: heap size 481 MB, throughput 0.989069
Reading from 2654: heap size 1325 MB, throughput 0.956097
Reading from 2655: heap size 484 MB, throughput 0.988542
Reading from 2654: heap size 1334 MB, throughput 0.957239
Reading from 2655: heap size 485 MB, throughput 0.985572
Equal recommendation: 3717 MB each
Reading from 2655: heap size 485 MB, throughput 0.986159
Reading from 2654: heap size 1334 MB, throughput 0.951875
Reading from 2655: heap size 487 MB, throughput 0.990896
Reading from 2655: heap size 488 MB, throughput 0.918614
Reading from 2655: heap size 487 MB, throughput 0.900038
Reading from 2655: heap size 490 MB, throughput 0.975756
Reading from 2654: heap size 1343 MB, throughput 0.561629
Reading from 2655: heap size 497 MB, throughput 0.99183
Reading from 2654: heap size 1454 MB, throughput 0.93925
Reading from 2655: heap size 498 MB, throughput 0.990386
Equal recommendation: 3717 MB each
Reading from 2654: heap size 1455 MB, throughput 0.978577
Reading from 2655: heap size 498 MB, throughput 0.990188
Reading from 2654: heap size 1463 MB, throughput 0.975521
Reading from 2655: heap size 500 MB, throughput 0.987645
Reading from 2654: heap size 1474 MB, throughput 0.979607
Reading from 2655: heap size 500 MB, throughput 0.987245
Reading from 2655: heap size 501 MB, throughput 0.985706
Equal recommendation: 3717 MB each
Reading from 2654: heap size 1475 MB, throughput 0.972388
Reading from 2655: heap size 504 MB, throughput 0.992421
Reading from 2655: heap size 504 MB, throughput 0.929798
Reading from 2655: heap size 504 MB, throughput 0.892144
Reading from 2654: heap size 1470 MB, throughput 0.971542
Reading from 2655: heap size 507 MB, throughput 0.987934
Reading from 2655: heap size 513 MB, throughput 0.991907
Reading from 2655: heap size 514 MB, throughput 0.989824
Equal recommendation: 3717 MB each
Reading from 2655: heap size 514 MB, throughput 0.989746
Reading from 2655: heap size 517 MB, throughput 0.988405
Reading from 2655: heap size 516 MB, throughput 0.987389
Equal recommendation: 3717 MB each
Reading from 2655: heap size 518 MB, throughput 0.970664
Reading from 2654: heap size 1475 MB, throughput 0.987852
Reading from 2655: heap size 517 MB, throughput 0.983789
Reading from 2655: heap size 522 MB, throughput 0.904524
Reading from 2655: heap size 526 MB, throughput 0.983175
Reading from 2655: heap size 527 MB, throughput 0.992041
Reading from 2655: heap size 529 MB, throughput 0.990914
Equal recommendation: 3717 MB each
Reading from 2655: heap size 531 MB, throughput 0.99034
Reading from 2655: heap size 529 MB, throughput 0.989639
Reading from 2654: heap size 1448 MB, throughput 0.990688
Reading from 2655: heap size 531 MB, throughput 0.988901
Reading from 2655: heap size 532 MB, throughput 0.986034
Equal recommendation: 3717 MB each
Reading from 2654: heap size 1477 MB, throughput 0.975169
Reading from 2654: heap size 1495 MB, throughput 0.775002
Reading from 2655: heap size 533 MB, throughput 0.978569
Reading from 2655: heap size 537 MB, throughput 0.843952
Reading from 2654: heap size 1518 MB, throughput 0.625598
Reading from 2654: heap size 1542 MB, throughput 0.665272
Reading from 2654: heap size 1573 MB, throughput 0.675494
Reading from 2654: heap size 1608 MB, throughput 0.685657
Reading from 2654: heap size 1627 MB, throughput 0.6649
Reading from 2655: heap size 538 MB, throughput 0.989721
Reading from 2654: heap size 1668 MB, throughput 0.904027
Reading from 2655: heap size 545 MB, throughput 0.992196
Reading from 2654: heap size 1675 MB, throughput 0.961658
Reading from 2655: heap size 546 MB, throughput 0.991038
Equal recommendation: 3717 MB each
Reading from 2654: heap size 1657 MB, throughput 0.96653
Reading from 2655: heap size 547 MB, throughput 0.99045
Reading from 2654: heap size 1676 MB, throughput 0.965264
Reading from 2655: heap size 549 MB, throughput 0.9878
Reading from 2654: heap size 1654 MB, throughput 0.967917
Reading from 2655: heap size 549 MB, throughput 0.988867
Reading from 2654: heap size 1673 MB, throughput 0.964157
Equal recommendation: 3717 MB each
Reading from 2655: heap size 551 MB, throughput 0.991903
Reading from 2655: heap size 552 MB, throughput 0.913289
Reading from 2655: heap size 553 MB, throughput 0.982337
Reading from 2654: heap size 1654 MB, throughput 0.76022
Reading from 2655: heap size 561 MB, throughput 0.99268
Reading from 2654: heap size 1646 MB, throughput 0.991306
Reading from 2655: heap size 562 MB, throughput 0.992657
Equal recommendation: 3717 MB each
Reading from 2654: heap size 1650 MB, throughput 0.986938
Reading from 2655: heap size 562 MB, throughput 0.990515
Reading from 2654: heap size 1662 MB, throughput 0.984636
Reading from 2655: heap size 564 MB, throughput 0.989474
Reading from 2654: heap size 1678 MB, throughput 0.981928
Reading from 2655: heap size 565 MB, throughput 0.988904
Equal recommendation: 3717 MB each
Reading from 2655: heap size 565 MB, throughput 0.987233
Reading from 2655: heap size 567 MB, throughput 0.92559
Reading from 2654: heap size 1682 MB, throughput 0.983227
Reading from 2655: heap size 570 MB, throughput 0.991547
Reading from 2654: heap size 1670 MB, throughput 0.978198
Reading from 2655: heap size 575 MB, throughput 0.992656
Reading from 2655: heap size 576 MB, throughput 0.992163
Reading from 2654: heap size 1680 MB, throughput 0.983857
Equal recommendation: 3717 MB each
Reading from 2655: heap size 576 MB, throughput 0.991423
Reading from 2654: heap size 1651 MB, throughput 0.974296
Reading from 2655: heap size 578 MB, throughput 0.990268
Reading from 2654: heap size 1489 MB, throughput 0.971398
Reading from 2655: heap size 579 MB, throughput 0.993686
Reading from 2655: heap size 580 MB, throughput 0.936519
Equal recommendation: 3717 MB each
Reading from 2655: heap size 580 MB, throughput 0.982007
Reading from 2654: heap size 1637 MB, throughput 0.97114
Reading from 2655: heap size 582 MB, throughput 0.99276
Reading from 2654: heap size 1650 MB, throughput 0.969598
Reading from 2655: heap size 585 MB, throughput 0.992131
Reading from 2654: heap size 1652 MB, throughput 0.964706
Equal recommendation: 3717 MB each
Reading from 2655: heap size 586 MB, throughput 0.991638
Reading from 2654: heap size 1654 MB, throughput 0.96502
Reading from 2655: heap size 586 MB, throughput 0.989969
Reading from 2655: heap size 587 MB, throughput 0.990332
Reading from 2654: heap size 1665 MB, throughput 0.965659
Reading from 2655: heap size 590 MB, throughput 0.969215
Reading from 2655: heap size 591 MB, throughput 0.972667
Equal recommendation: 3717 MB each
Reading from 2654: heap size 1671 MB, throughput 0.959599
Reading from 2655: heap size 596 MB, throughput 0.994807
Reading from 2654: heap size 1685 MB, throughput 0.959239
Reading from 2655: heap size 597 MB, throughput 0.992627
Equal recommendation: 3717 MB each
Reading from 2655: heap size 596 MB, throughput 0.992381
Reading from 2654: heap size 1696 MB, throughput 0.967206
Reading from 2655: heap size 598 MB, throughput 0.990368
Reading from 2654: heap size 1716 MB, throughput 0.960963
Reading from 2655: heap size 599 MB, throughput 0.994854
Reading from 2655: heap size 600 MB, throughput 0.944891
Client 2655 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 2654: heap size 1723 MB, throughput 0.969342
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 2654: heap size 1743 MB, throughput 0.9971
Recommendation: one client; give it all the memory
Reading from 2654: heap size 1748 MB, throughput 0.98644
Reading from 2654: heap size 1746 MB, throughput 0.863857
Reading from 2654: heap size 1754 MB, throughput 0.786765
Reading from 2654: heap size 1787 MB, throughput 0.823179
Client 2654 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
