#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
VM_DIR="${DIR}/jikesrvm"

if [ ! -e "${VM_DIR}/patch.done" ]; then
    cd "${VM_DIR}/jikesrvm" &&
    patch -p1 < "${DIR}/economem.patch" &&
    touch "${VM_DIR}/patch.done" &&
    exit 0

    exit 1
fi
