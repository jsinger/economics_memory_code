{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, DeriveDataTypeable #-}
{-# OPTIONS_GHC -fno-warn-missing-fields -fno-warn-missing-methods #-}

import Control.Applicative
import Economem.ArgHelper
import Economem.Common
import Economem.Files
import Economem.MinHeap
import Economem.Records
import System.Console.CmdLib

data Args = Args { inputFile :: FilePath,
                   minHeapsFile :: FilePath,
                   factorsFile :: FilePath,
                   outputPath :: FilePath }
          deriving (Typeable, Data, Eq, Show)

instance Attributes Args where
  attributes _ = group "Options" [
    inputFile %> [ Positional 0,
                   Required True,
                   Help "Benchmark combinations to run",
                   ArgHelp "INPUT_FILE" ],
    minHeapsFile %> [ Positional 1,
                   Required True,
                   Help "Min heaps of the benchmarks",
                   ArgHelp "MIN_HEAPS_FILE" ],
    factorsFile %> [ Positional 2,
                   Required True,
                   Help "Factors of min heap at which to measure",
                   ArgHelp "FACTORS_FILE" ],
    outputPath %> [ Positional 3,
                   Required True,
                   Help "File to save to",
                   ArgHelp "OUTPUT_FILE" ]]

instance RecordCommand Args where
  mode_summary _ = "Generate input for timer_comparison_many based on factors of the combined min heap"

main :: IO ()
main = getArgs >>= executeR Args {} >>= \opts -> do
  (input, minHeaps, factors, outputFile) <- validate opts
  put outputFile =<< concat <$> mapM (doCombination minHeaps factors) input

  where
    validate :: Args -> IO ([BenchmarkCombination],
                            [BenchmarkThreadsMB],
                            [Double],
                            CsvFile BenchmarkCombinationMB)
    validate a = do
      input <- get =<< CsvFile <$> existingAbsoluteFile (inputFile a)
      minHeaps <- get =<< CsvFile <$> existingAbsoluteFile (minHeapsFile a)
      factors <- get =<< LinesFile <$> existingAbsoluteFile (factorsFile a)
      let outputFile = CsvFile (outputPath a)
      return (input, minHeaps, factors, outputFile)

doCombination :: [BenchmarkThreadsMB] -> [Double] -> BenchmarkCombination -> IO [BenchmarkCombinationMB]
doCombination minHeaps factors bms = do
  minHeap <- sumMinHeaps
  return (map (\factor -> BenchmarkCombinationMB bms (round (factor * fromIntegral minHeap))) factors)

  where
    sumMinHeaps :: IO Int
    sumMinHeaps = sum <$> mapM
                  (\bm -> fromIntegral . sizeToInt . toMB
                          <$> lookupMinHeap (btiBenchmark bm) minHeaps)
                  (bcBenchmarks bms)
