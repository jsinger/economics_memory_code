economemd
    total memory: 6265 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub24_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 10142: heap size 9 MB, throughput 0.990247
Clients: 1
Client 10142 has a minimum heap size of 30 MB
Reading from 10141: heap size 9 MB, throughput 0.990809
Clients: 2
Client 10141 has a minimum heap size of 1223 MB
Reading from 10142: heap size 9 MB, throughput 0.928345
Reading from 10142: heap size 9 MB, throughput 0.96781
Reading from 10141: heap size 9 MB, throughput 0.973202
Reading from 10142: heap size 9 MB, throughput 0.966669
Reading from 10142: heap size 11 MB, throughput 0.691267
Reading from 10141: heap size 9 MB, throughput 0.926708
Reading from 10142: heap size 11 MB, throughput 0.485294
Reading from 10142: heap size 16 MB, throughput 0.641711
Reading from 10142: heap size 16 MB, throughput 0.670429
Reading from 10142: heap size 24 MB, throughput 0.833459
Reading from 10142: heap size 24 MB, throughput 0.808703
Reading from 10142: heap size 34 MB, throughput 0.893939
Reading from 10142: heap size 34 MB, throughput 0.881463
Reading from 10141: heap size 9 MB, throughput 0.967527
Reading from 10142: heap size 50 MB, throughput 0.9238
Reading from 10142: heap size 50 MB, throughput 0.91439
Reading from 10142: heap size 75 MB, throughput 0.946984
Reading from 10142: heap size 75 MB, throughput 0.792465
Reading from 10142: heap size 116 MB, throughput 0.963807
Reading from 10142: heap size 116 MB, throughput 0.949704
Reading from 10141: heap size 11 MB, throughput 0.984254
Reading from 10141: heap size 11 MB, throughput 0.973969
Reading from 10141: heap size 17 MB, throughput 0.947654
Reading from 10141: heap size 17 MB, throughput 0.515176
Reading from 10141: heap size 30 MB, throughput 0.981333
Reading from 10141: heap size 31 MB, throughput 0.844005
Reading from 10141: heap size 33 MB, throughput 0.486656
Reading from 10142: heap size 159 MB, throughput 0.955912
Reading from 10141: heap size 42 MB, throughput 0.708013
Reading from 10141: heap size 47 MB, throughput 0.883193
Reading from 10141: heap size 48 MB, throughput 0.273207
Reading from 10141: heap size 65 MB, throughput 0.704823
Reading from 10142: heap size 165 MB, throughput 0.818117
Reading from 10141: heap size 67 MB, throughput 0.17748
Reading from 10141: heap size 91 MB, throughput 0.782311
Reading from 10141: heap size 92 MB, throughput 0.752486
Reading from 10141: heap size 94 MB, throughput 0.58288
Reading from 10141: heap size 98 MB, throughput 0.778297
Reading from 10142: heap size 224 MB, throughput 0.99242
Reading from 10141: heap size 101 MB, throughput 0.14604
Reading from 10141: heap size 131 MB, throughput 0.791242
Reading from 10141: heap size 135 MB, throughput 0.78383
Reading from 10141: heap size 137 MB, throughput 0.647646
Reading from 10142: heap size 238 MB, throughput 0.990023
Reading from 10141: heap size 141 MB, throughput 0.711251
Reading from 10141: heap size 146 MB, throughput 0.491148
Reading from 10141: heap size 150 MB, throughput 0.648588
Reading from 10141: heap size 157 MB, throughput 0.530527
Reading from 10141: heap size 163 MB, throughput 0.105207
Reading from 10141: heap size 197 MB, throughput 0.452903
Reading from 10141: heap size 203 MB, throughput 0.644796
Reading from 10141: heap size 204 MB, throughput 0.497143
Reading from 10142: heap size 259 MB, throughput 0.987375
Reading from 10141: heap size 208 MB, throughput 0.118682
Reading from 10141: heap size 244 MB, throughput 0.608476
Reading from 10141: heap size 240 MB, throughput 0.666759
Reading from 10141: heap size 243 MB, throughput 0.611751
Reading from 10142: heap size 260 MB, throughput 0.857882
Reading from 10141: heap size 246 MB, throughput 0.689079
Reading from 10141: heap size 249 MB, throughput 0.595844
Reading from 10141: heap size 252 MB, throughput 0.650891
Reading from 10141: heap size 259 MB, throughput 0.477484
Reading from 10141: heap size 263 MB, throughput 0.44862
Reading from 10141: heap size 272 MB, throughput 0.501052
Reading from 10142: heap size 296 MB, throughput 0.976931
Reading from 10141: heap size 277 MB, throughput 0.078671
Reading from 10141: heap size 323 MB, throughput 0.542384
Reading from 10142: heap size 298 MB, throughput 0.994064
Reading from 10141: heap size 323 MB, throughput 0.607018
Reading from 10141: heap size 327 MB, throughput 0.0969983
Reading from 10141: heap size 370 MB, throughput 0.440147
Reading from 10141: heap size 372 MB, throughput 0.604956
Reading from 10141: heap size 364 MB, throughput 0.532902
Reading from 10142: heap size 315 MB, throughput 0.986798
Reading from 10141: heap size 368 MB, throughput 0.548205
Reading from 10141: heap size 369 MB, throughput 0.535132
Equal recommendation: 3132 MB each
Reading from 10141: heap size 371 MB, throughput 0.516066
Reading from 10141: heap size 375 MB, throughput 0.579775
Reading from 10141: heap size 378 MB, throughput 0.534582
Reading from 10142: heap size 318 MB, throughput 0.971897
Reading from 10141: heap size 382 MB, throughput 0.0887572
Reading from 10141: heap size 433 MB, throughput 0.469192
Reading from 10141: heap size 440 MB, throughput 0.596117
Reading from 10142: heap size 339 MB, throughput 0.987897
Reading from 10141: heap size 442 MB, throughput 0.531256
Reading from 10141: heap size 446 MB, throughput 0.510538
Reading from 10141: heap size 452 MB, throughput 0.523427
Reading from 10141: heap size 458 MB, throughput 0.411351
Reading from 10142: heap size 342 MB, throughput 0.971843
Reading from 10141: heap size 464 MB, throughput 0.0882947
Reading from 10141: heap size 525 MB, throughput 0.415293
Reading from 10141: heap size 528 MB, throughput 0.533746
Reading from 10141: heap size 524 MB, throughput 0.561074
Reading from 10142: heap size 359 MB, throughput 0.881584
Reading from 10141: heap size 527 MB, throughput 0.103103
Reading from 10141: heap size 588 MB, throughput 0.501393
Reading from 10141: heap size 589 MB, throughput 0.604091
Reading from 10141: heap size 590 MB, throughput 0.567701
Reading from 10142: heap size 370 MB, throughput 0.998231
Reading from 10141: heap size 594 MB, throughput 0.474694
Reading from 10141: heap size 597 MB, throughput 0.490773
Reading from 10141: heap size 607 MB, throughput 0.517168
Reading from 10141: heap size 615 MB, throughput 0.505479
Reading from 10141: heap size 623 MB, throughput 0.548074
Reading from 10142: heap size 392 MB, throughput 0.99218
Reading from 10142: heap size 393 MB, throughput 0.970063
Reading from 10141: heap size 630 MB, throughput 0.220899
Reading from 10141: heap size 700 MB, throughput 0.719508
Reading from 10142: heap size 411 MB, throughput 0.986414
Reading from 10141: heap size 708 MB, throughput 0.832616
Equal recommendation: 3132 MB each
Reading from 10141: heap size 712 MB, throughput 0.839102
Reading from 10142: heap size 409 MB, throughput 0.990224
Reading from 10141: heap size 718 MB, throughput 0.369788
Reading from 10141: heap size 726 MB, throughput 0.572307
Reading from 10141: heap size 733 MB, throughput 0.345265
Reading from 10142: heap size 427 MB, throughput 0.980388
Reading from 10141: heap size 734 MB, throughput 0.0216161
Reading from 10141: heap size 792 MB, throughput 0.281864
Reading from 10141: heap size 800 MB, throughput 0.438364
Reading from 10142: heap size 430 MB, throughput 0.989347
Reading from 10141: heap size 802 MB, throughput 0.780311
Reading from 10142: heap size 446 MB, throughput 0.987642
Reading from 10141: heap size 803 MB, throughput 0.11701
Reading from 10141: heap size 882 MB, throughput 0.407426
Reading from 10141: heap size 885 MB, throughput 0.638874
Reading from 10142: heap size 449 MB, throughput 0.983064
Reading from 10141: heap size 890 MB, throughput 0.797127
Reading from 10141: heap size 891 MB, throughput 0.840665
Reading from 10141: heap size 890 MB, throughput 0.811374
Reading from 10141: heap size 893 MB, throughput 0.67619
Reading from 10142: heap size 465 MB, throughput 0.986093
Reading from 10141: heap size 897 MB, throughput 0.214357
Reading from 10141: heap size 989 MB, throughput 0.587576
Reading from 10141: heap size 992 MB, throughput 0.93236
Reading from 10142: heap size 468 MB, throughput 0.984287
Reading from 10141: heap size 833 MB, throughput 0.827265
Reading from 10141: heap size 976 MB, throughput 0.797594
Reading from 10141: heap size 838 MB, throughput 0.839195
Reading from 10141: heap size 965 MB, throughput 0.816363
Reading from 10141: heap size 843 MB, throughput 0.832746
Reading from 10141: heap size 957 MB, throughput 0.842731
Reading from 10141: heap size 843 MB, throughput 0.855465
Reading from 10142: heap size 484 MB, throughput 0.994299
Reading from 10141: heap size 950 MB, throughput 0.816533
Equal recommendation: 3132 MB each
Reading from 10141: heap size 957 MB, throughput 0.843132
Reading from 10141: heap size 945 MB, throughput 0.846298
Reading from 10141: heap size 951 MB, throughput 0.983246
Reading from 10142: heap size 486 MB, throughput 0.97952
Reading from 10141: heap size 946 MB, throughput 0.961918
Reading from 10142: heap size 500 MB, throughput 0.995046
Reading from 10141: heap size 949 MB, throughput 0.774216
Reading from 10141: heap size 950 MB, throughput 0.71376
Reading from 10141: heap size 951 MB, throughput 0.749878
Reading from 10141: heap size 953 MB, throughput 0.797538
Reading from 10141: heap size 954 MB, throughput 0.813314
Reading from 10141: heap size 956 MB, throughput 0.81492
Reading from 10141: heap size 958 MB, throughput 0.819763
Reading from 10142: heap size 502 MB, throughput 0.976782
Reading from 10141: heap size 959 MB, throughput 0.81995
Reading from 10141: heap size 961 MB, throughput 0.895335
Reading from 10141: heap size 960 MB, throughput 0.882736
Reading from 10141: heap size 964 MB, throughput 0.827743
Reading from 10141: heap size 967 MB, throughput 0.698695
Reading from 10142: heap size 518 MB, throughput 0.9893
Reading from 10141: heap size 968 MB, throughput 0.638544
Reading from 10141: heap size 997 MB, throughput 0.661479
Reading from 10142: heap size 519 MB, throughput 0.986391
Reading from 10141: heap size 998 MB, throughput 0.0714175
Reading from 10141: heap size 1107 MB, throughput 0.435351
Reading from 10141: heap size 1111 MB, throughput 0.657717
Reading from 10141: heap size 1119 MB, throughput 0.675108
Reading from 10142: heap size 537 MB, throughput 0.9902
Reading from 10141: heap size 1121 MB, throughput 0.721692
Reading from 10141: heap size 1127 MB, throughput 0.737259
Reading from 10141: heap size 1131 MB, throughput 0.629176
Equal recommendation: 3132 MB each
Reading from 10142: heap size 538 MB, throughput 0.991232
Reading from 10142: heap size 553 MB, throughput 0.987964
Reading from 10141: heap size 1144 MB, throughput 0.963035
Reading from 10142: heap size 555 MB, throughput 0.994355
Reading from 10142: heap size 570 MB, throughput 0.985628
Reading from 10141: heap size 1145 MB, throughput 0.964504
Reading from 10142: heap size 573 MB, throughput 0.98824
Reading from 10142: heap size 595 MB, throughput 0.979955
Reading from 10141: heap size 1158 MB, throughput 0.97358
Equal recommendation: 3132 MB each
Reading from 10142: heap size 595 MB, throughput 0.987134
Reading from 10141: heap size 1160 MB, throughput 0.963556
Reading from 10142: heap size 617 MB, throughput 0.989835
Reading from 10142: heap size 618 MB, throughput 0.993422
Reading from 10141: heap size 1173 MB, throughput 0.973367
Reading from 10142: heap size 632 MB, throughput 0.986988
Reading from 10142: heap size 635 MB, throughput 0.987307
Reading from 10141: heap size 1177 MB, throughput 0.953653
Equal recommendation: 3132 MB each
Reading from 10142: heap size 657 MB, throughput 0.995883
Reading from 10142: heap size 659 MB, throughput 0.987297
Reading from 10141: heap size 1190 MB, throughput 0.977411
Reading from 10142: heap size 677 MB, throughput 0.989439
Reading from 10141: heap size 1192 MB, throughput 0.972103
Reading from 10142: heap size 677 MB, throughput 0.992446
Reading from 10142: heap size 694 MB, throughput 0.995081
Equal recommendation: 3132 MB each
Reading from 10141: heap size 1188 MB, throughput 0.971925
Reading from 10142: heap size 696 MB, throughput 0.988158
Reading from 10142: heap size 713 MB, throughput 0.98368
Reading from 10141: heap size 1193 MB, throughput 0.97564
Reading from 10142: heap size 713 MB, throughput 0.98812
Reading from 10141: heap size 1195 MB, throughput 0.970657
Reading from 10142: heap size 735 MB, throughput 0.992307
Equal recommendation: 3132 MB each
Reading from 10142: heap size 736 MB, throughput 0.993796
Reading from 10141: heap size 1196 MB, throughput 0.969876
Reading from 10142: heap size 753 MB, throughput 0.990464
Reading from 10142: heap size 756 MB, throughput 0.991984
Reading from 10141: heap size 1201 MB, throughput 0.976654
Reading from 10142: heap size 774 MB, throughput 0.990709
Reading from 10141: heap size 1203 MB, throughput 0.975705
Equal recommendation: 3132 MB each
Reading from 10142: heap size 775 MB, throughput 0.9886
Reading from 10142: heap size 797 MB, throughput 0.986548
Reading from 10141: heap size 1207 MB, throughput 0.975455
Reading from 10142: heap size 797 MB, throughput 0.990691
Reading from 10141: heap size 1211 MB, throughput 0.973403
Reading from 10142: heap size 819 MB, throughput 0.992297
Equal recommendation: 3132 MB each
Reading from 10142: heap size 821 MB, throughput 0.994188
Reading from 10141: heap size 1216 MB, throughput 0.658867
Reading from 10142: heap size 839 MB, throughput 0.991901
Reading from 10142: heap size 841 MB, throughput 0.989657
Reading from 10141: heap size 1299 MB, throughput 0.992355
Reading from 10142: heap size 864 MB, throughput 0.990621
Reading from 10141: heap size 1297 MB, throughput 0.990311
Equal recommendation: 3132 MB each
Reading from 10142: heap size 864 MB, throughput 0.988206
Reading from 10141: heap size 1307 MB, throughput 0.989942
Reading from 10142: heap size 888 MB, throughput 0.99175
Reading from 10141: heap size 1314 MB, throughput 0.98756
Reading from 10142: heap size 888 MB, throughput 0.990552
Equal recommendation: 3132 MB each
Reading from 10142: heap size 913 MB, throughput 0.991962
Reading from 10141: heap size 1315 MB, throughput 0.984999
Reading from 10142: heap size 913 MB, throughput 0.990937
Reading from 10141: heap size 1310 MB, throughput 0.983465
Reading from 10142: heap size 938 MB, throughput 0.9919
Reading from 10141: heap size 1211 MB, throughput 0.984894
Reading from 10142: heap size 939 MB, throughput 0.988369
Equal recommendation: 3132 MB each
Reading from 10141: heap size 1297 MB, throughput 0.98056
Reading from 10142: heap size 963 MB, throughput 0.992941
Reading from 10142: heap size 964 MB, throughput 0.990331
Reading from 10141: heap size 1230 MB, throughput 0.980536
Reading from 10142: heap size 989 MB, throughput 0.992016
Equal recommendation: 3132 MB each
Reading from 10141: heap size 1293 MB, throughput 0.973392
Reading from 10142: heap size 990 MB, throughput 0.995686
Reading from 10141: heap size 1297 MB, throughput 0.977198
Reading from 10142: heap size 1011 MB, throughput 0.994399
Reading from 10142: heap size 1014 MB, throughput 0.984511
Reading from 10141: heap size 1298 MB, throughput 0.970833
Equal recommendation: 3132 MB each
Reading from 10142: heap size 1038 MB, throughput 0.989819
Reading from 10141: heap size 1300 MB, throughput 0.971531
Reading from 10142: heap size 1037 MB, throughput 0.991817
Reading from 10141: heap size 1302 MB, throughput 0.976091
Reading from 10142: heap size 1063 MB, throughput 0.992845
Reading from 10141: heap size 1309 MB, throughput 0.960706
Equal recommendation: 3132 MB each
Reading from 10142: heap size 1065 MB, throughput 0.994846
Reading from 10141: heap size 1312 MB, throughput 0.969946
Reading from 10142: heap size 1088 MB, throughput 0.993195
Reading from 10141: heap size 1320 MB, throughput 0.972937
Reading from 10142: heap size 1091 MB, throughput 0.989012
Equal recommendation: 3132 MB each
Reading from 10141: heap size 1327 MB, throughput 0.96685
Reading from 10142: heap size 1116 MB, throughput 0.992066
Reading from 10141: heap size 1332 MB, throughput 0.970399
Reading from 10142: heap size 1117 MB, throughput 0.991934
Reading from 10141: heap size 1339 MB, throughput 0.969424
Reading from 10142: heap size 1145 MB, throughput 0.990273
Equal recommendation: 3132 MB each
Reading from 10142: heap size 1146 MB, throughput 0.990966
Reading from 10141: heap size 1341 MB, throughput 0.975249
Reading from 10142: heap size 1178 MB, throughput 0.993795
Reading from 10141: heap size 1348 MB, throughput 0.97001
Equal recommendation: 3132 MB each
Reading from 10142: heap size 1178 MB, throughput 0.991094
Reading from 10141: heap size 1348 MB, throughput 0.973125
Reading from 10142: heap size 1207 MB, throughput 0.992722
Reading from 10141: heap size 1353 MB, throughput 0.97016
Reading from 10142: heap size 1207 MB, throughput 0.993106
Equal recommendation: 3132 MB each
Reading from 10142: heap size 1236 MB, throughput 0.989671
Reading from 10141: heap size 1353 MB, throughput 0.984429
Reading from 10142: heap size 1237 MB, throughput 0.992066
Reading from 10142: heap size 1273 MB, throughput 0.987634
Equal recommendation: 3132 MB each
Reading from 10142: heap size 1274 MB, throughput 0.990562
Reading from 10142: heap size 1315 MB, throughput 0.987725
Equal recommendation: 3132 MB each
Reading from 10142: heap size 1316 MB, throughput 0.990949
Reading from 10142: heap size 1360 MB, throughput 0.986418
Reading from 10141: heap size 1354 MB, throughput 0.994696
Reading from 10142: heap size 1361 MB, throughput 0.991823
Equal recommendation: 3132 MB each
Reading from 10142: heap size 1404 MB, throughput 0.988834
Reading from 10141: heap size 1358 MB, throughput 0.893701
Reading from 10142: heap size 1407 MB, throughput 0.987891
Reading from 10141: heap size 1395 MB, throughput 0.993042
Reading from 10141: heap size 1427 MB, throughput 0.921327
Reading from 10141: heap size 1432 MB, throughput 0.857823
Reading from 10141: heap size 1437 MB, throughput 0.893551
Reading from 10141: heap size 1430 MB, throughput 0.904638
Reading from 10141: heap size 1436 MB, throughput 0.891113
Reading from 10141: heap size 1432 MB, throughput 0.90024
Equal recommendation: 3132 MB each
Reading from 10141: heap size 1438 MB, throughput 0.99237
Reading from 10142: heap size 1454 MB, throughput 0.990695
Reading from 10141: heap size 1443 MB, throughput 0.991465
Reading from 10142: heap size 1455 MB, throughput 0.989189
Equal recommendation: 3132 MB each
Reading from 10141: heap size 1450 MB, throughput 0.993473
Reading from 10142: heap size 1496 MB, throughput 0.989135
Client 10142 died
Clients: 1
Reading from 10141: heap size 1457 MB, throughput 0.992331
Reading from 10141: heap size 1461 MB, throughput 0.990931
Recommendation: one client; give it all the memory
Reading from 10141: heap size 1448 MB, throughput 0.990715
Reading from 10141: heap size 1318 MB, throughput 0.98957
Recommendation: one client; give it all the memory
Reading from 10141: heap size 1434 MB, throughput 0.987768
Reading from 10141: heap size 1342 MB, throughput 0.98722
Recommendation: one client; give it all the memory
Reading from 10141: heap size 1429 MB, throughput 0.985753
Reading from 10141: heap size 1434 MB, throughput 0.985682
Recommendation: one client; give it all the memory
Reading from 10141: heap size 1436 MB, throughput 0.983978
Reading from 10141: heap size 1437 MB, throughput 0.983695
Recommendation: one client; give it all the memory
Reading from 10141: heap size 1440 MB, throughput 0.983055
Reading from 10141: heap size 1446 MB, throughput 0.981916
Recommendation: one client; give it all the memory
Reading from 10141: heap size 1450 MB, throughput 0.980907
Reading from 10141: heap size 1459 MB, throughput 0.97976
Recommendation: one client; give it all the memory
Reading from 10141: heap size 1467 MB, throughput 0.980347
Reading from 10141: heap size 1474 MB, throughput 0.979182
Recommendation: one client; give it all the memory
Reading from 10141: heap size 1481 MB, throughput 0.979901
Reading from 10141: heap size 1485 MB, throughput 0.979287
Recommendation: one client; give it all the memory
Reading from 10141: heap size 1492 MB, throughput 0.980767
Reading from 10141: heap size 1494 MB, throughput 0.978129
Recommendation: one client; give it all the memory
Reading from 10141: heap size 1501 MB, throughput 0.97884
Reading from 10141: heap size 1501 MB, throughput 0.979085
Recommendation: one client; give it all the memory
Reading from 10141: heap size 1505 MB, throughput 0.979594
Reading from 10141: heap size 1506 MB, throughput 0.97929
Recommendation: one client; give it all the memory
Reading from 10141: heap size 1507 MB, throughput 0.978485
Reading from 10141: heap size 1509 MB, throughput 0.752085
Recommendation: one client; give it all the memory
Reading from 10141: heap size 1609 MB, throughput 0.995861
Reading from 10141: heap size 1611 MB, throughput 0.994427
Recommendation: one client; give it all the memory
Reading from 10141: heap size 1625 MB, throughput 0.993
Reading from 10141: heap size 1631 MB, throughput 0.991701
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 10141: heap size 1632 MB, throughput 0.994114
Recommendation: one client; give it all the memory
Reading from 10141: heap size 1482 MB, throughput 0.995525
Recommendation: one client; give it all the memory
Reading from 10141: heap size 1629 MB, throughput 0.989371
Reading from 10141: heap size 1630 MB, throughput 0.829574
Reading from 10141: heap size 1616 MB, throughput 0.782623
Reading from 10141: heap size 1648 MB, throughput 0.764387
Reading from 10141: heap size 1679 MB, throughput 0.792825
Reading from 10141: heap size 1699 MB, throughput 0.773226
Client 10141 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
