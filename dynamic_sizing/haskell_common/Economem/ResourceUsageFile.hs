module Economem.ResourceUsageFile (ResourceUsageFile(..),
                                   PageFaults(..),
                                   getPageFaults,
                                   getPid) where

import Control.Applicative
import Data.Char
import Data.List
import Economem.Common
import Economem.Files
import Economem.Process


timeWrapper :: IO FilePath
timeWrapper = (</> "time_wrapper.sh") <$> measurementScriptsDir

data ResourceUsageFile = ResourceUsageFile FilePath

instance Path ResourceUsageFile where
  getPath (ResourceUsageFile p) = p

instance ArgsModifier ResourceUsageFile where
  modifyArgsInternal ru (cmd, args) = do
    timeCmd <- timeWrapper
    return (timeCmd, [getPath ru, cmd] ++ args)


data PageFaults = PageFaults { majorFaults :: Integer, minorFaults :: Integer } deriving (Eq, Ord)

getPageFaults :: ResourceUsageFile -> IO PageFaults
getPageFaults ru = PageFaults <$> major <*> minor
  where
    major :: IO Integer
    major = findNum ru "Major (requiring I/O) page faults"

    minor :: IO Integer
    minor = findNum ru "Minor (reclaiming a frame) page faults"

getPid :: ResourceUsageFile -> IO Integer
getPid ru = findNum ru "PID"

findNum :: ResourceUsageFile -> String -> IO Integer
findNum ru field = do
  raw <- map (dropWhile isSpace) <$> get (LinesFile (getPath ru))
  let matches = filter (field `isPrefixOf`) raw
  case matches of
   (x:[]) -> readIO (last (words x))
   _ -> fail ("Incorrect format in resource usage file '" ++ getPath ru ++ "'")
