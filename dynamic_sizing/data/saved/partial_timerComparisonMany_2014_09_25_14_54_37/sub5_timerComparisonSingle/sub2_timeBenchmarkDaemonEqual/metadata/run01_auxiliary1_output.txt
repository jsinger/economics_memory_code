economemd
    total memory: 7435 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub5_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
TimerThread: starting
ReadingThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 9137: heap size 9 MB, throughput 0.991884
Clients: 1
Client 9137 has a minimum heap size of 1211 MB
Reading from 9138: heap size 9 MB, throughput 0.992004
Clients: 2
Client 9138 has a minimum heap size of 276 MB
Reading from 9137: heap size 9 MB, throughput 0.974017
Reading from 9138: heap size 9 MB, throughput 0.981371
Reading from 9137: heap size 9 MB, throughput 0.956261
Reading from 9138: heap size 9 MB, throughput 0.961956
Reading from 9137: heap size 9 MB, throughput 0.94701
Reading from 9138: heap size 9 MB, throughput 0.950177
Reading from 9137: heap size 11 MB, throughput 0.968853
Reading from 9138: heap size 11 MB, throughput 0.97768
Reading from 9138: heap size 11 MB, throughput 0.970408
Reading from 9137: heap size 11 MB, throughput 0.979164
Reading from 9138: heap size 17 MB, throughput 0.803438
Reading from 9137: heap size 17 MB, throughput 0.925704
Reading from 9138: heap size 17 MB, throughput 0.565099
Reading from 9137: heap size 17 MB, throughput 0.555369
Reading from 9138: heap size 30 MB, throughput 0.821906
Reading from 9137: heap size 30 MB, throughput 0.865331
Reading from 9138: heap size 31 MB, throughput 0.847551
Reading from 9137: heap size 31 MB, throughput 0.814985
Reading from 9138: heap size 36 MB, throughput 0.276397
Reading from 9138: heap size 48 MB, throughput 0.859834
Reading from 9137: heap size 35 MB, throughput 0.351606
Reading from 9137: heap size 49 MB, throughput 0.743952
Reading from 9137: heap size 50 MB, throughput 0.806172
Reading from 9138: heap size 50 MB, throughput 0.477918
Reading from 9138: heap size 64 MB, throughput 0.884874
Reading from 9137: heap size 53 MB, throughput 0.363955
Reading from 9138: heap size 72 MB, throughput 0.322468
Reading from 9137: heap size 77 MB, throughput 0.828655
Reading from 9138: heap size 88 MB, throughput 0.638902
Reading from 9138: heap size 96 MB, throughput 0.811691
Reading from 9137: heap size 77 MB, throughput 0.21186
Reading from 9137: heap size 104 MB, throughput 0.707848
Reading from 9138: heap size 97 MB, throughput 0.336743
Reading from 9137: heap size 105 MB, throughput 0.809416
Reading from 9137: heap size 107 MB, throughput 0.687427
Reading from 9138: heap size 131 MB, throughput 0.689553
Reading from 9137: heap size 111 MB, throughput 0.198915
Reading from 9138: heap size 132 MB, throughput 0.246554
Reading from 9137: heap size 139 MB, throughput 0.734181
Reading from 9138: heap size 170 MB, throughput 0.821705
Reading from 9137: heap size 143 MB, throughput 0.809197
Reading from 9138: heap size 170 MB, throughput 0.783528
Reading from 9137: heap size 145 MB, throughput 0.752299
Reading from 9138: heap size 172 MB, throughput 0.772208
Reading from 9137: heap size 148 MB, throughput 0.115153
Reading from 9138: heap size 176 MB, throughput 0.183813
Reading from 9137: heap size 189 MB, throughput 0.732868
Reading from 9138: heap size 214 MB, throughput 0.775468
Reading from 9137: heap size 191 MB, throughput 0.752561
Reading from 9137: heap size 193 MB, throughput 0.641069
Reading from 9137: heap size 199 MB, throughput 0.66746
Reading from 9138: heap size 220 MB, throughput 0.869965
Reading from 9137: heap size 201 MB, throughput 0.630136
Reading from 9137: heap size 207 MB, throughput 0.639949
Reading from 9137: heap size 211 MB, throughput 0.570889
Reading from 9138: heap size 220 MB, throughput 0.909113
Reading from 9137: heap size 220 MB, throughput 0.644126
Reading from 9137: heap size 224 MB, throughput 0.489942
Reading from 9137: heap size 235 MB, throughput 0.0889007
Reading from 9138: heap size 224 MB, throughput 0.16698
Reading from 9137: heap size 279 MB, throughput 0.409851
Reading from 9138: heap size 276 MB, throughput 0.735765
Reading from 9138: heap size 276 MB, throughput 0.932079
Reading from 9138: heap size 273 MB, throughput 0.887891
Reading from 9137: heap size 285 MB, throughput 0.109013
Reading from 9137: heap size 327 MB, throughput 0.469396
Reading from 9138: heap size 275 MB, throughput 0.916121
Reading from 9137: heap size 266 MB, throughput 0.641779
Reading from 9138: heap size 276 MB, throughput 0.880457
Reading from 9137: heap size 318 MB, throughput 0.663256
Reading from 9138: heap size 277 MB, throughput 0.746711
Reading from 9137: heap size 323 MB, throughput 0.699851
Reading from 9138: heap size 281 MB, throughput 0.838831
Reading from 9137: heap size 324 MB, throughput 0.592588
Reading from 9138: heap size 283 MB, throughput 0.839376
Reading from 9137: heap size 325 MB, throughput 0.658005
Reading from 9137: heap size 327 MB, throughput 0.578886
Reading from 9137: heap size 333 MB, throughput 0.571114
Reading from 9137: heap size 336 MB, throughput 0.492874
Reading from 9138: heap size 287 MB, throughput 0.940327
Reading from 9137: heap size 345 MB, throughput 0.451389
Reading from 9137: heap size 350 MB, throughput 0.538152
Reading from 9138: heap size 288 MB, throughput 0.831287
Reading from 9137: heap size 354 MB, throughput 0.494913
Reading from 9138: heap size 292 MB, throughput 0.79731
Reading from 9138: heap size 295 MB, throughput 0.71838
Reading from 9138: heap size 300 MB, throughput 0.746825
Reading from 9138: heap size 302 MB, throughput 0.900718
Reading from 9137: heap size 358 MB, throughput 0.0797145
Reading from 9138: heap size 304 MB, throughput 0.792211
Reading from 9137: heap size 407 MB, throughput 0.323206
Reading from 9138: heap size 305 MB, throughput 0.645416
Reading from 9137: heap size 411 MB, throughput 0.500766
Reading from 9138: heap size 309 MB, throughput 0.71159
Reading from 9137: heap size 413 MB, throughput 0.607306
Reading from 9138: heap size 310 MB, throughput 0.728112
Reading from 9137: heap size 417 MB, throughput 0.489392
Reading from 9138: heap size 317 MB, throughput 0.697297
Reading from 9138: heap size 319 MB, throughput 0.561664
Reading from 9137: heap size 418 MB, throughput 0.469056
Reading from 9138: heap size 326 MB, throughput 0.614014
Equal recommendation: 3717 MB each
Reading from 9137: heap size 422 MB, throughput 0.0638787
Reading from 9137: heap size 476 MB, throughput 0.343781
Reading from 9137: heap size 468 MB, throughput 0.530381
Reading from 9137: heap size 473 MB, throughput 0.532724
Reading from 9137: heap size 473 MB, throughput 0.530339
Reading from 9137: heap size 473 MB, throughput 0.485715
Reading from 9138: heap size 327 MB, throughput 0.985994
Reading from 9137: heap size 477 MB, throughput 0.49583
Reading from 9137: heap size 479 MB, throughput 0.533587
Reading from 9137: heap size 483 MB, throughput 0.100326
Reading from 9138: heap size 330 MB, throughput 0.974916
Reading from 9137: heap size 543 MB, throughput 0.447144
Reading from 9137: heap size 551 MB, throughput 0.467416
Reading from 9137: heap size 554 MB, throughput 0.586773
Reading from 9137: heap size 560 MB, throughput 0.538114
Reading from 9137: heap size 567 MB, throughput 0.50291
Reading from 9137: heap size 573 MB, throughput 0.60512
Reading from 9138: heap size 333 MB, throughput 0.957881
Reading from 9137: heap size 580 MB, throughput 0.481446
Reading from 9137: heap size 585 MB, throughput 0.0827943
Reading from 9138: heap size 335 MB, throughput 0.973026
Reading from 9137: heap size 652 MB, throughput 0.347549
Reading from 9137: heap size 663 MB, throughput 0.414776
Reading from 9137: heap size 664 MB, throughput 0.800122
Reading from 9138: heap size 336 MB, throughput 0.972939
Reading from 9137: heap size 667 MB, throughput 0.788261
Reading from 9138: heap size 334 MB, throughput 0.929308
Reading from 9137: heap size 670 MB, throughput 0.266695
Reading from 9137: heap size 739 MB, throughput 0.65459
Reading from 9137: heap size 749 MB, throughput 0.434621
Reading from 9138: heap size 337 MB, throughput 0.945964
Reading from 9137: heap size 752 MB, throughput 0.622292
Reading from 9137: heap size 753 MB, throughput 0.345753
Reading from 9137: heap size 741 MB, throughput 0.373841
Equal recommendation: 3717 MB each
Reading from 9138: heap size 337 MB, throughput 0.953743
Reading from 9138: heap size 338 MB, throughput 0.975128
Reading from 9137: heap size 748 MB, throughput 0.0202908
Reading from 9137: heap size 828 MB, throughput 0.229557
Reading from 9137: heap size 828 MB, throughput 0.868433
Reading from 9137: heap size 833 MB, throughput 0.505131
Reading from 9137: heap size 835 MB, throughput 0.665442
Reading from 9137: heap size 835 MB, throughput 0.611492
Reading from 9137: heap size 837 MB, throughput 0.819511
Reading from 9138: heap size 341 MB, throughput 0.95382
Reading from 9137: heap size 841 MB, throughput 0.799329
Reading from 9138: heap size 342 MB, throughput 0.963621
Reading from 9137: heap size 842 MB, throughput 0.0446513
Reading from 9137: heap size 938 MB, throughput 0.684024
Reading from 9137: heap size 941 MB, throughput 0.924844
Reading from 9137: heap size 948 MB, throughput 0.859529
Reading from 9137: heap size 813 MB, throughput 0.773298
Reading from 9137: heap size 932 MB, throughput 0.667459
Reading from 9138: heap size 345 MB, throughput 0.663481
Reading from 9137: heap size 819 MB, throughput 0.855642
Reading from 9137: heap size 923 MB, throughput 0.784601
Reading from 9137: heap size 931 MB, throughput 0.828671
Reading from 9137: heap size 917 MB, throughput 0.828199
Reading from 9137: heap size 924 MB, throughput 0.83645
Reading from 9137: heap size 916 MB, throughput 0.824802
Reading from 9137: heap size 921 MB, throughput 0.86675
Reading from 9138: heap size 386 MB, throughput 0.968751
Reading from 9137: heap size 913 MB, throughput 0.974987
Reading from 9138: heap size 391 MB, throughput 0.980964
Equal recommendation: 3717 MB each
Reading from 9137: heap size 918 MB, throughput 0.941092
Reading from 9137: heap size 915 MB, throughput 0.797511
Reading from 9137: heap size 919 MB, throughput 0.788554
Reading from 9137: heap size 914 MB, throughput 0.800343
Reading from 9137: heap size 918 MB, throughput 0.75785
Reading from 9137: heap size 914 MB, throughput 0.812355
Reading from 9137: heap size 918 MB, throughput 0.768065
Reading from 9138: heap size 391 MB, throughput 0.98682
Reading from 9137: heap size 917 MB, throughput 0.854175
Reading from 9138: heap size 390 MB, throughput 0.886506
Reading from 9138: heap size 391 MB, throughput 0.838742
Reading from 9137: heap size 920 MB, throughput 0.912848
Reading from 9138: heap size 396 MB, throughput 0.818162
Reading from 9138: heap size 398 MB, throughput 0.830391
Reading from 9137: heap size 924 MB, throughput 0.848674
Reading from 9137: heap size 925 MB, throughput 0.845902
Reading from 9138: heap size 405 MB, throughput 0.94463
Reading from 9137: heap size 929 MB, throughput 0.12566
Reading from 9138: heap size 407 MB, throughput 0.988354
Reading from 9137: heap size 1026 MB, throughput 0.445836
Reading from 9137: heap size 1056 MB, throughput 0.689865
Reading from 9137: heap size 1056 MB, throughput 0.710825
Reading from 9137: heap size 1065 MB, throughput 0.754836
Reading from 9137: heap size 1067 MB, throughput 0.73478
Reading from 9137: heap size 1074 MB, throughput 0.73434
Reading from 9137: heap size 1077 MB, throughput 0.731878
Reading from 9138: heap size 410 MB, throughput 0.985608
Reading from 9137: heap size 1083 MB, throughput 0.736644
Reading from 9137: heap size 1086 MB, throughput 0.855764
Reading from 9138: heap size 412 MB, throughput 0.978214
Reading from 9137: heap size 1095 MB, throughput 0.956736
Reading from 9138: heap size 410 MB, throughput 0.985848
Equal recommendation: 3717 MB each
Reading from 9137: heap size 1098 MB, throughput 0.950745
Reading from 9138: heap size 413 MB, throughput 0.984164
Reading from 9137: heap size 1112 MB, throughput 0.955213
Reading from 9138: heap size 411 MB, throughput 0.985374
Reading from 9138: heap size 413 MB, throughput 0.982352
Reading from 9137: heap size 1115 MB, throughput 0.934009
Reading from 9138: heap size 414 MB, throughput 0.983562
Reading from 9137: heap size 1126 MB, throughput 0.953997
Reading from 9138: heap size 414 MB, throughput 0.981628
Reading from 9137: heap size 1133 MB, throughput 0.651314
Reading from 9138: heap size 417 MB, throughput 0.981978
Equal recommendation: 3717 MB each
Reading from 9137: heap size 1240 MB, throughput 0.990028
Reading from 9138: heap size 417 MB, throughput 0.99016
Reading from 9138: heap size 419 MB, throughput 0.8878
Reading from 9138: heap size 419 MB, throughput 0.845297
Reading from 9138: heap size 424 MB, throughput 0.849877
Reading from 9138: heap size 426 MB, throughput 0.96989
Reading from 9137: heap size 1243 MB, throughput 0.986334
Reading from 9138: heap size 433 MB, throughput 0.990182
Reading from 9137: heap size 1257 MB, throughput 0.981989
Reading from 9138: heap size 434 MB, throughput 0.989485
Reading from 9137: heap size 1262 MB, throughput 0.981365
Reading from 9138: heap size 435 MB, throughput 0.987539
Reading from 9137: heap size 1264 MB, throughput 0.975789
Equal recommendation: 3717 MB each
Reading from 9138: heap size 437 MB, throughput 0.988023
Reading from 9137: heap size 1267 MB, throughput 0.972528
Reading from 9138: heap size 435 MB, throughput 0.98829
Reading from 9137: heap size 1257 MB, throughput 0.975334
Reading from 9138: heap size 437 MB, throughput 0.985601
Reading from 9137: heap size 1184 MB, throughput 0.969654
Reading from 9138: heap size 435 MB, throughput 0.986608
Reading from 9137: heap size 1251 MB, throughput 0.966803
Reading from 9138: heap size 437 MB, throughput 0.983926
Equal recommendation: 3717 MB each
Reading from 9137: heap size 1257 MB, throughput 0.961368
Reading from 9138: heap size 439 MB, throughput 0.981673
Reading from 9138: heap size 439 MB, throughput 0.974893
Reading from 9137: heap size 1259 MB, throughput 0.960056
Reading from 9138: heap size 444 MB, throughput 0.894258
Reading from 9138: heap size 444 MB, throughput 0.859396
Reading from 9138: heap size 449 MB, throughput 0.971099
Reading from 9137: heap size 1260 MB, throughput 0.968285
Reading from 9138: heap size 451 MB, throughput 0.991326
Reading from 9137: heap size 1264 MB, throughput 0.961445
Reading from 9138: heap size 454 MB, throughput 0.990509
Reading from 9137: heap size 1269 MB, throughput 0.953072
Equal recommendation: 3717 MB each
Reading from 9138: heap size 455 MB, throughput 0.988663
Reading from 9137: heap size 1274 MB, throughput 0.955625
Reading from 9138: heap size 454 MB, throughput 0.987534
Reading from 9138: heap size 456 MB, throughput 0.986058
Reading from 9137: heap size 1282 MB, throughput 0.953703
Reading from 9138: heap size 455 MB, throughput 0.986995
Reading from 9137: heap size 1290 MB, throughput 0.953131
Reading from 9138: heap size 456 MB, throughput 0.976413
Reading from 9137: heap size 1298 MB, throughput 0.940663
Equal recommendation: 3717 MB each
Reading from 9138: heap size 458 MB, throughput 0.983048
Reading from 9137: heap size 1306 MB, throughput 0.949412
Reading from 9138: heap size 458 MB, throughput 0.991041
Reading from 9138: heap size 462 MB, throughput 0.897459
Reading from 9138: heap size 462 MB, throughput 0.87863
Reading from 9137: heap size 1311 MB, throughput 0.954714
Reading from 9138: heap size 467 MB, throughput 0.988419
Reading from 9137: heap size 1320 MB, throughput 0.950027
Reading from 9138: heap size 469 MB, throughput 0.991307
Reading from 9137: heap size 1322 MB, throughput 0.952333
Reading from 9138: heap size 471 MB, throughput 0.991675
Equal recommendation: 3717 MB each
Reading from 9137: heap size 1332 MB, throughput 0.950405
Reading from 9138: heap size 473 MB, throughput 0.989835
Reading from 9138: heap size 472 MB, throughput 0.989245
Reading from 9137: heap size 1333 MB, throughput 0.949345
Reading from 9138: heap size 474 MB, throughput 0.986952
Reading from 9137: heap size 1342 MB, throughput 0.961647
Reading from 9138: heap size 473 MB, throughput 0.987973
Reading from 9137: heap size 1342 MB, throughput 0.952621
Equal recommendation: 3717 MB each
Reading from 9138: heap size 474 MB, throughput 0.985386
Reading from 9137: heap size 1351 MB, throughput 0.956339
Reading from 9138: heap size 476 MB, throughput 0.986022
Reading from 9138: heap size 477 MB, throughput 0.886748
Reading from 9138: heap size 479 MB, throughput 0.891989
Reading from 9138: heap size 480 MB, throughput 0.989802
Reading from 9137: heap size 1351 MB, throughput 0.955163
Reading from 9138: heap size 487 MB, throughput 0.991735
Reading from 9137: heap size 1358 MB, throughput 0.551528
Equal recommendation: 3717 MB each
Reading from 9138: heap size 488 MB, throughput 0.991369
Reading from 9137: heap size 1480 MB, throughput 0.94357
Reading from 9138: heap size 487 MB, throughput 0.989868
Reading from 9138: heap size 490 MB, throughput 0.988538
Reading from 9137: heap size 1479 MB, throughput 0.981199
Reading from 9138: heap size 489 MB, throughput 0.988282
Reading from 9137: heap size 1488 MB, throughput 0.977088
Equal recommendation: 3717 MB each
Reading from 9138: heap size 490 MB, throughput 0.986143
Reading from 9137: heap size 1501 MB, throughput 0.974327
Reading from 9138: heap size 493 MB, throughput 0.990419
Reading from 9138: heap size 493 MB, throughput 0.907372
Reading from 9138: heap size 493 MB, throughput 0.899092
Reading from 9138: heap size 496 MB, throughput 0.990482
Reading from 9138: heap size 501 MB, throughput 0.99218
Reading from 9137: heap size 1502 MB, throughput 0.990048
Equal recommendation: 3717 MB each
Reading from 9138: heap size 503 MB, throughput 0.992515
Reading from 9138: heap size 503 MB, throughput 0.98843
Reading from 9138: heap size 505 MB, throughput 0.9911
Reading from 9138: heap size 504 MB, throughput 0.989221
Equal recommendation: 3717 MB each
Reading from 9138: heap size 505 MB, throughput 0.991766
Reading from 9138: heap size 505 MB, throughput 0.971511
Reading from 9138: heap size 507 MB, throughput 0.895913
Reading from 9138: heap size 510 MB, throughput 0.989414
Reading from 9137: heap size 1488 MB, throughput 0.991121
Reading from 9138: heap size 512 MB, throughput 0.993569
Equal recommendation: 3717 MB each
Reading from 9138: heap size 515 MB, throughput 0.990011
Reading from 9137: heap size 1498 MB, throughput 0.982601
Reading from 9137: heap size 1484 MB, throughput 0.820466
Reading from 9137: heap size 1509 MB, throughput 0.696014
Reading from 9137: heap size 1530 MB, throughput 0.69144
Reading from 9137: heap size 1561 MB, throughput 0.683828
Reading from 9138: heap size 516 MB, throughput 0.990885
Reading from 9137: heap size 1593 MB, throughput 0.724625
Reading from 9137: heap size 1612 MB, throughput 0.690853
Reading from 9137: heap size 1648 MB, throughput 0.928592
Reading from 9138: heap size 514 MB, throughput 0.990154
Reading from 9137: heap size 1658 MB, throughput 0.962716
Reading from 9138: heap size 516 MB, throughput 0.98891
Equal recommendation: 3717 MB each
Reading from 9137: heap size 1633 MB, throughput 0.961715
Reading from 9138: heap size 518 MB, throughput 0.991383
Reading from 9138: heap size 518 MB, throughput 0.958194
Reading from 9138: heap size 518 MB, throughput 0.907992
Reading from 9137: heap size 1653 MB, throughput 0.960168
Reading from 9138: heap size 520 MB, throughput 0.990206
Reading from 9137: heap size 1633 MB, throughput 0.966193
Reading from 9138: heap size 527 MB, throughput 0.992736
Equal recommendation: 3717 MB each
Reading from 9137: heap size 1649 MB, throughput 0.966885
Reading from 9138: heap size 527 MB, throughput 0.992233
Reading from 9137: heap size 1633 MB, throughput 0.973166
Reading from 9138: heap size 528 MB, throughput 0.991154
Reading from 9138: heap size 529 MB, throughput 0.989485
Reading from 9137: heap size 1646 MB, throughput 0.757046
Equal recommendation: 3717 MB each
Reading from 9138: heap size 529 MB, throughput 0.990683
Reading from 9137: heap size 1618 MB, throughput 0.990252
Reading from 9138: heap size 530 MB, throughput 0.993154
Reading from 9138: heap size 533 MB, throughput 0.914043
Reading from 9138: heap size 533 MB, throughput 0.982282
Reading from 9137: heap size 1622 MB, throughput 0.988913
Reading from 9138: heap size 540 MB, throughput 0.993437
Reading from 9137: heap size 1646 MB, throughput 0.985923
Equal recommendation: 3717 MB each
Reading from 9138: heap size 540 MB, throughput 0.993189
Reading from 9137: heap size 1653 MB, throughput 0.981955
Reading from 9138: heap size 540 MB, throughput 0.99193
Reading from 9137: heap size 1657 MB, throughput 0.978862
Reading from 9138: heap size 542 MB, throughput 0.991736
Equal recommendation: 3717 MB each
Reading from 9137: heap size 1662 MB, throughput 0.977151
Reading from 9138: heap size 541 MB, throughput 0.989741
Reading from 9137: heap size 1647 MB, throughput 0.975233
Reading from 9138: heap size 542 MB, throughput 0.994462
Reading from 9138: heap size 545 MB, throughput 0.929544
Reading from 9138: heap size 545 MB, throughput 0.976708
Reading from 9137: heap size 1493 MB, throughput 0.977944
Reading from 9138: heap size 552 MB, throughput 0.993346
Equal recommendation: 3717 MB each
Reading from 9138: heap size 552 MB, throughput 0.992612
Reading from 9137: heap size 1629 MB, throughput 0.973442
Reading from 9138: heap size 551 MB, throughput 0.991324
Reading from 9137: heap size 1643 MB, throughput 0.971385
Reading from 9138: heap size 553 MB, throughput 0.990987
Equal recommendation: 3717 MB each
Reading from 9137: heap size 1639 MB, throughput 0.967184
Reading from 9138: heap size 554 MB, throughput 0.989615
Reading from 9138: heap size 555 MB, throughput 0.989714
Reading from 9138: heap size 556 MB, throughput 0.920087
Reading from 9137: heap size 1643 MB, throughput 0.963178
Reading from 9138: heap size 559 MB, throughput 0.987486
Reading from 9137: heap size 1652 MB, throughput 0.963298
Equal recommendation: 3717 MB each
Reading from 9138: heap size 564 MB, throughput 0.993587
Reading from 9138: heap size 565 MB, throughput 0.992002
Reading from 9137: heap size 1657 MB, throughput 0.959653
Reading from 9138: heap size 564 MB, throughput 0.991692
Reading from 9137: heap size 1670 MB, throughput 0.960862
Equal recommendation: 3717 MB each
Reading from 9138: heap size 566 MB, throughput 0.990155
Reading from 9137: heap size 1680 MB, throughput 0.955683
Reading from 9138: heap size 567 MB, throughput 0.995091
Reading from 9138: heap size 568 MB, throughput 0.938163
Reading from 9138: heap size 567 MB, throughput 0.967283
Reading from 9137: heap size 1699 MB, throughput 0.961909
Reading from 9138: heap size 570 MB, throughput 0.993449
Equal recommendation: 3717 MB each
Reading from 9137: heap size 1709 MB, throughput 0.954394
Reading from 9138: heap size 572 MB, throughput 0.993419
Reading from 9138: heap size 573 MB, throughput 0.992095
Reading from 9138: heap size 572 MB, throughput 0.990443
Equal recommendation: 3717 MB each
Reading from 9138: heap size 573 MB, throughput 0.987589
Reading from 9138: heap size 573 MB, throughput 0.984853
Reading from 9138: heap size 577 MB, throughput 0.923092
Client 9138 died
Clients: 1
Reading from 9137: heap size 1730 MB, throughput 0.98646
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 9137: heap size 1736 MB, throughput 0.992931
Reading from 9137: heap size 1770 MB, throughput 0.889365
Reading from 9137: heap size 1776 MB, throughput 0.777289
Reading from 9137: heap size 1805 MB, throughput 0.808126
Reading from 9137: heap size 1821 MB, throughput 0.854033
Client 9137 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
