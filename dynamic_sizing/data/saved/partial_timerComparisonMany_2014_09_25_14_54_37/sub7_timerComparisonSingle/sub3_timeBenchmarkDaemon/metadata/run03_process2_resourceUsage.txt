	Command being timed: "cgexec -g memory:economem /home/callum/economics_memory_code/dynamic_sizing/hotspot8/openjdk8/jdk8/build/linux-x86_64-normal-server-release/jdk/bin/java -XX:+EconomemVengerovThroughput -XX:EconomemMinHeap=8 -Xms10m -Xmx1219m -XX:+DisableExplicitGC -XX:+UseAdaptiveSizePolicyWithSystemGC -jar /home/callum/economics_memory_code/dynamic_sizing/haskell_common/dacapo-9.12-bach.jar --no-pre-iteration-gc --scratch-directory /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub7_timerComparisonSingle/sub3_timeBenchmarkDaemon/scratch2 -t 2 -n 32 -s large sunflow"
	User time (seconds): 620.47
	System time (seconds): 5.27
	Percent of CPU this job got: 118%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 8:48.97
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 479140
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 34453
	Minor (reclaiming a frame) page faults: 322389
	Voluntary context switches: 63795
	Involuntary context switches: 94979
	Swaps: 0
	File system inputs: 2084272
	File system outputs: 19064
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
