economemd
    total memory: 7314 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub10_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
TimerThread: starting
ReadingThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 31359: heap size 9 MB, throughput 0.990637
Clients: 1
Client 31359 has a minimum heap size of 8 MB
Reading from 31358: heap size 9 MB, throughput 0.991256
Clients: 2
Client 31358 has a minimum heap size of 1211 MB
Reading from 31358: heap size 9 MB, throughput 0.97948
Reading from 31359: heap size 9 MB, throughput 0.983215
Reading from 31358: heap size 9 MB, throughput 0.96448
Reading from 31358: heap size 9 MB, throughput 0.950108
Reading from 31359: heap size 9 MB, throughput 0.975074
Reading from 31359: heap size 9 MB, throughput 0.93232
Reading from 31359: heap size 11 MB, throughput 0.901049
Reading from 31358: heap size 11 MB, throughput 0.985963
Reading from 31359: heap size 11 MB, throughput 0.974941
Reading from 31359: heap size 17 MB, throughput 0.926003
Reading from 31358: heap size 11 MB, throughput 0.988474
Reading from 31358: heap size 17 MB, throughput 0.888381
Reading from 31359: heap size 17 MB, throughput 0.977827
Reading from 31359: heap size 25 MB, throughput 0.715751
Reading from 31359: heap size 28 MB, throughput 0.973594
Reading from 31358: heap size 17 MB, throughput 0.420638
Reading from 31359: heap size 30 MB, throughput 0.969865
Reading from 31359: heap size 34 MB, throughput 0.979656
Reading from 31358: heap size 30 MB, throughput 0.976195
Reading from 31359: heap size 39 MB, throughput 0.981909
Reading from 31358: heap size 31 MB, throughput 0.90398
Reading from 31359: heap size 39 MB, throughput 0.97816
Reading from 31359: heap size 46 MB, throughput 0.979334
Reading from 31359: heap size 46 MB, throughput 0.979723
Reading from 31358: heap size 34 MB, throughput 0.491154
Reading from 31359: heap size 55 MB, throughput 0.97587
Reading from 31358: heap size 48 MB, throughput 0.854679
Reading from 31359: heap size 55 MB, throughput 0.984626
Reading from 31358: heap size 51 MB, throughput 0.798826
Reading from 31359: heap size 63 MB, throughput 0.979271
Reading from 31359: heap size 63 MB, throughput 0.985528
Reading from 31358: heap size 52 MB, throughput 0.22261
Reading from 31359: heap size 70 MB, throughput 0.988216
Reading from 31358: heap size 76 MB, throughput 0.752821
Reading from 31359: heap size 70 MB, throughput 0.986553
Reading from 31359: heap size 78 MB, throughput 0.992396
Reading from 31358: heap size 76 MB, throughput 0.268041
Reading from 31359: heap size 79 MB, throughput 0.988523
Reading from 31358: heap size 99 MB, throughput 0.796042
Reading from 31358: heap size 102 MB, throughput 0.780956
Reading from 31359: heap size 87 MB, throughput 0.991299
Reading from 31359: heap size 87 MB, throughput 0.991994
Reading from 31358: heap size 104 MB, throughput 0.240679
Reading from 31359: heap size 93 MB, throughput 0.993268
Reading from 31358: heap size 132 MB, throughput 0.62378
Reading from 31359: heap size 93 MB, throughput 0.987074
Reading from 31358: heap size 137 MB, throughput 0.743199
Reading from 31358: heap size 138 MB, throughput 0.696645
Reading from 31359: heap size 100 MB, throughput 0.991434
Reading from 31358: heap size 147 MB, throughput 0.710108
Reading from 31359: heap size 100 MB, throughput 0.991568
Reading from 31359: heap size 106 MB, throughput 0.995151
Reading from 31359: heap size 106 MB, throughput 0.991158
Reading from 31358: heap size 148 MB, throughput 0.141053
Reading from 31359: heap size 112 MB, throughput 0.996261
Reading from 31358: heap size 195 MB, throughput 0.6418
Reading from 31359: heap size 112 MB, throughput 0.989344
Reading from 31358: heap size 197 MB, throughput 0.721965
Reading from 31358: heap size 204 MB, throughput 0.783433
Reading from 31359: heap size 117 MB, throughput 0.99609
Reading from 31359: heap size 117 MB, throughput 0.990323
Reading from 31359: heap size 122 MB, throughput 0.995042
Reading from 31358: heap size 206 MB, throughput 0.17234
Reading from 31359: heap size 122 MB, throughput 0.995165
Reading from 31358: heap size 248 MB, throughput 0.719078
Reading from 31358: heap size 256 MB, throughput 0.714424
Reading from 31359: heap size 127 MB, throughput 0.991684
Reading from 31358: heap size 258 MB, throughput 0.702837
Reading from 31359: heap size 127 MB, throughput 0.993361
Reading from 31358: heap size 262 MB, throughput 0.623231
Reading from 31359: heap size 133 MB, throughput 0.995254
Reading from 31359: heap size 133 MB, throughput 0.992617
Reading from 31359: heap size 138 MB, throughput 0.993507
Reading from 31358: heap size 265 MB, throughput 0.13317
Reading from 31359: heap size 138 MB, throughput 0.989243
Reading from 31358: heap size 321 MB, throughput 0.537284
Reading from 31358: heap size 330 MB, throughput 0.681936
Reading from 31359: heap size 142 MB, throughput 0.996523
Reading from 31358: heap size 332 MB, throughput 0.627375
Reading from 31359: heap size 142 MB, throughput 0.99683
Reading from 31358: heap size 337 MB, throughput 0.572878
Reading from 31358: heap size 346 MB, throughput 0.528627
Reading from 31359: heap size 147 MB, throughput 0.992665
Reading from 31358: heap size 351 MB, throughput 0.487666
Reading from 31358: heap size 364 MB, throughput 0.564877
Reading from 31359: heap size 148 MB, throughput 0.992004
Reading from 31358: heap size 371 MB, throughput 0.544055
Reading from 31359: heap size 153 MB, throughput 0.996641
Reading from 31359: heap size 153 MB, throughput 0.995749
Reading from 31358: heap size 379 MB, throughput 0.0937598
Reading from 31359: heap size 157 MB, throughput 0.996683
Reading from 31358: heap size 438 MB, throughput 0.371745
Reading from 31359: heap size 158 MB, throughput 0.99607
Reading from 31359: heap size 162 MB, throughput 0.995335
Reading from 31358: heap size 366 MB, throughput 0.0976709
Reading from 31358: heap size 486 MB, throughput 0.541194
Reading from 31359: heap size 162 MB, throughput 0.997244
Reading from 31358: heap size 492 MB, throughput 0.601842
Reading from 31358: heap size 484 MB, throughput 0.627163
Reading from 31359: heap size 166 MB, throughput 0.995911
Reading from 31358: heap size 489 MB, throughput 0.552975
Reading from 31359: heap size 166 MB, throughput 0.995747
Reading from 31358: heap size 487 MB, throughput 0.557324
Reading from 31359: heap size 170 MB, throughput 0.993457
Reading from 31358: heap size 488 MB, throughput 0.578301
Reading from 31358: heap size 490 MB, throughput 0.530423
Reading from 31359: heap size 170 MB, throughput 0.996475
Reading from 31358: heap size 496 MB, throughput 0.43966
Reading from 31359: heap size 174 MB, throughput 0.997702
Reading from 31358: heap size 504 MB, throughput 0.495883
Equal recommendation: 3657 MB each
Reading from 31359: heap size 174 MB, throughput 0.9958
Reading from 31358: heap size 512 MB, throughput 0.44557
Reading from 31358: heap size 518 MB, throughput 0.465459
Reading from 31359: heap size 178 MB, throughput 0.997422
Reading from 31359: heap size 178 MB, throughput 0.995629
Reading from 31359: heap size 181 MB, throughput 0.996926
Reading from 31359: heap size 181 MB, throughput 0.996847
Reading from 31358: heap size 525 MB, throughput 0.0741554
Reading from 31358: heap size 582 MB, throughput 0.391423
Reading from 31359: heap size 185 MB, throughput 0.996576
Reading from 31358: heap size 593 MB, throughput 0.531775
Reading from 31358: heap size 598 MB, throughput 0.597906
Reading from 31359: heap size 185 MB, throughput 0.996093
Reading from 31358: heap size 599 MB, throughput 0.515599
Reading from 31359: heap size 189 MB, throughput 0.99641
Reading from 31359: heap size 189 MB, throughput 0.995782
Reading from 31359: heap size 194 MB, throughput 0.996557
Reading from 31359: heap size 194 MB, throughput 0.99704
Reading from 31358: heap size 605 MB, throughput 0.0700909
Reading from 31359: heap size 198 MB, throughput 0.99201
Reading from 31358: heap size 662 MB, throughput 0.391309
Reading from 31358: heap size 664 MB, throughput 0.566862
Reading from 31359: heap size 198 MB, throughput 0.992609
Reading from 31358: heap size 666 MB, throughput 0.799224
Reading from 31359: heap size 203 MB, throughput 0.996648
Reading from 31359: heap size 203 MB, throughput 0.996882
Reading from 31358: heap size 667 MB, throughput 0.822219
Reading from 31359: heap size 208 MB, throughput 0.996925
Reading from 31359: heap size 208 MB, throughput 0.995108
Reading from 31358: heap size 675 MB, throughput 0.773508
Reading from 31359: heap size 213 MB, throughput 0.995464
Reading from 31359: heap size 213 MB, throughput 0.99169
Reading from 31358: heap size 678 MB, throughput 0.752808
Reading from 31359: heap size 219 MB, throughput 0.991951
Reading from 31358: heap size 695 MB, throughput 0.581116
Reading from 31359: heap size 219 MB, throughput 0.995491
Reading from 31359: heap size 226 MB, throughput 0.996243
Reading from 31359: heap size 226 MB, throughput 0.99703
Reading from 31359: heap size 231 MB, throughput 0.997125
Reading from 31358: heap size 706 MB, throughput 0.0756585
Reading from 31358: heap size 782 MB, throughput 0.309576
Reading from 31358: heap size 784 MB, throughput 0.263237
Reading from 31359: heap size 232 MB, throughput 0.995688
Reading from 31358: heap size 694 MB, throughput 0.276334
Reading from 31358: heap size 769 MB, throughput 0.315701
Reading from 31359: heap size 237 MB, throughput 0.997356
Reading from 31359: heap size 237 MB, throughput 0.996375
Equal recommendation: 3657 MB each
Reading from 31358: heap size 776 MB, throughput 0.0378957
Reading from 31359: heap size 243 MB, throughput 0.998341
Reading from 31359: heap size 243 MB, throughput 0.993845
Reading from 31358: heap size 861 MB, throughput 0.739125
Reading from 31358: heap size 861 MB, throughput 0.558302
Reading from 31358: heap size 867 MB, throughput 0.531323
Reading from 31359: heap size 247 MB, throughput 0.997326
Reading from 31359: heap size 247 MB, throughput 0.995865
Reading from 31359: heap size 253 MB, throughput 0.996793
Reading from 31359: heap size 253 MB, throughput 0.996912
Reading from 31358: heap size 869 MB, throughput 0.0387108
Reading from 31359: heap size 257 MB, throughput 0.997695
Reading from 31358: heap size 969 MB, throughput 0.607091
Reading from 31358: heap size 974 MB, throughput 0.746474
Reading from 31359: heap size 257 MB, throughput 0.997207
Reading from 31358: heap size 980 MB, throughput 0.896547
Reading from 31358: heap size 981 MB, throughput 0.846733
Reading from 31359: heap size 261 MB, throughput 0.996058
Reading from 31358: heap size 973 MB, throughput 0.92012
Reading from 31358: heap size 787 MB, throughput 0.794499
Reading from 31359: heap size 263 MB, throughput 0.99707
Reading from 31358: heap size 963 MB, throughput 0.877269
Reading from 31359: heap size 269 MB, throughput 0.996889
Reading from 31358: heap size 823 MB, throughput 0.729248
Reading from 31358: heap size 940 MB, throughput 0.684689
Reading from 31358: heap size 840 MB, throughput 0.70337
Reading from 31359: heap size 269 MB, throughput 0.996987
Reading from 31358: heap size 929 MB, throughput 0.696742
Reading from 31358: heap size 846 MB, throughput 0.693706
Reading from 31358: heap size 924 MB, throughput 0.767018
Reading from 31359: heap size 274 MB, throughput 0.997813
Reading from 31358: heap size 930 MB, throughput 0.701182
Reading from 31358: heap size 919 MB, throughput 0.776521
Reading from 31359: heap size 274 MB, throughput 0.997393
Reading from 31358: heap size 925 MB, throughput 0.774289
Reading from 31358: heap size 918 MB, throughput 0.791393
Reading from 31359: heap size 278 MB, throughput 0.997903
Reading from 31358: heap size 922 MB, throughput 0.936025
Reading from 31359: heap size 278 MB, throughput 0.99723
Reading from 31359: heap size 265 MB, throughput 0.996179
Reading from 31359: heap size 269 MB, throughput 0.997518
Reading from 31359: heap size 257 MB, throughput 0.997054
Reading from 31358: heap size 923 MB, throughput 0.962858
Reading from 31359: heap size 245 MB, throughput 0.998086
Reading from 31358: heap size 926 MB, throughput 0.739982
Reading from 31358: heap size 933 MB, throughput 0.739557
Reading from 31359: heap size 234 MB, throughput 0.997614
Reading from 31358: heap size 933 MB, throughput 0.759944
Reading from 31358: heap size 937 MB, throughput 0.742967
Reading from 31359: heap size 223 MB, throughput 0.997494
Reading from 31358: heap size 938 MB, throughput 0.741033
Reading from 31359: heap size 213 MB, throughput 0.995539
Reading from 31358: heap size 942 MB, throughput 0.810315
Reading from 31358: heap size 943 MB, throughput 0.768323
Reading from 31359: heap size 204 MB, throughput 0.997288
Reading from 31358: heap size 946 MB, throughput 0.756753
Equal recommendation: 3657 MB each
Reading from 31359: heap size 194 MB, throughput 0.995453
Reading from 31358: heap size 948 MB, throughput 0.83533
Reading from 31359: heap size 186 MB, throughput 0.997655
Reading from 31359: heap size 177 MB, throughput 0.997057
Reading from 31358: heap size 950 MB, throughput 0.862146
Reading from 31359: heap size 169 MB, throughput 0.992706
Reading from 31358: heap size 953 MB, throughput 0.772102
Reading from 31359: heap size 173 MB, throughput 0.992098
Reading from 31358: heap size 955 MB, throughput 0.665842
Reading from 31358: heap size 958 MB, throughput 0.608898
Reading from 31359: heap size 178 MB, throughput 0.996542
Reading from 31358: heap size 986 MB, throughput 0.603504
Reading from 31359: heap size 182 MB, throughput 0.996641
Reading from 31359: heap size 186 MB, throughput 0.996333
Reading from 31359: heap size 177 MB, throughput 0.997302
Reading from 31359: heap size 171 MB, throughput 0.993325
Reading from 31359: heap size 174 MB, throughput 0.995312
Reading from 31359: heap size 179 MB, throughput 0.997514
Reading from 31358: heap size 990 MB, throughput 0.0755547
Reading from 31359: heap size 170 MB, throughput 0.996764
Reading from 31358: heap size 1104 MB, throughput 0.540479
Reading from 31358: heap size 1108 MB, throughput 0.753099
Reading from 31359: heap size 163 MB, throughput 0.967668
Reading from 31359: heap size 157 MB, throughput 0.995695
Reading from 31358: heap size 1112 MB, throughput 0.679573
Reading from 31359: heap size 162 MB, throughput 0.994341
Reading from 31358: heap size 1116 MB, throughput 0.712669
Reading from 31359: heap size 169 MB, throughput 0.990664
Reading from 31358: heap size 1121 MB, throughput 0.671813
Reading from 31359: heap size 177 MB, throughput 0.996651
Reading from 31359: heap size 185 MB, throughput 0.997704
Reading from 31359: heap size 193 MB, throughput 0.997099
Reading from 31358: heap size 1125 MB, throughput 0.907163
Reading from 31359: heap size 200 MB, throughput 0.996031
Reading from 31359: heap size 208 MB, throughput 0.996821
Reading from 31359: heap size 215 MB, throughput 0.994876
Reading from 31359: heap size 224 MB, throughput 0.996984
Reading from 31359: heap size 232 MB, throughput 0.996766
Reading from 31359: heap size 241 MB, throughput 0.996604
Reading from 31358: heap size 1137 MB, throughput 0.958333
Reading from 31359: heap size 249 MB, throughput 0.997377
Reading from 31359: heap size 258 MB, throughput 0.997622
Reading from 31359: heap size 263 MB, throughput 0.997885
Reading from 31359: heap size 263 MB, throughput 0.997262
Equal recommendation: 3657 MB each
Reading from 31358: heap size 1142 MB, throughput 0.948123
Reading from 31359: heap size 270 MB, throughput 0.998095
Reading from 31359: heap size 270 MB, throughput 0.998404
Reading from 31359: heap size 277 MB, throughput 0.997812
Reading from 31359: heap size 277 MB, throughput 0.997769
Reading from 31358: heap size 1155 MB, throughput 0.923721
Reading from 31359: heap size 284 MB, throughput 0.997452
Reading from 31359: heap size 284 MB, throughput 0.997723
Reading from 31359: heap size 291 MB, throughput 0.997889
Reading from 31359: heap size 291 MB, throughput 0.997794
Reading from 31359: heap size 298 MB, throughput 0.997806
Reading from 31358: heap size 1157 MB, throughput 0.939336
Reading from 31359: heap size 298 MB, throughput 0.997787
Reading from 31359: heap size 305 MB, throughput 0.997169
Reading from 31359: heap size 305 MB, throughput 0.99705
Reading from 31359: heap size 313 MB, throughput 0.997547
Reading from 31358: heap size 1158 MB, throughput 0.931954
Reading from 31359: heap size 313 MB, throughput 0.997478
Reading from 31359: heap size 322 MB, throughput 0.997432
Reading from 31359: heap size 322 MB, throughput 0.997589
Reading from 31359: heap size 330 MB, throughput 0.997582
Reading from 31358: heap size 1163 MB, throughput 0.934133
Reading from 31359: heap size 330 MB, throughput 0.997092
Reading from 31359: heap size 339 MB, throughput 0.997687
Reading from 31359: heap size 339 MB, throughput 0.998274
Reading from 31359: heap size 347 MB, throughput 0.997467
Reading from 31358: heap size 1162 MB, throughput 0.922454
Equal recommendation: 3657 MB each
Reading from 31359: heap size 348 MB, throughput 0.998068
Reading from 31359: heap size 356 MB, throughput 0.99809
Reading from 31359: heap size 356 MB, throughput 0.997762
Reading from 31359: heap size 365 MB, throughput 0.997513
Reading from 31358: heap size 1166 MB, throughput 0.922561
Reading from 31359: heap size 365 MB, throughput 0.997894
Reading from 31359: heap size 374 MB, throughput 0.998364
Reading from 31359: heap size 374 MB, throughput 0.998119
Reading from 31359: heap size 382 MB, throughput 0.996923
Reading from 31358: heap size 1174 MB, throughput 0.923942
Reading from 31359: heap size 382 MB, throughput 0.99759
Reading from 31359: heap size 392 MB, throughput 0.998136
Reading from 31359: heap size 392 MB, throughput 0.997344
Reading from 31358: heap size 1175 MB, throughput 0.932192
Reading from 31359: heap size 402 MB, throughput 0.997896
Reading from 31359: heap size 402 MB, throughput 0.997151
Reading from 31359: heap size 412 MB, throughput 0.996808
Reading from 31359: heap size 412 MB, throughput 0.997185
Reading from 31358: heap size 1184 MB, throughput 0.94718
Reading from 31359: heap size 424 MB, throughput 0.998185
Reading from 31359: heap size 424 MB, throughput 0.998059
Equal recommendation: 3657 MB each
Reading from 31359: heap size 434 MB, throughput 0.997124
Reading from 31358: heap size 1188 MB, throughput 0.928302
Reading from 31359: heap size 435 MB, throughput 0.997187
Reading from 31359: heap size 447 MB, throughput 0.997386
Reading from 31359: heap size 447 MB, throughput 0.997861
Reading from 31358: heap size 1196 MB, throughput 0.928606
Reading from 31359: heap size 458 MB, throughput 0.998084
Reading from 31359: heap size 459 MB, throughput 0.996911
Reading from 31359: heap size 471 MB, throughput 0.997435
Reading from 31358: heap size 1201 MB, throughput 0.932601
Reading from 31359: heap size 471 MB, throughput 0.997861
Reading from 31359: heap size 484 MB, throughput 0.997585
Reading from 31359: heap size 484 MB, throughput 0.997055
Reading from 31358: heap size 1210 MB, throughput 0.943723
Reading from 31359: heap size 497 MB, throughput 0.997606
Reading from 31359: heap size 497 MB, throughput 0.997511
Reading from 31359: heap size 511 MB, throughput 0.998227
Reading from 31358: heap size 1216 MB, throughput 0.939464
Reading from 31359: heap size 511 MB, throughput 0.998105
Equal recommendation: 3657 MB each
Reading from 31359: heap size 523 MB, throughput 0.997194
Reading from 31359: heap size 524 MB, throughput 0.997248
Reading from 31358: heap size 1226 MB, throughput 0.932891
Reading from 31359: heap size 537 MB, throughput 0.998038
Reading from 31359: heap size 537 MB, throughput 0.997578
Reading from 31359: heap size 551 MB, throughput 0.997869
Reading from 31358: heap size 1231 MB, throughput 0.945988
Reading from 31359: heap size 551 MB, throughput 0.998422
Reading from 31359: heap size 563 MB, throughput 0.998204
Reading from 31358: heap size 1240 MB, throughput 0.927716
Reading from 31359: heap size 564 MB, throughput 0.998169
Reading from 31359: heap size 577 MB, throughput 0.998323
Reading from 31359: heap size 577 MB, throughput 0.998059
Reading from 31358: heap size 1243 MB, throughput 0.937996
Reading from 31359: heap size 589 MB, throughput 0.998697
Equal recommendation: 3657 MB each
Reading from 31359: heap size 589 MB, throughput 0.997723
Reading from 31358: heap size 1252 MB, throughput 0.928275
Reading from 31359: heap size 601 MB, throughput 0.998222
Reading from 31359: heap size 601 MB, throughput 0.99831
Reading from 31359: heap size 613 MB, throughput 0.997848
Reading from 31358: heap size 1254 MB, throughput 0.94018
Reading from 31359: heap size 614 MB, throughput 0.998411
Reading from 31359: heap size 626 MB, throughput 0.998426
Reading from 31358: heap size 1263 MB, throughput 0.938605
Reading from 31359: heap size 626 MB, throughput 0.998576
Reading from 31359: heap size 638 MB, throughput 0.998278
Reading from 31359: heap size 638 MB, throughput 0.99842
Reading from 31359: heap size 651 MB, throughput 0.998494
Equal recommendation: 3657 MB each
Reading from 31359: heap size 651 MB, throughput 0.998721
Reading from 31358: heap size 1265 MB, throughput 0.497426
Reading from 31359: heap size 662 MB, throughput 0.998558
Reading from 31359: heap size 663 MB, throughput 0.998449
Reading from 31358: heap size 1370 MB, throughput 0.922374
Reading from 31359: heap size 675 MB, throughput 0.998053
Reading from 31359: heap size 675 MB, throughput 0.998276
Reading from 31359: heap size 688 MB, throughput 0.998614
Reading from 31358: heap size 1371 MB, throughput 0.970286
Reading from 31359: heap size 688 MB, throughput 0.998371
Reading from 31359: heap size 701 MB, throughput 0.998413
Reading from 31358: heap size 1386 MB, throughput 0.968779
Reading from 31359: heap size 701 MB, throughput 0.998556
Equal recommendation: 3657 MB each
Reading from 31359: heap size 714 MB, throughput 0.998126
Reading from 31358: heap size 1390 MB, throughput 0.958466
Reading from 31359: heap size 714 MB, throughput 0.99844
Reading from 31359: heap size 727 MB, throughput 0.998609
Reading from 31359: heap size 727 MB, throughput 0.998446
Reading from 31358: heap size 1393 MB, throughput 0.960159
Reading from 31359: heap size 741 MB, throughput 0.998826
Reading from 31359: heap size 741 MB, throughput 0.998439
Reading from 31358: heap size 1396 MB, throughput 0.949954
Reading from 31359: heap size 754 MB, throughput 0.998346
Reading from 31359: heap size 754 MB, throughput 0.998805
Reading from 31358: heap size 1388 MB, throughput 0.965522
Reading from 31359: heap size 766 MB, throughput 0.998896
Equal recommendation: 3657 MB each
Reading from 31359: heap size 767 MB, throughput 0.99856
Reading from 31359: heap size 779 MB, throughput 0.998837
Reading from 31358: heap size 1394 MB, throughput 0.961525
Reading from 31359: heap size 779 MB, throughput 0.998473
Reading from 31359: heap size 791 MB, throughput 0.998731
Reading from 31358: heap size 1383 MB, throughput 0.953802
Reading from 31359: heap size 791 MB, throughput 0.998598
Reading from 31359: heap size 804 MB, throughput 0.998489
Reading from 31358: heap size 1389 MB, throughput 0.949264
Reading from 31359: heap size 804 MB, throughput 0.998682
Reading from 31359: heap size 817 MB, throughput 0.998868
Equal recommendation: 3657 MB each
Reading from 31359: heap size 817 MB, throughput 0.998823
Reading from 31358: heap size 1395 MB, throughput 0.949792
Reading from 31359: heap size 830 MB, throughput 0.998673
Reading from 31359: heap size 830 MB, throughput 0.998652
Reading from 31358: heap size 1395 MB, throughput 0.951297
Reading from 31359: heap size 843 MB, throughput 0.998947
Reading from 31359: heap size 843 MB, throughput 0.998656
Reading from 31358: heap size 1402 MB, throughput 0.948985
Reading from 31359: heap size 855 MB, throughput 0.998958
Reading from 31359: heap size 855 MB, throughput 0.998933
Equal recommendation: 3657 MB each
Reading from 31359: heap size 867 MB, throughput 0.998357
Reading from 31358: heap size 1408 MB, throughput 0.942172
Reading from 31359: heap size 868 MB, throughput 0.998832
Reading from 31359: heap size 881 MB, throughput 0.998609
Reading from 31359: heap size 881 MB, throughput 0.998926
Reading from 31359: heap size 894 MB, throughput 0.998793
Reading from 31359: heap size 894 MB, throughput 0.998918
Reading from 31359: heap size 907 MB, throughput 0.999048
Equal recommendation: 3657 MB each
Reading from 31359: heap size 907 MB, throughput 0.998825
Reading from 31359: heap size 920 MB, throughput 0.99924
Reading from 31359: heap size 920 MB, throughput 0.998637
Reading from 31359: heap size 932 MB, throughput 0.998576
Reading from 31359: heap size 932 MB, throughput 0.99858
Reading from 31359: heap size 946 MB, throughput 0.99913
Reading from 31359: heap size 946 MB, throughput 0.998882
Equal recommendation: 3657 MB each
Reading from 31359: heap size 959 MB, throughput 0.999015
Reading from 31359: heap size 959 MB, throughput 0.998765
Reading from 31358: heap size 1417 MB, throughput 0.992875
Reading from 31359: heap size 971 MB, throughput 0.999042
Reading from 31359: heap size 971 MB, throughput 0.998632
Reading from 31359: heap size 984 MB, throughput 0.99911
Reading from 31359: heap size 984 MB, throughput 0.998989
Reading from 31359: heap size 997 MB, throughput 0.999001
Equal recommendation: 3657 MB each
Reading from 31359: heap size 997 MB, throughput 0.998622
Reading from 31358: heap size 1426 MB, throughput 0.97642
Reading from 31358: heap size 1414 MB, throughput 0.776662
Reading from 31359: heap size 1010 MB, throughput 0.99877
Reading from 31358: heap size 1456 MB, throughput 0.236093
Reading from 31358: heap size 1528 MB, throughput 0.991676
Reading from 31358: heap size 1565 MB, throughput 0.990527
Reading from 31358: heap size 1590 MB, throughput 0.9923
Reading from 31359: heap size 1010 MB, throughput 0.999131
Reading from 31358: heap size 1594 MB, throughput 0.981903
Reading from 31358: heap size 1599 MB, throughput 0.984286
Reading from 31359: heap size 1023 MB, throughput 0.998299
Reading from 31359: heap size 1024 MB, throughput 0.99912
Reading from 31358: heap size 1604 MB, throughput 0.985037
Reading from 31359: heap size 1038 MB, throughput 0.999
Equal recommendation: 3657 MB each
Reading from 31358: heap size 1588 MB, throughput 0.977203
Reading from 31359: heap size 1039 MB, throughput 0.998868
Reading from 31359: heap size 1052 MB, throughput 0.998897
Reading from 31358: heap size 1341 MB, throughput 0.974882
Reading from 31359: heap size 1052 MB, throughput 0.998903
Reading from 31359: heap size 1066 MB, throughput 0.999085
Reading from 31358: heap size 1567 MB, throughput 0.979364
Reading from 31359: heap size 1067 MB, throughput 0.998923
Reading from 31358: heap size 1370 MB, throughput 0.9677
Reading from 31359: heap size 1080 MB, throughput 0.998868
Equal recommendation: 3657 MB each
Reading from 31359: heap size 1080 MB, throughput 0.998855
Reading from 31358: heap size 1542 MB, throughput 0.966325
Reading from 31359: heap size 1094 MB, throughput 0.999027
Reading from 31359: heap size 1094 MB, throughput 0.99881
Reading from 31358: heap size 1398 MB, throughput 0.960207
Reading from 31359: heap size 1108 MB, throughput 0.999327
Reading from 31359: heap size 1108 MB, throughput 0.999098
Reading from 31358: heap size 1531 MB, throughput 0.958629
Reading from 31359: heap size 1121 MB, throughput 0.998974
Equal recommendation: 3657 MB each
Reading from 31358: heap size 1541 MB, throughput 0.960609
Reading from 31359: heap size 1122 MB, throughput 0.998942
Reading from 31359: heap size 1135 MB, throughput 0.999252
Reading from 31358: heap size 1539 MB, throughput 0.959264
Reading from 31359: heap size 1135 MB, throughput 0.999038
Reading from 31359: heap size 1148 MB, throughput 0.999056
Reading from 31358: heap size 1542 MB, throughput 0.94936
Reading from 31359: heap size 1148 MB, throughput 0.999035
Equal recommendation: 3657 MB each
Reading from 31359: heap size 1161 MB, throughput 0.998907
Reading from 31358: heap size 1550 MB, throughput 0.951395
Reading from 31359: heap size 1161 MB, throughput 0.999285
Reading from 31358: heap size 1553 MB, throughput 0.950972
Reading from 31359: heap size 1174 MB, throughput 0.998944
Reading from 31359: heap size 1175 MB, throughput 0.99928
Reading from 31358: heap size 1563 MB, throughput 0.949656
Reading from 31359: heap size 1187 MB, throughput 0.999222
Reading from 31359: heap size 1187 MB, throughput 0.998989
Equal recommendation: 3657 MB each
Reading from 31358: heap size 1571 MB, throughput 0.941479
Reading from 31359: heap size 1200 MB, throughput 0.99926
Reading from 31359: heap size 1200 MB, throughput 0.99879
Reading from 31358: heap size 1584 MB, throughput 0.942444
Reading from 31359: heap size 1212 MB, throughput 0.999232
Reading from 31358: heap size 1594 MB, throughput 0.940618
Reading from 31359: heap size 1212 MB, throughput 0.999098
Reading from 31359: heap size 1226 MB, throughput 0.999014
Equal recommendation: 3657 MB each
Reading from 31358: heap size 1610 MB, throughput 0.945222
Reading from 31359: heap size 1226 MB, throughput 0.999271
Reading from 31359: heap size 1239 MB, throughput 0.999064
Reading from 31358: heap size 1616 MB, throughput 0.939215
Reading from 31359: heap size 1240 MB, throughput 0.999188
Reading from 31358: heap size 1632 MB, throughput 0.954409
Reading from 31359: heap size 1252 MB, throughput 0.999297
Reading from 31359: heap size 1252 MB, throughput 0.999134
Equal recommendation: 3657 MB each
Reading from 31358: heap size 1635 MB, throughput 0.947805
Reading from 31359: heap size 1265 MB, throughput 0.999307
Reading from 31359: heap size 1265 MB, throughput 0.999116
Reading from 31358: heap size 1652 MB, throughput 0.939685
Reading from 31359: heap size 1278 MB, throughput 0.999384
Reading from 31359: heap size 1278 MB, throughput 0.99924
Reading from 31358: heap size 1653 MB, throughput 0.943104
Reading from 31359: heap size 1290 MB, throughput 0.999017
Equal recommendation: 3657 MB each
Reading from 31359: heap size 1290 MB, throughput 0.999198
Reading from 31358: heap size 1670 MB, throughput 0.945909
Reading from 31359: heap size 1303 MB, throughput 0.999237
Reading from 31359: heap size 1303 MB, throughput 0.999231
Reading from 31358: heap size 1671 MB, throughput 0.941572
Reading from 31359: heap size 1316 MB, throughput 0.999298
Reading from 31359: heap size 1316 MB, throughput 0.999034
Equal recommendation: 3657 MB each
Reading from 31358: heap size 1688 MB, throughput 0.683929
Reading from 31359: heap size 1328 MB, throughput 0.999339
Reading from 31359: heap size 1328 MB, throughput 0.99937
Reading from 31358: heap size 1751 MB, throughput 0.986926
Reading from 31359: heap size 1341 MB, throughput 0.999262
Reading from 31359: heap size 1341 MB, throughput 0.999272
Reading from 31358: heap size 1758 MB, throughput 0.98164
Equal recommendation: 3657 MB each
Reading from 31359: heap size 1353 MB, throughput 0.999306
Reading from 31359: heap size 1353 MB, throughput 0.999235
Reading from 31359: heap size 1365 MB, throughput 0.99943
Reading from 31358: heap size 1769 MB, throughput 0.982329
Reading from 31359: heap size 1365 MB, throughput 0.999181
Reading from 31359: heap size 1377 MB, throughput 0.999283
Equal recommendation: 3657 MB each
Reading from 31359: heap size 1377 MB, throughput 0.999307
Reading from 31359: heap size 1390 MB, throughput 0.999292
Reading from 31359: heap size 1390 MB, throughput 0.999147
Reading from 31359: heap size 1402 MB, throughput 0.999289
Equal recommendation: 3657 MB each
Reading from 31359: heap size 1402 MB, throughput 0.999336
Reading from 31359: heap size 1415 MB, throughput 0.999315
Reading from 31359: heap size 1415 MB, throughput 0.999305
Reading from 31358: heap size 1777 MB, throughput 0.987868
Reading from 31359: heap size 1427 MB, throughput 0.999372
Reading from 31359: heap size 1427 MB, throughput 0.999217
Equal recommendation: 3657 MB each
Reading from 31358: heap size 1783 MB, throughput 0.977041
Reading from 31358: heap size 1798 MB, throughput 0.788204
Reading from 31358: heap size 1802 MB, throughput 0.728637
Reading from 31358: heap size 1836 MB, throughput 0.757987
Reading from 31359: heap size 1439 MB, throughput 0.999269
Client 31358 died
Clients: 1
Reading from 31359: heap size 1439 MB, throughput 0.999017
Reading from 31359: heap size 1452 MB, throughput 0.998872
Reading from 31359: heap size 1452 MB, throughput 0.998956
Recommendation: one client; give it all the memory
Reading from 31359: heap size 1468 MB, throughput 0.998765
Reading from 31359: heap size 1468 MB, throughput 0.998962
Reading from 31359: heap size 1485 MB, throughput 0.999029
Reading from 31359: heap size 1485 MB, throughput 0.998841
Reading from 31359: heap size 1503 MB, throughput 0.999063
Recommendation: one client; give it all the memory
Reading from 31359: heap size 1429 MB, throughput 0.999296
Reading from 31359: heap size 1359 MB, throughput 0.999018
Client 31359 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
