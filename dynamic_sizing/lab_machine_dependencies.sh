#!/bin/bash
# Do a local install of all the packages needed to run experiment scripts on the lab machines

BASE_DIR="${HOME}/.usr"
HGDIR="${BASE_DIR}/hg"

function do_hg()
{
    if [ ! -e "${BASE_DIR}/hg.done" ]; then
        mkdir -p "${HGDIR}" &&
        cd "${HGDIR}" &&
        wget http://mercurial.selenic.com/release/mercurial-3.1.tar.gz &&
        tar -xf mercurial-3.1.tar.gz &&
        cd mercurial-3.1 &&
        make local &&
        touch "${BASE_DIR}/hg.done"
    fi
}

function do_haskell()
{
    if [ ! -e "${BASE_DIR}/ghc.done" ]; then
        local GHC_BUILD_DIR="${BASE_DIR}/ghc-build" &&
        mkdir -p "${GHC_BUILD_DIR}" &&
        cd "${GHC_BUILD_DIR}" &&
        wget http://www.haskell.org/ghc/dist/7.6.3/ghc-7.6.3-x86_64-unknown-linux.tar.bz2 &&
        tar -xf ghc-7.6.3-x86_64-unknown-linux.tar.bz2 &&
        cd ghc-7.6.3 &&
        ./configure "--prefix=${BASE_DIR}/local" &&
        make install &&
        touch "${BASE_DIR}/ghc.done"
    fi
}

function do_cabal()
{
    if [ ! -e "${BASE_DIR}/cabal.done" ]; then
        local CABAL_BUILD_DIR="${BASE_DIR}/cabal-build" &&
        mkdir -p "${CABAL_BUILD_DIR}" &&
        cd "${CABAL_BUILD_DIR}" &&
        wget http://www.haskell.org/cabal/release/cabal-install-1.20.0.3/cabal-install-1.20.0.3.tar.gz &&
        tar -xf cabal-install-1.20.0.3.tar.gz &&
        cd cabal-install-1.20.0.3 &&
        export PATH="${HOME}/.cabal/bin:${BASE_DIR}/local/bin:${PATH}" &&
        ./bootstrap.sh &&
        cabal update &&
        cabal install cabal cabal-install cassava cmdlib cond download missingh zip-archive &&
        touch "${BASE_DIR}/cabal.done"
    fi
}

function do_scons()
{
    if [ ! -e "${BASE_DIR}/scons.done" ]; then
        local SCONS_BUILD_DIR="${BASE_DIR}/scons-build" &&
        mkdir -p "${SCONS_BUILD_DIR}" &&
        cd "${SCONS_BUILD_DIR}" &&
        wget http://downloads.sourceforge.net/project/scons/scons/2.3.2/scons-2.3.2.tar.gz &&
        tar -xf scons-2.3.2.tar.gz &&
        cd scons-2.3.2 &&
        python setup.py install "--prefix=${BASE_DIR}/local" &&
        touch "${BASE_DIR}/scons.done"
    fi
}

function do_libc()
{
    if [ ! -e "${BASE_DIR}/libc.done" ]; then
        local LIBC_BUILD_DIR="${BASE_DIR}/libc-build" &&
        mkdir -p "${LIBC_BUILD_DIR}" &&
        cd "${LIBC_BUILD_DIR}" &&
        wget http://ftp.gnu.org/gnu/glibc/glibc-2.19.tar.xz &&
        tar -xf glibc-2.19.tar.xz &&
        mkdir glibc-build &&
        cd glibc-build &&
        ../glibc-2.19/configure "--prefix=${BASE_DIR}/local" &&
        make -j "$(nproc)" &&
        env LANGUAGE=C LC_ALL=C make install &&
        touch "${BASE_DIR}/libc.done"
    fi
}

function do_gcc()
{
    if [ ! -e "${BASE_DIR}/gcc.done" ]; then
        local GCC_BUILD_DIR="${BASE_DIR}/gcc-build" &&
        mkdir -p "${GCC_BUILD_DIR}" &&
        cd "${GCC_BUILD_DIR}" &&
        wget ftp://ftp.gnu.org/gnu/gcc/gcc-4.8.2/gcc-4.8.2.tar.bz2 &&
        tar -xf gcc-4.8.2.tar.bz2 &&
        wget http://ftp.gnu.org/gnu/binutils/binutils-2.24.tar.bz2 &&
        tar -xf binutils-2.24.tar.bz2 &&
        cd gcc-4.8.2 &&
        ./contrib/download_prerequisites &&
        for file in ../binutils-2.24/*; do ln -s "${file}"; done
        cd .. &&
        mkdir gcc_build &&
        cd gcc_build &&
        ../gcc-4.8.2/configure "--prefix=${BASE_DIR}/local" --enable-languages=c,c++ --disable-multiarch --disable-multilib &&
        make -j "$(nproc)" &&
        make install &&
        touch "${BASE_DIR}/gcc.done"
    fi
}

mkdir -p "${BASE_DIR}" &&
do_hg &&
do_haskell &&
do_cabal &&
do_scons &&
do_libc &&
do_gcc &&

echo &&
echo "Add the following to your path:" &&
echo "${BASE_DIR}/local/bin" &&
echo "${HGDIR}/mercurial-3.1" &&
echo "${HOME}/.cabal/bin"
