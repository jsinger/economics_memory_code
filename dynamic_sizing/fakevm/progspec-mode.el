;;; package --- progspec-mode
;;; Commentary:
;;
;; Emacs syntax highlighting for fakevm program specifications.
;;
;;; Code:

(defvar progspec-mode-hook nil)
(defvar progspec-mode-map
  (let ((map (make-keymap)))
    (define-key map (kbd "RET") 'newline-and-indent)
    map)
  "Keymap for progspec mode.")

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.progspec\\'" . progspec-mode))

(defvar progspec-font-lock-keywords
  (list (cons (regexp-opt '("minheap" "phase" "transition") 'words) 'font-lock-keyword-face))
  "Default highlighting expressions for progspec mode.")

(defvar progspec-mode-syntax-table
  (let ((st (make-syntax-table)))
    (modify-syntax-entry ?# "<" st)
    (modify-syntax-entry ?\n ">" st)
    st)
  "Syntax table for progspec-mode.")

(defun progspec-indent-line ()
  "Indent current line correctly."
  (interactive)
  (back-to-indentation)
  (if (looking-at "transition")
      (indent-line-to tab-width)
    (indent-line-to 0)))

(define-derived-mode progspec-mode prog-mode "ProgSpec"
  "Major mode for editing fakevm program specification files."
  (set (make-local-variable 'font-lock-defaults) '(progspec-font-lock-keywords))
  (set (make-local-variable 'indent-line-function) 'progspec-indent-line))

(provide 'progspec-mode)

;;; progspec-mode.el ends here
