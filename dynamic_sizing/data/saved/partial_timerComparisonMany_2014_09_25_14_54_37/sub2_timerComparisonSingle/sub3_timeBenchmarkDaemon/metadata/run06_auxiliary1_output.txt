economemd
    total memory: 2974 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub2_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 3201: heap size 9 MB, throughput 0.992021
Clients: 1
Client 3201 has a minimum heap size of 276 MB
Reading from 3200: heap size 9 MB, throughput 0.992483
Clients: 2
Client 3200 has a minimum heap size of 1211 MB
Reading from 3200: heap size 9 MB, throughput 0.988915
Reading from 3201: heap size 9 MB, throughput 0.988948
Reading from 3200: heap size 9 MB, throughput 0.982307
Reading from 3201: heap size 9 MB, throughput 0.981842
Reading from 3200: heap size 9 MB, throughput 0.972214
Reading from 3201: heap size 9 MB, throughput 0.972174
Reading from 3201: heap size 11 MB, throughput 0.974758
Reading from 3200: heap size 11 MB, throughput 0.941682
Reading from 3200: heap size 11 MB, throughput 0.959191
Reading from 3201: heap size 11 MB, throughput 0.982418
Reading from 3200: heap size 17 MB, throughput 0.960453
Reading from 3201: heap size 17 MB, throughput 0.902417
Reading from 3200: heap size 17 MB, throughput 0.862943
Reading from 3201: heap size 17 MB, throughput 0.874431
Reading from 3200: heap size 30 MB, throughput 0.635078
Reading from 3201: heap size 30 MB, throughput 0.797165
Reading from 3201: heap size 31 MB, throughput 0.204521
Reading from 3200: heap size 31 MB, throughput 0.368131
Reading from 3201: heap size 35 MB, throughput 0.652305
Reading from 3200: heap size 34 MB, throughput 0.661617
Reading from 3201: heap size 46 MB, throughput 0.352161
Reading from 3200: heap size 47 MB, throughput 0.525648
Reading from 3200: heap size 51 MB, throughput 0.427771
Reading from 3201: heap size 50 MB, throughput 0.544882
Reading from 3200: heap size 52 MB, throughput 0.508201
Reading from 3201: heap size 64 MB, throughput 0.187986
Reading from 3200: heap size 77 MB, throughput 0.308353
Reading from 3201: heap size 71 MB, throughput 0.432633
Reading from 3200: heap size 77 MB, throughput 0.393073
Reading from 3201: heap size 88 MB, throughput 0.199435
Reading from 3200: heap size 99 MB, throughput 0.307947
Reading from 3200: heap size 103 MB, throughput 0.416353
Reading from 3201: heap size 97 MB, throughput 0.378512
Reading from 3201: heap size 117 MB, throughput 0.25652
Reading from 3200: heap size 104 MB, throughput 0.356703
Reading from 3201: heap size 125 MB, throughput 0.265407
Reading from 3200: heap size 131 MB, throughput 0.515833
Reading from 3200: heap size 136 MB, throughput 0.290312
Reading from 3201: heap size 126 MB, throughput 0.419922
Reading from 3200: heap size 137 MB, throughput 0.352832
Reading from 3201: heap size 171 MB, throughput 0.502938
Reading from 3200: heap size 178 MB, throughput 0.393072
Reading from 3201: heap size 171 MB, throughput 0.261407
Reading from 3200: heap size 179 MB, throughput 0.407209
Reading from 3200: heap size 181 MB, throughput 0.116852
Reading from 3201: heap size 175 MB, throughput 0.412522
Reading from 3200: heap size 186 MB, throughput 0.430394
Reading from 3201: heap size 214 MB, throughput 0.319494
Reading from 3200: heap size 235 MB, throughput 0.237992
Reading from 3200: heap size 236 MB, throughput 0.209293
Reading from 3201: heap size 221 MB, throughput 0.16597
Reading from 3200: heap size 238 MB, throughput 0.715711
Reading from 3200: heap size 243 MB, throughput 0.652255
Reading from 3200: heap size 253 MB, throughput 0.681978
Reading from 3200: heap size 257 MB, throughput 0.692012
Reading from 3201: heap size 224 MB, throughput 0.885648
Reading from 3201: heap size 231 MB, throughput 0.880703
Reading from 3200: heap size 262 MB, throughput 0.588875
Reading from 3201: heap size 236 MB, throughput 0.850612
Reading from 3201: heap size 242 MB, throughput 0.822955
Reading from 3201: heap size 247 MB, throughput 0.797669
Reading from 3201: heap size 252 MB, throughput 0.833316
Reading from 3200: heap size 271 MB, throughput 0.424325
Reading from 3201: heap size 256 MB, throughput 0.752468
Reading from 3200: heap size 328 MB, throughput 0.528798
Reading from 3201: heap size 262 MB, throughput 0.75954
Reading from 3200: heap size 334 MB, throughput 0.601343
Reading from 3201: heap size 268 MB, throughput 0.767199
Reading from 3200: heap size 337 MB, throughput 0.609408
Reading from 3201: heap size 274 MB, throughput 0.763425
Reading from 3200: heap size 345 MB, throughput 0.625098
Reading from 3200: heap size 350 MB, throughput 0.588179
Reading from 3200: heap size 361 MB, throughput 0.572246
Reading from 3201: heap size 277 MB, throughput 0.858085
Reading from 3201: heap size 281 MB, throughput 0.784924
Reading from 3201: heap size 285 MB, throughput 0.74563
Reading from 3200: heap size 369 MB, throughput 0.364967
Reading from 3200: heap size 428 MB, throughput 0.410363
Reading from 3200: heap size 434 MB, throughput 0.514869
Reading from 3201: heap size 293 MB, throughput 0.720588
Reading from 3200: heap size 437 MB, throughput 0.501522
Reading from 3201: heap size 332 MB, throughput 0.679348
Reading from 3201: heap size 331 MB, throughput 0.786948
Reading from 3201: heap size 334 MB, throughput 0.80902
Reading from 3201: heap size 331 MB, throughput 0.784759
Reading from 3200: heap size 435 MB, throughput 0.257132
Reading from 3201: heap size 335 MB, throughput 0.76498
Reading from 3200: heap size 489 MB, throughput 0.453784
Reading from 3200: heap size 478 MB, throughput 0.564074
Reading from 3200: heap size 486 MB, throughput 0.530039
Reading from 3201: heap size 334 MB, throughput 0.513568
Reading from 3200: heap size 487 MB, throughput 0.555751
Reading from 3201: heap size 377 MB, throughput 0.649471
Numeric result:
Recommendation: 2 clients, utility 0.516006:
    h1: 1763 MB (U(h) = 0.411775*h^0.0612909)
    h2: 1211 MB (U(h) = 0.786948*h^0.001)
Recommendation: 2 clients, utility 0.516006:
    h1: 1763 MB (U(h) = 0.411775*h^0.0612909)
    h2: 1211 MB (U(h) = 0.786948*h^0.001)
Reading from 3201: heap size 381 MB, throughput 0.648154
Reading from 3200: heap size 489 MB, throughput 0.529731
Reading from 3200: heap size 495 MB, throughput 0.496612
Reading from 3201: heap size 382 MB, throughput 0.934045
Reading from 3200: heap size 498 MB, throughput 0.314816
Reading from 3200: heap size 562 MB, throughput 0.460701
Reading from 3200: heap size 565 MB, throughput 0.498382
Reading from 3200: heap size 568 MB, throughput 0.601007
Reading from 3200: heap size 572 MB, throughput 0.544748
Reading from 3201: heap size 387 MB, throughput 0.958878
Reading from 3200: heap size 579 MB, throughput 0.308608
Reading from 3200: heap size 648 MB, throughput 0.503699
Reading from 3200: heap size 660 MB, throughput 0.522223
Reading from 3201: heap size 389 MB, throughput 0.955306
Reading from 3200: heap size 661 MB, throughput 0.505835
Reading from 3200: heap size 669 MB, throughput 0.812265
Reading from 3201: heap size 388 MB, throughput 0.96527
Reading from 3200: heap size 674 MB, throughput 0.837472
Reading from 3200: heap size 672 MB, throughput 0.849784
Reading from 3200: heap size 691 MB, throughput 0.777065
Reading from 3201: heap size 391 MB, throughput 0.966553
Reading from 3200: heap size 700 MB, throughput 0.7009
Reading from 3201: heap size 388 MB, throughput 0.971756
Reading from 3200: heap size 715 MB, throughput 0.542359
Reading from 3200: heap size 793 MB, throughput 0.428501
Reading from 3200: heap size 799 MB, throughput 0.397467
Numeric result:
Recommendation: 2 clients, utility 0.457718:
    h1: 1763 MB (U(h) = 0.335539*h^0.113219)
    h2: 1211 MB (U(h) = 0.58108*h^0.001)
Recommendation: 2 clients, utility 0.457718:
    h1: 1763 MB (U(h) = 0.335539*h^0.113219)
    h2: 1211 MB (U(h) = 0.58108*h^0.001)
Reading from 3200: heap size 782 MB, throughput 0.107003
Reading from 3201: heap size 392 MB, throughput 0.970451
Reading from 3200: heap size 866 MB, throughput 0.675534
Reading from 3200: heap size 858 MB, throughput 0.642033
Reading from 3200: heap size 862 MB, throughput 0.610772
Reading from 3200: heap size 858 MB, throughput 0.585237
Reading from 3200: heap size 861 MB, throughput 0.75179
Reading from 3201: heap size 393 MB, throughput 0.956773
Reading from 3200: heap size 863 MB, throughput 0.413937
Reading from 3200: heap size 947 MB, throughput 0.677493
Reading from 3200: heap size 951 MB, throughput 0.778562
Reading from 3201: heap size 394 MB, throughput 0.96291
Reading from 3200: heap size 953 MB, throughput 0.820424
Reading from 3200: heap size 961 MB, throughput 0.880066
Reading from 3200: heap size 775 MB, throughput 0.860101
Reading from 3200: heap size 945 MB, throughput 0.822469
Reading from 3200: heap size 792 MB, throughput 0.821923
Reading from 3200: heap size 934 MB, throughput 0.813338
Reading from 3200: heap size 798 MB, throughput 0.812802
Reading from 3201: heap size 398 MB, throughput 0.963427
Reading from 3200: heap size 924 MB, throughput 0.81501
Reading from 3200: heap size 799 MB, throughput 0.823666
Reading from 3200: heap size 918 MB, throughput 0.829215
Reading from 3200: heap size 925 MB, throughput 0.846891
Reading from 3200: heap size 911 MB, throughput 0.847827
Reading from 3201: heap size 399 MB, throughput 0.968032
Reading from 3200: heap size 918 MB, throughput 0.962708
Reading from 3201: heap size 396 MB, throughput 0.970321
Reading from 3200: heap size 908 MB, throughput 0.955156
Reading from 3200: heap size 913 MB, throughput 0.917741
Reading from 3200: heap size 908 MB, throughput 0.885618
Reading from 3200: heap size 912 MB, throughput 0.855597
Reading from 3200: heap size 906 MB, throughput 0.813908
Reading from 3200: heap size 910 MB, throughput 0.813563
Reading from 3201: heap size 398 MB, throughput 0.970759
Reading from 3200: heap size 907 MB, throughput 0.808965
Reading from 3200: heap size 911 MB, throughput 0.811297
Reading from 3200: heap size 909 MB, throughput 0.832297
Reading from 3200: heap size 913 MB, throughput 0.84652
Numeric result:
Recommendation: 2 clients, utility 0.541835:
    h1: 1763 MB (U(h) = 0.287633*h^0.152058)
    h2: 1211 MB (U(h) = 0.361246*h^0.0725252)
Recommendation: 2 clients, utility 0.541835:
    h1: 1763 MB (U(h) = 0.287633*h^0.152058)
    h2: 1211 MB (U(h) = 0.361246*h^0.0725252)
Reading from 3200: heap size 914 MB, throughput 0.86542
Reading from 3201: heap size 400 MB, throughput 0.978328
Reading from 3201: heap size 400 MB, throughput 0.956933
Reading from 3201: heap size 397 MB, throughput 0.907099
Reading from 3200: heap size 917 MB, throughput 0.709471
Reading from 3201: heap size 401 MB, throughput 0.875387
Reading from 3201: heap size 412 MB, throughput 0.809319
Reading from 3200: heap size 990 MB, throughput 0.683975
Reading from 3200: heap size 1010 MB, throughput 0.675845
Reading from 3200: heap size 1020 MB, throughput 0.64848
Reading from 3200: heap size 1024 MB, throughput 0.664276
Reading from 3200: heap size 1024 MB, throughput 0.679676
Reading from 3200: heap size 1028 MB, throughput 0.684268
Reading from 3200: heap size 1030 MB, throughput 0.693578
Reading from 3200: heap size 1035 MB, throughput 0.686038
Reading from 3201: heap size 413 MB, throughput 0.955327
Reading from 3200: heap size 1045 MB, throughput 0.691203
Reading from 3200: heap size 1047 MB, throughput 0.689164
Reading from 3201: heap size 421 MB, throughput 0.975782
Reading from 3200: heap size 1062 MB, throughput 0.925085
Reading from 3201: heap size 423 MB, throughput 0.981314
Reading from 3200: heap size 1065 MB, throughput 0.934889
Reading from 3201: heap size 426 MB, throughput 0.982296
Reading from 3200: heap size 1080 MB, throughput 0.94168
Numeric result:
Recommendation: 2 clients, utility 0.649366:
    h1: 1734.2 MB (U(h) = 0.256241*h^0.180779)
    h2: 1239.8 MB (U(h) = 0.26213*h^0.129233)
Recommendation: 2 clients, utility 0.649366:
    h1: 1734.25 MB (U(h) = 0.256241*h^0.180779)
    h2: 1239.75 MB (U(h) = 0.26213*h^0.129233)
Reading from 3201: heap size 429 MB, throughput 0.982966
Reading from 3200: heap size 1084 MB, throughput 0.944449
Reading from 3201: heap size 429 MB, throughput 0.979958
Reading from 3200: heap size 1096 MB, throughput 0.829014
Reading from 3201: heap size 431 MB, throughput 0.952974
Reading from 3200: heap size 1202 MB, throughput 0.970391
Reading from 3200: heap size 1205 MB, throughput 0.977868
Reading from 3201: heap size 453 MB, throughput 0.988409
Reading from 3200: heap size 1214 MB, throughput 0.981045
Numeric result:
Recommendation: 2 clients, utility 0.801225:
    h1: 1157.92 MB (U(h) = 0.246152*h^0.190544)
    h2: 1816.08 MB (U(h) = 0.0901129*h^0.298852)
Recommendation: 2 clients, utility 0.801225:
    h1: 1157.92 MB (U(h) = 0.246152*h^0.190544)
    h2: 1816.08 MB (U(h) = 0.0901129*h^0.298852)
Reading from 3201: heap size 456 MB, throughput 0.99082
Reading from 3200: heap size 1227 MB, throughput 0.981078
Reading from 3201: heap size 462 MB, throughput 0.992452
Reading from 3200: heap size 1229 MB, throughput 0.978444
Reading from 3201: heap size 464 MB, throughput 0.986203
Reading from 3201: heap size 465 MB, throughput 0.973669
Reading from 3201: heap size 467 MB, throughput 0.956828
Reading from 3201: heap size 473 MB, throughput 0.968429
Reading from 3200: heap size 1226 MB, throughput 0.976325
Reading from 3201: heap size 474 MB, throughput 0.98581
Reading from 3200: heap size 1230 MB, throughput 0.975627
Reading from 3201: heap size 475 MB, throughput 0.987825
Reading from 3200: heap size 1218 MB, throughput 0.97568
Numeric result:
Recommendation: 2 clients, utility 0.965221:
    h1: 1006.26 MB (U(h) = 0.225907*h^0.210958)
    h2: 1967.74 MB (U(h) = 0.0434923*h^0.412523)
Recommendation: 2 clients, utility 0.965221:
    h1: 1006.27 MB (U(h) = 0.225907*h^0.210958)
    h2: 1967.73 MB (U(h) = 0.0434923*h^0.412523)
Reading from 3201: heap size 478 MB, throughput 0.988028
Reading from 3200: heap size 1151 MB, throughput 0.974357
Reading from 3201: heap size 474 MB, throughput 0.981473
Reading from 3200: heap size 1215 MB, throughput 0.969374
Reading from 3201: heap size 478 MB, throughput 0.98346
Reading from 3200: heap size 1219 MB, throughput 0.964805
Reading from 3200: heap size 1222 MB, throughput 0.965187
Reading from 3201: heap size 477 MB, throughput 0.985655
Numeric result:
Recommendation: 2 clients, utility 1.04503:
    h1: 926.244 MB (U(h) = 0.222044*h^0.215027)
    h2: 2047.76 MB (U(h) = 0.0288824*h^0.475384)
Recommendation: 2 clients, utility 1.04503:
    h1: 926.246 MB (U(h) = 0.222044*h^0.215027)
    h2: 2047.75 MB (U(h) = 0.0288824*h^0.475384)
Reading from 3200: heap size 1223 MB, throughput 0.965879
Reading from 3201: heap size 479 MB, throughput 0.985633
Reading from 3200: heap size 1227 MB, throughput 0.958471
Reading from 3201: heap size 482 MB, throughput 0.988757
Reading from 3201: heap size 483 MB, throughput 0.984662
Reading from 3201: heap size 481 MB, throughput 0.974839
Reading from 3201: heap size 484 MB, throughput 0.959149
Reading from 3200: heap size 1233 MB, throughput 0.953842
Reading from 3201: heap size 492 MB, throughput 0.984117
Reading from 3200: heap size 1238 MB, throughput 0.952794
Reading from 3201: heap size 493 MB, throughput 0.984666
Reading from 3200: heap size 1246 MB, throughput 0.949621
Numeric result:
Recommendation: 2 clients, utility 1.06473:
    h1: 987.906 MB (U(h) = 0.211558*h^0.226374)
    h2: 1986.09 MB (U(h) = 0.033334*h^0.455124)
Recommendation: 2 clients, utility 1.06473:
    h1: 987.878 MB (U(h) = 0.211558*h^0.226374)
    h2: 1986.12 MB (U(h) = 0.033334*h^0.455124)
Reading from 3200: heap size 1254 MB, throughput 0.951757
Reading from 3201: heap size 496 MB, throughput 0.987535
Reading from 3200: heap size 1259 MB, throughput 0.950536
Reading from 3201: heap size 498 MB, throughput 0.987998
Reading from 3200: heap size 1268 MB, throughput 0.951773
Reading from 3201: heap size 496 MB, throughput 0.988149
Reading from 3200: heap size 1271 MB, throughput 0.953524
Reading from 3201: heap size 499 MB, throughput 0.987373
Numeric result:
Recommendation: 2 clients, utility 1.07271:
    h1: 1017.89 MB (U(h) = 0.20782*h^0.230513)
    h2: 1956.11 MB (U(h) = 0.0364193*h^0.443019)
Recommendation: 2 clients, utility 1.07271:
    h1: 1017.84 MB (U(h) = 0.20782*h^0.230513)
    h2: 1956.16 MB (U(h) = 0.0364193*h^0.443019)
Reading from 3200: heap size 1280 MB, throughput 0.948241
Reading from 3201: heap size 499 MB, throughput 0.984062
Reading from 3200: heap size 1281 MB, throughput 0.952313
Reading from 3201: heap size 500 MB, throughput 0.988816
Reading from 3201: heap size 501 MB, throughput 0.983249
Reading from 3201: heap size 502 MB, throughput 0.971886
Reading from 3201: heap size 508 MB, throughput 0.982155
Reading from 3200: heap size 1290 MB, throughput 0.954397
Reading from 3201: heap size 510 MB, throughput 0.984483
Reading from 3200: heap size 1291 MB, throughput 0.953981
Numeric result:
Recommendation: 2 clients, utility 1.21838:
    h1: 859.332 MB (U(h) = 0.202846*h^0.236108)
    h2: 2114.67 MB (U(h) = 0.0142463*h^0.581051)
Recommendation: 2 clients, utility 1.21838:
    h1: 859.3 MB (U(h) = 0.202846*h^0.236108)
    h2: 2114.7 MB (U(h) = 0.0142463*h^0.581051)
Reading from 3200: heap size 1300 MB, throughput 0.95473
Reading from 3201: heap size 513 MB, throughput 0.988745
Reading from 3200: heap size 1300 MB, throughput 0.959654
Reading from 3201: heap size 515 MB, throughput 0.989896
Reading from 3200: heap size 1308 MB, throughput 0.960338
Reading from 3201: heap size 513 MB, throughput 0.989323
Reading from 3201: heap size 516 MB, throughput 0.983312
Numeric result:
Recommendation: 2 clients, utility 1.25804:
    h1: 836.499 MB (U(h) = 0.200196*h^0.239119)
    h2: 2137.5 MB (U(h) = 0.0116081*h^0.611026)
Recommendation: 2 clients, utility 1.25804:
    h1: 836.493 MB (U(h) = 0.200196*h^0.239119)
    h2: 2137.51 MB (U(h) = 0.0116081*h^0.611026)
Reading from 3200: heap size 1308 MB, throughput 0.931924
Reading from 3201: heap size 518 MB, throughput 0.985675
Reading from 3200: heap size 1422 MB, throughput 0.950648
Reading from 3201: heap size 518 MB, throughput 0.988891
Reading from 3201: heap size 521 MB, throughput 0.98324
Reading from 3201: heap size 522 MB, throughput 0.976795
Reading from 3200: heap size 1423 MB, throughput 0.964709
Reading from 3201: heap size 531 MB, throughput 0.98983
Reading from 3200: heap size 1438 MB, throughput 0.971313
Numeric result:
Recommendation: 2 clients, utility 1.35855:
    h1: 764.311 MB (U(h) = 0.197233*h^0.242512)
    h2: 2209.69 MB (U(h) = 0.00622529*h^0.7011)
Recommendation: 2 clients, utility 1.35855:
    h1: 764.329 MB (U(h) = 0.197233*h^0.242512)
    h2: 2209.67 MB (U(h) = 0.00622529*h^0.7011)
Reading from 3201: heap size 530 MB, throughput 0.991051
Reading from 3200: heap size 1441 MB, throughput 0.972756
Reading from 3201: heap size 532 MB, throughput 0.990125
Reading from 3200: heap size 1444 MB, throughput 0.973142
Reading from 3201: heap size 534 MB, throughput 0.99032
Reading from 3200: heap size 1447 MB, throughput 0.970971
Reading from 3201: heap size 533 MB, throughput 0.989092
Numeric result:
Recommendation: 2 clients, utility 1.43186:
    h1: 724.611 MB (U(h) = 0.194538*h^0.245619)
    h2: 2249.39 MB (U(h) = 0.00406072*h^0.762455)
Recommendation: 2 clients, utility 1.43186:
    h1: 724.619 MB (U(h) = 0.194538*h^0.245619)
    h2: 2249.38 MB (U(h) = 0.00406072*h^0.762455)
Reading from 3200: heap size 1438 MB, throughput 0.970583
Reading from 3201: heap size 535 MB, throughput 0.98876
Reading from 3201: heap size 537 MB, throughput 0.989549
Reading from 3201: heap size 539 MB, throughput 0.98348
Reading from 3201: heap size 538 MB, throughput 0.980278
Reading from 3201: heap size 541 MB, throughput 0.990584
Numeric result:
Recommendation: 2 clients, utility 1.62189:
    h1: 996.429 MB (U(h) = 0.0863748*h^0.384161)
    h2: 1977.57 MB (U(h) = 0.00406115*h^0.762439)
Recommendation: 2 clients, utility 1.62189:
    h1: 996.419 MB (U(h) = 0.0863748*h^0.384161)
    h2: 1977.58 MB (U(h) = 0.00406115*h^0.762439)
Reading from 3201: heap size 544 MB, throughput 0.991465
Reading from 3201: heap size 545 MB, throughput 0.991761
Reading from 3200: heap size 1445 MB, throughput 0.986585
Reading from 3201: heap size 544 MB, throughput 0.990745
Numeric result:
Recommendation: 2 clients, utility 1.89357:
    h1: 1223.75 MB (U(h) = 0.0360356*h^0.531207)
    h2: 1750.25 MB (U(h) = 0.00413461*h^0.75974)
Recommendation: 2 clients, utility 1.89357:
    h1: 1223.76 MB (U(h) = 0.0360356*h^0.531207)
    h2: 1750.24 MB (U(h) = 0.00413461*h^0.75974)
Reading from 3201: heap size 546 MB, throughput 0.989734
Reading from 3201: heap size 548 MB, throughput 0.991869
Reading from 3201: heap size 549 MB, throughput 0.985493
Reading from 3201: heap size 549 MB, throughput 0.977751
Reading from 3201: heap size 552 MB, throughput 0.988926
Numeric result:
Recommendation: 2 clients, utility 2.32963:
    h1: 1433.86 MB (U(h) = 0.0124906*h^0.707287)
    h2: 1540.14 MB (U(h) = 0.00413461*h^0.75974)
Recommendation: 2 clients, utility 2.32963:
    h1: 1433.83 MB (U(h) = 0.0124906*h^0.707287)
    h2: 1540.17 MB (U(h) = 0.00413461*h^0.75974)
Reading from 3201: heap size 558 MB, throughput 0.991087
Reading from 3200: heap size 1422 MB, throughput 0.989218
Reading from 3201: heap size 560 MB, throughput 0.991808
Reading from 3201: heap size 559 MB, throughput 0.987651
Reading from 3200: heap size 1445 MB, throughput 0.98503
Reading from 3200: heap size 1451 MB, throughput 0.972171
Reading from 3200: heap size 1480 MB, throughput 0.948561
Reading from 3200: heap size 1513 MB, throughput 0.915544
Reading from 3200: heap size 1539 MB, throughput 0.865617
Numeric result:
Recommendation: 2 clients, utility 2.24539:
    h1: 1436.43 MB (U(h) = 0.0136168*h^0.69247)
    h2: 1537.57 MB (U(h) = 0.00466365*h^0.741227)
Recommendation: 2 clients, utility 2.24539:
    h1: 1436.43 MB (U(h) = 0.0136168*h^0.69247)
    h2: 1537.57 MB (U(h) = 0.00466365*h^0.741227)
Reading from 3200: heap size 1586 MB, throughput 0.813906
Reading from 3200: heap size 1486 MB, throughput 0.775801
Reading from 3201: heap size 562 MB, throughput 0.989111
Reading from 3200: heap size 1583 MB, throughput 0.855287
Reading from 3200: heap size 1388 MB, throughput 0.956234
Reading from 3201: heap size 565 MB, throughput 0.992368
Reading from 3201: heap size 565 MB, throughput 0.987287
Reading from 3201: heap size 565 MB, throughput 0.982742
Reading from 3200: heap size 1523 MB, throughput 0.96922
Reading from 3201: heap size 568 MB, throughput 0.990788
Numeric result:
Recommendation: 2 clients, utility 1.98316:
    h1: 1394.56 MB (U(h) = 0.0218625*h^0.614206)
    h2: 1579.44 MB (U(h) = 0.00632855*h^0.695645)
Recommendation: 2 clients, utility 1.98316:
    h1: 1394.55 MB (U(h) = 0.0218625*h^0.614206)
    h2: 1579.45 MB (U(h) = 0.00632855*h^0.695645)
Reading from 3200: heap size 1523 MB, throughput 0.978183
Reading from 3200: heap size 1532 MB, throughput 0.980885
Reading from 3201: heap size 572 MB, throughput 0.992452
Reading from 3200: heap size 1532 MB, throughput 0.9821
Reading from 3201: heap size 573 MB, throughput 0.992411
Reading from 3200: heap size 1532 MB, throughput 0.979763
Numeric result:
Recommendation: 2 clients, utility 1.87739:
    h1: 1354.93 MB (U(h) = 0.0284166*h^0.571048)
    h2: 1619.07 MB (U(h) = 0.00694342*h^0.682378)
Recommendation: 2 clients, utility 1.87739:
    h1: 1354.92 MB (U(h) = 0.0284166*h^0.571048)
    h2: 1619.08 MB (U(h) = 0.00694342*h^0.682378)
Reading from 3201: heap size 573 MB, throughput 0.989953
Reading from 3200: heap size 1249 MB, throughput 0.976568
Reading from 3201: heap size 575 MB, throughput 0.98964
Reading from 3200: heap size 1527 MB, throughput 0.974865
Reading from 3201: heap size 578 MB, throughput 0.989791
Reading from 3201: heap size 579 MB, throughput 0.983162
Reading from 3200: heap size 1279 MB, throughput 0.972873
Reading from 3201: heap size 583 MB, throughput 0.987363
Reading from 3200: heap size 1493 MB, throughput 0.975201
Numeric result:
Recommendation: 2 clients, utility 1.4636:
    h1: 1009.39 MB (U(h) = 0.128096*h^0.326072)
    h2: 1964.61 MB (U(h) = 0.00973329*h^0.634654)
Recommendation: 2 clients, utility 1.4636:
    h1: 1009.38 MB (U(h) = 0.128096*h^0.326072)
    h2: 1964.62 MB (U(h) = 0.00973329*h^0.634654)
Reading from 3201: heap size 585 MB, throughput 0.991432
Reading from 3200: heap size 1310 MB, throughput 0.973134
Reading from 3201: heap size 588 MB, throughput 0.991835
Reading from 3200: heap size 1470 MB, throughput 0.96909
Reading from 3201: heap size 590 MB, throughput 0.991095
Reading from 3200: heap size 1341 MB, throughput 0.967129
Numeric result:
Recommendation: 2 clients, utility 1.47687:
    h1: 1135.28 MB (U(h) = 0.101106*h^0.364082)
    h2: 1838.72 MB (U(h) = 0.0134054*h^0.589667)
Recommendation: 2 clients, utility 1.47687:
    h1: 1135.29 MB (U(h) = 0.101106*h^0.364082)
    h2: 1838.71 MB (U(h) = 0.0134054*h^0.589667)
Reading from 3200: heap size 1472 MB, throughput 0.964872
Reading from 3201: heap size 590 MB, throughput 0.99111
Reading from 3200: heap size 1479 MB, throughput 0.962581
Reading from 3201: heap size 591 MB, throughput 0.992556
Reading from 3201: heap size 594 MB, throughput 0.987068
Reading from 3201: heap size 595 MB, throughput 0.988222
Reading from 3200: heap size 1487 MB, throughput 0.961801
Numeric result:
Recommendation: 2 clients, utility 1.54512:
    h1: 1062.04 MB (U(h) = 0.0929372*h^0.37736)
    h2: 1911.96 MB (U(h) = 0.0070708*h^0.679377)
Recommendation: 2 clients, utility 1.54512:
    h1: 1062.01 MB (U(h) = 0.0929372*h^0.37736)
    h2: 1911.99 MB (U(h) = 0.0070708*h^0.679377)
Reading from 3201: heap size 602 MB, throughput 0.991897
Reading from 3200: heap size 1488 MB, throughput 0.958672
Reading from 3200: heap size 1503 MB, throughput 0.95769
Reading from 3201: heap size 603 MB, throughput 0.992227
Reading from 3200: heap size 1504 MB, throughput 0.963067
Reading from 3201: heap size 603 MB, throughput 0.992161
Numeric result:
Recommendation: 2 clients, utility 1.54742:
    h1: 1027.88 MB (U(h) = 0.0965335*h^0.371034)
    h2: 1946.12 MB (U(h) = 0.00598135*h^0.702486)
Recommendation: 2 clients, utility 1.54742:
    h1: 1027.88 MB (U(h) = 0.0965335*h^0.371034)
    h2: 1946.12 MB (U(h) = 0.00598135*h^0.702486)
Reading from 3200: heap size 1520 MB, throughput 0.964994
Reading from 3201: heap size 605 MB, throughput 0.991615
Reading from 3200: heap size 1521 MB, throughput 0.96142
Reading from 3201: heap size 607 MB, throughput 0.992843
Reading from 3201: heap size 608 MB, throughput 0.987254
Reading from 3201: heap size 608 MB, throughput 0.988796
Reading from 3200: heap size 1535 MB, throughput 0.958939
Numeric result:
Recommendation: 2 clients, utility 1.42999:
    h1: 1319.8 MB (U(h) = 0.0935936*h^0.375663)
    h2: 1654.2 MB (U(h) = 0.0313578*h^0.470853)
Recommendation: 2 clients, utility 1.42999:
    h1: 1319.79 MB (U(h) = 0.0935936*h^0.375663)
    h2: 1654.21 MB (U(h) = 0.0313578*h^0.470853)
Reading from 3201: heap size 611 MB, throughput 0.99132
Reading from 3200: heap size 1536 MB, throughput 0.960875
Reading from 3201: heap size 612 MB, throughput 0.992137
Reading from 3200: heap size 1552 MB, throughput 0.959596
Numeric result:
Recommendation: 2 clients, utility 1.36611:
    h1: 1304.63 MB (U(h) = 0.117911*h^0.338425)
    h2: 1669.37 MB (U(h) = 0.0411229*h^0.433038)
Recommendation: 2 clients, utility 1.36611:
    h1: 1304.63 MB (U(h) = 0.117911*h^0.338425)
    h2: 1669.37 MB (U(h) = 0.0411229*h^0.433038)
Reading from 3201: heap size 614 MB, throughput 0.992224
Reading from 3200: heap size 1553 MB, throughput 0.958939
Reading from 3201: heap size 616 MB, throughput 0.991486
Reading from 3200: heap size 1570 MB, throughput 0.958535
Reading from 3201: heap size 616 MB, throughput 0.990598
Reading from 3201: heap size 619 MB, throughput 0.984636
Reading from 3200: heap size 1571 MB, throughput 0.959193
Numeric result:
Recommendation: 2 clients, utility 1.24741:
    h1: 1234.7 MB (U(h) = 0.19536*h^0.2575)
    h2: 1739.3 MB (U(h) = 0.0681801*h^0.362748)
Recommendation: 2 clients, utility 1.24741:
    h1: 1234.68 MB (U(h) = 0.19536*h^0.2575)
    h2: 1739.32 MB (U(h) = 0.0681801*h^0.362748)
Reading from 3201: heap size 621 MB, throughput 0.991157
Reading from 3201: heap size 626 MB, throughput 0.988031
Reading from 3200: heap size 1589 MB, throughput 0.940508
Reading from 3201: heap size 628 MB, throughput 0.99081
Reading from 3200: heap size 1723 MB, throughput 0.959276
Numeric result:
Recommendation: 2 clients, utility 1.12959:
    h1: 659.314 MB (U(h) = 0.524996*h^0.100061)
    h2: 2314.69 MB (U(h) = 0.073919*h^0.351294)
Recommendation: 2 clients, utility 1.12959:
    h1: 659.308 MB (U(h) = 0.524996*h^0.100061)
    h2: 2314.69 MB (U(h) = 0.073919*h^0.351294)
Reading from 3201: heap size 628 MB, throughput 0.99135
Reading from 3200: heap size 1732 MB, throughput 0.972373
Reading from 3201: heap size 630 MB, throughput 0.993283
Reading from 3201: heap size 634 MB, throughput 0.989293
Client 3201 died
Clients: 1
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 3200: heap size 1529 MB, throughput 0.994302
Reading from 3200: heap size 1751 MB, throughput 0.992894
Reading from 3200: heap size 1636 MB, throughput 0.985838
Recommendation: one client; give it all the memory
Reading from 3200: heap size 1709 MB, throughput 0.974948
Reading from 3200: heap size 1688 MB, throughput 0.957183
Reading from 3200: heap size 1685 MB, throughput 0.930421
Client 3200 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
