#!/bin/bash
# Kill various services to get a 'clean' machine for experimenting on.
# Specific to Mint 17

sudo service mdm stop
sudo service smbd stop
sudo service nmbd stop
sudo service dictd stop
sudo service avahi-daemon stop
sudo service bluetooth stop
sudo service cups-browsed stop
sudo service cron stop
sudo service cups stop
