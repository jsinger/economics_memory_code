economemd
    total memory: 2974 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub2_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 3034: heap size 9 MB, throughput 0.992524
Clients: 1
Client 3034 has a minimum heap size of 1211 MB
Reading from 3036: heap size 9 MB, throughput 0.991389
Clients: 2
Client 3036 has a minimum heap size of 276 MB
Reading from 3036: heap size 9 MB, throughput 0.987832
Reading from 3034: heap size 9 MB, throughput 0.988384
Reading from 3036: heap size 9 MB, throughput 0.981847
Reading from 3034: heap size 9 MB, throughput 0.982152
Reading from 3036: heap size 9 MB, throughput 0.971486
Reading from 3034: heap size 9 MB, throughput 0.971429
Reading from 3036: heap size 11 MB, throughput 0.975709
Reading from 3034: heap size 11 MB, throughput 0.974939
Reading from 3036: heap size 11 MB, throughput 0.92584
Reading from 3034: heap size 11 MB, throughput 0.978211
Reading from 3036: heap size 17 MB, throughput 0.948392
Reading from 3034: heap size 17 MB, throughput 0.969168
Reading from 3036: heap size 17 MB, throughput 0.893565
Reading from 3034: heap size 17 MB, throughput 0.898501
Reading from 3036: heap size 30 MB, throughput 0.869579
Reading from 3034: heap size 30 MB, throughput 0.851084
Reading from 3036: heap size 31 MB, throughput 0.610721
Reading from 3034: heap size 31 MB, throughput 0.500587
Reading from 3034: heap size 35 MB, throughput 0.7827
Reading from 3036: heap size 35 MB, throughput 0.723016
Reading from 3036: heap size 48 MB, throughput 0.688218
Reading from 3034: heap size 44 MB, throughput 0.343568
Reading from 3036: heap size 51 MB, throughput 0.328021
Reading from 3034: heap size 50 MB, throughput 0.0405168
Reading from 3036: heap size 52 MB, throughput 0.49963
Reading from 3034: heap size 51 MB, throughput 0.519808
Reading from 3036: heap size 71 MB, throughput 0.101808
Reading from 3036: heap size 74 MB, throughput 0.186839
Reading from 3034: heap size 71 MB, throughput 0.367204
Reading from 3034: heap size 91 MB, throughput 0.639844
Reading from 3034: heap size 96 MB, throughput 0.522256
Reading from 3036: heap size 78 MB, throughput 0.431257
Reading from 3036: heap size 98 MB, throughput 0.0431554
Reading from 3034: heap size 97 MB, throughput 0.383294
Reading from 3036: heap size 105 MB, throughput 0.0872227
Reading from 3034: heap size 129 MB, throughput 0.557123
Reading from 3034: heap size 130 MB, throughput 0.550928
Reading from 3034: heap size 133 MB, throughput 0.191469
Reading from 3036: heap size 106 MB, throughput 0.468434
Reading from 3036: heap size 137 MB, throughput 0.39866
Reading from 3036: heap size 139 MB, throughput 0.0129227
Reading from 3036: heap size 142 MB, throughput 0.74098
Reading from 3034: heap size 136 MB, throughput 0.427609
Reading from 3036: heap size 147 MB, throughput 0.745649
Reading from 3034: heap size 179 MB, throughput 0.255339
Reading from 3036: heap size 151 MB, throughput 0.750616
Reading from 3034: heap size 180 MB, throughput 0.204524
Reading from 3034: heap size 184 MB, throughput 0.709223
Reading from 3034: heap size 189 MB, throughput 0.743566
Reading from 3036: heap size 155 MB, throughput 0.427369
Reading from 3036: heap size 200 MB, throughput 0.651479
Reading from 3036: heap size 203 MB, throughput 0.667832
Reading from 3034: heap size 192 MB, throughput 0.433994
Reading from 3034: heap size 240 MB, throughput 0.62406
Reading from 3034: heap size 244 MB, throughput 0.613576
Reading from 3036: heap size 206 MB, throughput 0.782032
Reading from 3034: heap size 248 MB, throughput 0.631332
Reading from 3034: heap size 253 MB, throughput 0.65221
Reading from 3034: heap size 261 MB, throughput 0.568702
Reading from 3036: heap size 213 MB, throughput 0.834164
Reading from 3036: heap size 223 MB, throughput 0.810023
Reading from 3036: heap size 228 MB, throughput 0.774046
Reading from 3036: heap size 234 MB, throughput 0.792292
Reading from 3034: heap size 273 MB, throughput 0.349189
Reading from 3036: heap size 238 MB, throughput 0.769307
Reading from 3036: heap size 242 MB, throughput 0.699477
Reading from 3034: heap size 325 MB, throughput 0.56382
Reading from 3036: heap size 245 MB, throughput 0.687364
Reading from 3034: heap size 332 MB, throughput 0.664913
Reading from 3036: heap size 250 MB, throughput 0.716149
Reading from 3034: heap size 334 MB, throughput 0.648585
Reading from 3036: heap size 252 MB, throughput 0.731677
Reading from 3034: heap size 336 MB, throughput 0.682559
Reading from 3036: heap size 254 MB, throughput 0.69973
Reading from 3034: heap size 343 MB, throughput 0.620022
Reading from 3034: heap size 347 MB, throughput 0.585061
Reading from 3034: heap size 359 MB, throughput 0.529116
Reading from 3034: heap size 367 MB, throughput 0.540605
Reading from 3036: heap size 258 MB, throughput 0.6274
Reading from 3036: heap size 304 MB, throughput 0.555642
Reading from 3036: heap size 304 MB, throughput 0.680254
Reading from 3036: heap size 300 MB, throughput 0.625337
Reading from 3034: heap size 377 MB, throughput 0.423115
Reading from 3034: heap size 438 MB, throughput 0.506765
Reading from 3034: heap size 442 MB, throughput 0.520402
Reading from 3036: heap size 302 MB, throughput 0.548227
Reading from 3036: heap size 344 MB, throughput 0.796077
Reading from 3036: heap size 347 MB, throughput 0.821879
Reading from 3036: heap size 353 MB, throughput 0.788216
Reading from 3034: heap size 437 MB, throughput 0.251765
Reading from 3036: heap size 353 MB, throughput 0.857561
Reading from 3034: heap size 498 MB, throughput 0.490831
Reading from 3036: heap size 356 MB, throughput 0.857438
Reading from 3034: heap size 489 MB, throughput 0.567209
Reading from 3034: heap size 426 MB, throughput 0.567957
Reading from 3036: heap size 357 MB, throughput 0.896058
Reading from 3034: heap size 490 MB, throughput 0.565101
Reading from 3036: heap size 351 MB, throughput 0.760664
Reading from 3036: heap size 355 MB, throughput 0.752156
Reading from 3034: heap size 492 MB, throughput 0.57447
Reading from 3036: heap size 355 MB, throughput 0.705552
Numeric result:
Recommendation: 2 clients, utility 0.390083:
    h1: 1211 MB (U(h) = 0.603834*h^0.001)
    h2: 1763 MB (U(h) = 0.360961*h^0.0769183)
Recommendation: 2 clients, utility 0.390083:
    h1: 1211 MB (U(h) = 0.603834*h^0.001)
    h2: 1763 MB (U(h) = 0.360961*h^0.0769183)
Reading from 3036: heap size 357 MB, throughput 0.704099
Reading from 3036: heap size 362 MB, throughput 0.654793
Reading from 3034: heap size 497 MB, throughput 0.314757
Reading from 3034: heap size 555 MB, throughput 0.538036
Reading from 3036: heap size 363 MB, throughput 0.939454
Reading from 3034: heap size 558 MB, throughput 0.500125
Reading from 3034: heap size 561 MB, throughput 0.526363
Reading from 3034: heap size 565 MB, throughput 0.535021
Reading from 3034: heap size 572 MB, throughput 0.552448
Reading from 3034: heap size 579 MB, throughput 0.586519
Reading from 3036: heap size 368 MB, throughput 0.962848
Reading from 3034: heap size 590 MB, throughput 0.372875
Reading from 3034: heap size 658 MB, throughput 0.494359
Reading from 3034: heap size 667 MB, throughput 0.509458
Reading from 3036: heap size 370 MB, throughput 0.965235
Reading from 3034: heap size 673 MB, throughput 0.739403
Reading from 3034: heap size 677 MB, throughput 0.798362
Reading from 3036: heap size 368 MB, throughput 0.966109
Reading from 3034: heap size 675 MB, throughput 0.819288
Reading from 3034: heap size 692 MB, throughput 0.723849
Reading from 3034: heap size 700 MB, throughput 0.683454
Reading from 3034: heap size 716 MB, throughput 0.555184
Reading from 3036: heap size 372 MB, throughput 0.966816
Reading from 3034: heap size 726 MB, throughput 0.486189
Reading from 3034: heap size 803 MB, throughput 0.33945
Reading from 3036: heap size 372 MB, throughput 0.964061
Reading from 3034: heap size 784 MB, throughput 0.0692398
Numeric result:
Recommendation: 2 clients, utility 0.446604:
    h1: 1211 MB (U(h) = 0.596995*h^0.001)
    h2: 1763 MB (U(h) = 0.304878*h^0.119135)
Recommendation: 2 clients, utility 0.446604:
    h1: 1211 MB (U(h) = 0.596995*h^0.001)
    h2: 1763 MB (U(h) = 0.304878*h^0.119135)
Reading from 3034: heap size 879 MB, throughput 0.27995
Reading from 3034: heap size 880 MB, throughput 0.729679
Reading from 3034: heap size 881 MB, throughput 0.700274
Reading from 3034: heap size 871 MB, throughput 0.649847
Reading from 3034: heap size 876 MB, throughput 0.556236
Reading from 3036: heap size 375 MB, throughput 0.963674
Reading from 3034: heap size 866 MB, throughput 0.620329
Reading from 3034: heap size 871 MB, throughput 0.731435
Reading from 3034: heap size 871 MB, throughput 0.420772
Reading from 3036: heap size 372 MB, throughput 0.967347
Reading from 3034: heap size 968 MB, throughput 0.591646
Reading from 3034: heap size 973 MB, throughput 0.780157
Reading from 3034: heap size 976 MB, throughput 0.823673
Reading from 3034: heap size 973 MB, throughput 0.882378
Reading from 3034: heap size 793 MB, throughput 0.869425
Reading from 3034: heap size 950 MB, throughput 0.832981
Reading from 3034: heap size 821 MB, throughput 0.803411
Reading from 3034: heap size 935 MB, throughput 0.771013
Reading from 3034: heap size 826 MB, throughput 0.756495
Reading from 3036: heap size 375 MB, throughput 0.963451
Reading from 3034: heap size 925 MB, throughput 0.764195
Reading from 3034: heap size 825 MB, throughput 0.783682
Reading from 3034: heap size 919 MB, throughput 0.797701
Reading from 3034: heap size 926 MB, throughput 0.804214
Reading from 3034: heap size 913 MB, throughput 0.81944
Reading from 3034: heap size 920 MB, throughput 0.832264
Reading from 3036: heap size 375 MB, throughput 0.969938
Reading from 3034: heap size 911 MB, throughput 0.957237
Reading from 3034: heap size 916 MB, throughput 0.952825
Reading from 3034: heap size 916 MB, throughput 0.913165
Reading from 3034: heap size 919 MB, throughput 0.861861
Reading from 3036: heap size 376 MB, throughput 0.971682
Reading from 3034: heap size 922 MB, throughput 0.836558
Reading from 3034: heap size 923 MB, throughput 0.818952
Reading from 3034: heap size 925 MB, throughput 0.82926
Reading from 3034: heap size 927 MB, throughput 0.815034
Reading from 3034: heap size 929 MB, throughput 0.809056
Reading from 3034: heap size 931 MB, throughput 0.824789
Reading from 3034: heap size 933 MB, throughput 0.856709
Reading from 3036: heap size 379 MB, throughput 0.972422
Reading from 3034: heap size 936 MB, throughput 0.867576
Reading from 3034: heap size 938 MB, throughput 0.852633
Numeric result:
Recommendation: 2 clients, utility 0.503758:
    h1: 1211 MB (U(h) = 0.353699*h^0.0796648)
    h2: 1763 MB (U(h) = 0.275976*h^0.143889)
Recommendation: 2 clients, utility 0.503758:
    h1: 1211 MB (U(h) = 0.353699*h^0.0796648)
    h2: 1763 MB (U(h) = 0.275976*h^0.143889)
Reading from 3034: heap size 940 MB, throughput 0.786745
Reading from 3036: heap size 380 MB, throughput 0.966863
Reading from 3034: heap size 929 MB, throughput 0.703504
Reading from 3034: heap size 1039 MB, throughput 0.601814
Reading from 3034: heap size 1037 MB, throughput 0.662331
Reading from 3034: heap size 1042 MB, throughput 0.676593
Reading from 3034: heap size 1045 MB, throughput 0.714121
Reading from 3036: heap size 383 MB, throughput 0.973599
Reading from 3034: heap size 1048 MB, throughput 0.716823
Reading from 3034: heap size 1052 MB, throughput 0.725161
Reading from 3034: heap size 1055 MB, throughput 0.719124
Reading from 3036: heap size 384 MB, throughput 0.958563
Reading from 3034: heap size 1067 MB, throughput 0.722846
Reading from 3036: heap size 386 MB, throughput 0.942487
Reading from 3034: heap size 1068 MB, throughput 0.689191
Reading from 3036: heap size 388 MB, throughput 0.90585
Reading from 3036: heap size 395 MB, throughput 0.891669
Reading from 3036: heap size 398 MB, throughput 0.954631
Reading from 3034: heap size 1080 MB, throughput 0.919701
Reading from 3034: heap size 1085 MB, throughput 0.934113
Reading from 3036: heap size 404 MB, throughput 0.939801
Reading from 3034: heap size 1103 MB, throughput 0.94765
Reading from 3036: heap size 435 MB, throughput 0.988721
Numeric result:
Recommendation: 2 clients, utility 0.752098:
    h1: 1836.26 MB (U(h) = 0.0831276*h^0.310528)
    h2: 1137.74 MB (U(h) = 0.226476*h^0.192394)
Recommendation: 2 clients, utility 0.752098:
    h1: 1836.29 MB (U(h) = 0.0831276*h^0.310528)
    h2: 1137.71 MB (U(h) = 0.226476*h^0.192394)
Reading from 3034: heap size 1104 MB, throughput 0.951238
Reading from 3036: heap size 437 MB, throughput 0.991607
Reading from 3034: heap size 1108 MB, throughput 0.952835
Reading from 3036: heap size 440 MB, throughput 0.989876
Reading from 3034: heap size 1112 MB, throughput 0.956605
Reading from 3036: heap size 445 MB, throughput 0.989863
Reading from 3034: heap size 1108 MB, throughput 0.94789
Reading from 3036: heap size 446 MB, throughput 0.988459
Reading from 3034: heap size 1113 MB, throughput 0.95221
Reading from 3036: heap size 442 MB, throughput 0.986765
Reading from 3034: heap size 1119 MB, throughput 0.948468
Numeric result:
Recommendation: 2 clients, utility 0.808132:
    h1: 1751.9 MB (U(h) = 0.0862899*h^0.306868)
    h2: 1222.1 MB (U(h) = 0.206701*h^0.214069)
Recommendation: 2 clients, utility 0.808132:
    h1: 1751.89 MB (U(h) = 0.0862899*h^0.306868)
    h2: 1222.11 MB (U(h) = 0.206701*h^0.214069)
Reading from 3036: heap size 445 MB, throughput 0.986687
Reading from 3034: heap size 1119 MB, throughput 0.947379
Reading from 3036: heap size 439 MB, throughput 0.985656
Reading from 3034: heap size 1126 MB, throughput 0.949148
Reading from 3036: heap size 442 MB, throughput 0.984384
Reading from 3036: heap size 443 MB, throughput 0.978434
Reading from 3036: heap size 444 MB, throughput 0.958174
Reading from 3034: heap size 1128 MB, throughput 0.942677
Reading from 3036: heap size 448 MB, throughput 0.939189
Reading from 3036: heap size 452 MB, throughput 0.964817
Reading from 3034: heap size 1135 MB, throughput 0.947908
Reading from 3036: heap size 457 MB, throughput 0.984738
Reading from 3034: heap size 1139 MB, throughput 0.946431
Numeric result:
Recommendation: 2 clients, utility 0.880346:
    h1: 1752.94 MB (U(h) = 0.0735541*h^0.332852)
    h2: 1221.06 MB (U(h) = 0.191718*h^0.23186)
Recommendation: 2 clients, utility 0.880346:
    h1: 1752.93 MB (U(h) = 0.0735541*h^0.332852)
    h2: 1221.07 MB (U(h) = 0.191718*h^0.23186)
Reading from 3036: heap size 460 MB, throughput 0.98651
Reading from 3034: heap size 1146 MB, throughput 0.951502
Reading from 3036: heap size 456 MB, throughput 0.987413
Reading from 3034: heap size 1151 MB, throughput 0.949917
Reading from 3036: heap size 461 MB, throughput 0.986132
Reading from 3034: heap size 1158 MB, throughput 0.951319
Reading from 3036: heap size 458 MB, throughput 0.985391
Reading from 3034: heap size 1163 MB, throughput 0.952176
Reading from 3036: heap size 461 MB, throughput 0.985319
Reading from 3034: heap size 1172 MB, throughput 0.944883
Numeric result:
Recommendation: 2 clients, utility 0.937359:
    h1: 1775.68 MB (U(h) = 0.0627102*h^0.35871)
    h2: 1198.32 MB (U(h) = 0.183545*h^0.242075)
Recommendation: 2 clients, utility 0.937359:
    h1: 1775.68 MB (U(h) = 0.0627102*h^0.35871)
    h2: 1198.32 MB (U(h) = 0.183545*h^0.242075)
Reading from 3036: heap size 462 MB, throughput 0.985228
Reading from 3034: heap size 1175 MB, throughput 0.946968
Reading from 3036: heap size 463 MB, throughput 0.984485
Reading from 3034: heap size 1183 MB, throughput 0.94749
Reading from 3036: heap size 467 MB, throughput 0.987015
Reading from 3036: heap size 467 MB, throughput 0.979454
Reading from 3036: heap size 467 MB, throughput 0.963417
Reading from 3034: heap size 1185 MB, throughput 0.95197
Reading from 3036: heap size 469 MB, throughput 0.973594
Reading from 3036: heap size 476 MB, throughput 0.984452
Numeric result:
Recommendation: 2 clients, utility 1.01249:
    h1: 1850.33 MB (U(h) = 0.0430635*h^0.415335)
    h2: 1123.67 MB (U(h) = 0.175721*h^0.252227)
Recommendation: 2 clients, utility 1.01249:
    h1: 1850.33 MB (U(h) = 0.0430635*h^0.415335)
    h2: 1123.67 MB (U(h) = 0.175721*h^0.252227)
Reading from 3034: heap size 1193 MB, throughput 0.901435
Reading from 3036: heap size 477 MB, throughput 0.988114
Reading from 3034: heap size 1284 MB, throughput 0.941054
Reading from 3036: heap size 479 MB, throughput 0.989827
Reading from 3034: heap size 1286 MB, throughput 0.957378
Reading from 3034: heap size 1292 MB, throughput 0.964672
Reading from 3036: heap size 481 MB, throughput 0.988931
Reading from 3034: heap size 1302 MB, throughput 0.967741
Reading from 3036: heap size 480 MB, throughput 0.988203
Numeric result:
Recommendation: 2 clients, utility 1.12321:
    h1: 1981.6 MB (U(h) = 0.0216172*h^0.517768)
    h2: 992.398 MB (U(h) = 0.17042*h^0.259304)
Recommendation: 2 clients, utility 1.12321:
    h1: 1981.6 MB (U(h) = 0.0216172*h^0.517768)
    h2: 992.404 MB (U(h) = 0.17042*h^0.259304)
Reading from 3034: heap size 1302 MB, throughput 0.97048
Reading from 3036: heap size 482 MB, throughput 0.987675
Reading from 3034: heap size 1297 MB, throughput 0.968107
Reading from 3036: heap size 483 MB, throughput 0.987579
Reading from 3034: heap size 1302 MB, throughput 0.967261
Reading from 3036: heap size 484 MB, throughput 0.989665
Reading from 3036: heap size 488 MB, throughput 0.983114
Reading from 3036: heap size 488 MB, throughput 0.970872
Reading from 3034: heap size 1288 MB, throughput 0.966212
Numeric result:
Recommendation: 2 clients, utility 1.19964:
    h1: 2042.85 MB (U(h) = 0.013968*h^0.581865)
    h2: 931.15 MB (U(h) = 0.166095*h^0.265221)
Recommendation: 2 clients, utility 1.19964:
    h1: 2042.85 MB (U(h) = 0.013968*h^0.581865)
    h2: 931.154 MB (U(h) = 0.166095*h^0.265221)
Reading from 3036: heap size 495 MB, throughput 0.987144
Reading from 3034: heap size 1295 MB, throughput 0.963964
Reading from 3036: heap size 496 MB, throughput 0.990693
Reading from 3034: heap size 1293 MB, throughput 0.963499
Reading from 3036: heap size 499 MB, throughput 0.99108
Reading from 3034: heap size 1295 MB, throughput 0.966611
Reading from 3036: heap size 502 MB, throughput 0.990922
Reading from 3034: heap size 1300 MB, throughput 0.961917
Numeric result:
Recommendation: 2 clients, utility 1.32751:
    h1: 2139.37 MB (U(h) = 0.00651635*h^0.693196)
    h2: 834.625 MB (U(h) = 0.162339*h^0.270434)
Recommendation: 2 clients, utility 1.32751:
    h1: 2139.37 MB (U(h) = 0.00651635*h^0.693196)
    h2: 834.625 MB (U(h) = 0.162339*h^0.270434)
Reading from 3036: heap size 500 MB, throughput 0.990177
Reading from 3034: heap size 1302 MB, throughput 0.962999
Reading from 3036: heap size 503 MB, throughput 0.990118
Reading from 3034: heap size 1308 MB, throughput 0.958134
Reading from 3036: heap size 505 MB, throughput 0.988442
Reading from 3034: heap size 1314 MB, throughput 0.955384
Reading from 3036: heap size 506 MB, throughput 0.988265
Reading from 3036: heap size 508 MB, throughput 0.980975
Reading from 3036: heap size 510 MB, throughput 0.967192
Reading from 3034: heap size 1322 MB, throughput 0.953708
Numeric result:
Recommendation: 2 clients, utility 1.42303:
    h1: 2182.87 MB (U(h) = 0.00402077*h^0.763276)
    h2: 791.132 MB (U(h) = 0.157955*h^0.276641)
Recommendation: 2 clients, utility 1.42303:
    h1: 2182.85 MB (U(h) = 0.00402077*h^0.763276)
    h2: 791.15 MB (U(h) = 0.157955*h^0.276641)
Reading from 3034: heap size 1330 MB, throughput 0.953749
Reading from 3036: heap size 518 MB, throughput 0.98725
Reading from 3034: heap size 1339 MB, throughput 0.953494
Reading from 3036: heap size 519 MB, throughput 0.990255
Reading from 3034: heap size 1345 MB, throughput 0.952353
Reading from 3036: heap size 521 MB, throughput 0.991112
Numeric result:
Recommendation: 2 clients, utility 1.80962:
    h1: 1780.09 MB (U(h) = 0.00439275*h^0.750339)
    h2: 1193.91 MB (U(h) = 0.0424049*h^0.503255)
Recommendation: 2 clients, utility 1.80962:
    h1: 1780.09 MB (U(h) = 0.00439275*h^0.750339)
    h2: 1193.91 MB (U(h) = 0.0424049*h^0.503255)
Reading from 3036: heap size 523 MB, throughput 0.990517
Reading from 3036: heap size 523 MB, throughput 0.990417
Reading from 3036: heap size 525 MB, throughput 0.988913
Reading from 3036: heap size 528 MB, throughput 0.987507
Numeric result:
Recommendation: 2 clients, utility 2.41714:
    h1: 1510.53 MB (U(h) = 0.00439275*h^0.750339)
    h2: 1463.47 MB (U(h) = 0.0113242*h^0.726969)
Recommendation: 2 clients, utility 2.41714:
    h1: 1510.52 MB (U(h) = 0.00439275*h^0.750339)
    h2: 1463.48 MB (U(h) = 0.0113242*h^0.726969)
Reading from 3036: heap size 529 MB, throughput 0.980721
Reading from 3034: heap size 1355 MB, throughput 0.980497
Reading from 3036: heap size 533 MB, throughput 0.983325
Reading from 3036: heap size 534 MB, throughput 0.989538
Reading from 3036: heap size 537 MB, throughput 0.990874
Numeric result:
Recommendation: 2 clients, utility 3.04554:
    h1: 1360.17 MB (U(h) = 0.00426108*h^0.754703)
    h2: 1613.83 MB (U(h) = 0.00413793*h^0.895435)
Recommendation: 2 clients, utility 3.04554:
    h1: 1360.18 MB (U(h) = 0.00426108*h^0.754703)
    h2: 1613.82 MB (U(h) = 0.00413793*h^0.895435)
Reading from 3036: heap size 539 MB, throughput 0.990676
Reading from 3036: heap size 538 MB, throughput 0.990489
Reading from 3034: heap size 1358 MB, throughput 0.958352
Reading from 3036: heap size 540 MB, throughput 0.989695
Reading from 3036: heap size 543 MB, throughput 0.99107
Reading from 3036: heap size 543 MB, throughput 0.985802
Numeric result:
Recommendation: 2 clients, utility 2.34213:
    h1: 1566.8 MB (U(h) = 0.003768*h^0.772305)
    h2: 1407.2 MB (U(h) = 0.0138734*h^0.69363)
Recommendation: 2 clients, utility 2.34213:
    h1: 1566.81 MB (U(h) = 0.003768*h^0.772305)
    h2: 1407.19 MB (U(h) = 0.0138734*h^0.69363)
Reading from 3036: heap size 543 MB, throughput 0.979615
Reading from 3034: heap size 1426 MB, throughput 0.988765
Reading from 3034: heap size 1443 MB, throughput 0.985915
Reading from 3034: heap size 1449 MB, throughput 0.978983
Reading from 3034: heap size 1450 MB, throughput 0.9648
Reading from 3036: heap size 545 MB, throughput 0.989475
Reading from 3034: heap size 1451 MB, throughput 0.947002
Reading from 3034: heap size 1453 MB, throughput 0.918761
Reading from 3034: heap size 1459 MB, throughput 0.890471
Reading from 3034: heap size 1463 MB, throughput 0.948535
Reading from 3036: heap size 549 MB, throughput 0.991839
Reading from 3034: heap size 1479 MB, throughput 0.978154
Numeric result:
Recommendation: 2 clients, utility 2.40975:
    h1: 1596.06 MB (U(h) = 0.00255823*h^0.825934)
    h2: 1377.94 MB (U(h) = 0.0123041*h^0.713065)
Recommendation: 2 clients, utility 2.40975:
    h1: 1596.06 MB (U(h) = 0.00255823*h^0.825934)
    h2: 1377.94 MB (U(h) = 0.0123041*h^0.713065)
Reading from 3036: heap size 551 MB, throughput 0.992142
Reading from 3034: heap size 1484 MB, throughput 0.980389
Reading from 3036: heap size 549 MB, throughput 0.991797
Reading from 3034: heap size 1495 MB, throughput 0.980175
Reading from 3036: heap size 552 MB, throughput 0.990916
Reading from 3034: heap size 1500 MB, throughput 0.978898
Numeric result:
Recommendation: 2 clients, utility 1.71952:
    h1: 1955.84 MB (U(h) = 0.00343107*h^0.783703)
    h2: 1018.16 MB (U(h) = 0.0782337*h^0.407989)
Recommendation: 2 clients, utility 1.71952:
    h1: 1955.82 MB (U(h) = 0.00343107*h^0.783703)
    h2: 1018.18 MB (U(h) = 0.0782337*h^0.407989)
Reading from 3036: heap size 556 MB, throughput 0.991957
Reading from 3036: heap size 556 MB, throughput 0.986981
Reading from 3034: heap size 1493 MB, throughput 0.977427
Reading from 3036: heap size 557 MB, throughput 0.985896
Reading from 3034: heap size 1502 MB, throughput 0.971687
Reading from 3036: heap size 560 MB, throughput 0.991431
Reading from 3034: heap size 1494 MB, throughput 0.973069
Reading from 3036: heap size 562 MB, throughput 0.992234
Numeric result:
Recommendation: 2 clients, utility 1.74401:
    h1: 1986.26 MB (U(h) = 0.00260139*h^0.822222)
    h2: 987.736 MB (U(h) = 0.077657*h^0.408879)
Recommendation: 2 clients, utility 1.74401:
    h1: 1986.26 MB (U(h) = 0.00260139*h^0.822222)
    h2: 987.74 MB (U(h) = 0.077657*h^0.408879)
Reading from 3034: heap size 1500 MB, throughput 0.972302
Reading from 3036: heap size 564 MB, throughput 0.992722
Reading from 3034: heap size 1497 MB, throughput 0.970651
Reading from 3036: heap size 563 MB, throughput 0.991957
Reading from 3034: heap size 1504 MB, throughput 0.969543
Reading from 3036: heap size 565 MB, throughput 0.990964
Numeric result:
Recommendation: 2 clients, utility 1.82987:
    h1: 1990.34 MB (U(h) = 0.00166498*h^0.884725)
    h2: 983.662 MB (U(h) = 0.0651246*h^0.437251)
Recommendation: 2 clients, utility 1.82987:
    h1: 1990.33 MB (U(h) = 0.00166498*h^0.884725)
    h2: 983.667 MB (U(h) = 0.0651246*h^0.437251)
Reading from 3036: heap size 567 MB, throughput 0.989672
Reading from 3036: heap size 569 MB, throughput 0.983651
Reading from 3034: heap size 1512 MB, throughput 0.970099
Reading from 3036: heap size 573 MB, throughput 0.98994
Reading from 3034: heap size 1514 MB, throughput 0.968988
Reading from 3036: heap size 575 MB, throughput 0.991745
Reading from 3034: heap size 1523 MB, throughput 0.966731
Numeric result:
Recommendation: 2 clients, utility 1.87122:
    h1: 1903.71 MB (U(h) = 0.0018727*h^0.867421)
    h2: 1070.29 MB (U(h) = 0.0475825*h^0.487676)
Recommendation: 2 clients, utility 1.87122:
    h1: 1903.71 MB (U(h) = 0.0018727*h^0.867421)
    h2: 1070.29 MB (U(h) = 0.0475825*h^0.487676)
Reading from 3036: heap size 576 MB, throughput 0.992134
Reading from 3034: heap size 1531 MB, throughput 0.962933
Reading from 3036: heap size 578 MB, throughput 0.991855
Reading from 3034: heap size 1541 MB, throughput 0.960334
Reading from 3036: heap size 578 MB, throughput 0.991415
Numeric result:
Recommendation: 2 clients, utility 1.66835:
    h1: 1610.7 MB (U(h) = 0.0159445*h^0.566854)
    h2: 1363.3 MB (U(h) = 0.0498699*h^0.479784)
Recommendation: 2 clients, utility 1.66835:
    h1: 1610.7 MB (U(h) = 0.0159445*h^0.566854)
    h2: 1363.3 MB (U(h) = 0.0498699*h^0.479784)
Reading from 3034: heap size 1554 MB, throughput 0.961306
Reading from 3036: heap size 580 MB, throughput 0.991147
Reading from 3036: heap size 583 MB, throughput 0.986497
Reading from 3034: heap size 1569 MB, throughput 0.961809
Reading from 3036: heap size 584 MB, throughput 0.989871
Reading from 3034: heap size 1580 MB, throughput 0.958851
Reading from 3036: heap size 592 MB, throughput 0.992679
Numeric result:
Recommendation: 2 clients, utility 1.55939:
    h1: 1538.12 MB (U(h) = 0.0317955*h^0.470269)
    h2: 1435.88 MB (U(h) = 0.0639524*h^0.439004)
Recommendation: 2 clients, utility 1.55939:
    h1: 1538.13 MB (U(h) = 0.0317955*h^0.470269)
    h2: 1435.87 MB (U(h) = 0.0639524*h^0.439004)
Reading from 3034: heap size 1595 MB, throughput 0.957941
Reading from 3036: heap size 592 MB, throughput 0.992608
Reading from 3034: heap size 1538 MB, throughput 0.955995
Reading from 3036: heap size 592 MB, throughput 0.992581
Reading from 3034: heap size 1597 MB, throughput 0.958022
Numeric result:
Recommendation: 2 clients, utility 1.54215:
    h1: 1392.56 MB (U(h) = 0.0578966*h^0.386545)
    h2: 1581.44 MB (U(h) = 0.0639545*h^0.438999)
Recommendation: 2 clients, utility 1.54215:
    h1: 1392.52 MB (U(h) = 0.0578966*h^0.386545)
    h2: 1581.48 MB (U(h) = 0.0639545*h^0.438999)
Reading from 3036: heap size 594 MB, throughput 0.991803
Reading from 3036: heap size 597 MB, throughput 0.988005
Reading from 3036: heap size 597 MB, throughput 0.978717
Reading from 3034: heap size 1529 MB, throughput 0.952822
Reading from 3036: heap size 602 MB, throughput 0.98668
Reading from 3034: heap size 1693 MB, throughput 0.97333
Numeric result:
Recommendation: 2 clients, utility 1.28583:
    h1: 1692.89 MB (U(h) = 0.0695394*h^0.361047)
    h2: 1281.11 MB (U(h) = 0.178737*h^0.273219)
Recommendation: 2 clients, utility 1.28583:
    h1: 1692.91 MB (U(h) = 0.0695394*h^0.361047)
    h2: 1281.09 MB (U(h) = 0.178737*h^0.273219)
Reading from 3036: heap size 603 MB, throughput 0.991087
Reading from 3034: heap size 1691 MB, throughput 0.978914
Reading from 3036: heap size 607 MB, throughput 0.989817
Reading from 3034: heap size 1699 MB, throughput 0.979327
Reading from 3034: heap size 1467 MB, throughput 0.979774
Reading from 3036: heap size 609 MB, throughput 0.990821
Numeric result:
Recommendation: 2 clients, utility 1.23611:
    h1: 1836.24 MB (U(h) = 0.0660363*h^0.36806)
    h2: 1137.76 MB (U(h) = 0.236583*h^0.228062)
Recommendation: 2 clients, utility 1.23611:
    h1: 1836.22 MB (U(h) = 0.0660363*h^0.36806)
    h2: 1137.78 MB (U(h) = 0.236583*h^0.228062)
Reading from 3036: heap size 610 MB, throughput 0.9898
Reading from 3036: heap size 611 MB, throughput 0.987936
Reading from 3036: heap size 620 MB, throughput 0.985421
Reading from 3034: heap size 1703 MB, throughput 0.988105
Reading from 3036: heap size 621 MB, throughput 0.992004
Numeric result:
Recommendation: 2 clients, utility 1.16242:
    h1: 2194.57 MB (U(h) = 0.0659017*h^0.36826)
    h2: 779.426 MB (U(h) = 0.434291*h^0.130785)
Recommendation: 2 clients, utility 1.16242:
    h1: 2194.6 MB (U(h) = 0.0659017*h^0.36826)
    h2: 779.396 MB (U(h) = 0.434291*h^0.130785)
Reading from 3036: heap size 624 MB, throughput 0.992782
Reading from 3036: heap size 625 MB, throughput 0.992885
Numeric result:
Recommendation: 2 clients, utility 1.14996:
    h1: 2480.26 MB (U(h) = 0.0659017*h^0.36826)
    h2: 493.738 MB (U(h) = 0.622722*h^0.0732997)
Recommendation: 2 clients, utility 1.14996:
    h1: 2480.31 MB (U(h) = 0.0659017*h^0.36826)
    h2: 493.689 MB (U(h) = 0.622722*h^0.0732997)
Reading from 3036: heap size 623 MB, throughput 0.991973
Reading from 3036: heap size 600 MB, throughput 0.991498
Reading from 3036: heap size 595 MB, throughput 0.986843
Client 3036 died
Clients: 1
Reading from 3034: heap size 1510 MB, throughput 0.993025
Recommendation: one client; give it all the memory
Reading from 3034: heap size 1696 MB, throughput 0.9915
Reading from 3034: heap size 1698 MB, throughput 0.982508
Reading from 3034: heap size 1677 MB, throughput 0.968317
Reading from 3034: heap size 1647 MB, throughput 0.945654
Reading from 3034: heap size 1655 MB, throughput 0.912368
Reading from 3034: heap size 1576 MB, throughput 0.873333
Client 3034 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
