Behaviour Plotter {#behaviourPlotter}
=================

These scripts plot the behaviour of clients and the daemon over time. Unlike \ref heapVsThroughput, where time is implicit, here time is explicitly represented on an axis, with separate graphs showing how the reported heap size and throughput vary over time. All clients are plotted on the same graph, for easy comparison.

In all cases, clients must be started manually.


Live plotting
-------------

 - `live_without_daemon.sh` -- take live readings directly from clients, without a daemon, for a single throughput model.
 - `live_with_daemon.sh` -- take live readings from a daemon, for a single throughput model.
 - `live_with_daemon_all_models.sh` -- same as above, but for all throughput models. Will be laggy!
 - `live_behaviour_and_function` -- runs `live_with_daemon.sh` and \ref heapVsThroughput at the same time -- will be very laggy!


Offline plotting
----------------

 - `plot_behaviour_batch.py` -- takes readings from stdin and saves them to a file. Intended to be used with a log file saved by the daemon.


Internals
---------

 - `behaviour_plotter.py` -- core functionality
 - `plot_behaviour_live.py` -- main functions for live plotting; use the wrappers listed above rather than calling this directly
