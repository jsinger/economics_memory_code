#!/usr/bin/env python
# Measure the number of iterations a benchmark can complete, on a remote machine
# For internal use only

import argparse
import argtypes
import glob
import distributor.distribute as d
import os
import os.path
import shutil
import staticutility_core as su
import sys
import tempfile
from measureBenchmark import measureBenchmark

parser = argparse.ArgumentParser(description="Measure the number of iterations a benchmark can complete, on a remote machine")

parser.add_argument("outDir", help="where to save the output")
parser.add_argument("runs", type=argtypes.positive_int, help="how many times to run the measurement, for taking averages")

args = parser.parse_args()
outDir = su.MeasureDir(args.outDir)

su.checkDaCapo()
outDir.makedir()
outDir.machineInfo()

tempDir = su.MeasureDir(tempfile.mkdtemp())
distributeRunDir = d.runDirFromEnv()

for line in sys.stdin:
    if distributeRunDir.aborted():
        break

    l = su.measureInputLineFromStr(line)
    resultsFile = outDir.hostResultsFile(l.b, su.me())
    tempResults = tempfile.mktemp()

    measureBenchmark(args.runs, tempDir, tempResults, l.b, l.heap, l.maxHeap)

    for f in glob.glob(os.path.join(str(tempDir), "*")):
        shutil.move(f, str(outDir))

    with open(tempResults) as infile, open(resultsFile, "a") as outfile:
        for result in infile:
            outfile.write(result)

    os.remove(tempResults)

shutil.rmtree(str(tempDir))
