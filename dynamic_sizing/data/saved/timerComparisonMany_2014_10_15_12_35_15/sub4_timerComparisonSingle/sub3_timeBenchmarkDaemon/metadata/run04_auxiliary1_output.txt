economemd
    total memory: 7435 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub4_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 3389: heap size 9 MB, throughput 0.993014
Clients: 1
Client 3389 has a minimum heap size of 276 MB
Reading from 3388: heap size 9 MB, throughput 0.987542
Clients: 2
Client 3388 has a minimum heap size of 1211 MB
Reading from 3389: heap size 9 MB, throughput 0.988524
Reading from 3388: heap size 9 MB, throughput 0.983406
Reading from 3389: heap size 9 MB, throughput 0.982445
Reading from 3389: heap size 9 MB, throughput 0.963708
Reading from 3388: heap size 11 MB, throughput 0.980715
Reading from 3389: heap size 11 MB, throughput 0.976194
Reading from 3388: heap size 11 MB, throughput 0.981881
Reading from 3389: heap size 11 MB, throughput 0.971452
Reading from 3389: heap size 17 MB, throughput 0.96663
Reading from 3388: heap size 15 MB, throughput 0.955914
Reading from 3388: heap size 18 MB, throughput 0.960723
Reading from 3389: heap size 17 MB, throughput 0.886913
Reading from 3388: heap size 24 MB, throughput 0.883311
Reading from 3389: heap size 30 MB, throughput 0.805769
Reading from 3388: heap size 28 MB, throughput 0.802767
Reading from 3389: heap size 31 MB, throughput 0.458688
Reading from 3388: heap size 32 MB, throughput 0.754437
Reading from 3388: heap size 41 MB, throughput 0.277447
Reading from 3389: heap size 34 MB, throughput 0.625608
Reading from 3388: heap size 49 MB, throughput 0.870267
Reading from 3389: heap size 46 MB, throughput 0.378488
Reading from 3389: heap size 46 MB, throughput 0.401538
Reading from 3388: heap size 50 MB, throughput 0.46969
Reading from 3389: heap size 49 MB, throughput 0.516817
Reading from 3389: heap size 72 MB, throughput 0.535966
Reading from 3388: heap size 71 MB, throughput 0.275174
Reading from 3388: heap size 90 MB, throughput 0.569784
Reading from 3389: heap size 72 MB, throughput 0.432797
Reading from 3388: heap size 96 MB, throughput 0.241257
Reading from 3389: heap size 97 MB, throughput 0.75542
Reading from 3389: heap size 97 MB, throughput 0.681897
Reading from 3389: heap size 98 MB, throughput 0.564878
Reading from 3388: heap size 98 MB, throughput 0.44692
Reading from 3389: heap size 100 MB, throughput 0.444348
Reading from 3388: heap size 130 MB, throughput 0.259081
Reading from 3388: heap size 132 MB, throughput 0.743803
Reading from 3389: heap size 102 MB, throughput 0.487469
Reading from 3389: heap size 132 MB, throughput 0.333603
Reading from 3389: heap size 135 MB, throughput 0.235586
Reading from 3388: heap size 135 MB, throughput 0.404734
Reading from 3389: heap size 137 MB, throughput 0.724426
Reading from 3389: heap size 145 MB, throughput 0.681852
Reading from 3388: heap size 168 MB, throughput 0.72745
Reading from 3389: heap size 147 MB, throughput 0.689961
Reading from 3388: heap size 176 MB, throughput 0.772507
Reading from 3388: heap size 178 MB, throughput 0.699584
Reading from 3388: heap size 181 MB, throughput 0.701918
Reading from 3388: heap size 189 MB, throughput 0.648293
Reading from 3389: heap size 150 MB, throughput 0.357138
Reading from 3389: heap size 189 MB, throughput 0.644986
Reading from 3389: heap size 192 MB, throughput 0.687506
Reading from 3389: heap size 196 MB, throughput 0.740211
Reading from 3388: heap size 194 MB, throughput 0.508917
Reading from 3388: heap size 236 MB, throughput 0.620581
Reading from 3388: heap size 243 MB, throughput 0.626044
Reading from 3389: heap size 199 MB, throughput 0.868245
Reading from 3388: heap size 248 MB, throughput 0.640593
Reading from 3388: heap size 253 MB, throughput 0.586299
Reading from 3389: heap size 207 MB, throughput 0.871636
Reading from 3388: heap size 262 MB, throughput 0.659936
Reading from 3388: heap size 271 MB, throughput 0.584145
Reading from 3389: heap size 215 MB, throughput 0.824231
Reading from 3389: heap size 221 MB, throughput 0.773719
Reading from 3389: heap size 224 MB, throughput 0.651399
Reading from 3389: heap size 228 MB, throughput 0.652295
Reading from 3389: heap size 232 MB, throughput 0.576491
Reading from 3389: heap size 235 MB, throughput 0.641219
Reading from 3388: heap size 279 MB, throughput 0.349303
Reading from 3388: heap size 328 MB, throughput 0.525235
Reading from 3389: heap size 238 MB, throughput 0.578201
Reading from 3389: heap size 277 MB, throughput 0.607151
Reading from 3388: heap size 331 MB, throughput 0.560839
Reading from 3389: heap size 271 MB, throughput 0.629361
Reading from 3388: heap size 333 MB, throughput 0.566038
Reading from 3389: heap size 274 MB, throughput 0.756248
Reading from 3389: heap size 270 MB, throughput 0.658708
Reading from 3389: heap size 273 MB, throughput 0.384988
Reading from 3388: heap size 337 MB, throughput 0.291832
Reading from 3389: heap size 315 MB, throughput 0.606235
Reading from 3389: heap size 316 MB, throughput 0.64399
Reading from 3388: heap size 389 MB, throughput 0.55212
Reading from 3389: heap size 322 MB, throughput 0.745174
Reading from 3388: heap size 391 MB, throughput 0.602142
Reading from 3389: heap size 322 MB, throughput 0.869237
Reading from 3388: heap size 393 MB, throughput 0.608367
Reading from 3388: heap size 394 MB, throughput 0.581744
Reading from 3388: heap size 398 MB, throughput 0.568579
Reading from 3389: heap size 322 MB, throughput 0.937097
Reading from 3389: heap size 275 MB, throughput 0.888237
Reading from 3388: heap size 405 MB, throughput 0.593359
Reading from 3389: heap size 316 MB, throughput 0.886837
Reading from 3389: heap size 320 MB, throughput 0.777753
Reading from 3389: heap size 315 MB, throughput 0.784093
Reading from 3389: heap size 317 MB, throughput 0.846071
Reading from 3389: heap size 315 MB, throughput 0.853124
Reading from 3389: heap size 317 MB, throughput 0.827561
Reading from 3389: heap size 318 MB, throughput 0.814782
Reading from 3388: heap size 412 MB, throughput 0.333792
Reading from 3389: heap size 321 MB, throughput 0.782628
Reading from 3389: heap size 325 MB, throughput 0.692162
Reading from 3388: heap size 465 MB, throughput 0.508447
Reading from 3389: heap size 326 MB, throughput 0.715206
Numeric result:
Recommendation: 2 clients, utility 0.692157:
    h1: 6224 MB (U(h) = 0.626876*h^0.0055604)
    h2: 1211 MB (U(h) = 1.04434*h^0.001)
Recommendation: 2 clients, utility 0.692157:
    h1: 6224 MB (U(h) = 0.626876*h^0.0055604)
    h2: 1211 MB (U(h) = 1.04434*h^0.001)
Reading from 3389: heap size 333 MB, throughput 0.593364
Reading from 3388: heap size 475 MB, throughput 0.525274
Reading from 3389: heap size 333 MB, throughput 0.658199
Reading from 3388: heap size 476 MB, throughput 0.485059
Reading from 3388: heap size 482 MB, throughput 0.516167
Reading from 3388: heap size 488 MB, throughput 0.474236
Reading from 3389: heap size 339 MB, throughput 0.934353
Reading from 3388: heap size 494 MB, throughput 0.248449
Reading from 3388: heap size 552 MB, throughput 0.447554
Reading from 3389: heap size 341 MB, throughput 0.951147
Reading from 3388: heap size 555 MB, throughput 0.485891
Reading from 3388: heap size 558 MB, throughput 0.223683
Reading from 3389: heap size 345 MB, throughput 0.961263
Reading from 3388: heap size 622 MB, throughput 0.482649
Reading from 3388: heap size 622 MB, throughput 0.526006
Reading from 3388: heap size 624 MB, throughput 0.57748
Reading from 3388: heap size 628 MB, throughput 0.563732
Reading from 3388: heap size 631 MB, throughput 0.572236
Reading from 3388: heap size 643 MB, throughput 0.555034
Reading from 3389: heap size 346 MB, throughput 0.9694
Reading from 3388: heap size 651 MB, throughput 0.790719
Reading from 3389: heap size 346 MB, throughput 0.970687
Reading from 3388: heap size 665 MB, throughput 0.813952
Reading from 3389: heap size 348 MB, throughput 0.964942
Reading from 3388: heap size 669 MB, throughput 0.538138
Reading from 3388: heap size 748 MB, throughput 0.694417
Reading from 3388: heap size 765 MB, throughput 0.653336
Reading from 3389: heap size 344 MB, throughput 0.965427
Reading from 3388: heap size 769 MB, throughput 0.641652
Numeric result:
Recommendation: 2 clients, utility 0.728446:
    h1: 6224 MB (U(h) = 0.533533*h^0.045978)
    h2: 1211 MB (U(h) = 0.907212*h^0.001)
Recommendation: 2 clients, utility 0.728446:
    h1: 6224 MB (U(h) = 0.533533*h^0.045978)
    h2: 1211 MB (U(h) = 0.907212*h^0.001)
Reading from 3388: heap size 768 MB, throughput 0.263922
Reading from 3388: heap size 732 MB, throughput 0.392641
Reading from 3389: heap size 347 MB, throughput 0.96481
Reading from 3388: heap size 820 MB, throughput 0.401668
Reading from 3388: heap size 700 MB, throughput 0.392882
Reading from 3389: heap size 347 MB, throughput 0.959027
Reading from 3388: heap size 823 MB, throughput 0.263624
Reading from 3388: heap size 900 MB, throughput 0.58887
Reading from 3388: heap size 903 MB, throughput 0.583727
Reading from 3388: heap size 904 MB, throughput 0.566516
Reading from 3389: heap size 348 MB, throughput 0.957497
Reading from 3388: heap size 911 MB, throughput 0.779971
Reading from 3388: heap size 911 MB, throughput 0.766501
Reading from 3389: heap size 351 MB, throughput 0.96118
Reading from 3388: heap size 916 MB, throughput 0.392503
Reading from 3388: heap size 996 MB, throughput 0.745777
Reading from 3388: heap size 1001 MB, throughput 0.807267
Reading from 3389: heap size 351 MB, throughput 0.966132
Reading from 3388: heap size 1001 MB, throughput 0.884835
Reading from 3388: heap size 985 MB, throughput 0.868533
Reading from 3388: heap size 815 MB, throughput 0.853638
Reading from 3388: heap size 968 MB, throughput 0.833076
Reading from 3388: heap size 820 MB, throughput 0.83224
Reading from 3388: heap size 953 MB, throughput 0.848916
Reading from 3388: heap size 825 MB, throughput 0.788918
Reading from 3389: heap size 354 MB, throughput 0.965383
Reading from 3388: heap size 946 MB, throughput 0.812916
Reading from 3388: heap size 825 MB, throughput 0.818536
Reading from 3388: heap size 937 MB, throughput 0.851332
Reading from 3388: heap size 831 MB, throughput 0.869442
Reading from 3389: heap size 355 MB, throughput 0.969099
Reading from 3388: heap size 931 MB, throughput 0.967203
Reading from 3389: heap size 357 MB, throughput 0.969804
Reading from 3388: heap size 937 MB, throughput 0.950795
Numeric result:
Recommendation: 2 clients, utility 0.630943:
    h1: 6224 MB (U(h) = 0.482512*h^0.070962)
    h2: 1211 MB (U(h) = 0.698495*h^0.001)
Recommendation: 2 clients, utility 0.630943:
    h1: 6224 MB (U(h) = 0.482512*h^0.070962)
    h2: 1211 MB (U(h) = 0.698495*h^0.001)
Reading from 3388: heap size 928 MB, throughput 0.914623
Reading from 3388: heap size 933 MB, throughput 0.880178
Reading from 3388: heap size 925 MB, throughput 0.823409
Reading from 3388: heap size 929 MB, throughput 0.804237
Reading from 3388: heap size 923 MB, throughput 0.795858
Reading from 3389: heap size 359 MB, throughput 0.977427
Reading from 3388: heap size 928 MB, throughput 0.777818
Reading from 3388: heap size 926 MB, throughput 0.782654
Reading from 3389: heap size 362 MB, throughput 0.965957
Reading from 3389: heap size 363 MB, throughput 0.925084
Reading from 3389: heap size 362 MB, throughput 0.884469
Reading from 3389: heap size 366 MB, throughput 0.819747
Reading from 3388: heap size 929 MB, throughput 0.872185
Reading from 3389: heap size 374 MB, throughput 0.811213
Reading from 3388: heap size 931 MB, throughput 0.875948
Reading from 3388: heap size 933 MB, throughput 0.863709
Reading from 3388: heap size 937 MB, throughput 0.823437
Reading from 3388: heap size 938 MB, throughput 0.754958
Reading from 3389: heap size 376 MB, throughput 0.957189
Reading from 3388: heap size 958 MB, throughput 0.744466
Reading from 3389: heap size 379 MB, throughput 0.96774
Reading from 3388: heap size 967 MB, throughput 0.694467
Reading from 3388: heap size 1081 MB, throughput 0.603239
Reading from 3388: heap size 1086 MB, throughput 0.626376
Reading from 3388: heap size 1086 MB, throughput 0.693742
Reading from 3388: heap size 1090 MB, throughput 0.661035
Reading from 3388: heap size 1100 MB, throughput 0.676557
Reading from 3389: heap size 382 MB, throughput 0.931717
Reading from 3388: heap size 1101 MB, throughput 0.676994
Reading from 3389: heap size 409 MB, throughput 0.982051
Reading from 3388: heap size 1114 MB, throughput 0.918216
Reading from 3389: heap size 412 MB, throughput 0.98504
Numeric result:
Recommendation: 2 clients, utility 0.61752:
    h1: 6224 MB (U(h) = 0.426175*h^0.10105)
    h2: 1211 MB (U(h) = 0.595098*h^0.001)
Recommendation: 2 clients, utility 0.61752:
    h1: 6224 MB (U(h) = 0.426175*h^0.10105)
    h2: 1211 MB (U(h) = 0.595098*h^0.001)
Reading from 3388: heap size 1119 MB, throughput 0.942266
Reading from 3389: heap size 420 MB, throughput 0.987787
Reading from 3388: heap size 1134 MB, throughput 0.944868
Reading from 3389: heap size 421 MB, throughput 0.98874
Reading from 3389: heap size 421 MB, throughput 0.987233
Reading from 3388: heap size 1136 MB, throughput 0.954143
Reading from 3389: heap size 423 MB, throughput 0.987645
Reading from 3388: heap size 1139 MB, throughput 0.95839
Reading from 3389: heap size 417 MB, throughput 0.986761
Reading from 3388: heap size 1144 MB, throughput 0.955598
Reading from 3389: heap size 420 MB, throughput 0.98161
Numeric result:
Recommendation: 2 clients, utility 0.707073:
    h1: 4974.69 MB (U(h) = 0.404176*h^0.113455)
    h2: 2460.31 MB (U(h) = 0.429743*h^0.05611)
Recommendation: 2 clients, utility 0.707073:
    h1: 4974.71 MB (U(h) = 0.404176*h^0.113455)
    h2: 2460.29 MB (U(h) = 0.429743*h^0.05611)
Reading from 3388: heap size 1138 MB, throughput 0.959266
Reading from 3389: heap size 417 MB, throughput 0.98555
Reading from 3389: heap size 419 MB, throughput 0.98104
Reading from 3389: heap size 417 MB, throughput 0.965055
Reading from 3389: heap size 419 MB, throughput 0.940155
Reading from 3389: heap size 426 MB, throughput 0.915018
Reading from 3388: heap size 1144 MB, throughput 0.953506
Reading from 3389: heap size 430 MB, throughput 0.973948
Reading from 3388: heap size 1147 MB, throughput 0.951833
Reading from 3389: heap size 434 MB, throughput 0.980072
Reading from 3389: heap size 438 MB, throughput 0.970737
Reading from 3388: heap size 1148 MB, throughput 0.944883
Reading from 3389: heap size 435 MB, throughput 0.980541
Reading from 3388: heap size 1155 MB, throughput 0.951545
Numeric result:
Recommendation: 2 clients, utility 0.931211:
    h1: 3137.68 MB (U(h) = 0.382085*h^0.126508)
    h2: 4297.32 MB (U(h) = 0.20661*h^0.173232)
Recommendation: 2 clients, utility 0.931211:
    h1: 3138.01 MB (U(h) = 0.382085*h^0.126508)
    h2: 4296.99 MB (U(h) = 0.20661*h^0.173232)
Reading from 3389: heap size 438 MB, throughput 0.983595
Reading from 3388: heap size 1156 MB, throughput 0.951941
Reading from 3389: heap size 436 MB, throughput 0.982507
Reading from 3388: heap size 1163 MB, throughput 0.955834
Reading from 3389: heap size 439 MB, throughput 0.982977
Reading from 3388: heap size 1166 MB, throughput 0.958291
Reading from 3389: heap size 439 MB, throughput 0.983126
Reading from 3388: heap size 1173 MB, throughput 0.956968
Reading from 3389: heap size 440 MB, throughput 0.983542
Numeric result:
Recommendation: 2 clients, utility 1.13329:
    h1: 2540.72 MB (U(h) = 0.371889*h^0.132753)
    h2: 4894.28 MB (U(h) = 0.122555*h^0.255734)
Recommendation: 2 clients, utility 1.13329:
    h1: 2540.68 MB (U(h) = 0.371889*h^0.132753)
    h2: 4894.32 MB (U(h) = 0.122555*h^0.255734)
Reading from 3388: heap size 1177 MB, throughput 0.956179
Reading from 3389: heap size 443 MB, throughput 0.981117
Reading from 3389: heap size 443 MB, throughput 0.98271
Reading from 3389: heap size 449 MB, throughput 0.972381
Reading from 3389: heap size 450 MB, throughput 0.956325
Reading from 3388: heap size 1184 MB, throughput 0.957576
Reading from 3389: heap size 456 MB, throughput 0.966848
Reading from 3389: heap size 456 MB, throughput 0.985582
Reading from 3388: heap size 1189 MB, throughput 0.960112
Reading from 3389: heap size 459 MB, throughput 0.983501
Reading from 3388: heap size 1197 MB, throughput 0.956551
Reading from 3389: heap size 461 MB, throughput 0.986489
Reading from 3388: heap size 1200 MB, throughput 0.95614
Numeric result:
Recommendation: 2 clients, utility 1.15892:
    h1: 2775.92 MB (U(h) = 0.355969*h^0.142746)
    h2: 4659.08 MB (U(h) = 0.138716*h^0.239615)
Recommendation: 2 clients, utility 1.15892:
    h1: 2775.69 MB (U(h) = 0.355969*h^0.142746)
    h2: 4659.31 MB (U(h) = 0.138716*h^0.239615)
Reading from 3389: heap size 460 MB, throughput 0.987268
Reading from 3388: heap size 1208 MB, throughput 0.954916
Reading from 3389: heap size 462 MB, throughput 0.986889
Reading from 3388: heap size 1210 MB, throughput 0.954149
Reading from 3389: heap size 459 MB, throughput 0.987189
Reading from 3389: heap size 462 MB, throughput 0.981943
Reading from 3388: heap size 1217 MB, throughput 0.909494
Reading from 3389: heap size 464 MB, throughput 0.983242
Numeric result:
Recommendation: 2 clients, utility 1.30558:
    h1: 2484.48 MB (U(h) = 0.348939*h^0.14727)
    h2: 4950.52 MB (U(h) = 0.0974821*h^0.293428)
Recommendation: 2 clients, utility 1.30558:
    h1: 2484.59 MB (U(h) = 0.348939*h^0.14727)
    h2: 4950.41 MB (U(h) = 0.0974821*h^0.293428)
Reading from 3388: heap size 1311 MB, throughput 0.944489
Reading from 3389: heap size 464 MB, throughput 0.986716
Reading from 3389: heap size 467 MB, throughput 0.979458
Reading from 3389: heap size 467 MB, throughput 0.965665
Reading from 3389: heap size 472 MB, throughput 0.979015
Reading from 3388: heap size 1314 MB, throughput 0.960858
Reading from 3389: heap size 474 MB, throughput 0.987341
Reading from 3388: heap size 1319 MB, throughput 0.967763
Reading from 3389: heap size 477 MB, throughput 0.989252
Reading from 3388: heap size 1327 MB, throughput 0.970186
Reading from 3389: heap size 478 MB, throughput 0.98992
Numeric result:
Recommendation: 2 clients, utility 1.67173:
    h1: 2012.37 MB (U(h) = 0.339015*h^0.15376)
    h2: 5422.63 MB (U(h) = 0.0434278*h^0.414332)
Recommendation: 2 clients, utility 1.67173:
    h1: 2012.37 MB (U(h) = 0.339015*h^0.15376)
    h2: 5422.63 MB (U(h) = 0.0434278*h^0.414332)
Reading from 3388: heap size 1328 MB, throughput 0.970528
Reading from 3389: heap size 476 MB, throughput 0.990457
Reading from 3388: heap size 1322 MB, throughput 0.969107
Reading from 3389: heap size 479 MB, throughput 0.98877
Reading from 3388: heap size 1327 MB, throughput 0.967352
Reading from 3389: heap size 479 MB, throughput 0.987332
Reading from 3389: heap size 480 MB, throughput 0.986524
Reading from 3388: heap size 1314 MB, throughput 0.966925
Numeric result:
Recommendation: 2 clients, utility 1.83888:
    h1: 1896.96 MB (U(h) = 0.333739*h^0.157277)
    h2: 5538.04 MB (U(h) = 0.0321237*h^0.459148)
Recommendation: 2 clients, utility 1.83888:
    h1: 1896.99 MB (U(h) = 0.333739*h^0.157277)
    h2: 5538.01 MB (U(h) = 0.0321237*h^0.459148)
Reading from 3389: heap size 482 MB, throughput 0.987635
Reading from 3389: heap size 483 MB, throughput 0.980243
Reading from 3389: heap size 482 MB, throughput 0.968939
Reading from 3388: heap size 1321 MB, throughput 0.969453
Reading from 3389: heap size 484 MB, throughput 0.985939
Reading from 3388: heap size 1318 MB, throughput 0.969155
Reading from 3389: heap size 491 MB, throughput 0.990199
Reading from 3388: heap size 1320 MB, throughput 0.96459
Reading from 3389: heap size 492 MB, throughput 0.990236
Numeric result:
Recommendation: 2 clients, utility 2.99534:
    h1: 2757.23 MB (U(h) = 0.129758*h^0.322182)
    h2: 4677.77 MB (U(h) = 0.017735*h^0.546595)
Recommendation: 2 clients, utility 2.99534:
    h1: 2757.24 MB (U(h) = 0.129758*h^0.322182)
    h2: 4677.76 MB (U(h) = 0.017735*h^0.546595)
Reading from 3388: heap size 1325 MB, throughput 0.963608
Reading from 3389: heap size 492 MB, throughput 0.989969
Reading from 3389: heap size 494 MB, throughput 0.985219
Reading from 3388: heap size 1327 MB, throughput 0.957657
Reading from 3389: heap size 494 MB, throughput 0.981313
Reading from 3388: heap size 1333 MB, throughput 0.957241
Reading from 3389: heap size 495 MB, throughput 0.984357
Reading from 3388: heap size 1339 MB, throughput 0.956817
Numeric result:
Recommendation: 2 clients, utility 3.86428:
    h1: 2872.97 MB (U(h) = 0.0880267*h^0.388838)
    h2: 4562.03 MB (U(h) = 0.0109337*h^0.617343)
Recommendation: 2 clients, utility 3.86428:
    h1: 2873.25 MB (U(h) = 0.0880267*h^0.388838)
    h2: 4561.75 MB (U(h) = 0.0109337*h^0.617343)
Reading from 3389: heap size 498 MB, throughput 0.987123
Reading from 3389: heap size 499 MB, throughput 0.9803
Reading from 3389: heap size 500 MB, throughput 0.968494
Reading from 3388: heap size 1345 MB, throughput 0.957015
Reading from 3389: heap size 502 MB, throughput 0.98561
Reading from 3388: heap size 1353 MB, throughput 0.957233
Reading from 3389: heap size 509 MB, throughput 0.990076
Reading from 3389: heap size 510 MB, throughput 0.990563
Numeric result:
Recommendation: 2 clients, utility 5.81971:
    h1: 3371.78 MB (U(h) = 0.0341421*h^0.54923)
    h2: 4063.22 MB (U(h) = 0.00804324*h^0.661861)
Recommendation: 2 clients, utility 5.81971:
    h1: 3371.77 MB (U(h) = 0.0341421*h^0.54923)
    h2: 4063.23 MB (U(h) = 0.00804324*h^0.661861)
Reading from 3389: heap size 510 MB, throughput 0.990486
Reading from 3389: heap size 513 MB, throughput 0.989911
Reading from 3389: heap size 512 MB, throughput 0.989178
Numeric result:
Recommendation: 2 clients, utility 5.96238:
    h1: 3411.35 MB (U(h) = 0.0317633*h^0.561094)
    h2: 4023.65 MB (U(h) = 0.00804324*h^0.661861)
Recommendation: 2 clients, utility 5.96238:
    h1: 3411.19 MB (U(h) = 0.0317633*h^0.561094)
    h2: 4023.81 MB (U(h) = 0.00804324*h^0.661861)
Reading from 3389: heap size 514 MB, throughput 0.991728
Reading from 3389: heap size 516 MB, throughput 0.986854
Reading from 3389: heap size 517 MB, throughput 0.977161
Reading from 3389: heap size 522 MB, throughput 0.985101
Reading from 3388: heap size 1363 MB, throughput 0.933917
Reading from 3389: heap size 524 MB, throughput 0.989442
Reading from 3389: heap size 527 MB, throughput 0.990782
Numeric result:
Recommendation: 2 clients, utility 4.99609:
    h1: 2970.04 MB (U(h) = 0.0583623*h^0.458859)
    h2: 4464.96 MB (U(h) = 0.00662839*h^0.689783)
Recommendation: 2 clients, utility 4.99609:
    h1: 2970.13 MB (U(h) = 0.0583623*h^0.458859)
    h2: 4464.87 MB (U(h) = 0.00662839*h^0.689783)
Reading from 3389: heap size 528 MB, throughput 0.99113
Reading from 3389: heap size 525 MB, throughput 0.990219
Reading from 3389: heap size 528 MB, throughput 0.989395
Reading from 3388: heap size 1507 MB, throughput 0.985413
Numeric result:
Recommendation: 2 clients, utility 4.86168:
    h1: 3024.26 MB (U(h) = 0.0578717*h^0.460043)
    h2: 4410.74 MB (U(h) = 0.00754157*h^0.671043)
Recommendation: 2 clients, utility 4.86168:
    h1: 3024.01 MB (U(h) = 0.0578717*h^0.460043)
    h2: 4410.99 MB (U(h) = 0.00754157*h^0.671043)
Reading from 3389: heap size 531 MB, throughput 0.991147
Reading from 3389: heap size 532 MB, throughput 0.985293
Reading from 3389: heap size 532 MB, throughput 0.981442
Reading from 3388: heap size 1549 MB, throughput 0.98399
Reading from 3388: heap size 1557 MB, throughput 0.972844
Reading from 3389: heap size 534 MB, throughput 0.990865
Reading from 3388: heap size 1546 MB, throughput 0.953934
Reading from 3388: heap size 1572 MB, throughput 0.923798
Reading from 3388: heap size 1600 MB, throughput 0.890287
Reading from 3388: heap size 1614 MB, throughput 0.850442
Reading from 3388: heap size 1648 MB, throughput 0.823281
Reading from 3389: heap size 537 MB, throughput 0.991112
Reading from 3388: heap size 1654 MB, throughput 0.932766
Numeric result:
Recommendation: 2 clients, utility 6.67168:
    h1: 3061.31 MB (U(h) = 0.0332063*h^0.551007)
    h2: 4373.69 MB (U(h) = 0.00328141*h^0.787233)
Recommendation: 2 clients, utility 6.67168:
    h1: 3061.29 MB (U(h) = 0.0332063*h^0.551007)
    h2: 4373.71 MB (U(h) = 0.00328141*h^0.787233)
Reading from 3389: heap size 538 MB, throughput 0.991559
Reading from 3388: heap size 1654 MB, throughput 0.961018
Reading from 3389: heap size 537 MB, throughput 0.991037
Reading from 3388: heap size 1666 MB, throughput 0.963604
Reading from 3389: heap size 539 MB, throughput 0.990432
Reading from 3388: heap size 1659 MB, throughput 0.968849
Numeric result:
Recommendation: 2 clients, utility 6.92922:
    h1: 3069.61 MB (U(h) = 0.0306341*h^0.563911)
    h2: 4365.39 MB (U(h) = 0.00294391*h^0.801959)
Recommendation: 2 clients, utility 6.92922:
    h1: 3069.6 MB (U(h) = 0.0306341*h^0.563911)
    h2: 4365.4 MB (U(h) = 0.00294391*h^0.801959)
Reading from 3389: heap size 541 MB, throughput 0.990567
Reading from 3389: heap size 541 MB, throughput 0.982507
Reading from 3388: heap size 1672 MB, throughput 0.965759
Reading from 3389: heap size 542 MB, throughput 0.975584
Reading from 3389: heap size 544 MB, throughput 0.987688
Reading from 3388: heap size 1659 MB, throughput 0.966737
Reading from 3389: heap size 550 MB, throughput 0.991003
Reading from 3388: heap size 1672 MB, throughput 0.968251
Numeric result:
Recommendation: 2 clients, utility 6.12634:
    h1: 2817.9 MB (U(h) = 0.0463889*h^0.495256)
    h2: 4617.1 MB (U(h) = 0.0027457*h^0.811473)
Recommendation: 2 clients, utility 6.12634:
    h1: 2817.9 MB (U(h) = 0.0463889*h^0.495256)
    h2: 4617.1 MB (U(h) = 0.0027457*h^0.811473)
Reading from 3389: heap size 552 MB, throughput 0.990616
Reading from 3388: heap size 1665 MB, throughput 0.945721
Reading from 3389: heap size 552 MB, throughput 0.991138
Reading from 3388: heap size 1607 MB, throughput 0.98049
Reading from 3389: heap size 554 MB, throughput 0.990381
Reading from 3388: heap size 1610 MB, throughput 0.984391
Numeric result:
Recommendation: 2 clients, utility 5.0564:
    h1: 2756.66 MB (U(h) = 0.0646104*h^0.440975)
    h2: 4678.34 MB (U(h) = 0.00426406*h^0.748372)
Recommendation: 2 clients, utility 5.0564:
    h1: 2756.68 MB (U(h) = 0.0646104*h^0.440975)
    h2: 4678.32 MB (U(h) = 0.00426406*h^0.748372)
Reading from 3389: heap size 556 MB, throughput 0.992807
Reading from 3389: heap size 557 MB, throughput 0.988243
Reading from 3389: heap size 557 MB, throughput 0.983206
Reading from 3388: heap size 1623 MB, throughput 0.984943
Reading from 3389: heap size 559 MB, throughput 0.991532
Reading from 3388: heap size 1640 MB, throughput 0.986887
Reading from 3389: heap size 562 MB, throughput 0.99264
Numeric result:
Recommendation: 2 clients, utility 3.56567:
    h1: 2326 MB (U(h) = 0.149288*h^0.304262)
    h2: 5109 MB (U(h) = 0.00750838*h^0.66829)
Recommendation: 2 clients, utility 3.56567:
    h1: 2326.03 MB (U(h) = 0.149288*h^0.304262)
    h2: 5108.97 MB (U(h) = 0.00750838*h^0.66829)
Reading from 3388: heap size 1641 MB, throughput 0.984529
Reading from 3389: heap size 564 MB, throughput 0.992251
Reading from 3389: heap size 563 MB, throughput 0.991275
Reading from 3388: heap size 1632 MB, throughput 0.981265
Reading from 3389: heap size 565 MB, throughput 0.990493
Reading from 3388: heap size 1640 MB, throughput 0.97884
Numeric result:
Recommendation: 2 clients, utility 3.3784:
    h1: 2241.14 MB (U(h) = 0.169983*h^0.283064)
    h2: 5193.86 MB (U(h) = 0.00817695*h^0.655988)
Recommendation: 2 clients, utility 3.3784:
    h1: 2241.18 MB (U(h) = 0.169983*h^0.283064)
    h2: 5193.82 MB (U(h) = 0.00817695*h^0.655988)
Reading from 3389: heap size 567 MB, throughput 0.990849
Reading from 3389: heap size 568 MB, throughput 0.985084
Reading from 3389: heap size 572 MB, throughput 0.988095
Reading from 3388: heap size 1617 MB, throughput 0.977029
Reading from 3389: heap size 574 MB, throughput 0.991424
Reading from 3388: heap size 1505 MB, throughput 0.974947
Reading from 3389: heap size 575 MB, throughput 0.991989
Numeric result:
Recommendation: 2 clients, utility 2.96084:
    h1: 1690.86 MB (U(h) = 0.297302*h^0.19252)
    h2: 5744.14 MB (U(h) = 0.00828185*h^0.654021)
Recommendation: 2 clients, utility 2.96084:
    h1: 1690.87 MB (U(h) = 0.297302*h^0.19252)
    h2: 5744.13 MB (U(h) = 0.00828185*h^0.654021)
Reading from 3388: heap size 1617 MB, throughput 0.97319
Reading from 3389: heap size 577 MB, throughput 0.991283
Reading from 3388: heap size 1622 MB, throughput 0.969818
Reading from 3389: heap size 577 MB, throughput 0.991063
Reading from 3389: heap size 578 MB, throughput 0.991914
Reading from 3389: heap size 579 MB, throughput 0.987369
Numeric result:
Recommendation: 2 clients, utility 2.67616:
    h1: 1015.66 MB (U(h) = 0.519875*h^0.102495)
    h2: 6419.34 MB (U(h) = 0.00864748*h^0.647814)
Recommendation: 2 clients, utility 2.67616:
    h1: 1015.64 MB (U(h) = 0.519875*h^0.102495)
    h2: 6419.36 MB (U(h) = 0.00864748*h^0.647814)
Reading from 3388: heap size 1631 MB, throughput 0.968192
Reading from 3389: heap size 580 MB, throughput 0.986435
Reading from 3388: heap size 1634 MB, throughput 0.96641
Reading from 3389: heap size 587 MB, throughput 0.99219
Reading from 3389: heap size 587 MB, throughput 0.992161
Reading from 3388: heap size 1645 MB, throughput 0.967897
Numeric result:
Recommendation: 2 clients, utility 2.86184:
    h1: 970.895 MB (U(h) = 0.515317*h^0.103858)
    h2: 6464.11 MB (U(h) = 0.00629936*h^0.691518)
Recommendation: 2 clients, utility 2.86184:
    h1: 970.841 MB (U(h) = 0.515317*h^0.103858)
    h2: 6464.16 MB (U(h) = 0.00629936*h^0.691518)
Reading from 3389: heap size 587 MB, throughput 0.991744
Reading from 3388: heap size 1654 MB, throughput 0.964965
Reading from 3389: heap size 589 MB, throughput 0.990879
Reading from 3388: heap size 1668 MB, throughput 0.96245
Reading from 3389: heap size 590 MB, throughput 0.993096
Reading from 3389: heap size 591 MB, throughput 0.988203
Client 3389 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 3388: heap size 1681 MB, throughput 0.965656
Reading from 3388: heap size 1700 MB, throughput 0.967459
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 3388: heap size 1708 MB, throughput 0.993035
Recommendation: one client; give it all the memory
Reading from 3388: heap size 1700 MB, throughput 0.991131
Reading from 3388: heap size 1716 MB, throughput 0.98234
Reading from 3388: heap size 1711 MB, throughput 0.969326
Reading from 3388: heap size 1739 MB, throughput 0.94634
Client 3388 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
