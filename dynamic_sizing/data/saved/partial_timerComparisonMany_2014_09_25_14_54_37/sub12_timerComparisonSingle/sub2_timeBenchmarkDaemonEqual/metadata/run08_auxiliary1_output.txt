economemd
    total memory: 7314 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub12_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
TimerThread: starting
ReadingThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 12828: heap size 9 MB, throughput 0.989261
Clients: 1
Client 12828 has a minimum heap size of 8 MB
Reading from 12827: heap size 9 MB, throughput 0.991073
Clients: 2
Client 12827 has a minimum heap size of 1211 MB
Reading from 12827: heap size 9 MB, throughput 0.976488
Reading from 12828: heap size 9 MB, throughput 0.968917
Reading from 12827: heap size 9 MB, throughput 0.957653
Reading from 12828: heap size 9 MB, throughput 0.961115
Reading from 12828: heap size 9 MB, throughput 0.846178
Reading from 12827: heap size 9 MB, throughput 0.93091
Reading from 12828: heap size 11 MB, throughput 0.948161
Reading from 12828: heap size 11 MB, throughput 0.964985
Reading from 12827: heap size 11 MB, throughput 0.961047
Reading from 12828: heap size 16 MB, throughput 0.971906
Reading from 12827: heap size 11 MB, throughput 0.92924
Reading from 12828: heap size 16 MB, throughput 0.959775
Reading from 12827: heap size 17 MB, throughput 0.921692
Reading from 12828: heap size 25 MB, throughput 0.745342
Reading from 12828: heap size 28 MB, throughput 0.95028
Reading from 12828: heap size 31 MB, throughput 0.963254
Reading from 12827: heap size 17 MB, throughput 0.353376
Reading from 12828: heap size 35 MB, throughput 0.959959
Reading from 12828: heap size 40 MB, throughput 0.971823
Reading from 12828: heap size 40 MB, throughput 0.970184
Reading from 12827: heap size 29 MB, throughput 0.96329
Reading from 12828: heap size 48 MB, throughput 0.981067
Reading from 12828: heap size 48 MB, throughput 0.971393
Reading from 12827: heap size 30 MB, throughput 0.936997
Reading from 12828: heap size 56 MB, throughput 0.956365
Reading from 12828: heap size 57 MB, throughput 0.986718
Reading from 12828: heap size 64 MB, throughput 0.987934
Reading from 12827: heap size 35 MB, throughput 0.576105
Reading from 12828: heap size 65 MB, throughput 0.985683
Reading from 12827: heap size 48 MB, throughput 0.908641
Reading from 12828: heap size 72 MB, throughput 0.988946
Reading from 12827: heap size 53 MB, throughput 0.898986
Reading from 12828: heap size 73 MB, throughput 0.989877
Reading from 12828: heap size 80 MB, throughput 0.990846
Reading from 12827: heap size 54 MB, throughput 0.227126
Reading from 12828: heap size 80 MB, throughput 0.991548
Reading from 12828: heap size 87 MB, throughput 0.991546
Reading from 12827: heap size 70 MB, throughput 0.310675
Reading from 12827: heap size 89 MB, throughput 0.69261
Reading from 12828: heap size 88 MB, throughput 0.991148
Reading from 12827: heap size 94 MB, throughput 0.88913
Reading from 12827: heap size 95 MB, throughput 0.730759
Reading from 12828: heap size 93 MB, throughput 0.993785
Reading from 12828: heap size 93 MB, throughput 0.993854
Reading from 12828: heap size 97 MB, throughput 0.989049
Reading from 12827: heap size 99 MB, throughput 0.207727
Reading from 12827: heap size 121 MB, throughput 0.685063
Reading from 12828: heap size 98 MB, throughput 0.993311
Reading from 12827: heap size 128 MB, throughput 0.797049
Reading from 12827: heap size 129 MB, throughput 0.739101
Reading from 12828: heap size 103 MB, throughput 0.995568
Reading from 12828: heap size 103 MB, throughput 0.994627
Reading from 12828: heap size 107 MB, throughput 0.993721
Reading from 12827: heap size 132 MB, throughput 0.145548
Reading from 12828: heap size 108 MB, throughput 0.974107
Reading from 12827: heap size 163 MB, throughput 0.622141
Reading from 12827: heap size 168 MB, throughput 0.680551
Reading from 12828: heap size 112 MB, throughput 0.996055
Reading from 12828: heap size 112 MB, throughput 0.994535
Reading from 12827: heap size 173 MB, throughput 0.16332
Reading from 12828: heap size 116 MB, throughput 0.99573
Reading from 12827: heap size 215 MB, throughput 0.661829
Reading from 12827: heap size 218 MB, throughput 0.68631
Reading from 12828: heap size 117 MB, throughput 0.993498
Reading from 12827: heap size 220 MB, throughput 0.692036
Reading from 12828: heap size 121 MB, throughput 0.995616
Reading from 12827: heap size 224 MB, throughput 0.654047
Reading from 12828: heap size 121 MB, throughput 0.993573
Reading from 12827: heap size 228 MB, throughput 0.569494
Reading from 12828: heap size 126 MB, throughput 0.992542
Reading from 12827: heap size 237 MB, throughput 0.591392
Reading from 12828: heap size 126 MB, throughput 0.993147
Reading from 12827: heap size 241 MB, throughput 0.462261
Reading from 12827: heap size 252 MB, throughput 0.512458
Reading from 12828: heap size 129 MB, throughput 0.996071
Reading from 12827: heap size 258 MB, throughput 0.617628
Reading from 12827: heap size 268 MB, throughput 0.458811
Reading from 12828: heap size 129 MB, throughput 0.996629
Reading from 12828: heap size 133 MB, throughput 0.991059
Reading from 12828: heap size 133 MB, throughput 0.993925
Reading from 12827: heap size 275 MB, throughput 0.0765983
Reading from 12827: heap size 319 MB, throughput 0.381186
Reading from 12828: heap size 137 MB, throughput 0.997714
Reading from 12828: heap size 137 MB, throughput 0.996789
Reading from 12828: heap size 140 MB, throughput 0.99463
Reading from 12827: heap size 311 MB, throughput 0.0932524
Reading from 12827: heap size 298 MB, throughput 0.504756
Reading from 12828: heap size 140 MB, throughput 0.99702
Reading from 12827: heap size 353 MB, throughput 0.572493
Reading from 12827: heap size 299 MB, throughput 0.587471
Reading from 12828: heap size 143 MB, throughput 0.995666
Reading from 12827: heap size 349 MB, throughput 0.550262
Reading from 12827: heap size 352 MB, throughput 0.552318
Reading from 12827: heap size 354 MB, throughput 0.551064
Reading from 12828: heap size 144 MB, throughput 0.996829
Reading from 12827: heap size 356 MB, throughput 0.520459
Reading from 12827: heap size 362 MB, throughput 0.523212
Reading from 12828: heap size 146 MB, throughput 0.996621
Reading from 12827: heap size 365 MB, throughput 0.572267
Reading from 12828: heap size 146 MB, throughput 0.995325
Reading from 12828: heap size 149 MB, throughput 0.996333
Reading from 12828: heap size 149 MB, throughput 0.995133
Reading from 12827: heap size 370 MB, throughput 0.0801758
Reading from 12828: heap size 152 MB, throughput 0.997189
Reading from 12827: heap size 418 MB, throughput 0.440057
Reading from 12828: heap size 152 MB, throughput 0.993724
Reading from 12827: heap size 425 MB, throughput 0.519348
Reading from 12828: heap size 155 MB, throughput 0.991071
Reading from 12827: heap size 425 MB, throughput 0.549552
Reading from 12827: heap size 428 MB, throughput 0.506383
Reading from 12828: heap size 155 MB, throughput 0.995028
Reading from 12827: heap size 433 MB, throughput 0.50883
Reading from 12828: heap size 159 MB, throughput 0.995427
Reading from 12827: heap size 436 MB, throughput 0.488351
Equal recommendation: 3657 MB each
Reading from 12827: heap size 444 MB, throughput 0.469676
Reading from 12828: heap size 159 MB, throughput 0.994267
Reading from 12827: heap size 451 MB, throughput 0.446271
Reading from 12828: heap size 163 MB, throughput 0.996752
Reading from 12828: heap size 163 MB, throughput 0.996549
Reading from 12828: heap size 167 MB, throughput 0.997481
Reading from 12827: heap size 457 MB, throughput 0.0579599
Reading from 12828: heap size 167 MB, throughput 0.997218
Reading from 12827: heap size 515 MB, throughput 0.346057
Reading from 12828: heap size 170 MB, throughput 0.995819
Reading from 12827: heap size 516 MB, throughput 0.398409
Reading from 12828: heap size 170 MB, throughput 0.995155
Reading from 12828: heap size 173 MB, throughput 0.993415
Reading from 12828: heap size 173 MB, throughput 0.996326
Reading from 12827: heap size 515 MB, throughput 0.0989705
Reading from 12828: heap size 177 MB, throughput 0.99249
Reading from 12827: heap size 574 MB, throughput 0.355969
Reading from 12827: heap size 567 MB, throughput 0.535419
Reading from 12828: heap size 177 MB, throughput 0.994721
Reading from 12827: heap size 571 MB, throughput 0.506489
Reading from 12828: heap size 182 MB, throughput 0.993258
Reading from 12827: heap size 566 MB, throughput 0.553798
Reading from 12828: heap size 182 MB, throughput 0.99636
Reading from 12828: heap size 186 MB, throughput 0.993758
Reading from 12828: heap size 186 MB, throughput 0.994744
Reading from 12827: heap size 569 MB, throughput 0.0971239
Reading from 12827: heap size 633 MB, throughput 0.520291
Reading from 12828: heap size 191 MB, throughput 0.997269
Reading from 12827: heap size 633 MB, throughput 0.602346
Reading from 12828: heap size 191 MB, throughput 0.995581
Reading from 12827: heap size 634 MB, throughput 0.594194
Reading from 12827: heap size 634 MB, throughput 0.546227
Reading from 12828: heap size 195 MB, throughput 0.997103
Reading from 12828: heap size 195 MB, throughput 0.994892
Reading from 12828: heap size 199 MB, throughput 0.994197
Reading from 12827: heap size 637 MB, throughput 0.828186
Reading from 12828: heap size 199 MB, throughput 0.994664
Reading from 12828: heap size 204 MB, throughput 0.995688
Reading from 12828: heap size 204 MB, throughput 0.994055
Reading from 12827: heap size 647 MB, throughput 0.818027
Reading from 12828: heap size 208 MB, throughput 0.988988
Reading from 12828: heap size 208 MB, throughput 0.99552
Reading from 12827: heap size 653 MB, throughput 0.849001
Reading from 12828: heap size 214 MB, throughput 0.997648
Reading from 12828: heap size 214 MB, throughput 0.997048
Reading from 12828: heap size 218 MB, throughput 0.997409
Reading from 12828: heap size 219 MB, throughput 0.996862
Reading from 12827: heap size 669 MB, throughput 0.120968
Equal recommendation: 3657 MB each
Reading from 12828: heap size 222 MB, throughput 0.997811
Reading from 12827: heap size 751 MB, throughput 0.484007
Reading from 12828: heap size 223 MB, throughput 0.996156
Reading from 12827: heap size 760 MB, throughput 0.316195
Reading from 12827: heap size 768 MB, throughput 0.312424
Reading from 12828: heap size 227 MB, throughput 0.993359
Reading from 12827: heap size 768 MB, throughput 0.325543
Reading from 12827: heap size 759 MB, throughput 0.361088
Reading from 12828: heap size 227 MB, throughput 0.996709
Reading from 12828: heap size 231 MB, throughput 0.99719
Reading from 12828: heap size 231 MB, throughput 0.996524
Reading from 12828: heap size 234 MB, throughput 0.996751
Reading from 12828: heap size 235 MB, throughput 0.995943
Reading from 12827: heap size 765 MB, throughput 0.151636
Reading from 12828: heap size 239 MB, throughput 0.998064
Reading from 12827: heap size 838 MB, throughput 0.52562
Reading from 12827: heap size 841 MB, throughput 0.506992
Reading from 12828: heap size 239 MB, throughput 0.970543
Reading from 12827: heap size 842 MB, throughput 0.536486
Reading from 12828: heap size 243 MB, throughput 0.995971
Reading from 12827: heap size 844 MB, throughput 0.789879
Reading from 12828: heap size 244 MB, throughput 0.996305
Reading from 12827: heap size 846 MB, throughput 0.705119
Reading from 12827: heap size 847 MB, throughput 0.797271
Reading from 12828: heap size 252 MB, throughput 0.996473
Reading from 12828: heap size 252 MB, throughput 0.996105
Reading from 12828: heap size 259 MB, throughput 0.99559
Reading from 12828: heap size 259 MB, throughput 0.997514
Reading from 12827: heap size 854 MB, throughput 0.198165
Reading from 12827: heap size 947 MB, throughput 0.555559
Reading from 12828: heap size 266 MB, throughput 0.996629
Reading from 12827: heap size 954 MB, throughput 0.898069
Reading from 12828: heap size 267 MB, throughput 0.996274
Reading from 12827: heap size 957 MB, throughput 0.809987
Reading from 12828: heap size 275 MB, throughput 0.996393
Reading from 12827: heap size 942 MB, throughput 0.786339
Reading from 12827: heap size 829 MB, throughput 0.791341
Reading from 12828: heap size 275 MB, throughput 0.995888
Reading from 12827: heap size 935 MB, throughput 0.796371
Reading from 12828: heap size 284 MB, throughput 0.996431
Reading from 12827: heap size 942 MB, throughput 0.786529
Reading from 12827: heap size 929 MB, throughput 0.799687
Reading from 12828: heap size 284 MB, throughput 0.997044
Reading from 12827: heap size 935 MB, throughput 0.826088
Reading from 12827: heap size 927 MB, throughput 0.841991
Reading from 12828: heap size 291 MB, throughput 0.996962
Reading from 12827: heap size 933 MB, throughput 0.825485
Reading from 12828: heap size 292 MB, throughput 0.997733
Reading from 12827: heap size 926 MB, throughput 0.954469
Equal recommendation: 3657 MB each
Reading from 12828: heap size 299 MB, throughput 0.997767
Reading from 12828: heap size 300 MB, throughput 0.997542
Reading from 12828: heap size 307 MB, throughput 0.997235
Reading from 12828: heap size 307 MB, throughput 0.998126
Reading from 12827: heap size 931 MB, throughput 0.964632
Reading from 12827: heap size 936 MB, throughput 0.729788
Reading from 12828: heap size 314 MB, throughput 0.998517
Reading from 12827: heap size 938 MB, throughput 0.748455
Reading from 12827: heap size 933 MB, throughput 0.759152
Reading from 12828: heap size 314 MB, throughput 0.998152
Reading from 12827: heap size 937 MB, throughput 0.783483
Reading from 12827: heap size 931 MB, throughput 0.762858
Reading from 12828: heap size 321 MB, throughput 0.998451
Reading from 12827: heap size 936 MB, throughput 0.744367
Reading from 12828: heap size 321 MB, throughput 0.990223
Reading from 12827: heap size 934 MB, throughput 0.809052
Reading from 12827: heap size 938 MB, throughput 0.914651
Reading from 12828: heap size 326 MB, throughput 0.996128
Reading from 12827: heap size 942 MB, throughput 0.873197
Reading from 12828: heap size 327 MB, throughput 0.996946
Reading from 12827: heap size 943 MB, throughput 0.862696
Reading from 12827: heap size 947 MB, throughput 0.69546
Reading from 12828: heap size 337 MB, throughput 0.99658
Reading from 12827: heap size 948 MB, throughput 0.602572
Reading from 12827: heap size 971 MB, throughput 0.58925
Reading from 12828: heap size 338 MB, throughput 0.996348
Reading from 12827: heap size 979 MB, throughput 0.630815
Reading from 12828: heap size 349 MB, throughput 0.997723
Reading from 12828: heap size 349 MB, throughput 0.995501
Reading from 12828: heap size 359 MB, throughput 0.995295
Reading from 12827: heap size 997 MB, throughput 0.0768011
Reading from 12828: heap size 360 MB, throughput 0.995416
Reading from 12827: heap size 1108 MB, throughput 0.474623
Reading from 12827: heap size 1107 MB, throughput 0.637144
Reading from 12827: heap size 1113 MB, throughput 0.667273
Reading from 12828: heap size 372 MB, throughput 0.99734
Reading from 12827: heap size 1119 MB, throughput 0.632927
Reading from 12827: heap size 1123 MB, throughput 0.66534
Reading from 12828: heap size 372 MB, throughput 0.996868
Reading from 12828: heap size 384 MB, throughput 0.998457
Equal recommendation: 3657 MB each
Reading from 12828: heap size 384 MB, throughput 0.997267
Reading from 12828: heap size 394 MB, throughput 0.996583
Reading from 12827: heap size 1136 MB, throughput 0.950287
Reading from 12828: heap size 395 MB, throughput 0.996298
Reading from 12828: heap size 406 MB, throughput 0.997956
Reading from 12828: heap size 406 MB, throughput 0.998456
Reading from 12827: heap size 1141 MB, throughput 0.946848
Reading from 12828: heap size 415 MB, throughput 0.997582
Reading from 12828: heap size 416 MB, throughput 0.996903
Reading from 12828: heap size 425 MB, throughput 0.997237
Reading from 12828: heap size 426 MB, throughput 0.997281
Reading from 12827: heap size 1158 MB, throughput 0.950997
Reading from 12828: heap size 436 MB, throughput 0.997395
Reading from 12828: heap size 436 MB, throughput 0.996656
Reading from 12828: heap size 447 MB, throughput 0.996853
Reading from 12827: heap size 1161 MB, throughput 0.925921
Reading from 12828: heap size 447 MB, throughput 0.997203
Reading from 12828: heap size 459 MB, throughput 0.997909
Reading from 12828: heap size 459 MB, throughput 0.99766
Reading from 12827: heap size 1166 MB, throughput 0.950409
Equal recommendation: 3657 MB each
Reading from 12828: heap size 470 MB, throughput 0.997545
Reading from 12828: heap size 471 MB, throughput 0.996791
Reading from 12828: heap size 482 MB, throughput 0.997832
Reading from 12827: heap size 1170 MB, throughput 0.934767
Reading from 12828: heap size 482 MB, throughput 0.997123
Reading from 12828: heap size 493 MB, throughput 0.997253
Reading from 12828: heap size 493 MB, throughput 0.997333
Reading from 12827: heap size 1166 MB, throughput 0.934689
Reading from 12828: heap size 506 MB, throughput 0.997351
Reading from 12828: heap size 506 MB, throughput 0.998083
Reading from 12828: heap size 517 MB, throughput 0.997674
Reading from 12827: heap size 1172 MB, throughput 0.938527
Reading from 12828: heap size 518 MB, throughput 0.997174
Reading from 12828: heap size 530 MB, throughput 0.997651
Reading from 12828: heap size 530 MB, throughput 0.997906
Reading from 12827: heap size 1179 MB, throughput 0.942685
Reading from 12828: heap size 541 MB, throughput 0.998218
Reading from 12828: heap size 542 MB, throughput 0.99729
Equal recommendation: 3657 MB each
Reading from 12828: heap size 553 MB, throughput 0.998477
Reading from 12827: heap size 1180 MB, throughput 0.939043
Reading from 12828: heap size 553 MB, throughput 0.998059
Reading from 12828: heap size 564 MB, throughput 0.997507
Reading from 12828: heap size 564 MB, throughput 0.997781
Reading from 12827: heap size 1190 MB, throughput 0.934319
Reading from 12828: heap size 575 MB, throughput 0.997948
Reading from 12828: heap size 576 MB, throughput 0.997802
Reading from 12828: heap size 588 MB, throughput 0.996388
Reading from 12827: heap size 1193 MB, throughput 0.941415
Reading from 12828: heap size 588 MB, throughput 0.998036
Reading from 12828: heap size 601 MB, throughput 0.997887
Reading from 12827: heap size 1203 MB, throughput 0.928841
Reading from 12828: heap size 601 MB, throughput 0.998117
Reading from 12828: heap size 613 MB, throughput 0.997962
Equal recommendation: 3657 MB each
Reading from 12828: heap size 614 MB, throughput 0.998213
Reading from 12827: heap size 1207 MB, throughput 0.938815
Reading from 12828: heap size 626 MB, throughput 0.998514
Reading from 12828: heap size 626 MB, throughput 0.997828
Reading from 12828: heap size 638 MB, throughput 0.998222
Reading from 12827: heap size 1216 MB, throughput 0.944006
Reading from 12828: heap size 638 MB, throughput 0.998049
Reading from 12828: heap size 650 MB, throughput 0.997679
Reading from 12827: heap size 1222 MB, throughput 0.931452
Reading from 12828: heap size 650 MB, throughput 0.998274
Reading from 12828: heap size 662 MB, throughput 0.998536
Reading from 12828: heap size 662 MB, throughput 0.998163
Reading from 12827: heap size 1233 MB, throughput 0.937919
Reading from 12828: heap size 674 MB, throughput 0.998257
Equal recommendation: 3657 MB each
Reading from 12828: heap size 674 MB, throughput 0.99826
Reading from 12828: heap size 686 MB, throughput 0.998118
Reading from 12827: heap size 1239 MB, throughput 0.9408
Reading from 12828: heap size 686 MB, throughput 0.998417
Reading from 12828: heap size 698 MB, throughput 0.998624
Reading from 12827: heap size 1250 MB, throughput 0.932181
Reading from 12828: heap size 698 MB, throughput 0.997965
Reading from 12828: heap size 710 MB, throughput 0.998137
Reading from 12828: heap size 710 MB, throughput 0.989063
Reading from 12827: heap size 1254 MB, throughput 0.953574
Reading from 12828: heap size 722 MB, throughput 0.998753
Reading from 12828: heap size 722 MB, throughput 0.998487
Reading from 12827: heap size 1264 MB, throughput 0.940281
Reading from 12828: heap size 742 MB, throughput 0.998403
Equal recommendation: 3657 MB each
Reading from 12828: heap size 744 MB, throughput 0.998294
Reading from 12828: heap size 761 MB, throughput 0.998624
Reading from 12827: heap size 1267 MB, throughput 0.938179
Reading from 12828: heap size 761 MB, throughput 0.998558
Reading from 12828: heap size 775 MB, throughput 0.998138
Reading from 12828: heap size 776 MB, throughput 0.9984
Reading from 12827: heap size 1276 MB, throughput 0.518513
Reading from 12828: heap size 790 MB, throughput 0.999197
Reading from 12828: heap size 790 MB, throughput 0.998309
Reading from 12828: heap size 802 MB, throughput 0.99873
Equal recommendation: 3657 MB each
Reading from 12827: heap size 1369 MB, throughput 0.930806
Reading from 12828: heap size 802 MB, throughput 0.998804
Reading from 12828: heap size 814 MB, throughput 0.998746
Reading from 12827: heap size 1376 MB, throughput 0.968087
Reading from 12828: heap size 814 MB, throughput 0.998477
Reading from 12828: heap size 825 MB, throughput 0.998758
Reading from 12827: heap size 1381 MB, throughput 0.967599
Reading from 12828: heap size 825 MB, throughput 0.998734
Reading from 12828: heap size 836 MB, throughput 0.99853
Reading from 12828: heap size 837 MB, throughput 0.998817
Reading from 12827: heap size 1389 MB, throughput 0.962663
Reading from 12828: heap size 848 MB, throughput 0.998413
Equal recommendation: 3657 MB each
Reading from 12828: heap size 848 MB, throughput 0.998779
Reading from 12827: heap size 1391 MB, throughput 0.966192
Reading from 12828: heap size 859 MB, throughput 0.998595
Reading from 12828: heap size 860 MB, throughput 0.998341
Reading from 12827: heap size 1385 MB, throughput 0.957261
Reading from 12828: heap size 871 MB, throughput 0.99896
Reading from 12828: heap size 871 MB, throughput 0.998672
Reading from 12828: heap size 883 MB, throughput 0.998933
Reading from 12827: heap size 1391 MB, throughput 0.957912
Reading from 12828: heap size 883 MB, throughput 0.998488
Equal recommendation: 3657 MB each
Reading from 12828: heap size 895 MB, throughput 0.998723
Reading from 12827: heap size 1379 MB, throughput 0.955932
Reading from 12828: heap size 895 MB, throughput 0.998784
Reading from 12828: heap size 906 MB, throughput 0.998591
Reading from 12828: heap size 907 MB, throughput 0.998666
Reading from 12827: heap size 1387 MB, throughput 0.95409
Reading from 12828: heap size 918 MB, throughput 0.998963
Reading from 12828: heap size 918 MB, throughput 0.998964
Reading from 12827: heap size 1390 MB, throughput 0.955452
Reading from 12828: heap size 929 MB, throughput 0.99857
Reading from 12828: heap size 930 MB, throughput 0.998774
Equal recommendation: 3657 MB each
Reading from 12827: heap size 1391 MB, throughput 0.957224
Reading from 12828: heap size 941 MB, throughput 0.99884
Reading from 12828: heap size 941 MB, throughput 0.998738
Reading from 12828: heap size 953 MB, throughput 0.998825
Reading from 12827: heap size 1399 MB, throughput 0.949852
Reading from 12828: heap size 953 MB, throughput 0.998699
Reading from 12828: heap size 965 MB, throughput 0.998945
Reading from 12827: heap size 1403 MB, throughput 0.944616
Reading from 12828: heap size 965 MB, throughput 0.998702
Reading from 12828: heap size 976 MB, throughput 0.998741
Equal recommendation: 3657 MB each
Reading from 12828: heap size 976 MB, throughput 0.998764
Reading from 12828: heap size 988 MB, throughput 0.999122
Reading from 12828: heap size 988 MB, throughput 0.998881
Reading from 12828: heap size 1000 MB, throughput 0.998977
Reading from 12828: heap size 1000 MB, throughput 0.99858
Reading from 12828: heap size 1011 MB, throughput 0.999124
Equal recommendation: 3657 MB each
Reading from 12828: heap size 1011 MB, throughput 0.998804
Reading from 12828: heap size 1023 MB, throughput 0.999126
Reading from 12828: heap size 1023 MB, throughput 0.998996
Reading from 12828: heap size 1034 MB, throughput 0.999159
Reading from 12828: heap size 1034 MB, throughput 0.999183
Reading from 12827: heap size 1413 MB, throughput 0.833463
Reading from 12828: heap size 1044 MB, throughput 0.999188
Equal recommendation: 3657 MB each
Reading from 12828: heap size 1044 MB, throughput 0.998865
Reading from 12828: heap size 1054 MB, throughput 0.999104
Reading from 12828: heap size 1054 MB, throughput 0.99889
Reading from 12828: heap size 1064 MB, throughput 0.999048
Reading from 12828: heap size 1064 MB, throughput 0.998774
Reading from 12828: heap size 1075 MB, throughput 0.999136
Equal recommendation: 3657 MB each
Reading from 12828: heap size 1075 MB, throughput 0.999038
Reading from 12828: heap size 1086 MB, throughput 0.998994
Reading from 12828: heap size 1086 MB, throughput 0.99881
Reading from 12827: heap size 1524 MB, throughput 0.925508
Reading from 12828: heap size 1097 MB, throughput 0.999106
Reading from 12828: heap size 1097 MB, throughput 0.999056
Reading from 12827: heap size 1509 MB, throughput 0.992022
Reading from 12828: heap size 1108 MB, throughput 0.99897
Reading from 12827: heap size 1517 MB, throughput 0.899339
Reading from 12827: heap size 1530 MB, throughput 0.879818
Reading from 12827: heap size 1533 MB, throughput 0.85299
Equal recommendation: 3657 MB each
Reading from 12827: heap size 1543 MB, throughput 0.880763
Reading from 12828: heap size 1108 MB, throughput 0.999071
Reading from 12827: heap size 1545 MB, throughput 0.981798
Reading from 12828: heap size 1119 MB, throughput 0.999077
Reading from 12828: heap size 1119 MB, throughput 0.999007
Reading from 12827: heap size 1560 MB, throughput 0.985306
Reading from 12828: heap size 1130 MB, throughput 0.998993
Reading from 12828: heap size 1130 MB, throughput 0.999179
Reading from 12827: heap size 1568 MB, throughput 0.984137
Reading from 12828: heap size 1141 MB, throughput 0.99879
Equal recommendation: 3657 MB each
Reading from 12828: heap size 1142 MB, throughput 0.999077
Reading from 12827: heap size 1585 MB, throughput 0.983897
Reading from 12828: heap size 1153 MB, throughput 0.999093
Reading from 12828: heap size 1153 MB, throughput 0.998997
Reading from 12828: heap size 1165 MB, throughput 0.999238
Reading from 12827: heap size 1589 MB, throughput 0.97743
Reading from 12828: heap size 1165 MB, throughput 0.99904
Reading from 12828: heap size 1176 MB, throughput 0.999125
Reading from 12827: heap size 1585 MB, throughput 0.979033
Equal recommendation: 3657 MB each
Reading from 12828: heap size 1176 MB, throughput 0.999043
Reading from 12828: heap size 1187 MB, throughput 0.99897
Reading from 12827: heap size 1594 MB, throughput 0.96895
Reading from 12828: heap size 1187 MB, throughput 0.998999
Reading from 12828: heap size 1198 MB, throughput 0.998792
Reading from 12827: heap size 1597 MB, throughput 0.966905
Reading from 12828: heap size 1198 MB, throughput 0.999284
Equal recommendation: 3657 MB each
Reading from 12828: heap size 1210 MB, throughput 0.999158
Reading from 12828: heap size 1211 MB, throughput 0.998978
Reading from 12827: heap size 1599 MB, throughput 0.968563
Reading from 12828: heap size 1221 MB, throughput 0.999204
Reading from 12828: heap size 1221 MB, throughput 0.999041
Reading from 12827: heap size 1611 MB, throughput 0.964602
Reading from 12828: heap size 1233 MB, throughput 0.999244
Reading from 12828: heap size 1233 MB, throughput 0.99905
Equal recommendation: 3657 MB each
Reading from 12827: heap size 1617 MB, throughput 0.960072
Reading from 12828: heap size 1244 MB, throughput 0.999259
Reading from 12828: heap size 1244 MB, throughput 0.998987
Reading from 12827: heap size 1631 MB, throughput 0.965997
Reading from 12828: heap size 1255 MB, throughput 0.999168
Reading from 12828: heap size 1255 MB, throughput 0.999158
Reading from 12828: heap size 1266 MB, throughput 0.999124
Reading from 12827: heap size 1643 MB, throughput 0.955882
Equal recommendation: 3657 MB each
Reading from 12828: heap size 1266 MB, throughput 0.999254
Reading from 12828: heap size 1277 MB, throughput 0.999172
Reading from 12827: heap size 1657 MB, throughput 0.948928
Reading from 12828: heap size 1278 MB, throughput 0.999024
Reading from 12828: heap size 1288 MB, throughput 0.999241
Reading from 12827: heap size 1673 MB, throughput 0.949844
Reading from 12828: heap size 1288 MB, throughput 0.999243
Equal recommendation: 3657 MB each
Reading from 12828: heap size 1299 MB, throughput 0.999028
Reading from 12827: heap size 1691 MB, throughput 0.950289
Reading from 12828: heap size 1300 MB, throughput 0.999267
Reading from 12828: heap size 1311 MB, throughput 0.999061
Reading from 12827: heap size 1709 MB, throughput 0.951793
Reading from 12828: heap size 1311 MB, throughput 0.999173
Reading from 12828: heap size 1322 MB, throughput 0.999145
Equal recommendation: 3657 MB each
Reading from 12827: heap size 1733 MB, throughput 0.95235
Reading from 12828: heap size 1322 MB, throughput 0.999247
Reading from 12828: heap size 1257 MB, throughput 0.998971
Reading from 12828: heap size 1196 MB, throughput 0.991506
Reading from 12827: heap size 1747 MB, throughput 0.954594
Reading from 12828: heap size 1139 MB, throughput 0.999327
Reading from 12828: heap size 1153 MB, throughput 0.999481
Reading from 12827: heap size 1771 MB, throughput 0.949157
Reading from 12828: heap size 1165 MB, throughput 0.999117
Equal recommendation: 3657 MB each
Reading from 12828: heap size 1181 MB, throughput 0.999251
Reading from 12828: heap size 1194 MB, throughput 0.999212
Reading from 12827: heap size 1780 MB, throughput 0.949684
Reading from 12828: heap size 1207 MB, throughput 0.999368
Reading from 12828: heap size 1219 MB, throughput 0.99903
Reading from 12827: heap size 1803 MB, throughput 0.951424
Reading from 12828: heap size 1234 MB, throughput 0.99902
Equal recommendation: 3657 MB each
Reading from 12828: heap size 1249 MB, throughput 0.999153
Reading from 12828: heap size 1265 MB, throughput 0.999244
Reading from 12827: heap size 1808 MB, throughput 0.945637
Reading from 12828: heap size 1278 MB, throughput 0.999101
Reading from 12828: heap size 1294 MB, throughput 0.999193
Reading from 12828: heap size 1310 MB, throughput 0.998952
Reading from 12827: heap size 1832 MB, throughput 0.72597
Equal recommendation: 3657 MB each
Reading from 12828: heap size 1318 MB, throughput 0.999103
Reading from 12828: heap size 1329 MB, throughput 0.998876
Reading from 12828: heap size 1329 MB, throughput 0.998688
Reading from 12828: heap size 1348 MB, throughput 0.998874
Reading from 12828: heap size 1348 MB, throughput 0.9989
Equal recommendation: 3657 MB each
Reading from 12828: heap size 1369 MB, throughput 0.998929
Reading from 12828: heap size 1369 MB, throughput 0.999096
Reading from 12828: heap size 1388 MB, throughput 0.998836
Reading from 12828: heap size 1389 MB, throughput 0.998692
Reading from 12828: heap size 1411 MB, throughput 0.99917
Equal recommendation: 3657 MB each
Reading from 12828: heap size 1411 MB, throughput 0.999054
Reading from 12828: heap size 1431 MB, throughput 0.998889
Client 12828 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 12827: heap size 1921 MB, throughput 0.99597
Reading from 12827: heap size 1918 MB, throughput 0.974586
Reading from 12827: heap size 1932 MB, throughput 0.860257
Reading from 12827: heap size 1956 MB, throughput 0.876531
Client 12827 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
