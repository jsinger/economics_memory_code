economemd
    total memory: 2438 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub6_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 9378: heap size 9 MB, throughput 0.991386
Clients: 1
Client 9378 has a minimum heap size of 8 MB
Reading from 9377: heap size 9 MB, throughput 0.991291
Clients: 2
Client 9377 has a minimum heap size of 1211 MB
Reading from 9377: heap size 9 MB, throughput 0.977634
Reading from 9378: heap size 9 MB, throughput 0.942713
Reading from 9377: heap size 9 MB, throughput 0.953374
Reading from 9378: heap size 9 MB, throughput 0.95143
Reading from 9377: heap size 9 MB, throughput 0.9464
Reading from 9378: heap size 9 MB, throughput 0.930613
Reading from 9378: heap size 11 MB, throughput 0.919523
Reading from 9378: heap size 11 MB, throughput 0.97317
Reading from 9377: heap size 11 MB, throughput 0.979956
Reading from 9378: heap size 16 MB, throughput 0.966695
Reading from 9377: heap size 11 MB, throughput 0.976315
Reading from 9378: heap size 16 MB, throughput 0.978088
Reading from 9377: heap size 17 MB, throughput 0.907667
Reading from 9378: heap size 24 MB, throughput 0.966298
Reading from 9378: heap size 25 MB, throughput 0.582839
Reading from 9377: heap size 17 MB, throughput 0.597002
Reading from 9378: heap size 36 MB, throughput 0.985805
Reading from 9378: heap size 36 MB, throughput 0.975722
Reading from 9377: heap size 29 MB, throughput 0.949947
Reading from 9378: heap size 42 MB, throughput 0.985349
Reading from 9377: heap size 31 MB, throughput 0.761097
Reading from 9378: heap size 42 MB, throughput 0.978214
Reading from 9378: heap size 49 MB, throughput 0.970622
Reading from 9378: heap size 49 MB, throughput 0.97262
Reading from 9378: heap size 56 MB, throughput 0.984137
Reading from 9377: heap size 34 MB, throughput 0.653716
Reading from 9378: heap size 56 MB, throughput 0.937688
Reading from 9377: heap size 46 MB, throughput 0.917478
Reading from 9378: heap size 63 MB, throughput 0.975558
Reading from 9378: heap size 63 MB, throughput 0.989417
Reading from 9377: heap size 48 MB, throughput 0.410942
Reading from 9377: heap size 63 MB, throughput 0.840756
Reading from 9378: heap size 69 MB, throughput 0.993142
Reading from 9377: heap size 67 MB, throughput 0.896593
Reading from 9378: heap size 70 MB, throughput 0.985036
Reading from 9378: heap size 77 MB, throughput 0.989158
Reading from 9378: heap size 77 MB, throughput 0.987511
Reading from 9377: heap size 69 MB, throughput 0.182945
Reading from 9378: heap size 84 MB, throughput 0.991793
Reading from 9377: heap size 97 MB, throughput 0.774416
Reading from 9378: heap size 84 MB, throughput 0.987022
Reading from 9378: heap size 90 MB, throughput 0.992369
Reading from 9377: heap size 98 MB, throughput 0.196417
Reading from 9377: heap size 128 MB, throughput 0.921352
Reading from 9378: heap size 90 MB, throughput 0.991273
Reading from 9377: heap size 128 MB, throughput 0.660974
Reading from 9378: heap size 94 MB, throughput 0.991427
Reading from 9377: heap size 129 MB, throughput 0.783119
Reading from 9377: heap size 132 MB, throughput 0.753175
Reading from 9378: heap size 94 MB, throughput 0.973007
Reading from 9378: heap size 99 MB, throughput 0.993079
Reading from 9378: heap size 99 MB, throughput 0.989585
Reading from 9377: heap size 135 MB, throughput 0.163615
Reading from 9377: heap size 171 MB, throughput 0.689185
Reading from 9378: heap size 105 MB, throughput 0.996709
Reading from 9377: heap size 177 MB, throughput 0.74934
Reading from 9378: heap size 105 MB, throughput 0.992183
Reading from 9377: heap size 179 MB, throughput 0.645097
Reading from 9377: heap size 186 MB, throughput 0.723387
Reading from 9378: heap size 110 MB, throughput 0.993703
Reading from 9378: heap size 110 MB, throughput 0.993304
Reading from 9378: heap size 114 MB, throughput 0.995529
Reading from 9377: heap size 188 MB, throughput 0.175839
Reading from 9378: heap size 114 MB, throughput 0.989288
Reading from 9377: heap size 238 MB, throughput 0.652193
Reading from 9377: heap size 240 MB, throughput 0.656928
Reading from 9378: heap size 118 MB, throughput 0.994865
Reading from 9377: heap size 243 MB, throughput 0.625924
Reading from 9378: heap size 118 MB, throughput 0.990916
Reading from 9378: heap size 122 MB, throughput 0.993051
Reading from 9377: heap size 250 MB, throughput 0.584867
Reading from 9378: heap size 122 MB, throughput 0.991336
Reading from 9377: heap size 260 MB, throughput 0.651527
Reading from 9378: heap size 125 MB, throughput 0.991982
Reading from 9377: heap size 264 MB, throughput 0.529107
Reading from 9377: heap size 268 MB, throughput 0.541453
Reading from 9378: heap size 125 MB, throughput 0.994213
Reading from 9378: heap size 128 MB, throughput 0.991912
Reading from 9378: heap size 129 MB, throughput 0.994801
Reading from 9378: heap size 132 MB, throughput 0.996774
Reading from 9377: heap size 277 MB, throughput 0.115475
Reading from 9377: heap size 332 MB, throughput 0.502493
Reading from 9378: heap size 132 MB, throughput 0.995436
Reading from 9377: heap size 338 MB, throughput 0.535024
Reading from 9377: heap size 341 MB, throughput 0.563762
Reading from 9378: heap size 135 MB, throughput 0.995545
Reading from 9378: heap size 135 MB, throughput 0.992165
Reading from 9378: heap size 138 MB, throughput 0.995105
Reading from 9377: heap size 346 MB, throughput 0.136816
Reading from 9378: heap size 138 MB, throughput 0.996791
Reading from 9377: heap size 405 MB, throughput 0.466711
Reading from 9377: heap size 408 MB, throughput 0.608165
Reading from 9378: heap size 141 MB, throughput 0.98502
Reading from 9377: heap size 407 MB, throughput 0.609072
Reading from 9377: heap size 407 MB, throughput 0.501766
Reading from 9378: heap size 141 MB, throughput 0.997112
Reading from 9377: heap size 414 MB, throughput 0.607188
Reading from 9378: heap size 144 MB, throughput 0.995445
Reading from 9377: heap size 417 MB, throughput 0.609234
Reading from 9377: heap size 422 MB, throughput 0.512411
Reading from 9378: heap size 145 MB, throughput 0.996308
Reading from 9378: heap size 148 MB, throughput 0.993889
Reading from 9378: heap size 148 MB, throughput 0.996405
Reading from 9378: heap size 151 MB, throughput 0.991578
Reading from 9378: heap size 151 MB, throughput 0.996677
Reading from 9377: heap size 429 MB, throughput 0.0994522
Reading from 9378: heap size 154 MB, throughput 0.996391
Reading from 9377: heap size 486 MB, throughput 0.408142
Reading from 9378: heap size 154 MB, throughput 0.993826
Reading from 9377: heap size 492 MB, throughput 0.505087
Reading from 9377: heap size 498 MB, throughput 0.483748
Reading from 9378: heap size 157 MB, throughput 0.996478
Equal recommendation: 1219 MB each
Reading from 9378: heap size 157 MB, throughput 0.994035
Reading from 9378: heap size 160 MB, throughput 0.994515
Reading from 9378: heap size 160 MB, throughput 0.995005
Reading from 9377: heap size 499 MB, throughput 0.0998308
Reading from 9378: heap size 163 MB, throughput 0.994248
Reading from 9377: heap size 565 MB, throughput 0.461423
Reading from 9378: heap size 163 MB, throughput 0.996604
Reading from 9377: heap size 568 MB, throughput 0.659892
Reading from 9378: heap size 166 MB, throughput 0.99138
Reading from 9377: heap size 568 MB, throughput 0.581991
Reading from 9377: heap size 569 MB, throughput 0.591897
Reading from 9378: heap size 167 MB, throughput 0.994245
Reading from 9377: heap size 575 MB, throughput 0.658493
Reading from 9378: heap size 169 MB, throughput 0.99457
Reading from 9378: heap size 170 MB, throughput 0.995354
Reading from 9378: heap size 173 MB, throughput 0.993359
Reading from 9378: heap size 174 MB, throughput 0.994118
Reading from 9377: heap size 584 MB, throughput 0.106684
Reading from 9378: heap size 178 MB, throughput 0.996891
Reading from 9377: heap size 658 MB, throughput 0.418584
Reading from 9378: heap size 178 MB, throughput 0.994008
Reading from 9377: heap size 668 MB, throughput 0.543292
Reading from 9378: heap size 181 MB, throughput 0.996072
Reading from 9377: heap size 669 MB, throughput 0.651447
Reading from 9378: heap size 181 MB, throughput 0.996303
Reading from 9378: heap size 185 MB, throughput 0.994897
Reading from 9378: heap size 185 MB, throughput 0.996293
Reading from 9377: heap size 675 MB, throughput 0.813459
Reading from 9378: heap size 188 MB, throughput 0.995848
Reading from 9378: heap size 188 MB, throughput 0.994627
Reading from 9378: heap size 192 MB, throughput 0.993785
Reading from 9377: heap size 674 MB, throughput 0.804158
Reading from 9378: heap size 192 MB, throughput 0.989845
Reading from 9378: heap size 196 MB, throughput 0.991306
Reading from 9377: heap size 693 MB, throughput 0.738931
Reading from 9377: heap size 701 MB, throughput 0.363206
Reading from 9378: heap size 196 MB, throughput 0.98965
Reading from 9377: heap size 722 MB, throughput 0.450709
Reading from 9378: heap size 201 MB, throughput 0.996402
Reading from 9378: heap size 201 MB, throughput 0.996234
Reading from 9378: heap size 205 MB, throughput 0.996594
Reading from 9378: heap size 205 MB, throughput 0.996625
Reading from 9377: heap size 739 MB, throughput 0.0246321
Reading from 9377: heap size 818 MB, throughput 0.15252
Reading from 9378: heap size 209 MB, throughput 0.988829
Reading from 9377: heap size 814 MB, throughput 0.317981
Reading from 9378: heap size 209 MB, throughput 0.995794
Equal recommendation: 1219 MB each
Reading from 9378: heap size 214 MB, throughput 0.99712
Reading from 9377: heap size 667 MB, throughput 0.0341011
Reading from 9377: heap size 900 MB, throughput 0.236127
Reading from 9378: heap size 214 MB, throughput 0.997261
Reading from 9377: heap size 903 MB, throughput 0.786421
Reading from 9378: heap size 218 MB, throughput 0.996374
Reading from 9377: heap size 908 MB, throughput 0.876456
Reading from 9377: heap size 909 MB, throughput 0.564367
Reading from 9378: heap size 218 MB, throughput 0.996623
Reading from 9377: heap size 899 MB, throughput 0.608349
Reading from 9377: heap size 725 MB, throughput 0.555809
Reading from 9378: heap size 221 MB, throughput 0.99643
Reading from 9377: heap size 892 MB, throughput 0.870458
Reading from 9378: heap size 222 MB, throughput 0.997249
Reading from 9378: heap size 225 MB, throughput 0.998099
Reading from 9378: heap size 225 MB, throughput 0.997045
Reading from 9378: heap size 228 MB, throughput 0.997952
Reading from 9377: heap size 898 MB, throughput 0.0612216
Reading from 9377: heap size 997 MB, throughput 0.736634
Reading from 9377: heap size 997 MB, throughput 0.821847
Reading from 9378: heap size 228 MB, throughput 0.974154
Reading from 9378: heap size 233 MB, throughput 0.996732
Reading from 9377: heap size 998 MB, throughput 0.933411
Reading from 9377: heap size 786 MB, throughput 0.920682
Reading from 9378: heap size 234 MB, throughput 0.996299
Reading from 9377: heap size 987 MB, throughput 0.913156
Reading from 9378: heap size 240 MB, throughput 0.996306
Reading from 9377: heap size 833 MB, throughput 0.744605
Reading from 9377: heap size 958 MB, throughput 0.693335
Reading from 9378: heap size 240 MB, throughput 0.996711
Reading from 9377: heap size 838 MB, throughput 0.752126
Reading from 9378: heap size 245 MB, throughput 0.994354
Reading from 9377: heap size 943 MB, throughput 0.779847
Reading from 9377: heap size 842 MB, throughput 0.792822
Reading from 9377: heap size 932 MB, throughput 0.811009
Reading from 9378: heap size 245 MB, throughput 0.997118
Reading from 9377: heap size 848 MB, throughput 0.79343
Reading from 9378: heap size 251 MB, throughput 0.996557
Reading from 9377: heap size 925 MB, throughput 0.810566
Reading from 9377: heap size 854 MB, throughput 0.816416
Reading from 9378: heap size 252 MB, throughput 0.996281
Reading from 9377: heap size 920 MB, throughput 0.80189
Reading from 9377: heap size 926 MB, throughput 0.817975
Reading from 9378: heap size 258 MB, throughput 0.996461
Reading from 9378: heap size 258 MB, throughput 0.996243
Reading from 9378: heap size 265 MB, throughput 0.996546
Reading from 9378: heap size 265 MB, throughput 0.996816
Reading from 9378: heap size 272 MB, throughput 0.998377
Reading from 9377: heap size 918 MB, throughput 0.97218
Reading from 9378: heap size 272 MB, throughput 0.997095
Reading from 9377: heap size 923 MB, throughput 0.857795
Reading from 9377: heap size 923 MB, throughput 0.747058
Reading from 9378: heap size 278 MB, throughput 0.995809
Reading from 9377: heap size 925 MB, throughput 0.756916
Reading from 9377: heap size 933 MB, throughput 0.754155
Reading from 9377: heap size 934 MB, throughput 0.714444
Reading from 9378: heap size 278 MB, throughput 0.997266
Equal recommendation: 1219 MB each
Reading from 9377: heap size 942 MB, throughput 0.748868
Reading from 9377: heap size 942 MB, throughput 0.750225
Reading from 9378: heap size 284 MB, throughput 0.998357
Reading from 9377: heap size 950 MB, throughput 0.753633
Reading from 9378: heap size 284 MB, throughput 0.997415
Reading from 9377: heap size 951 MB, throughput 0.852238
Reading from 9378: heap size 290 MB, throughput 0.997718
Reading from 9377: heap size 956 MB, throughput 0.828602
Reading from 9378: heap size 290 MB, throughput 0.996602
Reading from 9377: heap size 958 MB, throughput 0.86135
Reading from 9377: heap size 961 MB, throughput 0.667309
Reading from 9378: heap size 296 MB, throughput 0.996132
Reading from 9377: heap size 963 MB, throughput 0.566654
Reading from 9377: heap size 973 MB, throughput 0.568329
Reading from 9377: heap size 989 MB, throughput 0.634639
Reading from 9378: heap size 296 MB, throughput 0.998204
Reading from 9377: heap size 994 MB, throughput 0.641957
Reading from 9377: heap size 996 MB, throughput 0.649891
Reading from 9378: heap size 302 MB, throughput 0.997708
Reading from 9377: heap size 1009 MB, throughput 0.632013
Reading from 9377: heap size 1009 MB, throughput 0.637361
Reading from 9378: heap size 303 MB, throughput 0.996309
Reading from 9377: heap size 1025 MB, throughput 0.611324
Reading from 9378: heap size 307 MB, throughput 0.99764
Reading from 9377: heap size 1026 MB, throughput 0.610861
Reading from 9377: heap size 1043 MB, throughput 0.646537
Reading from 9378: heap size 308 MB, throughput 0.995183
Reading from 9378: heap size 315 MB, throughput 0.996773
Reading from 9378: heap size 315 MB, throughput 0.997056
Reading from 9378: heap size 323 MB, throughput 0.998427
Reading from 9377: heap size 1046 MB, throughput 0.95032
Reading from 9378: heap size 323 MB, throughput 0.997211
Reading from 9378: heap size 330 MB, throughput 0.996634
Reading from 9378: heap size 330 MB, throughput 0.996896
Reading from 9378: heap size 337 MB, throughput 0.99702
Reading from 9377: heap size 1065 MB, throughput 0.932739
Reading from 9378: heap size 337 MB, throughput 0.997404
Reading from 9378: heap size 345 MB, throughput 0.998241
Reading from 9378: heap size 345 MB, throughput 0.997811
Equal recommendation: 1219 MB each
Reading from 9377: heap size 1067 MB, throughput 0.940294
Reading from 9378: heap size 351 MB, throughput 0.997832
Reading from 9378: heap size 351 MB, throughput 0.997153
Reading from 9378: heap size 358 MB, throughput 0.997258
Reading from 9378: heap size 358 MB, throughput 0.997796
Reading from 9377: heap size 1074 MB, throughput 0.934682
Reading from 9378: heap size 364 MB, throughput 0.998375
Reading from 9378: heap size 364 MB, throughput 0.997068
Reading from 9378: heap size 371 MB, throughput 0.997564
Reading from 9378: heap size 371 MB, throughput 0.995793
Reading from 9377: heap size 1078 MB, throughput 0.933293
Reading from 9378: heap size 378 MB, throughput 0.997397
Reading from 9378: heap size 378 MB, throughput 0.996192
Reading from 9378: heap size 386 MB, throughput 0.997237
Reading from 9377: heap size 1075 MB, throughput 0.933598
Reading from 9378: heap size 387 MB, throughput 0.996092
Reading from 9378: heap size 395 MB, throughput 0.996717
Reading from 9378: heap size 395 MB, throughput 0.995577
Reading from 9378: heap size 405 MB, throughput 0.995714
Reading from 9378: heap size 405 MB, throughput 0.997258
Equal recommendation: 1219 MB each
Reading from 9378: heap size 415 MB, throughput 0.998296
Reading from 9377: heap size 1081 MB, throughput 0.45374
Reading from 9378: heap size 415 MB, throughput 0.99739
Reading from 9378: heap size 425 MB, throughput 0.996598
Reading from 9378: heap size 425 MB, throughput 0.996786
Reading from 9378: heap size 435 MB, throughput 0.996795
Reading from 9377: heap size 1168 MB, throughput 0.905224
Reading from 9378: heap size 435 MB, throughput 0.996634
Reading from 9378: heap size 445 MB, throughput 0.9969
Reading from 9378: heap size 445 MB, throughput 0.997544
Reading from 9377: heap size 1172 MB, throughput 0.966898
Reading from 9378: heap size 454 MB, throughput 0.997361
Reading from 9378: heap size 455 MB, throughput 0.99766
Reading from 9378: heap size 463 MB, throughput 0.997507
Reading from 9377: heap size 1185 MB, throughput 0.963118
Reading from 9378: heap size 464 MB, throughput 0.997151
Reading from 9378: heap size 473 MB, throughput 0.998019
Reading from 9378: heap size 473 MB, throughput 0.997531
Reading from 9377: heap size 1186 MB, throughput 0.96053
Reading from 9378: heap size 482 MB, throughput 0.998097
Equal recommendation: 1219 MB each
Reading from 9378: heap size 482 MB, throughput 0.997252
Reading from 9378: heap size 491 MB, throughput 0.997649
Reading from 9377: heap size 1185 MB, throughput 0.964429
Reading from 9378: heap size 491 MB, throughput 0.997894
Reading from 9378: heap size 499 MB, throughput 0.997713
Reading from 9378: heap size 500 MB, throughput 0.996984
Reading from 9377: heap size 1189 MB, throughput 0.953778
Reading from 9378: heap size 509 MB, throughput 0.998331
Reading from 9378: heap size 509 MB, throughput 0.997465
Reading from 9377: heap size 1183 MB, throughput 0.962918
Reading from 9378: heap size 518 MB, throughput 0.997882
Reading from 9378: heap size 518 MB, throughput 0.997195
Reading from 9378: heap size 527 MB, throughput 0.99747
Reading from 9377: heap size 1188 MB, throughput 0.948793
Reading from 9378: heap size 527 MB, throughput 0.997818
Reading from 9378: heap size 537 MB, throughput 0.998403
Equal recommendation: 1219 MB each
Reading from 9378: heap size 537 MB, throughput 0.997062
Reading from 9377: heap size 1192 MB, throughput 0.947596
Reading from 9378: heap size 546 MB, throughput 0.997597
Reading from 9378: heap size 546 MB, throughput 0.998258
Reading from 9378: heap size 555 MB, throughput 0.997827
Reading from 9377: heap size 1193 MB, throughput 0.941882
Reading from 9378: heap size 556 MB, throughput 0.997526
Reading from 9378: heap size 565 MB, throughput 0.998017
Reading from 9378: heap size 565 MB, throughput 0.997907
Reading from 9377: heap size 1199 MB, throughput 0.94972
Reading from 9378: heap size 574 MB, throughput 0.997829
Reading from 9378: heap size 575 MB, throughput 0.998151
Reading from 9378: heap size 584 MB, throughput 0.997409
Reading from 9377: heap size 1203 MB, throughput 0.948574
Reading from 9378: heap size 584 MB, throughput 0.998454
Reading from 9378: heap size 593 MB, throughput 0.998074
Equal recommendation: 1219 MB each
Reading from 9377: heap size 1210 MB, throughput 0.940504
Reading from 9378: heap size 594 MB, throughput 0.997701
Reading from 9378: heap size 603 MB, throughput 0.99813
Reading from 9378: heap size 603 MB, throughput 0.997818
Reading from 9377: heap size 1217 MB, throughput 0.942354
Reading from 9378: heap size 613 MB, throughput 0.998045
Reading from 9378: heap size 613 MB, throughput 0.998041
Reading from 9378: heap size 623 MB, throughput 0.997214
Reading from 9377: heap size 1224 MB, throughput 0.947046
Reading from 9378: heap size 623 MB, throughput 0.998033
Reading from 9378: heap size 633 MB, throughput 0.998686
Reading from 9377: heap size 1229 MB, throughput 0.933745
Reading from 9378: heap size 634 MB, throughput 0.998439
Reading from 9378: heap size 643 MB, throughput 0.998043
Equal recommendation: 1219 MB each
Reading from 9377: heap size 1231 MB, throughput 0.935743
Reading from 9378: heap size 643 MB, throughput 0.997578
Reading from 9378: heap size 653 MB, throughput 0.998542
Reading from 9378: heap size 653 MB, throughput 0.998138
Reading from 9377: heap size 1235 MB, throughput 0.93266
Reading from 9378: heap size 662 MB, throughput 0.998578
Reading from 9378: heap size 663 MB, throughput 0.998539
Reading from 9377: heap size 1236 MB, throughput 0.941933
Reading from 9378: heap size 672 MB, throughput 0.998439
Reading from 9378: heap size 672 MB, throughput 0.99831
Reading from 9378: heap size 681 MB, throughput 0.998102
Reading from 9377: heap size 1238 MB, throughput 0.935919
Reading from 9378: heap size 681 MB, throughput 0.998739
Reading from 9378: heap size 690 MB, throughput 0.998422
Reading from 9377: heap size 1239 MB, throughput 0.934057
Equal recommendation: 1219 MB each
Reading from 9378: heap size 690 MB, throughput 0.998837
Reading from 9378: heap size 699 MB, throughput 0.998399
Reading from 9377: heap size 1187 MB, throughput 0.929119
Reading from 9378: heap size 699 MB, throughput 0.998596
Reading from 9378: heap size 707 MB, throughput 0.998675
Reading from 9377: heap size 1239 MB, throughput 0.929204
Reading from 9378: heap size 707 MB, throughput 0.998206
Reading from 9378: heap size 716 MB, throughput 0.998644
Reading from 9378: heap size 716 MB, throughput 0.998621
Reading from 9378: heap size 724 MB, throughput 0.997035
Reading from 9377: heap size 1182 MB, throughput 0.477185
Reading from 9378: heap size 724 MB, throughput 0.998706
Equal recommendation: 1219 MB each
Reading from 9378: heap size 734 MB, throughput 0.998418
Reading from 9377: heap size 1338 MB, throughput 0.900772
Reading from 9378: heap size 735 MB, throughput 0.998516
Reading from 9378: heap size 744 MB, throughput 0.998746
Reading from 9377: heap size 1334 MB, throughput 0.959298
Reading from 9378: heap size 744 MB, throughput 0.998485
Reading from 9378: heap size 754 MB, throughput 0.998375
Reading from 9377: heap size 1333 MB, throughput 0.965064
Reading from 9378: heap size 754 MB, throughput 0.998866
Reading from 9378: heap size 763 MB, throughput 0.998677
Reading from 9377: heap size 1209 MB, throughput 0.953623
Reading from 9378: heap size 763 MB, throughput 0.998379
Equal recommendation: 1219 MB each
Reading from 9378: heap size 772 MB, throughput 0.9987
Reading from 9377: heap size 1333 MB, throughput 0.954075
Reading from 9378: heap size 772 MB, throughput 0.998659
Reading from 9378: heap size 781 MB, throughput 0.998914
Reading from 9377: heap size 1217 MB, throughput 0.954028
Reading from 9378: heap size 781 MB, throughput 0.998754
Reading from 9378: heap size 789 MB, throughput 0.998306
Reading from 9377: heap size 1325 MB, throughput 0.950025
Reading from 9378: heap size 789 MB, throughput 0.998642
Reading from 9378: heap size 798 MB, throughput 0.999013
Reading from 9377: heap size 1228 MB, throughput 0.951585
Reading from 9378: heap size 798 MB, throughput 0.998436
Equal recommendation: 1219 MB each
Reading from 9378: heap size 807 MB, throughput 0.998988
Reading from 9377: heap size 1296 MB, throughput 0.943886
Reading from 9378: heap size 807 MB, throughput 0.998505
Reading from 9378: heap size 815 MB, throughput 0.998858
Reading from 9377: heap size 1231 MB, throughput 0.945463
Reading from 9378: heap size 815 MB, throughput 0.998515
Reading from 9378: heap size 822 MB, throughput 0.998735
Reading from 9377: heap size 1280 MB, throughput 0.940453
Reading from 9378: heap size 822 MB, throughput 0.999234
Reading from 9378: heap size 822 MB, throughput 0.99884
Reading from 9378: heap size 822 MB, throughput 0.99906
Equal recommendation: 1219 MB each
Reading from 9378: heap size 822 MB, throughput 0.998897
Reading from 9378: heap size 822 MB, throughput 0.999095
Reading from 9378: heap size 822 MB, throughput 0.999075
Reading from 9378: heap size 822 MB, throughput 0.998848
Reading from 9378: heap size 822 MB, throughput 0.999165
Reading from 9378: heap size 822 MB, throughput 0.999234
Reading from 9378: heap size 822 MB, throughput 0.99893
Reading from 9378: heap size 822 MB, throughput 0.998967
Reading from 9377: heap size 1235 MB, throughput 0.991201
Equal recommendation: 1219 MB each
Reading from 9378: heap size 822 MB, throughput 0.999001
Reading from 9378: heap size 822 MB, throughput 0.999198
Reading from 9378: heap size 822 MB, throughput 0.99905
Reading from 9378: heap size 822 MB, throughput 0.998456
Reading from 9378: heap size 822 MB, throughput 0.999101
Reading from 9378: heap size 822 MB, throughput 0.999019
Reading from 9378: heap size 822 MB, throughput 0.999067
Reading from 9378: heap size 822 MB, throughput 0.999035
Equal recommendation: 1219 MB each
Reading from 9378: heap size 822 MB, throughput 0.999041
Reading from 9378: heap size 822 MB, throughput 0.999072
Reading from 9378: heap size 822 MB, throughput 0.998859
Reading from 9377: heap size 1275 MB, throughput 0.991377
Reading from 9378: heap size 822 MB, throughput 0.998979
Reading from 9378: heap size 822 MB, throughput 0.999152
Reading from 9378: heap size 822 MB, throughput 0.998933
Reading from 9378: heap size 822 MB, throughput 0.998714
Reading from 9377: heap size 1280 MB, throughput 0.703186
Reading from 9378: heap size 822 MB, throughput 0.999078
Equal recommendation: 1219 MB each
Reading from 9378: heap size 822 MB, throughput 0.999245
Reading from 9378: heap size 822 MB, throughput 0.999186
Reading from 9377: heap size 1321 MB, throughput 0.989896
Reading from 9377: heap size 1370 MB, throughput 0.95788
Reading from 9377: heap size 1372 MB, throughput 0.828528
Reading from 9377: heap size 1256 MB, throughput 0.748908
Reading from 9378: heap size 822 MB, throughput 0.998794
Reading from 9377: heap size 1342 MB, throughput 0.734231
Reading from 9377: heap size 1253 MB, throughput 0.734544
Reading from 9377: heap size 1325 MB, throughput 0.702759
Reading from 9377: heap size 1251 MB, throughput 0.766684
Reading from 9377: heap size 1311 MB, throughput 0.751721
Reading from 9377: heap size 1249 MB, throughput 0.694012
Reading from 9377: heap size 1299 MB, throughput 0.687652
Reading from 9378: heap size 822 MB, throughput 0.999085
Reading from 9377: heap size 1247 MB, throughput 0.874914
Reading from 9377: heap size 1292 MB, throughput 0.964222
Reading from 9378: heap size 822 MB, throughput 0.998925
Reading from 9378: heap size 822 MB, throughput 0.999068
Reading from 9377: heap size 1298 MB, throughput 0.970211
Reading from 9378: heap size 822 MB, throughput 0.999037
Reading from 9377: heap size 1302 MB, throughput 0.969786
Equal recommendation: 1219 MB each
Reading from 9378: heap size 822 MB, throughput 0.998969
Reading from 9377: heap size 1162 MB, throughput 0.963374
Reading from 9378: heap size 822 MB, throughput 0.998978
Reading from 9377: heap size 1302 MB, throughput 0.965957
Reading from 9378: heap size 822 MB, throughput 0.998771
Reading from 9377: heap size 1159 MB, throughput 0.962529
Reading from 9378: heap size 822 MB, throughput 0.99905
Reading from 9377: heap size 1301 MB, throughput 0.959881
Reading from 9378: heap size 822 MB, throughput 0.99915
Reading from 9378: heap size 822 MB, throughput 0.998825
Reading from 9377: heap size 1173 MB, throughput 0.955959
Reading from 9378: heap size 822 MB, throughput 0.999156
Reading from 9377: heap size 1292 MB, throughput 0.952601
Reading from 9378: heap size 822 MB, throughput 0.998919
Equal recommendation: 1219 MB each
Reading from 9377: heap size 1189 MB, throughput 0.950469
Reading from 9378: heap size 822 MB, throughput 0.999193
Reading from 9378: heap size 822 MB, throughput 0.999143
Reading from 9377: heap size 1283 MB, throughput 0.957212
Reading from 9378: heap size 822 MB, throughput 0.99886
Reading from 9377: heap size 1206 MB, throughput 0.947317
Reading from 9378: heap size 822 MB, throughput 0.999174
Reading from 9377: heap size 1277 MB, throughput 0.947821
Reading from 9378: heap size 822 MB, throughput 0.999001
Reading from 9378: heap size 822 MB, throughput 0.999094
Reading from 9377: heap size 1219 MB, throughput 0.940689
Reading from 9378: heap size 822 MB, throughput 0.999008
Reading from 9377: heap size 1271 MB, throughput 0.938681
Reading from 9378: heap size 822 MB, throughput 0.999096
Equal recommendation: 1219 MB each
Reading from 9377: heap size 1228 MB, throughput 0.93661
Reading from 9378: heap size 822 MB, throughput 0.999137
Reading from 9378: heap size 822 MB, throughput 0.998663
Reading from 9377: heap size 1268 MB, throughput 0.930239
Reading from 9378: heap size 822 MB, throughput 0.999103
Reading from 9377: heap size 1228 MB, throughput 0.928651
Reading from 9378: heap size 822 MB, throughput 0.999252
Reading from 9377: heap size 1267 MB, throughput 0.930111
Reading from 9378: heap size 822 MB, throughput 0.998778
Reading from 9377: heap size 1227 MB, throughput 0.932826
Reading from 9378: heap size 822 MB, throughput 0.999087
Reading from 9378: heap size 822 MB, throughput 0.998844
Reading from 9377: heap size 1264 MB, throughput 0.929862
Reading from 9378: heap size 822 MB, throughput 0.999134
Equal recommendation: 1219 MB each
Reading from 9377: heap size 1225 MB, throughput 0.939832
Reading from 9378: heap size 822 MB, throughput 0.999161
Reading from 9377: heap size 1260 MB, throughput 0.929482
Reading from 9378: heap size 822 MB, throughput 0.999076
Reading from 9378: heap size 822 MB, throughput 0.999165
Reading from 9377: heap size 1224 MB, throughput 0.926726
Reading from 9378: heap size 822 MB, throughput 0.998954
Reading from 9377: heap size 1256 MB, throughput 0.930706
Reading from 9378: heap size 822 MB, throughput 0.999107
Reading from 9377: heap size 1222 MB, throughput 0.929292
Reading from 9378: heap size 822 MB, throughput 0.998981
Reading from 9378: heap size 822 MB, throughput 0.999115
Reading from 9377: heap size 1251 MB, throughput 0.931789
Reading from 9378: heap size 822 MB, throughput 0.999077
Equal recommendation: 1219 MB each
Reading from 9377: heap size 1220 MB, throughput 0.931745
Reading from 9378: heap size 822 MB, throughput 0.998814
Reading from 9377: heap size 1246 MB, throughput 0.925813
Reading from 9378: heap size 822 MB, throughput 0.9992
Reading from 9377: heap size 1217 MB, throughput 0.922847
Reading from 9378: heap size 822 MB, throughput 0.999212
Reading from 9378: heap size 822 MB, throughput 0.998801
Reading from 9377: heap size 1247 MB, throughput 0.933451
Reading from 9378: heap size 822 MB, throughput 0.999093
Reading from 9377: heap size 1217 MB, throughput 0.925455
Reading from 9378: heap size 822 MB, throughput 0.998877
Reading from 9378: heap size 822 MB, throughput 0.999123
Reading from 9377: heap size 1244 MB, throughput 0.935621
Reading from 9378: heap size 822 MB, throughput 0.999018
Equal recommendation: 1219 MB each
Reading from 9377: heap size 1217 MB, throughput 0.931405
Reading from 9378: heap size 822 MB, throughput 0.999066
Reading from 9377: heap size 1241 MB, throughput 0.9354
Reading from 9378: heap size 822 MB, throughput 0.999176
Reading from 9378: heap size 822 MB, throughput 0.994081
Reading from 9377: heap size 1217 MB, throughput 0.943949
Reading from 9378: heap size 822 MB, throughput 0.999281
Reading from 9378: heap size 822 MB, throughput 0.998583
Reading from 9377: heap size 1239 MB, throughput 0.518044
Reading from 9378: heap size 822 MB, throughput 0.999073
Reading from 9377: heap size 1188 MB, throughput 0.984421
Reading from 9378: heap size 822 MB, throughput 0.99911
Reading from 9377: heap size 1195 MB, throughput 0.978191
Equal recommendation: 1219 MB each
Reading from 9378: heap size 822 MB, throughput 0.998878
Reading from 9377: heap size 1205 MB, throughput 0.97328
Reading from 9378: heap size 822 MB, throughput 0.999099
Reading from 9377: heap size 1215 MB, throughput 0.970962
Reading from 9378: heap size 822 MB, throughput 0.999138
Reading from 9378: heap size 822 MB, throughput 0.998771
Reading from 9377: heap size 1218 MB, throughput 0.964545
Reading from 9378: heap size 822 MB, throughput 0.999024
Reading from 9377: heap size 1224 MB, throughput 0.962594
Reading from 9378: heap size 822 MB, throughput 0.998946
Reading from 9377: heap size 1143 MB, throughput 0.958311
Reading from 9378: heap size 822 MB, throughput 0.999217
Reading from 9378: heap size 822 MB, throughput 0.999089
Reading from 9377: heap size 1224 MB, throughput 0.95686
Equal recommendation: 1219 MB each
Reading from 9378: heap size 822 MB, throughput 0.999017
Reading from 9377: heap size 1156 MB, throughput 0.952185
Reading from 9378: heap size 822 MB, throughput 0.999194
Reading from 9377: heap size 1217 MB, throughput 0.949948
Reading from 9378: heap size 822 MB, throughput 0.998968
Reading from 9378: heap size 822 MB, throughput 0.999048
Reading from 9377: heap size 1215 MB, throughput 0.953871
Reading from 9378: heap size 822 MB, throughput 0.999095
Reading from 9377: heap size 1218 MB, throughput 0.944462
Reading from 9378: heap size 822 MB, throughput 0.99912
Reading from 9378: heap size 822 MB, throughput 0.99908
Reading from 9377: heap size 1219 MB, throughput 0.941687
Reading from 9378: heap size 822 MB, throughput 0.998915
Equal recommendation: 1219 MB each
Reading from 9377: heap size 1213 MB, throughput 0.939893
Reading from 9378: heap size 822 MB, throughput 0.999223
Reading from 9377: heap size 1221 MB, throughput 0.940252
Reading from 9378: heap size 822 MB, throughput 0.999086
Reading from 9378: heap size 822 MB, throughput 0.998797
Reading from 9377: heap size 1220 MB, throughput 0.927665
Reading from 9378: heap size 822 MB, throughput 0.999071
Reading from 9378: heap size 822 MB, throughput 0.998702
Reading from 9377: heap size 1225 MB, throughput 0.941601
Reading from 9378: heap size 822 MB, throughput 0.999157
Reading from 9377: heap size 1226 MB, throughput 0.930006
Reading from 9378: heap size 822 MB, throughput 0.999004
Reading from 9377: heap size 1228 MB, throughput 0.93395
Reading from 9378: heap size 822 MB, throughput 0.99907
Equal recommendation: 1219 MB each
Reading from 9377: heap size 1229 MB, throughput 0.928437
Reading from 9378: heap size 822 MB, throughput 0.999164
Reading from 9378: heap size 822 MB, throughput 0.998756
Reading from 9377: heap size 1197 MB, throughput 0.934655
Reading from 9378: heap size 822 MB, throughput 0.99912
Reading from 9377: heap size 1229 MB, throughput 0.945124
Reading from 9378: heap size 822 MB, throughput 0.999059
Reading from 9378: heap size 822 MB, throughput 0.999135
Reading from 9377: heap size 1195 MB, throughput 0.929726
Reading from 9378: heap size 822 MB, throughput 0.999044
Reading from 9377: heap size 1229 MB, throughput 0.927318
Reading from 9378: heap size 822 MB, throughput 0.999125
Reading from 9377: heap size 1194 MB, throughput 0.9398
Reading from 9378: heap size 822 MB, throughput 0.999133
Equal recommendation: 1219 MB each
Reading from 9378: heap size 822 MB, throughput 0.999126
Reading from 9377: heap size 1228 MB, throughput 0.921559
Reading from 9378: heap size 822 MB, throughput 0.998664
Reading from 9377: heap size 1194 MB, throughput 0.933496
Reading from 9378: heap size 822 MB, throughput 0.999074
Reading from 9378: heap size 822 MB, throughput 0.998886
Reading from 9377: heap size 1226 MB, throughput 0.93692
Reading from 9378: heap size 822 MB, throughput 0.999204
Reading from 9378: heap size 822 MB, throughput 0.999032
Reading from 9378: heap size 822 MB, throughput 0.999046
Equal recommendation: 1219 MB each
Reading from 9378: heap size 822 MB, throughput 0.999196
Reading from 9378: heap size 822 MB, throughput 0.999018
Reading from 9378: heap size 822 MB, throughput 0.998949
Reading from 9378: heap size 822 MB, throughput 0.998905
Reading from 9378: heap size 822 MB, throughput 0.999065
Reading from 9378: heap size 822 MB, throughput 0.999002
Reading from 9378: heap size 822 MB, throughput 0.999053
Reading from 9377: heap size 1194 MB, throughput 0.988437
Reading from 9378: heap size 822 MB, throughput 0.999106
Equal recommendation: 1219 MB each
Reading from 9378: heap size 822 MB, throughput 0.99894
Reading from 9378: heap size 822 MB, throughput 0.998752
Reading from 9378: heap size 822 MB, throughput 0.999049
Reading from 9378: heap size 822 MB, throughput 0.998148
Reading from 9378: heap size 822 MB, throughput 0.999006
Reading from 9377: heap size 1209 MB, throughput 0.611504
Reading from 9378: heap size 822 MB, throughput 0.999212
Reading from 9378: heap size 822 MB, throughput 0.99893
Reading from 9378: heap size 822 MB, throughput 0.999093
Equal recommendation: 1219 MB each
Reading from 9378: heap size 822 MB, throughput 0.998884
Reading from 9378: heap size 782 MB, throughput 0.998912
Client 9378 died
Clients: 1
Reading from 9377: heap size 1362 MB, throughput 0.988606
Recommendation: one client; give it all the memory
Reading from 9377: heap size 1378 MB, throughput 0.981003
Reading from 9377: heap size 1387 MB, throughput 0.973944
Reading from 9377: heap size 1389 MB, throughput 0.889567
Reading from 9377: heap size 1394 MB, throughput 0.760359
Reading from 9377: heap size 1376 MB, throughput 0.696258
Reading from 9377: heap size 1398 MB, throughput 0.674311
Reading from 9377: heap size 1418 MB, throughput 0.706785
Reading from 9377: heap size 1427 MB, throughput 0.682004
Reading from 9377: heap size 1451 MB, throughput 0.725938
Reading from 9377: heap size 1455 MB, throughput 0.703724
Reading from 9377: heap size 1472 MB, throughput 0.183554
Reading from 9377: heap size 1376 MB, throughput 0.990993
Reading from 9377: heap size 1394 MB, throughput 0.982518
Client 9377 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
