Using scaled threading model. 4 processors detected, 1 threads used to drive the workload, in a possible range of [1,256000]
===== DaCapo 9.12 h2 starting warmup 1 =====
................................................................................................................................................................................................................................................................
Completed 256000 transactions
	Stock level ............. 10178 ( 4.0%)
	Order status by name ....  6096 ( 2.4%)
	Order status by ID ......  4109 ( 1.6%)
	Payment by name ......... 66251 (25.9%)
	Payment by ID ........... 43986 (17.2%)
	Delivery schedule ....... 10282 ( 4.0%)
	New order ...............113955 (44.5%)
	New order rollback ......  1143 ( 0.4%)
Resetting database to initial state
===== DaCapo 9.12 h2 completed warmup 1 in 146806 msec =====
===== DaCapo 9.12 h2 starting =====
................................................................................................................................................................................................................................................................
Completed 256000 transactions
	Stock level ............. 10178 ( 4.0%)
	Order status by name ....  6096 ( 2.4%)
	Order status by ID ......  4109 ( 1.6%)
	Payment by name ......... 66251 (25.9%)
	Payment by ID ........... 43986 (17.2%)
	Delivery schedule ....... 10282 ( 4.0%)
	New order ...............113955 (44.5%)
	New order rollback ......  1143 ( 0.4%)
Resetting database to initial state
===== DaCapo 9.12 h2 PASSED in 141213 msec =====
