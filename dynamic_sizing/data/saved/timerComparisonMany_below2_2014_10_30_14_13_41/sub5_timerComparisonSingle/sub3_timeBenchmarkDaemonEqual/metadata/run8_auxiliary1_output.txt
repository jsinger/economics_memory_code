economemd
    total memory: 1049 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_30_14_13_41/sub5_timerComparisonSingle/sub3_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 9380: heap size 9 MB, throughput 0.991777
Clients: 1
Client 9380 has a minimum heap size of 276 MB
Reading from 9379: heap size 9 MB, throughput 0.986365
Clients: 2
Client 9379 has a minimum heap size of 276 MB
Reading from 9380: heap size 9 MB, throughput 0.978837
Reading from 9379: heap size 9 MB, throughput 0.980451
Reading from 9380: heap size 9 MB, throughput 0.963815
Reading from 9380: heap size 9 MB, throughput 0.926506
Reading from 9379: heap size 11 MB, throughput 0.972522
Reading from 9380: heap size 11 MB, throughput 0.957356
Reading from 9379: heap size 11 MB, throughput 0.985388
Reading from 9380: heap size 11 MB, throughput 0.984372
Reading from 9379: heap size 15 MB, throughput 0.796045
Reading from 9380: heap size 17 MB, throughput 0.894129
Reading from 9379: heap size 19 MB, throughput 0.970826
Reading from 9380: heap size 17 MB, throughput 0.45294
Reading from 9379: heap size 23 MB, throughput 0.904733
Reading from 9380: heap size 30 MB, throughput 0.982605
Reading from 9379: heap size 27 MB, throughput 0.848075
Reading from 9380: heap size 31 MB, throughput 0.901432
Reading from 9379: heap size 27 MB, throughput 0.590467
Reading from 9380: heap size 34 MB, throughput 0.509317
Reading from 9379: heap size 38 MB, throughput 0.870513
Reading from 9380: heap size 44 MB, throughput 0.865172
Reading from 9379: heap size 43 MB, throughput 0.911367
Reading from 9380: heap size 50 MB, throughput 0.791385
Reading from 9379: heap size 43 MB, throughput 0.872028
Reading from 9379: heap size 48 MB, throughput 0.233489
Reading from 9380: heap size 50 MB, throughput 0.165016
Reading from 9379: heap size 62 MB, throughput 0.865915
Reading from 9379: heap size 67 MB, throughput 0.757038
Reading from 9379: heap size 69 MB, throughput 0.792933
Reading from 9380: heap size 71 MB, throughput 0.281648
Reading from 9380: heap size 91 MB, throughput 0.778834
Reading from 9380: heap size 95 MB, throughput 0.866061
Reading from 9379: heap size 74 MB, throughput 0.156136
Reading from 9380: heap size 96 MB, throughput 0.712053
Reading from 9379: heap size 99 MB, throughput 0.76545
Reading from 9379: heap size 105 MB, throughput 0.895305
Reading from 9380: heap size 101 MB, throughput 0.338538
Reading from 9379: heap size 106 MB, throughput 0.841334
Reading from 9380: heap size 125 MB, throughput 0.730623
Reading from 9380: heap size 132 MB, throughput 0.848916
Reading from 9380: heap size 133 MB, throughput 0.619701
Reading from 9379: heap size 109 MB, throughput 0.254122
Reading from 9379: heap size 137 MB, throughput 0.593066
Reading from 9379: heap size 142 MB, throughput 0.805082
Reading from 9379: heap size 146 MB, throughput 0.70227
Reading from 9379: heap size 150 MB, throughput 0.663802
Reading from 9379: heap size 156 MB, throughput 0.614655
Reading from 9380: heap size 136 MB, throughput 0.136594
Reading from 9379: heap size 160 MB, throughput 0.595301
Reading from 9380: heap size 170 MB, throughput 0.649956
Reading from 9380: heap size 176 MB, throughput 0.683153
Reading from 9380: heap size 179 MB, throughput 0.525418
Reading from 9379: heap size 169 MB, throughput 0.104537
Reading from 9380: heap size 187 MB, throughput 0.677259
Reading from 9379: heap size 205 MB, throughput 0.556531
Reading from 9380: heap size 190 MB, throughput 0.840775
Reading from 9379: heap size 209 MB, throughput 0.101088
Reading from 9379: heap size 243 MB, throughput 0.864581
Reading from 9379: heap size 246 MB, throughput 0.958794
Reading from 9380: heap size 194 MB, throughput 0.466845
Reading from 9379: heap size 243 MB, throughput 0.843331
Reading from 9379: heap size 245 MB, throughput 0.614701
Reading from 9380: heap size 240 MB, throughput 0.663015
Reading from 9379: heap size 245 MB, throughput 0.823812
Reading from 9380: heap size 248 MB, throughput 0.619427
Reading from 9379: heap size 246 MB, throughput 0.555484
Reading from 9380: heap size 252 MB, throughput 0.783179
Reading from 9379: heap size 249 MB, throughput 0.804811
Reading from 9380: heap size 258 MB, throughput 0.533118
Reading from 9379: heap size 249 MB, throughput 0.660928
Reading from 9380: heap size 261 MB, throughput 0.760864
Reading from 9380: heap size 267 MB, throughput 0.795566
Reading from 9379: heap size 253 MB, throughput 0.819477
Reading from 9379: heap size 253 MB, throughput 0.661729
Reading from 9380: heap size 269 MB, throughput 0.795694
Reading from 9379: heap size 260 MB, throughput 0.701613
Reading from 9380: heap size 271 MB, throughput 0.658712
Reading from 9379: heap size 262 MB, throughput 0.691267
Reading from 9380: heap size 276 MB, throughput 0.652173
Reading from 9379: heap size 270 MB, throughput 0.754492
Reading from 9380: heap size 283 MB, throughput 0.673028
Reading from 9380: heap size 285 MB, throughput 0.666517
Reading from 9380: heap size 289 MB, throughput 0.697628
Reading from 9379: heap size 270 MB, throughput 0.376824
Reading from 9379: heap size 308 MB, throughput 0.739469
Reading from 9379: heap size 311 MB, throughput 0.718134
Reading from 9380: heap size 291 MB, throughput 0.431615
Reading from 9379: heap size 314 MB, throughput 0.818728
Reading from 9380: heap size 327 MB, throughput 0.626933
Reading from 9379: heap size 314 MB, throughput 0.817845
Reading from 9380: heap size 331 MB, throughput 0.759481
Reading from 9379: heap size 320 MB, throughput 0.861467
Reading from 9380: heap size 336 MB, throughput 0.789593
Reading from 9380: heap size 336 MB, throughput 0.855115
Reading from 9379: heap size 321 MB, throughput 0.831242
Reading from 9379: heap size 326 MB, throughput 0.761222
Reading from 9380: heap size 338 MB, throughput 0.898605
Reading from 9379: heap size 326 MB, throughput 0.733092
Reading from 9379: heap size 326 MB, throughput 0.67367
Reading from 9380: heap size 340 MB, throughput 0.775842
Reading from 9379: heap size 328 MB, throughput 0.628763
Reading from 9380: heap size 341 MB, throughput 0.658464
Reading from 9380: heap size 342 MB, throughput 0.59065
Reading from 9379: heap size 333 MB, throughput 0.670693
Reading from 9380: heap size 347 MB, throughput 0.720789
Reading from 9380: heap size 348 MB, throughput 0.638845
Reading from 9380: heap size 355 MB, throughput 0.615215
Equal recommendation: 524 MB each
Reading from 9379: heap size 334 MB, throughput 0.970185
Reading from 9380: heap size 356 MB, throughput 0.976828
Reading from 9379: heap size 342 MB, throughput 0.982336
Reading from 9380: heap size 361 MB, throughput 0.983988
Reading from 9379: heap size 343 MB, throughput 0.971004
Reading from 9380: heap size 364 MB, throughput 0.961817
Reading from 9379: heap size 345 MB, throughput 0.97634
Reading from 9380: heap size 363 MB, throughput 0.699505
Reading from 9379: heap size 347 MB, throughput 0.977176
Reading from 9380: heap size 411 MB, throughput 0.9963
Reading from 9379: heap size 348 MB, throughput 0.973175
Reading from 9380: heap size 415 MB, throughput 0.995489
Reading from 9379: heap size 350 MB, throughput 0.974877
Equal recommendation: 524 MB each
Reading from 9380: heap size 418 MB, throughput 0.992006
Reading from 9379: heap size 349 MB, throughput 0.978707
Reading from 9380: heap size 422 MB, throughput 0.989007
Reading from 9379: heap size 351 MB, throughput 0.97165
Reading from 9380: heap size 423 MB, throughput 0.987066
Reading from 9379: heap size 353 MB, throughput 0.976245
Reading from 9380: heap size 420 MB, throughput 0.990355
Reading from 9379: heap size 354 MB, throughput 0.976631
Reading from 9380: heap size 423 MB, throughput 0.985359
Reading from 9379: heap size 356 MB, throughput 0.980832
Reading from 9380: heap size 416 MB, throughput 0.982883
Reading from 9379: heap size 358 MB, throughput 0.970692
Reading from 9380: heap size 385 MB, throughput 0.981656
Equal recommendation: 524 MB each
Reading from 9379: heap size 360 MB, throughput 0.971615
Reading from 9380: heap size 416 MB, throughput 0.976472
Reading from 9380: heap size 417 MB, throughput 0.784731
Reading from 9379: heap size 362 MB, throughput 0.304714
Reading from 9380: heap size 414 MB, throughput 0.816475
Reading from 9379: heap size 397 MB, throughput 0.662496
Reading from 9380: heap size 422 MB, throughput 0.677308
Reading from 9379: heap size 402 MB, throughput 0.833806
Reading from 9380: heap size 432 MB, throughput 0.746556
Reading from 9379: heap size 407 MB, throughput 0.923555
Reading from 9379: heap size 406 MB, throughput 0.992241
Reading from 9380: heap size 437 MB, throughput 0.963737
Reading from 9379: heap size 409 MB, throughput 0.985484
Reading from 9380: heap size 440 MB, throughput 0.985037
Reading from 9379: heap size 412 MB, throughput 0.993014
Reading from 9380: heap size 446 MB, throughput 0.985411
Reading from 9379: heap size 415 MB, throughput 0.990231
Reading from 9380: heap size 442 MB, throughput 0.978395
Reading from 9379: heap size 416 MB, throughput 0.992719
Equal recommendation: 524 MB each
Reading from 9380: heap size 446 MB, throughput 0.988497
Reading from 9379: heap size 413 MB, throughput 0.988401
Reading from 9380: heap size 442 MB, throughput 0.979835
Reading from 9379: heap size 380 MB, throughput 0.988334
Reading from 9380: heap size 446 MB, throughput 0.986296
Reading from 9379: heap size 409 MB, throughput 0.983113
Reading from 9380: heap size 442 MB, throughput 0.980734
Reading from 9379: heap size 412 MB, throughput 0.985413
Reading from 9379: heap size 412 MB, throughput 0.978324
Reading from 9380: heap size 445 MB, throughput 0.98168
Reading from 9379: heap size 413 MB, throughput 0.977816
Reading from 9380: heap size 441 MB, throughput 0.974217
Equal recommendation: 524 MB each
Reading from 9379: heap size 414 MB, throughput 0.98946
Reading from 9379: heap size 391 MB, throughput 0.902065
Reading from 9379: heap size 411 MB, throughput 0.845258
Reading from 9380: heap size 444 MB, throughput 0.982619
Reading from 9379: heap size 416 MB, throughput 0.736433
Reading from 9379: heap size 423 MB, throughput 0.891602
Reading from 9380: heap size 445 MB, throughput 0.979309
Reading from 9380: heap size 446 MB, throughput 0.906589
Reading from 9380: heap size 448 MB, throughput 0.860958
Reading from 9380: heap size 449 MB, throughput 0.954204
Reading from 9379: heap size 426 MB, throughput 0.988564
Reading from 9379: heap size 427 MB, throughput 0.984208
Reading from 9380: heap size 456 MB, throughput 0.990682
Reading from 9379: heap size 430 MB, throughput 0.986812
Reading from 9380: heap size 456 MB, throughput 0.987042
Reading from 9379: heap size 429 MB, throughput 0.986842
Reading from 9380: heap size 459 MB, throughput 0.982677
Equal recommendation: 524 MB each
Reading from 9379: heap size 432 MB, throughput 0.979169
Reading from 9380: heap size 460 MB, throughput 0.992066
Reading from 9379: heap size 431 MB, throughput 0.984036
Reading from 9380: heap size 458 MB, throughput 0.987585
Reading from 9379: heap size 433 MB, throughput 0.983557
Reading from 9380: heap size 461 MB, throughput 0.983952
Reading from 9379: heap size 434 MB, throughput 0.9834
Reading from 9380: heap size 458 MB, throughput 0.983224
Reading from 9379: heap size 435 MB, throughput 0.984761
Reading from 9380: heap size 460 MB, throughput 0.98442
Equal recommendation: 524 MB each
Reading from 9379: heap size 438 MB, throughput 0.99
Reading from 9379: heap size 439 MB, throughput 0.91061
Reading from 9379: heap size 439 MB, throughput 0.843497
Reading from 9379: heap size 441 MB, throughput 0.873327
Reading from 9380: heap size 462 MB, throughput 0.988951
Reading from 9380: heap size 463 MB, throughput 0.955997
Reading from 9379: heap size 449 MB, throughput 0.990209
Reading from 9380: heap size 462 MB, throughput 0.881241
Reading from 9380: heap size 464 MB, throughput 0.876305
Reading from 9380: heap size 471 MB, throughput 0.989414
Reading from 9379: heap size 450 MB, throughput 0.989795
Reading from 9380: heap size 472 MB, throughput 0.993111
Reading from 9379: heap size 455 MB, throughput 0.989081
Reading from 9380: heap size 476 MB, throughput 0.987297
Reading from 9379: heap size 456 MB, throughput 0.986595
Equal recommendation: 524 MB each
Reading from 9379: heap size 455 MB, throughput 0.987545
Reading from 9380: heap size 478 MB, throughput 0.990263
Reading from 9379: heap size 458 MB, throughput 0.986798
Reading from 9380: heap size 477 MB, throughput 0.990914
Reading from 9379: heap size 457 MB, throughput 0.988681
Reading from 9380: heap size 479 MB, throughput 0.987494
Reading from 9379: heap size 458 MB, throughput 0.982221
Reading from 9380: heap size 478 MB, throughput 0.985602
Reading from 9379: heap size 461 MB, throughput 0.990612
Equal recommendation: 524 MB each
Reading from 9379: heap size 462 MB, throughput 0.905177
Reading from 9379: heap size 462 MB, throughput 0.875395
Reading from 9380: heap size 480 MB, throughput 0.98419
Reading from 9379: heap size 464 MB, throughput 0.982277
Reading from 9380: heap size 481 MB, throughput 0.984792
Reading from 9380: heap size 483 MB, throughput 0.880868
Reading from 9380: heap size 484 MB, throughput 0.908208
Reading from 9379: heap size 472 MB, throughput 0.990827
Reading from 9380: heap size 486 MB, throughput 0.98933
Reading from 9379: heap size 473 MB, throughput 0.989633
Reading from 9380: heap size 493 MB, throughput 0.992604
Reading from 9379: heap size 475 MB, throughput 0.99025
Reading from 9380: heap size 494 MB, throughput 0.989893
Equal recommendation: 524 MB each
Reading from 9379: heap size 476 MB, throughput 0.986101
Reading from 9380: heap size 495 MB, throughput 0.992595
Reading from 9379: heap size 476 MB, throughput 0.988548
Reading from 9380: heap size 497 MB, throughput 0.987368
Reading from 9379: heap size 477 MB, throughput 0.986469
Reading from 9380: heap size 496 MB, throughput 0.98663
Reading from 9379: heap size 480 MB, throughput 0.983158
Reading from 9380: heap size 498 MB, throughput 0.985554
Reading from 9379: heap size 480 MB, throughput 0.981883
Reading from 9379: heap size 485 MB, throughput 0.899291
Reading from 9379: heap size 487 MB, throughput 0.925142
Equal recommendation: 524 MB each
Reading from 9380: heap size 500 MB, throughput 0.989941
Reading from 9380: heap size 501 MB, throughput 0.909983
Reading from 9380: heap size 501 MB, throughput 0.901346
Reading from 9379: heap size 493 MB, throughput 0.993669
Reading from 9380: heap size 503 MB, throughput 0.989082
Reading from 9379: heap size 494 MB, throughput 0.99198
Reading from 9380: heap size 511 MB, throughput 0.991886
Reading from 9379: heap size 495 MB, throughput 0.993286
Reading from 9380: heap size 511 MB, throughput 0.993375
Equal recommendation: 524 MB each
Reading from 9379: heap size 497 MB, throughput 0.989144
Reading from 9380: heap size 512 MB, throughput 0.986518
Reading from 9379: heap size 495 MB, throughput 0.987467
Reading from 9380: heap size 514 MB, throughput 0.985972
Reading from 9379: heap size 498 MB, throughput 0.9876
Reading from 9380: heap size 514 MB, throughput 0.9883
Reading from 9379: heap size 500 MB, throughput 0.991207
Reading from 9379: heap size 501 MB, throughput 0.964336
Reading from 9379: heap size 501 MB, throughput 0.904856
Equal recommendation: 524 MB each
Reading from 9380: heap size 515 MB, throughput 0.994872
Reading from 9379: heap size 503 MB, throughput 0.989707
Reading from 9380: heap size 518 MB, throughput 0.976169
Reading from 9380: heap size 518 MB, throughput 0.878153
Reading from 9380: heap size 522 MB, throughput 0.979066
Reading from 9379: heap size 509 MB, throughput 0.99379
Reading from 9380: heap size 524 MB, throughput 0.992069
Reading from 9379: heap size 510 MB, throughput 0.991987
Reading from 9380: heap size 510 MB, throughput 0.990798
Reading from 9379: heap size 511 MB, throughput 0.988939
Equal recommendation: 524 MB each
Reading from 9380: heap size 518 MB, throughput 0.990713
Reading from 9379: heap size 513 MB, throughput 0.990108
Reading from 9380: heap size 517 MB, throughput 0.989696
Reading from 9379: heap size 513 MB, throughput 0.988417
Reading from 9380: heap size 519 MB, throughput 0.987174
Reading from 9379: heap size 514 MB, throughput 0.992514
Reading from 9379: heap size 514 MB, throughput 0.972243
Reading from 9379: heap size 516 MB, throughput 0.904833
Reading from 9380: heap size 521 MB, throughput 0.985242
Equal recommendation: 524 MB each
Reading from 9379: heap size 513 MB, throughput 0.991365
Reading from 9380: heap size 522 MB, throughput 0.970422
Reading from 9380: heap size 524 MB, throughput 0.899945
Reading from 9380: heap size 509 MB, throughput 0.981477
Reading from 9379: heap size 487 MB, throughput 0.992569
Reading from 9380: heap size 521 MB, throughput 0.991972
Reading from 9379: heap size 515 MB, throughput 0.989345
Reading from 9380: heap size 521 MB, throughput 0.992354
Reading from 9379: heap size 492 MB, throughput 0.990498
Equal recommendation: 524 MB each
Reading from 9380: heap size 522 MB, throughput 0.988011
Reading from 9379: heap size 515 MB, throughput 0.988372
Reading from 9380: heap size 523 MB, throughput 0.988307
Reading from 9379: heap size 513 MB, throughput 0.991258
Reading from 9380: heap size 522 MB, throughput 0.987529
Reading from 9379: heap size 516 MB, throughput 0.993208
Reading from 9379: heap size 516 MB, throughput 0.930059
Reading from 9379: heap size 513 MB, throughput 0.895521
Reading from 9380: heap size 523 MB, throughput 0.986848
Equal recommendation: 524 MB each
Reading from 9379: heap size 508 MB, throughput 0.992567
Reading from 9380: heap size 525 MB, throughput 0.985918
Reading from 9380: heap size 513 MB, throughput 0.895327
Reading from 9380: heap size 521 MB, throughput 0.979847
Reading from 9379: heap size 514 MB, throughput 0.992567
Reading from 9380: heap size 522 MB, throughput 0.991379
Reading from 9379: heap size 488 MB, throughput 0.991014
Reading from 9380: heap size 525 MB, throughput 0.992518
Reading from 9379: heap size 515 MB, throughput 0.989911
Equal recommendation: 524 MB each
Reading from 9380: heap size 493 MB, throughput 0.990581
Reading from 9379: heap size 496 MB, throughput 0.989845
Reading from 9380: heap size 520 MB, throughput 0.990382
Reading from 9379: heap size 513 MB, throughput 0.987709
Reading from 9380: heap size 519 MB, throughput 0.989171
Reading from 9379: heap size 513 MB, throughput 0.993774
Reading from 9379: heap size 516 MB, throughput 0.910337
Reading from 9379: heap size 510 MB, throughput 0.938832
Reading from 9380: heap size 520 MB, throughput 0.986679
Equal recommendation: 524 MB each
Reading from 9379: heap size 515 MB, throughput 0.99257
Reading from 9380: heap size 520 MB, throughput 0.987588
Reading from 9380: heap size 523 MB, throughput 0.910239
Reading from 9380: heap size 524 MB, throughput 0.965476
Reading from 9379: heap size 488 MB, throughput 0.992233
Reading from 9380: heap size 526 MB, throughput 0.991456
Reading from 9379: heap size 516 MB, throughput 0.990795
Reading from 9380: heap size 493 MB, throughput 0.991083
Reading from 9379: heap size 495 MB, throughput 0.989253
Equal recommendation: 524 MB each
Reading from 9380: heap size 522 MB, throughput 0.989796
Reading from 9379: heap size 514 MB, throughput 0.988799
Reading from 9380: heap size 521 MB, throughput 0.986784
Reading from 9379: heap size 514 MB, throughput 0.986489
Reading from 9380: heap size 520 MB, throughput 0.988892
Reading from 9379: heap size 516 MB, throughput 0.986565
Reading from 9379: heap size 512 MB, throughput 0.928088
Reading from 9379: heap size 512 MB, throughput 0.972498
Reading from 9380: heap size 521 MB, throughput 0.987437
Equal recommendation: 524 MB each
Reading from 9379: heap size 483 MB, throughput 0.993318
Reading from 9380: heap size 523 MB, throughput 0.990076
Reading from 9380: heap size 524 MB, throughput 0.909628
Reading from 9380: heap size 515 MB, throughput 0.962562
Reading from 9379: heap size 514 MB, throughput 0.993278
Reading from 9380: heap size 521 MB, throughput 0.992788
Reading from 9379: heap size 488 MB, throughput 0.990948
Reading from 9380: heap size 525 MB, throughput 0.992115
Reading from 9379: heap size 513 MB, throughput 0.990133
Equal recommendation: 524 MB each
Reading from 9380: heap size 496 MB, throughput 0.990952
Reading from 9379: heap size 512 MB, throughput 0.987468
Reading from 9380: heap size 521 MB, throughput 0.9891
Reading from 9379: heap size 514 MB, throughput 0.98876
Reading from 9380: heap size 520 MB, throughput 0.988852
Reading from 9379: heap size 514 MB, throughput 0.984395
Reading from 9379: heap size 511 MB, throughput 0.910124
Reading from 9379: heap size 503 MB, throughput 0.985573
Reading from 9380: heap size 521 MB, throughput 0.989261
Equal recommendation: 524 MB each
Reading from 9379: heap size 511 MB, throughput 0.99232
Reading from 9380: heap size 522 MB, throughput 0.987368
Reading from 9380: heap size 524 MB, throughput 0.927572
Reading from 9380: heap size 525 MB, throughput 0.971866
Reading from 9379: heap size 512 MB, throughput 0.994231
Reading from 9380: heap size 516 MB, throughput 0.992922
Reading from 9379: heap size 509 MB, throughput 0.992373
Reading from 9380: heap size 522 MB, throughput 0.991456
Reading from 9379: heap size 510 MB, throughput 0.987176
Equal recommendation: 524 MB each
Reading from 9380: heap size 523 MB, throughput 0.990599
Reading from 9379: heap size 509 MB, throughput 0.987656
Reading from 9380: heap size 524 MB, throughput 0.98869
Reading from 9379: heap size 510 MB, throughput 0.986364
Reading from 9379: heap size 511 MB, throughput 0.984244
Reading from 9379: heap size 504 MB, throughput 0.88824
Reading from 9380: heap size 506 MB, throughput 0.988695
Reading from 9379: heap size 511 MB, throughput 0.982364
Reading from 9380: heap size 516 MB, throughput 0.987055
Equal recommendation: 524 MB each
Reading from 9379: heap size 479 MB, throughput 0.993418
Reading from 9380: heap size 518 MB, throughput 0.890332
Reading from 9380: heap size 541 MB, throughput 0.982982
Reading from 9380: heap size 535 MB, throughput 0.984494
Reading from 9379: heap size 512 MB, throughput 0.991385
Reading from 9380: heap size 488 MB, throughput 0.994747
Reading from 9379: heap size 484 MB, throughput 0.988895
Reading from 9380: heap size 536 MB, throughput 0.990423
Reading from 9379: heap size 512 MB, throughput 0.991208
Equal recommendation: 524 MB each
Reading from 9380: heap size 496 MB, throughput 0.991731
Reading from 9379: heap size 510 MB, throughput 0.987315
Reading from 9380: heap size 530 MB, throughput 0.98885
Reading from 9379: heap size 512 MB, throughput 0.985869
Reading from 9380: heap size 506 MB, throughput 0.987854
Reading from 9379: heap size 512 MB, throughput 0.987463
Reading from 9379: heap size 511 MB, throughput 0.896242
Client 9379 died
Clients: 1
Reading from 9380: heap size 523 MB, throughput 0.98965
Recommendation: one client; give it all the memory
Reading from 9380: heap size 525 MB, throughput 0.989334
Reading from 9380: heap size 527 MB, throughput 0.955651
Reading from 9380: heap size 531 MB, throughput 0.879792
Client 9380 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
