economemd
    total memory: 1000 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: no
    equal recommendations: no

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/gcFactor_2014_09_24_12_05_40/daemon_log_25.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
ReadingThread: starting
TimerThread: starting
Reading from 6850: heap size 9 MB, throughput 0.99272
Clients: 1
Reading from 6850: heap size 9 MB, throughput 0.979568
Reading from 6850: heap size 9 MB, throughput 0.969768
Reading from 6850: heap size 9 MB, throughput 0.965015
Reading from 6850: heap size 11 MB, throughput 0.986881
Reading from 6850: heap size 11 MB, throughput 0.968988
Reading from 6850: heap size 17 MB, throughput 0.949823
Reading from 6850: heap size 17 MB, throughput 0.633806
Reading from 6850: heap size 30 MB, throughput 0.934038
Reading from 6850: heap size 31 MB, throughput 0.936011
Reading from 6850: heap size 35 MB, throughput 0.436907
Reading from 6850: heap size 45 MB, throughput 0.86341
Reading from 6850: heap size 49 MB, throughput 0.875808
Reading from 6850: heap size 51 MB, throughput 0.216918
Reading from 6850: heap size 70 MB, throughput 0.79692
Reading from 6850: heap size 72 MB, throughput 0.327911
Reading from 6850: heap size 97 MB, throughput 0.752938
Reading from 6850: heap size 98 MB, throughput 0.759683
Reading from 6850: heap size 100 MB, throughput 0.789328
Reading from 6850: heap size 103 MB, throughput 0.70668
Reading from 6850: heap size 110 MB, throughput 0.763877
Reading from 6850: heap size 112 MB, throughput 0.783696
Reading from 6850: heap size 115 MB, throughput 0.157951
Reading from 6850: heap size 147 MB, throughput 0.759664
Reading from 6850: heap size 150 MB, throughput 0.724942
Reading from 6850: heap size 155 MB, throughput 0.680146
Reading from 6850: heap size 158 MB, throughput 0.717802
Reading from 6850: heap size 164 MB, throughput 0.610363
Reading from 6850: heap size 168 MB, throughput 0.565077
Reading from 6850: heap size 176 MB, throughput 0.627864
Reading from 6850: heap size 180 MB, throughput 0.0985184
Reading from 6850: heap size 218 MB, throughput 0.577409
Reading from 6850: heap size 212 MB, throughput 0.676922
Reading from 6850: heap size 217 MB, throughput 0.699323
Reading from 6850: heap size 215 MB, throughput 0.124923
Reading from 6850: heap size 254 MB, throughput 0.647453
Reading from 6850: heap size 249 MB, throughput 0.791142
Reading from 6850: heap size 252 MB, throughput 0.714449
Reading from 6850: heap size 253 MB, throughput 0.744125
Reading from 6850: heap size 253 MB, throughput 0.692301
Reading from 6850: heap size 254 MB, throughput 0.687593
Reading from 6850: heap size 259 MB, throughput 0.59935
Reading from 6850: heap size 261 MB, throughput 0.615712
Reading from 6850: heap size 267 MB, throughput 0.615948
Reading from 6850: heap size 272 MB, throughput 0.511412
Reading from 6850: heap size 277 MB, throughput 0.506714
Reading from 6850: heap size 280 MB, throughput 0.544815
Reading from 6850: heap size 284 MB, throughput 0.470712
Reading from 6850: heap size 287 MB, throughput 0.086948
Reading from 6850: heap size 331 MB, throughput 0.530488
Reading from 6850: heap size 330 MB, throughput 0.601044
Reading from 6850: heap size 333 MB, throughput 0.102504
Reading from 6850: heap size 372 MB, throughput 0.539163
Reading from 6850: heap size 376 MB, throughput 0.566829
Reading from 6850: heap size 369 MB, throughput 0.599545
Reading from 6850: heap size 372 MB, throughput 0.525963
Reading from 6850: heap size 373 MB, throughput 0.560107
Reading from 6850: heap size 375 MB, throughput 0.525485
Reading from 6850: heap size 379 MB, throughput 0.559999
Reading from 6850: heap size 380 MB, throughput 0.521725
Reading from 6850: heap size 384 MB, throughput 0.561204
Reading from 6850: heap size 387 MB, throughput 0.0973148
Reading from 6850: heap size 437 MB, throughput 0.474235
Reading from 6850: heap size 441 MB, throughput 0.565997
Reading from 6850: heap size 444 MB, throughput 0.592322
Reading from 6850: heap size 446 MB, throughput 0.552336
Reading from 6850: heap size 449 MB, throughput 0.558558
Reading from 6850: heap size 454 MB, throughput 0.550461
Reading from 6850: heap size 458 MB, throughput 0.0979522
Reading from 6850: heap size 517 MB, throughput 0.46474
Reading from 6850: heap size 519 MB, throughput 0.563118
Reading from 6850: heap size 522 MB, throughput 0.541753
Reading from 6850: heap size 524 MB, throughput 0.107247
Reading from 6850: heap size 584 MB, throughput 0.494498
Reading from 6850: heap size 588 MB, throughput 0.55174
Reading from 6850: heap size 591 MB, throughput 0.556099
Reading from 6850: heap size 592 MB, throughput 0.539609
Reading from 6850: heap size 599 MB, throughput 0.522495
Reading from 6850: heap size 605 MB, throughput 0.517705
Reading from 6850: heap size 611 MB, throughput 0.510381
Reading from 6850: heap size 618 MB, throughput 0.272203
Reading from 6850: heap size 689 MB, throughput 0.784333
Reading from 6850: heap size 691 MB, throughput 0.871004
Reading from 6850: heap size 704 MB, throughput 0.605488
Reading from 6850: heap size 713 MB, throughput 0.423846
Reading from 6850: heap size 723 MB, throughput 0.645298
Reading from 6850: heap size 729 MB, throughput 0.358359
Reading from 6850: heap size 732 MB, throughput 0.366568
Reading from 6850: heap size 727 MB, throughput 0.026473
Reading from 6850: heap size 809 MB, throughput 0.311589
Reading from 6850: heap size 807 MB, throughput 0.892341
Reading from 6850: heap size 808 MB, throughput 0.697351
Reading from 6850: heap size 809 MB, throughput 0.641382
Reading from 6850: heap size 811 MB, throughput 0.641536
Reading from 6850: heap size 812 MB, throughput 0.0967122
Reading from 6850: heap size 897 MB, throughput 0.685922
Reading from 6850: heap size 903 MB, throughput 0.874664
Reading from 6850: heap size 905 MB, throughput 0.782425
Reading from 6850: heap size 910 MB, throughput 0.956927
Reading from 6850: heap size 914 MB, throughput 0.921346
Reading from 6850: heap size 915 MB, throughput 0.932471
Reading from 6850: heap size 766 MB, throughput 0.830614
Reading from 6850: heap size 891 MB, throughput 0.797201
Reading from 6850: heap size 775 MB, throughput 0.799397
Reading from 6850: heap size 878 MB, throughput 0.808267
Reading from 6850: heap size 780 MB, throughput 0.800077
Reading from 6850: heap size 872 MB, throughput 0.832256
Reading from 6850: heap size 878 MB, throughput 0.834555
Reading from 6850: heap size 867 MB, throughput 0.843095
Reading from 6850: heap size 873 MB, throughput 0.841126
Reading from 6850: heap size 862 MB, throughput 0.847537
Reading from 6850: heap size 868 MB, throughput 0.848973
Reading from 6850: heap size 861 MB, throughput 0.982478
Reading from 6850: heap size 865 MB, throughput 0.916195
Reading from 6850: heap size 861 MB, throughput 0.817281
Reading from 6850: heap size 863 MB, throughput 0.815972
Reading from 6850: heap size 866 MB, throughput 0.820821
Reading from 6850: heap size 867 MB, throughput 0.820543
Reading from 6850: heap size 869 MB, throughput 0.828617
Reading from 6850: heap size 871 MB, throughput 0.82878
Reading from 6850: heap size 873 MB, throughput 0.836438
Reading from 6850: heap size 875 MB, throughput 0.904135
Reading from 6850: heap size 880 MB, throughput 0.888962
Reading from 6850: heap size 881 MB, throughput 0.919824
Reading from 6850: heap size 882 MB, throughput 0.808295
Reading from 6850: heap size 884 MB, throughput 0.0855478
Reading from 6850: heap size 973 MB, throughput 0.53631
Reading from 6850: heap size 995 MB, throughput 0.746548
Reading from 6850: heap size 991 MB, throughput 0.748981
Reading from 6850: heap size 995 MB, throughput 0.738042
Reading from 6850: heap size 996 MB, throughput 0.741169
Reading from 6850: heap size 998 MB, throughput 0.728371
Reading from 6850: heap size 1008 MB, throughput 0.732176
Reading from 6850: heap size 1008 MB, throughput 0.722825
Reading from 6850: heap size 1021 MB, throughput 0.731569
Reading from 6850: heap size 1022 MB, throughput 0.790756
Reading from 6850: heap size 1035 MB, throughput 0.966981
Reading from 6850: heap size 1038 MB, throughput 0.977468
Reading from 6850: heap size 1053 MB, throughput 0.980128
Reading from 6850: heap size 1054 MB, throughput 0.978651
Reading from 6850: heap size 1055 MB, throughput 0.977373
Reading from 6850: heap size 1059 MB, throughput 0.977675
Reading from 6850: heap size 1053 MB, throughput 0.977142
Reading from 6850: heap size 1058 MB, throughput 0.976875
Reading from 6850: heap size 1054 MB, throughput 0.978607
Reading from 6850: heap size 1057 MB, throughput 0.97653
Reading from 6850: heap size 1061 MB, throughput 0.975411
Reading from 6850: heap size 1061 MB, throughput 0.97779
Reading from 6850: heap size 1064 MB, throughput 0.977234
Reading from 6850: heap size 1067 MB, throughput 0.977014
Reading from 6850: heap size 1070 MB, throughput 0.97636
Reading from 6850: heap size 1073 MB, throughput 0.977152
Reading from 6850: heap size 1076 MB, throughput 0.976723
Reading from 6850: heap size 1081 MB, throughput 0.667178
Reading from 6850: heap size 1180 MB, throughput 0.967251
Reading from 6850: heap size 1183 MB, throughput 0.987638
Reading from 6850: heap size 1189 MB, throughput 0.98726
Reading from 6850: heap size 1191 MB, throughput 0.986965
Reading from 6850: heap size 1190 MB, throughput 0.986348
Reading from 6850: heap size 1110 MB, throughput 0.986047
Reading from 6850: heap size 1182 MB, throughput 0.984905
Reading from 6850: heap size 1123 MB, throughput 0.984278
Reading from 6850: heap size 1175 MB, throughput 0.982203
Reading from 6850: heap size 1180 MB, throughput 0.98191
Reading from 6850: heap size 1179 MB, throughput 0.979716
Reading from 6850: heap size 1180 MB, throughput 0.980703
Reading from 6850: heap size 1180 MB, throughput 0.979358
Reading from 6850: heap size 1184 MB, throughput 0.978574
Reading from 6850: heap size 1186 MB, throughput 0.97835
Reading from 6850: heap size 1191 MB, throughput 0.974448
Reading from 6850: heap size 1196 MB, throughput 0.974839
Reading from 6850: heap size 1201 MB, throughput 0.975097
Reading from 6850: heap size 1206 MB, throughput 0.976662
Reading from 6850: heap size 1209 MB, throughput 0.975665
Reading from 6850: heap size 1213 MB, throughput 0.975844
Reading from 6850: heap size 1215 MB, throughput 0.975149
Reading from 6850: heap size 1220 MB, throughput 0.975922
Reading from 6850: heap size 1220 MB, throughput 0.975776
Reading from 6850: heap size 1223 MB, throughput 0.666993
Reading from 6850: heap size 1336 MB, throughput 0.967003
Reading from 6850: heap size 1335 MB, throughput 0.988259
Reading from 6850: heap size 1339 MB, throughput 0.986568
Reading from 6850: heap size 1345 MB, throughput 0.986098
Reading from 6850: heap size 1345 MB, throughput 0.98549
Reading from 6850: heap size 1340 MB, throughput 0.985696
Reading from 6850: heap size 1263 MB, throughput 0.989395
Reading from 6850: heap size 1317 MB, throughput 0.993077
Reading from 6850: heap size 1342 MB, throughput 0.984825
Reading from 6850: heap size 1325 MB, throughput 0.866033
Reading from 6850: heap size 1351 MB, throughput 0.757355
Reading from 6850: heap size 1352 MB, throughput 0.684495
Reading from 6850: heap size 1382 MB, throughput 0.598716
Reading from 6850: heap size 1406 MB, throughput 0.627361
Reading from 6850: heap size 1433 MB, throughput 0.60625
Reading from 6850: heap size 1461 MB, throughput 0.639355
Reading from 6850: heap size 1475 MB, throughput 0.643391
Reading from 6850: heap size 1504 MB, throughput 0.170388
Reading from 6850: heap size 1537 MB, throughput 0.991356
Reading from 6850: heap size 1536 MB, throughput 0.991041
Reading from 6850: heap size 1549 MB, throughput 0.995237
Reading from 6850: heap size 1575 MB, throughput 0.99402
Reading from 6850: heap size 1578 MB, throughput 0.992414
Reading from 6850: heap size 1571 MB, throughput 0.99129
Reading from 6850: heap size 1582 MB, throughput 0.990239
Reading from 6850: heap size 1555 MB, throughput 0.989559
Reading from 6850: heap size 1294 MB, throughput 0.988504
Reading from 6850: heap size 1531 MB, throughput 0.987394
Reading from 6850: heap size 1318 MB, throughput 0.986159
Reading from 6850: heap size 1505 MB, throughput 0.985973
Reading from 6850: heap size 1341 MB, throughput 0.985507
Reading from 6850: heap size 1482 MB, throughput 0.984188
Reading from 6850: heap size 1364 MB, throughput 0.983459
Reading from 6850: heap size 1480 MB, throughput 0.982725
Reading from 6850: heap size 1487 MB, throughput 0.981847
Reading from 6850: heap size 1489 MB, throughput 0.980677
Reading from 6850: heap size 1489 MB, throughput 0.980763
Reading from 6850: heap size 1493 MB, throughput 0.980897
Reading from 6850: heap size 1494 MB, throughput 0.980602
Reading from 6850: heap size 1498 MB, throughput 0.980891
Reading from 6850: heap size 1499 MB, throughput 0.979561
Reading from 6850: heap size 1503 MB, throughput 0.979847
Reading from 6850: heap size 1504 MB, throughput 0.97996
Reading from 6850: heap size 1509 MB, throughput 0.979489
Reading from 6850: heap size 1510 MB, throughput 0.980836
Reading from 6850: heap size 1515 MB, throughput 0.979024
Reading from 6850: heap size 1516 MB, throughput 0.979244
Reading from 6850: heap size 1521 MB, throughput 0.98174
Reading from 6850: heap size 1522 MB, throughput 0.97958
Reading from 6850: heap size 1528 MB, throughput 0.980395
Reading from 6850: heap size 1529 MB, throughput 0.978757
Reading from 6850: heap size 1535 MB, throughput 0.826468
Reading from 6850: heap size 1591 MB, throughput 0.995881
Reading from 6850: heap size 1586 MB, throughput 0.994549
Reading from 6850: heap size 1599 MB, throughput 0.993357
Reading from 6850: heap size 1612 MB, throughput 0.991767
Reading from 6850: heap size 1613 MB, throughput 0.991295
Reading from 6850: heap size 1615 MB, throughput 0.991205
Reading from 6850: heap size 1616 MB, throughput 0.984718
Reading from 6850: heap size 1637 MB, throughput 0.839753
Reading from 6850: heap size 1640 MB, throughput 0.743137
Reading from 6850: heap size 1668 MB, throughput 0.792601
Reading from 6850: heap size 1692 MB, throughput 0.769219
Reading from 6850: heap size 1736 MB, throughput 0.842652
Client 6850 died
Clients: 0
ReadingThread: finished
CommandThread: finished
TimerThread: finished
Main: All threads killed, cleaning up
