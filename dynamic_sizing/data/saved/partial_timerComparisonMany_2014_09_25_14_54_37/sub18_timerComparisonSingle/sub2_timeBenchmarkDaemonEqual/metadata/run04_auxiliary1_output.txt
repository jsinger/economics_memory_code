economemd
    total memory: 7338 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub18_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
TimerThread: starting
ReadingThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 2977: heap size 9 MB, throughput 0.987934
Clients: 1
Client 2977 has a minimum heap size of 12 MB
Reading from 2976: heap size 9 MB, throughput 0.992047
Clients: 2
Client 2976 has a minimum heap size of 1211 MB
Reading from 2976: heap size 9 MB, throughput 0.974518
Reading from 2977: heap size 9 MB, throughput 0.983587
Reading from 2976: heap size 9 MB, throughput 0.974539
Reading from 2976: heap size 9 MB, throughput 0.957764
Reading from 2977: heap size 11 MB, throughput 0.9844
Reading from 2977: heap size 11 MB, throughput 0.972909
Reading from 2976: heap size 11 MB, throughput 0.985002
Reading from 2977: heap size 15 MB, throughput 0.981838
Reading from 2976: heap size 11 MB, throughput 0.977485
Reading from 2976: heap size 17 MB, throughput 0.950071
Reading from 2977: heap size 15 MB, throughput 0.987092
Reading from 2976: heap size 17 MB, throughput 0.605031
Reading from 2976: heap size 30 MB, throughput 0.915784
Reading from 2976: heap size 31 MB, throughput 0.850377
Reading from 2976: heap size 35 MB, throughput 0.281474
Reading from 2976: heap size 46 MB, throughput 0.822771
Reading from 2977: heap size 24 MB, throughput 0.906146
Reading from 2976: heap size 47 MB, throughput 0.909631
Reading from 2976: heap size 50 MB, throughput 0.293991
Reading from 2976: heap size 72 MB, throughput 0.83924
Reading from 2977: heap size 30 MB, throughput 0.98674
Reading from 2976: heap size 72 MB, throughput 0.332356
Reading from 2977: heap size 35 MB, throughput 0.976465
Reading from 2976: heap size 99 MB, throughput 0.767169
Reading from 2976: heap size 99 MB, throughput 0.798911
Reading from 2976: heap size 100 MB, throughput 0.796616
Reading from 2977: heap size 39 MB, throughput 0.98763
Reading from 2977: heap size 41 MB, throughput 0.977709
Reading from 2976: heap size 103 MB, throughput 0.202681
Reading from 2977: heap size 45 MB, throughput 0.980472
Reading from 2976: heap size 138 MB, throughput 0.818738
Reading from 2976: heap size 138 MB, throughput 0.758652
Reading from 2977: heap size 49 MB, throughput 0.983983
Reading from 2976: heap size 139 MB, throughput 0.721559
Reading from 2976: heap size 143 MB, throughput 0.697428
Reading from 2977: heap size 50 MB, throughput 0.966812
Reading from 2977: heap size 56 MB, throughput 0.985277
Reading from 2977: heap size 56 MB, throughput 0.978922
Reading from 2977: heap size 64 MB, throughput 0.73236
Reading from 2976: heap size 146 MB, throughput 0.0906255
Reading from 2976: heap size 185 MB, throughput 0.62258
Reading from 2977: heap size 69 MB, throughput 0.988834
Reading from 2976: heap size 188 MB, throughput 0.674881
Reading from 2977: heap size 76 MB, throughput 0.989814
Reading from 2976: heap size 190 MB, throughput 0.764229
Reading from 2976: heap size 198 MB, throughput 0.693468
Reading from 2976: heap size 200 MB, throughput 0.707833
Reading from 2977: heap size 76 MB, throughput 0.99622
Reading from 2976: heap size 203 MB, throughput 0.146134
Reading from 2976: heap size 253 MB, throughput 0.651152
Reading from 2976: heap size 258 MB, throughput 0.689708
Reading from 2976: heap size 261 MB, throughput 0.611861
Reading from 2976: heap size 262 MB, throughput 0.641208
Reading from 2976: heap size 269 MB, throughput 0.547007
Reading from 2976: heap size 273 MB, throughput 0.586013
Reading from 2976: heap size 283 MB, throughput 0.491812
Reading from 2977: heap size 82 MB, throughput 0.997819
Reading from 2976: heap size 289 MB, throughput 0.459942
Reading from 2977: heap size 83 MB, throughput 0.995605
Reading from 2976: heap size 297 MB, throughput 0.11201
Reading from 2976: heap size 352 MB, throughput 0.419245
Reading from 2976: heap size 290 MB, throughput 0.652745
Reading from 2977: heap size 88 MB, throughput 0.994757
Reading from 2976: heap size 342 MB, throughput 0.147227
Reading from 2976: heap size 346 MB, throughput 0.503078
Reading from 2976: heap size 394 MB, throughput 0.755236
Reading from 2976: heap size 344 MB, throughput 0.672785
Reading from 2976: heap size 390 MB, throughput 0.686155
Reading from 2977: heap size 89 MB, throughput 0.996125
Reading from 2976: heap size 393 MB, throughput 0.620299
Reading from 2976: heap size 394 MB, throughput 0.603294
Reading from 2976: heap size 397 MB, throughput 0.607495
Reading from 2976: heap size 400 MB, throughput 0.559072
Reading from 2976: heap size 407 MB, throughput 0.556916
Reading from 2977: heap size 93 MB, throughput 0.996792
Reading from 2976: heap size 412 MB, throughput 0.523184
Reading from 2976: heap size 418 MB, throughput 0.473419
Reading from 2976: heap size 422 MB, throughput 0.480841
Reading from 2976: heap size 430 MB, throughput 0.455981
Reading from 2976: heap size 434 MB, throughput 0.440687
Reading from 2977: heap size 94 MB, throughput 0.995339
Reading from 2977: heap size 97 MB, throughput 0.996035
Reading from 2976: heap size 442 MB, throughput 0.0607531
Equal recommendation: 3669 MB each
Reading from 2976: heap size 505 MB, throughput 0.394233
Reading from 2976: heap size 434 MB, throughput 0.468144
Reading from 2977: heap size 97 MB, throughput 0.994441
Reading from 2977: heap size 100 MB, throughput 0.99254
Reading from 2976: heap size 494 MB, throughput 0.0641338
Reading from 2976: heap size 564 MB, throughput 0.423792
Reading from 2976: heap size 563 MB, throughput 0.665644
Reading from 2976: heap size 564 MB, throughput 0.676335
Reading from 2977: heap size 100 MB, throughput 0.993203
Reading from 2976: heap size 557 MB, throughput 0.565402
Reading from 2976: heap size 502 MB, throughput 0.515495
Reading from 2976: heap size 555 MB, throughput 0.51254
Reading from 2976: heap size 557 MB, throughput 0.52186
Reading from 2976: heap size 560 MB, throughput 0.595221
Reading from 2977: heap size 104 MB, throughput 0.99661
Reading from 2976: heap size 564 MB, throughput 0.605326
Reading from 2976: heap size 568 MB, throughput 0.564355
Reading from 2976: heap size 574 MB, throughput 0.593864
Reading from 2976: heap size 578 MB, throughput 0.560534
Reading from 2976: heap size 586 MB, throughput 0.520425
Reading from 2977: heap size 104 MB, throughput 0.996208
Reading from 2976: heap size 590 MB, throughput 0.489582
Reading from 2977: heap size 107 MB, throughput 0.995129
Reading from 2976: heap size 597 MB, throughput 0.0697784
Reading from 2977: heap size 107 MB, throughput 0.996468
Reading from 2976: heap size 666 MB, throughput 0.345571
Reading from 2977: heap size 110 MB, throughput 0.99653
Reading from 2976: heap size 671 MB, throughput 0.858589
Reading from 2976: heap size 669 MB, throughput 0.794215
Reading from 2977: heap size 110 MB, throughput 0.996902
Reading from 2976: heap size 674 MB, throughput 0.799271
Reading from 2976: heap size 676 MB, throughput 0.815939
Reading from 2977: heap size 112 MB, throughput 0.998114
Reading from 2976: heap size 683 MB, throughput 0.755432
Reading from 2977: heap size 112 MB, throughput 0.997354
Reading from 2977: heap size 115 MB, throughput 0.997276
Reading from 2977: heap size 115 MB, throughput 0.997689
Reading from 2976: heap size 690 MB, throughput 0.0594277
Reading from 2976: heap size 764 MB, throughput 0.304545
Reading from 2976: heap size 751 MB, throughput 0.65267
Reading from 2976: heap size 681 MB, throughput 0.37314
Reading from 2976: heap size 748 MB, throughput 0.354647
Reading from 2976: heap size 752 MB, throughput 0.436933
Reading from 2977: heap size 115 MB, throughput 0.998029
Reading from 2976: heap size 752 MB, throughput 0.0534173
Equal recommendation: 3669 MB each
Reading from 2977: heap size 116 MB, throughput 0.992474
Reading from 2976: heap size 831 MB, throughput 0.261519
Reading from 2976: heap size 834 MB, throughput 0.788418
Reading from 2976: heap size 836 MB, throughput 0.902858
Reading from 2976: heap size 835 MB, throughput 0.669429
Reading from 2976: heap size 839 MB, throughput 0.652142
Reading from 2977: heap size 118 MB, throughput 0.998565
Reading from 2976: heap size 837 MB, throughput 0.696063
Reading from 2976: heap size 839 MB, throughput 0.755738
Reading from 2977: heap size 118 MB, throughput 0.998031
Reading from 2976: heap size 841 MB, throughput 0.20176
Reading from 2976: heap size 932 MB, throughput 0.986982
Reading from 2977: heap size 120 MB, throughput 0.99833
Reading from 2976: heap size 933 MB, throughput 0.97826
Reading from 2976: heap size 936 MB, throughput 0.965883
Reading from 2976: heap size 934 MB, throughput 0.97416
Reading from 2976: heap size 768 MB, throughput 0.95472
Reading from 2977: heap size 120 MB, throughput 0.998346
Reading from 2976: heap size 924 MB, throughput 0.944496
Reading from 2976: heap size 806 MB, throughput 0.858403
Reading from 2976: heap size 903 MB, throughput 0.800169
Reading from 2976: heap size 816 MB, throughput 0.693449
Reading from 2976: heap size 895 MB, throughput 0.81682
Reading from 2976: heap size 821 MB, throughput 0.769429
Reading from 2977: heap size 122 MB, throughput 0.998084
Reading from 2976: heap size 887 MB, throughput 0.76683
Reading from 2976: heap size 826 MB, throughput 0.706195
Reading from 2977: heap size 122 MB, throughput 0.992965
Reading from 2976: heap size 883 MB, throughput 0.748039
Reading from 2976: heap size 888 MB, throughput 0.754019
Reading from 2976: heap size 882 MB, throughput 0.748691
Reading from 2977: heap size 124 MB, throughput 0.9896
Reading from 2976: heap size 886 MB, throughput 0.675982
Reading from 2977: heap size 124 MB, throughput 0.978577
Reading from 2976: heap size 881 MB, throughput 0.777254
Reading from 2976: heap size 885 MB, throughput 0.987486
Reading from 2977: heap size 127 MB, throughput 0.997363
Reading from 2977: heap size 128 MB, throughput 0.996301
Reading from 2976: heap size 887 MB, throughput 0.974343
Reading from 2976: heap size 890 MB, throughput 0.822723
Reading from 2977: heap size 132 MB, throughput 0.997125
Reading from 2976: heap size 897 MB, throughput 0.811143
Reading from 2976: heap size 901 MB, throughput 0.79567
Reading from 2976: heap size 909 MB, throughput 0.759446
Reading from 2976: heap size 911 MB, throughput 0.803278
Reading from 2976: heap size 920 MB, throughput 0.812125
Reading from 2977: heap size 133 MB, throughput 0.996504
Reading from 2976: heap size 921 MB, throughput 0.816057
Reading from 2976: heap size 929 MB, throughput 0.831683
Reading from 2976: heap size 929 MB, throughput 0.821708
Reading from 2977: heap size 136 MB, throughput 0.994523
Reading from 2976: heap size 936 MB, throughput 0.902843
Reading from 2976: heap size 937 MB, throughput 0.879781
Reading from 2976: heap size 935 MB, throughput 0.903411
Reading from 2977: heap size 137 MB, throughput 0.995618
Reading from 2976: heap size 939 MB, throughput 0.789696
Equal recommendation: 3669 MB each
Reading from 2976: heap size 938 MB, throughput 0.718999
Reading from 2976: heap size 944 MB, throughput 0.616425
Reading from 2976: heap size 975 MB, throughput 0.727126
Reading from 2976: heap size 975 MB, throughput 0.69193
Reading from 2977: heap size 140 MB, throughput 0.997233
Reading from 2976: heap size 986 MB, throughput 0.729036
Reading from 2976: heap size 986 MB, throughput 0.700714
Reading from 2976: heap size 999 MB, throughput 0.715419
Reading from 2976: heap size 1000 MB, throughput 0.686783
Reading from 2977: heap size 140 MB, throughput 0.994444
Reading from 2976: heap size 1014 MB, throughput 0.690695
Reading from 2976: heap size 1016 MB, throughput 0.677909
Reading from 2976: heap size 1032 MB, throughput 0.778706
Reading from 2977: heap size 144 MB, throughput 0.996545
Reading from 2977: heap size 144 MB, throughput 0.996349
Reading from 2976: heap size 1035 MB, throughput 0.966573
Reading from 2977: heap size 145 MB, throughput 0.997589
Reading from 2977: heap size 146 MB, throughput 0.99765
Reading from 2976: heap size 1052 MB, throughput 0.963599
Reading from 2977: heap size 148 MB, throughput 0.997358
Reading from 2977: heap size 149 MB, throughput 0.997467
Reading from 2976: heap size 1054 MB, throughput 0.960933
Reading from 2977: heap size 151 MB, throughput 0.998105
Reading from 2977: heap size 151 MB, throughput 0.998176
Reading from 2976: heap size 1059 MB, throughput 0.960924
Equal recommendation: 3669 MB each
Reading from 2977: heap size 153 MB, throughput 0.998303
Reading from 2977: heap size 153 MB, throughput 0.998455
Reading from 2976: heap size 1063 MB, throughput 0.959472
Reading from 2977: heap size 155 MB, throughput 0.998547
Reading from 2977: heap size 155 MB, throughput 0.993436
Reading from 2977: heap size 157 MB, throughput 0.992338
Reading from 2976: heap size 1058 MB, throughput 0.934201
Reading from 2977: heap size 157 MB, throughput 0.993788
Reading from 2977: heap size 161 MB, throughput 0.997543
Reading from 2976: heap size 1064 MB, throughput 0.963068
Reading from 2977: heap size 161 MB, throughput 0.997379
Reading from 2977: heap size 164 MB, throughput 0.997633
Reading from 2976: heap size 1054 MB, throughput 0.962492
Reading from 2977: heap size 164 MB, throughput 0.997222
Reading from 2977: heap size 167 MB, throughput 0.997609
Reading from 2977: heap size 167 MB, throughput 0.997073
Equal recommendation: 3669 MB each
Reading from 2977: heap size 170 MB, throughput 0.997248
Reading from 2976: heap size 1061 MB, throughput 0.503588
Reading from 2977: heap size 170 MB, throughput 0.996782
Reading from 2976: heap size 1149 MB, throughput 0.934557
Reading from 2977: heap size 173 MB, throughput 0.997482
Reading from 2977: heap size 173 MB, throughput 0.997184
Reading from 2976: heap size 1152 MB, throughput 0.979491
Reading from 2977: heap size 176 MB, throughput 0.997343
Reading from 2977: heap size 176 MB, throughput 0.996731
Reading from 2976: heap size 1158 MB, throughput 0.97675
Reading from 2977: heap size 178 MB, throughput 0.997377
Reading from 2977: heap size 178 MB, throughput 0.9977
Reading from 2976: heap size 1160 MB, throughput 0.972344
Reading from 2977: heap size 180 MB, throughput 0.998404
Reading from 2977: heap size 181 MB, throughput 0.995673
Equal recommendation: 3669 MB each
Reading from 2977: heap size 183 MB, throughput 0.991076
Reading from 2976: heap size 1154 MB, throughput 0.961177
Reading from 2977: heap size 183 MB, throughput 0.995956
Reading from 2977: heap size 187 MB, throughput 0.997894
Reading from 2976: heap size 1159 MB, throughput 0.970475
Reading from 2977: heap size 188 MB, throughput 0.997654
Reading from 2977: heap size 190 MB, throughput 0.997822
Reading from 2976: heap size 1155 MB, throughput 0.969318
Reading from 2977: heap size 191 MB, throughput 0.997212
Reading from 2976: heap size 1157 MB, throughput 0.968342
Reading from 2977: heap size 194 MB, throughput 0.997883
Reading from 2977: heap size 194 MB, throughput 0.997576
Reading from 2976: heap size 1162 MB, throughput 0.966577
Reading from 2977: heap size 197 MB, throughput 0.997843
Equal recommendation: 3669 MB each
Reading from 2977: heap size 197 MB, throughput 0.997638
Reading from 2976: heap size 1162 MB, throughput 0.965356
Reading from 2977: heap size 200 MB, throughput 0.997924
Reading from 2977: heap size 200 MB, throughput 0.997208
Reading from 2976: heap size 1167 MB, throughput 0.967343
Reading from 2977: heap size 202 MB, throughput 0.997852
Reading from 2977: heap size 202 MB, throughput 0.997465
Reading from 2976: heap size 1170 MB, throughput 0.965641
Reading from 2977: heap size 193 MB, throughput 0.997572
Reading from 2977: heap size 184 MB, throughput 0.809369
Reading from 2977: heap size 195 MB, throughput 0.993156
Reading from 2976: heap size 1175 MB, throughput 0.967154
Reading from 2977: heap size 201 MB, throughput 0.99847
Equal recommendation: 3669 MB each
Reading from 2977: heap size 204 MB, throughput 0.997821
Reading from 2976: heap size 1179 MB, throughput 0.963484
Reading from 2977: heap size 205 MB, throughput 0.998141
Reading from 2976: heap size 1184 MB, throughput 0.960772
Reading from 2977: heap size 210 MB, throughput 0.998551
Reading from 2977: heap size 210 MB, throughput 0.997989
Reading from 2976: heap size 1190 MB, throughput 0.957495
Reading from 2977: heap size 213 MB, throughput 0.998474
Reading from 2977: heap size 214 MB, throughput 0.998157
Reading from 2976: heap size 1197 MB, throughput 0.957769
Reading from 2977: heap size 218 MB, throughput 0.998486
Reading from 2977: heap size 218 MB, throughput 0.998167
Reading from 2976: heap size 1203 MB, throughput 0.961051
Equal recommendation: 3669 MB each
Reading from 2977: heap size 221 MB, throughput 0.998418
Reading from 2976: heap size 1211 MB, throughput 0.958826
Reading from 2977: heap size 221 MB, throughput 0.998128
Reading from 2977: heap size 224 MB, throughput 0.998458
Reading from 2977: heap size 224 MB, throughput 0.99455
Reading from 2976: heap size 1214 MB, throughput 0.947885
Reading from 2977: heap size 227 MB, throughput 0.99207
Reading from 2977: heap size 228 MB, throughput 0.997717
Reading from 2976: heap size 1221 MB, throughput 0.958293
Reading from 2977: heap size 234 MB, throughput 0.997955
Reading from 2977: heap size 235 MB, throughput 0.997908
Reading from 2976: heap size 1224 MB, throughput 0.956994
Equal recommendation: 3669 MB each
Reading from 2977: heap size 239 MB, throughput 0.998206
Reading from 2976: heap size 1231 MB, throughput 0.957035
Reading from 2977: heap size 240 MB, throughput 0.997799
Reading from 2977: heap size 244 MB, throughput 0.998083
Reading from 2977: heap size 244 MB, throughput 0.997679
Reading from 2977: heap size 249 MB, throughput 0.998222
Reading from 2976: heap size 1232 MB, throughput 0.55273
Reading from 2977: heap size 249 MB, throughput 0.997704
Reading from 2976: heap size 1347 MB, throughput 0.946465
Reading from 2977: heap size 253 MB, throughput 0.998091
Equal recommendation: 3669 MB each
Reading from 2977: heap size 253 MB, throughput 0.997655
Reading from 2977: heap size 257 MB, throughput 0.990869
Reading from 2976: heap size 1348 MB, throughput 0.978106
Reading from 2977: heap size 257 MB, throughput 0.99681
Reading from 2976: heap size 1358 MB, throughput 0.977417
Reading from 2977: heap size 264 MB, throughput 0.99808
Reading from 2977: heap size 265 MB, throughput 0.997851
Reading from 2976: heap size 1358 MB, throughput 0.976437
Reading from 2977: heap size 270 MB, throughput 0.998352
Reading from 2977: heap size 271 MB, throughput 0.998031
Reading from 2976: heap size 1357 MB, throughput 0.975321
Equal recommendation: 3669 MB each
Reading from 2977: heap size 275 MB, throughput 0.998295
Reading from 2976: heap size 1360 MB, throughput 0.972151
Reading from 2977: heap size 276 MB, throughput 0.99795
Reading from 2977: heap size 281 MB, throughput 0.998227
Reading from 2976: heap size 1353 MB, throughput 0.968473
Reading from 2977: heap size 281 MB, throughput 0.997997
Reading from 2977: heap size 285 MB, throughput 0.997676
Reading from 2977: heap size 285 MB, throughput 0.971997
Reading from 2976: heap size 1358 MB, throughput 0.964958
Reading from 2977: heap size 289 MB, throughput 0.998142
Equal recommendation: 3669 MB each
Reading from 2976: heap size 1351 MB, throughput 0.97075
Reading from 2977: heap size 290 MB, throughput 0.997753
Reading from 2977: heap size 300 MB, throughput 0.997506
Reading from 2976: heap size 1355 MB, throughput 0.97073
Reading from 2977: heap size 302 MB, throughput 0.997915
Reading from 2977: heap size 311 MB, throughput 0.998163
Reading from 2977: heap size 311 MB, throughput 0.998086
Equal recommendation: 3669 MB each
Reading from 2977: heap size 317 MB, throughput 0.998165
Reading from 2977: heap size 319 MB, throughput 0.99815
Reading from 2977: heap size 325 MB, throughput 0.99562
Reading from 2977: heap size 325 MB, throughput 0.996513
Reading from 2977: heap size 333 MB, throughput 0.998217
Reading from 2977: heap size 333 MB, throughput 0.997901
Equal recommendation: 3669 MB each
Reading from 2977: heap size 339 MB, throughput 0.998267
Reading from 2977: heap size 340 MB, throughput 0.998102
Reading from 2977: heap size 345 MB, throughput 0.998421
Reading from 2977: heap size 346 MB, throughput 0.997193
Reading from 2976: heap size 1360 MB, throughput 0.995202
Reading from 2977: heap size 352 MB, throughput 0.998455
Equal recommendation: 3669 MB each
Reading from 2977: heap size 352 MB, throughput 0.993375
Reading from 2977: heap size 357 MB, throughput 0.998505
Reading from 2977: heap size 358 MB, throughput 0.99812
Reading from 2976: heap size 1361 MB, throughput 0.907694
Reading from 2977: heap size 363 MB, throughput 0.998552
Reading from 2976: heap size 1365 MB, throughput 0.994416
Reading from 2976: heap size 1400 MB, throughput 0.934023
Reading from 2976: heap size 1402 MB, throughput 0.903333
Reading from 2976: heap size 1408 MB, throughput 0.89776
Reading from 2976: heap size 1403 MB, throughput 0.902543
Reading from 2976: heap size 1408 MB, throughput 0.89783
Reading from 2976: heap size 1409 MB, throughput 0.990082
Reading from 2977: heap size 364 MB, throughput 0.997932
Equal recommendation: 3669 MB each
Reading from 2976: heap size 1414 MB, throughput 0.993877
Reading from 2977: heap size 371 MB, throughput 0.998335
Reading from 2977: heap size 371 MB, throughput 0.998067
Reading from 2976: heap size 1434 MB, throughput 0.991546
Reading from 2977: heap size 376 MB, throughput 0.998037
Reading from 2977: heap size 376 MB, throughput 0.99521
Reading from 2976: heap size 1438 MB, throughput 0.989442
Equal recommendation: 3669 MB each
Reading from 2977: heap size 382 MB, throughput 0.99838
Reading from 2976: heap size 1441 MB, throughput 0.986674
Reading from 2977: heap size 382 MB, throughput 0.998318
Reading from 2976: heap size 1445 MB, throughput 0.984715
Reading from 2977: heap size 389 MB, throughput 0.998457
Reading from 2976: heap size 1432 MB, throughput 0.983287
Reading from 2977: heap size 389 MB, throughput 0.998197
Reading from 2977: heap size 396 MB, throughput 0.998233
Equal recommendation: 3669 MB each
Reading from 2976: heap size 1348 MB, throughput 0.979315
Reading from 2977: heap size 396 MB, throughput 0.998142
Reading from 2977: heap size 401 MB, throughput 0.911451
Reading from 2976: heap size 1429 MB, throughput 0.966016
Reading from 2977: heap size 407 MB, throughput 0.998325
Reading from 2976: heap size 1434 MB, throughput 0.977586
Reading from 2977: heap size 416 MB, throughput 0.999065
Equal recommendation: 3669 MB each
Reading from 2976: heap size 1440 MB, throughput 0.97638
Reading from 2977: heap size 416 MB, throughput 0.998903
Reading from 2977: heap size 423 MB, throughput 0.998953
Reading from 2976: heap size 1442 MB, throughput 0.973487
Reading from 2977: heap size 424 MB, throughput 0.99887
Reading from 2976: heap size 1449 MB, throughput 0.973182
Reading from 2977: heap size 431 MB, throughput 0.998968
Equal recommendation: 3669 MB each
Reading from 2977: heap size 431 MB, throughput 0.996608
Reading from 2976: heap size 1457 MB, throughput 0.965954
Reading from 2977: heap size 436 MB, throughput 0.998282
Reading from 2976: heap size 1464 MB, throughput 0.967509
Reading from 2977: heap size 437 MB, throughput 0.998364
Reading from 2976: heap size 1476 MB, throughput 0.966876
Reading from 2977: heap size 445 MB, throughput 0.998563
Equal recommendation: 3669 MB each
Reading from 2976: heap size 1485 MB, throughput 0.96365
Reading from 2977: heap size 446 MB, throughput 0.998279
Reading from 2976: heap size 1499 MB, throughput 0.963947
Reading from 2977: heap size 454 MB, throughput 0.998469
Reading from 2977: heap size 454 MB, throughput 0.998033
Reading from 2976: heap size 1513 MB, throughput 0.965584
Reading from 2977: heap size 462 MB, throughput 0.995163
Equal recommendation: 3669 MB each
Reading from 2976: heap size 1521 MB, throughput 0.966019
Reading from 2977: heap size 462 MB, throughput 0.998162
Reading from 2977: heap size 473 MB, throughput 0.998477
Reading from 2976: heap size 1535 MB, throughput 0.966132
Reading from 2977: heap size 474 MB, throughput 0.99845
Reading from 2976: heap size 1540 MB, throughput 0.966121
Reading from 2977: heap size 482 MB, throughput 0.998486
Equal recommendation: 3669 MB each
Reading from 2976: heap size 1554 MB, throughput 0.965542
Reading from 2977: heap size 483 MB, throughput 0.998578
Reading from 2977: heap size 490 MB, throughput 0.996083
Reading from 2976: heap size 1556 MB, throughput 0.965355
Reading from 2977: heap size 491 MB, throughput 0.99846
Reading from 2976: heap size 1569 MB, throughput 0.963868
Equal recommendation: 3669 MB each
Reading from 2977: heap size 501 MB, throughput 0.99854
Reading from 2976: heap size 1570 MB, throughput 0.965494
Reading from 2977: heap size 502 MB, throughput 0.998447
Reading from 2976: heap size 1582 MB, throughput 0.96725
Reading from 2977: heap size 510 MB, throughput 0.998649
Reading from 2976: heap size 1583 MB, throughput 0.970441
Equal recommendation: 3669 MB each
Client 2977 died
Clients: 1
Reading from 2976: heap size 1592 MB, throughput 0.763427
Reading from 2976: heap size 1738 MB, throughput 0.993951
Recommendation: one client; give it all the memory
Reading from 2976: heap size 1737 MB, throughput 0.993039
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 2976: heap size 1750 MB, throughput 0.991524
Reading from 2976: heap size 1748 MB, throughput 0.978139
Reading from 2976: heap size 1756 MB, throughput 0.838478
Reading from 2976: heap size 1743 MB, throughput 0.824643
Reading from 2976: heap size 1761 MB, throughput 0.804127
Reading from 2976: heap size 1798 MB, throughput 0.876915
Client 2976 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
