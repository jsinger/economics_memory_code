module Economem.Finder (Finder(..),
                        newFinder,
                        finderDone,
                        addReading) where

import Economem.Common

data Reading a = Reading { size :: Integer, value :: a } deriving (Read, Show)

data Phase = Increasing
           | HomeIn Integer Integer -- lower bound, upper bound
           | Match
           deriving (Read, Show)

data Finder a = Finder { targetValue :: a,
                         baseSize :: Integer,
                         currentSize :: Integer,
                         triedSizes :: [Reading a],
                         phase :: Phase }
              deriving (Read, Show)

newFinder :: a -> DataSize -> Finder a
newFinder target base =
  Finder { targetValue = target,
           baseSize = sizeToInt (toMB base),
           currentSize = sizeToInt (toMB base),
           triedSizes = [],
           phase = Increasing }


finderDone :: Finder a -> Maybe DataSize
finderDone f = case phase f of
  Match -> Just (DataSize (currentSize f) MB)
  _ -> Nothing


addReading :: Finder a -> (Finder a -> a -> Bool) -> a -> Finder a
addReading f cond newValue = case phase f of
  -- In this phase, we increase the size exponentially until
  -- we find a size which satisfies the condition
  Increasing -> f { currentSize = newCurrent,
                    triedSizes = newTried,
                    phase = newPhase}
    where
      newCurrent :: Integer
      newCurrent = case newPhase of
        -- Exponential increase
        Increasing -> if currentSize f == baseSize f then
                        baseSize f + 10
                      else
                        baseSize f + ((currentSize f - baseSize f) * 2)

        -- Prepare for binary search
        HomeIn lower upper -> (lower + upper) `div` 2

        -- No overhead
        Match -> currentSize f

      newPhase :: Phase
      newPhase = if cond f newValue then
                   (if currentSize f == baseSize f then
                      Match -- The base size is adequate
                    else
                       -- The size we're looking for is between the last size and this one
                      HomeIn (size (last (triedSizes f))) (size newReading))
                 else Increasing

  -- In this phase, we binary search to find the lowest size
  -- which satisfies the condition
  HomeIn lower upper -> f { currentSize = newCurrent,
                            triedSizes = newTried,
                            phase = newPhase }
    where
      (newPhase, newCurrent)
        | lower == upper = (Match, currentSize f)
        | lower > upper = (Match, upper + 1)
        | otherwise =
            let (newMin, newMax) = if cond f newValue then
                                     (lower, currentSize f - 1)
                                   else
                                     (currentSize f + 1, upper) in
            (HomeIn newMin newMax, (newMin + newMax) `div` 2)

  -- We've already found the target, so don't accept any more readings
  Match -> f

  where
    newTried = triedSizes f ++ [newReading]
    newReading = Reading { size = currentSize f, value = newValue }
