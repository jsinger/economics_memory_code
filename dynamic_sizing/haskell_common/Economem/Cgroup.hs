module Economem.Cgroup (Cgroup(..),
                        setCgroupMemoryLimit,
                        defaultCgroup) where

import Control.Applicative
import Control.Monad
import Economem.Common
import Economem.Process
import System.Directory
import System.FilePath
import qualified System.Exit as E
import qualified System.IO as I
import qualified System.Posix.User as U
import qualified System.Process as P


data Cgroup = Cgroup { cgroupName :: String } deriving (Read, Show)

defaultCgroup :: Cgroup
defaultCgroup = Cgroup "economem"

setCgroupMemoryLimit :: Cgroup -> DataSize -> IO ()
setCgroupMemoryLimit cg size = do
  path <- initialiseCgroup cg
  writeFile
    (path </> "memory.limit_in_bytes")
    (show (sizeToInt (toB size)) ++ "\n")


instance ArgsModifier Cgroup where
  modifyArgsInternal cg (cmd, args) = return ("cgexec", ["-g", "memory:" ++ cgroupName cg, cmd] ++ args)


initialiseCgroup :: Cgroup -> IO FilePath
initialiseCgroup cg = do
  path <- cgroupPath
  reallyInitialise path
  return path

  where
    name :: String
    name = cgroupName cg

    reallyInitialise :: FilePath -> IO ()
    reallyInitialise path = do
      exists <- doesDirectoryExist path
      unless exists
        (do userID <- U.getRealUserID
            userData <- U.getUserEntryForID userID
            groupData <- U.getGroupEntryForID (U.userGroupID userData)
            let userName = U.userName userData
            let groupName = U.groupName groupData
            let userGroup = userName ++ ":" ++ groupName
            putStr "Using sudo to create cgroup; enter your password if prompted: "
            I.hFlush I.stdout
            (exit, _, _) <- P.readProcessWithExitCode "sudo" ["cgcreate",
                                                              "-a",
                                                              userGroup,
                                                              "-t",
                                                              userGroup,
                                                              "-g",
                                                              "memory:" ++ name] ""
            putStrLn ""
            case exit of
             E.ExitSuccess -> return ()
             (E.ExitFailure _) -> fail ("Failed to create cgroup '" ++ name ++ "'"))

    cgroupPath :: IO FilePath
    cgroupPath = (</> name) <$> cgroupsMainPath

    cgroupsMainPath :: IO FilePath
    cgroupsMainPath = do
      path <- P.readProcess "findmnt" ["-n", "-o", "TARGET", "-t", "cgroup", "-O", "memory"] ""
      case lines path of
        [] -> fail "Cgroups not mounted"
        (x:_) -> return x
