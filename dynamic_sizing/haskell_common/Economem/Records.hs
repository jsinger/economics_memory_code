{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE LambdaCase #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Economem.Records where

import Control.Applicative
import qualified Data.ByteString.Char8 as C8
import Data.Char
import Data.Csv
import qualified Data.List as L
import Data.List.Split
import Data.Tuple
import Economem.Common
import Economem.Files
import Text.Read


type Seconds = Double

data Runtime = Runtime Seconds deriving (Show, Read, Eq, Ord)
instance Pretty Runtime where
  pretty (Runtime r) = secondsToTime r ++ if r >= 60 then " (" ++ show r ++ " s)" else ""

instance FromNamedRecord Runtime where
  parseNamedRecord r = Runtime <$>
                       r .: "SECONDS"
instance ToNamedRecord Runtime where
  toNamedRecord = namedRecord . getNamedRecordInput
instance CsvFields Runtime where
  getCsvFieldNames _ = ["SECONDS"]
  getNamedRecordInput (Runtime time) = ["SECONDS" .= time]
instance Fileable Runtime where
  readForFile s = Runtime <$> readForFile s
  showForFile (Runtime r) = showForFile r
fromRuntime :: Runtime -> Seconds
fromRuntime (Runtime r) = r


data UncertainRuntime = UncertainRuntime { uncVal :: Seconds, uncErr :: Seconds } deriving (Eq, Ord)
(+/-) :: Seconds -> Seconds -> UncertainRuntime
(+/-) = UncertainRuntime

instance Show UncertainRuntime where
  show r = show (uncVal r) ++ " +/- " ++ show (uncErr r)
instance Read UncertainRuntime where
  readsPrec d = readParen False $ \r -> do
    (v, r') <- readsPrec d r
    ("+/-", r'') <- lex r'
    (e, r''')  <- readsPrec d r''
    return (v +/- e, r''')
instance Pretty UncertainRuntime where
  pretty r = "(" ++ show r ++ ") s"

instance FromNamedRecord UncertainRuntime where
  parseNamedRecord r = read <$>
                       r .: "SECONDS"
instance ToNamedRecord UncertainRuntime where
  toNamedRecord = namedRecord . getNamedRecordInput
instance CsvFields UncertainRuntime where
  getCsvFieldNames _ = ["SECONDS"]
  getNamedRecordInput r = ["SECONDS" .= show r]
instance FromField UncertainRuntime where
  parseField f = case readMaybe (C8.unpack f) of
    Just r -> return r
    Nothing -> fail "Cannot parse uncertain runtime"
instance ToField UncertainRuntime where
  toField = C8.pack . show
instance Fileable UncertainRuntime


data TimerType = Unconstrained
               | StaticEqual
               | CgroupSimple
               | CgroupFair
               | DaemonEqual
               | DaemonSimple
               | DaemonVengerov
               | DaemonAvgVengerov
               | DaemonMajorOnly
               deriving (Read, Show, Eq, Ord)

timerNames :: [(TimerType, String)]
timerNames = L.zip timers (L.map timerToName timers)
  where
    timers = [Unconstrained,
              StaticEqual,
              CgroupSimple,
              CgroupFair,
              DaemonEqual,
              DaemonSimple,
              DaemonVengerov,
              DaemonAvgVengerov,
              DaemonMajorOnly]

timerToName :: TimerType -> String
timerToName t = toLower (L.head s) : L.tail s
  where
    s = show t

timerFromName :: Monad m => String -> m TimerType
timerFromName s = case L.lookup s flipped of
  Just t -> return t
  Nothing -> fail $ "Invalid timer type '" ++ s ++ "'"
  where
    flipped :: [(String, TimerType)]
    flipped = L.map swap timerNames

instance FromNamedRecord TimerType where
  parseNamedRecord r = timerFromName =<<
                       r .: "TIMER"
instance ToNamedRecord TimerType where
  toNamedRecord r = namedRecord (getNamedRecordInput r)
instance CsvFields TimerType where
  getCsvFieldNames _ = ["TIMER"]
  getNamedRecordInput timer = ["TIMER" .= timerToName timer]
instance Fileable TimerType where
  readForFile = timerFromName
  showForFile = timerToName


data Benchmark = Benchmark { bmName :: String, bmSize :: String } deriving (Show, Read, Eq)
instance Pretty Benchmark where
  pretty bm = bmName bm ++ ", " ++ bmSize bm
instance FromNamedRecord Benchmark where
  parseNamedRecord r = Benchmark <$>
                       r .: "BENCHMARK" <*>
                       r .: "SIZE"
instance ToNamedRecord Benchmark where
  toNamedRecord bm = namedRecord (getNamedRecordInput bm)
instance CsvFields Benchmark where
  getCsvFieldNames _ = ["BENCHMARK", "SIZE"]
  getNamedRecordInput (Benchmark name size) = ["BENCHMARK" .= name, "SIZE" .= size]


data BenchmarkThreads = BenchmarkThreads { btBenchmark :: Benchmark, btThreads :: Int } deriving (Show, Read, Eq)
instance Pretty BenchmarkThreads where
  pretty bm = pretty (btBenchmark bm) ++ ", " ++ show (btThreads bm)
instance FromNamedRecord BenchmarkThreads where
  parseNamedRecord r = BenchmarkThreads <$>
                       parseNamedRecord r <*>
                       r .: "THREADS"
instance ToNamedRecord BenchmarkThreads where
  toNamedRecord bm = namedRecord (getNamedRecordInput bm)
instance CsvFields BenchmarkThreads where
  getCsvFieldNames _ = getCsvFieldNames (Nothing :: Maybe Benchmark) ++ ["THREADS"]
  getNamedRecordInput (BenchmarkThreads bm threads) = getNamedRecordInput bm ++ ["THREADS" .= threads]


data BenchmarkThreadsIters = BenchmarkThreadsIters { btiBenchmark :: BenchmarkThreads, btiIters :: Int } deriving (Show, Read, Eq)
instance Pretty BenchmarkThreadsIters where
  pretty bm = pretty (btiBenchmark bm) ++ ", " ++ show (btiIters bm)
instance FromNamedRecord BenchmarkThreadsIters where
  parseNamedRecord r = BenchmarkThreadsIters <$>
                       parseNamedRecord r <*>
                       r .: "ITERATIONS"
instance ToNamedRecord BenchmarkThreadsIters where
  toNamedRecord bm = namedRecord (getNamedRecordInput bm)
instance CsvFields BenchmarkThreadsIters where
  getCsvFieldNames _ = getCsvFieldNames (Nothing :: Maybe BenchmarkThreads) ++ ["ITERATIONS"]
  getNamedRecordInput (BenchmarkThreadsIters bm iters) = getNamedRecordInput bm ++ ["ITERATIONS" .= iters]




data BenchmarkCombination = BenchmarkCombination { bcBenchmarks :: [BenchmarkThreadsIters] } deriving (Show, Read, Eq)
instance Pretty BenchmarkCombination where
  pretty (BenchmarkCombination bms) = L.intercalate "|" (map pretty bms)

instance FromNamedRecord BenchmarkCombination where
  parseNamedRecord r = BenchmarkCombination <$>
                       r .: "BENCHMARKS"
instance ToNamedRecord BenchmarkCombination where
  toNamedRecord r = namedRecord (getNamedRecordInput r)
instance CsvFields BenchmarkCombination where
  getCsvFieldNames _ = ["BENCHMARKS"]
  getNamedRecordInput (BenchmarkCombination bms) = ["BENCHMARKS" .= bms]

instance FromField [BenchmarkThreadsIters] where
  parseField f = case parseBenchmarks (C8.unpack f) of
    Just bms -> return bms
    Nothing -> fail "Cannot parse benchmark list"
    where
      parseBenchmarks :: String -> Maybe [BenchmarkThreadsIters]
      parseBenchmarks = mapM parseOne . wordsBy (== '|')

      parseOne :: String -> Maybe BenchmarkThreadsIters
      parseOne s = case wordsBy (== ':') s of
        (name:size:t:i:[]) ->
          do threads <- readMaybe t
             iters <- readMaybe i
             Just (BenchmarkThreadsIters (BenchmarkThreads (Benchmark name size) threads) iters)
        _ -> Nothing

instance ToField [BenchmarkThreadsIters] where
  toField = C8.pack . L.intercalate "|" . L.map flattenOne
    where
      flattenOne :: BenchmarkThreadsIters -> String
      flattenOne (BenchmarkThreadsIters (BenchmarkThreads (Benchmark name size) threads) iters) =
        name ++ ":" ++ size ++ ":" ++ show threads ++ ":" ++ show iters




data BenchmarkThreadsRuntime = BenchmarkThreadsRuntime { btrBenchmark :: BenchmarkThreads, btrRuntime :: Runtime } deriving (Show, Read, Eq)
instance Pretty BenchmarkThreadsRuntime where
  pretty bm = pretty (btrBenchmark bm) ++ ", " ++ pretty (btrRuntime bm)
instance FromNamedRecord BenchmarkThreadsRuntime where
  parseNamedRecord r = BenchmarkThreadsRuntime <$>
                       parseNamedRecord r <*>
                       parseNamedRecord r
instance ToNamedRecord BenchmarkThreadsRuntime where
  toNamedRecord bm = namedRecord (getNamedRecordInput bm)
instance CsvFields BenchmarkThreadsRuntime where
  getCsvFieldNames _ = getCsvFieldNames (Nothing :: Maybe BenchmarkThreads) ++
                       getCsvFieldNames (Nothing :: Maybe Runtime)
  getNamedRecordInput (BenchmarkThreadsRuntime bm runtime) = getNamedRecordInput bm ++
                                                             getNamedRecordInput runtime


data BenchmarkThreadsMBCrashes = BenchmarkThreadsMBCrashes { btcBenchmark :: BenchmarkThreadsMB, btcCrashes :: Bool } deriving (Show, Read, Eq)
instance Pretty BenchmarkThreadsMBCrashes where
  pretty bm = pretty (btcBenchmark bm) ++ ", " ++ show (btcCrashes bm)
instance Fileable BenchmarkThreadsMBCrashes
instance FromNamedRecord BenchmarkThreadsMBCrashes where
  parseNamedRecord r = BenchmarkThreadsMBCrashes <$>
                       parseNamedRecord r <*>
                       r .: "CRASHES"
instance ToNamedRecord BenchmarkThreadsMBCrashes where
  toNamedRecord bm = namedRecord (getNamedRecordInput bm)
instance CsvFields BenchmarkThreadsMBCrashes where
  getCsvFieldNames _ = getCsvFieldNames (Nothing :: Maybe BenchmarkThreadsMB) ++ ["CRASHES"]
  getNamedRecordInput (BenchmarkThreadsMBCrashes bm crashes) = getNamedRecordInput bm ++ ["CRASHES" .= crashes]
instance FromField Bool where
  parseField f = case readMaybe (C8.unpack f) of
    Just b -> return b
    Nothing -> fail "Cannot parse bool"
instance ToField Bool where
  toField = C8.pack . show

data BenchmarkThreadsMB = BenchmarkThreadsMB { btmBenchmark :: BenchmarkThreads, btmMB :: Int } deriving (Show, Read, Eq)
instance Pretty BenchmarkThreadsMB where
  pretty bm = pretty (btmBenchmark bm) ++ ", " ++ show (btmMB bm)
instance Fileable BenchmarkThreadsMB
instance FromNamedRecord BenchmarkThreadsMB where
  parseNamedRecord r = BenchmarkThreadsMB <$>
                       parseNamedRecord r <*>
                       r .: "SIZE_MB"
instance ToNamedRecord BenchmarkThreadsMB where
  toNamedRecord bm = namedRecord (getNamedRecordInput bm)
instance CsvFields BenchmarkThreadsMB where
  getCsvFieldNames _ = getCsvFieldNames (Nothing :: Maybe BenchmarkThreads) ++ ["SIZE_MB"]
  getNamedRecordInput (BenchmarkThreadsMB bm mb) = getNamedRecordInput bm ++ ["SIZE_MB" .= mb]


data BenchmarkThreadsMBRuntime = BenchmarkThreadsMBRuntime { btmrBenchmark :: BenchmarkThreadsMB, btmrRuntime :: Runtime } deriving (Show, Read, Eq)
instance Pretty BenchmarkThreadsMBRuntime where
  pretty bm = pretty (btmrBenchmark bm) ++ ", " ++ pretty (btmrRuntime bm)
instance FromNamedRecord BenchmarkThreadsMBRuntime where
  parseNamedRecord r = BenchmarkThreadsMBRuntime <$>
                       parseNamedRecord r <*>
                       parseNamedRecord r
instance ToNamedRecord BenchmarkThreadsMBRuntime where
  toNamedRecord bm = namedRecord (getNamedRecordInput bm)
instance CsvFields BenchmarkThreadsMBRuntime where
  getCsvFieldNames _ = getCsvFieldNames (Nothing :: Maybe BenchmarkThreadsMB) ++
                       getCsvFieldNames (Nothing :: Maybe Runtime)
  getNamedRecordInput (BenchmarkThreadsMBRuntime bm runtime) = getNamedRecordInput bm ++
                                                               getNamedRecordInput runtime


data BenchmarkCombinationMB = BenchmarkCombinationMB { bcmBenchmarks :: BenchmarkCombination, bcmMB :: Int } deriving (Show, Read, Eq)
instance FromNamedRecord BenchmarkCombinationMB where
  parseNamedRecord r = BenchmarkCombinationMB <$>
                       parseNamedRecord r <*>
                       r .: "SIZE_MB"
instance ToNamedRecord BenchmarkCombinationMB where
  toNamedRecord bm = namedRecord (getNamedRecordInput bm)
instance CsvFields BenchmarkCombinationMB where
  getCsvFieldNames _ = getCsvFieldNames (Nothing :: Maybe BenchmarkCombination) ++ ["SIZE_MB"]
  getNamedRecordInput (BenchmarkCombinationMB bm mb) = getNamedRecordInput bm ++ ["SIZE_MB" .= mb]


data TimerRuntime = TimerRuntime { trTimer :: TimerType, trRuntime :: UncertainRuntime } deriving (Show, Read, Eq)
instance Pretty TimerRuntime where
  pretty tr = timerToName (trTimer tr) ++ ", " ++ pretty (trRuntime tr)
instance FromNamedRecord TimerRuntime where
  parseNamedRecord r = TimerRuntime <$>
                       parseNamedRecord r <*>
                       parseNamedRecord r
instance ToNamedRecord TimerRuntime where
  toNamedRecord tr = namedRecord (getNamedRecordInput tr)
instance CsvFields TimerRuntime where
  getCsvFieldNames _ = getCsvFieldNames (Nothing :: Maybe TimerType) ++ getCsvFieldNames (Nothing :: Maybe Runtime)
  getNamedRecordInput (TimerRuntime t r) = getNamedRecordInput t ++ getNamedRecordInput r


data BenchmarkFilter = BenchmarkFilter { filterName :: String, filterSize :: String } deriving (Show, Eq)
instance FromNamedRecord BenchmarkFilter where
  parseNamedRecord r = BenchmarkFilter <$>
                       r .: "BENCHMARK" <*>
                       r .: "SIZE"
instance ToNamedRecord BenchmarkFilter where
  toNamedRecord bm = namedRecord (getNamedRecordInput bm)
instance CsvFields BenchmarkFilter where
  getCsvFieldNames _ = ["BENCHMARK", "SIZE"]
  getNamedRecordInput (BenchmarkFilter name size) = ["BENCHMARK" .= name, "SIZE" .= size]



iterationsInTimeAll :: Integer -> [BenchmarkThreadsRuntime] -> [BenchmarkThreadsIters]
iterationsInTimeAll seconds = L.map (iterationsInTime seconds)

iterationsInTime :: Integer -> BenchmarkThreadsRuntime -> BenchmarkThreadsIters
iterationsInTime seconds bm = BenchmarkThreadsIters
                              (btrBenchmark bm)
                              (round (fromIntegral seconds / fromRuntime (btrRuntime bm))::Int)


data PidName = PidName { pnPid :: Integer,
                         pnName :: String }

instance FromNamedRecord PidName where
  parseNamedRecord r = PidName <$>
                       r .: "PID" <*>
                       r .: "NAME"
instance ToNamedRecord PidName where
  toNamedRecord tr = namedRecord (getNamedRecordInput tr)
instance CsvFields PidName where
  getCsvFieldNames _ = ["PID",
                        "NAME"]
  getNamedRecordInput (PidName p n) =
    ["PID" .= p,
     "NAME" .= n]
