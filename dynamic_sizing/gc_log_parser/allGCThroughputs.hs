import EconoGCLog

main :: IO ()
main = interact (\s -> unlines (map (show . throughput) (parseGCs (lines s))))
