economemd
    total memory: 1800 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_09_11_14_34/sub1_timerComparisonSingle/sub4_timeDacapoDaemon/daemon_run3_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 7003: heap size 9 MB, throughput 0.99237
Clients: 1
Client 7003 has a minimum heap size of 276 MB
Reading from 7002: heap size 9 MB, throughput 0.990673
Clients: 2
Client 7002 has a minimum heap size of 1223 MB
Reading from 7002: heap size 9 MB, throughput 0.987843
Reading from 7003: heap size 9 MB, throughput 0.988088
Reading from 7002: heap size 9 MB, throughput 0.982459
Reading from 7003: heap size 9 MB, throughput 0.981825
Reading from 7002: heap size 9 MB, throughput 0.976308
Reading from 7003: heap size 9 MB, throughput 0.974102
Reading from 7002: heap size 11 MB, throughput 0.979637
Reading from 7003: heap size 11 MB, throughput 0.957029
Reading from 7002: heap size 11 MB, throughput 0.979083
Reading from 7003: heap size 11 MB, throughput 0.943942
Reading from 7003: heap size 17 MB, throughput 0.899393
Reading from 7002: heap size 17 MB, throughput 0.931002
Reading from 7002: heap size 17 MB, throughput 0.875921
Reading from 7003: heap size 17 MB, throughput 0.854339
Reading from 7003: heap size 30 MB, throughput 0.68263
Reading from 7002: heap size 30 MB, throughput 0.80512
Reading from 7003: heap size 31 MB, throughput 0.339013
Reading from 7002: heap size 31 MB, throughput 0.511872
Reading from 7003: heap size 34 MB, throughput 0.635819
Reading from 7002: heap size 35 MB, throughput 0.703705
Reading from 7003: heap size 46 MB, throughput 0.367132
Reading from 7002: heap size 48 MB, throughput 0.753643
Reading from 7002: heap size 51 MB, throughput 0.388463
Reading from 7003: heap size 49 MB, throughput 0.52152
Reading from 7002: heap size 52 MB, throughput 0.458729
Reading from 7002: heap size 74 MB, throughput 0.425163
Reading from 7003: heap size 63 MB, throughput 0.441404
Reading from 7002: heap size 75 MB, throughput 0.194525
Reading from 7003: heap size 67 MB, throughput 0.377252
Reading from 7002: heap size 80 MB, throughput 0.349409
Reading from 7003: heap size 69 MB, throughput 0.451738
Reading from 7002: heap size 102 MB, throughput 0.0849861
Reading from 7003: heap size 97 MB, throughput 0.485948
Reading from 7002: heap size 107 MB, throughput 0.167345
Reading from 7002: heap size 109 MB, throughput 0.673166
Reading from 7003: heap size 97 MB, throughput 0.405645
Reading from 7003: heap size 128 MB, throughput 0.760593
Reading from 7002: heap size 112 MB, throughput 0.411827
Reading from 7003: heap size 128 MB, throughput 0.712915
Reading from 7003: heap size 129 MB, throughput 0.430544
Reading from 7002: heap size 142 MB, throughput 0.738651
Reading from 7003: heap size 133 MB, throughput 0.422623
Reading from 7002: heap size 147 MB, throughput 0.64053
Reading from 7002: heap size 150 MB, throughput 0.642254
Reading from 7002: heap size 154 MB, throughput 0.57967
Reading from 7002: heap size 161 MB, throughput 0.593941
Reading from 7003: heap size 135 MB, throughput 0.533112
Reading from 7003: heap size 171 MB, throughput 0.380591
Reading from 7003: heap size 175 MB, throughput 0.233894
Reading from 7002: heap size 165 MB, throughput 0.456077
Reading from 7003: heap size 178 MB, throughput 0.662881
Reading from 7002: heap size 201 MB, throughput 0.600216
Reading from 7003: heap size 187 MB, throughput 0.735637
Reading from 7002: heap size 209 MB, throughput 0.632823
Reading from 7002: heap size 211 MB, throughput 0.59131
Reading from 7002: heap size 214 MB, throughput 0.547448
Reading from 7003: heap size 189 MB, throughput 0.445275
Reading from 7002: heap size 221 MB, throughput 0.333168
Reading from 7002: heap size 270 MB, throughput 0.534158
Reading from 7002: heap size 272 MB, throughput 0.573771
Reading from 7003: heap size 228 MB, throughput 0.9051
Reading from 7002: heap size 275 MB, throughput 0.637285
Reading from 7003: heap size 236 MB, throughput 0.89466
Reading from 7003: heap size 240 MB, throughput 0.900695
Reading from 7002: heap size 276 MB, throughput 0.352529
Reading from 7003: heap size 243 MB, throughput 0.890238
Reading from 7002: heap size 329 MB, throughput 0.609945
Reading from 7003: heap size 247 MB, throughput 0.849826
Reading from 7002: heap size 331 MB, throughput 0.616067
Reading from 7003: heap size 249 MB, throughput 0.794918
Reading from 7002: heap size 332 MB, throughput 0.586763
Reading from 7003: heap size 253 MB, throughput 0.839022
Reading from 7002: heap size 336 MB, throughput 0.629924
Reading from 7003: heap size 254 MB, throughput 0.837533
Reading from 7002: heap size 342 MB, throughput 0.627292
Reading from 7003: heap size 255 MB, throughput 0.768057
Reading from 7003: heap size 259 MB, throughput 0.70126
Reading from 7003: heap size 265 MB, throughput 0.738182
Reading from 7003: heap size 268 MB, throughput 0.70974
Reading from 7002: heap size 350 MB, throughput 0.385833
Reading from 7002: heap size 406 MB, throughput 0.556552
Reading from 7003: heap size 274 MB, throughput 0.845208
Reading from 7002: heap size 412 MB, throughput 0.599343
Reading from 7002: heap size 414 MB, throughput 0.586521
Reading from 7003: heap size 276 MB, throughput 0.873732
Reading from 7002: heap size 418 MB, throughput 0.534495
Reading from 7002: heap size 423 MB, throughput 0.529955
Reading from 7002: heap size 432 MB, throughput 0.496786
Reading from 7003: heap size 274 MB, throughput 0.70249
Reading from 7003: heap size 317 MB, throughput 0.779628
Numeric result:
Recommendation: 2 clients, utility 0.488295:
    h1: 577 MB (U(h) = 0.501721*h^0.0466594)
    h2: 1223 MB (U(h) = 0.718286*h^0.001)
Recommendation: 2 clients, utility 0.488295:
    h1: 577 MB (U(h) = 0.501721*h^0.0466594)
    h2: 1223 MB (U(h) = 0.718286*h^0.001)
Reading from 7003: heap size 319 MB, throughput 0.812795
Reading from 7003: heap size 319 MB, throughput 0.839824
Reading from 7002: heap size 439 MB, throughput 0.315165
Reading from 7003: heap size 323 MB, throughput 0.913519
Reading from 7002: heap size 502 MB, throughput 0.491215
Reading from 7003: heap size 324 MB, throughput 0.9217
Reading from 7002: heap size 503 MB, throughput 0.566517
Reading from 7003: heap size 328 MB, throughput 0.810385
Reading from 7003: heap size 328 MB, throughput 0.773143
Reading from 7003: heap size 326 MB, throughput 0.702777
Reading from 7003: heap size 328 MB, throughput 0.63866
Reading from 7003: heap size 332 MB, throughput 0.5405
Reading from 7003: heap size 333 MB, throughput 0.600755
Reading from 7002: heap size 506 MB, throughput 0.246946
Reading from 7002: heap size 571 MB, throughput 0.541871
Reading from 7002: heap size 573 MB, throughput 0.587585
Reading from 7002: heap size 569 MB, throughput 0.614079
Reading from 7002: heap size 571 MB, throughput 0.620295
Reading from 7002: heap size 570 MB, throughput 0.576387
Reading from 7002: heap size 581 MB, throughput 0.516327
Reading from 7003: heap size 341 MB, throughput 0.960801
Reading from 7002: heap size 585 MB, throughput 0.48305
Reading from 7002: heap size 601 MB, throughput 0.436519
Reading from 7003: heap size 342 MB, throughput 0.956315
Reading from 7002: heap size 610 MB, throughput 0.367981
Reading from 7002: heap size 690 MB, throughput 0.406173
Reading from 7002: heap size 687 MB, throughput 0.564897
Reading from 7002: heap size 695 MB, throughput 0.753839
Reading from 7003: heap size 346 MB, throughput 0.969765
Reading from 7002: heap size 697 MB, throughput 0.799434
Reading from 7002: heap size 698 MB, throughput 0.821018
Reading from 7003: heap size 349 MB, throughput 0.962841
Reading from 7002: heap size 700 MB, throughput 0.737756
Reading from 7002: heap size 712 MB, throughput 0.679695
Reading from 7003: heap size 348 MB, throughput 0.968073
Numeric result:
Recommendation: 2 clients, utility 0.45999:
    h1: 577 MB (U(h) = 0.415943*h^0.0950266)
    h2: 1223 MB (U(h) = 0.600125*h^0.001)
Recommendation: 2 clients, utility 0.45999:
    h1: 577 MB (U(h) = 0.415943*h^0.0950266)
    h2: 1223 MB (U(h) = 0.600125*h^0.001)
Reading from 7002: heap size 718 MB, throughput 0.506462
Reading from 7002: heap size 802 MB, throughput 0.416032
Reading from 7002: heap size 791 MB, throughput 0.40656
Reading from 7002: heap size 695 MB, throughput 0.40453
Reading from 7003: heap size 351 MB, throughput 0.963552
Reading from 7002: heap size 793 MB, throughput 0.201809
Reading from 7002: heap size 883 MB, throughput 0.638356
Reading from 7002: heap size 887 MB, throughput 0.613439
Reading from 7002: heap size 887 MB, throughput 0.605783
Reading from 7002: heap size 891 MB, throughput 0.617002
Reading from 7003: heap size 352 MB, throughput 0.968057
Reading from 7002: heap size 892 MB, throughput 0.808516
Reading from 7002: heap size 894 MB, throughput 0.822561
Reading from 7002: heap size 896 MB, throughput 0.477246
Reading from 7003: heap size 353 MB, throughput 0.969249
Reading from 7002: heap size 992 MB, throughput 0.897471
Reading from 7002: heap size 994 MB, throughput 0.92265
Reading from 7002: heap size 1000 MB, throughput 0.916596
Reading from 7002: heap size 836 MB, throughput 0.876375
Reading from 7002: heap size 979 MB, throughput 0.839338
Reading from 7002: heap size 841 MB, throughput 0.802917
Reading from 7002: heap size 969 MB, throughput 0.780105
Reading from 7002: heap size 848 MB, throughput 0.74989
Reading from 7003: heap size 358 MB, throughput 0.968454
Reading from 7002: heap size 960 MB, throughput 0.789556
Reading from 7002: heap size 968 MB, throughput 0.82394
Reading from 7002: heap size 954 MB, throughput 0.828209
Reading from 7002: heap size 961 MB, throughput 0.830108
Reading from 7002: heap size 948 MB, throughput 0.914342
Reading from 7003: heap size 358 MB, throughput 0.970495
Reading from 7002: heap size 955 MB, throughput 0.972069
Reading from 7003: heap size 362 MB, throughput 0.965261
Reading from 7002: heap size 957 MB, throughput 0.942981
Reading from 7002: heap size 960 MB, throughput 0.897664
Reading from 7002: heap size 966 MB, throughput 0.868791
Numeric result:
Recommendation: 2 clients, utility 0.493918:
    h1: 577 MB (U(h) = 0.379467*h^0.118294)
    h2: 1223 MB (U(h) = 0.406053*h^0.0580636)
Recommendation: 2 clients, utility 0.493918:
    h1: 577 MB (U(h) = 0.379467*h^0.118294)
    h2: 1223 MB (U(h) = 0.406053*h^0.0580636)
Reading from 7002: heap size 967 MB, throughput 0.835857
Reading from 7002: heap size 971 MB, throughput 0.809769
Reading from 7002: heap size 972 MB, throughput 0.809424
Reading from 7003: heap size 364 MB, throughput 0.960311
Reading from 7002: heap size 975 MB, throughput 0.805379
Reading from 7002: heap size 977 MB, throughput 0.814143
Reading from 7002: heap size 980 MB, throughput 0.840904
Reading from 7002: heap size 983 MB, throughput 0.883752
Reading from 7002: heap size 982 MB, throughput 0.839027
Reading from 7003: heap size 367 MB, throughput 0.962203
Reading from 7002: heap size 986 MB, throughput 0.772524
Reading from 7002: heap size 984 MB, throughput 0.727656
Reading from 7002: heap size 1003 MB, throughput 0.673188
Reading from 7002: heap size 1017 MB, throughput 0.6395
Reading from 7002: heap size 1027 MB, throughput 0.635274
Reading from 7002: heap size 1029 MB, throughput 0.636112
Reading from 7002: heap size 1033 MB, throughput 0.616838
Reading from 7002: heap size 1044 MB, throughput 0.640073
Reading from 7002: heap size 1046 MB, throughput 0.621537
Reading from 7002: heap size 1064 MB, throughput 0.62656
Reading from 7003: heap size 369 MB, throughput 0.975627
Reading from 7003: heap size 373 MB, throughput 0.962453
Reading from 7003: heap size 374 MB, throughput 0.946066
Reading from 7003: heap size 372 MB, throughput 0.929174
Reading from 7003: heap size 377 MB, throughput 0.886307
Reading from 7003: heap size 386 MB, throughput 0.93791
Reading from 7002: heap size 1064 MB, throughput 0.941278
Reading from 7003: heap size 388 MB, throughput 0.976246
Numeric result:
Recommendation: 2 clients, utility 0.627629:
    h1: 577 MB (U(h) = 0.341996*h^0.144172)
    h2: 1223 MB (U(h) = 0.130784*h^0.242611)
Recommendation: 2 clients, utility 0.627629:
    h1: 577 MB (U(h) = 0.341996*h^0.144172)
    h2: 1223 MB (U(h) = 0.130784*h^0.242611)
Reading from 7002: heap size 1085 MB, throughput 0.960785
Reading from 7003: heap size 389 MB, throughput 0.980725
Reading from 7003: heap size 392 MB, throughput 0.980671
Reading from 7002: heap size 1087 MB, throughput 0.962793
Reading from 7003: heap size 390 MB, throughput 0.982055
Reading from 7003: heap size 393 MB, throughput 0.982251
Reading from 7002: heap size 1093 MB, throughput 0.969244
Reading from 7003: heap size 389 MB, throughput 0.977402
Numeric result:
Recommendation: 2 clients, utility 0.676035:
    h1: 577 MB (U(h) = 0.32838*h^0.154108)
    h2: 1223 MB (U(h) = 0.0867815*h^0.307585)
Recommendation: 2 clients, utility 0.676035:
    h1: 577 MB (U(h) = 0.32838*h^0.154108)
    h2: 1223 MB (U(h) = 0.0867815*h^0.307585)
Reading from 7003: heap size 392 MB, throughput 0.966305
Reading from 7002: heap size 1098 MB, throughput 0.967172
Reading from 7003: heap size 391 MB, throughput 0.945456
Reading from 7002: heap size 1092 MB, throughput 0.968522
Reading from 7003: heap size 431 MB, throughput 0.970684
Reading from 7003: heap size 432 MB, throughput 0.976606
Reading from 7002: heap size 1099 MB, throughput 0.970017
Reading from 7003: heap size 434 MB, throughput 0.983743
Reading from 7003: heap size 434 MB, throughput 0.974673
Reading from 7003: heap size 436 MB, throughput 0.956674
Reading from 7003: heap size 442 MB, throughput 0.934832
Numeric result:
Recommendation: 2 clients, utility 0.699099:
    h1: 577 MB (U(h) = 0.314306*h^0.164485)
    h2: 1223 MB (U(h) = 0.0865735*h^0.309523)
Recommendation: 2 clients, utility 0.699099:
    h1: 577 MB (U(h) = 0.314306*h^0.164485)
    h2: 1223 MB (U(h) = 0.0865735*h^0.309523)
Reading from 7002: heap size 1089 MB, throughput 0.969771
Reading from 7003: heap size 443 MB, throughput 0.972288
Reading from 7003: heap size 450 MB, throughput 0.981159
Reading from 7002: heap size 1096 MB, throughput 0.968583
Reading from 7003: heap size 452 MB, throughput 0.982582
Reading from 7003: heap size 454 MB, throughput 0.985985
Reading from 7002: heap size 1095 MB, throughput 0.969536
Reading from 7003: heap size 456 MB, throughput 0.985604
Numeric result:
Recommendation: 2 clients, utility 0.695023:
    h1: 577 MB (U(h) = 0.303699*h^0.172471)
    h2: 1223 MB (U(h) = 0.135437*h^0.243437)
Recommendation: 2 clients, utility 0.695023:
    h1: 577 MB (U(h) = 0.303699*h^0.172471)
    h2: 1223 MB (U(h) = 0.135437*h^0.243437)
Reading from 7002: heap size 1097 MB, throughput 0.96513
Reading from 7003: heap size 455 MB, throughput 0.985375
Reading from 7003: heap size 458 MB, throughput 0.985329
Reading from 7002: heap size 1103 MB, throughput 0.967084
Reading from 7003: heap size 460 MB, throughput 0.98306
Reading from 7003: heap size 461 MB, throughput 0.980806
Numeric result:
Recommendation: 2 clients, utility 0.71646:
    h1: 577 MB (U(h) = 0.296797*h^0.17779)
    h2: 1223 MB (U(h) = 0.112531*h^0.27225)
Recommendation: 2 clients, utility 0.71646:
    h1: 577 MB (U(h) = 0.296797*h^0.17779)
    h2: 1223 MB (U(h) = 0.112531*h^0.27225)
Reading from 7002: heap size 1104 MB, throughput 0.905414
Reading from 7003: heap size 464 MB, throughput 0.985913
Reading from 7003: heap size 466 MB, throughput 0.977639
Reading from 7003: heap size 467 MB, throughput 0.963853
Reading from 7003: heap size 469 MB, throughput 0.973562
Reading from 7002: heap size 1197 MB, throughput 0.959339
Reading from 7003: heap size 477 MB, throughput 0.984397
Reading from 7003: heap size 478 MB, throughput 0.986831
Reading from 7002: heap size 1201 MB, throughput 0.968078
Numeric result:
Recommendation: 2 clients, utility 0.752083:
    h1: 577 MB (U(h) = 0.288934*h^0.183948)
    h2: 1223 MB (U(h) = 0.0719776*h^0.340205)
Recommendation: 2 clients, utility 0.752083:
    h1: 577 MB (U(h) = 0.288934*h^0.183948)
    h2: 1223 MB (U(h) = 0.0719776*h^0.340205)
Reading from 7003: heap size 480 MB, throughput 0.987543
Reading from 7002: heap size 1204 MB, throughput 0.974871
Reading from 7003: heap size 482 MB, throughput 0.98772
Reading from 7003: heap size 481 MB, throughput 0.986677
Reading from 7002: heap size 1206 MB, throughput 0.975952
Reading from 7003: heap size 483 MB, throughput 0.986313
Reading from 7002: heap size 1201 MB, throughput 0.977269
Numeric result:
Recommendation: 2 clients, utility 0.773098:
    h1: 577 MB (U(h) = 0.284305*h^0.187621)
    h2: 1223 MB (U(h) = 0.0558429*h^0.378771)
Recommendation: 2 clients, utility 0.773098:
    h1: 577 MB (U(h) = 0.284305*h^0.187621)
    h2: 1223 MB (U(h) = 0.0558429*h^0.378771)
Reading from 7003: heap size 486 MB, throughput 0.985206
Reading from 7003: heap size 486 MB, throughput 0.985405
Reading from 7003: heap size 491 MB, throughput 0.972889
Reading from 7002: heap size 1205 MB, throughput 0.977332
Reading from 7003: heap size 491 MB, throughput 0.955519
Reading from 7003: heap size 500 MB, throughput 0.977908
Reading from 7002: heap size 1200 MB, throughput 0.977877
Reading from 7003: heap size 502 MB, throughput 0.985256
Numeric result:
Recommendation: 2 clients, utility 0.795958:
    h1: 550.431 MB (U(h) = 0.280841*h^0.190382)
    h2: 1249.57 MB (U(h) = 0.0391029*h^0.432207)
Recommendation: 2 clients, utility 0.795958:
    h1: 550.424 MB (U(h) = 0.280841*h^0.190382)
    h2: 1249.58 MB (U(h) = 0.0391029*h^0.432207)
Reading from 7002: heap size 1202 MB, throughput 0.975731
Reading from 7003: heap size 506 MB, throughput 0.990301
Reading from 7003: heap size 509 MB, throughput 0.989215
Reading from 7002: heap size 1206 MB, throughput 0.976507
Reading from 7003: heap size 508 MB, throughput 0.98756
Reading from 7002: heap size 1207 MB, throughput 0.976354
Numeric result:
Recommendation: 2 clients, utility 0.819153:
    h1: 509.014 MB (U(h) = 0.278236*h^0.192462)
    h2: 1290.99 MB (U(h) = 0.0268831*h^0.488129)
Recommendation: 2 clients, utility 0.819153:
    h1: 509.016 MB (U(h) = 0.278236*h^0.192462)
    h2: 1290.98 MB (U(h) = 0.0268831*h^0.488129)
Reading from 7003: heap size 511 MB, throughput 0.986594
Reading from 7002: heap size 1210 MB, throughput 0.972291
Reading from 7003: heap size 496 MB, throughput 0.986031
Reading from 7003: heap size 506 MB, throughput 0.985601
Reading from 7003: heap size 514 MB, throughput 0.98005
Reading from 7003: heap size 496 MB, throughput 0.972332
Reading from 7002: heap size 1214 MB, throughput 0.972419
Reading from 7003: heap size 509 MB, throughput 0.987675
Numeric result:
Recommendation: 2 clients, utility 0.848385:
    h1: 577 MB (U(h) = 0.210418*h^0.241124)
    h2: 1223 MB (U(h) = 0.024539*h^0.501993)
Recommendation: 2 clients, utility 0.848385:
    h1: 577 MB (U(h) = 0.210418*h^0.241124)
    h2: 1223 MB (U(h) = 0.024539*h^0.501993)
Reading from 7002: heap size 1218 MB, throughput 0.971114
Reading from 7003: heap size 510 MB, throughput 0.988141
Reading from 7003: heap size 511 MB, throughput 0.988454
Reading from 7002: heap size 1224 MB, throughput 0.969536
Reading from 7003: heap size 513 MB, throughput 0.98619
Reading from 7002: heap size 1227 MB, throughput 0.96759
Numeric result:
Recommendation: 2 clients, utility 0.925254:
    h1: 577 MB (U(h) = 0.10436*h^0.361798)
    h2: 1223 MB (U(h) = 0.0165561*h^0.560266)
Recommendation: 2 clients, utility 0.925254:
    h1: 577 MB (U(h) = 0.10436*h^0.361798)
    h2: 1223 MB (U(h) = 0.0165561*h^0.560266)
Reading from 7003: heap size 511 MB, throughput 0.98675
Reading from 7002: heap size 1230 MB, throughput 0.964752
Reading from 7003: heap size 513 MB, throughput 0.987805
Reading from 7003: heap size 516 MB, throughput 0.99011
Reading from 7003: heap size 516 MB, throughput 0.981087
Reading from 7002: heap size 1232 MB, throughput 0.965354
Reading from 7003: heap size 516 MB, throughput 0.968109
Reading from 7003: heap size 518 MB, throughput 0.983706
Numeric result:
Recommendation: 2 clients, utility 0.95873:
    h1: 577 MB (U(h) = 0.0822968*h^0.402118)
    h2: 1223 MB (U(h) = 0.0110821*h^0.619082)
Recommendation: 2 clients, utility 0.95873:
    h1: 577 MB (U(h) = 0.0822968*h^0.402118)
    h2: 1223 MB (U(h) = 0.0110821*h^0.619082)
Reading from 7002: heap size 1234 MB, throughput 0.964806
Reading from 7003: heap size 526 MB, throughput 0.988946
Reading from 7002: heap size 1235 MB, throughput 0.96663
Reading from 7003: heap size 528 MB, throughput 0.989483
Reading from 7002: heap size 1195 MB, throughput 0.962781
Reading from 7003: heap size 530 MB, throughput 0.987774
Numeric result:
Recommendation: 2 clients, utility 1.02559:
    h1: 577 MB (U(h) = 0.0498075*h^0.48686)
    h2: 1223 MB (U(h) = 0.0048523*h^0.73959)
Recommendation: 2 clients, utility 1.02559:
    h1: 577 MB (U(h) = 0.0498075*h^0.48686)
    h2: 1223 MB (U(h) = 0.0048523*h^0.73959)
Reading from 7003: heap size 532 MB, throughput 0.984576
Reading from 7002: heap size 1236 MB, throughput 0.945548
Reading from 7003: heap size 533 MB, throughput 0.986395
Reading from 7002: heap size 1280 MB, throughput 0.950874
Reading from 7003: heap size 534 MB, throughput 0.989633
Reading from 7003: heap size 539 MB, throughput 0.982683
Reading from 7003: heap size 539 MB, throughput 0.980921
Numeric result:
Recommendation: 2 clients, utility 1.03879:
    h1: 577 MB (U(h) = 0.0424785*h^0.512956)
    h2: 1223 MB (U(h) = 0.00394358*h^0.769608)
Recommendation: 2 clients, utility 1.03879:
    h1: 577 MB (U(h) = 0.0424785*h^0.512956)
    h2: 1223 MB (U(h) = 0.00394358*h^0.769608)
Reading from 7002: heap size 1321 MB, throughput 0.964154
Reading from 7003: heap size 548 MB, throughput 0.990129
Reading from 7002: heap size 1217 MB, throughput 0.96916
Reading from 7003: heap size 548 MB, throughput 0.990283
Reading from 7002: heap size 1321 MB, throughput 0.972687
Numeric result:
Recommendation: 2 clients, utility 1.06333:
    h1: 577 MB (U(h) = 0.035398*h^0.543068)
    h2: 1223 MB (U(h) = 0.00237367*h^0.84302)
Recommendation: 2 clients, utility 1.06333:
    h1: 577 MB (U(h) = 0.035398*h^0.543068)
    h2: 1223 MB (U(h) = 0.00237367*h^0.84302)
Reading from 7003: heap size 549 MB, throughput 0.990495
Reading from 7002: heap size 1221 MB, throughput 0.973558
Reading from 7003: heap size 551 MB, throughput 0.989464
Reading from 7002: heap size 1316 MB, throughput 0.974793
Reading from 7003: heap size 552 MB, throughput 0.988435
Numeric result:
Recommendation: 2 clients, utility 1.05716:
    h1: 577 MB (U(h) = 0.0462211*h^0.498184)
    h2: 1223 MB (U(h) = 0.00145609*h^0.913556)
Recommendation: 2 clients, utility 1.05716:
    h1: 577 MB (U(h) = 0.0462211*h^0.498184)
    h2: 1223 MB (U(h) = 0.00145609*h^0.913556)
Reading from 7002: heap size 1228 MB, throughput 0.970699
Reading from 7003: heap size 553 MB, throughput 0.989484
Reading from 7003: heap size 556 MB, throughput 0.984135
Reading from 7003: heap size 557 MB, throughput 0.983078
Reading from 7002: heap size 1290 MB, throughput 0.968498
Reading from 7003: heap size 566 MB, throughput 0.989139
Reading from 7003: heap size 567 MB, throughput 0.98986
Numeric result:
Recommendation: 2 clients, utility 1.01376:
    h1: 412.272 MB (U(h) = 0.178364*h^0.275075)
    h2: 1387.73 MB (U(h) = 0.00133579*h^0.925918)
Recommendation: 2 clients, utility 1.01376:
    h1: 412.272 MB (U(h) = 0.178364*h^0.275075)
    h2: 1387.73 MB (U(h) = 0.00133579*h^0.925918)
Reading from 7003: heap size 567 MB, throughput 0.990305
Reading from 7003: heap size 543 MB, throughput 0.989037
Numeric result:
Recommendation: 2 clients, utility 1.01253:
    h1: 425.678 MB (U(h) = 0.165981*h^0.286791)
    h2: 1374.32 MB (U(h) = 0.00133579*h^0.925918)
Recommendation: 2 clients, utility 1.01253:
    h1: 425.678 MB (U(h) = 0.165981*h^0.286791)
    h2: 1374.32 MB (U(h) = 0.00133579*h^0.925918)
Reading from 7003: heap size 540 MB, throughput 0.988821
Reading from 7003: heap size 532 MB, throughput 0.987738
Reading from 7003: heap size 531 MB, throughput 0.98112
Reading from 7003: heap size 520 MB, throughput 0.978289
Reading from 7002: heap size 1228 MB, throughput 0.920823
Reading from 7003: heap size 517 MB, throughput 0.988442
Numeric result:
Recommendation: 2 clients, utility 1.00964:
    h1: 470.402 MB (U(h) = 0.129411*h^0.327194)
    h2: 1329.6 MB (U(h) = 0.0013455*h^0.924825)
Recommendation: 2 clients, utility 1.00964:
    h1: 470.399 MB (U(h) = 0.129411*h^0.327194)
    h2: 1329.6 MB (U(h) = 0.0013455*h^0.924825)
Reading from 7003: heap size 488 MB, throughput 0.988629
Reading from 7003: heap size 503 MB, throughput 0.988496
Reading from 7003: heap size 483 MB, throughput 0.98793
Reading from 7003: heap size 490 MB, throughput 0.984833
Numeric result:
Recommendation: 2 clients, utility 1.01012:
    h1: 486.627 MB (U(h) = 0.11769*h^0.342657)
    h2: 1313.37 MB (U(h) = 0.0013455*h^0.924825)
Recommendation: 2 clients, utility 1.01012:
    h1: 486.62 MB (U(h) = 0.11769*h^0.342657)
    h2: 1313.38 MB (U(h) = 0.0013455*h^0.924825)
Reading from 7002: heap size 1385 MB, throughput 0.986668
Reading from 7003: heap size 478 MB, throughput 0.984555
Reading from 7003: heap size 490 MB, throughput 0.987495
Reading from 7003: heap size 464 MB, throughput 0.980917
Reading from 7003: heap size 484 MB, throughput 0.965125
Reading from 7003: heap size 486 MB, throughput 0.973873
Reading from 7002: heap size 1358 MB, throughput 0.986902
Reading from 7002: heap size 1416 MB, throughput 0.979286
Reading from 7003: heap size 495 MB, throughput 0.984347
Reading from 7002: heap size 1420 MB, throughput 0.957645
Reading from 7002: heap size 1391 MB, throughput 0.927565
Reading from 7002: heap size 1420 MB, throughput 0.870235
Reading from 7002: heap size 1425 MB, throughput 0.808432
Numeric result:
Recommendation: 2 clients, utility 0.993483:
    h1: 466.746 MB (U(h) = 0.133141*h^0.322557)
    h2: 1333.25 MB (U(h) = 0.00135735*h^0.921379)
Recommendation: 2 clients, utility 0.993483:
    h1: 466.746 MB (U(h) = 0.133141*h^0.322557)
    h2: 1333.25 MB (U(h) = 0.00135735*h^0.921379)
Reading from 7002: heap size 1435 MB, throughput 0.737525
Reading from 7002: heap size 1438 MB, throughput 0.686391
Reading from 7002: heap size 1442 MB, throughput 0.648736
Reading from 7002: heap size 1421 MB, throughput 0.904832
Reading from 7002: heap size 1366 MB, throughput 0.752868
Reading from 7002: heap size 1377 MB, throughput 0.841218
Reading from 7003: heap size 463 MB, throughput 0.98765
Reading from 7002: heap size 1382 MB, throughput 0.902292
Reading from 7002: heap size 1390 MB, throughput 0.9821
Reading from 7003: heap size 492 MB, throughput 0.988203
Reading from 7002: heap size 1393 MB, throughput 0.987969
Reading from 7003: heap size 464 MB, throughput 0.987885
Reading from 7002: heap size 1121 MB, throughput 0.986055
Numeric result:
Recommendation: 2 clients, utility 0.938455:
    h1: 489.052 MB (U(h) = 0.162266*h^0.29048)
    h2: 1310.95 MB (U(h) = 0.00357642*h^0.778658)
Recommendation: 2 clients, utility 0.938455:
    h1: 489.052 MB (U(h) = 0.162266*h^0.29048)
    h2: 1310.95 MB (U(h) = 0.00357642*h^0.778658)
Reading from 7003: heap size 486 MB, throughput 0.986828
Reading from 7002: heap size 1395 MB, throughput 0.986537
Reading from 7002: heap size 1136 MB, throughput 0.984942
Reading from 7003: heap size 485 MB, throughput 0.984208
Reading from 7002: heap size 1378 MB, throughput 0.9829
Reading from 7003: heap size 488 MB, throughput 0.98917
Reading from 7002: heap size 1153 MB, throughput 0.980446
Reading from 7003: heap size 488 MB, throughput 0.986021
Reading from 7003: heap size 489 MB, throughput 0.977386
Reading from 7003: heap size 491 MB, throughput 0.965273
Reading from 7002: heap size 1357 MB, throughput 0.980086
Numeric result:
Recommendation: 2 clients, utility 0.931691:
    h1: 559.125 MB (U(h) = 0.165191*h^0.287591)
    h2: 1240.88 MB (U(h) = 0.00969471*h^0.638256)
Recommendation: 2 clients, utility 0.931691:
    h1: 559.125 MB (U(h) = 0.165191*h^0.287591)
    h2: 1240.88 MB (U(h) = 0.00969471*h^0.638256)
Reading from 7003: heap size 478 MB, throughput 0.98616
Reading from 7002: heap size 1169 MB, throughput 0.976561
Reading from 7003: heap size 489 MB, throughput 0.984324
Reading from 7002: heap size 1335 MB, throughput 0.980243
Reading from 7003: heap size 491 MB, throughput 0.986284
Reading from 7002: heap size 1184 MB, throughput 0.976396
Reading from 7003: heap size 493 MB, throughput 0.986675
Reading from 7002: heap size 1315 MB, throughput 0.972615
Numeric result:
Recommendation: 2 clients, utility 0.929717:
    h1: 577 MB (U(h) = 0.178349*h^0.275242)
    h2: 1223 MB (U(h) = 0.0621167*h^0.376975)
Recommendation: 2 clients, utility 0.929717:
    h1: 577 MB (U(h) = 0.178349*h^0.275242)
    h2: 1223 MB (U(h) = 0.0621167*h^0.376975)
Reading from 7003: heap size 492 MB, throughput 0.986663
Reading from 7002: heap size 1199 MB, throughput 0.972095
Reading from 7003: heap size 495 MB, throughput 0.985603
Reading from 7002: heap size 1303 MB, throughput 0.973415
Reading from 7003: heap size 498 MB, throughput 0.984428
Reading from 7002: heap size 1214 MB, throughput 0.971972
Reading from 7003: heap size 499 MB, throughput 0.985073
Reading from 7003: heap size 501 MB, throughput 0.978204
Reading from 7003: heap size 504 MB, throughput 0.965894
Numeric result:
Recommendation: 2 clients, utility 0.926637:
    h1: 577 MB (U(h) = 0.210798*h^0.248181)
    h2: 1223 MB (U(h) = 0.0586199*h^0.385346)
Recommendation: 2 clients, utility 0.926637:
    h1: 577 MB (U(h) = 0.210798*h^0.248181)
    h2: 1223 MB (U(h) = 0.0586199*h^0.385346)
Reading from 7002: heap size 1300 MB, throughput 0.968788
Reading from 7003: heap size 512 MB, throughput 0.984635
Reading from 7002: heap size 1223 MB, throughput 0.96638
Reading from 7003: heap size 512 MB, throughput 0.984513
Reading from 7002: heap size 1283 MB, throughput 0.965812
Reading from 7002: heap size 1222 MB, throughput 0.965881
Reading from 7003: heap size 516 MB, throughput 0.989437
Numeric result:
Recommendation: 2 clients, utility 0.924139:
    h1: 577 MB (U(h) = 0.256461*h^0.216535)
    h2: 1223 MB (U(h) = 0.078237*h^0.345081)
Recommendation: 2 clients, utility 0.924139:
    h1: 577 MB (U(h) = 0.256461*h^0.216535)
    h2: 1223 MB (U(h) = 0.078237*h^0.345081)
Reading from 7002: heap size 1287 MB, throughput 0.968422
Reading from 7003: heap size 518 MB, throughput 0.988618
Reading from 7002: heap size 1225 MB, throughput 0.96731
Reading from 7003: heap size 517 MB, throughput 0.989925
Reading from 7002: heap size 1273 MB, throughput 0.968137
Reading from 7003: heap size 519 MB, throughput 0.988097
Reading from 7002: heap size 1223 MB, throughput 0.308559
Numeric result:
Recommendation: 2 clients, utility 0.912061:
    h1: 505.488 MB (U(h) = 0.411641*h^0.140205)
    h2: 1294.51 MB (U(h) = 0.0706349*h^0.359054)
Recommendation: 2 clients, utility 0.912061:
    h1: 505.487 MB (U(h) = 0.411641*h^0.140205)
    h2: 1294.51 MB (U(h) = 0.0706349*h^0.359054)
Reading from 7003: heap size 523 MB, throughput 0.990399
Reading from 7003: heap size 492 MB, throughput 0.984487
Reading from 7003: heap size 516 MB, throughput 0.972558
Client 7003 died
Clients: 1
Reading from 7002: heap size 1267 MB, throughput 0.567337
Reading from 7002: heap size 1270 MB, throughput 0.691857
Reading from 7002: heap size 1270 MB, throughput 0.76199
Recommendation: one client; give it all the memory
Reading from 7002: heap size 1272 MB, throughput 0.800914
Reading from 7002: heap size 1272 MB, throughput 0.830998
Reading from 7002: heap size 1274 MB, throughput 0.846169
Recommendation: one client; give it all the memory
Reading from 7002: heap size 1275 MB, throughput 0.861782
Reading from 7002: heap size 1277 MB, throughput 0.872698
Reading from 7002: heap size 1279 MB, throughput 0.882788
Recommendation: one client; give it all the memory
Reading from 7002: heap size 1280 MB, throughput 0.884084
Reading from 7002: heap size 1283 MB, throughput 0.888003
Reading from 7002: heap size 1284 MB, throughput 0.889395
Recommendation: one client; give it all the memory
Reading from 7002: heap size 1288 MB, throughput 0.896881
Reading from 7002: heap size 1289 MB, throughput 0.898273
Reading from 7002: heap size 1295 MB, throughput 0.904511
Recommendation: one client; give it all the memory
Reading from 7002: heap size 1295 MB, throughput 0.906953
Reading from 7002: heap size 1302 MB, throughput 0.965996
Reading from 7002: heap size 1307 MB, throughput 0.908032
Recommendation: one client; give it all the memory
Reading from 7002: heap size 1303 MB, throughput 0.989331
Reading from 7002: heap size 1314 MB, throughput 0.990139
Recommendation: one client; give it all the memory
Reading from 7002: heap size 1323 MB, throughput 0.989703
Reading from 7002: heap size 1326 MB, throughput 0.989231
Reading from 7002: heap size 1326 MB, throughput 0.988718
Recommendation: one client; give it all the memory
Reading from 7002: heap size 1328 MB, throughput 0.986535
Reading from 7002: heap size 1319 MB, throughput 0.981805
Reading from 7002: heap size 1263 MB, throughput 0.983652
Recommendation: one client; give it all the memory
Reading from 7002: heap size 1318 MB, throughput 0.984013
Reading from 7002: heap size 1321 MB, throughput 0.983717
Recommendation: one client; give it all the memory
Reading from 7002: heap size 1325 MB, throughput 0.983423
Reading from 7002: heap size 1326 MB, throughput 0.982608
Reading from 7002: heap size 1332 MB, throughput 0.980581
Recommendation: one client; give it all the memory
Reading from 7002: heap size 1337 MB, throughput 0.977931
Reading from 7002: heap size 1343 MB, throughput 0.977345
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 7002: heap size 1351 MB, throughput 0.987352
Recommendation: one client; give it all the memory
Reading from 7002: heap size 1336 MB, throughput 0.989848
Recommendation: one client; give it all the memory
Reading from 7002: heap size 1365 MB, throughput 0.985277
Reading from 7002: heap size 1356 MB, throughput 0.976418
Reading from 7002: heap size 1389 MB, throughput 0.96179
Reading from 7002: heap size 1409 MB, throughput 0.931704
Reading from 7002: heap size 1422 MB, throughput 0.872916
Reading from 7002: heap size 1456 MB, throughput 0.972963
Reading from 7002: heap size 1480 MB, throughput 0.843754
Reading from 7002: heap size 1516 MB, throughput 0.883026
Reading from 7002: heap size 1524 MB, throughput 0.925462
Reading from 7002: heap size 1549 MB, throughput 0.955088
Reading from 7002: heap size 1553 MB, throughput 0.969577
Client 7002 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
