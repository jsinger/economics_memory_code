economemd
    total memory: 8922 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub6_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 11879: heap size 9 MB, throughput 0.991535
Clients: 1
Client 11879 has a minimum heap size of 276 MB
Reading from 11878: heap size 9 MB, throughput 0.990837
Clients: 2
Client 11878 has a minimum heap size of 1211 MB
Reading from 11878: heap size 9 MB, throughput 0.972068
Reading from 11879: heap size 9 MB, throughput 0.981659
Reading from 11878: heap size 9 MB, throughput 0.966552
Reading from 11879: heap size 9 MB, throughput 0.953308
Reading from 11878: heap size 9 MB, throughput 0.954679
Reading from 11879: heap size 9 MB, throughput 0.951796
Reading from 11878: heap size 11 MB, throughput 0.941151
Reading from 11879: heap size 11 MB, throughput 0.985783
Reading from 11878: heap size 11 MB, throughput 0.985047
Reading from 11879: heap size 11 MB, throughput 0.951115
Reading from 11878: heap size 17 MB, throughput 0.96612
Reading from 11879: heap size 17 MB, throughput 0.922586
Reading from 11878: heap size 17 MB, throughput 0.711014
Reading from 11879: heap size 17 MB, throughput 0.617878
Reading from 11878: heap size 30 MB, throughput 0.796368
Reading from 11879: heap size 30 MB, throughput 0.843168
Reading from 11878: heap size 31 MB, throughput 0.915234
Reading from 11879: heap size 31 MB, throughput 0.922103
Reading from 11879: heap size 35 MB, throughput 0.238952
Reading from 11878: heap size 36 MB, throughput 0.326859
Reading from 11879: heap size 47 MB, throughput 0.937554
Reading from 11878: heap size 48 MB, throughput 0.953729
Reading from 11879: heap size 49 MB, throughput 0.356122
Reading from 11878: heap size 50 MB, throughput 0.418832
Reading from 11879: heap size 65 MB, throughput 0.850688
Reading from 11878: heap size 66 MB, throughput 0.889139
Reading from 11879: heap size 72 MB, throughput 0.82765
Reading from 11878: heap size 72 MB, throughput 0.874629
Reading from 11878: heap size 73 MB, throughput 0.264456
Reading from 11879: heap size 72 MB, throughput 0.197231
Reading from 11879: heap size 100 MB, throughput 0.74637
Reading from 11878: heap size 102 MB, throughput 0.674679
Reading from 11879: heap size 101 MB, throughput 0.297393
Reading from 11878: heap size 102 MB, throughput 0.218485
Reading from 11879: heap size 133 MB, throughput 0.868887
Reading from 11878: heap size 136 MB, throughput 0.849991
Reading from 11879: heap size 134 MB, throughput 0.803804
Reading from 11878: heap size 136 MB, throughput 0.849339
Reading from 11879: heap size 135 MB, throughput 0.722541
Reading from 11878: heap size 139 MB, throughput 0.806475
Reading from 11879: heap size 138 MB, throughput 0.832282
Reading from 11878: heap size 142 MB, throughput 0.681449
Reading from 11879: heap size 141 MB, throughput 0.139349
Reading from 11878: heap size 145 MB, throughput 0.142593
Reading from 11879: heap size 179 MB, throughput 0.817595
Reading from 11878: heap size 187 MB, throughput 0.771071
Reading from 11879: heap size 182 MB, throughput 0.786197
Reading from 11878: heap size 191 MB, throughput 0.777559
Reading from 11879: heap size 186 MB, throughput 0.75269
Reading from 11879: heap size 193 MB, throughput 0.748688
Reading from 11878: heap size 194 MB, throughput 0.653243
Reading from 11878: heap size 201 MB, throughput 0.782116
Reading from 11878: heap size 205 MB, throughput 0.125641
Reading from 11878: heap size 258 MB, throughput 0.607296
Reading from 11879: heap size 196 MB, throughput 0.508811
Reading from 11878: heap size 260 MB, throughput 0.721496
Reading from 11878: heap size 263 MB, throughput 0.630387
Reading from 11879: heap size 235 MB, throughput 0.889322
Reading from 11878: heap size 270 MB, throughput 0.633783
Reading from 11879: heap size 245 MB, throughput 0.866568
Reading from 11878: heap size 275 MB, throughput 0.663927
Reading from 11879: heap size 246 MB, throughput 0.868329
Reading from 11879: heap size 247 MB, throughput 0.719029
Reading from 11878: heap size 286 MB, throughput 0.64594
Reading from 11878: heap size 289 MB, throughput 0.602333
Reading from 11879: heap size 250 MB, throughput 0.780048
Reading from 11879: heap size 250 MB, throughput 0.652066
Reading from 11879: heap size 254 MB, throughput 0.847056
Reading from 11879: heap size 254 MB, throughput 0.793211
Reading from 11878: heap size 296 MB, throughput 0.106646
Reading from 11879: heap size 255 MB, throughput 0.781389
Reading from 11879: heap size 259 MB, throughput 0.716793
Reading from 11878: heap size 350 MB, throughput 0.521717
Reading from 11878: heap size 359 MB, throughput 0.572789
Reading from 11879: heap size 265 MB, throughput 0.660022
Reading from 11879: heap size 267 MB, throughput 0.665465
Reading from 11878: heap size 360 MB, throughput 0.60007
Reading from 11879: heap size 274 MB, throughput 0.852235
Reading from 11878: heap size 363 MB, throughput 0.116263
Reading from 11879: heap size 275 MB, throughput 0.789417
Reading from 11878: heap size 422 MB, throughput 0.454372
Reading from 11878: heap size 362 MB, throughput 0.601908
Reading from 11878: heap size 417 MB, throughput 0.537923
Reading from 11878: heap size 419 MB, throughput 0.521934
Reading from 11879: heap size 281 MB, throughput 0.115116
Reading from 11878: heap size 425 MB, throughput 0.580021
Reading from 11879: heap size 325 MB, throughput 0.594106
Reading from 11879: heap size 329 MB, throughput 0.800449
Reading from 11878: heap size 428 MB, throughput 0.584624
Reading from 11879: heap size 329 MB, throughput 0.774594
Reading from 11878: heap size 434 MB, throughput 0.536231
Reading from 11879: heap size 332 MB, throughput 0.777172
Reading from 11878: heap size 439 MB, throughput 0.473412
Reading from 11879: heap size 334 MB, throughput 0.6155
Reading from 11879: heap size 339 MB, throughput 0.670533
Reading from 11879: heap size 340 MB, throughput 0.639142
Reading from 11879: heap size 349 MB, throughput 0.637995
Reading from 11879: heap size 350 MB, throughput 0.662051
Equal recommendation: 4461 MB each
Reading from 11878: heap size 445 MB, throughput 0.0825519
Reading from 11879: heap size 360 MB, throughput 0.950262
Reading from 11878: heap size 506 MB, throughput 0.437611
Reading from 11878: heap size 516 MB, throughput 0.552983
Reading from 11878: heap size 517 MB, throughput 0.584481
Reading from 11879: heap size 361 MB, throughput 0.963437
Reading from 11878: heap size 523 MB, throughput 0.111332
Reading from 11878: heap size 584 MB, throughput 0.524315
Reading from 11878: heap size 590 MB, throughput 0.631238
Reading from 11878: heap size 592 MB, throughput 0.598548
Reading from 11878: heap size 595 MB, throughput 0.643311
Reading from 11878: heap size 605 MB, throughput 0.605572
Reading from 11878: heap size 610 MB, throughput 0.550812
Reading from 11879: heap size 363 MB, throughput 0.97714
Reading from 11878: heap size 626 MB, throughput 0.571378
Reading from 11878: heap size 634 MB, throughput 0.463453
Reading from 11878: heap size 649 MB, throughput 0.792239
Reading from 11879: heap size 366 MB, throughput 0.960894
Reading from 11878: heap size 654 MB, throughput 0.759737
Reading from 11879: heap size 369 MB, throughput 0.958635
Reading from 11878: heap size 666 MB, throughput 0.213202
Reading from 11878: heap size 734 MB, throughput 0.715058
Reading from 11878: heap size 749 MB, throughput 0.396767
Reading from 11878: heap size 758 MB, throughput 0.406364
Reading from 11878: heap size 647 MB, throughput 0.502833
Reading from 11879: heap size 370 MB, throughput 0.970693
Reading from 11879: heap size 370 MB, throughput 0.974967
Equal recommendation: 4461 MB each
Reading from 11878: heap size 739 MB, throughput 0.0959093
Reading from 11878: heap size 710 MB, throughput 0.185179
Reading from 11878: heap size 825 MB, throughput 0.361338
Reading from 11878: heap size 698 MB, throughput 0.317209
Reading from 11878: heap size 825 MB, throughput 0.0449442
Reading from 11878: heap size 917 MB, throughput 0.280911
Reading from 11879: heap size 372 MB, throughput 0.971915
Reading from 11878: heap size 918 MB, throughput 0.911605
Reading from 11878: heap size 923 MB, throughput 0.70491
Reading from 11878: heap size 921 MB, throughput 0.616103
Reading from 11878: heap size 924 MB, throughput 0.683935
Reading from 11878: heap size 921 MB, throughput 0.675116
Reading from 11878: heap size 923 MB, throughput 0.950983
Reading from 11878: heap size 924 MB, throughput 0.907425
Reading from 11878: heap size 926 MB, throughput 0.880681
Reading from 11878: heap size 915 MB, throughput 0.894652
Reading from 11879: heap size 371 MB, throughput 0.974714
Reading from 11878: heap size 709 MB, throughput 0.920404
Reading from 11878: heap size 901 MB, throughput 0.924696
Reading from 11878: heap size 717 MB, throughput 0.88443
Reading from 11878: heap size 886 MB, throughput 0.906468
Reading from 11878: heap size 761 MB, throughput 0.788177
Reading from 11878: heap size 869 MB, throughput 0.804354
Reading from 11878: heap size 766 MB, throughput 0.765546
Reading from 11879: heap size 373 MB, throughput 0.970375
Reading from 11878: heap size 853 MB, throughput 0.813098
Reading from 11878: heap size 760 MB, throughput 0.81126
Reading from 11878: heap size 840 MB, throughput 0.820954
Reading from 11878: heap size 765 MB, throughput 0.844874
Reading from 11879: heap size 376 MB, throughput 0.957743
Reading from 11878: heap size 831 MB, throughput 0.103087
Reading from 11878: heap size 852 MB, throughput 0.56174
Reading from 11878: heap size 921 MB, throughput 0.727823
Reading from 11878: heap size 922 MB, throughput 0.804308
Reading from 11878: heap size 923 MB, throughput 0.714205
Reading from 11879: heap size 376 MB, throughput 0.971159
Reading from 11878: heap size 925 MB, throughput 0.952654
Reading from 11879: heap size 380 MB, throughput 0.971812
Reading from 11878: heap size 917 MB, throughput 0.954612
Reading from 11878: heap size 923 MB, throughput 0.669684
Reading from 11878: heap size 941 MB, throughput 0.769733
Reading from 11878: heap size 943 MB, throughput 0.753664
Equal recommendation: 4461 MB each
Reading from 11878: heap size 951 MB, throughput 0.764048
Reading from 11878: heap size 951 MB, throughput 0.770936
Reading from 11878: heap size 957 MB, throughput 0.773124
Reading from 11878: heap size 958 MB, throughput 0.710181
Reading from 11879: heap size 381 MB, throughput 0.978919
Reading from 11878: heap size 964 MB, throughput 0.783618
Reading from 11878: heap size 965 MB, throughput 0.766638
Reading from 11878: heap size 970 MB, throughput 0.806947
Reading from 11879: heap size 383 MB, throughput 0.968035
Reading from 11879: heap size 384 MB, throughput 0.825898
Reading from 11878: heap size 972 MB, throughput 0.888366
Reading from 11879: heap size 379 MB, throughput 0.739302
Reading from 11879: heap size 385 MB, throughput 0.710218
Reading from 11878: heap size 974 MB, throughput 0.761093
Reading from 11878: heap size 977 MB, throughput 0.767405
Reading from 11879: heap size 393 MB, throughput 0.962902
Reading from 11878: heap size 966 MB, throughput 0.0854108
Reading from 11878: heap size 1101 MB, throughput 0.466151
Reading from 11878: heap size 1106 MB, throughput 0.798608
Reading from 11878: heap size 1108 MB, throughput 0.775143
Reading from 11878: heap size 1119 MB, throughput 0.703479
Reading from 11879: heap size 395 MB, throughput 0.985513
Reading from 11878: heap size 1119 MB, throughput 0.779534
Reading from 11878: heap size 1124 MB, throughput 0.780222
Reading from 11878: heap size 1127 MB, throughput 0.721137
Reading from 11878: heap size 1131 MB, throughput 0.773418
Reading from 11878: heap size 1135 MB, throughput 0.732006
Reading from 11879: heap size 399 MB, throughput 0.983411
Reading from 11878: heap size 1137 MB, throughput 0.922125
Reading from 11879: heap size 401 MB, throughput 0.979654
Reading from 11878: heap size 1142 MB, throughput 0.951122
Equal recommendation: 4461 MB each
Reading from 11878: heap size 1145 MB, throughput 0.941848
Reading from 11879: heap size 401 MB, throughput 0.738706
Reading from 11878: heap size 1149 MB, throughput 0.955723
Reading from 11879: heap size 438 MB, throughput 0.992273
Reading from 11878: heap size 1161 MB, throughput 0.94007
Reading from 11879: heap size 438 MB, throughput 0.992749
Reading from 11879: heap size 441 MB, throughput 0.990146
Reading from 11878: heap size 1163 MB, throughput 0.957762
Reading from 11879: heap size 445 MB, throughput 0.989493
Reading from 11878: heap size 1163 MB, throughput 0.956749
Reading from 11879: heap size 446 MB, throughput 0.988843
Reading from 11878: heap size 1167 MB, throughput 0.959349
Equal recommendation: 4461 MB each
Reading from 11879: heap size 442 MB, throughput 0.976245
Reading from 11878: heap size 1175 MB, throughput 0.948149
Reading from 11879: heap size 445 MB, throughput 0.984816
Reading from 11879: heap size 440 MB, throughput 0.869424
Reading from 11879: heap size 443 MB, throughput 0.830763
Reading from 11879: heap size 449 MB, throughput 0.838828
Reading from 11878: heap size 1176 MB, throughput 0.954312
Reading from 11879: heap size 453 MB, throughput 0.989153
Reading from 11878: heap size 1183 MB, throughput 0.956411
Reading from 11879: heap size 452 MB, throughput 0.965655
Reading from 11878: heap size 1186 MB, throughput 0.949056
Reading from 11879: heap size 456 MB, throughput 0.984752
Reading from 11878: heap size 1193 MB, throughput 0.956845
Equal recommendation: 4461 MB each
Reading from 11879: heap size 456 MB, throughput 0.969438
Reading from 11878: heap size 1198 MB, throughput 0.941488
Reading from 11879: heap size 459 MB, throughput 0.98571
Reading from 11878: heap size 1204 MB, throughput 0.957064
Reading from 11879: heap size 463 MB, throughput 0.984015
Reading from 11878: heap size 1210 MB, throughput 0.953057
Reading from 11878: heap size 1217 MB, throughput 0.960473
Reading from 11879: heap size 463 MB, throughput 0.986077
Equal recommendation: 4461 MB each
Reading from 11878: heap size 1223 MB, throughput 0.961497
Reading from 11879: heap size 468 MB, throughput 0.983052
Reading from 11878: heap size 1231 MB, throughput 0.955175
Reading from 11879: heap size 468 MB, throughput 0.985607
Reading from 11879: heap size 470 MB, throughput 0.980244
Reading from 11879: heap size 473 MB, throughput 0.873418
Reading from 11879: heap size 477 MB, throughput 0.887851
Reading from 11878: heap size 1235 MB, throughput 0.948229
Reading from 11879: heap size 478 MB, throughput 0.986969
Reading from 11878: heap size 1242 MB, throughput 0.948232
Reading from 11879: heap size 485 MB, throughput 0.988996
Reading from 11878: heap size 1245 MB, throughput 0.954782
Equal recommendation: 4461 MB each
Reading from 11879: heap size 486 MB, throughput 0.989987
Reading from 11879: heap size 487 MB, throughput 0.991645
Reading from 11878: heap size 1252 MB, throughput 0.505418
Reading from 11878: heap size 1310 MB, throughput 0.989422
Reading from 11879: heap size 489 MB, throughput 0.98788
Reading from 11878: heap size 1309 MB, throughput 0.988803
Reading from 11879: heap size 486 MB, throughput 0.988887
Reading from 11878: heap size 1318 MB, throughput 0.982349
Equal recommendation: 4461 MB each
Reading from 11879: heap size 489 MB, throughput 0.987422
Reading from 11878: heap size 1328 MB, throughput 0.980636
Reading from 11879: heap size 489 MB, throughput 0.985257
Reading from 11878: heap size 1329 MB, throughput 0.975375
Reading from 11879: heap size 490 MB, throughput 0.98961
Reading from 11879: heap size 491 MB, throughput 0.907781
Reading from 11879: heap size 492 MB, throughput 0.89178
Reading from 11878: heap size 1323 MB, throughput 0.975538
Reading from 11879: heap size 498 MB, throughput 0.991524
Reading from 11878: heap size 1220 MB, throughput 0.973548
Equal recommendation: 4461 MB each
Reading from 11879: heap size 499 MB, throughput 0.991071
Reading from 11878: heap size 1312 MB, throughput 0.968315
Reading from 11879: heap size 504 MB, throughput 0.990866
Reading from 11878: heap size 1242 MB, throughput 0.965996
Reading from 11879: heap size 505 MB, throughput 0.990481
Reading from 11878: heap size 1311 MB, throughput 0.966946
Reading from 11879: heap size 503 MB, throughput 0.989076
Reading from 11878: heap size 1314 MB, throughput 0.965622
Equal recommendation: 4461 MB each
Reading from 11879: heap size 506 MB, throughput 0.987924
Reading from 11878: heap size 1318 MB, throughput 0.968208
Reading from 11879: heap size 507 MB, throughput 0.986085
Reading from 11878: heap size 1320 MB, throughput 0.955373
Reading from 11879: heap size 508 MB, throughput 0.989681
Reading from 11879: heap size 509 MB, throughput 0.902865
Reading from 11879: heap size 511 MB, throughput 0.920688
Reading from 11878: heap size 1325 MB, throughput 0.954747
Reading from 11879: heap size 518 MB, throughput 0.992715
Reading from 11878: heap size 1331 MB, throughput 0.955134
Equal recommendation: 4461 MB each
Reading from 11879: heap size 519 MB, throughput 0.992134
Reading from 11878: heap size 1338 MB, throughput 0.95044
Reading from 11879: heap size 522 MB, throughput 0.981431
Reading from 11878: heap size 1347 MB, throughput 0.947763
Reading from 11879: heap size 524 MB, throughput 0.989218
Reading from 11878: heap size 1357 MB, throughput 0.950988
Reading from 11878: heap size 1362 MB, throughput 0.961158
Equal recommendation: 4461 MB each
Reading from 11879: heap size 523 MB, throughput 0.98955
Reading from 11878: heap size 1374 MB, throughput 0.95209
Reading from 11879: heap size 525 MB, throughput 0.988373
Reading from 11879: heap size 528 MB, throughput 0.992386
Reading from 11879: heap size 529 MB, throughput 0.899714
Reading from 11879: heap size 530 MB, throughput 0.927259
Reading from 11879: heap size 532 MB, throughput 0.991831
Equal recommendation: 4461 MB each
Reading from 11879: heap size 537 MB, throughput 0.991783
Reading from 11879: heap size 539 MB, throughput 0.991527
Reading from 11879: heap size 538 MB, throughput 0.99093
Equal recommendation: 4461 MB each
Reading from 11879: heap size 541 MB, throughput 0.979354
Reading from 11878: heap size 1376 MB, throughput 0.860063
Reading from 11879: heap size 544 MB, throughput 0.987508
Reading from 11879: heap size 545 MB, throughput 0.979094
Reading from 11879: heap size 550 MB, throughput 0.902851
Reading from 11879: heap size 554 MB, throughput 0.987603
Equal recommendation: 4461 MB each
Reading from 11879: heap size 560 MB, throughput 0.991222
Reading from 11879: heap size 563 MB, throughput 0.990748
Reading from 11878: heap size 1491 MB, throughput 0.984201
Reading from 11879: heap size 562 MB, throughput 0.989799
Equal recommendation: 4461 MB each
Reading from 11878: heap size 1540 MB, throughput 0.981559
Reading from 11879: heap size 564 MB, throughput 0.988313
Reading from 11878: heap size 1529 MB, throughput 0.85197
Reading from 11878: heap size 1535 MB, throughput 0.759789
Reading from 11878: heap size 1521 MB, throughput 0.724173
Reading from 11878: heap size 1541 MB, throughput 0.703625
Reading from 11878: heap size 1568 MB, throughput 0.72753
Reading from 11878: heap size 1579 MB, throughput 0.706529
Reading from 11878: heap size 1612 MB, throughput 0.737717
Reading from 11878: heap size 1616 MB, throughput 0.917315
Reading from 11879: heap size 568 MB, throughput 0.988729
Reading from 11879: heap size 569 MB, throughput 0.983782
Reading from 11879: heap size 573 MB, throughput 0.90305
Reading from 11878: heap size 1641 MB, throughput 0.980105
Reading from 11879: heap size 576 MB, throughput 0.990366
Equal recommendation: 4461 MB each
Reading from 11878: heap size 1646 MB, throughput 0.971513
Reading from 11879: heap size 582 MB, throughput 0.99195
Reading from 11878: heap size 1652 MB, throughput 0.973491
Reading from 11878: heap size 1662 MB, throughput 0.975343
Reading from 11879: heap size 584 MB, throughput 0.991036
Reading from 11878: heap size 1657 MB, throughput 0.973505
Reading from 11879: heap size 584 MB, throughput 0.989919
Equal recommendation: 4461 MB each
Reading from 11878: heap size 1668 MB, throughput 0.971006
Reading from 11879: heap size 586 MB, throughput 0.990036
Reading from 11878: heap size 1673 MB, throughput 0.977007
Reading from 11879: heap size 583 MB, throughput 0.994821
Reading from 11879: heap size 586 MB, throughput 0.941554
Reading from 11879: heap size 589 MB, throughput 0.974516
Reading from 11878: heap size 1677 MB, throughput 0.975519
Equal recommendation: 4461 MB each
Reading from 11879: heap size 590 MB, throughput 0.99337
Reading from 11878: heap size 1677 MB, throughput 0.974134
Reading from 11879: heap size 593 MB, throughput 0.992408
Reading from 11878: heap size 1683 MB, throughput 0.96977
Reading from 11879: heap size 595 MB, throughput 0.991803
Equal recommendation: 4461 MB each
Reading from 11878: heap size 1669 MB, throughput 0.970503
Reading from 11879: heap size 593 MB, throughput 0.991511
Reading from 11878: heap size 1679 MB, throughput 0.966759
Reading from 11879: heap size 596 MB, throughput 0.994956
Reading from 11879: heap size 597 MB, throughput 0.977728
Reading from 11878: heap size 1674 MB, throughput 0.968606
Reading from 11879: heap size 598 MB, throughput 0.945483
Equal recommendation: 4461 MB each
Reading from 11878: heap size 1678 MB, throughput 0.972612
Reading from 11879: heap size 603 MB, throughput 0.994191
Reading from 11879: heap size 603 MB, throughput 0.987124
Reading from 11878: heap size 1687 MB, throughput 0.9616
Reading from 11878: heap size 1691 MB, throughput 0.960075
Reading from 11879: heap size 603 MB, throughput 0.992669
Equal recommendation: 4461 MB each
Reading from 11878: heap size 1704 MB, throughput 0.960768
Reading from 11879: heap size 605 MB, throughput 0.991694
Reading from 11879: heap size 606 MB, throughput 0.988797
Reading from 11878: heap size 1710 MB, throughput 0.748619
Reading from 11879: heap size 607 MB, throughput 0.975229
Reading from 11879: heap size 607 MB, throughput 0.98445
Equal recommendation: 4461 MB each
Reading from 11878: heap size 1673 MB, throughput 0.990138
Reading from 11879: heap size 609 MB, throughput 0.99437
Reading from 11878: heap size 1678 MB, throughput 0.99163
Reading from 11879: heap size 610 MB, throughput 0.993615
Reading from 11878: heap size 1700 MB, throughput 0.985995
Equal recommendation: 4461 MB each
Reading from 11879: heap size 612 MB, throughput 0.993145
Reading from 11878: heap size 1705 MB, throughput 0.984358
Reading from 11879: heap size 611 MB, throughput 0.992117
Reading from 11878: heap size 1706 MB, throughput 0.982313
Equal recommendation: 4461 MB each
Reading from 11879: heap size 613 MB, throughput 0.99481
Reading from 11879: heap size 615 MB, throughput 0.522962
Reading from 11878: heap size 1711 MB, throughput 0.980966
Reading from 11879: heap size 635 MB, throughput 0.995342
Reading from 11878: heap size 1694 MB, throughput 0.975893
Reading from 11879: heap size 641 MB, throughput 0.994538
Equal recommendation: 4461 MB each
Reading from 11878: heap size 1579 MB, throughput 0.974003
Reading from 11879: heap size 644 MB, throughput 0.993698
Reading from 11879: heap size 646 MB, throughput 0.992357
Reading from 11879: heap size 648 MB, throughput 0.98911
Equal recommendation: 4461 MB each
Reading from 11879: heap size 646 MB, throughput 0.98266
Client 11879 died
Clients: 1
Reading from 11878: heap size 1691 MB, throughput 0.993317
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 11878: heap size 1697 MB, throughput 0.991582
Reading from 11878: heap size 1691 MB, throughput 0.96218
Reading from 11878: heap size 1718 MB, throughput 0.798642
Reading from 11878: heap size 1727 MB, throughput 0.81485
Reading from 11878: heap size 1758 MB, throughput 0.798294
Client 11878 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
