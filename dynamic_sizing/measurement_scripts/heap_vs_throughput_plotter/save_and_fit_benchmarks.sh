#!/bin/bash
# Take readings from a sequence of benchmarks, each run individually, with no recommendations
# Input file should have one benchmark per line, in the format:
# benchmark size threads runtime


DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") input_file [daemon_args...]"
    exit 1
}

test -z "${1}" && usage
test ! -f "${1}" && usage
INPUT_FILE="${1}"

OUTDIR="$(readlink -f "benchmarks_$(date '+%Y_%m_%d_%H_%M_%S')")"
mkdir -p "${OUTDIR}"
LOGFILE="${OUTDIR}/run.log"

function stamp()
{
    if [ -z "${1}" ]; then
        echo | tee -a "${LOGFILE}"
    else
        echo "$(date '+%Y/%m/%d  %H:%M:%S')    ${@}" | tee -a "${LOGFILE}"
    fi
}

function cleanup()
{
    stamp
    stamp 'Killing...'
    test ! -z "${SLEEP}" && kill "${SLEEP}" &>/dev/null
    test ! -z "${DACAPO}" && kill "${DACAPO}" &>/dev/null
    test ! -z "${READER}" && kill "${READER}" &>/dev/null
    wait
    stamp 'Killed'
    exit
}

trap "cleanup" SIGINT SIGHUP SIGTERM


exec 3<"${INPUT_FILE}"

while read -u 3 benchmark size threads runtime; do
    PREFIX="${OUTDIR}/${benchmark}_${size}_${threads}_${runtime}"
    stamp "Setting up ${benchmark} ${size} ${threads} ${runtime}..."

    "${DIR}/save_and_fit_all_models.sh" "${PREFIX}" 1000 -v --dont-recommend "${@:2}" &>"${PREFIX}_daemons.log" &
    READER="${!}"

    sleep 5 &
    SLEEP="${!}"
    wait "${SLEEP}"

    "${DIR}/../../hotspot8/dacapo" -s "${size}" -t "${threads}" --simple --vengerov --avg-vengerov "${benchmark}" &>"${PREFIX}_vm.log" &
    DACAPO="${!}"

    stamp "Running ${benchmark} ${size} ${threads} ${runtime}..."
    sleep "${runtime}" &
    SLEEP="${!}"
    wait "${SLEEP}"

    stamp "Cleaning up ${benchmark} ${size} ${threads} ${runtime}..."
    kill "${DACAPO}" &>/dev/null
    sleep 5 &
    SLEEP="${!}"
    wait "${SLEEP}"
    kill "${READER}" &>/dev/null
    wait

    stamp "Generating graphs for ${benchmark} ${size} ${threads} ${runtime}..."
    for f in ${PREFIX}_*_raw.txt; do
        REGEX="^${PREFIX}_([0-9]+)_([a-zA-Z]+)_raw.txt$"
        [[ $f =~ $REGEX ]]
        PID="${BASH_REMATCH[1]}"
        MODEL="${BASH_REMATCH[2]}"
        "${DIR}/plot_readings_batch.py" "${MODEL}" "${f}" "${f}.pdf"
    done

    stamp "Merging graphs for ${benchmark} ${size} ${threads} ${runtime}..."
    "${DIR}/merge_pdfs.py" "${PREFIX}_*.pdf" "${PREFIX}_all.pdf"

    stamp "Completed ${benchmark} ${size} ${threads} ${runtime}"
    stamp
done

stamp "Done"
