	Command being timed: "cgexec -g memory:economem /home/callum/economics_memory_code/dynamic_sizing/hotspot8/openjdk8/jdk8/build/linux-x86_64-normal-server-release/jdk/bin/java -XX:+EconomemSimpleThroughput -XX:-EconomemVengerovThroughput -XX:EconomemMinHeap=276 -Xms10m -Xmx568m -XX:+DisableExplicitGC -XX:+UseAdaptiveSizePolicyWithSystemGC -jar /home/callum/economics_memory_code/dynamic_sizing/haskell_common/dacapo-9.12-bach.jar --no-pre-iteration-gc --scratch-directory /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub30_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/scratch1 -t 2 -n 13 -s large h2"
	User time (seconds): 373.62
	System time (seconds): 5.44
	Percent of CPU this job got: 81%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 7:46.64
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 470292
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 30166
	Minor (reclaiming a frame) page faults: 537428
	Voluntary context switches: 273644
	Involuntary context switches: 106447
	Swaps: 0
	File system inputs: 1803736
	File system outputs: 16616
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
