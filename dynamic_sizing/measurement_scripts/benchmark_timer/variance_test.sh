#!/bin/bash
# Run benchmarks unconstrained and in series, to see how stable their individual runtimes are.

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") input_file"
    exit 1
}

test -z "${1}" && usage
test ! -f "${1}" && usage

"${DIR}/timer_comparison_many" --unconstrained -r 30 "${1}"
