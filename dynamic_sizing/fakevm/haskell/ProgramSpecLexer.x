{
module ProgramSpecLexer where
}

%wrapper "posn"

$text = [A-Za-z0-9]
$whitespace = [\ \t]

tokens :-

  [1-9][0-9]*           { \_ s -> Integer (read s) }
  [0-9]+\.[0-9]+        { \_ s -> Float (read s) }
  ^phase                { \_ _ -> Phase }
  ^minheap              { \_ _ -> Minheap }
  transition            { \_ _ -> Transition }
  $text+                { \_ s -> String s }
  ^\#.*\n               ; -- Ignore comments
  ^$whitespace+         { \_ _ -> Indent }
  $whitespace|\n        ;

{
data Token = Integer Int |
             Float Double |
             String String |
             Indent |
             Phase |
             Minheap |
             Transition
           deriving (Eq,Show)
}
