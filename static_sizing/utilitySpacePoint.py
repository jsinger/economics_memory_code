#!/usr/bin/env python
# Measure the number of iterations completed by two or more concurrent benchmarks in a fixed time, at a given heap size combination

import argparse
import argtypes
import os
import os.path
import shutil
import staticutility_core as su
import subprocess
import tempfile
from time import sleep


def utilitySpacePoint(time, runs, outDir, resultsFile, bids, heaps, maxHeaps, strace=False):
    if len(bids) < 2:
        raise su.UtilityException("At least two benchmarks are required for a utility space run")

    if not su.allSameLength(bids, heaps, maxHeaps):
        raise su.UtilityException("Invalid argument lengths to utility space point")

    pointID = outDir.pointID(heaps, maxHeaps)
    seconds = su.timeToSeconds(time)


    class UtilityBenchmark:
        """A single benchmark within a utility space run"""

        def __init__(self, bid, num, heap, maxHeap):
            self.b = bid
            self.num = int(num)
            self.heap = int(heap)
            self.maxHeap = int(maxHeap)

        def rawFile(self, run):
            return outDir.pointRawFile(pointID, run, runs, self.num)

        def iterationsFile(self):
            return outDir.pointIterationsFile(pointID, self.num)

        def start(self, run):
            self.scratch = os.path.realpath(tempfile.mkdtemp())
            with open(self.rawFile(run), "w") as f, open(os.devnull) as devnull:
                args = []
                if strace:
                    args = ["strace", "-ttt", "-T", "-f"]
                args += ["java", "-Xms%dm" % self.heap, "-Xmx%dm" % self.heap, "-jar", su.dacapo(), "--scratch-directory", self.scratch, "-t", self.b.threads, "-s", self.b.inputSize, "-n", "1000000", self.b.name]
                self.p = subprocess.Popen(args, stdin=devnull, stdout=f, stderr=subprocess.STDOUT)

        def kill(self):
            self.p.send_signal(15)

        def wait(self):
            self.p.wait()
            shutil.rmtree(self.scratch)
            self.p = None
            self.scratch = None

        def addToIterations(self, run):
            with open(self.iterationsFile(), "a") as f:
                f.write("%d\n" % su.benchmarkIterations(self.rawFile(run)))

        def calculateResults(self):
            l = su.fileToList(self.iterationsFile(), float)
            self.average = su.average(l)
            self.stddev = su.stddev(l)
            self.itersPerSec = self.average / float(seconds)
            self.stddevItersPerSec = self.stddev / float(seconds)
            self.median = su.median(l)
            self.medianItersPerSec = self.median / float(seconds)


    benchmarks = map(lambda b: UtilityBenchmark(*b), zip(bids, range(1, len(bids) + 1), heaps, maxHeaps))

    for i in range(runs):
        print "%s, time: %s, inputs: %s, threads %s, run %d of %d" % (pointID, time, " ".join(map(lambda b: b.b.inputSize, benchmarks)), " ".join(map(lambda b: b.b.threads, benchmarks)), i + 1, runs)

        for b in benchmarks:
            b.start(i)

        sleep(seconds)

        for b in benchmarks:
            b.kill()

        for b in benchmarks:
            b.wait()

        for b in benchmarks:
            b.addToIterations(i)


    for b in benchmarks:
        b.calculateResults()

    results = su.UtilityRawResult(map(lambda b: b.heap, benchmarks), map(lambda b: b.average, benchmarks), map(lambda b: b.stddev, benchmarks), map(lambda b: b.itersPerSec, benchmarks), map(lambda b: b.stddevItersPerSec, benchmarks), map(lambda b: b.median, benchmarks), map(lambda b: b.medianItersPerSec, benchmarks))

    results.append(resultsFile)



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Measure the number of iterations completed by two or more concurrent benchmarks in a fixed time, at a given heap size combination")

    parser.add_argument("time", help="how long to run for, in a format suitable for input to 'sleep'")
    parser.add_argument("runs", type=argtypes.positive_int, help="how many times to run the measurement, for taking averages")
    parser.add_argument("outDir", type=argtypes.existing_absolute_dir, help="where to save the output")
    parser.add_argument("resultsFile", type=argtypes.absolute_path, help="running log file for all the results")

    parser.add_argument("-b", "--benchmark", action="append", nargs=6, default=[], metavar=("benchmark", "time", "inputSize", "threads", "heap", "maxHeap"), help="a benchmark to include in the measurement; at least two are required")
    parser.add_argument("--strace", action="store_true", help="if present, run the benchmarks inside strace, for system call investigation")

    args = parser.parse_args()
    outDir = su.UtilitySpaceDir(args.outDir)

    if len(args.benchmark) < 2:
        parser.error("At least two benchmarks must be specified")

    bids = []
    heaps = []
    maxHeaps = []
    for b in args.benchmark:
        bids.append(su.BenchmarkId(b[0], b[1], b[2], b[3]))
        heaps.append(int(b[4]))
        maxHeaps.append(int(b[5]))

    utilitySpacePoint(args.time, args.runs, outDir, args.resultsFile, bids, heaps, maxHeaps, strace=args.strace)
