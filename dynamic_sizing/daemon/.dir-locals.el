((nil
  (flycheck-clang-language-standard . "c++11")
  (flycheck-clang-include-path . ("../common" "./mct/include" "./tclap/include" "/usr/include/python2.7" "/usr/include/x86_64-linux-gnu/python2.7"))
  (linuxconfig-compile-command . "./build.sh")
  (linuxconfig-compile-clean-command . "./build.sh clean")
  (linuxconfig-compile-target-command . "./build.sh")))
