economemd
    total memory: 2506 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub21_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 5655: heap size 9 MB, throughput 0.990735
Clients: 1
Client 5655 has a minimum heap size of 30 MB
Reading from 5654: heap size 9 MB, throughput 0.991426
Clients: 2
Client 5654 has a minimum heap size of 1223 MB
Reading from 5655: heap size 9 MB, throughput 0.961943
Reading from 5655: heap size 9 MB, throughput 0.969123
Reading from 5654: heap size 9 MB, throughput 0.974183
Reading from 5655: heap size 9 MB, throughput 0.95891
Reading from 5655: heap size 11 MB, throughput 0.711703
Reading from 5655: heap size 11 MB, throughput 0.365292
Reading from 5655: heap size 16 MB, throughput 0.711555
Reading from 5655: heap size 16 MB, throughput 0.744716
Reading from 5655: heap size 24 MB, throughput 0.740878
Reading from 5654: heap size 9 MB, throughput 0.960234
Reading from 5655: heap size 24 MB, throughput 0.820161
Reading from 5655: heap size 34 MB, throughput 0.900165
Reading from 5655: heap size 34 MB, throughput 0.886953
Reading from 5655: heap size 50 MB, throughput 0.935865
Reading from 5654: heap size 9 MB, throughput 0.937423
Reading from 5655: heap size 50 MB, throughput 0.927115
Reading from 5655: heap size 75 MB, throughput 0.960548
Reading from 5655: heap size 75 MB, throughput 0.96048
Reading from 5655: heap size 116 MB, throughput 0.966604
Reading from 5655: heap size 116 MB, throughput 0.954484
Reading from 5654: heap size 11 MB, throughput 0.973569
Reading from 5654: heap size 11 MB, throughput 0.936229
Reading from 5654: heap size 17 MB, throughput 0.968925
Reading from 5654: heap size 17 MB, throughput 0.545472
Reading from 5654: heap size 30 MB, throughput 0.946848
Reading from 5654: heap size 31 MB, throughput 0.948378
Reading from 5654: heap size 34 MB, throughput 0.359115
Reading from 5654: heap size 47 MB, throughput 0.912601
Reading from 5655: heap size 159 MB, throughput 0.972857
Reading from 5654: heap size 49 MB, throughput 0.861353
Reading from 5654: heap size 51 MB, throughput 0.313381
Reading from 5654: heap size 69 MB, throughput 0.846463
Reading from 5654: heap size 70 MB, throughput 0.790107
Reading from 5654: heap size 71 MB, throughput 0.650346
Reading from 5655: heap size 164 MB, throughput 0.870117
Reading from 5654: heap size 74 MB, throughput 0.71759
Reading from 5654: heap size 77 MB, throughput 0.189854
Reading from 5654: heap size 101 MB, throughput 0.672849
Reading from 5654: heap size 104 MB, throughput 0.582979
Reading from 5654: heap size 106 MB, throughput 0.553811
Reading from 5655: heap size 224 MB, throughput 0.983302
Reading from 5654: heap size 109 MB, throughput 0.635651
Reading from 5654: heap size 114 MB, throughput 0.118598
Reading from 5654: heap size 143 MB, throughput 0.723111
Reading from 5654: heap size 146 MB, throughput 0.724503
Reading from 5654: heap size 147 MB, throughput 0.783639
Reading from 5654: heap size 147 MB, throughput 0.655707
Reading from 5654: heap size 150 MB, throughput 0.589289
Reading from 5655: heap size 235 MB, throughput 0.897774
Reading from 5654: heap size 154 MB, throughput 0.59238
Reading from 5654: heap size 158 MB, throughput 0.639008
Reading from 5654: heap size 164 MB, throughput 0.129932
Reading from 5654: heap size 195 MB, throughput 0.524283
Reading from 5654: heap size 201 MB, throughput 0.634654
Reading from 5654: heap size 204 MB, throughput 0.146877
Reading from 5654: heap size 240 MB, throughput 0.361275
Reading from 5655: heap size 263 MB, throughput 0.996527
Reading from 5654: heap size 238 MB, throughput 0.600644
Reading from 5654: heap size 241 MB, throughput 0.572391
Reading from 5654: heap size 243 MB, throughput 0.497229
Reading from 5654: heap size 246 MB, throughput 0.63111
Reading from 5654: heap size 251 MB, throughput 0.618735
Reading from 5655: heap size 263 MB, throughput 0.867413
Reading from 5654: heap size 256 MB, throughput 0.453985
Reading from 5654: heap size 260 MB, throughput 0.557306
Reading from 5654: heap size 265 MB, throughput 0.0831302
Reading from 5655: heap size 302 MB, throughput 0.994685
Reading from 5654: heap size 310 MB, throughput 0.622773
Reading from 5654: heap size 313 MB, throughput 0.618308
Reading from 5654: heap size 315 MB, throughput 0.121412
Reading from 5654: heap size 361 MB, throughput 0.522002
Reading from 5655: heap size 306 MB, throughput 0.995584
Reading from 5654: heap size 357 MB, throughput 0.713048
Reading from 5654: heap size 361 MB, throughput 0.573196
Reading from 5654: heap size 363 MB, throughput 0.551826
Reading from 5654: heap size 365 MB, throughput 0.601087
Reading from 5654: heap size 368 MB, throughput 0.510698
Reading from 5654: heap size 375 MB, throughput 0.408554
Reading from 5654: heap size 381 MB, throughput 0.45186
Reading from 5655: heap size 319 MB, throughput 0.988896
Equal recommendation: 1253 MB each
Reading from 5654: heap size 390 MB, throughput 0.428907
Reading from 5655: heap size 322 MB, throughput 0.981874
Reading from 5654: heap size 397 MB, throughput 0.0713194
Reading from 5654: heap size 453 MB, throughput 0.47642
Reading from 5654: heap size 453 MB, throughput 0.565736
Reading from 5654: heap size 456 MB, throughput 0.565738
Reading from 5655: heap size 342 MB, throughput 0.988539
Reading from 5654: heap size 454 MB, throughput 0.0991392
Reading from 5654: heap size 512 MB, throughput 0.478659
Reading from 5654: heap size 506 MB, throughput 0.466292
Reading from 5654: heap size 511 MB, throughput 0.537085
Reading from 5654: heap size 512 MB, throughput 0.502322
Reading from 5655: heap size 342 MB, throughput 0.90521
Reading from 5654: heap size 515 MB, throughput 0.57264
Reading from 5654: heap size 520 MB, throughput 0.541691
Reading from 5655: heap size 370 MB, throughput 0.994482
Reading from 5654: heap size 523 MB, throughput 0.0924233
Reading from 5654: heap size 586 MB, throughput 0.443845
Reading from 5654: heap size 593 MB, throughput 0.5457
Reading from 5654: heap size 594 MB, throughput 0.508291
Reading from 5654: heap size 598 MB, throughput 0.475373
Reading from 5655: heap size 376 MB, throughput 0.99167
Reading from 5654: heap size 603 MB, throughput 0.486082
Reading from 5654: heap size 609 MB, throughput 0.495747
Reading from 5655: heap size 392 MB, throughput 0.985688
Reading from 5654: heap size 614 MB, throughput 0.10466
Reading from 5655: heap size 394 MB, throughput 0.987655
Reading from 5654: heap size 687 MB, throughput 0.817323
Reading from 5654: heap size 691 MB, throughput 0.783975
Reading from 5655: heap size 412 MB, throughput 0.983739
Reading from 5654: heap size 696 MB, throughput 0.834891
Equal recommendation: 1253 MB each
Reading from 5654: heap size 697 MB, throughput 0.421036
Reading from 5654: heap size 713 MB, throughput 0.471538
Reading from 5654: heap size 725 MB, throughput 0.49607
Reading from 5655: heap size 415 MB, throughput 0.982215
Reading from 5654: heap size 731 MB, throughput 0.288515
Reading from 5655: heap size 436 MB, throughput 0.976506
Reading from 5654: heap size 737 MB, throughput 0.0219573
Reading from 5654: heap size 725 MB, throughput 0.226737
Reading from 5654: heap size 803 MB, throughput 0.432379
Reading from 5655: heap size 439 MB, throughput 0.956485
Reading from 5654: heap size 806 MB, throughput 0.188002
Reading from 5655: heap size 465 MB, throughput 0.971375
Reading from 5654: heap size 896 MB, throughput 0.410422
Reading from 5654: heap size 898 MB, throughput 0.670038
Reading from 5654: heap size 905 MB, throughput 0.661621
Reading from 5654: heap size 906 MB, throughput 0.662231
Reading from 5654: heap size 907 MB, throughput 0.836535
Reading from 5654: heap size 909 MB, throughput 0.841168
Reading from 5654: heap size 909 MB, throughput 0.803549
Reading from 5655: heap size 468 MB, throughput 0.985634
Reading from 5654: heap size 910 MB, throughput 0.928871
Reading from 5654: heap size 907 MB, throughput 0.860465
Reading from 5654: heap size 911 MB, throughput 0.910484
Reading from 5654: heap size 890 MB, throughput 0.797328
Reading from 5654: heap size 785 MB, throughput 0.728388
Reading from 5654: heap size 875 MB, throughput 0.727527
Reading from 5655: heap size 501 MB, throughput 0.992193
Reading from 5654: heap size 791 MB, throughput 0.771296
Reading from 5654: heap size 864 MB, throughput 0.749894
Reading from 5654: heap size 873 MB, throughput 0.792941
Reading from 5654: heap size 865 MB, throughput 0.843018
Reading from 5654: heap size 869 MB, throughput 0.766458
Reading from 5654: heap size 862 MB, throughput 0.818642
Reading from 5654: heap size 867 MB, throughput 0.845647
Reading from 5655: heap size 502 MB, throughput 0.978235
Reading from 5654: heap size 861 MB, throughput 0.309995
Equal recommendation: 1253 MB each
Reading from 5655: heap size 525 MB, throughput 0.987491
Reading from 5654: heap size 938 MB, throughput 0.972516
Reading from 5654: heap size 936 MB, throughput 0.888676
Reading from 5654: heap size 942 MB, throughput 0.758149
Reading from 5655: heap size 529 MB, throughput 0.984869
Reading from 5654: heap size 940 MB, throughput 0.798069
Reading from 5654: heap size 942 MB, throughput 0.812182
Reading from 5654: heap size 942 MB, throughput 0.820333
Reading from 5654: heap size 945 MB, throughput 0.830793
Reading from 5654: heap size 946 MB, throughput 0.835511
Reading from 5654: heap size 948 MB, throughput 0.817115
Reading from 5654: heap size 950 MB, throughput 0.860639
Reading from 5655: heap size 556 MB, throughput 0.987288
Reading from 5654: heap size 952 MB, throughput 0.865295
Reading from 5654: heap size 957 MB, throughput 0.888701
Reading from 5654: heap size 959 MB, throughput 0.737671
Reading from 5654: heap size 947 MB, throughput 0.683364
Reading from 5654: heap size 967 MB, throughput 0.675246
Reading from 5655: heap size 557 MB, throughput 0.966875
Reading from 5655: heap size 586 MB, throughput 0.97699
Reading from 5654: heap size 978 MB, throughput 0.0556505
Reading from 5654: heap size 1094 MB, throughput 0.537843
Reading from 5654: heap size 1098 MB, throughput 0.735462
Reading from 5654: heap size 1100 MB, throughput 0.728341
Reading from 5654: heap size 1107 MB, throughput 0.715969
Reading from 5654: heap size 1109 MB, throughput 0.6535
Reading from 5654: heap size 1114 MB, throughput 0.717993
Reading from 5654: heap size 1118 MB, throughput 0.631833
Reading from 5655: heap size 584 MB, throughput 0.944508
Equal recommendation: 1253 MB each
Reading from 5654: heap size 1129 MB, throughput 0.96071
Reading from 5655: heap size 604 MB, throughput 0.993039
Reading from 5655: heap size 607 MB, throughput 0.988848
Reading from 5654: heap size 1130 MB, throughput 0.97473
Reading from 5655: heap size 634 MB, throughput 0.995575
Reading from 5655: heap size 637 MB, throughput 0.969549
Reading from 5654: heap size 1139 MB, throughput 0.967055
Reading from 5655: heap size 666 MB, throughput 0.987319
Reading from 5654: heap size 1141 MB, throughput 0.957143
Equal recommendation: 1253 MB each
Reading from 5655: heap size 667 MB, throughput 0.994736
Reading from 5654: heap size 1153 MB, throughput 0.965795
Reading from 5655: heap size 696 MB, throughput 0.985088
Reading from 5655: heap size 701 MB, throughput 0.989697
Reading from 5654: heap size 1156 MB, throughput 0.969385
Reading from 5655: heap size 732 MB, throughput 0.989493
Reading from 5654: heap size 1160 MB, throughput 0.969475
Reading from 5655: heap size 734 MB, throughput 0.99085
Equal recommendation: 1253 MB each
Reading from 5655: heap size 763 MB, throughput 0.994807
Reading from 5654: heap size 1163 MB, throughput 0.974018
Reading from 5655: heap size 766 MB, throughput 0.98923
Reading from 5654: heap size 1161 MB, throughput 0.964825
Reading from 5655: heap size 796 MB, throughput 0.991049
Reading from 5654: heap size 1164 MB, throughput 0.96754
Reading from 5655: heap size 796 MB, throughput 0.988772
Equal recommendation: 1253 MB each
Reading from 5655: heap size 827 MB, throughput 0.990775
Reading from 5654: heap size 1170 MB, throughput 0.969577
Reading from 5655: heap size 828 MB, throughput 0.990681
Reading from 5654: heap size 1170 MB, throughput 0.970777
Reading from 5655: heap size 842 MB, throughput 0.988084
Reading from 5654: heap size 1175 MB, throughput 0.968457
Reading from 5655: heap size 843 MB, throughput 0.991172
Equal recommendation: 1253 MB each
Reading from 5654: heap size 1178 MB, throughput 0.962591
Reading from 5655: heap size 845 MB, throughput 0.989381
Reading from 5654: heap size 1182 MB, throughput 0.965953
Reading from 5655: heap size 845 MB, throughput 0.991742
Reading from 5655: heap size 845 MB, throughput 0.993082
Reading from 5654: heap size 1186 MB, throughput 0.970196
Equal recommendation: 1253 MB each
Reading from 5655: heap size 846 MB, throughput 0.990742
Reading from 5654: heap size 1191 MB, throughput 0.963498
Reading from 5655: heap size 846 MB, throughput 0.992843
Reading from 5654: heap size 1196 MB, throughput 0.969821
Reading from 5655: heap size 846 MB, throughput 0.992493
Reading from 5654: heap size 1202 MB, throughput 0.970608
Reading from 5655: heap size 846 MB, throughput 0.997259
Equal recommendation: 1253 MB each
Reading from 5655: heap size 847 MB, throughput 0.991133
Reading from 5654: heap size 1205 MB, throughput 0.632476
Reading from 5655: heap size 848 MB, throughput 0.992572
Reading from 5655: heap size 838 MB, throughput 0.992197
Reading from 5654: heap size 1278 MB, throughput 0.994464
Reading from 5655: heap size 848 MB, throughput 0.992248
Equal recommendation: 1253 MB each
Reading from 5654: heap size 1277 MB, throughput 0.991348
Reading from 5655: heap size 837 MB, throughput 0.992416
Reading from 5654: heap size 1282 MB, throughput 0.988103
Reading from 5655: heap size 847 MB, throughput 0.991922
Reading from 5654: heap size 1157 MB, throughput 0.984678
Reading from 5655: heap size 847 MB, throughput 0.991086
Equal recommendation: 1253 MB each
Reading from 5655: heap size 848 MB, throughput 0.991106
Reading from 5654: heap size 1287 MB, throughput 0.98573
Reading from 5655: heap size 848 MB, throughput 0.9902
Reading from 5654: heap size 1171 MB, throughput 0.984358
Reading from 5655: heap size 847 MB, throughput 0.991991
Reading from 5654: heap size 1274 MB, throughput 0.981762
Reading from 5655: heap size 848 MB, throughput 0.99136
Equal recommendation: 1253 MB each
Reading from 5654: heap size 1185 MB, throughput 0.973318
Reading from 5655: heap size 847 MB, throughput 0.991702
Reading from 5655: heap size 847 MB, throughput 0.989039
Reading from 5654: heap size 1261 MB, throughput 0.978927
Reading from 5655: heap size 849 MB, throughput 0.992785
Reading from 5654: heap size 1201 MB, throughput 0.975932
Reading from 5655: heap size 841 MB, throughput 0.991036
Equal recommendation: 1253 MB each
Reading from 5654: heap size 1259 MB, throughput 0.968884
Reading from 5655: heap size 848 MB, throughput 0.994083
Reading from 5654: heap size 1214 MB, throughput 0.974468
Reading from 5655: heap size 849 MB, throughput 0.992742
Reading from 5654: heap size 1253 MB, throughput 0.975455
Reading from 5655: heap size 850 MB, throughput 0.993295
Equal recommendation: 1253 MB each
Reading from 5655: heap size 850 MB, throughput 0.992962
Reading from 5654: heap size 1256 MB, throughput 0.964806
Reading from 5655: heap size 850 MB, throughput 0.991586
Reading from 5654: heap size 1258 MB, throughput 0.968892
Reading from 5655: heap size 850 MB, throughput 0.99295
Reading from 5654: heap size 1261 MB, throughput 0.971334
Reading from 5655: heap size 850 MB, throughput 0.996607
Equal recommendation: 1253 MB each
Reading from 5654: heap size 1263 MB, throughput 0.968708
Reading from 5655: heap size 850 MB, throughput 0.992606
Reading from 5655: heap size 850 MB, throughput 0.992438
Reading from 5654: heap size 1266 MB, throughput 0.965052
Reading from 5655: heap size 841 MB, throughput 0.992043
Reading from 5654: heap size 1267 MB, throughput 0.96593
Reading from 5655: heap size 849 MB, throughput 0.99203
Equal recommendation: 1253 MB each
Reading from 5654: heap size 1220 MB, throughput 0.967011
Reading from 5655: heap size 849 MB, throughput 0.991313
Reading from 5654: heap size 1268 MB, throughput 0.967037
Reading from 5655: heap size 848 MB, throughput 0.991678
Reading from 5654: heap size 1216 MB, throughput 0.964146
Reading from 5655: heap size 849 MB, throughput 0.991644
Equal recommendation: 1253 MB each
Reading from 5654: heap size 1266 MB, throughput 0.967314
Reading from 5655: heap size 849 MB, throughput 0.990755
Reading from 5655: heap size 849 MB, throughput 0.990237
Reading from 5654: heap size 1214 MB, throughput 0.970286
Reading from 5655: heap size 850 MB, throughput 0.993647
Reading from 5654: heap size 1263 MB, throughput 0.973491
Reading from 5655: heap size 850 MB, throughput 0.991888
Equal recommendation: 1253 MB each
Reading from 5654: heap size 1213 MB, throughput 0.96527
Reading from 5655: heap size 850 MB, throughput 0.991271
Reading from 5654: heap size 1259 MB, throughput 0.964516
Reading from 5655: heap size 850 MB, throughput 0.995449
Reading from 5655: heap size 851 MB, throughput 0.990754
Reading from 5655: heap size 846 MB, throughput 0.990813
Equal recommendation: 1253 MB each
Reading from 5655: heap size 850 MB, throughput 0.990224
Reading from 5655: heap size 850 MB, throughput 0.989546
Reading from 5655: heap size 849 MB, throughput 0.990437
Reading from 5654: heap size 1212 MB, throughput 0.989969
Reading from 5655: heap size 844 MB, throughput 0.992351
Equal recommendation: 1253 MB each
Reading from 5655: heap size 850 MB, throughput 0.992434
Reading from 5655: heap size 849 MB, throughput 0.99127
Reading from 5655: heap size 849 MB, throughput 0.991423
Reading from 5655: heap size 834 MB, throughput 0.991961
Equal recommendation: 1253 MB each
Reading from 5655: heap size 849 MB, throughput 0.996434
Reading from 5654: heap size 1241 MB, throughput 0.933495
Reading from 5655: heap size 849 MB, throughput 0.99556
Reading from 5655: heap size 849 MB, throughput 0.990441
Reading from 5654: heap size 1289 MB, throughput 0.974488
Reading from 5655: heap size 849 MB, throughput 0.990567
Equal recommendation: 1253 MB each
Reading from 5655: heap size 849 MB, throughput 0.991075
Reading from 5654: heap size 1282 MB, throughput 0.989148
Reading from 5654: heap size 1219 MB, throughput 0.885078
Reading from 5654: heap size 1289 MB, throughput 0.796481
Reading from 5654: heap size 1286 MB, throughput 0.693714
Reading from 5654: heap size 1291 MB, throughput 0.708874
Reading from 5654: heap size 1301 MB, throughput 0.658984
Reading from 5654: heap size 1302 MB, throughput 0.698803
Reading from 5654: heap size 1308 MB, throughput 0.708666
Reading from 5655: heap size 849 MB, throughput 0.991257
Reading from 5654: heap size 1309 MB, throughput 0.694861
Reading from 5654: heap size 1311 MB, throughput 0.702026
Reading from 5654: heap size 1312 MB, throughput 0.788064
Reading from 5654: heap size 1184 MB, throughput 0.948299
Reading from 5655: heap size 848 MB, throughput 0.988067
Reading from 5654: heap size 1300 MB, throughput 0.990606
Reading from 5655: heap size 849 MB, throughput 0.991678
Reading from 5654: heap size 1312 MB, throughput 0.988831
Equal recommendation: 1253 MB each
Reading from 5655: heap size 850 MB, throughput 0.989252
Reading from 5654: heap size 1316 MB, throughput 0.987065
Reading from 5655: heap size 850 MB, throughput 0.992666
Reading from 5654: heap size 1105 MB, throughput 0.985348
Reading from 5655: heap size 849 MB, throughput 0.989938
Reading from 5654: heap size 1321 MB, throughput 0.979301
Reading from 5655: heap size 846 MB, throughput 0.99139
Reading from 5654: heap size 1118 MB, throughput 0.98517
Equal recommendation: 1253 MB each
Reading from 5654: heap size 1309 MB, throughput 0.983018
Reading from 5655: heap size 849 MB, throughput 0.992084
Reading from 5654: heap size 1134 MB, throughput 0.965537
Reading from 5655: heap size 840 MB, throughput 0.990221
Client 5655 died
Clients: 1
Reading from 5654: heap size 1293 MB, throughput 0.983329
Reading from 5654: heap size 1149 MB, throughput 0.982958
Recommendation: one client; give it all the memory
Reading from 5654: heap size 1274 MB, throughput 0.983848
Reading from 5654: heap size 1164 MB, throughput 0.980256
Reading from 5654: heap size 1254 MB, throughput 0.980826
Reading from 5654: heap size 1178 MB, throughput 0.978984
Recommendation: one client; give it all the memory
Reading from 5654: heap size 1253 MB, throughput 0.978787
Reading from 5654: heap size 1257 MB, throughput 0.97746
Reading from 5654: heap size 1251 MB, throughput 0.977583
Recommendation: one client; give it all the memory
Reading from 5654: heap size 1254 MB, throughput 0.978103
Reading from 5654: heap size 1251 MB, throughput 0.978457
Reading from 5654: heap size 1253 MB, throughput 0.978859
Recommendation: one client; give it all the memory
Reading from 5654: heap size 1252 MB, throughput 0.75387
Reading from 5654: heap size 1303 MB, throughput 0.995415
Reading from 5654: heap size 1299 MB, throughput 0.993928
Reading from 5654: heap size 1305 MB, throughput 0.991927
Recommendation: one client; give it all the memory
Reading from 5654: heap size 1310 MB, throughput 0.991584
Reading from 5654: heap size 1310 MB, throughput 0.990003
Reading from 5654: heap size 1303 MB, throughput 0.98848
Recommendation: one client; give it all the memory
Reading from 5654: heap size 1218 MB, throughput 0.987608
Reading from 5654: heap size 1292 MB, throughput 0.987147
Reading from 5654: heap size 1233 MB, throughput 0.985377
Recommendation: one client; give it all the memory
Reading from 5654: heap size 1289 MB, throughput 0.983888
Reading from 5654: heap size 1292 MB, throughput 0.983044
Reading from 5654: heap size 1294 MB, throughput 0.982704
Reading from 5654: heap size 1295 MB, throughput 0.982265
Recommendation: one client; give it all the memory
Reading from 5654: heap size 1297 MB, throughput 0.980284
Reading from 5654: heap size 1301 MB, throughput 0.980558
Reading from 5654: heap size 1305 MB, throughput 0.978881
Recommendation: one client; give it all the memory
Reading from 5654: heap size 1311 MB, throughput 0.976905
Reading from 5654: heap size 1317 MB, throughput 0.977935
Reading from 5654: heap size 1321 MB, throughput 0.978536
Recommendation: one client; give it all the memory
Reading from 5654: heap size 1327 MB, throughput 0.978685
Reading from 5654: heap size 1328 MB, throughput 0.978897
Reading from 5654: heap size 1335 MB, throughput 0.97884
Recommendation: one client; give it all the memory
Reading from 5654: heap size 1335 MB, throughput 0.979577
Reading from 5654: heap size 1341 MB, throughput 0.979222
Reading from 5654: heap size 1341 MB, throughput 0.979334
Recommendation: one client; give it all the memory
Reading from 5654: heap size 1345 MB, throughput 0.978655
Reading from 5654: heap size 1345 MB, throughput 0.978727
Reading from 5654: heap size 1349 MB, throughput 0.979859
Recommendation: one client; give it all the memory
Reading from 5654: heap size 1350 MB, throughput 0.97996
Reading from 5654: heap size 1354 MB, throughput 0.980249
Recommendation: one client; give it all the memory
Reading from 5654: heap size 1354 MB, throughput 0.980989
Reading from 5654: heap size 1358 MB, throughput 0.979189
Reading from 5654: heap size 1359 MB, throughput 0.980281
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 5654: heap size 1364 MB, throughput 0.901924
Recommendation: one client; give it all the memory
Reading from 5654: heap size 1575 MB, throughput 0.989713
Recommendation: one client; give it all the memory
Reading from 5654: heap size 1616 MB, throughput 0.991593
Reading from 5654: heap size 1616 MB, throughput 0.907004
Reading from 5654: heap size 1617 MB, throughput 0.839514
Reading from 5654: heap size 1616 MB, throughput 0.741698
Reading from 5654: heap size 1644 MB, throughput 0.79916
Reading from 5654: heap size 1656 MB, throughput 0.775354
Reading from 5654: heap size 1688 MB, throughput 0.811313
Reading from 5654: heap size 1696 MB, throughput 0.857411
Client 5654 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
