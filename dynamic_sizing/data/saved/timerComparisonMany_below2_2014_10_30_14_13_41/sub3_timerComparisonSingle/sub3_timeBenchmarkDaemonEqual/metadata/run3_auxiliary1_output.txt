economemd
    total memory: 828 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_30_14_13_41/sub3_timerComparisonSingle/sub3_timeBenchmarkDaemonEqual/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
TimerThread: starting
ReadingThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 32462: heap size 9 MB, throughput 0.991709
Clients: 1
Client 32462 has a minimum heap size of 276 MB
Reading from 32461: heap size 9 MB, throughput 0.989066
Clients: 2
Client 32461 has a minimum heap size of 276 MB
Reading from 32461: heap size 9 MB, throughput 0.975459
Reading from 32462: heap size 9 MB, throughput 0.967583
Reading from 32462: heap size 9 MB, throughput 0.962429
Reading from 32462: heap size 9 MB, throughput 0.924527
Reading from 32461: heap size 11 MB, throughput 0.972637
Reading from 32462: heap size 11 MB, throughput 0.98534
Reading from 32461: heap size 11 MB, throughput 0.989339
Reading from 32462: heap size 11 MB, throughput 0.970928
Reading from 32462: heap size 17 MB, throughput 0.955423
Reading from 32461: heap size 15 MB, throughput 0.765543
Reading from 32461: heap size 19 MB, throughput 0.973193
Reading from 32462: heap size 17 MB, throughput 0.537707
Reading from 32461: heap size 22 MB, throughput 0.967165
Reading from 32462: heap size 30 MB, throughput 0.969267
Reading from 32462: heap size 31 MB, throughput 0.921234
Reading from 32461: heap size 25 MB, throughput 0.361566
Reading from 32461: heap size 36 MB, throughput 0.930233
Reading from 32461: heap size 40 MB, throughput 0.842761
Reading from 32461: heap size 41 MB, throughput 0.796952
Reading from 32462: heap size 35 MB, throughput 0.369413
Reading from 32461: heap size 42 MB, throughput 0.625976
Reading from 32462: heap size 49 MB, throughput 0.849927
Reading from 32461: heap size 45 MB, throughput 0.73133
Reading from 32461: heap size 47 MB, throughput 0.646967
Reading from 32462: heap size 50 MB, throughput 0.888577
Reading from 32461: heap size 53 MB, throughput 0.1708
Reading from 32461: heap size 69 MB, throughput 0.654662
Reading from 32462: heap size 52 MB, throughput 0.217429
Reading from 32462: heap size 73 MB, throughput 0.701644
Reading from 32461: heap size 75 MB, throughput 0.278972
Reading from 32462: heap size 74 MB, throughput 0.269936
Reading from 32461: heap size 97 MB, throughput 0.782849
Reading from 32461: heap size 100 MB, throughput 0.73756
Reading from 32462: heap size 100 MB, throughput 0.581437
Reading from 32462: heap size 101 MB, throughput 0.742156
Reading from 32461: heap size 102 MB, throughput 0.633
Reading from 32462: heap size 103 MB, throughput 0.656646
Reading from 32461: heap size 105 MB, throughput 0.159358
Reading from 32462: heap size 105 MB, throughput 0.187866
Reading from 32461: heap size 135 MB, throughput 0.615132
Reading from 32462: heap size 141 MB, throughput 0.789611
Reading from 32461: heap size 140 MB, throughput 0.648379
Reading from 32462: heap size 142 MB, throughput 0.751495
Reading from 32461: heap size 142 MB, throughput 0.744322
Reading from 32462: heap size 145 MB, throughput 0.748758
Reading from 32461: heap size 147 MB, throughput 0.744569
Reading from 32462: heap size 148 MB, throughput 0.771467
Reading from 32461: heap size 153 MB, throughput 0.667356
Reading from 32461: heap size 155 MB, throughput 0.569212
Reading from 32461: heap size 161 MB, throughput 0.562783
Reading from 32462: heap size 152 MB, throughput 0.150868
Reading from 32462: heap size 192 MB, throughput 0.545348
Reading from 32462: heap size 195 MB, throughput 0.713555
Reading from 32461: heap size 166 MB, throughput 0.14683
Reading from 32462: heap size 198 MB, throughput 0.72714
Reading from 32461: heap size 203 MB, throughput 0.462716
Reading from 32462: heap size 208 MB, throughput 0.925232
Reading from 32461: heap size 215 MB, throughput 0.507941
Reading from 32461: heap size 252 MB, throughput 0.867487
Reading from 32461: heap size 258 MB, throughput 0.763193
Reading from 32462: heap size 210 MB, throughput 0.413275
Reading from 32461: heap size 259 MB, throughput 0.822912
Reading from 32462: heap size 265 MB, throughput 0.647689
Reading from 32461: heap size 262 MB, throughput 0.785263
Reading from 32462: heap size 266 MB, throughput 0.758965
Reading from 32461: heap size 263 MB, throughput 0.655185
Reading from 32462: heap size 269 MB, throughput 0.854123
Reading from 32461: heap size 267 MB, throughput 0.836084
Reading from 32462: heap size 270 MB, throughput 0.807207
Reading from 32461: heap size 268 MB, throughput 0.843515
Reading from 32462: heap size 274 MB, throughput 0.900135
Reading from 32461: heap size 271 MB, throughput 0.782595
Reading from 32462: heap size 275 MB, throughput 0.8212
Reading from 32462: heap size 278 MB, throughput 0.625504
Reading from 32461: heap size 274 MB, throughput 0.644182
Reading from 32462: heap size 282 MB, throughput 0.74824
Reading from 32461: heap size 280 MB, throughput 0.775181
Reading from 32462: heap size 288 MB, throughput 0.776149
Reading from 32461: heap size 283 MB, throughput 0.797704
Reading from 32462: heap size 291 MB, throughput 0.907157
Reading from 32461: heap size 282 MB, throughput 0.900121
Reading from 32462: heap size 294 MB, throughput 0.932366
Reading from 32461: heap size 285 MB, throughput 0.893005
Reading from 32462: heap size 296 MB, throughput 0.601656
Reading from 32462: heap size 303 MB, throughput 0.647892
Reading from 32461: heap size 282 MB, throughput 0.719333
Reading from 32462: heap size 306 MB, throughput 0.674235
Reading from 32462: heap size 308 MB, throughput 0.854281
Reading from 32462: heap size 310 MB, throughput 0.896344
Reading from 32462: heap size 315 MB, throughput 0.787026
Reading from 32462: heap size 316 MB, throughput 0.728215
Reading from 32461: heap size 286 MB, throughput 0.0923832
Reading from 32462: heap size 321 MB, throughput 0.735893
Reading from 32461: heap size 329 MB, throughput 0.682524
Reading from 32462: heap size 321 MB, throughput 0.688453
Reading from 32462: heap size 327 MB, throughput 0.72196
Reading from 32461: heap size 329 MB, throughput 0.892479
Reading from 32462: heap size 327 MB, throughput 0.926932
Reading from 32461: heap size 333 MB, throughput 0.905591
Reading from 32461: heap size 334 MB, throughput 0.741886
Reading from 32461: heap size 339 MB, throughput 0.710154
Reading from 32461: heap size 339 MB, throughput 0.673712
Equal recommendation: 414 MB each
Reading from 32461: heap size 347 MB, throughput 0.508312
Reading from 32461: heap size 346 MB, throughput 0.611365
Reading from 32461: heap size 356 MB, throughput 0.922331
Reading from 32462: heap size 334 MB, throughput 0.969749
Reading from 32461: heap size 356 MB, throughput 0.977999
Reading from 32462: heap size 335 MB, throughput 0.969235
Reading from 32461: heap size 360 MB, throughput 0.969646
Reading from 32462: heap size 336 MB, throughput 0.979403
Reading from 32461: heap size 363 MB, throughput 0.955053
Reading from 32462: heap size 339 MB, throughput 0.624519
Reading from 32461: heap size 361 MB, throughput 0.967372
Reading from 32462: heap size 384 MB, throughput 0.969642
Reading from 32461: heap size 365 MB, throughput 0.977693
Reading from 32462: heap size 385 MB, throughput 0.986389
Reading from 32461: heap size 369 MB, throughput 0.975865
Equal recommendation: 414 MB each
Reading from 32462: heap size 388 MB, throughput 0.973738
Reading from 32461: heap size 370 MB, throughput 0.971585
Reading from 32462: heap size 389 MB, throughput 0.982915
Reading from 32461: heap size 367 MB, throughput 0.974724
Reading from 32462: heap size 387 MB, throughput 0.984356
Reading from 32461: heap size 370 MB, throughput 0.976355
Reading from 32462: heap size 389 MB, throughput 0.984447
Reading from 32461: heap size 369 MB, throughput 0.974148
Reading from 32462: heap size 387 MB, throughput 0.978355
Reading from 32461: heap size 371 MB, throughput 0.715516
Reading from 32462: heap size 388 MB, throughput 0.982227
Reading from 32461: heap size 419 MB, throughput 0.970368
Reading from 32462: heap size 390 MB, throughput 0.973014
Equal recommendation: 414 MB each
Reading from 32462: heap size 391 MB, throughput 0.974709
Reading from 32461: heap size 417 MB, throughput 0.987924
Reading from 32462: heap size 397 MB, throughput 0.872201
Reading from 32462: heap size 397 MB, throughput 0.795622
Reading from 32462: heap size 402 MB, throughput 0.801612
Reading from 32462: heap size 406 MB, throughput 0.763789
Reading from 32461: heap size 416 MB, throughput 0.981561
Reading from 32461: heap size 384 MB, throughput 0.879275
Reading from 32461: heap size 413 MB, throughput 0.817139
Reading from 32461: heap size 415 MB, throughput 0.852885
Reading from 32461: heap size 409 MB, throughput 0.961208
Reading from 32462: heap size 416 MB, throughput 0.984442
Reading from 32461: heap size 416 MB, throughput 0.988821
Reading from 32462: heap size 381 MB, throughput 0.986981
Reading from 32461: heap size 404 MB, throughput 0.987332
Reading from 32461: heap size 413 MB, throughput 0.990416
Reading from 32462: heap size 415 MB, throughput 0.986601
Reading from 32461: heap size 412 MB, throughput 0.982393
Reading from 32462: heap size 383 MB, throughput 0.983139
Equal recommendation: 414 MB each
Reading from 32461: heap size 414 MB, throughput 0.988232
Reading from 32462: heap size 411 MB, throughput 0.987227
Reading from 32461: heap size 402 MB, throughput 0.987679
Reading from 32462: heap size 409 MB, throughput 0.982842
Reading from 32461: heap size 409 MB, throughput 0.97476
Reading from 32462: heap size 407 MB, throughput 0.978752
Reading from 32461: heap size 408 MB, throughput 0.981271
Reading from 32462: heap size 409 MB, throughput 0.971901
Reading from 32461: heap size 409 MB, throughput 0.970529
Reading from 32461: heap size 411 MB, throughput 0.973826
Reading from 32462: heap size 409 MB, throughput 0.9835
Reading from 32461: heap size 411 MB, throughput 0.984426
Reading from 32462: heap size 410 MB, throughput 0.980364
Equal recommendation: 414 MB each
Reading from 32461: heap size 411 MB, throughput 0.976064
Reading from 32462: heap size 411 MB, throughput 0.980807
Reading from 32461: heap size 412 MB, throughput 0.9887
Reading from 32461: heap size 413 MB, throughput 0.916563
Reading from 32461: heap size 414 MB, throughput 0.839061
Reading from 32461: heap size 413 MB, throughput 0.858864
Reading from 32461: heap size 417 MB, throughput 0.899448
Reading from 32462: heap size 412 MB, throughput 0.986495
Reading from 32462: heap size 415 MB, throughput 0.868612
Reading from 32462: heap size 406 MB, throughput 0.87928
Reading from 32462: heap size 413 MB, throughput 0.864203
Reading from 32461: heap size 408 MB, throughput 0.987485
Reading from 32462: heap size 414 MB, throughput 0.981897
Reading from 32461: heap size 417 MB, throughput 0.987951
Reading from 32462: heap size 405 MB, throughput 0.993059
Reading from 32461: heap size 406 MB, throughput 0.984027
Reading from 32462: heap size 414 MB, throughput 0.988534
Reading from 32461: heap size 415 MB, throughput 0.986294
Equal recommendation: 414 MB each
Reading from 32461: heap size 403 MB, throughput 0.965527
Reading from 32462: heap size 416 MB, throughput 0.983975
Reading from 32461: heap size 411 MB, throughput 0.983537
Reading from 32462: heap size 388 MB, throughput 0.985867
Reading from 32461: heap size 410 MB, throughput 0.981324
Reading from 32462: heap size 415 MB, throughput 0.980368
Reading from 32461: heap size 411 MB, throughput 0.980584
Reading from 32462: heap size 390 MB, throughput 0.984739
Reading from 32461: heap size 411 MB, throughput 0.984095
Reading from 32461: heap size 412 MB, throughput 0.978795
Reading from 32462: heap size 410 MB, throughput 0.982885
Reading from 32461: heap size 414 MB, throughput 0.981383
Equal recommendation: 414 MB each
Reading from 32462: heap size 409 MB, throughput 0.981318
Reading from 32462: heap size 410 MB, throughput 0.981762
Reading from 32461: heap size 414 MB, throughput 0.993123
Reading from 32461: heap size 408 MB, throughput 0.98287
Reading from 32461: heap size 412 MB, throughput 0.877963
Reading from 32461: heap size 411 MB, throughput 0.858851
Reading from 32461: heap size 414 MB, throughput 0.852696
Reading from 32461: heap size 411 MB, throughput 0.964097
Reading from 32462: heap size 410 MB, throughput 0.981008
Reading from 32462: heap size 411 MB, throughput 0.984479
Reading from 32461: heap size 417 MB, throughput 0.988861
Reading from 32462: heap size 413 MB, throughput 0.851792
Reading from 32462: heap size 414 MB, throughput 0.882985
Reading from 32462: heap size 404 MB, throughput 0.856786
Reading from 32462: heap size 414 MB, throughput 0.982257
Reading from 32461: heap size 406 MB, throughput 0.988976
Reading from 32461: heap size 415 MB, throughput 0.98815
Reading from 32462: heap size 415 MB, throughput 0.988931
Reading from 32461: heap size 404 MB, throughput 0.985772
Equal recommendation: 414 MB each
Reading from 32462: heap size 404 MB, throughput 0.988661
Reading from 32461: heap size 411 MB, throughput 0.984406
Reading from 32462: heap size 413 MB, throughput 0.987657
Reading from 32461: heap size 409 MB, throughput 0.984253
Reading from 32461: heap size 411 MB, throughput 0.985798
Reading from 32462: heap size 412 MB, throughput 0.987356
Reading from 32461: heap size 410 MB, throughput 0.981536
Reading from 32462: heap size 414 MB, throughput 0.985383
Reading from 32461: heap size 411 MB, throughput 0.979393
Reading from 32462: heap size 402 MB, throughput 0.984981
Reading from 32461: heap size 412 MB, throughput 0.982351
Equal recommendation: 414 MB each
Reading from 32462: heap size 409 MB, throughput 0.983944
Reading from 32461: heap size 413 MB, throughput 0.983643
Reading from 32462: heap size 408 MB, throughput 0.983709
Reading from 32461: heap size 415 MB, throughput 0.985396
Reading from 32461: heap size 404 MB, throughput 0.963298
Reading from 32461: heap size 411 MB, throughput 0.837161
Reading from 32461: heap size 413 MB, throughput 0.83902
Reading from 32461: heap size 419 MB, throughput 0.871148
Reading from 32462: heap size 409 MB, throughput 0.980361
Reading from 32461: heap size 410 MB, throughput 0.984992
Reading from 32462: heap size 410 MB, throughput 0.981347
Reading from 32461: heap size 419 MB, throughput 0.988523
Reading from 32462: heap size 411 MB, throughput 0.987539
Reading from 32462: heap size 413 MB, throughput 0.892453
Reading from 32462: heap size 413 MB, throughput 0.849174
Reading from 32462: heap size 418 MB, throughput 0.883527
Reading from 32461: heap size 387 MB, throughput 0.988641
Reading from 32462: heap size 408 MB, throughput 0.98319
Reading from 32461: heap size 419 MB, throughput 0.985689
Equal recommendation: 414 MB each
Reading from 32462: heap size 417 MB, throughput 0.988179
Reading from 32461: heap size 389 MB, throughput 0.977444
Reading from 32462: heap size 385 MB, throughput 0.988132
Reading from 32461: heap size 417 MB, throughput 0.983022
Reading from 32461: heap size 392 MB, throughput 0.980416
Reading from 32462: heap size 415 MB, throughput 0.988192
Reading from 32461: heap size 413 MB, throughput 0.98767
Reading from 32462: heap size 386 MB, throughput 0.987223
Reading from 32461: heap size 412 MB, throughput 0.987002
Reading from 32462: heap size 413 MB, throughput 0.984939
Reading from 32461: heap size 412 MB, throughput 0.982077
Equal recommendation: 414 MB each
Reading from 32462: heap size 411 MB, throughput 0.989076
Reading from 32461: heap size 412 MB, throughput 0.981009
Reading from 32461: heap size 415 MB, throughput 0.983012
Reading from 32462: heap size 410 MB, throughput 0.98614
Reading from 32461: heap size 405 MB, throughput 0.991818
Reading from 32461: heap size 415 MB, throughput 0.923369
Reading from 32461: heap size 412 MB, throughput 0.842556
Reading from 32462: heap size 411 MB, throughput 0.979294
Reading from 32461: heap size 413 MB, throughput 0.752514
Reading from 32461: heap size 415 MB, throughput 0.882608
Reading from 32461: heap size 405 MB, throughput 0.98851
Reading from 32462: heap size 412 MB, throughput 0.97845
Reading from 32461: heap size 415 MB, throughput 0.986037
Reading from 32462: heap size 412 MB, throughput 0.991225
Reading from 32461: heap size 404 MB, throughput 0.988151
Reading from 32462: heap size 415 MB, throughput 0.978611
Reading from 32462: heap size 388 MB, throughput 0.86753
Reading from 32462: heap size 409 MB, throughput 0.856735
Reading from 32462: heap size 412 MB, throughput 0.807381
Equal recommendation: 414 MB each
Reading from 32461: heap size 413 MB, throughput 0.982321
Reading from 32462: heap size 419 MB, throughput 0.984366
Reading from 32461: heap size 413 MB, throughput 0.989129
Reading from 32462: heap size 386 MB, throughput 0.989891
Reading from 32461: heap size 415 MB, throughput 0.984399
Reading from 32462: heap size 418 MB, throughput 0.990134
Reading from 32461: heap size 403 MB, throughput 0.98179
Reading from 32461: heap size 410 MB, throughput 0.981272
Reading from 32462: heap size 388 MB, throughput 0.987877
Reading from 32461: heap size 411 MB, throughput 0.977842
Reading from 32462: heap size 416 MB, throughput 0.987815
Reading from 32461: heap size 411 MB, throughput 0.979654
Equal recommendation: 414 MB each
Reading from 32461: heap size 413 MB, throughput 0.977553
Reading from 32462: heap size 390 MB, throughput 0.985564
Reading from 32461: heap size 414 MB, throughput 0.993157
Reading from 32462: heap size 411 MB, throughput 0.985249
Reading from 32461: heap size 417 MB, throughput 0.983396
Reading from 32461: heap size 393 MB, throughput 0.828325
Reading from 32461: heap size 412 MB, throughput 0.830639
Reading from 32461: heap size 415 MB, throughput 0.818983
Reading from 32462: heap size 410 MB, throughput 0.985808
Reading from 32461: heap size 410 MB, throughput 0.977104
Reading from 32461: heap size 418 MB, throughput 0.986424
Reading from 32462: heap size 412 MB, throughput 0.985462
Reading from 32461: heap size 405 MB, throughput 0.987911
Reading from 32462: heap size 412 MB, throughput 0.981484
Reading from 32461: heap size 415 MB, throughput 0.985051
Equal recommendation: 414 MB each
Reading from 32462: heap size 414 MB, throughput 0.980467
Reading from 32461: heap size 404 MB, throughput 0.970177
Reading from 32462: heap size 415 MB, throughput 0.952096
Reading from 32462: heap size 412 MB, throughput 0.890047
Reading from 32462: heap size 414 MB, throughput 0.858694
Reading from 32462: heap size 408 MB, throughput 0.890713
Reading from 32461: heap size 412 MB, throughput 0.988416
Reading from 32462: heap size 414 MB, throughput 0.985936
Reading from 32461: heap size 409 MB, throughput 0.984843
Reading from 32462: heap size 404 MB, throughput 0.988894
Reading from 32461: heap size 412 MB, throughput 0.979648
Reading from 32461: heap size 411 MB, throughput 0.978282
Reading from 32462: heap size 413 MB, throughput 0.987697
Reading from 32461: heap size 412 MB, throughput 0.979418
Reading from 32462: heap size 414 MB, throughput 0.988298
Equal recommendation: 414 MB each
Reading from 32461: heap size 414 MB, throughput 0.981317
Reading from 32462: heap size 415 MB, throughput 0.988059
Reading from 32461: heap size 403 MB, throughput 0.976659
Reading from 32462: heap size 402 MB, throughput 0.986226
Reading from 32461: heap size 412 MB, throughput 0.991158
Reading from 32461: heap size 413 MB, throughput 0.949956
Reading from 32461: heap size 413 MB, throughput 0.888502
Reading from 32461: heap size 415 MB, throughput 0.845979
Reading from 32461: heap size 412 MB, throughput 0.854335
Reading from 32462: heap size 410 MB, throughput 0.982533
Reading from 32461: heap size 417 MB, throughput 0.989415
Reading from 32461: heap size 407 MB, throughput 0.983646
Reading from 32462: heap size 409 MB, throughput 0.988935
Reading from 32461: heap size 417 MB, throughput 0.988234
Reading from 32462: heap size 410 MB, throughput 0.984872
Equal recommendation: 414 MB each
Reading from 32461: heap size 407 MB, throughput 0.988213
Reading from 32462: heap size 411 MB, throughput 0.983681
Reading from 32461: heap size 415 MB, throughput 0.983771
Reading from 32461: heap size 404 MB, throughput 0.98256
Reading from 32462: heap size 412 MB, throughput 0.990536
Reading from 32462: heap size 415 MB, throughput 0.931399
Reading from 32462: heap size 407 MB, throughput 0.851478
Reading from 32462: heap size 412 MB, throughput 0.858883
Reading from 32462: heap size 414 MB, throughput 0.957462
Reading from 32461: heap size 411 MB, throughput 0.981115
Reading from 32462: heap size 405 MB, throughput 0.987646
Reading from 32461: heap size 409 MB, throughput 0.985162
Reading from 32461: heap size 410 MB, throughput 0.982699
Reading from 32462: heap size 414 MB, throughput 0.989606
Reading from 32461: heap size 410 MB, throughput 0.978043
Equal recommendation: 414 MB each
Reading from 32462: heap size 415 MB, throughput 0.991476
Reading from 32461: heap size 411 MB, throughput 0.859859
Reading from 32462: heap size 388 MB, throughput 0.987488
Reading from 32461: heap size 427 MB, throughput 0.995935
Reading from 32462: heap size 414 MB, throughput 0.9854
Reading from 32461: heap size 425 MB, throughput 0.989201
Reading from 32461: heap size 422 MB, throughput 0.944981
Reading from 32461: heap size 423 MB, throughput 0.862334
Reading from 32461: heap size 424 MB, throughput 0.863875
Reading from 32461: heap size 424 MB, throughput 0.833655
Reading from 32462: heap size 390 MB, throughput 0.984579
Reading from 32461: heap size 424 MB, throughput 0.98399
Reading from 32462: heap size 410 MB, throughput 0.982012
Reading from 32461: heap size 389 MB, throughput 0.981361
Equal recommendation: 414 MB each
Reading from 32461: heap size 424 MB, throughput 0.986333
Reading from 32462: heap size 409 MB, throughput 0.985771
Reading from 32461: heap size 391 MB, throughput 0.985623
Reading from 32462: heap size 410 MB, throughput 0.983245
Reading from 32461: heap size 423 MB, throughput 0.985076
Reading from 32462: heap size 410 MB, throughput 0.987622
Reading from 32461: heap size 395 MB, throughput 0.983986
Reading from 32461: heap size 419 MB, throughput 0.982112
Reading from 32462: heap size 412 MB, throughput 0.989612
Reading from 32462: heap size 413 MB, throughput 0.905507
Reading from 32462: heap size 413 MB, throughput 0.847796
Reading from 32462: heap size 415 MB, throughput 0.844731
Reading from 32462: heap size 409 MB, throughput 0.953163
Reading from 32461: heap size 399 MB, throughput 0.979531
Reading from 32461: heap size 415 MB, throughput 0.981258
Reading from 32462: heap size 416 MB, throughput 0.993368
Equal recommendation: 414 MB each
Reading from 32461: heap size 401 MB, throughput 0.980294
Reading from 32462: heap size 404 MB, throughput 0.987819
Reading from 32461: heap size 413 MB, throughput 0.979691
Reading from 32461: heap size 413 MB, throughput 0.974503
Reading from 32462: heap size 413 MB, throughput 0.98549
Reading from 32461: heap size 415 MB, throughput 0.968761
Reading from 32462: heap size 414 MB, throughput 0.991821
Reading from 32461: heap size 414 MB, throughput 0.976819
Reading from 32461: heap size 418 MB, throughput 0.847991
Reading from 32461: heap size 418 MB, throughput 0.797666
Reading from 32461: heap size 421 MB, throughput 0.794472
Reading from 32461: heap size 423 MB, throughput 0.782201
Reading from 32461: heap size 424 MB, throughput 0.969598
Reading from 32462: heap size 415 MB, throughput 0.986492
Reading from 32461: heap size 389 MB, throughput 0.987104
Reading from 32462: heap size 402 MB, throughput 0.990269
Reading from 32461: heap size 424 MB, throughput 0.981804
Equal recommendation: 414 MB each
Reading from 32461: heap size 390 MB, throughput 0.972381
Reading from 32462: heap size 409 MB, throughput 0.979874
Reading from 32461: heap size 422 MB, throughput 0.978957
Reading from 32462: heap size 409 MB, throughput 0.988838
Reading from 32461: heap size 390 MB, throughput 0.980227
Reading from 32462: heap size 410 MB, throughput 0.98798
Reading from 32461: heap size 419 MB, throughput 0.986114
Reading from 32461: heap size 390 MB, throughput 0.979364
Reading from 32462: heap size 411 MB, throughput 0.980928
Reading from 32461: heap size 416 MB, throughput 0.983352
Reading from 32462: heap size 412 MB, throughput 0.98711
Reading from 32462: heap size 414 MB, throughput 0.898028
Reading from 32462: heap size 414 MB, throughput 0.87264
Reading from 32462: heap size 410 MB, throughput 0.849661
Reading from 32461: heap size 390 MB, throughput 0.974339
Reading from 32462: heap size 415 MB, throughput 0.983095
Equal recommendation: 414 MB each
Reading from 32461: heap size 413 MB, throughput 0.978355
Reading from 32462: heap size 406 MB, throughput 0.992676
Reading from 32461: heap size 412 MB, throughput 0.984659
Reading from 32461: heap size 411 MB, throughput 0.969028
Reading from 32462: heap size 415 MB, throughput 0.989829
Reading from 32461: heap size 412 MB, throughput 0.978521
Reading from 32462: heap size 403 MB, throughput 0.986867
Reading from 32461: heap size 414 MB, throughput 0.991744
Reading from 32461: heap size 391 MB, throughput 0.922516
Reading from 32461: heap size 409 MB, throughput 0.83635
Reading from 32461: heap size 412 MB, throughput 0.813034
Reading from 32461: heap size 419 MB, throughput 0.846883
Reading from 32461: heap size 408 MB, throughput 0.909365
Reading from 32462: heap size 411 MB, throughput 0.986318
Reading from 32461: heap size 420 MB, throughput 0.986515
Reading from 32462: heap size 410 MB, throughput 0.984897
Equal recommendation: 414 MB each
Reading from 32461: heap size 386 MB, throughput 0.988105
Reading from 32462: heap size 412 MB, throughput 0.982836
Reading from 32461: heap size 419 MB, throughput 0.986391
Reading from 32462: heap size 410 MB, throughput 0.984971
Reading from 32461: heap size 387 MB, throughput 0.981902
Reading from 32461: heap size 418 MB, throughput 0.987445
Reading from 32462: heap size 411 MB, throughput 0.984181
Reading from 32461: heap size 389 MB, throughput 0.982367
Reading from 32462: heap size 412 MB, throughput 0.983258
Reading from 32461: heap size 415 MB, throughput 0.9812
Reading from 32461: heap size 391 MB, throughput 0.980554
Reading from 32462: heap size 413 MB, throughput 0.976791
Reading from 32461: heap size 412 MB, throughput 0.984039
Equal recommendation: 414 MB each
Reading from 32462: heap size 411 MB, throughput 0.97519
Reading from 32462: heap size 416 MB, throughput 0.295728
Reading from 32462: heap size 441 MB, throughput 0.992938
Reading from 32462: heap size 446 MB, throughput 0.991828
Reading from 32461: heap size 411 MB, throughput 0.983873
Reading from 32462: heap size 447 MB, throughput 0.996666
Reading from 32461: heap size 410 MB, throughput 0.980319
Reading from 32461: heap size 411 MB, throughput 0.976645
Reading from 32462: heap size 385 MB, throughput 0.994094
Reading from 32461: heap size 414 MB, throughput 0.978602
Reading from 32462: heap size 449 MB, throughput 0.992509
Reading from 32461: heap size 414 MB, throughput 0.988017
Reading from 32461: heap size 417 MB, throughput 0.896958
Reading from 32461: heap size 416 MB, throughput 0.862227
Reading from 32461: heap size 417 MB, throughput 0.841521
Reading from 32461: heap size 418 MB, throughput 0.817985
Reading from 32462: heap size 390 MB, throughput 0.990325
Reading from 32461: heap size 419 MB, throughput 0.767323
Reading from 32462: heap size 445 MB, throughput 0.990208
Reading from 32461: heap size 370 MB, throughput 0.993536
Equal recommendation: 414 MB each
Reading from 32461: heap size 400 MB, throughput 0.994528
Reading from 32462: heap size 398 MB, throughput 0.986928
Reading from 32461: heap size 403 MB, throughput 0.991233
Reading from 32462: heap size 436 MB, throughput 0.985731
Reading from 32461: heap size 410 MB, throughput 0.991703
Reading from 32462: heap size 405 MB, throughput 0.984964
Reading from 32461: heap size 411 MB, throughput 0.992434
Reading from 32461: heap size 410 MB, throughput 0.985507
Reading from 32462: heap size 434 MB, throughput 0.984069
Reading from 32461: heap size 412 MB, throughput 0.987642
Reading from 32462: heap size 410 MB, throughput 0.981928
Reading from 32461: heap size 408 MB, throughput 0.98294
Equal recommendation: 414 MB each
Reading from 32462: heap size 430 MB, throughput 0.979443
Reading from 32461: heap size 411 MB, throughput 0.980513
Reading from 32462: heap size 429 MB, throughput 0.989376
Reading from 32461: heap size 409 MB, throughput 0.979714
Reading from 32462: heap size 423 MB, throughput 0.968713
Reading from 32462: heap size 405 MB, throughput 0.838638
Reading from 32462: heap size 418 MB, throughput 0.791675
Reading from 32462: heap size 424 MB, throughput 0.791536
Reading from 32462: heap size 425 MB, throughput 0.797699
Reading from 32461: heap size 410 MB, throughput 0.980108
Reading from 32462: heap size 389 MB, throughput 0.982037
Reading from 32461: heap size 413 MB, throughput 0.974418
Reading from 32462: heap size 427 MB, throughput 0.985421
Reading from 32461: heap size 414 MB, throughput 0.9865
Reading from 32461: heap size 406 MB, throughput 0.887447
Reading from 32461: heap size 413 MB, throughput 0.806844
Reading from 32461: heap size 421 MB, throughput 0.869706
Reading from 32461: heap size 424 MB, throughput 0.879198
Reading from 32462: heap size 388 MB, throughput 0.978806
Reading from 32461: heap size 420 MB, throughput 0.986067
Reading from 32462: heap size 423 MB, throughput 0.982372
Equal recommendation: 414 MB each
Reading from 32461: heap size 385 MB, throughput 0.983265
Reading from 32462: heap size 390 MB, throughput 0.980732
Reading from 32461: heap size 422 MB, throughput 0.980872
Reading from 32462: heap size 419 MB, throughput 0.979263
Reading from 32461: heap size 387 MB, throughput 0.984523
Reading from 32462: heap size 392 MB, throughput 0.983314
Reading from 32461: heap size 418 MB, throughput 0.983076
Reading from 32462: heap size 415 MB, throughput 0.981134
Reading from 32461: heap size 389 MB, throughput 0.979161
Reading from 32462: heap size 393 MB, throughput 0.986029
Reading from 32461: heap size 415 MB, throughput 0.985403
Reading from 32462: heap size 411 MB, throughput 0.982859
Reading from 32461: heap size 392 MB, throughput 0.981243
Equal recommendation: 414 MB each
Reading from 32462: heap size 410 MB, throughput 0.981958
Reading from 32461: heap size 410 MB, throughput 0.980404
Reading from 32462: heap size 409 MB, throughput 0.981986
Reading from 32461: heap size 409 MB, throughput 0.981782
Reading from 32462: heap size 410 MB, throughput 0.976919
Reading from 32461: heap size 412 MB, throughput 0.975662
Reading from 32461: heap size 412 MB, throughput 0.980269
Reading from 32462: heap size 412 MB, throughput 0.986622
Reading from 32462: heap size 412 MB, throughput 0.924675
Reading from 32462: heap size 413 MB, throughput 0.832044
Reading from 32462: heap size 414 MB, throughput 0.822118
Reading from 32462: heap size 413 MB, throughput 0.829489
Reading from 32462: heap size 418 MB, throughput 0.950588
Reading from 32461: heap size 413 MB, throughput 0.988387
Reading from 32461: heap size 415 MB, throughput 0.908073
Reading from 32461: heap size 412 MB, throughput 0.885475
Reading from 32461: heap size 415 MB, throughput 0.856297
Client 32461 died
Clients: 1
Reading from 32462: heap size 409 MB, throughput 0.992136
Recommendation: one client; give it all the memory
Reading from 32462: heap size 419 MB, throughput 0.989601
Reading from 32462: heap size 422 MB, throughput 0.990436
Reading from 32462: heap size 424 MB, throughput 0.989368
Reading from 32462: heap size 424 MB, throughput 0.98947
Reading from 32462: heap size 426 MB, throughput 0.988594
Reading from 32462: heap size 424 MB, throughput 0.988359
Recommendation: one client; give it all the memory
Reading from 32462: heap size 426 MB, throughput 0.986065
Reading from 32462: heap size 424 MB, throughput 0.989004
Reading from 32462: heap size 426 MB, throughput 0.985652
Reading from 32462: heap size 427 MB, throughput 0.987565
Reading from 32462: heap size 427 MB, throughput 0.991517
Reading from 32462: heap size 429 MB, throughput 0.979986
Reading from 32462: heap size 429 MB, throughput 0.876678
Reading from 32462: heap size 426 MB, throughput 0.873481
Reading from 32462: heap size 430 MB, throughput 0.865279
Client 32462 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
