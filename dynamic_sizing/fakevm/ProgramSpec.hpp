#ifndef PROGRAMSPEC_HPP
#define PROGRAMSPEC_HPP

#include <list>
#include <map>
#include <random>
#include <string>
#include <vector>

class Phase;
class Transition;

class Program
{
public:
    Program();
    ~Program();
    void addPhase(Phase* p);
    bool validate();
    unsigned long long getMinHeapMB() const;
    void setMinHeapMB(unsigned long long minHeapMB);
    const Phase* getCurrentPhase() const;
    void changePhase();
    void print() const;

    static Program* parseFile(const char* filename);

private:
    std::map<std::string, Phase*> m_Phases;
    Phase* m_CurrentPhase;
    unsigned long long m_MinHeapMB;
};

extern Program* g_BisonParsedProgram;

class Phase
{
public:
    Phase(const std::string& name, int allocRateB, double lengthSeconds, int noise);
    ~Phase();
    const std::string& getName() const;
    int getAllocRateBytes() const;
    double getAllocRateMB() const;
    double getLengthSeconds() const;
    double getLengthWithRandomError() const;
    void addTransitions(const std::list<Transition*>& l);
    bool validate(const std::map<std::string, Phase*>& phases);
    const std::string& pickTransition() const;
    void print() const;

private:
    static std::default_random_engine s_Generator;
    std::string m_Name;
    int m_AllocRateB;
    double m_LengthSeconds;
    int m_LenPercentError;
    std::list<Transition*> m_Transitions;
    std::vector<Transition*> m_TransitionProbability;
};

class Transition
{
public:
    Transition(const std::string& target, unsigned int probability) : m_Target(target), m_Probability(probability) {}
    const std::string& getTarget() const;
    unsigned int getProbability() const;
    void print() const;

private:
    std::string m_Target;
    unsigned int m_Probability;
};

#endif
