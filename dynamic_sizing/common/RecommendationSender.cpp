#include "RecommendationSender.hpp"
#include "Utilities.hpp"

economem::RecommendationSender::RecommendationSender() : SendSock() {}

bool economem::RecommendationSender::sendRecommendation(const economem::Recommendation& r, pid_t dest)
{
    return sendToClient(economem::TYPE_RECOMMENDATION, dest, &r, sizeof(r));
}
