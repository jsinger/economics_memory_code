#ifndef HOTSPOTNAIVEVENGEROV_HPP
#define HOTSPOTNAIVEVENGEROV_HPP

#include "gc_implementation/shared/NaiveVengerov.hpp"
#include "gc_implementation/parallelScavenge/parallelScavengeHeap.hpp"

class HotspotNaiveVengerov : public economem::NaiveVengerov
{
public:
    HotspotNaiveVengerov();

private:
    virtual unsigned long long getHeapSizeB();
    virtual unsigned long long getYoungGenSizeB();
    virtual unsigned long long getOldGenSizeB();
    virtual void logReading(unsigned long long heapSizeB, double throughput);
};

#endif
