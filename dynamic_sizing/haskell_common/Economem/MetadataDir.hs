module Economem.MetadataDir (MetadataDir(..),
                             TimingStats(..),
                             saveStaticMetadata,
                             generateDynamicMetadata,
                             generateStats,
                             isCompleted,
                             markCompleted) where

import Economem.Common
import Economem.Files
import Economem.Records
import Economem.ResourceUsageFile
import Economem.Run
import Economem.Process
import Math.Statistics
import System.Directory
import System.Process
import Text.Printf

data MetadataDir = MetadataDir { dirPath :: FilePath,
                                 experimentNameFile :: StringFile,
                                 logFile :: FilePath,
                                 cpuInfoFile :: StringFile,
                                 memoryInfoFile :: StringFile,
                                 hostInfoFile :: StringFile,
                                 userInfoFile :: StringFile,
                                 otherProcessesInfoFile :: StringFile,
                                 numRunsFile :: IntFile,
                                 numSubexperimentsFile :: IntFile,
                                 mainRuntimes :: TimingStats,
                                 subexperimentRuntimes :: TimingStats,
                                 dynamicSubexpRuntimes :: TimingStats,
                                 totalRuntimeFile :: WholeFile Runtime,
                                 argsFile :: LinesFile Run,
                                 lastCompletedRunFile :: IntFile,
                                 lastCompletedSubexperimentFile :: IntFile,
                                 lastCompletedDynamicSubexperimentFile :: IntFile,
                                 completedFile :: FilePath,
                                 procOutputFile :: RunNum -> ProcessNum -> FilePath,
                                 procResourceUsageFile :: RunNum -> ProcessNum -> ResourceUsageFile,
                                 auxOutputFile :: RunNum -> ProcessNum -> FilePath,
                                 auxResourceUsageFile :: RunNum -> ProcessNum -> ResourceUsageFile }

instance Path MetadataDir where
  getPath = dirPath

instance Dir MetadataDir where
  asDir path =
    MetadataDir { dirPath = getPath path,
                  experimentNameFile = WholeFile (path </> "experiment_name.txt"),
                  logFile = path </> "run.log",
                  cpuInfoFile = WholeFile (path </> "cpu.txt"),
                  memoryInfoFile = WholeFile (path </> "memory.txt"),
                  hostInfoFile = WholeFile (path </> "host.txt"),
                  userInfoFile = WholeFile (path </> "users.txt"),
                  otherProcessesInfoFile = WholeFile (path </> "other_processes.txt"),
                  numRunsFile = WholeFile (path </> "num_runs.txt"),
                  numSubexperimentsFile = WholeFile (path </> "num_subexperiments.txt"),
                  mainRuntimes = timingStats "main" path,
                  subexperimentRuntimes = timingStats "subexp" path,
                  dynamicSubexpRuntimes = timingStats "dynamic_subexp" path,
                  totalRuntimeFile = WholeFile (path </> "runtimes_total.txt"),
                  argsFile = LinesFile (path </> "args.txt"),
                  lastCompletedRunFile = WholeFile (path </> "last_completed_run.txt"),
                  lastCompletedSubexperimentFile = WholeFile (path </> "last_completed_subexperiment.txt"),
                  lastCompletedDynamicSubexperimentFile =
                    WholeFile (path </> "last_completed_dynamic_subexperiment.txt"),
                  completedFile = path </> "completed",
                  procOutputFile = outputFile process,
                  procResourceUsageFile = resFile process,
                  auxOutputFile = outputFile aux,
                  auxResourceUsageFile = resFile aux }
    where
      process = "process"
      aux = "auxiliary"

      outputFile :: String -> RunNum -> ProcessNum -> FilePath
      outputFile s = runProcFile s "output"

      resFile :: String -> RunNum -> ProcessNum -> ResourceUsageFile
      resFile s r pNum = ResourceUsageFile (runProcFile s "resourceUsage" r pNum)

      runProcFile :: String -> String -> RunNum -> ProcessNum -> FilePath
      runProcFile procType name r pNum =
        path </> (printf "run%s_%s%s_%s.txt"
                  (show (getNum r))
                  procType
                  (show (getNum pNum))
                  name :: FilePath)

data TimingStats =
  TimingStats { timesFile :: CsvFile Runtime,
                avgTimeFile :: WholeFile UncertainRuntime,
                medianTimeFile :: WholeFile Runtime,
                varianceTimeFile :: WholeFile Runtime }

timingStats :: Path a => String -> a -> TimingStats
timingStats statsName path =
  TimingStats { timesFile = CsvFile (prefix ++ ".csv"),
                avgTimeFile = WholeFile (prefix ++ "_avg.txt"),
                medianTimeFile = WholeFile (prefix ++ "_median.txt"),
                varianceTimeFile = WholeFile (prefix ++ "_variance.txt") }
  where
    prefix = path </> ("runtimes_" ++ statsName)

generateStats :: (MetadataDir -> TimingStats) -> MetadataDir -> IO UncertainRuntime
generateStats stats d = do mapM_ ($ d) (statsRelationships stats)
                           get (avgTimeFile (stats d))

statsRelationships :: (MetadataDir -> TimingStats) -> [MetadataDir -> IO ()]
statsRelationships stats =
  [(avgTimeFile . stats) `is` avgStdDev `ofFile` (timesFile . stats),
   (medianTimeFile . stats) `is` (runtime median) `ofFile` (timesFile . stats),
   (varianceTimeFile . stats) `is` (runtime safeVar) `ofFile` (timesFile . stats)]
  where
    runtime :: ([Double] -> Double) -> [Runtime] -> Runtime
    runtime f l = Runtime (f (map (\(Runtime r) -> r) l))

    avgStdDev :: [Runtime] -> UncertainRuntime
    avgStdDev rs =
      let rawTimes = map (\(Runtime r) -> r) rs
          avg = mean rawTimes
          std = case rawTimes of
            (_:_:_) -> stddev rawTimes
            _ -> 0
      in avg +/- std

    safeVar :: [Double] -> Double
    safeVar xs@(_:_:_) = var xs
    safeVar _ = 0


saveStaticMetadata :: MetadataDir -> String -> [Run] -> Int -> IO ()
saveStaticMetadata d name runs numSubexps = do
  ensureExists d
  fromIntegral (length runs) |> numRunsFile d
  fromIntegral numSubexps |> numSubexperimentsFile d
  0 |> lastCompletedRunFile d
  0 |> lastCompletedSubexperimentFile d
  0 |> lastCompletedDynamicSubexperimentFile d
  name |> experimentNameFile d
  runs |> argsFile d

generateDynamicMetadata :: MetadataDir  -> IO ()
generateDynamicMetadata d = do
  ensureExists d
  copyFile "/proc/cpuinfo" (getPath (cpuInfoFile d))
  copyFile "/proc/meminfo" (getPath (memoryInfoFile d))
  dumpToFile hostInfoFile "uname" ["-a"]
  dumpToFile userInfoFile "w" []
  dumpToFile otherProcessesInfoFile "ps" ["-elFww"]
  where
    dumpToFile :: (MetadataDir -> StringFile) -> FilePath -> [String] -> IO ()
    dumpToFile f cmd args =
      put (f d) =<< readProcess cmd args ""

isCompleted :: MetadataDir -> IO Bool
isCompleted = doesFileExist . completedFile

markCompleted :: MetadataDir -> IO ()
markCompleted d = writeFile (completedFile d) ""
