{-# LANGUAGE LambdaCase #-}

module Economem.MinHeap (defaultIterations,
                         MinHeapDir(..),
                         newMinHeap,
                         resumeMinHeap,
                         lookupMinHeap) where

import Prelude hiding(exp)
import Control.Applicative
import Control.Conditional(ifM)
import Data.List
import Economem.Common
import Economem.Experiment
import Economem.Files
import Economem.Finder
import Economem.Java
import Economem.Log
import qualified Economem.MinHeapAttempt as A
import Economem.Records
import System.Directory

title :: String
title = "minHeap"

defaultIterations :: Integer
defaultIterations = 5

data DynamicState = InProgress { currentBenchmark :: BenchmarkThreads,
                                 remainingBenchmarks :: [BenchmarkThreads],
                                 finder :: Finder Bool}
                  | Done
                  deriving (Show, Read)
instance Fileable DynamicState


data MinHeapDir =
  MinHeapDir { dirPath :: FilePath,
               benchmarksFile :: CsvFile BenchmarkThreads,
               iterationsFile :: IntFile,
               auxiliaryBenchmarksFile :: CsvFile BenchmarkThreads,
               javaNameFile :: StringFile,
               dynamicStateFile :: WholeFile DynamicState,
               allReadingsFile :: CsvFile BenchmarkThreadsMBCrashes,
               resultsFile :: CsvFile BenchmarkThreadsMB }

instance Path MinHeapDir where
  getPath = dirPath
instance Dir MinHeapDir where
  asDir path =
    MinHeapDir { dirPath = getPath path,
                 benchmarksFile = CsvFile (path </> "benchmarks.csv"),
                 iterationsFile = WholeFile (path </> "iterations.txt"),
                 auxiliaryBenchmarksFile = CsvFile (path </> "auxiliary_benchmarks.csv"),
                 javaNameFile = WholeFile (path </> "java_name.txt"),
                 dynamicStateFile = WholeFile (path </> "dynamic_state.txt"),
                 allReadingsFile = CsvFile (path </> "all_readings.csv"),
                 resultsFile = CsvFile (path </> "results.csv") }

newMinHeap :: Input BenchmarkThreads ->
              Integer ->
              Java ->
              Maybe (Input BenchmarkThreads) ->
              Maybe SubexpInfo ->
              IO Experiment
newMinHeap input iterations j auxBMs info = do
  (expDir, num ,parentLog) <- newExperimentProperties title info
  makeMinHeap expDir num input iterations j auxBMs parentLog


resumeMinHeap :: FilePath -> IO Experiment
resumeMinHeap =
  resumeExperiment
  (\expDir -> do let d = asDir expDir
                 let input = FromFile (benchmarksFile d)
                 iterations <- get (iterationsFile d)
                 java <- javaFromName =<< get (javaNameFile d)
                 auxBMs <- maybeGet (auxiliaryBenchmarksFile d)
                 makeMinHeap expDir topLevel input iterations java auxBMs Nothing)


makeMinHeap :: ExperimentDir ->
               ExperimentNum ->
               Input BenchmarkThreads ->
               Integer ->
               Java ->
               Maybe (Input BenchmarkThreads) ->
               Maybe Log ->
               IO Experiment
makeMinHeap expDir expNum inputLoader iterations j auxBMLoader parentLog = do
  let ex = makeExperiment title expDir expNum 0 parentLog
  let d = asDir expDir
  input <- loadInput inputLoader
  return (ex { saveAdditionalStaticMetadata = saveStatic d input,
               canResume = canResumeMinHeap d,
               resumeDynamicSubexperiment = resumeDynamic,
               nextDynamicSubexperiment = nextDynamic d input,
               dynamicSubexperimentCompleted = dynamicCompleted d })

  where
    saveStatic :: MinHeapDir -> [BenchmarkThreads] -> IO ()
    saveStatic d input = do
      input |> benchmarksFile d
      iterations |> iterationsFile d
      javaName j |> javaNameFile d
      maybePut (auxiliaryBenchmarksFile d) auxBMLoader

    canResumeMinHeap :: MinHeapDir -> IO Bool
    canResumeMinHeap d =
      and <$> mapM doesFileExist [getPath (benchmarksFile d),
                                  getPath (iterationsFile d),
                                  getPath (javaNameFile d)]

    resumeDynamic :: Experiment -> FilePath -> IO Experiment
    resumeDynamic _ = A.resumeMinHeapAttempt

    nextDynamic :: MinHeapDir -> [BenchmarkThreads] -> Experiment -> Integer -> IO (Maybe Experiment)
    nextDynamic d input e subNum = do
      state <- ifM
               (exists (dynamicStateFile d))
               (get (dynamicStateFile d))
               (return InProgress { currentBenchmark = head input,
                                    remainingBenchmarks = tail input,
                                    finder = newFinder False (DataSize 1 MB) })
      put (dynamicStateFile d) state
      case state of
       Done -> return Nothing
       InProgress {} ->
         Just <$> (A.newMinHeapAttempt
                   (BenchmarkThreadsMB (currentBenchmark state)
                    (fromIntegral (currentSize (finder state))))
                   j
                   iterations
                   auxBMLoader
                   (Just (SubexpInfo (ExperimentNum subNum subNum) (experimentDir e) (experimentLog e) Dynamic)))

    dynamicCompleted :: MinHeapDir -> Experiment -> Experiment -> IO ()
    dynamicCompleted d _ subExp =
      get (dynamicStateFile d) >>= \case
        Done -> fail "Should never get here; how can we be completing another subexperiment if we are already done?"
        state@(InProgress {}) -> do
          doesItCrash <- get (A.doesItCrashFile (asDir (experimentDir subExp)))
          append
            (allReadingsFile d)
            (BenchmarkThreadsMBCrashes
             (BenchmarkThreadsMB (currentBenchmark state) (fromIntegral (currentSize (finder (state)))))
             doesItCrash)
          let f = addReading (finder state) (\fi v -> v == targetValue fi) doesItCrash
          newState <- case finderDone f of
            Nothing -> return state { finder = f }
            Just size -> do
              append (resultsFile d) (BenchmarkThreadsMB (currentBenchmark state)
                                      (fromIntegral (sizeToInt (toMB size) + 1)))
              case remainingBenchmarks state of
               [] -> return Done
               (x:xs) -> do
                 return InProgress { currentBenchmark = x,
                                     remainingBenchmarks = xs,
                                     finder = newFinder False (DataSize 1 MB) }
          put (dynamicStateFile d) newState


lookupMinHeap :: BenchmarkThreads -> [BenchmarkThreadsMB] -> IO DataSize
lookupMinHeap targetBM minHeaps = case find (\b -> btmBenchmark b == targetBM) minHeaps of
  Just mh -> return (DataSize (fromIntegral (btmMB mh)) MB)
  Nothing -> fail ("No min heap data found for '" ++ pretty targetBM ++ "'")
