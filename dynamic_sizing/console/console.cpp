#include <cstdio>
#include <cstdlib>
#include <list>
#include <pthread.h>
#include <readline/readline.h>
#include <readline/history.h>
#include <unistd.h>
#include <Messages.hpp>
#include <CommandResultReceiver.hpp>
#include <CommandSender.hpp>
#include <ReceiverThread.hpp>
#include <SignalHandler.hpp>
#include <Thread.hpp>
#include <Utilities.hpp>
#include "Args.hpp"

class StrTok
{
private:
    char* m_S;
    char* m_SavePtr;
    static const char* s_Delim;

public:
    StrTok(char* s) : m_S(s) {}

    char* getStr()
    {
        char* s = strtok_r(m_S, s_Delim, &m_SavePtr);
        if (m_S != NULL)
            m_S = NULL;
        return s;
    }

    bool getInt(unsigned long long& i)
    {
        char* s = getStr();
        if (s == NULL)
            return false;

        char* endptr;
        i = strtoull(s, &endptr, 10);
        return endptr != s;
    }
};

const char* StrTok::s_Delim = " ";


class MainThread : public economem::Thread
{
public:
    MainThread(economem::CommandSender& sender) : Thread(), m_Sender(sender) {};
    bool commandSent() const;

private:
    class Command;

    virtual void threadFunc();

    static void invalidInput();
    char* readLine();
    void doCommand(char* line, const std::list<Command*>& commands);


    economem::CommandSender& m_Sender;
    bool m_CommandSent;

    class Command
    {
    public:
        virtual ~Command() {}
        size_t len() const { return strlen(m_Name) + strlen(m_Args) + 1; }
        void print() const { printf("%s %s", m_Name, m_Args); }
        void printHelp() const { printf("%s", m_Help); }
        bool is(const char* s) const { return strcmp(m_Name, s) == 0; }
        virtual bool invoke(StrTok& t, const std::list<Command*>& l, economem::CommandSender& sender) const = 0;

    protected:
        Command(const char* name, const char* args, economem::CommandType type, const char* help) : m_Type(type), m_Name(name), m_Args(args), m_Help(help) {}
        void usage() const { printf("    Usage: "); print(); printf("\n"); }

        const economem::CommandType m_Type;

    private:
        const char* m_Name;
        const char* m_Args;
        const char* m_Help;
    };

    class SimpleCommand : public Command
    {
    public:
        SimpleCommand(const char* name, economem::CommandType type, const char* help) : Command(name, "", type, help) {}
        virtual bool invoke(StrTok& t, const std::list<Command*>& l, economem::CommandSender& sender) const
        {
            (void)t;
            (void)l;
            return sender.sendCommand(m_Type);
        }
    };

    class OneArgCommand : public Command
    {
    protected:
        virtual bool valid(unsigned long long i) const { return i > 0; }
    public:
        OneArgCommand(const char* name, const char* arg, economem::CommandType type, const char* help) : Command(name, arg, type, help) {}
        virtual bool invoke(StrTok& t, const std::list<Command*>& l, economem::CommandSender& sender) const
        {
            (void)l;
            unsigned long long i;
            if (t.getInt(i) && valid(i))
                return sender.sendCommand(m_Type, i);
            else
            {
                usage();
                return false;
            }
        }
    };

    class BoolCommand : public Command
    {
    protected:
        virtual bool valid(unsigned long long i) const { (void)i; return true; }
    public:
        BoolCommand(const char* name, const char* arg, economem::CommandType type, const char* help) : Command(name, arg, type, help) {}
        virtual bool invoke(StrTok& t, const std::list<Command*>& l, economem::CommandSender& sender) const
        {
            (void)l;
            unsigned long long i;
            if (t.getInt(i) && valid(i))
                return sender.sendCommand(m_Type, i != 0);
            else
            {
                usage();
                return false;
            }
        }
    };

    class TwoArgCommand : public Command
    {
    protected:
        virtual bool valid(unsigned long long i, unsigned long long j) const = 0;
    public:
        TwoArgCommand(const char* name, const char* args, economem::CommandType type, const char* help): Command(name, args, type, help) {}
        virtual bool invoke(StrTok& t, const std::list<Command*>& l, economem::CommandSender& sender) const
        {
            (void)l;
            unsigned long long i;
            unsigned long long j;
            if (t.getInt(i) && t.getInt(j) && valid(i, j))
                return sender.sendCommand(m_Type, i, j);
            else
            {
                usage();
                return false;
            }
        }
    };

    class HelpCommand : public Command
    {
    public:
        HelpCommand() : Command("help", "", (economem::CommandType)0, "display this message") {}
        virtual bool invoke(StrTok& t, const std::list<Command*>& l, economem::CommandSender& sender) const
        {
            (void)t;
            (void)sender;
            std::list<Command*>::const_iterator it;
            size_t maxlen = 0;

            for (const Command* c : l)
            {
                if (c->len() > maxlen)
                    maxlen = c->len();
            }

            printf("Recognised commands:\n");
            for (const Command* c : l)
            {
                printf("    ");
                c->print();
                printf("%*s    ", (int)(maxlen - c->len()), "");
                c->printHelp();
                printf("\n");
            }

            return false;
        }
    };

    class SetMinHeapCommand : public TwoArgCommand
    {
    protected:
        virtual bool valid(unsigned long long i, unsigned long long j) const
        {
            (void)j;
            return i > 0;
        }
    public:
        SetMinHeapCommand() : TwoArgCommand("setminheap", "client_id heap_MB", economem::COMMANDTYPE_SETMINHEAP, "set the minimum heap size of a client in MB") {}
    };

    class SendCommand : public TwoArgCommand
    {
    protected:
        virtual bool valid(unsigned long long i, unsigned long long j) const
        {
            (void)i;
            return j > 0;
        }
    public:
        SendCommand() : TwoArgCommand("send", "client_id recommendation_MB", economem::COMMANDTYPE_RECOMMEND, "Manually send a recommendation (client_id of 0 sends to all)") {}
    };
};

class RecvThread : public economem::ReceiverThread<economem::CommandResult>
{
public:
    RecvThread(economem::CommandResultReceiver& receiver) : ReceiverThread(receiver, "RecvThread") {}

private:
    virtual void received(const economem::CommandResult& r);
};


int main(int argc, char** argv)
{
    Args::parse(argc, argv);

    economem::CommandSender sender(Args::getCommandTarget());
    economem::CommandResultReceiver receiver;

    RecvThread r(receiver);
    MainThread m(sender);

    economem::SignalHandler::init([&] () { m.kill(); });

    if (!r.start() || !m.start())
        economem::die("Failed to create threads");

    m.wait();

    if (!Args::singleCommandMode() || !m.commandSent())
        r.kill();
    r.wait();

    economem::info("Main", "All threads killed, cleaning up");

    return 0;
}


void MainThread::invalidInput()
{
    printf("Invalid input\n");
}

char* MainThread::readLine()
{
    char* s = readline(NULL);
    if (s != NULL && !isAlive())
    {
        free(s);
        s = NULL;
    }
    return s;
}

void MainThread::threadFunc()
{
    // Setup readline
    rl_bind_key('\t', rl_insert);
    rl_catch_signals = 0;

    // Setup commands
    std::list<Command*> commands;
    commands.push_back(new HelpCommand);
    commands.push_back(new SimpleCommand("clients", economem::COMMANDTYPE_CLIENTS, "how many clients are connected"));
    commands.push_back(new OneArgCommand("pid", "client_id", economem::COMMANDTYPE_PID, "get the pid of a client"));
    commands.push_back(new OneArgCommand("minheap", "client_id", economem::COMMANDTYPE_GETMINHEAP, "get the minimum heap size of a client in MB"));
    commands.push_back(new SetMinHeapCommand);
    commands.push_back(new SimpleCommand("mem", economem::COMMANDTYPE_GETTOTALMEM, "get the total memory shared between the clients in MB"));
    commands.push_back(new OneArgCommand("setmem", "mem_MB", economem::COMMANDTYPE_SETTOTALMEM, "set the total memory shared between the clients in MB"));
    commands.push_back(new SimpleCommand("state", economem::COMMANDTYPE_TIMERSTATE, "is the timer running?"));
    commands.push_back(new SimpleCommand("pause", economem::COMMANDTYPE_PAUSE, "pause/stop the timer"));
    commands.push_back(new SimpleCommand("unpause", economem::COMMANDTYPE_UNPAUSE, "unpause/start the timer"));
    commands.push_back(new SimpleCommand("timer", economem::COMMANDTYPE_GETTIMER, "get the timer interval in seconds"));
    commands.push_back(new OneArgCommand("settimer", "t", economem::COMMANDTYPE_SETTIMER, "set the timer interval in seconds"));
    commands.push_back(new SimpleCommand("logsreadings", economem::COMMANDTYPE_LOGSREADINGS, "does the daemon log readings"));
    commands.push_back(new BoolCommand("logreadings", "bool", economem::COMMANDTYPE_SETLOGREADINGS, "set whether the daemon logs readings"));
    commands.push_back(new SimpleCommand("logsfunctions", economem::COMMANDTYPE_LOGSFUNCTIONS, "does the daemon log functions"));
    commands.push_back(new BoolCommand("logfunctions", "bool", economem::COMMANDTYPE_SETLOGFUNCTIONS, "set whether the daemon logs functions"));
    commands.push_back(new SendCommand);
    commands.push_back(new SimpleCommand("dump", economem::COMMANDTYPE_DUMP, "dump the daemon's current data to file"));
    commands.push_back(new SimpleCommand("clear", economem::COMMANDTYPE_CLEAR, "delete all the daemon's records of clients"));
    commands.push_back(new SimpleCommand("exit", economem::COMMANDTYPE_EXIT, "tell the daemon to exit"));

    economem::info("MainThread", "starting");

    if (Args::singleCommandMode())
    {
        doCommand(Args::getSingleCommand(), commands);
        kill();
    }
    else
    {
        while (isAlive())
        {
            char* line = NULL;
            if ((line = readLine()) != NULL)
            {
                if (line[0] != '\0')
                    add_history(line);

                doCommand(line, commands);

                free(line);
            }
            else
                kill();
        }
    }

    for (Command* c : commands)
        delete c;

    economem::info("MainThread", "finished");
}

void MainThread::doCommand(char* line, const std::list<Command*>& commands)
{
    StrTok t(line);
    char* command;

    if ((command = t.getStr()) != NULL)
    {
        bool found = false;

        for (const Command* c : commands)
        {
            if (c->is(command))
            {
                if (c->invoke(t, commands, m_Sender))
                    m_CommandSent = true;
                found = true;
                break;
            }
        }

        if (!found)
            invalidInput();
    }
    else
        invalidInput();
}

bool MainThread::commandSent() const
{
    return m_CommandSent;
}


void RecvThread::received(const economem::CommandResult& r)
{
    switch (r.type)
    {
    case economem::CMDRESULTTYPE_CLIENTS: // number of clients
        printf("Clients: %llu\n", r.i);
        break;
    case economem::CMDRESULTTYPE_PID: // 0 if invalid client index
        if (r.i == 0)
            printf("Invalid client index\n");
        else
            printf("Client pid: %llu\n", r.i);
        break;
    case economem::CMDRESULTTYPE_GETMINHEAP: // 0 means unknown (or invalid index)
        if (r.i == 0)
            printf("Min heap: unknown\n");
        else
            printf("Min heap: %llu MB\n", r.i);
        break;
    case economem::CMDRESULTTYPE_SETMINHEAP: // bool: success
        if (r.i)
            printf("Min heap set\n");
        else
            printf("Failed to set min heap\n");
        break;
    case economem::CMDRESULTTYPE_GETTOTALMEM: // total mem
        printf("Total memory: %llu MB\n", r.i);
        break;
    case economem::CMDRESULTTYPE_SETTOTALMEM: // bool: success
        if (r.i)
            printf("Total memory set\n");
        else
            printf("Failed to set total memory\n");
        break;
    case economem::CMDRESULTTYPE_TIMERSTATE: // bool: paused
        printf("Timer: %s\n", (r.i ? "paused" : "running"));
        break;
    case economem::CMDRESULTTYPE_PAUSE: // bool: stopped (false if already stopped)
        if (r.i)
            printf("Timer paused\n");
        else
            printf("Timer already paused\n");
        break;
    case economem::CMDRESULTTYPE_UNPAUSE: // bool: started (false if already running)
        if (r.i)
            printf("Timer unpaused\n");
        else
            printf("Timer already running\n");
        break;
    case economem::CMDRESULTTYPE_GETTIMER: // timer value
        printf("Current timer interval: %llu s\n", r.i);
        break;
    case economem::CMDRESULTTYPE_SETTIMER: // bool: success
        if (r.i)
            printf("Timer interval changed\n");
        else
            printf("Invalid timer interval\n");
        break;
    case economem::CMDRESULTTYPE_LOGSREADINGS: // bool: logs readings
        if (r.i == 0)
            printf("Daemon does not log readings\n");
        else
            printf("Daemon logs readings\n");
        break;
    case economem::CMDRESULTTYPE_LOGSFUNCTIONS: // bool: logs functions
        if (r.i == 0)
            printf("Daemon does not log functions\n");
        else
            printf("Daemon logs functions\n");
        break;
    case economem::CMDRESULTTYPE_RECOMMEND: // (no return value)
        printf("Recommendation sent\n");
        break;
    case economem::CMDRESULTTYPE_DUMP: // bool: success
        if (r.i)
            printf("Dumped\n");
        else
            printf("Failed to dump\n");
        break;
    case economem::CMDRESULTTYPE_CLEAR: // (no return value)
        printf("Daemon cleared all client records\n");
        break;
    case economem::CMDRESULTTYPE_EXIT: // (no return value)
        printf("Daemon exited\n");
        break;
    default:
        economem::warning("Unknown result type %u", (unsigned int)r.type);
        break;
    }

    if (Args::singleCommandMode())
        kill();
}
