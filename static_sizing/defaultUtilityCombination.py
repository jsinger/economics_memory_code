#!/usr/bin/env python
# Measure the utility of a benchmark combination without fixing the heap size - let the JVM use its default behaviour
#
# Note: before running this, a cgroup called 'defaultUtility' must exist, and the user must be able to set a memory limit
# and run things in it. To set it up, do, e.g.:
#     sudo cgcreate -a user:user -t user:user -g memory:defaultUtility
# for the appropriate user (see also defaultUtilityCreateCgroup.sh)
#

import argparse
import argtypes
import os
import os.path
import shutil
import staticutility_core as su
import subprocess
import tempfile
from time import sleep


RUNS = 5 # default number of runs


def defaultUtilityCombination(totalMem, time, runs, bids, cgroupPath):
    seconds = su.timeToSeconds(time)
    su.checkDaCapo()

    outdir = su.DefaultUtilityDir(su.outputDir.join(su.defaultUtilityDirName(bids, time)))
    outdir.makedir()
    outdir.writeBenchmarksFile(bids)
    outdir.writeRuns(runs)
    outdir.writeTotalMem(totalMem)
    outdir.machineInfo()


    class UtilityBenchmark:
        """A single benchmark within a default utility run"""

        def __init__(self, bid, num):
            self.b = bid
            self.num = int(num)

        def rawFile(self, run):
            return outdir.rawFile(run, runs, self.num)

        def iterationsFile(self):
            return outdir.iterationsFile(self.num)

        def start(self, run):
            self.scratch = os.path.realpath(tempfile.mkdtemp())
            with open(self.rawFile(run), "w") as f, open(os.devnull) as devnull:
                self.p = subprocess.Popen(["cgexec", "-g", "memory:defaultUtility", "java", "-jar", su.dacapo(), "--scratch-directory", self.scratch, "-t", self.b.threads, "-s", self.b.inputSize, "-n", "1000000", self.b.name], stdin=devnull, stdout=f, stderr=subprocess.STDOUT)

        def kill(self):
            self.p.send_signal(15)

        def wait(self):
            self.p.wait()
            shutil.rmtree(self.scratch)
            self.p = None
            self.scratch = None

        def addToIterations(self, run):
            with open(self.iterationsFile(), "a") as f:
                f.write("%d\n" % su.benchmarkIterations(self.rawFile(run)))

        def calculateResults(self):
            l = su.fileToList(self.iterationsFile(), float)
            self.average = su.average(l)
            self.stddev = su.stddev(l)
            self.itersPerSec = self.average / float(seconds)
            self.stddevItersPerSec = self.stddev / float(seconds)
            self.median = su.median(l)
            self.medianItersPerSec = self.median / float(seconds)


    # Configure cgroups
    with open(os.path.join(cgroupPath, "defaultUtility", "memory.limit_in_bytes"), "w") as f:
        f.write("%d\n" % (totalMem * 1024 * 1024))

    starttime = outdir.timestampStartLocal(1, su.timeToSeconds(time), runs)


    benchmarks = map(lambda b: UtilityBenchmark(*b), zip(bids, range(1, len(bids) + 1)))

    for i in range(runs):
        print "%s, time: %s, inputs: %s, threads %s, run %d of %d" % (" ".join(map(lambda b: b.b.name, benchmarks)), time, " ".join(map(lambda b: b.b.inputSize, benchmarks)), " ".join(map(lambda b: b.b.threads, benchmarks)), i + 1, runs)

        for b in benchmarks:
            b.start(i)

        sleep(seconds)

        for b in benchmarks:
            b.kill()

        for b in benchmarks:
            b.wait()

        for b in benchmarks:
            b.addToIterations(i)

    for b in benchmarks:
        b.calculateResults()

    results = su.DefaultUtilityRawResult(map(lambda b: b.average, benchmarks), map(lambda b: b.stddev, benchmarks), map(lambda b: b.itersPerSec, benchmarks), map(lambda b: b.stddevItersPerSec, benchmarks), map(lambda b: b.median, benchmarks), map(lambda b: b.medianItersPerSec, benchmarks))

    results.append(outdir.rawResultsFile())

    outdir.timestampEndLocal(starttime)

    print "Don't forget to postprocess the data"



if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Measure the utility of a benchmark combination without fixing the heap size - let the JVM use its default behaviour")

    parser.add_argument("totalMem", type=argtypes.positive_int, help="total memory a combination is allowed to use")
    parser.add_argument("time", help="how long to run for, in a format suitable for input to 'sleep'")
    parser.add_argument("cgroupPath", type=argtypes.existing_absolute_dir, help="location of the cgroups control files")

    parser.add_argument("-r", "--runs", type=argtypes.positive_int, default=RUNS, help="how many times to run the measurement, for taking averages")

    parser.add_argument("-b", "--benchmark", action="append", nargs=4, default=[], metavar=("benchmark", "inputSize", "threads", "time"), help="a benchmark to include in the measurement; at least two are required")

    args = parser.parse_args()

    if len(args.benchmark) < 2:
        parser.error("At least two benchmarks must be specified")

    bids = []
    for b in args.benchmark:
        bids.append(su.BenchmarkId(b[0], b[3], b[1], b[2]))

    defaultUtilityCombination(args.totalMem, args.time, args.runs, bids, args.cgroupPath)
