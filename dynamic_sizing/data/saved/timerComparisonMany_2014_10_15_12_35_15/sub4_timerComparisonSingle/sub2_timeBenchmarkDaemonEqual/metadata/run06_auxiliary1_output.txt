economemd
    total memory: 7435 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub4_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 2736: heap size 9 MB, throughput 0.993296
Clients: 1
Client 2736 has a minimum heap size of 276 MB
Reading from 2735: heap size 9 MB, throughput 0.992302
Clients: 2
Client 2735 has a minimum heap size of 1211 MB
Reading from 2735: heap size 9 MB, throughput 0.977418
Reading from 2736: heap size 9 MB, throughput 0.981084
Reading from 2735: heap size 9 MB, throughput 0.936878
Reading from 2736: heap size 9 MB, throughput 0.956315
Reading from 2735: heap size 9 MB, throughput 0.929396
Reading from 2736: heap size 9 MB, throughput 0.957653
Reading from 2735: heap size 11 MB, throughput 0.981914
Reading from 2736: heap size 11 MB, throughput 0.98168
Reading from 2735: heap size 11 MB, throughput 0.976735
Reading from 2736: heap size 11 MB, throughput 0.978917
Reading from 2736: heap size 17 MB, throughput 0.905548
Reading from 2735: heap size 17 MB, throughput 0.942293
Reading from 2736: heap size 17 MB, throughput 0.417602
Reading from 2735: heap size 17 MB, throughput 0.460142
Reading from 2735: heap size 30 MB, throughput 0.977577
Reading from 2736: heap size 30 MB, throughput 0.929333
Reading from 2735: heap size 31 MB, throughput 0.882805
Reading from 2736: heap size 31 MB, throughput 0.784724
Reading from 2735: heap size 33 MB, throughput 0.521693
Reading from 2735: heap size 47 MB, throughput 0.789137
Reading from 2736: heap size 34 MB, throughput 0.261997
Reading from 2735: heap size 50 MB, throughput 0.88865
Reading from 2736: heap size 48 MB, throughput 0.855506
Reading from 2735: heap size 51 MB, throughput 0.865078
Reading from 2736: heap size 51 MB, throughput 0.918688
Reading from 2735: heap size 55 MB, throughput 0.180296
Reading from 2735: heap size 80 MB, throughput 0.625
Reading from 2736: heap size 52 MB, throughput 0.186107
Reading from 2735: heap size 84 MB, throughput 0.798323
Reading from 2736: heap size 75 MB, throughput 0.693422
Reading from 2735: heap size 85 MB, throughput 0.250394
Reading from 2736: heap size 75 MB, throughput 0.259904
Reading from 2735: heap size 111 MB, throughput 0.683139
Reading from 2736: heap size 102 MB, throughput 0.757097
Reading from 2735: heap size 113 MB, throughput 0.683766
Reading from 2736: heap size 102 MB, throughput 0.740586
Reading from 2735: heap size 114 MB, throughput 0.695093
Reading from 2736: heap size 105 MB, throughput 0.783365
Reading from 2735: heap size 118 MB, throughput 0.75961
Reading from 2736: heap size 108 MB, throughput 0.206449
Reading from 2736: heap size 135 MB, throughput 0.632251
Reading from 2735: heap size 121 MB, throughput 0.130618
Reading from 2736: heap size 139 MB, throughput 0.711078
Reading from 2735: heap size 153 MB, throughput 0.652244
Reading from 2736: heap size 142 MB, throughput 0.759288
Reading from 2735: heap size 156 MB, throughput 0.765939
Reading from 2735: heap size 159 MB, throughput 0.613686
Reading from 2735: heap size 161 MB, throughput 0.618694
Reading from 2735: heap size 168 MB, throughput 0.515284
Reading from 2736: heap size 145 MB, throughput 0.171523
Reading from 2736: heap size 187 MB, throughput 0.728805
Reading from 2736: heap size 188 MB, throughput 0.660285
Reading from 2735: heap size 170 MB, throughput 0.17276
Reading from 2736: heap size 189 MB, throughput 0.769145
Reading from 2735: heap size 207 MB, throughput 0.605837
Reading from 2736: heap size 196 MB, throughput 0.829192
Reading from 2735: heap size 216 MB, throughput 0.578983
Reading from 2735: heap size 217 MB, throughput 0.709159
Reading from 2735: heap size 222 MB, throughput 0.667048
Reading from 2736: heap size 198 MB, throughput 0.936867
Reading from 2735: heap size 225 MB, throughput 0.104181
Reading from 2735: heap size 266 MB, throughput 0.430888
Reading from 2735: heap size 269 MB, throughput 0.583564
Reading from 2736: heap size 205 MB, throughput 0.181348
Reading from 2735: heap size 271 MB, throughput 0.574771
Reading from 2736: heap size 261 MB, throughput 0.629535
Reading from 2735: heap size 274 MB, throughput 0.511366
Reading from 2736: heap size 262 MB, throughput 0.834236
Reading from 2735: heap size 279 MB, throughput 0.611004
Reading from 2736: heap size 266 MB, throughput 0.622405
Reading from 2735: heap size 285 MB, throughput 0.553872
Reading from 2736: heap size 267 MB, throughput 0.713393
Reading from 2736: heap size 272 MB, throughput 0.784077
Reading from 2736: heap size 272 MB, throughput 0.809567
Reading from 2736: heap size 274 MB, throughput 0.818028
Reading from 2735: heap size 292 MB, throughput 0.174941
Reading from 2736: heap size 278 MB, throughput 0.68795
Reading from 2735: heap size 332 MB, throughput 0.578869
Reading from 2736: heap size 284 MB, throughput 0.734344
Reading from 2735: heap size 341 MB, throughput 0.642711
Reading from 2736: heap size 286 MB, throughput 0.607658
Reading from 2735: heap size 344 MB, throughput 0.614464
Reading from 2736: heap size 292 MB, throughput 0.900349
Reading from 2736: heap size 294 MB, throughput 0.758537
Reading from 2735: heap size 349 MB, throughput 0.12323
Reading from 2736: heap size 294 MB, throughput 0.770363
Reading from 2735: heap size 395 MB, throughput 0.569266
Reading from 2735: heap size 398 MB, throughput 0.469468
Reading from 2735: heap size 400 MB, throughput 0.547388
Reading from 2735: heap size 403 MB, throughput 0.510816
Reading from 2736: heap size 296 MB, throughput 0.161652
Reading from 2735: heap size 408 MB, throughput 0.592516
Reading from 2736: heap size 343 MB, throughput 0.672384
Reading from 2735: heap size 413 MB, throughput 0.49385
Reading from 2736: heap size 344 MB, throughput 0.86231
Reading from 2736: heap size 348 MB, throughput 0.868532
Reading from 2736: heap size 349 MB, throughput 0.681045
Reading from 2736: heap size 354 MB, throughput 0.594908
Reading from 2736: heap size 354 MB, throughput 0.633647
Reading from 2736: heap size 362 MB, throughput 0.612091
Equal recommendation: 3717 MB each
Reading from 2736: heap size 362 MB, throughput 0.767568
Reading from 2735: heap size 420 MB, throughput 0.10049
Reading from 2735: heap size 478 MB, throughput 0.411595
Reading from 2735: heap size 481 MB, throughput 0.531191
Reading from 2736: heap size 370 MB, throughput 0.963431
Reading from 2735: heap size 487 MB, throughput 0.110524
Reading from 2735: heap size 543 MB, throughput 0.43697
Reading from 2735: heap size 540 MB, throughput 0.634603
Reading from 2735: heap size 545 MB, throughput 0.503374
Reading from 2735: heap size 547 MB, throughput 0.524376
Reading from 2735: heap size 552 MB, throughput 0.55408
Reading from 2736: heap size 372 MB, throughput 0.970657
Reading from 2735: heap size 557 MB, throughput 0.481363
Reading from 2735: heap size 569 MB, throughput 0.535343
Reading from 2735: heap size 577 MB, throughput 0.103797
Reading from 2735: heap size 646 MB, throughput 0.415045
Reading from 2736: heap size 374 MB, throughput 0.978289
Reading from 2735: heap size 658 MB, throughput 0.510844
Reading from 2735: heap size 661 MB, throughput 0.54116
Reading from 2735: heap size 666 MB, throughput 0.838363
Reading from 2736: heap size 377 MB, throughput 0.973965
Reading from 2735: heap size 669 MB, throughput 0.804154
Reading from 2735: heap size 668 MB, throughput 0.870235
Reading from 2735: heap size 683 MB, throughput 0.503896
Reading from 2736: heap size 378 MB, throughput 0.956283
Reading from 2735: heap size 692 MB, throughput 0.0452987
Reading from 2736: heap size 380 MB, throughput 0.944079
Reading from 2735: heap size 771 MB, throughput 0.443665
Reading from 2735: heap size 776 MB, throughput 0.43059
Reading from 2735: heap size 676 MB, throughput 0.397817
Equal recommendation: 3717 MB each
Reading from 2735: heap size 759 MB, throughput 0.0424785
Reading from 2735: heap size 848 MB, throughput 0.336935
Reading from 2736: heap size 382 MB, throughput 0.98036
Reading from 2735: heap size 845 MB, throughput 0.852433
Reading from 2735: heap size 847 MB, throughput 0.674653
Reading from 2735: heap size 836 MB, throughput 0.582131
Reading from 2735: heap size 842 MB, throughput 0.613157
Reading from 2736: heap size 384 MB, throughput 0.967048
Reading from 2735: heap size 840 MB, throughput 0.0728349
Reading from 2735: heap size 926 MB, throughput 0.799281
Reading from 2735: heap size 930 MB, throughput 0.832071
Reading from 2735: heap size 932 MB, throughput 0.783253
Reading from 2735: heap size 937 MB, throughput 0.931697
Reading from 2735: heap size 941 MB, throughput 0.896907
Reading from 2736: heap size 383 MB, throughput 0.966164
Reading from 2735: heap size 939 MB, throughput 0.930708
Reading from 2735: heap size 770 MB, throughput 0.853937
Reading from 2735: heap size 916 MB, throughput 0.811669
Reading from 2735: heap size 790 MB, throughput 0.809649
Reading from 2735: heap size 903 MB, throughput 0.849582
Reading from 2735: heap size 795 MB, throughput 0.800049
Reading from 2735: heap size 895 MB, throughput 0.808152
Reading from 2736: heap size 385 MB, throughput 0.974343
Reading from 2735: heap size 788 MB, throughput 0.846858
Reading from 2735: heap size 889 MB, throughput 0.820474
Reading from 2735: heap size 895 MB, throughput 0.844428
Reading from 2735: heap size 883 MB, throughput 0.84083
Reading from 2735: heap size 889 MB, throughput 0.864782
Reading from 2736: heap size 388 MB, throughput 0.970434
Reading from 2735: heap size 881 MB, throughput 0.980321
Reading from 2736: heap size 388 MB, throughput 0.951833
Reading from 2735: heap size 885 MB, throughput 0.950851
Reading from 2735: heap size 882 MB, throughput 0.772163
Reading from 2735: heap size 886 MB, throughput 0.793138
Equal recommendation: 3717 MB each
Reading from 2735: heap size 883 MB, throughput 0.801496
Reading from 2735: heap size 886 MB, throughput 0.810245
Reading from 2735: heap size 885 MB, throughput 0.781692
Reading from 2735: heap size 887 MB, throughput 0.783813
Reading from 2735: heap size 888 MB, throughput 0.768263
Reading from 2735: heap size 890 MB, throughput 0.819417
Reading from 2735: heap size 890 MB, throughput 0.842093
Reading from 2736: heap size 391 MB, throughput 0.961782
Reading from 2735: heap size 893 MB, throughput 0.868722
Reading from 2735: heap size 895 MB, throughput 0.84687
Reading from 2736: heap size 393 MB, throughput 0.973385
Reading from 2736: heap size 397 MB, throughput 0.729828
Reading from 2736: heap size 399 MB, throughput 0.72602
Reading from 2736: heap size 408 MB, throughput 0.759535
Reading from 2735: heap size 898 MB, throughput 0.0858412
Reading from 2736: heap size 410 MB, throughput 0.965838
Reading from 2735: heap size 979 MB, throughput 0.487155
Reading from 2735: heap size 999 MB, throughput 0.74632
Reading from 2735: heap size 1000 MB, throughput 0.712015
Reading from 2735: heap size 1002 MB, throughput 0.752105
Reading from 2735: heap size 1001 MB, throughput 0.675278
Reading from 2735: heap size 1005 MB, throughput 0.734193
Reading from 2735: heap size 1012 MB, throughput 0.706163
Reading from 2735: heap size 1014 MB, throughput 0.674446
Reading from 2735: heap size 1028 MB, throughput 0.716507
Reading from 2735: heap size 1028 MB, throughput 0.651389
Reading from 2736: heap size 418 MB, throughput 0.987683
Reading from 2735: heap size 1044 MB, throughput 0.889975
Reading from 2736: heap size 420 MB, throughput 0.986951
Reading from 2735: heap size 1046 MB, throughput 0.944718
Equal recommendation: 3717 MB each
Reading from 2736: heap size 422 MB, throughput 0.987901
Reading from 2735: heap size 1065 MB, throughput 0.938481
Reading from 2735: heap size 1067 MB, throughput 0.951244
Reading from 2736: heap size 425 MB, throughput 0.986533
Reading from 2735: heap size 1075 MB, throughput 0.950935
Reading from 2736: heap size 425 MB, throughput 0.982743
Reading from 2735: heap size 1078 MB, throughput 0.949919
Reading from 2736: heap size 427 MB, throughput 0.984601
Reading from 2735: heap size 1076 MB, throughput 0.959764
Reading from 2736: heap size 424 MB, throughput 0.978387
Reading from 2735: heap size 1081 MB, throughput 0.947186
Equal recommendation: 3717 MB each
Reading from 2736: heap size 426 MB, throughput 0.983418
Reading from 2735: heap size 1079 MB, throughput 0.952805
Reading from 2736: heap size 425 MB, throughput 0.981875
Reading from 2735: heap size 1082 MB, throughput 0.945483
Reading from 2736: heap size 427 MB, throughput 0.98616
Reading from 2736: heap size 428 MB, throughput 0.889126
Reading from 2736: heap size 429 MB, throughput 0.864393
Reading from 2736: heap size 434 MB, throughput 0.910864
Reading from 2735: heap size 1089 MB, throughput 0.954203
Reading from 2735: heap size 1089 MB, throughput 0.945594
Reading from 2736: heap size 434 MB, throughput 0.989759
Reading from 2735: heap size 1096 MB, throughput 0.948096
Reading from 2736: heap size 441 MB, throughput 0.988844
Equal recommendation: 3717 MB each
Reading from 2735: heap size 1098 MB, throughput 0.947901
Reading from 2736: heap size 442 MB, throughput 0.989046
Reading from 2735: heap size 1105 MB, throughput 0.959073
Reading from 2736: heap size 441 MB, throughput 0.98812
Reading from 2735: heap size 1109 MB, throughput 0.951587
Reading from 2736: heap size 444 MB, throughput 0.986489
Reading from 2735: heap size 1114 MB, throughput 0.962071
Reading from 2736: heap size 441 MB, throughput 0.808581
Equal recommendation: 3717 MB each
Reading from 2735: heap size 1120 MB, throughput 0.956737
Reading from 2736: heap size 476 MB, throughput 0.995033
Reading from 2736: heap size 475 MB, throughput 0.989156
Reading from 2735: heap size 1127 MB, throughput 0.504064
Reading from 2736: heap size 478 MB, throughput 0.975392
Reading from 2735: heap size 1222 MB, throughput 0.940165
Reading from 2736: heap size 479 MB, throughput 0.897236
Reading from 2736: heap size 481 MB, throughput 0.861919
Reading from 2736: heap size 487 MB, throughput 0.983096
Reading from 2735: heap size 1227 MB, throughput 0.97535
Equal recommendation: 3717 MB each
Reading from 2736: heap size 490 MB, throughput 0.991443
Reading from 2735: heap size 1231 MB, throughput 0.973601
Reading from 2736: heap size 489 MB, throughput 0.990427
Reading from 2735: heap size 1237 MB, throughput 0.963291
Reading from 2736: heap size 492 MB, throughput 0.989025
Reading from 2735: heap size 1238 MB, throughput 0.970957
Reading from 2736: heap size 490 MB, throughput 0.986257
Reading from 2735: heap size 1233 MB, throughput 0.968261
Reading from 2736: heap size 493 MB, throughput 0.986623
Reading from 2735: heap size 1237 MB, throughput 0.973714
Equal recommendation: 3717 MB each
Reading from 2735: heap size 1226 MB, throughput 0.973737
Reading from 2736: heap size 496 MB, throughput 0.988022
Reading from 2736: heap size 496 MB, throughput 0.975853
Reading from 2735: heap size 1232 MB, throughput 0.957529
Reading from 2736: heap size 499 MB, throughput 0.989308
Reading from 2735: heap size 1234 MB, throughput 0.961851
Reading from 2736: heap size 501 MB, throughput 0.807927
Reading from 2736: heap size 503 MB, throughput 0.892136
Reading from 2736: heap size 506 MB, throughput 0.989868
Reading from 2735: heap size 1234 MB, throughput 0.96136
Equal recommendation: 3717 MB each
Reading from 2735: heap size 1239 MB, throughput 0.959443
Reading from 2736: heap size 513 MB, throughput 0.992357
Reading from 2735: heap size 1242 MB, throughput 0.953868
Reading from 2736: heap size 516 MB, throughput 0.989078
Reading from 2735: heap size 1249 MB, throughput 0.956045
Reading from 2736: heap size 517 MB, throughput 0.990075
Reading from 2735: heap size 1254 MB, throughput 0.956197
Equal recommendation: 3717 MB each
Reading from 2736: heap size 519 MB, throughput 0.988805
Reading from 2735: heap size 1261 MB, throughput 0.945818
Reading from 2736: heap size 518 MB, throughput 0.989067
Reading from 2735: heap size 1269 MB, throughput 0.960509
Reading from 2736: heap size 520 MB, throughput 0.986238
Reading from 2735: heap size 1278 MB, throughput 0.955499
Reading from 2736: heap size 521 MB, throughput 0.985939
Reading from 2736: heap size 524 MB, throughput 0.903721
Reading from 2736: heap size 528 MB, throughput 0.976682
Reading from 2735: heap size 1283 MB, throughput 0.958055
Equal recommendation: 3717 MB each
Reading from 2736: heap size 529 MB, throughput 0.992191
Reading from 2735: heap size 1292 MB, throughput 0.949365
Reading from 2736: heap size 533 MB, throughput 0.991984
Reading from 2735: heap size 1295 MB, throughput 0.959724
Reading from 2736: heap size 534 MB, throughput 0.99036
Reading from 2735: heap size 1304 MB, throughput 0.956742
Equal recommendation: 3717 MB each
Reading from 2736: heap size 533 MB, throughput 0.989892
Reading from 2735: heap size 1305 MB, throughput 0.544912
Reading from 2736: heap size 535 MB, throughput 0.990163
Reading from 2735: heap size 1423 MB, throughput 0.929841
Reading from 2736: heap size 536 MB, throughput 0.988966
Reading from 2735: heap size 1423 MB, throughput 0.969038
Reading from 2736: heap size 537 MB, throughput 0.989288
Reading from 2736: heap size 539 MB, throughput 0.902382
Reading from 2736: heap size 540 MB, throughput 0.980523
Equal recommendation: 3717 MB each
Reading from 2736: heap size 548 MB, throughput 0.99262
Reading from 2736: heap size 548 MB, throughput 0.99139
Reading from 2736: heap size 549 MB, throughput 0.991792
Equal recommendation: 3717 MB each
Reading from 2736: heap size 551 MB, throughput 0.990259
Reading from 2736: heap size 550 MB, throughput 0.989552
Reading from 2736: heap size 551 MB, throughput 0.994492
Reading from 2736: heap size 553 MB, throughput 0.937089
Reading from 2736: heap size 554 MB, throughput 0.908212
Equal recommendation: 3717 MB each
Reading from 2736: heap size 560 MB, throughput 0.992677
Reading from 2736: heap size 562 MB, throughput 0.99264
Reading from 2736: heap size 565 MB, throughput 0.991919
Equal recommendation: 3717 MB each
Reading from 2735: heap size 1434 MB, throughput 0.997472
Reading from 2736: heap size 567 MB, throughput 0.990548
Reading from 2736: heap size 567 MB, throughput 0.99013
Reading from 2735: heap size 1435 MB, throughput 0.983323
Reading from 2735: heap size 1424 MB, throughput 0.847231
Reading from 2736: heap size 568 MB, throughput 0.989628
Reading from 2735: heap size 1443 MB, throughput 0.742111
Reading from 2735: heap size 1430 MB, throughput 0.678744
Reading from 2736: heap size 570 MB, throughput 0.92202
Reading from 2736: heap size 571 MB, throughput 0.940299
Reading from 2735: heap size 1468 MB, throughput 0.694706
Equal recommendation: 3717 MB each
Reading from 2735: heap size 1496 MB, throughput 0.734635
Reading from 2735: heap size 1509 MB, throughput 0.706101
Reading from 2735: heap size 1542 MB, throughput 0.775453
Reading from 2736: heap size 581 MB, throughput 0.993576
Reading from 2735: heap size 1549 MB, throughput 0.95762
Reading from 2735: heap size 1543 MB, throughput 0.967429
Reading from 2736: heap size 581 MB, throughput 0.993466
Reading from 2735: heap size 1556 MB, throughput 0.961393
Equal recommendation: 3717 MB each
Reading from 2736: heap size 583 MB, throughput 0.992282
Reading from 2735: heap size 1546 MB, throughput 0.969675
Reading from 2736: heap size 585 MB, throughput 0.990656
Reading from 2735: heap size 1560 MB, throughput 0.965139
Reading from 2736: heap size 587 MB, throughput 0.990613
Reading from 2735: heap size 1552 MB, throughput 0.964607
Equal recommendation: 3717 MB each
Reading from 2736: heap size 587 MB, throughput 0.989998
Reading from 2736: heap size 591 MB, throughput 0.918586
Reading from 2735: heap size 1563 MB, throughput 0.755445
Reading from 2736: heap size 593 MB, throughput 0.991915
Reading from 2735: heap size 1558 MB, throughput 0.991576
Reading from 2736: heap size 600 MB, throughput 0.99275
Equal recommendation: 3717 MB each
Reading from 2735: heap size 1561 MB, throughput 0.987515
Reading from 2736: heap size 601 MB, throughput 0.99236
Reading from 2735: heap size 1587 MB, throughput 0.989842
Reading from 2736: heap size 601 MB, throughput 0.991759
Reading from 2735: heap size 1594 MB, throughput 0.982612
Reading from 2736: heap size 603 MB, throughput 0.990274
Equal recommendation: 3717 MB each
Reading from 2736: heap size 606 MB, throughput 0.990867
Reading from 2736: heap size 606 MB, throughput 0.9269
Reading from 2735: heap size 1602 MB, throughput 0.98368
Reading from 2736: heap size 610 MB, throughput 0.991698
Reading from 2735: heap size 1606 MB, throughput 0.97814
Reading from 2736: heap size 612 MB, throughput 0.993816
Equal recommendation: 3717 MB each
Reading from 2735: heap size 1593 MB, throughput 0.977873
Reading from 2736: heap size 614 MB, throughput 0.992411
Reading from 2735: heap size 1602 MB, throughput 0.97467
Reading from 2736: heap size 616 MB, throughput 0.992289
Reading from 2735: heap size 1579 MB, throughput 0.973063
Equal recommendation: 3717 MB each
Reading from 2736: heap size 617 MB, throughput 0.989821
Reading from 2736: heap size 618 MB, throughput 0.969633
Reading from 2735: heap size 1591 MB, throughput 0.968561
Reading from 2736: heap size 623 MB, throughput 0.94953
Reading from 2735: heap size 1595 MB, throughput 0.969317
Reading from 2736: heap size 625 MB, throughput 0.994308
Equal recommendation: 3717 MB each
Reading from 2736: heap size 627 MB, throughput 0.992564
Reading from 2735: heap size 1595 MB, throughput 0.963151
Reading from 2736: heap size 630 MB, throughput 0.99236
Reading from 2735: heap size 1605 MB, throughput 0.969023
Reading from 2736: heap size 629 MB, throughput 0.992088
Reading from 2735: heap size 1611 MB, throughput 0.962834
Equal recommendation: 3717 MB each
Reading from 2736: heap size 631 MB, throughput 0.995122
Reading from 2736: heap size 634 MB, throughput 0.941873
Reading from 2735: heap size 1623 MB, throughput 0.963005
Reading from 2736: heap size 635 MB, throughput 0.991288
Reading from 2735: heap size 1634 MB, throughput 0.956193
Equal recommendation: 3717 MB each
Reading from 2736: heap size 641 MB, throughput 0.993637
Reading from 2735: heap size 1652 MB, throughput 0.960821
Reading from 2736: heap size 642 MB, throughput 0.992699
Reading from 2735: heap size 1663 MB, throughput 0.963333
Reading from 2736: heap size 640 MB, throughput 0.992099
Equal recommendation: 3717 MB each
Reading from 2735: heap size 1682 MB, throughput 0.96163
Reading from 2736: heap size 643 MB, throughput 0.995781
Reading from 2736: heap size 645 MB, throughput 0.939234
Client 2736 died
Clients: 1
Reading from 2735: heap size 1688 MB, throughput 0.971804
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 2735: heap size 1706 MB, throughput 0.993634
Reading from 2735: heap size 1711 MB, throughput 0.972118
Reading from 2735: heap size 1732 MB, throughput 0.826142
Reading from 2735: heap size 1744 MB, throughput 0.787333
Reading from 2735: heap size 1777 MB, throughput 0.820811
Client 2735 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
Recommendation: no clients; no recommendation
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
