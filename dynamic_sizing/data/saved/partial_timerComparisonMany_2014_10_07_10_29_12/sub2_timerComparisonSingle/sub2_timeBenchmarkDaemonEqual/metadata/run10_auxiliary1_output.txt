economemd
    total memory: 5012 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub2_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run10_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 9842: heap size 9 MB, throughput 0.989409
Clients: 1
Client 9842 has a minimum heap size of 30 MB
Reading from 9841: heap size 9 MB, throughput 0.987402
Clients: 2
Client 9841 has a minimum heap size of 1223 MB
Reading from 9842: heap size 9 MB, throughput 0.973038
Reading from 9842: heap size 9 MB, throughput 0.957103
Reading from 9841: heap size 9 MB, throughput 0.975488
Reading from 9842: heap size 9 MB, throughput 0.835677
Reading from 9842: heap size 11 MB, throughput 0.689932
Reading from 9842: heap size 11 MB, throughput 0.763191
Reading from 9842: heap size 16 MB, throughput 0.688414
Reading from 9842: heap size 16 MB, throughput 0.656073
Reading from 9842: heap size 24 MB, throughput 0.86546
Reading from 9842: heap size 24 MB, throughput 0.849448
Reading from 9842: heap size 34 MB, throughput 0.911317
Reading from 9842: heap size 34 MB, throughput 0.870482
Reading from 9842: heap size 50 MB, throughput 0.942804
Reading from 9842: heap size 50 MB, throughput 0.922828
Reading from 9841: heap size 11 MB, throughput 0.968386
Reading from 9842: heap size 76 MB, throughput 0.964727
Reading from 9842: heap size 76 MB, throughput 0.93689
Reading from 9842: heap size 115 MB, throughput 0.972616
Reading from 9842: heap size 116 MB, throughput 0.96514
Reading from 9841: heap size 11 MB, throughput 0.991343
Reading from 9841: heap size 15 MB, throughput 0.838322
Reading from 9841: heap size 18 MB, throughput 0.88738
Reading from 9841: heap size 24 MB, throughput 0.968765
Reading from 9841: heap size 28 MB, throughput 0.610181
Reading from 9841: heap size 40 MB, throughput 0.925533
Reading from 9841: heap size 41 MB, throughput 0.913226
Reading from 9841: heap size 44 MB, throughput 0.89162
Reading from 9842: heap size 159 MB, throughput 0.955614
Reading from 9841: heap size 46 MB, throughput 0.242108
Reading from 9841: heap size 62 MB, throughput 0.907624
Reading from 9841: heap size 65 MB, throughput 0.714199
Reading from 9842: heap size 165 MB, throughput 0.810727
Reading from 9841: heap size 69 MB, throughput 0.249116
Reading from 9841: heap size 89 MB, throughput 0.645108
Reading from 9841: heap size 97 MB, throughput 0.874582
Reading from 9841: heap size 98 MB, throughput 0.780874
Reading from 9842: heap size 177 MB, throughput 0.982989
Reading from 9841: heap size 101 MB, throughput 0.150848
Reading from 9841: heap size 127 MB, throughput 0.692857
Reading from 9841: heap size 133 MB, throughput 0.602224
Reading from 9841: heap size 136 MB, throughput 0.552516
Reading from 9842: heap size 206 MB, throughput 0.905215
Reading from 9841: heap size 141 MB, throughput 0.698183
Reading from 9841: heap size 147 MB, throughput 0.455872
Reading from 9841: heap size 155 MB, throughput 0.125769
Reading from 9841: heap size 188 MB, throughput 0.61356
Reading from 9842: heap size 233 MB, throughput 0.996427
Reading from 9841: heap size 195 MB, throughput 0.703953
Reading from 9841: heap size 196 MB, throughput 0.681788
Reading from 9841: heap size 201 MB, throughput 0.715092
Reading from 9841: heap size 205 MB, throughput 0.551719
Reading from 9841: heap size 210 MB, throughput 0.121447
Reading from 9842: heap size 236 MB, throughput 0.889394
Reading from 9841: heap size 250 MB, throughput 0.505311
Reading from 9841: heap size 260 MB, throughput 0.70378
Reading from 9841: heap size 263 MB, throughput 0.650099
Reading from 9841: heap size 267 MB, throughput 0.698014
Reading from 9842: heap size 253 MB, throughput 0.986247
Reading from 9841: heap size 274 MB, throughput 0.398455
Reading from 9841: heap size 278 MB, throughput 0.49971
Reading from 9842: heap size 271 MB, throughput 0.970355
Reading from 9841: heap size 288 MB, throughput 0.139117
Reading from 9841: heap size 336 MB, throughput 0.468039
Reading from 9841: heap size 340 MB, throughput 0.123229
Reading from 9841: heap size 383 MB, throughput 0.495598
Reading from 9841: heap size 309 MB, throughput 0.481473
Reading from 9842: heap size 278 MB, throughput 0.992033
Reading from 9841: heap size 371 MB, throughput 0.528291
Reading from 9841: heap size 378 MB, throughput 0.516224
Reading from 9842: heap size 296 MB, throughput 0.81698
Equal recommendation: 2506 MB each
Reading from 9841: heap size 371 MB, throughput 0.173593
Reading from 9841: heap size 422 MB, throughput 0.586126
Reading from 9841: heap size 417 MB, throughput 0.626813
Reading from 9842: heap size 305 MB, throughput 0.992349
Reading from 9841: heap size 422 MB, throughput 0.615989
Reading from 9841: heap size 423 MB, throughput 0.576578
Reading from 9841: heap size 426 MB, throughput 0.559089
Reading from 9841: heap size 430 MB, throughput 0.473158
Reading from 9841: heap size 439 MB, throughput 0.546788
Reading from 9841: heap size 445 MB, throughput 0.506697
Reading from 9842: heap size 327 MB, throughput 0.975543
Reading from 9841: heap size 455 MB, throughput 0.495507
Reading from 9842: heap size 330 MB, throughput 0.974763
Reading from 9841: heap size 460 MB, throughput 0.070417
Reading from 9841: heap size 517 MB, throughput 0.457773
Reading from 9841: heap size 531 MB, throughput 0.508222
Reading from 9841: heap size 531 MB, throughput 0.555863
Reading from 9842: heap size 349 MB, throughput 0.978875
Reading from 9841: heap size 535 MB, throughput 0.0951202
Reading from 9842: heap size 353 MB, throughput 0.97579
Reading from 9841: heap size 591 MB, throughput 0.374646
Reading from 9841: heap size 587 MB, throughput 0.635249
Reading from 9841: heap size 593 MB, throughput 0.467927
Reading from 9841: heap size 595 MB, throughput 0.584057
Reading from 9842: heap size 374 MB, throughput 0.973402
Reading from 9841: heap size 599 MB, throughput 0.0946712
Reading from 9841: heap size 670 MB, throughput 0.438961
Reading from 9841: heap size 671 MB, throughput 0.558086
Reading from 9842: heap size 377 MB, throughput 0.893335
Reading from 9841: heap size 673 MB, throughput 0.80576
Reading from 9841: heap size 676 MB, throughput 0.797834
Reading from 9842: heap size 412 MB, throughput 0.996526
Reading from 9841: heap size 676 MB, throughput 0.852631
Reading from 9841: heap size 688 MB, throughput 0.573639
Reading from 9842: heap size 416 MB, throughput 0.987988
Equal recommendation: 2506 MB each
Reading from 9842: heap size 437 MB, throughput 0.988476
Reading from 9841: heap size 696 MB, throughput 0.0946842
Reading from 9841: heap size 773 MB, throughput 0.319503
Reading from 9841: heap size 776 MB, throughput 0.361832
Reading from 9841: heap size 780 MB, throughput 0.364438
Reading from 9841: heap size 765 MB, throughput 0.37977
Reading from 9842: heap size 438 MB, throughput 0.982125
Reading from 9842: heap size 460 MB, throughput 0.993548
Reading from 9841: heap size 772 MB, throughput 0.217443
Reading from 9841: heap size 840 MB, throughput 0.346329
Reading from 9841: heap size 845 MB, throughput 0.601216
Reading from 9841: heap size 852 MB, throughput 0.615259
Reading from 9842: heap size 460 MB, throughput 0.983166
Reading from 9841: heap size 852 MB, throughput 0.833786
Reading from 9841: heap size 859 MB, throughput 0.824917
Reading from 9842: heap size 478 MB, throughput 0.973853
Reading from 9841: heap size 860 MB, throughput 0.0847744
Reading from 9841: heap size 947 MB, throughput 0.697768
Reading from 9841: heap size 949 MB, throughput 0.832553
Reading from 9841: heap size 959 MB, throughput 0.903845
Reading from 9842: heap size 482 MB, throughput 0.984333
Reading from 9841: heap size 961 MB, throughput 0.810838
Reading from 9841: heap size 951 MB, throughput 0.842772
Reading from 9841: heap size 804 MB, throughput 0.855092
Reading from 9841: heap size 943 MB, throughput 0.793928
Reading from 9841: heap size 950 MB, throughput 0.851879
Reading from 9841: heap size 937 MB, throughput 0.824151
Reading from 9841: heap size 943 MB, throughput 0.835241
Reading from 9842: heap size 502 MB, throughput 0.972827
Reading from 9841: heap size 930 MB, throughput 0.789878
Reading from 9841: heap size 937 MB, throughput 0.860878
Equal recommendation: 2506 MB each
Reading from 9842: heap size 505 MB, throughput 0.981137
Reading from 9841: heap size 924 MB, throughput 0.973878
Reading from 9841: heap size 931 MB, throughput 0.892693
Reading from 9841: heap size 922 MB, throughput 0.716639
Reading from 9842: heap size 529 MB, throughput 0.977826
Reading from 9841: heap size 927 MB, throughput 0.737794
Reading from 9841: heap size 922 MB, throughput 0.791355
Reading from 9841: heap size 926 MB, throughput 0.813078
Reading from 9841: heap size 921 MB, throughput 0.819867
Reading from 9841: heap size 926 MB, throughput 0.771143
Reading from 9841: heap size 923 MB, throughput 0.842955
Reading from 9842: heap size 532 MB, throughput 0.97474
Reading from 9841: heap size 927 MB, throughput 0.886054
Reading from 9841: heap size 928 MB, throughput 0.919133
Reading from 9841: heap size 931 MB, throughput 0.786725
Reading from 9841: heap size 930 MB, throughput 0.691732
Reading from 9841: heap size 939 MB, throughput 0.632019
Reading from 9841: heap size 958 MB, throughput 0.601621
Reading from 9842: heap size 560 MB, throughput 0.982578
Reading from 9842: heap size 562 MB, throughput 0.990266
Reading from 9841: heap size 968 MB, throughput 0.0481189
Reading from 9841: heap size 1086 MB, throughput 0.485988
Reading from 9841: heap size 1091 MB, throughput 0.68761
Reading from 9841: heap size 1094 MB, throughput 0.709692
Reading from 9842: heap size 581 MB, throughput 0.988337
Reading from 9841: heap size 1097 MB, throughput 0.684854
Reading from 9841: heap size 1110 MB, throughput 0.638209
Reading from 9841: heap size 1111 MB, throughput 0.873869
Equal recommendation: 2506 MB each
Reading from 9842: heap size 587 MB, throughput 0.980519
Reading from 9842: heap size 617 MB, throughput 0.975038
Reading from 9841: heap size 1125 MB, throughput 0.97127
Reading from 9842: heap size 617 MB, throughput 0.990921
Reading from 9842: heap size 643 MB, throughput 0.990303
Reading from 9841: heap size 1130 MB, throughput 0.964313
Reading from 9842: heap size 647 MB, throughput 0.986431
Reading from 9841: heap size 1141 MB, throughput 0.967795
Equal recommendation: 2506 MB each
Reading from 9842: heap size 675 MB, throughput 0.987886
Reading from 9842: heap size 676 MB, throughput 0.995386
Reading from 9841: heap size 1145 MB, throughput 0.968394
Reading from 9842: heap size 696 MB, throughput 0.989623
Reading from 9841: heap size 1143 MB, throughput 0.964025
Reading from 9842: heap size 702 MB, throughput 0.989157
Reading from 9842: heap size 728 MB, throughput 0.989013
Reading from 9841: heap size 1149 MB, throughput 0.969703
Equal recommendation: 2506 MB each
Reading from 9842: heap size 728 MB, throughput 0.996323
Reading from 9841: heap size 1141 MB, throughput 0.971648
Reading from 9842: heap size 751 MB, throughput 0.996007
Reading from 9842: heap size 753 MB, throughput 0.988392
Reading from 9841: heap size 1147 MB, throughput 0.970421
Reading from 9842: heap size 772 MB, throughput 0.988997
Reading from 9841: heap size 1140 MB, throughput 0.970972
Reading from 9842: heap size 773 MB, throughput 0.989774
Equal recommendation: 2506 MB each
Reading from 9841: heap size 1144 MB, throughput 0.973072
Reading from 9842: heap size 797 MB, throughput 0.981432
Reading from 9842: heap size 797 MB, throughput 0.99212
Reading from 9841: heap size 1148 MB, throughput 0.970449
Reading from 9842: heap size 822 MB, throughput 0.990569
Reading from 9841: heap size 1148 MB, throughput 0.970457
Reading from 9842: heap size 825 MB, throughput 0.990726
Equal recommendation: 2506 MB each
Reading from 9842: heap size 853 MB, throughput 0.991423
Reading from 9841: heap size 1153 MB, throughput 0.974358
Reading from 9842: heap size 853 MB, throughput 0.990937
Reading from 9841: heap size 1155 MB, throughput 0.963898
Reading from 9842: heap size 881 MB, throughput 0.991613
Equal recommendation: 2506 MB each
Reading from 9841: heap size 1159 MB, throughput 0.96894
Reading from 9842: heap size 881 MB, throughput 0.990724
Reading from 9842: heap size 910 MB, throughput 0.992671
Reading from 9841: heap size 1163 MB, throughput 0.973444
Reading from 9842: heap size 910 MB, throughput 0.990573
Reading from 9841: heap size 1168 MB, throughput 0.969864
Reading from 9842: heap size 937 MB, throughput 0.992514
Equal recommendation: 2506 MB each
Reading from 9841: heap size 1172 MB, throughput 0.972435
Reading from 9842: heap size 937 MB, throughput 0.991615
Reading from 9841: heap size 1177 MB, throughput 0.973365
Reading from 9842: heap size 966 MB, throughput 0.991817
Reading from 9842: heap size 966 MB, throughput 0.991187
Reading from 9841: heap size 1179 MB, throughput 0.973129
Equal recommendation: 2506 MB each
Reading from 9842: heap size 994 MB, throughput 0.990098
Reading from 9841: heap size 1184 MB, throughput 0.969419
Reading from 9842: heap size 994 MB, throughput 0.990776
Reading from 9842: heap size 1027 MB, throughput 0.993298
Reading from 9841: heap size 1185 MB, throughput 0.631174
Equal recommendation: 2506 MB each
Reading from 9842: heap size 1027 MB, throughput 0.991429
Reading from 9841: heap size 1282 MB, throughput 0.955768
Reading from 9842: heap size 1057 MB, throughput 0.993263
Reading from 9841: heap size 1283 MB, throughput 0.983036
Reading from 9842: heap size 1058 MB, throughput 0.990892
Reading from 9841: heap size 1292 MB, throughput 0.982395
Equal recommendation: 2506 MB each
Reading from 9842: heap size 1088 MB, throughput 0.990099
Reading from 9841: heap size 1294 MB, throughput 0.984194
Reading from 9842: heap size 1088 MB, throughput 0.991639
Reading from 9841: heap size 1293 MB, throughput 0.979326
Reading from 9842: heap size 1121 MB, throughput 0.994036
Equal recommendation: 2506 MB each
Reading from 9841: heap size 1296 MB, throughput 0.98048
Reading from 9842: heap size 1122 MB, throughput 0.991466
Reading from 9841: heap size 1285 MB, throughput 0.977695
Reading from 9842: heap size 1153 MB, throughput 0.989105
Reading from 9841: heap size 1218 MB, throughput 0.979676
Reading from 9842: heap size 1153 MB, throughput 0.99118
Equal recommendation: 2506 MB each
Reading from 9841: heap size 1277 MB, throughput 0.975798
Reading from 9842: heap size 1192 MB, throughput 0.991937
Reading from 9841: heap size 1283 MB, throughput 0.98118
Reading from 9842: heap size 1194 MB, throughput 0.987445
Equal recommendation: 2506 MB each
Reading from 9841: heap size 1284 MB, throughput 0.973899
Reading from 9842: heap size 1227 MB, throughput 0.992501
Reading from 9841: heap size 1284 MB, throughput 0.971258
Reading from 9842: heap size 1233 MB, throughput 0.988568
Reading from 9841: heap size 1288 MB, throughput 0.968438
Reading from 9842: heap size 1272 MB, throughput 0.99199
Equal recommendation: 2506 MB each
Reading from 9842: heap size 1274 MB, throughput 0.980189
Reading from 9841: heap size 1291 MB, throughput 0.970844
Reading from 9841: heap size 1295 MB, throughput 0.973567
Reading from 9842: heap size 1314 MB, throughput 0.983285
Equal recommendation: 2506 MB each
Reading from 9841: heap size 1301 MB, throughput 0.973164
Reading from 9842: heap size 1315 MB, throughput 0.991923
Reading from 9841: heap size 1307 MB, throughput 0.973127
Reading from 9842: heap size 1367 MB, throughput 0.989653
Equal recommendation: 2506 MB each
Reading from 9841: heap size 1313 MB, throughput 0.969883
Reading from 9842: heap size 1371 MB, throughput 0.990076
Reading from 9841: heap size 1319 MB, throughput 0.967793
Reading from 9842: heap size 1424 MB, throughput 0.991439
Reading from 9842: heap size 1426 MB, throughput 0.987808
Equal recommendation: 2506 MB each
Reading from 9842: heap size 1474 MB, throughput 0.991006
Reading from 9842: heap size 1476 MB, throughput 0.958433
Equal recommendation: 2506 MB each
Reading from 9841: heap size 1322 MB, throughput 0.822485
Reading from 9842: heap size 1549 MB, throughput 0.994196
Reading from 9842: heap size 1549 MB, throughput 0.991406
Equal recommendation: 2506 MB each
Reading from 9842: heap size 1610 MB, throughput 0.993008
Reading from 9841: heap size 1436 MB, throughput 0.983789
Reading from 9842: heap size 1612 MB, throughput 0.990303
Reading from 9841: heap size 1380 MB, throughput 0.984869
Equal recommendation: 2506 MB each
Reading from 9841: heap size 1463 MB, throughput 0.831583
Reading from 9842: heap size 1674 MB, throughput 0.991179
Reading from 9841: heap size 1469 MB, throughput 0.760958
Reading from 9841: heap size 1452 MB, throughput 0.689466
Reading from 9841: heap size 1482 MB, throughput 0.702478
Reading from 9841: heap size 1507 MB, throughput 0.70935
Reading from 9841: heap size 1519 MB, throughput 0.710932
Reading from 9841: heap size 1548 MB, throughput 0.7255
Reading from 9841: heap size 1554 MB, throughput 0.76814
Reading from 9842: heap size 1674 MB, throughput 0.991515
Reading from 9841: heap size 1573 MB, throughput 0.981842
Equal recommendation: 2506 MB each
Reading from 9841: heap size 1581 MB, throughput 0.979304
Reading from 9842: heap size 1697 MB, throughput 0.992107
Reading from 9841: heap size 1580 MB, throughput 0.980273
Reading from 9842: heap size 1697 MB, throughput 0.99239
Reading from 9841: heap size 1592 MB, throughput 0.972843
Equal recommendation: 2506 MB each
Reading from 9842: heap size 1697 MB, throughput 0.991572
Client 9842 died
Clients: 1
Reading from 9841: heap size 1585 MB, throughput 0.983901
Recommendation: one client; give it all the memory
Reading from 9841: heap size 1596 MB, throughput 0.866037
Reading from 9841: heap size 1534 MB, throughput 0.995962
Recommendation: one client; give it all the memory
Reading from 9841: heap size 1536 MB, throughput 0.995062
Reading from 9841: heap size 1558 MB, throughput 0.99346
Recommendation: one client; give it all the memory
Reading from 9841: heap size 1563 MB, throughput 0.992664
Reading from 9841: heap size 1563 MB, throughput 0.991128
Recommendation: one client; give it all the memory
Reading from 9841: heap size 1569 MB, throughput 0.990267
Reading from 9841: heap size 1549 MB, throughput 0.990027
Recommendation: one client; give it all the memory
Reading from 9841: heap size 1397 MB, throughput 0.989017
Reading from 9841: heap size 1528 MB, throughput 0.987498
Recommendation: one client; give it all the memory
Reading from 9841: heap size 1425 MB, throughput 0.986443
Reading from 9841: heap size 1527 MB, throughput 0.985579
Recommendation: one client; give it all the memory
Reading from 9841: heap size 1532 MB, throughput 0.984507
Recommendation: one client; give it all the memory
Reading from 9841: heap size 1535 MB, throughput 0.983762
Reading from 9841: heap size 1538 MB, throughput 0.982406
Recommendation: one client; give it all the memory
Reading from 9841: heap size 1541 MB, throughput 0.982376
Reading from 9841: heap size 1549 MB, throughput 0.979617
Recommendation: one client; give it all the memory
Reading from 9841: heap size 1558 MB, throughput 0.981135
Reading from 9841: heap size 1566 MB, throughput 0.979775
Recommendation: one client; give it all the memory
Reading from 9841: heap size 1575 MB, throughput 0.979991
Reading from 9841: heap size 1580 MB, throughput 0.979301
Recommendation: one client; give it all the memory
Reading from 9841: heap size 1589 MB, throughput 0.979636
Recommendation: one client; give it all the memory
Reading from 9841: heap size 1591 MB, throughput 0.980443
Reading from 9841: heap size 1601 MB, throughput 0.979971
Recommendation: one client; give it all the memory
Reading from 9841: heap size 1601 MB, throughput 0.988233
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 9841: heap size 1598 MB, throughput 0.989114
Reading from 9841: heap size 1610 MB, throughput 0.980853
Reading from 9841: heap size 1622 MB, throughput 0.833671
Reading from 9841: heap size 1627 MB, throughput 0.754927
Reading from 9841: heap size 1653 MB, throughput 0.790651
Reading from 9841: heap size 1670 MB, throughput 0.784217
Recommendation: one client; give it all the memory
Client 9841 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
