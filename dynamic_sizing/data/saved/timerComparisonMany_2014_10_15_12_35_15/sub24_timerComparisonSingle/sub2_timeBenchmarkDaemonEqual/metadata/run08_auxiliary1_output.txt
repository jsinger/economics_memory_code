economemd
    total memory: 6265 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub24_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 12641: heap size 9 MB, throughput 0.984501
Clients: 1
Client 12641 has a minimum heap size of 30 MB
Reading from 12639: heap size 9 MB, throughput 0.987064
Clients: 2
Client 12639 has a minimum heap size of 1223 MB
Reading from 12641: heap size 9 MB, throughput 0.971396
Reading from 12641: heap size 11 MB, throughput 0.980397
Reading from 12639: heap size 9 MB, throughput 0.973071
Reading from 12641: heap size 11 MB, throughput 0.850874
Reading from 12641: heap size 15 MB, throughput 0.734355
Reading from 12641: heap size 15 MB, throughput 0.529045
Reading from 12641: heap size 24 MB, throughput 0.831916
Reading from 12641: heap size 24 MB, throughput 0.576389
Reading from 12641: heap size 40 MB, throughput 0.909543
Reading from 12641: heap size 40 MB, throughput 0.9218
Reading from 12641: heap size 60 MB, throughput 0.935463
Reading from 12641: heap size 60 MB, throughput 0.927135
Reading from 12639: heap size 11 MB, throughput 0.975141
Reading from 12641: heap size 91 MB, throughput 0.966393
Reading from 12641: heap size 91 MB, throughput 0.944388
Reading from 12641: heap size 140 MB, throughput 0.970952
Reading from 12639: heap size 11 MB, throughput 0.988042
Reading from 12639: heap size 15 MB, throughput 0.840364
Reading from 12639: heap size 18 MB, throughput 0.955881
Reading from 12639: heap size 23 MB, throughput 0.934041
Reading from 12639: heap size 28 MB, throughput 0.842316
Reading from 12639: heap size 28 MB, throughput 0.225097
Reading from 12641: heap size 140 MB, throughput 0.932074
Reading from 12639: heap size 38 MB, throughput 0.719466
Reading from 12639: heap size 42 MB, throughput 0.784424
Reading from 12639: heap size 43 MB, throughput 0.676615
Reading from 12639: heap size 47 MB, throughput 0.260804
Reading from 12639: heap size 61 MB, throughput 0.854784
Reading from 12639: heap size 64 MB, throughput 0.719931
Reading from 12641: heap size 223 MB, throughput 0.893265
Reading from 12639: heap size 67 MB, throughput 0.394275
Reading from 12639: heap size 89 MB, throughput 0.613205
Reading from 12639: heap size 92 MB, throughput 0.805663
Reading from 12639: heap size 95 MB, throughput 0.73096
Reading from 12639: heap size 97 MB, throughput 0.189259
Reading from 12641: heap size 241 MB, throughput 0.983489
Reading from 12639: heap size 125 MB, throughput 0.508693
Reading from 12639: heap size 128 MB, throughput 0.649875
Reading from 12639: heap size 131 MB, throughput 0.541922
Reading from 12639: heap size 137 MB, throughput 0.542836
Reading from 12641: heap size 275 MB, throughput 0.99293
Reading from 12639: heap size 145 MB, throughput 0.0854614
Reading from 12639: heap size 178 MB, throughput 0.568202
Reading from 12639: heap size 182 MB, throughput 0.638611
Reading from 12639: heap size 184 MB, throughput 0.576425
Reading from 12639: heap size 186 MB, throughput 0.168825
Reading from 12639: heap size 224 MB, throughput 0.602146
Reading from 12639: heap size 234 MB, throughput 0.617112
Reading from 12639: heap size 234 MB, throughput 0.613357
Reading from 12641: heap size 284 MB, throughput 0.97873
Reading from 12639: heap size 238 MB, throughput 0.532499
Reading from 12639: heap size 244 MB, throughput 0.685343
Reading from 12641: heap size 305 MB, throughput 0.80221
Reading from 12639: heap size 249 MB, throughput 0.211887
Reading from 12639: heap size 291 MB, throughput 0.536105
Reading from 12639: heap size 301 MB, throughput 0.630746
Reading from 12639: heap size 304 MB, throughput 0.525766
Reading from 12639: heap size 307 MB, throughput 0.556767
Reading from 12641: heap size 327 MB, throughput 0.994507
Reading from 12639: heap size 315 MB, throughput 0.471045
Reading from 12639: heap size 322 MB, throughput 0.129511
Reading from 12639: heap size 372 MB, throughput 0.464021
Reading from 12639: heap size 377 MB, throughput 0.629515
Reading from 12641: heap size 362 MB, throughput 0.99092
Equal recommendation: 3132 MB each
Reading from 12639: heap size 379 MB, throughput 0.0966368
Reading from 12639: heap size 432 MB, throughput 0.533465
Reading from 12639: heap size 433 MB, throughput 0.606267
Reading from 12641: heap size 365 MB, throughput 0.980332
Reading from 12639: heap size 433 MB, throughput 0.658483
Reading from 12639: heap size 433 MB, throughput 0.560045
Reading from 12639: heap size 435 MB, throughput 0.619654
Reading from 12639: heap size 443 MB, throughput 0.528738
Reading from 12639: heap size 447 MB, throughput 0.456397
Reading from 12639: heap size 459 MB, throughput 0.514875
Reading from 12641: heap size 395 MB, throughput 0.989391
Reading from 12639: heap size 466 MB, throughput 0.0930787
Reading from 12639: heap size 527 MB, throughput 0.386839
Reading from 12639: heap size 537 MB, throughput 0.519732
Reading from 12641: heap size 398 MB, throughput 0.966778
Reading from 12639: heap size 539 MB, throughput 0.573895
Reading from 12639: heap size 545 MB, throughput 0.450892
Reading from 12641: heap size 418 MB, throughput 0.971371
Reading from 12639: heap size 548 MB, throughput 0.100404
Reading from 12639: heap size 615 MB, throughput 0.500871
Reading from 12639: heap size 539 MB, throughput 0.534547
Reading from 12641: heap size 423 MB, throughput 0.962157
Reading from 12639: heap size 604 MB, throughput 0.0831956
Reading from 12639: heap size 677 MB, throughput 0.492593
Reading from 12639: heap size 665 MB, throughput 0.661839
Reading from 12639: heap size 672 MB, throughput 0.614023
Reading from 12641: heap size 448 MB, throughput 0.983591
Reading from 12639: heap size 673 MB, throughput 0.8125
Reading from 12639: heap size 677 MB, throughput 0.744276
Reading from 12641: heap size 451 MB, throughput 0.989293
Reading from 12639: heap size 675 MB, throughput 0.859224
Reading from 12639: heap size 695 MB, throughput 0.490463
Equal recommendation: 3132 MB each
Reading from 12639: heap size 702 MB, throughput 0.675979
Reading from 12641: heap size 473 MB, throughput 0.977667
Reading from 12639: heap size 714 MB, throughput 0.0303777
Reading from 12641: heap size 478 MB, throughput 0.989456
Reading from 12639: heap size 791 MB, throughput 0.2318
Reading from 12639: heap size 700 MB, throughput 0.42109
Reading from 12639: heap size 776 MB, throughput 0.440116
Reading from 12639: heap size 783 MB, throughput 0.679012
Reading from 12639: heap size 773 MB, throughput 0.794935
Reading from 12641: heap size 502 MB, throughput 0.975119
Reading from 12639: heap size 778 MB, throughput 0.0528723
Reading from 12639: heap size 845 MB, throughput 0.401792
Reading from 12639: heap size 848 MB, throughput 0.608497
Reading from 12639: heap size 855 MB, throughput 0.799494
Reading from 12641: heap size 505 MB, throughput 0.977691
Reading from 12639: heap size 857 MB, throughput 0.848837
Reading from 12639: heap size 862 MB, throughput 0.137435
Reading from 12641: heap size 528 MB, throughput 0.993204
Reading from 12639: heap size 949 MB, throughput 0.730395
Reading from 12639: heap size 955 MB, throughput 0.901968
Reading from 12639: heap size 959 MB, throughput 0.922662
Reading from 12639: heap size 953 MB, throughput 0.841983
Reading from 12639: heap size 809 MB, throughput 0.819199
Reading from 12639: heap size 940 MB, throughput 0.827318
Reading from 12639: heap size 814 MB, throughput 0.806857
Reading from 12641: heap size 532 MB, throughput 0.981003
Reading from 12639: heap size 931 MB, throughput 0.842198
Reading from 12639: heap size 817 MB, throughput 0.834903
Reading from 12639: heap size 924 MB, throughput 0.84531
Reading from 12639: heap size 930 MB, throughput 0.831683
Reading from 12639: heap size 918 MB, throughput 0.855131
Reading from 12639: heap size 924 MB, throughput 0.851323
Equal recommendation: 3132 MB each
Reading from 12639: heap size 914 MB, throughput 0.931756
Reading from 12641: heap size 553 MB, throughput 0.995205
Reading from 12641: heap size 556 MB, throughput 0.983799
Reading from 12639: heap size 919 MB, throughput 0.983055
Reading from 12639: heap size 912 MB, throughput 0.876015
Reading from 12639: heap size 920 MB, throughput 0.793173
Reading from 12639: heap size 918 MB, throughput 0.801314
Reading from 12639: heap size 920 MB, throughput 0.799479
Reading from 12639: heap size 919 MB, throughput 0.716581
Reading from 12639: heap size 921 MB, throughput 0.699607
Reading from 12641: heap size 576 MB, throughput 0.986542
Reading from 12639: heap size 923 MB, throughput 0.766372
Reading from 12639: heap size 925 MB, throughput 0.792638
Reading from 12639: heap size 927 MB, throughput 0.901036
Reading from 12639: heap size 929 MB, throughput 0.872569
Reading from 12639: heap size 929 MB, throughput 0.882363
Reading from 12641: heap size 579 MB, throughput 0.993102
Reading from 12639: heap size 933 MB, throughput 0.738103
Reading from 12639: heap size 927 MB, throughput 0.666377
Reading from 12641: heap size 596 MB, throughput 0.980578
Reading from 12639: heap size 940 MB, throughput 0.0530525
Reading from 12639: heap size 1059 MB, throughput 0.498335
Reading from 12639: heap size 1061 MB, throughput 0.749654
Reading from 12639: heap size 1064 MB, throughput 0.752149
Reading from 12639: heap size 1066 MB, throughput 0.713747
Reading from 12639: heap size 1077 MB, throughput 0.725094
Reading from 12639: heap size 1078 MB, throughput 0.685043
Reading from 12641: heap size 600 MB, throughput 0.990368
Reading from 12639: heap size 1093 MB, throughput 0.645903
Reading from 12639: heap size 1094 MB, throughput 0.786152
Equal recommendation: 3132 MB each
Reading from 12641: heap size 618 MB, throughput 0.995485
Reading from 12639: heap size 1110 MB, throughput 0.958435
Reading from 12641: heap size 621 MB, throughput 0.987166
Reading from 12639: heap size 1114 MB, throughput 0.974717
Reading from 12641: heap size 639 MB, throughput 0.991151
Reading from 12641: heap size 640 MB, throughput 0.999999
Reading from 12641: heap size 640 MB, throughput 1
Reading from 12641: heap size 640 MB, throughput 0.0114533
Reading from 12639: heap size 1129 MB, throughput 0.967701
Reading from 12641: heap size 652 MB, throughput 0.989788
Equal recommendation: 3132 MB each
Reading from 12639: heap size 1131 MB, throughput 0.981806
Reading from 12641: heap size 655 MB, throughput 0.987833
Reading from 12641: heap size 674 MB, throughput 0.991039
Reading from 12639: heap size 1132 MB, throughput 0.976232
Reading from 12641: heap size 675 MB, throughput 0.995068
Reading from 12639: heap size 1138 MB, throughput 0.966789
Reading from 12641: heap size 687 MB, throughput 0.98894
Reading from 12641: heap size 690 MB, throughput 0.987546
Equal recommendation: 3132 MB each
Reading from 12639: heap size 1129 MB, throughput 0.97219
Reading from 12641: heap size 709 MB, throughput 0.993101
Reading from 12641: heap size 710 MB, throughput 0.99216
Reading from 12639: heap size 1136 MB, throughput 0.974924
Reading from 12641: heap size 725 MB, throughput 0.989944
Reading from 12639: heap size 1133 MB, throughput 0.970227
Reading from 12641: heap size 726 MB, throughput 0.987653
Equal recommendation: 3132 MB each
Reading from 12641: heap size 743 MB, throughput 0.989329
Reading from 12639: heap size 1136 MB, throughput 0.961428
Reading from 12641: heap size 745 MB, throughput 0.996287
Reading from 12639: heap size 1141 MB, throughput 0.97116
Reading from 12641: heap size 760 MB, throughput 0.991164
Reading from 12641: heap size 763 MB, throughput 0.995327
Reading from 12639: heap size 1141 MB, throughput 0.975882
Equal recommendation: 3132 MB each
Reading from 12641: heap size 775 MB, throughput 0.989351
Reading from 12639: heap size 1146 MB, throughput 0.972838
Reading from 12641: heap size 777 MB, throughput 0.989113
Reading from 12641: heap size 795 MB, throughput 0.989656
Reading from 12639: heap size 1149 MB, throughput 0.974791
Reading from 12641: heap size 795 MB, throughput 0.989568
Reading from 12639: heap size 1153 MB, throughput 0.970891
Equal recommendation: 3132 MB each
Reading from 12641: heap size 816 MB, throughput 0.991304
Reading from 12639: heap size 1157 MB, throughput 0.973185
Reading from 12641: heap size 816 MB, throughput 0.989009
Reading from 12641: heap size 835 MB, throughput 0.99321
Reading from 12639: heap size 1162 MB, throughput 0.971194
Reading from 12641: heap size 837 MB, throughput 0.992231
Reading from 12639: heap size 1167 MB, throughput 0.968898
Equal recommendation: 3132 MB each
Reading from 12641: heap size 853 MB, throughput 0.992381
Reading from 12639: heap size 1172 MB, throughput 0.96555
Reading from 12641: heap size 855 MB, throughput 0.991573
Reading from 12641: heap size 871 MB, throughput 0.991787
Reading from 12639: heap size 1175 MB, throughput 0.96915
Reading from 12641: heap size 873 MB, throughput 0.992195
Equal recommendation: 3132 MB each
Reading from 12639: heap size 1181 MB, throughput 0.965008
Reading from 12641: heap size 891 MB, throughput 0.992934
Reading from 12639: heap size 1182 MB, throughput 0.967804
Reading from 12641: heap size 892 MB, throughput 0.99122
Reading from 12641: heap size 910 MB, throughput 0.992444
Reading from 12639: heap size 1187 MB, throughput 0.956952
Equal recommendation: 3132 MB each
Reading from 12641: heap size 910 MB, throughput 0.991891
Reading from 12639: heap size 1188 MB, throughput 0.63295
Reading from 12641: heap size 928 MB, throughput 0.992853
Reading from 12641: heap size 929 MB, throughput 0.994654
Reading from 12639: heap size 1287 MB, throughput 0.95037
Equal recommendation: 3132 MB each
Reading from 12641: heap size 945 MB, throughput 0.992458
Reading from 12639: heap size 1287 MB, throughput 0.98375
Reading from 12641: heap size 948 MB, throughput 0.991703
Reading from 12639: heap size 1297 MB, throughput 0.979107
Reading from 12641: heap size 969 MB, throughput 0.991301
Reading from 12641: heap size 970 MB, throughput 0.990984
Reading from 12639: heap size 1299 MB, throughput 0.982726
Equal recommendation: 3132 MB each
Reading from 12641: heap size 993 MB, throughput 0.992763
Reading from 12639: heap size 1299 MB, throughput 0.974514
Reading from 12641: heap size 993 MB, throughput 0.992389
Reading from 12639: heap size 1302 MB, throughput 0.975039
Reading from 12641: heap size 1014 MB, throughput 0.989261
Equal recommendation: 3132 MB each
Reading from 12639: heap size 1292 MB, throughput 0.977609
Reading from 12641: heap size 1015 MB, throughput 0.99018
Reading from 12641: heap size 1040 MB, throughput 0.993613
Reading from 12639: heap size 1223 MB, throughput 0.978241
Reading from 12641: heap size 1040 MB, throughput 0.995562
Equal recommendation: 3132 MB each
Reading from 12639: heap size 1285 MB, throughput 0.979539
Reading from 12641: heap size 1059 MB, throughput 0.992891
Reading from 12639: heap size 1291 MB, throughput 0.976046
Reading from 12641: heap size 1061 MB, throughput 0.990159
Reading from 12639: heap size 1292 MB, throughput 0.97365
Reading from 12641: heap size 1083 MB, throughput 0.991251
Equal recommendation: 3132 MB each
Reading from 12639: heap size 1293 MB, throughput 0.970331
Reading from 12641: heap size 1083 MB, throughput 0.991762
Reading from 12641: heap size 1107 MB, throughput 0.993481
Reading from 12639: heap size 1296 MB, throughput 0.970816
Equal recommendation: 3132 MB each
Reading from 12641: heap size 1107 MB, throughput 0.988735
Reading from 12639: heap size 1300 MB, throughput 0.973676
Reading from 12641: heap size 1131 MB, throughput 0.992498
Reading from 12639: heap size 1304 MB, throughput 0.974745
Reading from 12641: heap size 1131 MB, throughput 0.993305
Equal recommendation: 3132 MB each
Reading from 12639: heap size 1310 MB, throughput 0.970888
Reading from 12641: heap size 1154 MB, throughput 0.990725
Reading from 12639: heap size 1317 MB, throughput 0.969733
Reading from 12641: heap size 1156 MB, throughput 0.992672
Reading from 12641: heap size 1180 MB, throughput 0.994088
Reading from 12639: heap size 1323 MB, throughput 0.972313
Equal recommendation: 3132 MB each
Reading from 12641: heap size 1181 MB, throughput 0.991015
Reading from 12641: heap size 1205 MB, throughput 0.991289
Reading from 12641: heap size 1206 MB, throughput 0.992892
Equal recommendation: 3132 MB each
Reading from 12641: heap size 1232 MB, throughput 0.988622
Reading from 12641: heap size 1233 MB, throughput 0.986563
Reading from 12639: heap size 1330 MB, throughput 0.861009
Equal recommendation: 3132 MB each
Reading from 12641: heap size 1264 MB, throughput 0.99032
Reading from 12641: heap size 1266 MB, throughput 0.990136
Reading from 12641: heap size 1303 MB, throughput 0.950939
Equal recommendation: 3132 MB each
Reading from 12641: heap size 1324 MB, throughput 0.99325
Reading from 12639: heap size 1459 MB, throughput 0.986496
Reading from 12641: heap size 1371 MB, throughput 0.987599
Equal recommendation: 3132 MB each
Reading from 12641: heap size 1370 MB, throughput 0.990615
Reading from 12639: heap size 1495 MB, throughput 0.986349
Reading from 12639: heap size 1499 MB, throughput 0.83903
Reading from 12639: heap size 1496 MB, throughput 0.727114
Reading from 12639: heap size 1516 MB, throughput 0.702902
Reading from 12639: heap size 1546 MB, throughput 0.723146
Reading from 12639: heap size 1567 MB, throughput 0.687547
Reading from 12639: heap size 1600 MB, throughput 0.736571
Reading from 12639: heap size 1611 MB, throughput 0.75533
Reading from 12641: heap size 1421 MB, throughput 0.989215
Reading from 12639: heap size 1631 MB, throughput 0.977975
Reading from 12641: heap size 1422 MB, throughput 0.988121
Equal recommendation: 3132 MB each
Reading from 12639: heap size 1641 MB, throughput 0.97778
Reading from 12641: heap size 1475 MB, throughput 0.991282
Client 12641 died
Clients: 1
Reading from 12639: heap size 1637 MB, throughput 0.985036
Recommendation: one client; give it all the memory
Reading from 12639: heap size 1650 MB, throughput 0.984914
Reading from 12639: heap size 1639 MB, throughput 0.985061
Recommendation: one client; give it all the memory
Reading from 12639: heap size 1652 MB, throughput 0.984528
Reading from 12639: heap size 1643 MB, throughput 0.877011
Recommendation: one client; give it all the memory
Reading from 12639: heap size 1600 MB, throughput 0.996327
Reading from 12639: heap size 1604 MB, throughput 0.995077
Recommendation: one client; give it all the memory
Reading from 12639: heap size 1616 MB, throughput 0.994151
Recommendation: one client; give it all the memory
Reading from 12639: heap size 1628 MB, throughput 0.992732
Reading from 12639: heap size 1631 MB, throughput 0.991983
Recommendation: one client; give it all the memory
Reading from 12639: heap size 1616 MB, throughput 0.990938
Recommendation: one client; give it all the memory
Reading from 12639: heap size 1432 MB, throughput 0.990216
Reading from 12639: heap size 1596 MB, throughput 0.989398
Recommendation: one client; give it all the memory
Reading from 12639: heap size 1462 MB, throughput 0.988125
Reading from 12639: heap size 1582 MB, throughput 0.987362
Recommendation: one client; give it all the memory
Reading from 12639: heap size 1592 MB, throughput 0.986151
Recommendation: one client; give it all the memory
Reading from 12639: heap size 1593 MB, throughput 0.98554
Reading from 12639: heap size 1593 MB, throughput 0.984308
Recommendation: one client; give it all the memory
Reading from 12639: heap size 1597 MB, throughput 0.983837
Recommendation: one client; give it all the memory
Reading from 12639: heap size 1603 MB, throughput 0.981679
Reading from 12639: heap size 1608 MB, throughput 0.980213
Recommendation: one client; give it all the memory
Reading from 12639: heap size 1619 MB, throughput 0.98042
Reading from 12639: heap size 1630 MB, throughput 0.980185
Recommendation: one client; give it all the memory
Reading from 12639: heap size 1636 MB, throughput 0.981314
Recommendation: one client; give it all the memory
Reading from 12639: heap size 1646 MB, throughput 0.981818
Recommendation: one client; give it all the memory
Reading from 12639: heap size 1650 MB, throughput 0.990279
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 12639: heap size 1647 MB, throughput 0.989973
Reading from 12639: heap size 1661 MB, throughput 0.978342
Reading from 12639: heap size 1670 MB, throughput 0.832213
Reading from 12639: heap size 1677 MB, throughput 0.766567
Reading from 12639: heap size 1706 MB, throughput 0.803678
Reading from 12639: heap size 1721 MB, throughput 0.844338
Client 12639 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
