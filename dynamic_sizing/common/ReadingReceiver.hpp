#ifndef READINGRECEIVER_HPP
#define READINGRECEIVER_HPP

#include "Messages.hpp"
#include "RecvSock.hpp"
#include "Utilities.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
//! Receive readings from a client (for use in the daemon).
    class ReadingReceiver : public RecvSock<economem::Reading>
    {
    public:
        ReadingReceiver(const char* daemonName=economem::NAME_DEFAULT);
    };
}

/*! @} */

#endif
