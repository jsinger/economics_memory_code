#ifndef AVERAGEDVENGEROV_HPP
#define AVERAGEDVENGEROV_HPP

#include "AdaptiveWeightedAverage.hpp"
#include "Stopwatch.hpp"
#include "ThroughputCalculator.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
//! The same as NaiveVengerov, but with some additional averaging.
    class AveragedVengerov : public ThroughputCalculator
    {
    public:
        AveragedVengerov();

        virtual void gcBegin();
        virtual ThroughputCalculator::Result minorGCEnd();
        virtual ThroughputCalculator::Result majorGCEnd();

    protected:
        //! Return the current heap size in bytes.
        virtual unsigned long long getHeapSizeB() = 0;

        //! Return the current young generation size in bytes (this is the _whole_ of the young gen, not just one region inside it).
        virtual unsigned long long getYoungGenSizeB() = 0;

        //! Return the current old generation size in bytes.
        virtual unsigned long long getOldGenSizeB() = 0;

        //! Use if you want to log the calculated throughputs in your client.
        virtual void logReading(unsigned long long heapSizeB, double throughput) = 0;

    private:
        economem::Stopwatch m_GCTimer; // From the beginning of a GC to its end
        economem::Stopwatch m_MajorToMajorTimer; // From the end of one major GC to the end of the next
        economem::Stopwatch m_EdenFillTimer; // From the end of a GC to the beginning of the next

        economem::AdaptiveWeightedAverage m_AvgInitialOldUsedB;
        unsigned long long m_MajorGCTime;
        unsigned long long m_MutatorTime;
        unsigned long long m_GCStartHeapSizeB;
        unsigned long long m_GCStartYoungSizeB;
        unsigned long long m_GCStartOldUsedB;

        economem::AdaptiveWeightedAverage m_AvgMinorGCTime;
        economem::AdaptiveWeightedAverage m_AvgEdenFillTime;
        economem::AdaptiveWeightedAverage m_AvgMinorPromoted;
    };
}

/*! @} */

#endif
