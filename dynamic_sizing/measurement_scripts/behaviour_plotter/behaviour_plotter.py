import economem.dacapo
import economem.reading_log
import economem.signals
import matplotlib.pyplot as plt

class DataLine(object):
    def __init__(self, colour, style, get_y, get_x=lambda e: e.timestamp, label=None):
        super(DataLine, self).__init__()
        self.colour = colour
        self.linestyle = style
        self.get_x = get_x
        self.get_y = get_y
        self.label = label
        self.x_data = []
        self.y_data = []
        self.line = None
        self.alive = True

    def add_point(self, event):
        if self.y_data and self.get_y(event) != self.y_data[-1]:
            self.x_data.append(self.get_x(event))
            self.y_data.append(self.y_data[-1])

        self.x_data.append(self.get_x(event))
        self.y_data.append(self.get_y(event))

    def set_dead(self, timestamp):
        if self.x_data and self.x_data[-1] != timestamp:
            self.x_data.append(timestamp)
            self.y_data.append(self.y_data[-1])
        self.alive = False

    def get_highest_value(self):
        if self.y_data:
            return max(self.y_data)
        else:
            return 0

    def draw(self, axes, latest_timestamp):
        if self.alive and self.x_data and latest_timestamp > self.x_data[-1]:
            x_data = self.x_data + [latest_timestamp]
            y_data = self.y_data + [self.y_data[-1]]
        else:
            x_data = self.x_data
            y_data = self.y_data

        if self.line:
            self.line.set_xdata(x_data)
            self.line.set_ydata(y_data)
        else:
            self.line = axes.plot(x_data, y_data, linestyle=self.linestyle, color=self.colour, label=self.label)[0]
            if self.label:
                axes.legend(loc="lower right")


class VerticalLine(object):
    def __init__(self, event, colour):
        super(VerticalLine, self).__init__()
        self.event = event
        self.colour = colour
        self.lines = []

    def draw(self, heap_axes, throughput_axes):
        if not self.lines:
            self.lines.append(heap_axes.axvline(self.event.timestamp, color=self.colour, linestyle="--"))
            self.lines.append(throughput_axes.axvline(self.event.timestamp, color=self.colour, linestyle="--"))


class Client(object):
    def __init__(self, pid, colour, pid_names):
        super(Client, self).__init__()
        self.pid = pid
        self.colour = colour
        self.current_heap = 0
        self.heaps = DataLine(self.colour, "-", lambda e: e.heap_size_MB)
        self.throughputs = DataLine(self.colour, "-", lambda e: e.throughput, label=pid_names.get_name(self.pid))
        self.recommendations = DataLine(self.colour, ":", lambda e: e.heap_size_MB)
        self.min_heaps = DataLine(self.colour, ":", lambda e: e.heap_size_MB)
        self.creation = None
        self.death = None

    def current_mem(self):
        return self.current_heap

    def add_creation(self, e):
        self.creation = VerticalLine(e, self.colour)

    def add_reading(self, e):
        self.current_heap = e.heap_size_MB
        self.heaps.add_point(e)
        self.throughputs.add_point(e)

    def add_min_heap(self, e):
        self.min_heaps.add_point(e)

    def add_recommendation(self, e):
        self.recommendations.add_point(e)

    def add_death(self, e):
        self.death = VerticalLine(e, self.colour)
        self.heaps.set_dead(e.timestamp)
        self.throughputs.set_dead(e.timestamp)
        self.recommendations.set_dead(e.timestamp)
        self.min_heaps.set_dead(e.timestamp)


    def draw(self, heap_axes, throughput_axes, latest_timestamp):
        if self.creation:
            self.creation.draw(heap_axes, throughput_axes)

        if self.death:
            self.death.draw(heap_axes, throughput_axes)

        self.recommendations.draw(heap_axes, latest_timestamp)
        self.min_heaps.draw(heap_axes, latest_timestamp)
        self.heaps.draw(heap_axes, latest_timestamp)
        self.throughputs.draw(throughput_axes, 0)


class Recommendation(object):
    def __init__(self, e):
        super(Recommendation, self).__init__()
        self.event = e
        self.labels = []

    def draw(self, heap_axes, throughput_axes):
        if not self.labels:
            self.labels.append(heap_axes.annotate("R", (self.event.timestamp, 0)))
            self.labels.append(throughput_axes.annotate("R", (self.event.timestamp, 0)))

class BehaviourPlotter(object):
    def __init__(self, throughput_model, input_stream, pid_names_file, guess_pids, draw_Rs, title, greyscale):
        super(BehaviourPlotter, self).__init__()
        self.input_stream = input_stream
        self.pid_names = economem.dacapo.PidNames(pid_names_file, guess_pids)
        self.draw_Rs = draw_Rs
        if greyscale:
            self.colours = ["0.8", "0.65", "0.5", "0.35", "0.3", "0.2"]
        else:
            self.colours = ["r", "g", "b", "c", "m", "y"]
        self.current_colour = 0
        self.throughput_model = throughput_model
        self.title = title
        self.latest_timestamp = 0
        self.max_heap_y = 1
        self.clients = {}
        self.recommendations = []
        self.total_mem = DataLine("k", ":", lambda e: e.total_mem_MB)
        self.used_mem = DataLine("k", "--", lambda x: x[1], lambda x: x[0])
        self.heap_axes = plt.subplot(211)
        self.throughput_axes = plt.subplot(212)
        self.heap_axes.set_xlabel("Time (s)")
        self.heap_axes.set_ylabel("Heap size (MB)")
        self.throughput_axes.set_xlabel("Time (s)")
        self.throughput_axes.set_ylabel("Throughput")
        self.throughput_axes.set_ylim(0, 1.1)
        self.set_title()
        economem.signals.exit_on_signal()


    def next_colour(self):
        colour = self.colours[self.current_colour]
        self.current_colour = (self.current_colour + 1) % len(self.colours)
        return colour

    def update_max_x(self, x):
        self.heap_axes.set_xlim(0, max(x*1.1, 1))
        self.throughput_axes.set_xlim(0, max(x*1.1, 1))

    def update_max_heap_y(self, y):
        if y > self.max_heap_y:
            self.max_heap_y = y
            self.heap_axes.set_ylim((0, 1.1 * self.max_heap_y))

    def begin(self):
        pass

    def end_iteration(self):
        pass

    def end(self):
        pass

    def set_title(self):
        if self.title:
            self.heap_axes.set_title(self.title)
        else:
            self.heap_axes.set_title("Throughput model: %s" % self.throughput_model)

    def get_client(self, event):
        if event.pid == 0:
            return None
        elif event.pid in self.clients:
            return self.clients[event.pid]
        else:
            client = Client(event.pid, self.next_colour(), self.pid_names)
            self.clients[event.pid] = client

            if not isinstance(event, economem.reading_log.Creation):
                client.add_creation(economem.reading_log.Creation(event.timestamp, event.pid))

            return client

    def plot(self):
        """Plot readings"""

        got_first_time = False
        first_time = 0
        used_mem = 0
        self.begin()

        while True:
            # Have to do it this way because 'for line in self.input_stream' is buffered
            # and unresponsive in the interactive case
            line = self.input_stream.readline()
            if not line:
                break

            event = economem.reading_log.event_from_str(line)

            if not got_first_time:
                first_time = event.timestamp
                got_first_time = True

            event.timestamp -= first_time
            event.timestamp /= 1000.0
            self.latest_timestamp = event.timestamp
            self.update_max_x(event.timestamp)

            client = self.get_client(event)

            if isinstance(event, economem.reading_log.Creation):
                client.add_creation(event)
            elif isinstance(event, economem.reading_log.Reading):
                used_mem -= client.current_mem()
                client.add_reading(event)
                used_mem += client.current_mem()
                self.used_mem.add_point((event.timestamp, used_mem))
                self.update_max_heap_y(event.heap_size_MB)
            elif isinstance(event, economem.reading_log.MinHeap):
                self.update_max_heap_y(event.heap_size_MB)
                client.add_min_heap(event)
            elif isinstance(event, economem.reading_log.Recommendation):
                if (not self.recommendations or (self.recommendations[-1].event.timestamp != event.timestamp)) and self.draw_Rs:
                    self.recommendations.append(Recommendation(event))
                self.update_max_heap_y(event.heap_size_MB)
                client.add_recommendation(event)
            elif isinstance(event, economem.reading_log.Death):
                used_mem -= client.current_mem()
                client.add_death(event)
                self.used_mem.add_point((event.timestamp, used_mem))
            elif isinstance(event, economem.reading_log.TotalMem):
                self.total_mem.add_point(event)
                self.update_max_heap_y(event.total_mem_MB)

            self.end_iteration()

        self.end()

    def draw_clients(self):
        self.total_mem.draw(self.heap_axes, self.latest_timestamp)
        self.used_mem.draw(self.heap_axes, self.latest_timestamp)

        for rec in self.recommendations:
            rec.draw(self.heap_axes, self.throughput_axes)

        for pid in self.clients:
            self.clients[pid].draw(self.heap_axes, self.throughput_axes, self.latest_timestamp)
