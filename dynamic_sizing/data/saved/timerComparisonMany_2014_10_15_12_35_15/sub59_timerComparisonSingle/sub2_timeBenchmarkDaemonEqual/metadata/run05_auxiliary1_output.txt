economemd
    total memory: 1440 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub59_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run05_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 15860: heap size 9 MB, throughput 0.986978
Clients: 1
Client 15860 has a minimum heap size of 12 MB
Reading from 15858: heap size 9 MB, throughput 0.989917
Clients: 2
Client 15858 has a minimum heap size of 276 MB
Reading from 15858: heap size 9 MB, throughput 0.961803
Reading from 15860: heap size 9 MB, throughput 0.989571
Reading from 15858: heap size 9 MB, throughput 0.963053
Reading from 15858: heap size 9 MB, throughput 0.957253
Reading from 15860: heap size 11 MB, throughput 0.984098
Reading from 15858: heap size 11 MB, throughput 0.980894
Reading from 15860: heap size 11 MB, throughput 0.980119
Reading from 15858: heap size 11 MB, throughput 0.951432
Reading from 15858: heap size 17 MB, throughput 0.883984
Reading from 15860: heap size 15 MB, throughput 0.984407
Reading from 15858: heap size 17 MB, throughput 0.683992
Reading from 15860: heap size 15 MB, throughput 0.981944
Reading from 15858: heap size 30 MB, throughput 0.941866
Reading from 15858: heap size 31 MB, throughput 0.954783
Reading from 15860: heap size 24 MB, throughput 0.909369
Reading from 15858: heap size 34 MB, throughput 0.544025
Reading from 15858: heap size 48 MB, throughput 0.760466
Reading from 15858: heap size 51 MB, throughput 0.894823
Reading from 15858: heap size 52 MB, throughput 0.898199
Reading from 15860: heap size 29 MB, throughput 0.963043
Reading from 15858: heap size 57 MB, throughput 0.227772
Reading from 15858: heap size 81 MB, throughput 0.79193
Reading from 15860: heap size 36 MB, throughput 0.987985
Reading from 15858: heap size 85 MB, throughput 0.873708
Reading from 15858: heap size 87 MB, throughput 0.231721
Reading from 15860: heap size 41 MB, throughput 0.982533
Reading from 15858: heap size 113 MB, throughput 0.715922
Reading from 15858: heap size 115 MB, throughput 0.848993
Reading from 15860: heap size 42 MB, throughput 0.988982
Reading from 15858: heap size 117 MB, throughput 0.825523
Reading from 15860: heap size 45 MB, throughput 0.951289
Reading from 15858: heap size 121 MB, throughput 0.790068
Reading from 15860: heap size 51 MB, throughput 0.985174
Reading from 15858: heap size 124 MB, throughput 0.155249
Reading from 15860: heap size 51 MB, throughput 0.983203
Reading from 15858: heap size 156 MB, throughput 0.567875
Reading from 15858: heap size 160 MB, throughput 0.660364
Reading from 15860: heap size 58 MB, throughput 0.978483
Reading from 15858: heap size 163 MB, throughput 0.651784
Reading from 15860: heap size 59 MB, throughput 0.969659
Reading from 15858: heap size 166 MB, throughput 0.555893
Reading from 15860: heap size 69 MB, throughput 0.617849
Reading from 15860: heap size 73 MB, throughput 0.990255
Reading from 15858: heap size 172 MB, throughput 0.135143
Reading from 15858: heap size 209 MB, throughput 0.699087
Reading from 15858: heap size 213 MB, throughput 0.839382
Reading from 15860: heap size 85 MB, throughput 0.99474
Reading from 15858: heap size 215 MB, throughput 0.962893
Reading from 15860: heap size 85 MB, throughput 0.994552
Reading from 15858: heap size 215 MB, throughput 0.86161
Reading from 15858: heap size 221 MB, throughput 0.659947
Reading from 15858: heap size 226 MB, throughput 0.746118
Reading from 15858: heap size 231 MB, throughput 0.437494
Reading from 15858: heap size 235 MB, throughput 0.673923
Reading from 15860: heap size 95 MB, throughput 0.995592
Reading from 15860: heap size 97 MB, throughput 0.995572
Reading from 15858: heap size 243 MB, throughput 0.149809
Reading from 15858: heap size 280 MB, throughput 0.774257
Reading from 15858: heap size 278 MB, throughput 0.805048
Reading from 15858: heap size 281 MB, throughput 0.709324
Reading from 15858: heap size 283 MB, throughput 0.708858
Reading from 15858: heap size 284 MB, throughput 0.816362
Reading from 15860: heap size 105 MB, throughput 0.995859
Reading from 15858: heap size 286 MB, throughput 0.881573
Reading from 15858: heap size 288 MB, throughput 0.932377
Reading from 15858: heap size 289 MB, throughput 0.751845
Reading from 15860: heap size 107 MB, throughput 0.994819
Reading from 15858: heap size 291 MB, throughput 0.185167
Reading from 15858: heap size 335 MB, throughput 0.714778
Reading from 15858: heap size 335 MB, throughput 0.942855
Reading from 15860: heap size 115 MB, throughput 0.990085
Reading from 15858: heap size 342 MB, throughput 0.888756
Reading from 15858: heap size 343 MB, throughput 0.823576
Reading from 15858: heap size 347 MB, throughput 0.702623
Reading from 15858: heap size 347 MB, throughput 0.699476
Reading from 15858: heap size 354 MB, throughput 0.711436
Reading from 15858: heap size 355 MB, throughput 0.672776
Equal recommendation: 720 MB each
Reading from 15860: heap size 116 MB, throughput 0.995162
Reading from 15860: heap size 121 MB, throughput 0.995754
Reading from 15858: heap size 364 MB, throughput 0.962431
Reading from 15860: heap size 123 MB, throughput 0.995031
Reading from 15858: heap size 366 MB, throughput 0.984516
Reading from 15860: heap size 129 MB, throughput 0.996374
Reading from 15860: heap size 129 MB, throughput 0.997192
Reading from 15858: heap size 368 MB, throughput 0.975377
Reading from 15860: heap size 133 MB, throughput 0.997064
Reading from 15860: heap size 134 MB, throughput 0.995716
Reading from 15858: heap size 371 MB, throughput 0.982167
Reading from 15860: heap size 139 MB, throughput 0.997287
Reading from 15860: heap size 139 MB, throughput 0.997126
Reading from 15858: heap size 370 MB, throughput 0.97906
Reading from 15860: heap size 141 MB, throughput 0.997319
Reading from 15858: heap size 373 MB, throughput 0.983018
Reading from 15860: heap size 142 MB, throughput 0.998029
Reading from 15860: heap size 145 MB, throughput 0.998656
Equal recommendation: 720 MB each
Reading from 15858: heap size 374 MB, throughput 0.983515
Reading from 15860: heap size 145 MB, throughput 0.99829
Reading from 15860: heap size 147 MB, throughput 0.998537
Reading from 15860: heap size 148 MB, throughput 0.998131
Reading from 15858: heap size 376 MB, throughput 0.985054
Reading from 15860: heap size 150 MB, throughput 0.997943
Reading from 15860: heap size 150 MB, throughput 0.991127
Reading from 15860: heap size 151 MB, throughput 0.9934
Reading from 15858: heap size 374 MB, throughput 0.968108
Reading from 15860: heap size 152 MB, throughput 0.99631
Reading from 15858: heap size 376 MB, throughput 0.97674
Reading from 15860: heap size 157 MB, throughput 0.997756
Reading from 15860: heap size 157 MB, throughput 0.997208
Reading from 15858: heap size 378 MB, throughput 0.976762
Reading from 15860: heap size 160 MB, throughput 0.997867
Reading from 15860: heap size 161 MB, throughput 0.99729
Reading from 15858: heap size 379 MB, throughput 0.98126
Equal recommendation: 720 MB each
Reading from 15860: heap size 164 MB, throughput 0.997289
Reading from 15858: heap size 381 MB, throughput 0.973543
Reading from 15860: heap size 164 MB, throughput 0.996779
Reading from 15860: heap size 167 MB, throughput 0.997875
Reading from 15858: heap size 383 MB, throughput 0.983006
Reading from 15860: heap size 167 MB, throughput 0.997389
Reading from 15858: heap size 384 MB, throughput 0.959738
Reading from 15858: heap size 386 MB, throughput 0.827402
Reading from 15858: heap size 388 MB, throughput 0.832908
Reading from 15858: heap size 391 MB, throughput 0.826656
Reading from 15860: heap size 170 MB, throughput 0.997256
Reading from 15858: heap size 399 MB, throughput 0.978271
Reading from 15860: heap size 170 MB, throughput 0.997133
Reading from 15858: heap size 400 MB, throughput 0.989782
Reading from 15860: heap size 173 MB, throughput 0.992887
Reading from 15860: heap size 173 MB, throughput 0.997427
Reading from 15858: heap size 403 MB, throughput 0.988511
Reading from 15860: heap size 177 MB, throughput 0.99705
Equal recommendation: 720 MB each
Reading from 15858: heap size 405 MB, throughput 0.988748
Reading from 15860: heap size 177 MB, throughput 0.997972
Reading from 15860: heap size 180 MB, throughput 0.998834
Reading from 15860: heap size 180 MB, throughput 0.996004
Reading from 15858: heap size 404 MB, throughput 0.981785
Reading from 15860: heap size 183 MB, throughput 0.992948
Reading from 15860: heap size 183 MB, throughput 0.996906
Reading from 15858: heap size 407 MB, throughput 0.987815
Reading from 15860: heap size 187 MB, throughput 0.998524
Reading from 15860: heap size 187 MB, throughput 0.99749
Reading from 15858: heap size 405 MB, throughput 0.986536
Reading from 15860: heap size 191 MB, throughput 0.997989
Reading from 15858: heap size 407 MB, throughput 0.985913
Reading from 15860: heap size 191 MB, throughput 0.997121
Reading from 15860: heap size 194 MB, throughput 0.998335
Reading from 15858: heap size 403 MB, throughput 0.986744
Equal recommendation: 720 MB each
Reading from 15860: heap size 194 MB, throughput 0.998046
Reading from 15858: heap size 405 MB, throughput 0.987206
Reading from 15860: heap size 197 MB, throughput 0.997969
Reading from 15860: heap size 197 MB, throughput 0.995882
Reading from 15858: heap size 403 MB, throughput 0.978808
Reading from 15860: heap size 199 MB, throughput 0.997984
Reading from 15858: heap size 405 MB, throughput 0.992818
Reading from 15858: heap size 406 MB, throughput 0.935679
Reading from 15860: heap size 199 MB, throughput 0.997653
Reading from 15858: heap size 406 MB, throughput 0.874885
Reading from 15858: heap size 410 MB, throughput 0.863122
Reading from 15858: heap size 411 MB, throughput 0.983474
Reading from 15860: heap size 202 MB, throughput 0.997561
Reading from 15860: heap size 202 MB, throughput 0.997502
Reading from 15858: heap size 417 MB, throughput 0.992883
Reading from 15860: heap size 204 MB, throughput 0.997235
Reading from 15860: heap size 205 MB, throughput 0.992791
Equal recommendation: 720 MB each
Reading from 15860: heap size 208 MB, throughput 0.997364
Reading from 15858: heap size 418 MB, throughput 0.990027
Reading from 15860: heap size 208 MB, throughput 0.997794
Reading from 15858: heap size 420 MB, throughput 0.809915
Reading from 15860: heap size 211 MB, throughput 0.997774
Reading from 15860: heap size 212 MB, throughput 0.997864
Reading from 15858: heap size 446 MB, throughput 0.997133
Reading from 15860: heap size 215 MB, throughput 0.998174
Reading from 15860: heap size 205 MB, throughput 0.997849
Reading from 15858: heap size 446 MB, throughput 0.99487
Reading from 15860: heap size 196 MB, throughput 0.996689
Equal recommendation: 720 MB each
Reading from 15860: heap size 187 MB, throughput 0.996544
Reading from 15858: heap size 449 MB, throughput 0.994729
Reading from 15860: heap size 179 MB, throughput 0.997079
Reading from 15860: heap size 172 MB, throughput 0.99775
Reading from 15858: heap size 450 MB, throughput 0.991612
Reading from 15860: heap size 165 MB, throughput 0.997477
Reading from 15860: heap size 158 MB, throughput 0.997287
Reading from 15858: heap size 451 MB, throughput 0.991995
Reading from 15860: heap size 151 MB, throughput 0.997492
Reading from 15860: heap size 145 MB, throughput 0.996971
Reading from 15858: heap size 446 MB, throughput 0.977791
Reading from 15860: heap size 139 MB, throughput 0.888747
Reading from 15860: heap size 144 MB, throughput 0.992271
Reading from 15860: heap size 149 MB, throughput 0.991325
Reading from 15860: heap size 156 MB, throughput 0.997197
Reading from 15858: heap size 449 MB, throughput 0.988789
Reading from 15858: heap size 447 MB, throughput 0.908867
Reading from 15858: heap size 449 MB, throughput 0.856778
Reading from 15858: heap size 456 MB, throughput 0.968306
Reading from 15860: heap size 161 MB, throughput 0.997747
Equal recommendation: 720 MB each
Reading from 15860: heap size 166 MB, throughput 0.997985
Reading from 15858: heap size 460 MB, throughput 0.99226
Reading from 15860: heap size 171 MB, throughput 0.998036
Reading from 15860: heap size 176 MB, throughput 0.998141
Reading from 15860: heap size 180 MB, throughput 0.99756
Reading from 15858: heap size 457 MB, throughput 0.992866
Reading from 15860: heap size 185 MB, throughput 0.998078
Reading from 15860: heap size 189 MB, throughput 0.997379
Reading from 15858: heap size 461 MB, throughput 0.989588
Reading from 15860: heap size 192 MB, throughput 0.995334
Reading from 15858: heap size 461 MB, throughput 0.990255
Reading from 15860: heap size 195 MB, throughput 0.9982
Reading from 15860: heap size 194 MB, throughput 0.997886
Equal recommendation: 720 MB each
Reading from 15860: heap size 199 MB, throughput 0.996349
Reading from 15858: heap size 463 MB, throughput 0.990558
Reading from 15860: heap size 199 MB, throughput 0.997767
Reading from 15858: heap size 466 MB, throughput 0.987752
Reading from 15860: heap size 203 MB, throughput 0.997733
Reading from 15860: heap size 203 MB, throughput 0.995998
Reading from 15860: heap size 207 MB, throughput 0.992221
Reading from 15858: heap size 467 MB, throughput 0.986808
Reading from 15860: heap size 208 MB, throughput 0.996298
Reading from 15860: heap size 215 MB, throughput 0.998122
Reading from 15858: heap size 470 MB, throughput 0.985671
Reading from 15860: heap size 215 MB, throughput 0.99801
Reading from 15860: heap size 221 MB, throughput 0.998049
Equal recommendation: 720 MB each
Reading from 15858: heap size 471 MB, throughput 0.991983
Reading from 15858: heap size 473 MB, throughput 0.922544
Reading from 15858: heap size 474 MB, throughput 0.88935
Reading from 15858: heap size 480 MB, throughput 0.986321
Reading from 15860: heap size 221 MB, throughput 0.996953
Reading from 15860: heap size 226 MB, throughput 0.997768
Reading from 15860: heap size 226 MB, throughput 0.99789
Reading from 15858: heap size 481 MB, throughput 0.99466
Reading from 15860: heap size 231 MB, throughput 0.997465
Reading from 15858: heap size 484 MB, throughput 0.991213
Reading from 15860: heap size 231 MB, throughput 0.997882
Reading from 15860: heap size 236 MB, throughput 0.998226
Reading from 15858: heap size 486 MB, throughput 0.992955
Equal recommendation: 720 MB each
Reading from 15860: heap size 236 MB, throughput 0.997964
Reading from 15860: heap size 240 MB, throughput 0.996271
Reading from 15860: heap size 241 MB, throughput 0.991002
Reading from 15858: heap size 484 MB, throughput 0.992178
Reading from 15860: heap size 246 MB, throughput 0.997742
Reading from 15860: heap size 246 MB, throughput 0.998283
Reading from 15858: heap size 487 MB, throughput 0.990819
Reading from 15860: heap size 252 MB, throughput 0.998206
Reading from 15858: heap size 485 MB, throughput 0.989377
Reading from 15860: heap size 253 MB, throughput 0.998088
Reading from 15860: heap size 259 MB, throughput 0.998345
Equal recommendation: 720 MB each
Reading from 15858: heap size 487 MB, throughput 0.989727
Reading from 15860: heap size 259 MB, throughput 0.997996
Reading from 15858: heap size 488 MB, throughput 0.99279
Reading from 15858: heap size 489 MB, throughput 0.924827
Reading from 15860: heap size 264 MB, throughput 0.998568
Reading from 15858: heap size 490 MB, throughput 0.917674
Reading from 15860: heap size 264 MB, throughput 0.998083
Reading from 15858: heap size 492 MB, throughput 0.992485
Reading from 15860: heap size 268 MB, throughput 0.997186
Reading from 15858: heap size 498 MB, throughput 0.994213
Reading from 15860: heap size 269 MB, throughput 0.998083
Reading from 15860: heap size 273 MB, throughput 0.9917
Equal recommendation: 720 MB each
Reading from 15860: heap size 274 MB, throughput 0.997051
Reading from 15858: heap size 499 MB, throughput 0.993772
Reading from 15860: heap size 283 MB, throughput 0.998024
Reading from 15858: heap size 499 MB, throughput 0.99226
Reading from 15860: heap size 283 MB, throughput 0.997635
Reading from 15860: heap size 289 MB, throughput 0.998358
Reading from 15858: heap size 501 MB, throughput 0.990275
Reading from 15860: heap size 290 MB, throughput 0.997068
Reading from 15860: heap size 296 MB, throughput 0.998812
Equal recommendation: 720 MB each
Reading from 15858: heap size 500 MB, throughput 0.990573
Reading from 15860: heap size 296 MB, throughput 0.998086
Reading from 15858: heap size 502 MB, throughput 0.990108
Reading from 15860: heap size 300 MB, throughput 0.998409
Reading from 15858: heap size 504 MB, throughput 0.993272
Reading from 15860: heap size 301 MB, throughput 0.998256
Reading from 15858: heap size 505 MB, throughput 0.890142
Reading from 15858: heap size 505 MB, throughput 0.841838
Reading from 15860: heap size 307 MB, throughput 0.909523
Reading from 15860: heap size 315 MB, throughput 0.997908
Reading from 15858: heap size 507 MB, throughput 0.994805
Equal recommendation: 720 MB each
Reading from 15860: heap size 325 MB, throughput 0.999118
Reading from 15858: heap size 516 MB, throughput 0.99424
Reading from 15860: heap size 325 MB, throughput 0.99893
Reading from 15860: heap size 332 MB, throughput 0.999131
Reading from 15858: heap size 518 MB, throughput 0.992523
Reading from 15860: heap size 333 MB, throughput 0.998882
Reading from 15858: heap size 520 MB, throughput 0.992221
Reading from 15860: heap size 339 MB, throughput 0.999057
Equal recommendation: 720 MB each
Reading from 15860: heap size 340 MB, throughput 0.998451
Reading from 15858: heap size 523 MB, throughput 0.992428
Reading from 15860: heap size 345 MB, throughput 0.997332
Reading from 15860: heap size 345 MB, throughput 0.992455
Reading from 15858: heap size 524 MB, throughput 0.99112
Reading from 15860: heap size 351 MB, throughput 0.998503
Reading from 15858: heap size 525 MB, throughput 0.995272
Reading from 15858: heap size 527 MB, throughput 0.952029
Reading from 15860: heap size 352 MB, throughput 0.998304
Reading from 15858: heap size 529 MB, throughput 0.915164
Equal recommendation: 720 MB each
Reading from 15860: heap size 360 MB, throughput 0.997992
Reading from 15858: heap size 535 MB, throughput 0.993376
Reading from 15860: heap size 362 MB, throughput 0.998425
Reading from 15858: heap size 537 MB, throughput 0.993713
Reading from 15860: heap size 370 MB, throughput 0.998398
Reading from 15860: heap size 371 MB, throughput 0.99839
Reading from 15858: heap size 538 MB, throughput 0.993709
Equal recommendation: 720 MB each
Reading from 15860: heap size 377 MB, throughput 0.998528
Reading from 15860: heap size 378 MB, throughput 0.993532
Reading from 15858: heap size 541 MB, throughput 0.99299
Reading from 15860: heap size 385 MB, throughput 0.998591
Reading from 15858: heap size 539 MB, throughput 0.992274
Reading from 15860: heap size 386 MB, throughput 0.997652
Reading from 15858: heap size 542 MB, throughput 0.990014
Reading from 15860: heap size 396 MB, throughput 0.998382
Reading from 15858: heap size 545 MB, throughput 0.989943
Reading from 15858: heap size 546 MB, throughput 0.927703
Equal recommendation: 720 MB each
Reading from 15860: heap size 396 MB, throughput 0.998261
Reading from 15858: heap size 550 MB, throughput 0.993392
Reading from 15860: heap size 404 MB, throughput 0.998528
Reading from 15858: heap size 552 MB, throughput 0.994554
Reading from 15860: heap size 405 MB, throughput 0.998222
Reading from 15860: heap size 412 MB, throughput 0.996962
Reading from 15858: heap size 554 MB, throughput 0.994826
Reading from 15860: heap size 413 MB, throughput 0.997951
Equal recommendation: 720 MB each
Reading from 15860: heap size 423 MB, throughput 0.998682
Reading from 15858: heap size 556 MB, throughput 0.994127
Reading from 15860: heap size 424 MB, throughput 0.998576
Reading from 15858: heap size 553 MB, throughput 0.992578
Reading from 15860: heap size 431 MB, throughput 0.998487
Equal recommendation: 720 MB each
Reading from 15858: heap size 556 MB, throughput 0.99101
Reading from 15860: heap size 432 MB, throughput 0.998351
Reading from 15858: heap size 558 MB, throughput 0.990129
Reading from 15858: heap size 559 MB, throughput 0.929675
Reading from 15860: heap size 440 MB, throughput 0.998126
Reading from 15858: heap size 562 MB, throughput 0.990722
Reading from 15860: heap size 440 MB, throughput 0.994738
Reading from 15860: heap size 447 MB, throughput 0.998181
Reading from 15858: heap size 564 MB, throughput 0.994759
Equal recommendation: 720 MB each
Reading from 15860: heap size 448 MB, throughput 0.998602
Reading from 15858: heap size 566 MB, throughput 0.994487
Reading from 15860: heap size 457 MB, throughput 0.998691
Reading from 15858: heap size 568 MB, throughput 0.993365
Reading from 15860: heap size 458 MB, throughput 0.998302
Reading from 15858: heap size 566 MB, throughput 0.992838
Equal recommendation: 720 MB each
Reading from 15860: heap size 466 MB, throughput 0.998598
Reading from 15860: heap size 467 MB, throughput 0.996079
Reading from 15858: heap size 568 MB, throughput 0.991137
Reading from 15858: heap size 571 MB, throughput 0.973942
Reading from 15858: heap size 571 MB, throughput 0.940976
Reading from 15860: heap size 475 MB, throughput 0.997515
Reading from 15858: heap size 577 MB, throughput 0.994373
Reading from 15860: heap size 476 MB, throughput 0.998642
Equal recommendation: 720 MB each
Reading from 15860: heap size 486 MB, throughput 0.998701
Reading from 15858: heap size 577 MB, throughput 0.994878
Reading from 15860: heap size 487 MB, throughput 0.998396
Reading from 15858: heap size 580 MB, throughput 0.994214
Reading from 15860: heap size 497 MB, throughput 0.998784
Reading from 15860: heap size 497 MB, throughput 0.996316
Reading from 15858: heap size 581 MB, throughput 0.993204
Equal recommendation: 720 MB each
Reading from 15860: heap size 496 MB, throughput 0.998468
Reading from 15858: heap size 581 MB, throughput 0.99245
Reading from 15860: heap size 498 MB, throughput 0.99866
Reading from 15858: heap size 582 MB, throughput 0.987866
Reading from 15858: heap size 585 MB, throughput 0.929581
Reading from 15860: heap size 497 MB, throughput 0.998737
Reading from 15858: heap size 587 MB, throughput 0.994477
Equal recommendation: 720 MB each
Reading from 15860: heap size 498 MB, throughput 0.998541
Reading from 15858: heap size 593 MB, throughput 0.995379
Reading from 15860: heap size 498 MB, throughput 0.998332
Reading from 15858: heap size 595 MB, throughput 0.994264
Client 15860 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 15858: heap size 595 MB, throughput 0.994142
Reading from 15858: heap size 597 MB, throughput 0.992351
Reading from 15858: heap size 599 MB, throughput 0.991038
Reading from 15858: heap size 600 MB, throughput 0.935988
Recommendation: one client; give it all the memory
Reading from 15858: heap size 603 MB, throughput 0.994959
Reading from 15858: heap size 605 MB, throughput 0.995231
Reading from 15858: heap size 607 MB, throughput 0.994191
Recommendation: one client; give it all the memory
Reading from 15858: heap size 609 MB, throughput 0.99337
Reading from 15858: heap size 610 MB, throughput 0.995436
Reading from 15858: heap size 611 MB, throughput 0.978174
Client 15858 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
