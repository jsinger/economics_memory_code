economemd
    total memory: 1728 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub60_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 19067: heap size 9 MB, throughput 0.990939
Clients: 1
Client 19067 has a minimum heap size of 12 MB
Reading from 19065: heap size 9 MB, throughput 0.992127
Clients: 2
Client 19065 has a minimum heap size of 276 MB
Reading from 19065: heap size 9 MB, throughput 0.962397
Reading from 19067: heap size 9 MB, throughput 0.985219
Reading from 19065: heap size 9 MB, throughput 0.933548
Reading from 19067: heap size 9 MB, throughput 0.963791
Reading from 19065: heap size 9 MB, throughput 0.971671
Reading from 19067: heap size 9 MB, throughput 0.979977
Reading from 19065: heap size 11 MB, throughput 0.986702
Reading from 19067: heap size 11 MB, throughput 0.976895
Reading from 19067: heap size 11 MB, throughput 0.97519
Reading from 19065: heap size 11 MB, throughput 0.979722
Reading from 19067: heap size 16 MB, throughput 0.985774
Reading from 19065: heap size 17 MB, throughput 0.941866
Reading from 19065: heap size 17 MB, throughput 0.619893
Reading from 19067: heap size 16 MB, throughput 0.984275
Reading from 19065: heap size 30 MB, throughput 0.94874
Reading from 19065: heap size 31 MB, throughput 0.830432
Reading from 19067: heap size 23 MB, throughput 0.973596
Reading from 19065: heap size 36 MB, throughput 0.417264
Reading from 19065: heap size 45 MB, throughput 0.842867
Reading from 19067: heap size 24 MB, throughput 0.821238
Reading from 19065: heap size 50 MB, throughput 0.344185
Reading from 19065: heap size 64 MB, throughput 0.817003
Reading from 19065: heap size 71 MB, throughput 0.775174
Reading from 19067: heap size 39 MB, throughput 0.991889
Reading from 19067: heap size 40 MB, throughput 0.836686
Reading from 19065: heap size 73 MB, throughput 0.220491
Reading from 19065: heap size 99 MB, throughput 0.730409
Reading from 19067: heap size 45 MB, throughput 0.989954
Reading from 19065: heap size 101 MB, throughput 0.877807
Reading from 19065: heap size 103 MB, throughput 0.632878
Reading from 19067: heap size 45 MB, throughput 0.981596
Reading from 19065: heap size 108 MB, throughput 0.665677
Reading from 19067: heap size 54 MB, throughput 0.988307
Reading from 19065: heap size 111 MB, throughput 0.183399
Reading from 19067: heap size 55 MB, throughput 0.989646
Reading from 19065: heap size 139 MB, throughput 0.518375
Reading from 19065: heap size 150 MB, throughput 0.719934
Reading from 19067: heap size 63 MB, throughput 0.990536
Reading from 19065: heap size 151 MB, throughput 0.632395
Reading from 19065: heap size 155 MB, throughput 0.716322
Reading from 19067: heap size 65 MB, throughput 0.980042
Reading from 19065: heap size 160 MB, throughput 0.574221
Reading from 19067: heap size 72 MB, throughput 0.986379
Reading from 19067: heap size 72 MB, throughput 0.982356
Reading from 19065: heap size 163 MB, throughput 0.148016
Reading from 19065: heap size 204 MB, throughput 0.644271
Reading from 19065: heap size 211 MB, throughput 0.864734
Reading from 19067: heap size 81 MB, throughput 0.997137
Reading from 19065: heap size 215 MB, throughput 0.869722
Reading from 19065: heap size 216 MB, throughput 0.640725
Reading from 19065: heap size 226 MB, throughput 0.578128
Reading from 19065: heap size 231 MB, throughput 0.431357
Reading from 19067: heap size 81 MB, throughput 0.993525
Reading from 19067: heap size 87 MB, throughput 0.995744
Reading from 19065: heap size 238 MB, throughput 0.180529
Reading from 19065: heap size 282 MB, throughput 0.594462
Reading from 19065: heap size 217 MB, throughput 0.733999
Reading from 19065: heap size 271 MB, throughput 0.699821
Reading from 19065: heap size 276 MB, throughput 0.794734
Reading from 19067: heap size 88 MB, throughput 0.993101
Reading from 19065: heap size 266 MB, throughput 0.829155
Reading from 19065: heap size 271 MB, throughput 0.730032
Reading from 19065: heap size 269 MB, throughput 0.71088
Reading from 19067: heap size 95 MB, throughput 0.993014
Reading from 19065: heap size 272 MB, throughput 0.137212
Reading from 19065: heap size 312 MB, throughput 0.721349
Reading from 19065: heap size 314 MB, throughput 0.811449
Reading from 19065: heap size 318 MB, throughput 0.946782
Reading from 19067: heap size 95 MB, throughput 0.994787
Reading from 19065: heap size 319 MB, throughput 0.957575
Reading from 19065: heap size 317 MB, throughput 0.860302
Reading from 19065: heap size 320 MB, throughput 0.859951
Reading from 19065: heap size 315 MB, throughput 0.789325
Reading from 19065: heap size 318 MB, throughput 0.862826
Reading from 19067: heap size 99 MB, throughput 0.986905
Reading from 19065: heap size 315 MB, throughput 0.869821
Reading from 19065: heap size 318 MB, throughput 0.836599
Reading from 19065: heap size 319 MB, throughput 0.739452
Reading from 19065: heap size 320 MB, throughput 0.645042
Reading from 19065: heap size 327 MB, throughput 0.71931
Reading from 19065: heap size 327 MB, throughput 0.782559
Reading from 19065: heap size 328 MB, throughput 0.673708
Reading from 19067: heap size 100 MB, throughput 0.994311
Reading from 19065: heap size 330 MB, throughput 0.730373
Equal recommendation: 864 MB each
Reading from 19067: heap size 105 MB, throughput 0.996015
Reading from 19065: heap size 335 MB, throughput 0.967768
Reading from 19067: heap size 105 MB, throughput 0.994279
Reading from 19067: heap size 110 MB, throughput 0.996858
Reading from 19065: heap size 336 MB, throughput 0.980849
Reading from 19067: heap size 110 MB, throughput 0.995971
Reading from 19067: heap size 114 MB, throughput 0.996844
Reading from 19065: heap size 341 MB, throughput 0.691564
Reading from 19067: heap size 114 MB, throughput 0.994793
Reading from 19067: heap size 118 MB, throughput 0.99779
Reading from 19065: heap size 382 MB, throughput 0.991179
Reading from 19067: heap size 118 MB, throughput 0.997325
Reading from 19067: heap size 121 MB, throughput 0.998137
Reading from 19065: heap size 386 MB, throughput 0.99225
Reading from 19067: heap size 121 MB, throughput 0.997968
Reading from 19067: heap size 123 MB, throughput 0.997768
Reading from 19065: heap size 388 MB, throughput 0.992532
Reading from 19067: heap size 123 MB, throughput 0.998231
Reading from 19065: heap size 391 MB, throughput 0.989966
Reading from 19067: heap size 125 MB, throughput 0.998059
Equal recommendation: 864 MB each
Reading from 19067: heap size 125 MB, throughput 0.998014
Reading from 19065: heap size 393 MB, throughput 0.991026
Reading from 19067: heap size 127 MB, throughput 0.998505
Reading from 19067: heap size 127 MB, throughput 0.998339
Reading from 19065: heap size 390 MB, throughput 0.987011
Reading from 19067: heap size 128 MB, throughput 0.998636
Reading from 19067: heap size 128 MB, throughput 0.4998
Reading from 19067: heap size 131 MB, throughput 0.994041
Reading from 19065: heap size 392 MB, throughput 0.974435
Reading from 19067: heap size 131 MB, throughput 0.992223
Reading from 19067: heap size 138 MB, throughput 0.992708
Reading from 19065: heap size 386 MB, throughput 0.987873
Reading from 19067: heap size 139 MB, throughput 0.99637
Reading from 19067: heap size 146 MB, throughput 0.997483
Reading from 19065: heap size 359 MB, throughput 0.987018
Reading from 19067: heap size 147 MB, throughput 0.995422
Reading from 19067: heap size 153 MB, throughput 0.997394
Reading from 19065: heap size 386 MB, throughput 0.984065
Reading from 19067: heap size 153 MB, throughput 0.997149
Reading from 19065: heap size 387 MB, throughput 0.972646
Equal recommendation: 864 MB each
Reading from 19067: heap size 159 MB, throughput 0.997918
Reading from 19065: heap size 388 MB, throughput 0.978811
Reading from 19067: heap size 159 MB, throughput 0.997426
Reading from 19067: heap size 164 MB, throughput 0.997853
Reading from 19065: heap size 389 MB, throughput 0.980352
Reading from 19065: heap size 392 MB, throughput 0.880173
Reading from 19065: heap size 396 MB, throughput 0.731084
Reading from 19065: heap size 401 MB, throughput 0.763856
Reading from 19065: heap size 408 MB, throughput 0.805665
Reading from 19065: heap size 418 MB, throughput 0.920177
Reading from 19067: heap size 164 MB, throughput 0.997283
Reading from 19067: heap size 169 MB, throughput 0.996592
Reading from 19065: heap size 420 MB, throughput 0.98844
Reading from 19067: heap size 169 MB, throughput 0.997398
Reading from 19067: heap size 173 MB, throughput 0.997832
Reading from 19065: heap size 417 MB, throughput 0.989903
Reading from 19067: heap size 173 MB, throughput 0.992742
Reading from 19065: heap size 421 MB, throughput 0.988968
Reading from 19067: heap size 177 MB, throughput 0.997454
Equal recommendation: 864 MB each
Reading from 19067: heap size 177 MB, throughput 0.998051
Reading from 19065: heap size 416 MB, throughput 0.986831
Reading from 19067: heap size 182 MB, throughput 0.998226
Reading from 19067: heap size 182 MB, throughput 0.99273
Reading from 19065: heap size 420 MB, throughput 0.979299
Reading from 19067: heap size 186 MB, throughput 0.993296
Reading from 19067: heap size 187 MB, throughput 0.99737
Reading from 19065: heap size 416 MB, throughput 0.986915
Reading from 19067: heap size 193 MB, throughput 0.997117
Reading from 19067: heap size 194 MB, throughput 0.997648
Reading from 19065: heap size 419 MB, throughput 0.988056
Reading from 19067: heap size 199 MB, throughput 0.99734
Reading from 19065: heap size 414 MB, throughput 0.986169
Reading from 19067: heap size 199 MB, throughput 0.997803
Reading from 19065: heap size 417 MB, throughput 0.984779
Reading from 19067: heap size 203 MB, throughput 0.998207
Equal recommendation: 864 MB each
Reading from 19067: heap size 204 MB, throughput 0.997686
Reading from 19065: heap size 413 MB, throughput 0.987295
Reading from 19067: heap size 209 MB, throughput 0.998465
Reading from 19065: heap size 416 MB, throughput 0.986762
Reading from 19067: heap size 209 MB, throughput 0.997622
Reading from 19067: heap size 213 MB, throughput 0.998032
Reading from 19065: heap size 413 MB, throughput 0.985933
Reading from 19065: heap size 415 MB, throughput 0.964294
Reading from 19065: heap size 413 MB, throughput 0.879215
Reading from 19065: heap size 416 MB, throughput 0.874381
Reading from 19065: heap size 421 MB, throughput 0.880509
Reading from 19067: heap size 213 MB, throughput 0.997886
Reading from 19065: heap size 422 MB, throughput 0.989768
Reading from 19067: heap size 218 MB, throughput 0.998089
Reading from 19067: heap size 218 MB, throughput 0.997319
Reading from 19065: heap size 429 MB, throughput 0.987142
Reading from 19067: heap size 222 MB, throughput 0.993463
Equal recommendation: 864 MB each
Reading from 19067: heap size 222 MB, throughput 0.996428
Reading from 19065: heap size 429 MB, throughput 0.991783
Reading from 19067: heap size 229 MB, throughput 0.998056
Reading from 19067: heap size 229 MB, throughput 0.996738
Reading from 19065: heap size 432 MB, throughput 0.990793
Reading from 19067: heap size 235 MB, throughput 0.998124
Reading from 19065: heap size 433 MB, throughput 0.989954
Reading from 19067: heap size 235 MB, throughput 0.99792
Reading from 19065: heap size 432 MB, throughput 0.989542
Reading from 19067: heap size 240 MB, throughput 0.998001
Equal recommendation: 864 MB each
Reading from 19065: heap size 434 MB, throughput 0.983597
Reading from 19067: heap size 241 MB, throughput 0.996717
Reading from 19067: heap size 246 MB, throughput 0.997504
Reading from 19065: heap size 431 MB, throughput 0.988561
Reading from 19067: heap size 246 MB, throughput 0.998282
Reading from 19065: heap size 433 MB, throughput 0.986771
Reading from 19067: heap size 251 MB, throughput 0.998141
Reading from 19065: heap size 434 MB, throughput 0.988123
Reading from 19067: heap size 251 MB, throughput 0.998339
Reading from 19067: heap size 256 MB, throughput 0.993044
Reading from 19067: heap size 256 MB, throughput 0.996755
Reading from 19065: heap size 434 MB, throughput 0.992397
Reading from 19065: heap size 436 MB, throughput 0.926619
Reading from 19065: heap size 436 MB, throughput 0.88731
Reading from 19065: heap size 441 MB, throughput 0.900253
Equal recommendation: 864 MB each
Reading from 19067: heap size 264 MB, throughput 0.99785
Reading from 19065: heap size 442 MB, throughput 0.989946
Reading from 19067: heap size 265 MB, throughput 0.998019
Reading from 19065: heap size 448 MB, throughput 0.99337
Reading from 19067: heap size 270 MB, throughput 0.998548
Reading from 19065: heap size 449 MB, throughput 0.991911
Reading from 19067: heap size 271 MB, throughput 0.998228
Reading from 19067: heap size 276 MB, throughput 0.998442
Reading from 19065: heap size 451 MB, throughput 0.991424
Reading from 19067: heap size 276 MB, throughput 0.99735
Reading from 19065: heap size 452 MB, throughput 0.990506
Equal recommendation: 864 MB each
Reading from 19067: heap size 281 MB, throughput 0.99844
Reading from 19065: heap size 451 MB, throughput 0.990029
Reading from 19067: heap size 281 MB, throughput 0.997821
Reading from 19067: heap size 287 MB, throughput 0.997603
Reading from 19065: heap size 453 MB, throughput 0.987553
Reading from 19067: heap size 287 MB, throughput 0.888965
Reading from 19067: heap size 300 MB, throughput 0.99927
Reading from 19065: heap size 452 MB, throughput 0.989156
Reading from 19067: heap size 301 MB, throughput 0.999152
Reading from 19065: heap size 453 MB, throughput 0.986011
Equal recommendation: 864 MB each
Reading from 19065: heap size 453 MB, throughput 0.990244
Reading from 19067: heap size 308 MB, throughput 0.999293
Reading from 19065: heap size 456 MB, throughput 0.908764
Reading from 19065: heap size 457 MB, throughput 0.912779
Reading from 19065: heap size 458 MB, throughput 0.98338
Reading from 19067: heap size 309 MB, throughput 0.999074
Reading from 19065: heap size 464 MB, throughput 0.992427
Reading from 19067: heap size 315 MB, throughput 0.999189
Reading from 19065: heap size 465 MB, throughput 0.993006
Reading from 19067: heap size 315 MB, throughput 0.999165
Reading from 19067: heap size 319 MB, throughput 0.999315
Reading from 19065: heap size 465 MB, throughput 0.992497
Equal recommendation: 864 MB each
Reading from 19067: heap size 320 MB, throughput 0.999106
Reading from 19065: heap size 467 MB, throughput 0.984989
Reading from 19067: heap size 324 MB, throughput 0.992876
Reading from 19067: heap size 324 MB, throughput 0.998179
Reading from 19065: heap size 465 MB, throughput 0.991423
Reading from 19067: heap size 335 MB, throughput 0.997938
Reading from 19065: heap size 467 MB, throughput 0.990922
Reading from 19067: heap size 335 MB, throughput 0.998359
Reading from 19065: heap size 468 MB, throughput 0.990421
Equal recommendation: 864 MB each
Reading from 19067: heap size 343 MB, throughput 0.997915
Reading from 19067: heap size 344 MB, throughput 0.998405
Reading from 19065: heap size 468 MB, throughput 0.993251
Reading from 19065: heap size 467 MB, throughput 0.974682
Reading from 19065: heap size 469 MB, throughput 0.903635
Reading from 19065: heap size 472 MB, throughput 0.942404
Reading from 19067: heap size 352 MB, throughput 0.998634
Reading from 19065: heap size 474 MB, throughput 0.992758
Reading from 19067: heap size 353 MB, throughput 0.998427
Reading from 19067: heap size 359 MB, throughput 0.997891
Reading from 19065: heap size 478 MB, throughput 0.992311
Equal recommendation: 864 MB each
Reading from 19067: heap size 360 MB, throughput 0.996282
Reading from 19065: heap size 479 MB, throughput 0.990834
Reading from 19067: heap size 369 MB, throughput 0.99817
Reading from 19065: heap size 479 MB, throughput 0.989019
Reading from 19067: heap size 369 MB, throughput 0.997749
Reading from 19067: heap size 378 MB, throughput 0.998271
Reading from 19065: heap size 481 MB, throughput 0.988526
Reading from 19067: heap size 379 MB, throughput 0.998075
Equal recommendation: 864 MB each
Reading from 19065: heap size 481 MB, throughput 0.99107
Reading from 19067: heap size 388 MB, throughput 0.997555
Reading from 19065: heap size 481 MB, throughput 0.989646
Reading from 19067: heap size 388 MB, throughput 0.998393
Reading from 19065: heap size 483 MB, throughput 0.991388
Reading from 19067: heap size 396 MB, throughput 0.995238
Reading from 19065: heap size 484 MB, throughput 0.919153
Reading from 19065: heap size 483 MB, throughput 0.913825
Reading from 19065: heap size 486 MB, throughput 0.982582
Reading from 19067: heap size 397 MB, throughput 0.997472
Equal recommendation: 864 MB each
Reading from 19065: heap size 493 MB, throughput 0.993451
Reading from 19067: heap size 410 MB, throughput 0.998375
Reading from 19065: heap size 494 MB, throughput 0.992576
Reading from 19067: heap size 411 MB, throughput 0.998643
Reading from 19065: heap size 493 MB, throughput 0.992793
Reading from 19067: heap size 420 MB, throughput 0.998346
Reading from 19065: heap size 496 MB, throughput 0.99204
Reading from 19067: heap size 421 MB, throughput 0.998301
Equal recommendation: 864 MB each
Reading from 19065: heap size 495 MB, throughput 0.991903
Reading from 19067: heap size 431 MB, throughput 0.99844
Reading from 19067: heap size 431 MB, throughput 0.994872
Reading from 19065: heap size 496 MB, throughput 0.991115
Reading from 19067: heap size 440 MB, throughput 0.998517
Reading from 19065: heap size 498 MB, throughput 0.994873
Reading from 19065: heap size 499 MB, throughput 0.942135
Reading from 19065: heap size 498 MB, throughput 0.92874
Reading from 19067: heap size 441 MB, throughput 0.998155
Reading from 19065: heap size 501 MB, throughput 0.986039
Equal recommendation: 864 MB each
Reading from 19067: heap size 453 MB, throughput 0.998602
Reading from 19065: heap size 507 MB, throughput 0.994412
Reading from 19067: heap size 453 MB, throughput 0.997978
Reading from 19065: heap size 508 MB, throughput 0.994107
Reading from 19067: heap size 464 MB, throughput 0.998624
Reading from 19065: heap size 508 MB, throughput 0.990451
Equal recommendation: 864 MB each
Reading from 19067: heap size 464 MB, throughput 0.996722
Reading from 19067: heap size 475 MB, throughput 0.997836
Reading from 19065: heap size 509 MB, throughput 0.991851
Reading from 19067: heap size 475 MB, throughput 0.997968
Reading from 19065: heap size 508 MB, throughput 0.991279
Reading from 19067: heap size 488 MB, throughput 0.99832
Reading from 19065: heap size 509 MB, throughput 0.991166
Equal recommendation: 864 MB each
Reading from 19065: heap size 511 MB, throughput 0.989364
Reading from 19065: heap size 512 MB, throughput 0.921133
Reading from 19065: heap size 515 MB, throughput 0.961208
Reading from 19067: heap size 488 MB, throughput 0.997801
Reading from 19065: heap size 517 MB, throughput 0.99361
Reading from 19067: heap size 500 MB, throughput 0.998593
Reading from 19067: heap size 500 MB, throughput 0.996221
Reading from 19065: heap size 520 MB, throughput 0.993459
Equal recommendation: 864 MB each
Reading from 19067: heap size 511 MB, throughput 0.997878
Reading from 19065: heap size 521 MB, throughput 0.992417
Reading from 19067: heap size 512 MB, throughput 0.998116
Reading from 19065: heap size 519 MB, throughput 0.993681
Reading from 19067: heap size 525 MB, throughput 0.997744
Reading from 19065: heap size 522 MB, throughput 0.992413
Equal recommendation: 864 MB each
Reading from 19067: heap size 526 MB, throughput 0.998138
Reading from 19065: heap size 523 MB, throughput 0.914302
Reading from 19067: heap size 539 MB, throughput 0.998258
Reading from 19065: heap size 553 MB, throughput 0.984866
Reading from 19065: heap size 556 MB, throughput 0.943096
Reading from 19065: heap size 556 MB, throughput 0.986412
Reading from 19067: heap size 540 MB, throughput 0.996814
Reading from 19065: heap size 562 MB, throughput 0.99541
Equal recommendation: 864 MB each
Reading from 19067: heap size 553 MB, throughput 0.998426
Reading from 19065: heap size 563 MB, throughput 0.993291
Reading from 19067: heap size 554 MB, throughput 0.964455
Reading from 19065: heap size 564 MB, throughput 0.99093
Reading from 19067: heap size 570 MB, throughput 0.999275
Reading from 19065: heap size 565 MB, throughput 0.992698
Equal recommendation: 864 MB each
Reading from 19067: heap size 571 MB, throughput 0.999097
Reading from 19067: heap size 584 MB, throughput 0.997596
Reading from 19065: heap size 565 MB, throughput 0.992687
Reading from 19067: heap size 586 MB, throughput 0.998911
Reading from 19065: heap size 566 MB, throughput 0.995298
Reading from 19065: heap size 568 MB, throughput 0.953754
Reading from 19065: heap size 569 MB, throughput 0.923132
Reading from 19065: heap size 574 MB, throughput 0.991726
Equal recommendation: 864 MB each
Reading from 19067: heap size 594 MB, throughput 0.999098
Reading from 19065: heap size 577 MB, throughput 0.99499
Reading from 19067: heap size 589 MB, throughput 0.999016
Reading from 19065: heap size 577 MB, throughput 0.994393
Reading from 19067: heap size 594 MB, throughput 0.999183
Equal recommendation: 864 MB each
Reading from 19067: heap size 593 MB, throughput 0.997559
Reading from 19065: heap size 580 MB, throughput 0.993086
Reading from 19067: heap size 593 MB, throughput 0.999061
Reading from 19065: heap size 581 MB, throughput 0.99199
Reading from 19067: heap size 594 MB, throughput 0.998827
Reading from 19065: heap size 582 MB, throughput 0.994708
Reading from 19065: heap size 583 MB, throughput 0.977641
Reading from 19065: heap size 585 MB, throughput 0.924079
Equal recommendation: 864 MB each
Reading from 19067: heap size 593 MB, throughput 0.998875
Reading from 19065: heap size 590 MB, throughput 0.99485
Reading from 19065: heap size 592 MB, throughput 0.995054
Client 19067 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 19065: heap size 595 MB, throughput 0.993561
Reading from 19065: heap size 597 MB, throughput 0.993215
Reading from 19065: heap size 597 MB, throughput 0.991557
Reading from 19065: heap size 598 MB, throughput 0.994782
Recommendation: one client; give it all the memory
Reading from 19065: heap size 599 MB, throughput 0.945582
Reading from 19065: heap size 601 MB, throughput 0.985967
Reading from 19065: heap size 609 MB, throughput 0.995244
Reading from 19065: heap size 610 MB, throughput 0.994239
Recommendation: one client; give it all the memory
Reading from 19065: heap size 609 MB, throughput 0.994004
Reading from 19065: heap size 611 MB, throughput 0.992955
Reading from 19065: heap size 614 MB, throughput 0.993391
Reading from 19065: heap size 614 MB, throughput 0.971298
Recommendation: one client; give it all the memory
Client 19065 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
CommandThread: finished
TimerThread: finished
Main: All threads killed, cleaning up
