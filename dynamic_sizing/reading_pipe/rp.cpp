#include <Log.hpp>
#include <Messages.hpp>
#include <ReadingReceiver.hpp>
#include <ReceiverThread.hpp>
#include <SignalHandler.hpp>
#include <Utilities.hpp>

class RecvThread : public economem::ReceiverThread<economem::Reading>
{
public:
    RecvThread(economem::ReadingReceiver& receiver, economem::Log& log) : ReceiverThread(receiver, "RecvThread"), m_Log(log) {}

private:
    virtual void received(const economem::Reading& reading);
    economem::Log& m_Log;
};

int main(int argc, char** argv)
{
    const char* name = economem::NAME_DEFAULT;

    if (argc > 1)
        name = argv[1];

    economem::Log log(stdout, true, true);
    economem::ReadingReceiver receiver(name);
    RecvThread r(receiver, log);

    economem::SignalHandler::init([&] () { r.kill(); });

    if (!r.start())
        economem::die("Failed to create thread");

    r.wait();

    return 0;
}

void RecvThread::received(const economem::Reading& reading)
{
    switch (reading.type)
    {
    case economem::READINGTYPE_READING:
    {
        m_Log.logReading(reading.sender, reading.heapSizeMB, reading.throughput);
    }
    break;
    case economem::READINGTYPE_DEATH:
    {
        m_Log.logDeath(reading.sender, 0);
    }
    break;
    case economem::READINGTYPE_MINHEAP:
    {
        m_Log.logMinHeap(reading.sender, reading.heapSizeMB);
    }
    break;
    default:
        economem::die("Unknown reading type %u", (unsigned int)reading.type);
        break;
    }
}
