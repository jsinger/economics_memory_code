#!/usr/bin/env python

"""Plot readings from stdin as a batch, and save to a file"""

import argparse
import argtypes as h
import matplotlib.pyplot as plt
import behaviour_plotter as p

class BatchPlotter(p.BehaviourPlotter):
    def __init__(self, throughput_model, in_filename, out_filename, pid_names_file, draw_Rs, title, highest_used_mem_file, greyscale):
        super(BatchPlotter, self).__init__(throughput_model, open(in_filename), pid_names_file, False, draw_Rs, title, greyscale)
        self.out_filename = out_filename
        self.highest_used_mem_file = highest_used_mem_file

    def end(self):
        self.draw_clients()

        plt.savefig(self.out_filename)

        if self.highest_used_mem_file:
            with open(self.highest_used_mem_file, "w") as f:
                f.write(str(self.used_mem.get_highest_value()) + "\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot readings as a batch, and save to a file")
    parser.add_argument("--dontDrawRs", action="store_true", help="Don't mark recommendations with 'R'")
    parser.add_argument("--title", default=None, help="Override the auto-generated title")
    parser.add_argument("--pidNamesFile", type=h.existing_absolute_path, default=None, help="The file containing the mapping of PIDs to names")
    parser.add_argument("--highestUsedFile", type=h.absolute_path, default=None, help="Write the highest total memory used to this file")
    parser.add_argument("--greyscale", action="store_true", help="Plot the graph in greyscale")
    parser.add_argument("throughput_model", help="The name of the throughput model used to generate the readings")
    parser.add_argument("inFile", type=h.existing_absolute_path, help="The file containing the readings to plot")
    parser.add_argument("outFile", help="The file to save to")

    args = parser.parse_args()

    BatchPlotter(args.throughput_model, args.inFile, args.outFile, args.pidNamesFile, not args.dontDrawRs, args.title, args.highestUsedFile, args.greyscale).plot()
