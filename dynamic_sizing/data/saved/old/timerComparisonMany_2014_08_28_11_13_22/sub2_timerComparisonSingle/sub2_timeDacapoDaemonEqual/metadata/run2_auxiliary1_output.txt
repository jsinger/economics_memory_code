economemd
    total memory: 100 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_08_28_11_13_22/sub2_timerComparisonSingle/sub2_timeDacapoDaemonEqual/daemon_run2_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 14344: heap size 9 MB, throughput 0.984794
Clients: 1
Client 14344 has a minimum heap size of 8 MB
Reading from 14345: heap size 9 MB, throughput 0.991036
Clients: 2
Client 14345 has a minimum heap size of 8 MB
Reading from 14345: heap size 9 MB, throughput 0.964775
Reading from 14344: heap size 9 MB, throughput 0.963593
Reading from 14345: heap size 9 MB, throughput 0.917801
Reading from 14345: heap size 9 MB, throughput 0.956248
Reading from 14344: heap size 11 MB, throughput 0.965597
Reading from 14345: heap size 11 MB, throughput 0.963769
Reading from 14344: heap size 11 MB, throughput 0.910926
Reading from 14345: heap size 11 MB, throughput 0.924373
Reading from 14345: heap size 16 MB, throughput 0.73574
Reading from 14344: heap size 15 MB, throughput 0.590647
Reading from 14345: heap size 22 MB, throughput 0.998668
Reading from 14344: heap size 23 MB, throughput 0.997744
Reading from 14345: heap size 22 MB, throughput 0.998906
Reading from 14344: heap size 26 MB, throughput 0.997504
Reading from 14345: heap size 27 MB, throughput 0.99818
Reading from 14344: heap size 28 MB, throughput 0.997016
Reading from 14345: heap size 28 MB, throughput 0.998263
Reading from 14344: heap size 29 MB, throughput 0.997368
Reading from 14345: heap size 28 MB, throughput 0.998923
Reading from 14344: heap size 31 MB, throughput 0.997741
Reading from 14345: heap size 28 MB, throughput 0.998605
Reading from 14344: heap size 31 MB, throughput 0.99734
Reading from 14345: heap size 29 MB, throughput 0.997865
Reading from 14344: heap size 31 MB, throughput 0.996672
Equal recommendation: 50 MB each
Reading from 14345: heap size 30 MB, throughput 0.996977
Reading from 14344: heap size 32 MB, throughput 0.996964
Reading from 14345: heap size 30 MB, throughput 0.99758
Reading from 14344: heap size 32 MB, throughput 0.995633
Reading from 14345: heap size 31 MB, throughput 0.9975
Reading from 14344: heap size 33 MB, throughput 0.995854
Reading from 14345: heap size 31 MB, throughput 0.996981
Reading from 14344: heap size 33 MB, throughput 0.994851
Reading from 14345: heap size 31 MB, throughput 0.996542
Reading from 14344: heap size 33 MB, throughput 0.995411
Reading from 14345: heap size 31 MB, throughput 0.996347
Reading from 14344: heap size 34 MB, throughput 0.994293
Equal recommendation: 50 MB each
Reading from 14344: heap size 34 MB, throughput 0.994406
Reading from 14345: heap size 32 MB, throughput 0.996075
Reading from 14344: heap size 34 MB, throughput 0.99373
Reading from 14345: heap size 32 MB, throughput 0.995932
Reading from 14344: heap size 35 MB, throughput 0.993218
Reading from 14345: heap size 33 MB, throughput 0.99531
Reading from 14344: heap size 35 MB, throughput 0.997884
Reading from 14345: heap size 33 MB, throughput 0.998417
Reading from 14344: heap size 36 MB, throughput 0.997748
Reading from 14345: heap size 33 MB, throughput 0.996604
Reading from 14344: heap size 36 MB, throughput 0.993701
Reading from 14345: heap size 34 MB, throughput 0.996608
Reading from 14344: heap size 34 MB, throughput 0.964387
Reading from 14345: heap size 34 MB, throughput 0.996775
Equal recommendation: 50 MB each
Reading from 14344: heap size 38 MB, throughput 0.999492
Reading from 14345: heap size 36 MB, throughput 0.997315
Reading from 14344: heap size 39 MB, throughput 0.999137
Reading from 14345: heap size 37 MB, throughput 0.997585
Reading from 14344: heap size 39 MB, throughput 0.999332
Reading from 14345: heap size 37 MB, throughput 0.996634
Reading from 14344: heap size 40 MB, throughput 0.998983
Reading from 14345: heap size 38 MB, throughput 0.997484
Reading from 14344: heap size 40 MB, throughput 0.999107
Reading from 14345: heap size 38 MB, throughput 0.996982
Reading from 14344: heap size 39 MB, throughput 0.999117
Reading from 14345: heap size 39 MB, throughput 0.975404
Reading from 14344: heap size 40 MB, throughput 0.998982
Equal recommendation: 50 MB each
Reading from 14345: heap size 41 MB, throughput 0.999475
Reading from 14344: heap size 40 MB, throughput 0.998877
Reading from 14344: heap size 40 MB, throughput 0.998634
Reading from 14345: heap size 42 MB, throughput 0.99936
Reading from 14344: heap size 39 MB, throughput 0.998106
Reading from 14345: heap size 42 MB, throughput 0.999276
Reading from 14344: heap size 39 MB, throughput 0.998573
Reading from 14345: heap size 42 MB, throughput 0.999312
Reading from 14344: heap size 38 MB, throughput 0.99878
Reading from 14345: heap size 42 MB, throughput 0.999159
Reading from 14344: heap size 39 MB, throughput 0.998469
Reading from 14345: heap size 42 MB, throughput 0.998946
Reading from 14344: heap size 39 MB, throughput 0.998074
Equal recommendation: 50 MB each
Reading from 14345: heap size 42 MB, throughput 0.999065
Reading from 14344: heap size 39 MB, throughput 0.998537
Reading from 14345: heap size 41 MB, throughput 0.994538
Reading from 14344: heap size 39 MB, throughput 0.991883
Reading from 14345: heap size 42 MB, throughput 0.997436
Reading from 14344: heap size 40 MB, throughput 0.996972
Reading from 14345: heap size 43 MB, throughput 0.997717
Reading from 14344: heap size 41 MB, throughput 0.997074
Reading from 14345: heap size 44 MB, throughput 0.997257
Reading from 14344: heap size 42 MB, throughput 0.995817
Reading from 14344: heap size 43 MB, throughput 0.99572
Reading from 14345: heap size 45 MB, throughput 0.99771
Equal recommendation: 50 MB each
Reading from 14344: heap size 44 MB, throughput 0.996218
Reading from 14345: heap size 45 MB, throughput 0.997251
Reading from 14344: heap size 45 MB, throughput 0.995471
Reading from 14345: heap size 45 MB, throughput 0.99813
Reading from 14344: heap size 45 MB, throughput 0.995902
Reading from 14345: heap size 46 MB, throughput 0.998313
Reading from 14344: heap size 46 MB, throughput 0.993629
Reading from 14345: heap size 46 MB, throughput 0.997038
Reading from 14344: heap size 46 MB, throughput 0.994822
Reading from 14345: heap size 46 MB, throughput 0.99741
Reading from 14344: heap size 47 MB, throughput 0.993336
Equal recommendation: 50 MB each
Reading from 14344: heap size 47 MB, throughput 0.99457
Reading from 14345: heap size 46 MB, throughput 0.997817
Reading from 14344: heap size 47 MB, throughput 0.9942
Reading from 14345: heap size 46 MB, throughput 0.998173
Reading from 14344: heap size 48 MB, throughput 0.992544
Reading from 14345: heap size 46 MB, throughput 0.998159
Reading from 14344: heap size 47 MB, throughput 0.993275
Reading from 14345: heap size 40 MB, throughput 0.998765
Reading from 14344: heap size 48 MB, throughput 0.979016
Reading from 14345: heap size 45 MB, throughput 0.997889
Reading from 14344: heap size 48 MB, throughput 0.996737
Equal recommendation: 50 MB each
Reading from 14345: heap size 46 MB, throughput 0.997969
Reading from 14344: heap size 49 MB, throughput 0.997683
Reading from 14345: heap size 45 MB, throughput 0.99796
Reading from 14344: heap size 49 MB, throughput 0.997655
Reading from 14344: heap size 49 MB, throughput 0.996855
Reading from 14345: heap size 45 MB, throughput 0.998402
Reading from 14344: heap size 48 MB, throughput 0.996791
Reading from 14345: heap size 44 MB, throughput 0.998115
Reading from 14344: heap size 49 MB, throughput 0.996148
Reading from 14345: heap size 45 MB, throughput 0.997748
Equal recommendation: 50 MB each
Reading from 14344: heap size 48 MB, throughput 0.99571
Reading from 14345: heap size 44 MB, throughput 0.998292
Reading from 14344: heap size 48 MB, throughput 0.995503
Reading from 14345: heap size 44 MB, throughput 0.998314
Reading from 14344: heap size 48 MB, throughput 0.995187
Reading from 14345: heap size 44 MB, throughput 0.998017
Reading from 14344: heap size 48 MB, throughput 0.995058
Reading from 14345: heap size 44 MB, throughput 0.99816
Reading from 14344: heap size 49 MB, throughput 0.994613
Reading from 14345: heap size 44 MB, throughput 0.998965
Equal recommendation: 50 MB each
Reading from 14344: heap size 49 MB, throughput 0.994929
Reading from 14345: heap size 44 MB, throughput 0.999288
Reading from 14344: heap size 49 MB, throughput 0.994963
Reading from 14345: heap size 45 MB, throughput 0.998042
Reading from 14344: heap size 49 MB, throughput 0.991118
Reading from 14344: heap size 47 MB, throughput 0.995979
Reading from 14345: heap size 45 MB, throughput 0.998179
Reading from 14344: heap size 50 MB, throughput 0.997392
Reading from 14345: heap size 45 MB, throughput 0.998832
Reading from 14344: heap size 49 MB, throughput 0.996417
Equal recommendation: 50 MB each
Reading from 14345: heap size 45 MB, throughput 0.99821
Reading from 14344: heap size 49 MB, throughput 0.998265
Reading from 14345: heap size 45 MB, throughput 0.99819
Reading from 14344: heap size 49 MB, throughput 0.998257
Reading from 14344: heap size 49 MB, throughput 0.997742
Reading from 14345: heap size 45 MB, throughput 0.998493
Reading from 14344: heap size 50 MB, throughput 0.997778
Reading from 14345: heap size 45 MB, throughput 0.998579
Reading from 14344: heap size 50 MB, throughput 0.998462
Equal recommendation: 50 MB each
Reading from 14345: heap size 45 MB, throughput 0.998421
Reading from 14344: heap size 50 MB, throughput 0.998025
Reading from 14345: heap size 46 MB, throughput 0.998143
Reading from 14344: heap size 50 MB, throughput 0.99849
Reading from 14344: heap size 50 MB, throughput 0.997811
Reading from 14345: heap size 46 MB, throughput 0.998906
Reading from 14344: heap size 50 MB, throughput 0.999496
Reading from 14345: heap size 46 MB, throughput 0.998437
Reading from 14344: heap size 49 MB, throughput 0.997983
Equal recommendation: 50 MB each
Reading from 14345: heap size 46 MB, throughput 0.998948
Reading from 14344: heap size 50 MB, throughput 0.99807
Reading from 14344: heap size 49 MB, throughput 0.998314
Reading from 14345: heap size 46 MB, throughput 0.998646
Reading from 14344: heap size 49 MB, throughput 0.998529
Reading from 14345: heap size 46 MB, throughput 0.998511
Reading from 14344: heap size 49 MB, throughput 0.998453
Reading from 14345: heap size 47 MB, throughput 0.998775
Equal recommendation: 50 MB each
Reading from 14344: heap size 49 MB, throughput 0.998596
Reading from 14345: heap size 47 MB, throughput 0.998652
Reading from 14344: heap size 49 MB, throughput 0.998164
Reading from 14345: heap size 46 MB, throughput 0.998471
Reading from 14344: heap size 49 MB, throughput 0.998966
Reading from 14344: heap size 48 MB, throughput 0.998211
Reading from 14345: heap size 47 MB, throughput 0.998567
Reading from 14344: heap size 49 MB, throughput 0.998396
Equal recommendation: 50 MB each
Reading from 14345: heap size 47 MB, throughput 0.998991
Reading from 14344: heap size 48 MB, throughput 0.996842
Reading from 14345: heap size 47 MB, throughput 0.998887
Reading from 14344: heap size 49 MB, throughput 0.996925
Reading from 14345: heap size 48 MB, throughput 0.998252
Reading from 14344: heap size 49 MB, throughput 0.998593
Reading from 14345: heap size 48 MB, throughput 0.998186
Reading from 14344: heap size 49 MB, throughput 0.997974
Equal recommendation: 50 MB each
Reading from 14345: heap size 48 MB, throughput 0.998716
Reading from 14344: heap size 49 MB, throughput 0.998835
Reading from 14344: heap size 49 MB, throughput 0.997735
Reading from 14345: heap size 48 MB, throughput 0.999015
Reading from 14344: heap size 49 MB, throughput 0.997536
Reading from 14345: heap size 49 MB, throughput 0.99864
Reading from 14344: heap size 49 MB, throughput 0.998459
Equal recommendation: 50 MB each
Reading from 14345: heap size 49 MB, throughput 0.998703
Reading from 14344: heap size 50 MB, throughput 0.99836
Reading from 14345: heap size 49 MB, throughput 0.99862
Reading from 14344: heap size 50 MB, throughput 0.997557
Reading from 14345: heap size 49 MB, throughput 0.998915
Reading from 14344: heap size 50 MB, throughput 0.998345
Reading from 14345: heap size 50 MB, throughput 0.998716
Reading from 14344: heap size 50 MB, throughput 0.998515
Equal recommendation: 50 MB each
Reading from 14345: heap size 50 MB, throughput 0.998105
Reading from 14344: heap size 50 MB, throughput 0.998869
Reading from 14344: heap size 49 MB, throughput 0.997935
Reading from 14345: heap size 50 MB, throughput 0.998693
Reading from 14344: heap size 50 MB, throughput 0.998556
Reading from 14345: heap size 49 MB, throughput 0.998938
Equal recommendation: 50 MB each
Reading from 14344: heap size 50 MB, throughput 0.998409
Reading from 14345: heap size 49 MB, throughput 0.998699
Reading from 14344: heap size 50 MB, throughput 0.998762
Reading from 14345: heap size 50 MB, throughput 0.998825
Reading from 14344: heap size 50 MB, throughput 0.998052
Reading from 14345: heap size 50 MB, throughput 0.998681
Reading from 14344: heap size 50 MB, throughput 0.998842
Equal recommendation: 50 MB each
Reading from 14345: heap size 50 MB, throughput 0.998681
Reading from 14344: heap size 49 MB, throughput 0.998356
Reading from 14344: heap size 50 MB, throughput 0.999067
Reading from 14345: heap size 50 MB, throughput 0.946276
Reading from 14344: heap size 50 MB, throughput 0.998848
Reading from 14345: heap size 56 MB, throughput 0.999326
Reading from 14344: heap size 51 MB, throughput 0.998682
Equal recommendation: 50 MB each
Reading from 14345: heap size 57 MB, throughput 0.999503
Reading from 14344: heap size 49 MB, throughput 0.99832
Reading from 14345: heap size 58 MB, throughput 0.999518
Reading from 14344: heap size 50 MB, throughput 0.998527
Reading from 14345: heap size 57 MB, throughput 0.999286
Reading from 14344: heap size 49 MB, throughput 0.998156
Reading from 14345: heap size 50 MB, throughput 0.999288
Reading from 14344: heap size 50 MB, throughput 0.99854
Equal recommendation: 50 MB each
Reading from 14344: heap size 50 MB, throughput 0.998555
Reading from 14345: heap size 56 MB, throughput 0.999264
Reading from 14344: heap size 49 MB, throughput 0.998514
Reading from 14345: heap size 51 MB, throughput 0.999122
Reading from 14344: heap size 50 MB, throughput 0.9987
Reading from 14345: heap size 54 MB, throughput 0.998893
Reading from 14344: heap size 50 MB, throughput 0.998947
Equal recommendation: 50 MB each
Reading from 14345: heap size 51 MB, throughput 0.997483
Reading from 14344: heap size 49 MB, throughput 0.998832
Reading from 14345: heap size 52 MB, throughput 0.998189
Reading from 14345: heap size 54 MB, throughput 0.997619
Reading from 14344: heap size 50 MB, throughput 0.998735
Reading from 14345: heap size 54 MB, throughput 0.99745
Reading from 14344: heap size 50 MB, throughput 0.998643
Equal recommendation: 50 MB each
Reading from 14345: heap size 54 MB, throughput 0.997065
Reading from 14344: heap size 50 MB, throughput 0.998392
Reading from 14345: heap size 54 MB, throughput 0.996423
Reading from 14344: heap size 49 MB, throughput 0.998496
Reading from 14345: heap size 55 MB, throughput 0.995547
Reading from 14344: heap size 50 MB, throughput 0.998842
Reading from 14345: heap size 56 MB, throughput 0.995614
Reading from 14344: heap size 50 MB, throughput 0.998838
Reading from 14345: heap size 56 MB, throughput 0.995118
Equal recommendation: 50 MB each
Reading from 14345: heap size 56 MB, throughput 0.994929
Reading from 14344: heap size 50 MB, throughput 0.998496
Reading from 14345: heap size 57 MB, throughput 0.995258
Reading from 14344: heap size 49 MB, throughput 0.998949
Reading from 14345: heap size 55 MB, throughput 0.995137
Reading from 14344: heap size 50 MB, throughput 0.998805
Reading from 14345: heap size 57 MB, throughput 0.995245
Reading from 14345: heap size 57 MB, throughput 0.994564
Reading from 14344: heap size 50 MB, throughput 0.998446
Equal recommendation: 50 MB each
Reading from 14345: heap size 58 MB, throughput 0.995445
Reading from 14344: heap size 50 MB, throughput 0.998865
Reading from 14345: heap size 58 MB, throughput 0.994462
Reading from 14345: heap size 48 MB, throughput 0.997543
Reading from 14344: heap size 49 MB, throughput 0.998842
Reading from 14345: heap size 57 MB, throughput 0.982003
Reading from 14344: heap size 50 MB, throughput 0.998777
Reading from 14345: heap size 48 MB, throughput 0.999531
Equal recommendation: 50 MB each
Reading from 14345: heap size 56 MB, throughput 0.999459
Reading from 14344: heap size 50 MB, throughput 0.998848
Reading from 14345: heap size 57 MB, throughput 0.999427
Reading from 14344: heap size 50 MB, throughput 0.998506
Reading from 14345: heap size 58 MB, throughput 0.999307
Reading from 14345: heap size 58 MB, throughput 0.999122
Reading from 14344: heap size 49 MB, throughput 0.997745
Reading from 14345: heap size 57 MB, throughput 0.999216
Reading from 14345: heap size 40 MB, throughput 0.996226
Reading from 14344: heap size 49 MB, throughput 0.994003
Equal recommendation: 50 MB each
Reading from 14345: heap size 57 MB, throughput 0.997587
Reading from 14344: heap size 50 MB, throughput 0.978695
Reading from 14345: heap size 46 MB, throughput 0.996697
Reading from 14345: heap size 55 MB, throughput 0.997109
Reading from 14344: heap size 61 MB, throughput 0.999612
Reading from 14345: heap size 47 MB, throughput 0.997368
Reading from 14344: heap size 62 MB, throughput 0.999527
Reading from 14345: heap size 55 MB, throughput 0.997341
Reading from 14344: heap size 63 MB, throughput 0.999424
Reading from 14345: heap size 47 MB, throughput 0.997841
Equal recommendation: 50 MB each
Reading from 14345: heap size 55 MB, throughput 0.997648
Reading from 14344: heap size 63 MB, throughput 0.999005
Reading from 14345: heap size 48 MB, throughput 0.997383
Reading from 14344: heap size 62 MB, throughput 0.999328
Reading from 14345: heap size 54 MB, throughput 0.997662
Reading from 14344: heap size 55 MB, throughput 0.999228
Reading from 14345: heap size 48 MB, throughput 0.998004
Reading from 14344: heap size 62 MB, throughput 0.99886
Equal recommendation: 50 MB each
Reading from 14345: heap size 54 MB, throughput 0.997937
Reading from 14344: heap size 56 MB, throughput 0.999017
Reading from 14345: heap size 47 MB, throughput 0.997835
Client 14345 died
Clients: 1
Reading from 14344: heap size 60 MB, throughput 0.998853
Client 14344 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
