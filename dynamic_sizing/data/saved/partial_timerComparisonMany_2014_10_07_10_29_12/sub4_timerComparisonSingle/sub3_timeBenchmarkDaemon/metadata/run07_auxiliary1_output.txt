economemd
    total memory: 7518 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub4_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 5310: heap size 9 MB, throughput 0.987767
Clients: 1
Client 5310 has a minimum heap size of 30 MB
Reading from 5309: heap size 9 MB, throughput 0.989626
Clients: 2
Client 5309 has a minimum heap size of 1223 MB
Reading from 5310: heap size 9 MB, throughput 0.983403
Reading from 5309: heap size 9 MB, throughput 0.985225
Reading from 5310: heap size 11 MB, throughput 0.98126
Reading from 5310: heap size 11 MB, throughput 0.968563
Reading from 5310: heap size 15 MB, throughput 0.945926
Reading from 5310: heap size 15 MB, throughput 0.907972
Reading from 5310: heap size 24 MB, throughput 0.891835
Reading from 5309: heap size 9 MB, throughput 0.979276
Reading from 5310: heap size 24 MB, throughput 0.868795
Reading from 5310: heap size 40 MB, throughput 0.892608
Reading from 5310: heap size 40 MB, throughput 0.90033
Reading from 5310: heap size 60 MB, throughput 0.928918
Reading from 5309: heap size 9 MB, throughput 0.965922
Reading from 5310: heap size 60 MB, throughput 0.939592
Reading from 5310: heap size 91 MB, throughput 0.95709
Reading from 5310: heap size 91 MB, throughput 0.953427
Reading from 5310: heap size 140 MB, throughput 0.963294
Reading from 5309: heap size 11 MB, throughput 0.979903
Reading from 5309: heap size 11 MB, throughput 0.972842
Reading from 5309: heap size 17 MB, throughput 0.936489
Reading from 5309: heap size 17 MB, throughput 0.787635
Reading from 5309: heap size 30 MB, throughput 0.612508
Reading from 5309: heap size 31 MB, throughput 0.187338
Reading from 5309: heap size 33 MB, throughput 0.685974
Reading from 5310: heap size 140 MB, throughput 0.961082
Reading from 5309: heap size 46 MB, throughput 0.627972
Reading from 5309: heap size 50 MB, throughput 0.506776
Reading from 5309: heap size 50 MB, throughput 0.557829
Reading from 5309: heap size 67 MB, throughput 0.191136
Reading from 5309: heap size 70 MB, throughput 0.292867
Reading from 5310: heap size 224 MB, throughput 0.902438
Reading from 5309: heap size 71 MB, throughput 0.455856
Reading from 5309: heap size 92 MB, throughput 0.278311
Reading from 5309: heap size 99 MB, throughput 0.313665
Reading from 5309: heap size 99 MB, throughput 0.743896
Reading from 5309: heap size 102 MB, throughput 0.457498
Reading from 5309: heap size 129 MB, throughput 0.675421
Reading from 5309: heap size 134 MB, throughput 0.618889
Reading from 5310: heap size 240 MB, throughput 0.981521
Reading from 5309: heap size 136 MB, throughput 0.643381
Reading from 5309: heap size 141 MB, throughput 0.686682
Reading from 5309: heap size 146 MB, throughput 0.702013
Reading from 5309: heap size 151 MB, throughput 0.392384
Reading from 5309: heap size 184 MB, throughput 0.629377
Reading from 5309: heap size 192 MB, throughput 0.697547
Reading from 5310: heap size 281 MB, throughput 0.988033
Reading from 5309: heap size 193 MB, throughput 0.680614
Reading from 5309: heap size 197 MB, throughput 0.689358
Reading from 5309: heap size 203 MB, throughput 0.605524
Reading from 5309: heap size 207 MB, throughput 0.629241
Reading from 5309: heap size 216 MB, throughput 0.4438
Reading from 5309: heap size 256 MB, throughput 0.505582
Reading from 5309: heap size 262 MB, throughput 0.525507
Reading from 5309: heap size 266 MB, throughput 0.546895
Reading from 5310: heap size 294 MB, throughput 0.958927
Reading from 5309: heap size 268 MB, throughput 0.23398
Reading from 5309: heap size 311 MB, throughput 0.560518
Reading from 5309: heap size 263 MB, throughput 0.553477
Reading from 5310: heap size 332 MB, throughput 0.982903
Reading from 5309: heap size 311 MB, throughput 0.665439
Reading from 5309: heap size 312 MB, throughput 0.664429
Reading from 5309: heap size 313 MB, throughput 0.633731
Reading from 5309: heap size 319 MB, throughput 0.591758
Reading from 5309: heap size 322 MB, throughput 0.601206
Reading from 5309: heap size 332 MB, throughput 0.512767
Reading from 5310: heap size 337 MB, throughput 0.981081
Reading from 5309: heap size 337 MB, throughput 0.444735
Reading from 5309: heap size 391 MB, throughput 0.392871
Reading from 5309: heap size 399 MB, throughput 0.452911
Numeric result:
Recommendation: 2 clients, utility 0.684415:
    h1: 6295 MB (U(h) = 0.916358*h^0.00822618)
    h2: 1223 MB (U(h) = 0.690106*h^0.001)
Recommendation: 2 clients, utility 0.684415:
    h1: 6295 MB (U(h) = 0.916358*h^0.00822618)
    h2: 1223 MB (U(h) = 0.690106*h^0.001)
Reading from 5310: heap size 382 MB, throughput 0.977029
Reading from 5309: heap size 399 MB, throughput 0.164011
Reading from 5309: heap size 450 MB, throughput 0.448996
Reading from 5309: heap size 454 MB, throughput 0.52631
Reading from 5309: heap size 446 MB, throughput 0.583762
Reading from 5309: heap size 451 MB, throughput 0.582049
Reading from 5309: heap size 451 MB, throughput 0.587549
Reading from 5310: heap size 383 MB, throughput 0.96061
Reading from 5309: heap size 452 MB, throughput 0.591298
Reading from 5309: heap size 457 MB, throughput 0.349793
Reading from 5309: heap size 520 MB, throughput 0.513966
Reading from 5309: heap size 527 MB, throughput 0.527588
Reading from 5310: heap size 440 MB, throughput 0.983648
Reading from 5309: heap size 529 MB, throughput 0.53427
Reading from 5309: heap size 531 MB, throughput 0.565392
Reading from 5309: heap size 538 MB, throughput 0.521206
Reading from 5309: heap size 542 MB, throughput 0.53863
Reading from 5309: heap size 552 MB, throughput 0.539235
Reading from 5309: heap size 558 MB, throughput 0.513583
Reading from 5310: heap size 441 MB, throughput 0.984885
Reading from 5309: heap size 569 MB, throughput 0.338657
Reading from 5309: heap size 640 MB, throughput 0.483717
Reading from 5309: heap size 644 MB, throughput 0.530761
Reading from 5309: heap size 642 MB, throughput 0.530414
Reading from 5310: heap size 480 MB, throughput 0.986388
Reading from 5309: heap size 644 MB, throughput 0.214054
Reading from 5310: heap size 483 MB, throughput 0.98845
Reading from 5309: heap size 712 MB, throughput 0.770672
Reading from 5309: heap size 712 MB, throughput 0.829296
Reading from 5309: heap size 709 MB, throughput 0.832
Reading from 5310: heap size 522 MB, throughput 0.982961
Reading from 5309: heap size 717 MB, throughput 0.75125
Reading from 5309: heap size 725 MB, throughput 0.705049
Reading from 5309: heap size 736 MB, throughput 0.629277
Numeric result:
Recommendation: 2 clients, utility 0.598075:
    h1: 6295 MB (U(h) = 0.904188*h^0.0120966)
    h2: 1223 MB (U(h) = 0.590819*h^0.001)
Recommendation: 2 clients, utility 0.598075:
    h1: 6295 MB (U(h) = 0.904188*h^0.0120966)
    h2: 1223 MB (U(h) = 0.590819*h^0.001)
Reading from 5309: heap size 746 MB, throughput 0.510122
Reading from 5310: heap size 525 MB, throughput 0.986423
Reading from 5309: heap size 747 MB, throughput 0.496136
Reading from 5309: heap size 808 MB, throughput 0.321272
Reading from 5309: heap size 816 MB, throughput 0.663856
Reading from 5309: heap size 811 MB, throughput 0.675274
Reading from 5309: heap size 816 MB, throughput 0.62672
Reading from 5309: heap size 812 MB, throughput 0.615494
Reading from 5310: heap size 568 MB, throughput 0.987462
Reading from 5309: heap size 815 MB, throughput 0.368347
Reading from 5309: heap size 896 MB, throughput 0.680929
Reading from 5310: heap size 572 MB, throughput 0.988793
Reading from 5309: heap size 897 MB, throughput 0.782131
Reading from 5309: heap size 905 MB, throughput 0.826806
Reading from 5309: heap size 907 MB, throughput 0.843241
Reading from 5309: heap size 912 MB, throughput 0.895183
Reading from 5309: heap size 915 MB, throughput 0.876334
Reading from 5309: heap size 902 MB, throughput 0.854125
Reading from 5310: heap size 617 MB, throughput 0.991055
Reading from 5309: heap size 776 MB, throughput 0.843321
Reading from 5309: heap size 894 MB, throughput 0.840693
Reading from 5309: heap size 782 MB, throughput 0.846745
Reading from 5309: heap size 886 MB, throughput 0.60583
Reading from 5310: heap size 619 MB, throughput 0.988744
Reading from 5309: heap size 981 MB, throughput 0.652774
Reading from 5309: heap size 980 MB, throughput 0.729644
Reading from 5309: heap size 981 MB, throughput 0.742192
Reading from 5309: heap size 981 MB, throughput 0.780935
Numeric result:
Recommendation: 2 clients, utility 0.626092:
    h1: 1977.82 MB (U(h) = 0.899063*h^0.0136311)
    h2: 5540.18 MB (U(h) = 0.451942*h^0.0381553)
Recommendation: 2 clients, utility 0.626092:
    h1: 1978.87 MB (U(h) = 0.899063*h^0.0136311)
    h2: 5539.13 MB (U(h) = 0.451942*h^0.0381553)
Reading from 5310: heap size 650 MB, throughput 0.989916
Reading from 5309: heap size 984 MB, throughput 0.933941
Reading from 5309: heap size 970 MB, throughput 0.928529
Reading from 5309: heap size 979 MB, throughput 0.889915
Reading from 5309: heap size 989 MB, throughput 0.841641
Reading from 5310: heap size 653 MB, throughput 0.991689
Reading from 5309: heap size 994 MB, throughput 0.796901
Reading from 5309: heap size 991 MB, throughput 0.797904
Reading from 5309: heap size 999 MB, throughput 0.785016
Reading from 5309: heap size 988 MB, throughput 0.783464
Reading from 5309: heap size 995 MB, throughput 0.805149
Reading from 5309: heap size 987 MB, throughput 0.847451
Reading from 5309: heap size 993 MB, throughput 0.847304
Reading from 5309: heap size 990 MB, throughput 0.860444
Reading from 5310: heap size 680 MB, throughput 0.990369
Reading from 5309: heap size 996 MB, throughput 0.812439
Reading from 5309: heap size 988 MB, throughput 0.751512
Reading from 5309: heap size 1005 MB, throughput 0.698715
Reading from 5310: heap size 685 MB, throughput 0.988901
Reading from 5309: heap size 1019 MB, throughput 0.665826
Reading from 5309: heap size 1132 MB, throughput 0.589333
Reading from 5309: heap size 1141 MB, throughput 0.674819
Reading from 5309: heap size 1143 MB, throughput 0.698121
Reading from 5309: heap size 1154 MB, throughput 0.730731
Reading from 5309: heap size 1156 MB, throughput 0.753832
Reading from 5309: heap size 1160 MB, throughput 0.732665
Reading from 5309: heap size 1164 MB, throughput 0.707278
Reading from 5310: heap size 722 MB, throughput 0.985578
Numeric result:
Recommendation: 2 clients, utility 0.976381:
    h1: 533.047 MB (U(h) = 0.896647*h^0.0143266)
    h2: 6984.95 MB (U(h) = 0.188917*h^0.187729)
Recommendation: 2 clients, utility 0.976381:
    h1: 533.059 MB (U(h) = 0.896647*h^0.0143266)
    h2: 6984.94 MB (U(h) = 0.188917*h^0.187729)
Reading from 5309: heap size 1166 MB, throughput 0.940533
Reading from 5310: heap size 723 MB, throughput 0.989273
Reading from 5310: heap size 666 MB, throughput 0.991822
Reading from 5309: heap size 1172 MB, throughput 0.96182
Reading from 5310: heap size 630 MB, throughput 0.990229
Reading from 5309: heap size 1167 MB, throughput 0.968933
Reading from 5310: heap size 612 MB, throughput 0.989921
Reading from 5310: heap size 581 MB, throughput 0.991868
Numeric result:
Recommendation: 2 clients, utility 1.03437:
    h1: 506.347 MB (U(h) = 0.894819*h^0.0148687)
    h2: 7011.65 MB (U(h) = 0.170172*h^0.205897)
Recommendation: 2 clients, utility 1.03437:
    h1: 506.34 MB (U(h) = 0.894819*h^0.0148687)
    h2: 7011.66 MB (U(h) = 0.170172*h^0.205897)
Reading from 5309: heap size 1175 MB, throughput 0.971474
Reading from 5310: heap size 547 MB, throughput 0.988302
Reading from 5310: heap size 534 MB, throughput 0.989197
Reading from 5309: heap size 1180 MB, throughput 0.972254
Reading from 5310: heap size 505 MB, throughput 0.991744
Reading from 5310: heap size 524 MB, throughput 0.988579
Reading from 5309: heap size 1183 MB, throughput 0.972314
Reading from 5310: heap size 510 MB, throughput 0.991412
Reading from 5310: heap size 483 MB, throughput 0.986661
Numeric result:
Recommendation: 2 clients, utility 1.01724:
    h1: 551.206 MB (U(h) = 0.893693*h^0.0152558)
    h2: 6966.79 MB (U(h) = 0.187657*h^0.19283)
Recommendation: 2 clients, utility 1.01724:
    h1: 551.183 MB (U(h) = 0.893693*h^0.0152558)
    h2: 6966.82 MB (U(h) = 0.187657*h^0.19283)
Reading from 5309: heap size 1180 MB, throughput 0.973033
Reading from 5310: heap size 517 MB, throughput 0.990989
Reading from 5310: heap size 528 MB, throughput 0.988384
Reading from 5309: heap size 1185 MB, throughput 0.973676
Reading from 5310: heap size 568 MB, throughput 0.992866
Reading from 5310: heap size 524 MB, throughput 0.993662
Reading from 5310: heap size 552 MB, throughput 0.989623
Reading from 5309: heap size 1183 MB, throughput 0.973669
Reading from 5310: heap size 524 MB, throughput 0.993117
Numeric result:
Recommendation: 2 clients, utility 1.04687:
    h1: 531.853 MB (U(h) = 0.892972*h^0.0154968)
    h2: 6986.15 MB (U(h) = 0.175637*h^0.203472)
Recommendation: 2 clients, utility 1.04687:
    h1: 532.061 MB (U(h) = 0.892972*h^0.0154968)
    h2: 6985.94 MB (U(h) = 0.175637*h^0.203472)
Reading from 5310: heap size 534 MB, throughput 0.99085
Reading from 5309: heap size 1186 MB, throughput 0.972742
Reading from 5310: heap size 504 MB, throughput 0.993098
Reading from 5310: heap size 524 MB, throughput 0.987252
Reading from 5309: heap size 1191 MB, throughput 0.973099
Reading from 5310: heap size 524 MB, throughput 0.989042
Reading from 5310: heap size 546 MB, throughput 0.99195
Reading from 5310: heap size 506 MB, throughput 0.989373
Reading from 5309: heap size 1192 MB, throughput 0.972561
Reading from 5310: heap size 524 MB, throughput 0.992408
Numeric result:
Recommendation: 2 clients, utility 1.15754:
    h1: 454.792 MB (U(h) = 0.892419*h^0.0156898)
    h2: 7063.21 MB (U(h) = 0.135992*h^0.243636)
Recommendation: 2 clients, utility 1.15754:
    h1: 454.855 MB (U(h) = 0.892419*h^0.0156898)
    h2: 7063.14 MB (U(h) = 0.135992*h^0.243636)
Reading from 5310: heap size 527 MB, throughput 0.992996
Reading from 5309: heap size 1198 MB, throughput 0.971977
Reading from 5310: heap size 488 MB, throughput 0.994471
Reading from 5310: heap size 464 MB, throughput 0.990443
Reading from 5310: heap size 456 MB, throughput 0.992425
Reading from 5309: heap size 1201 MB, throughput 0.971914
Reading from 5310: heap size 430 MB, throughput 0.988341
Reading from 5310: heap size 460 MB, throughput 0.991918
Reading from 5309: heap size 1206 MB, throughput 0.973511
Reading from 5310: heap size 431 MB, throughput 0.988198
Numeric result:
Recommendation: 2 clients, utility 1.36288:
    h1: 366.055 MB (U(h) = 0.891616*h^0.0160398)
    h2: 7151.94 MB (U(h) = 0.0861265*h^0.313412)
Recommendation: 2 clients, utility 1.36288:
    h1: 366.023 MB (U(h) = 0.891616*h^0.0160398)
    h2: 7151.98 MB (U(h) = 0.0861265*h^0.313412)
Reading from 5310: heap size 451 MB, throughput 0.990304
Reading from 5310: heap size 417 MB, throughput 0.986921
Reading from 5309: heap size 1211 MB, throughput 0.970938
Reading from 5310: heap size 413 MB, throughput 0.990495
Reading from 5310: heap size 384 MB, throughput 0.98805
Reading from 5310: heap size 372 MB, throughput 0.990051
Reading from 5310: heap size 356 MB, throughput 0.99158
Reading from 5309: heap size 1216 MB, throughput 0.972346
Reading from 5310: heap size 377 MB, throughput 0.983951
Reading from 5310: heap size 357 MB, throughput 0.988228
Reading from 5310: heap size 366 MB, throughput 0.990829
Numeric result:
Recommendation: 2 clients, utility 1.43401:
    h1: 347.438 MB (U(h) = 0.891847*h^0.0161921)
    h2: 7170.56 MB (U(h) = 0.0753464*h^0.334079)
Recommendation: 2 clients, utility 1.43401:
    h1: 347.537 MB (U(h) = 0.891847*h^0.0161921)
    h2: 7170.46 MB (U(h) = 0.0753464*h^0.334079)
Reading from 5310: heap size 369 MB, throughput 0.982915
Reading from 5309: heap size 1221 MB, throughput 0.971664
Reading from 5310: heap size 348 MB, throughput 0.987587
Reading from 5310: heap size 326 MB, throughput 0.991651
Reading from 5310: heap size 350 MB, throughput 0.990592
Reading from 5310: heap size 325 MB, throughput 0.99062
Reading from 5310: heap size 342 MB, throughput 0.9834
Reading from 5309: heap size 1227 MB, throughput 0.942947
Reading from 5310: heap size 341 MB, throughput 0.987635
Reading from 5310: heap size 351 MB, throughput 0.990492
Reading from 5310: heap size 327 MB, throughput 0.986513
Numeric result:
Recommendation: 2 clients, utility 1.60644:
    h1: 303.556 MB (U(h) = 0.892693*h^0.0161978)
    h2: 7214.44 MB (U(h) = 0.0536738*h^0.384944)
Recommendation: 2 clients, utility 1.60644:
    h1: 303.571 MB (U(h) = 0.892693*h^0.0161978)
    h2: 7214.43 MB (U(h) = 0.0536738*h^0.384944)
Reading from 5310: heap size 342 MB, throughput 0.987634
Reading from 5309: heap size 1296 MB, throughput 0.983755
Reading from 5310: heap size 316 MB, throughput 0.990159
Reading from 5310: heap size 305 MB, throughput 0.990701
Reading from 5310: heap size 290 MB, throughput 0.988574
Reading from 5310: heap size 308 MB, throughput 0.989931
Reading from 5310: heap size 287 MB, throughput 0.991733
Reading from 5309: heap size 1295 MB, throughput 0.986524
Reading from 5310: heap size 299 MB, throughput 0.981727
Reading from 5310: heap size 298 MB, throughput 0.978311
Reading from 5310: heap size 313 MB, throughput 0.982794
Reading from 5310: heap size 298 MB, throughput 0.978931
Reading from 5309: heap size 1304 MB, throughput 0.987238
Reading from 5310: heap size 305 MB, throughput 0.978571
Reading from 5310: heap size 290 MB, throughput 0.98212
Numeric result:
Recommendation: 2 clients, utility 1.934:
    h1: 249.317 MB (U(h) = 0.893736*h^0.0161138)
    h2: 7268.68 MB (U(h) = 0.030361*h^0.469851)
Recommendation: 2 clients, utility 1.934:
    h1: 249.285 MB (U(h) = 0.893736*h^0.0161138)
    h2: 7268.72 MB (U(h) = 0.030361*h^0.469851)
Reading from 5310: heap size 299 MB, throughput 0.984298
Reading from 5310: heap size 279 MB, throughput 0.972663
Reading from 5310: heap size 279 MB, throughput 0.974786
Reading from 5309: heap size 1311 MB, throughput 0.984778
Reading from 5310: heap size 275 MB, throughput 0.976466
Reading from 5310: heap size 265 MB, throughput 0.965816
Reading from 5310: heap size 256 MB, throughput 0.965218
Reading from 5310: heap size 258 MB, throughput 0.987643
Reading from 5310: heap size 234 MB, throughput 0.977589
Reading from 5309: heap size 1312 MB, throughput 0.986244
Reading from 5310: heap size 252 MB, throughput 0.985555
Reading from 5310: heap size 211 MB, throughput 0.986224
Reading from 5310: heap size 249 MB, throughput 0.982645
Reading from 5310: heap size 248 MB, throughput 0.983408
Reading from 5310: heap size 258 MB, throughput 0.982119
Reading from 5310: heap size 231 MB, throughput 0.982703
Reading from 5309: heap size 1304 MB, throughput 0.98306
Reading from 5310: heap size 253 MB, throughput 0.979858
Reading from 5310: heap size 228 MB, throughput 0.973659
Numeric result:
Recommendation: 2 clients, utility 2.04711:
    h1: 234.668 MB (U(h) = 0.894671*h^0.0159712)
    h2: 7283.33 MB (U(h) = 0.0255318*h^0.495695)
Recommendation: 2 clients, utility 2.04711:
    h1: 234.668 MB (U(h) = 0.894671*h^0.0159712)
    h2: 7283.33 MB (U(h) = 0.0255318*h^0.495695)
Reading from 5310: heap size 245 MB, throughput 0.977956
Reading from 5310: heap size 226 MB, throughput 0.98146
Reading from 5310: heap size 237 MB, throughput 0.982142
Reading from 5310: heap size 218 MB, throughput 0.974775
Reading from 5310: heap size 229 MB, throughput 0.97527
Reading from 5309: heap size 1202 MB, throughput 0.984203
Reading from 5310: heap size 230 MB, throughput 0.978063
Reading from 5310: heap size 242 MB, throughput 0.981145
Reading from 5310: heap size 221 MB, throughput 0.978833
Reading from 5310: heap size 238 MB, throughput 0.977726
Reading from 5310: heap size 217 MB, throughput 0.981919
Reading from 5310: heap size 231 MB, throughput 0.98512
Reading from 5310: heap size 231 MB, throughput 0.983956
Reading from 5309: heap size 1291 MB, throughput 0.983871
Reading from 5310: heap size 243 MB, throughput 0.978271
Reading from 5310: heap size 225 MB, throughput 0.977369
Reading from 5310: heap size 238 MB, throughput 0.983691
Reading from 5310: heap size 218 MB, throughput 0.986625
Reading from 5310: heap size 231 MB, throughput 0.988442
Numeric result:
Recommendation: 2 clients, utility 2.21983:
    h1: 248.289 MB (U(h) = 0.883477*h^0.0182335)
    h2: 7269.71 MB (U(h) = 0.0197373*h^0.533772)
Recommendation: 2 clients, utility 2.21983:
    h1: 248.33 MB (U(h) = 0.883477*h^0.0182335)
    h2: 7269.67 MB (U(h) = 0.0197373*h^0.533772)
Reading from 5310: heap size 231 MB, throughput 0.980669
Reading from 5310: heap size 243 MB, throughput 0.981231
Reading from 5309: heap size 1221 MB, throughput 0.981697
Reading from 5310: heap size 243 MB, throughput 0.981652
Reading from 5310: heap size 257 MB, throughput 0.985549
Reading from 5310: heap size 240 MB, throughput 0.984623
Reading from 5310: heap size 250 MB, throughput 0.982848
Reading from 5310: heap size 236 MB, throughput 0.987597
Reading from 5310: heap size 242 MB, throughput 0.988751
Reading from 5309: heap size 1288 MB, throughput 0.980484
Reading from 5310: heap size 243 MB, throughput 0.988474
Reading from 5310: heap size 253 MB, throughput 0.981146
Reading from 5310: heap size 244 MB, throughput 0.983003
Reading from 5310: heap size 248 MB, throughput 0.987103
Reading from 5310: heap size 251 MB, throughput 0.987664
Reading from 5309: heap size 1292 MB, throughput 0.978424
Reading from 5310: heap size 236 MB, throughput 0.983574
Reading from 5310: heap size 252 MB, throughput 0.983228
Numeric result:
Recommendation: 2 clients, utility 2.21627:
    h1: 145.224 MB (U(h) = 0.926595*h^0.0104125)
    h2: 7372.78 MB (U(h) = 0.0204925*h^0.52865)
Recommendation: 2 clients, utility 2.21627:
    h1: 145.217 MB (U(h) = 0.926595*h^0.0104125)
    h2: 7372.78 MB (U(h) = 0.0204925*h^0.52865)
Reading from 5310: heap size 242 MB, throughput 0.984495
Reading from 5310: heap size 233 MB, throughput 0.954231
Reading from 5310: heap size 216 MB, throughput 0.987085
Reading from 5310: heap size 208 MB, throughput 0.984843
Reading from 5310: heap size 208 MB, throughput 0.980592
Reading from 5309: heap size 1293 MB, throughput 0.9785
Reading from 5310: heap size 201 MB, throughput 0.979866
Reading from 5310: heap size 187 MB, throughput 0.987236
Reading from 5310: heap size 179 MB, throughput 0.989575
Reading from 5310: heap size 188 MB, throughput 0.990103
Reading from 5310: heap size 173 MB, throughput 0.985988
Reading from 5310: heap size 181 MB, throughput 0.981442
Reading from 5310: heap size 171 MB, throughput 0.983378
Reading from 5310: heap size 172 MB, throughput 0.979369
Reading from 5310: heap size 164 MB, throughput 0.977755
Reading from 5310: heap size 165 MB, throughput 0.974278
Reading from 5309: heap size 1295 MB, throughput 0.978112
Reading from 5310: heap size 157 MB, throughput 0.977967
Reading from 5310: heap size 158 MB, throughput 0.9718
Reading from 5310: heap size 153 MB, throughput 0.970749
Reading from 5310: heap size 153 MB, throughput 0.926379
Reading from 5310: heap size 147 MB, throughput 0.810388
Reading from 5310: heap size 148 MB, throughput 0.91462
Reading from 5310: heap size 145 MB, throughput 0.94725
Reading from 5310: heap size 148 MB, throughput 0.960349
Reading from 5310: heap size 139 MB, throughput 0.847113
Reading from 5310: heap size 146 MB, throughput 0.536009
Numeric result:
Recommendation: 2 clients, utility 2.34023:
    h1: 697.253 MB (U(h) = 0.707183*h^0.0564891)
    h2: 6820.75 MB (U(h) = 0.0173818*h^0.552714)
Recommendation: 2 clients, utility 2.34023:
    h1: 697.116 MB (U(h) = 0.707183*h^0.0564891)
    h2: 6820.88 MB (U(h) = 0.0173818*h^0.552714)
Reading from 5310: heap size 129 MB, throughput 0.60177
Reading from 5310: heap size 144 MB, throughput 0.505723
Reading from 5310: heap size 145 MB, throughput 0.978515
Reading from 5310: heap size 153 MB, throughput 0.678646
Reading from 5309: heap size 1298 MB, throughput 0.974234
Reading from 5310: heap size 153 MB, throughput 0.529971
Reading from 5310: heap size 163 MB, throughput 0.965714
Reading from 5310: heap size 164 MB, throughput 0.874922
Reading from 5310: heap size 176 MB, throughput 0.418086
Reading from 5310: heap size 177 MB, throughput 0.419854
Reading from 5310: heap size 190 MB, throughput 0.993009
Reading from 5310: heap size 190 MB, throughput 0.561854
Reading from 5310: heap size 202 MB, throughput 0.954487
Reading from 5310: heap size 199 MB, throughput 0.897541
Reading from 5309: heap size 1304 MB, throughput 0.971321
Reading from 5310: heap size 210 MB, throughput 0.867097
Reading from 5310: heap size 213 MB, throughput 0.914612
Reading from 5310: heap size 225 MB, throughput 0.961689
Reading from 5310: heap size 227 MB, throughput 0.969754
Reading from 5310: heap size 239 MB, throughput 0.971101
Reading from 5310: heap size 241 MB, throughput 0.969047
Reading from 5309: heap size 1307 MB, throughput 0.970402
Reading from 5310: heap size 257 MB, throughput 0.977551
Reading from 5310: heap size 258 MB, throughput 0.976769
Numeric result:
Recommendation: 2 clients, utility 3.22971:
    h1: 1847.22 MB (U(h) = 0.307753*h^0.201006)
    h2: 5670.78 MB (U(h) = 0.0111664*h^0.617123)
Recommendation: 2 clients, utility 3.22971:
    h1: 1847.09 MB (U(h) = 0.307753*h^0.201006)
    h2: 5670.91 MB (U(h) = 0.0111664*h^0.617123)
Reading from 5310: heap size 278 MB, throughput 0.982179
Reading from 5310: heap size 279 MB, throughput 0.985026
Reading from 5310: heap size 297 MB, throughput 0.983522
Reading from 5310: heap size 299 MB, throughput 0.981231
Reading from 5309: heap size 1316 MB, throughput 0.970818
Reading from 5310: heap size 320 MB, throughput 0.987423
Reading from 5310: heap size 320 MB, throughput 0.984473
Reading from 5310: heap size 342 MB, throughput 0.984112
Reading from 5310: heap size 342 MB, throughput 0.987576
Reading from 5309: heap size 1322 MB, throughput 0.968798
Reading from 5310: heap size 362 MB, throughput 0.98769
Reading from 5310: heap size 364 MB, throughput 0.988154
Reading from 5310: heap size 386 MB, throughput 0.991293
Numeric result:
Recommendation: 2 clients, utility 3.88672:
    h1: 1909.77 MB (U(h) = 0.259393*h^0.23284)
    h2: 5608.23 MB (U(h) = 0.00705372*h^0.683751)
Recommendation: 2 clients, utility 3.88672:
    h1: 1909.78 MB (U(h) = 0.259393*h^0.23284)
    h2: 5608.22 MB (U(h) = 0.00705372*h^0.683751)
Reading from 5310: heap size 387 MB, throughput 0.984046
Reading from 5309: heap size 1327 MB, throughput 0.969011
Reading from 5310: heap size 406 MB, throughput 0.986845
Reading from 5310: heap size 410 MB, throughput 0.984562
Reading from 5310: heap size 436 MB, throughput 0.985175
Reading from 5309: heap size 1334 MB, throughput 0.967664
Reading from 5310: heap size 438 MB, throughput 0.985285
Reading from 5310: heap size 469 MB, throughput 0.987521
Reading from 5310: heap size 470 MB, throughput 0.986928
Reading from 5309: heap size 1337 MB, throughput 0.965652
Numeric result:
Recommendation: 2 clients, utility 4.13742:
    h1: 1811.74 MB (U(h) = 0.263618*h^0.229838)
    h2: 5706.26 MB (U(h) = 0.0053438*h^0.723859)
Recommendation: 2 clients, utility 4.13742:
    h1: 1811.81 MB (U(h) = 0.263618*h^0.229838)
    h2: 5706.19 MB (U(h) = 0.0053438*h^0.723859)
Reading from 5310: heap size 503 MB, throughput 0.988199
Reading from 5310: heap size 503 MB, throughput 0.987077
Reading from 5310: heap size 536 MB, throughput 0.989391
Reading from 5309: heap size 1344 MB, throughput 0.969903
Reading from 5310: heap size 537 MB, throughput 0.985708
Reading from 5310: heap size 566 MB, throughput 0.984566
Reading from 5309: heap size 1345 MB, throughput 0.969819
Reading from 5310: heap size 570 MB, throughput 0.981678
Numeric result:
Recommendation: 2 clients, utility 3.68461:
    h1: 1630.98 MB (U(h) = 0.320294*h^0.193788)
    h2: 5887.02 MB (U(h) = 0.00632928*h^0.699476)
Recommendation: 2 clients, utility 3.68461:
    h1: 1630.99 MB (U(h) = 0.320294*h^0.193788)
    h2: 5887.01 MB (U(h) = 0.00632928*h^0.699476)
Reading from 5310: heap size 605 MB, throughput 0.983867
Reading from 5310: heap size 614 MB, throughput 0.986137
Reading from 5310: heap size 658 MB, throughput 0.988821
Reading from 5310: heap size 657 MB, throughput 0.985022
Reading from 5310: heap size 703 MB, throughput 0.98569
Reading from 5310: heap size 708 MB, throughput 0.981665
Numeric result:
Recommendation: 2 clients, utility 3.38941:
    h1: 1327.12 MB (U(h) = 0.405723*h^0.149956)
    h2: 6190.88 MB (U(h) = 0.00632928*h^0.699476)
Recommendation: 2 clients, utility 3.38941:
    h1: 1327.2 MB (U(h) = 0.405723*h^0.149956)
    h2: 6190.8 MB (U(h) = 0.00632928*h^0.699476)
Reading from 5310: heap size 762 MB, throughput 0.982352
Reading from 5310: heap size 769 MB, throughput 0.980253
Reading from 5309: heap size 1351 MB, throughput 0.984997
Reading from 5310: heap size 836 MB, throughput 0.981188
Reading from 5310: heap size 844 MB, throughput 0.980692
Numeric result:
Recommendation: 2 clients, utility 3.30971:
    h1: 1123.57 MB (U(h) = 0.464368*h^0.124853)
    h2: 6394.43 MB (U(h) = 0.00585744*h^0.710584)
Recommendation: 2 clients, utility 3.30971:
    h1: 1123.54 MB (U(h) = 0.464368*h^0.124853)
    h2: 6394.46 MB (U(h) = 0.00585744*h^0.710584)
Reading from 5310: heap size 922 MB, throughput 0.982601
Reading from 5310: heap size 927 MB, throughput 0.972544
Reading from 5310: heap size 937 MB, throughput 0.986978
Reading from 5310: heap size 1027 MB, throughput 0.987195
Numeric result:
Recommendation: 2 clients, utility 3.20916:
    h1: 972.95 MB (U(h) = 0.514626*h^0.105641)
    h2: 6545.05 MB (U(h) = 0.00585744*h^0.710584)
Recommendation: 2 clients, utility 3.20916:
    h1: 973.023 MB (U(h) = 0.514626*h^0.105641)
    h2: 6544.98 MB (U(h) = 0.00585744*h^0.710584)
Reading from 5310: heap size 1025 MB, throughput 0.987191
Reading from 5309: heap size 1351 MB, throughput 0.989114
Reading from 5310: heap size 945 MB, throughput 0.987918
Reading from 5310: heap size 1043 MB, throughput 0.987702
Reading from 5310: heap size 998 MB, throughput 0.972096
Numeric result:
Recommendation: 2 clients, utility 3.15287:
    h1: 880.752 MB (U(h) = 0.546243*h^0.0942976)
    h2: 6637.25 MB (U(h) = 0.00585442*h^0.710663)
Recommendation: 2 clients, utility 3.15287:
    h1: 880.701 MB (U(h) = 0.546243*h^0.0942976)
    h2: 6637.3 MB (U(h) = 0.00585442*h^0.710663)
Reading from 5309: heap size 1353 MB, throughput 0.97441
Reading from 5309: heap size 1407 MB, throughput 0.986103
Reading from 5309: heap size 1433 MB, throughput 0.982299
Reading from 5309: heap size 1442 MB, throughput 0.974315
Reading from 5309: heap size 1435 MB, throughput 0.959188
Reading from 5309: heap size 1438 MB, throughput 0.946446
Reading from 5309: heap size 1434 MB, throughput 0.933009
Reading from 5310: heap size 946 MB, throughput 0.99197
Reading from 5309: heap size 1440 MB, throughput 0.91917
Reading from 5309: heap size 1439 MB, throughput 0.979939
Reading from 5310: heap size 894 MB, throughput 0.994263
Reading from 5310: heap size 861 MB, throughput 0.993125
Reading from 5309: heap size 1445 MB, throughput 0.990526
Numeric result:
Recommendation: 2 clients, utility 3.61161:
    h1: 783.083 MB (U(h) = 0.550327*h^0.0923894)
    h2: 6734.92 MB (U(h) = 0.00321744*h^0.794657)
Recommendation: 2 clients, utility 3.61161:
    h1: 783.029 MB (U(h) = 0.550327*h^0.0923894)
    h2: 6734.97 MB (U(h) = 0.00321744*h^0.794657)
Reading from 5310: heap size 939 MB, throughput 0.991822
Reading from 5310: heap size 854 MB, throughput 0.988758
Reading from 5309: heap size 1466 MB, throughput 0.99081
Client 5310 died
Clients: 1
Reading from 5309: heap size 1468 MB, throughput 0.992004
Recommendation: one client; give it all the memory
Reading from 5309: heap size 1463 MB, throughput 0.992019
Reading from 5309: heap size 1471 MB, throughput 0.991545
Recommendation: one client; give it all the memory
Reading from 5309: heap size 1450 MB, throughput 0.990708
Reading from 5309: heap size 1343 MB, throughput 0.990183
Recommendation: one client; give it all the memory
Reading from 5309: heap size 1439 MB, throughput 0.989443
Reading from 5309: heap size 1448 MB, throughput 0.988365
Recommendation: one client; give it all the memory
Reading from 5309: heap size 1449 MB, throughput 0.987591
Recommendation: one client; give it all the memory
Reading from 5309: heap size 1450 MB, throughput 0.986621
Reading from 5309: heap size 1453 MB, throughput 0.985943
Recommendation: one client; give it all the memory
Reading from 5309: heap size 1458 MB, throughput 0.984983
Reading from 5309: heap size 1461 MB, throughput 0.984062
Recommendation: one client; give it all the memory
Reading from 5309: heap size 1471 MB, throughput 0.982641
Reading from 5309: heap size 1477 MB, throughput 0.981432
Recommendation: one client; give it all the memory
Reading from 5309: heap size 1488 MB, throughput 0.980454
Reading from 5309: heap size 1497 MB, throughput 0.980985
Recommendation: one client; give it all the memory
Reading from 5309: heap size 1503 MB, throughput 0.980398
Reading from 5309: heap size 1511 MB, throughput 0.980894
Recommendation: one client; give it all the memory
Reading from 5309: heap size 1514 MB, throughput 0.980242
Reading from 5309: heap size 1522 MB, throughput 0.980212
Recommendation: one client; give it all the memory
Reading from 5309: heap size 1523 MB, throughput 0.980721
Reading from 5309: heap size 1529 MB, throughput 0.981075
Recommendation: one client; give it all the memory
Reading from 5309: heap size 1529 MB, throughput 0.980588
Recommendation: one client; give it all the memory
Reading from 5309: heap size 1533 MB, throughput 0.973006
Reading from 5309: heap size 1641 MB, throughput 0.988493
Recommendation: one client; give it all the memory
Reading from 5309: heap size 1634 MB, throughput 0.991594
Reading from 5309: heap size 1646 MB, throughput 0.992576
Recommendation: one client; give it all the memory
Reading from 5309: heap size 1658 MB, throughput 0.992631
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 5309: heap size 1659 MB, throughput 0.994781
Recommendation: one client; give it all the memory
Reading from 5309: heap size 1634 MB, throughput 0.994248
Recommendation: one client; give it all the memory
Reading from 5309: heap size 1647 MB, throughput 0.992182
Reading from 5309: heap size 1660 MB, throughput 0.984504
Reading from 5309: heap size 1661 MB, throughput 0.966309
Reading from 5309: heap size 1687 MB, throughput 0.944261
Reading from 5309: heap size 1711 MB, throughput 0.908692
Reading from 5309: heap size 1752 MB, throughput 0.891314
Client 5309 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
