#!/bin/bash
# Download and build Jikes RVM

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
VM_DIR="${DIR}/jikesrvm"

mkdir -p "${VM_DIR}" || exit 1

function download()
{
    if [ -e "${VM_DIR}/download.done" ]; then
        return 0
    else
        cd "${VM_DIR}" &&
        hg clone http://hg.code.sourceforge.net/p/jikesrvm/code jikesrvm &&
        cd "${VM_DIR}/jikesrvm" &&
        hg checkout 527b36e231ea &&
        touch "${VM_DIR}/download.done" &&
        return 0

        return 1
    fi
}

function build()
{
    if [ -e "${VM_DIR}/install.done" ]; then
        return 0
    else
        ant -Dhost.name=x86_64-linux -Dconfig.name=production -Dcp.enable.gtk-peer="--disable-gtk-peer" &&
        touch "${VM_DIR}/install.done" &&
        return 0

        return 1
    fi
}

download || exit 1
build || exit 1
exit 0
