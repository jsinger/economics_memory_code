economemd
    total memory: 3669 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub12_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 7461: heap size 9 MB, throughput 0.986334
Clients: 1
Client 7461 has a minimum heap size of 12 MB
Reading from 7460: heap size 9 MB, throughput 0.990045
Clients: 2
Client 7460 has a minimum heap size of 1211 MB
Reading from 7460: heap size 9 MB, throughput 0.986516
Reading from 7461: heap size 9 MB, throughput 0.986787
Reading from 7460: heap size 9 MB, throughput 0.974765
Reading from 7460: heap size 9 MB, throughput 0.968627
Reading from 7461: heap size 11 MB, throughput 0.984574
Reading from 7460: heap size 11 MB, throughput 0.969094
Reading from 7461: heap size 11 MB, throughput 0.961961
Reading from 7460: heap size 11 MB, throughput 0.953764
Reading from 7461: heap size 15 MB, throughput 0.982865
Reading from 7460: heap size 17 MB, throughput 0.963119
Reading from 7460: heap size 17 MB, throughput 0.891348
Reading from 7461: heap size 15 MB, throughput 0.978565
Reading from 7460: heap size 30 MB, throughput 0.774354
Reading from 7460: heap size 31 MB, throughput 0.406346
Reading from 7461: heap size 24 MB, throughput 0.95798
Reading from 7460: heap size 35 MB, throughput 0.731556
Reading from 7460: heap size 48 MB, throughput 0.554199
Reading from 7460: heap size 50 MB, throughput 0.442698
Reading from 7460: heap size 65 MB, throughput 0.426727
Reading from 7461: heap size 29 MB, throughput 0.957913
Reading from 7460: heap size 69 MB, throughput 0.453433
Reading from 7460: heap size 71 MB, throughput 0.490873
Reading from 7461: heap size 36 MB, throughput 0.935891
Reading from 7460: heap size 99 MB, throughput 0.60739
Reading from 7460: heap size 99 MB, throughput 0.363975
Reading from 7461: heap size 43 MB, throughput 0.971741
Reading from 7460: heap size 130 MB, throughput 0.725176
Reading from 7460: heap size 129 MB, throughput 0.622067
Reading from 7461: heap size 44 MB, throughput 0.912536
Reading from 7460: heap size 131 MB, throughput 0.404259
Reading from 7460: heap size 134 MB, throughput 0.440763
Reading from 7461: heap size 45 MB, throughput 0.923679
Reading from 7461: heap size 51 MB, throughput 0.927792
Reading from 7461: heap size 51 MB, throughput 0.735162
Reading from 7460: heap size 137 MB, throughput 0.395648
Reading from 7460: heap size 175 MB, throughput 0.388487
Reading from 7461: heap size 62 MB, throughput 0.67974
Reading from 7460: heap size 180 MB, throughput 0.249991
Reading from 7461: heap size 63 MB, throughput 0.717049
Reading from 7460: heap size 182 MB, throughput 0.0327555
Reading from 7461: heap size 74 MB, throughput 0.962301
Reading from 7460: heap size 191 MB, throughput 0.698213
Reading from 7461: heap size 80 MB, throughput 0.944253
Reading from 7460: heap size 193 MB, throughput 0.690465
Reading from 7461: heap size 94 MB, throughput 0.984399
Reading from 7460: heap size 196 MB, throughput 0.417768
Reading from 7460: heap size 245 MB, throughput 0.657293
Reading from 7460: heap size 248 MB, throughput 0.695499
Reading from 7460: heap size 254 MB, throughput 0.690426
Reading from 7460: heap size 257 MB, throughput 0.70779
Reading from 7460: heap size 267 MB, throughput 0.669497
Reading from 7460: heap size 270 MB, throughput 0.638731
Reading from 7460: heap size 282 MB, throughput 0.579848
Reading from 7461: heap size 94 MB, throughput 0.993742
Reading from 7460: heap size 286 MB, throughput 0.48947
Reading from 7460: heap size 297 MB, throughput 0.410402
Reading from 7460: heap size 353 MB, throughput 0.426022
Reading from 7461: heap size 107 MB, throughput 0.992217
Reading from 7460: heap size 281 MB, throughput 0.491646
Reading from 7460: heap size 339 MB, throughput 0.19367
Reading from 7460: heap size 332 MB, throughput 0.447387
Reading from 7461: heap size 109 MB, throughput 0.99432
Reading from 7460: heap size 390 MB, throughput 0.545444
Reading from 7460: heap size 393 MB, throughput 0.559109
Reading from 7460: heap size 383 MB, throughput 0.549011
Reading from 7460: heap size 388 MB, throughput 0.553737
Reading from 7460: heap size 389 MB, throughput 0.550642
Reading from 7460: heap size 389 MB, throughput 0.567014
Reading from 7460: heap size 392 MB, throughput 0.521317
Reading from 7460: heap size 394 MB, throughput 0.525671
Reading from 7461: heap size 121 MB, throughput 0.996392
Reading from 7460: heap size 398 MB, throughput 0.527652
Reading from 7460: heap size 402 MB, throughput 0.529166
Reading from 7460: heap size 406 MB, throughput 0.524047
Reading from 7460: heap size 413 MB, throughput 0.496203
Reading from 7461: heap size 123 MB, throughput 0.996677
Reading from 7460: heap size 417 MB, throughput 0.367989
Numeric result:
Recommendation: 2 clients, utility 0.915685:
    h1: 1836.65 MB (U(h) = 0.968042*h^0.001)
    h2: 1832.35 MB (U(h) = 0.931805*h^0.001)
Recommendation: 2 clients, utility 0.915685:
    h1: 1834.5 MB (U(h) = 0.968042*h^0.001)
    h2: 1834.5 MB (U(h) = 0.931805*h^0.001)
Reading from 7460: heap size 476 MB, throughput 0.455363
Reading from 7460: heap size 483 MB, throughput 0.515813
Reading from 7461: heap size 134 MB, throughput 0.997101
Reading from 7460: heap size 484 MB, throughput 0.526452
Reading from 7460: heap size 488 MB, throughput 0.541372
Reading from 7461: heap size 135 MB, throughput 0.997188
Reading from 7460: heap size 488 MB, throughput 0.194748
Reading from 7461: heap size 141 MB, throughput 0.997264
Reading from 7460: heap size 551 MB, throughput 0.534472
Reading from 7460: heap size 553 MB, throughput 0.533362
Reading from 7460: heap size 551 MB, throughput 0.513554
Reading from 7460: heap size 552 MB, throughput 0.562575
Reading from 7460: heap size 555 MB, throughput 0.567734
Reading from 7460: heap size 565 MB, throughput 0.547943
Reading from 7461: heap size 142 MB, throughput 0.995674
Reading from 7460: heap size 568 MB, throughput 0.55361
Reading from 7460: heap size 582 MB, throughput 0.483218
Reading from 7460: heap size 589 MB, throughput 0.49151
Reading from 7461: heap size 149 MB, throughput 0.996906
Reading from 7460: heap size 598 MB, throughput 0.327293
Reading from 7460: heap size 667 MB, throughput 0.457539
Reading from 7461: heap size 149 MB, throughput 0.996694
Reading from 7460: heap size 597 MB, throughput 0.48973
Reading from 7460: heap size 661 MB, throughput 0.785096
Reading from 7460: heap size 667 MB, throughput 0.823368
Reading from 7461: heap size 154 MB, throughput 0.99739
Reading from 7460: heap size 664 MB, throughput 0.829098
Reading from 7461: heap size 155 MB, throughput 0.997521
Reading from 7461: heap size 160 MB, throughput 0.997555
Reading from 7460: heap size 668 MB, throughput 0.517011
Reading from 7460: heap size 745 MB, throughput 0.715583
Reading from 7460: heap size 753 MB, throughput 0.602805
Reading from 7460: heap size 739 MB, throughput 0.604757
Reading from 7460: heap size 676 MB, throughput 0.534812
Reading from 7461: heap size 160 MB, throughput 0.996384
Reading from 7460: heap size 736 MB, throughput 0.213437
Numeric result:
Recommendation: 2 clients, utility 0.739041:
    h1: 2458 MB (U(h) = 0.877482*h^0.0182427)
    h2: 1211 MB (U(h) = 0.72526*h^0.001)
Recommendation: 2 clients, utility 0.739041:
    h1: 2458 MB (U(h) = 0.877482*h^0.0182427)
    h2: 1211 MB (U(h) = 0.72526*h^0.001)
Reading from 7460: heap size 818 MB, throughput 0.34058
Reading from 7460: heap size 817 MB, throughput 0.38365
Reading from 7460: heap size 820 MB, throughput 0.421313
Reading from 7461: heap size 163 MB, throughput 0.997086
Reading from 7460: heap size 826 MB, throughput 0.820398
Reading from 7460: heap size 827 MB, throughput 0.771253
Reading from 7460: heap size 824 MB, throughput 0.729397
Reading from 7460: heap size 826 MB, throughput 0.701049
Reading from 7460: heap size 825 MB, throughput 0.70059
Reading from 7461: heap size 164 MB, throughput 0.997471
Reading from 7460: heap size 827 MB, throughput 0.443698
Reading from 7461: heap size 167 MB, throughput 0.997909
Reading from 7460: heap size 920 MB, throughput 0.731255
Reading from 7460: heap size 922 MB, throughput 0.846414
Reading from 7460: heap size 929 MB, throughput 0.907882
Reading from 7460: heap size 930 MB, throughput 0.912522
Reading from 7460: heap size 923 MB, throughput 0.889493
Reading from 7461: heap size 167 MB, throughput 0.997655
Reading from 7460: heap size 927 MB, throughput 0.894039
Reading from 7461: heap size 170 MB, throughput 0.988913
Reading from 7460: heap size 906 MB, throughput 0.834134
Reading from 7460: heap size 814 MB, throughput 0.797399
Reading from 7461: heap size 170 MB, throughput 0.988193
Reading from 7460: heap size 897 MB, throughput 0.751706
Reading from 7460: heap size 819 MB, throughput 0.791385
Reading from 7460: heap size 887 MB, throughput 0.823797
Reading from 7460: heap size 818 MB, throughput 0.840652
Reading from 7460: heap size 882 MB, throughput 0.854003
Reading from 7460: heap size 823 MB, throughput 0.861257
Reading from 7461: heap size 176 MB, throughput 0.993733
Reading from 7460: heap size 877 MB, throughput 0.85637
Reading from 7460: heap size 882 MB, throughput 0.868808
Reading from 7460: heap size 876 MB, throughput 0.873496
Reading from 7461: heap size 176 MB, throughput 0.9952
Reading from 7460: heap size 880 MB, throughput 0.965767
Reading from 7461: heap size 181 MB, throughput 0.996363
Reading from 7460: heap size 882 MB, throughput 0.96671
Reading from 7460: heap size 883 MB, throughput 0.936341
Reading from 7460: heap size 888 MB, throughput 0.868619
Reading from 7460: heap size 890 MB, throughput 0.846671
Reading from 7461: heap size 182 MB, throughput 0.996412
Reading from 7460: heap size 897 MB, throughput 0.831252
Reading from 7460: heap size 898 MB, throughput 0.824088
Reading from 7460: heap size 904 MB, throughput 0.82637
Reading from 7460: heap size 905 MB, throughput 0.823349
Numeric result:
Recommendation: 2 clients, utility 0.659703:
    h1: 1105.69 MB (U(h) = 0.855636*h^0.0255076)
    h2: 2563.31 MB (U(h) = 0.405289*h^0.0591584)
Recommendation: 2 clients, utility 0.659703:
    h1: 1105.37 MB (U(h) = 0.855636*h^0.0255076)
    h2: 2563.63 MB (U(h) = 0.405289*h^0.0591584)
Reading from 7460: heap size 911 MB, throughput 0.833329
Reading from 7460: heap size 912 MB, throughput 0.872924
Reading from 7461: heap size 187 MB, throughput 0.996905
Reading from 7460: heap size 918 MB, throughput 0.866609
Reading from 7460: heap size 919 MB, throughput 0.892928
Reading from 7460: heap size 921 MB, throughput 0.867034
Reading from 7460: heap size 922 MB, throughput 0.79391
Reading from 7461: heap size 187 MB, throughput 0.996966
Reading from 7461: heap size 191 MB, throughput 0.996392
Reading from 7460: heap size 905 MB, throughput 0.749408
Reading from 7460: heap size 1043 MB, throughput 0.643173
Reading from 7460: heap size 1038 MB, throughput 0.692701
Reading from 7460: heap size 1043 MB, throughput 0.720484
Reading from 7460: heap size 1041 MB, throughput 0.733471
Reading from 7460: heap size 1046 MB, throughput 0.0755847
Reading from 7460: heap size 1053 MB, throughput 0.406763
Reading from 7461: heap size 191 MB, throughput 0.996744
Reading from 7460: heap size 1054 MB, throughput 0.564849
Reading from 7460: heap size 1067 MB, throughput 0.648084
Reading from 7460: heap size 1067 MB, throughput 0.629025
Reading from 7460: heap size 1082 MB, throughput 0.641094
Reading from 7461: heap size 196 MB, throughput 0.997079
Reading from 7461: heap size 196 MB, throughput 0.996176
Reading from 7460: heap size 1084 MB, throughput 0.945689
Reading from 7461: heap size 201 MB, throughput 0.996751
Reading from 7460: heap size 1101 MB, throughput 0.959679
Reading from 7461: heap size 201 MB, throughput 0.996996
Numeric result:
Recommendation: 2 clients, utility 1.09716:
    h1: 261.292 MB (U(h) = 0.848932*h^0.0276953)
    h2: 3407.71 MB (U(h) = 0.0586775*h^0.361216)
Recommendation: 2 clients, utility 1.09716:
    h1: 261.278 MB (U(h) = 0.848932*h^0.0276953)
    h2: 3407.72 MB (U(h) = 0.0586775*h^0.361216)
Reading from 7460: heap size 1104 MB, throughput 0.964421
Reading from 7461: heap size 205 MB, throughput 0.993012
Reading from 7461: heap size 205 MB, throughput 0.988401
Reading from 7460: heap size 1111 MB, throughput 0.942817
Reading from 7461: heap size 210 MB, throughput 0.98408
Reading from 7461: heap size 210 MB, throughput 0.988728
Reading from 7460: heap size 1114 MB, throughput 0.952721
Reading from 7461: heap size 216 MB, throughput 0.993818
Reading from 7460: heap size 1112 MB, throughput 0.702081
Reading from 7461: heap size 216 MB, throughput 0.996086
Reading from 7460: heap size 1117 MB, throughput 0.792664
Reading from 7461: heap size 220 MB, throughput 0.996619
Reading from 7461: heap size 221 MB, throughput 0.997274
Reading from 7460: heap size 1116 MB, throughput 0.81813
Numeric result:
Recommendation: 2 clients, utility 0.990779:
    h1: 339.71 MB (U(h) = 0.845472*h^0.0287969)
    h2: 3329.29 MB (U(h) = 0.100445*h^0.282215)
Recommendation: 2 clients, utility 0.990779:
    h1: 339.717 MB (U(h) = 0.845472*h^0.0287969)
    h2: 3329.28 MB (U(h) = 0.100445*h^0.282215)
Reading from 7461: heap size 224 MB, throughput 0.997635
Reading from 7460: heap size 1119 MB, throughput 0.799206
Reading from 7461: heap size 225 MB, throughput 0.997693
Reading from 7460: heap size 1126 MB, throughput 0.791497
Reading from 7461: heap size 229 MB, throughput 0.997824
Reading from 7461: heap size 229 MB, throughput 0.99769
Reading from 7460: heap size 1127 MB, throughput 0.736511
Reading from 7461: heap size 218 MB, throughput 0.997748
Reading from 7460: heap size 1132 MB, throughput 0.611414
Reading from 7461: heap size 208 MB, throughput 0.997815
Reading from 7461: heap size 199 MB, throughput 0.997649
Reading from 7460: heap size 1135 MB, throughput 0.952933
Reading from 7461: heap size 191 MB, throughput 0.993214
Reading from 7461: heap size 183 MB, throughput 0.988417
Numeric result:
Recommendation: 2 clients, utility 1.13705:
    h1: 273.519 MB (U(h) = 0.841205*h^0.030168)
    h2: 3395.48 MB (U(h) = 0.054321*h^0.374519)
Recommendation: 2 clients, utility 1.13705:
    h1: 273.512 MB (U(h) = 0.841205*h^0.030168)
    h2: 3395.49 MB (U(h) = 0.054321*h^0.374519)
Reading from 7461: heap size 189 MB, throughput 0.995564
Reading from 7460: heap size 1141 MB, throughput 0.958253
Reading from 7461: heap size 195 MB, throughput 0.993545
Reading from 7460: heap size 1145 MB, throughput 0.958796
Reading from 7461: heap size 200 MB, throughput 0.9962
Reading from 7461: heap size 205 MB, throughput 0.997492
Reading from 7460: heap size 1150 MB, throughput 0.958221
Reading from 7461: heap size 214 MB, throughput 0.998215
Reading from 7461: heap size 215 MB, throughput 0.998193
Reading from 7460: heap size 1155 MB, throughput 0.960055
Reading from 7461: heap size 219 MB, throughput 0.998566
Reading from 7460: heap size 1162 MB, throughput 0.960667
Reading from 7461: heap size 223 MB, throughput 0.998828
Numeric result:
Recommendation: 2 clients, utility 1.41427:
    h1: 208.381 MB (U(h) = 0.838032*h^0.0312043)
    h2: 3460.62 MB (U(h) = 0.0209355*h^0.518211)
Recommendation: 2 clients, utility 1.41427:
    h1: 208.383 MB (U(h) = 0.838032*h^0.0312043)
    h2: 3460.62 MB (U(h) = 0.0209355*h^0.518211)
Reading from 7461: heap size 223 MB, throughput 0.998502
Reading from 7460: heap size 1166 MB, throughput 0.961121
Reading from 7461: heap size 209 MB, throughput 0.998651
Reading from 7460: heap size 1172 MB, throughput 0.96216
Reading from 7461: heap size 205 MB, throughput 0.9987
Reading from 7461: heap size 204 MB, throughput 0.998807
Reading from 7460: heap size 1174 MB, throughput 0.960314
Reading from 7461: heap size 211 MB, throughput 0.998457
Reading from 7461: heap size 200 MB, throughput 0.996512
Reading from 7461: heap size 209 MB, throughput 0.986844
Reading from 7460: heap size 1181 MB, throughput 0.959132
Reading from 7461: heap size 199 MB, throughput 0.989435
Reading from 7461: heap size 206 MB, throughput 0.99417
Numeric result:
Recommendation: 2 clients, utility 1.40739:
    h1: 215.085 MB (U(h) = 0.837017*h^0.0315356)
    h2: 3453.91 MB (U(h) = 0.0229238*h^0.506409)
Recommendation: 2 clients, utility 1.40739:
    h1: 215.086 MB (U(h) = 0.837017*h^0.0315356)
    h2: 3453.91 MB (U(h) = 0.0229238*h^0.506409)
Reading from 7460: heap size 1182 MB, throughput 0.959726
Reading from 7461: heap size 212 MB, throughput 0.99626
Reading from 7460: heap size 1188 MB, throughput 0.957494
Reading from 7461: heap size 212 MB, throughput 0.996781
Reading from 7461: heap size 217 MB, throughput 0.997361
Reading from 7460: heap size 1189 MB, throughput 0.960448
Reading from 7461: heap size 206 MB, throughput 0.997766
Reading from 7461: heap size 212 MB, throughput 0.997377
Reading from 7461: heap size 214 MB, throughput 0.994384
Reading from 7460: heap size 1195 MB, throughput 0.918855
Reading from 7461: heap size 219 MB, throughput 0.996154
Numeric result:
Recommendation: 2 clients, utility 1.52279:
    h1: 196.278 MB (U(h) = 0.836571*h^0.0316797)
    h2: 3472.72 MB (U(h) = 0.0159656*h^0.560435)
Recommendation: 2 clients, utility 1.52279:
    h1: 196.301 MB (U(h) = 0.836571*h^0.0316797)
    h2: 3472.7 MB (U(h) = 0.0159656*h^0.560435)
Reading from 7461: heap size 208 MB, throughput 0.996933
Reading from 7460: heap size 1294 MB, throughput 0.957534
Reading from 7461: heap size 200 MB, throughput 0.99729
Reading from 7461: heap size 192 MB, throughput 0.997497
Reading from 7461: heap size 197 MB, throughput 0.99507
Reading from 7460: heap size 1292 MB, throughput 0.970803
Reading from 7461: heap size 191 MB, throughput 0.980255
Reading from 7461: heap size 199 MB, throughput 0.986334
Reading from 7460: heap size 1301 MB, throughput 0.977961
Reading from 7461: heap size 191 MB, throughput 0.993527
Reading from 7461: heap size 198 MB, throughput 0.995699
Reading from 7461: heap size 190 MB, throughput 0.996621
Reading from 7460: heap size 1310 MB, throughput 0.980476
Reading from 7461: heap size 196 MB, throughput 0.997197
Numeric result:
Recommendation: 2 clients, utility 1.68575:
    h1: 175.673 MB (U(h) = 0.835853*h^0.0319283)
    h2: 3493.33 MB (U(h) = 0.00962438*h^0.634905)
Recommendation: 2 clients, utility 1.68575:
    h1: 175.674 MB (U(h) = 0.835853*h^0.0319283)
    h2: 3493.33 MB (U(h) = 0.00962438*h^0.634905)
Reading from 7461: heap size 189 MB, throughput 0.995917
Reading from 7460: heap size 1312 MB, throughput 0.980486
Reading from 7461: heap size 181 MB, throughput 0.9967
Reading from 7461: heap size 174 MB, throughput 0.997051
Reading from 7460: heap size 1307 MB, throughput 0.978795
Reading from 7461: heap size 179 MB, throughput 0.997174
Reading from 7461: heap size 172 MB, throughput 0.997222
Reading from 7460: heap size 1213 MB, throughput 0.977796
Reading from 7461: heap size 177 MB, throughput 0.997326
Reading from 7461: heap size 171 MB, throughput 0.997286
Reading from 7461: heap size 175 MB, throughput 0.995647
Reading from 7460: heap size 1297 MB, throughput 0.976519
Reading from 7461: heap size 181 MB, throughput 0.988705
Reading from 7461: heap size 173 MB, throughput 0.983903
Numeric result:
Recommendation: 2 clients, utility 1.8947:
    h1: 157.346 MB (U(h) = 0.834724*h^0.032381)
    h2: 3511.65 MB (U(h) = 0.00527831*h^0.722707)
Recommendation: 2 clients, utility 1.8947:
    h1: 157.34 MB (U(h) = 0.834724*h^0.032381)
    h2: 3511.66 MB (U(h) = 0.00527831*h^0.722707)
Reading from 7461: heap size 180 MB, throughput 0.982502
Reading from 7460: heap size 1232 MB, throughput 0.974114
Reading from 7461: heap size 174 MB, throughput 0.987598
Reading from 7461: heap size 167 MB, throughput 0.987782
Reading from 7461: heap size 158 MB, throughput 0.990846
Reading from 7460: heap size 1295 MB, throughput 0.973694
Reading from 7461: heap size 153 MB, throughput 0.994309
Reading from 7461: heap size 158 MB, throughput 0.996011
Reading from 7461: heap size 152 MB, throughput 0.996828
Reading from 7460: heap size 1298 MB, throughput 0.971516
Reading from 7461: heap size 156 MB, throughput 0.997292
Reading from 7461: heap size 161 MB, throughput 0.997423
Reading from 7461: heap size 155 MB, throughput 0.994672
Reading from 7460: heap size 1301 MB, throughput 0.970167
Reading from 7461: heap size 161 MB, throughput 0.995964
Reading from 7461: heap size 155 MB, throughput 0.996529
Numeric result:
Recommendation: 2 clients, utility 1.90988:
    h1: 157.097 MB (U(h) = 0.834626*h^0.0325279)
    h2: 3511.9 MB (U(h) = 0.00512781*h^0.727151)
Recommendation: 2 clients, utility 1.90988:
    h1: 157.099 MB (U(h) = 0.834626*h^0.0325279)
    h2: 3511.9 MB (U(h) = 0.00512781*h^0.727151)
Reading from 7460: heap size 1303 MB, throughput 0.968235
Reading from 7461: heap size 160 MB, throughput 0.997054
Reading from 7461: heap size 154 MB, throughput 0.997132
Reading from 7461: heap size 158 MB, throughput 0.992637
Reading from 7460: heap size 1306 MB, throughput 0.966291
Reading from 7461: heap size 152 MB, throughput 0.988486
Reading from 7461: heap size 155 MB, throughput 0.9907
Reading from 7461: heap size 161 MB, throughput 0.994283
Reading from 7460: heap size 1312 MB, throughput 0.964159
Reading from 7461: heap size 152 MB, throughput 0.995823
Reading from 7461: heap size 158 MB, throughput 0.994505
Reading from 7461: heap size 153 MB, throughput 0.996324
Reading from 7461: heap size 159 MB, throughput 0.9962
Reading from 7460: heap size 1317 MB, throughput 0.96346
Reading from 7461: heap size 154 MB, throughput 0.997628
Reading from 7461: heap size 159 MB, throughput 0.998016
Numeric result:
Recommendation: 2 clients, utility 1.90524:
    h1: 157.812 MB (U(h) = 0.834553*h^0.0325653)
    h2: 3511.19 MB (U(h) = 0.00522598*h^0.724518)
Recommendation: 2 clients, utility 1.90524:
    h1: 157.819 MB (U(h) = 0.834553*h^0.0325653)
    h2: 3511.18 MB (U(h) = 0.00522598*h^0.724518)
Reading from 7461: heap size 150 MB, throughput 0.998203
Reading from 7460: heap size 1325 MB, throughput 0.963467
Reading from 7461: heap size 156 MB, throughput 0.998334
Reading from 7461: heap size 156 MB, throughput 0.998178
Reading from 7461: heap size 160 MB, throughput 0.998268
Reading from 7460: heap size 1333 MB, throughput 0.962873
Reading from 7461: heap size 151 MB, throughput 0.998339
Reading from 7461: heap size 158 MB, throughput 0.998241
Reading from 7461: heap size 157 MB, throughput 0.998239
Reading from 7460: heap size 1338 MB, throughput 0.963193
Reading from 7461: heap size 161 MB, throughput 0.998368
Reading from 7461: heap size 153 MB, throughput 0.998419
Reading from 7461: heap size 159 MB, throughput 0.998414
Reading from 7461: heap size 151 MB, throughput 0.998413
Reading from 7461: heap size 156 MB, throughput 0.998446
Numeric result:
Recommendation: 2 clients, utility 1.97177:
    h1: 152.959 MB (U(h) = 0.834363*h^0.0327421)
    h2: 3516.04 MB (U(h) = 0.00429673*h^0.752621)
Recommendation: 2 clients, utility 1.97177:
    h1: 152.962 MB (U(h) = 0.834363*h^0.0327421)
    h2: 3516.04 MB (U(h) = 0.00429673*h^0.752621)
Reading from 7461: heap size 157 MB, throughput 0.997024
Reading from 7461: heap size 149 MB, throughput 0.997784
Reading from 7461: heap size 154 MB, throughput 0.998021
Reading from 7461: heap size 147 MB, throughput 0.995531
Reading from 7461: heap size 153 MB, throughput 0.988175
Reading from 7461: heap size 148 MB, throughput 0.989429
Reading from 7461: heap size 155 MB, throughput 0.994482
Reading from 7461: heap size 148 MB, throughput 0.996085
Reading from 7461: heap size 156 MB, throughput 0.996729
Reading from 7461: heap size 149 MB, throughput 0.996915
Reading from 7461: heap size 155 MB, throughput 0.99708
Reading from 7461: heap size 148 MB, throughput 0.997102
Reading from 7461: heap size 155 MB, throughput 0.997117
Numeric result:
Recommendation: 2 clients, utility 1.97231:
    h1: 153.191 MB (U(h) = 0.83438*h^0.0327932)
    h2: 3515.81 MB (U(h) = 0.00429673*h^0.752621)
Recommendation: 2 clients, utility 1.97231:
    h1: 153.191 MB (U(h) = 0.83438*h^0.0327932)
    h2: 3515.81 MB (U(h) = 0.00429673*h^0.752621)
Reading from 7461: heap size 148 MB, throughput 0.99696
Reading from 7461: heap size 153 MB, throughput 0.997109
Reading from 7461: heap size 146 MB, throughput 0.996919
Reading from 7461: heap size 153 MB, throughput 0.996842
Reading from 7461: heap size 153 MB, throughput 0.99674
Reading from 7461: heap size 146 MB, throughput 0.992006
Reading from 7461: heap size 150 MB, throughput 0.988968
Reading from 7461: heap size 151 MB, throughput 0.990491
Reading from 7460: heap size 1346 MB, throughput 0.942818
Reading from 7461: heap size 158 MB, throughput 0.994331
Reading from 7461: heap size 149 MB, throughput 0.996479
Reading from 7461: heap size 154 MB, throughput 0.997446
Reading from 7461: heap size 145 MB, throughput 0.996392
Reading from 7461: heap size 151 MB, throughput 0.99375
Reading from 7461: heap size 151 MB, throughput 0.991812
Numeric result:
Recommendation: 2 clients, utility 1.98783:
    h1: 152.003 MB (U(h) = 0.834437*h^0.0328249)
    h2: 3517 MB (U(h) = 0.00409439*h^0.75946)
Recommendation: 2 clients, utility 1.98783:
    h1: 152.009 MB (U(h) = 0.834437*h^0.0328249)
    h2: 3516.99 MB (U(h) = 0.00409439*h^0.75946)
Reading from 7461: heap size 159 MB, throughput 0.994833
Reading from 7461: heap size 152 MB, throughput 0.996346
Reading from 7461: heap size 147 MB, throughput 0.996885
Reading from 7461: heap size 155 MB, throughput 0.997043
Reading from 7461: heap size 148 MB, throughput 0.997245
Reading from 7461: heap size 155 MB, throughput 0.997218
Reading from 7461: heap size 148 MB, throughput 0.997228
Reading from 7461: heap size 154 MB, throughput 0.997153
Reading from 7461: heap size 147 MB, throughput 0.997016
Reading from 7461: heap size 153 MB, throughput 0.99698
Reading from 7461: heap size 146 MB, throughput 0.996836
Reading from 7461: heap size 152 MB, throughput 0.996967
Numeric result:
Recommendation: 2 clients, utility 1.98793:
    h1: 152.044 MB (U(h) = 0.834442*h^0.0328341)
    h2: 3516.96 MB (U(h) = 0.00409439*h^0.75946)
Recommendation: 2 clients, utility 1.98793:
    h1: 152.05 MB (U(h) = 0.834442*h^0.0328341)
    h2: 3516.95 MB (U(h) = 0.00409439*h^0.75946)
Reading from 7461: heap size 146 MB, throughput 0.99399
Reading from 7460: heap size 1484 MB, throughput 0.989453
Reading from 7461: heap size 152 MB, throughput 0.992008
Reading from 7461: heap size 152 MB, throughput 0.992066
Reading from 7461: heap size 144 MB, throughput 0.993972
Reading from 7461: heap size 149 MB, throughput 0.996207
Reading from 7461: heap size 149 MB, throughput 0.997372
Reading from 7461: heap size 153 MB, throughput 0.997999
Reading from 7461: heap size 144 MB, throughput 0.996633
Reading from 7461: heap size 149 MB, throughput 0.994738
Reading from 7461: heap size 149 MB, throughput 0.993251
Reading from 7461: heap size 156 MB, throughput 0.996237
Reading from 7460: heap size 1532 MB, throughput 0.988293
Reading from 7460: heap size 1532 MB, throughput 0.980332
Reading from 7461: heap size 149 MB, throughput 0.996943
Reading from 7460: heap size 1525 MB, throughput 0.965084
Reading from 7460: heap size 1540 MB, throughput 0.935039
Reading from 7461: heap size 152 MB, throughput 0.989681
Reading from 7460: heap size 1565 MB, throughput 0.898997
Reading from 7460: heap size 1585 MB, throughput 0.849002
Numeric result:
Recommendation: 2 clients, utility 1.71437:
    h1: 178.42 MB (U(h) = 0.834492*h^0.0328367)
    h2: 3490.58 MB (U(h) = 0.00917951*h^0.642393)
Recommendation: 2 clients, utility 1.71437:
    h1: 178.425 MB (U(h) = 0.834492*h^0.0328367)
    h2: 3490.58 MB (U(h) = 0.00917951*h^0.642393)
Reading from 7460: heap size 1617 MB, throughput 0.815555
Reading from 7461: heap size 154 MB, throughput 0.993511
Reading from 7460: heap size 1628 MB, throughput 0.812706
Reading from 7461: heap size 161 MB, throughput 0.995284
Reading from 7461: heap size 162 MB, throughput 0.995979
Reading from 7460: heap size 1646 MB, throughput 0.943375
Reading from 7461: heap size 170 MB, throughput 0.99655
Reading from 7461: heap size 171 MB, throughput 0.996773
Reading from 7461: heap size 178 MB, throughput 0.996869
Reading from 7460: heap size 1657 MB, throughput 0.96343
Reading from 7461: heap size 178 MB, throughput 0.996878
Reading from 7461: heap size 170 MB, throughput 0.997107
Reading from 7461: heap size 177 MB, throughput 0.99717
Reading from 7460: heap size 1651 MB, throughput 0.969731
Reading from 7461: heap size 182 MB, throughput 0.993843
Numeric result:
Recommendation: 2 clients, utility 1.60216:
    h1: 192.834 MB (U(h) = 0.834218*h^0.0329472)
    h2: 3476.17 MB (U(h) = 0.0127341*h^0.59393)
Recommendation: 2 clients, utility 1.60216:
    h1: 192.834 MB (U(h) = 0.834218*h^0.0329472)
    h2: 3476.17 MB (U(h) = 0.0127341*h^0.59393)
Reading from 7461: heap size 174 MB, throughput 0.993265
Reading from 7461: heap size 179 MB, throughput 0.993483
Reading from 7460: heap size 1664 MB, throughput 0.971478
Reading from 7461: heap size 180 MB, throughput 0.995902
Reading from 7461: heap size 185 MB, throughput 0.995767
Reading from 7461: heap size 185 MB, throughput 0.993939
Reading from 7461: heap size 192 MB, throughput 0.995226
Reading from 7460: heap size 1653 MB, throughput 0.970999
Reading from 7461: heap size 192 MB, throughput 0.996679
Reading from 7461: heap size 200 MB, throughput 0.997044
Reading from 7461: heap size 191 MB, throughput 0.997384
Reading from 7460: heap size 1666 MB, throughput 0.96967
Reading from 7461: heap size 198 MB, throughput 0.997613
Reading from 7461: heap size 189 MB, throughput 0.997661
Numeric result:
Recommendation: 2 clients, utility 1.37398:
    h1: 251.99 MB (U(h) = 0.833902*h^0.0330553)
    h2: 3417.01 MB (U(h) = 0.035781*h^0.448216)
Recommendation: 2 clients, utility 1.37398:
    h1: 251.999 MB (U(h) = 0.833902*h^0.0330553)
    h2: 3417 MB (U(h) = 0.035781*h^0.448216)
Reading from 7461: heap size 194 MB, throughput 0.997764
Reading from 7460: heap size 1662 MB, throughput 0.970673
Reading from 7461: heap size 196 MB, throughput 0.997628
Reading from 7461: heap size 202 MB, throughput 0.997578
Reading from 7461: heap size 202 MB, throughput 0.997435
Reading from 7460: heap size 1669 MB, throughput 0.952066
Reading from 7461: heap size 209 MB, throughput 0.997612
Reading from 7461: heap size 209 MB, throughput 0.997484
Reading from 7461: heap size 215 MB, throughput 0.997559
Reading from 7460: heap size 1598 MB, throughput 0.983022
Reading from 7461: heap size 215 MB, throughput 0.997072
Reading from 7461: heap size 221 MB, throughput 0.991728
Numeric result:
Recommendation: 2 clients, utility 1.25764:
    h1: 311.699 MB (U(h) = 0.833654*h^0.0331359)
    h2: 3357.3 MB (U(h) = 0.068791*h^0.356895)
Recommendation: 2 clients, utility 1.25764:
    h1: 311.708 MB (U(h) = 0.833654*h^0.0331359)
    h2: 3357.29 MB (U(h) = 0.068791*h^0.356895)
Reading from 7461: heap size 221 MB, throughput 0.992894
Reading from 7461: heap size 232 MB, throughput 0.996131
Reading from 7460: heap size 1600 MB, throughput 0.987025
Reading from 7461: heap size 232 MB, throughput 0.997097
Reading from 7461: heap size 240 MB, throughput 0.997684
Reading from 7461: heap size 241 MB, throughput 0.997784
Reading from 7460: heap size 1620 MB, throughput 0.988069
Reading from 7461: heap size 249 MB, throughput 0.997999
Reading from 7461: heap size 249 MB, throughput 0.997964
Reading from 7460: heap size 1624 MB, throughput 0.986532
Numeric result:
Recommendation: 2 clients, utility 1.24112:
    h1: 323.733 MB (U(h) = 0.834087*h^0.033011)
    h2: 3345.27 MB (U(h) = 0.0771821*h^0.341111)
Recommendation: 2 clients, utility 1.24112:
    h1: 323.738 MB (U(h) = 0.834087*h^0.033011)
    h2: 3345.26 MB (U(h) = 0.0771821*h^0.341111)
Reading from 7461: heap size 256 MB, throughput 0.998014
Reading from 7461: heap size 257 MB, throughput 0.997967
Reading from 7461: heap size 264 MB, throughput 0.997941
Reading from 7460: heap size 1620 MB, throughput 0.983874
Reading from 7461: heap size 264 MB, throughput 0.997893
Reading from 7461: heap size 271 MB, throughput 0.992709
Reading from 7461: heap size 271 MB, throughput 0.993579
Reading from 7460: heap size 1627 MB, throughput 0.983428
Reading from 7461: heap size 281 MB, throughput 0.995224
Numeric result:
Recommendation: 2 clients, utility 1.27014:
    h1: 551.973 MB (U(h) = 0.726009*h^0.0599354)
    h2: 3117.03 MB (U(h) = 0.0787198*h^0.338459)
Recommendation: 2 clients, utility 1.27014:
    h1: 551.973 MB (U(h) = 0.726009*h^0.0599354)
    h2: 3117.03 MB (U(h) = 0.0787198*h^0.338459)
Reading from 7461: heap size 282 MB, throughput 0.996774
Reading from 7460: heap size 1605 MB, throughput 0.983117
Reading from 7461: heap size 290 MB, throughput 0.997659
Reading from 7461: heap size 291 MB, throughput 0.997864
Reading from 7460: heap size 1471 MB, throughput 0.982135
Reading from 7461: heap size 299 MB, throughput 0.998016
Reading from 7461: heap size 300 MB, throughput 0.998046
Reading from 7461: heap size 308 MB, throughput 0.998043
Reading from 7460: heap size 1593 MB, throughput 0.979054
Numeric result:
Recommendation: 2 clients, utility 1.3611:
    h1: 782.695 MB (U(h) = 0.590865*h^0.0989012)
    h2: 2886.31 MB (U(h) = 0.0651845*h^0.364726)
Recommendation: 2 clients, utility 1.3611:
    h1: 782.672 MB (U(h) = 0.590865*h^0.0989012)
    h2: 2886.33 MB (U(h) = 0.0651845*h^0.364726)
Reading from 7461: heap size 308 MB, throughput 0.998086
Reading from 7461: heap size 317 MB, throughput 0.996495
Reading from 7460: heap size 1604 MB, throughput 0.977342
Reading from 7461: heap size 321 MB, throughput 0.997726
Reading from 7461: heap size 332 MB, throughput 0.998367
Reading from 7460: heap size 1609 MB, throughput 0.97728
Reading from 7461: heap size 332 MB, throughput 0.998639
Numeric result:
Recommendation: 2 clients, utility 1.28739:
    h1: 118.154 MB (U(h) = 0.931685*h^0.012564)
    h2: 3550.85 MB (U(h) = 0.0594093*h^0.377584)
Recommendation: 2 clients, utility 1.28739:
    h1: 118.153 MB (U(h) = 0.931685*h^0.012564)
    h2: 3550.85 MB (U(h) = 0.0594093*h^0.377584)
Reading from 7461: heap size 343 MB, throughput 0.998757
Reading from 7461: heap size 299 MB, throughput 0.998237
Reading from 7460: heap size 1609 MB, throughput 0.975476
Reading from 7461: heap size 268 MB, throughput 0.998409
Reading from 7461: heap size 244 MB, throughput 0.998486
Reading from 7460: heap size 1617 MB, throughput 0.973868
Reading from 7461: heap size 225 MB, throughput 0.998553
Reading from 7461: heap size 209 MB, throughput 0.99853
Reading from 7461: heap size 195 MB, throughput 0.998884
Numeric result:
Recommendation: 2 clients, utility 1.31041:
    h1: 29.6075 MB (U(h) = 0.97979*h^0.00311964)
    h2: 3639.39 MB (U(h) = 0.0570306*h^0.383478)
Recommendation: 2 clients, utility 1.31041:
    h1: 29.6069 MB (U(h) = 0.97979*h^0.00311964)
    h2: 3639.39 MB (U(h) = 0.0570306*h^0.383478)
Reading from 7460: heap size 1623 MB, throughput 0.972885
Client 7461 died
Clients: 1
Reading from 7460: heap size 1634 MB, throughput 0.971661
Reading from 7460: heap size 1645 MB, throughput 0.969588
Recommendation: one client; give it all the memory
Reading from 7460: heap size 1661 MB, throughput 0.968723
Reading from 7460: heap size 1671 MB, throughput 0.96828
Reading from 7460: heap size 1688 MB, throughput 0.968229
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 7460: heap size 1694 MB, throughput 0.984341
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 7460: heap size 1675 MB, throughput 0.990251
Reading from 7460: heap size 1714 MB, throughput 0.984203
Reading from 7460: heap size 1698 MB, throughput 0.972825
Reading from 7460: heap size 1720 MB, throughput 0.952185
Reading from 7460: heap size 1757 MB, throughput 0.925012
Client 7460 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
