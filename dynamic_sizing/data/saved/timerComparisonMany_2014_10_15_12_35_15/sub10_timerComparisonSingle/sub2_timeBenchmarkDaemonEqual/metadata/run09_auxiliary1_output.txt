economemd
    total memory: 7314 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub10_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run09_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 31912: heap size 9 MB, throughput 0.982544
Clients: 1
Client 31912 has a minimum heap size of 8 MB
Reading from 31911: heap size 9 MB, throughput 0.990879
Clients: 2
Client 31911 has a minimum heap size of 1211 MB
Reading from 31911: heap size 9 MB, throughput 0.979845
Reading from 31912: heap size 9 MB, throughput 0.980638
Reading from 31911: heap size 9 MB, throughput 0.94744
Reading from 31912: heap size 11 MB, throughput 0.936568
Reading from 31912: heap size 11 MB, throughput 0.956244
Reading from 31911: heap size 9 MB, throughput 0.958037
Reading from 31911: heap size 11 MB, throughput 0.982181
Reading from 31912: heap size 15 MB, throughput 0.843471
Reading from 31911: heap size 11 MB, throughput 0.976726
Reading from 31911: heap size 17 MB, throughput 0.946424
Reading from 31912: heap size 20 MB, throughput 0.989682
Reading from 31912: heap size 24 MB, throughput 0.976679
Reading from 31912: heap size 28 MB, throughput 0.971661
Reading from 31912: heap size 32 MB, throughput 0.979056
Reading from 31911: heap size 17 MB, throughput 0.521782
Reading from 31912: heap size 32 MB, throughput 0.978679
Reading from 31912: heap size 36 MB, throughput 0.971325
Reading from 31911: heap size 30 MB, throughput 0.955103
Reading from 31912: heap size 36 MB, throughput 0.973467
Reading from 31912: heap size 42 MB, throughput 0.982357
Reading from 31911: heap size 31 MB, throughput 0.928703
Reading from 31912: heap size 42 MB, throughput 0.976275
Reading from 31912: heap size 48 MB, throughput 0.98948
Reading from 31912: heap size 48 MB, throughput 0.979878
Reading from 31912: heap size 54 MB, throughput 0.982986
Reading from 31911: heap size 35 MB, throughput 0.460629
Reading from 31912: heap size 55 MB, throughput 0.97506
Reading from 31911: heap size 45 MB, throughput 0.706267
Reading from 31912: heap size 60 MB, throughput 0.984835
Reading from 31911: heap size 50 MB, throughput 0.918114
Reading from 31912: heap size 60 MB, throughput 0.990844
Reading from 31912: heap size 66 MB, throughput 0.982819
Reading from 31912: heap size 66 MB, throughput 0.980988
Reading from 31912: heap size 73 MB, throughput 0.987039
Reading from 31911: heap size 51 MB, throughput 0.159187
Reading from 31911: heap size 70 MB, throughput 0.655996
Reading from 31912: heap size 73 MB, throughput 0.98612
Reading from 31912: heap size 79 MB, throughput 0.992712
Reading from 31911: heap size 71 MB, throughput 0.274285
Reading from 31912: heap size 79 MB, throughput 0.929107
Reading from 31911: heap size 97 MB, throughput 0.723077
Reading from 31911: heap size 98 MB, throughput 0.843137
Reading from 31912: heap size 84 MB, throughput 0.993825
Reading from 31911: heap size 100 MB, throughput 0.761942
Reading from 31912: heap size 84 MB, throughput 0.959762
Reading from 31911: heap size 103 MB, throughput 0.691238
Reading from 31911: heap size 110 MB, throughput 0.835666
Reading from 31912: heap size 92 MB, throughput 0.993518
Reading from 31911: heap size 113 MB, throughput 0.723326
Reading from 31912: heap size 92 MB, throughput 0.992245
Reading from 31912: heap size 99 MB, throughput 0.991757
Reading from 31912: heap size 100 MB, throughput 0.992504
Reading from 31911: heap size 115 MB, throughput 0.128379
Reading from 31911: heap size 146 MB, throughput 0.631329
Reading from 31912: heap size 107 MB, throughput 0.990373
Reading from 31911: heap size 151 MB, throughput 0.640402
Reading from 31911: heap size 156 MB, throughput 0.589207
Reading from 31912: heap size 107 MB, throughput 0.988767
Reading from 31911: heap size 159 MB, throughput 0.641766
Reading from 31911: heap size 166 MB, throughput 0.487182
Reading from 31911: heap size 172 MB, throughput 0.604787
Reading from 31912: heap size 113 MB, throughput 0.994895
Reading from 31912: heap size 113 MB, throughput 0.983998
Reading from 31912: heap size 119 MB, throughput 0.991415
Reading from 31911: heap size 178 MB, throughput 0.0990704
Reading from 31912: heap size 119 MB, throughput 0.990753
Reading from 31911: heap size 218 MB, throughput 0.506821
Reading from 31911: heap size 219 MB, throughput 0.601865
Reading from 31912: heap size 124 MB, throughput 0.990316
Reading from 31911: heap size 220 MB, throughput 0.589522
Reading from 31912: heap size 124 MB, throughput 0.987854
Reading from 31912: heap size 130 MB, throughput 0.990148
Reading from 31911: heap size 221 MB, throughput 0.1178
Reading from 31912: heap size 130 MB, throughput 0.995937
Reading from 31911: heap size 261 MB, throughput 0.578613
Reading from 31911: heap size 261 MB, throughput 0.618245
Reading from 31912: heap size 135 MB, throughput 0.99603
Reading from 31911: heap size 262 MB, throughput 0.667468
Reading from 31912: heap size 135 MB, throughput 0.993645
Reading from 31911: heap size 262 MB, throughput 0.580548
Reading from 31911: heap size 265 MB, throughput 0.675812
Reading from 31912: heap size 139 MB, throughput 0.996124
Reading from 31911: heap size 270 MB, throughput 0.56425
Reading from 31912: heap size 139 MB, throughput 0.995004
Reading from 31911: heap size 273 MB, throughput 0.553866
Reading from 31911: heap size 282 MB, throughput 0.47762
Reading from 31911: heap size 287 MB, throughput 0.472363
Reading from 31912: heap size 143 MB, throughput 0.997346
Reading from 31912: heap size 143 MB, throughput 0.99386
Reading from 31912: heap size 147 MB, throughput 0.996314
Reading from 31911: heap size 293 MB, throughput 0.0818856
Reading from 31911: heap size 340 MB, throughput 0.443395
Reading from 31912: heap size 147 MB, throughput 0.995376
Reading from 31911: heap size 345 MB, throughput 0.535267
Reading from 31912: heap size 150 MB, throughput 0.995057
Reading from 31912: heap size 150 MB, throughput 0.994585
Reading from 31911: heap size 346 MB, throughput 0.0968392
Reading from 31912: heap size 154 MB, throughput 0.995133
Reading from 31911: heap size 395 MB, throughput 0.396962
Reading from 31911: heap size 388 MB, throughput 0.600644
Reading from 31912: heap size 154 MB, throughput 0.995479
Reading from 31911: heap size 338 MB, throughput 0.553172
Reading from 31911: heap size 388 MB, throughput 0.594231
Reading from 31912: heap size 157 MB, throughput 0.991924
Reading from 31911: heap size 389 MB, throughput 0.593535
Reading from 31911: heap size 394 MB, throughput 0.519498
Reading from 31912: heap size 157 MB, throughput 0.996881
Reading from 31912: heap size 161 MB, throughput 0.993959
Reading from 31912: heap size 161 MB, throughput 0.991262
Reading from 31912: heap size 164 MB, throughput 0.997395
Reading from 31911: heap size 396 MB, throughput 0.102994
Reading from 31911: heap size 452 MB, throughput 0.441959
Reading from 31912: heap size 164 MB, throughput 0.997211
Reading from 31911: heap size 455 MB, throughput 0.519657
Reading from 31912: heap size 168 MB, throughput 0.996642
Reading from 31911: heap size 456 MB, throughput 0.434519
Equal recommendation: 3657 MB each
Reading from 31912: heap size 168 MB, throughput 0.994989
Reading from 31911: heap size 458 MB, throughput 0.495235
Reading from 31911: heap size 462 MB, throughput 0.470301
Reading from 31912: heap size 171 MB, throughput 0.99737
Reading from 31911: heap size 468 MB, throughput 0.40984
Reading from 31912: heap size 171 MB, throughput 0.996706
Reading from 31912: heap size 174 MB, throughput 0.99591
Reading from 31912: heap size 174 MB, throughput 0.995811
Reading from 31912: heap size 177 MB, throughput 0.997513
Reading from 31911: heap size 475 MB, throughput 0.0868968
Reading from 31911: heap size 536 MB, throughput 0.411919
Reading from 31912: heap size 177 MB, throughput 0.995027
Reading from 31911: heap size 538 MB, throughput 0.479431
Reading from 31912: heap size 179 MB, throughput 0.993459
Reading from 31912: heap size 180 MB, throughput 0.991702
Reading from 31912: heap size 183 MB, throughput 0.993019
Reading from 31912: heap size 183 MB, throughput 0.996634
Reading from 31911: heap size 542 MB, throughput 0.102536
Reading from 31911: heap size 603 MB, throughput 0.437697
Reading from 31912: heap size 187 MB, throughput 0.995219
Reading from 31911: heap size 606 MB, throughput 0.650191
Reading from 31911: heap size 605 MB, throughput 0.637782
Reading from 31912: heap size 187 MB, throughput 0.995783
Reading from 31911: heap size 607 MB, throughput 0.562444
Reading from 31912: heap size 191 MB, throughput 0.995572
Reading from 31911: heap size 609 MB, throughput 0.532413
Reading from 31912: heap size 191 MB, throughput 0.995987
Reading from 31911: heap size 617 MB, throughput 0.501577
Reading from 31912: heap size 195 MB, throughput 0.992659
Reading from 31911: heap size 624 MB, throughput 0.68319
Reading from 31912: heap size 195 MB, throughput 0.99354
Reading from 31912: heap size 200 MB, throughput 0.994618
Reading from 31912: heap size 200 MB, throughput 0.993384
Reading from 31912: heap size 204 MB, throughput 0.994189
Reading from 31912: heap size 204 MB, throughput 0.993602
Reading from 31911: heap size 635 MB, throughput 0.29813
Reading from 31912: heap size 209 MB, throughput 0.995645
Reading from 31912: heap size 209 MB, throughput 0.997444
Reading from 31911: heap size 705 MB, throughput 0.669995
Reading from 31912: heap size 213 MB, throughput 0.997909
Reading from 31912: heap size 214 MB, throughput 0.996286
Reading from 31911: heap size 719 MB, throughput 0.807945
Reading from 31912: heap size 218 MB, throughput 0.996928
Reading from 31911: heap size 721 MB, throughput 0.448556
Reading from 31911: heap size 735 MB, throughput 0.579032
Reading from 31912: heap size 218 MB, throughput 0.996495
Reading from 31911: heap size 742 MB, throughput 0.476604
Reading from 31911: heap size 747 MB, throughput 0.407873
Reading from 31911: heap size 752 MB, throughput 0.402383
Reading from 31912: heap size 221 MB, throughput 0.995969
Reading from 31912: heap size 221 MB, throughput 0.996093
Equal recommendation: 3657 MB each
Reading from 31912: heap size 225 MB, throughput 0.99556
Reading from 31912: heap size 225 MB, throughput 0.994535
Reading from 31911: heap size 753 MB, throughput 0.0248206
Reading from 31912: heap size 229 MB, throughput 0.995637
Reading from 31911: heap size 822 MB, throughput 0.258727
Reading from 31911: heap size 827 MB, throughput 0.789699
Reading from 31912: heap size 229 MB, throughput 0.99695
Reading from 31912: heap size 233 MB, throughput 0.995863
Reading from 31912: heap size 233 MB, throughput 0.996803
Reading from 31912: heap size 236 MB, throughput 0.996929
Reading from 31911: heap size 833 MB, throughput 0.130231
Reading from 31912: heap size 237 MB, throughput 0.9941
Reading from 31911: heap size 921 MB, throughput 0.333534
Reading from 31911: heap size 922 MB, throughput 0.571246
Reading from 31911: heap size 926 MB, throughput 0.744596
Reading from 31912: heap size 241 MB, throughput 0.972038
Reading from 31911: heap size 930 MB, throughput 0.84927
Reading from 31912: heap size 242 MB, throughput 0.994662
Reading from 31911: heap size 931 MB, throughput 0.684106
Reading from 31911: heap size 934 MB, throughput 0.866175
Reading from 31912: heap size 248 MB, throughput 0.997559
Reading from 31911: heap size 935 MB, throughput 0.917367
Reading from 31911: heap size 933 MB, throughput 0.792057
Reading from 31912: heap size 248 MB, throughput 0.996912
Reading from 31911: heap size 936 MB, throughput 0.867026
Reading from 31912: heap size 253 MB, throughput 0.996815
Reading from 31911: heap size 924 MB, throughput 0.877052
Reading from 31911: heap size 785 MB, throughput 0.723594
Reading from 31912: heap size 254 MB, throughput 0.996165
Reading from 31911: heap size 899 MB, throughput 0.728984
Reading from 31911: heap size 791 MB, throughput 0.664749
Reading from 31912: heap size 261 MB, throughput 0.996006
Reading from 31911: heap size 885 MB, throughput 0.746554
Reading from 31911: heap size 794 MB, throughput 0.731948
Reading from 31912: heap size 261 MB, throughput 0.995893
Reading from 31911: heap size 879 MB, throughput 0.777368
Reading from 31911: heap size 886 MB, throughput 0.777677
Reading from 31912: heap size 269 MB, throughput 0.994085
Reading from 31911: heap size 874 MB, throughput 0.76684
Reading from 31911: heap size 881 MB, throughput 0.759085
Reading from 31912: heap size 269 MB, throughput 0.995943
Reading from 31911: heap size 873 MB, throughput 0.818486
Reading from 31912: heap size 278 MB, throughput 0.997343
Reading from 31912: heap size 278 MB, throughput 0.996543
Reading from 31912: heap size 285 MB, throughput 0.99804
Reading from 31911: heap size 878 MB, throughput 0.972842
Reading from 31912: heap size 286 MB, throughput 0.997263
Reading from 31911: heap size 875 MB, throughput 0.913819
Reading from 31912: heap size 293 MB, throughput 0.998025
Reading from 31911: heap size 878 MB, throughput 0.750554
Reading from 31911: heap size 886 MB, throughput 0.735439
Reading from 31912: heap size 293 MB, throughput 0.997612
Reading from 31911: heap size 885 MB, throughput 0.746275
Reading from 31911: heap size 892 MB, throughput 0.757362
Reading from 31912: heap size 299 MB, throughput 0.997876
Equal recommendation: 3657 MB each
Reading from 31911: heap size 893 MB, throughput 0.775221
Reading from 31912: heap size 300 MB, throughput 0.996283
Reading from 31911: heap size 899 MB, throughput 0.782068
Reading from 31911: heap size 900 MB, throughput 0.779653
Reading from 31912: heap size 306 MB, throughput 0.998153
Reading from 31911: heap size 905 MB, throughput 0.866226
Reading from 31912: heap size 306 MB, throughput 0.998218
Reading from 31911: heap size 907 MB, throughput 0.855273
Reading from 31912: heap size 312 MB, throughput 0.997861
Reading from 31911: heap size 911 MB, throughput 0.84007
Reading from 31912: heap size 312 MB, throughput 0.997548
Reading from 31912: heap size 318 MB, throughput 0.997772
Reading from 31912: heap size 318 MB, throughput 0.997074
Reading from 31911: heap size 913 MB, throughput 0.13833
Reading from 31912: heap size 324 MB, throughput 0.997382
Reading from 31912: heap size 325 MB, throughput 0.995441
Reading from 31911: heap size 992 MB, throughput 0.135765
Reading from 31912: heap size 332 MB, throughput 0.997491
Reading from 31911: heap size 1082 MB, throughput 0.14794
Reading from 31911: heap size 1121 MB, throughput 0.915081
Reading from 31912: heap size 332 MB, throughput 0.998086
Reading from 31911: heap size 1122 MB, throughput 0.890832
Reading from 31911: heap size 1130 MB, throughput 0.823296
Reading from 31911: heap size 1139 MB, throughput 0.82265
Reading from 31912: heap size 339 MB, throughput 0.997294
Reading from 31911: heap size 1141 MB, throughput 0.769703
Reading from 31911: heap size 1146 MB, throughput 0.746681
Reading from 31912: heap size 340 MB, throughput 0.996676
Reading from 31911: heap size 1148 MB, throughput 0.742284
Reading from 31912: heap size 347 MB, throughput 0.996989
Reading from 31911: heap size 1148 MB, throughput 0.85447
Reading from 31912: heap size 347 MB, throughput 0.996412
Reading from 31912: heap size 356 MB, throughput 0.998011
Reading from 31912: heap size 356 MB, throughput 0.996325
Reading from 31911: heap size 1151 MB, throughput 0.964233
Equal recommendation: 3657 MB each
Reading from 31912: heap size 364 MB, throughput 0.996964
Reading from 31912: heap size 364 MB, throughput 0.997733
Reading from 31912: heap size 372 MB, throughput 0.996958
Reading from 31911: heap size 1143 MB, throughput 0.968672
Reading from 31912: heap size 373 MB, throughput 0.99731
Reading from 31912: heap size 382 MB, throughput 0.997757
Reading from 31912: heap size 382 MB, throughput 0.997474
Reading from 31911: heap size 1150 MB, throughput 0.955267
Reading from 31912: heap size 390 MB, throughput 0.998638
Reading from 31912: heap size 390 MB, throughput 0.996876
Reading from 31911: heap size 1141 MB, throughput 0.936584
Reading from 31912: heap size 397 MB, throughput 0.997145
Reading from 31912: heap size 398 MB, throughput 0.995936
Reading from 31912: heap size 407 MB, throughput 0.997214
Reading from 31911: heap size 1146 MB, throughput 0.939004
Reading from 31912: heap size 407 MB, throughput 0.996873
Reading from 31912: heap size 417 MB, throughput 0.995879
Reading from 31912: heap size 417 MB, throughput 0.99625
Reading from 31911: heap size 1150 MB, throughput 0.93912
Reading from 31912: heap size 428 MB, throughput 0.997413
Reading from 31912: heap size 428 MB, throughput 0.997294
Reading from 31911: heap size 1151 MB, throughput 0.931561
Reading from 31912: heap size 439 MB, throughput 0.997413
Equal recommendation: 3657 MB each
Reading from 31912: heap size 439 MB, throughput 0.996492
Reading from 31912: heap size 450 MB, throughput 0.996546
Reading from 31911: heap size 1155 MB, throughput 0.933957
Reading from 31912: heap size 450 MB, throughput 0.99714
Reading from 31912: heap size 461 MB, throughput 0.997255
Reading from 31912: heap size 462 MB, throughput 0.993781
Reading from 31911: heap size 1159 MB, throughput 0.951916
Reading from 31912: heap size 473 MB, throughput 0.996273
Reading from 31912: heap size 473 MB, throughput 0.997548
Reading from 31911: heap size 1164 MB, throughput 0.930869
Reading from 31912: heap size 485 MB, throughput 0.997651
Reading from 31912: heap size 486 MB, throughput 0.997424
Reading from 31911: heap size 1168 MB, throughput 0.930283
Reading from 31912: heap size 498 MB, throughput 0.997191
Reading from 31912: heap size 499 MB, throughput 0.997115
Reading from 31912: heap size 511 MB, throughput 0.997826
Reading from 31911: heap size 1172 MB, throughput 0.939072
Reading from 31912: heap size 511 MB, throughput 0.997802
Reading from 31912: heap size 522 MB, throughput 0.996158
Reading from 31911: heap size 1177 MB, throughput 0.941227
Equal recommendation: 3657 MB each
Reading from 31912: heap size 522 MB, throughput 0.997569
Reading from 31912: heap size 534 MB, throughput 0.997839
Reading from 31911: heap size 1182 MB, throughput 0.935672
Reading from 31912: heap size 535 MB, throughput 0.998045
Reading from 31912: heap size 546 MB, throughput 0.997735
Reading from 31911: heap size 1186 MB, throughput 0.939055
Reading from 31912: heap size 546 MB, throughput 0.998219
Reading from 31912: heap size 556 MB, throughput 0.998076
Reading from 31911: heap size 1189 MB, throughput 0.932221
Reading from 31912: heap size 557 MB, throughput 0.998063
Reading from 31912: heap size 567 MB, throughput 0.997311
Reading from 31911: heap size 1194 MB, throughput 0.93324
Reading from 31912: heap size 567 MB, throughput 0.99804
Reading from 31912: heap size 578 MB, throughput 0.998294
Reading from 31911: heap size 1200 MB, throughput 0.935205
Reading from 31912: heap size 578 MB, throughput 0.998432
Reading from 31912: heap size 587 MB, throughput 0.997839
Reading from 31911: heap size 1202 MB, throughput 0.930329
Equal recommendation: 3657 MB each
Reading from 31912: heap size 588 MB, throughput 0.997848
Reading from 31912: heap size 598 MB, throughput 0.998203
Reading from 31911: heap size 1208 MB, throughput 0.923017
Reading from 31912: heap size 598 MB, throughput 0.997687
Reading from 31912: heap size 609 MB, throughput 0.998198
Reading from 31911: heap size 1208 MB, throughput 0.948168
Reading from 31912: heap size 609 MB, throughput 0.998266
Reading from 31912: heap size 619 MB, throughput 0.997938
Reading from 31911: heap size 1212 MB, throughput 0.927736
Reading from 31912: heap size 619 MB, throughput 0.997924
Reading from 31912: heap size 630 MB, throughput 0.998454
Reading from 31911: heap size 1213 MB, throughput 0.938454
Reading from 31912: heap size 630 MB, throughput 0.99834
Reading from 31912: heap size 639 MB, throughput 0.998211
Reading from 31911: heap size 1216 MB, throughput 0.939169
Reading from 31912: heap size 640 MB, throughput 0.998109
Equal recommendation: 3657 MB each
Reading from 31912: heap size 650 MB, throughput 0.998658
Reading from 31911: heap size 1217 MB, throughput 0.942952
Reading from 31912: heap size 650 MB, throughput 0.998051
Reading from 31912: heap size 660 MB, throughput 0.998609
Reading from 31911: heap size 1220 MB, throughput 0.934253
Reading from 31912: heap size 660 MB, throughput 0.998231
Reading from 31912: heap size 670 MB, throughput 0.99805
Reading from 31911: heap size 1220 MB, throughput 0.933489
Reading from 31912: heap size 670 MB, throughput 0.998582
Reading from 31912: heap size 680 MB, throughput 0.998471
Reading from 31912: heap size 680 MB, throughput 0.99819
Reading from 31912: heap size 690 MB, throughput 0.99826
Reading from 31911: heap size 1223 MB, throughput 0.449061
Equal recommendation: 3657 MB each
Reading from 31912: heap size 690 MB, throughput 0.998567
Reading from 31912: heap size 700 MB, throughput 0.998382
Reading from 31911: heap size 1282 MB, throughput 0.984481
Reading from 31912: heap size 700 MB, throughput 0.998265
Reading from 31912: heap size 710 MB, throughput 0.998796
Reading from 31911: heap size 1278 MB, throughput 0.978793
Reading from 31912: heap size 710 MB, throughput 0.997905
Reading from 31912: heap size 720 MB, throughput 0.998665
Reading from 31911: heap size 1287 MB, throughput 0.975427
Reading from 31912: heap size 720 MB, throughput 0.998602
Reading from 31911: heap size 1295 MB, throughput 0.969267
Reading from 31912: heap size 730 MB, throughput 0.998217
Reading from 31912: heap size 730 MB, throughput 0.998366
Reading from 31911: heap size 1296 MB, throughput 0.966253
Reading from 31912: heap size 740 MB, throughput 0.998662
Equal recommendation: 3657 MB each
Reading from 31912: heap size 740 MB, throughput 0.998606
Reading from 31911: heap size 1291 MB, throughput 0.959946
Reading from 31912: heap size 750 MB, throughput 0.998579
Reading from 31912: heap size 751 MB, throughput 0.998176
Reading from 31911: heap size 1199 MB, throughput 0.959405
Reading from 31912: heap size 760 MB, throughput 0.998887
Reading from 31912: heap size 760 MB, throughput 0.998802
Reading from 31911: heap size 1281 MB, throughput 0.956146
Reading from 31912: heap size 770 MB, throughput 0.998644
Reading from 31912: heap size 770 MB, throughput 0.998435
Reading from 31911: heap size 1216 MB, throughput 0.964222
Reading from 31912: heap size 779 MB, throughput 0.998733
Reading from 31912: heap size 779 MB, throughput 0.998277
Equal recommendation: 3657 MB each
Reading from 31911: heap size 1277 MB, throughput 0.959996
Reading from 31912: heap size 789 MB, throughput 0.998648
Reading from 31912: heap size 789 MB, throughput 0.998074
Reading from 31911: heap size 1281 MB, throughput 0.946856
Reading from 31912: heap size 800 MB, throughput 0.998981
Reading from 31911: heap size 1283 MB, throughput 0.946578
Reading from 31912: heap size 800 MB, throughput 0.99884
Reading from 31912: heap size 810 MB, throughput 0.998487
Reading from 31911: heap size 1284 MB, throughput 0.94155
Reading from 31912: heap size 810 MB, throughput 0.998775
Reading from 31912: heap size 820 MB, throughput 0.998789
Reading from 31911: heap size 1287 MB, throughput 0.942989
Reading from 31912: heap size 820 MB, throughput 0.998594
Equal recommendation: 3657 MB each
Reading from 31912: heap size 830 MB, throughput 0.998807
Reading from 31911: heap size 1292 MB, throughput 0.936027
Reading from 31912: heap size 830 MB, throughput 0.998416
Reading from 31912: heap size 840 MB, throughput 0.998971
Reading from 31911: heap size 1296 MB, throughput 0.937248
Reading from 31912: heap size 840 MB, throughput 0.998983
Reading from 31911: heap size 1304 MB, throughput 0.931799
Reading from 31912: heap size 850 MB, throughput 0.998702
Reading from 31912: heap size 850 MB, throughput 0.998661
Reading from 31911: heap size 1311 MB, throughput 0.935351
Reading from 31912: heap size 859 MB, throughput 0.998904
Reading from 31912: heap size 859 MB, throughput 0.998216
Equal recommendation: 3657 MB each
Reading from 31911: heap size 1315 MB, throughput 0.926352
Reading from 31912: heap size 869 MB, throughput 0.998665
Reading from 31912: heap size 869 MB, throughput 0.998672
Reading from 31911: heap size 1323 MB, throughput 0.930392
Reading from 31912: heap size 879 MB, throughput 0.998867
Reading from 31912: heap size 879 MB, throughput 0.998621
Reading from 31911: heap size 1325 MB, throughput 0.930331
Reading from 31912: heap size 890 MB, throughput 0.998918
Reading from 31912: heap size 890 MB, throughput 0.998554
Reading from 31911: heap size 1333 MB, throughput 0.937661
Reading from 31912: heap size 900 MB, throughput 0.998698
Equal recommendation: 3657 MB each
Reading from 31912: heap size 900 MB, throughput 0.998627
Reading from 31912: heap size 911 MB, throughput 0.998951
Reading from 31912: heap size 911 MB, throughput 0.998902
Reading from 31912: heap size 921 MB, throughput 0.998701
Reading from 31912: heap size 921 MB, throughput 0.998705
Reading from 31912: heap size 931 MB, throughput 0.998787
Reading from 31912: heap size 931 MB, throughput 0.999
Reading from 31912: heap size 941 MB, throughput 0.998573
Equal recommendation: 3657 MB each
Reading from 31912: heap size 942 MB, throughput 0.998913
Reading from 31912: heap size 952 MB, throughput 0.998592
Reading from 31912: heap size 952 MB, throughput 0.99898
Reading from 31911: heap size 1334 MB, throughput 0.98883
Reading from 31912: heap size 962 MB, throughput 0.998683
Reading from 31912: heap size 963 MB, throughput 0.998845
Reading from 31912: heap size 973 MB, throughput 0.998721
Equal recommendation: 3657 MB each
Reading from 31912: heap size 973 MB, throughput 0.998908
Reading from 31912: heap size 984 MB, throughput 0.999051
Reading from 31912: heap size 984 MB, throughput 0.998786
Reading from 31912: heap size 994 MB, throughput 0.99896
Reading from 31912: heap size 994 MB, throughput 0.998958
Reading from 31912: heap size 1004 MB, throughput 0.998298
Reading from 31911: heap size 1311 MB, throughput 0.891167
Reading from 31912: heap size 1005 MB, throughput 0.999072
Equal recommendation: 3657 MB each
Reading from 31912: heap size 1016 MB, throughput 0.998942
Reading from 31912: heap size 1016 MB, throughput 0.998948
Reading from 31911: heap size 1433 MB, throughput 0.990188
Reading from 31911: heap size 1473 MB, throughput 0.942967
Reading from 31911: heap size 1477 MB, throughput 0.817995
Reading from 31911: heap size 1471 MB, throughput 0.800989
Reading from 31912: heap size 1026 MB, throughput 0.99864
Reading from 31911: heap size 1477 MB, throughput 0.783875
Reading from 31911: heap size 1468 MB, throughput 0.804784
Reading from 31911: heap size 1474 MB, throughput 0.779601
Reading from 31911: heap size 1472 MB, throughput 0.812896
Reading from 31911: heap size 1478 MB, throughput 0.916135
Reading from 31912: heap size 1027 MB, throughput 0.998876
Reading from 31912: heap size 1037 MB, throughput 0.998848
Reading from 31911: heap size 1485 MB, throughput 0.98604
Reading from 31912: heap size 1037 MB, throughput 0.998947
Equal recommendation: 3657 MB each
Reading from 31911: heap size 1491 MB, throughput 0.975016
Reading from 31912: heap size 1048 MB, throughput 0.999104
Reading from 31912: heap size 1048 MB, throughput 0.998948
Reading from 31911: heap size 1500 MB, throughput 0.982014
Reading from 31912: heap size 1059 MB, throughput 0.998744
Reading from 31912: heap size 1059 MB, throughput 0.998821
Reading from 31911: heap size 1504 MB, throughput 0.977924
Reading from 31912: heap size 1070 MB, throughput 0.998909
Reading from 31911: heap size 1495 MB, throughput 0.969593
Reading from 31912: heap size 1070 MB, throughput 0.999119
Reading from 31912: heap size 1080 MB, throughput 0.999046
Equal recommendation: 3657 MB each
Reading from 31911: heap size 1504 MB, throughput 0.965804
Reading from 31912: heap size 1081 MB, throughput 0.999042
Reading from 31912: heap size 1091 MB, throughput 0.999019
Reading from 31911: heap size 1487 MB, throughput 0.963836
Reading from 31912: heap size 1091 MB, throughput 0.999214
Reading from 31912: heap size 1100 MB, throughput 0.998903
Reading from 31911: heap size 1496 MB, throughput 0.966665
Reading from 31912: heap size 1101 MB, throughput 0.999061
Reading from 31911: heap size 1481 MB, throughput 0.960407
Reading from 31912: heap size 1110 MB, throughput 0.998876
Equal recommendation: 3657 MB each
Reading from 31912: heap size 1110 MB, throughput 0.999033
Reading from 31911: heap size 1489 MB, throughput 0.964305
Reading from 31912: heap size 1121 MB, throughput 0.998961
Reading from 31912: heap size 1121 MB, throughput 0.998951
Reading from 31911: heap size 1491 MB, throughput 0.954295
Reading from 31912: heap size 1131 MB, throughput 0.999088
Reading from 31912: heap size 1131 MB, throughput 0.998602
Reading from 31911: heap size 1492 MB, throughput 0.954206
Reading from 31912: heap size 1142 MB, throughput 0.999005
Equal recommendation: 3657 MB each
Reading from 31911: heap size 1498 MB, throughput 0.954909
Reading from 31912: heap size 1142 MB, throughput 0.998862
Reading from 31912: heap size 1153 MB, throughput 0.999167
Reading from 31911: heap size 1503 MB, throughput 0.946435
Reading from 31912: heap size 1153 MB, throughput 0.998817
Reading from 31912: heap size 1164 MB, throughput 0.999329
Reading from 31911: heap size 1510 MB, throughput 0.946404
Reading from 31912: heap size 1164 MB, throughput 0.999106
Equal recommendation: 3657 MB each
Reading from 31912: heap size 1175 MB, throughput 0.999071
Reading from 31911: heap size 1519 MB, throughput 0.941007
Reading from 31912: heap size 1175 MB, throughput 0.999108
Reading from 31911: heap size 1530 MB, throughput 0.951261
Reading from 31912: heap size 1185 MB, throughput 0.999173
Reading from 31912: heap size 1185 MB, throughput 0.999153
Reading from 31911: heap size 1538 MB, throughput 0.941342
Reading from 31912: heap size 1195 MB, throughput 0.99905
Reading from 31912: heap size 1195 MB, throughput 0.999155
Reading from 31911: heap size 1549 MB, throughput 0.936748
Equal recommendation: 3657 MB each
Reading from 31912: heap size 1205 MB, throughput 0.999314
Reading from 31912: heap size 1205 MB, throughput 0.99899
Reading from 31911: heap size 1554 MB, throughput 0.950576
Reading from 31912: heap size 1214 MB, throughput 0.999179
Reading from 31911: heap size 1566 MB, throughput 0.95105
Reading from 31912: heap size 1214 MB, throughput 0.998958
Reading from 31912: heap size 1224 MB, throughput 0.999053
Reading from 31911: heap size 1569 MB, throughput 0.949509
Equal recommendation: 3657 MB each
Reading from 31912: heap size 1224 MB, throughput 0.999312
Reading from 31912: heap size 1234 MB, throughput 0.99896
Reading from 31911: heap size 1580 MB, throughput 0.94855
Reading from 31912: heap size 1235 MB, throughput 0.999273
Reading from 31912: heap size 1245 MB, throughput 0.998868
Reading from 31911: heap size 1582 MB, throughput 0.944641
Reading from 31912: heap size 1245 MB, throughput 0.998988
Reading from 31911: heap size 1593 MB, throughput 0.944287
Reading from 31912: heap size 1256 MB, throughput 0.999188
Equal recommendation: 3657 MB each
Reading from 31912: heap size 1256 MB, throughput 0.9991
Reading from 31911: heap size 1594 MB, throughput 0.949528
Reading from 31912: heap size 1266 MB, throughput 0.999239
Reading from 31912: heap size 1267 MB, throughput 0.999183
Reading from 31911: heap size 1604 MB, throughput 0.948248
Reading from 31912: heap size 1277 MB, throughput 0.999188
Reading from 31912: heap size 1277 MB, throughput 0.999274
Reading from 31911: heap size 1605 MB, throughput 0.943111
Equal recommendation: 3657 MB each
Reading from 31912: heap size 1287 MB, throughput 0.999084
Reading from 31912: heap size 1287 MB, throughput 0.999294
Reading from 31911: heap size 1615 MB, throughput 0.950185
Reading from 31912: heap size 1297 MB, throughput 0.998985
Reading from 31912: heap size 1297 MB, throughput 0.999031
Reading from 31911: heap size 1615 MB, throughput 0.666811
Reading from 31912: heap size 1307 MB, throughput 0.999347
Equal recommendation: 3657 MB each
Reading from 31912: heap size 1307 MB, throughput 0.999043
Reading from 31911: heap size 1699 MB, throughput 0.986473
Reading from 31912: heap size 1317 MB, throughput 0.999226
Reading from 31912: heap size 1317 MB, throughput 0.999136
Reading from 31911: heap size 1699 MB, throughput 0.982834
Reading from 31912: heap size 1253 MB, throughput 0.999061
Reading from 31912: heap size 1191 MB, throughput 0.999394
Equal recommendation: 3657 MB each
Reading from 31912: heap size 1130 MB, throughput 0.999288
Reading from 31912: heap size 1076 MB, throughput 0.999282
Reading from 31912: heap size 1023 MB, throughput 0.999309
Reading from 31912: heap size 973 MB, throughput 0.998976
Reading from 31912: heap size 928 MB, throughput 0.999012
Reading from 31912: heap size 883 MB, throughput 0.989602
Reading from 31912: heap size 842 MB, throughput 0.997854
Equal recommendation: 3657 MB each
Reading from 31912: heap size 854 MB, throughput 0.997878
Reading from 31912: heap size 869 MB, throughput 0.999197
Reading from 31911: heap size 1717 MB, throughput 0.987987
Reading from 31912: heap size 884 MB, throughput 0.999337
Reading from 31912: heap size 897 MB, throughput 0.998722
Reading from 31912: heap size 913 MB, throughput 0.999138
Reading from 31912: heap size 925 MB, throughput 0.998933
Reading from 31912: heap size 938 MB, throughput 0.999121
Equal recommendation: 3657 MB each
Reading from 31912: heap size 950 MB, throughput 0.99917
Reading from 31912: heap size 962 MB, throughput 0.998873
Reading from 31912: heap size 975 MB, throughput 0.998858
Client 31912 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 31911: heap size 1724 MB, throughput 0.995041
Reading from 31911: heap size 1729 MB, throughput 0.940087
Reading from 31911: heap size 1731 MB, throughput 0.815879
Reading from 31911: heap size 1722 MB, throughput 0.81522
Reading from 31911: heap size 1746 MB, throughput 0.797939
Reading from 31911: heap size 1773 MB, throughput 0.82096
Client 31911 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
