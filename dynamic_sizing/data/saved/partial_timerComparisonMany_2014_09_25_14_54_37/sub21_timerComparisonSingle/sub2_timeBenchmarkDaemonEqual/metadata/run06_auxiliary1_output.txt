economemd
    total memory: 3642 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub21_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 12952: heap size 9 MB, throughput 0.989834
Clients: 1
Client 12952 has a minimum heap size of 3 MB
Reading from 12950: heap size 9 MB, throughput 0.992583
Clients: 2
Client 12950 has a minimum heap size of 1211 MB
Reading from 12952: heap size 9 MB, throughput 0.974188
Reading from 12950: heap size 9 MB, throughput 0.972768
Reading from 12952: heap size 9 MB, throughput 0.955959
Reading from 12950: heap size 9 MB, throughput 0.955934
Reading from 12952: heap size 9 MB, throughput 0.9481
Reading from 12950: heap size 9 MB, throughput 0.932744
Reading from 12952: heap size 11 MB, throughput 0.966075
Reading from 12950: heap size 11 MB, throughput 0.971144
Reading from 12952: heap size 11 MB, throughput 0.981164
Reading from 12950: heap size 11 MB, throughput 0.961475
Reading from 12950: heap size 17 MB, throughput 0.907591
Reading from 12952: heap size 16 MB, throughput 0.98839
Reading from 12950: heap size 17 MB, throughput 0.490081
Reading from 12952: heap size 16 MB, throughput 0.975208
Reading from 12950: heap size 30 MB, throughput 0.859555
Reading from 12952: heap size 24 MB, throughput 0.968923
Reading from 12950: heap size 31 MB, throughput 0.971742
Reading from 12952: heap size 24 MB, throughput 0.917802
Reading from 12952: heap size 33 MB, throughput 0.993976
Reading from 12950: heap size 35 MB, throughput 0.562502
Reading from 12952: heap size 33 MB, throughput 0.993098
Reading from 12950: heap size 48 MB, throughput 0.820074
Reading from 12952: heap size 48 MB, throughput 0.99372
Reading from 12950: heap size 52 MB, throughput 0.937972
Reading from 12952: heap size 48 MB, throughput 0.995145
Reading from 12950: heap size 53 MB, throughput 0.206601
Reading from 12950: heap size 78 MB, throughput 0.751836
Reading from 12952: heap size 71 MB, throughput 0.965203
Reading from 12950: heap size 78 MB, throughput 0.27446
Reading from 12950: heap size 101 MB, throughput 0.702997
Reading from 12952: heap size 71 MB, throughput 0.996967
Reading from 12950: heap size 104 MB, throughput 0.831925
Reading from 12952: heap size 109 MB, throughput 0.996748
Reading from 12950: heap size 106 MB, throughput 0.171559
Reading from 12952: heap size 109 MB, throughput 0.993281
Reading from 12950: heap size 134 MB, throughput 0.851329
Reading from 12950: heap size 138 MB, throughput 0.790379
Reading from 12950: heap size 140 MB, throughput 0.774888
Reading from 12952: heap size 148 MB, throughput 0.996579
Reading from 12952: heap size 141 MB, throughput 0.996046
Reading from 12950: heap size 149 MB, throughput 0.139867
Reading from 12950: heap size 182 MB, throughput 0.732125
Reading from 12952: heap size 134 MB, throughput 0.995127
Reading from 12950: heap size 190 MB, throughput 0.826961
Reading from 12950: heap size 192 MB, throughput 0.752071
Reading from 12952: heap size 129 MB, throughput 0.995431
Reading from 12952: heap size 123 MB, throughput 0.993776
Reading from 12950: heap size 193 MB, throughput 0.196654
Reading from 12952: heap size 117 MB, throughput 0.995997
Reading from 12950: heap size 239 MB, throughput 0.743089
Reading from 12952: heap size 112 MB, throughput 0.995065
Reading from 12950: heap size 245 MB, throughput 0.778669
Reading from 12952: heap size 107 MB, throughput 0.993097
Reading from 12950: heap size 249 MB, throughput 0.654157
Reading from 12952: heap size 103 MB, throughput 0.992699
Reading from 12950: heap size 251 MB, throughput 0.625397
Reading from 12952: heap size 98 MB, throughput 0.993045
Reading from 12950: heap size 261 MB, throughput 0.66928
Reading from 12952: heap size 94 MB, throughput 0.993682
Reading from 12950: heap size 264 MB, throughput 0.581841
Reading from 12952: heap size 90 MB, throughput 0.991642
Reading from 12950: heap size 271 MB, throughput 0.647361
Reading from 12952: heap size 86 MB, throughput 0.994309
Reading from 12952: heap size 83 MB, throughput 0.989266
Reading from 12950: heap size 274 MB, throughput 0.67242
Reading from 12952: heap size 79 MB, throughput 0.991363
Reading from 12950: heap size 285 MB, throughput 0.473095
Reading from 12952: heap size 76 MB, throughput 0.991872
Reading from 12952: heap size 73 MB, throughput 0.988993
Reading from 12950: heap size 288 MB, throughput 0.448917
Reading from 12952: heap size 70 MB, throughput 0.983122
Reading from 12952: heap size 67 MB, throughput 0.987926
Reading from 12952: heap size 65 MB, throughput 0.991007
Reading from 12952: heap size 62 MB, throughput 0.982303
Reading from 12952: heap size 76 MB, throughput 0.989273
Reading from 12952: heap size 94 MB, throughput 0.992674
Reading from 12952: heap size 90 MB, throughput 0.994161
Reading from 12950: heap size 301 MB, throughput 0.0715456
Reading from 12952: heap size 86 MB, throughput 0.993006
Reading from 12952: heap size 82 MB, throughput 0.989296
Reading from 12950: heap size 346 MB, throughput 0.169766
Reading from 12952: heap size 79 MB, throughput 0.99327
Reading from 12952: heap size 76 MB, throughput 0.994495
Reading from 12952: heap size 73 MB, throughput 0.99034
Reading from 12952: heap size 70 MB, throughput 0.992122
Reading from 12950: heap size 391 MB, throughput 0.149416
Reading from 12952: heap size 67 MB, throughput 0.984601
Reading from 12952: heap size 64 MB, throughput 0.991797
Reading from 12952: heap size 62 MB, throughput 0.977132
Reading from 12950: heap size 442 MB, throughput 0.142157
Reading from 12952: heap size 73 MB, throughput 0.993954
Reading from 12950: heap size 461 MB, throughput 0.868935
Reading from 12952: heap size 87 MB, throughput 0.991552
Reading from 12950: heap size 474 MB, throughput 0.691578
Reading from 12952: heap size 84 MB, throughput 0.98905
Reading from 12950: heap size 476 MB, throughput 0.6901
Reading from 12952: heap size 80 MB, throughput 0.946455
Reading from 12950: heap size 467 MB, throughput 0.70745
Reading from 12952: heap size 96 MB, throughput 0.992226
Reading from 12950: heap size 382 MB, throughput 0.673771
Reading from 12950: heap size 455 MB, throughput 0.643894
Reading from 12952: heap size 114 MB, throughput 0.993291
Reading from 12950: heap size 401 MB, throughput 0.622997
Reading from 12952: heap size 130 MB, throughput 0.991561
Reading from 12950: heap size 460 MB, throughput 0.543815
Reading from 12952: heap size 135 MB, throughput 0.993002
Reading from 12950: heap size 460 MB, throughput 0.567912
Reading from 12952: heap size 136 MB, throughput 0.992785
Reading from 12950: heap size 461 MB, throughput 0.503858
Reading from 12952: heap size 129 MB, throughput 0.991776
Reading from 12950: heap size 466 MB, throughput 0.578558
Equal recommendation: 1821 MB each
Reading from 12950: heap size 468 MB, throughput 0.443889
Reading from 12952: heap size 124 MB, throughput 0.995064
Reading from 12950: heap size 477 MB, throughput 0.476507
Reading from 12952: heap size 118 MB, throughput 0.991236
Reading from 12950: heap size 480 MB, throughput 0.367788
Reading from 12952: heap size 113 MB, throughput 0.9938
Reading from 12950: heap size 490 MB, throughput 0.364651
Reading from 12952: heap size 108 MB, throughput 0.979737
Reading from 12950: heap size 494 MB, throughput 0.335835
Reading from 12952: heap size 129 MB, throughput 0.993094
Reading from 12950: heap size 505 MB, throughput 0.310295
Reading from 12950: heap size 511 MB, throughput 0.364669
Reading from 12952: heap size 123 MB, throughput 0.993947
Reading from 12950: heap size 515 MB, throughput 0.339802
Reading from 12952: heap size 118 MB, throughput 0.993119
Reading from 12950: heap size 521 MB, throughput 0.307598
Reading from 12952: heap size 113 MB, throughput 0.991107
Reading from 12952: heap size 108 MB, throughput 0.999964
Reading from 12952: heap size 108 MB, throughput 0.944444
Reading from 12952: heap size 108 MB, throughput 0.773372
Reading from 12952: heap size 103 MB, throughput 0.207604
Reading from 12952: heap size 122 MB, throughput 0.995661
Reading from 12952: heap size 144 MB, throughput 0.993293
Reading from 12952: heap size 155 MB, throughput 0.994344
Reading from 12950: heap size 521 MB, throughput 0.0576174
Reading from 12950: heap size 583 MB, throughput 0.28261
Reading from 12952: heap size 168 MB, throughput 0.994797
Reading from 12950: heap size 505 MB, throughput 0.429172
Reading from 12950: heap size 568 MB, throughput 0.406206
Reading from 12952: heap size 168 MB, throughput 0.995358
Reading from 12950: heap size 512 MB, throughput 0.529543
Reading from 12950: heap size 564 MB, throughput 0.477081
Reading from 12952: heap size 193 MB, throughput 0.994077
Reading from 12952: heap size 194 MB, throughput 0.994005
Reading from 12952: heap size 221 MB, throughput 0.995343
Reading from 12952: heap size 221 MB, throughput 0.994857
Reading from 12950: heap size 513 MB, throughput 0.0669992
Reading from 12952: heap size 250 MB, throughput 0.994383
Reading from 12950: heap size 630 MB, throughput 0.413821
Reading from 12950: heap size 634 MB, throughput 0.57713
Reading from 12950: heap size 629 MB, throughput 0.586959
Reading from 12952: heap size 251 MB, throughput 0.993486
Reading from 12950: heap size 632 MB, throughput 0.564749
Reading from 12952: heap size 239 MB, throughput 0.990148
Reading from 12950: heap size 628 MB, throughput 0.626634
Reading from 12950: heap size 630 MB, throughput 0.539829
Reading from 12952: heap size 271 MB, throughput 0.99549
Reading from 12952: heap size 258 MB, throughput 0.994553
Reading from 12952: heap size 246 MB, throughput 0.99336
Reading from 12950: heap size 632 MB, throughput 0.885309
Reading from 12952: heap size 234 MB, throughput 0.993996
Reading from 12952: heap size 223 MB, throughput 0.993338
Reading from 12950: heap size 635 MB, throughput 0.821059
Reading from 12952: heap size 213 MB, throughput 0.995089
Reading from 12952: heap size 203 MB, throughput 0.991729
Reading from 12950: heap size 633 MB, throughput 0.77561
Reading from 12952: heap size 193 MB, throughput 0.994652
Reading from 12952: heap size 184 MB, throughput 0.994786
Reading from 12952: heap size 176 MB, throughput 0.995812
Reading from 12952: heap size 168 MB, throughput 0.993357
Reading from 12952: heap size 160 MB, throughput 0.99625
Reading from 12952: heap size 153 MB, throughput 0.991115
Reading from 12952: heap size 146 MB, throughput 0.993054
Reading from 12950: heap size 647 MB, throughput 0.206575
Reading from 12952: heap size 139 MB, throughput 0.982097
Reading from 12950: heap size 737 MB, throughput 0.439818
Reading from 12950: heap size 744 MB, throughput 0.535581
Reading from 12952: heap size 159 MB, throughput 0.995137
Equal recommendation: 1821 MB each
Reading from 12950: heap size 748 MB, throughput 0.623561
Reading from 12952: heap size 181 MB, throughput 0.994204
Reading from 12950: heap size 749 MB, throughput 0.544438
Reading from 12950: heap size 748 MB, throughput 0.348674
Reading from 12950: heap size 695 MB, throughput 0.38163
Reading from 12952: heap size 204 MB, throughput 0.994365
Reading from 12950: heap size 742 MB, throughput 0.390055
Reading from 12950: heap size 746 MB, throughput 0.341363
Reading from 12952: heap size 231 MB, throughput 0.995383
Reading from 12952: heap size 220 MB, throughput 0.980391
Reading from 12952: heap size 251 MB, throughput 0.995844
Reading from 12952: heap size 271 MB, throughput 0.996454
Reading from 12952: heap size 271 MB, throughput 0.995669
Reading from 12950: heap size 747 MB, throughput 0.190222
Reading from 12952: heap size 258 MB, throughput 0.992925
Reading from 12950: heap size 836 MB, throughput 0.333512
Reading from 12950: heap size 839 MB, throughput 0.594292
Reading from 12950: heap size 841 MB, throughput 0.601687
Reading from 12952: heap size 246 MB, throughput 0.995777
Reading from 12950: heap size 846 MB, throughput 0.67745
Reading from 12952: heap size 234 MB, throughput 0.9945
Reading from 12950: heap size 847 MB, throughput 0.853797
Reading from 12950: heap size 852 MB, throughput 0.79647
Reading from 12952: heap size 223 MB, throughput 0.994628
Reading from 12950: heap size 856 MB, throughput 0.922895
Reading from 12950: heap size 862 MB, throughput 0.77368
Reading from 12952: heap size 213 MB, throughput 0.992818
Reading from 12952: heap size 203 MB, throughput 0.976609
Reading from 12950: heap size 863 MB, throughput 0.916257
Reading from 12950: heap size 857 MB, throughput 0.827354
Reading from 12952: heap size 232 MB, throughput 0.994381
Reading from 12950: heap size 862 MB, throughput 0.895967
Reading from 12952: heap size 265 MB, throughput 0.994906
Reading from 12950: heap size 841 MB, throughput 0.793533
Reading from 12950: heap size 761 MB, throughput 0.693083
Reading from 12952: heap size 301 MB, throughput 0.994989
Reading from 12950: heap size 838 MB, throughput 0.733932
Reading from 12950: heap size 766 MB, throughput 0.704612
Reading from 12952: heap size 302 MB, throughput 0.994382
Reading from 12950: heap size 829 MB, throughput 0.745743
Reading from 12950: heap size 836 MB, throughput 0.760777
Reading from 12952: heap size 341 MB, throughput 0.993859
Reading from 12950: heap size 824 MB, throughput 0.753559
Reading from 12950: heap size 830 MB, throughput 0.756874
Reading from 12950: heap size 821 MB, throughput 0.763348
Reading from 12952: heap size 325 MB, throughput 0.996656
Reading from 12950: heap size 826 MB, throughput 0.777152
Reading from 12950: heap size 821 MB, throughput 0.791008
Reading from 12952: heap size 308 MB, throughput 0.995265
Reading from 12952: heap size 295 MB, throughput 0.994593
Reading from 12950: heap size 825 MB, throughput 0.966687
Reading from 12952: heap size 281 MB, throughput 0.99467
Reading from 12952: heap size 267 MB, throughput 0.941482
Reading from 12952: heap size 255 MB, throughput 0.985095
Reading from 12952: heap size 244 MB, throughput 0.993542
Reading from 12952: heap size 232 MB, throughput 0.91439
Reading from 12952: heap size 224 MB, throughput 0.993582
Reading from 12952: heap size 241 MB, throughput 0.996648
Reading from 12952: heap size 257 MB, throughput 0.995825
Equal recommendation: 1821 MB each
Reading from 12950: heap size 826 MB, throughput 0.522037
Reading from 12952: heap size 274 MB, throughput 0.995362
Reading from 12950: heap size 924 MB, throughput 0.614142
Reading from 12952: heap size 292 MB, throughput 0.995169
Reading from 12950: heap size 929 MB, throughput 0.748638
Reading from 12952: heap size 310 MB, throughput 0.993943
Reading from 12950: heap size 929 MB, throughput 0.742061
Reading from 12950: heap size 935 MB, throughput 0.757122
Reading from 12952: heap size 331 MB, throughput 0.994842
Reading from 12950: heap size 935 MB, throughput 0.756295
Reading from 12950: heap size 942 MB, throughput 0.773128
Reading from 12952: heap size 337 MB, throughput 0.995442
Reading from 12950: heap size 942 MB, throughput 0.745242
Reading from 12950: heap size 948 MB, throughput 0.775523
Reading from 12952: heap size 338 MB, throughput 0.993229
Reading from 12950: heap size 949 MB, throughput 0.799419
Reading from 12952: heap size 360 MB, throughput 0.994037
Reading from 12950: heap size 957 MB, throughput 0.822934
Reading from 12952: heap size 360 MB, throughput 0.994111
Reading from 12950: heap size 958 MB, throughput 0.844417
Reading from 12950: heap size 972 MB, throughput 0.655966
Reading from 12952: heap size 384 MB, throughput 0.994444
Reading from 12950: heap size 974 MB, throughput 0.59759
Reading from 12952: heap size 384 MB, throughput 0.991457
Reading from 12952: heap size 411 MB, throughput 0.996372
Reading from 12952: heap size 411 MB, throughput 0.994814
Reading from 12950: heap size 970 MB, throughput 0.0704521
Reading from 12950: heap size 1157 MB, throughput 0.442521
Reading from 12950: heap size 1164 MB, throughput 0.747157
Reading from 12952: heap size 439 MB, throughput 0.994842
Reading from 12950: heap size 1169 MB, throughput 0.806679
Reading from 12950: heap size 1183 MB, throughput 0.722609
Reading from 12952: heap size 439 MB, throughput 0.992107
Reading from 12950: heap size 1183 MB, throughput 0.706357
Reading from 12950: heap size 1190 MB, throughput 0.790417
Reading from 12950: heap size 1193 MB, throughput 0.676597
Reading from 12952: heap size 469 MB, throughput 0.994931
Reading from 12950: heap size 1194 MB, throughput 0.670236
Reading from 12950: heap size 1199 MB, throughput 0.695254
Reading from 12952: heap size 469 MB, throughput 0.995279
Reading from 12952: heap size 501 MB, throughput 0.996068
Reading from 12952: heap size 502 MB, throughput 0.994012
Reading from 12950: heap size 1196 MB, throughput 0.962406
Reading from 12952: heap size 534 MB, throughput 0.99544
Equal recommendation: 1821 MB each
Reading from 12952: heap size 534 MB, throughput 0.994789
Reading from 12950: heap size 1203 MB, throughput 0.95455
Reading from 12952: heap size 568 MB, throughput 0.995433
Reading from 12952: heap size 568 MB, throughput 0.995158
Reading from 12952: heap size 604 MB, throughput 0.995285
Reading from 12950: heap size 1195 MB, throughput 0.937135
Reading from 12952: heap size 604 MB, throughput 0.99527
Reading from 12952: heap size 642 MB, throughput 0.995757
Reading from 12950: heap size 1204 MB, throughput 0.942322
Reading from 12952: heap size 642 MB, throughput 0.9947
Reading from 12952: heap size 681 MB, throughput 0.995642
Reading from 12952: heap size 682 MB, throughput 0.994462
Reading from 12950: heap size 1209 MB, throughput 0.948474
Reading from 12952: heap size 724 MB, throughput 0.995768
Reading from 12952: heap size 724 MB, throughput 0.995337
Reading from 12950: heap size 1211 MB, throughput 0.939942
Reading from 12952: heap size 769 MB, throughput 0.995412
Reading from 12952: heap size 769 MB, throughput 0.994705
Equal recommendation: 1821 MB each
Reading from 12950: heap size 1219 MB, throughput 0.93358
Reading from 12952: heap size 816 MB, throughput 0.995799
Reading from 12952: heap size 816 MB, throughput 0.995181
Reading from 12950: heap size 1220 MB, throughput 0.930865
Reading from 12952: heap size 865 MB, throughput 0.995896
Reading from 12952: heap size 866 MB, throughput 0.995314
Reading from 12950: heap size 1229 MB, throughput 0.941008
Reading from 12952: heap size 918 MB, throughput 0.995985
Reading from 12952: heap size 918 MB, throughput 0.995481
Reading from 12950: heap size 1232 MB, throughput 0.935639
Reading from 12952: heap size 972 MB, throughput 0.995733
Reading from 12950: heap size 1240 MB, throughput 0.946651
Reading from 12952: heap size 972 MB, throughput 0.995436
Reading from 12952: heap size 1030 MB, throughput 0.995652
Equal recommendation: 1821 MB each
Reading from 12950: heap size 1244 MB, throughput 0.928393
Reading from 12952: heap size 1030 MB, throughput 0.995684
Reading from 12952: heap size 1091 MB, throughput 0.995811
Reading from 12950: heap size 1252 MB, throughput 0.931764
Reading from 12952: heap size 1091 MB, throughput 0.995539
Reading from 12950: heap size 1257 MB, throughput 0.928878
Reading from 12952: heap size 1155 MB, throughput 0.994924
Reading from 12952: heap size 1155 MB, throughput 0.997475
Reading from 12950: heap size 1265 MB, throughput 0.950381
Reading from 12952: heap size 1217 MB, throughput 0.995726
Reading from 12950: heap size 1271 MB, throughput 0.937655
Reading from 12952: heap size 1218 MB, throughput 0.996153
Equal recommendation: 1821 MB each
Reading from 12952: heap size 1217 MB, throughput 0.996038
Reading from 12950: heap size 1279 MB, throughput 0.932168
Reading from 12952: heap size 1217 MB, throughput 0.996148
Reading from 12950: heap size 1284 MB, throughput 0.924203
Reading from 12952: heap size 1217 MB, throughput 0.996104
Reading from 12952: heap size 1217 MB, throughput 0.996024
Reading from 12950: heap size 1293 MB, throughput 0.95442
Reading from 12952: heap size 1217 MB, throughput 0.99628
Reading from 12950: heap size 1296 MB, throughput 0.950467
Reading from 12952: heap size 1217 MB, throughput 0.99622
Reading from 12952: heap size 1217 MB, throughput 0.996115
Equal recommendation: 1821 MB each
Reading from 12950: heap size 1305 MB, throughput 0.938818
Reading from 12952: heap size 1217 MB, throughput 0.99611
Reading from 12952: heap size 1217 MB, throughput 0.996176
Reading from 12950: heap size 1306 MB, throughput 0.933687
Reading from 12952: heap size 1217 MB, throughput 0.996243
Reading from 12950: heap size 1315 MB, throughput 0.938336
Reading from 12952: heap size 1217 MB, throughput 0.996137
Reading from 12952: heap size 1216 MB, throughput 0.996194
Reading from 12950: heap size 1316 MB, throughput 0.934686
Reading from 12952: heap size 1217 MB, throughput 0.996118
Reading from 12952: heap size 1217 MB, throughput 0.9961
Equal recommendation: 1821 MB each
Reading from 12950: heap size 1325 MB, throughput 0.935843
Reading from 12952: heap size 1218 MB, throughput 0.996146
Reading from 12952: heap size 1218 MB, throughput 0.99602
Reading from 12950: heap size 1325 MB, throughput 0.9436
Reading from 12952: heap size 1218 MB, throughput 0.996221
Reading from 12952: heap size 1218 MB, throughput 0.994898
Reading from 12950: heap size 1334 MB, throughput 0.940973
Reading from 12952: heap size 1218 MB, throughput 0.996441
Reading from 12950: heap size 1335 MB, throughput 0.936972
Reading from 12952: heap size 1218 MB, throughput 0.996182
Reading from 12952: heap size 1218 MB, throughput 0.996207
Equal recommendation: 1821 MB each
Reading from 12950: heap size 1343 MB, throughput 0.948445
Reading from 12952: heap size 1216 MB, throughput 0.996237
Reading from 12952: heap size 1217 MB, throughput 0.996073
Reading from 12952: heap size 1217 MB, throughput 0.996385
Reading from 12950: heap size 1344 MB, throughput 0.507827
Reading from 12952: heap size 1218 MB, throughput 0.997211
Reading from 12952: heap size 1218 MB, throughput 0.996137
Reading from 12950: heap size 1417 MB, throughput 0.986135
Reading from 12952: heap size 1218 MB, throughput 0.99608
Equal recommendation: 1821 MB each
Reading from 12952: heap size 1218 MB, throughput 0.99614
Reading from 12950: heap size 1417 MB, throughput 0.982019
Reading from 12952: heap size 1218 MB, throughput 0.996181
Reading from 12952: heap size 1218 MB, throughput 0.996171
Reading from 12950: heap size 1431 MB, throughput 0.977573
Reading from 12952: heap size 1218 MB, throughput 0.996205
Reading from 12952: heap size 1218 MB, throughput 0.99615
Reading from 12950: heap size 1436 MB, throughput 0.974965
Reading from 12952: heap size 1218 MB, throughput 0.996087
Reading from 12952: heap size 1218 MB, throughput 0.996186
Reading from 12950: heap size 1440 MB, throughput 0.96988
Equal recommendation: 1821 MB each
Reading from 12952: heap size 1218 MB, throughput 0.996216
Reading from 12952: heap size 1218 MB, throughput 0.996054
Reading from 12950: heap size 1442 MB, throughput 0.969683
Reading from 12952: heap size 1218 MB, throughput 0.996149
Reading from 12952: heap size 1218 MB, throughput 0.99609
Reading from 12950: heap size 1431 MB, throughput 0.964449
Reading from 12952: heap size 1218 MB, throughput 0.996118
Reading from 12952: heap size 1218 MB, throughput 0.996106
Reading from 12952: heap size 1218 MB, throughput 0.99473
Reading from 12950: heap size 1339 MB, throughput 0.964532
Reading from 12952: heap size 1218 MB, throughput 0.996087
Equal recommendation: 1821 MB each
Reading from 12952: heap size 1218 MB, throughput 0.996276
Reading from 12950: heap size 1424 MB, throughput 0.959712
Reading from 12952: heap size 1218 MB, throughput 0.996311
Reading from 12952: heap size 1218 MB, throughput 0.996221
Reading from 12950: heap size 1430 MB, throughput 0.964179
Reading from 12952: heap size 1218 MB, throughput 0.996157
Reading from 12952: heap size 1218 MB, throughput 0.99612
Reading from 12950: heap size 1434 MB, throughput 0.951587
Reading from 12952: heap size 1218 MB, throughput 0.996221
Reading from 12952: heap size 1218 MB, throughput 0.996251
Equal recommendation: 1821 MB each
Reading from 12952: heap size 1218 MB, throughput 0.996158
Reading from 12952: heap size 1218 MB, throughput 0.996212
Reading from 12952: heap size 1218 MB, throughput 0.996135
Reading from 12952: heap size 1218 MB, throughput 0.996033
Reading from 12952: heap size 1218 MB, throughput 0.996107
Reading from 12952: heap size 1218 MB, throughput 0.99612
Reading from 12952: heap size 1218 MB, throughput 0.996144
Equal recommendation: 1821 MB each
Reading from 12952: heap size 1218 MB, throughput 0.996108
Reading from 12952: heap size 1218 MB, throughput 0.994385
Reading from 12950: heap size 1435 MB, throughput 0.983041
Reading from 12952: heap size 1218 MB, throughput 0.996277
Reading from 12952: heap size 1218 MB, throughput 0.996146
Reading from 12952: heap size 1218 MB, throughput 0.996119
Reading from 12952: heap size 1218 MB, throughput 0.997982
Equal recommendation: 1821 MB each
Reading from 12952: heap size 1218 MB, throughput 0.996044
Reading from 12952: heap size 1218 MB, throughput 0.994005
Reading from 12952: heap size 1218 MB, throughput 0.996175
Reading from 12952: heap size 1218 MB, throughput 0.995796
Reading from 12950: heap size 1399 MB, throughput 0.905137
Reading from 12952: heap size 1218 MB, throughput 0.996364
Reading from 12952: heap size 1218 MB, throughput 0.996105
Reading from 12952: heap size 1218 MB, throughput 0.996182
Equal recommendation: 1821 MB each
Reading from 12950: heap size 1501 MB, throughput 0.989314
Reading from 12952: heap size 1218 MB, throughput 0.996162
Reading from 12950: heap size 1542 MB, throughput 0.892641
Reading from 12950: heap size 1416 MB, throughput 0.806925
Reading from 12950: heap size 1538 MB, throughput 0.838117
Reading from 12952: heap size 1218 MB, throughput 0.996179
Reading from 12950: heap size 1541 MB, throughput 0.823446
Reading from 12950: heap size 1542 MB, throughput 0.842831
Reading from 12950: heap size 1548 MB, throughput 0.926774
Reading from 12952: heap size 1218 MB, throughput 0.996235
Reading from 12952: heap size 1218 MB, throughput 0.996578
Reading from 12950: heap size 1563 MB, throughput 0.987074
Reading from 12952: heap size 1218 MB, throughput 0.996129
Reading from 12952: heap size 1218 MB, throughput 0.996023
Reading from 12950: heap size 1567 MB, throughput 0.979674
Reading from 12952: heap size 1218 MB, throughput 0.996231
Equal recommendation: 1821 MB each
Reading from 12952: heap size 1218 MB, throughput 0.99616
Reading from 12952: heap size 1218 MB, throughput 0.996115
Reading from 12950: heap size 1581 MB, throughput 0.97705
Reading from 12952: heap size 1218 MB, throughput 0.996112
Reading from 12952: heap size 1218 MB, throughput 0.996108
Reading from 12950: heap size 1586 MB, throughput 0.973037
Reading from 12952: heap size 1218 MB, throughput 0.996102
Reading from 12952: heap size 1158 MB, throughput 0.984017
Reading from 12952: heap size 1101 MB, throughput 0.99606
Reading from 12950: heap size 1580 MB, throughput 0.978785
Equal recommendation: 1821 MB each
Reading from 12952: heap size 1178 MB, throughput 0.996131
Reading from 12952: heap size 1216 MB, throughput 0.996153
Reading from 12950: heap size 1590 MB, throughput 0.969215
Reading from 12952: heap size 1216 MB, throughput 0.995896
Reading from 12952: heap size 1222 MB, throughput 0.996108
Reading from 12952: heap size 1222 MB, throughput 0.996098
Reading from 12950: heap size 1578 MB, throughput 0.967582
Reading from 12952: heap size 1222 MB, throughput 0.996059
Reading from 12952: heap size 1222 MB, throughput 0.996125
Equal recommendation: 1821 MB each
Reading from 12950: heap size 1587 MB, throughput 0.966024
Reading from 12952: heap size 1222 MB, throughput 0.996147
Reading from 12952: heap size 1222 MB, throughput 0.996024
Reading from 12952: heap size 1222 MB, throughput 0.996099
Reading from 12950: heap size 1597 MB, throughput 0.96117
Reading from 12952: heap size 1222 MB, throughput 0.99608
Reading from 12952: heap size 1222 MB, throughput 0.996121
Reading from 12952: heap size 1222 MB, throughput 0.995995
Reading from 12950: heap size 1597 MB, throughput 0.962967
Reading from 12952: heap size 1222 MB, throughput 0.996026
Equal recommendation: 1821 MB each
Reading from 12952: heap size 1222 MB, throughput 0.996165
Reading from 12952: heap size 1222 MB, throughput 0.995548
Reading from 12950: heap size 1606 MB, throughput 0.95793
Reading from 12952: heap size 1222 MB, throughput 0.9962
Reading from 12952: heap size 1222 MB, throughput 0.996136
Reading from 12950: heap size 1609 MB, throughput 0.963229
Reading from 12952: heap size 1222 MB, throughput 0.996128
Reading from 12952: heap size 1222 MB, throughput 0.996009
Reading from 12952: heap size 1222 MB, throughput 0.996123
Reading from 12950: heap size 1621 MB, throughput 0.95825
Equal recommendation: 1821 MB each
Reading from 12952: heap size 1222 MB, throughput 0.996173
Reading from 12952: heap size 1222 MB, throughput 0.996053
Reading from 12952: heap size 1222 MB, throughput 0.996438
Reading from 12950: heap size 1629 MB, throughput 0.963732
Reading from 12952: heap size 1221 MB, throughput 0.996912
Reading from 12952: heap size 1221 MB, throughput 0.996176
Reading from 12950: heap size 1642 MB, throughput 0.954794
Reading from 12952: heap size 1221 MB, throughput 0.996119
Reading from 12952: heap size 1221 MB, throughput 0.996129
Equal recommendation: 1821 MB each
Reading from 12952: heap size 1222 MB, throughput 0.995935
Reading from 12950: heap size 1655 MB, throughput 0.951674
Reading from 12952: heap size 1222 MB, throughput 0.996264
Reading from 12952: heap size 1222 MB, throughput 0.996185
Reading from 12952: heap size 1222 MB, throughput 0.996101
Reading from 12950: heap size 1672 MB, throughput 0.955833
Reading from 12952: heap size 1222 MB, throughput 0.996396
Reading from 12952: heap size 1222 MB, throughput 0.996159
Reading from 12952: heap size 1222 MB, throughput 0.994582
Reading from 12950: heap size 1684 MB, throughput 0.948874
Equal recommendation: 1821 MB each
Reading from 12952: heap size 1222 MB, throughput 0.996071
Reading from 12952: heap size 1222 MB, throughput 0.996083
Reading from 12950: heap size 1702 MB, throughput 0.955939
Reading from 12952: heap size 1222 MB, throughput 0.996136
Reading from 12952: heap size 1222 MB, throughput 0.996088
Reading from 12952: heap size 1222 MB, throughput 0.996237
Reading from 12950: heap size 1710 MB, throughput 0.954917
Reading from 12952: heap size 1222 MB, throughput 0.996265
Reading from 12952: heap size 1222 MB, throughput 0.995957
Equal recommendation: 1821 MB each
Reading from 12952: heap size 1222 MB, throughput 0.995825
Reading from 12950: heap size 1728 MB, throughput 0.958132
Reading from 12952: heap size 1222 MB, throughput 0.996269
Reading from 12952: heap size 1222 MB, throughput 0.996087
Reading from 12952: heap size 1222 MB, throughput 0.995948
Reading from 12950: heap size 1733 MB, throughput 0.949477
Reading from 12952: heap size 1222 MB, throughput 0.996211
Reading from 12952: heap size 1222 MB, throughput 0.996146
Reading from 12952: heap size 1222 MB, throughput 0.996011
Reading from 12950: heap size 1750 MB, throughput 0.954372
Equal recommendation: 1821 MB each
Reading from 12952: heap size 1221 MB, throughput 0.996359
Reading from 12952: heap size 1222 MB, throughput 0.996113
Reading from 12952: heap size 1222 MB, throughput 0.996169
Reading from 12950: heap size 1754 MB, throughput 0.955844
Reading from 12952: heap size 1222 MB, throughput 0.996182
Reading from 12952: heap size 1221 MB, throughput 0.996031
Reading from 12952: heap size 1222 MB, throughput 0.996155
Reading from 12950: heap size 1772 MB, throughput 0.94954
Reading from 12952: heap size 1222 MB, throughput 0.996194
Equal recommendation: 1821 MB each
Reading from 12952: heap size 1222 MB, throughput 0.996001
Reading from 12952: heap size 1221 MB, throughput 0.996214
Reading from 12950: heap size 1774 MB, throughput 0.950111
Reading from 12952: heap size 1222 MB, throughput 0.996345
Reading from 12952: heap size 1222 MB, throughput 0.995911
Reading from 12952: heap size 1222 MB, throughput 0.996101
Reading from 12952: heap size 1222 MB, throughput 0.996148
Reading from 12952: heap size 1222 MB, throughput 0.996134
Equal recommendation: 1821 MB each
Reading from 12952: heap size 1222 MB, throughput 0.997024
Reading from 12952: heap size 1222 MB, throughput 0.987443
Reading from 12952: heap size 1222 MB, throughput 0.995961
Reading from 12952: heap size 1222 MB, throughput 0.996023
Reading from 12952: heap size 1222 MB, throughput 0.996086
Reading from 12952: heap size 1222 MB, throughput 0.996078
Reading from 12952: heap size 1222 MB, throughput 0.996089
Equal recommendation: 1821 MB each
Reading from 12952: heap size 1222 MB, throughput 0.99606
Reading from 12952: heap size 1222 MB, throughput 0.996126
Reading from 12952: heap size 1222 MB, throughput 0.997474
Reading from 12952: heap size 1222 MB, throughput 0.996204
Reading from 12952: heap size 1222 MB, throughput 0.99612
Reading from 12952: heap size 1222 MB, throughput 0.996097
Equal recommendation: 1821 MB each
Reading from 12950: heap size 1793 MB, throughput 0.980951
Reading from 12952: heap size 1222 MB, throughput 0.9969
Reading from 12952: heap size 1222 MB, throughput 0.996087
Reading from 12952: heap size 1222 MB, throughput 0.996087
Reading from 12952: heap size 1221 MB, throughput 0.996286
Reading from 12950: heap size 1794 MB, throughput 0.925901
Reading from 12950: heap size 1818 MB, throughput 0.270118
Reading from 12952: heap size 1222 MB, throughput 0.998813
Reading from 12950: heap size 1822 MB, throughput 0.205726
Equal recommendation: 1821 MB each
Reading from 12950: heap size 1799 MB, throughput 0.994877
Reading from 12952: heap size 1222 MB, throughput 0.996666
Client 12950 died
Clients: 1
Reading from 12952: heap size 1222 MB, throughput 0.996136
Reading from 12952: heap size 1221 MB, throughput 0.99613
Reading from 12952: heap size 1222 MB, throughput 0.993952
Reading from 12952: heap size 1221 MB, throughput 0.996188
Reading from 12952: heap size 1222 MB, throughput 0.996197
Reading from 12952: heap size 1222 MB, throughput 0.996216
Client 12952 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
