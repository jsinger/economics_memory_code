economemd
    total memory: 1049 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_30_14_13_41/sub5_timerComparisonSingle/sub3_timeBenchmarkDaemonEqual/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
TimerThread: starting
CommandThread: starting
ReadingThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 8581: heap size 9 MB, throughput 0.990907
Clients: 1
Client 8581 has a minimum heap size of 276 MB
Reading from 8580: heap size 9 MB, throughput 0.991611
Clients: 2
Client 8580 has a minimum heap size of 276 MB
Reading from 8581: heap size 9 MB, throughput 0.978567
Reading from 8580: heap size 9 MB, throughput 0.981137
Reading from 8581: heap size 9 MB, throughput 0.965316
Reading from 8580: heap size 9 MB, throughput 0.933923
Reading from 8581: heap size 9 MB, throughput 0.949497
Reading from 8580: heap size 9 MB, throughput 0.946726
Reading from 8581: heap size 11 MB, throughput 0.984015
Reading from 8580: heap size 11 MB, throughput 0.933981
Reading from 8580: heap size 11 MB, throughput 0.972682
Reading from 8581: heap size 11 MB, throughput 0.988379
Reading from 8580: heap size 17 MB, throughput 0.934984
Reading from 8581: heap size 17 MB, throughput 0.975545
Reading from 8580: heap size 17 MB, throughput 0.688256
Reading from 8581: heap size 17 MB, throughput 0.488025
Reading from 8580: heap size 30 MB, throughput 0.885701
Reading from 8581: heap size 30 MB, throughput 0.963415
Reading from 8580: heap size 31 MB, throughput 0.948766
Reading from 8581: heap size 31 MB, throughput 0.815738
Reading from 8580: heap size 34 MB, throughput 0.260721
Reading from 8581: heap size 34 MB, throughput 0.495856
Reading from 8580: heap size 47 MB, throughput 0.754128
Reading from 8581: heap size 47 MB, throughput 0.915856
Reading from 8580: heap size 48 MB, throughput 0.867531
Reading from 8581: heap size 48 MB, throughput 0.915346
Reading from 8580: heap size 50 MB, throughput 0.326215
Reading from 8581: heap size 50 MB, throughput 0.219256
Reading from 8580: heap size 72 MB, throughput 0.88931
Reading from 8581: heap size 71 MB, throughput 0.84728
Reading from 8580: heap size 72 MB, throughput 0.244757
Reading from 8581: heap size 71 MB, throughput 0.247883
Reading from 8580: heap size 100 MB, throughput 0.911753
Reading from 8581: heap size 97 MB, throughput 0.776159
Reading from 8581: heap size 98 MB, throughput 0.73764
Reading from 8580: heap size 100 MB, throughput 0.868029
Reading from 8581: heap size 99 MB, throughput 0.760569
Reading from 8580: heap size 101 MB, throughput 0.770237
Reading from 8580: heap size 103 MB, throughput 0.699353
Reading from 8581: heap size 102 MB, throughput 0.193185
Reading from 8580: heap size 105 MB, throughput 0.166459
Reading from 8581: heap size 136 MB, throughput 0.742886
Reading from 8580: heap size 135 MB, throughput 0.741955
Reading from 8581: heap size 137 MB, throughput 0.742436
Reading from 8580: heap size 138 MB, throughput 0.796809
Reading from 8581: heap size 139 MB, throughput 0.789648
Reading from 8580: heap size 141 MB, throughput 0.785789
Reading from 8581: heap size 144 MB, throughput 0.790151
Reading from 8580: heap size 147 MB, throughput 0.741607
Reading from 8580: heap size 150 MB, throughput 0.597709
Reading from 8581: heap size 147 MB, throughput 0.110185
Reading from 8581: heap size 189 MB, throughput 0.625165
Reading from 8580: heap size 152 MB, throughput 0.074863
Reading from 8581: heap size 191 MB, throughput 0.749905
Reading from 8580: heap size 192 MB, throughput 0.616818
Reading from 8581: heap size 194 MB, throughput 0.758109
Reading from 8580: heap size 195 MB, throughput 0.598818
Reading from 8580: heap size 199 MB, throughput 0.555015
Reading from 8581: heap size 202 MB, throughput 0.890387
Reading from 8580: heap size 202 MB, throughput 0.86421
Reading from 8581: heap size 205 MB, throughput 0.900696
Reading from 8580: heap size 209 MB, throughput 0.856979
Reading from 8580: heap size 212 MB, throughput 0.763455
Reading from 8580: heap size 222 MB, throughput 0.447759
Reading from 8580: heap size 226 MB, throughput 0.335086
Reading from 8580: heap size 233 MB, throughput 0.410647
Reading from 8580: heap size 240 MB, throughput 0.701799
Reading from 8581: heap size 206 MB, throughput 0.264942
Reading from 8581: heap size 262 MB, throughput 0.697475
Reading from 8581: heap size 262 MB, throughput 0.878397
Reading from 8581: heap size 263 MB, throughput 0.811065
Reading from 8580: heap size 241 MB, throughput 0.0862088
Reading from 8580: heap size 271 MB, throughput 0.392678
Reading from 8581: heap size 265 MB, throughput 0.711803
Reading from 8580: heap size 276 MB, throughput 0.755983
Reading from 8581: heap size 265 MB, throughput 0.796732
Reading from 8580: heap size 273 MB, throughput 0.828066
Reading from 8580: heap size 275 MB, throughput 0.811406
Reading from 8580: heap size 270 MB, throughput 0.688057
Reading from 8581: heap size 269 MB, throughput 0.76019
Reading from 8581: heap size 269 MB, throughput 0.710798
Reading from 8580: heap size 273 MB, throughput 0.86138
Reading from 8581: heap size 274 MB, throughput 0.646072
Reading from 8580: heap size 264 MB, throughput 0.60979
Reading from 8580: heap size 269 MB, throughput 0.509354
Reading from 8581: heap size 277 MB, throughput 0.744524
Reading from 8581: heap size 283 MB, throughput 0.687411
Reading from 8581: heap size 285 MB, throughput 0.866333
Reading from 8580: heap size 267 MB, throughput 0.104153
Reading from 8580: heap size 311 MB, throughput 0.525453
Reading from 8580: heap size 314 MB, throughput 0.776276
Reading from 8580: heap size 314 MB, throughput 0.762669
Reading from 8581: heap size 287 MB, throughput 0.901971
Reading from 8580: heap size 315 MB, throughput 0.764485
Reading from 8581: heap size 289 MB, throughput 0.616286
Reading from 8581: heap size 292 MB, throughput 0.794349
Reading from 8581: heap size 294 MB, throughput 0.862585
Reading from 8581: heap size 295 MB, throughput 0.915032
Reading from 8580: heap size 316 MB, throughput 0.939517
Reading from 8580: heap size 311 MB, throughput 0.661264
Reading from 8581: heap size 297 MB, throughput 0.904448
Reading from 8580: heap size 315 MB, throughput 0.724241
Reading from 8581: heap size 300 MB, throughput 0.816423
Reading from 8580: heap size 311 MB, throughput 0.761667
Reading from 8581: heap size 301 MB, throughput 0.677424
Reading from 8580: heap size 313 MB, throughput 0.815452
Reading from 8581: heap size 307 MB, throughput 0.7367
Reading from 8580: heap size 310 MB, throughput 0.840239
Reading from 8581: heap size 307 MB, throughput 0.581968
Reading from 8581: heap size 314 MB, throughput 0.558319
Reading from 8581: heap size 314 MB, throughput 0.712047
Reading from 8580: heap size 313 MB, throughput 0.930159
Reading from 8580: heap size 310 MB, throughput 0.816756
Reading from 8580: heap size 313 MB, throughput 0.755172
Reading from 8580: heap size 316 MB, throughput 0.549561
Reading from 8580: heap size 317 MB, throughput 0.590387
Reading from 8580: heap size 322 MB, throughput 0.626768
Equal recommendation: 524 MB each
Reading from 8580: heap size 324 MB, throughput 0.555785
Reading from 8580: heap size 330 MB, throughput 0.598666
Reading from 8580: heap size 332 MB, throughput 0.677866
Reading from 8581: heap size 321 MB, throughput 0.96598
Reading from 8580: heap size 339 MB, throughput 0.976434
Reading from 8581: heap size 322 MB, throughput 0.96307
Reading from 8580: heap size 340 MB, throughput 0.985595
Reading from 8580: heap size 344 MB, throughput 0.961317
Reading from 8581: heap size 325 MB, throughput 0.665337
Reading from 8580: heap size 345 MB, throughput 0.973058
Reading from 8581: heap size 369 MB, throughput 0.968685
Reading from 8580: heap size 344 MB, throughput 0.96824
Reading from 8581: heap size 374 MB, throughput 0.987632
Reading from 8580: heap size 347 MB, throughput 0.974277
Reading from 8581: heap size 375 MB, throughput 0.985181
Reading from 8580: heap size 343 MB, throughput 0.967871
Reading from 8581: heap size 375 MB, throughput 0.977012
Reading from 8580: heap size 346 MB, throughput 0.962574
Equal recommendation: 524 MB each
Reading from 8580: heap size 344 MB, throughput 0.974636
Reading from 8581: heap size 378 MB, throughput 0.982722
Reading from 8580: heap size 346 MB, throughput 0.971626
Reading from 8581: heap size 375 MB, throughput 0.982352
Reading from 8580: heap size 348 MB, throughput 0.965694
Reading from 8581: heap size 378 MB, throughput 0.977923
Reading from 8580: heap size 348 MB, throughput 0.970953
Reading from 8581: heap size 377 MB, throughput 0.982462
Reading from 8580: heap size 350 MB, throughput 0.969556
Reading from 8580: heap size 351 MB, throughput 0.972573
Reading from 8581: heap size 378 MB, throughput 0.977799
Reading from 8581: heap size 379 MB, throughput 0.97187
Reading from 8580: heap size 353 MB, throughput 0.971434
Reading from 8580: heap size 354 MB, throughput 0.973439
Reading from 8581: heap size 381 MB, throughput 0.97452
Equal recommendation: 524 MB each
Reading from 8581: heap size 380 MB, throughput 0.979245
Reading from 8581: heap size 385 MB, throughput 0.80613
Reading from 8581: heap size 382 MB, throughput 0.615205
Reading from 8580: heap size 356 MB, throughput 0.981545
Reading from 8581: heap size 387 MB, throughput 0.792667
Reading from 8581: heap size 396 MB, throughput 0.807237
Reading from 8580: heap size 358 MB, throughput 0.962505
Reading from 8580: heap size 357 MB, throughput 0.784983
Reading from 8580: heap size 358 MB, throughput 0.751504
Reading from 8580: heap size 365 MB, throughput 0.693905
Reading from 8581: heap size 399 MB, throughput 0.975456
Reading from 8580: heap size 368 MB, throughput 0.763289
Reading from 8580: heap size 376 MB, throughput 0.977331
Reading from 8581: heap size 405 MB, throughput 0.989925
Reading from 8580: heap size 378 MB, throughput 0.989502
Reading from 8581: heap size 407 MB, throughput 0.985733
Reading from 8580: heap size 379 MB, throughput 0.746546
Reading from 8581: heap size 406 MB, throughput 0.989357
Reading from 8580: heap size 406 MB, throughput 0.995297
Reading from 8581: heap size 409 MB, throughput 0.989267
Reading from 8580: heap size 407 MB, throughput 0.991934
Equal recommendation: 524 MB each
Reading from 8581: heap size 406 MB, throughput 0.984051
Reading from 8580: heap size 410 MB, throughput 0.991447
Reading from 8581: heap size 409 MB, throughput 0.985178
Reading from 8580: heap size 413 MB, throughput 0.988057
Reading from 8581: heap size 405 MB, throughput 0.988299
Reading from 8580: heap size 414 MB, throughput 0.990715
Reading from 8581: heap size 408 MB, throughput 0.98174
Reading from 8580: heap size 410 MB, throughput 0.989531
Reading from 8580: heap size 413 MB, throughput 0.982187
Reading from 8581: heap size 406 MB, throughput 0.985041
Reading from 8580: heap size 406 MB, throughput 0.982188
Reading from 8581: heap size 407 MB, throughput 0.982207
Reading from 8580: heap size 410 MB, throughput 0.970827
Equal recommendation: 524 MB each
Reading from 8581: heap size 407 MB, throughput 0.983332
Reading from 8580: heap size 410 MB, throughput 0.977795
Reading from 8581: heap size 408 MB, throughput 0.94148
Reading from 8581: heap size 407 MB, throughput 0.851739
Reading from 8581: heap size 409 MB, throughput 0.845289
Reading from 8581: heap size 415 MB, throughput 0.9492
Reading from 8580: heap size 410 MB, throughput 0.986442
Reading from 8580: heap size 408 MB, throughput 0.867783
Reading from 8580: heap size 410 MB, throughput 0.79774
Reading from 8580: heap size 416 MB, throughput 0.815517
Reading from 8580: heap size 421 MB, throughput 0.858359
Reading from 8581: heap size 416 MB, throughput 0.988022
Reading from 8580: heap size 428 MB, throughput 0.986084
Reading from 8581: heap size 421 MB, throughput 0.99052
Reading from 8580: heap size 430 MB, throughput 0.983988
Reading from 8581: heap size 422 MB, throughput 0.990623
Reading from 8580: heap size 429 MB, throughput 0.990007
Reading from 8581: heap size 423 MB, throughput 0.988324
Equal recommendation: 524 MB each
Reading from 8580: heap size 432 MB, throughput 0.978883
Reading from 8581: heap size 425 MB, throughput 0.985216
Reading from 8580: heap size 429 MB, throughput 0.983515
Reading from 8581: heap size 422 MB, throughput 0.984519
Reading from 8580: heap size 432 MB, throughput 0.983293
Reading from 8581: heap size 425 MB, throughput 0.984127
Reading from 8580: heap size 432 MB, throughput 0.980065
Reading from 8580: heap size 433 MB, throughput 0.98254
Reading from 8581: heap size 424 MB, throughput 0.984097
Reading from 8580: heap size 434 MB, throughput 0.981631
Reading from 8581: heap size 425 MB, throughput 0.980313
Equal recommendation: 524 MB each
Reading from 8580: heap size 435 MB, throughput 0.982078
Reading from 8581: heap size 427 MB, throughput 0.991013
Reading from 8581: heap size 428 MB, throughput 0.915446
Reading from 8581: heap size 428 MB, throughput 0.872781
Reading from 8581: heap size 430 MB, throughput 0.86779
Reading from 8580: heap size 438 MB, throughput 0.989074
Reading from 8580: heap size 439 MB, throughput 0.883136
Reading from 8580: heap size 439 MB, throughput 0.893847
Reading from 8580: heap size 441 MB, throughput 0.862134
Reading from 8581: heap size 435 MB, throughput 0.987846
Reading from 8580: heap size 447 MB, throughput 0.990988
Reading from 8581: heap size 437 MB, throughput 0.992841
Reading from 8580: heap size 448 MB, throughput 0.989303
Reading from 8581: heap size 441 MB, throughput 0.99015
Reading from 8580: heap size 452 MB, throughput 0.988748
Equal recommendation: 524 MB each
Reading from 8581: heap size 442 MB, throughput 0.988578
Reading from 8580: heap size 454 MB, throughput 0.984704
Reading from 8581: heap size 440 MB, throughput 0.991726
Reading from 8580: heap size 454 MB, throughput 0.987714
Reading from 8581: heap size 443 MB, throughput 0.985441
Reading from 8580: heap size 456 MB, throughput 0.983014
Reading from 8581: heap size 441 MB, throughput 0.989927
Reading from 8580: heap size 454 MB, throughput 0.986072
Reading from 8581: heap size 443 MB, throughput 0.986458
Reading from 8580: heap size 456 MB, throughput 0.985123
Equal recommendation: 524 MB each
Reading from 8580: heap size 458 MB, throughput 0.981118
Reading from 8581: heap size 445 MB, throughput 0.991341
Reading from 8581: heap size 445 MB, throughput 0.974186
Reading from 8581: heap size 445 MB, throughput 0.875389
Reading from 8581: heap size 447 MB, throughput 0.827276
Reading from 8580: heap size 458 MB, throughput 0.980455
Reading from 8580: heap size 464 MB, throughput 0.919042
Reading from 8580: heap size 464 MB, throughput 0.880514
Reading from 8580: heap size 469 MB, throughput 0.970834
Reading from 8581: heap size 452 MB, throughput 0.99209
Reading from 8580: heap size 470 MB, throughput 0.988939
Reading from 8581: heap size 454 MB, throughput 0.991623
Reading from 8580: heap size 474 MB, throughput 0.989431
Reading from 8581: heap size 458 MB, throughput 0.989344
Equal recommendation: 524 MB each
Reading from 8580: heap size 475 MB, throughput 0.988116
Reading from 8581: heap size 459 MB, throughput 0.990122
Reading from 8580: heap size 474 MB, throughput 0.988293
Reading from 8581: heap size 458 MB, throughput 0.988867
Reading from 8580: heap size 476 MB, throughput 0.985998
Reading from 8581: heap size 460 MB, throughput 0.985562
Reading from 8580: heap size 475 MB, throughput 0.986184
Reading from 8581: heap size 459 MB, throughput 0.987618
Equal recommendation: 524 MB each
Reading from 8580: heap size 477 MB, throughput 0.985453
Reading from 8581: heap size 461 MB, throughput 0.981
Reading from 8581: heap size 464 MB, throughput 0.988007
Reading from 8581: heap size 464 MB, throughput 0.889685
Reading from 8580: heap size 479 MB, throughput 0.990503
Reading from 8581: heap size 465 MB, throughput 0.89061
Reading from 8580: heap size 480 MB, throughput 0.846732
Reading from 8580: heap size 479 MB, throughput 0.881286
Reading from 8580: heap size 481 MB, throughput 0.978915
Reading from 8581: heap size 467 MB, throughput 0.983417
Reading from 8580: heap size 489 MB, throughput 0.990829
Reading from 8581: heap size 475 MB, throughput 0.990363
Reading from 8581: heap size 475 MB, throughput 0.992058
Reading from 8580: heap size 490 MB, throughput 0.990979
Equal recommendation: 524 MB each
Reading from 8581: heap size 475 MB, throughput 0.989077
Reading from 8580: heap size 492 MB, throughput 0.991162
Reading from 8581: heap size 477 MB, throughput 0.988578
Reading from 8580: heap size 494 MB, throughput 0.98833
Reading from 8581: heap size 476 MB, throughput 0.987527
Reading from 8580: heap size 493 MB, throughput 0.989799
Reading from 8581: heap size 478 MB, throughput 0.986749
Reading from 8580: heap size 495 MB, throughput 0.986954
Equal recommendation: 524 MB each
Reading from 8581: heap size 480 MB, throughput 0.992185
Reading from 8581: heap size 481 MB, throughput 0.918772
Reading from 8581: heap size 480 MB, throughput 0.89626
Reading from 8580: heap size 498 MB, throughput 0.98823
Reading from 8580: heap size 498 MB, throughput 0.945486
Reading from 8580: heap size 500 MB, throughput 0.920334
Reading from 8581: heap size 483 MB, throughput 0.987945
Reading from 8580: heap size 502 MB, throughput 0.98798
Reading from 8581: heap size 490 MB, throughput 0.992905
Reading from 8580: heap size 508 MB, throughput 0.994806
Reading from 8581: heap size 490 MB, throughput 0.991191
Reading from 8580: heap size 509 MB, throughput 0.990323
Equal recommendation: 524 MB each
Reading from 8580: heap size 509 MB, throughput 0.9906
Reading from 8581: heap size 491 MB, throughput 0.994002
Reading from 8580: heap size 511 MB, throughput 0.989294
Reading from 8581: heap size 492 MB, throughput 0.98891
Reading from 8581: heap size 491 MB, throughput 0.98752
Reading from 8580: heap size 509 MB, throughput 0.988365
Reading from 8581: heap size 492 MB, throughput 0.990087
Equal recommendation: 524 MB each
Reading from 8580: heap size 511 MB, throughput 0.987007
Reading from 8581: heap size 495 MB, throughput 0.987813
Reading from 8581: heap size 496 MB, throughput 0.884474
Reading from 8581: heap size 499 MB, throughput 0.955111
Reading from 8580: heap size 513 MB, throughput 0.990397
Reading from 8580: heap size 514 MB, throughput 0.927259
Reading from 8580: heap size 514 MB, throughput 0.891284
Reading from 8581: heap size 500 MB, throughput 0.991807
Reading from 8580: heap size 516 MB, throughput 0.993239
Reading from 8581: heap size 505 MB, throughput 0.992015
Reading from 8580: heap size 520 MB, throughput 0.994359
Equal recommendation: 524 MB each
Reading from 8581: heap size 506 MB, throughput 0.990645
Reading from 8580: heap size 522 MB, throughput 0.99406
Reading from 8581: heap size 505 MB, throughput 0.989608
Reading from 8580: heap size 520 MB, throughput 0.989373
Reading from 8581: heap size 506 MB, throughput 0.988082
Reading from 8580: heap size 523 MB, throughput 0.988762
Reading from 8581: heap size 507 MB, throughput 0.987717
Equal recommendation: 524 MB each
Reading from 8580: heap size 525 MB, throughput 0.989018
Reading from 8581: heap size 508 MB, throughput 0.98998
Reading from 8581: heap size 509 MB, throughput 0.912056
Reading from 8581: heap size 510 MB, throughput 0.966042
Reading from 8580: heap size 512 MB, throughput 0.994007
Reading from 8580: heap size 521 MB, throughput 0.909746
Reading from 8580: heap size 521 MB, throughput 0.947195
Reading from 8581: heap size 509 MB, throughput 0.993897
Reading from 8580: heap size 526 MB, throughput 0.99377
Reading from 8581: heap size 510 MB, throughput 0.991553
Reading from 8580: heap size 494 MB, throughput 0.991788
Equal recommendation: 524 MB each
Reading from 8581: heap size 508 MB, throughput 0.989322
Reading from 8580: heap size 522 MB, throughput 0.990336
Reading from 8581: heap size 509 MB, throughput 0.987332
Reading from 8580: heap size 520 MB, throughput 0.990248
Reading from 8581: heap size 507 MB, throughput 0.988498
Reading from 8580: heap size 521 MB, throughput 0.988103
Reading from 8581: heap size 509 MB, throughput 0.986843
Reading from 8580: heap size 521 MB, throughput 0.986584
Equal recommendation: 524 MB each
Reading from 8581: heap size 511 MB, throughput 0.9868
Reading from 8581: heap size 507 MB, throughput 0.908424
Reading from 8580: heap size 523 MB, throughput 0.991523
Reading from 8581: heap size 507 MB, throughput 0.986017
Reading from 8580: heap size 525 MB, throughput 0.880886
Reading from 8580: heap size 517 MB, throughput 0.961359
Reading from 8581: heap size 478 MB, throughput 0.994045
Reading from 8580: heap size 522 MB, throughput 0.99288
Reading from 8581: heap size 508 MB, throughput 0.990759
Reading from 8580: heap size 525 MB, throughput 0.991066
Equal recommendation: 524 MB each
Reading from 8581: heap size 483 MB, throughput 0.989881
Reading from 8580: heap size 496 MB, throughput 0.989722
Reading from 8581: heap size 508 MB, throughput 0.990809
Reading from 8580: heap size 520 MB, throughput 0.990266
Reading from 8581: heap size 506 MB, throughput 0.988787
Reading from 8580: heap size 518 MB, throughput 0.989354
Reading from 8581: heap size 508 MB, throughput 0.987532
Reading from 8580: heap size 520 MB, throughput 0.988178
Reading from 8581: heap size 508 MB, throughput 0.979124
Equal recommendation: 524 MB each
Reading from 8581: heap size 506 MB, throughput 0.904364
Reading from 8581: heap size 497 MB, throughput 0.841636
Reading from 8580: heap size 520 MB, throughput 0.986805
Reading from 8580: heap size 522 MB, throughput 0.896997
Reading from 8580: heap size 516 MB, throughput 0.970972
Reading from 8581: heap size 525 MB, throughput 0.995811
Reading from 8580: heap size 523 MB, throughput 0.991105
Reading from 8581: heap size 472 MB, throughput 0.993987
Reading from 8580: heap size 524 MB, throughput 0.994428
Equal recommendation: 524 MB each
Reading from 8581: heap size 521 MB, throughput 0.991175
Reading from 8580: heap size 522 MB, throughput 0.992252
Reading from 8581: heap size 482 MB, throughput 0.989961
Reading from 8580: heap size 523 MB, throughput 0.986066
Reading from 8581: heap size 521 MB, throughput 0.98942
Reading from 8580: heap size 522 MB, throughput 0.989148
Reading from 8581: heap size 521 MB, throughput 0.986193
Reading from 8580: heap size 523 MB, throughput 0.986624
Equal recommendation: 524 MB each
Reading from 8581: heap size 522 MB, throughput 0.984514
Reading from 8581: heap size 519 MB, throughput 0.888562
Reading from 8581: heap size 512 MB, throughput 0.963428
Reading from 8580: heap size 525 MB, throughput 0.987676
Reading from 8580: heap size 514 MB, throughput 0.893653
Reading from 8580: heap size 521 MB, throughput 0.985275
Reading from 8581: heap size 516 MB, throughput 0.990629
Reading from 8580: heap size 523 MB, throughput 0.993404
Reading from 8581: heap size 510 MB, throughput 0.991162
Reading from 8580: heap size 523 MB, throughput 0.991815
Reading from 8581: heap size 514 MB, throughput 0.989314
Equal recommendation: 524 MB each
Reading from 8580: heap size 497 MB, throughput 0.990545
Reading from 8581: heap size 515 MB, throughput 0.990746
Reading from 8580: heap size 524 MB, throughput 0.989073
Reading from 8581: heap size 516 MB, throughput 0.985913
Reading from 8580: heap size 523 MB, throughput 0.992394
Reading from 8581: heap size 519 MB, throughput 0.985467
Reading from 8580: heap size 523 MB, throughput 0.986679
Reading from 8581: heap size 510 MB, throughput 0.991773
Equal recommendation: 524 MB each
Reading from 8581: heap size 521 MB, throughput 0.916097
Reading from 8581: heap size 510 MB, throughput 0.872217
Reading from 8580: heap size 524 MB, throughput 0.985659
Reading from 8580: heap size 523 MB, throughput 0.897577
Reading from 8581: heap size 515 MB, throughput 0.983887
Reading from 8580: heap size 516 MB, throughput 0.983581
Reading from 8581: heap size 516 MB, throughput 0.991253
Reading from 8580: heap size 523 MB, throughput 0.991576
Reading from 8581: heap size 515 MB, throughput 0.9905
Reading from 8580: heap size 523 MB, throughput 0.991458
Reading from 8581: heap size 516 MB, throughput 0.98835
Equal recommendation: 524 MB each
Reading from 8580: heap size 521 MB, throughput 0.988872
Reading from 8581: heap size 515 MB, throughput 0.98969
Reading from 8580: heap size 522 MB, throughput 0.988349
Reading from 8581: heap size 516 MB, throughput 0.988201
Reading from 8580: heap size 520 MB, throughput 0.990705
Reading from 8581: heap size 514 MB, throughput 0.987442
Reading from 8580: heap size 522 MB, throughput 0.985803
Reading from 8581: heap size 516 MB, throughput 0.989329
Reading from 8581: heap size 519 MB, throughput 0.901991
Reading from 8581: heap size 519 MB, throughput 0.927184
Client 8581 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 8580: heap size 524 MB, throughput 0.990557
Reading from 8580: heap size 525 MB, throughput 0.925361
Client 8580 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
