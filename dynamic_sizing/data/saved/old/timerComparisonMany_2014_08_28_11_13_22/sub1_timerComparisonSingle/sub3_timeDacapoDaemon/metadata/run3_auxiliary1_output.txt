economemd
    total memory: 200 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_08_28_11_13_22/sub1_timerComparisonSingle/sub3_timeDacapoDaemon/daemon_run3_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 10943: heap size 9 MB, throughput 0.987749
Clients: 1
Client 10943 has a minimum heap size of 8 MB
Reading from 10944: heap size 9 MB, throughput 0.990698
Clients: 2
Client 10944 has a minimum heap size of 8 MB
Reading from 10943: heap size 9 MB, throughput 0.979636
Reading from 10944: heap size 9 MB, throughput 0.986798
Reading from 10944: heap size 9 MB, throughput 0.981044
Reading from 10943: heap size 11 MB, throughput 0.980016
Reading from 10944: heap size 9 MB, throughput 0.975308
Reading from 10943: heap size 11 MB, throughput 0.975597
Reading from 10944: heap size 11 MB, throughput 0.9707
Reading from 10944: heap size 11 MB, throughput 0.963519
Reading from 10943: heap size 15 MB, throughput 0.83714
Reading from 10944: heap size 16 MB, throughput 0.887656
Reading from 10943: heap size 23 MB, throughput 0.980559
Reading from 10944: heap size 22 MB, throughput 0.925182
Reading from 10943: heap size 26 MB, throughput 0.991088
Reading from 10944: heap size 22 MB, throughput 0.978356
Reading from 10943: heap size 27 MB, throughput 0.994738
Reading from 10944: heap size 23 MB, throughput 0.989815
Reading from 10943: heap size 29 MB, throughput 0.996303
Reading from 10944: heap size 26 MB, throughput 0.995767
Reading from 10943: heap size 30 MB, throughput 0.996858
Reading from 10944: heap size 27 MB, throughput 0.99721
Reading from 10943: heap size 30 MB, throughput 0.996802
Reading from 10944: heap size 28 MB, throughput 0.997768
Reading from 10943: heap size 31 MB, throughput 0.996655
Reading from 10944: heap size 28 MB, throughput 0.996997
Numeric result:
Recommendation: 2 clients, utility 1.0599:
    h1: 117.54 MB (U(h) = 0.855119*h^0.0422094)
    h2: 82.4602 MB (U(h) = 0.889437*h^0.0296118)
Recommendation: 2 clients, utility 1.0599:
    h1: 117.54 MB (U(h) = 0.855119*h^0.0422094)
    h2: 82.4597 MB (U(h) = 0.889437*h^0.0296118)
Reading from 10943: heap size 31 MB, throughput 0.996576
Reading from 10944: heap size 28 MB, throughput 0.997823
Reading from 10943: heap size 31 MB, throughput 0.995993
Reading from 10944: heap size 28 MB, throughput 0.996744
Reading from 10943: heap size 32 MB, throughput 0.995611
Reading from 10944: heap size 28 MB, throughput 0.997352
Reading from 10943: heap size 32 MB, throughput 0.995399
Reading from 10944: heap size 29 MB, throughput 0.997401
Reading from 10943: heap size 33 MB, throughput 0.995109
Reading from 10944: heap size 29 MB, throughput 0.997189
Reading from 10943: heap size 33 MB, throughput 0.994835
Reading from 10944: heap size 29 MB, throughput 0.997575
Reading from 10943: heap size 34 MB, throughput 0.994352
Reading from 10944: heap size 29 MB, throughput 0.963458
Numeric result:
Recommendation: 2 clients, utility 1.06271:
    h1: 120.509 MB (U(h) = 0.851262*h^0.0439481)
    h2: 79.491 MB (U(h) = 0.890851*h^0.0289894)
Recommendation: 2 clients, utility 1.06271:
    h1: 120.509 MB (U(h) = 0.851262*h^0.0439481)
    h2: 79.491 MB (U(h) = 0.890851*h^0.0289894)
Reading from 10943: heap size 34 MB, throughput 0.993558
Reading from 10944: heap size 30 MB, throughput 0.997342
Reading from 10943: heap size 35 MB, throughput 0.965562
Reading from 10944: heap size 31 MB, throughput 0.997728
Reading from 10943: heap size 35 MB, throughput 0.976522
Reading from 10944: heap size 31 MB, throughput 0.997904
Reading from 10943: heap size 36 MB, throughput 0.983226
Reading from 10944: heap size 32 MB, throughput 0.998016
Reading from 10944: heap size 32 MB, throughput 0.994775
Reading from 10943: heap size 36 MB, throughput 0.992935
Reading from 10944: heap size 34 MB, throughput 0.990647
Reading from 10943: heap size 37 MB, throughput 0.994102
Numeric result:
Recommendation: 2 clients, utility 1.06549:
    h1: 105.276 MB (U(h) = 0.861841*h^0.0393586)
    h2: 94.7236 MB (U(h) = 0.876057*h^0.0354137)
Recommendation: 2 clients, utility 1.06549:
    h1: 105.276 MB (U(h) = 0.861841*h^0.0393586)
    h2: 94.7241 MB (U(h) = 0.876057*h^0.0354137)
Reading from 10944: heap size 36 MB, throughput 0.990251
Reading from 10943: heap size 40 MB, throughput 0.99653
Reading from 10944: heap size 37 MB, throughput 0.994903
Reading from 10943: heap size 40 MB, throughput 0.997756
Reading from 10944: heap size 37 MB, throughput 0.997795
Reading from 10943: heap size 40 MB, throughput 0.998521
Reading from 10943: heap size 41 MB, throughput 0.998863
Reading from 10944: heap size 37 MB, throughput 0.998372
Reading from 10943: heap size 41 MB, throughput 0.998804
Reading from 10944: heap size 37 MB, throughput 0.998566
Reading from 10943: heap size 40 MB, throughput 0.998854
Reading from 10944: heap size 37 MB, throughput 0.998594
Reading from 10943: heap size 40 MB, throughput 0.998762
Reading from 10944: heap size 37 MB, throughput 0.998653
Numeric result:
Recommendation: 2 clients, utility 1.06598:
    h1: 106.125 MB (U(h) = 0.860735*h^0.0398105)
    h2: 93.875 MB (U(h) = 0.876526*h^0.0352152)
Recommendation: 2 clients, utility 1.06598:
    h1: 106.125 MB (U(h) = 0.860735*h^0.0398105)
    h2: 93.875 MB (U(h) = 0.876526*h^0.0352152)
Reading from 10943: heap size 40 MB, throughput 0.998767
Reading from 10944: heap size 36 MB, throughput 0.99881
Reading from 10943: heap size 40 MB, throughput 0.998701
Reading from 10944: heap size 36 MB, throughput 0.998728
Reading from 10943: heap size 39 MB, throughput 0.998479
Reading from 10944: heap size 36 MB, throughput 0.998779
Reading from 10943: heap size 40 MB, throughput 0.998325
Reading from 10944: heap size 36 MB, throughput 0.998614
Reading from 10943: heap size 40 MB, throughput 0.998484
Reading from 10944: heap size 37 MB, throughput 0.99842
Reading from 10943: heap size 40 MB, throughput 0.998131
Reading from 10944: heap size 37 MB, throughput 0.998469
Numeric result:
Recommendation: 2 clients, utility 1.06886:
    h1: 104.706 MB (U(h) = 0.860116*h^0.0400679)
    h2: 95.2937 MB (U(h) = 0.8735*h^0.036465)
Recommendation: 2 clients, utility 1.06886:
    h1: 104.708 MB (U(h) = 0.860116*h^0.0400679)
    h2: 95.2924 MB (U(h) = 0.8735*h^0.036465)
Reading from 10943: heap size 41 MB, throughput 0.997855
Reading from 10944: heap size 37 MB, throughput 0.998132
Reading from 10943: heap size 41 MB, throughput 0.995088
Reading from 10944: heap size 37 MB, throughput 0.960483
Reading from 10943: heap size 41 MB, throughput 0.978558
Reading from 10944: heap size 36 MB, throughput 0.99707
Reading from 10944: heap size 38 MB, throughput 0.997208
Reading from 10943: heap size 42 MB, throughput 0.980484
Reading from 10944: heap size 39 MB, throughput 0.997395
Reading from 10943: heap size 43 MB, throughput 0.982553
Reading from 10944: heap size 39 MB, throughput 0.996863
Reading from 10943: heap size 44 MB, throughput 0.984917
Numeric result:
Recommendation: 2 clients, utility 1.05189:
    h1: 99.8751 MB (U(h) = 0.876251*h^0.0336524)
    h2: 100.125 MB (U(h) = 0.88017*h^0.033736)
Recommendation: 2 clients, utility 1.05189:
    h1: 99.876 MB (U(h) = 0.876251*h^0.0336524)
    h2: 100.124 MB (U(h) = 0.88017*h^0.033736)
Reading from 10944: heap size 40 MB, throughput 0.997015
Reading from 10943: heap size 45 MB, throughput 0.977262
Reading from 10944: heap size 40 MB, throughput 0.997253
Reading from 10943: heap size 45 MB, throughput 0.987068
Reading from 10944: heap size 41 MB, throughput 0.997437
Reading from 10943: heap size 45 MB, throughput 0.997569
Reading from 10944: heap size 41 MB, throughput 0.997272
Reading from 10943: heap size 46 MB, throughput 0.997555
Reading from 10944: heap size 42 MB, throughput 0.997388
Reading from 10943: heap size 46 MB, throughput 0.997497
Reading from 10944: heap size 42 MB, throughput 0.997481
Reading from 10943: heap size 46 MB, throughput 0.997441
Numeric result:
Recommendation: 2 clients, utility 1.05078:
    h1: 99.2878 MB (U(h) = 0.877588*h^0.0331358)
    h2: 100.712 MB (U(h) = 0.88049*h^0.0336115)
Recommendation: 2 clients, utility 1.05078:
    h1: 99.2874 MB (U(h) = 0.877588*h^0.0331358)
    h2: 100.713 MB (U(h) = 0.88049*h^0.0336115)
Reading from 10943: heap size 46 MB, throughput 0.941237
Reading from 10944: heap size 42 MB, throughput 0.997369
Reading from 10943: heap size 46 MB, throughput 0.937972
Reading from 10944: heap size 42 MB, throughput 0.997453
Reading from 10943: heap size 46 MB, throughput 0.977317
Reading from 10944: heap size 42 MB, throughput 0.997518
Reading from 10944: heap size 42 MB, throughput 0.996818
Reading from 10943: heap size 46 MB, throughput 0.984585
Reading from 10943: heap size 45 MB, throughput 0.963326
Reading from 10944: heap size 42 MB, throughput 0.99576
Numeric result:
Recommendation: 2 clients, utility 1.04454:
    h1: 93.7504 MB (U(h) = 0.886985*h^0.0295497)
    h2: 106.25 MB (U(h) = 0.880801*h^0.0334901)
Recommendation: 2 clients, utility 1.04454:
    h1: 93.7494 MB (U(h) = 0.886985*h^0.0295497)
    h2: 106.251 MB (U(h) = 0.880801*h^0.0334901)
Reading from 10944: heap size 46 MB, throughput 0.998213
Reading from 10943: heap size 46 MB, throughput 0.997127
Reading from 10944: heap size 47 MB, throughput 0.99881
Reading from 10943: heap size 52 MB, throughput 0.996358
Reading from 10944: heap size 48 MB, throughput 0.999038
Reading from 10943: heap size 52 MB, throughput 0.997822
Reading from 10944: heap size 49 MB, throughput 0.99918
Reading from 10943: heap size 53 MB, throughput 0.998563
Reading from 10944: heap size 49 MB, throughput 0.999139
Reading from 10943: heap size 53 MB, throughput 0.998949
Reading from 10944: heap size 49 MB, throughput 0.998603
Numeric result:
Recommendation: 2 clients, utility 1.04375:
    h1: 97.8202 MB (U(h) = 0.884162*h^0.0306219)
    h2: 102.18 MB (U(h) = 0.884788*h^0.0319866)
Recommendation: 2 clients, utility 1.04375:
    h1: 97.8203 MB (U(h) = 0.884162*h^0.0306219)
    h2: 102.18 MB (U(h) = 0.884788*h^0.0319866)
Reading from 10943: heap size 53 MB, throughput 0.999082
Reading from 10944: heap size 49 MB, throughput 0.998899
Reading from 10943: heap size 54 MB, throughput 0.99916
Reading from 10944: heap size 48 MB, throughput 0.998731
Reading from 10943: heap size 53 MB, throughput 0.999137
Reading from 10944: heap size 48 MB, throughput 0.998635
Reading from 10943: heap size 54 MB, throughput 0.99908
Reading from 10944: heap size 46 MB, throughput 0.998668
Reading from 10943: heap size 53 MB, throughput 0.998929
Numeric result:
Recommendation: 2 clients, utility 1.04378:
    h1: 97.8801 MB (U(h) = 0.884077*h^0.0306528)
    h2: 102.12 MB (U(h) = 0.884806*h^0.03198)
Recommendation: 2 clients, utility 1.04378:
    h1: 97.881 MB (U(h) = 0.884077*h^0.0306528)
    h2: 102.119 MB (U(h) = 0.884806*h^0.03198)
Reading from 10944: heap size 47 MB, throughput 0.998744
Reading from 10943: heap size 53 MB, throughput 0.998918
Reading from 10944: heap size 46 MB, throughput 0.998201
Reading from 10943: heap size 52 MB, throughput 0.998943
Reading from 10944: heap size 47 MB, throughput 0.997864
Reading from 10943: heap size 53 MB, throughput 0.99658
Reading from 10944: heap size 46 MB, throughput 0.997502
Reading from 10943: heap size 51 MB, throughput 0.996741
Reading from 10944: heap size 48 MB, throughput 0.997161
Reading from 10943: heap size 53 MB, throughput 0.996691
Reading from 10944: heap size 49 MB, throughput 0.997415
Numeric result:
Recommendation: 2 clients, utility 1.04313:
    h1: 98.0877 MB (U(h) = 0.884428*h^0.0305245)
    h2: 101.912 MB (U(h) = 0.885512*h^0.0317147)
Recommendation: 2 clients, utility 1.04313:
    h1: 98.0877 MB (U(h) = 0.884428*h^0.0305245)
    h2: 101.912 MB (U(h) = 0.885512*h^0.0317147)
Reading from 10943: heap size 54 MB, throughput 0.996839
Reading from 10944: heap size 50 MB, throughput 0.997369
Reading from 10943: heap size 55 MB, throughput 0.996301
Reading from 10944: heap size 50 MB, throughput 0.99766
Reading from 10943: heap size 56 MB, throughput 0.996045
Reading from 10944: heap size 51 MB, throughput 0.997783
Reading from 10943: heap size 56 MB, throughput 0.995878
Reading from 10944: heap size 51 MB, throughput 0.99785
Reading from 10944: heap size 51 MB, throughput 0.997675
Numeric result:
Recommendation: 2 clients, utility 1.04039:
    h1: 98.4999 MB (U(h) = 0.886421*h^0.0298036)
    h2: 101.5 MB (U(h) = 0.888224*h^0.030712)
Recommendation: 2 clients, utility 1.04039:
    h1: 98.4988 MB (U(h) = 0.886421*h^0.0298036)
    h2: 101.501 MB (U(h) = 0.888224*h^0.030712)
Reading from 10943: heap size 57 MB, throughput 0.99671
Reading from 10944: heap size 50 MB, throughput 0.997749
Reading from 10943: heap size 57 MB, throughput 0.996676
Reading from 10944: heap size 51 MB, throughput 0.997622
Reading from 10943: heap size 57 MB, throughput 0.996544
Reading from 10944: heap size 50 MB, throughput 0.997877
Reading from 10943: heap size 57 MB, throughput 0.996116
Reading from 10944: heap size 50 MB, throughput 0.997788
Reading from 10943: heap size 57 MB, throughput 0.99629
Reading from 10944: heap size 49 MB, throughput 0.944918
Numeric result:
Recommendation: 2 clients, utility 1.03594:
    h1: 102.118 MB (U(h) = 0.887263*h^0.0295016)
    h2: 97.8824 MB (U(h) = 0.89479*h^0.0282753)
Recommendation: 2 clients, utility 1.03594:
    h1: 102.122 MB (U(h) = 0.887263*h^0.0295016)
    h2: 97.8776 MB (U(h) = 0.89479*h^0.0282753)
Reading from 10943: heap size 57 MB, throughput 0.995898
Reading from 10944: heap size 50 MB, throughput 0.975383
Reading from 10943: heap size 57 MB, throughput 0.996418
Reading from 10944: heap size 50 MB, throughput 0.987249
Reading from 10943: heap size 57 MB, throughput 0.996652
Reading from 10944: heap size 50 MB, throughput 0.994815
Reading from 10943: heap size 56 MB, throughput 0.997761
Reading from 10944: heap size 50 MB, throughput 0.996655
Numeric result:
Recommendation: 2 clients, utility 1.03546:
    h1: 102.943 MB (U(h) = 0.887031*h^0.0295853)
    h2: 97.0572 MB (U(h) = 0.895831*h^0.0278929)
Recommendation: 2 clients, utility 1.03546:
    h1: 102.945 MB (U(h) = 0.887031*h^0.0295853)
    h2: 97.0554 MB (U(h) = 0.895831*h^0.0278929)
Reading from 10943: heap size 57 MB, throughput 0.998352
Reading from 10944: heap size 50 MB, throughput 0.997788
Reading from 10943: heap size 56 MB, throughput 0.99859
Reading from 10944: heap size 50 MB, throughput 0.997714
Reading from 10943: heap size 56 MB, throughput 0.998703
Reading from 10944: heap size 50 MB, throughput 0.997916
Reading from 10943: heap size 56 MB, throughput 0.998627
Reading from 10944: heap size 50 MB, throughput 0.998097
Numeric result:
Recommendation: 2 clients, utility 1.03637:
    h1: 102.619 MB (U(h) = 0.886459*h^0.0297908)
    h2: 97.3807 MB (U(h) = 0.894805*h^0.0282699)
Recommendation: 2 clients, utility 1.03637:
    h1: 102.62 MB (U(h) = 0.886459*h^0.0297908)
    h2: 97.3805 MB (U(h) = 0.894805*h^0.0282699)
Reading from 10943: heap size 56 MB, throughput 0.998534
Reading from 10944: heap size 50 MB, throughput 0.998475
Reading from 10943: heap size 56 MB, throughput 0.998659
Reading from 10944: heap size 50 MB, throughput 0.998359
Reading from 10943: heap size 56 MB, throughput 0.998432
Reading from 10944: heap size 50 MB, throughput 0.998592
Reading from 10943: heap size 56 MB, throughput 0.998504
Reading from 10944: heap size 50 MB, throughput 0.998504
Numeric result:
Recommendation: 2 clients, utility 1.0365:
    h1: 102.506 MB (U(h) = 0.886441*h^0.0297973)
    h2: 97.4943 MB (U(h) = 0.894612*h^0.0283405)
Recommendation: 2 clients, utility 1.0365:
    h1: 102.506 MB (U(h) = 0.886441*h^0.0297973)
    h2: 97.4942 MB (U(h) = 0.894612*h^0.0283405)
Reading from 10943: heap size 56 MB, throughput 0.998573
Reading from 10944: heap size 50 MB, throughput 0.998617
Reading from 10943: heap size 56 MB, throughput 0.998785
Reading from 10944: heap size 50 MB, throughput 0.998663
Reading from 10943: heap size 56 MB, throughput 0.998651
Reading from 10944: heap size 50 MB, throughput 0.998577
Reading from 10943: heap size 57 MB, throughput 0.998646
Reading from 10944: heap size 50 MB, throughput 0.998667
Numeric result:
Recommendation: 2 clients, utility 1.03662:
    h1: 102.586 MB (U(h) = 0.886261*h^0.0298618)
    h2: 97.4141 MB (U(h) = 0.894568*h^0.0283568)
Recommendation: 2 clients, utility 1.03662:
    h1: 102.585 MB (U(h) = 0.886261*h^0.0298618)
    h2: 97.415 MB (U(h) = 0.894568*h^0.0283568)
Reading from 10943: heap size 57 MB, throughput 0.998476
Reading from 10944: heap size 50 MB, throughput 0.99874
Reading from 10943: heap size 57 MB, throughput 0.998711
Reading from 10944: heap size 49 MB, throughput 0.998723
Reading from 10943: heap size 57 MB, throughput 0.998698
Reading from 10944: heap size 50 MB, throughput 0.998717
Reading from 10943: heap size 56 MB, throughput 0.999091
Reading from 10944: heap size 50 MB, throughput 0.998442
Numeric result:
Recommendation: 2 clients, utility 1.03877:
    h1: 100.525 MB (U(h) = 0.886081*h^0.0299265)
    h2: 99.4752 MB (U(h) = 0.891176*h^0.0296136)
Recommendation: 2 clients, utility 1.03877:
    h1: 100.526 MB (U(h) = 0.886081*h^0.0299265)
    h2: 99.4744 MB (U(h) = 0.891176*h^0.0296136)
Reading from 10943: heap size 57 MB, throughput 0.998881
Reading from 10944: heap size 50 MB, throughput 0.998574
Reading from 10943: heap size 57 MB, throughput 0.998643
Reading from 10944: heap size 50 MB, throughput 0.998644
Reading from 10943: heap size 58 MB, throughput 0.998601
Reading from 10944: heap size 50 MB, throughput 0.998622
Numeric result:
Recommendation: 2 clients, utility 1.03855:
    h1: 100.267 MB (U(h) = 0.886502*h^0.0297763)
    h2: 99.733 MB (U(h) = 0.891165*h^0.0296177)
Recommendation: 2 clients, utility 1.03855:
    h1: 100.267 MB (U(h) = 0.886502*h^0.0297763)
    h2: 99.733 MB (U(h) = 0.891165*h^0.0296177)
Reading from 10943: heap size 58 MB, throughput 0.998689
Reading from 10944: heap size 51 MB, throughput 0.998689
Reading from 10943: heap size 58 MB, throughput 0.99884
Reading from 10944: heap size 51 MB, throughput 0.998722
Reading from 10943: heap size 58 MB, throughput 0.998819
Reading from 10944: heap size 50 MB, throughput 0.998656
Reading from 10943: heap size 58 MB, throughput 0.998555
Reading from 10944: heap size 51 MB, throughput 0.998544
Numeric result:
Recommendation: 2 clients, utility 1.03869:
    h1: 100.137 MB (U(h) = 0.886487*h^0.0297817)
    h2: 99.8632 MB (U(h) = 0.89094*h^0.0297005)
Recommendation: 2 clients, utility 1.03869:
    h1: 100.137 MB (U(h) = 0.886487*h^0.0297817)
    h2: 99.8635 MB (U(h) = 0.89094*h^0.0297005)
Reading from 10944: heap size 51 MB, throughput 0.99859
Reading from 10943: heap size 59 MB, throughput 0.997732
Reading from 10943: heap size 59 MB, throughput 0.998143
Reading from 10944: heap size 51 MB, throughput 0.998348
Reading from 10943: heap size 58 MB, throughput 0.998572
Reading from 10944: heap size 52 MB, throughput 0.998573
Numeric result:
Recommendation: 2 clients, utility 1.03783:
    h1: 100.274 MB (U(h) = 0.887151*h^0.0295455)
    h2: 99.726 MB (U(h) = 0.891807*h^0.0293839)
Recommendation: 2 clients, utility 1.03783:
    h1: 100.274 MB (U(h) = 0.887151*h^0.0295455)
    h2: 99.7256 MB (U(h) = 0.891807*h^0.0293839)
Reading from 10943: heap size 59 MB, throughput 0.998526
Reading from 10944: heap size 52 MB, throughput 0.998644
Reading from 10943: heap size 59 MB, throughput 0.998652
Reading from 10944: heap size 52 MB, throughput 0.998721
Reading from 10943: heap size 59 MB, throughput 0.998833
Reading from 10944: heap size 52 MB, throughput 0.998479
Numeric result:
Recommendation: 2 clients, utility 1.03791:
    h1: 100.367 MB (U(h) = 0.886999*h^0.0295997)
    h2: 99.6329 MB (U(h) = 0.891807*h^0.0293837)
Recommendation: 2 clients, utility 1.03791:
    h1: 100.366 MB (U(h) = 0.886999*h^0.0295997)
    h2: 99.6338 MB (U(h) = 0.891807*h^0.0293837)
Reading from 10943: heap size 60 MB, throughput 0.998694
Reading from 10944: heap size 53 MB, throughput 0.998604
Reading from 10943: heap size 60 MB, throughput 0.998546
Reading from 10944: heap size 53 MB, throughput 0.998797
Reading from 10943: heap size 60 MB, throughput 0.998692
Reading from 10944: heap size 53 MB, throughput 0.999054
Reading from 10943: heap size 60 MB, throughput 0.998614
Numeric result:
Recommendation: 2 clients, utility 1.03716:
    h1: 100.54 MB (U(h) = 0.887555*h^0.0294028)
    h2: 99.4604 MB (U(h) = 0.892623*h^0.0290876)
Recommendation: 2 clients, utility 1.03716:
    h1: 100.539 MB (U(h) = 0.887555*h^0.0294028)
    h2: 99.4611 MB (U(h) = 0.892623*h^0.0290876)
Reading from 10944: heap size 53 MB, throughput 0.998876
Reading from 10943: heap size 60 MB, throughput 0.998912
Reading from 10944: heap size 54 MB, throughput 0.998852
Reading from 10943: heap size 60 MB, throughput 0.998925
Reading from 10944: heap size 54 MB, throughput 0.998845
Reading from 10943: heap size 61 MB, throughput 0.99886
Numeric result:
Recommendation: 2 clients, utility 1.03641:
    h1: 100.769 MB (U(h) = 0.888069*h^0.0292219)
    h2: 99.2309 MB (U(h) = 0.893484*h^0.0287768)
Recommendation: 2 clients, utility 1.03641:
    h1: 100.767 MB (U(h) = 0.888069*h^0.0292219)
    h2: 99.2327 MB (U(h) = 0.893484*h^0.0287768)
Reading from 10944: heap size 54 MB, throughput 0.999012
Reading from 10943: heap size 61 MB, throughput 0.998873
Reading from 10944: heap size 54 MB, throughput 0.99858
Reading from 10943: heap size 61 MB, throughput 0.998904
Reading from 10944: heap size 55 MB, throughput 0.99873
Reading from 10943: heap size 61 MB, throughput 0.998662
Numeric result:
Recommendation: 2 clients, utility 1.03588:
    h1: 101.343 MB (U(h) = 0.888087*h^0.0292156)
    h2: 98.6568 MB (U(h) = 0.894422*h^0.02844)
Recommendation: 2 clients, utility 1.03588:
    h1: 101.345 MB (U(h) = 0.888087*h^0.0292156)
    h2: 98.6547 MB (U(h) = 0.894422*h^0.02844)
Reading from 10944: heap size 55 MB, throughput 0.998834
Reading from 10943: heap size 62 MB, throughput 0.998964
Reading from 10944: heap size 55 MB, throughput 0.984103
Reading from 10943: heap size 62 MB, throughput 0.998881
Reading from 10944: heap size 55 MB, throughput 0.991413
Reading from 10943: heap size 62 MB, throughput 0.998745
Numeric result:
Recommendation: 2 clients, utility 1.03464:
    h1: 102.064 MB (U(h) = 0.888693*h^0.0290033)
    h2: 97.9358 MB (U(h) = 0.896124*h^0.0278299)
Recommendation: 2 clients, utility 1.03464:
    h1: 102.065 MB (U(h) = 0.888693*h^0.0290033)
    h2: 97.9354 MB (U(h) = 0.896124*h^0.0278299)
Reading from 10944: heap size 56 MB, throughput 0.995356
Reading from 10943: heap size 62 MB, throughput 0.998844
Reading from 10944: heap size 56 MB, throughput 0.997248
Reading from 10943: heap size 63 MB, throughput 0.998679
Reading from 10944: heap size 57 MB, throughput 0.997989
Numeric result:
Recommendation: 2 clients, utility 1.03308:
    h1: 103.163 MB (U(h) = 0.889363*h^0.0287695)
    h2: 96.8371 MB (U(h) = 0.898445*h^0.0270052)
Recommendation: 2 clients, utility 1.03308:
    h1: 103.163 MB (U(h) = 0.889363*h^0.0287695)
    h2: 96.8366 MB (U(h) = 0.898445*h^0.0270052)
Reading from 10943: heap size 63 MB, throughput 0.998931
Reading from 10944: heap size 57 MB, throughput 0.998455
Reading from 10943: heap size 63 MB, throughput 0.998884
Reading from 10944: heap size 58 MB, throughput 0.998852
Reading from 10943: heap size 63 MB, throughput 0.99887
Numeric result:
Recommendation: 2 clients, utility 1.03271:
    h1: 103.665 MB (U(h) = 0.889328*h^0.0287815)
    h2: 96.3351 MB (U(h) = 0.899182*h^0.0267454)
Recommendation: 2 clients, utility 1.03271:
    h1: 103.667 MB (U(h) = 0.889328*h^0.0287815)
    h2: 96.3331 MB (U(h) = 0.899182*h^0.0267454)
Reading from 10944: heap size 58 MB, throughput 0.998934
Reading from 10943: heap size 64 MB, throughput 0.998827
Reading from 10944: heap size 59 MB, throughput 0.999076
Reading from 10943: heap size 64 MB, throughput 0.999
Reading from 10943: heap size 64 MB, throughput 0.99908
Reading from 10944: heap size 59 MB, throughput 0.99904
Numeric result:
Recommendation: 2 clients, utility 1.03201:
    h1: 103.779 MB (U(h) = 0.889969*h^0.0285591)
    h2: 96.2209 MB (U(h) = 0.89994*h^0.0264797)
Recommendation: 2 clients, utility 1.03201:
    h1: 103.778 MB (U(h) = 0.889969*h^0.0285591)
    h2: 96.2219 MB (U(h) = 0.89994*h^0.0264797)
Reading from 10943: heap size 64 MB, throughput 0.999127
Reading from 10944: heap size 59 MB, throughput 0.998853
Reading from 10943: heap size 64 MB, throughput 0.998968
Reading from 10944: heap size 59 MB, throughput 0.999028
Numeric result:
Recommendation: 2 clients, utility 1.032:
    h1: 103.789 MB (U(h) = 0.889965*h^0.0285603)
    h2: 96.2113 MB (U(h) = 0.899954*h^0.0264749)
Recommendation: 2 clients, utility 1.032:
    h1: 103.789 MB (U(h) = 0.889965*h^0.0285603)
    h2: 96.2108 MB (U(h) = 0.899954*h^0.0264749)
Reading from 10943: heap size 64 MB, throughput 0.999056
Reading from 10944: heap size 60 MB, throughput 0.999191
Client 10944 died
Clients: 1
Reading from 10943: heap size 65 MB, throughput 0.999097
Client 10943 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
