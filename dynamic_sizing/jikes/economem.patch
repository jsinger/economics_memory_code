diff -r 527b36e231ea -r ae5dfeab544d MMTk/ext/vm/jikesrvm/org/jikesrvm/mm/mmtk/DaemonComms.java
--- /dev/null	Thu Jan 01 00:00:00 1970 +0000
+++ b/MMTk/ext/vm/jikesrvm/org/jikesrvm/mm/mmtk/DaemonComms.java	Wed Aug 13 12:30:46 2014 +0000
@@ -0,0 +1,58 @@
+/*
+ *  DaemonComms.java
+ *  Jeremy Singer
+ *  University of Glasgow
+ *  
+ */
+package org.jikesrvm.mm.mmtk;
+
+import org.vmmagic.pragma.*;
+import org.vmmagic.unboxed.Extent;
+
+import org.jikesrvm.runtime.SysCall;
+import static org.jikesrvm.runtime.SysCall.sysCall;
+
+/**
+ * Class that enables communication with external economics daemon
+ * during garbage collection
+ */
+public final class DaemonComms extends org.mmtk.vm.DaemonComms {
+
+    /** 
+     * all methods in this class are going to be simple wrappers,
+     * to real syscalls in sys.C, which will call out to daemon
+     * proxy routines
+     */
+
+    @Override
+    @Inline
+    public void initDaemon() {
+	SysCall.sysCall.sysInitDaemon();
+    }
+
+    @Override
+    @Inline
+    public void gcBegin() {
+	SysCall.sysCall.sysGcBeginDaemon();
+    }
+
+    @Override
+    @Inline
+    public void minorGcEnd(Extent heapSize, double throughput) {
+	SysCall.sysCall.sysMinorGcEndDaemon(heapSize, throughput);
+    }
+
+    @Override
+    @Inline
+    public void majorGcEnd(Extent heapSize, double throughput) {
+	SysCall.sysCall.sysMajorGcEndDaemon(heapSize, throughput);
+    }
+
+    @Override
+    @Inline
+    public int getRecommendedHeapSize() {
+	return SysCall.sysCall.sysGetRecommendedHeapSize();
+    }
+    
+
+}
diff -r 527b36e231ea -r ae5dfeab544d MMTk/ext/vm/jikesrvm/org/jikesrvm/mm/mmtk/Factory.java
--- a/MMTk/ext/vm/jikesrvm/org/jikesrvm/mm/mmtk/Factory.java	Fri Aug 08 16:07:32 2014 +0200
+++ b/MMTk/ext/vm/jikesrvm/org/jikesrvm/mm/mmtk/Factory.java	Wed Aug 13 12:30:46 2014 +0000
@@ -337,4 +337,15 @@
       return null; // never get here
     }
   }
+
+  @Override
+  public org.mmtk.vm.DaemonComms newDaemonComms() {
+    try {
+      return new DaemonComms();
+    } catch (Exception e) {
+      VM.sysFail("Failed to allocate new DaemonComms!");
+      return null; // never get here
+    }
+  }
+
 }
diff -r 527b36e231ea -r ae5dfeab544d MMTk/src/org/mmtk/plan/generational/Gen.java
--- a/MMTk/src/org/mmtk/plan/generational/Gen.java	Fri Aug 08 16:07:32 2014 +0200
+++ b/MMTk/src/org/mmtk/plan/generational/Gen.java	Wed Aug 13 12:30:46 2014 +0000
@@ -171,6 +171,10 @@
     if (phaseId == SET_COLLECTION_KIND) {
       super.collectionPhase(phaseId);
       gcFullHeap = requiresFullHeapCollection();
+      // @jsinger - economemd - only send msg for major GCs
+      if (gcFullHeap) {
+	  VM.daemonComms.gcBegin();
+      }
       return;
     }
 
diff -r 527b36e231ea -r ae5dfeab544d MMTk/src/org/mmtk/utility/heap/HeapGrowthManager.java
--- a/MMTk/src/org/mmtk/utility/heap/HeapGrowthManager.java	Fri Aug 08 16:07:32 2014 +0200
+++ b/MMTk/src/org/mmtk/utility/heap/HeapGrowthManager.java	Wed Aug 13 12:30:46 2014 +0000
@@ -100,6 +100,8 @@
     VM.events.heapSizeChanged(currentHeapSize);
     if (VM.VERIFY_ASSERTIONS) sanityCheck();
     endLastMajorGC = VM.statistics.nanoTime();
+    // @jsinger
+    VM.daemonComms.initDaemon();
   }
 
   /**
@@ -162,11 +164,30 @@
    */
   public static boolean considerHeapSize() {
     Extent oldSize = currentHeapSize;
+    // @jsinger - convert heap size from B to MB
+    Extent currentHeapMB = Word.fromIntSignExtend(currentHeapSize.toInt()>>LOG_BYTES_IN_MBYTE).toExtent();
+    // @jsinger - send stats for latest major GC to daemon
+    VM.daemonComms.majorGcEnd(currentHeapMB, computeThroughput());
+
     Extent reserved = Plan.reservedMemory();
     double liveRatio = reserved.toLong() / ((double) currentHeapSize.toLong());
-    double ratio = computeHeapChangeRatio(liveRatio);
-    Extent newSize = Word.fromIntSignExtend((int)(ratio * (oldSize.toLong()>>LOG_BYTES_IN_MBYTE))).lsh(LOG_BYTES_IN_MBYTE).toExtent(); // do arith in MB to avoid overflow
-    if (newSize.LT(reserved)) newSize = reserved;
+    // @jsinger - ask daemon for recommended new heap size
+    Extent newSize = Word.fromIntSignExtend(VM.daemonComms.getRecommendedHeapSize()).lsh(LOG_BYTES_IN_MBYTE).toExtent();
+    if (Options.verbose.getValue() >= 1) {
+	Log.write("economemd: recommended heap size: "); 
+	Log.writeDec(newSize.toWord().rshl(LOG_BYTES_IN_MBYTE));
+	Log.writeln(" MB");
+    }
+    // end @jsinger
+    /* ORIGINAL JIKES RVM RESIZE CODE FOLLOWS...
+     * double ratio = computeHeapChangeRatio(liveRatio);
+     * Extent newSize = Word.fromIntSignExtend((int)(ratio * (oldSize.toLong()>>LOG_BYTES_IN_MBYTE))).lsh(LOG_BYTES_IN_MBYTE).toExtent(); // do arith in MB to avoid overflow
+     */
+    if (newSize.EQ(Extent.zero())) 
+	newSize = oldSize; // @jsinger - no recommendation received from daemon
+    else if (newSize.LT(reserved)) 
+	newSize = reserved;  // @jsinger - don't go below minimum required heap size
+    
     newSize = newSize.plus(BYTES_IN_MBYTE - 1).toWord().rshl(LOG_BYTES_IN_MBYTE).lsh(LOG_BYTES_IN_MBYTE).toExtent(); // round to next megabyte
     if (newSize.GT(maxHeapSize)) newSize = maxHeapSize;
     if (newSize.NE(oldSize) && newSize.GT(Extent.zero())) {
@@ -184,6 +205,17 @@
     }
   }
 
+  /**
+   * @jsinger utility method to compute throughput
+   * of application since last major GC
+   */
+  private static double computeThroughput() {
+    long totalNanos = VM.statistics.nanoTime() - endLastMajorGC;
+    double totalTime = VM.statistics.nanosToMillis(totalNanos);
+    double throughput = (totalTime - accumulatedGCTime) / totalTime;
+    return throughput;
+  }
+
   private static double computeHeapChangeRatio(double liveRatio) {
     // (1) compute GC load.
     long totalNanos = VM.statistics.nanoTime() - endLastMajorGC;
diff -r 527b36e231ea -r ae5dfeab544d MMTk/src/org/mmtk/vm/DaemonComms.java
--- /dev/null	Thu Jan 01 00:00:00 1970 +0000
+++ b/MMTk/src/org/mmtk/vm/DaemonComms.java	Wed Aug 13 12:30:46 2014 +0000
@@ -0,0 +1,29 @@
+/*
+ * DaemonComms.java
+ * Jeremy Singer
+ * University of Glasgow
+ * May 2014
+ */
+package org.mmtk.vm;
+
+import org.vmmagic.unboxed.Extent;
+
+/**
+ * This class defines daemon communication  methods.
+ * The concrete class is defined in external (Jikes RVM-specific)
+ * MMTk code.
+ */
+public abstract class DaemonComms {
+
+    public abstract void initDaemon();
+
+    public abstract void gcBegin();
+
+    public abstract void minorGcEnd(Extent heapSize, double throughput);
+    
+    public abstract void majorGcEnd(Extent heapSize, double throughput);
+
+    public abstract int getRecommendedHeapSize();
+
+}
+
diff -r 527b36e231ea -r ae5dfeab544d MMTk/src/org/mmtk/vm/Factory.java
--- a/MMTk/src/org/mmtk/vm/Factory.java	Fri Aug 08 16:07:32 2014 +0200
+++ b/MMTk/src/org/mmtk/vm/Factory.java	Wed Aug 13 12:30:46 2014 +0000
@@ -354,4 +354,14 @@
       int indexMaxStream,
       Color colour,
       boolean summary);
+
+  /**
+   * Create a new DaemonComms instance using the appropriate VM-specific
+   * concrete DaemonComms sub-class.
+   *
+   * @see DaemonComms
+   * @return A concrete VM-specific DaemonComms instance.
+   */
+  public abstract DaemonComms newDaemonComms();
+
 }
diff -r 527b36e231ea -r ae5dfeab544d MMTk/src/org/mmtk/vm/VM.java
--- a/MMTk/src/org/mmtk/vm/VM.java	Fri Aug 08 16:07:32 2014 +0200
+++ b/MMTk/src/org/mmtk/vm/VM.java	Wed Aug 13 12:30:46 2014 +0000
@@ -119,6 +119,9 @@
   public static final MMTk_Events events;
   @Untraced
   public static final Debug debugging;
+  @Untraced
+  public static final DaemonComms daemonComms;
+
 
   /*
    * The remainder is does the static initialization of the
@@ -165,6 +168,7 @@
     traceInterface = factory.newTraceInterface();
     events = factory.newEvents();
     debugging = factory.newDebug();
+    daemonComms = factory.newDaemonComms();
     config = new Config(factory.newBuildTimeConfig());
 
     /* Now initialize the constants using the vm-specific singletons */
@@ -182,6 +186,7 @@
     ALIGNMENT_VALUE = Memory.alignmentValueTrapdoor(memory);
     ARRAY_BASE_OFFSET = ObjectModel.arrayBaseOffsetTrapdoor(objectModel);
     DEBUG = Debug.isEnabledTrapdoor(debugging);
+
   }
 
   /**
diff -r 527b36e231ea -r ae5dfeab544d build.xml
--- a/build.xml	Fri Aug 08 16:07:32 2014 +0200
+++ b/build.xml	Wed Aug 13 12:30:46 2014 +0000
@@ -736,7 +736,7 @@
     </exec>
     <exec executable="${c.exe}" failonerror="true">
       <arg
-          line="-Wextra -Wall -Wbad-function-cast -Wcast-align -Wpointer-arith -Wcast-qual -Wshadow -Wmissing-prototypes -Wmissing-declarations -Wwrite-strings -Wno-aggregate-return -Wsuggest-attribute=noreturn -Wnested-externs -Wconversion -Wsign-compare -fkeep-inline-functions -Wno-unused -Wno-strict-prototypes -Wno-undef"/>
+          line="-Wextra -Wall -Wbad-function-cast -Wcast-align -Wpointer-arith -Wcast-qual -Wshadow -Wmissing-prototypes -Wmissing-declarations -Wwrite-strings -Wno-aggregate-return -Wnested-externs -Wconversion -Wsign-compare -fkeep-inline-functions -Wno-unused -Wno-strict-prototypes -Wno-undef"/>
       <arg value="-I"/>
       <arg value="${basedir}/rvm/src-generated/opt-burs/jburg"/>
       <arg value="-o"/>
@@ -1773,7 +1773,22 @@
     </exec>
   </target>
 
-  <target name="build-bootloader" depends="build-gcspy-stub,build-ppc-bootThread">
+  <target name="build-economem" depends="check-properties">
+    <fail unless="economem.dir">ant property economem.dir must be specified explicitly</fail>
+
+    <apply executable="${c++.exe}" failonerror="true" verbose="true">
+      <arg line="-m32 -g -std=c++11 -c -I${economem.dir}"/>
+      <arg value="-o"/>
+      <targetfile/>
+      <mapper type="glob" from="*.cpp" to="${build.base}/*.o"/>
+      <fileset dir="${economem.dir}" casesensitive="yes">
+	<filename name="*.cpp"/>
+      </fileset>
+    </apply>
+
+  </target>
+
+  <target name="build-bootloader" depends="build-gcspy-stub,build-ppc-bootThread,build-economem">
 
     <!-- rdynamic flag used to control the exporting of symbols into an ELF -->
     <condition property="c++.rdynamic" value="" else="-rdynamic">
@@ -1837,8 +1852,23 @@
         <equals arg1="${classlib.provider}" arg2="GNU Classpath"/>
       </conditions>
       <sequential>
+        <!-- jsinger - this is where we need to link DaemonProxy -->
+
+	<fail unless="economem.dir">ant property economem.dir must be specified explicitly</fail>
+
+	<!-- jsinger -build this obj file in ant -->
         <exec executable="${c++.exe}" failonerror="true">
-          <arg line="${c++.args} ${rvm.common.args} ${gcspy.lib.dir} ${rvm.src} ${perfevent.lib} ${c++.librt} -lpthread ${c++.rdynamic} -g"/>
+	  <arg line = "-m32 -I${economem.dir} -I${bl.dir} -c ${bl.dir}/JikesThroughput.cpp"/>
+         <arg value="-o"/>
+         <arg value="${build.base}/JikesThroughput.o"/> <!-- put object file in target dir -->
+ 	</exec>
+
+	<!-- jsinger - these obj files built by earlier build-economem target  -->
+	<property name="daemon.deps"
+		  value="${build.base}/Utilities.o ${build.base}/DaemonProxy.o ${build.base}/RecvSock.o ${build.base}/Mutex.o ${build.base}/ReadingSender.o ${build.base}/SendSock.o ${build.base}/Thread.o ${build.base}/RecommendationReceiver.o"/>
+
+        <exec executable="${c++.exe}" failonerror="true">
+          <arg line="${c++.args} ${rvm.common.args} ${gcspy.lib.dir} ${rvm.src} ${perfevent.lib} ${c++.librt} -lpthread ${c++.rdynamic} -I${economem.dir} ${build.base}/JikesThroughput.o ${daemon.deps} -g"/>
           <arg value="-o"/>
           <arg value="${build.base}/JikesRVM"/>
           <arg value="-L${build.base}"/>
diff -r 527b36e231ea -r ae5dfeab544d build/primordials/RVM.txt
--- a/build/primordials/RVM.txt	Fri Aug 08 16:07:32 2014 +0200
+++ b/build/primordials/RVM.txt	Wed Aug 13 12:30:46 2014 +0000
@@ -80,3 +80,8 @@
 
 [Lorg/vmmagic/pragma/Inline$When;
 Lsun/misc/Unsafe;
+
+
+Lorg/mmtk/vm/DaemonComms;
+Lorg/jikesrvm/mm/mmtk/DaemonComms;
+
diff -r 527b36e231ea -r ae5dfeab544d rvm/src/org/jikesrvm/runtime/BootRecord.java
--- a/rvm/src/org/jikesrvm/runtime/BootRecord.java	Fri Aug 08 16:07:32 2014 +0200
+++ b/rvm/src/org/jikesrvm/runtime/BootRecord.java	Wed Aug 13 12:30:46 2014 +0000
@@ -385,4 +385,11 @@
   public Address sysPerfEventDisableIP;
   public Address sysPerfEventReadIP;
 
+    // @jsinger support for holistic mem mgt daemon
+    public Address sysInitDaemonIP;
+    public Address sysGcBeginDaemonIP;
+    public Address sysMinorGcEndDaemonIP;
+    public Address sysMajorGcEndDaemonIP;
+    public Address sysGetRecommendedHeapSizeIP;
+
 }
diff -r 527b36e231ea -r ae5dfeab544d rvm/src/org/jikesrvm/runtime/SysCall.java
--- a/rvm/src/org/jikesrvm/runtime/SysCall.java	Fri Aug 08 16:07:32 2014 +0200
+++ b/rvm/src/org/jikesrvm/runtime/SysCall.java	Wed Aug 13 12:30:46 2014 +0000
@@ -437,5 +437,22 @@
 
   @SysCallTemplate
   public abstract int gcspySprintf(Address str, Address format, Address value);
+
+    // @jsinger support for holistic mem mgt daemon
+    @SysCallTemplate
+    public abstract void sysInitDaemon();
+
+    @SysCallTemplate    
+    public abstract void sysGcBeginDaemon();
+
+    @SysCallTemplate
+    public abstract void sysMinorGcEndDaemon(Extent heapSize, double throughput);
+
+    @SysCallTemplate
+    public abstract void sysMajorGcEndDaemon(Extent heapsize, double throughput);
+
+    @SysCallTemplate
+    public abstract int sysGetRecommendedHeapSize();
+
 }
 
diff -r 527b36e231ea -r ae5dfeab544d tools/bootImageRunner/JikesThroughput.cpp
--- /dev/null	Thu Jan 01 00:00:00 1970 +0000
+++ b/tools/bootImageRunner/JikesThroughput.cpp	Wed Aug 13 12:30:46 2014 +0000
@@ -0,0 +1,27 @@
+/*
+ * JikesThroughput.cpp
+ * Jeremy Singer
+ * University of Glasgow
+ * Jul 2014
+ */
+
+#include "JikesThroughput.hpp"
+
+JikesThroughput::JikesThroughput() : heapSize(0), throughput(0.0) {};
+
+void 
+JikesThroughput::gcBegin() {
+    return;
+};
+
+ThroughputCalculator::Result 
+JikesThroughput::minorGCEnd() {
+    return ThroughputCalculator::Result(this->heapSize, this->throughput);
+};
+
+ThroughputCalculator::Result 
+JikesThroughput::majorGCEnd() {
+    return ThroughputCalculator::Result(this->heapSize, this->throughput);
+};
+
+
diff -r 527b36e231ea -r ae5dfeab544d tools/bootImageRunner/JikesThroughput.hpp
--- /dev/null	Thu Jan 01 00:00:00 1970 +0000
+++ b/tools/bootImageRunner/JikesThroughput.hpp	Wed Aug 13 12:30:46 2014 +0000
@@ -0,0 +1,51 @@
+/*
+ * JikesThroughput.hpp
+ * Jeremy Singer
+ * University of Glasgow
+ * Jul 2014
+ */
+
+/*!
+ *! build with
+ *! $ g++ -m32 -I/scratch1/jsinger/economics_memory_code/dynamic_sizing/common/ -c JikesThroughput.cpp
+ *! Then this class must be linked with Jikes RVM sys.C
+ *!
+ *! This is the custom throughput calculation class for Jikes RVM,
+ *! which is used to interact with economem daemon.
+ *!
+ */
+
+#ifndef JIKESTHROUGHPUT_HPP
+#define JIKESTHROUGHPUT_HPP
+
+#include <ThroughputCalculator.hpp>
+
+using namespace economem;
+
+class JikesThroughput : public ThroughputCalculator {
+public:
+    JikesThroughput();
+    ~JikesThroughput() {};
+
+  void setHeapSize(unsigned long long heapSize) {
+	this->heapSize = heapSize;
+    };
+
+    void setThroughput(double throughput) {
+	this->throughput = throughput;
+    };
+
+
+    virtual void gcBegin();
+    virtual Result minorGCEnd();
+    virtual Result majorGCEnd();
+
+private:
+    //! heap size in MB at start of most recent GC
+    unsigned long long heapSize;
+    //! throughput in range (0,1) measured at end of most recent GC
+    double throughput;
+};
+
+#endif
+
diff -r 527b36e231ea -r ae5dfeab544d tools/bootImageRunner/sys.C
--- a/tools/bootImageRunner/sys.C	Fri Aug 08 16:07:32 2014 +0200
+++ b/tools/bootImageRunner/sys.C	Wed Aug 13 12:30:46 2014 +0000
@@ -137,6 +137,13 @@
 #include <pthread.h>
 #endif
 
+//! FIXME add #ifdef stmt here
+//! @jsinger - support for economem daemon
+#include <JikesThroughput.hpp>
+#include <ReadingSender.hpp>
+#include <DaemonProxy.hpp>
+//! FIXME endif
+
 extern "C" Word sysMonitorCreate();
 extern "C" void sysMonitorDestroy(Word);
 extern "C" void sysMonitorEnter(Word);
@@ -344,6 +351,7 @@
     DeathLock = sysMonitorCreate();
 }
 
+static DaemonProxy *proxy = NULL; // @jsinger
 
 static bool systemExiting = false;
 
@@ -366,6 +374,11 @@
         fprintf(SysErrorFile, "%s: exit %d\n", Me, value);
     }
 
+    // @jsinger - delete daemon proxy
+    if (proxy != NULL) 
+      delete(proxy);
+    // end @jsinger addition
+
     fflush(SysErrorFile);
     fflush(SysTraceFile);
     fflush(stdout);
@@ -2499,5 +2512,77 @@
 
 #endif
 
+//! @jsinger Support for economem routines
+// FIXME - wrap these in a #ifdef stmt?
+JikesThroughput *jikesThroughput;
+ReadingSender *sender;
 
+/* @jsinger mem mgt routines for holistic mem mgt */
+extern "C" void sysInitDaemon() {
+  //fprintf(SysErrorFile, "init -> daemon\n");
+  jikesThroughput = new JikesThroughput();
+
+  // @callum: this seems easier than adding a new command line argument...
+  const char *s;
+  if ((s = getenv("ECONOMEM_JIKES_DAEMON_NAME")) != NULL)
+    sender = new ReadingSender(s);
+  else
+    sender = new ReadingSender();
+
+  proxy = new DaemonProxy();
+  proxy->addTarget(jikesThroughput, sender);
+
+  // @callum
+  if ((s = getenv("ECONOMEM_JIKES_MIN_HEAP")) != NULL) {
+    // if the conversion fails, this will be 0, which is OK
+    int i = atoi(s);
+    if (i > 0)
+      proxy->setMinHeapMB(i);
+  }
+  // DaemonProxy now owns jikesThroughput and sender objects
+}
 
+/* send this message at start of GC */
+extern "C" void sysGcBeginDaemon() {
+  //fprintf(SysErrorFile, "gc begin -> daemon\n");
+  proxy->gcBegin();
+}
+
+/* send this message at end of minor GC 
+ * heapsize in MB
+ * throughput between 0.0 and 1.0
+ */
+extern "C" void sysMinorGcEndDaemon(Extent heapSize, double throughput) {
+  //fprintf(SysErrorFile, "minor GC End -> daemon\n");
+  jikesThroughput->setHeapSize(heapSize);
+  jikesThroughput->setThroughput(throughput);
+  proxy->minorGCEnd();
+}
+
+/* send this message at end of major GC
+ * heapsize in MB
+ * throughput between 0.0 and 1.0
+ */
+extern "C" void sysMajorGcEndDaemon(Extent heapSize, double throughput) {
+  //fprintf(SysErrorFile, "major GC End -> daemon\n");
+  jikesThroughput->setHeapSize(heapSize);
+  jikesThroughput->setThroughput(throughput);
+  proxy->majorGCEnd();
+}
+
+/* get a recommended heap size from the daemon
+ * the recommendation struct just wraps a uint64 value
+ */
+extern "C" int sysGetRecommendedHeapSize() {
+  Recommendation recommendedSize(0);
+  bool valid = proxy->getRecommendation(recommendedSize);
+  if (valid) {
+    return (int)(recommendedSize.getHeapSizeMB());
+  }
+  else {
+    return 0;
+  }
+ 
+}
+
+
