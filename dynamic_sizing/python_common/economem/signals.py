import signal
import sys

handlers = []

def cb_main_handler(signum, frame):
    for h in handlers:
        h()
    sys.exit(0)

def add_handler(f):
    exit_on_signal()
    handlers.append(f)

def exit_on_signal():
    if not handlers:
        signal.signal(signal.SIGINT, cb_main_handler)
        signal.signal(signal.SIGTERM, cb_main_handler)
        signal.signal(signal.SIGHUP, cb_main_handler)

def clear_handlers():
    global handlers
    handlers = []

def cleanup_processes(processes):
    l = processes[:]
    def cleanup():
        for p in l:
            p.kill()
        for p in l:
            p.wait()

    add_handler(cleanup)
