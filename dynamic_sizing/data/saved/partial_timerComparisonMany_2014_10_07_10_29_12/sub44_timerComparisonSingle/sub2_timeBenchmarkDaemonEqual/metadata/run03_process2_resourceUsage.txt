	Command being timed: "cgexec -g memory:economem /home/callum/economics_memory_code/dynamic_sizing/hotspot8/openjdk8/jdk8/build/linux-x86_64-normal-server-release/jdk/bin/java -XX:+EconomemSimpleThroughput -XX:-EconomemVengerovThroughput -XX:EconomemMinHeap=3 -Xms10m -Xmx1674m -XX:+DisableExplicitGC -XX:+UseAdaptiveSizePolicyWithSystemGC -jar /home/callum/economics_memory_code/dynamic_sizing/haskell_common/dacapo-9.12-bach.jar --no-pre-iteration-gc --scratch-directory /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub44_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/scratch2 -t 2 -n 27 -s large xalan"
	User time (seconds): 427.10
	System time (seconds): 156.15
	Percent of CPU this job got: 193%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 5:00.81
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 673980
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 101
	Minor (reclaiming a frame) page faults: 88253
	Voluntary context switches: 35186
	Involuntary context switches: 88703
	Swaps: 0
	File system inputs: 5560
	File system outputs: 12616288
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
