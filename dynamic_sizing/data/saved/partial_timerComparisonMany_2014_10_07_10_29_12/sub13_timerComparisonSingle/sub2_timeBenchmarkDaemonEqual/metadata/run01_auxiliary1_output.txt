economemd
    total memory: 6175 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub13_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 29069: heap size 9 MB, throughput 0.985929
Clients: 1
Client 29069 has a minimum heap size of 12 MB
Reading from 29067: heap size 9 MB, throughput 0.991328
Clients: 2
Client 29067 has a minimum heap size of 1223 MB
Reading from 29069: heap size 9 MB, throughput 0.988311
Reading from 29067: heap size 9 MB, throughput 0.986305
Reading from 29067: heap size 9 MB, throughput 0.957411
Reading from 29067: heap size 9 MB, throughput 0.942764
Reading from 29069: heap size 11 MB, throughput 0.981611
Reading from 29067: heap size 11 MB, throughput 0.966675
Reading from 29069: heap size 11 MB, throughput 0.958441
Reading from 29067: heap size 11 MB, throughput 0.981725
Reading from 29069: heap size 15 MB, throughput 0.989513
Reading from 29067: heap size 17 MB, throughput 0.951117
Reading from 29067: heap size 17 MB, throughput 0.576255
Reading from 29069: heap size 15 MB, throughput 0.98356
Reading from 29067: heap size 30 MB, throughput 0.950819
Reading from 29067: heap size 31 MB, throughput 0.808901
Reading from 29069: heap size 24 MB, throughput 0.946018
Reading from 29067: heap size 35 MB, throughput 0.275559
Reading from 29067: heap size 47 MB, throughput 0.795247
Reading from 29067: heap size 49 MB, throughput 0.469541
Reading from 29069: heap size 29 MB, throughput 0.991726
Reading from 29067: heap size 64 MB, throughput 0.829346
Reading from 29067: heap size 70 MB, throughput 0.706425
Reading from 29069: heap size 36 MB, throughput 0.98842
Reading from 29067: heap size 70 MB, throughput 0.219764
Reading from 29067: heap size 99 MB, throughput 0.876619
Reading from 29069: heap size 42 MB, throughput 0.988945
Reading from 29069: heap size 43 MB, throughput 0.95958
Reading from 29067: heap size 99 MB, throughput 0.382934
Reading from 29069: heap size 45 MB, throughput 0.970154
Reading from 29067: heap size 129 MB, throughput 0.93335
Reading from 29067: heap size 129 MB, throughput 0.748351
Reading from 29069: heap size 54 MB, throughput 0.982241
Reading from 29067: heap size 131 MB, throughput 0.852275
Reading from 29067: heap size 134 MB, throughput 0.811609
Reading from 29069: heap size 54 MB, throughput 0.990279
Reading from 29069: heap size 63 MB, throughput 0.989129
Reading from 29069: heap size 64 MB, throughput 0.760456
Reading from 29067: heap size 136 MB, throughput 0.150495
Reading from 29067: heap size 174 MB, throughput 0.460767
Reading from 29069: heap size 84 MB, throughput 0.99136
Reading from 29067: heap size 179 MB, throughput 0.744802
Reading from 29067: heap size 181 MB, throughput 0.631911
Reading from 29067: heap size 189 MB, throughput 0.730614
Reading from 29069: heap size 84 MB, throughput 0.995686
Reading from 29067: heap size 191 MB, throughput 0.604563
Reading from 29067: heap size 194 MB, throughput 0.139432
Reading from 29067: heap size 242 MB, throughput 0.683583
Reading from 29069: heap size 91 MB, throughput 0.997844
Reading from 29067: heap size 246 MB, throughput 0.734219
Reading from 29067: heap size 251 MB, throughput 0.685517
Reading from 29067: heap size 254 MB, throughput 0.636529
Reading from 29067: heap size 263 MB, throughput 0.61118
Reading from 29067: heap size 267 MB, throughput 0.481314
Reading from 29067: heap size 278 MB, throughput 0.574101
Reading from 29069: heap size 92 MB, throughput 0.996942
Reading from 29069: heap size 98 MB, throughput 0.997385
Reading from 29067: heap size 285 MB, throughput 0.0792329
Reading from 29067: heap size 339 MB, throughput 0.479896
Reading from 29067: heap size 331 MB, throughput 0.665069
Reading from 29069: heap size 99 MB, throughput 0.98855
Reading from 29067: heap size 284 MB, throughput 0.111927
Reading from 29067: heap size 382 MB, throughput 0.545511
Reading from 29067: heap size 386 MB, throughput 0.679683
Reading from 29067: heap size 379 MB, throughput 0.701915
Reading from 29067: heap size 383 MB, throughput 0.698213
Reading from 29067: heap size 384 MB, throughput 0.625744
Reading from 29069: heap size 106 MB, throughput 0.997444
Reading from 29067: heap size 386 MB, throughput 0.629574
Reading from 29067: heap size 388 MB, throughput 0.588907
Reading from 29067: heap size 395 MB, throughput 0.584556
Reading from 29067: heap size 399 MB, throughput 0.500154
Reading from 29067: heap size 409 MB, throughput 0.452346
Reading from 29069: heap size 106 MB, throughput 0.996929
Reading from 29067: heap size 414 MB, throughput 0.47743
Reading from 29067: heap size 423 MB, throughput 0.452374
Reading from 29069: heap size 111 MB, throughput 0.995457
Equal recommendation: 3087 MB each
Reading from 29067: heap size 429 MB, throughput 0.0575202
Reading from 29067: heap size 489 MB, throughput 0.35732
Reading from 29067: heap size 484 MB, throughput 0.44437
Reading from 29069: heap size 112 MB, throughput 0.996975
Reading from 29069: heap size 115 MB, throughput 0.996634
Reading from 29067: heap size 426 MB, throughput 0.0989803
Reading from 29067: heap size 541 MB, throughput 0.431413
Reading from 29067: heap size 546 MB, throughput 0.641263
Reading from 29067: heap size 543 MB, throughput 0.561821
Reading from 29067: heap size 546 MB, throughput 0.601945
Reading from 29067: heap size 536 MB, throughput 0.55554
Reading from 29069: heap size 116 MB, throughput 0.996869
Reading from 29067: heap size 541 MB, throughput 0.585251
Reading from 29067: heap size 534 MB, throughput 0.62236
Reading from 29069: heap size 120 MB, throughput 0.990659
Reading from 29067: heap size 538 MB, throughput 0.110326
Reading from 29067: heap size 604 MB, throughput 0.471155
Reading from 29069: heap size 120 MB, throughput 0.991436
Reading from 29067: heap size 608 MB, throughput 0.530209
Reading from 29067: heap size 609 MB, throughput 0.599527
Reading from 29067: heap size 612 MB, throughput 0.555771
Reading from 29067: heap size 615 MB, throughput 0.489981
Reading from 29069: heap size 126 MB, throughput 0.995058
Reading from 29067: heap size 625 MB, throughput 0.461009
Reading from 29067: heap size 630 MB, throughput 0.750075
Reading from 29069: heap size 126 MB, throughput 0.996943
Reading from 29067: heap size 641 MB, throughput 0.821641
Reading from 29067: heap size 645 MB, throughput 0.753611
Reading from 29069: heap size 130 MB, throughput 0.997247
Reading from 29067: heap size 659 MB, throughput 0.781941
Reading from 29067: heap size 663 MB, throughput 0.781768
Reading from 29069: heap size 131 MB, throughput 0.997524
Reading from 29069: heap size 135 MB, throughput 0.996855
Reading from 29067: heap size 677 MB, throughput 0.0360354
Reading from 29067: heap size 751 MB, throughput 0.253373
Reading from 29067: heap size 666 MB, throughput 0.307395
Reading from 29069: heap size 135 MB, throughput 0.993583
Reading from 29067: heap size 738 MB, throughput 0.619357
Reading from 29067: heap size 660 MB, throughput 0.519302
Reading from 29067: heap size 726 MB, throughput 0.324152
Reading from 29069: heap size 139 MB, throughput 0.9978
Reading from 29067: heap size 643 MB, throughput 0.0334932
Reading from 29067: heap size 817 MB, throughput 0.270917
Reading from 29067: heap size 817 MB, throughput 0.378301
Equal recommendation: 3087 MB each
Reading from 29069: heap size 139 MB, throughput 0.997521
Reading from 29067: heap size 822 MB, throughput 0.0407131
Reading from 29067: heap size 916 MB, throughput 0.354158
Reading from 29067: heap size 919 MB, throughput 0.878645
Reading from 29069: heap size 142 MB, throughput 0.998565
Reading from 29067: heap size 927 MB, throughput 0.970044
Reading from 29067: heap size 930 MB, throughput 0.832441
Reading from 29067: heap size 733 MB, throughput 0.762308
Reading from 29067: heap size 920 MB, throughput 0.818795
Reading from 29067: heap size 736 MB, throughput 0.791253
Reading from 29067: heap size 908 MB, throughput 0.829877
Reading from 29067: heap size 738 MB, throughput 0.821317
Reading from 29067: heap size 895 MB, throughput 0.961075
Reading from 29067: heap size 740 MB, throughput 0.867136
Reading from 29067: heap size 883 MB, throughput 0.850901
Reading from 29069: heap size 143 MB, throughput 0.99834
Reading from 29067: heap size 742 MB, throughput 0.940959
Reading from 29067: heap size 868 MB, throughput 0.812515
Reading from 29067: heap size 747 MB, throughput 0.924275
Reading from 29067: heap size 856 MB, throughput 0.917127
Reading from 29067: heap size 754 MB, throughput 0.864252
Reading from 29067: heap size 843 MB, throughput 0.869778
Reading from 29069: heap size 146 MB, throughput 0.998621
Reading from 29067: heap size 744 MB, throughput 0.923538
Reading from 29067: heap size 847 MB, throughput 0.710928
Reading from 29067: heap size 848 MB, throughput 0.771933
Reading from 29067: heap size 852 MB, throughput 0.62788
Reading from 29069: heap size 146 MB, throughput 0.99516
Reading from 29067: heap size 852 MB, throughput 0.697254
Reading from 29067: heap size 854 MB, throughput 0.679828
Reading from 29067: heap size 855 MB, throughput 0.65286
Reading from 29069: heap size 148 MB, throughput 0.99357
Reading from 29067: heap size 852 MB, throughput 0.67539
Reading from 29067: heap size 855 MB, throughput 0.69096
Reading from 29067: heap size 853 MB, throughput 0.740074
Reading from 29067: heap size 855 MB, throughput 0.736746
Reading from 29069: heap size 149 MB, throughput 0.977155
Reading from 29067: heap size 848 MB, throughput 0.724631
Reading from 29067: heap size 852 MB, throughput 0.823045
Reading from 29067: heap size 844 MB, throughput 0.816712
Reading from 29067: heap size 849 MB, throughput 0.841255
Reading from 29069: heap size 153 MB, throughput 0.995518
Reading from 29067: heap size 844 MB, throughput 0.151827
Reading from 29069: heap size 154 MB, throughput 0.997057
Reading from 29067: heap size 919 MB, throughput 0.988963
Reading from 29069: heap size 159 MB, throughput 0.997604
Reading from 29067: heap size 925 MB, throughput 0.962102
Reading from 29067: heap size 927 MB, throughput 0.788305
Reading from 29067: heap size 934 MB, throughput 0.779851
Reading from 29069: heap size 160 MB, throughput 0.996534
Reading from 29067: heap size 941 MB, throughput 0.745156
Reading from 29067: heap size 949 MB, throughput 0.804253
Reading from 29067: heap size 953 MB, throughput 0.797983
Reading from 29067: heap size 960 MB, throughput 0.74077
Reading from 29067: heap size 962 MB, throughput 0.805475
Reading from 29067: heap size 971 MB, throughput 0.8134
Reading from 29069: heap size 166 MB, throughput 0.995035
Reading from 29067: heap size 971 MB, throughput 0.808604
Equal recommendation: 3087 MB each
Reading from 29067: heap size 978 MB, throughput 0.813011
Reading from 29067: heap size 979 MB, throughput 0.908688
Reading from 29067: heap size 980 MB, throughput 0.85946
Reading from 29067: heap size 983 MB, throughput 0.880337
Reading from 29069: heap size 166 MB, throughput 0.997335
Reading from 29067: heap size 986 MB, throughput 0.793732
Reading from 29067: heap size 986 MB, throughput 0.767012
Reading from 29067: heap size 990 MB, throughput 0.70108
Reading from 29067: heap size 997 MB, throughput 0.675079
Reading from 29067: heap size 1008 MB, throughput 0.695868
Reading from 29067: heap size 1014 MB, throughput 0.650366
Reading from 29069: heap size 171 MB, throughput 0.997718
Reading from 29067: heap size 1026 MB, throughput 0.670265
Reading from 29067: heap size 1030 MB, throughput 0.656473
Reading from 29067: heap size 1043 MB, throughput 0.680506
Reading from 29067: heap size 1047 MB, throughput 0.648703
Reading from 29067: heap size 1061 MB, throughput 0.675465
Reading from 29067: heap size 1064 MB, throughput 0.633478
Reading from 29069: heap size 171 MB, throughput 0.99667
Reading from 29067: heap size 1078 MB, throughput 0.65178
Reading from 29067: heap size 1081 MB, throughput 0.652199
Reading from 29067: heap size 1099 MB, throughput 0.914449
Reading from 29069: heap size 176 MB, throughput 0.99528
Reading from 29069: heap size 176 MB, throughput 0.996819
Reading from 29069: heap size 180 MB, throughput 0.997156
Reading from 29067: heap size 1100 MB, throughput 0.982609
Reading from 29069: heap size 180 MB, throughput 0.99721
Reading from 29069: heap size 184 MB, throughput 0.996108
Reading from 29067: heap size 1118 MB, throughput 0.978733
Equal recommendation: 3087 MB each
Reading from 29069: heap size 184 MB, throughput 0.997278
Reading from 29069: heap size 188 MB, throughput 0.998105
Reading from 29069: heap size 188 MB, throughput 0.996721
Reading from 29067: heap size 1120 MB, throughput 0.961505
Reading from 29069: heap size 192 MB, throughput 0.993999
Reading from 29069: heap size 192 MB, throughput 0.996636
Reading from 29069: heap size 198 MB, throughput 0.998152
Reading from 29067: heap size 1122 MB, throughput 0.979088
Reading from 29069: heap size 198 MB, throughput 0.997359
Reading from 29069: heap size 202 MB, throughput 0.998179
Reading from 29067: heap size 1126 MB, throughput 0.980847
Reading from 29069: heap size 202 MB, throughput 0.997335
Equal recommendation: 3087 MB each
Reading from 29069: heap size 206 MB, throughput 0.997178
Reading from 29069: heap size 206 MB, throughput 0.997593
Reading from 29067: heap size 1118 MB, throughput 0.979444
Reading from 29069: heap size 209 MB, throughput 0.996985
Reading from 29069: heap size 209 MB, throughput 0.997735
Reading from 29067: heap size 1124 MB, throughput 0.977365
Reading from 29069: heap size 213 MB, throughput 0.994925
Reading from 29069: heap size 213 MB, throughput 0.997858
Reading from 29067: heap size 1110 MB, throughput 0.979725
Reading from 29069: heap size 218 MB, throughput 0.997051
Reading from 29069: heap size 218 MB, throughput 0.997561
Equal recommendation: 3087 MB each
Reading from 29069: heap size 207 MB, throughput 0.990914
Reading from 29067: heap size 1038 MB, throughput 0.97623
Reading from 29069: heap size 214 MB, throughput 0.830301
Reading from 29069: heap size 222 MB, throughput 0.998753
Reading from 29069: heap size 226 MB, throughput 0.998763
Reading from 29067: heap size 1101 MB, throughput 0.978411
Reading from 29069: heap size 231 MB, throughput 0.99899
Reading from 29069: heap size 232 MB, throughput 0.998728
Reading from 29067: heap size 1051 MB, throughput 0.975359
Reading from 29069: heap size 238 MB, throughput 0.999179
Equal recommendation: 3087 MB each
Reading from 29069: heap size 238 MB, throughput 0.998337
Reading from 29067: heap size 1101 MB, throughput 0.978234
Reading from 29069: heap size 242 MB, throughput 0.99904
Reading from 29069: heap size 242 MB, throughput 0.998733
Reading from 29067: heap size 1104 MB, throughput 0.979696
Reading from 29069: heap size 246 MB, throughput 0.998884
Reading from 29069: heap size 246 MB, throughput 0.998634
Reading from 29069: heap size 249 MB, throughput 0.998572
Reading from 29067: heap size 1106 MB, throughput 0.965187
Reading from 29069: heap size 249 MB, throughput 0.993771
Reading from 29069: heap size 253 MB, throughput 0.997088
Equal recommendation: 3087 MB each
Reading from 29069: heap size 253 MB, throughput 0.99635
Reading from 29067: heap size 1106 MB, throughput 0.977788
Reading from 29069: heap size 260 MB, throughput 0.998394
Reading from 29069: heap size 260 MB, throughput 0.997788
Reading from 29067: heap size 1108 MB, throughput 0.980645
Reading from 29069: heap size 267 MB, throughput 0.998396
Reading from 29069: heap size 267 MB, throughput 0.997499
Reading from 29067: heap size 1110 MB, throughput 0.977113
Reading from 29069: heap size 272 MB, throughput 0.998352
Equal recommendation: 3087 MB each
Reading from 29069: heap size 272 MB, throughput 0.997566
Reading from 29069: heap size 278 MB, throughput 0.998029
Reading from 29067: heap size 1114 MB, throughput 0.677702
Reading from 29069: heap size 278 MB, throughput 0.997899
Reading from 29069: heap size 282 MB, throughput 0.995473
Reading from 29067: heap size 1208 MB, throughput 0.9942
Reading from 29069: heap size 283 MB, throughput 0.993265
Reading from 29069: heap size 291 MB, throughput 0.997566
Equal recommendation: 3087 MB each
Reading from 29067: heap size 1204 MB, throughput 0.993839
Reading from 29069: heap size 291 MB, throughput 0.996822
Reading from 29069: heap size 299 MB, throughput 0.998155
Reading from 29067: heap size 1212 MB, throughput 0.993008
Reading from 29069: heap size 299 MB, throughput 0.998231
Reading from 29069: heap size 306 MB, throughput 0.99771
Reading from 29067: heap size 1217 MB, throughput 0.99177
Reading from 29069: heap size 306 MB, throughput 0.997826
Equal recommendation: 3087 MB each
Reading from 29067: heap size 1218 MB, throughput 0.990443
Reading from 29069: heap size 312 MB, throughput 0.997381
Reading from 29069: heap size 313 MB, throughput 0.997684
Reading from 29069: heap size 318 MB, throughput 0.993906
Reading from 29067: heap size 1211 MB, throughput 0.988733
Reading from 29069: heap size 319 MB, throughput 0.996957
Reading from 29069: heap size 330 MB, throughput 0.995211
Reading from 29067: heap size 1121 MB, throughput 0.986136
Reading from 29069: heap size 330 MB, throughput 0.997973
Equal recommendation: 3087 MB each
Reading from 29067: heap size 1200 MB, throughput 0.98701
Reading from 29069: heap size 339 MB, throughput 0.997922
Reading from 29069: heap size 340 MB, throughput 0.998018
Reading from 29067: heap size 1137 MB, throughput 0.983372
Reading from 29069: heap size 347 MB, throughput 0.997822
Reading from 29067: heap size 1194 MB, throughput 0.983999
Reading from 29069: heap size 348 MB, throughput 0.998198
Equal recommendation: 3087 MB each
Reading from 29069: heap size 355 MB, throughput 0.997848
Reading from 29067: heap size 1197 MB, throughput 0.980889
Reading from 29069: heap size 356 MB, throughput 0.995245
Reading from 29069: heap size 363 MB, throughput 0.99833
Reading from 29067: heap size 1198 MB, throughput 0.982876
Reading from 29069: heap size 364 MB, throughput 0.997755
Reading from 29069: heap size 372 MB, throughput 0.998385
Reading from 29067: heap size 1199 MB, throughput 0.98176
Equal recommendation: 3087 MB each
Reading from 29069: heap size 372 MB, throughput 0.997731
Reading from 29067: heap size 1200 MB, throughput 0.97972
Reading from 29069: heap size 379 MB, throughput 0.998249
Reading from 29067: heap size 1204 MB, throughput 0.978369
Reading from 29069: heap size 380 MB, throughput 0.997886
Reading from 29069: heap size 387 MB, throughput 0.997443
Reading from 29067: heap size 1206 MB, throughput 0.975333
Reading from 29069: heap size 387 MB, throughput 0.996951
Equal recommendation: 3087 MB each
Reading from 29069: heap size 396 MB, throughput 0.998682
Reading from 29067: heap size 1213 MB, throughput 0.975744
Reading from 29069: heap size 396 MB, throughput 0.998051
Reading from 29067: heap size 1217 MB, throughput 0.975659
Reading from 29069: heap size 404 MB, throughput 0.997945
Reading from 29067: heap size 1221 MB, throughput 0.976831
Equal recommendation: 3087 MB each
Reading from 29069: heap size 404 MB, throughput 0.997206
Reading from 29069: heap size 412 MB, throughput 0.998501
Reading from 29067: heap size 1225 MB, throughput 0.979044
Reading from 29069: heap size 412 MB, throughput 0.997511
Reading from 29069: heap size 419 MB, throughput 0.993189
Reading from 29067: heap size 1227 MB, throughput 0.972834
Reading from 29069: heap size 419 MB, throughput 0.998549
Equal recommendation: 3087 MB each
Reading from 29067: heap size 1231 MB, throughput 0.974637
Reading from 29069: heap size 428 MB, throughput 0.998356
Reading from 29067: heap size 1231 MB, throughput 0.977126
Reading from 29069: heap size 431 MB, throughput 0.998104
Reading from 29069: heap size 441 MB, throughput 0.998404
Reading from 29067: heap size 1232 MB, throughput 0.977384
Equal recommendation: 3087 MB each
Reading from 29069: heap size 441 MB, throughput 0.998166
Reading from 29067: heap size 1234 MB, throughput 0.974294
Reading from 29069: heap size 450 MB, throughput 0.994945
Reading from 29069: heap size 450 MB, throughput 0.998181
Reading from 29067: heap size 1235 MB, throughput 0.977031
Reading from 29069: heap size 461 MB, throughput 0.99781
Reading from 29067: heap size 1236 MB, throughput 0.974865
Equal recommendation: 3087 MB each
Reading from 29069: heap size 462 MB, throughput 0.960111
Reading from 29067: heap size 1236 MB, throughput 0.97851
Reading from 29069: heap size 475 MB, throughput 0.999235
Reading from 29069: heap size 476 MB, throughput 0.999238
Reading from 29067: heap size 1237 MB, throughput 0.631289
Reading from 29069: heap size 485 MB, throughput 0.998366
Equal recommendation: 3087 MB each
Reading from 29069: heap size 486 MB, throughput 0.997783
Reading from 29067: heap size 1351 MB, throughput 0.968652
Reading from 29069: heap size 497 MB, throughput 0.99887
Reading from 29067: heap size 1352 MB, throughput 0.987988
Reading from 29069: heap size 497 MB, throughput 0.998545
Equal recommendation: 3087 MB each
Reading from 29067: heap size 1361 MB, throughput 0.986453
Reading from 29069: heap size 507 MB, throughput 0.998766
Reading from 29069: heap size 507 MB, throughput 0.998648
Reading from 29069: heap size 516 MB, throughput 0.997033
Equal recommendation: 3087 MB each
Reading from 29069: heap size 517 MB, throughput 0.998354
Reading from 29069: heap size 530 MB, throughput 0.998665
Reading from 29069: heap size 530 MB, throughput 0.998679
Reading from 29067: heap size 1364 MB, throughput 0.993197
Equal recommendation: 3087 MB each
Reading from 29069: heap size 540 MB, throughput 0.998488
Reading from 29069: heap size 540 MB, throughput 0.997955
Reading from 29069: heap size 550 MB, throughput 0.997716
Reading from 29069: heap size 551 MB, throughput 0.998439
Equal recommendation: 3087 MB each
Reading from 29069: heap size 562 MB, throughput 0.99851
Reading from 29067: heap size 1361 MB, throughput 0.994526
Reading from 29069: heap size 563 MB, throughput 0.99834
Reading from 29069: heap size 574 MB, throughput 0.998506
Equal recommendation: 3087 MB each
Reading from 29067: heap size 1369 MB, throughput 0.985389
Reading from 29067: heap size 1356 MB, throughput 0.871453
Reading from 29067: heap size 1377 MB, throughput 0.759783
Reading from 29067: heap size 1375 MB, throughput 0.681967
Reading from 29067: heap size 1409 MB, throughput 0.628905
Reading from 29069: heap size 574 MB, throughput 0.997456
Reading from 29067: heap size 1432 MB, throughput 0.655338
Reading from 29067: heap size 1454 MB, throughput 0.629662
Reading from 29067: heap size 1480 MB, throughput 0.657375
Reading from 29067: heap size 1493 MB, throughput 0.63552
Reading from 29067: heap size 1523 MB, throughput 0.686353
Reading from 29067: heap size 1526 MB, throughput 0.174445
Reading from 29067: heap size 1552 MB, throughput 0.9916
Reading from 29069: heap size 585 MB, throughput 0.998405
Equal recommendation: 3087 MB each
Reading from 29069: heap size 586 MB, throughput 0.998488
Reading from 29067: heap size 1561 MB, throughput 0.995631
Reading from 29069: heap size 598 MB, throughput 0.998451
Reading from 29067: heap size 1599 MB, throughput 0.994011
Reading from 29067: heap size 1603 MB, throughput 0.992882
Client 29069 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 29067: heap size 1609 MB, throughput 0.99131
Reading from 29067: heap size 1619 MB, throughput 0.990709
Recommendation: one client; give it all the memory
Reading from 29067: heap size 1600 MB, throughput 0.990595
Reading from 29067: heap size 1615 MB, throughput 0.989198
Recommendation: one client; give it all the memory
Reading from 29067: heap size 1581 MB, throughput 0.988882
Reading from 29067: heap size 1387 MB, throughput 0.987226
Recommendation: one client; give it all the memory
Reading from 29067: heap size 1558 MB, throughput 0.985881
Reading from 29067: heap size 1414 MB, throughput 0.986019
Recommendation: one client; give it all the memory
Reading from 29067: heap size 1545 MB, throughput 0.985395
Reading from 29067: heap size 1557 MB, throughput 0.985081
Recommendation: one client; give it all the memory
Reading from 29067: heap size 1550 MB, throughput 0.983571
Reading from 29067: heap size 1554 MB, throughput 0.983835
Recommendation: one client; give it all the memory
Reading from 29067: heap size 1559 MB, throughput 0.981824
Reading from 29067: heap size 1561 MB, throughput 0.982372
Recommendation: one client; give it all the memory
Reading from 29067: heap size 1568 MB, throughput 0.981014
Reading from 29067: heap size 1569 MB, throughput 0.981236
Recommendation: one client; give it all the memory
Reading from 29067: heap size 1577 MB, throughput 0.981201
Reading from 29067: heap size 1577 MB, throughput 0.980754
Recommendation: one client; give it all the memory
Reading from 29067: heap size 1585 MB, throughput 0.981561
Reading from 29067: heap size 1585 MB, throughput 0.979717
Recommendation: one client; give it all the memory
Reading from 29067: heap size 1593 MB, throughput 0.980279
Reading from 29067: heap size 1593 MB, throughput 0.980616
Recommendation: one client; give it all the memory
Reading from 29067: heap size 1600 MB, throughput 0.98084
Recommendation: one client; give it all the memory
Reading from 29067: heap size 1600 MB, throughput 0.980283
Reading from 29067: heap size 1606 MB, throughput 0.98077
Recommendation: one client; give it all the memory
Reading from 29067: heap size 1607 MB, throughput 0.979849
Reading from 29067: heap size 1614 MB, throughput 0.982417
Recommendation: one client; give it all the memory
Reading from 29067: heap size 1614 MB, throughput 0.83599
Recommendation: one client; give it all the memory
Reading from 29067: heap size 1681 MB, throughput 0.996179
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 29067: heap size 1681 MB, throughput 0.99546
Recommendation: one client; give it all the memory
Reading from 29067: heap size 1694 MB, throughput 0.995139
Reading from 29067: heap size 1700 MB, throughput 0.891819
Reading from 29067: heap size 1679 MB, throughput 0.815952
Reading from 29067: heap size 1701 MB, throughput 0.794948
Reading from 29067: heap size 1734 MB, throughput 0.81799
Reading from 29067: heap size 1748 MB, throughput 0.800479
Client 29067 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
