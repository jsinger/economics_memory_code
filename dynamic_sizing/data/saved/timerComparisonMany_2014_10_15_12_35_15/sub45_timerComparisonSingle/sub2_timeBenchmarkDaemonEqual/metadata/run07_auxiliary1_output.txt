economemd
    total memory: 3312 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub45_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 23668: heap size 9 MB, throughput 0.988023
Clients: 1
Reading from 23667: heap size 9 MB, throughput 0.987512
Clients: 2
Client 23668 has a minimum heap size of 276 MB
Client 23667 has a minimum heap size of 276 MB
Reading from 23668: heap size 9 MB, throughput 0.978383
Reading from 23667: heap size 9 MB, throughput 0.982781
Reading from 23668: heap size 11 MB, throughput 0.977728
Reading from 23667: heap size 11 MB, throughput 0.973405
Reading from 23667: heap size 11 MB, throughput 0.976752
Reading from 23668: heap size 11 MB, throughput 0.972606
Reading from 23668: heap size 15 MB, throughput 0.863815
Reading from 23667: heap size 15 MB, throughput 0.848599
Reading from 23668: heap size 18 MB, throughput 0.979684
Reading from 23667: heap size 18 MB, throughput 0.964193
Reading from 23668: heap size 23 MB, throughput 0.935899
Reading from 23667: heap size 23 MB, throughput 0.956062
Reading from 23668: heap size 28 MB, throughput 0.866771
Reading from 23667: heap size 27 MB, throughput 0.794508
Reading from 23667: heap size 27 MB, throughput 0.55168
Reading from 23668: heap size 28 MB, throughput 0.53195
Reading from 23668: heap size 38 MB, throughput 0.847261
Reading from 23667: heap size 36 MB, throughput 0.930597
Reading from 23668: heap size 42 MB, throughput 0.823773
Reading from 23667: heap size 40 MB, throughput 0.913119
Reading from 23668: heap size 43 MB, throughput 0.857538
Reading from 23667: heap size 41 MB, throughput 0.640702
Reading from 23668: heap size 47 MB, throughput 0.294978
Reading from 23667: heap size 45 MB, throughput 0.231917
Reading from 23667: heap size 58 MB, throughput 0.82204
Reading from 23667: heap size 64 MB, throughput 0.784724
Reading from 23668: heap size 61 MB, throughput 0.339626
Reading from 23668: heap size 82 MB, throughput 0.632513
Reading from 23668: heap size 84 MB, throughput 0.776983
Reading from 23667: heap size 66 MB, throughput 0.295194
Reading from 23667: heap size 87 MB, throughput 0.646559
Reading from 23668: heap size 87 MB, throughput 0.662591
Reading from 23668: heap size 89 MB, throughput 0.601767
Reading from 23667: heap size 90 MB, throughput 0.741651
Reading from 23667: heap size 93 MB, throughput 0.6312
Reading from 23668: heap size 94 MB, throughput 0.757296
Reading from 23667: heap size 96 MB, throughput 0.181309
Reading from 23667: heap size 123 MB, throughput 0.635302
Reading from 23668: heap size 97 MB, throughput 0.133429
Reading from 23668: heap size 127 MB, throughput 0.672931
Reading from 23668: heap size 129 MB, throughput 0.719151
Reading from 23668: heap size 132 MB, throughput 0.696202
Reading from 23667: heap size 126 MB, throughput 0.17551
Reading from 23667: heap size 157 MB, throughput 0.69375
Reading from 23668: heap size 136 MB, throughput 0.613885
Reading from 23667: heap size 157 MB, throughput 0.818962
Reading from 23668: heap size 142 MB, throughput 0.608653
Reading from 23667: heap size 160 MB, throughput 0.637774
Reading from 23667: heap size 163 MB, throughput 0.5868
Reading from 23667: heap size 169 MB, throughput 0.612395
Reading from 23668: heap size 149 MB, throughput 0.167329
Reading from 23667: heap size 173 MB, throughput 0.733721
Reading from 23668: heap size 181 MB, throughput 0.698869
Reading from 23668: heap size 185 MB, throughput 0.686287
Reading from 23668: heap size 187 MB, throughput 0.702742
Reading from 23668: heap size 192 MB, throughput 0.632084
Reading from 23667: heap size 177 MB, throughput 0.144309
Reading from 23668: heap size 196 MB, throughput 0.860688
Reading from 23667: heap size 218 MB, throughput 0.88607
Reading from 23668: heap size 203 MB, throughput 0.865887
Reading from 23668: heap size 205 MB, throughput 0.631646
Reading from 23668: heap size 214 MB, throughput 0.491528
Reading from 23667: heap size 220 MB, throughput 0.886976
Reading from 23668: heap size 219 MB, throughput 0.324615
Reading from 23667: heap size 227 MB, throughput 0.627147
Reading from 23667: heap size 233 MB, throughput 0.486149
Reading from 23667: heap size 240 MB, throughput 0.815553
Reading from 23667: heap size 244 MB, throughput 0.45517
Reading from 23668: heap size 224 MB, throughput 0.0925968
Reading from 23667: heap size 249 MB, throughput 0.667034
Reading from 23667: heap size 258 MB, throughput 0.66274
Reading from 23668: heap size 259 MB, throughput 0.760302
Reading from 23668: heap size 203 MB, throughput 0.0889761
Reading from 23668: heap size 301 MB, throughput 0.670534
Reading from 23668: heap size 303 MB, throughput 0.797912
Reading from 23667: heap size 258 MB, throughput 0.18524
Reading from 23668: heap size 305 MB, throughput 0.890758
Reading from 23668: heap size 306 MB, throughput 0.88815
Reading from 23667: heap size 291 MB, throughput 0.789051
Reading from 23668: heap size 301 MB, throughput 0.870022
Reading from 23667: heap size 295 MB, throughput 0.790662
Reading from 23668: heap size 239 MB, throughput 0.782667
Reading from 23668: heap size 295 MB, throughput 0.798774
Reading from 23667: heap size 290 MB, throughput 0.847014
Reading from 23668: heap size 299 MB, throughput 0.825217
Reading from 23667: heap size 293 MB, throughput 0.787247
Reading from 23668: heap size 291 MB, throughput 0.753726
Reading from 23668: heap size 295 MB, throughput 0.729295
Reading from 23667: heap size 289 MB, throughput 0.776405
Reading from 23667: heap size 292 MB, throughput 0.818354
Reading from 23668: heap size 293 MB, throughput 0.969525
Reading from 23668: heap size 295 MB, throughput 0.952619
Reading from 23668: heap size 288 MB, throughput 0.813295
Reading from 23667: heap size 291 MB, throughput 0.953389
Reading from 23668: heap size 293 MB, throughput 0.835757
Reading from 23668: heap size 288 MB, throughput 0.734554
Reading from 23668: heap size 290 MB, throughput 0.760029
Reading from 23667: heap size 292 MB, throughput 0.166416
Reading from 23668: heap size 289 MB, throughput 0.951467
Reading from 23667: heap size 339 MB, throughput 0.550151
Reading from 23668: heap size 290 MB, throughput 0.906757
Reading from 23667: heap size 339 MB, throughput 0.851124
Reading from 23668: heap size 289 MB, throughput 0.835259
Reading from 23668: heap size 291 MB, throughput 0.647666
Reading from 23667: heap size 345 MB, throughput 0.696142
Reading from 23668: heap size 293 MB, throughput 0.604877
Reading from 23668: heap size 294 MB, throughput 0.534882
Reading from 23667: heap size 346 MB, throughput 0.770658
Reading from 23668: heap size 300 MB, throughput 0.682467
Reading from 23668: heap size 301 MB, throughput 0.607956
Reading from 23667: heap size 349 MB, throughput 0.833047
Reading from 23668: heap size 308 MB, throughput 0.60294
Reading from 23667: heap size 351 MB, throughput 0.772492
Equal recommendation: 1656 MB each
Reading from 23667: heap size 353 MB, throughput 0.582614
Reading from 23667: heap size 354 MB, throughput 0.672079
Reading from 23668: heap size 309 MB, throughput 0.908055
Reading from 23667: heap size 361 MB, throughput 0.580065
Reading from 23667: heap size 361 MB, throughput 0.553068
Reading from 23667: heap size 371 MB, throughput 0.639782
Reading from 23667: heap size 371 MB, throughput 0.960363
Reading from 23668: heap size 316 MB, throughput 0.555935
Reading from 23668: heap size 356 MB, throughput 0.956843
Reading from 23667: heap size 377 MB, throughput 0.976578
Reading from 23668: heap size 362 MB, throughput 0.98082
Reading from 23667: heap size 379 MB, throughput 0.987356
Reading from 23668: heap size 362 MB, throughput 0.990782
Reading from 23667: heap size 380 MB, throughput 0.978066
Reading from 23668: heap size 363 MB, throughput 0.989403
Reading from 23667: heap size 383 MB, throughput 0.976036
Reading from 23668: heap size 365 MB, throughput 0.979661
Equal recommendation: 1656 MB each
Reading from 23668: heap size 362 MB, throughput 0.987258
Reading from 23667: heap size 380 MB, throughput 0.974041
Reading from 23668: heap size 365 MB, throughput 0.980286
Reading from 23667: heap size 384 MB, throughput 0.975906
Reading from 23668: heap size 358 MB, throughput 0.979658
Reading from 23667: heap size 386 MB, throughput 0.973528
Reading from 23668: heap size 361 MB, throughput 0.97141
Reading from 23667: heap size 387 MB, throughput 0.973074
Reading from 23668: heap size 360 MB, throughput 0.976437
Reading from 23668: heap size 361 MB, throughput 0.978265
Reading from 23667: heap size 391 MB, throughput 0.977153
Reading from 23668: heap size 363 MB, throughput 0.980763
Reading from 23667: heap size 392 MB, throughput 0.9733
Reading from 23668: heap size 364 MB, throughput 0.970898
Reading from 23667: heap size 396 MB, throughput 0.974805
Equal recommendation: 1656 MB each
Reading from 23668: heap size 365 MB, throughput 0.982193
Reading from 23667: heap size 398 MB, throughput 0.983798
Reading from 23668: heap size 367 MB, throughput 0.988167
Reading from 23667: heap size 400 MB, throughput 0.943344
Reading from 23667: heap size 401 MB, throughput 0.823704
Reading from 23668: heap size 367 MB, throughput 0.970048
Reading from 23667: heap size 399 MB, throughput 0.815719
Reading from 23668: heap size 369 MB, throughput 0.824321
Reading from 23667: heap size 404 MB, throughput 0.804668
Reading from 23668: heap size 367 MB, throughput 0.767621
Reading from 23668: heap size 373 MB, throughput 0.722133
Reading from 23668: heap size 379 MB, throughput 0.806045
Reading from 23668: heap size 383 MB, throughput 0.794983
Reading from 23667: heap size 413 MB, throughput 0.978513
Reading from 23668: heap size 389 MB, throughput 0.985791
Reading from 23667: heap size 415 MB, throughput 0.98016
Reading from 23668: heap size 390 MB, throughput 0.978755
Reading from 23667: heap size 416 MB, throughput 0.987493
Reading from 23668: heap size 388 MB, throughput 0.981076
Reading from 23668: heap size 392 MB, throughput 0.985651
Reading from 23667: heap size 419 MB, throughput 0.985311
Equal recommendation: 1656 MB each
Reading from 23668: heap size 390 MB, throughput 0.984237
Reading from 23667: heap size 416 MB, throughput 0.98465
Reading from 23668: heap size 392 MB, throughput 0.982648
Reading from 23667: heap size 419 MB, throughput 0.983947
Reading from 23668: heap size 388 MB, throughput 0.981969
Reading from 23667: heap size 415 MB, throughput 0.979764
Reading from 23668: heap size 391 MB, throughput 0.981247
Reading from 23667: heap size 418 MB, throughput 0.978221
Reading from 23668: heap size 387 MB, throughput 0.977366
Reading from 23668: heap size 389 MB, throughput 0.978403
Reading from 23667: heap size 417 MB, throughput 0.981381
Reading from 23668: heap size 387 MB, throughput 0.983499
Equal recommendation: 1656 MB each
Reading from 23667: heap size 418 MB, throughput 0.981618
Reading from 23668: heap size 389 MB, throughput 0.978871
Reading from 23667: heap size 419 MB, throughput 0.989579
Reading from 23668: heap size 390 MB, throughput 0.985477
Reading from 23667: heap size 420 MB, throughput 0.968606
Reading from 23667: heap size 420 MB, throughput 0.863357
Reading from 23667: heap size 421 MB, throughput 0.859845
Reading from 23668: heap size 390 MB, throughput 0.9696
Reading from 23668: heap size 390 MB, throughput 0.855279
Reading from 23668: heap size 390 MB, throughput 0.802144
Reading from 23668: heap size 396 MB, throughput 0.819537
Reading from 23667: heap size 426 MB, throughput 0.961616
Reading from 23668: heap size 398 MB, throughput 0.97009
Reading from 23667: heap size 427 MB, throughput 0.992378
Reading from 23668: heap size 406 MB, throughput 0.986188
Reading from 23667: heap size 432 MB, throughput 0.989121
Reading from 23668: heap size 407 MB, throughput 0.986308
Reading from 23668: heap size 409 MB, throughput 0.984969
Equal recommendation: 1656 MB each
Reading from 23667: heap size 433 MB, throughput 0.99118
Reading from 23668: heap size 410 MB, throughput 0.984964
Reading from 23667: heap size 434 MB, throughput 0.98603
Reading from 23668: heap size 409 MB, throughput 0.978877
Reading from 23667: heap size 436 MB, throughput 0.991652
Reading from 23668: heap size 411 MB, throughput 0.982037
Reading from 23667: heap size 433 MB, throughput 0.986317
Reading from 23668: heap size 410 MB, throughput 0.980212
Reading from 23667: heap size 435 MB, throughput 0.982307
Reading from 23668: heap size 411 MB, throughput 0.984087
Equal recommendation: 1656 MB each
Reading from 23667: heap size 436 MB, throughput 0.982393
Reading from 23668: heap size 413 MB, throughput 0.980782
Reading from 23668: heap size 413 MB, throughput 0.974255
Reading from 23667: heap size 436 MB, throughput 0.989084
Reading from 23667: heap size 437 MB, throughput 0.941916
Reading from 23667: heap size 438 MB, throughput 0.865868
Reading from 23668: heap size 411 MB, throughput 0.97583
Reading from 23667: heap size 442 MB, throughput 0.90988
Reading from 23668: heap size 416 MB, throughput 0.242795
Reading from 23668: heap size 443 MB, throughput 0.994311
Reading from 23668: heap size 450 MB, throughput 0.993077
Reading from 23668: heap size 457 MB, throughput 0.99454
Reading from 23667: heap size 443 MB, throughput 0.990343
Reading from 23668: heap size 460 MB, throughput 0.995755
Reading from 23667: heap size 449 MB, throughput 0.990284
Reading from 23668: heap size 462 MB, throughput 0.991221
Equal recommendation: 1656 MB each
Reading from 23667: heap size 450 MB, throughput 0.989969
Reading from 23668: heap size 463 MB, throughput 0.988212
Reading from 23667: heap size 451 MB, throughput 0.9882
Reading from 23668: heap size 458 MB, throughput 0.990121
Reading from 23668: heap size 417 MB, throughput 0.991004
Reading from 23667: heap size 453 MB, throughput 0.99045
Reading from 23668: heap size 453 MB, throughput 0.987206
Reading from 23667: heap size 451 MB, throughput 0.988359
Equal recommendation: 1656 MB each
Reading from 23668: heap size 457 MB, throughput 0.987666
Reading from 23667: heap size 453 MB, throughput 0.98333
Reading from 23668: heap size 457 MB, throughput 0.984489
Reading from 23667: heap size 455 MB, throughput 0.984945
Reading from 23668: heap size 457 MB, throughput 0.981837
Reading from 23667: heap size 455 MB, throughput 0.989106
Reading from 23667: heap size 456 MB, throughput 0.898754
Reading from 23667: heap size 457 MB, throughput 0.85842
Reading from 23668: heap size 457 MB, throughput 0.988189
Reading from 23668: heap size 460 MB, throughput 0.909273
Reading from 23668: heap size 456 MB, throughput 0.828977
Reading from 23668: heap size 462 MB, throughput 0.827951
Reading from 23667: heap size 463 MB, throughput 0.9892
Reading from 23668: heap size 470 MB, throughput 0.980684
Reading from 23668: heap size 473 MB, throughput 0.984586
Reading from 23667: heap size 465 MB, throughput 0.993717
Equal recommendation: 1656 MB each
Reading from 23668: heap size 468 MB, throughput 0.988075
Reading from 23667: heap size 469 MB, throughput 0.993163
Reading from 23668: heap size 473 MB, throughput 0.985994
Reading from 23667: heap size 470 MB, throughput 0.988407
Reading from 23668: heap size 469 MB, throughput 0.987961
Reading from 23668: heap size 473 MB, throughput 0.98245
Reading from 23667: heap size 470 MB, throughput 0.989222
Reading from 23668: heap size 471 MB, throughput 0.982583
Equal recommendation: 1656 MB each
Reading from 23667: heap size 472 MB, throughput 0.988435
Reading from 23668: heap size 473 MB, throughput 0.985643
Reading from 23667: heap size 473 MB, throughput 0.986304
Reading from 23668: heap size 475 MB, throughput 0.980876
Reading from 23668: heap size 475 MB, throughput 0.992601
Reading from 23667: heap size 473 MB, throughput 0.993125
Reading from 23668: heap size 477 MB, throughput 0.932691
Reading from 23668: heap size 478 MB, throughput 0.867347
Reading from 23667: heap size 475 MB, throughput 0.924075
Reading from 23668: heap size 483 MB, throughput 0.912503
Reading from 23667: heap size 476 MB, throughput 0.264546
Reading from 23668: heap size 484 MB, throughput 0.990885
Reading from 23667: heap size 515 MB, throughput 0.995802
Equal recommendation: 1656 MB each
Reading from 23668: heap size 489 MB, throughput 0.991516
Reading from 23667: heap size 517 MB, throughput 0.996269
Reading from 23668: heap size 491 MB, throughput 0.990496
Reading from 23667: heap size 526 MB, throughput 0.99118
Reading from 23668: heap size 491 MB, throughput 0.987142
Reading from 23667: heap size 527 MB, throughput 0.991951
Reading from 23668: heap size 493 MB, throughput 0.98675
Reading from 23668: heap size 491 MB, throughput 0.987629
Reading from 23667: heap size 525 MB, throughput 0.99058
Equal recommendation: 1656 MB each
Reading from 23668: heap size 493 MB, throughput 0.985763
Reading from 23667: heap size 528 MB, throughput 0.992458
Reading from 23668: heap size 496 MB, throughput 0.981735
Reading from 23667: heap size 529 MB, throughput 0.987344
Reading from 23668: heap size 496 MB, throughput 0.982909
Reading from 23668: heap size 502 MB, throughput 0.897063
Reading from 23668: heap size 504 MB, throughput 0.890056
Reading from 23667: heap size 529 MB, throughput 0.986202
Reading from 23667: heap size 529 MB, throughput 0.886904
Reading from 23667: heap size 535 MB, throughput 0.948381
Reading from 23668: heap size 510 MB, throughput 0.9915
Equal recommendation: 1656 MB each
Reading from 23668: heap size 511 MB, throughput 0.991564
Reading from 23667: heap size 543 MB, throughput 0.991564
Reading from 23668: heap size 515 MB, throughput 0.99154
Reading from 23667: heap size 545 MB, throughput 0.989425
Reading from 23668: heap size 517 MB, throughput 0.991103
Reading from 23667: heap size 544 MB, throughput 0.990451
Reading from 23668: heap size 515 MB, throughput 0.987731
Reading from 23667: heap size 546 MB, throughput 0.985987
Equal recommendation: 1656 MB each
Reading from 23668: heap size 518 MB, throughput 0.986275
Reading from 23667: heap size 550 MB, throughput 0.990084
Reading from 23668: heap size 518 MB, throughput 0.986697
Reading from 23667: heap size 551 MB, throughput 0.984194
Reading from 23668: heap size 519 MB, throughput 0.991338
Reading from 23668: heap size 521 MB, throughput 0.917716
Reading from 23668: heap size 522 MB, throughput 0.9375
Reading from 23667: heap size 556 MB, throughput 0.989973
Reading from 23667: heap size 558 MB, throughput 0.889903
Reading from 23667: heap size 563 MB, throughput 0.979862
Reading from 23668: heap size 529 MB, throughput 0.990114
Equal recommendation: 1656 MB each
Reading from 23668: heap size 529 MB, throughput 0.993928
Reading from 23667: heap size 564 MB, throughput 0.994463
Reading from 23668: heap size 531 MB, throughput 0.992352
Reading from 23667: heap size 565 MB, throughput 0.990495
Reading from 23668: heap size 533 MB, throughput 0.991287
Reading from 23667: heap size 568 MB, throughput 0.992328
Equal recommendation: 1656 MB each
Reading from 23668: heap size 531 MB, throughput 0.988609
Reading from 23667: heap size 566 MB, throughput 0.988187
Reading from 23668: heap size 533 MB, throughput 0.987906
Reading from 23667: heap size 569 MB, throughput 0.988483
Reading from 23668: heap size 536 MB, throughput 0.991933
Reading from 23668: heap size 537 MB, throughput 0.925409
Reading from 23668: heap size 536 MB, throughput 0.897036
Reading from 23667: heap size 569 MB, throughput 0.990015
Reading from 23667: heap size 570 MB, throughput 0.923859
Reading from 23667: heap size 571 MB, throughput 0.955215
Reading from 23668: heap size 539 MB, throughput 0.991935
Equal recommendation: 1656 MB each
Reading from 23667: heap size 572 MB, throughput 0.993056
Reading from 23668: heap size 546 MB, throughput 0.990569
Reading from 23668: heap size 548 MB, throughput 0.990923
Reading from 23667: heap size 576 MB, throughput 0.993394
Reading from 23668: heap size 548 MB, throughput 0.990843
Reading from 23667: heap size 578 MB, throughput 0.989309
Equal recommendation: 1656 MB each
Reading from 23668: heap size 550 MB, throughput 0.989139
Reading from 23667: heap size 578 MB, throughput 0.990494
Reading from 23668: heap size 552 MB, throughput 0.989735
Reading from 23667: heap size 580 MB, throughput 0.990642
Reading from 23668: heap size 553 MB, throughput 0.989222
Reading from 23668: heap size 555 MB, throughput 0.901129
Reading from 23668: heap size 557 MB, throughput 0.984024
Reading from 23667: heap size 583 MB, throughput 0.993021
Reading from 23667: heap size 583 MB, throughput 0.907584
Reading from 23667: heap size 585 MB, throughput 0.986914
Equal recommendation: 1656 MB each
Reading from 23668: heap size 565 MB, throughput 0.993229
Reading from 23667: heap size 587 MB, throughput 0.994287
Reading from 23668: heap size 566 MB, throughput 0.991695
Reading from 23667: heap size 588 MB, throughput 0.992291
Reading from 23668: heap size 567 MB, throughput 0.991358
Reading from 23668: heap size 569 MB, throughput 0.990252
Equal recommendation: 1656 MB each
Reading from 23667: heap size 591 MB, throughput 0.991241
Reading from 23668: heap size 569 MB, throughput 0.98931
Reading from 23667: heap size 590 MB, throughput 0.990194
Reading from 23668: heap size 570 MB, throughput 0.993589
Reading from 23668: heap size 573 MB, throughput 0.932654
Reading from 23668: heap size 573 MB, throughput 0.982829
Reading from 23667: heap size 592 MB, throughput 0.993815
Reading from 23667: heap size 593 MB, throughput 0.976163
Reading from 23667: heap size 595 MB, throughput 0.942449
Equal recommendation: 1656 MB each
Reading from 23668: heap size 581 MB, throughput 0.994572
Reading from 23667: heap size 602 MB, throughput 0.993847
Reading from 23668: heap size 581 MB, throughput 0.993573
Reading from 23667: heap size 602 MB, throughput 0.993524
Reading from 23668: heap size 581 MB, throughput 0.99177
Equal recommendation: 1656 MB each
Reading from 23668: heap size 583 MB, throughput 0.989127
Reading from 23667: heap size 603 MB, throughput 0.991223
Reading from 23668: heap size 584 MB, throughput 0.989504
Reading from 23667: heap size 605 MB, throughput 0.991559
Reading from 23668: heap size 585 MB, throughput 0.987431
Reading from 23668: heap size 586 MB, throughput 0.90498
Reading from 23668: heap size 589 MB, throughput 0.992901
Reading from 23667: heap size 607 MB, throughput 0.994192
Reading from 23667: heap size 608 MB, throughput 0.975345
Reading from 23667: heap size 608 MB, throughput 0.912461
Equal recommendation: 1656 MB each
Reading from 23668: heap size 595 MB, throughput 0.992524
Reading from 23667: heap size 611 MB, throughput 0.994055
Reading from 23668: heap size 596 MB, throughput 0.991126
Reading from 23667: heap size 615 MB, throughput 0.993822
Reading from 23668: heap size 596 MB, throughput 0.989646
Equal recommendation: 1656 MB each
Reading from 23667: heap size 617 MB, throughput 0.992045
Reading from 23668: heap size 598 MB, throughput 0.991413
Reading from 23668: heap size 601 MB, throughput 0.992889
Reading from 23667: heap size 619 MB, throughput 0.990671
Reading from 23668: heap size 601 MB, throughput 0.933054
Reading from 23668: heap size 602 MB, throughput 0.989322
Reading from 23667: heap size 620 MB, throughput 0.995354
Equal recommendation: 1656 MB each
Reading from 23667: heap size 623 MB, throughput 0.94415
Reading from 23668: heap size 605 MB, throughput 0.993555
Reading from 23667: heap size 625 MB, throughput 0.981363
Reading from 23668: heap size 606 MB, throughput 0.993027
Reading from 23667: heap size 633 MB, throughput 0.994499
Reading from 23668: heap size 608 MB, throughput 0.991129
Equal recommendation: 1656 MB each
Reading from 23667: heap size 634 MB, throughput 0.992652
Reading from 23668: heap size 607 MB, throughput 0.99279
Reading from 23667: heap size 634 MB, throughput 0.991971
Reading from 23668: heap size 609 MB, throughput 0.993408
Reading from 23668: heap size 611 MB, throughput 0.922699
Client 23668 died
Clients: 1
Reading from 23667: heap size 636 MB, throughput 0.992718
Recommendation: one client; give it all the memory
Reading from 23667: heap size 639 MB, throughput 0.991391
Reading from 23667: heap size 641 MB, throughput 0.963882
Client 23667 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
