#ifndef STOPWATCH_HPP
#define STOPWATCH_HPP

#include <sys/time.h>

/*! \addtogroup common
  @{
*/

namespace economem
{
    //! Measure how much time has elapsed.
    class Stopwatch
    {
    public:
        Stopwatch();

        void start();
        void stop();
        unsigned long long microseconds() const;
        double seconds() const;

    private:
        timeval m_StartTime;
        timeval m_StopTime;
    };
}

/*! @} */

#endif
