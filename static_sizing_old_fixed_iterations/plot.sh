#!/bin/bash
# Plot graphs for an existing set of data

function usage()
{
    echo "Usage: ${0} dir"
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "${DIR}"

if [ -z "${1}" ]; then
    usage
    exit 1
fi

if [ ! -d "${1}" ]; then
    usage
    exit 1
fi

cd "${1}"

BENCHMARKS_FILE='benchmarks.txt'
ITERS="$(cat iterations.txt)"

while read line; do
    BENCHMARK="${line}"

    HEAP_SIZES_FILE="${BENCHMARK}_heapSizes.txt"
    AVG_TIMES_FILE="${BENCHMARK}_avgTimes.txt"
    STDDEV_FILE="${BENCHMARK}_stddev.txt"
    THROUGHPUT_FILE="${BENCHMARK}_throughput.txt"
    COEFFICIENT_FILE="${BENCHMARK}_fit_coefficient.txt"
    EXPONENT_FILE="${BENCHMARK}_fit_exponent.txt"

    octave -qf "${DIR}/measure_plot.m" "${BENCHMARK} (${ITERS} iterations)" "${HEAP_SIZES_FILE}" "${AVG_TIMES_FILE}" "${STDDEV_FILE}" "${THROUGHPUT_FILE}" "${BENCHMARK}_runtime.pdf" "${BENCHMARK}_throughput.pdf" "${COEFFICIENT_FILE}" "${EXPONENT_FILE}"

done <"${BENCHMARKS_FILE}"
