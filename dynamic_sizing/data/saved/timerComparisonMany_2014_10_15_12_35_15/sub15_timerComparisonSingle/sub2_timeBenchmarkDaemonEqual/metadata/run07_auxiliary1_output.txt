economemd
    total memory: 7338 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub15_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 15306: heap size 9 MB, throughput 0.989803
Clients: 1
Client 15306 has a minimum heap size of 12 MB
Reading from 15305: heap size 9 MB, throughput 0.992538
Clients: 2
Client 15305 has a minimum heap size of 1211 MB
Reading from 15306: heap size 9 MB, throughput 0.988368
Reading from 15305: heap size 9 MB, throughput 0.98333
Reading from 15305: heap size 9 MB, throughput 0.938882
Reading from 15306: heap size 9 MB, throughput 0.972463
Reading from 15305: heap size 9 MB, throughput 0.959086
Reading from 15306: heap size 9 MB, throughput 0.978088
Reading from 15305: heap size 11 MB, throughput 0.959848
Reading from 15306: heap size 11 MB, throughput 0.982969
Reading from 15306: heap size 11 MB, throughput 0.958019
Reading from 15305: heap size 11 MB, throughput 0.981601
Reading from 15306: heap size 16 MB, throughput 0.984245
Reading from 15305: heap size 17 MB, throughput 0.947106
Reading from 15305: heap size 17 MB, throughput 0.520773
Reading from 15305: heap size 29 MB, throughput 0.9077
Reading from 15306: heap size 16 MB, throughput 0.987314
Reading from 15305: heap size 30 MB, throughput 0.81249
Reading from 15305: heap size 35 MB, throughput 0.443759
Reading from 15305: heap size 47 MB, throughput 0.865448
Reading from 15306: heap size 24 MB, throughput 0.990507
Reading from 15305: heap size 49 MB, throughput 0.473102
Reading from 15305: heap size 64 MB, throughput 0.784744
Reading from 15306: heap size 25 MB, throughput 0.892474
Reading from 15305: heap size 71 MB, throughput 0.759847
Reading from 15305: heap size 71 MB, throughput 0.286355
Reading from 15306: heap size 40 MB, throughput 0.988345
Reading from 15305: heap size 102 MB, throughput 0.769201
Reading from 15306: heap size 41 MB, throughput 0.982377
Reading from 15305: heap size 102 MB, throughput 0.281673
Reading from 15306: heap size 48 MB, throughput 0.991368
Reading from 15305: heap size 133 MB, throughput 0.875682
Reading from 15306: heap size 48 MB, throughput 0.986855
Reading from 15305: heap size 133 MB, throughput 0.725737
Reading from 15305: heap size 135 MB, throughput 0.840355
Reading from 15306: heap size 57 MB, throughput 0.990564
Reading from 15306: heap size 57 MB, throughput 0.9675
Reading from 15305: heap size 139 MB, throughput 0.157987
Reading from 15306: heap size 68 MB, throughput 0.991609
Reading from 15305: heap size 169 MB, throughput 0.674923
Reading from 15306: heap size 68 MB, throughput 0.987735
Reading from 15305: heap size 174 MB, throughput 0.749522
Reading from 15305: heap size 176 MB, throughput 0.693267
Reading from 15306: heap size 78 MB, throughput 0.953647
Reading from 15305: heap size 178 MB, throughput 0.163785
Reading from 15306: heap size 78 MB, throughput 0.995743
Reading from 15305: heap size 217 MB, throughput 0.614508
Reading from 15305: heap size 221 MB, throughput 0.772036
Reading from 15305: heap size 224 MB, throughput 0.732472
Reading from 15305: heap size 230 MB, throughput 0.686733
Reading from 15305: heap size 240 MB, throughput 0.699788
Reading from 15305: heap size 244 MB, throughput 0.62644
Reading from 15306: heap size 89 MB, throughput 0.995302
Reading from 15305: heap size 247 MB, throughput 0.533857
Reading from 15305: heap size 256 MB, throughput 0.502702
Reading from 15306: heap size 91 MB, throughput 0.994474
Reading from 15305: heap size 260 MB, throughput 0.111011
Reading from 15305: heap size 308 MB, throughput 0.444708
Reading from 15305: heap size 319 MB, throughput 0.655801
Reading from 15305: heap size 321 MB, throughput 0.533478
Reading from 15305: heap size 325 MB, throughput 0.612037
Reading from 15306: heap size 101 MB, throughput 0.988647
Reading from 15305: heap size 332 MB, throughput 0.0917243
Reading from 15305: heap size 379 MB, throughput 0.502572
Reading from 15305: heap size 316 MB, throughput 0.576061
Reading from 15306: heap size 103 MB, throughput 0.996048
Reading from 15305: heap size 377 MB, throughput 0.688946
Reading from 15305: heap size 379 MB, throughput 0.640377
Reading from 15305: heap size 381 MB, throughput 0.663142
Reading from 15305: heap size 382 MB, throughput 0.533415
Reading from 15305: heap size 385 MB, throughput 0.63346
Reading from 15306: heap size 114 MB, throughput 0.99571
Reading from 15305: heap size 391 MB, throughput 0.106107
Reading from 15305: heap size 443 MB, throughput 0.515564
Reading from 15305: heap size 449 MB, throughput 0.589509
Reading from 15305: heap size 450 MB, throughput 0.502018
Reading from 15306: heap size 116 MB, throughput 0.986972
Reading from 15305: heap size 454 MB, throughput 0.570747
Reading from 15305: heap size 461 MB, throughput 0.497979
Reading from 15305: heap size 463 MB, throughput 0.533703
Equal recommendation: 3669 MB each
Reading from 15306: heap size 126 MB, throughput 0.995615
Reading from 15305: heap size 467 MB, throughput 0.0668009
Reading from 15306: heap size 126 MB, throughput 0.996102
Reading from 15305: heap size 522 MB, throughput 0.478213
Reading from 15305: heap size 524 MB, throughput 0.582698
Reading from 15305: heap size 526 MB, throughput 0.537163
Reading from 15305: heap size 531 MB, throughput 0.468699
Reading from 15305: heap size 534 MB, throughput 0.482734
Reading from 15306: heap size 134 MB, throughput 0.996414
Reading from 15305: heap size 539 MB, throughput 0.0874297
Reading from 15306: heap size 135 MB, throughput 0.995986
Reading from 15305: heap size 601 MB, throughput 0.537553
Reading from 15305: heap size 604 MB, throughput 0.603606
Reading from 15305: heap size 607 MB, throughput 0.624174
Reading from 15305: heap size 609 MB, throughput 0.631249
Reading from 15305: heap size 615 MB, throughput 0.571272
Reading from 15306: heap size 142 MB, throughput 0.996309
Reading from 15305: heap size 622 MB, throughput 0.586829
Reading from 15306: heap size 143 MB, throughput 0.99673
Reading from 15306: heap size 151 MB, throughput 0.997107
Reading from 15305: heap size 630 MB, throughput 0.233772
Reading from 15305: heap size 699 MB, throughput 0.794772
Reading from 15306: heap size 151 MB, throughput 0.997671
Reading from 15305: heap size 709 MB, throughput 0.833496
Reading from 15305: heap size 710 MB, throughput 0.800038
Reading from 15305: heap size 720 MB, throughput 0.438155
Reading from 15306: heap size 157 MB, throughput 0.997667
Reading from 15306: heap size 157 MB, throughput 0.99803
Reading from 15305: heap size 729 MB, throughput 0.0852044
Equal recommendation: 3669 MB each
Reading from 15305: heap size 799 MB, throughput 0.25315
Reading from 15305: heap size 786 MB, throughput 0.413148
Reading from 15305: heap size 794 MB, throughput 0.444962
Reading from 15305: heap size 789 MB, throughput 0.485213
Reading from 15306: heap size 162 MB, throughput 0.998801
Reading from 15306: heap size 162 MB, throughput 0.998547
Reading from 15305: heap size 792 MB, throughput 0.247063
Reading from 15305: heap size 864 MB, throughput 0.460794
Reading from 15305: heap size 869 MB, throughput 0.644204
Reading from 15305: heap size 875 MB, throughput 0.68398
Reading from 15306: heap size 167 MB, throughput 0.99884
Reading from 15305: heap size 876 MB, throughput 0.902586
Reading from 15305: heap size 880 MB, throughput 0.819946
Reading from 15305: heap size 882 MB, throughput 0.805883
Reading from 15306: heap size 167 MB, throughput 0.99686
Reading from 15306: heap size 171 MB, throughput 0.643675
Reading from 15306: heap size 172 MB, throughput 0.983818
Reading from 15305: heap size 885 MB, throughput 0.145799
Reading from 15306: heap size 181 MB, throughput 0.995403
Reading from 15305: heap size 974 MB, throughput 0.632729
Reading from 15305: heap size 979 MB, throughput 0.937917
Reading from 15305: heap size 981 MB, throughput 0.912964
Reading from 15305: heap size 962 MB, throughput 0.85035
Reading from 15305: heap size 832 MB, throughput 0.851676
Reading from 15306: heap size 181 MB, throughput 0.998134
Reading from 15305: heap size 949 MB, throughput 0.85909
Reading from 15305: heap size 837 MB, throughput 0.858796
Reading from 15305: heap size 939 MB, throughput 0.864952
Reading from 15305: heap size 835 MB, throughput 0.871889
Reading from 15305: heap size 933 MB, throughput 0.871659
Reading from 15305: heap size 840 MB, throughput 0.881236
Reading from 15306: heap size 193 MB, throughput 0.998489
Reading from 15305: heap size 926 MB, throughput 0.886025
Reading from 15305: heap size 932 MB, throughput 0.979209
Reading from 15306: heap size 196 MB, throughput 0.998246
Equal recommendation: 3669 MB each
Reading from 15305: heap size 930 MB, throughput 0.978676
Reading from 15305: heap size 860 MB, throughput 0.852177
Reading from 15306: heap size 207 MB, throughput 0.998446
Reading from 15305: heap size 924 MB, throughput 0.823429
Reading from 15305: heap size 927 MB, throughput 0.809263
Reading from 15305: heap size 924 MB, throughput 0.830181
Reading from 15305: heap size 926 MB, throughput 0.832099
Reading from 15305: heap size 924 MB, throughput 0.835681
Reading from 15305: heap size 926 MB, throughput 0.771531
Reading from 15305: heap size 925 MB, throughput 0.845709
Reading from 15306: heap size 207 MB, throughput 0.998491
Reading from 15305: heap size 928 MB, throughput 0.847246
Reading from 15305: heap size 927 MB, throughput 0.9013
Reading from 15305: heap size 930 MB, throughput 0.906916
Reading from 15305: heap size 931 MB, throughput 0.780184
Reading from 15305: heap size 933 MB, throughput 0.71285
Reading from 15306: heap size 215 MB, throughput 0.997942
Reading from 15306: heap size 217 MB, throughput 0.997041
Reading from 15305: heap size 920 MB, throughput 0.0560456
Reading from 15305: heap size 1048 MB, throughput 0.551075
Reading from 15305: heap size 1045 MB, throughput 0.763527
Reading from 15305: heap size 1049 MB, throughput 0.732564
Reading from 15305: heap size 1049 MB, throughput 0.750971
Reading from 15305: heap size 1052 MB, throughput 0.740528
Reading from 15306: heap size 225 MB, throughput 0.998662
Reading from 15305: heap size 1056 MB, throughput 0.743313
Reading from 15305: heap size 1058 MB, throughput 0.733921
Reading from 15305: heap size 1070 MB, throughput 0.742699
Reading from 15305: heap size 1070 MB, throughput 0.708405
Reading from 15305: heap size 1084 MB, throughput 0.928148
Reading from 15306: heap size 225 MB, throughput 0.998172
Reading from 15306: heap size 233 MB, throughput 0.998448
Reading from 15305: heap size 1085 MB, throughput 0.968785
Equal recommendation: 3669 MB each
Reading from 15306: heap size 233 MB, throughput 0.998235
Reading from 15305: heap size 1101 MB, throughput 0.966365
Reading from 15306: heap size 241 MB, throughput 0.996805
Reading from 15306: heap size 241 MB, throughput 0.990305
Reading from 15305: heap size 1104 MB, throughput 0.960863
Reading from 15306: heap size 248 MB, throughput 0.998294
Reading from 15305: heap size 1111 MB, throughput 0.966593
Reading from 15306: heap size 250 MB, throughput 0.998025
Reading from 15305: heap size 1113 MB, throughput 0.962469
Reading from 15306: heap size 258 MB, throughput 0.998313
Reading from 15305: heap size 1109 MB, throughput 0.960841
Reading from 15306: heap size 260 MB, throughput 0.998111
Equal recommendation: 3669 MB each
Reading from 15305: heap size 1114 MB, throughput 0.961384
Reading from 15306: heap size 269 MB, throughput 0.998302
Reading from 15305: heap size 1109 MB, throughput 0.962698
Reading from 15306: heap size 269 MB, throughput 0.998172
Reading from 15305: heap size 1113 MB, throughput 0.96218
Reading from 15306: heap size 277 MB, throughput 0.998228
Reading from 15305: heap size 1117 MB, throughput 0.963749
Reading from 15306: heap size 278 MB, throughput 0.998109
Reading from 15305: heap size 1117 MB, throughput 0.963137
Reading from 15306: heap size 286 MB, throughput 0.998115
Reading from 15305: heap size 1122 MB, throughput 0.951604
Reading from 15306: heap size 286 MB, throughput 0.99284
Equal recommendation: 3669 MB each
Reading from 15306: heap size 294 MB, throughput 0.996176
Reading from 15305: heap size 1124 MB, throughput 0.961298
Reading from 15306: heap size 294 MB, throughput 0.997901
Reading from 15305: heap size 1129 MB, throughput 0.961838
Reading from 15306: heap size 306 MB, throughput 0.998228
Reading from 15305: heap size 1132 MB, throughput 0.962961
Reading from 15306: heap size 307 MB, throughput 0.998085
Reading from 15305: heap size 1136 MB, throughput 0.962519
Reading from 15306: heap size 317 MB, throughput 0.998118
Reading from 15305: heap size 1140 MB, throughput 0.96174
Equal recommendation: 3669 MB each
Reading from 15306: heap size 318 MB, throughput 0.998207
Reading from 15305: heap size 1146 MB, throughput 0.957267
Reading from 15306: heap size 327 MB, throughput 0.998374
Reading from 15305: heap size 1149 MB, throughput 0.959636
Reading from 15306: heap size 328 MB, throughput 0.998152
Reading from 15305: heap size 1155 MB, throughput 0.960844
Reading from 15306: heap size 336 MB, throughput 0.997588
Reading from 15305: heap size 1156 MB, throughput 0.956954
Reading from 15306: heap size 337 MB, throughput 0.93995
Reading from 15305: heap size 1161 MB, throughput 0.961997
Equal recommendation: 3669 MB each
Reading from 15306: heap size 351 MB, throughput 0.9993
Reading from 15305: heap size 1162 MB, throughput 0.961804
Reading from 15306: heap size 331 MB, throughput 0.999319
Reading from 15305: heap size 1168 MB, throughput 0.963563
Reading from 15306: heap size 316 MB, throughput 0.999281
Reading from 15305: heap size 1168 MB, throughput 0.9631
Reading from 15306: heap size 306 MB, throughput 0.999201
Reading from 15306: heap size 288 MB, throughput 0.999199
Equal recommendation: 3669 MB each
Reading from 15306: heap size 280 MB, throughput 0.999072
Reading from 15305: heap size 1173 MB, throughput 0.52896
Reading from 15306: heap size 262 MB, throughput 0.999151
Reading from 15305: heap size 1274 MB, throughput 0.942819
Reading from 15306: heap size 256 MB, throughput 0.998913
Reading from 15306: heap size 240 MB, throughput 0.993505
Reading from 15306: heap size 234 MB, throughput 0.996953
Reading from 15305: heap size 1270 MB, throughput 0.985875
Reading from 15306: heap size 222 MB, throughput 0.998358
Reading from 15305: heap size 1279 MB, throughput 0.985112
Reading from 15306: heap size 212 MB, throughput 0.99815
Reading from 15306: heap size 203 MB, throughput 0.998048
Reading from 15305: heap size 1288 MB, throughput 0.979493
Equal recommendation: 3669 MB each
Reading from 15306: heap size 194 MB, throughput 0.998075
Reading from 15306: heap size 186 MB, throughput 0.997879
Reading from 15305: heap size 1290 MB, throughput 0.981123
Reading from 15306: heap size 178 MB, throughput 0.997795
Reading from 15306: heap size 171 MB, throughput 0.997309
Reading from 15305: heap size 1286 MB, throughput 0.972785
Reading from 15306: heap size 164 MB, throughput 0.997266
Reading from 15306: heap size 157 MB, throughput 0.996446
Reading from 15306: heap size 151 MB, throughput 0.996905
Reading from 15305: heap size 1195 MB, throughput 0.975422
Reading from 15306: heap size 145 MB, throughput 0.996772
Reading from 15306: heap size 139 MB, throughput 0.996581
Reading from 15306: heap size 133 MB, throughput 0.996456
Reading from 15305: heap size 1277 MB, throughput 0.969001
Reading from 15306: heap size 128 MB, throughput 0.996651
Reading from 15306: heap size 122 MB, throughput 0.997694
Equal recommendation: 3669 MB each
Reading from 15306: heap size 117 MB, throughput 0.995674
Reading from 15306: heap size 125 MB, throughput 0.987264
Reading from 15306: heap size 134 MB, throughput 0.986526
Reading from 15305: heap size 1213 MB, throughput 0.974718
Reading from 15306: heap size 146 MB, throughput 0.99623
Reading from 15306: heap size 158 MB, throughput 0.99774
Reading from 15306: heap size 169 MB, throughput 0.997682
Reading from 15305: heap size 1273 MB, throughput 0.967652
Reading from 15306: heap size 180 MB, throughput 0.997946
Reading from 15306: heap size 192 MB, throughput 0.997909
Reading from 15305: heap size 1277 MB, throughput 0.968705
Reading from 15306: heap size 203 MB, throughput 0.99814
Reading from 15306: heap size 213 MB, throughput 0.998209
Reading from 15305: heap size 1280 MB, throughput 0.966736
Reading from 15306: heap size 204 MB, throughput 0.997965
Equal recommendation: 3669 MB each
Reading from 15306: heap size 196 MB, throughput 0.997705
Reading from 15305: heap size 1281 MB, throughput 0.966648
Reading from 15306: heap size 187 MB, throughput 0.997742
Reading from 15306: heap size 179 MB, throughput 0.997403
Reading from 15305: heap size 1284 MB, throughput 0.96512
Reading from 15306: heap size 172 MB, throughput 0.997485
Reading from 15306: heap size 165 MB, throughput 0.99696
Reading from 15306: heap size 157 MB, throughput 0.997583
Reading from 15305: heap size 1289 MB, throughput 0.964419
Reading from 15306: heap size 150 MB, throughput 0.998069
Reading from 15306: heap size 144 MB, throughput 0.979067
Reading from 15306: heap size 158 MB, throughput 0.989599
Reading from 15306: heap size 173 MB, throughput 0.899163
Reading from 15305: heap size 1294 MB, throughput 0.960285
Reading from 15306: heap size 188 MB, throughput 0.998837
Equal recommendation: 3669 MB each
Reading from 15306: heap size 202 MB, throughput 0.998878
Reading from 15305: heap size 1301 MB, throughput 0.960329
Reading from 15306: heap size 194 MB, throughput 0.999021
Reading from 15306: heap size 190 MB, throughput 0.998776
Reading from 15305: heap size 1309 MB, throughput 0.961026
Reading from 15306: heap size 178 MB, throughput 0.998857
Reading from 15306: heap size 174 MB, throughput 0.99862
Reading from 15306: heap size 163 MB, throughput 0.998813
Reading from 15305: heap size 1313 MB, throughput 0.962667
Reading from 15306: heap size 161 MB, throughput 0.998652
Reading from 15306: heap size 150 MB, throughput 0.998764
Reading from 15306: heap size 148 MB, throughput 0.998177
Reading from 15305: heap size 1320 MB, throughput 0.958777
Reading from 15306: heap size 139 MB, throughput 0.998492
Equal recommendation: 3669 MB each
Reading from 15306: heap size 137 MB, throughput 0.99833
Reading from 15306: heap size 128 MB, throughput 0.998362
Reading from 15305: heap size 1322 MB, throughput 0.961704
Reading from 15306: heap size 126 MB, throughput 0.998328
Reading from 15306: heap size 119 MB, throughput 0.998276
Reading from 15306: heap size 117 MB, throughput 0.997994
Reading from 15306: heap size 110 MB, throughput 0.998273
Reading from 15306: heap size 109 MB, throughput 0.992032
Reading from 15306: heap size 104 MB, throughput 0.986591
Reading from 15306: heap size 113 MB, throughput 0.984793
Reading from 15306: heap size 123 MB, throughput 0.993467
Reading from 15306: heap size 136 MB, throughput 0.997411
Reading from 15306: heap size 148 MB, throughput 0.997452
Reading from 15306: heap size 160 MB, throughput 0.997765
Reading from 15306: heap size 172 MB, throughput 0.997856
Reading from 15306: heap size 165 MB, throughput 0.997744
Equal recommendation: 3669 MB each
Reading from 15306: heap size 158 MB, throughput 0.997425
Reading from 15306: heap size 152 MB, throughput 0.9971
Reading from 15306: heap size 146 MB, throughput 0.997089
Reading from 15306: heap size 140 MB, throughput 0.996766
Reading from 15306: heap size 135 MB, throughput 0.996797
Reading from 15306: heap size 130 MB, throughput 0.99671
Reading from 15306: heap size 125 MB, throughput 0.996313
Reading from 15306: heap size 120 MB, throughput 0.996665
Reading from 15306: heap size 115 MB, throughput 0.997131
Reading from 15306: heap size 110 MB, throughput 0.997346
Reading from 15306: heap size 106 MB, throughput 0.997773
Reading from 15306: heap size 105 MB, throughput 0.997412
Reading from 15306: heap size 98 MB, throughput 0.997892
Reading from 15306: heap size 98 MB, throughput 0.997766
Reading from 15306: heap size 91 MB, throughput 0.997367
Reading from 15306: heap size 92 MB, throughput 0.99716
Equal recommendation: 3669 MB each
Reading from 15306: heap size 85 MB, throughput 0.993473
Reading from 15306: heap size 85 MB, throughput 0.989698
Reading from 15306: heap size 90 MB, throughput 0.990639
Reading from 15306: heap size 97 MB, throughput 0.985193
Reading from 15306: heap size 107 MB, throughput 0.939552
Reading from 15306: heap size 122 MB, throughput 0.992623
Reading from 15306: heap size 136 MB, throughput 0.997684
Reading from 15305: heap size 1329 MB, throughput 0.876832
Reading from 15306: heap size 153 MB, throughput 0.997332
Reading from 15306: heap size 170 MB, throughput 0.997787
Reading from 15306: heap size 181 MB, throughput 0.997567
Reading from 15306: heap size 191 MB, throughput 0.997373
Reading from 15306: heap size 190 MB, throughput 0.997906
Reading from 15306: heap size 205 MB, throughput 0.998321
Equal recommendation: 3669 MB each
Reading from 15306: heap size 195 MB, throughput 0.998075
Reading from 15306: heap size 187 MB, throughput 0.998343
Reading from 15306: heap size 182 MB, throughput 0.99835
Reading from 15306: heap size 171 MB, throughput 0.998517
Reading from 15306: heap size 164 MB, throughput 0.998291
Reading from 15306: heap size 158 MB, throughput 0.998231
Reading from 15306: heap size 151 MB, throughput 0.998103
Reading from 15305: heap size 1462 MB, throughput 0.985081
Reading from 15306: heap size 145 MB, throughput 0.998233
Reading from 15306: heap size 142 MB, throughput 0.990652
Reading from 15306: heap size 136 MB, throughput 0.989282
Reading from 15306: heap size 148 MB, throughput 0.852413
Reading from 15306: heap size 159 MB, throughput 0.99865
Reading from 15306: heap size 152 MB, throughput 0.998651
Equal recommendation: 3669 MB each
Reading from 15306: heap size 146 MB, throughput 0.998624
Reading from 15305: heap size 1488 MB, throughput 0.983172
Reading from 15305: heap size 1502 MB, throughput 0.840395
Reading from 15306: heap size 145 MB, throughput 0.998399
Reading from 15305: heap size 1496 MB, throughput 0.746251
Reading from 15305: heap size 1513 MB, throughput 0.706349
Reading from 15305: heap size 1535 MB, throughput 0.734021
Reading from 15306: heap size 135 MB, throughput 0.998678
Reading from 15305: heap size 1550 MB, throughput 0.724817
Reading from 15305: heap size 1577 MB, throughput 0.753983
Reading from 15306: heap size 134 MB, throughput 0.99836
Reading from 15305: heap size 1584 MB, throughput 0.740082
Reading from 15306: heap size 125 MB, throughput 0.998479
Reading from 15305: heap size 1615 MB, throughput 0.954991
Reading from 15306: heap size 124 MB, throughput 0.998063
Reading from 15306: heap size 116 MB, throughput 0.99821
Reading from 15306: heap size 115 MB, throughput 0.997776
Reading from 15306: heap size 108 MB, throughput 0.998293
Reading from 15306: heap size 107 MB, throughput 0.997814
Reading from 15305: heap size 1616 MB, throughput 0.973153
Reading from 15306: heap size 101 MB, throughput 0.997823
Reading from 15306: heap size 100 MB, throughput 0.997575
Reading from 15306: heap size 94 MB, throughput 0.997612
Reading from 15306: heap size 93 MB, throughput 0.997145
Reading from 15306: heap size 88 MB, throughput 0.997522
Equal recommendation: 3669 MB each
Reading from 15306: heap size 87 MB, throughput 0.997331
Reading from 15305: heap size 1619 MB, throughput 0.975324
Reading from 15306: heap size 83 MB, throughput 0.997603
Reading from 15306: heap size 82 MB, throughput 0.997035
Reading from 15306: heap size 78 MB, throughput 0.997175
Reading from 15306: heap size 77 MB, throughput 0.996755
Reading from 15306: heap size 73 MB, throughput 0.997079
Reading from 15306: heap size 73 MB, throughput 0.996842
Reading from 15306: heap size 69 MB, throughput 0.996704
Reading from 15306: heap size 68 MB, throughput 0.996385
Reading from 15305: heap size 1629 MB, throughput 0.974467
Reading from 15306: heap size 65 MB, throughput 0.997028
Reading from 15306: heap size 65 MB, throughput 0.995927
Reading from 15306: heap size 62 MB, throughput 0.996019
Reading from 15306: heap size 62 MB, throughput 0.989618
Reading from 15306: heap size 60 MB, throughput 0.978462
Reading from 15306: heap size 66 MB, throughput 0.981497
Reading from 15306: heap size 71 MB, throughput 0.984442
Reading from 15306: heap size 81 MB, throughput 0.983436
Reading from 15306: heap size 90 MB, throughput 0.982262
Reading from 15306: heap size 101 MB, throughput 0.994495
Reading from 15306: heap size 111 MB, throughput 0.997048
Reading from 15305: heap size 1621 MB, throughput 0.97428
Reading from 15306: heap size 126 MB, throughput 0.997459
Reading from 15306: heap size 136 MB, throughput 0.998033
Reading from 15306: heap size 152 MB, throughput 0.998123
Reading from 15306: heap size 143 MB, throughput 0.998492
Reading from 15305: heap size 1632 MB, throughput 0.972735
Reading from 15306: heap size 140 MB, throughput 0.997686
Equal recommendation: 3669 MB each
Reading from 15306: heap size 132 MB, throughput 0.998395
Reading from 15306: heap size 130 MB, throughput 0.997994
Reading from 15306: heap size 123 MB, throughput 0.998273
Reading from 15305: heap size 1625 MB, throughput 0.972289
Reading from 15306: heap size 121 MB, throughput 0.997904
Reading from 15306: heap size 114 MB, throughput 0.998116
Reading from 15306: heap size 112 MB, throughput 0.997798
Reading from 15306: heap size 106 MB, throughput 0.997911
Reading from 15306: heap size 104 MB, throughput 0.997659
Reading from 15306: heap size 99 MB, throughput 0.997776
Reading from 15306: heap size 97 MB, throughput 0.99744
Reading from 15306: heap size 92 MB, throughput 0.997573
Reading from 15305: heap size 1633 MB, throughput 0.785133
Reading from 15306: heap size 91 MB, throughput 0.997476
Reading from 15306: heap size 86 MB, throughput 0.997459
Reading from 15306: heap size 85 MB, throughput 0.996844
Reading from 15306: heap size 81 MB, throughput 0.997135
Reading from 15306: heap size 80 MB, throughput 0.996795
Reading from 15306: heap size 76 MB, throughput 0.996925
Reading from 15306: heap size 75 MB, throughput 0.99648
Reading from 15306: heap size 72 MB, throughput 0.996663
Reading from 15305: heap size 1556 MB, throughput 0.993348
Reading from 15306: heap size 71 MB, throughput 0.996244
Equal recommendation: 3669 MB each
Reading from 15306: heap size 68 MB, throughput 0.996288
Reading from 15306: heap size 67 MB, throughput 0.99612
Reading from 15306: heap size 65 MB, throughput 0.991358
Reading from 15306: heap size 64 MB, throughput 0.983838
Reading from 15306: heap size 69 MB, throughput 0.979858
Reading from 15306: heap size 77 MB, throughput 0.98455
Reading from 15306: heap size 85 MB, throughput 0.981739
Reading from 15306: heap size 96 MB, throughput 0.982679
Reading from 15306: heap size 106 MB, throughput 0.996333
Reading from 15306: heap size 119 MB, throughput 0.996996
Reading from 15305: heap size 1559 MB, throughput 0.991353
Reading from 15306: heap size 126 MB, throughput 0.997321
Reading from 15306: heap size 125 MB, throughput 0.910836
Reading from 15306: heap size 137 MB, throughput 0.997952
Reading from 15306: heap size 130 MB, throughput 0.998203
Reading from 15306: heap size 125 MB, throughput 0.998193
Reading from 15305: heap size 1577 MB, throughput 0.989498
Reading from 15306: heap size 124 MB, throughput 0.998068
Reading from 15306: heap size 116 MB, throughput 0.99806
Reading from 15306: heap size 115 MB, throughput 0.997835
Reading from 15306: heap size 108 MB, throughput 0.998109
Reading from 15306: heap size 107 MB, throughput 0.997729
Reading from 15306: heap size 101 MB, throughput 0.998061
Reading from 15305: heap size 1578 MB, throughput 0.984447
Equal recommendation: 3669 MB each
Reading from 15306: heap size 100 MB, throughput 0.99782
Reading from 15306: heap size 95 MB, throughput 0.997838
Reading from 15306: heap size 94 MB, throughput 0.997229
Reading from 15306: heap size 89 MB, throughput 0.997446
Reading from 15306: heap size 88 MB, throughput 0.997144
Reading from 15306: heap size 84 MB, throughput 0.997484
Reading from 15306: heap size 83 MB, throughput 0.996775
Reading from 15306: heap size 79 MB, throughput 0.996576
Reading from 15305: heap size 1570 MB, throughput 0.985677
Reading from 15306: heap size 78 MB, throughput 0.996827
Reading from 15306: heap size 75 MB, throughput 0.99691
Reading from 15306: heap size 74 MB, throughput 0.996601
Reading from 15306: heap size 71 MB, throughput 0.996592
Reading from 15306: heap size 70 MB, throughput 0.99611
Reading from 15306: heap size 67 MB, throughput 0.996434
Reading from 15306: heap size 67 MB, throughput 0.996462
Reading from 15306: heap size 64 MB, throughput 0.99642
Reading from 15306: heap size 64 MB, throughput 0.995831
Reading from 15306: heap size 61 MB, throughput 0.995716
Reading from 15306: heap size 60 MB, throughput 0.995309
Reading from 15306: heap size 58 MB, throughput 0.995785
Reading from 15305: heap size 1383 MB, throughput 0.984318
Reading from 15306: heap size 58 MB, throughput 0.993822
Reading from 15306: heap size 57 MB, throughput 0.975335
Reading from 15306: heap size 62 MB, throughput 0.979922
Reading from 15306: heap size 68 MB, throughput 0.978823
Reading from 15306: heap size 76 MB, throughput 0.965303
Reading from 15306: heap size 85 MB, throughput 0.983042
Reading from 15306: heap size 98 MB, throughput 0.982199
Reading from 15306: heap size 109 MB, throughput 0.995526
Reading from 15306: heap size 111 MB, throughput 0.995039
Reading from 15306: heap size 125 MB, throughput 0.996508
Equal recommendation: 3669 MB each
Reading from 15306: heap size 125 MB, throughput 0.996882
Reading from 15305: heap size 1553 MB, throughput 0.9815
Reading from 15306: heap size 137 MB, throughput 0.998078
Reading from 15306: heap size 138 MB, throughput 0.997756
Reading from 15306: heap size 130 MB, throughput 0.998053
Reading from 15306: heap size 129 MB, throughput 0.997656
Reading from 15306: heap size 121 MB, throughput 0.997985
Reading from 15306: heap size 119 MB, throughput 0.997482
Reading from 15305: heap size 1413 MB, throughput 0.979742
Reading from 15306: heap size 112 MB, throughput 0.99803
Reading from 15306: heap size 111 MB, throughput 0.997845
Reading from 15306: heap size 105 MB, throughput 0.997463
Reading from 15306: heap size 104 MB, throughput 0.997164
Reading from 15306: heap size 98 MB, throughput 0.997246
Reading from 15306: heap size 97 MB, throughput 0.997566
Reading from 15306: heap size 92 MB, throughput 0.997399
Reading from 15305: heap size 1536 MB, throughput 0.97966
Reading from 15306: heap size 91 MB, throughput 0.997537
Reading from 15306: heap size 86 MB, throughput 0.997424
Reading from 15306: heap size 86 MB, throughput 0.997085
Equal recommendation: 3669 MB each
Reading from 15306: heap size 81 MB, throughput 0.997046
Reading from 15306: heap size 80 MB, throughput 0.997047
Reading from 15306: heap size 77 MB, throughput 0.997166
Reading from 15306: heap size 76 MB, throughput 0.996465
Reading from 15306: heap size 73 MB, throughput 0.996768
Reading from 15306: heap size 72 MB, throughput 0.996569
Reading from 15305: heap size 1548 MB, throughput 0.975331
Reading from 15306: heap size 69 MB, throughput 0.997125
Reading from 15306: heap size 68 MB, throughput 0.996292
Reading from 15306: heap size 66 MB, throughput 0.996209
Reading from 15306: heap size 65 MB, throughput 0.995863
Reading from 15306: heap size 63 MB, throughput 0.995654
Reading from 15306: heap size 62 MB, throughput 0.994502
Reading from 15306: heap size 60 MB, throughput 0.977388
Reading from 15306: heap size 66 MB, throughput 0.982653
Reading from 15306: heap size 73 MB, throughput 0.976684
Reading from 15306: heap size 82 MB, throughput 0.98301
Reading from 15306: heap size 91 MB, throughput 0.982225
Reading from 15306: heap size 104 MB, throughput 0.752827
Reading from 15306: heap size 117 MB, throughput 0.998148
Reading from 15306: heap size 123 MB, throughput 0.997591
Reading from 15305: heap size 1545 MB, throughput 0.977037
Reading from 15306: heap size 134 MB, throughput 0.998041
Reading from 15306: heap size 134 MB, throughput 0.997883
Reading from 15306: heap size 126 MB, throughput 0.997884
Reading from 15306: heap size 124 MB, throughput 0.997542
Reading from 15306: heap size 117 MB, throughput 0.997731
Reading from 15306: heap size 116 MB, throughput 0.998102
Reading from 15305: heap size 1547 MB, throughput 0.971458
Equal recommendation: 3669 MB each
Reading from 15306: heap size 109 MB, throughput 0.997502
Reading from 15306: heap size 108 MB, throughput 0.997425
Reading from 15306: heap size 102 MB, throughput 0.998133
Reading from 15306: heap size 101 MB, throughput 0.997271
Reading from 15306: heap size 96 MB, throughput 0.997803
Reading from 15306: heap size 95 MB, throughput 0.997271
Reading from 15306: heap size 90 MB, throughput 0.997038
Reading from 15305: heap size 1554 MB, throughput 0.972944
Reading from 15306: heap size 89 MB, throughput 0.997438
Reading from 15306: heap size 85 MB, throughput 0.997166
Reading from 15306: heap size 84 MB, throughput 0.996715
Reading from 15306: heap size 80 MB, throughput 0.997034
Reading from 15306: heap size 79 MB, throughput 0.996527
Reading from 15306: heap size 76 MB, throughput 0.996387
Reading from 15306: heap size 75 MB, throughput 0.99647
Reading from 15306: heap size 72 MB, throughput 0.996369
Reading from 15306: heap size 71 MB, throughput 0.995975
Reading from 15306: heap size 68 MB, throughput 0.996254
Reading from 15306: heap size 68 MB, throughput 0.995362
Reading from 15305: heap size 1558 MB, throughput 0.967784
Reading from 15306: heap size 65 MB, throughput 0.996211
Reading from 15306: heap size 64 MB, throughput 0.995936
Reading from 15306: heap size 62 MB, throughput 0.996199
Reading from 15306: heap size 61 MB, throughput 0.995946
Reading from 15306: heap size 59 MB, throughput 0.995664
Reading from 15306: heap size 59 MB, throughput 0.995042
Reading from 15306: heap size 57 MB, throughput 0.995571
Reading from 15306: heap size 57 MB, throughput 0.994915
Reading from 15306: heap size 55 MB, throughput 0.995179
Equal recommendation: 3669 MB each
Reading from 15306: heap size 55 MB, throughput 0.994548
Reading from 15306: heap size 53 MB, throughput 0.993675
Reading from 15305: heap size 1566 MB, throughput 0.972068
Client 15306 died
Clients: 1
Reading from 15305: heap size 1575 MB, throughput 0.968597
Reading from 15305: heap size 1589 MB, throughput 0.968906
Recommendation: one client; give it all the memory
Reading from 15305: heap size 1597 MB, throughput 0.968168
Reading from 15305: heap size 1611 MB, throughput 0.96865
Reading from 15305: heap size 1616 MB, throughput 0.968065
Recommendation: one client; give it all the memory
Reading from 15305: heap size 1630 MB, throughput 0.969358
Reading from 15305: heap size 1632 MB, throughput 0.968
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 15305: heap size 1646 MB, throughput 0.996754
Recommendation: one client; give it all the memory
Reading from 15305: heap size 1647 MB, throughput 0.986631
Reading from 15305: heap size 1638 MB, throughput 0.869847
Reading from 15305: heap size 1649 MB, throughput 0.762116
Reading from 15305: heap size 1676 MB, throughput 0.796588
Reading from 15305: heap size 1697 MB, throughput 0.78607
Client 15305 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
