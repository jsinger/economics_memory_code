#!/usr/bin/ruby -w

# Jeremy Singer
# 10 Feb 14

if ARGV.empty?
  puts "Usage: count_syscalls_strace.rb STRACE_FILE"
  exit 1
end

file = ARGV[0]

#puts "parsing #{file}"

syscalls = Hash.new # index on syscall, value is numcalls

File.readlines(file).each do |line|
  if line =~ /resumed/ then
    next
  end
  # when run with strace -ttt then each syscall dump line
  # has timestamp of form NNNN.NNNN
  if line =~ /[0-9]+\.[0-9]+/ then
    # first '(' char is syscall invocation
    syscall = (line.split(/\(/).first).split(/ /).last
    if syscall =~ /</ then
      next
    end
    #puts "found syscall #{syscall}"
    if syscalls.has_key?(syscall)
      syscalls[syscall] = syscalls[syscall] + 1
    else
      syscalls[syscall] = 1
    end
  end
end

syscalls.each do |key, value|
  puts "#{key} : #{value}"
end
