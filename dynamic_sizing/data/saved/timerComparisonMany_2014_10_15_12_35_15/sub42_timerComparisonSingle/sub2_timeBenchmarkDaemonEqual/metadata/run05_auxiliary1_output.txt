economemd
    total memory: 1656 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub42_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run05_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 14220: heap size 9 MB, throughput 0.991259
Clients: 1
Client 14220 has a minimum heap size of 276 MB
Reading from 14218: heap size 9 MB, throughput 0.989954
Clients: 2
Client 14218 has a minimum heap size of 276 MB
Reading from 14218: heap size 9 MB, throughput 0.976751
Reading from 14220: heap size 9 MB, throughput 0.980466
Reading from 14220: heap size 9 MB, throughput 0.959436
Reading from 14218: heap size 9 MB, throughput 0.960281
Reading from 14220: heap size 9 MB, throughput 0.950765
Reading from 14218: heap size 9 MB, throughput 0.951085
Reading from 14220: heap size 11 MB, throughput 0.979989
Reading from 14218: heap size 11 MB, throughput 0.972747
Reading from 14220: heap size 11 MB, throughput 0.966653
Reading from 14218: heap size 11 MB, throughput 0.977901
Reading from 14220: heap size 17 MB, throughput 0.919772
Reading from 14218: heap size 17 MB, throughput 0.801332
Reading from 14220: heap size 17 MB, throughput 0.800836
Reading from 14218: heap size 17 MB, throughput 0.576052
Reading from 14220: heap size 30 MB, throughput 0.927426
Reading from 14218: heap size 30 MB, throughput 0.971871
Reading from 14218: heap size 31 MB, throughput 0.8298
Reading from 14220: heap size 31 MB, throughput 0.863183
Reading from 14220: heap size 35 MB, throughput 0.417454
Reading from 14218: heap size 36 MB, throughput 0.375236
Reading from 14220: heap size 47 MB, throughput 0.932738
Reading from 14218: heap size 47 MB, throughput 0.84217
Reading from 14220: heap size 50 MB, throughput 0.345999
Reading from 14218: heap size 50 MB, throughput 0.392146
Reading from 14220: heap size 64 MB, throughput 0.80743
Reading from 14218: heap size 65 MB, throughput 0.823827
Reading from 14220: heap size 68 MB, throughput 0.871243
Reading from 14218: heap size 72 MB, throughput 0.258051
Reading from 14220: heap size 71 MB, throughput 0.239314
Reading from 14218: heap size 89 MB, throughput 0.863073
Reading from 14220: heap size 101 MB, throughput 0.699315
Reading from 14218: heap size 97 MB, throughput 0.864759
Reading from 14220: heap size 101 MB, throughput 0.26089
Reading from 14220: heap size 133 MB, throughput 0.836868
Reading from 14220: heap size 133 MB, throughput 0.847723
Reading from 14218: heap size 98 MB, throughput 0.307814
Reading from 14220: heap size 135 MB, throughput 0.64653
Reading from 14218: heap size 132 MB, throughput 0.761047
Reading from 14220: heap size 139 MB, throughput 0.626418
Reading from 14218: heap size 132 MB, throughput 0.267606
Reading from 14218: heap size 171 MB, throughput 0.647071
Reading from 14218: heap size 171 MB, throughput 0.803886
Reading from 14220: heap size 142 MB, throughput 0.153079
Reading from 14218: heap size 172 MB, throughput 0.593311
Reading from 14220: heap size 181 MB, throughput 0.559402
Reading from 14220: heap size 185 MB, throughput 0.735578
Reading from 14218: heap size 175 MB, throughput 0.664731
Reading from 14220: heap size 189 MB, throughput 0.170094
Reading from 14218: heap size 178 MB, throughput 0.173177
Reading from 14218: heap size 223 MB, throughput 0.518507
Reading from 14220: heap size 236 MB, throughput 0.857234
Reading from 14220: heap size 238 MB, throughput 0.932538
Reading from 14218: heap size 227 MB, throughput 0.936565
Reading from 14220: heap size 240 MB, throughput 0.861928
Reading from 14220: heap size 245 MB, throughput 0.759662
Reading from 14218: heap size 230 MB, throughput 0.863834
Reading from 14220: heap size 249 MB, throughput 0.561846
Reading from 14218: heap size 243 MB, throughput 0.871402
Reading from 14220: heap size 254 MB, throughput 0.814414
Reading from 14218: heap size 243 MB, throughput 0.869919
Reading from 14220: heap size 261 MB, throughput 0.716337
Reading from 14218: heap size 248 MB, throughput 0.900876
Reading from 14220: heap size 264 MB, throughput 0.809874
Reading from 14218: heap size 248 MB, throughput 0.787876
Reading from 14220: heap size 266 MB, throughput 0.628904
Reading from 14220: heap size 272 MB, throughput 0.678392
Reading from 14218: heap size 253 MB, throughput 0.744725
Reading from 14220: heap size 278 MB, throughput 0.682802
Reading from 14218: heap size 253 MB, throughput 0.727787
Reading from 14220: heap size 282 MB, throughput 0.535613
Reading from 14218: heap size 259 MB, throughput 0.789241
Reading from 14218: heap size 263 MB, throughput 0.78906
Reading from 14220: heap size 288 MB, throughput 0.920608
Reading from 14220: heap size 290 MB, throughput 0.868376
Reading from 14220: heap size 294 MB, throughput 0.564685
Reading from 14218: heap size 270 MB, throughput 0.941405
Reading from 14218: heap size 271 MB, throughput 0.68012
Reading from 14218: heap size 278 MB, throughput 0.722958
Reading from 14218: heap size 281 MB, throughput 0.600339
Reading from 14220: heap size 295 MB, throughput 0.0966285
Reading from 14220: heap size 334 MB, throughput 0.691865
Reading from 14220: heap size 337 MB, throughput 0.927644
Reading from 14218: heap size 288 MB, throughput 0.207481
Reading from 14220: heap size 343 MB, throughput 0.90263
Reading from 14220: heap size 344 MB, throughput 0.75849
Reading from 14218: heap size 330 MB, throughput 0.786946
Reading from 14220: heap size 343 MB, throughput 0.795208
Reading from 14218: heap size 333 MB, throughput 0.834425
Reading from 14220: heap size 346 MB, throughput 0.683351
Reading from 14218: heap size 334 MB, throughput 0.659285
Reading from 14220: heap size 348 MB, throughput 0.701833
Reading from 14218: heap size 340 MB, throughput 0.773492
Reading from 14220: heap size 350 MB, throughput 0.711378
Reading from 14218: heap size 340 MB, throughput 0.737105
Reading from 14218: heap size 348 MB, throughput 0.632512
Reading from 14220: heap size 356 MB, throughput 0.9457
Equal recommendation: 828 MB each
Reading from 14218: heap size 348 MB, throughput 0.966563
Reading from 14220: heap size 357 MB, throughput 0.979825
Reading from 14218: heap size 356 MB, throughput 0.974989
Reading from 14220: heap size 359 MB, throughput 0.959346
Reading from 14218: heap size 358 MB, throughput 0.959816
Reading from 14220: heap size 362 MB, throughput 0.94739
Reading from 14220: heap size 360 MB, throughput 0.970425
Reading from 14218: heap size 359 MB, throughput 0.978025
Reading from 14220: heap size 363 MB, throughput 0.977835
Reading from 14218: heap size 362 MB, throughput 0.977948
Reading from 14220: heap size 362 MB, throughput 0.978145
Reading from 14218: heap size 362 MB, throughput 0.975665
Reading from 14220: heap size 365 MB, throughput 0.965442
Equal recommendation: 828 MB each
Reading from 14218: heap size 364 MB, throughput 0.979936
Reading from 14220: heap size 362 MB, throughput 0.973461
Reading from 14218: heap size 363 MB, throughput 0.974194
Reading from 14220: heap size 364 MB, throughput 0.971225
Reading from 14218: heap size 365 MB, throughput 0.969808
Reading from 14220: heap size 366 MB, throughput 0.97005
Reading from 14218: heap size 368 MB, throughput 0.971675
Reading from 14220: heap size 366 MB, throughput 0.978739
Reading from 14218: heap size 369 MB, throughput 0.971975
Reading from 14220: heap size 369 MB, throughput 0.971812
Reading from 14218: heap size 372 MB, throughput 0.978601
Reading from 14220: heap size 370 MB, throughput 0.970746
Reading from 14218: heap size 374 MB, throughput 0.967923
Equal recommendation: 828 MB each
Reading from 14220: heap size 368 MB, throughput 0.979653
Reading from 14220: heap size 374 MB, throughput 0.190965
Reading from 14220: heap size 409 MB, throughput 0.71029
Reading from 14218: heap size 372 MB, throughput 0.979688
Reading from 14220: heap size 413 MB, throughput 0.845766
Reading from 14220: heap size 419 MB, throughput 0.778676
Reading from 14218: heap size 379 MB, throughput 0.742325
Reading from 14218: heap size 376 MB, throughput 0.144249
Reading from 14218: heap size 420 MB, throughput 0.728227
Reading from 14218: heap size 429 MB, throughput 0.87739
Reading from 14220: heap size 420 MB, throughput 0.981854
Reading from 14220: heap size 426 MB, throughput 0.974217
Reading from 14218: heap size 430 MB, throughput 0.987021
Reading from 14220: heap size 427 MB, throughput 0.987648
Reading from 14218: heap size 434 MB, throughput 0.986822
Reading from 14220: heap size 424 MB, throughput 0.980314
Reading from 14218: heap size 437 MB, throughput 0.985754
Reading from 14220: heap size 428 MB, throughput 0.97979
Reading from 14218: heap size 435 MB, throughput 0.982894
Equal recommendation: 828 MB each
Reading from 14220: heap size 425 MB, throughput 0.982863
Reading from 14218: heap size 438 MB, throughput 0.984739
Reading from 14220: heap size 428 MB, throughput 0.981762
Reading from 14218: heap size 435 MB, throughput 0.984051
Reading from 14220: heap size 430 MB, throughput 0.981756
Reading from 14218: heap size 438 MB, throughput 0.977586
Reading from 14220: heap size 431 MB, throughput 0.981209
Reading from 14218: heap size 441 MB, throughput 0.97697
Reading from 14220: heap size 434 MB, throughput 0.971696
Reading from 14218: heap size 442 MB, throughput 0.972614
Reading from 14220: heap size 437 MB, throughput 0.973443
Equal recommendation: 828 MB each
Reading from 14218: heap size 445 MB, throughput 0.971503
Reading from 14220: heap size 440 MB, throughput 0.982797
Reading from 14220: heap size 443 MB, throughput 0.870089
Reading from 14220: heap size 442 MB, throughput 0.815639
Reading from 14220: heap size 444 MB, throughput 0.807669
Reading from 14220: heap size 452 MB, throughput 0.965017
Reading from 14218: heap size 448 MB, throughput 0.973057
Reading from 14218: heap size 454 MB, throughput 0.823187
Reading from 14218: heap size 455 MB, throughput 0.80864
Reading from 14218: heap size 462 MB, throughput 0.842592
Reading from 14220: heap size 453 MB, throughput 0.980206
Reading from 14218: heap size 462 MB, throughput 0.98525
Reading from 14220: heap size 455 MB, throughput 0.986593
Reading from 14218: heap size 468 MB, throughput 0.986621
Reading from 14220: heap size 458 MB, throughput 0.991011
Reading from 14218: heap size 470 MB, throughput 0.989627
Equal recommendation: 828 MB each
Reading from 14220: heap size 457 MB, throughput 0.985923
Reading from 14218: heap size 471 MB, throughput 0.986993
Reading from 14220: heap size 460 MB, throughput 0.985371
Reading from 14218: heap size 474 MB, throughput 0.987094
Reading from 14220: heap size 457 MB, throughput 0.982914
Reading from 14218: heap size 473 MB, throughput 0.982933
Reading from 14220: heap size 460 MB, throughput 0.982436
Reading from 14218: heap size 476 MB, throughput 0.983348
Reading from 14220: heap size 457 MB, throughput 0.986904
Reading from 14218: heap size 472 MB, throughput 0.983233
Equal recommendation: 828 MB each
Reading from 14220: heap size 459 MB, throughput 0.988652
Reading from 14220: heap size 458 MB, throughput 0.964245
Reading from 14220: heap size 460 MB, throughput 0.875905
Reading from 14220: heap size 464 MB, throughput 0.851733
Reading from 14218: heap size 475 MB, throughput 0.991962
Reading from 14218: heap size 474 MB, throughput 0.966094
Reading from 14218: heap size 476 MB, throughput 0.893825
Reading from 14218: heap size 480 MB, throughput 0.892947
Reading from 14220: heap size 464 MB, throughput 0.989725
Reading from 14218: heap size 480 MB, throughput 0.990873
Reading from 14220: heap size 471 MB, throughput 0.98945
Reading from 14218: heap size 486 MB, throughput 0.991548
Reading from 14220: heap size 472 MB, throughput 0.989728
Equal recommendation: 828 MB each
Reading from 14218: heap size 487 MB, throughput 0.992325
Reading from 14220: heap size 475 MB, throughput 0.986018
Reading from 14218: heap size 489 MB, throughput 0.989781
Reading from 14220: heap size 477 MB, throughput 0.988073
Reading from 14218: heap size 491 MB, throughput 0.986438
Reading from 14220: heap size 475 MB, throughput 0.988029
Reading from 14218: heap size 489 MB, throughput 0.987827
Reading from 14220: heap size 478 MB, throughput 0.987409
Equal recommendation: 828 MB each
Reading from 14218: heap size 491 MB, throughput 0.980031
Reading from 14220: heap size 478 MB, throughput 0.971862
Reading from 14220: heap size 479 MB, throughput 0.989786
Reading from 14218: heap size 494 MB, throughput 0.990146
Reading from 14220: heap size 482 MB, throughput 0.901445
Reading from 14218: heap size 494 MB, throughput 0.913464
Reading from 14220: heap size 482 MB, throughput 0.876736
Reading from 14218: heap size 494 MB, throughput 0.86895
Reading from 14220: heap size 489 MB, throughput 0.990156
Reading from 14218: heap size 496 MB, throughput 0.989757
Reading from 14220: heap size 490 MB, throughput 0.992738
Reading from 14218: heap size 504 MB, throughput 0.991207
Reading from 14220: heap size 494 MB, throughput 0.990174
Reading from 14218: heap size 505 MB, throughput 0.989503
Equal recommendation: 828 MB each
Reading from 14220: heap size 496 MB, throughput 0.989977
Reading from 14218: heap size 507 MB, throughput 0.990528
Reading from 14220: heap size 494 MB, throughput 0.988497
Reading from 14218: heap size 509 MB, throughput 0.987459
Reading from 14220: heap size 497 MB, throughput 0.990583
Reading from 14218: heap size 509 MB, throughput 0.988979
Reading from 14220: heap size 498 MB, throughput 0.988307
Equal recommendation: 828 MB each
Reading from 14218: heap size 510 MB, throughput 0.986322
Reading from 14220: heap size 499 MB, throughput 0.992517
Reading from 14220: heap size 500 MB, throughput 0.930486
Reading from 14218: heap size 513 MB, throughput 0.987371
Reading from 14220: heap size 501 MB, throughput 0.889875
Reading from 14218: heap size 514 MB, throughput 0.897833
Reading from 14218: heap size 518 MB, throughput 0.971867
Reading from 14220: heap size 507 MB, throughput 0.989009
Reading from 14218: heap size 519 MB, throughput 0.992271
Reading from 14220: heap size 509 MB, throughput 0.992096
Reading from 14218: heap size 523 MB, throughput 0.992336
Equal recommendation: 828 MB each
Reading from 14220: heap size 512 MB, throughput 0.991184
Reading from 14218: heap size 525 MB, throughput 0.991249
Reading from 14220: heap size 514 MB, throughput 0.991824
Reading from 14218: heap size 524 MB, throughput 0.989901
Reading from 14220: heap size 512 MB, throughput 0.991801
Reading from 14220: heap size 515 MB, throughput 0.987975
Reading from 14218: heap size 526 MB, throughput 0.989833
Equal recommendation: 828 MB each
Reading from 14220: heap size 517 MB, throughput 0.987855
Reading from 14218: heap size 527 MB, throughput 0.987741
Reading from 14220: heap size 517 MB, throughput 0.984413
Reading from 14220: heap size 521 MB, throughput 0.901494
Reading from 14218: heap size 528 MB, throughput 0.985147
Reading from 14218: heap size 530 MB, throughput 0.910637
Reading from 14220: heap size 522 MB, throughput 0.97139
Reading from 14218: heap size 532 MB, throughput 0.980994
Reading from 14220: heap size 530 MB, throughput 0.992351
Reading from 14218: heap size 539 MB, throughput 0.992625
Reading from 14220: heap size 530 MB, throughput 0.993118
Equal recommendation: 828 MB each
Reading from 14218: heap size 540 MB, throughput 0.992212
Reading from 14220: heap size 531 MB, throughput 0.990621
Reading from 14218: heap size 539 MB, throughput 0.990839
Reading from 14220: heap size 533 MB, throughput 0.990059
Reading from 14218: heap size 542 MB, throughput 0.990317
Reading from 14220: heap size 532 MB, throughput 0.989564
Equal recommendation: 828 MB each
Reading from 14218: heap size 542 MB, throughput 0.988758
Reading from 14220: heap size 534 MB, throughput 0.993628
Reading from 14220: heap size 536 MB, throughput 0.974775
Reading from 14220: heap size 536 MB, throughput 0.902684
Reading from 14218: heap size 543 MB, throughput 0.99304
Reading from 14218: heap size 545 MB, throughput 0.917077
Reading from 14220: heap size 541 MB, throughput 0.990751
Reading from 14218: heap size 546 MB, throughput 0.97578
Reading from 14220: heap size 542 MB, throughput 0.992363
Reading from 14218: heap size 554 MB, throughput 0.99304
Equal recommendation: 828 MB each
Reading from 14220: heap size 545 MB, throughput 0.990892
Reading from 14218: heap size 554 MB, throughput 0.994145
Reading from 14220: heap size 547 MB, throughput 0.989627
Reading from 14218: heap size 554 MB, throughput 0.990747
Reading from 14220: heap size 545 MB, throughput 0.989776
Reading from 14218: heap size 556 MB, throughput 0.993733
Reading from 14220: heap size 547 MB, throughput 0.987628
Equal recommendation: 828 MB each
Reading from 14218: heap size 557 MB, throughput 0.912266
Reading from 14220: heap size 550 MB, throughput 0.990611
Reading from 14220: heap size 551 MB, throughput 0.907656
Reading from 14220: heap size 554 MB, throughput 0.989217
Reading from 14218: heap size 573 MB, throughput 0.993083
Reading from 14218: heap size 576 MB, throughput 0.932491
Reading from 14218: heap size 577 MB, throughput 0.987749
Reading from 14220: heap size 556 MB, throughput 0.995151
Reading from 14218: heap size 584 MB, throughput 0.993983
Equal recommendation: 828 MB each
Reading from 14220: heap size 559 MB, throughput 0.931855
Reading from 14218: heap size 585 MB, throughput 0.993673
Reading from 14220: heap size 563 MB, throughput 0.992751
Reading from 14218: heap size 585 MB, throughput 0.991886
Reading from 14220: heap size 564 MB, throughput 0.99436
Equal recommendation: 828 MB each
Reading from 14218: heap size 587 MB, throughput 0.991022
Reading from 14220: heap size 566 MB, throughput 0.992275
Reading from 14220: heap size 566 MB, throughput 0.986603
Reading from 14220: heap size 568 MB, throughput 0.906177
Reading from 14218: heap size 590 MB, throughput 0.992831
Reading from 14220: heap size 573 MB, throughput 0.990165
Reading from 14218: heap size 591 MB, throughput 0.972383
Reading from 14218: heap size 590 MB, throughput 0.933435
Reading from 14220: heap size 576 MB, throughput 0.992141
Reading from 14218: heap size 593 MB, throughput 0.993637
Equal recommendation: 828 MB each
Reading from 14220: heap size 573 MB, throughput 0.993225
Reading from 14218: heap size 596 MB, throughput 0.991221
Reading from 14220: heap size 577 MB, throughput 0.990676
Reading from 14218: heap size 599 MB, throughput 0.990052
Reading from 14220: heap size 581 MB, throughput 0.988585
Equal recommendation: 828 MB each
Reading from 14218: heap size 602 MB, throughput 0.990855
Reading from 14220: heap size 581 MB, throughput 0.994317
Reading from 14220: heap size 583 MB, throughput 0.935073
Reading from 14220: heap size 585 MB, throughput 0.97366
Reading from 14218: heap size 602 MB, throughput 0.994634
Reading from 14218: heap size 606 MB, throughput 0.981033
Reading from 14218: heap size 607 MB, throughput 0.949429
Reading from 14220: heap size 594 MB, throughput 0.992791
Reading from 14218: heap size 616 MB, throughput 0.994416
Equal recommendation: 828 MB each
Reading from 14220: heap size 595 MB, throughput 0.990932
Reading from 14218: heap size 616 MB, throughput 0.991966
Reading from 14220: heap size 596 MB, throughput 0.990488
Reading from 14218: heap size 616 MB, throughput 0.991872
Reading from 14220: heap size 598 MB, throughput 0.991604
Equal recommendation: 828 MB each
Reading from 14218: heap size 618 MB, throughput 0.991698
Reading from 14220: heap size 602 MB, throughput 0.99234
Reading from 14220: heap size 602 MB, throughput 0.966017
Reading from 14220: heap size 604 MB, throughput 0.979235
Reading from 14218: heap size 621 MB, throughput 0.992829
Reading from 14218: heap size 623 MB, throughput 0.933888
Reading from 14218: heap size 623 MB, throughput 0.987765
Reading from 14220: heap size 606 MB, throughput 0.994462
Equal recommendation: 828 MB each
Reading from 14218: heap size 626 MB, throughput 0.993635
Reading from 14220: heap size 608 MB, throughput 0.993061
Reading from 14218: heap size 628 MB, throughput 0.992366
Reading from 14220: heap size 610 MB, throughput 0.992477
Reading from 14218: heap size 630 MB, throughput 0.991372
Equal recommendation: 828 MB each
Reading from 14220: heap size 610 MB, throughput 0.992636
Reading from 14218: heap size 634 MB, throughput 0.991048
Reading from 14220: heap size 611 MB, throughput 0.994604
Reading from 14220: heap size 614 MB, throughput 0.927731
Reading from 14218: heap size 634 MB, throughput 0.98953
Reading from 14218: heap size 638 MB, throughput 0.950481
Reading from 14220: heap size 615 MB, throughput 0.991533
Reading from 14218: heap size 640 MB, throughput 0.994881
Equal recommendation: 828 MB each
Reading from 14220: heap size 622 MB, throughput 0.993202
Reading from 14218: heap size 645 MB, throughput 0.99228
Reading from 14220: heap size 624 MB, throughput 0.992809
Reading from 14218: heap size 646 MB, throughput 0.991264
Reading from 14220: heap size 623 MB, throughput 0.992159
Equal recommendation: 828 MB each
Reading from 14218: heap size 648 MB, throughput 0.991578
Reading from 14220: heap size 626 MB, throughput 0.99115
Reading from 14220: heap size 628 MB, throughput 0.985884
Client 14220 died
Clients: 1
Reading from 14218: heap size 649 MB, throughput 0.992242
Reading from 14218: heap size 652 MB, throughput 0.947209
Client 14218 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
