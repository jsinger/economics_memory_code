{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, DeriveDataTypeable #-}
{-# OPTIONS_GHC -fno-warn-missing-fields -fno-warn-missing-methods #-}

import Data.Maybe
import Economem.ArgHelper
import qualified Economem.BenchmarkTimer as B
import Economem.Experiment
import Economem.Files
import Economem.Java
import Economem.Records
import Economem.Signals
import qualified Economem.TimerComparisonMany as M
import System.Console.CmdLib

data Args = Args { inputFile :: FilePath,
                   runs :: Int,
                   java :: String,
                   minHeapsFile :: FilePath,
                   forceHeapGrowth :: Bool,
                   alwaysNumericOptimisation :: Bool,
                   useUnconstrained :: Bool,
                   useStaticEqual :: Bool,
                   useCgroupSimple :: Bool,
                   useCgroupFair :: Bool,
                   useDaemonEqual :: Bool,
                   useDaemonSimple :: Bool,
                   useDaemonVengerov :: Bool,
                   useDaemonAvgVengerov :: Bool,
                   useDaemonMajorOnly :: Bool }
          deriving (Typeable, Data, Eq, Show)

instance Attributes Args where
  attributes _ = group "Options" [
    inputFile %> [ Positional 0,
                   Required True,
                   Help "Specifies which combinations of benchmarks to run",
                   ArgHelp "INPUT_FILE" ],
    runs %> [ Short ['r'],
              Long ["runs"],
              Default (fromIntegral B.defaultRuns::Int),
              Help "How many times to repeat, for taking the average",
              ArgHelp "RUNS" ],
    java %> [ Short ['j'],
              Long ["java"],
              Default (javaName defaultJava),
              Help ("Java version to use; available options: " ++ show validJavaNames),
              ArgHelp "JAVA" ],
    minHeapsFile %> [ Short ['m'],
                      Long ["min-heaps-file"],
                      Default "",
                      Help "Specify a min heaps file to use",
                      ArgHelp "MIN_HEAPS_FILE" ],
    forceHeapGrowth %> [ Long ["force-growth"],
                         Default False,
                         Help "Force heap growth in the VM"],
    alwaysNumericOptimisation %> [ Long ["always-numeric"],
                                   Default False,
                                   Help "In the daemon, always use numeric optimisation even when analytic is possible"],
    useUnconstrained %> [ Long ["unconstrained"],
                          Default False,
                          Help "Include unconstrained in the comparison"],
    useStaticEqual %> [ Long ["staticEqual"],
                        Default False,
                        Help "Include staticEqual in the comparison"],
    useCgroupSimple %> [ Long ["cgroupSimple"],
                         Default False,
                         Help "Include cgroupSimple in the comparison"],
    useCgroupFair %> [ Long ["cgroupFair"],
                       Default False,
                       Help "Include cgroupFair in the comparison"],
    useDaemonEqual %> [ Long ["daemonEqual"],
                        Default False,
                        Help "Include daemonEqual in the comparison"],
    useDaemonSimple %> [ Long ["daemonSimple"],
                         Default False,
                         Help "Include daemonSimple in the comparison"],
    useDaemonVengerov %> [ Long ["daemonVengerov"],
                           Default False,
                           Help "Include daemonVengerov in the comparison"],
    useDaemonAvgVengerov %> [ Long ["daemonAvgVengerov"],
                              Default False,
                              Help "Include daemonAvgVengerov in the comparison"],
    useDaemonMajorOnly %> [ Long ["daemonMajorOnly"],
                            Default False,
                            Help "Include daemonMajorOnly in the comparison"]]


instance RecordCommand Args where
  mode_summary _ = "Compare various heap sizing methods for many benchmark combinations"

main :: IO ()
main = getArgs >>= executeR Args {} >>= \opts -> do
  (i, r, j, m, timers, forceGrowth, alwaysNumeric) <- validate opts
  case timers of
    [] -> fail "At least one timer must be specified"
    _ -> do d <- M.newTimerComparisonMany i timers r j m forceGrowth alwaysNumeric Nothing []
            h <- makeSignalHandler
            runExperiment d h

  where
    validate :: Args -> IO (Input BenchmarkCombinationMB,
                            Integer,
                            Java,
                            Maybe (Input BenchmarkThreadsMB),
                            [TimerType],
                            Bool,
                            Bool)
    validate a = do
      i <- existingAbsoluteFile (inputFile a)
      r <- positive (runs a)
      j <- javaFromName (java a)
      return (fromFile i, fromIntegral r, j,
              case minHeapsFile a of
               "" -> Nothing
               x -> Just (fromFile x),
              argsToTimers a,
              forceHeapGrowth a,
              alwaysNumericOptimisation a)

argsToTimers :: Args -> [TimerType]
argsToTimers a = catMaybes args
  where
    doArg :: (Args -> Bool) -> TimerType -> Maybe TimerType
    doArg f s = if f a then Just s else Nothing

    args :: [Maybe TimerType]
    args = [doArg useUnconstrained Unconstrained,
            doArg useStaticEqual StaticEqual,
            doArg useCgroupSimple CgroupSimple,
            doArg useCgroupFair CgroupFair,
            doArg useDaemonEqual DaemonEqual,
            doArg useDaemonSimple DaemonSimple,
            doArg useDaemonVengerov DaemonVengerov,
            doArg useDaemonAvgVengerov DaemonAvgVengerov,
            doArg useDaemonMajorOnly DaemonMajorOnly]
