	Command being timed: "cgexec -g memory:economem /home/callum/economics_memory_code/dynamic_sizing/hotspot8/openjdk8/jdk8/build/linux-x86_64-normal-server-release/jdk/bin/java -XX:+EconomemSimpleThroughput -XX:-EconomemVengerovThroughput -XX:EconomemMinHeap=3 -Xms10m -Xmx1214m -XX:+DisableExplicitGC -XX:+UseAdaptiveSizePolicyWithSystemGC -jar /home/callum/economics_memory_code/dynamic_sizing/haskell_common/dacapo-9.12-bach.jar --no-pre-iteration-gc --scratch-directory /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub19_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/scratch2 -t 2 -n 27 -s large xalan"
	User time (seconds): 482.12
	System time (seconds): 159.47
	Percent of CPU this job got: 91%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 11:37.41
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 466216
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 43743
	Minor (reclaiming a frame) page faults: 410596
	Voluntary context switches: 119257
	Involuntary context switches: 125127
	Swaps: 0
	File system inputs: 2720064
	File system outputs: 12808616
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
