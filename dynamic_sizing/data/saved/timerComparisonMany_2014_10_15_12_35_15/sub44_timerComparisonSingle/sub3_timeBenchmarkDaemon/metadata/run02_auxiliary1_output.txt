economemd
    total memory: 2760 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub44_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 21118: heap size 9 MB, throughput 0.993113
Clients: 1
Client 21118 has a minimum heap size of 276 MB
Reading from 21117: heap size 9 MB, throughput 0.992373
Clients: 2
Client 21117 has a minimum heap size of 276 MB
Reading from 21117: heap size 9 MB, throughput 0.987971
Reading from 21118: heap size 9 MB, throughput 0.989691
Reading from 21117: heap size 9 MB, throughput 0.961115
Reading from 21118: heap size 9 MB, throughput 0.982504
Reading from 21117: heap size 9 MB, throughput 0.96086
Reading from 21118: heap size 9 MB, throughput 0.976641
Reading from 21117: heap size 11 MB, throughput 0.974539
Reading from 21118: heap size 11 MB, throughput 0.980004
Reading from 21117: heap size 11 MB, throughput 0.982965
Reading from 21118: heap size 11 MB, throughput 0.979607
Reading from 21117: heap size 17 MB, throughput 0.955118
Reading from 21118: heap size 17 MB, throughput 0.962378
Reading from 21117: heap size 17 MB, throughput 0.873933
Reading from 21118: heap size 17 MB, throughput 0.882113
Reading from 21117: heap size 30 MB, throughput 0.796885
Reading from 21118: heap size 30 MB, throughput 0.825493
Reading from 21118: heap size 31 MB, throughput 0.301242
Reading from 21117: heap size 31 MB, throughput 0.451343
Reading from 21117: heap size 35 MB, throughput 0.765102
Reading from 21117: heap size 47 MB, throughput 0.467127
Reading from 21118: heap size 34 MB, throughput 0.676362
Reading from 21118: heap size 47 MB, throughput 0.672643
Reading from 21118: heap size 49 MB, throughput 0.374224
Reading from 21117: heap size 51 MB, throughput 0.522025
Reading from 21117: heap size 64 MB, throughput 0.428588
Reading from 21118: heap size 50 MB, throughput 0.561312
Reading from 21117: heap size 69 MB, throughput 0.392879
Reading from 21118: heap size 68 MB, throughput 0.483247
Reading from 21118: heap size 70 MB, throughput 0.153212
Reading from 21118: heap size 72 MB, throughput 0.801382
Reading from 21117: heap size 71 MB, throughput 0.527278
Reading from 21117: heap size 100 MB, throughput 0.590147
Reading from 21118: heap size 76 MB, throughput 0.429032
Reading from 21118: heap size 108 MB, throughput 0.256661
Reading from 21117: heap size 100 MB, throughput 0.423607
Reading from 21118: heap size 108 MB, throughput 0.110212
Reading from 21117: heap size 132 MB, throughput 0.62614
Reading from 21118: heap size 111 MB, throughput 0.642974
Reading from 21118: heap size 114 MB, throughput 0.689097
Reading from 21117: heap size 132 MB, throughput 0.612033
Reading from 21118: heap size 121 MB, throughput 0.745418
Reading from 21117: heap size 135 MB, throughput 0.34933
Reading from 21118: heap size 124 MB, throughput 0.733589
Reading from 21117: heap size 138 MB, throughput 0.265274
Reading from 21118: heap size 129 MB, throughput 0.758969
Reading from 21117: heap size 141 MB, throughput 0.45476
Reading from 21118: heap size 135 MB, throughput 0.435543
Reading from 21117: heap size 179 MB, throughput 0.328861
Reading from 21118: heap size 172 MB, throughput 0.713901
Reading from 21117: heap size 183 MB, throughput 0.128599
Reading from 21118: heap size 175 MB, throughput 0.695906
Reading from 21118: heap size 178 MB, throughput 0.616917
Reading from 21117: heap size 186 MB, throughput 0.724269
Reading from 21117: heap size 194 MB, throughput 0.703546
Reading from 21118: heap size 182 MB, throughput 0.610721
Reading from 21118: heap size 188 MB, throughput 0.352625
Reading from 21118: heap size 225 MB, throughput 0.751928
Reading from 21117: heap size 197 MB, throughput 0.580898
Reading from 21118: heap size 232 MB, throughput 0.853139
Reading from 21117: heap size 235 MB, throughput 0.85046
Reading from 21117: heap size 246 MB, throughput 0.828743
Reading from 21118: heap size 233 MB, throughput 0.828034
Reading from 21118: heap size 237 MB, throughput 0.770125
Reading from 21117: heap size 245 MB, throughput 0.865622
Reading from 21117: heap size 247 MB, throughput 0.792381
Reading from 21118: heap size 239 MB, throughput 0.783894
Reading from 21117: heap size 251 MB, throughput 0.785645
Reading from 21118: heap size 243 MB, throughput 0.764311
Reading from 21117: heap size 251 MB, throughput 0.810157
Reading from 21118: heap size 245 MB, throughput 0.737658
Reading from 21117: heap size 254 MB, throughput 0.826491
Reading from 21117: heap size 255 MB, throughput 0.728665
Reading from 21118: heap size 250 MB, throughput 0.792488
Reading from 21117: heap size 261 MB, throughput 0.741012
Reading from 21118: heap size 251 MB, throughput 0.737787
Reading from 21117: heap size 264 MB, throughput 0.729946
Reading from 21117: heap size 271 MB, throughput 0.753259
Reading from 21118: heap size 254 MB, throughput 0.753409
Reading from 21118: heap size 256 MB, throughput 0.740938
Reading from 21117: heap size 273 MB, throughput 0.875521
Reading from 21117: heap size 277 MB, throughput 0.817966
Reading from 21118: heap size 262 MB, throughput 0.64317
Reading from 21118: heap size 299 MB, throughput 0.673564
Reading from 21118: heap size 296 MB, throughput 0.707452
Reading from 21117: heap size 281 MB, throughput 0.649419
Reading from 21117: heap size 325 MB, throughput 0.745111
Reading from 21117: heap size 326 MB, throughput 0.842076
Reading from 21118: heap size 299 MB, throughput 0.878243
Reading from 21118: heap size 298 MB, throughput 0.817514
Reading from 21117: heap size 332 MB, throughput 0.888055
Reading from 21117: heap size 333 MB, throughput 0.807233
Reading from 21117: heap size 336 MB, throughput 0.805682
Reading from 21117: heap size 338 MB, throughput 0.790871
Reading from 21118: heap size 300 MB, throughput 0.595782
Reading from 21117: heap size 342 MB, throughput 0.799795
Reading from 21117: heap size 343 MB, throughput 0.728202
Reading from 21118: heap size 347 MB, throughput 0.713137
Reading from 21118: heap size 347 MB, throughput 0.759204
Numeric result:
Recommendation: 2 clients, utility 0.462105:
    h1: 1554.76 MB (U(h) = 0.539473*h^0.032686)
    h2: 1205.24 MB (U(h) = 0.562825*h^0.02534)
Recommendation: 2 clients, utility 0.462105:
    h1: 1554.71 MB (U(h) = 0.539473*h^0.032686)
    h2: 1205.29 MB (U(h) = 0.562825*h^0.02534)
Reading from 21118: heap size 353 MB, throughput 0.855182
Reading from 21118: heap size 353 MB, throughput 0.825318
Reading from 21118: heap size 349 MB, throughput 0.753717
Reading from 21118: heap size 353 MB, throughput 0.710947
Reading from 21117: heap size 349 MB, throughput 0.931079
Reading from 21118: heap size 355 MB, throughput 0.672411
Reading from 21118: heap size 357 MB, throughput 0.68035
Reading from 21118: heap size 363 MB, throughput 0.645765
Reading from 21118: heap size 364 MB, throughput 0.939481
Reading from 21117: heap size 349 MB, throughput 0.965101
Reading from 21117: heap size 353 MB, throughput 0.969412
Reading from 21118: heap size 369 MB, throughput 0.966153
Reading from 21118: heap size 371 MB, throughput 0.968066
Reading from 21117: heap size 356 MB, throughput 0.977649
Reading from 21117: heap size 355 MB, throughput 0.976601
Reading from 21118: heap size 372 MB, throughput 0.974028
Reading from 21118: heap size 375 MB, throughput 0.972919
Reading from 21117: heap size 358 MB, throughput 0.978855
Reading from 21117: heap size 356 MB, throughput 0.976631
Reading from 21118: heap size 378 MB, throughput 0.977512
Numeric result:
Recommendation: 2 clients, utility 0.617823:
    h1: 1582.72 MB (U(h) = 0.438683*h^0.0841793)
    h2: 1177.28 MB (U(h) = 0.486529*h^0.0626141)
Recommendation: 2 clients, utility 0.617823:
    h1: 1582.73 MB (U(h) = 0.438683*h^0.0841793)
    h2: 1177.27 MB (U(h) = 0.486529*h^0.0626141)
Reading from 21117: heap size 359 MB, throughput 0.977324
Reading from 21118: heap size 380 MB, throughput 0.977698
Reading from 21117: heap size 362 MB, throughput 0.978555
Reading from 21118: heap size 378 MB, throughput 0.975036
Reading from 21117: heap size 363 MB, throughput 0.975857
Reading from 21118: heap size 381 MB, throughput 0.975196
Reading from 21117: heap size 365 MB, throughput 0.964809
Reading from 21118: heap size 379 MB, throughput 0.968752
Reading from 21117: heap size 367 MB, throughput 0.961957
Reading from 21118: heap size 381 MB, throughput 0.965087
Reading from 21117: heap size 371 MB, throughput 0.960777
Reading from 21118: heap size 383 MB, throughput 0.967071
Numeric result:
Recommendation: 2 clients, utility 0.727207:
    h1: 1458.4 MB (U(h) = 0.404297*h^0.104334)
    h2: 1301.6 MB (U(h) = 0.431371*h^0.0931162)
Recommendation: 2 clients, utility 0.727207:
    h1: 1458.4 MB (U(h) = 0.404297*h^0.104334)
    h2: 1301.6 MB (U(h) = 0.431371*h^0.0931162)
Reading from 21117: heap size 373 MB, throughput 0.974492
Reading from 21118: heap size 384 MB, throughput 0.969487
Reading from 21117: heap size 378 MB, throughput 0.970099
Reading from 21117: heap size 379 MB, throughput 0.951287
Reading from 21117: heap size 378 MB, throughput 0.924533
Reading from 21117: heap size 382 MB, throughput 0.890305
Reading from 21117: heap size 390 MB, throughput 0.940283
Reading from 21118: heap size 387 MB, throughput 0.979927
Reading from 21118: heap size 388 MB, throughput 0.967149
Reading from 21118: heap size 384 MB, throughput 0.949291
Reading from 21118: heap size 389 MB, throughput 0.920139
Reading from 21118: heap size 397 MB, throughput 0.902039
Reading from 21117: heap size 392 MB, throughput 0.980692
Reading from 21118: heap size 399 MB, throughput 0.973416
Reading from 21117: heap size 393 MB, throughput 0.984603
Reading from 21118: heap size 402 MB, throughput 0.98342
Reading from 21117: heap size 396 MB, throughput 0.986726
Reading from 21118: heap size 404 MB, throughput 0.987768
Reading from 21117: heap size 394 MB, throughput 0.98506
Numeric result:
Recommendation: 2 clients, utility 0.868439:
    h1: 1429.41 MB (U(h) = 0.362121*h^0.13128)
    h2: 1330.59 MB (U(h) = 0.383673*h^0.122186)
Recommendation: 2 clients, utility 0.868439:
    h1: 1429.51 MB (U(h) = 0.362121*h^0.13128)
    h2: 1330.49 MB (U(h) = 0.383673*h^0.122186)
Reading from 21118: heap size 404 MB, throughput 0.985291
Reading from 21117: heap size 397 MB, throughput 0.985824
Reading from 21118: heap size 407 MB, throughput 0.985333
Reading from 21117: heap size 393 MB, throughput 0.98501
Reading from 21118: heap size 406 MB, throughput 0.985234
Reading from 21117: heap size 396 MB, throughput 0.981351
Reading from 21117: heap size 394 MB, throughput 0.965834
Reading from 21118: heap size 408 MB, throughput 0.954231
Reading from 21117: heap size 396 MB, throughput 0.97384
Reading from 21118: heap size 442 MB, throughput 0.990082
Numeric result:
Recommendation: 2 clients, utility 0.905791:
    h1: 1469.95 MB (U(h) = 0.346065*h^0.142187)
    h2: 1290.05 MB (U(h) = 0.379625*h^0.124788)
Recommendation: 2 clients, utility 0.905791:
    h1: 1469.93 MB (U(h) = 0.346065*h^0.142187)
    h2: 1290.07 MB (U(h) = 0.379625*h^0.124788)
Reading from 21117: heap size 396 MB, throughput 0.955407
Reading from 21118: heap size 444 MB, throughput 0.991841
Reading from 21117: heap size 433 MB, throughput 0.974499
Reading from 21117: heap size 436 MB, throughput 0.967799
Reading from 21118: heap size 449 MB, throughput 0.991338
Reading from 21117: heap size 436 MB, throughput 0.958774
Reading from 21117: heap size 441 MB, throughput 0.937205
Reading from 21117: heap size 441 MB, throughput 0.977487
Reading from 21118: heap size 451 MB, throughput 0.991913
Reading from 21118: heap size 447 MB, throughput 0.98492
Reading from 21118: heap size 450 MB, throughput 0.966356
Reading from 21118: heap size 455 MB, throughput 0.944355
Reading from 21117: heap size 449 MB, throughput 0.987825
Reading from 21118: heap size 457 MB, throughput 0.969169
Reading from 21117: heap size 450 MB, throughput 0.987036
Reading from 21118: heap size 463 MB, throughput 0.986265
Numeric result:
Recommendation: 2 clients, utility 0.986768:
    h1: 1482.25 MB (U(h) = 0.322492*h^0.158747)
    h2: 1277.75 MB (U(h) = 0.360711*h^0.136865)
Recommendation: 2 clients, utility 0.986768:
    h1: 1482.16 MB (U(h) = 0.322492*h^0.158747)
    h2: 1277.84 MB (U(h) = 0.360711*h^0.136865)
Reading from 21117: heap size 454 MB, throughput 0.987291
Reading from 21118: heap size 465 MB, throughput 0.989256
Reading from 21117: heap size 455 MB, throughput 0.987937
Reading from 21118: heap size 462 MB, throughput 0.990344
Reading from 21117: heap size 454 MB, throughput 0.989407
Reading from 21118: heap size 466 MB, throughput 0.988485
Reading from 21117: heap size 457 MB, throughput 0.988842
Reading from 21118: heap size 465 MB, throughput 0.986619
Reading from 21117: heap size 456 MB, throughput 0.986521
Reading from 21118: heap size 467 MB, throughput 0.985369
Numeric result:
Recommendation: 2 clients, utility 1.03201:
    h1: 1468.74 MB (U(h) = 0.313354*h^0.165436)
    h2: 1291.26 MB (U(h) = 0.347729*h^0.145445)
Recommendation: 2 clients, utility 1.03201:
    h1: 1468.74 MB (U(h) = 0.313354*h^0.165436)
    h2: 1291.26 MB (U(h) = 0.347729*h^0.145445)
Reading from 21117: heap size 457 MB, throughput 0.976818
Reading from 21118: heap size 469 MB, throughput 0.982115
Reading from 21117: heap size 459 MB, throughput 0.982936
Reading from 21117: heap size 460 MB, throughput 0.975958
Reading from 21117: heap size 462 MB, throughput 0.965416
Reading from 21118: heap size 470 MB, throughput 0.983117
Reading from 21117: heap size 463 MB, throughput 0.977154
Reading from 21118: heap size 473 MB, throughput 0.984399
Reading from 21118: heap size 474 MB, throughput 0.978623
Reading from 21118: heap size 474 MB, throughput 0.966706
Reading from 21118: heap size 476 MB, throughput 0.96715
Reading from 21117: heap size 471 MB, throughput 0.987989
Reading from 21118: heap size 484 MB, throughput 0.986133
Reading from 21117: heap size 472 MB, throughput 0.99105
Numeric result:
Recommendation: 2 clients, utility 1.08506:
    h1: 1455.27 MB (U(h) = 0.303285*h^0.172989)
    h2: 1304.73 MB (U(h) = 0.333567*h^0.155115)
Recommendation: 2 clients, utility 1.08506:
    h1: 1455.18 MB (U(h) = 0.303285*h^0.172989)
    h2: 1304.82 MB (U(h) = 0.333567*h^0.155115)
Reading from 21118: heap size 484 MB, throughput 0.988145
Reading from 21117: heap size 473 MB, throughput 0.991456
Reading from 21118: heap size 485 MB, throughput 0.989792
Reading from 21117: heap size 475 MB, throughput 0.989703
Reading from 21117: heap size 472 MB, throughput 0.988342
Reading from 21118: heap size 487 MB, throughput 0.990762
Reading from 21117: heap size 475 MB, throughput 0.986901
Reading from 21118: heap size 485 MB, throughput 0.98968
Numeric result:
Recommendation: 2 clients, utility 1.10177:
    h1: 1451.62 MB (U(h) = 0.30017*h^0.175357)
    h2: 1308.38 MB (U(h) = 0.329347*h^0.158055)
Recommendation: 2 clients, utility 1.10177:
    h1: 1451.62 MB (U(h) = 0.30017*h^0.175357)
    h2: 1308.38 MB (U(h) = 0.329347*h^0.158055)
Reading from 21117: heap size 476 MB, throughput 0.987508
Reading from 21118: heap size 487 MB, throughput 0.988392
Reading from 21117: heap size 477 MB, throughput 0.989581
Reading from 21118: heap size 489 MB, throughput 0.987047
Reading from 21117: heap size 479 MB, throughput 0.983936
Reading from 21117: heap size 480 MB, throughput 0.973235
Reading from 21117: heap size 485 MB, throughput 0.98054
Reading from 21118: heap size 489 MB, throughput 0.989314
Reading from 21118: heap size 492 MB, throughput 0.984949
Reading from 21118: heap size 492 MB, throughput 0.968015
Reading from 21117: heap size 487 MB, throughput 0.987825
Reading from 21118: heap size 498 MB, throughput 0.977449
Numeric result:
Recommendation: 2 clients, utility 1.13391:
    h1: 1432.24 MB (U(h) = 0.296388*h^0.178253)
    h2: 1327.76 MB (U(h) = 0.319196*h^0.165249)
Recommendation: 2 clients, utility 1.13391:
    h1: 1432.24 MB (U(h) = 0.296388*h^0.178253)
    h2: 1327.76 MB (U(h) = 0.319196*h^0.165249)
Reading from 21117: heap size 491 MB, throughput 0.989972
Reading from 21118: heap size 499 MB, throughput 0.988526
Reading from 21117: heap size 492 MB, throughput 0.990738
Reading from 21118: heap size 503 MB, throughput 0.991785
Reading from 21117: heap size 491 MB, throughput 0.98974
Reading from 21118: heap size 505 MB, throughput 0.990698
Reading from 21117: heap size 493 MB, throughput 0.989801
Reading from 21118: heap size 504 MB, throughput 0.990006
Numeric result:
Recommendation: 2 clients, utility 1.15645:
    h1: 1433.33 MB (U(h) = 0.291542*h^0.181993)
    h2: 1326.67 MB (U(h) = 0.314731*h^0.168457)
Recommendation: 2 clients, utility 1.15645:
    h1: 1433.3 MB (U(h) = 0.291542*h^0.181993)
    h2: 1326.7 MB (U(h) = 0.314731*h^0.168457)
Reading from 21117: heap size 495 MB, throughput 0.987551
Reading from 21118: heap size 506 MB, throughput 0.989334
Reading from 21117: heap size 495 MB, throughput 0.990391
Reading from 21117: heap size 497 MB, throughput 0.985772
Reading from 21117: heap size 498 MB, throughput 0.971597
Reading from 21118: heap size 508 MB, throughput 0.989299
Reading from 21117: heap size 503 MB, throughput 0.9805
Reading from 21117: heap size 505 MB, throughput 0.989036
Reading from 21118: heap size 508 MB, throughput 0.989399
Reading from 21118: heap size 508 MB, throughput 0.986163
Reading from 21118: heap size 510 MB, throughput 0.976183
Reading from 21118: heap size 515 MB, throughput 0.981417
Numeric result:
Recommendation: 2 clients, utility 1.18025:
    h1: 1427.17 MB (U(h) = 0.287727*h^0.184969)
    h2: 1332.83 MB (U(h) = 0.308823*h^0.172749)
Recommendation: 2 clients, utility 1.18025:
    h1: 1427.14 MB (U(h) = 0.287727*h^0.184969)
    h2: 1332.86 MB (U(h) = 0.308823*h^0.172749)
Reading from 21117: heap size 509 MB, throughput 0.989966
Reading from 21118: heap size 517 MB, throughput 0.989561
Reading from 21117: heap size 511 MB, throughput 0.989595
Reading from 21118: heap size 520 MB, throughput 0.98814
Reading from 21117: heap size 509 MB, throughput 0.989844
Reading from 21118: heap size 522 MB, throughput 0.990298
Reading from 21117: heap size 512 MB, throughput 0.990142
Numeric result:
Recommendation: 2 clients, utility 1.50451:
    h1: 1804.24 MB (U(h) = 0.124056*h^0.330652)
    h2: 955.756 MB (U(h) = 0.305537*h^0.175152)
Recommendation: 2 clients, utility 1.50451:
    h1: 1804.25 MB (U(h) = 0.124056*h^0.330652)
    h2: 955.746 MB (U(h) = 0.305537*h^0.175152)
Reading from 21118: heap size 521 MB, throughput 0.989047
Reading from 21117: heap size 515 MB, throughput 0.989069
Reading from 21117: heap size 515 MB, throughput 0.988466
Reading from 21117: heap size 518 MB, throughput 0.983992
Reading from 21118: heap size 523 MB, throughput 0.988752
Reading from 21117: heap size 520 MB, throughput 0.98434
Reading from 21118: heap size 525 MB, throughput 0.987703
Reading from 21117: heap size 526 MB, throughput 0.991071
Reading from 21118: heap size 526 MB, throughput 0.986787
Reading from 21118: heap size 530 MB, throughput 0.979728
Reading from 21118: heap size 532 MB, throughput 0.981396
Numeric result:
Recommendation: 2 clients, utility 1.93434:
    h1: 1859.05 MB (U(h) = 0.0590473*h^0.456227)
    h2: 900.951 MB (U(h) = 0.2347*h^0.221099)
Recommendation: 2 clients, utility 1.93434:
    h1: 1859.05 MB (U(h) = 0.0590473*h^0.456227)
    h2: 900.945 MB (U(h) = 0.2347*h^0.221099)
Reading from 21117: heap size 527 MB, throughput 0.993234
Reading from 21118: heap size 539 MB, throughput 0.991779
Reading from 21117: heap size 527 MB, throughput 0.992505
Reading from 21118: heap size 539 MB, throughput 0.991698
Reading from 21117: heap size 529 MB, throughput 0.991658
Reading from 21118: heap size 541 MB, throughput 0.990639
Reading from 21117: heap size 528 MB, throughput 0.990066
Numeric result:
Recommendation: 2 clients, utility 2.15246:
    h1: 1398.72 MB (U(h) = 0.0844245*h^0.396156)
    h2: 1361.28 MB (U(h) = 0.0895317*h^0.385549)
Recommendation: 2 clients, utility 2.15246:
    h1: 1398.73 MB (U(h) = 0.0844245*h^0.396156)
    h2: 1361.27 MB (U(h) = 0.0895317*h^0.385549)
Reading from 21118: heap size 542 MB, throughput 0.990159
Reading from 21117: heap size 529 MB, throughput 0.992649
Reading from 21117: heap size 531 MB, throughput 0.989477
Reading from 21117: heap size 532 MB, throughput 0.980786
Reading from 21118: heap size 541 MB, throughput 0.989857
Reading from 21117: heap size 536 MB, throughput 0.987464
Reading from 21118: heap size 543 MB, throughput 0.990797
Reading from 21117: heap size 537 MB, throughput 0.991094
Reading from 21118: heap size 544 MB, throughput 0.986943
Reading from 21118: heap size 545 MB, throughput 0.979975
Numeric result:
Recommendation: 2 clients, utility 2.56428:
    h1: 1124.08 MB (U(h) = 0.0996798*h^0.368543)
    h2: 1635.92 MB (U(h) = 0.0365015*h^0.536349)
Recommendation: 2 clients, utility 2.56428:
    h1: 1124.09 MB (U(h) = 0.0996798*h^0.368543)
    h2: 1635.91 MB (U(h) = 0.0365015*h^0.536349)
Reading from 21118: heap size 550 MB, throughput 0.989107
Reading from 21117: heap size 540 MB, throughput 0.992184
Reading from 21118: heap size 552 MB, throughput 0.992191
Reading from 21117: heap size 542 MB, throughput 0.992216
Reading from 21118: heap size 555 MB, throughput 0.992555
Reading from 21117: heap size 541 MB, throughput 0.991436
Numeric result:
Recommendation: 2 clients, utility 3.04686:
    h1: 1134.53 MB (U(h) = 0.0680834*h^0.431351)
    h2: 1625.47 MB (U(h) = 0.0223185*h^0.61802)
Recommendation: 2 clients, utility 3.04686:
    h1: 1134.52 MB (U(h) = 0.0680834*h^0.431351)
    h2: 1625.48 MB (U(h) = 0.0223185*h^0.61802)
Reading from 21117: heap size 543 MB, throughput 0.989771
Reading from 21118: heap size 556 MB, throughput 0.992108
Reading from 21117: heap size 545 MB, throughput 0.988993
Reading from 21117: heap size 546 MB, throughput 0.983455
Reading from 21117: heap size 550 MB, throughput 0.984977
Reading from 21118: heap size 555 MB, throughput 0.99274
Reading from 21117: heap size 551 MB, throughput 0.98828
Reading from 21118: heap size 557 MB, throughput 0.987713
Numeric result:
Recommendation: 2 clients, utility 2.85204:
    h1: 1195.24 MB (U(h) = 0.066314*h^0.435495)
    h2: 1564.76 MB (U(h) = 0.0296567*h^0.570118)
Recommendation: 2 clients, utility 2.85204:
    h1: 1195.26 MB (U(h) = 0.066314*h^0.435495)
    h2: 1564.74 MB (U(h) = 0.0296567*h^0.570118)
Reading from 21118: heap size 555 MB, throughput 0.987444
Reading from 21118: heap size 560 MB, throughput 0.978806
Reading from 21117: heap size 555 MB, throughput 0.991279
Reading from 21118: heap size 564 MB, throughput 0.986421
Reading from 21117: heap size 556 MB, throughput 0.992031
Reading from 21118: heap size 565 MB, throughput 0.989655
Reading from 21117: heap size 554 MB, throughput 0.991879
Numeric result:
Recommendation: 2 clients, utility 2.22424:
    h1: 1572.01 MB (U(h) = 0.060004*h^0.451465)
    h2: 1187.99 MB (U(h) = 0.119365*h^0.341171)
Recommendation: 2 clients, utility 2.22424:
    h1: 1572.02 MB (U(h) = 0.060004*h^0.451465)
    h2: 1187.98 MB (U(h) = 0.119365*h^0.341171)
Reading from 21118: heap size 568 MB, throughput 0.991006
Reading from 21117: heap size 557 MB, throughput 0.990705
Reading from 21118: heap size 570 MB, throughput 0.990315
Reading from 21117: heap size 559 MB, throughput 0.989735
Reading from 21117: heap size 560 MB, throughput 0.983828
Reading from 21118: heap size 569 MB, throughput 0.989822
Reading from 21117: heap size 564 MB, throughput 0.990662
Numeric result:
Recommendation: 2 clients, utility 1.95613:
    h1: 1493.65 MB (U(h) = 0.0977416*h^0.371373)
    h2: 1266.35 MB (U(h) = 0.13983*h^0.314866)
Recommendation: 2 clients, utility 1.95613:
    h1: 1493.63 MB (U(h) = 0.0977416*h^0.371373)
    h2: 1266.37 MB (U(h) = 0.13983*h^0.314866)
Reading from 21118: heap size 571 MB, throughput 0.992961
Reading from 21117: heap size 565 MB, throughput 0.992624
Reading from 21118: heap size 573 MB, throughput 0.989558
Reading from 21118: heap size 574 MB, throughput 0.980896
Reading from 21117: heap size 569 MB, throughput 0.992643
Reading from 21118: heap size 578 MB, throughput 0.989979
Reading from 21117: heap size 570 MB, throughput 0.9908
Reading from 21118: heap size 580 MB, throughput 0.991735
Numeric result:
Recommendation: 2 clients, utility 2.03317:
    h1: 1542.37 MB (U(h) = 0.079196*h^0.404956)
    h2: 1217.63 MB (U(h) = 0.135494*h^0.319712)
Recommendation: 2 clients, utility 2.03317:
    h1: 1542.33 MB (U(h) = 0.079196*h^0.404956)
    h2: 1217.67 MB (U(h) = 0.135494*h^0.319712)
Reading from 21117: heap size 569 MB, throughput 0.992093
Reading from 21118: heap size 583 MB, throughput 0.992233
Reading from 21117: heap size 571 MB, throughput 0.993842
Reading from 21117: heap size 574 MB, throughput 0.990083
Reading from 21118: heap size 585 MB, throughput 0.991837
Reading from 21117: heap size 574 MB, throughput 0.987647
Reading from 21118: heap size 583 MB, throughput 0.99224
Reading from 21117: heap size 580 MB, throughput 0.993684
Numeric result:
Recommendation: 2 clients, utility 1.98964:
    h1: 1602.49 MB (U(h) = 0.0774374*h^0.408347)
    h2: 1157.51 MB (U(h) = 0.157602*h^0.294952)
Recommendation: 2 clients, utility 1.98964:
    h1: 1602.5 MB (U(h) = 0.0774374*h^0.408347)
    h2: 1157.5 MB (U(h) = 0.157602*h^0.294952)
Reading from 21118: heap size 586 MB, throughput 0.993434
Reading from 21118: heap size 589 MB, throughput 0.989044
Reading from 21117: heap size 581 MB, throughput 0.994393
Reading from 21118: heap size 589 MB, throughput 0.98887
Reading from 21117: heap size 580 MB, throughput 0.993398
Reading from 21118: heap size 596 MB, throughput 0.992603
Numeric result:
Recommendation: 2 clients, utility 1.9297:
    h1: 1625.77 MB (U(h) = 0.0825033*h^0.397735)
    h2: 1134.23 MB (U(h) = 0.175488*h^0.277481)
Recommendation: 2 clients, utility 1.9297:
    h1: 1625.77 MB (U(h) = 0.0825033*h^0.397735)
    h2: 1134.23 MB (U(h) = 0.175488*h^0.277481)
Reading from 21117: heap size 582 MB, throughput 0.992101
Reading from 21118: heap size 597 MB, throughput 0.992446
Reading from 21117: heap size 583 MB, throughput 0.993573
Reading from 21118: heap size 596 MB, throughput 0.992159
Reading from 21117: heap size 584 MB, throughput 0.991052
Reading from 21117: heap size 583 MB, throughput 0.98759
Reading from 21118: heap size 598 MB, throughput 0.99152
Reading from 21117: heap size 585 MB, throughput 0.992781
Numeric result:
Recommendation: 2 clients, utility 1.71032:
    h1: 1696.49 MB (U(h) = 0.116067*h^0.342494)
    h2: 1063.51 MB (U(h) = 0.258468*h^0.214706)
Recommendation: 2 clients, utility 1.71032:
    h1: 1696.49 MB (U(h) = 0.116067*h^0.342494)
    h2: 1063.51 MB (U(h) = 0.258468*h^0.214706)
Reading from 21118: heap size 601 MB, throughput 0.993332
Reading from 21118: heap size 601 MB, throughput 0.989739
Reading from 21117: heap size 587 MB, throughput 0.993193
Reading from 21118: heap size 601 MB, throughput 0.987848
Reading from 21117: heap size 589 MB, throughput 0.993136
Reading from 21118: heap size 603 MB, throughput 0.992855
Numeric result:
Recommendation: 2 clients, utility 1.63398:
    h1: 1773.91 MB (U(h) = 0.128281*h^0.326191)
    h2: 986.09 MB (U(h) = 0.318004*h^0.181327)
Recommendation: 2 clients, utility 1.63398:
    h1: 1773.9 MB (U(h) = 0.128281*h^0.326191)
    h2: 986.098 MB (U(h) = 0.318004*h^0.181327)
Reading from 21117: heap size 589 MB, throughput 0.992508
Reading from 21118: heap size 605 MB, throughput 0.993433
Reading from 21117: heap size 590 MB, throughput 0.9939
Reading from 21117: heap size 591 MB, throughput 0.990866
Reading from 21117: heap size 592 MB, throughput 0.988373
Reading from 21118: heap size 607 MB, throughput 0.992863
Numeric result:
Recommendation: 2 clients, utility 1.42819:
    h1: 1736.67 MB (U(h) = 0.218865*h^0.240336)
    h2: 1023.33 MB (U(h) = 0.407121*h^0.141616)
Recommendation: 2 clients, utility 1.42819:
    h1: 1736.68 MB (U(h) = 0.218865*h^0.240336)
    h2: 1023.32 MB (U(h) = 0.407121*h^0.141616)
Reading from 21118: heap size 606 MB, throughput 0.993059
Reading from 21117: heap size 598 MB, throughput 0.993982
Reading from 21118: heap size 608 MB, throughput 0.992963
Reading from 21118: heap size 610 MB, throughput 0.987851
Reading from 21117: heap size 599 MB, throughput 0.992073
Reading from 21118: heap size 611 MB, throughput 0.990417
Reading from 21117: heap size 599 MB, throughput 0.992514
Numeric result:
Recommendation: 2 clients, utility 1.19009:
    h1: 1461.1 MB (U(h) = 0.494198*h^0.110148)
    h2: 1298.9 MB (U(h) = 0.53488*h^0.0979067)
Recommendation: 2 clients, utility 1.19009:
    h1: 1461.19 MB (U(h) = 0.494198*h^0.110148)
    h2: 1298.81 MB (U(h) = 0.53488*h^0.0979067)
Reading from 21118: heap size 618 MB, throughput 0.992504
Reading from 21117: heap size 600 MB, throughput 0.992247
Reading from 21118: heap size 619 MB, throughput 0.992236
Reading from 21117: heap size 601 MB, throughput 0.994082
Reading from 21117: heap size 602 MB, throughput 0.990639
Client 21117 died
Clients: 1
Reading from 21118: heap size 618 MB, throughput 0.993199
Recommendation: one client; give it all the memory
Reading from 21118: heap size 621 MB, throughput 0.992953
Reading from 21118: heap size 622 MB, throughput 0.991661
Client 21118 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
