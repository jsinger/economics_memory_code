economemd
    total memory: 3312 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub24_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 1585: heap size 9 MB, throughput 0.992169
Clients: 1
Client 1585 has a minimum heap size of 276 MB
Reading from 1586: heap size 9 MB, throughput 0.991953
Clients: 2
Client 1586 has a minimum heap size of 276 MB
Reading from 1585: heap size 9 MB, throughput 0.98828
Reading from 1586: heap size 9 MB, throughput 0.987294
Reading from 1585: heap size 9 MB, throughput 0.983519
Reading from 1586: heap size 9 MB, throughput 0.981142
Reading from 1585: heap size 9 MB, throughput 0.973785
Reading from 1586: heap size 9 MB, throughput 0.973603
Reading from 1585: heap size 11 MB, throughput 0.974339
Reading from 1586: heap size 11 MB, throughput 0.982547
Reading from 1585: heap size 11 MB, throughput 0.975039
Reading from 1586: heap size 11 MB, throughput 0.97571
Reading from 1585: heap size 17 MB, throughput 0.949888
Reading from 1586: heap size 17 MB, throughput 0.942073
Reading from 1585: heap size 17 MB, throughput 0.916841
Reading from 1586: heap size 17 MB, throughput 0.858687
Reading from 1585: heap size 30 MB, throughput 0.856493
Reading from 1586: heap size 30 MB, throughput 0.77298
Reading from 1585: heap size 31 MB, throughput 0.631003
Reading from 1586: heap size 31 MB, throughput 0.483672
Reading from 1585: heap size 35 MB, throughput 0.700339
Reading from 1586: heap size 34 MB, throughput 0.723641
Reading from 1585: heap size 46 MB, throughput 0.463059
Reading from 1586: heap size 47 MB, throughput 0.682027
Reading from 1586: heap size 51 MB, throughput 0.43293
Reading from 1585: heap size 48 MB, throughput 0.472529
Reading from 1586: heap size 51 MB, throughput 0.470568
Reading from 1585: heap size 51 MB, throughput 0.497851
Reading from 1586: heap size 71 MB, throughput 0.322026
Reading from 1585: heap size 77 MB, throughput 0.469512
Reading from 1586: heap size 72 MB, throughput 0.11217
Reading from 1585: heap size 77 MB, throughput 0.406026
Reading from 1586: heap size 76 MB, throughput 0.458723
Reading from 1585: heap size 104 MB, throughput 0.684362
Reading from 1585: heap size 105 MB, throughput 0.500678
Reading from 1586: heap size 96 MB, throughput 0.828871
Reading from 1586: heap size 102 MB, throughput 0.0674587
Reading from 1585: heap size 106 MB, throughput 0.38579
Reading from 1586: heap size 103 MB, throughput 0.751911
Reading from 1585: heap size 109 MB, throughput 0.284137
Reading from 1586: heap size 106 MB, throughput 0.422806
Reading from 1585: heap size 112 MB, throughput 0.436232
Reading from 1586: heap size 133 MB, throughput 0.7538
Reading from 1585: heap size 147 MB, throughput 0.286783
Reading from 1586: heap size 138 MB, throughput 0.784607
Reading from 1585: heap size 151 MB, throughput 0.203875
Reading from 1586: heap size 142 MB, throughput 0.775966
Reading from 1585: heap size 153 MB, throughput 0.750659
Reading from 1586: heap size 149 MB, throughput 0.737649
Reading from 1585: heap size 161 MB, throughput 0.763903
Reading from 1586: heap size 152 MB, throughput 0.69999
Reading from 1585: heap size 163 MB, throughput 0.744523
Reading from 1586: heap size 154 MB, throughput 0.385864
Reading from 1585: heap size 166 MB, throughput 0.459301
Reading from 1586: heap size 196 MB, throughput 0.596247
Reading from 1585: heap size 211 MB, throughput 0.673279
Reading from 1586: heap size 200 MB, throughput 0.591028
Reading from 1585: heap size 215 MB, throughput 0.836516
Reading from 1586: heap size 204 MB, throughput 0.685066
Reading from 1586: heap size 207 MB, throughput 0.729642
Reading from 1585: heap size 218 MB, throughput 0.891278
Reading from 1585: heap size 221 MB, throughput 0.817269
Reading from 1585: heap size 230 MB, throughput 0.727268
Reading from 1586: heap size 215 MB, throughput 0.802323
Reading from 1585: heap size 235 MB, throughput 0.743017
Reading from 1586: heap size 219 MB, throughput 0.754749
Reading from 1585: heap size 241 MB, throughput 0.662361
Reading from 1586: heap size 229 MB, throughput 0.648098
Reading from 1586: heap size 233 MB, throughput 0.548116
Reading from 1585: heap size 246 MB, throughput 0.657555
Reading from 1585: heap size 250 MB, throughput 0.679245
Reading from 1585: heap size 257 MB, throughput 0.680824
Reading from 1585: heap size 257 MB, throughput 0.660804
Reading from 1585: heap size 262 MB, throughput 0.669318
Reading from 1585: heap size 262 MB, throughput 0.603597
Reading from 1586: heap size 239 MB, throughput 0.562453
Reading from 1586: heap size 276 MB, throughput 0.575673
Reading from 1586: heap size 210 MB, throughput 0.618605
Reading from 1586: heap size 273 MB, throughput 0.686088
Reading from 1586: heap size 275 MB, throughput 0.743605
Reading from 1586: heap size 271 MB, throughput 0.761474
Reading from 1586: heap size 274 MB, throughput 0.75059
Reading from 1585: heap size 263 MB, throughput 0.585324
Reading from 1585: heap size 304 MB, throughput 0.592587
Reading from 1586: heap size 269 MB, throughput 0.818716
Reading from 1585: heap size 304 MB, throughput 0.696196
Reading from 1585: heap size 305 MB, throughput 0.702781
Reading from 1586: heap size 222 MB, throughput 0.579559
Reading from 1586: heap size 303 MB, throughput 0.661221
Reading from 1586: heap size 307 MB, throughput 0.732472
Reading from 1586: heap size 309 MB, throughput 0.776114
Reading from 1586: heap size 310 MB, throughput 0.790508
Reading from 1585: heap size 309 MB, throughput 0.909473
Reading from 1586: heap size 311 MB, throughput 0.831368
Reading from 1585: heap size 310 MB, throughput 0.886851
Reading from 1585: heap size 303 MB, throughput 0.864341
Reading from 1585: heap size 307 MB, throughput 0.845604
Reading from 1585: heap size 302 MB, throughput 0.841474
Reading from 1586: heap size 312 MB, throughput 0.924048
Reading from 1585: heap size 305 MB, throughput 0.859879
Reading from 1586: heap size 304 MB, throughput 0.909331
Reading from 1586: heap size 263 MB, throughput 0.848126
Reading from 1585: heap size 304 MB, throughput 0.879804
Reading from 1586: heap size 301 MB, throughput 0.809758
Reading from 1585: heap size 306 MB, throughput 0.855337
Reading from 1586: heap size 305 MB, throughput 0.835836
Reading from 1585: heap size 309 MB, throughput 0.811007
Reading from 1586: heap size 299 MB, throughput 0.803307
Reading from 1586: heap size 302 MB, throughput 0.808412
Reading from 1586: heap size 300 MB, throughput 0.870283
Reading from 1586: heap size 302 MB, throughput 0.801242
Reading from 1586: heap size 304 MB, throughput 0.732075
Numeric result:
Recommendation: 2 clients, utility 0.493707:
    h1: 276 MB (U(h) = 0.625382*h^0.00499617)
    h2: 3036 MB (U(h) = 0.474042*h^0.0601074)
Recommendation: 2 clients, utility 0.493707:
    h1: 276 MB (U(h) = 0.625382*h^0.00499617)
    h2: 3036 MB (U(h) = 0.474042*h^0.0601074)
Reading from 1585: heap size 310 MB, throughput 0.595549
Reading from 1586: heap size 305 MB, throughput 0.714001
Reading from 1586: heap size 312 MB, throughput 0.653594
Reading from 1585: heap size 355 MB, throughput 0.669695
Reading from 1586: heap size 313 MB, throughput 0.621627
Reading from 1585: heap size 317 MB, throughput 0.644403
Reading from 1586: heap size 321 MB, throughput 0.640799
Reading from 1585: heap size 351 MB, throughput 0.63688
Reading from 1586: heap size 322 MB, throughput 0.665526
Reading from 1585: heap size 318 MB, throughput 0.904463
Reading from 1586: heap size 327 MB, throughput 0.948342
Reading from 1585: heap size 346 MB, throughput 0.960548
Reading from 1586: heap size 329 MB, throughput 0.971972
Reading from 1585: heap size 320 MB, throughput 0.968433
Reading from 1586: heap size 335 MB, throughput 0.978554
Reading from 1585: heap size 340 MB, throughput 0.969745
Reading from 1586: heap size 337 MB, throughput 0.978032
Reading from 1585: heap size 317 MB, throughput 0.968743
Reading from 1585: heap size 333 MB, throughput 0.969397
Reading from 1586: heap size 337 MB, throughput 0.977377
Reading from 1585: heap size 319 MB, throughput 0.968388
Reading from 1586: heap size 340 MB, throughput 0.974519
Reading from 1585: heap size 333 MB, throughput 0.962906
Reading from 1585: heap size 333 MB, throughput 0.961719
Reading from 1586: heap size 337 MB, throughput 0.975332
Numeric result:
Recommendation: 2 clients, utility 0.608758:
    h1: 1209.47 MB (U(h) = 0.528628*h^0.0481919)
    h2: 2102.53 MB (U(h) = 0.430887*h^0.083779)
Recommendation: 2 clients, utility 0.608758:
    h1: 1209.44 MB (U(h) = 0.528628*h^0.0481919)
    h2: 2102.56 MB (U(h) = 0.430887*h^0.083779)
Reading from 1585: heap size 333 MB, throughput 0.96321
Reading from 1586: heap size 340 MB, throughput 0.978377
Reading from 1585: heap size 334 MB, throughput 0.964045
Reading from 1585: heap size 335 MB, throughput 0.967262
Reading from 1586: heap size 339 MB, throughput 0.97881
Reading from 1585: heap size 335 MB, throughput 0.967029
Reading from 1586: heap size 341 MB, throughput 0.975103
Reading from 1585: heap size 334 MB, throughput 0.969081
Reading from 1586: heap size 343 MB, throughput 0.972997
Reading from 1585: heap size 335 MB, throughput 0.969532
Reading from 1585: heap size 334 MB, throughput 0.968056
Reading from 1586: heap size 344 MB, throughput 0.972308
Reading from 1585: heap size 335 MB, throughput 0.971001
Reading from 1586: heap size 347 MB, throughput 0.919197
Reading from 1585: heap size 333 MB, throughput 0.921734
Reading from 1585: heap size 366 MB, throughput 0.635628
Reading from 1586: heap size 387 MB, throughput 0.976142
Numeric result:
Recommendation: 2 clients, utility 0.688866:
    h1: 1119.9 MB (U(h) = 0.511738*h^0.0565634)
    h2: 2192.1 MB (U(h) = 0.386086*h^0.110729)
Recommendation: 2 clients, utility 0.688866:
    h1: 1119.82 MB (U(h) = 0.511738*h^0.0565634)
    h2: 2192.18 MB (U(h) = 0.386086*h^0.110729)
Reading from 1585: heap size 363 MB, throughput 0.986063
Reading from 1586: heap size 390 MB, throughput 0.980425
Reading from 1585: heap size 367 MB, throughput 0.983755
Reading from 1585: heap size 370 MB, throughput 0.979213
Reading from 1585: heap size 370 MB, throughput 0.958248
Reading from 1586: heap size 391 MB, throughput 0.984628
Reading from 1585: heap size 370 MB, throughput 0.936589
Reading from 1585: heap size 370 MB, throughput 0.869154
Reading from 1585: heap size 375 MB, throughput 0.833443
Reading from 1585: heap size 379 MB, throughput 0.79681
Reading from 1585: heap size 387 MB, throughput 0.706817
Reading from 1586: heap size 391 MB, throughput 0.980628
Reading from 1586: heap size 392 MB, throughput 0.964255
Reading from 1585: heap size 390 MB, throughput 0.900066
Reading from 1586: heap size 388 MB, throughput 0.934823
Reading from 1586: heap size 393 MB, throughput 0.892811
Reading from 1586: heap size 400 MB, throughput 0.867603
Reading from 1585: heap size 397 MB, throughput 0.96303
Reading from 1586: heap size 404 MB, throughput 0.965362
Reading from 1585: heap size 399 MB, throughput 0.977619
Reading from 1586: heap size 409 MB, throughput 0.973787
Reading from 1585: heap size 398 MB, throughput 0.981959
Reading from 1586: heap size 412 MB, throughput 0.980725
Reading from 1585: heap size 402 MB, throughput 0.981124
Reading from 1585: heap size 400 MB, throughput 0.981811
Reading from 1586: heap size 410 MB, throughput 0.984701
Numeric result:
Recommendation: 2 clients, utility 0.862122:
    h1: 1334.12 MB (U(h) = 0.435884*h^0.09574)
    h2: 1977.88 MB (U(h) = 0.33817*h^0.141938)
Recommendation: 2 clients, utility 0.862122:
    h1: 1334.12 MB (U(h) = 0.435884*h^0.09574)
    h2: 1977.88 MB (U(h) = 0.33817*h^0.141938)
Reading from 1585: heap size 403 MB, throughput 0.982954
Reading from 1586: heap size 414 MB, throughput 0.984426
Reading from 1585: heap size 400 MB, throughput 0.982674
Reading from 1586: heap size 412 MB, throughput 0.98434
Reading from 1585: heap size 403 MB, throughput 0.984601
Reading from 1586: heap size 415 MB, throughput 0.982536
Reading from 1585: heap size 402 MB, throughput 0.981486
Reading from 1586: heap size 415 MB, throughput 0.982403
Reading from 1585: heap size 404 MB, throughput 0.983718
Reading from 1586: heap size 416 MB, throughput 0.982448
Reading from 1585: heap size 401 MB, throughput 0.981774
Reading from 1585: heap size 403 MB, throughput 0.978754
Reading from 1586: heap size 417 MB, throughput 0.981265
Numeric result:
Recommendation: 2 clients, utility 0.918917:
    h1: 1351.86 MB (U(h) = 0.419036*h^0.105222)
    h2: 1960.14 MB (U(h) = 0.323061*h^0.152568)
Recommendation: 2 clients, utility 0.918917:
    h1: 1351.86 MB (U(h) = 0.419036*h^0.105222)
    h2: 1960.14 MB (U(h) = 0.323061*h^0.152568)
Reading from 1586: heap size 418 MB, throughput 0.980259
Reading from 1585: heap size 405 MB, throughput 0.978945
Reading from 1585: heap size 405 MB, throughput 0.982926
Reading from 1586: heap size 420 MB, throughput 0.98612
Reading from 1585: heap size 406 MB, throughput 0.980363
Reading from 1585: heap size 408 MB, throughput 0.0168144
Reading from 1585: heap size 403 MB, throughput 0.940993
Reading from 1585: heap size 409 MB, throughput 0.909989
Reading from 1585: heap size 417 MB, throughput 0.893456
Reading from 1586: heap size 420 MB, throughput 0.983665
Reading from 1586: heap size 423 MB, throughput 0.971895
Reading from 1586: heap size 423 MB, throughput 0.949346
Reading from 1586: heap size 428 MB, throughput 0.934588
Reading from 1585: heap size 418 MB, throughput 0.978842
Reading from 1586: heap size 431 MB, throughput 0.971605
Reading from 1585: heap size 422 MB, throughput 0.985034
Reading from 1586: heap size 437 MB, throughput 0.9826
Reading from 1585: heap size 424 MB, throughput 0.985905
Reading from 1586: heap size 439 MB, throughput 0.985953
Reading from 1585: heap size 426 MB, throughput 0.985593
Numeric result:
Recommendation: 2 clients, utility 0.894624:
    h1: 1079.71 MB (U(h) = 0.464188*h^0.0805418)
    h2: 2232.29 MB (U(h) = 0.304069*h^0.16653)
Recommendation: 2 clients, utility 0.894624:
    h1: 1079.67 MB (U(h) = 0.464188*h^0.0805418)
    h2: 2232.33 MB (U(h) = 0.304069*h^0.16653)
Reading from 1586: heap size 441 MB, throughput 0.989267
Reading from 1585: heap size 428 MB, throughput 0.984873
Reading from 1586: heap size 443 MB, throughput 0.988599
Reading from 1585: heap size 426 MB, throughput 0.984986
Reading from 1585: heap size 429 MB, throughput 0.983901
Reading from 1586: heap size 441 MB, throughput 0.986721
Reading from 1585: heap size 429 MB, throughput 0.982951
Reading from 1586: heap size 444 MB, throughput 0.985965
Reading from 1585: heap size 430 MB, throughput 0.979779
Reading from 1586: heap size 445 MB, throughput 0.986135
Reading from 1585: heap size 432 MB, throughput 0.981758
Numeric result:
Recommendation: 2 clients, utility 0.94528:
    h1: 1146.4 MB (U(h) = 0.442199*h^0.091939)
    h2: 2165.6 MB (U(h) = 0.29467*h^0.173683)
Recommendation: 2 clients, utility 0.94528:
    h1: 1146.37 MB (U(h) = 0.442199*h^0.091939)
    h2: 2165.63 MB (U(h) = 0.29467*h^0.173683)
Reading from 1586: heap size 445 MB, throughput 0.984
Reading from 1585: heap size 432 MB, throughput 0.980759
Reading from 1585: heap size 434 MB, throughput 0.984755
Reading from 1585: heap size 435 MB, throughput 0.976535
Reading from 1586: heap size 448 MB, throughput 0.987954
Reading from 1585: heap size 435 MB, throughput 0.959701
Reading from 1585: heap size 438 MB, throughput 0.938864
Reading from 1586: heap size 448 MB, throughput 0.982832
Reading from 1586: heap size 449 MB, throughput 0.968831
Reading from 1585: heap size 445 MB, throughput 0.957682
Reading from 1586: heap size 451 MB, throughput 0.953399
Reading from 1586: heap size 459 MB, throughput 0.966436
Reading from 1585: heap size 447 MB, throughput 0.982632
Reading from 1586: heap size 460 MB, throughput 0.986525
Reading from 1585: heap size 452 MB, throughput 0.986771
Reading from 1586: heap size 466 MB, throughput 0.98944
Reading from 1585: heap size 453 MB, throughput 0.989717
Numeric result:
Recommendation: 2 clients, utility 1.01443:
    h1: 1233.13 MB (U(h) = 0.41287*h^0.107865)
    h2: 2078.87 MB (U(h) = 0.284188*h^0.181863)
Recommendation: 2 clients, utility 1.01443:
    h1: 1233.05 MB (U(h) = 0.41287*h^0.107865)
    h2: 2078.95 MB (U(h) = 0.284188*h^0.181863)
Reading from 1585: heap size 454 MB, throughput 0.988689
Reading from 1586: heap size 467 MB, throughput 0.989719
Reading from 1585: heap size 456 MB, throughput 0.98722
Reading from 1586: heap size 466 MB, throughput 0.985826
Reading from 1585: heap size 453 MB, throughput 0.986613
Reading from 1586: heap size 469 MB, throughput 0.985804
Reading from 1585: heap size 456 MB, throughput 0.985548
Reading from 1586: heap size 468 MB, throughput 0.987251
Reading from 1585: heap size 458 MB, throughput 0.984448
Numeric result:
Recommendation: 2 clients, utility 1.04472:
    h1: 1260.34 MB (U(h) = 0.401831*h^0.114087)
    h2: 2051.66 MB (U(h) = 0.279351*h^0.185707)
Recommendation: 2 clients, utility 1.04472:
    h1: 1260.39 MB (U(h) = 0.401831*h^0.114087)
    h2: 2051.61 MB (U(h) = 0.279351*h^0.185707)
Reading from 1586: heap size 470 MB, throughput 0.986072
Reading from 1585: heap size 458 MB, throughput 0.983307
Reading from 1586: heap size 472 MB, throughput 0.98951
Reading from 1585: heap size 460 MB, throughput 0.986218
Reading from 1585: heap size 461 MB, throughput 0.977981
Reading from 1586: heap size 473 MB, throughput 0.983452
Reading from 1585: heap size 462 MB, throughput 0.965306
Reading from 1586: heap size 473 MB, throughput 0.974035
Reading from 1585: heap size 464 MB, throughput 0.946063
Reading from 1586: heap size 476 MB, throughput 0.938782
Reading from 1586: heap size 483 MB, throughput 0.978137
Reading from 1585: heap size 472 MB, throughput 0.985838
Reading from 1585: heap size 473 MB, throughput 0.988308
Reading from 1586: heap size 484 MB, throughput 0.988115
Numeric result:
Recommendation: 2 clients, utility 1.83162:
    h1: 721.335 MB (U(h) = 0.384193*h^0.124313)
    h2: 2590.66 MB (U(h) = 0.0629539*h^0.446462)
Recommendation: 2 clients, utility 1.83162:
    h1: 721.346 MB (U(h) = 0.384193*h^0.124313)
    h2: 2590.65 MB (U(h) = 0.0629539*h^0.446462)
Reading from 1585: heap size 475 MB, throughput 0.988361
Reading from 1586: heap size 489 MB, throughput 0.989407
Reading from 1585: heap size 477 MB, throughput 0.985428
Reading from 1586: heap size 491 MB, throughput 0.99068
Reading from 1585: heap size 476 MB, throughput 0.986471
Reading from 1586: heap size 491 MB, throughput 0.989349
Reading from 1585: heap size 478 MB, throughput 0.986722
Reading from 1585: heap size 480 MB, throughput 0.986553
Reading from 1586: heap size 494 MB, throughput 0.988964
Numeric result:
Recommendation: 2 clients, utility 2.79383:
    h1: 885.504 MB (U(h) = 0.222011*h^0.221375)
    h2: 2426.5 MB (U(h) = 0.0247685*h^0.606648)
Recommendation: 2 clients, utility 2.79383:
    h1: 885.475 MB (U(h) = 0.222011*h^0.221375)
    h2: 2426.52 MB (U(h) = 0.0247685*h^0.606648)
Reading from 1585: heap size 480 MB, throughput 0.985408
Reading from 1586: heap size 496 MB, throughput 0.988213
Reading from 1585: heap size 483 MB, throughput 0.988001
Reading from 1585: heap size 484 MB, throughput 0.980434
Reading from 1585: heap size 485 MB, throughput 0.968476
Reading from 1586: heap size 497 MB, throughput 0.991183
Reading from 1586: heap size 499 MB, throughput 0.984388
Reading from 1585: heap size 487 MB, throughput 0.979166
Reading from 1586: heap size 500 MB, throughput 0.970296
Reading from 1586: heap size 507 MB, throughput 0.980607
Reading from 1585: heap size 495 MB, throughput 0.987603
Reading from 1586: heap size 509 MB, throughput 0.989241
Reading from 1585: heap size 496 MB, throughput 0.989837
Numeric result:
Recommendation: 2 clients, utility 2.93499:
    h1: 1688.84 MB (U(h) = 0.0537431*h^0.465253)
    h2: 1623.16 MB (U(h) = 0.06311*h^0.447158)
Recommendation: 2 clients, utility 2.93499:
    h1: 1688.84 MB (U(h) = 0.0537431*h^0.465253)
    h2: 1623.16 MB (U(h) = 0.06311*h^0.447158)
Reading from 1585: heap size 497 MB, throughput 0.989424
Reading from 1586: heap size 514 MB, throughput 0.990528
Reading from 1585: heap size 499 MB, throughput 0.988286
Reading from 1586: heap size 515 MB, throughput 0.991892
Reading from 1585: heap size 497 MB, throughput 0.988342
Reading from 1586: heap size 514 MB, throughput 0.9906
Reading from 1585: heap size 499 MB, throughput 0.988417
Numeric result:
Recommendation: 2 clients, utility 3.21012:
    h1: 1775.5 MB (U(h) = 0.0388321*h^0.520437)
    h2: 1536.5 MB (U(h) = 0.0618167*h^0.450382)
Recommendation: 2 clients, utility 3.21012:
    h1: 1775.5 MB (U(h) = 0.0388321*h^0.520437)
    h2: 1536.5 MB (U(h) = 0.0618167*h^0.450382)
Reading from 1586: heap size 517 MB, throughput 0.989888
Reading from 1585: heap size 502 MB, throughput 0.985951
Reading from 1585: heap size 502 MB, throughput 0.984655
Reading from 1585: heap size 507 MB, throughput 0.975894
Reading from 1585: heap size 509 MB, throughput 0.96795
Reading from 1586: heap size 519 MB, throughput 0.987824
Reading from 1586: heap size 520 MB, throughput 0.983987
Reading from 1586: heap size 524 MB, throughput 0.975061
Reading from 1586: heap size 528 MB, throughput 0.974582
Reading from 1585: heap size 516 MB, throughput 0.987028
Reading from 1585: heap size 516 MB, throughput 0.990179
Reading from 1586: heap size 536 MB, throughput 0.988974
Numeric result:
Recommendation: 2 clients, utility 3.45385:
    h1: 1679.77 MB (U(h) = 0.0386871*h^0.520962)
    h2: 1632.23 MB (U(h) = 0.0440685*h^0.506221)
Recommendation: 2 clients, utility 3.45385:
    h1: 1679.77 MB (U(h) = 0.0386871*h^0.520962)
    h2: 1632.23 MB (U(h) = 0.0440685*h^0.506221)
Reading from 1585: heap size 518 MB, throughput 0.990985
Reading from 1586: heap size 538 MB, throughput 0.989223
Reading from 1585: heap size 520 MB, throughput 0.991269
Reading from 1586: heap size 536 MB, throughput 0.988206
Reading from 1585: heap size 518 MB, throughput 0.98963
Reading from 1586: heap size 539 MB, throughput 0.987972
Reading from 1585: heap size 521 MB, throughput 0.989095
Numeric result:
Recommendation: 2 clients, utility 2.68444:
    h1: 1361.52 MB (U(h) = 0.110932*h^0.34545)
    h2: 1950.48 MB (U(h) = 0.0470773*h^0.494914)
Recommendation: 2 clients, utility 2.68444:
    h1: 1361.47 MB (U(h) = 0.110932*h^0.34545)
    h2: 1950.53 MB (U(h) = 0.0470773*h^0.494914)
Reading from 1586: heap size 542 MB, throughput 0.987545
Reading from 1585: heap size 524 MB, throughput 0.99018
Reading from 1585: heap size 524 MB, throughput 0.985951
Reading from 1585: heap size 524 MB, throughput 0.975658
Reading from 1586: heap size 542 MB, throughput 0.989649
Reading from 1586: heap size 544 MB, throughput 0.986524
Reading from 1585: heap size 526 MB, throughput 0.980409
Reading from 1586: heap size 546 MB, throughput 0.976188
Reading from 1586: heap size 552 MB, throughput 0.984738
Reading from 1585: heap size 534 MB, throughput 0.990024
Numeric result:
Recommendation: 2 clients, utility 2.99017:
    h1: 1484.05 MB (U(h) = 0.0727441*h^0.415624)
    h2: 1827.95 MB (U(h) = 0.0422549*h^0.511928)
Recommendation: 2 clients, utility 2.99017:
    h1: 1484.06 MB (U(h) = 0.0727441*h^0.415624)
    h2: 1827.94 MB (U(h) = 0.0422549*h^0.511928)
Reading from 1586: heap size 553 MB, throughput 0.990559
Reading from 1585: heap size 535 MB, throughput 0.990696
Reading from 1586: heap size 556 MB, throughput 0.991062
Reading from 1585: heap size 535 MB, throughput 0.990947
Reading from 1585: heap size 537 MB, throughput 0.989803
Reading from 1586: heap size 558 MB, throughput 0.983664
Reading from 1585: heap size 536 MB, throughput 0.987222
Numeric result:
Recommendation: 2 clients, utility 2.59493:
    h1: 1576.05 MB (U(h) = 0.0831789*h^0.39332)
    h2: 1735.95 MB (U(h) = 0.0680828*h^0.433212)
Recommendation: 2 clients, utility 2.59493:
    h1: 1576.07 MB (U(h) = 0.0831789*h^0.39332)
    h2: 1735.93 MB (U(h) = 0.0680828*h^0.433212)
Reading from 1586: heap size 571 MB, throughput 0.992565
Reading from 1585: heap size 538 MB, throughput 0.986101
Reading from 1586: heap size 573 MB, throughput 0.992724
Reading from 1585: heap size 537 MB, throughput 0.985291
Reading from 1585: heap size 542 MB, throughput 0.978478
Reading from 1585: heap size 545 MB, throughput 0.981841
Reading from 1586: heap size 580 MB, throughput 0.990153
Reading from 1586: heap size 581 MB, throughput 0.983002
Reading from 1586: heap size 586 MB, throughput 0.987411
Reading from 1585: heap size 546 MB, throughput 0.987957
Numeric result:
Recommendation: 2 clients, utility 2.61904:
    h1: 1833.44 MB (U(h) = 0.0549357*h^0.461423)
    h2: 1478.56 MB (U(h) = 0.0984102*h^0.372101)
Recommendation: 2 clients, utility 2.61904:
    h1: 1833.46 MB (U(h) = 0.0549357*h^0.461423)
    h2: 1478.54 MB (U(h) = 0.0984102*h^0.372101)
Reading from 1586: heap size 587 MB, throughput 0.991334
Reading from 1585: heap size 550 MB, throughput 0.989688
Reading from 1585: heap size 551 MB, throughput 0.989307
Reading from 1586: heap size 587 MB, throughput 0.991435
Reading from 1585: heap size 549 MB, throughput 0.982289
Reading from 1586: heap size 590 MB, throughput 0.991132
Numeric result:
Recommendation: 2 clients, utility 2.40622:
    h1: 1885.3 MB (U(h) = 0.0635763*h^0.437341)
    h2: 1426.7 MB (U(h) = 0.126366*h^0.330958)
Recommendation: 2 clients, utility 2.40622:
    h1: 1885.3 MB (U(h) = 0.0635763*h^0.437341)
    h2: 1426.7 MB (U(h) = 0.126366*h^0.330958)
Reading from 1585: heap size 553 MB, throughput 0.993338
Reading from 1586: heap size 589 MB, throughput 0.991879
Reading from 1585: heap size 553 MB, throughput 0.993371
Reading from 1585: heap size 556 MB, throughput 0.988164
Reading from 1585: heap size 556 MB, throughput 0.978576
Reading from 1586: heap size 591 MB, throughput 0.993909
Reading from 1586: heap size 593 MB, throughput 0.987754
Reading from 1586: heap size 595 MB, throughput 0.980754
Reading from 1585: heap size 558 MB, throughput 0.988492
Reading from 1586: heap size 607 MB, throughput 0.989926
Numeric result:
Recommendation: 2 clients, utility 2.02106:
    h1: 1927.37 MB (U(h) = 0.0971936*h^0.367917)
    h2: 1384.63 MB (U(h) = 0.190138*h^0.264308)
Recommendation: 2 clients, utility 2.02106:
    h1: 1927.39 MB (U(h) = 0.0971936*h^0.367917)
    h2: 1384.61 MB (U(h) = 0.190138*h^0.264308)
Reading from 1585: heap size 565 MB, throughput 0.991158
Reading from 1586: heap size 606 MB, throughput 0.991539
Reading from 1585: heap size 568 MB, throughput 0.99203
Reading from 1585: heap size 567 MB, throughput 0.990945
Reading from 1586: heap size 607 MB, throughput 0.991614
Numeric result:
Recommendation: 2 clients, utility 2.07884:
    h1: 1972.2 MB (U(h) = 0.0853834*h^0.388848)
    h2: 1339.8 MB (U(h) = 0.190209*h^0.264142)
Recommendation: 2 clients, utility 2.07884:
    h1: 1972.26 MB (U(h) = 0.0853834*h^0.388848)
    h2: 1339.74 MB (U(h) = 0.190209*h^0.264142)
Reading from 1585: heap size 570 MB, throughput 0.991113
Reading from 1586: heap size 610 MB, throughput 0.990988
Reading from 1585: heap size 574 MB, throughput 0.992405
Reading from 1585: heap size 575 MB, throughput 0.987089
Reading from 1585: heap size 574 MB, throughput 0.978931
Reading from 1586: heap size 616 MB, throughput 0.992167
Reading from 1586: heap size 617 MB, throughput 0.988794
Reading from 1586: heap size 618 MB, throughput 0.985472
Reading from 1585: heap size 578 MB, throughput 0.988537
Numeric result:
Recommendation: 2 clients, utility 2.0259:
    h1: 2097.28 MB (U(h) = 0.0812979*h^0.396547)
    h2: 1214.72 MB (U(h) = 0.234914*h^0.229679)
Recommendation: 2 clients, utility 2.0259:
    h1: 2097.27 MB (U(h) = 0.0812979*h^0.396547)
    h2: 1214.73 MB (U(h) = 0.234914*h^0.229679)
Reading from 1586: heap size 621 MB, throughput 0.993574
Reading from 1585: heap size 585 MB, throughput 0.99061
Reading from 1585: heap size 588 MB, throughput 0.99089
Reading from 1586: heap size 624 MB, throughput 0.994389
Reading from 1585: heap size 588 MB, throughput 0.990414
Reading from 1586: heap size 626 MB, throughput 0.994369
Numeric result:
Recommendation: 2 clients, utility 1.85412:
    h1: 2187.72 MB (U(h) = 0.10042*h^0.362393)
    h2: 1124.28 MB (U(h) = 0.307424*h^0.186239)
Recommendation: 2 clients, utility 1.85412:
    h1: 2187.71 MB (U(h) = 0.10042*h^0.362393)
    h2: 1124.29 MB (U(h) = 0.307424*h^0.186239)
Reading from 1585: heap size 591 MB, throughput 0.990207
Reading from 1586: heap size 627 MB, throughput 0.992446
Reading from 1585: heap size 595 MB, throughput 0.991576
Reading from 1585: heap size 596 MB, throughput 0.986044
Reading from 1585: heap size 598 MB, throughput 0.985714
Reading from 1586: heap size 628 MB, throughput 0.993362
Reading from 1586: heap size 630 MB, throughput 0.989864
Reading from 1586: heap size 632 MB, throughput 0.98986
Reading from 1585: heap size 600 MB, throughput 0.991071
Numeric result:
Recommendation: 2 clients, utility 1.85842:
    h1: 2926.29 MB (U(h) = 0.0811202*h^0.39639)
    h2: 385.711 MB (U(h) = 0.709356*h^0.0522521)
Recommendation: 2 clients, utility 1.85842:
    h1: 2926.26 MB (U(h) = 0.0811202*h^0.39639)
    h2: 385.74 MB (U(h) = 0.709356*h^0.0522521)
Reading from 1586: heap size 639 MB, throughput 0.992162
Reading from 1585: heap size 603 MB, throughput 0.991926
Reading from 1586: heap size 602 MB, throughput 0.99222
Reading from 1585: heap size 605 MB, throughput 0.992927
Numeric result:
Recommendation: 2 clients, utility 1.9477:
    h1: 2926.31 MB (U(h) = 0.0681187*h^0.424301)
    h2: 385.686 MB (U(h) = 0.693205*h^0.055923)
Recommendation: 2 clients, utility 1.9477:
    h1: 2926.31 MB (U(h) = 0.0681187*h^0.424301)
    h2: 385.689 MB (U(h) = 0.693205*h^0.055923)
Reading from 1586: heap size 593 MB, throughput 0.992923
Reading from 1585: heap size 606 MB, throughput 0.992122
Reading from 1586: heap size 583 MB, throughput 0.991345
Reading from 1585: heap size 608 MB, throughput 0.992885
Reading from 1585: heap size 610 MB, throughput 0.988549
Reading from 1585: heap size 611 MB, throughput 0.987916
Reading from 1586: heap size 576 MB, throughput 0.99112
Reading from 1586: heap size 567 MB, throughput 0.985858
Reading from 1586: heap size 558 MB, throughput 0.985083
Reading from 1585: heap size 620 MB, throughput 0.991859
Numeric result:
Recommendation: 2 clients, utility 2.14794:
    h1: 2926.9 MB (U(h) = 0.0467934*h^0.483914)
    h2: 385.101 MB (U(h) = 0.660343*h^0.063671)
Recommendation: 2 clients, utility 2.14794:
    h1: 2926.89 MB (U(h) = 0.0467934*h^0.483914)
    h2: 385.106 MB (U(h) = 0.660343*h^0.063671)
Reading from 1586: heap size 528 MB, throughput 0.992238
Reading from 1585: heap size 621 MB, throughput 0.992272
Reading from 1586: heap size 540 MB, throughput 0.990858
Reading from 1586: heap size 518 MB, throughput 0.990791
Reading from 1585: heap size 621 MB, throughput 0.991648
Reading from 1586: heap size 522 MB, throughput 0.988007
Numeric result:
Recommendation: 2 clients, utility 2.2131:
    h1: 2895.26 MB (U(h) = 0.0414009*h^0.503287)
    h2: 416.743 MB (U(h) = 0.625135*h^0.0724451)
Recommendation: 2 clients, utility 2.2131:
    h1: 2895.25 MB (U(h) = 0.0414009*h^0.503287)
    h2: 416.753 MB (U(h) = 0.625135*h^0.0724451)
Reading from 1585: heap size 623 MB, throughput 0.990944
Reading from 1586: heap size 509 MB, throughput 0.987625
Reading from 1585: heap size 627 MB, throughput 0.991155
Reading from 1585: heap size 628 MB, throughput 0.98565
Client 1585 died
Clients: 1
Reading from 1586: heap size 511 MB, throughput 0.987764
Reading from 1586: heap size 511 MB, throughput 0.987128
Reading from 1586: heap size 505 MB, throughput 0.980971
Reading from 1586: heap size 495 MB, throughput 0.975411
Client 1586 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
