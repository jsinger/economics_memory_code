#!/bin/bash
# The lab machines have cgroups in a weird place...

sudo mkdir -p /cgroup
sudo mount -t tmpfs cgroup_root /cgroup
sudo mkdir -p /cgroup/memory
sudo mount -t cgroup -o memory hier1 /cgroup/memory
