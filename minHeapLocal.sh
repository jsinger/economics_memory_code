#!/bin/bash
# Calculate the min heap values for the DaCapo benchmarks

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DACAPO='dacapo-9.12-bach.jar'

"${DIR}/checkDacapo.sh" || exit 1

ME="$(whoami)@$(hostname)"
GLOBAL_MAX_THREADS='4'
OUTDIR="${DIR}/minHeap_${ME}_$(date '+%Y_%m_%d_%H_%M_%S')"
mkdir -p "${OUTDIR}"


DACAPODIR="$(readlink -f "$(mktemp -d)")"
cp "${DIR}/${DACAPO}" "${DACAPODIR}/${DACAPO}"
cd "${DACAPODIR}"
unzip "${DACAPO}"

cd 'cnf'

# Any benchmarks to ignore get rm'd here
# batik doesn't work in Java 7
rm batik.cnf
rm tradebeans.cnf
rm tradesoap.cnf

INPUT="${OUTDIR}/input.txt"

for i in *.cnf; do
    FILE="$(readlink -f "${i}")"
    NAME="$(grep benchmark "${FILE}" | cut '--delimiter= ' -f 2)"

    if grep 'thread-model per_cpu' "${FILE}" >/dev/null; then
        MAX_THREADS="${GLOBAL_MAX_THREADS}"
    else
        MAX_THREADS='1'
    fi

    grep 'size ' "${FILE}" | cut '--delimiter= ' -f 2 | while read line; do
        j='1'
        while (( j <= MAX_THREADS )); do
            echo "${NAME} ${line} ${j}" >> "${INPUT}"
            j=$(( j + 1 ))
        done
    done
done

cd
rm -r "${DACAPODIR}"

RESULTS="${OUTDIR}/results_${ME}.txt"

while read name size threads; do
    "${DIR}/minHeap_benchmark.sh" "${name}" "${size}" "${threads}" "${RESULTS}"
done < "${INPUT}"
