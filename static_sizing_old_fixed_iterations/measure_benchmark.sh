#!/bin/bash
# Measure the average runtime of a single benchmark/heap size combination
#
# Usage: measure_benchmark.sh benchmark heap_size iterations max_heap out_dir n_for_nover results_file
#
#    benchmark: name of the DaCapo benchmark to run
#    heap_size: in MB
#    iterations: how many times to run the benchmark (DaCapo's -n parameter)
#    max_heap: biggest heap size used in any run; for pretty-printing
#    out_dir: where to save the output
#    n_for_nover
#    results_file: running log file for all the results
#

function usage()
{
    echo "Usage: $(basename "${0}") benchmark heap_size iterations max_heap out_dir n_for_nover results_file"
    exit 1
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DACAPO="${DIR}/../dacapo-9.12-bach.jar"

test -z "${1}" && usage
test -z "${2}" && usage
test -z "${3}" && usage
test -z "${4}" && usage
test -z "${5}" && usage
test ! -d "${5}" && usage
test -z "${6}" && usage
test -z "${7}" && usage

BENCHMARK="${1}"
HEAP_SIZE="${2}"
ITERATIONS="${3}"
MAX_HEAP="${4}"
OUTDIR="$(readlink -f "${5}")"
N_FOR_NOVER="${6}"
RESULTS_FILE="$(readlink -f "${7}")"

PRETTY_HEAP_SIZE="$(printf "%0${#MAX_HEAP}d" "${HEAP_SIZE}")"
RAW_FILE="${OUTDIR}/${BENCHMARK}_${PRETTY_HEAP_SIZE}_raw.txt"
PROCESSED_FILE="${OUTDIR}/${BENCHMARK}_${PRETTY_HEAP_SIZE}.txt"

echo "${BENCHMARK}, heap: ${PRETTY_HEAP_SIZE} MB, ${ITERATIONS} iterations"

SCRATCH="$(readlink -f "$(mktemp -d -p "${OUTDIR}")")"

java "-Xms${HEAP_SIZE}m" "-Xmx${HEAP_SIZE}m" -jar "${DACAPO}" --scratch-directory "${SCRATCH}" -s large -n "${ITERATIONS}" "${BENCHMARK}" 2> "${RAW_FILE}" || exit 1

rm -r "${SCRATCH}"

grep -o '[[:digit:]]\+ msec' "${RAW_FILE}" | cut '--delimiter= ' -f 1 > "${PROCESSED_FILE}"

AVERAGE="$(python "${DIR}/../average.py" 1000 < "${PROCESSED_FILE}")"
STDDEV="$(python "${DIR}/stddev.py" < "${PROCESSED_FILE}")"
THROUGHPUT="$(echo "${AVERAGE}" | python "${DIR}/nOver.py" "${N_FOR_NOVER}")"

echo "${HEAP_SIZE} ${AVERAGE} ${STDDEV} ${THROUGHPUT}" >> "${RESULTS_FILE}"
