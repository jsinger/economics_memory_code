#ifndef HEAP_HPP
#define HEAP_HPP

#include <cstddef>

class Heap
{
public:
    virtual ~Heap() {}
    virtual void alloc(unsigned long long bytes) = 0;
    virtual unsigned long long getCapacityMB() const = 0;
};

#endif
