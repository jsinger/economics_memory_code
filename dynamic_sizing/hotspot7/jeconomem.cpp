#include "precompiled.hpp"

#include "classfile/symbolTable.hpp"
#include "classfile/systemDictionary.hpp"
#include "code/codeCache.hpp"
#include "gc_implementation/parallelScavenge/cardTableExtension.hpp"
#include "gc_implementation/parallelScavenge/gcTaskManager.hpp"
#include "gc_implementation/parallelScavenge/generationSizer.hpp"
#include "gc_implementation/parallelScavenge/parallelScavengeHeap.hpp"
#include "gc_implementation/parallelScavenge/parallelScavengeHeap.inline.hpp"
#include "gc_implementation/parallelScavenge/pcTasks.hpp"
#include "gc_implementation/parallelScavenge/psAdaptiveSizePolicy.hpp"
#include "gc_implementation/parallelScavenge/psCompactionManager.inline.hpp"
#include "gc_implementation/parallelScavenge/psGCAdaptivePolicyCounters.hpp"
#include "gc_implementation/parallelScavenge/psMarkSweepDecorator.hpp"
#include "gc_implementation/parallelScavenge/psMarkSweep.hpp"
#include "gc_implementation/parallelScavenge/psOldGen.hpp"
#include "gc_implementation/parallelScavenge/psParallelCompact.hpp"
#include "gc_implementation/parallelScavenge/psPermGen.hpp"
#include "gc_implementation/parallelScavenge/psPromotionManager.inline.hpp"
#include "gc_implementation/parallelScavenge/psScavenge.hpp"
#include "gc_implementation/parallelScavenge/psScavenge.inline.hpp"
#include "gc_implementation/parallelScavenge/psTasks.hpp"
#include "gc_implementation/parallelScavenge/psYoungGen.hpp"
#include "gc_implementation/shared/adaptiveSizePolicy.hpp"
#include "gc_implementation/shared/gcHeapSummary.hpp"
#include "gc_implementation/shared/gcPolicyCounters.hpp"
#include "gc_implementation/shared/gcTimer.hpp"
#include "gc_implementation/shared/gcTrace.hpp"
#include "gc_implementation/shared/gcTraceTime.hpp"
#include "gc_implementation/shared/isGCActiveMark.hpp"
#include "gc_implementation/shared/spaceDecorator.hpp"
#include "gc_interface/gcCause.hpp"
#include "memory/collectorPolicy.hpp"
#include "memory/gcLocker.inline.hpp"
#include "memory/referencePolicy.hpp"
#include "memory/referenceProcessor.hpp"
#include "memory/resourceArea.hpp"
#include "oops/methodDataOop.hpp"
#include "oops/oop.inline.hpp"
#include "oops/oop.pcgc.inline.hpp"
#include "oops/oop.psgc.inline.hpp"
#include "runtime/biasedLocking.hpp"
#include "runtime/fprofiler.hpp"
#include "runtime/handles.inline.hpp"
#include "runtime/safepoint.hpp"
#include "runtime/threadCritical.hpp"
#include "runtime/timer.hpp"
#include "runtime/vm_operations.hpp"
#include "runtime/vmThread.hpp"
#include "services/management.hpp"
#include "services/memoryService.hpp"
#include "services/memTracker.hpp"
#include "utilities/dtrace.hpp"
#include "utilities/events.hpp"
#include "utilities/ostream.hpp"
#include "utilities/stack.inline.hpp"
#include "utilities/top.hpp"
#include "utilities/workgroup.hpp"

#include <math.h>

#include <Messages.hpp>
#include <DaemonProxy.hpp>
#include <ReadingSender.hpp>
#include "jeconomem.hpp"
#include "HotspotSimpleThroughput.hpp"

#define AVG_WEIGHT 50.0F
//#define TRACE
//#define GC_LOGGING

#ifdef TRACE
static int s_Indent = 0;
#define TAB_SIZE 4
#define TRACE_PREFIX "##    "

#define TRACE_IN() do { printf(TRACE_PREFIX " %*s%s {\n", s_Indent * TAB_SIZE, "", __FUNCTION__); s_Indent++; } while (0)
#define TRACE_OUT() do { s_Indent--; printf(TRACE_PREFIX " %*s%s }\n", s_Indent * TAB_SIZE, "", __FUNCTION__); } while (0)
#else
#define TRACE_IN()
#define TRACE_OUT()
#endif

#ifdef GC_LOGGING
#define GC_LOG printf
#else
#define GC_LOG
#endif

static economem::Recommendation s_Recommendation;
static bool s_HasRecommendation = false;

static economem::DaemonProxy* s_Proxy = NULL;

void JEconoMem::daemon_proxy_cleanup()
{
    delete s_Proxy;
}

size_t JEconoMem::heap_size(PSAdaptiveSizePolicy* that)
{
    return that->_eden_size + that->_survivor_size + that->_promo_size;
}

void JEconoMem::gc_start(PSAdaptiveSizePolicy* that)
{
    TRACE_IN();
    GC_LOG("## Heap size: %lu\n", heap_size(that));
    if (s_Proxy->getRecommendation(s_Recommendation))
    {
        s_HasRecommendation = true;
        GC_LOG("## ############################\n");
        GC_LOG("## New GC recommendation: %lu\n", s_Recommendation.getHeapSizeBytes());
    }

    if (s_HasRecommendation)
        GC_LOG("## GC recommendation: %lu\n", s_Recommendation.getHeapSizeBytes());

    s_Proxy->gcBegin();
    TRACE_OUT();
}

void JEconoMem::gc_end(PSAdaptiveSizePolicy* that)
{
    TRACE_IN();
    s_Proxy->minorGCEnd();
    TRACE_OUT();
}

void JEconoMem::psasp_created(PSAdaptiveSizePolicy* that)
{
    TRACE_IN();
    s_Proxy = new economem::DaemonProxy;
    s_Proxy->addTarget(new HotspotSimpleThroughput, new economem::ReadingSender);
    TRACE_OUT();
}

void JEconoMem::psasp_major_collection_begin(PSAdaptiveSizePolicy* that)
{
    TRACE_IN();
    // Update the interval time
    that->_major_timer.stop();
    // Save most recent collection time
    that->_latest_major_mutator_interval_seconds = that->_major_timer.seconds();
    that->_major_timer.reset();
    that->_major_timer.start();
    TRACE_OUT();
}

void JEconoMem::psasp_update_minor_pause_old_estimator(PSAdaptiveSizePolicy* that, double minor_pause_in_ms)
{
    TRACE_IN();
    double promo_size_in_mbytes = ((double)(that->_promo_size))/((double)(M));
    that->_minor_pause_old_estimator->update(promo_size_in_mbytes, minor_pause_in_ms);
    TRACE_OUT();
}

void JEconoMem::psasp_major_collection_end(PSAdaptiveSizePolicy* that, size_t amount_live, GCCause::Cause gc_cause)
{
    TRACE_IN();
    // Update the pause time.
    that->_major_timer.stop();
    
    if (gc_cause != GCCause::_java_lang_system_gc ||
        UseAdaptiveSizePolicyWithSystemGC) {
        double major_pause_in_seconds = that->_major_timer.seconds();
        double major_pause_in_ms = major_pause_in_seconds * MILLIUNITS;

        // Sample for performance counter
        that->_avg_major_pause->sample(major_pause_in_seconds);
        
        // Cost of collection (unit-less)
        double collection_cost = 0.0;
        if ((that->_latest_major_mutator_interval_seconds > 0.0) &&
            (major_pause_in_seconds > 0.0)) {
            double interval_in_seconds =
                that->_latest_major_mutator_interval_seconds + major_pause_in_seconds;
            collection_cost =
                major_pause_in_seconds / interval_in_seconds;
            that->avg_major_gc_cost()->sample(collection_cost);
            
            // Sample for performance counter
            that->_avg_major_interval->sample(interval_in_seconds);
        }
        
        // Calculate variables used to estimate pause time vs. gen sizes
        double eden_size_in_mbytes = ((double)that->_eden_size)/((double)M);
        double promo_size_in_mbytes = ((double)that->_promo_size)/((double)M);
        that->_major_pause_old_estimator->update(promo_size_in_mbytes,
                                                 major_pause_in_ms);
        that->_major_pause_young_estimator->update(eden_size_in_mbytes,
                                                   major_pause_in_ms);
        
        if (PrintAdaptiveSizePolicy && Verbose) {
            gclog_or_tty->print("psAdaptiveSizePolicy::major_collection_end: "
                                "major gc cost: %f  average: %f", collection_cost,
                                that->avg_major_gc_cost()->average());
            gclog_or_tty->print_cr("  major pause: %f major period %f",
                                   major_pause_in_ms,
                                   that->_latest_major_mutator_interval_seconds * MILLIUNITS);
        }
        
        // Calculate variable used to estimate collection cost vs. gen sizes
        assert(collection_cost >= 0.0, "Expected to be non-negative");
        that->_major_collection_estimator->update(promo_size_in_mbytes,
                                                  collection_cost);
    }
    
    // Update the amount live at the end of a full GC
    that->_live_at_last_full_gc = amount_live;
    
    // The policy does not have enough data until at least some major collections
    // have been done.
    if (that->_avg_major_pause->count() >= AdaptiveSizePolicyReadyThreshold) {
        that->_old_gen_policy_is_ready = true;
    }
    
    // Interval times use this timer to measure the interval that
    // the mutator runs.  Reset after the GC pause has been measured.
    that->_major_timer.reset();
    that->_major_timer.start();
    TRACE_OUT();
}

bool JEconoMem::psasp_should_full_GC(PSAdaptiveSizePolicy* that, size_t old_free_in_bytes)
{
    TRACE_IN();
    // A similar test is done in the scavenge's should_attempt_scavenge().  If
    // this is changed, decide if that test should also be changed.
    bool result = that->padded_average_promoted_in_bytes() > (float) old_free_in_bytes;
    if (PrintGCDetails && Verbose) {
        if (result) {
            gclog_or_tty->print("  full after scavenge: ");
        } else {
            gclog_or_tty->print("  no full after scavenge: ");
        }
        gclog_or_tty->print_cr(" average_promoted " SIZE_FORMAT
                               " padded_average_promoted " SIZE_FORMAT
                               " free in old gen " SIZE_FORMAT,
                               (size_t) that->average_promoted_in_bytes(),
                               (size_t) that->padded_average_promoted_in_bytes(),
                               old_free_in_bytes);
    }
    TRACE_OUT();
    return result;
}

void JEconoMem::psasp_clear_generation_free_space_flags(PSAdaptiveSizePolicy* that)
{
    TRACE_IN();
    that->AdaptiveSizePolicy::clear_generation_free_space_flags();
    
    that->set_change_old_gen_for_min_pauses(0);
    
    that->set_change_young_gen_for_maj_pauses(0);
    TRACE_OUT();
}

void JEconoMem::psasp_compute_generation_free_space(PSAdaptiveSizePolicy* that,
                                                   size_t young_live,
                                                   size_t eden_live,
                                                   size_t old_live,
                                                   size_t perm_live,
                                                   size_t cur_eden,  // current eden in bytes
                                                   size_t max_old_gen_size,
                                                   size_t max_eden_size,
                                                   bool   is_full_gc,
                                                   GCCause::Cause gc_cause,
                                                   CollectorPolicy* collector_policy)
{
    TRACE_IN();
    // Update statistics
    // Time statistics are updated as we go, update footprint stats here
    that->_avg_base_footprint->sample(BaseFootPrintEstimate + perm_live);
    
    that->avg_young_live()->sample(young_live);
    that->avg_eden_live()->sample(eden_live);
    if (is_full_gc) {
        // old_live is only accurate after a full gc
        that->avg_old_live()->sample(old_live);
    }
    
    // This code used to return if the policy was not ready , i.e.,
    // policy_is_ready() returning false.  The intent was that
    // decisions below needed major collection times and so could
    // not be made before two major collections.  A consequence was
    // adjustments to the young generation were not done until after
    // two major collections even if the minor collections times
    // exceeded the requested goals.  Now let the young generation
    // adjust for the minor collection times.  Major collection times
    // will be zero for the first collection and will naturally be
    // ignored.  Tenured generation adjustments are only made at the
    // full collections so until the second major collection has
    // been reached, no tenured generation adjustments will be made.
    
    // Until we know better, desired promotion size uses the last calculation
    size_t desired_promo_size = that->_promo_size;
    
    // Start eden at the current value.  The desired value that is stored
    // in _eden_size is not bounded by constraints of the heap and can
    // run away.
    //
    // As expected setting desired_eden_size to the current
    // value of desired_eden_size as a starting point
    // caused desired_eden_size to grow way too large and caused
    // an overflow down stream.  It may have improved performance in
    // some case but is dangerous.
    size_t desired_eden_size = cur_eden;
    
#ifdef ASSERT
    size_t original_promo_size = desired_promo_size;
    size_t original_eden_size = desired_eden_size;
#endif
    
    // Cache some values. There's a bit of work getting these, so
    // we might save a little time.
    const double major_cost = that->major_gc_cost();
    const double minor_cost = that->minor_gc_cost();
    
    // Used for diagnostics
    that->clear_generation_free_space_flags();
    
    // Limits on our growth
    size_t promo_limit = (size_t)(max_old_gen_size - that->avg_old_live()->average());
    
    // This method sets the desired eden size.  That plus the
    // desired survivor space sizes sets the desired young generation
    // size.  This methods does not know what the desired survivor
    // size is but expects that other policy will attempt to make
    // the survivor sizes compatible with the live data in the
    // young generation.  This limit is an estimate of the space left
    // in the young generation after the survivor spaces have been
    // subtracted out.
    size_t eden_limit = max_eden_size;
    
    // But don't force a promo size below the current promo size. Otherwise,
    // the promo size will shrink for no good reason.
    promo_limit = MAX2(promo_limit, that->_promo_size);
    
    const double gc_cost_limit = GCTimeLimit/100.0;
    
    // Which way should we go?
    // if pause requirement is not met
    //   adjust size of any generation with average pause exceeding
    //   the pause limit.  Adjust one pause at a time (the larger)
    //   and only make adjustments for the major pause at full collections.
    // else if throughput requirement not met
    //   adjust the size of the generation with larger gc time.  Only
    //   adjust one generation at a time.
    // else
    //   adjust down the total heap size.  Adjust down the larger of the
    //   generations.
    
    // Add some checks for a threshhold for a change.  For example,
    // a change less than the necessary alignment is probably not worth
    // attempting.
    
///////////////////////////////////////////////////////////////////////////////////////////@

    if (s_HasRecommendation && heap_size(that) > s_Recommendation.getHeapSizeBytes())
    {
        GC_LOG("## GC recommendation: %lu; shrinking heap...\n", s_Recommendation.getHeapSizeBytes());

        size_t desired_sum = s_Recommendation.getHeapSizeBytes();
        desired_eden_size = that->adjust_eden_for_footprint(desired_eden_size, desired_sum);
        if (is_full_gc) {
            that->set_decide_at_full_gc(that->decide_at_full_gc_true);
            desired_promo_size = that->adjust_promo_for_footprint(desired_promo_size, desired_sum);
        }
    }
    else if ((that->_avg_minor_pause->padded_average() > that->gc_pause_goal_sec()) ||
        (that->_avg_major_pause->padded_average() > that->gc_pause_goal_sec())) {
        //
        // Check pauses
        //
        // Make changes only to affect one of the pauses (the larger)
        // at a time.
        that->adjust_for_pause_time(is_full_gc, &desired_promo_size, &desired_eden_size);
        
    } else if (that->_avg_minor_pause->padded_average() > that->gc_minor_pause_goal_sec()) {
        // Adjust only for the minor pause time goal
        that->adjust_for_minor_pause_time(is_full_gc, &desired_promo_size, &desired_eden_size);
        
    } else if(that->adjusted_mutator_cost() < that->_throughput_goal) {
        // This branch used to require that (mutator_cost() > 0.0 in 1.4.2.
        // This sometimes resulted in skipping to the minimize footprint
        // code.  Change this to try and reduce GC time if mutator time is
        // negative for whatever reason.  Or for future consideration,
        // bail out of the code if mutator time is negative.
        //
        // Throughput
        //
        assert(major_cost >= 0.0, "major cost is < 0.0");
        assert(minor_cost >= 0.0, "minor cost is < 0.0");
        // Try to reduce the GC times.
        that->adjust_for_throughput(is_full_gc, &desired_promo_size, &desired_eden_size);
        
    } else {
        
        // Be conservative about reducing the footprint.
        //   Do a minimum number of major collections first.
        //   Have reasonable averages for major and minor collections costs.
        if (UseAdaptiveSizePolicyFootprintGoal &&
            that->young_gen_policy_is_ready() &&
            that->avg_major_gc_cost()->average() >= 0.0 &&
            that->avg_minor_gc_cost()->average() >= 0.0) {
            size_t desired_sum = desired_eden_size + desired_promo_size;
            desired_eden_size = that->adjust_eden_for_footprint(desired_eden_size,
                                                                desired_sum);
            if (is_full_gc) {
                that->set_decide_at_full_gc(that->decide_at_full_gc_true);
                desired_promo_size = that->adjust_promo_for_footprint(desired_promo_size,
                                                                      desired_sum);
            }
        }
    }

///////////////////////////////////////////////////////////////////////////////////////////@
    
    // Note we make the same tests as in the code block below;  the code
    // seems a little easier to read with the printing in another block.
    if (PrintAdaptiveSizePolicy) {
        if (desired_promo_size > promo_limit)  {
            // "free_in_old_gen" was the original value for used for promo_limit
            size_t free_in_old_gen = (size_t)(max_old_gen_size - that->avg_old_live()->average());
            gclog_or_tty->print_cr(
                "PSAdaptiveSizePolicy::compute_generation_free_space limits:"
                " desired_promo_size: " SIZE_FORMAT
                " promo_limit: " SIZE_FORMAT
                " free_in_old_gen: " SIZE_FORMAT
                " max_old_gen_size: " SIZE_FORMAT
                " avg_old_live: " SIZE_FORMAT,
                desired_promo_size, promo_limit, free_in_old_gen,
                max_old_gen_size, (size_t) that->avg_old_live()->average());
        }
        if (desired_eden_size > eden_limit) {
            gclog_or_tty->print_cr(
                "AdaptiveSizePolicy::compute_generation_free_space limits:"
                " desired_eden_size: " SIZE_FORMAT
                " old_eden_size: " SIZE_FORMAT
                " eden_limit: " SIZE_FORMAT
                " cur_eden: " SIZE_FORMAT
                " max_eden_size: " SIZE_FORMAT
                " avg_young_live: " SIZE_FORMAT,
                desired_eden_size, that->_eden_size, eden_limit, cur_eden,
                max_eden_size, (size_t)that->avg_young_live()->average());
        }
        if (that->gc_cost() > gc_cost_limit) {
            gclog_or_tty->print_cr(
                "AdaptiveSizePolicy::compute_generation_free_space: gc time limit"
                " gc_cost: %f "
                " GCTimeLimit: %d",
                that->gc_cost(), GCTimeLimit);
        }
    }
    
    // Align everything and make a final limit check
    const size_t alignment = that->_intra_generation_alignment;
    desired_eden_size  = align_size_up(desired_eden_size, alignment);
    desired_eden_size  = MAX2(desired_eden_size, alignment);
    desired_promo_size = align_size_up(desired_promo_size, alignment);
    desired_promo_size = MAX2(desired_promo_size, alignment);
    
    eden_limit  = align_size_down(eden_limit, alignment);
    promo_limit = align_size_down(promo_limit, alignment);
    
    // Is too much time being spent in GC?
    //   Is the heap trying to grow beyond it's limits?
    
    const size_t free_in_old_gen =
        (size_t)(max_old_gen_size - that->avg_old_live()->average());
    if (desired_promo_size > free_in_old_gen && desired_eden_size > eden_limit) {
        that->check_gc_overhead_limit(young_live,
                                      eden_live,
                                      max_old_gen_size,
                                      max_eden_size,
                                      is_full_gc,
                                      gc_cause,
                                      collector_policy);
    }

    
    // And one last limit check, now that we've aligned things.
    if (desired_eden_size > eden_limit) {
        // If the policy says to get a larger eden but
        // is hitting the limit, don't decrease eden.
        // This can lead to a general drifting down of the
        // eden size.  Let the tenuring calculation push more
        // into the old gen.
        desired_eden_size = MAX2(eden_limit, cur_eden);
    }
    desired_promo_size = MIN2(desired_promo_size, promo_limit);
    
    
    if (PrintAdaptiveSizePolicy) {
        // Timing stats
        gclog_or_tty->print(
            "PSAdaptiveSizePolicy::compute_generation_free_space: costs"
            " minor_time: %f"
            " major_cost: %f"
            " mutator_cost: %f"
            " throughput_goal: %f",
            that->minor_gc_cost(), that->major_gc_cost(), that->mutator_cost(),
            that->_throughput_goal);
        
        // We give more details if Verbose is set
        if (Verbose) {
            gclog_or_tty->print( " minor_pause: %f"
                                 " major_pause: %f"
                                 " minor_interval: %f"
                                 " major_interval: %f"
                                 " pause_goal: %f",
                                 that->_avg_minor_pause->padded_average(),
                                 that->_avg_major_pause->padded_average(),
                                 that->_avg_minor_interval->average(),
                                 that->_avg_major_interval->average(),
                                 that->gc_pause_goal_sec());
        }
        
        // Footprint stats
        gclog_or_tty->print( " live_space: " SIZE_FORMAT
                             " free_space: " SIZE_FORMAT,
                             that->live_space(), that->free_space());
        // More detail
        if (Verbose) {
            gclog_or_tty->print( " base_footprint: " SIZE_FORMAT
                                 " avg_young_live: " SIZE_FORMAT
                                 " avg_old_live: " SIZE_FORMAT,
                                 (size_t)that->_avg_base_footprint->average(),
                                 (size_t)that->avg_young_live()->average(),
                                 (size_t)that->avg_old_live()->average());
        }
        
        // And finally, our old and new sizes.
        gclog_or_tty->print(" old_promo_size: " SIZE_FORMAT
                            " old_eden_size: " SIZE_FORMAT
                            " desired_promo_size: " SIZE_FORMAT
                            " desired_eden_size: " SIZE_FORMAT,
                            that->_promo_size, that->_eden_size,
                            desired_promo_size, desired_eden_size);
        gclog_or_tty->cr();
    }
    
    that->decay_supplemental_growth(is_full_gc);

//    printf("desired promo: %ld, desired eden: %ld\n", desired_promo_size, desired_eden_size);
    
    that->set_promo_size(desired_promo_size);
    that->set_eden_size(desired_eden_size);
    TRACE_OUT();
}

void JEconoMem::psasp_decay_supplemental_growth(PSAdaptiveSizePolicy* that, bool is_full_gc)
{
    TRACE_IN();
    // Decay the supplemental increment?  Decay the supplement growth
    // factor even if it is not used.  It is only meant to give a boost
    // to the initial growth and if it is not used, then it was not
    // needed.
    if (is_full_gc) {
        // Don't wait for the threshold value for the major collections.  If
        // here, the supplemental growth term was used and should decay.
        if ((that->_avg_major_pause->count() % TenuredGenerationSizeSupplementDecay)
            == 0) {
            that->_old_gen_size_increment_supplement =
                that->_old_gen_size_increment_supplement >> 1;
        }
    } else {
        if ((that->_avg_minor_pause->count() >= AdaptiveSizePolicyReadyThreshold) &&
            (that->_avg_minor_pause->count() % YoungGenerationSizeSupplementDecay) == 0) {
            that->_young_gen_size_increment_supplement =
                that->_young_gen_size_increment_supplement >> 1;
        }
    }
    TRACE_OUT();
}

void JEconoMem::psasp_adjust_for_minor_pause_time(PSAdaptiveSizePolicy* that, bool is_full_gc,
                                                 size_t* desired_promo_size_ptr,
                                                 size_t* desired_eden_size_ptr)
{
    TRACE_IN();
    // Adjust the young generation size to reduce pause time of
    // of collections.
    //
    // The AdaptiveSizePolicyInitializingSteps test is not used
    // here.  It has not seemed to be needed but perhaps should
    // be added for consistency.
    if (that->minor_pause_young_estimator()->decrement_will_decrease()) {
        // reduce eden size
        that->set_change_young_gen_for_min_pauses(
            that->decrease_young_gen_for_min_pauses_true);
        *desired_eden_size_ptr = *desired_eden_size_ptr -
            that->eden_decrement_aligned_down(*desired_eden_size_ptr);
    } else {
        // EXPERIMENTAL ADJUSTMENT
        // Only record that the estimator indicated such an action.
        // *desired_eden_size_ptr = *desired_eden_size_ptr + eden_heap_delta;
        that->set_change_young_gen_for_min_pauses(
            that->increase_young_gen_for_min_pauses_true);
    }
    if (PSAdjustTenuredGenForMinorPause) {
        // If the desired eden size is as small as it will get,
        // try to adjust the old gen size.
        if (*desired_eden_size_ptr <= that->_intra_generation_alignment) {
            // Vary the old gen size to reduce the young gen pause.  This
            // may not be a good idea.  This is just a test.
            if (that->minor_pause_old_estimator()->decrement_will_decrease()) {
                that->set_change_old_gen_for_min_pauses(
                    that->decrease_old_gen_for_min_pauses_true);
                *desired_promo_size_ptr =
                    that->_promo_size - that->promo_decrement_aligned_down(*desired_promo_size_ptr);
            } else {
                that->set_change_old_gen_for_min_pauses(
                    that->increase_old_gen_for_min_pauses_true);
                size_t promo_heap_delta =
                    that->promo_increment_with_supplement_aligned_up(*desired_promo_size_ptr);
                if ((*desired_promo_size_ptr + promo_heap_delta) >
                    *desired_promo_size_ptr) {
                    *desired_promo_size_ptr =
                        that->_promo_size + promo_heap_delta;
                }
            }
        }
    }
    TRACE_OUT();
}

void JEconoMem::psasp_adjust_for_pause_time(PSAdaptiveSizePolicy* that, bool is_full_gc,
                                           size_t* desired_promo_size_ptr,
                                           size_t* desired_eden_size_ptr)
{
    TRACE_IN();
    size_t promo_heap_delta = 0;
    size_t eden_heap_delta = 0;
    // Add some checks for a threshhold for a change.  For example,
    // a change less than the required alignment is probably not worth
    // attempting.
    if (is_full_gc) {
        that->set_decide_at_full_gc(that->decide_at_full_gc_true);
    }

    if (that->_avg_minor_pause->padded_average() > that->_avg_major_pause->padded_average()) {
        that->adjust_for_minor_pause_time(is_full_gc,
                                          desired_promo_size_ptr,
                                          desired_eden_size_ptr);
        // major pause adjustments
    } else if (is_full_gc) {
        // Adjust for the major pause time only at full gc's because the
        // affects of a change can only be seen at full gc's.

        // Reduce old generation size to reduce pause?
        if (that->major_pause_old_estimator()->decrement_will_decrease()) {
            // reduce old generation size
            that->set_change_old_gen_for_maj_pauses(that->decrease_old_gen_for_maj_pauses_true);
            promo_heap_delta = that->promo_decrement_aligned_down(*desired_promo_size_ptr);
            *desired_promo_size_ptr = that->_promo_size - promo_heap_delta;
        } else {
            // EXPERIMENTAL ADJUSTMENT
            // Only record that the estimator indicated such an action.
            // *desired_promo_size_ptr = _promo_size +
            //   promo_increment_aligned_up(*desired_promo_size_ptr);
            that->set_change_old_gen_for_maj_pauses(that->increase_old_gen_for_maj_pauses_true);
        }
        if (PSAdjustYoungGenForMajorPause) {
            // If the promo size is at the minimum (i.e., the old gen
            // size will not actually decrease), consider changing the
            // young gen size.
            if (*desired_promo_size_ptr < that->_intra_generation_alignment) {
                // If increasing the young generation will decrease the old gen
                // pause, do it.
                // During startup there is noise in the statistics for deciding
                // on whether to increase or decrease the young gen size.  For
                // some number of iterations, just try to increase the young
                // gen size if the major pause is too long to try and establish
                // good statistics for later decisions.
                if (that->major_pause_young_estimator()->increment_will_decrease() ||
                    (that->_young_gen_change_for_major_pause_count
                     <= AdaptiveSizePolicyInitializingSteps)) {
                    that->set_change_young_gen_for_maj_pauses(
                        that->increase_young_gen_for_maj_pauses_true);
                    eden_heap_delta = that->eden_increment_aligned_up(*desired_eden_size_ptr);
                    *desired_eden_size_ptr = that->_eden_size + eden_heap_delta;
                    (that->_young_gen_change_for_major_pause_count)++;
                } else {
                    // Record that decreasing the young gen size would decrease
                    // the major pause
                    that->set_change_young_gen_for_maj_pauses(
                        that->decrease_young_gen_for_maj_pauses_true);
                    eden_heap_delta = that->eden_decrement_aligned_down(*desired_eden_size_ptr);
                    *desired_eden_size_ptr = that->_eden_size - eden_heap_delta;
                }
            }
        }
    }

    if (PrintAdaptiveSizePolicy && Verbose) {
        gclog_or_tty->print_cr(
            "AdaptiveSizePolicy::compute_generation_free_space "
            "adjusting gen sizes for major pause (avg %f goal %f). "
            "desired_promo_size " SIZE_FORMAT "desired_eden_size "
            SIZE_FORMAT
            " promo delta " SIZE_FORMAT  " eden delta " SIZE_FORMAT,
            that->_avg_major_pause->average(), that->gc_pause_goal_sec(),
            *desired_promo_size_ptr, *desired_eden_size_ptr,
            promo_heap_delta, eden_heap_delta);
    }
    TRACE_OUT();
}

void JEconoMem::psasp_adjust_for_throughput(PSAdaptiveSizePolicy* that, bool is_full_gc,
                                           size_t* desired_promo_size_ptr,
                                           size_t* desired_eden_size_ptr)
{
    TRACE_IN();
    // Add some checks for a threshhold for a change.  For example,
    // a change less than the required alignment is probably not worth
    // attempting.
    if (is_full_gc) {
        that->set_decide_at_full_gc(that->decide_at_full_gc_true);
    }

    if ((that->gc_cost() + that->mutator_cost()) == 0.0) {
        TRACE_OUT();
        return;
    }

    if (PrintAdaptiveSizePolicy && Verbose) {
        gclog_or_tty->print("\nPSAdaptiveSizePolicy::adjust_for_throughput("
                            "is_full: %d, promo: " SIZE_FORMAT ",  cur_eden: " SIZE_FORMAT "): ",
                            is_full_gc, *desired_promo_size_ptr, *desired_eden_size_ptr);
        gclog_or_tty->print_cr("mutator_cost %f  major_gc_cost %f "
                               "minor_gc_cost %f", that->mutator_cost(), that->major_gc_cost(), that->minor_gc_cost());
    }

    // Tenured generation
    if (is_full_gc) {

        // Calculate the change to use for the tenured gen.
        size_t scaled_promo_heap_delta = 0;
        // Can the increment to the generation be scaled?
        if (that->gc_cost() >= 0.0 && that->major_gc_cost() >= 0.0) {
            size_t promo_heap_delta =
                that->promo_increment_with_supplement_aligned_up(*desired_promo_size_ptr);
            double scale_by_ratio = that->major_gc_cost() / that->gc_cost();
            scaled_promo_heap_delta =
                (size_t) (scale_by_ratio * (double) promo_heap_delta);
            if (PrintAdaptiveSizePolicy && Verbose) {
                gclog_or_tty->print_cr(
                    "Scaled tenured increment: " SIZE_FORMAT " by %f down to "
                    SIZE_FORMAT,
                    promo_heap_delta, scale_by_ratio, scaled_promo_heap_delta);
            }
        } else if (that->major_gc_cost() >= 0.0) {
            // Scaling is not going to work.  If the major gc time is the
            // larger, give it a full increment.
            if (that->major_gc_cost() >= that->minor_gc_cost()) {
                scaled_promo_heap_delta =
                    that->promo_increment_with_supplement_aligned_up(*desired_promo_size_ptr);
            }
        } else {
            // Don't expect to get here but it's ok if it does
            // in the product build since the delta will be 0
            // and nothing will change.
            assert(false, "Unexpected value for gc costs");
        }

        switch (AdaptiveSizeThroughPutPolicy) {
        case 1:
            // Early in the run the statistics might not be good.  Until
            // a specific number of collections have been, use the heuristic
            // that a larger generation size means lower collection costs.
            if (that->major_collection_estimator()->increment_will_decrease() ||
                (that->_old_gen_change_for_major_throughput
                 <= AdaptiveSizePolicyInitializingSteps)) {
                // Increase tenured generation size to reduce major collection cost
                if ((*desired_promo_size_ptr + scaled_promo_heap_delta) >
                    *desired_promo_size_ptr) {
                    *desired_promo_size_ptr = that->_promo_size + scaled_promo_heap_delta;
                }
                that->set_change_old_gen_for_throughput(
                    that->increase_old_gen_for_throughput_true);
                (that->_old_gen_change_for_major_throughput)++;
            } else {
                // EXPERIMENTAL ADJUSTMENT
                // Record that decreasing the old gen size would decrease
                // the major collection cost but don't do it.
                // *desired_promo_size_ptr = _promo_size -
                //   promo_decrement_aligned_down(*desired_promo_size_ptr);
                that->set_change_old_gen_for_throughput(
                    that->decrease_old_gen_for_throughput_true);
            }

            break;
        default:
            // Simplest strategy
            if ((*desired_promo_size_ptr + scaled_promo_heap_delta) >
                *desired_promo_size_ptr) {
                *desired_promo_size_ptr = *desired_promo_size_ptr +
                    scaled_promo_heap_delta;
            }
            that->set_change_old_gen_for_throughput(
                that->increase_old_gen_for_throughput_true);
            (that->_old_gen_change_for_major_throughput)++;
        }

        if (PrintAdaptiveSizePolicy && Verbose) {
            gclog_or_tty->print_cr(
                "adjusting tenured gen for throughput (avg %f goal %f). "
                "desired_promo_size " SIZE_FORMAT " promo_delta " SIZE_FORMAT ,
                that->mutator_cost(), that->_throughput_goal,
                *desired_promo_size_ptr, scaled_promo_heap_delta);
        }
    }

    // Young generation
    size_t scaled_eden_heap_delta = 0;
    // Can the increment to the generation be scaled?
    if (that->gc_cost() >= 0.0 && that->minor_gc_cost() >= 0.0) {
        size_t eden_heap_delta =
            that->eden_increment_with_supplement_aligned_up(*desired_eden_size_ptr);
        double scale_by_ratio = that->minor_gc_cost() / that->gc_cost();
        assert(scale_by_ratio <= 1.0 && scale_by_ratio >= 0.0, "Scaling is wrong");
        scaled_eden_heap_delta =
            (size_t) (scale_by_ratio * (double) eden_heap_delta);
        if (PrintAdaptiveSizePolicy && Verbose) {
            gclog_or_tty->print_cr(
                "Scaled eden increment: " SIZE_FORMAT " by %f down to "
                SIZE_FORMAT,
                eden_heap_delta, scale_by_ratio, scaled_eden_heap_delta);
        }
    } else if (that->minor_gc_cost() >= 0.0) {
        // Scaling is not going to work.  If the minor gc time is the
        // larger, give it a full increment.
        if (that->minor_gc_cost() > that->major_gc_cost()) {
            scaled_eden_heap_delta =
                that->eden_increment_with_supplement_aligned_up(*desired_eden_size_ptr);
        }
    } else {
        // Don't expect to get here but it's ok if it does
        // in the product build since the delta will be 0
        // and nothing will change.
        assert(false, "Unexpected value for gc costs");
    }

    // Use a heuristic for some number of collections to give
    // the averages time to settle down.
    switch (AdaptiveSizeThroughPutPolicy) {
    case 1:
        if (that->minor_collection_estimator()->increment_will_decrease() ||
            (that->_young_gen_change_for_minor_throughput
             <= AdaptiveSizePolicyInitializingSteps)) {
            // Expand young generation size to reduce frequency
            // of collections.
            if ((*desired_eden_size_ptr + scaled_eden_heap_delta) >
                *desired_eden_size_ptr) {
                *desired_eden_size_ptr =
                    *desired_eden_size_ptr + scaled_eden_heap_delta;
            }
            that->set_change_young_gen_for_throughput(
                that->increase_young_gen_for_througput_true);
            (that->_young_gen_change_for_minor_throughput)++;
        } else {
            // EXPERIMENTAL ADJUSTMENT
            // Record that decreasing the young gen size would decrease
            // the minor collection cost but don't do it.
            // *desired_eden_size_ptr = _eden_size -
            //   eden_decrement_aligned_down(*desired_eden_size_ptr);
            that->set_change_young_gen_for_throughput(
                that->decrease_young_gen_for_througput_true);
        }
        break;
    default:
        if ((*desired_eden_size_ptr + scaled_eden_heap_delta) >
            *desired_eden_size_ptr) {
            *desired_eden_size_ptr =
                *desired_eden_size_ptr + scaled_eden_heap_delta;
        }
        that->set_change_young_gen_for_throughput(
            that->increase_young_gen_for_througput_true);
        (that->_young_gen_change_for_minor_throughput)++;
    }

    if (PrintAdaptiveSizePolicy && Verbose) {
        gclog_or_tty->print_cr(
            "adjusting eden for throughput (avg %f goal %f). desired_eden_size "
            SIZE_FORMAT " eden delta " SIZE_FORMAT "\n",
            that->mutator_cost(), that->_throughput_goal,
            *desired_eden_size_ptr, scaled_eden_heap_delta);
    }
    TRACE_OUT();
}

size_t JEconoMem::psasp_adjust_promo_for_footprint(PSAdaptiveSizePolicy* that, size_t desired_promo_size,
                                                  size_t desired_sum)
{
    TRACE_IN();
    assert(desired_promo_size <= desired_sum, "Inconsistent parameters");
    that->set_decrease_for_footprint(that->decrease_old_gen_for_footprint_true);

    size_t change = that->promo_decrement(desired_promo_size);
    change = that->scale_down(change, desired_promo_size, desired_sum);

    size_t reduced_size = desired_promo_size - change;

    if (PrintAdaptiveSizePolicy && Verbose) {
        gclog_or_tty->print_cr(
            "AdaptiveSizePolicy::compute_generation_free_space "
            "adjusting tenured gen for footprint. "
            "starting promo size " SIZE_FORMAT
            " reduced promo size " SIZE_FORMAT,
            " promo delta " SIZE_FORMAT,
            desired_promo_size, reduced_size, change );
    }
    
    assert(reduced_size <= desired_promo_size, "Inconsistent result");
    TRACE_OUT();
    return reduced_size;
}

size_t JEconoMem::psasp_adjust_eden_for_footprint(PSAdaptiveSizePolicy* that, size_t desired_eden_size,
                                                 size_t desired_sum)
{
    TRACE_IN();
    assert(desired_eden_size <= desired_sum, "Inconsistent parameters");
    that->set_decrease_for_footprint(that->decrease_young_gen_for_footprint_true);
    
    size_t change = that->eden_decrement(desired_eden_size);
    change = that->scale_down(change, desired_eden_size, desired_sum);
    
    size_t reduced_size = desired_eden_size - change;
    
    if (PrintAdaptiveSizePolicy && Verbose) {
        gclog_or_tty->print_cr(
            "AdaptiveSizePolicy::compute_generation_free_space "
            "adjusting eden for footprint. "
            " starting eden size " SIZE_FORMAT
            " reduced eden size " SIZE_FORMAT
            " eden delta " SIZE_FORMAT,
            desired_eden_size, reduced_size, change);
    }
    
    assert(reduced_size <= desired_eden_size, "Inconsistent result");
    TRACE_OUT();
    return reduced_size;
}

size_t JEconoMem::psasp_scale_down(PSAdaptiveSizePolicy* that, size_t change, double part, double total)
{
    TRACE_IN();
    assert(part <= total, "Inconsistent input");
    size_t reduced_change = change;
    if (total > 0) {
        double fraction =  part / total;
        reduced_change = (size_t) (fraction * (double) change);
    }
    assert(reduced_change <= change, "Inconsistent result");
    TRACE_OUT();
    return reduced_change;
}

size_t JEconoMem::psasp_eden_increment(PSAdaptiveSizePolicy* that, size_t cur_eden, uint percent_change)
{
    TRACE_IN();
    size_t eden_heap_delta;
    eden_heap_delta = cur_eden / 100 * percent_change;
    TRACE_OUT();
    return eden_heap_delta;
}

size_t JEconoMem::psasp_eden_increment(PSAdaptiveSizePolicy* that, size_t cur_eden)
{
    TRACE_IN();
    size_t temp = that->eden_increment(cur_eden, YoungGenerationSizeIncrement);
    TRACE_OUT();
    return temp;
}

size_t JEconoMem::psasp_eden_increment_aligned_up(PSAdaptiveSizePolicy* that, size_t cur_eden)
{
    TRACE_IN();
    size_t result = that->eden_increment(cur_eden, YoungGenerationSizeIncrement);
    size_t temp = align_size_up(result, that->_intra_generation_alignment);
    TRACE_OUT();
    return temp;
}

size_t JEconoMem::psasp_eden_increment_aligned_down(PSAdaptiveSizePolicy* that, size_t cur_eden)
{
    TRACE_IN();
    size_t result = that->eden_increment(cur_eden);
    size_t temp = align_size_down(result, that->_intra_generation_alignment);
    TRACE_OUT();
    return temp;
}

size_t JEconoMem::psasp_eden_increment_with_supplement_aligned_up(PSAdaptiveSizePolicy* that, size_t cur_eden)
{
    TRACE_IN();
    size_t result = that->eden_increment(cur_eden,
                                         YoungGenerationSizeIncrement + that->_young_gen_size_increment_supplement);
    size_t temp = align_size_up(result, that->_intra_generation_alignment);
    TRACE_OUT();
    return temp;
}

size_t JEconoMem::psasp_eden_decrement_aligned_down(PSAdaptiveSizePolicy* that, size_t cur_eden)
{
    TRACE_IN();
    size_t eden_heap_delta = that->eden_decrement(cur_eden);
    size_t temp = align_size_down(eden_heap_delta, that->_intra_generation_alignment);
    TRACE_OUT();
    return temp;
}

size_t JEconoMem::psasp_eden_decrement(PSAdaptiveSizePolicy* that, size_t cur_eden)
{
    TRACE_IN();
    size_t eden_heap_delta = that->eden_increment(cur_eden) /
        AdaptiveSizeDecrementScaleFactor;
    TRACE_OUT();
    return eden_heap_delta;
}

size_t JEconoMem::psasp_promo_increment(PSAdaptiveSizePolicy* that, size_t cur_promo, uint percent_change)
{
    TRACE_IN();
    size_t promo_heap_delta;
    promo_heap_delta = cur_promo / 100 * percent_change;
    TRACE_OUT();
    return promo_heap_delta;
}

size_t JEconoMem::psasp_promo_increment(PSAdaptiveSizePolicy* that, size_t cur_promo)
{
    TRACE_IN();
    size_t temp = that->promo_increment(cur_promo, TenuredGenerationSizeIncrement);
    TRACE_OUT();
    return temp;
}

size_t JEconoMem::psasp_promo_increment_aligned_up(PSAdaptiveSizePolicy* that, size_t cur_promo)
{
    TRACE_IN();
    size_t result =  that->promo_increment(cur_promo, TenuredGenerationSizeIncrement);
    size_t temp = align_size_up(result, that->_intra_generation_alignment);
    TRACE_OUT();
    return temp;
}

size_t JEconoMem::psasp_promo_increment_aligned_down(PSAdaptiveSizePolicy* that, size_t cur_promo)
{
    TRACE_IN();
    size_t result =  that->promo_increment(cur_promo, TenuredGenerationSizeIncrement);
    size_t temp = align_size_down(result, that->_intra_generation_alignment);
    TRACE_OUT();
    return temp;
}

size_t JEconoMem::psasp_promo_increment_with_supplement_aligned_up(PSAdaptiveSizePolicy* that, size_t cur_promo)
{
    TRACE_IN();
    size_t result =  that->promo_increment(cur_promo,
                                           TenuredGenerationSizeIncrement + that->_old_gen_size_increment_supplement);
    size_t temp = align_size_up(result, that->_intra_generation_alignment);
    TRACE_OUT();
    return temp;
}

size_t JEconoMem::psasp_promo_decrement_aligned_down(PSAdaptiveSizePolicy* that, size_t cur_promo)
{
    TRACE_IN();
    size_t promo_heap_delta = that->promo_decrement(cur_promo);
    size_t temp = align_size_down(promo_heap_delta, that->_intra_generation_alignment);
    TRACE_OUT();
    return temp;
}

size_t JEconoMem::psasp_promo_decrement(PSAdaptiveSizePolicy* that, size_t cur_promo)
{
    TRACE_IN();
    size_t promo_heap_delta = that->promo_increment(cur_promo);
    promo_heap_delta = promo_heap_delta / AdaptiveSizeDecrementScaleFactor;
    TRACE_OUT();
    return promo_heap_delta;
}

int JEconoMem::psasp_compute_survivor_space_size_and_threshold(PSAdaptiveSizePolicy* that,
                                                              bool   is_survivor_overflow,
                                                              int    tenuring_threshold,
                                                              size_t survivor_limit)
{
    TRACE_IN();
    assert(survivor_limit >= that->_intra_generation_alignment,
           "survivor_limit too small");
    assert((size_t)align_size_down(survivor_limit, that->_intra_generation_alignment)
           == survivor_limit, "survivor_limit not aligned");
    
    // This method is called even if the tenuring threshold and survivor
    // spaces are not adjusted so that the averages are sampled above.
    if (!UsePSAdaptiveSurvivorSizePolicy ||
        !that->young_gen_policy_is_ready()) {
        TRACE_OUT();
        return tenuring_threshold;
    }

    // We'll decide whether to increase or decrease the tenuring
    // threshold based partly on the newly computed survivor size
    // (if we hit the maximum limit allowed, we'll always choose to
    // decrement the threshold).
    bool incr_tenuring_threshold = false;
    bool decr_tenuring_threshold = false;

    that->set_decrement_tenuring_threshold_for_gc_cost(false);
    that->set_increment_tenuring_threshold_for_gc_cost(false);
    that->set_decrement_tenuring_threshold_for_survivor_limit(false);

    if (!is_survivor_overflow) {
        // Keep running averages on how much survived

        // We use the tenuring threshold to equalize the cost of major
        // and minor collections.
        // ThresholdTolerance is used to indicate how sensitive the
        // tenuring threshold is to differences in cost betweent the
        // collection types.

        // Get the times of interest. This involves a little work, so
        // we cache the values here.
        const double major_cost = that->major_gc_cost();
        const double minor_cost = that->minor_gc_cost();

        if (minor_cost > major_cost * that->_threshold_tolerance_percent) {
            // Minor times are getting too long;  lower the threshold so
            // less survives and more is promoted.
            decr_tenuring_threshold = true;
            that->set_decrement_tenuring_threshold_for_gc_cost(true);
        } else if (major_cost > minor_cost * that->_threshold_tolerance_percent) {
            // Major times are too long, so we want less promotion.
            incr_tenuring_threshold = true;
            that->set_increment_tenuring_threshold_for_gc_cost(true);
        }

    } else {
        // Survivor space overflow occurred, so promoted and survived are
        // not accurate. We'll make our best guess by combining survived
        // and promoted and count them as survivors.
        //
        // We'll lower the tenuring threshold to see if we can correct
        // things. Also, set the survivor size conservatively. We're
        // trying to avoid many overflows from occurring if defnew size
        // is just too small.

        decr_tenuring_threshold = true;
    }

    // The padded average also maintains a deviation from the average;
    // we use this to see how good of an estimate we have of what survived.
    // We're trying to pad the survivor size as little as possible without
    // overflowing the survivor spaces.
    size_t target_size = align_size_up((size_t)that->_avg_survived->padded_average(),
                                       that->_intra_generation_alignment);
    target_size = MAX2(target_size, that->_intra_generation_alignment);

    if (target_size > survivor_limit) {
        // Target size is bigger than we can handle. Let's also reduce
        // the tenuring threshold.
        target_size = survivor_limit;
        decr_tenuring_threshold = true;
        that->set_decrement_tenuring_threshold_for_survivor_limit(true);
    }

    // Finally, increment or decrement the tenuring threshold, as decided above.
    // We test for decrementing first, as we might have hit the target size
    // limit.
    if (decr_tenuring_threshold && !(AlwaysTenure || NeverTenure)) {
        if (tenuring_threshold > 1) {
            tenuring_threshold--;
        }
    } else if (incr_tenuring_threshold && !(AlwaysTenure || NeverTenure)) {
        if (tenuring_threshold < MaxTenuringThreshold) {
            tenuring_threshold++;
        }
    }

    // We keep a running average of the amount promoted which is used
    // to decide when we should collect the old generation (when
    // the amount of old gen free space is less than what we expect to
    // promote).

    if (PrintAdaptiveSizePolicy) {
        // A little more detail if Verbose is on
        if (Verbose) {
            gclog_or_tty->print( "  avg_survived: %f"
                                 "  avg_deviation: %f",
                                 that->_avg_survived->average(),
                                 that->_avg_survived->deviation());
        }

        gclog_or_tty->print( "  avg_survived_padded_avg: %f",
                             that->_avg_survived->padded_average());

        if (Verbose) {
            gclog_or_tty->print( "  avg_promoted_avg: %f"
                                 "  avg_promoted_dev: %f",
                                 that->avg_promoted()->average(),
                                 that->avg_promoted()->deviation());
        }

        gclog_or_tty->print( "  avg_promoted_padded_avg: %f"
                             "  avg_pretenured_padded_avg: %f"
                             "  tenuring_thresh: %d"
                             "  target_size: " SIZE_FORMAT,
                             that->avg_promoted()->padded_average(),
                             that->_avg_pretenured->padded_average(),
                             tenuring_threshold, target_size);
        tty->cr();
    }

    that->set_survivor_size(target_size);

    TRACE_OUT();
    return tenuring_threshold;
}

void JEconoMem::psasp_update_averages(PSAdaptiveSizePolicy* that, bool is_survivor_overflow,
                                     size_t survived,
                                     size_t promoted)
{
    TRACE_IN();
    // Update averages
    if (!is_survivor_overflow) {
        // Keep running averages on how much survived
        that->_avg_survived->sample(survived);
    } else {
        size_t survived_guess = survived + promoted;
        that->_avg_survived->sample(survived_guess);
    }
    that->avg_promoted()->sample(promoted + that->_avg_pretenured->padded_average());

    if (PrintAdaptiveSizePolicy) {
        gclog_or_tty->print(
            "AdaptiveSizePolicy::compute_survivor_space_size_and_thresh:"
            "  survived: "  SIZE_FORMAT
            "  promoted: "  SIZE_FORMAT
            "  overflow: %s",
            survived, promoted, is_survivor_overflow ? "true" : "false");
    }
    TRACE_OUT();
}


bool JEconoMem::psasp_print_adaptive_size_policy_on(const PSAdaptiveSizePolicy* that, outputStream* st)
{
    TRACE_IN();
    if (!UseAdaptiveSizePolicy) { TRACE_OUT(); return false; }
    
    bool temp = that->AdaptiveSizePolicy::print_adaptive_size_policy_on(
        st,
        PSScavenge::tenuring_threshold());
    TRACE_OUT();
    return temp;
}

////////////////////////////////////////////////////////

bool JEconoMem::psScavenge_invoke()
{
    TRACE_IN();
    assert(SafepointSynchronize::is_at_safepoint(), "should be at safepoint");
    assert(Thread::current() == (Thread*)VMThread::vm_thread(), "should be in vm thread");
    assert(!Universe::heap()->is_gc_active(), "not reentrant");

    ParallelScavengeHeap* const heap = (ParallelScavengeHeap*)Universe::heap();
    assert(heap->kind() == CollectedHeap::ParallelScavengeHeap, "Sanity");

    PSAdaptiveSizePolicy* policy = heap->size_policy();
    gc_start(policy);
    IsGCActiveMark mark;
    
#ifndef USDT2
    HS_DTRACE_PROBE2(hotspot, gc__collection__PSScavenge__begin, &heap, heap->gc_cause());
#endif /* !USDT2 */
    const bool scavenge_done = psScavenge_invoke_no_policy();
#ifndef USDT2
    HS_DTRACE_PROBE2(hotspot, gc__collection__PSScavenge__end, &heap, heap->gc_cause());
#endif /* !USDT2 */
    const bool need_full_gc = !scavenge_done ||
        policy->should_full_GC(heap->old_gen()->free_in_bytes());
    bool full_gc_done = false;
    
    if (UsePerfData) {
        PSGCAdaptivePolicyCounters* const counters = heap->gc_policy_counters();
        const int ffs_val = need_full_gc ? PSScavenge::full_follows_scavenge : PSScavenge::not_skipped;
        counters->update_full_follows_scavenge(ffs_val);
    }
    
    if (need_full_gc) {
        GCCauseSetter gccs(heap, GCCause::_adaptive_size_policy);
        CollectorPolicy* cp = heap->collector_policy();
        const bool clear_all_softrefs = cp->should_clear_all_soft_refs();
        
        if (UseParallelOldGC) {
#ifndef USDT2
            HS_DTRACE_PROBE2(hotspot, gc__collection__PSParallelCompact__begin, &heap, heap->gc_cause());
#endif /* !USDT2 */
            full_gc_done = psParallelCompact_invoke_no_policy(clear_all_softrefs);
#ifndef USDT2
            HS_DTRACE_PROBE2(hotspot, gc__collection__PSParallelCompact__end, &heap, heap->gc_cause());
#endif /* !USDT2 */
        } else {
#ifndef USDT2
            HS_DTRACE_PROBE2(hotspot, gc__collection__PSMarkSweep__begin, &heap, heap->gc_cause());
#endif /* !USDT2 */
            full_gc_done = psMarkSweep_invoke_no_policy(clear_all_softrefs);
#ifndef USDT2
            HS_DTRACE_PROBE2(hotspot, gc__collection__PSMarkSweep__end, &heap, heap->gc_cause());
#endif /* !USDT2 */
        }
    }
    
    gc_end(policy);
    TRACE_OUT();
    return full_gc_done;
}

bool JEconoMem::psScavenge_invoke_no_policy()
{
    TRACE_IN();
    bool temp = PSScavenge::invoke_no_policy();
    TRACE_OUT();
    return temp;
}

void JEconoMem::psMarkSweep_invoke(bool maximum_heap_compaction)
{
    TRACE_IN();
    assert(SafepointSynchronize::is_at_safepoint(), "should be at safepoint");
    assert(Thread::current() == (Thread*)VMThread::vm_thread(), "should be in vm thread");
    assert(!Universe::heap()->is_gc_active(), "not reentrant");
    
    ParallelScavengeHeap* heap = (ParallelScavengeHeap*)Universe::heap();
    GCCause::Cause gc_cause = heap->gc_cause();
    PSAdaptiveSizePolicy* policy = heap->size_policy();
    gc_start(policy);
    IsGCActiveMark mark;
    
    if (ScavengeBeforeFullGC) {
        psScavenge_invoke_no_policy();
    }

    const bool clear_all_soft_refs =
        heap->collector_policy()->should_clear_all_soft_refs();
    
    int count = (maximum_heap_compaction)?1:MarkSweepAlwaysCompactCount;
    IntFlagSetting flag_setting(MarkSweepAlwaysCompactCount, count);
    psMarkSweep_invoke_no_policy(clear_all_soft_refs || maximum_heap_compaction);
    gc_end(policy);
    TRACE_OUT();
}

bool JEconoMem::psMarkSweep_invoke_no_policy(bool maximum_heap_compaction)
{
    TRACE_IN();
    bool temp = PSMarkSweep::invoke_no_policy(maximum_heap_compaction);
    TRACE_OUT();
    return temp;
}

void JEconoMem::psParallelCompact_invoke(bool maximum_heap_compaction)
{
    TRACE_IN();
    assert(SafepointSynchronize::is_at_safepoint(), "should be at safepoint");
    assert(Thread::current() == (Thread*)VMThread::vm_thread(),
           "should be in vm thread");
    
    ParallelScavengeHeap* heap = PSParallelCompact::gc_heap();
#ifndef USDT2
    HS_DTRACE_PROBE2(hotspot, gc__collection__parallel__collect, heap, heap->gc_cause());
#endif /* !USDT2 */
    GCCause::Cause gc_cause = heap->gc_cause();
    assert(!heap->is_gc_active(), "not reentrant");
    
    PSAdaptiveSizePolicy* policy = heap->size_policy();
    gc_start(policy);
    IsGCActiveMark mark;
    
    if (ScavengeBeforeFullGC) {
        psScavenge_invoke_no_policy();
    }
    
    const bool clear_all_soft_refs =
        heap->collector_policy()->should_clear_all_soft_refs();
    
    psParallelCompact_invoke_no_policy(clear_all_soft_refs ||
                                        maximum_heap_compaction);

    gc_end(policy);
    TRACE_OUT();
}

bool JEconoMem::psParallelCompact_invoke_no_policy(bool maximum_heap_compaction)
{
    TRACE_IN();
    bool temp = PSParallelCompact::invoke_no_policy(maximum_heap_compaction);
    TRACE_OUT();
    return temp;
}


///////////////////////////////////////////////////////////////////

void JEconoMem::asp_create(AdaptiveSizePolicy* that)
{
    TRACE_IN();
    TRACE_OUT();
}

int JEconoMem::asp_calc_default_active_workers(uintx total_workers,
                                              const uintx min_workers,
                                              uintx active_workers,
                                              uintx application_workers) {
    TRACE_IN();
    // If the user has specifically set the number of
    // GC threads, use them.

    // If the user has turned off using a dynamic number of GC threads
    // or the users has requested a specific number, set the active
    // number of workers to all the workers.

    uintx new_active_workers = total_workers;
    uintx prev_active_workers = active_workers;
    uintx active_workers_by_JT = 0;
    uintx active_workers_by_heap_size = 0;

    // Always use at least min_workers but use up to
    // GCThreadsPerJavaThreads * application threads.
    active_workers_by_JT =
        MAX2((uintx) AdaptiveSizePolicy::GCWorkersPerJavaThread * application_workers,
             min_workers);

    // Choose a number of GC threads based on the current size
    // of the heap.  This may be complicated because the size of
    // the heap depends on factors such as the thoughput goal.
    // Still a large heap should be collected by more GC threads.
    active_workers_by_heap_size =
        MAX2((size_t) 2U, Universe::heap()->capacity() / HeapSizePerGCThread);

    uintx max_active_workers =
        MAX2(active_workers_by_JT, active_workers_by_heap_size);

    // Limit the number of workers to the the number created,
    // (workers()).
    new_active_workers = MIN2(max_active_workers,
                              (uintx) total_workers);

    // Increase GC workers instantly but decrease them more
    // slowly.
    if (new_active_workers < prev_active_workers) {
        new_active_workers =
            MAX2(min_workers, (prev_active_workers + new_active_workers) / 2);
    }

    // Check once more that the number of workers is within the limits.
    assert(min_workers <= total_workers, "Minimum workers not consistent with total workers");
    assert(new_active_workers >= min_workers, "Minimum workers not observed");
    assert(new_active_workers <= total_workers, "Total workers not observed");

    if (ForceDynamicNumberOfGCThreads) {
        // Assume this is debugging and jiggle the number of GC threads.
        if (new_active_workers == prev_active_workers) {
            if (new_active_workers < total_workers) {
                new_active_workers++;
            } else if (new_active_workers > min_workers) {
                new_active_workers--;
            }
        }
        if (new_active_workers == total_workers) {
            if (AdaptiveSizePolicy::_debug_perturbation) {
                new_active_workers =  min_workers;
            }
            AdaptiveSizePolicy::_debug_perturbation = !AdaptiveSizePolicy::_debug_perturbation;
        }
        assert((new_active_workers <= (uintx) ParallelGCThreads) &&
               (new_active_workers >= min_workers),
               "Jiggled active workers too much");
    }

    if (TraceDynamicGCThreads) {
        gclog_or_tty->print_cr("GCTaskManager::calc_default_active_workers() : "
                               "active_workers(): %d  new_acitve_workers: %d  "
                               "prev_active_workers: %d\n"
                               " active_workers_by_JT: %d  active_workers_by_heap_size: %d",
                               active_workers, new_active_workers, prev_active_workers,
                               active_workers_by_JT, active_workers_by_heap_size);
    }
    assert(new_active_workers > 0, "Always need at least 1");
    TRACE_OUT();
    return new_active_workers;
}

int JEconoMem::asp_calc_active_workers(uintx total_workers,
                                      uintx active_workers,
                                      uintx application_workers) {
    TRACE_IN();
    // If the user has specifically set the number of
    // GC threads, use them.

    // If the user has turned off using a dynamic number of GC threads
    // or the users has requested a specific number, set the active
    // number of workers to all the workers.

    int new_active_workers;
    if (!UseDynamicNumberOfGCThreads ||
        (!FLAG_IS_DEFAULT(ParallelGCThreads) && !ForceDynamicNumberOfGCThreads)) {
        new_active_workers = total_workers;
    } else {
        new_active_workers = AdaptiveSizePolicy::calc_default_active_workers(total_workers,
                                                                             2, /* Minimum number of workers */
                                                                             active_workers,
                                                                             application_workers);
    }
    assert(new_active_workers > 0, "Always need at least 1");
    TRACE_OUT();
    return new_active_workers;
}

int JEconoMem::asp_calc_active_conc_workers(uintx total_workers,
                                           uintx active_workers,
                                           uintx application_workers) {
    TRACE_IN();
    if (!UseDynamicNumberOfGCThreads ||
        (!FLAG_IS_DEFAULT(ConcGCThreads) && !ForceDynamicNumberOfGCThreads)) {
        TRACE_OUT();
        return ConcGCThreads;
    } else {
        int no_of_gc_threads = AdaptiveSizePolicy::calc_default_active_workers(
            total_workers,
            1, /* Minimum number of workers */
            active_workers,
            application_workers);
        TRACE_OUT();
        return no_of_gc_threads;
    }
}

bool JEconoMem::asp_tenuring_threshold_change(const AdaptiveSizePolicy* that) {
    TRACE_IN();
    bool temp = that->decrement_tenuring_threshold_for_gc_cost() ||
        that->increment_tenuring_threshold_for_gc_cost() ||
        that->decrement_tenuring_threshold_for_survivor_limit();
    TRACE_OUT();
    return temp;
}

void JEconoMem::asp_minor_collection_begin(AdaptiveSizePolicy* that) {
    TRACE_IN();
    // Update the interval time
    that->_minor_timer.stop();
    // Save most recent collection time
    that->_latest_minor_mutator_interval_seconds = that->_minor_timer.seconds();
    that->_minor_timer.reset();
    that->_minor_timer.start();
    TRACE_OUT();
}

void JEconoMem::asp_update_minor_pause_young_estimator(AdaptiveSizePolicy* that,
                                                      double minor_pause_in_ms) {
    TRACE_IN();
    double eden_size_in_mbytes = ((double)that->_eden_size)/((double)M);
    that->_minor_pause_young_estimator->update(eden_size_in_mbytes,
                                               minor_pause_in_ms);
    TRACE_OUT();
}

void JEconoMem::asp_minor_collection_end(AdaptiveSizePolicy* that, GCCause::Cause gc_cause) {
    TRACE_IN();
    // Update the pause time.
    that->_minor_timer.stop();

    if (gc_cause != GCCause::_java_lang_system_gc ||
        UseAdaptiveSizePolicyWithSystemGC) {
        double minor_pause_in_seconds = that->_minor_timer.seconds();
        double minor_pause_in_ms = minor_pause_in_seconds * MILLIUNITS;

        // Sample for performance counter
        that->_avg_minor_pause->sample(minor_pause_in_seconds);

        // Cost of collection (unit-less)
        double collection_cost = 0.0;
        if ((that->_latest_minor_mutator_interval_seconds > 0.0) &&
            (minor_pause_in_seconds > 0.0)) {
            double interval_in_seconds =
                that->_latest_minor_mutator_interval_seconds + minor_pause_in_seconds;
            collection_cost =
                minor_pause_in_seconds / interval_in_seconds;
            that->_avg_minor_gc_cost->sample(collection_cost);
            // Sample for performance counter
            that->_avg_minor_interval->sample(interval_in_seconds);
        }

        // The policy does not have enough data until at least some
        // minor collections have been done.
        that->_young_gen_policy_is_ready =
            (that->_avg_minor_gc_cost->count() >= AdaptiveSizePolicyReadyThreshold);

        // Calculate variables used to estimate pause time vs. gen sizes
        double eden_size_in_mbytes = ((double)that->_eden_size)/((double)M);
        that->update_minor_pause_young_estimator(minor_pause_in_ms);
        that->update_minor_pause_old_estimator(minor_pause_in_ms);

        if (PrintAdaptiveSizePolicy && Verbose) {
            gclog_or_tty->print("AdaptiveSizePolicy::minor_collection_end: "
                                "minor gc cost: %f  average: %f", collection_cost,
                                that->_avg_minor_gc_cost->average());
            gclog_or_tty->print_cr("  minor pause: %f minor period %f",
                                   minor_pause_in_ms,
                                   that->_latest_minor_mutator_interval_seconds * MILLIUNITS);
        }

        // Calculate variable used to estimate collection cost vs. gen sizes
        assert(collection_cost >= 0.0, "Expected to be non-negative");
        that->_minor_collection_estimator->update(eden_size_in_mbytes, collection_cost);
    }

    // Interval times use this timer to measure the mutator time.
    // Reset the timer after the GC pause.
    that->_minor_timer.reset();
    that->_minor_timer.start();
    TRACE_OUT();
}

size_t JEconoMem::asp_eden_increment(AdaptiveSizePolicy* that,
                                    size_t cur_eden,
                                    uint percent_change) {
    TRACE_IN();
    size_t eden_heap_delta;
    eden_heap_delta = cur_eden / 100 * percent_change;
    TRACE_OUT();
    return eden_heap_delta;
}

size_t JEconoMem::asp_eden_increment(AdaptiveSizePolicy* that, size_t cur_eden) {
    TRACE_IN();
    size_t temp = that->eden_increment(cur_eden, YoungGenerationSizeIncrement);
    TRACE_OUT();
    return temp;
}

size_t JEconoMem::asp_eden_decrement(AdaptiveSizePolicy* that, size_t cur_eden) {
    TRACE_IN();
    size_t eden_heap_delta = that->eden_increment(cur_eden) /
        AdaptiveSizeDecrementScaleFactor;
    TRACE_OUT();
    return eden_heap_delta;
}

size_t JEconoMem::asp_promo_increment(AdaptiveSizePolicy* that,
                                     size_t cur_promo,
                                     uint percent_change) {
    TRACE_IN();
    size_t promo_heap_delta;
    promo_heap_delta = cur_promo / 100 * percent_change;
    TRACE_OUT();
    return promo_heap_delta;
}

size_t JEconoMem::asp_promo_increment(AdaptiveSizePolicy* that, size_t cur_promo) {
    TRACE_IN();
    size_t temp = that->promo_increment(cur_promo, TenuredGenerationSizeIncrement);
    TRACE_OUT();
    return temp;
}

size_t JEconoMem::asp_promo_decrement(AdaptiveSizePolicy* that, size_t cur_promo) {
    TRACE_IN();
    size_t promo_heap_delta = that->promo_increment(cur_promo);
    promo_heap_delta = promo_heap_delta / AdaptiveSizeDecrementScaleFactor;
    TRACE_OUT();
    return promo_heap_delta;
}

double JEconoMem::asp_time_since_major_gc(const AdaptiveSizePolicy* that) {
    TRACE_IN();
    that->_major_timer.stop();
    double result = that->_major_timer.seconds();
    that->_major_timer.start();
    TRACE_OUT();
    return result;
}

double JEconoMem::asp_decaying_major_gc_cost(const AdaptiveSizePolicy* that) {
    TRACE_IN();
    double major_interval = that->major_gc_interval_average_for_decay();
    double major_gc_cost_average = that->major_gc_cost();
    double decayed_major_gc_cost = major_gc_cost_average;
    if(that->time_since_major_gc() > 0.0) {
        decayed_major_gc_cost = that->major_gc_cost() *
            (((double) AdaptiveSizeMajorGCDecayTimeScale) * major_interval)
            / that->time_since_major_gc();
    }

    // The decayed cost should always be smaller than the
    // average cost but the vagaries of finite arithmetic could
    // produce a larger value in decayed_major_gc_cost so protect
    // against that.
    double temp = MIN2(major_gc_cost_average, decayed_major_gc_cost);
    TRACE_OUT();
    return temp;
}

double JEconoMem::asp_decaying_gc_cost(const AdaptiveSizePolicy* that) {
    TRACE_IN();
    double decayed_major_gc_cost = that->major_gc_cost();
    double avg_major_interval = that->major_gc_interval_average_for_decay();
    if (UseAdaptiveSizeDecayMajorGCCost &&
        (AdaptiveSizeMajorGCDecayTimeScale > 0) &&
        (avg_major_interval > 0.00)) {
        double time_since_last_major_gc = that->time_since_major_gc();

        // Decay the major gc cost?
        if (time_since_last_major_gc >
            ((double) AdaptiveSizeMajorGCDecayTimeScale) * avg_major_interval) {

            // Decay using the time-since-last-major-gc
            decayed_major_gc_cost = that->decaying_major_gc_cost();
            if (PrintGCDetails && Verbose) {
                gclog_or_tty->print_cr("\ndecaying_gc_cost: major interval average:"
                                       " %f  time since last major gc: %f",
                                       avg_major_interval, time_since_last_major_gc);
                gclog_or_tty->print_cr("  major gc cost: %f  decayed major gc cost: %f",
                                       that->major_gc_cost(), decayed_major_gc_cost);
            }
        }
    }
    double result = MIN2(1.0, decayed_major_gc_cost + that->minor_gc_cost());
    TRACE_OUT();
    return result;
}

void JEconoMem::asp_clear_generation_free_space_flags(AdaptiveSizePolicy* that) {
    TRACE_IN();
    that->set_change_young_gen_for_min_pauses(0);
    that->set_change_old_gen_for_maj_pauses(0);

    that->set_change_old_gen_for_throughput(0);
    that->set_change_young_gen_for_throughput(0);
    that->set_decrease_for_footprint(0);
    that->set_decide_at_full_gc(0);
    TRACE_OUT();
}

void JEconoMem::asp_check_gc_overhead_limit(AdaptiveSizePolicy* that,
                                           size_t young_live,
                                           size_t eden_live,
                                           size_t max_old_gen_size,
                                           size_t max_eden_size,
                                           bool   is_full_gc,
                                           GCCause::Cause gc_cause,
                                           CollectorPolicy* collector_policy) {
    TRACE_IN();
    // Ignore explicit GC's.  Exiting here does not set the flag and
    // does not reset the count.  Updating of the averages for system
    // GC's is still controlled by UseAdaptiveSizePolicyWithSystemGC.
    if (GCCause::is_user_requested_gc(gc_cause) ||
        GCCause::is_serviceability_requested_gc(gc_cause)) {
        TRACE_OUT();
        return;
    }
    // eden_limit is the upper limit on the size of eden based on
    // the maximum size of the young generation and the sizes
    // of the survivor space.
    // The question being asked is whether the gc costs are high
    // and the space being recovered by a collection is low.
    // free_in_young_gen is the free space in the young generation
    // after a collection and promo_live is the free space in the old
    // generation after a collection.
    //
    // Use the minimum of the current value of the live in the
    // young gen or the average of the live in the young gen.
    // If the current value drops quickly, that should be taken
    // into account (i.e., don't trigger if the amount of free
    // space has suddenly jumped up).  If the current is much
    // higher than the average, use the average since it represents
    // the longer term behavor.
    const size_t live_in_eden =
        MIN2(eden_live, (size_t) that->avg_eden_live()->average());
    const size_t free_in_eden = max_eden_size > live_in_eden ?
        max_eden_size - live_in_eden : 0;
    const size_t free_in_old_gen = (size_t)(max_old_gen_size - that->avg_old_live()->average());
    const size_t total_free_limit = free_in_old_gen + free_in_eden;
    const size_t total_mem = max_old_gen_size + max_eden_size;
    const double mem_free_limit = total_mem * (GCHeapFreeLimit/100.0);
    const double mem_free_old_limit = max_old_gen_size * (GCHeapFreeLimit/100.0);
    const double mem_free_eden_limit = max_eden_size * (GCHeapFreeLimit/100.0);
    const double gc_cost_limit = GCTimeLimit/100.0;
    size_t promo_limit = (size_t)(max_old_gen_size - that->avg_old_live()->average());
    // But don't force a promo size below the current promo size. Otherwise,
    // the promo size will shrink for no good reason.
    promo_limit = MAX2(promo_limit, that->_promo_size);


    if (PrintAdaptiveSizePolicy && (Verbose ||
                                    (free_in_old_gen < (size_t) mem_free_old_limit &&
                                     free_in_eden < (size_t) mem_free_eden_limit))) {
        gclog_or_tty->print_cr(
            "PSAdaptiveSizePolicy::compute_generation_free_space limits:"
            " promo_limit: " SIZE_FORMAT
            " max_eden_size: " SIZE_FORMAT
            " total_free_limit: " SIZE_FORMAT
            " max_old_gen_size: " SIZE_FORMAT
            " max_eden_size: " SIZE_FORMAT
            " mem_free_limit: " SIZE_FORMAT,
            promo_limit, max_eden_size, total_free_limit,
            max_old_gen_size, max_eden_size,
            (size_t) mem_free_limit);
    }

    bool print_gc_overhead_limit_would_be_exceeded = false;
    if (is_full_gc) {
        if (that->gc_cost() > gc_cost_limit &&
            free_in_old_gen < (size_t) mem_free_old_limit &&
            free_in_eden < (size_t) mem_free_eden_limit) {
            // Collections, on average, are taking too much time, and
            //      gc_cost() > gc_cost_limit
            // we have too little space available after a full gc.
            //      total_free_limit < mem_free_limit
            // where
            //   total_free_limit is the free space available in
            //     both generations
            //   total_mem is the total space available for allocation
            //     in both generations (survivor spaces are not included
            //     just as they are not included in eden_limit).
            //   mem_free_limit is a fraction of total_mem judged to be an
            //     acceptable amount that is still unused.
            // The heap can ask for the value of this variable when deciding
            // whether to thrown an OutOfMemory error.
            // Note that the gc time limit test only works for the collections
            // of the young gen + tenured gen and not for collections of the
            // permanent gen.  That is because the calculation of the space
            // freed by the collection is the free space in the young gen +
            // tenured gen.
            // At this point the GC overhead limit is being exceeded.
            that->inc_gc_overhead_limit_count();
            if (UseGCOverheadLimit) {
                if (that->gc_overhead_limit_count() >=
                    AdaptiveSizePolicyGCTimeLimitThreshold){
                    // All conditions have been met for throwing an out-of-memory
                    that->set_gc_overhead_limit_exceeded(true);
                    // Avoid consecutive OOM due to the gc time limit by resetting
                    // the counter.
                    that->reset_gc_overhead_limit_count();
                } else {
                    // The required consecutive collections which exceed the
                    // GC time limit may or may not have been reached. We
                    // are approaching that condition and so as not to
                    // throw an out-of-memory before all SoftRef's have been
                    // cleared, set _should_clear_all_soft_refs in CollectorPolicy.
                    // The clearing will be done on the next GC.
                    bool near_limit = that->gc_overhead_limit_near();
                    if (near_limit) {
                        collector_policy->set_should_clear_all_soft_refs(true);
                        if (PrintGCDetails && Verbose) {
                            gclog_or_tty->print_cr("  Nearing GC overhead limit, "
                                                   "will be clearing all SoftReference");
                        }
                    }
                }
            }
            // Set this even when the overhead limit will not
            // cause an out-of-memory.  Diagnostic message indicating
            // that the overhead limit is being exceeded is sometimes
            // printed.
            print_gc_overhead_limit_would_be_exceeded = true;

        } else {
            // Did not exceed overhead limits
            that->reset_gc_overhead_limit_count();
        }
    }

    if (UseGCOverheadLimit && PrintGCDetails && Verbose) {
        if (that->gc_overhead_limit_exceeded()) {
            gclog_or_tty->print_cr("      GC is exceeding overhead limit "
                                   "of %d%%", GCTimeLimit);
            that->reset_gc_overhead_limit_count();
        } else if (print_gc_overhead_limit_would_be_exceeded) {
            assert(gc_overhead_limit_count() > 0, "Should not be printing");
            gclog_or_tty->print_cr("      GC would exceed overhead limit "
                                   "of %d%% %d consecutive time(s)",
                                   GCTimeLimit, that->gc_overhead_limit_count());
        }
    }
    TRACE_OUT();
}

bool JEconoMem::asp_print_adaptive_size_policy_on(const AdaptiveSizePolicy* that, outputStream* st) {
    TRACE_IN();
    //  Should only be used with adaptive size policy turned on.
    // Otherwise, there may be variables that are undefined.
    if (!UseAdaptiveSizePolicy) { TRACE_OUT(); return false; }

    // Print goal for which action is needed.
    char* action = NULL;
    bool change_for_pause = false;
    if ((that->change_old_gen_for_maj_pauses() ==
         that->decrease_old_gen_for_maj_pauses_true) ||
        (that->change_young_gen_for_min_pauses() ==
         that->decrease_young_gen_for_min_pauses_true)) {
        action = (char*) " *** pause time goal ***";
        change_for_pause = true;
    } else if ((that->change_old_gen_for_throughput() ==
                that->increase_old_gen_for_throughput_true) ||
               (that->change_young_gen_for_throughput() ==
                that->increase_young_gen_for_througput_true)) {
        action = (char*) " *** throughput goal ***";
    } else if (that->decrease_for_footprint()) {
        action = (char*) " *** reduced footprint ***";
    } else {
        // No actions were taken.  This can legitimately be the
        // situation if not enough data has been gathered to make
        // decisions.
        TRACE_OUT();
        return false;
    }

    // Pauses
    // Currently the size of the old gen is only adjusted to
    // change the major pause times.
    char* young_gen_action = NULL;
    char* tenured_gen_action = NULL;

    char* shrink_msg = (char*) "(attempted to shrink)";
    char* grow_msg = (char*) "(attempted to grow)";
    char* no_change_msg = (char*) "(no change)";
    if (that->change_young_gen_for_min_pauses() ==
        that->decrease_young_gen_for_min_pauses_true) {
        young_gen_action = shrink_msg;
    } else if (change_for_pause) {
        young_gen_action = no_change_msg;
    }

    if (that->change_old_gen_for_maj_pauses() == that->decrease_old_gen_for_maj_pauses_true) {
        tenured_gen_action = shrink_msg;
    } else if (change_for_pause) {
        tenured_gen_action = no_change_msg;
    }

    // Throughput
    if (that->change_old_gen_for_throughput() == that->increase_old_gen_for_throughput_true) {
        assert(change_young_gen_for_throughput() ==
               increase_young_gen_for_througput_true,
               "Both generations should be growing");
        young_gen_action = grow_msg;
        tenured_gen_action = grow_msg;
    } else if (that->change_young_gen_for_throughput() ==
               that->increase_young_gen_for_througput_true) {
        // Only the young generation may grow at start up (before
        // enough full collections have been done to grow the old generation).
        young_gen_action = grow_msg;
        tenured_gen_action = no_change_msg;
    }

    // Minimum footprint
    if (that->decrease_for_footprint() != 0) {
        young_gen_action = shrink_msg;
        tenured_gen_action = shrink_msg;
    }

    st->print_cr("    UseAdaptiveSizePolicy actions to meet %s", action);
    st->print_cr("                       GC overhead (%%)");
    st->print_cr("    Young generation:     %7.2f\t  %s",
                 100.0 * that->avg_minor_gc_cost()->average(),
                 young_gen_action);
    st->print_cr("    Tenured generation:   %7.2f\t  %s",
                 100.0 * that->avg_major_gc_cost()->average(),
                 tenured_gen_action);
    TRACE_OUT();
    return true;
}

bool JEconoMem::asp_print_adaptive_size_policy_on(const AdaptiveSizePolicy* that,
                                                 outputStream* st,
                                                 int tenuring_threshold_arg) {
    TRACE_IN();
    if (!that->AdaptiveSizePolicy::print_adaptive_size_policy_on(st)) {
        TRACE_OUT();
        return false;
    }

    // Tenuring threshold
    bool tenuring_threshold_changed = true;
    if (that->decrement_tenuring_threshold_for_survivor_limit()) {
        st->print("    Tenuring threshold:    (attempted to decrease to avoid"
                  " survivor space overflow) = ");
    } else if (that->decrement_tenuring_threshold_for_gc_cost()) {
        st->print("    Tenuring threshold:    (attempted to decrease to balance"
                  " GC costs) = ");
    } else if (that->increment_tenuring_threshold_for_gc_cost()) {
        st->print("    Tenuring threshold:    (attempted to increase to balance"
                  " GC costs) = ");
    } else {
        tenuring_threshold_changed = false;
        assert(!tenuring_threshold_change(), "(no change was attempted)");
    }
    if (tenuring_threshold_changed) {
        st->print_cr("%d", tenuring_threshold_arg);
    }
    TRACE_OUT();
    return true;
}
