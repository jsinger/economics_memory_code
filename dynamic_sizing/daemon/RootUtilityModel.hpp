#ifndef ROOTUTILITYMODEL_HPP
#define ROOTUTILITYMODEL_HPP

#include "UtilityModel.hpp"
#include "UtilityFunction.hpp"

// A utility model based on U(h) = ah^b, where b<1; i.e. a 'nth root' function
// Combined utility for clients is found by multiplying the individual utilities


class RootUtilityFunction : public UtilityFunction
{
public:
    double c; // coefficient
    double e; // exponent

    RootUtilityFunction(double c, double e) : c(c), e(e) {}
    virtual double evaluate(double heapSize) const;
    virtual bool isValid() const;
    virtual void forceValidity();
    virtual std::string toString() const;
    virtual std::string toPython() const;
};


class RootUtilityModel : public UtilityModel
{
public:
    static const char* NAME;

    RootUtilityModel(bool alwaysNumericOptimisation);

    virtual UtilityFunction* calculateFuncFromData(ReadingIterator& it) const;
    virtual PartitionResult* partitionMemory(unsigned long long totalMem, const std::vector<const UtilityFunction*>& functions, const std::vector<unsigned long long>& minHeaps) const;

protected:
    virtual double combineRawUtilities(const std::vector<double>& utilities) const;

private:
    PartitionResult* partitionMemoryAnalytic(unsigned long long totalMem, const UtilityFunction* func1, const UtilityFunction* func2, unsigned long long minHeap1, unsigned long long minHeap2) const;

    bool m_AlwaysNumericOptimisation;

};

#endif
