economemd
    total memory: 864 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub36_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run05_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 17999: heap size 9 MB, throughput 0.999012
Clients: 1
Client 17999 has a minimum heap size of 12 MB
Reading from 17998: heap size 9 MB, throughput 0.998914
Clients: 2
Client 17998 has a minimum heap size of 276 MB
Reading from 17999: heap size 9 MB, throughput 0.997527
Reading from 17999: heap size 9 MB, throughput 0.960628
Reading from 17998: heap size 9 MB, throughput 0.995395
Reading from 17998: heap size 9 MB, throughput 0.989625
Reading from 17998: heap size 9 MB, throughput 0.963498
Reading from 17999: heap size 9 MB, throughput 0.997265
Reading from 17998: heap size 9 MB, throughput 0.995034
Reading from 17998: heap size 9 MB, throughput 0.977754
Reading from 17998: heap size 12 MB, throughput 0.973668
Reading from 17998: heap size 12 MB, throughput 0.936034
Reading from 17998: heap size 16 MB, throughput 0.928375
Reading from 17999: heap size 9 MB, throughput 0.984865
Reading from 17998: heap size 17 MB, throughput 0.734902
Reading from 17998: heap size 26 MB, throughput 0.936759
Reading from 17999: heap size 9 MB, throughput 0.978225
Reading from 17998: heap size 27 MB, throughput 0.326261
Reading from 17999: heap size 12 MB, throughput 0.958734
Reading from 17998: heap size 39 MB, throughput 0.937282
Reading from 17998: heap size 39 MB, throughput 0.881277
Reading from 17999: heap size 12 MB, throughput 0.984928
Reading from 17998: heap size 40 MB, throughput 0.355444
Reading from 17998: heap size 53 MB, throughput 0.797752
Reading from 17998: heap size 55 MB, throughput 0.818321
Reading from 17998: heap size 56 MB, throughput 0.192412
Reading from 17999: heap size 16 MB, throughput 0.995361
Reading from 17998: heap size 74 MB, throughput 0.827928
Reading from 17998: heap size 76 MB, throughput 0.870741
Reading from 17998: heap size 76 MB, throughput 0.183325
Reading from 17998: heap size 101 MB, throughput 0.888919
Reading from 17998: heap size 100 MB, throughput 0.796235
Reading from 17999: heap size 16 MB, throughput 0.992377
Reading from 17998: heap size 101 MB, throughput 0.692897
Reading from 17998: heap size 102 MB, throughput 0.759775
Reading from 17998: heap size 105 MB, throughput 0.817111
Reading from 17998: heap size 106 MB, throughput 0.772652
Reading from 17998: heap size 109 MB, throughput 0.716162
Reading from 17998: heap size 111 MB, throughput 0.764925
Reading from 17998: heap size 113 MB, throughput 0.741935
Reading from 17999: heap size 21 MB, throughput 0.930358
Reading from 17998: heap size 115 MB, throughput 0.142832
Reading from 17998: heap size 149 MB, throughput 0.642511
Reading from 17998: heap size 151 MB, throughput 0.613974
Reading from 17998: heap size 154 MB, throughput 0.624946
Reading from 17998: heap size 155 MB, throughput 0.671215
Reading from 17998: heap size 159 MB, throughput 0.57058
Reading from 17998: heap size 161 MB, throughput 0.49437
Reading from 17999: heap size 26 MB, throughput 0.988546
Reading from 17998: heap size 167 MB, throughput 0.504301
Reading from 17998: heap size 171 MB, throughput 0.500629
Reading from 17999: heap size 30 MB, throughput 0.98492
Reading from 17998: heap size 173 MB, throughput 0.100691
Reading from 17998: heap size 209 MB, throughput 0.500955
Reading from 17999: heap size 33 MB, throughput 0.992991
Reading from 17998: heap size 210 MB, throughput 0.602849
Reading from 17998: heap size 206 MB, throughput 0.798199
Reading from 17999: heap size 37 MB, throughput 0.99106
Reading from 17998: heap size 208 MB, throughput 0.843631
Reading from 17999: heap size 37 MB, throughput 0.992866
Reading from 17999: heap size 43 MB, throughput 0.992031
Reading from 17998: heap size 205 MB, throughput 0.8816
Reading from 17998: heap size 207 MB, throughput 0.503463
Reading from 17999: heap size 43 MB, throughput 0.980977
Reading from 17998: heap size 210 MB, throughput 0.666763
Reading from 17998: heap size 211 MB, throughput 0.506945
Reading from 17998: heap size 215 MB, throughput 0.676593
Reading from 17999: heap size 49 MB, throughput 0.987708
Reading from 17998: heap size 215 MB, throughput 0.838738
Reading from 17998: heap size 212 MB, throughput 0.74611
Reading from 17998: heap size 214 MB, throughput 0.845924
Reading from 17999: heap size 49 MB, throughput 0.993919
Reading from 17998: heap size 213 MB, throughput 0.847151
Reading from 17999: heap size 55 MB, throughput 0.988057
Reading from 17998: heap size 215 MB, throughput 0.775501
Reading from 17998: heap size 216 MB, throughput 0.764552
Reading from 17999: heap size 55 MB, throughput 0.727771
Reading from 17999: heap size 66 MB, throughput 0.989541
Reading from 17999: heap size 66 MB, throughput 0.992105
Reading from 17998: heap size 216 MB, throughput 0.100383
Reading from 17998: heap size 255 MB, throughput 0.464942
Reading from 17998: heap size 257 MB, throughput 0.653779
Reading from 17999: heap size 74 MB, throughput 0.995459
Reading from 17998: heap size 258 MB, throughput 0.735156
Reading from 17998: heap size 259 MB, throughput 0.716607
Reading from 17998: heap size 261 MB, throughput 0.885208
Reading from 17998: heap size 262 MB, throughput 0.948134
Reading from 17999: heap size 75 MB, throughput 0.995368
Reading from 17998: heap size 264 MB, throughput 0.958742
Reading from 17998: heap size 265 MB, throughput 0.791723
Reading from 17998: heap size 269 MB, throughput 0.764373
Reading from 17998: heap size 270 MB, throughput 0.726188
Reading from 17998: heap size 274 MB, throughput 0.845888
Reading from 17998: heap size 274 MB, throughput 0.938479
Reading from 17999: heap size 82 MB, throughput 0.996508
Equal recommendation: 432 MB each
Reading from 17998: heap size 275 MB, throughput 0.906563
Reading from 17998: heap size 277 MB, throughput 0.846833
Reading from 17999: heap size 82 MB, throughput 0.995504
Reading from 17998: heap size 277 MB, throughput 0.110495
Reading from 17998: heap size 320 MB, throughput 0.550983
Reading from 17998: heap size 324 MB, throughput 0.712879
Reading from 17998: heap size 324 MB, throughput 0.684096
Reading from 17998: heap size 330 MB, throughput 0.681736
Reading from 17998: heap size 330 MB, throughput 0.708799
Reading from 17999: heap size 88 MB, throughput 0.993371
Reading from 17998: heap size 334 MB, throughput 0.931516
Reading from 17999: heap size 89 MB, throughput 0.994646
Reading from 17999: heap size 96 MB, throughput 0.992432
Reading from 17998: heap size 335 MB, throughput 0.958959
Reading from 17999: heap size 96 MB, throughput 0.996243
Reading from 17999: heap size 101 MB, throughput 0.996378
Reading from 17998: heap size 338 MB, throughput 0.973105
Reading from 17999: heap size 103 MB, throughput 0.996556
Reading from 17998: heap size 339 MB, throughput 0.93712
Reading from 17999: heap size 108 MB, throughput 0.996459
Reading from 17999: heap size 108 MB, throughput 0.996572
Reading from 17998: heap size 342 MB, throughput 0.978596
Reading from 17999: heap size 114 MB, throughput 0.997194
Reading from 17999: heap size 114 MB, throughput 0.996162
Reading from 17998: heap size 343 MB, throughput 0.970241
Reading from 17999: heap size 119 MB, throughput 0.997333
Reading from 17999: heap size 119 MB, throughput 0.99781
Reading from 17998: heap size 346 MB, throughput 0.975271
Equal recommendation: 432 MB each
Reading from 17999: heap size 123 MB, throughput 0.998215
Reading from 17998: heap size 348 MB, throughput 0.96955
Reading from 17999: heap size 123 MB, throughput 0.997328
Reading from 17999: heap size 127 MB, throughput 0.998299
Reading from 17998: heap size 351 MB, throughput 0.97286
Reading from 17999: heap size 127 MB, throughput 0.997781
Reading from 17998: heap size 353 MB, throughput 0.972834
Reading from 17999: heap size 130 MB, throughput 0.997791
Reading from 17999: heap size 131 MB, throughput 0.997749
Reading from 17998: heap size 353 MB, throughput 0.971141
Reading from 17999: heap size 134 MB, throughput 0.998508
Reading from 17998: heap size 355 MB, throughput 0.976804
Reading from 17999: heap size 134 MB, throughput 0.997898
Reading from 17999: heap size 138 MB, throughput 0.998505
Reading from 17998: heap size 356 MB, throughput 0.98088
Reading from 17999: heap size 138 MB, throughput 0.996886
Reading from 17999: heap size 140 MB, throughput 0.988277
Reading from 17999: heap size 141 MB, throughput 0.985841
Reading from 17998: heap size 356 MB, throughput 0.980344
Reading from 17999: heap size 148 MB, throughput 0.997008
Equal recommendation: 432 MB each
Reading from 17998: heap size 358 MB, throughput 0.979701
Reading from 17999: heap size 149 MB, throughput 0.997457
Reading from 17998: heap size 359 MB, throughput 0.977671
Reading from 17999: heap size 156 MB, throughput 0.997088
Reading from 17999: heap size 157 MB, throughput 0.996903
Reading from 17998: heap size 361 MB, throughput 0.98631
Reading from 17999: heap size 163 MB, throughput 0.997507
Reading from 17998: heap size 362 MB, throughput 0.97678
Reading from 17998: heap size 365 MB, throughput 0.851927
Reading from 17999: heap size 164 MB, throughput 0.995274
Reading from 17998: heap size 365 MB, throughput 0.145026
Reading from 17998: heap size 410 MB, throughput 0.990829
Reading from 17998: heap size 412 MB, throughput 0.990176
Reading from 17998: heap size 419 MB, throughput 0.992563
Reading from 17999: heap size 170 MB, throughput 0.996958
Reading from 17998: heap size 422 MB, throughput 0.994504
Reading from 17999: heap size 170 MB, throughput 0.995913
Reading from 17999: heap size 177 MB, throughput 0.996671
Reading from 17998: heap size 426 MB, throughput 0.994785
Reading from 17999: heap size 177 MB, throughput 0.996355
Equal recommendation: 432 MB each
Reading from 17998: heap size 427 MB, throughput 0.993046
Reading from 17999: heap size 184 MB, throughput 0.947136
Reading from 17998: heap size 424 MB, throughput 0.987394
Reading from 17999: heap size 184 MB, throughput 0.996371
Reading from 17999: heap size 198 MB, throughput 0.934245
Reading from 17998: heap size 380 MB, throughput 0.99228
Reading from 17999: heap size 202 MB, throughput 0.998783
Reading from 17998: heap size 420 MB, throughput 0.990438
Reading from 17999: heap size 218 MB, throughput 0.998017
Reading from 17998: heap size 389 MB, throughput 0.973145
Equal recommendation: 432 MB each
Equal recommendation: 432 MB each
Reading from 17999: heap size 218 MB, throughput 0.748072
Equal recommendation: 432 MB each
Reading from 17998: heap size 417 MB, throughput 0.962728
Reading from 17998: heap size 419 MB, throughput 0.936421
Reading from 17999: heap size 233 MB, throughput 0.999017
Equal recommendation: 432 MB each
Reading from 17999: heap size 234 MB, throughput 0.9965
Reading from 17998: heap size 421 MB, throughput 0.942836
Reading from 17999: heap size 267 MB, throughput 0.99847
Reading from 17998: heap size 421 MB, throughput 0.93491
Reading from 17999: heap size 268 MB, throughput 0.998418
Reading from 17998: heap size 425 MB, throughput 0.919087
Reading from 17999: heap size 298 MB, throughput 0.998399
Reading from 17998: heap size 427 MB, throughput 0.989957
Reading from 17998: heap size 431 MB, throughput 0.97859
Reading from 17998: heap size 433 MB, throughput 0.823459
Equal recommendation: 432 MB each
Reading from 17998: heap size 430 MB, throughput 0.0255213
Reading from 17999: heap size 298 MB, throughput 0.738317
Reading from 17998: heap size 436 MB, throughput 0.539818
Reading from 17999: heap size 299 MB, throughput 0.994844
Reading from 17998: heap size 429 MB, throughput 0.990521
Reading from 17999: heap size 297 MB, throughput 0.978
Reading from 17998: heap size 442 MB, throughput 0.971828
Reading from 17999: heap size 298 MB, throughput 0.998798
Reading from 17998: heap size 420 MB, throughput 0.987283
Equal recommendation: 432 MB each
Reading from 17998: heap size 437 MB, throughput 0.943608
Reading from 17999: heap size 299 MB, throughput 0.995929
Reading from 17999: heap size 299 MB, throughput 0.996935
Reading from 17998: heap size 416 MB, throughput 0.984017
Reading from 17999: heap size 299 MB, throughput 0.998822
Reading from 17998: heap size 430 MB, throughput 0.986165
Reading from 17999: heap size 298 MB, throughput 0.98297
Reading from 17998: heap size 428 MB, throughput 0.982407
Reading from 17998: heap size 432 MB, throughput 0.98459
Reading from 17999: heap size 299 MB, throughput 0.998654
Equal recommendation: 432 MB each
Reading from 17998: heap size 432 MB, throughput 0.984464
Reading from 17999: heap size 299 MB, throughput 0.999037
Reading from 17998: heap size 434 MB, throughput 0.982971
Reading from 17999: heap size 299 MB, throughput 0.999128
Reading from 17998: heap size 424 MB, throughput 0.986743
Reading from 17999: heap size 299 MB, throughput 0.999352
Reading from 17998: heap size 431 MB, throughput 0.986535
Reading from 17999: heap size 294 MB, throughput 0.999232
Reading from 17998: heap size 433 MB, throughput 0.993033
Reading from 17999: heap size 298 MB, throughput 0.999057
Reading from 17998: heap size 410 MB, throughput 0.983278
Reading from 17998: heap size 430 MB, throughput 0.892506
Reading from 17998: heap size 430 MB, throughput 0.861313
Reading from 17998: heap size 435 MB, throughput 0.80031
Reading from 17999: heap size 298 MB, throughput 0.997748
Reading from 17998: heap size 426 MB, throughput 0.975671
Equal recommendation: 432 MB each
Reading from 17999: heap size 298 MB, throughput 0.997329
Reading from 17998: heap size 437 MB, throughput 0.991998
Reading from 17999: heap size 297 MB, throughput 0.998783
Reading from 17998: heap size 404 MB, throughput 0.990191
Reading from 17999: heap size 298 MB, throughput 0.998578
Reading from 17998: heap size 435 MB, throughput 0.988548
Reading from 17998: heap size 406 MB, throughput 0.988442
Reading from 17999: heap size 298 MB, throughput 0.99866
Reading from 17998: heap size 433 MB, throughput 0.981437
Reading from 17999: heap size 297 MB, throughput 0.998568
Reading from 17998: heap size 409 MB, throughput 0.982038
Equal recommendation: 432 MB each
Reading from 17999: heap size 298 MB, throughput 0.998533
Reading from 17998: heap size 429 MB, throughput 0.985943
Reading from 17999: heap size 299 MB, throughput 0.998288
Reading from 17998: heap size 428 MB, throughput 0.986491
Reading from 17999: heap size 296 MB, throughput 0.998557
Reading from 17998: heap size 429 MB, throughput 0.986257
Reading from 17999: heap size 298 MB, throughput 0.998587
Reading from 17998: heap size 429 MB, throughput 0.981445
Reading from 17999: heap size 298 MB, throughput 0.994452
Reading from 17998: heap size 432 MB, throughput 0.980594
Reading from 17999: heap size 297 MB, throughput 0.997566
Equal recommendation: 432 MB each
Reading from 17998: heap size 432 MB, throughput 0.989279
Reading from 17998: heap size 433 MB, throughput 0.917171
Reading from 17998: heap size 432 MB, throughput 0.84608
Reading from 17998: heap size 433 MB, throughput 0.876077
Reading from 17999: heap size 298 MB, throughput 0.998362
Reading from 17998: heap size 425 MB, throughput 0.915104
Reading from 17998: heap size 434 MB, throughput 0.991803
Reading from 17999: heap size 298 MB, throughput 0.998382
Reading from 17998: heap size 403 MB, throughput 0.990954
Reading from 17999: heap size 295 MB, throughput 0.997288
Reading from 17998: heap size 433 MB, throughput 0.990826
Reading from 17999: heap size 298 MB, throughput 0.997786
Reading from 17998: heap size 404 MB, throughput 0.988037
Reading from 17999: heap size 298 MB, throughput 0.998616
Reading from 17998: heap size 431 MB, throughput 0.989251
Equal recommendation: 432 MB each
Reading from 17999: heap size 298 MB, throughput 0.997953
Reading from 17998: heap size 430 MB, throughput 0.988088
Reading from 17999: heap size 298 MB, throughput 0.998393
Reading from 17998: heap size 427 MB, throughput 0.986384
Reading from 17999: heap size 298 MB, throughput 0.998334
Reading from 17998: heap size 429 MB, throughput 0.981967
Reading from 17999: heap size 299 MB, throughput 0.556235
Reading from 17998: heap size 430 MB, throughput 0.987037
Reading from 17999: heap size 304 MB, throughput 0.998703
Reading from 17998: heap size 430 MB, throughput 0.983007
Reading from 17999: heap size 305 MB, throughput 0.998788
Reading from 17998: heap size 432 MB, throughput 0.984205
Equal recommendation: 432 MB each
Reading from 17999: heap size 305 MB, throughput 0.999166
Reading from 17998: heap size 422 MB, throughput 0.988484
Reading from 17998: heap size 433 MB, throughput 0.957005
Reading from 17998: heap size 430 MB, throughput 0.877577
Reading from 17998: heap size 431 MB, throughput 0.869526
Reading from 17998: heap size 433 MB, throughput 0.868211
Reading from 17999: heap size 305 MB, throughput 0.998773
Reading from 17998: heap size 427 MB, throughput 0.987865
Reading from 17999: heap size 287 MB, throughput 0.998404
Reading from 17998: heap size 434 MB, throughput 0.991094
Reading from 17999: heap size 275 MB, throughput 0.99901
Reading from 17998: heap size 425 MB, throughput 0.990614
Reading from 17999: heap size 263 MB, throughput 0.998939
Reading from 17998: heap size 433 MB, throughput 0.990669
Reading from 17999: heap size 251 MB, throughput 0.99896
Equal recommendation: 432 MB each
Reading from 17998: heap size 423 MB, throughput 0.98945
Reading from 17999: heap size 241 MB, throughput 0.998408
Reading from 17999: heap size 231 MB, throughput 0.99559
Reading from 17998: heap size 430 MB, throughput 0.989461
Reading from 17999: heap size 242 MB, throughput 0.994535
Reading from 17998: heap size 427 MB, throughput 0.984783
Reading from 17999: heap size 254 MB, throughput 0.99843
Reading from 17998: heap size 429 MB, throughput 0.986091
Reading from 17999: heap size 262 MB, throughput 0.99782
Reading from 17998: heap size 427 MB, throughput 0.985794
Reading from 17999: heap size 273 MB, throughput 0.998221
Reading from 17998: heap size 429 MB, throughput 0.986451
Reading from 17999: heap size 283 MB, throughput 0.998129
Equal recommendation: 432 MB each
Reading from 17998: heap size 430 MB, throughput 0.983906
Reading from 17999: heap size 283 MB, throughput 0.998093
Reading from 17998: heap size 430 MB, throughput 0.982156
Reading from 17999: heap size 292 MB, throughput 0.998431
Reading from 17998: heap size 431 MB, throughput 0.990065
Reading from 17998: heap size 432 MB, throughput 0.906983
Reading from 17998: heap size 430 MB, throughput 0.867836
Reading from 17998: heap size 432 MB, throughput 0.85924
Reading from 17998: heap size 428 MB, throughput 0.888905
Reading from 17999: heap size 292 MB, throughput 0.998302
Reading from 17998: heap size 434 MB, throughput 0.991965
Reading from 17999: heap size 301 MB, throughput 0.998252
Reading from 17998: heap size 424 MB, throughput 0.990616
Reading from 17999: heap size 301 MB, throughput 0.9982
Reading from 17999: heap size 306 MB, throughput 0.993761
Equal recommendation: 432 MB each
Reading from 17998: heap size 433 MB, throughput 0.989141
Reading from 17999: heap size 306 MB, throughput 0.997469
Reading from 17998: heap size 422 MB, throughput 0.988891
Reading from 17999: heap size 303 MB, throughput 0.997857
Reading from 17998: heap size 430 MB, throughput 0.989436
Reading from 17998: heap size 428 MB, throughput 0.987501
Reading from 17999: heap size 304 MB, throughput 0.998194
Reading from 17998: heap size 430 MB, throughput 0.985484
Reading from 17999: heap size 304 MB, throughput 0.997406
Reading from 17998: heap size 428 MB, throughput 0.988494
Reading from 17999: heap size 304 MB, throughput 0.998216
Equal recommendation: 432 MB each
Reading from 17998: heap size 430 MB, throughput 0.988068
Reading from 17999: heap size 303 MB, throughput 0.99859
Reading from 17998: heap size 431 MB, throughput 0.984763
Reading from 17999: heap size 304 MB, throughput 0.99853
Reading from 17998: heap size 431 MB, throughput 0.980691
Reading from 17999: heap size 305 MB, throughput 0.998368
Reading from 17998: heap size 433 MB, throughput 0.987995
Reading from 17998: heap size 420 MB, throughput 0.971199
Reading from 17998: heap size 430 MB, throughput 0.860737
Reading from 17998: heap size 431 MB, throughput 0.863987
Reading from 17998: heap size 436 MB, throughput 0.872028
Reading from 17999: heap size 302 MB, throughput 0.997698
Reading from 17999: heap size 304 MB, throughput 0.992003
Reading from 17998: heap size 428 MB, throughput 0.9893
Reading from 17999: heap size 304 MB, throughput 0.99747
Reading from 17998: heap size 436 MB, throughput 0.990004
Equal recommendation: 432 MB each
Reading from 17999: heap size 305 MB, throughput 0.998164
Reading from 17998: heap size 406 MB, throughput 0.991699
Reading from 17998: heap size 436 MB, throughput 0.990024
Reading from 17999: heap size 304 MB, throughput 0.998449
Reading from 17998: heap size 407 MB, throughput 0.988296
Reading from 17999: heap size 304 MB, throughput 0.998388
Reading from 17998: heap size 434 MB, throughput 0.987797
Reading from 17999: heap size 304 MB, throughput 0.998308
Reading from 17998: heap size 410 MB, throughput 0.985948
Reading from 17999: heap size 304 MB, throughput 0.998567
Reading from 17998: heap size 430 MB, throughput 0.98606
Equal recommendation: 432 MB each
Reading from 17999: heap size 304 MB, throughput 0.99862
Reading from 17998: heap size 429 MB, throughput 0.985066
Reading from 17999: heap size 305 MB, throughput 0.998334
Reading from 17998: heap size 429 MB, throughput 0.984452
Reading from 17999: heap size 290 MB, throughput 0.997145
Reading from 17999: heap size 299 MB, throughput 0.995459
Reading from 17998: heap size 430 MB, throughput 0.560365
Reading from 17999: heap size 302 MB, throughput 0.998349
Reading from 17998: heap size 433 MB, throughput 0.992678
Reading from 17999: heap size 305 MB, throughput 0.99837
Reading from 17998: heap size 431 MB, throughput 0.989286
Reading from 17998: heap size 433 MB, throughput 0.86237
Reading from 17998: heap size 431 MB, throughput 0.880235
Reading from 17998: heap size 431 MB, throughput 0.886497
Equal recommendation: 432 MB each
Reading from 17998: heap size 433 MB, throughput 0.97667
Reading from 17999: heap size 305 MB, throughput 0.998457
Reading from 17998: heap size 423 MB, throughput 0.993774
Reading from 17999: heap size 305 MB, throughput 0.998337
Reading from 17998: heap size 433 MB, throughput 0.993269
Reading from 17999: heap size 305 MB, throughput 0.997612
Reading from 17998: heap size 422 MB, throughput 0.991517
Reading from 17999: heap size 304 MB, throughput 0.998501
Reading from 17998: heap size 430 MB, throughput 0.99037
Reading from 17999: heap size 305 MB, throughput 0.998546
Reading from 17998: heap size 429 MB, throughput 0.988187
Equal recommendation: 432 MB each
Reading from 17999: heap size 306 MB, throughput 0.99839
Reading from 17998: heap size 431 MB, throughput 0.984203
Reading from 17999: heap size 290 MB, throughput 0.879544
Reading from 17999: heap size 301 MB, throughput 0.996831
Reading from 17998: heap size 428 MB, throughput 0.987247
Reading from 17998: heap size 430 MB, throughput 0.984408
Reading from 17999: heap size 288 MB, throughput 0.999014
Reading from 17998: heap size 432 MB, throughput 0.983045
Reading from 17999: heap size 275 MB, throughput 0.998946
Reading from 17999: heap size 263 MB, throughput 0.998885
Reading from 17998: heap size 422 MB, throughput 0.974402
Reading from 17999: heap size 251 MB, throughput 0.998572
Equal recommendation: 432 MB each
Reading from 17998: heap size 431 MB, throughput 0.968741
Reading from 17999: heap size 241 MB, throughput 0.998657
Reading from 17998: heap size 432 MB, throughput 0.993102
Reading from 17998: heap size 434 MB, throughput 0.931963
Reading from 17998: heap size 433 MB, throughput 0.867472
Reading from 17998: heap size 434 MB, throughput 0.86661
Reading from 17999: heap size 231 MB, throughput 0.998749
Reading from 17998: heap size 425 MB, throughput 0.908858
Reading from 17999: heap size 221 MB, throughput 0.998716
Reading from 17998: heap size 436 MB, throughput 0.991694
Reading from 17999: heap size 212 MB, throughput 0.998178
Reading from 17998: heap size 402 MB, throughput 0.986419
Reading from 17999: heap size 204 MB, throughput 0.998505
Reading from 17999: heap size 195 MB, throughput 0.997656
Reading from 17998: heap size 434 MB, throughput 0.991557
Reading from 17999: heap size 191 MB, throughput 0.99496
Reading from 17999: heap size 182 MB, throughput 0.989286
Reading from 17999: heap size 195 MB, throughput 0.996491
Reading from 17998: heap size 404 MB, throughput 0.990553
Equal recommendation: 432 MB each
Reading from 17999: heap size 206 MB, throughput 0.997593
Reading from 17998: heap size 432 MB, throughput 0.989584
Reading from 17999: heap size 219 MB, throughput 0.998436
Reading from 17998: heap size 431 MB, throughput 0.984752
Reading from 17999: heap size 210 MB, throughput 0.997357
Reading from 17999: heap size 201 MB, throughput 0.998162
Reading from 17998: heap size 429 MB, throughput 0.987831
Reading from 17999: heap size 193 MB, throughput 0.998329
Reading from 17998: heap size 431 MB, throughput 0.986146
Reading from 17999: heap size 185 MB, throughput 0.997987
Reading from 17999: heap size 178 MB, throughput 0.997825
Reading from 17998: heap size 431 MB, throughput 0.987764
Reading from 17999: heap size 171 MB, throughput 0.99749
Equal recommendation: 432 MB each
Reading from 17998: heap size 432 MB, throughput 0.984514
Reading from 17999: heap size 164 MB, throughput 0.996946
Reading from 17999: heap size 158 MB, throughput 0.997197
Reading from 17998: heap size 434 MB, throughput 0.984915
Reading from 17999: heap size 152 MB, throughput 0.997297
Reading from 17999: heap size 146 MB, throughput 0.997062
Reading from 17999: heap size 141 MB, throughput 0.997145
Reading from 17998: heap size 424 MB, throughput 0.992478
Reading from 17999: heap size 135 MB, throughput 0.99734
Reading from 17998: heap size 434 MB, throughput 0.979324
Reading from 17998: heap size 416 MB, throughput 0.88946
Reading from 17998: heap size 426 MB, throughput 0.869978
Reading from 17998: heap size 430 MB, throughput 0.859501
Reading from 17999: heap size 129 MB, throughput 0.996754
Reading from 17999: heap size 128 MB, throughput 0.991543
Reading from 17999: heap size 134 MB, throughput 0.989682
Reading from 17998: heap size 438 MB, throughput 0.980737
Reading from 17999: heap size 145 MB, throughput 0.989781
Reading from 17999: heap size 156 MB, throughput 0.995551
Reading from 17999: heap size 169 MB, throughput 0.996744
Reading from 17998: heap size 408 MB, throughput 0.993173
Reading from 17999: heap size 182 MB, throughput 0.997987
Equal recommendation: 432 MB each
Reading from 17998: heap size 438 MB, throughput 0.991124
Reading from 17999: heap size 194 MB, throughput 0.997363
Reading from 17999: heap size 207 MB, throughput 0.99745
Reading from 17998: heap size 409 MB, throughput 0.990854
Reading from 17999: heap size 219 MB, throughput 0.99797
Reading from 17998: heap size 436 MB, throughput 0.989115
Reading from 17999: heap size 210 MB, throughput 0.99739
Reading from 17998: heap size 411 MB, throughput 0.987681
Reading from 17999: heap size 202 MB, throughput 0.997884
Reading from 17999: heap size 194 MB, throughput 0.996856
Reading from 17998: heap size 433 MB, throughput 0.987409
Reading from 17999: heap size 186 MB, throughput 0.997726
Reading from 17998: heap size 413 MB, throughput 0.985955
Reading from 17999: heap size 178 MB, throughput 0.99742
Equal recommendation: 432 MB each
Reading from 17999: heap size 171 MB, throughput 0.997187
Reading from 17998: heap size 428 MB, throughput 0.986403
Reading from 17999: heap size 165 MB, throughput 0.997246
Reading from 17999: heap size 158 MB, throughput 0.997428
Reading from 17998: heap size 428 MB, throughput 0.985145
Reading from 17999: heap size 151 MB, throughput 0.998116
Reading from 17999: heap size 145 MB, throughput 0.991246
Reading from 17999: heap size 156 MB, throughput 0.990139
Reading from 17998: heap size 430 MB, throughput 0.978664
Reading from 17999: heap size 169 MB, throughput 0.995056
Reading from 17999: heap size 179 MB, throughput 0.998137
Reading from 17998: heap size 430 MB, throughput 0.982364
Reading from 17999: heap size 191 MB, throughput 0.998098
Reading from 17998: heap size 429 MB, throughput 0.980777
Reading from 17998: heap size 434 MB, throughput 0.842516
Reading from 17998: heap size 430 MB, throughput 0.820135
Reading from 17998: heap size 437 MB, throughput 0.816064
Reading from 17998: heap size 432 MB, throughput 0.863463
Reading from 17999: heap size 203 MB, throughput 0.997573
Reading from 17999: heap size 195 MB, throughput 0.998114
Reading from 17998: heap size 440 MB, throughput 0.988175
Reading from 17999: heap size 187 MB, throughput 0.9981
Equal recommendation: 432 MB each
Reading from 17998: heap size 422 MB, throughput 0.986577
Reading from 17999: heap size 179 MB, throughput 0.997853
Reading from 17999: heap size 172 MB, throughput 0.996416
Reading from 17998: heap size 436 MB, throughput 0.990124
Reading from 17999: heap size 166 MB, throughput 0.996072
Reading from 17999: heap size 159 MB, throughput 0.996609
Reading from 17998: heap size 421 MB, throughput 0.985932
Reading from 17999: heap size 153 MB, throughput 0.996388
Reading from 17999: heap size 147 MB, throughput 0.997396
Reading from 17998: heap size 431 MB, throughput 0.987278
Reading from 17999: heap size 142 MB, throughput 0.997047
Reading from 17999: heap size 137 MB, throughput 0.994957
Reading from 17998: heap size 432 MB, throughput 0.986087
Reading from 17999: heap size 131 MB, throughput 0.901615
Reading from 17999: heap size 123 MB, throughput 0.998469
Reading from 17999: heap size 122 MB, throughput 0.997725
Reading from 17998: heap size 434 MB, throughput 0.988043
Equal recommendation: 432 MB each
Reading from 17998: heap size 423 MB, throughput 0.986103
Client 17999 died
Clients: 1
Reading from 17998: heap size 430 MB, throughput 0.985432
Reading from 17998: heap size 429 MB, throughput 0.983316
Reading from 17998: heap size 430 MB, throughput 0.863221
Reading from 17998: heap size 442 MB, throughput 0.98765
Reading from 17998: heap size 413 MB, throughput 0.989235
Reading from 17998: heap size 442 MB, throughput 0.898944
Reading from 17998: heap size 432 MB, throughput 0.872068
Reading from 17998: heap size 442 MB, throughput 0.876908
Reading from 17998: heap size 430 MB, throughput 0.922262
Recommendation: one client; give it all the memory
Reading from 17998: heap size 440 MB, throughput 0.991732
Reading from 17998: heap size 441 MB, throughput 0.99205
Reading from 17998: heap size 446 MB, throughput 0.991318
Reading from 17998: heap size 448 MB, throughput 0.990592
Reading from 17998: heap size 449 MB, throughput 0.990107
Reading from 17998: heap size 451 MB, throughput 0.987817
Recommendation: one client; give it all the memory
Reading from 17998: heap size 450 MB, throughput 0.988518
Reading from 17998: heap size 452 MB, throughput 0.987451
Reading from 17998: heap size 454 MB, throughput 0.987032
Reading from 17998: heap size 455 MB, throughput 0.986531
Reading from 17998: heap size 459 MB, throughput 0.984458
Reading from 17998: heap size 459 MB, throughput 0.989412
Reading from 17998: heap size 466 MB, throughput 0.907071
Reading from 17998: heap size 466 MB, throughput 0.855896
Reading from 17998: heap size 473 MB, throughput 0.892428
Client 17998 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
Recommendation: no clients; no recommendation
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
