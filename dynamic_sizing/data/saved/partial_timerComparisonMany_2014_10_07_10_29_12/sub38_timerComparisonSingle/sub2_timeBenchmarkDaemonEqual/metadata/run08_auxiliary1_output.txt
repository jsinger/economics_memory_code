economemd
    total memory: 1440 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub38_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 25879: heap size 9 MB, throughput 0.983995
Clients: 1
Client 25879 has a minimum heap size of 12 MB
Reading from 25878: heap size 9 MB, throughput 0.985794
Clients: 2
Client 25878 has a minimum heap size of 276 MB
Reading from 25879: heap size 9 MB, throughput 0.988237
Reading from 25878: heap size 9 MB, throughput 0.968063
Reading from 25879: heap size 11 MB, throughput 0.977808
Reading from 25878: heap size 11 MB, throughput 0.971737
Reading from 25879: heap size 11 MB, throughput 0.982906
Reading from 25878: heap size 11 MB, throughput 0.97067
Reading from 25878: heap size 15 MB, throughput 0.886973
Reading from 25879: heap size 15 MB, throughput 0.977545
Reading from 25878: heap size 19 MB, throughput 0.927389
Reading from 25879: heap size 15 MB, throughput 0.985613
Reading from 25878: heap size 25 MB, throughput 0.882105
Reading from 25878: heap size 29 MB, throughput 0.886879
Reading from 25878: heap size 34 MB, throughput 0.499225
Reading from 25878: heap size 44 MB, throughput 0.914907
Reading from 25879: heap size 25 MB, throughput 0.914218
Reading from 25878: heap size 50 MB, throughput 0.860838
Reading from 25878: heap size 53 MB, throughput 0.317953
Reading from 25878: heap size 70 MB, throughput 0.763977
Reading from 25879: heap size 30 MB, throughput 0.988668
Reading from 25878: heap size 73 MB, throughput 0.762453
Reading from 25878: heap size 77 MB, throughput 0.241249
Reading from 25879: heap size 36 MB, throughput 0.986692
Reading from 25878: heap size 98 MB, throughput 0.789013
Reading from 25878: heap size 107 MB, throughput 0.882026
Reading from 25879: heap size 40 MB, throughput 0.991699
Reading from 25878: heap size 108 MB, throughput 0.761065
Reading from 25879: heap size 42 MB, throughput 0.832613
Reading from 25879: heap size 50 MB, throughput 0.992672
Reading from 25878: heap size 112 MB, throughput 0.271474
Reading from 25878: heap size 139 MB, throughput 0.612852
Reading from 25879: heap size 53 MB, throughput 0.99351
Reading from 25878: heap size 144 MB, throughput 0.744622
Reading from 25878: heap size 149 MB, throughput 0.567235
Reading from 25879: heap size 54 MB, throughput 0.990388
Reading from 25878: heap size 154 MB, throughput 0.613746
Reading from 25879: heap size 58 MB, throughput 0.985729
Reading from 25878: heap size 162 MB, throughput 0.524395
Reading from 25879: heap size 58 MB, throughput 0.977901
Reading from 25879: heap size 62 MB, throughput 0.981722
Reading from 25879: heap size 62 MB, throughput 0.96829
Reading from 25878: heap size 171 MB, throughput 0.104951
Reading from 25879: heap size 67 MB, throughput 0.987616
Reading from 25878: heap size 211 MB, throughput 0.159587
Reading from 25879: heap size 67 MB, throughput 0.991598
Reading from 25878: heap size 253 MB, throughput 0.826157
Reading from 25879: heap size 72 MB, throughput 0.994099
Reading from 25878: heap size 255 MB, throughput 0.952911
Reading from 25878: heap size 255 MB, throughput 0.890581
Reading from 25878: heap size 256 MB, throughput 0.652843
Reading from 25879: heap size 73 MB, throughput 0.994292
Reading from 25878: heap size 260 MB, throughput 0.824161
Reading from 25878: heap size 260 MB, throughput 0.842136
Reading from 25878: heap size 265 MB, throughput 0.831482
Reading from 25878: heap size 266 MB, throughput 0.847992
Reading from 25879: heap size 76 MB, throughput 0.995082
Reading from 25878: heap size 267 MB, throughput 0.720767
Reading from 25878: heap size 271 MB, throughput 0.67287
Reading from 25878: heap size 277 MB, throughput 0.691385
Reading from 25878: heap size 279 MB, throughput 0.67614
Reading from 25879: heap size 77 MB, throughput 0.994634
Reading from 25878: heap size 284 MB, throughput 0.950867
Reading from 25879: heap size 81 MB, throughput 0.994333
Reading from 25878: heap size 285 MB, throughput 0.770755
Reading from 25878: heap size 287 MB, throughput 0.713727
Reading from 25879: heap size 81 MB, throughput 0.99378
Reading from 25878: heap size 289 MB, throughput 0.117001
Reading from 25879: heap size 84 MB, throughput 0.992565
Reading from 25878: heap size 327 MB, throughput 0.781997
Reading from 25878: heap size 329 MB, throughput 0.903924
Reading from 25878: heap size 334 MB, throughput 0.757666
Reading from 25878: heap size 335 MB, throughput 0.793382
Reading from 25879: heap size 84 MB, throughput 0.995025
Reading from 25878: heap size 335 MB, throughput 0.707688
Reading from 25878: heap size 337 MB, throughput 0.717627
Reading from 25878: heap size 341 MB, throughput 0.716897
Reading from 25878: heap size 342 MB, throughput 0.722289
Reading from 25879: heap size 87 MB, throughput 0.994444
Equal recommendation: 720 MB each
Reading from 25879: heap size 87 MB, throughput 0.992185
Reading from 25879: heap size 90 MB, throughput 0.992413
Reading from 25878: heap size 351 MB, throughput 0.972766
Reading from 25879: heap size 90 MB, throughput 0.995025
Reading from 25879: heap size 93 MB, throughput 0.995879
Reading from 25879: heap size 93 MB, throughput 0.996118
Reading from 25878: heap size 351 MB, throughput 0.979496
Reading from 25879: heap size 96 MB, throughput 0.995485
Reading from 25879: heap size 96 MB, throughput 0.994507
Reading from 25879: heap size 98 MB, throughput 0.997106
Reading from 25878: heap size 353 MB, throughput 0.983077
Reading from 25879: heap size 98 MB, throughput 0.997484
Reading from 25879: heap size 100 MB, throughput 0.998087
Reading from 25879: heap size 100 MB, throughput 0.997691
Reading from 25879: heap size 101 MB, throughput 0.998016
Reading from 25878: heap size 356 MB, throughput 0.710037
Reading from 25879: heap size 101 MB, throughput 0.996372
Reading from 25879: heap size 102 MB, throughput 0.997854
Reading from 25878: heap size 405 MB, throughput 0.981024
Reading from 25879: heap size 102 MB, throughput 0.998089
Reading from 25879: heap size 104 MB, throughput 0.998245
Equal recommendation: 720 MB each
Reading from 25879: heap size 104 MB, throughput 0.991075
Reading from 25878: heap size 405 MB, throughput 0.993171
Reading from 25879: heap size 105 MB, throughput 0.998374
Reading from 25879: heap size 105 MB, throughput 0.997785
Reading from 25878: heap size 412 MB, throughput 0.992278
Reading from 25879: heap size 106 MB, throughput 0.998163
Reading from 25879: heap size 106 MB, throughput 0.998002
Reading from 25879: heap size 107 MB, throughput 0.998279
Reading from 25879: heap size 107 MB, throughput 0.990252
Reading from 25878: heap size 413 MB, throughput 0.976269
Reading from 25879: heap size 108 MB, throughput 0.989532
Reading from 25879: heap size 109 MB, throughput 0.966921
Reading from 25879: heap size 111 MB, throughput 0.975752
Reading from 25879: heap size 112 MB, throughput 0.995456
Reading from 25878: heap size 413 MB, throughput 0.985585
Reading from 25879: heap size 116 MB, throughput 0.996843
Reading from 25879: heap size 117 MB, throughput 0.995377
Reading from 25878: heap size 415 MB, throughput 0.990342
Reading from 25879: heap size 121 MB, throughput 0.996387
Reading from 25879: heap size 122 MB, throughput 0.996415
Reading from 25879: heap size 125 MB, throughput 0.996432
Reading from 25878: heap size 409 MB, throughput 0.990345
Reading from 25879: heap size 125 MB, throughput 0.996085
Equal recommendation: 720 MB each
Reading from 25879: heap size 128 MB, throughput 0.996991
Reading from 25878: heap size 380 MB, throughput 0.984728
Reading from 25879: heap size 128 MB, throughput 0.996583
Reading from 25879: heap size 130 MB, throughput 0.997222
Reading from 25878: heap size 408 MB, throughput 0.989769
Reading from 25879: heap size 130 MB, throughput 0.996057
Reading from 25879: heap size 133 MB, throughput 0.995603
Reading from 25878: heap size 381 MB, throughput 0.97991
Reading from 25878: heap size 408 MB, throughput 0.860796
Reading from 25878: heap size 409 MB, throughput 0.807914
Reading from 25878: heap size 416 MB, throughput 0.837369
Reading from 25879: heap size 133 MB, throughput 0.997276
Reading from 25878: heap size 420 MB, throughput 0.964131
Reading from 25879: heap size 134 MB, throughput 0.997992
Reading from 25879: heap size 135 MB, throughput 0.997752
Reading from 25878: heap size 425 MB, throughput 0.988864
Reading from 25879: heap size 137 MB, throughput 0.99833
Reading from 25879: heap size 137 MB, throughput 0.996497
Reading from 25878: heap size 428 MB, throughput 0.987745
Reading from 25879: heap size 138 MB, throughput 0.998345
Reading from 25879: heap size 138 MB, throughput 0.998602
Equal recommendation: 720 MB each
Reading from 25879: heap size 140 MB, throughput 0.998619
Reading from 25878: heap size 424 MB, throughput 0.989432
Reading from 25879: heap size 140 MB, throughput 0.998089
Reading from 25879: heap size 141 MB, throughput 0.995879
Reading from 25879: heap size 141 MB, throughput 0.981368
Reading from 25878: heap size 428 MB, throughput 0.978852
Reading from 25879: heap size 142 MB, throughput 0.991065
Reading from 25879: heap size 143 MB, throughput 0.995735
Reading from 25879: heap size 147 MB, throughput 0.997351
Reading from 25878: heap size 425 MB, throughput 0.988448
Reading from 25879: heap size 147 MB, throughput 0.996752
Reading from 25878: heap size 427 MB, throughput 0.984132
Reading from 25879: heap size 150 MB, throughput 0.997353
Reading from 25879: heap size 150 MB, throughput 0.997328
Reading from 25879: heap size 152 MB, throughput 0.99758
Reading from 25878: heap size 426 MB, throughput 0.987699
Reading from 25879: heap size 153 MB, throughput 0.997179
Reading from 25879: heap size 155 MB, throughput 0.997498
Equal recommendation: 720 MB each
Reading from 25878: heap size 427 MB, throughput 0.986959
Reading from 25879: heap size 155 MB, throughput 0.997286
Reading from 25879: heap size 157 MB, throughput 0.997434
Reading from 25878: heap size 428 MB, throughput 0.986087
Reading from 25879: heap size 157 MB, throughput 0.997189
Reading from 25879: heap size 159 MB, throughput 0.997303
Reading from 25878: heap size 428 MB, throughput 0.982159
Reading from 25879: heap size 159 MB, throughput 0.99677
Reading from 25879: heap size 160 MB, throughput 0.997561
Reading from 25878: heap size 430 MB, throughput 0.991912
Reading from 25878: heap size 430 MB, throughput 0.908049
Reading from 25878: heap size 431 MB, throughput 0.890992
Reading from 25878: heap size 432 MB, throughput 0.877989
Reading from 25879: heap size 160 MB, throughput 0.898027
Reading from 25879: heap size 166 MB, throughput 0.998768
Reading from 25878: heap size 438 MB, throughput 0.990752
Reading from 25879: heap size 166 MB, throughput 0.998028
Reading from 25879: heap size 168 MB, throughput 0.99746
Equal recommendation: 720 MB each
Reading from 25879: heap size 169 MB, throughput 0.991972
Reading from 25878: heap size 439 MB, throughput 0.988957
Reading from 25879: heap size 170 MB, throughput 0.99258
Reading from 25879: heap size 171 MB, throughput 0.99684
Reading from 25879: heap size 176 MB, throughput 0.996722
Reading from 25878: heap size 444 MB, throughput 0.992518
Reading from 25879: heap size 176 MB, throughput 0.996219
Reading from 25879: heap size 179 MB, throughput 0.997894
Reading from 25878: heap size 445 MB, throughput 0.991073
Reading from 25879: heap size 179 MB, throughput 0.997314
Reading from 25879: heap size 182 MB, throughput 0.997955
Reading from 25878: heap size 445 MB, throughput 0.991573
Reading from 25879: heap size 183 MB, throughput 0.996076
Reading from 25879: heap size 186 MB, throughput 0.997515
Reading from 25878: heap size 447 MB, throughput 0.989757
Equal recommendation: 720 MB each
Reading from 25879: heap size 186 MB, throughput 0.997301
Reading from 25879: heap size 189 MB, throughput 0.997579
Reading from 25878: heap size 443 MB, throughput 0.989454
Reading from 25879: heap size 189 MB, throughput 0.995548
Reading from 25879: heap size 193 MB, throughput 0.994523
Reading from 25878: heap size 446 MB, throughput 0.988368
Reading from 25879: heap size 193 MB, throughput 0.996594
Reading from 25879: heap size 196 MB, throughput 0.997934
Reading from 25878: heap size 446 MB, throughput 0.989076
Reading from 25879: heap size 197 MB, throughput 0.996956
Reading from 25879: heap size 201 MB, throughput 0.991225
Reading from 25879: heap size 201 MB, throughput 0.995496
Reading from 25879: heap size 206 MB, throughput 0.998069
Reading from 25878: heap size 447 MB, throughput 0.994085
Reading from 25878: heap size 449 MB, throughput 0.937411
Equal recommendation: 720 MB each
Reading from 25878: heap size 449 MB, throughput 0.886741
Reading from 25878: heap size 453 MB, throughput 0.935099
Reading from 25879: heap size 206 MB, throughput 0.99744
Reading from 25879: heap size 209 MB, throughput 0.99822
Reading from 25878: heap size 454 MB, throughput 0.994051
Reading from 25879: heap size 210 MB, throughput 0.99768
Reading from 25879: heap size 214 MB, throughput 0.998185
Reading from 25878: heap size 458 MB, throughput 0.993164
Reading from 25879: heap size 214 MB, throughput 0.997643
Reading from 25878: heap size 459 MB, throughput 0.991943
Reading from 25879: heap size 217 MB, throughput 0.997462
Reading from 25879: heap size 217 MB, throughput 0.997446
Equal recommendation: 720 MB each
Reading from 25878: heap size 459 MB, throughput 0.99235
Reading from 25879: heap size 221 MB, throughput 0.99754
Reading from 25879: heap size 221 MB, throughput 0.997651
Reading from 25878: heap size 461 MB, throughput 0.989972
Reading from 25879: heap size 224 MB, throughput 0.997274
Reading from 25879: heap size 224 MB, throughput 0.997595
Reading from 25879: heap size 228 MB, throughput 0.993117
Reading from 25878: heap size 458 MB, throughput 0.991693
Reading from 25879: heap size 228 MB, throughput 0.995812
Reading from 25879: heap size 233 MB, throughput 0.997869
Reading from 25878: heap size 460 MB, throughput 0.989236
Reading from 25879: heap size 233 MB, throughput 0.997858
Equal recommendation: 720 MB each
Reading from 25879: heap size 237 MB, throughput 0.998313
Reading from 25878: heap size 462 MB, throughput 0.989598
Reading from 25879: heap size 238 MB, throughput 0.998171
Reading from 25878: heap size 462 MB, throughput 0.993128
Reading from 25878: heap size 463 MB, throughput 0.929859
Reading from 25878: heap size 464 MB, throughput 0.896459
Reading from 25879: heap size 242 MB, throughput 0.998472
Reading from 25878: heap size 468 MB, throughput 0.986557
Reading from 25879: heap size 242 MB, throughput 0.997926
Reading from 25879: heap size 244 MB, throughput 0.998165
Reading from 25878: heap size 470 MB, throughput 0.994275
Reading from 25879: heap size 245 MB, throughput 0.997589
Reading from 25879: heap size 248 MB, throughput 0.994993
Reading from 25878: heap size 473 MB, throughput 0.993149
Equal recommendation: 720 MB each
Reading from 25879: heap size 248 MB, throughput 0.997849
Reading from 25879: heap size 253 MB, throughput 0.994
Reading from 25878: heap size 474 MB, throughput 0.992771
Reading from 25879: heap size 253 MB, throughput 0.994264
Reading from 25879: heap size 258 MB, throughput 0.99672
Reading from 25878: heap size 473 MB, throughput 0.99202
Reading from 25879: heap size 259 MB, throughput 0.997863
Reading from 25878: heap size 475 MB, throughput 0.990638
Reading from 25879: heap size 264 MB, throughput 0.997912
Reading from 25879: heap size 264 MB, throughput 0.998145
Equal recommendation: 720 MB each
Reading from 25878: heap size 474 MB, throughput 0.990857
Reading from 25879: heap size 268 MB, throughput 0.998301
Reading from 25879: heap size 269 MB, throughput 0.9973
Reading from 25878: heap size 475 MB, throughput 0.988668
Reading from 25879: heap size 273 MB, throughput 0.998279
Reading from 25878: heap size 476 MB, throughput 0.993419
Reading from 25878: heap size 477 MB, throughput 0.92652
Reading from 25878: heap size 476 MB, throughput 0.912469
Reading from 25879: heap size 273 MB, throughput 0.998048
Reading from 25878: heap size 478 MB, throughput 0.987867
Reading from 25879: heap size 277 MB, throughput 0.998033
Reading from 25879: heap size 277 MB, throughput 0.996161
Equal recommendation: 720 MB each
Reading from 25879: heap size 280 MB, throughput 0.994941
Reading from 25878: heap size 485 MB, throughput 0.994213
Reading from 25879: heap size 281 MB, throughput 0.998036
Reading from 25878: heap size 485 MB, throughput 0.994311
Reading from 25879: heap size 285 MB, throughput 0.998191
Reading from 25879: heap size 286 MB, throughput 0.997812
Reading from 25878: heap size 486 MB, throughput 0.99373
Reading from 25879: heap size 290 MB, throughput 0.998324
Reading from 25878: heap size 487 MB, throughput 0.99119
Reading from 25879: heap size 290 MB, throughput 0.997144
Equal recommendation: 720 MB each
Reading from 25879: heap size 295 MB, throughput 0.998368
Reading from 25878: heap size 486 MB, throughput 0.991088
Reading from 25879: heap size 295 MB, throughput 0.997484
Reading from 25878: heap size 487 MB, throughput 0.990707
Reading from 25879: heap size 299 MB, throughput 0.998602
Reading from 25879: heap size 299 MB, throughput 0.996254
Reading from 25879: heap size 302 MB, throughput 0.9959
Reading from 25878: heap size 488 MB, throughput 0.99354
Reading from 25878: heap size 489 MB, throughput 0.97412
Reading from 25878: heap size 487 MB, throughput 0.918331
Reading from 25879: heap size 303 MB, throughput 0.998145
Reading from 25878: heap size 490 MB, throughput 0.984293
Equal recommendation: 720 MB each
Reading from 25879: heap size 308 MB, throughput 0.998288
Reading from 25878: heap size 495 MB, throughput 0.993845
Reading from 25879: heap size 308 MB, throughput 0.997992
Reading from 25879: heap size 312 MB, throughput 0.997697
Reading from 25878: heap size 496 MB, throughput 0.993643
Reading from 25879: heap size 312 MB, throughput 0.998097
Reading from 25878: heap size 496 MB, throughput 0.992445
Reading from 25879: heap size 317 MB, throughput 0.998329
Equal recommendation: 720 MB each
Reading from 25879: heap size 317 MB, throughput 0.998334
Reading from 25878: heap size 498 MB, throughput 0.992608
Reading from 25879: heap size 321 MB, throughput 0.998424
Reading from 25879: heap size 321 MB, throughput 0.991324
Reading from 25878: heap size 496 MB, throughput 0.990776
Reading from 25879: heap size 324 MB, throughput 0.9972
Reading from 25878: heap size 497 MB, throughput 0.990931
Reading from 25879: heap size 325 MB, throughput 0.998006
Equal recommendation: 720 MB each
Reading from 25879: heap size 330 MB, throughput 0.998491
Reading from 25878: heap size 498 MB, throughput 0.995476
Reading from 25878: heap size 499 MB, throughput 0.943293
Reading from 25878: heap size 498 MB, throughput 0.923884
Reading from 25879: heap size 331 MB, throughput 0.998174
Reading from 25878: heap size 501 MB, throughput 0.990098
Reading from 25879: heap size 334 MB, throughput 0.998319
Reading from 25878: heap size 506 MB, throughput 0.994816
Reading from 25879: heap size 335 MB, throughput 0.997893
Reading from 25879: heap size 339 MB, throughput 0.998052
Reading from 25878: heap size 507 MB, throughput 0.99389
Equal recommendation: 720 MB each
Reading from 25879: heap size 340 MB, throughput 0.997731
Reading from 25879: heap size 344 MB, throughput 0.29127
Reading from 25879: heap size 348 MB, throughput 0.997318
Reading from 25878: heap size 507 MB, throughput 0.992974
Reading from 25879: heap size 372 MB, throughput 0.999022
Reading from 25878: heap size 509 MB, throughput 0.992268
Reading from 25879: heap size 372 MB, throughput 0.998669
Reading from 25878: heap size 508 MB, throughput 0.992202
Reading from 25879: heap size 388 MB, throughput 0.998806
Equal recommendation: 720 MB each
Reading from 25879: heap size 391 MB, throughput 0.998602
Reading from 25878: heap size 509 MB, throughput 0.991564
Reading from 25879: heap size 403 MB, throughput 0.998993
Reading from 25878: heap size 510 MB, throughput 0.987939
Reading from 25878: heap size 511 MB, throughput 0.921093
Reading from 25878: heap size 514 MB, throughput 0.985859
Reading from 25879: heap size 406 MB, throughput 0.998604
Reading from 25879: heap size 414 MB, throughput 0.99437
Reading from 25878: heap size 515 MB, throughput 0.994192
Equal recommendation: 720 MB each
Reading from 25879: heap size 416 MB, throughput 0.997893
Reading from 25878: heap size 518 MB, throughput 0.994125
Reading from 25879: heap size 428 MB, throughput 0.99825
Reading from 25878: heap size 519 MB, throughput 0.993519
Reading from 25879: heap size 429 MB, throughput 0.998428
Reading from 25878: heap size 516 MB, throughput 0.99225
Reading from 25879: heap size 438 MB, throughput 0.99772
Equal recommendation: 720 MB each
Reading from 25879: heap size 439 MB, throughput 0.997632
Reading from 25878: heap size 518 MB, throughput 0.992887
Reading from 25879: heap size 448 MB, throughput 0.998198
Reading from 25878: heap size 519 MB, throughput 0.989165
Reading from 25879: heap size 448 MB, throughput 0.996352
Reading from 25878: heap size 519 MB, throughput 0.989064
Reading from 25878: heap size 520 MB, throughput 0.914033
Reading from 25878: heap size 522 MB, throughput 0.987244
Reading from 25879: heap size 455 MB, throughput 0.998279
Equal recommendation: 720 MB each
Reading from 25878: heap size 528 MB, throughput 0.995292
Reading from 25879: heap size 456 MB, throughput 0.998016
Reading from 25879: heap size 464 MB, throughput 0.998667
Reading from 25878: heap size 529 MB, throughput 0.993755
Reading from 25879: heap size 464 MB, throughput 0.998541
Reading from 25878: heap size 529 MB, throughput 0.993406
Equal recommendation: 720 MB each
Reading from 25879: heap size 471 MB, throughput 0.998172
Reading from 25878: heap size 530 MB, throughput 0.98991
Reading from 25879: heap size 471 MB, throughput 0.995367
Reading from 25878: heap size 529 MB, throughput 0.992485
Reading from 25879: heap size 478 MB, throughput 0.998578
Reading from 25878: heap size 530 MB, throughput 0.995069
Reading from 25879: heap size 479 MB, throughput 0.998672
Equal recommendation: 720 MB each
Reading from 25878: heap size 529 MB, throughput 0.977892
Reading from 25878: heap size 531 MB, throughput 0.915533
Reading from 25879: heap size 485 MB, throughput 0.998041
Reading from 25878: heap size 533 MB, throughput 0.994504
Reading from 25879: heap size 486 MB, throughput 0.998501
Reading from 25878: heap size 535 MB, throughput 0.994669
Reading from 25879: heap size 492 MB, throughput 0.998283
Equal recommendation: 720 MB each
Reading from 25878: heap size 537 MB, throughput 0.994492
Reading from 25879: heap size 493 MB, throughput 0.995954
Reading from 25879: heap size 496 MB, throughput 0.998723
Reading from 25878: heap size 538 MB, throughput 0.993291
Reading from 25879: heap size 497 MB, throughput 0.998731
Reading from 25878: heap size 537 MB, throughput 0.993254
Equal recommendation: 720 MB each
Reading from 25879: heap size 496 MB, throughput 0.998648
Reading from 25878: heap size 538 MB, throughput 0.990609
Reading from 25878: heap size 539 MB, throughput 0.991304
Reading from 25878: heap size 540 MB, throughput 0.916891
Reading from 25879: heap size 497 MB, throughput 0.998739
Reading from 25878: heap size 543 MB, throughput 0.992099
Client 25879 died
Clients: 1
Reading from 25878: heap size 545 MB, throughput 0.994648
Recommendation: one client; give it all the memory
Reading from 25878: heap size 547 MB, throughput 0.99426
Reading from 25878: heap size 548 MB, throughput 0.992819
Reading from 25878: heap size 546 MB, throughput 0.993581
Recommendation: one client; give it all the memory
Reading from 25878: heap size 548 MB, throughput 0.991474
Reading from 25878: heap size 550 MB, throughput 0.991077
Reading from 25878: heap size 550 MB, throughput 0.924436
Reading from 25878: heap size 553 MB, throughput 0.991467
Reading from 25878: heap size 555 MB, throughput 0.994756
Recommendation: one client; give it all the memory
Reading from 25878: heap size 557 MB, throughput 0.994281
Reading from 25878: heap size 558 MB, throughput 0.994208
Reading from 25878: heap size 556 MB, throughput 0.992688
Recommendation: one client; give it all the memory
Reading from 25878: heap size 558 MB, throughput 0.991079
Reading from 25878: heap size 558 MB, throughput 0.987591
Reading from 25878: heap size 560 MB, throughput 0.929338
Client 25878 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
