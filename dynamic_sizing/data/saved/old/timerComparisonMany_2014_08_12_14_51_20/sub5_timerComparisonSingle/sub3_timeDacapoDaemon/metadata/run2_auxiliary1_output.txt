economemd
    total memory: 200 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_08_12_14_51_20/sub5_timerComparisonSingle/sub3_timeDacapoDaemon/daemon_run2_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 27654: heap size 9 MB, throughput 0.98967
Clients: 1
Reading from 27654: heap size 9 MB, throughput 0.979486
Reading from 27656: heap size 9 MB, throughput 0.990888
Clients: 2
Reading from 27654: heap size 9 MB, throughput 0.972731
Reading from 27654: heap size 9 MB, throughput 0.964116
Reading from 27656: heap size 9 MB, throughput 0.990059
Reading from 27654: heap size 11 MB, throughput 0.923451
Reading from 27656: heap size 9 MB, throughput 0.991544
Reading from 27656: heap size 9 MB, throughput 0.987414
Reading from 27656: heap size 9 MB, throughput 0.983579
Reading from 27656: heap size 9 MB, throughput 0.975567
Reading from 27656: heap size 12 MB, throughput 0.978316
Reading from 27654: heap size 11 MB, throughput 0.966612
Reading from 27656: heap size 12 MB, throughput 0.982626
Reading from 27654: heap size 16 MB, throughput 0.925286
Reading from 27656: heap size 15 MB, throughput 0.996388
Reading from 27656: heap size 16 MB, throughput 0.99231
Reading from 27654: heap size 23 MB, throughput 0.979907
Reading from 27656: heap size 19 MB, throughput 0.98888
Reading from 27656: heap size 20 MB, throughput 0.979773
Reading from 27656: heap size 27 MB, throughput 0.977631
Reading from 27656: heap size 29 MB, throughput 0.84033
Reading from 27656: heap size 36 MB, throughput 0.796159
Reading from 27656: heap size 38 MB, throughput 0.798362
Reading from 27656: heap size 50 MB, throughput 0.900333
Reading from 27656: heap size 50 MB, throughput 0.950366
Reading from 27654: heap size 23 MB, throughput 0.991805
Reading from 27656: heap size 61 MB, throughput 0.941489
Reading from 27656: heap size 61 MB, throughput 0.949345
Reading from 27656: heap size 73 MB, throughput 0.956513
Reading from 27656: heap size 74 MB, throughput 0.96445
Reading from 27654: heap size 27 MB, throughput 0.995173
Reading from 27656: heap size 90 MB, throughput 0.952733
Reading from 27656: heap size 97 MB, throughput 0.92463
Reading from 27654: heap size 29 MB, throughput 0.997064
Reading from 27656: heap size 112 MB, throughput 0.831516
Reading from 27656: heap size 127 MB, throughput 0.820126
Reading from 27654: heap size 29 MB, throughput 0.998058
Reading from 27656: heap size 136 MB, throughput 0.910566
Reading from 27656: heap size 137 MB, throughput 0.909678
Reading from 27656: heap size 145 MB, throughput 0.909587
Reading from 27656: heap size 145 MB, throughput 0.906849
Reading from 27656: heap size 161 MB, throughput 0.927156
Reading from 27656: heap size 167 MB, throughput 0.942196
Reading from 27656: heap size 185 MB, throughput 0.954613
Reading from 27656: heap size 187 MB, throughput 0.975979
Reading from 27656: heap size 201 MB, throughput 0.989457
Reading from 27656: heap size 203 MB, throughput 0.98936
Reading from 27654: heap size 29 MB, throughput 0.994649
Reading from 27656: heap size 213 MB, throughput 0.997361
Reading from 27656: heap size 215 MB, throughput 0.996476
Reading from 27654: heap size 29 MB, throughput 0.996723
Reading from 27656: heap size 222 MB, throughput 0.996915
Numeric result:
Recommendation: 2 clients, utility 0.999767:
    h1: 194.818 MB (U(h) = 0.870632*h^0.0375921)
    h2: 5.18242 MB (U(h) = 0.940325*h^0.001)
Recommendation: 2 clients, utility 0.999767:
    h1: 194.818 MB (U(h) = 0.870632*h^0.0375921)
    h2: 5.18241 MB (U(h) = 0.940325*h^0.001)
Reading from 27654: heap size 30 MB, throughput 0.996878
Reading from 27656: heap size 224 MB, throughput 0.995283
Reading from 27656: heap size 233 MB, throughput 0.99596
Reading from 27654: heap size 30 MB, throughput 0.997591
Reading from 27656: heap size 233 MB, throughput 0.995698
Reading from 27654: heap size 31 MB, throughput 0.998085
Reading from 27654: heap size 31 MB, throughput 0.997951
Reading from 27654: heap size 32 MB, throughput 0.998153
Reading from 27654: heap size 32 MB, throughput 0.995477
Reading from 27654: heap size 33 MB, throughput 0.996785
Reading from 27654: heap size 33 MB, throughput 0.997183
Reading from 27654: heap size 34 MB, throughput 0.996701
Numeric result:
Recommendation: 2 clients, utility 1.00089:
    h1: 185.197 MB (U(h) = 0.862198*h^0.0414846)
    h2: 14.8033 MB (U(h) = 0.926461*h^0.0033161)
Recommendation: 2 clients, utility 1.00089:
    h1: 185.196 MB (U(h) = 0.862198*h^0.0414846)
    h2: 14.8038 MB (U(h) = 0.926461*h^0.0033161)
Reading from 27656: heap size 5359 MB, throughput 0.999592
Reading from 27654: heap size 34 MB, throughput 0.993844
Reading from 27654: heap size 34 MB, throughput 0.99501
Reading from 27654: heap size 36 MB, throughput 0.995547
Reading from 27654: heap size 37 MB, throughput 0.995909
Reading from 27654: heap size 37 MB, throughput 0.996009
Reading from 27654: heap size 38 MB, throughput 0.996289
Reading from 27654: heap size 38 MB, throughput 0.992219
Reading from 27654: heap size 39 MB, throughput 0.99183
Reading from 27656: heap size 5359 MB, throughput 0.999722
Numeric result:
Recommendation: 2 clients, utility 0.987323:
    h1: 167.435 MB (U(h) = 0.871828*h^0.0374241)
    h2: 32.5648 MB (U(h) = 0.911575*h^0.00727869)
Recommendation: 2 clients, utility 0.987323:
    h1: 167.435 MB (U(h) = 0.871828*h^0.0374241)
    h2: 32.5648 MB (U(h) = 0.911575*h^0.00727869)
Reading from 27654: heap size 39 MB, throughput 0.994698
Reading from 27654: heap size 38 MB, throughput 0.995759
Reading from 27654: heap size 39 MB, throughput 0.99686
Reading from 27654: heap size 38 MB, throughput 0.995209
Reading from 27654: heap size 41 MB, throughput 0.998201
Reading from 27654: heap size 40 MB, throughput 0.998836
Reading from 27654: heap size 41 MB, throughput 0.997784
Reading from 27654: heap size 42 MB, throughput 0.997794
Reading from 27656: heap size 5359 MB, throughput 0.999776
Numeric result:
Recommendation: 2 clients, utility 0.98551:
    h1: 166.806 MB (U(h) = 0.873903*h^0.0366011)
    h2: 33.1944 MB (U(h) = 0.911556*h^0.00728365)
Recommendation: 2 clients, utility 0.98551:
    h1: 166.806 MB (U(h) = 0.873903*h^0.0366011)
    h2: 33.1945 MB (U(h) = 0.911556*h^0.00728365)
Reading from 27654: heap size 42 MB, throughput 0.997881
Reading from 27654: heap size 42 MB, throughput 0.9977
Reading from 27654: heap size 42 MB, throughput 0.994448
Reading from 27654: heap size 43 MB, throughput 0.995887
Reading from 27654: heap size 43 MB, throughput 0.996869
Reading from 27656: heap size 5359 MB, throughput 0.999743
Reading from 27654: heap size 42 MB, throughput 0.997311
Numeric result:
Recommendation: 2 clients, utility 0.98366:
    h1: 166.152 MB (U(h) = 0.876034*h^0.0357578)
    h2: 33.8483 MB (U(h) = 0.911552*h^0.00728475)
Recommendation: 2 clients, utility 0.98366:
    h1: 166.151 MB (U(h) = 0.876034*h^0.0357578)
    h2: 33.8491 MB (U(h) = 0.911552*h^0.00728475)
Reading from 27654: heap size 43 MB, throughput 0.995443
Reading from 27654: heap size 43 MB, throughput 0.996636
Reading from 27654: heap size 43 MB, throughput 0.994794
Reading from 27656: heap size 5359 MB, throughput 0.99969
Reading from 27654: heap size 43 MB, throughput 0.994289
Reading from 27654: heap size 44 MB, throughput 0.995388
Reading from 27654: heap size 45 MB, throughput 0.993416
Reading from 27656: heap size 5360 MB, throughput 0.999706
Numeric result:
Recommendation: 2 clients, utility 0.978742:
    h1: 157.634 MB (U(h) = 0.881042*h^0.033811)
    h2: 42.3655 MB (U(h) = 0.904861*h^0.00908695)
Recommendation: 2 clients, utility 0.978742:
    h1: 157.635 MB (U(h) = 0.881042*h^0.033811)
    h2: 42.3654 MB (U(h) = 0.904861*h^0.00908695)
Reading from 27654: heap size 45 MB, throughput 0.992313
Reading from 27654: heap size 44 MB, throughput 0.995307
Reading from 27654: heap size 45 MB, throughput 0.996886
Reading from 27656: heap size 5361 MB, throughput 0.999716
Reading from 27654: heap size 46 MB, throughput 0.997608
Reading from 27654: heap size 46 MB, throughput 0.997968
Reading from 27656: heap size 5358 MB, throughput 0.999763
Reading from 27654: heap size 46 MB, throughput 0.998072
Numeric result:
Recommendation: 2 clients, utility 0.977587:
    h1: 151.24 MB (U(h) = 0.881963*h^0.0334603)
    h2: 48.7596 MB (U(h) = 0.898592*h^0.0107878)
Recommendation: 2 clients, utility 0.977587:
    h1: 151.239 MB (U(h) = 0.881963*h^0.0334603)
    h2: 48.7606 MB (U(h) = 0.898592*h^0.0107878)
Reading from 27654: heap size 46 MB, throughput 0.998234
Reading from 27654: heap size 45 MB, throughput 0.997865
Reading from 27656: heap size 5360 MB, throughput 0.999752
Reading from 27654: heap size 46 MB, throughput 0.997857
Reading from 27654: heap size 47 MB, throughput 0.997817
Reading from 27656: heap size 5360 MB, throughput 0.999738
Reading from 27654: heap size 47 MB, throughput 0.996371
Numeric result:
Recommendation: 2 clients, utility 0.976688:
    h1: 150.725 MB (U(h) = 0.883175*h^0.0330032)
    h2: 49.2748 MB (U(h) = 0.898588*h^0.0107889)
Recommendation: 2 clients, utility 0.976688:
    h1: 150.727 MB (U(h) = 0.883175*h^0.0330032)
    h2: 49.2735 MB (U(h) = 0.898588*h^0.0107889)
Reading from 27654: heap size 47 MB, throughput 0.997396
Reading from 27656: heap size 5361 MB, throughput 0.999733
Reading from 27654: heap size 47 MB, throughput 0.997796
Reading from 27654: heap size 47 MB, throughput 0.997089
Reading from 27656: heap size 5361 MB, throughput 0.99976
Reading from 27654: heap size 47 MB, throughput 0.997733
Reading from 27654: heap size 47 MB, throughput 0.998295
Numeric result:
Recommendation: 2 clients, utility 0.976843:
    h1: 150.81 MB (U(h) = 0.882968*h^0.0330815)
    h2: 49.1897 MB (U(h) = 0.898584*h^0.01079)
Recommendation: 2 clients, utility 0.976843:
    h1: 150.811 MB (U(h) = 0.882968*h^0.0330815)
    h2: 49.189 MB (U(h) = 0.898584*h^0.01079)
Reading from 27654: heap size 47 MB, throughput 0.998234
Reading from 27656: heap size 5362 MB, throughput 0.999755
Reading from 27654: heap size 48 MB, throughput 0.99818
Reading from 27654: heap size 48 MB, throughput 0.998286
Reading from 27656: heap size 5360 MB, throughput 0.999721
Reading from 27654: heap size 48 MB, throughput 0.998593
Reading from 27654: heap size 48 MB, throughput 0.998491
Reading from 27656: heap size 5361 MB, throughput 0.99878
Numeric result:
Recommendation: 2 clients, utility 0.975862:
    h1: 148.737 MB (U(h) = 0.884205*h^0.0326169)
    h2: 51.263 MB (U(h) = 0.896926*h^0.0112416)
Recommendation: 2 clients, utility 0.975862:
    h1: 148.737 MB (U(h) = 0.884205*h^0.0326169)
    h2: 51.2631 MB (U(h) = 0.896926*h^0.0112416)
Reading from 27654: heap size 48 MB, throughput 0.998318
Reading from 27654: heap size 48 MB, throughput 0.998412
Reading from 27656: heap size 5361 MB, throughput 0.999182
Reading from 27654: heap size 49 MB, throughput 0.998179
Reading from 27654: heap size 49 MB, throughput 0.998291
Reading from 27656: heap size 5362 MB, throughput 0.999458
Numeric result:
Recommendation: 2 clients, utility 0.974878:
    h1: 148.152 MB (U(h) = 0.885582*h^0.0321039)
    h2: 51.8484 MB (U(h) = 0.896949*h^0.0112354)
Recommendation: 2 clients, utility 0.974878:
    h1: 148.152 MB (U(h) = 0.885582*h^0.0321039)
    h2: 51.8484 MB (U(h) = 0.896949*h^0.0112354)
Reading from 27654: heap size 49 MB, throughput 0.998662
Reading from 27654: heap size 49 MB, throughput 0.998638
Reading from 27656: heap size 5361 MB, throughput 0.999582
Reading from 27654: heap size 50 MB, throughput 0.998651
Reading from 27654: heap size 50 MB, throughput 0.998644
Reading from 27656: heap size 5362 MB, throughput 0.999678
Reading from 27654: heap size 50 MB, throughput 0.998488
Numeric result:
Recommendation: 2 clients, utility 0.974014:
    h1: 147.575 MB (U(h) = 0.886818*h^0.0316474)
    h2: 52.4251 MB (U(h) = 0.896923*h^0.0112426)
Recommendation: 2 clients, utility 0.974014:
    h1: 147.575 MB (U(h) = 0.886818*h^0.0316474)
    h2: 52.4252 MB (U(h) = 0.896923*h^0.0112426)
Reading from 27656: heap size 5362 MB, throughput 0.999691
Reading from 27654: heap size 50 MB, throughput 0.998635
Reading from 27654: heap size 51 MB, throughput 0.997209
Reading from 27654: heap size 51 MB, throughput 0.998159
Reading from 27656: heap size 5363 MB, throughput 0.999514
Reading from 27654: heap size 51 MB, throughput 0.998422
Numeric result:
Recommendation: 2 clients, utility 0.97299:
    h1: 145.744 MB (U(h) = 0.888262*h^0.0311181)
    h2: 54.2563 MB (U(h) = 0.895671*h^0.0115841)
Recommendation: 2 clients, utility 0.97299:
    h1: 145.745 MB (U(h) = 0.888262*h^0.0311181)
    h2: 54.2553 MB (U(h) = 0.895671*h^0.0115841)
Reading from 27656: heap size 5363 MB, throughput 0.999549
Reading from 27654: heap size 51 MB, throughput 0.99872
Reading from 27654: heap size 52 MB, throughput 0.99886
Reading from 27656: heap size 5363 MB, throughput 0.999646
Reading from 27654: heap size 52 MB, throughput 0.998675
Reading from 27654: heap size 52 MB, throughput 0.998739
Reading from 27656: heap size 5363 MB, throughput 0.999657
Numeric result:
Recommendation: 2 clients, utility 0.972173:
    h1: 145.164 MB (U(h) = 0.889486*h^0.0306729)
    h2: 54.8357 MB (U(h) = 0.89566*h^0.0115873)
Recommendation: 2 clients, utility 0.972173:
    h1: 145.162 MB (U(h) = 0.889486*h^0.0306729)
    h2: 54.8378 MB (U(h) = 0.89566*h^0.0115873)
Reading from 27654: heap size 52 MB, throughput 0.996691
Reading from 27654: heap size 53 MB, throughput 0.997971
Reading from 27656: heap size 5362 MB, throughput 0.999711
Reading from 27654: heap size 53 MB, throughput 0.998153
Reading from 27654: heap size 53 MB, throughput 0.998459
Reading from 27656: heap size 5362 MB, throughput 0.999716
Numeric result:
Recommendation: 2 clients, utility 0.971096:
    h1: 144.374 MB (U(h) = 0.891129*h^0.0300791)
    h2: 55.6255 MB (U(h) = 0.895656*h^0.0115883)
Recommendation: 2 clients, utility 0.971096:
    h1: 144.377 MB (U(h) = 0.891129*h^0.0300791)
    h2: 55.6228 MB (U(h) = 0.895656*h^0.0115883)
Reading from 27654: heap size 53 MB, throughput 0.99878
Reading from 27656: heap size 5363 MB, throughput 0.999673
Reading from 27654: heap size 53 MB, throughput 0.998783
Reading from 27654: heap size 53 MB, throughput 0.998741
Reading from 27656: heap size 5363 MB, throughput 0.999647
Reading from 27654: heap size 54 MB, throughput 0.998593
Numeric result:
Recommendation: 2 clients, utility 0.9703:
    h1: 143.775 MB (U(h) = 0.892375*h^0.029633)
    h2: 56.2245 MB (U(h) = 0.895654*h^0.011589)
Recommendation: 2 clients, utility 0.9703:
    h1: 143.773 MB (U(h) = 0.892375*h^0.029633)
    h2: 56.2273 MB (U(h) = 0.895654*h^0.011589)
Reading from 27656: heap size 5363 MB, throughput 0.999687
Reading from 27654: heap size 54 MB, throughput 0.998687
Reading from 27654: heap size 54 MB, throughput 0.997521
Reading from 27656: heap size 5363 MB, throughput 0.999746
Reading from 27654: heap size 54 MB, throughput 0.998168
Reading from 27654: heap size 55 MB, throughput 0.998658
Reading from 27656: heap size 5363 MB, throughput 0.999746
Numeric result:
Recommendation: 2 clients, utility 0.969403:
    h1: 143.06 MB (U(h) = 0.893802*h^0.0291251)
    h2: 56.9398 MB (U(h) = 0.895645*h^0.0115913)
Recommendation: 2 clients, utility 0.969403:
    h1: 143.063 MB (U(h) = 0.893802*h^0.0291251)
    h2: 56.9368 MB (U(h) = 0.895645*h^0.0115913)
Reading from 27654: heap size 55 MB, throughput 0.998705
Reading from 27656: heap size 5363 MB, throughput 0.99971
Reading from 27654: heap size 55 MB, throughput 0.998753
Reading from 27654: heap size 55 MB, throughput 0.998816
Reading from 27656: heap size 5363 MB, throughput 0.999683
Reading from 27654: heap size 56 MB, throughput 0.998185
Numeric result:
Recommendation: 2 clients, utility 0.96854:
    h1: 142.363 MB (U(h) = 0.895204*h^0.0286295)
    h2: 57.6369 MB (U(h) = 0.895648*h^0.0115905)
Recommendation: 2 clients, utility 0.96854:
    h1: 142.364 MB (U(h) = 0.895204*h^0.0286295)
    h2: 57.6357 MB (U(h) = 0.895648*h^0.0115905)
Reading from 27654: heap size 56 MB, throughput 0.99851
Reading from 27656: heap size 5363 MB, throughput 0.999687
Reading from 27654: heap size 56 MB, throughput 0.998726
Reading from 27656: heap size 5363 MB, throughput 0.999715
Reading from 27654: heap size 56 MB, throughput 0.998781
Numeric result:
Recommendation: 2 clients, utility 0.968601:
    h1: 142.414 MB (U(h) = 0.895104*h^0.0286647)
    h2: 57.5856 MB (U(h) = 0.895648*h^0.0115907)
Recommendation: 2 clients, utility 0.968601:
    h1: 142.414 MB (U(h) = 0.895104*h^0.0286647)
    h2: 57.5857 MB (U(h) = 0.895648*h^0.0115907)
Reading from 27654: heap size 57 MB, throughput 0.99877
Reading from 27656: heap size 5363 MB, throughput 0.99971
Reading from 27654: heap size 57 MB, throughput 0.99885
Reading from 27656: heap size 5363 MB, throughput 0.999752
Reading from 27654: heap size 57 MB, throughput 0.997824
Reading from 27654: heap size 57 MB, throughput 0.998291
Reading from 27656: heap size 5363 MB, throughput 0.999703
Numeric result:
Recommendation: 2 clients, utility 0.967758:
    h1: 141.704 MB (U(h) = 0.8965*h^0.0281745)
    h2: 58.2961 MB (U(h) = 0.895646*h^0.011591)
Recommendation: 2 clients, utility 0.967758:
    h1: 141.703 MB (U(h) = 0.8965*h^0.0281745)
    h2: 58.2967 MB (U(h) = 0.895646*h^0.011591)
Reading from 27654: heap size 58 MB, throughput 0.998757
Reading from 27656: heap size 5363 MB, throughput 0.999749
Reading from 27654: heap size 58 MB, throughput 0.998826
Client 27654 died
Clients: 1
Reading from 27656: heap size 5363 MB, throughput 0.999672
Recommendation: one client; give it all the memory
Reading from 27656: heap size 5364 MB, throughput 0.999636
Reading from 27656: heap size 5364 MB, throughput 0.999555
Recommendation: one client; give it all the memory
Reading from 27656: heap size 5364 MB, throughput 0.999585
Reading from 27656: heap size 5364 MB, throughput 0.999606
Reading from 27656: heap size 5364 MB, throughput 0.999576
Recommendation: one client; give it all the memory
Reading from 27656: heap size 5364 MB, throughput 0.999616
Reading from 27656: heap size 5364 MB, throughput 0.999543
Recommendation: one client; give it all the memory
Reading from 27656: heap size 5364 MB, throughput 0.999655
Reading from 27656: heap size 5364 MB, throughput 0.999672
Reading from 27656: heap size 5364 MB, throughput 0.999623
Recommendation: one client; give it all the memory
Reading from 27656: heap size 5364 MB, throughput 0.999588
Reading from 27656: heap size 5364 MB, throughput 0.999664
Recommendation: one client; give it all the memory
Reading from 27656: heap size 5364 MB, throughput 0.999635
Reading from 27656: heap size 5364 MB, throughput 0.999676
Reading from 27656: heap size 5364 MB, throughput 0.999613
Recommendation: one client; give it all the memory
Reading from 27656: heap size 5364 MB, throughput 0.999614
Reading from 27656: heap size 5364 MB, throughput 0.999656
Recommendation: one client; give it all the memory
Reading from 27656: heap size 5364 MB, throughput 0.999627
Reading from 27656: heap size 5364 MB, throughput 0.999596
Reading from 27656: heap size 5364 MB, throughput 0.999572
Recommendation: one client; give it all the memory
Reading from 27656: heap size 5364 MB, throughput 0.999659
Reading from 27656: heap size 5364 MB, throughput 0.999639
Client 27656 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
