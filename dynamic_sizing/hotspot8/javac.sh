#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ARCH="$(uname -m)"

if [ "${ARCH}" = 'x86_64' ]; then
    BUILD_BASE="${DIR}/openjdk8/jdk8/build/linux-x86_64-normal-server-release/jdk"
    LIB_PATH='amd64'
elif [ "${ARCH}" = 'i686' ]; then
    BUILD_BASE="${DIR}/openjdk8/jdk8/build/linux-x86-normal-server-release/jdk"
    LIB_PATH='i386'
else
    echo "Don't know how to run on this architecture!"
    exit 1
fi

export LD_LIBRARY_PATH="${BUILD_BASE}/lib/${LIB_PATH}/server:${LD_LIBRARY_PATH}"

"${BUILD_BASE}/bin/javac" "${@}"
