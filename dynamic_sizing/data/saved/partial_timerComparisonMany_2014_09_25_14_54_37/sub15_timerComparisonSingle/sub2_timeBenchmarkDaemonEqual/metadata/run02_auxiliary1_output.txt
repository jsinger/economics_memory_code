economemd
    total memory: 3669 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub15_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 26057: heap size 9 MB, throughput 0.982578
Clients: 1
Client 26057 has a minimum heap size of 12 MB
Reading from 26055: heap size 9 MB, throughput 0.992193
Clients: 2
Client 26055 has a minimum heap size of 1211 MB
Reading from 26057: heap size 9 MB, throughput 0.987228
Reading from 26055: heap size 9 MB, throughput 0.982386
Reading from 26055: heap size 9 MB, throughput 0.971358
Reading from 26055: heap size 9 MB, throughput 0.950024
Reading from 26057: heap size 11 MB, throughput 0.981373
Reading from 26057: heap size 11 MB, throughput 0.961029
Reading from 26055: heap size 11 MB, throughput 0.981475
Reading from 26055: heap size 11 MB, throughput 0.965524
Reading from 26055: heap size 17 MB, throughput 0.969466
Reading from 26057: heap size 15 MB, throughput 0.992461
Reading from 26055: heap size 17 MB, throughput 0.746249
Reading from 26057: heap size 15 MB, throughput 0.958605
Reading from 26055: heap size 30 MB, throughput 0.770916
Reading from 26055: heap size 31 MB, throughput 0.911234
Reading from 26057: heap size 24 MB, throughput 0.892711
Reading from 26055: heap size 36 MB, throughput 0.480963
Reading from 26055: heap size 50 MB, throughput 0.876182
Reading from 26055: heap size 53 MB, throughput 0.340456
Reading from 26057: heap size 30 MB, throughput 0.989047
Reading from 26055: heap size 70 MB, throughput 0.855005
Reading from 26055: heap size 77 MB, throughput 0.782705
Reading from 26057: heap size 35 MB, throughput 0.978629
Reading from 26055: heap size 78 MB, throughput 0.272553
Reading from 26057: heap size 41 MB, throughput 0.984436
Reading from 26055: heap size 111 MB, throughput 0.851484
Reading from 26057: heap size 42 MB, throughput 0.984735
Reading from 26055: heap size 112 MB, throughput 0.331914
Reading from 26057: heap size 46 MB, throughput 0.981032
Reading from 26055: heap size 146 MB, throughput 0.828533
Reading from 26055: heap size 147 MB, throughput 0.771609
Reading from 26057: heap size 50 MB, throughput 0.965808
Reading from 26055: heap size 149 MB, throughput 0.69477
Reading from 26055: heap size 153 MB, throughput 0.777841
Reading from 26057: heap size 51 MB, throughput 0.984168
Reading from 26057: heap size 58 MB, throughput 0.981331
Reading from 26057: heap size 58 MB, throughput 0.933104
Reading from 26055: heap size 157 MB, throughput 0.165901
Reading from 26057: heap size 67 MB, throughput 0.721476
Reading from 26055: heap size 202 MB, throughput 0.598742
Reading from 26057: heap size 71 MB, throughput 0.991493
Reading from 26055: heap size 207 MB, throughput 0.762882
Reading from 26055: heap size 211 MB, throughput 0.682685
Reading from 26057: heap size 80 MB, throughput 0.992023
Reading from 26055: heap size 219 MB, throughput 0.707588
Reading from 26055: heap size 224 MB, throughput 0.738862
Reading from 26057: heap size 80 MB, throughput 0.997329
Reading from 26055: heap size 228 MB, throughput 0.162782
Reading from 26055: heap size 284 MB, throughput 0.589761
Reading from 26055: heap size 289 MB, throughput 0.744244
Reading from 26055: heap size 295 MB, throughput 0.659148
Reading from 26055: heap size 299 MB, throughput 0.544733
Reading from 26055: heap size 310 MB, throughput 0.593249
Reading from 26057: heap size 88 MB, throughput 0.996782
Reading from 26055: heap size 317 MB, throughput 0.587432
Reading from 26057: heap size 90 MB, throughput 0.997111
Reading from 26055: heap size 328 MB, throughput 0.113911
Reading from 26055: heap size 385 MB, throughput 0.504841
Reading from 26057: heap size 97 MB, throughput 0.99672
Reading from 26055: heap size 390 MB, throughput 0.117863
Reading from 26055: heap size 436 MB, throughput 0.521749
Reading from 26055: heap size 441 MB, throughput 0.661685
Reading from 26055: heap size 430 MB, throughput 0.667853
Reading from 26057: heap size 98 MB, throughput 0.996524
Reading from 26055: heap size 436 MB, throughput 0.649311
Reading from 26055: heap size 438 MB, throughput 0.637877
Reading from 26055: heap size 440 MB, throughput 0.550972
Reading from 26055: heap size 443 MB, throughput 0.548221
Reading from 26057: heap size 102 MB, throughput 0.99724
Reading from 26055: heap size 452 MB, throughput 0.508981
Reading from 26055: heap size 459 MB, throughput 0.481171
Reading from 26055: heap size 469 MB, throughput 0.506516
Reading from 26055: heap size 474 MB, throughput 0.488554
Reading from 26057: heap size 104 MB, throughput 0.995753
Equal recommendation: 1834 MB each
Reading from 26057: heap size 108 MB, throughput 0.995688
Reading from 26055: heap size 483 MB, throughput 0.0733698
Reading from 26055: heap size 548 MB, throughput 0.484977
Reading from 26055: heap size 552 MB, throughput 0.585049
Reading from 26057: heap size 108 MB, throughput 0.996625
Reading from 26057: heap size 111 MB, throughput 0.996877
Reading from 26055: heap size 541 MB, throughput 0.100895
Reading from 26055: heap size 612 MB, throughput 0.463708
Reading from 26055: heap size 599 MB, throughput 0.598651
Reading from 26055: heap size 606 MB, throughput 0.585364
Reading from 26057: heap size 112 MB, throughput 0.993239
Reading from 26055: heap size 607 MB, throughput 0.642494
Reading from 26055: heap size 610 MB, throughput 0.552961
Reading from 26055: heap size 613 MB, throughput 0.489027
Reading from 26055: heap size 623 MB, throughput 0.499945
Reading from 26057: heap size 116 MB, throughput 0.997396
Reading from 26055: heap size 631 MB, throughput 0.583801
Reading from 26055: heap size 640 MB, throughput 0.71469
Reading from 26057: heap size 116 MB, throughput 0.996352
Reading from 26057: heap size 119 MB, throughput 0.995692
Reading from 26055: heap size 646 MB, throughput 0.315277
Reading from 26057: heap size 119 MB, throughput 0.997167
Reading from 26055: heap size 721 MB, throughput 0.795619
Reading from 26057: heap size 122 MB, throughput 0.997443
Reading from 26055: heap size 730 MB, throughput 0.877129
Reading from 26055: heap size 734 MB, throughput 0.550744
Reading from 26055: heap size 744 MB, throughput 0.489448
Reading from 26055: heap size 746 MB, throughput 0.64112
Reading from 26057: heap size 122 MB, throughput 0.997745
Reading from 26055: heap size 748 MB, throughput 0.37048
Reading from 26057: heap size 125 MB, throughput 0.997505
Reading from 26057: heap size 125 MB, throughput 0.997781
Reading from 26055: heap size 749 MB, throughput 0.0323111
Reading from 26055: heap size 809 MB, throughput 0.354856
Reading from 26055: heap size 815 MB, throughput 0.63959
Reading from 26055: heap size 819 MB, throughput 0.889103
Reading from 26057: heap size 127 MB, throughput 0.998492
Reading from 26055: heap size 820 MB, throughput 0.636426
Reading from 26055: heap size 820 MB, throughput 0.671512
Equal recommendation: 1834 MB each
Reading from 26057: heap size 128 MB, throughput 0.997563
Reading from 26057: heap size 130 MB, throughput 0.997744
Reading from 26055: heap size 822 MB, throughput 0.0559608
Reading from 26055: heap size 903 MB, throughput 0.733008
Reading from 26055: heap size 905 MB, throughput 0.894804
Reading from 26055: heap size 907 MB, throughput 0.746323
Reading from 26057: heap size 130 MB, throughput 0.99841
Reading from 26055: heap size 909 MB, throughput 0.923008
Reading from 26055: heap size 915 MB, throughput 0.910793
Reading from 26057: heap size 132 MB, throughput 0.998163
Reading from 26055: heap size 916 MB, throughput 0.921717
Reading from 26055: heap size 899 MB, throughput 0.800475
Reading from 26055: heap size 787 MB, throughput 0.792826
Reading from 26055: heap size 889 MB, throughput 0.804134
Reading from 26055: heap size 793 MB, throughput 0.803927
Reading from 26055: heap size 881 MB, throughput 0.71255
Reading from 26057: heap size 132 MB, throughput 0.995782
Reading from 26055: heap size 888 MB, throughput 0.712646
Reading from 26055: heap size 881 MB, throughput 0.775221
Reading from 26057: heap size 134 MB, throughput 0.993558
Reading from 26055: heap size 885 MB, throughput 0.760977
Reading from 26055: heap size 880 MB, throughput 0.708874
Reading from 26057: heap size 134 MB, throughput 0.992388
Reading from 26057: heap size 136 MB, throughput 0.995409
Reading from 26057: heap size 137 MB, throughput 0.994405
Reading from 26055: heap size 884 MB, throughput 0.516591
Reading from 26057: heap size 142 MB, throughput 0.997452
Reading from 26057: heap size 142 MB, throughput 0.995755
Reading from 26055: heap size 968 MB, throughput 0.91267
Reading from 26055: heap size 970 MB, throughput 0.83102
Reading from 26055: heap size 963 MB, throughput 0.848578
Reading from 26055: heap size 968 MB, throughput 0.858639
Reading from 26055: heap size 963 MB, throughput 0.863902
Reading from 26057: heap size 145 MB, throughput 0.997817
Reading from 26055: heap size 967 MB, throughput 0.859545
Reading from 26055: heap size 963 MB, throughput 0.863509
Reading from 26055: heap size 967 MB, throughput 0.870667
Reading from 26057: heap size 145 MB, throughput 0.99661
Reading from 26055: heap size 966 MB, throughput 0.911157
Equal recommendation: 1834 MB each
Reading from 26055: heap size 969 MB, throughput 0.938088
Reading from 26055: heap size 977 MB, throughput 0.842199
Reading from 26055: heap size 978 MB, throughput 0.748304
Reading from 26057: heap size 149 MB, throughput 0.997376
Reading from 26055: heap size 966 MB, throughput 0.72828
Reading from 26055: heap size 985 MB, throughput 0.698227
Reading from 26055: heap size 997 MB, throughput 0.700242
Reading from 26055: heap size 1004 MB, throughput 0.707484
Reading from 26055: heap size 1018 MB, throughput 0.731144
Reading from 26057: heap size 149 MB, throughput 0.996553
Reading from 26055: heap size 1019 MB, throughput 0.668617
Reading from 26055: heap size 1034 MB, throughput 0.725445
Reading from 26055: heap size 1034 MB, throughput 0.699012
Reading from 26057: heap size 152 MB, throughput 0.996778
Reading from 26055: heap size 1050 MB, throughput 0.69063
Reading from 26057: heap size 152 MB, throughput 0.994133
Reading from 26055: heap size 1052 MB, throughput 0.954499
Reading from 26057: heap size 155 MB, throughput 0.997496
Reading from 26057: heap size 155 MB, throughput 0.995972
Reading from 26055: heap size 1068 MB, throughput 0.95031
Reading from 26057: heap size 158 MB, throughput 0.997251
Reading from 26057: heap size 158 MB, throughput 0.994441
Reading from 26055: heap size 1070 MB, throughput 0.960598
Reading from 26057: heap size 161 MB, throughput 0.997318
Reading from 26057: heap size 161 MB, throughput 0.998003
Reading from 26055: heap size 1075 MB, throughput 0.957566
Equal recommendation: 1834 MB each
Reading from 26057: heap size 164 MB, throughput 0.998476
Reading from 26057: heap size 164 MB, throughput 0.998205
Reading from 26055: heap size 1080 MB, throughput 0.962044
Reading from 26057: heap size 166 MB, throughput 0.996945
Reading from 26057: heap size 167 MB, throughput 0.991312
Reading from 26057: heap size 169 MB, throughput 0.996389
Reading from 26055: heap size 1075 MB, throughput 0.965224
Reading from 26057: heap size 170 MB, throughput 0.997467
Reading from 26057: heap size 173 MB, throughput 0.997644
Reading from 26055: heap size 1081 MB, throughput 0.951142
Reading from 26057: heap size 173 MB, throughput 0.997297
Reading from 26057: heap size 177 MB, throughput 0.997673
Reading from 26055: heap size 1076 MB, throughput 0.963558
Reading from 26057: heap size 177 MB, throughput 0.997482
Reading from 26057: heap size 179 MB, throughput 0.995014
Reading from 26055: heap size 1081 MB, throughput 0.957223
Equal recommendation: 1834 MB each
Reading from 26057: heap size 179 MB, throughput 0.997161
Reading from 26055: heap size 1084 MB, throughput 0.963985
Reading from 26057: heap size 183 MB, throughput 0.997446
Reading from 26057: heap size 183 MB, throughput 0.997053
Reading from 26055: heap size 1085 MB, throughput 0.956808
Reading from 26057: heap size 185 MB, throughput 0.997703
Reading from 26057: heap size 186 MB, throughput 0.997222
Reading from 26055: heap size 1091 MB, throughput 0.963459
Reading from 26057: heap size 189 MB, throughput 0.997412
Reading from 26057: heap size 189 MB, throughput 0.997242
Reading from 26055: heap size 1093 MB, throughput 0.958798
Reading from 26057: heap size 192 MB, throughput 0.99798
Reading from 26057: heap size 192 MB, throughput 0.888513
Reading from 26057: heap size 199 MB, throughput 0.993139
Equal recommendation: 1834 MB each
Reading from 26057: heap size 199 MB, throughput 0.997578
Reading from 26055: heap size 1099 MB, throughput 0.547413
Reading from 26057: heap size 205 MB, throughput 0.998364
Reading from 26057: heap size 205 MB, throughput 0.998107
Reading from 26055: heap size 1190 MB, throughput 0.949275
Reading from 26057: heap size 209 MB, throughput 0.998412
Reading from 26057: heap size 210 MB, throughput 0.998081
Reading from 26055: heap size 1199 MB, throughput 0.976257
Reading from 26057: heap size 214 MB, throughput 0.998341
Reading from 26055: heap size 1200 MB, throughput 0.976524
Reading from 26057: heap size 214 MB, throughput 0.998123
Reading from 26057: heap size 218 MB, throughput 0.998313
Equal recommendation: 1834 MB each
Reading from 26055: heap size 1201 MB, throughput 0.974451
Reading from 26057: heap size 218 MB, throughput 0.998042
Reading from 26057: heap size 222 MB, throughput 0.998309
Reading from 26055: heap size 1203 MB, throughput 0.971817
Reading from 26057: heap size 222 MB, throughput 0.998201
Reading from 26055: heap size 1196 MB, throughput 0.968408
Reading from 26057: heap size 226 MB, throughput 0.998288
Reading from 26057: heap size 226 MB, throughput 0.985369
Reading from 26057: heap size 229 MB, throughput 0.99204
Reading from 26055: heap size 1201 MB, throughput 0.972848
Reading from 26057: heap size 230 MB, throughput 0.997758
Reading from 26057: heap size 238 MB, throughput 0.997867
Reading from 26055: heap size 1196 MB, throughput 0.970466
Equal recommendation: 1834 MB each
Reading from 26057: heap size 239 MB, throughput 0.997643
Reading from 26055: heap size 1199 MB, throughput 0.970575
Reading from 26057: heap size 245 MB, throughput 0.998299
Reading from 26057: heap size 246 MB, throughput 0.998004
Reading from 26055: heap size 1203 MB, throughput 0.97018
Reading from 26057: heap size 252 MB, throughput 0.998082
Reading from 26055: heap size 1205 MB, throughput 0.966789
Reading from 26057: heap size 252 MB, throughput 0.997834
Reading from 26057: heap size 257 MB, throughput 0.998082
Reading from 26055: heap size 1210 MB, throughput 0.964313
Equal recommendation: 1834 MB each
Reading from 26057: heap size 257 MB, throughput 0.997836
Reading from 26057: heap size 262 MB, throughput 0.998047
Reading from 26055: heap size 1213 MB, throughput 0.964914
Reading from 26057: heap size 262 MB, throughput 0.99588
Reading from 26057: heap size 267 MB, throughput 0.994542
Reading from 26055: heap size 1218 MB, throughput 0.965663
Reading from 26057: heap size 267 MB, throughput 0.997332
Reading from 26055: heap size 1225 MB, throughput 0.96297
Reading from 26057: heap size 274 MB, throughput 0.99795
Reading from 26057: heap size 275 MB, throughput 0.997788
Reading from 26055: heap size 1231 MB, throughput 0.962909
Equal recommendation: 1834 MB each
Reading from 26057: heap size 281 MB, throughput 0.998438
Reading from 26055: heap size 1240 MB, throughput 0.960723
Reading from 26057: heap size 281 MB, throughput 0.998038
Reading from 26057: heap size 285 MB, throughput 0.99813
Reading from 26055: heap size 1248 MB, throughput 0.959668
Reading from 26057: heap size 286 MB, throughput 0.99798
Reading from 26055: heap size 1254 MB, throughput 0.959659
Reading from 26057: heap size 292 MB, throughput 0.998359
Reading from 26057: heap size 292 MB, throughput 0.998411
Reading from 26055: heap size 1261 MB, throughput 0.957259
Reading from 26057: heap size 297 MB, throughput 0.992898
Equal recommendation: 1834 MB each
Reading from 26057: heap size 297 MB, throughput 0.99743
Reading from 26057: heap size 305 MB, throughput 0.996851
Reading from 26055: heap size 1265 MB, throughput 0.555648
Reading from 26057: heap size 305 MB, throughput 0.997916
Reading from 26055: heap size 1377 MB, throughput 0.944154
Reading from 26057: heap size 311 MB, throughput 0.998164
Reading from 26057: heap size 312 MB, throughput 0.997941
Reading from 26055: heap size 1379 MB, throughput 0.973879
Equal recommendation: 1834 MB each
Reading from 26057: heap size 318 MB, throughput 0.99824
Reading from 26055: heap size 1385 MB, throughput 0.974314
Reading from 26057: heap size 319 MB, throughput 0.998087
Reading from 26057: heap size 324 MB, throughput 0.998402
Reading from 26055: heap size 1387 MB, throughput 0.973308
Reading from 26057: heap size 325 MB, throughput 0.995028
Reading from 26057: heap size 329 MB, throughput 0.997425
Reading from 26055: heap size 1385 MB, throughput 0.974059
Reading from 26057: heap size 330 MB, throughput 0.997933
Equal recommendation: 1834 MB each
Reading from 26055: heap size 1389 MB, throughput 0.97089
Reading from 26057: heap size 338 MB, throughput 0.998173
Reading from 26057: heap size 338 MB, throughput 0.998103
Reading from 26057: heap size 343 MB, throughput 0.998294
Reading from 26057: heap size 344 MB, throughput 0.998067
Equal recommendation: 1834 MB each
Reading from 26057: heap size 349 MB, throughput 0.998644
Reading from 26057: heap size 350 MB, throughput 0.998002
Reading from 26057: heap size 356 MB, throughput 0.995653
Reading from 26055: heap size 1381 MB, throughput 0.988475
Reading from 26057: heap size 356 MB, throughput 0.998008
Reading from 26057: heap size 364 MB, throughput 0.998278
Equal recommendation: 1834 MB each
Reading from 26057: heap size 364 MB, throughput 0.998158
Reading from 26057: heap size 371 MB, throughput 0.99819
Reading from 26057: heap size 371 MB, throughput 0.998204
Reading from 26055: heap size 1386 MB, throughput 0.989139
Reading from 26057: heap size 377 MB, throughput 0.998188
Reading from 26057: heap size 377 MB, throughput 0.997147
Equal recommendation: 1834 MB each
Reading from 26057: heap size 383 MB, throughput 0.951442
Reading from 26055: heap size 1394 MB, throughput 0.982829
Reading from 26057: heap size 389 MB, throughput 0.999083
Reading from 26055: heap size 1420 MB, throughput 0.358485
Reading from 26055: heap size 1476 MB, throughput 0.993542
Reading from 26055: heap size 1514 MB, throughput 0.99501
Reading from 26055: heap size 1531 MB, throughput 0.995027
Reading from 26055: heap size 1536 MB, throughput 0.994248
Reading from 26055: heap size 1538 MB, throughput 0.995207
Reading from 26057: heap size 397 MB, throughput 0.999283
Reading from 26055: heap size 1541 MB, throughput 0.975494
Reading from 26055: heap size 1529 MB, throughput 0.990583
Reading from 26057: heap size 397 MB, throughput 0.999278
Equal recommendation: 1834 MB each
Reading from 26057: heap size 404 MB, throughput 0.998911
Reading from 26055: heap size 1280 MB, throughput 0.989832
Reading from 26057: heap size 404 MB, throughput 0.999096
Reading from 26055: heap size 1508 MB, throughput 0.988014
Reading from 26057: heap size 410 MB, throughput 0.998523
Reading from 26055: heap size 1303 MB, throughput 0.979345
Reading from 26057: heap size 410 MB, throughput 0.99644
Reading from 26055: heap size 1486 MB, throughput 0.984032
Reading from 26057: heap size 415 MB, throughput 0.998846
Equal recommendation: 1834 MB each
Reading from 26055: heap size 1326 MB, throughput 0.979988
Reading from 26057: heap size 416 MB, throughput 0.998624
Reading from 26055: heap size 1461 MB, throughput 0.976801
Reading from 26057: heap size 423 MB, throughput 0.998773
Reading from 26055: heap size 1349 MB, throughput 0.974459
Reading from 26057: heap size 424 MB, throughput 0.998646
Reading from 26055: heap size 1460 MB, throughput 0.976087
Equal recommendation: 1834 MB each
Reading from 26057: heap size 431 MB, throughput 0.998695
Reading from 26055: heap size 1466 MB, throughput 0.971528
Reading from 26057: heap size 431 MB, throughput 0.998136
Reading from 26057: heap size 438 MB, throughput 0.997127
Reading from 26055: heap size 1470 MB, throughput 0.972904
Reading from 26057: heap size 438 MB, throughput 0.998365
Reading from 26055: heap size 1470 MB, throughput 0.970287
Reading from 26057: heap size 446 MB, throughput 0.998464
Equal recommendation: 1834 MB each
Reading from 26055: heap size 1477 MB, throughput 0.970749
Reading from 26057: heap size 447 MB, throughput 0.998283
Reading from 26055: heap size 1481 MB, throughput 0.966704
Reading from 26057: heap size 456 MB, throughput 0.998377
Reading from 26055: heap size 1489 MB, throughput 0.965267
Reading from 26057: heap size 456 MB, throughput 0.998305
Reading from 26057: heap size 465 MB, throughput 0.995293
Reading from 26055: heap size 1497 MB, throughput 0.964405
Equal recommendation: 1834 MB each
Reading from 26057: heap size 465 MB, throughput 0.998165
Reading from 26055: heap size 1510 MB, throughput 0.963463
Reading from 26057: heap size 476 MB, throughput 0.998547
Reading from 26055: heap size 1517 MB, throughput 0.962961
Reading from 26057: heap size 477 MB, throughput 0.99846
Reading from 26055: heap size 1529 MB, throughput 0.963711
Equal recommendation: 1834 MB each
Reading from 26057: heap size 485 MB, throughput 0.998506
Reading from 26055: heap size 1533 MB, throughput 0.960515
Reading from 26057: heap size 487 MB, throughput 0.998426
Reading from 26055: heap size 1546 MB, throughput 0.960831
Reading from 26057: heap size 496 MB, throughput 0.996096
Reading from 26055: heap size 1548 MB, throughput 0.964139
Reading from 26057: heap size 496 MB, throughput 0.998245
Equal recommendation: 1834 MB each
Reading from 26055: heap size 1561 MB, throughput 0.966496
Reading from 26057: heap size 508 MB, throughput 0.998423
Reading from 26055: heap size 1562 MB, throughput 0.964666
Reading from 26057: heap size 509 MB, throughput 0.998332
Reading from 26055: heap size 1575 MB, throughput 0.964309
Reading from 26057: heap size 519 MB, throughput 0.998607
Equal recommendation: 1834 MB each
Reading from 26057: heap size 519 MB, throughput 0.997798
Reading from 26055: heap size 1575 MB, throughput 0.954659
Reading from 26057: heap size 530 MB, throughput 0.997527
Reading from 26055: heap size 1589 MB, throughput 0.96614
Reading from 26057: heap size 530 MB, throughput 0.998363
Reading from 26055: heap size 1589 MB, throughput 0.963807
Equal recommendation: 1834 MB each
Reading from 26057: heap size 540 MB, throughput 0.998333
Reading from 26055: heap size 1601 MB, throughput 0.737808
Reading from 26057: heap size 541 MB, throughput 0.998312
Reading from 26055: heap size 1661 MB, throughput 0.993245
Reading from 26057: heap size 552 MB, throughput 0.998567
Equal recommendation: 1834 MB each
Client 26057 died
Clients: 1
Reading from 26055: heap size 1664 MB, throughput 0.990996
Reading from 26055: heap size 1675 MB, throughput 0.988991
Reading from 26055: heap size 1689 MB, throughput 0.986762
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 26055: heap size 1691 MB, throughput 0.99631
Reading from 26055: heap size 1671 MB, throughput 0.988655
Reading from 26055: heap size 1684 MB, throughput 0.834333
Reading from 26055: heap size 1674 MB, throughput 0.806845
Reading from 26055: heap size 1706 MB, throughput 0.783927
Reading from 26055: heap size 1745 MB, throughput 0.813954
Client 26055 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
