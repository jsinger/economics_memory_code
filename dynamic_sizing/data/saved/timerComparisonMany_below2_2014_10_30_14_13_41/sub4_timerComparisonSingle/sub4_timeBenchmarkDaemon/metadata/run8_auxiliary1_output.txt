economemd
    total memory: 938 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_30_14_13_41/sub4_timerComparisonSingle/sub4_timeBenchmarkDaemon/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 6179: heap size 9 MB, throughput 0.992588
Clients: 1
Client 6179 has a minimum heap size of 276 MB
Reading from 6180: heap size 9 MB, throughput 0.992449
Clients: 2
Client 6180 has a minimum heap size of 276 MB
Reading from 6179: heap size 9 MB, throughput 0.988806
Reading from 6180: heap size 9 MB, throughput 0.989273
Reading from 6179: heap size 9 MB, throughput 0.981904
Reading from 6180: heap size 9 MB, throughput 0.981018
Reading from 6179: heap size 9 MB, throughput 0.974425
Reading from 6180: heap size 9 MB, throughput 0.973295
Reading from 6179: heap size 11 MB, throughput 0.979134
Reading from 6180: heap size 11 MB, throughput 0.972056
Reading from 6180: heap size 11 MB, throughput 0.973812
Reading from 6179: heap size 11 MB, throughput 0.974692
Reading from 6179: heap size 17 MB, throughput 0.941313
Reading from 6180: heap size 17 MB, throughput 0.953057
Reading from 6179: heap size 17 MB, throughput 0.872324
Reading from 6180: heap size 17 MB, throughput 0.849609
Reading from 6179: heap size 30 MB, throughput 0.808338
Reading from 6180: heap size 30 MB, throughput 0.759059
Reading from 6179: heap size 31 MB, throughput 0.495631
Reading from 6180: heap size 31 MB, throughput 0.448284
Reading from 6179: heap size 34 MB, throughput 0.734973
Reading from 6180: heap size 35 MB, throughput 0.698872
Reading from 6180: heap size 44 MB, throughput 0.198577
Reading from 6179: heap size 47 MB, throughput 0.733148
Reading from 6180: heap size 51 MB, throughput 0.0379764
Reading from 6179: heap size 51 MB, throughput 0.508453
Reading from 6179: heap size 52 MB, throughput 0.636786
Reading from 6179: heap size 69 MB, throughput 0.173137
Reading from 6180: heap size 51 MB, throughput 0.463679
Reading from 6179: heap size 71 MB, throughput 0.262228
Reading from 6180: heap size 71 MB, throughput 0.0296187
Reading from 6180: heap size 72 MB, throughput 0.421155
Reading from 6179: heap size 75 MB, throughput 0.392441
Reading from 6180: heap size 97 MB, throughput 0.203804
Reading from 6179: heap size 95 MB, throughput 0.056556
Reading from 6180: heap size 99 MB, throughput 0.780281
Reading from 6179: heap size 102 MB, throughput 0.123695
Reading from 6180: heap size 100 MB, throughput 0.739891
Reading from 6180: heap size 104 MB, throughput 0.775648
Reading from 6179: heap size 103 MB, throughput 0.473906
Reading from 6179: heap size 133 MB, throughput 0.494965
Reading from 6179: heap size 135 MB, throughput 0.767786
Reading from 6180: heap size 107 MB, throughput 0.448929
Reading from 6180: heap size 134 MB, throughput 0.658941
Reading from 6179: heap size 139 MB, throughput 0.749641
Reading from 6180: heap size 144 MB, throughput 0.653379
Reading from 6179: heap size 144 MB, throughput 0.699712
Reading from 6180: heap size 144 MB, throughput 0.672257
Reading from 6180: heap size 148 MB, throughput 0.667159
Reading from 6180: heap size 153 MB, throughput 0.664293
Reading from 6179: heap size 149 MB, throughput 0.478821
Reading from 6179: heap size 181 MB, throughput 0.577887
Reading from 6179: heap size 192 MB, throughput 0.700885
Reading from 6180: heap size 155 MB, throughput 0.38569
Reading from 6179: heap size 193 MB, throughput 0.680755
Reading from 6180: heap size 193 MB, throughput 0.654221
Reading from 6180: heap size 200 MB, throughput 0.644195
Reading from 6180: heap size 204 MB, throughput 0.767455
Reading from 6180: heap size 207 MB, throughput 0.878466
Reading from 6179: heap size 196 MB, throughput 0.541499
Reading from 6180: heap size 214 MB, throughput 0.821983
Reading from 6180: heap size 219 MB, throughput 0.741594
Reading from 6179: heap size 236 MB, throughput 0.835181
Reading from 6180: heap size 228 MB, throughput 0.640367
Reading from 6179: heap size 250 MB, throughput 0.83152
Reading from 6180: heap size 238 MB, throughput 0.618573
Reading from 6179: heap size 252 MB, throughput 0.882004
Reading from 6179: heap size 257 MB, throughput 0.837241
Reading from 6179: heap size 259 MB, throughput 0.807395
Reading from 6179: heap size 263 MB, throughput 0.768996
Reading from 6179: heap size 265 MB, throughput 0.740975
Reading from 6180: heap size 238 MB, throughput 0.491349
Reading from 6179: heap size 272 MB, throughput 0.702426
Reading from 6180: heap size 279 MB, throughput 0.553097
Reading from 6179: heap size 276 MB, throughput 0.672451
Reading from 6180: heap size 279 MB, throughput 0.599503
Reading from 6179: heap size 284 MB, throughput 0.666566
Reading from 6180: heap size 276 MB, throughput 0.343318
Reading from 6180: heap size 321 MB, throughput 0.700217
Reading from 6180: heap size 321 MB, throughput 0.739726
Reading from 6179: heap size 286 MB, throughput 0.89049
Reading from 6180: heap size 323 MB, throughput 0.776632
Reading from 6180: heap size 320 MB, throughput 0.715246
Reading from 6179: heap size 293 MB, throughput 0.833272
Reading from 6180: heap size 322 MB, throughput 0.764957
Reading from 6180: heap size 318 MB, throughput 0.771318
Reading from 6180: heap size 321 MB, throughput 0.85978
Reading from 6179: heap size 294 MB, throughput 0.686319
Reading from 6180: heap size 323 MB, throughput 0.912556
Reading from 6179: heap size 334 MB, throughput 0.718933
Reading from 6180: heap size 323 MB, throughput 0.870344
Reading from 6180: heap size 319 MB, throughput 0.858117
Reading from 6179: heap size 336 MB, throughput 0.80895
Reading from 6180: heap size 321 MB, throughput 0.842289
Reading from 6180: heap size 316 MB, throughput 0.830036
Reading from 6179: heap size 341 MB, throughput 0.841381
Reading from 6180: heap size 319 MB, throughput 0.894968
Reading from 6179: heap size 342 MB, throughput 0.794663
Reading from 6180: heap size 315 MB, throughput 0.856504
Reading from 6179: heap size 346 MB, throughput 0.746499
Reading from 6180: heap size 318 MB, throughput 0.817045
Reading from 6179: heap size 346 MB, throughput 0.748621
Reading from 6180: heap size 319 MB, throughput 0.7855
Reading from 6180: heap size 319 MB, throughput 0.641628
Reading from 6179: heap size 355 MB, throughput 0.72909
Reading from 6180: heap size 325 MB, throughput 0.655969
Reading from 6180: heap size 326 MB, throughput 0.609088
Reading from 6180: heap size 332 MB, throughput 0.608642
Numeric result:
Recommendation: 2 clients, utility 0.422036:
    h1: 432.656 MB (U(h) = 0.435852*h^0.0672636)
    h2: 505.344 MB (U(h) = 0.394708*h^0.0785693)
Recommendation: 2 clients, utility 0.422036:
    h1: 432.641 MB (U(h) = 0.435852*h^0.0672636)
    h2: 505.359 MB (U(h) = 0.394708*h^0.0785693)
Reading from 6179: heap size 355 MB, throughput 0.916075
Reading from 6180: heap size 333 MB, throughput 0.946855
Reading from 6179: heap size 364 MB, throughput 0.958013
Reading from 6180: heap size 334 MB, throughput 0.956244
Reading from 6180: heap size 337 MB, throughput 0.962709
Reading from 6179: heap size 365 MB, throughput 0.962004
Reading from 6180: heap size 339 MB, throughput 0.968936
Reading from 6179: heap size 366 MB, throughput 0.971208
Reading from 6180: heap size 341 MB, throughput 0.972104
Reading from 6179: heap size 370 MB, throughput 0.974014
Reading from 6180: heap size 337 MB, throughput 0.974079
Reading from 6180: heap size 341 MB, throughput 0.964261
Reading from 6179: heap size 370 MB, throughput 0.890698
Reading from 6180: heap size 339 MB, throughput 0.965711
Numeric result:
Recommendation: 2 clients, utility 0.503114:
    h1: 429.705 MB (U(h) = 0.378796*h^0.102724)
    h2: 508.295 MB (U(h) = 0.334143*h^0.121516)
Recommendation: 2 clients, utility 0.503114:
    h1: 429.697 MB (U(h) = 0.378796*h^0.102724)
    h2: 508.303 MB (U(h) = 0.334143*h^0.121516)
Reading from 6179: heap size 417 MB, throughput 0.976647
Reading from 6180: heap size 341 MB, throughput 0.974688
Reading from 6180: heap size 342 MB, throughput 0.972354
Reading from 6179: heap size 420 MB, throughput 0.983034
Reading from 6180: heap size 343 MB, throughput 0.972269
Reading from 6179: heap size 422 MB, throughput 0.981733
Reading from 6180: heap size 346 MB, throughput 0.970252
Reading from 6179: heap size 425 MB, throughput 0.982448
Reading from 6180: heap size 346 MB, throughput 0.971474
Reading from 6180: heap size 349 MB, throughput 0.973056
Reading from 6179: heap size 426 MB, throughput 0.982299
Reading from 6180: heap size 350 MB, throughput 0.974968
Reading from 6179: heap size 423 MB, throughput 0.978094
Numeric result:
Recommendation: 2 clients, utility 0.581977:
    h1: 450.105 MB (U(h) = 0.32473*h^0.140376)
    h2: 487.895 MB (U(h) = 0.29639*h^0.152161)
Recommendation: 2 clients, utility 0.581977:
    h1: 450.105 MB (U(h) = 0.32473*h^0.140376)
    h2: 487.895 MB (U(h) = 0.29639*h^0.152161)
Reading from 6180: heap size 351 MB, throughput 0.97301
Reading from 6180: heap size 354 MB, throughput 0.957575
Reading from 6180: heap size 351 MB, throughput 0.935992
Reading from 6179: heap size 426 MB, throughput 0.972306
Reading from 6180: heap size 355 MB, throughput 0.926581
Reading from 6179: heap size 425 MB, throughput 0.950065
Reading from 6180: heap size 397 MB, throughput 0.795401
Reading from 6179: heap size 427 MB, throughput 0.93194
Reading from 6180: heap size 399 MB, throughput 0.877057
Reading from 6179: heap size 437 MB, throughput 0.914589
Reading from 6179: heap size 440 MB, throughput 0.970066
Reading from 6180: heap size 404 MB, throughput 0.973009
Reading from 6180: heap size 405 MB, throughput 0.979978
Reading from 6179: heap size 447 MB, throughput 0.983773
Reading from 6180: heap size 404 MB, throughput 0.979773
Reading from 6179: heap size 450 MB, throughput 0.986068
Reading from 6180: heap size 407 MB, throughput 0.981132
Reading from 6179: heap size 430 MB, throughput 0.985924
Reading from 6180: heap size 402 MB, throughput 0.981944
Numeric result:
Recommendation: 2 clients, utility 0.650645:
    h1: 439.889 MB (U(h) = 0.296558*h^0.162352)
    h2: 498.111 MB (U(h) = 0.260745*h^0.183837)
Recommendation: 2 clients, utility 0.650645:
    h1: 439.893 MB (U(h) = 0.296558*h^0.162352)
    h2: 498.107 MB (U(h) = 0.260745*h^0.183837)
Reading from 6179: heap size 443 MB, throughput 0.985518
Reading from 6180: heap size 405 MB, throughput 0.981854
Reading from 6180: heap size 407 MB, throughput 0.977922
Reading from 6179: heap size 426 MB, throughput 0.980847
Reading from 6180: heap size 407 MB, throughput 0.974803
Reading from 6179: heap size 436 MB, throughput 0.983028
Reading from 6180: heap size 410 MB, throughput 0.974563
Reading from 6179: heap size 437 MB, throughput 0.984521
Reading from 6180: heap size 412 MB, throughput 0.97561
Reading from 6179: heap size 438 MB, throughput 0.982659
Reading from 6180: heap size 414 MB, throughput 0.968785
Numeric result:
Recommendation: 2 clients, utility 0.675977:
    h1: 442.375 MB (U(h) = 0.28471*h^0.172226)
    h2: 495.625 MB (U(h) = 0.25108*h^0.192955)
Recommendation: 2 clients, utility 0.675977:
    h1: 442.378 MB (U(h) = 0.28471*h^0.172226)
    h2: 495.622 MB (U(h) = 0.25108*h^0.192955)
Reading from 6180: heap size 417 MB, throughput 0.978386
Reading from 6179: heap size 441 MB, throughput 0.98567
Reading from 6180: heap size 416 MB, throughput 0.979125
Reading from 6180: heap size 417 MB, throughput 0.963774
Reading from 6180: heap size 416 MB, throughput 0.934907
Reading from 6179: heap size 441 MB, throughput 0.979876
Reading from 6180: heap size 418 MB, throughput 0.905225
Reading from 6179: heap size 442 MB, throughput 0.971485
Reading from 6179: heap size 429 MB, throughput 0.960578
Reading from 6180: heap size 427 MB, throughput 0.942827
Reading from 6179: heap size 441 MB, throughput 0.979384
Reading from 6180: heap size 427 MB, throughput 0.976166
Reading from 6179: heap size 440 MB, throughput 0.984708
Reading from 6180: heap size 430 MB, throughput 0.982904
Reading from 6179: heap size 447 MB, throughput 0.98785
Reading from 6180: heap size 432 MB, throughput 0.983265
Reading from 6180: heap size 431 MB, throughput 0.984055
Reading from 6179: heap size 415 MB, throughput 0.988973
Numeric result:
Recommendation: 2 clients, utility 0.711334:
    h1: 437.062 MB (U(h) = 0.273535*h^0.181979)
    h2: 500.938 MB (U(h) = 0.235195*h^0.208576)
Recommendation: 2 clients, utility 0.711334:
    h1: 437.061 MB (U(h) = 0.273535*h^0.181979)
    h2: 500.939 MB (U(h) = 0.235195*h^0.208576)
Reading from 6180: heap size 434 MB, throughput 0.985026
Reading from 6179: heap size 445 MB, throughput 0.988048
Reading from 6180: heap size 431 MB, throughput 0.983498
Reading from 6179: heap size 417 MB, throughput 0.986182
Reading from 6180: heap size 434 MB, throughput 0.98438
Reading from 6179: heap size 441 MB, throughput 0.98486
Reading from 6180: heap size 431 MB, throughput 0.983813
Reading from 6179: heap size 420 MB, throughput 0.985262
Reading from 6180: heap size 433 MB, throughput 0.981942
Reading from 6179: heap size 436 MB, throughput 0.983769
Numeric result:
Recommendation: 2 clients, utility 0.720453:
    h1: 435.615 MB (U(h) = 0.270841*h^0.184365)
    h2: 502.385 MB (U(h) = 0.231208*h^0.212633)
Recommendation: 2 clients, utility 0.720453:
    h1: 435.605 MB (U(h) = 0.270841*h^0.184365)
    h2: 502.395 MB (U(h) = 0.231208*h^0.212633)
Reading from 6180: heap size 432 MB, throughput 0.986776
Reading from 6179: heap size 436 MB, throughput 0.987641
Reading from 6180: heap size 433 MB, throughput 0.983299
Reading from 6180: heap size 433 MB, throughput 0.97253
Reading from 6179: heap size 438 MB, throughput 0.980854
Reading from 6180: heap size 434 MB, throughput 0.953254
Reading from 6179: heap size 429 MB, throughput 0.969118
Reading from 6180: heap size 439 MB, throughput 0.934852
Reading from 6179: heap size 437 MB, throughput 0.950157
Reading from 6179: heap size 424 MB, throughput 0.977971
Reading from 6180: heap size 440 MB, throughput 0.982048
Reading from 6179: heap size 435 MB, throughput 0.985971
Reading from 6180: heap size 446 MB, throughput 0.987718
Reading from 6179: heap size 436 MB, throughput 0.986693
Reading from 6180: heap size 447 MB, throughput 0.989521
Reading from 6179: heap size 438 MB, throughput 0.986745
Reading from 6180: heap size 448 MB, throughput 0.989118
Numeric result:
Recommendation: 2 clients, utility 0.738034:
    h1: 432.157 MB (U(h) = 0.26625*h^0.188536)
    h2: 505.843 MB (U(h) = 0.223426*h^0.220687)
Recommendation: 2 clients, utility 0.738034:
    h1: 432.153 MB (U(h) = 0.26625*h^0.188536)
    h2: 505.847 MB (U(h) = 0.223426*h^0.220687)
Reading from 6179: heap size 411 MB, throughput 0.988353
Reading from 6180: heap size 450 MB, throughput 0.989456
Reading from 6179: heap size 437 MB, throughput 0.988273
Reading from 6180: heap size 447 MB, throughput 0.988392
Reading from 6179: heap size 415 MB, throughput 0.986586
Reading from 6180: heap size 450 MB, throughput 0.986362
Reading from 6179: heap size 432 MB, throughput 0.987292
Reading from 6180: heap size 452 MB, throughput 0.984861
Reading from 6179: heap size 417 MB, throughput 0.986385
Numeric result:
Recommendation: 2 clients, utility 0.748488:
    h1: 433.918 MB (U(h) = 0.261891*h^0.192593)
    h2: 504.082 MB (U(h) = 0.220537*h^0.223732)
Recommendation: 2 clients, utility 0.748488:
    h1: 433.921 MB (U(h) = 0.261891*h^0.192593)
    h2: 504.079 MB (U(h) = 0.220537*h^0.223732)
Reading from 6179: heap size 428 MB, throughput 0.986782
Reading from 6180: heap size 452 MB, throughput 0.989721
Reading from 6180: heap size 455 MB, throughput 0.982713
Reading from 6180: heap size 455 MB, throughput 0.969573
Reading from 6179: heap size 428 MB, throughput 0.983191
Reading from 6180: heap size 460 MB, throughput 0.953823
Reading from 6179: heap size 429 MB, throughput 0.969991
Reading from 6179: heap size 431 MB, throughput 0.954544
Reading from 6179: heap size 437 MB, throughput 0.963427
Reading from 6180: heap size 462 MB, throughput 0.983242
Reading from 6179: heap size 403 MB, throughput 0.986046
Reading from 6180: heap size 468 MB, throughput 0.987911
Reading from 6179: heap size 436 MB, throughput 0.987469
Reading from 6180: heap size 470 MB, throughput 0.988753
Reading from 6179: heap size 405 MB, throughput 0.988381
Reading from 6179: heap size 434 MB, throughput 0.987915
Reading from 6180: heap size 470 MB, throughput 0.988692
Numeric result:
Recommendation: 2 clients, utility 0.769274:
    h1: 436.854 MB (U(h) = 0.253758*h^0.200372)
    h2: 501.146 MB (U(h) = 0.214782*h^0.22986)
Recommendation: 2 clients, utility 0.769274:
    h1: 436.855 MB (U(h) = 0.253758*h^0.200372)
    h2: 501.145 MB (U(h) = 0.214782*h^0.22986)
Reading from 6179: heap size 432 MB, throughput 0.98623
Reading from 6180: heap size 472 MB, throughput 0.977965
Reading from 6179: heap size 430 MB, throughput 0.984809
Reading from 6180: heap size 487 MB, throughput 0.992623
Reading from 6179: heap size 432 MB, throughput 0.984993
Reading from 6179: heap size 434 MB, throughput 0.985264
Reading from 6180: heap size 489 MB, throughput 0.994156
Reading from 6179: heap size 434 MB, throughput 0.98512
Numeric result:
Recommendation: 2 clients, utility 0.773954:
    h1: 433.443 MB (U(h) = 0.253802*h^0.200329)
    h2: 504.557 MB (U(h) = 0.211664*h^0.2332)
Recommendation: 2 clients, utility 0.773954:
    h1: 433.44 MB (U(h) = 0.253802*h^0.200329)
    h2: 504.56 MB (U(h) = 0.211664*h^0.2332)
Reading from 6180: heap size 494 MB, throughput 0.994286
Reading from 6179: heap size 437 MB, throughput 0.987451
Reading from 6179: heap size 409 MB, throughput 0.97869
Reading from 6179: heap size 433 MB, throughput 0.966362
Reading from 6179: heap size 424 MB, throughput 0.957227
Reading from 6180: heap size 495 MB, throughput 0.989442
Reading from 6180: heap size 489 MB, throughput 0.981142
Reading from 6180: heap size 481 MB, throughput 0.963552
Reading from 6179: heap size 434 MB, throughput 0.984836
Reading from 6180: heap size 485 MB, throughput 0.983984
Reading from 6179: heap size 401 MB, throughput 0.986877
Reading from 6180: heap size 487 MB, throughput 0.98803
Reading from 6179: heap size 432 MB, throughput 0.987276
Reading from 6180: heap size 483 MB, throughput 0.988768
Reading from 6179: heap size 431 MB, throughput 0.986225
Numeric result:
Recommendation: 2 clients, utility 0.786752:
    h1: 433.694 MB (U(h) = 0.249698*h^0.204376)
    h2: 504.306 MB (U(h) = 0.207565*h^0.237649)
Recommendation: 2 clients, utility 0.786752:
    h1: 433.696 MB (U(h) = 0.249698*h^0.204376)
    h2: 504.304 MB (U(h) = 0.207565*h^0.237649)
Reading from 6180: heap size 485 MB, throughput 0.987923
Reading from 6179: heap size 429 MB, throughput 0.98536
Reading from 6180: heap size 483 MB, throughput 0.987513
Reading from 6179: heap size 432 MB, throughput 0.984928
Reading from 6180: heap size 485 MB, throughput 0.986292
Reading from 6179: heap size 432 MB, throughput 0.986015
Reading from 6179: heap size 433 MB, throughput 0.984603
Reading from 6180: heap size 488 MB, throughput 0.986353
Reading from 6179: heap size 435 MB, throughput 0.984153
Reading from 6180: heap size 489 MB, throughput 0.984674
Numeric result:
Recommendation: 2 clients, utility 0.788214:
    h1: 433.071 MB (U(h) = 0.249527*h^0.204542)
    h2: 504.929 MB (U(h) = 0.206803*h^0.238486)
Recommendation: 2 clients, utility 0.788214:
    h1: 433.066 MB (U(h) = 0.249527*h^0.204542)
    h2: 504.934 MB (U(h) = 0.206803*h^0.238486)
Reading from 6180: heap size 490 MB, throughput 0.978075
Reading from 6179: heap size 425 MB, throughput 0.978994
Reading from 6180: heap size 492 MB, throughput 0.969653
Reading from 6179: heap size 434 MB, throughput 0.970864
Reading from 6180: heap size 488 MB, throughput 0.957015
Reading from 6179: heap size 426 MB, throughput 0.960191
Reading from 6179: heap size 431 MB, throughput 0.951103
Reading from 6180: heap size 478 MB, throughput 0.979234
Reading from 6179: heap size 433 MB, throughput 0.978425
Reading from 6180: heap size 490 MB, throughput 0.986817
Reading from 6179: heap size 438 MB, throughput 0.98819
Reading from 6180: heap size 456 MB, throughput 0.985966
Reading from 6179: heap size 409 MB, throughput 0.985112
Reading from 6179: heap size 437 MB, throughput 0.986345
Reading from 6180: heap size 490 MB, throughput 0.987526
Numeric result:
Recommendation: 2 clients, utility 0.792186:
    h1: 430.281 MB (U(h) = 0.249618*h^0.204454)
    h2: 507.719 MB (U(h) = 0.204322*h^0.241259)
Recommendation: 2 clients, utility 0.792186:
    h1: 430.272 MB (U(h) = 0.249618*h^0.204454)
    h2: 507.728 MB (U(h) = 0.204322*h^0.241259)
Reading from 6179: heap size 412 MB, throughput 0.985618
Reading from 6180: heap size 463 MB, throughput 0.988054
Reading from 6179: heap size 433 MB, throughput 0.984903
Reading from 6180: heap size 489 MB, throughput 0.986995
Reading from 6179: heap size 414 MB, throughput 0.985425
Reading from 6180: heap size 488 MB, throughput 0.988583
Reading from 6179: heap size 427 MB, throughput 0.984257
Reading from 6180: heap size 489 MB, throughput 0.980486
Reading from 6179: heap size 427 MB, throughput 0.97575
Numeric result:
Recommendation: 2 clients, utility 0.798122:
    h1: 432.748 MB (U(h) = 0.246723*h^0.207345)
    h2: 505.252 MB (U(h) = 0.203597*h^0.242085)
Recommendation: 2 clients, utility 0.798122:
    h1: 432.747 MB (U(h) = 0.246723*h^0.207345)
    h2: 505.253 MB (U(h) = 0.203597*h^0.242085)
Reading from 6180: heap size 490 MB, throughput 0.984875
Reading from 6180: heap size 493 MB, throughput 0.978035
Reading from 6179: heap size 430 MB, throughput 0.983101
Reading from 6180: heap size 494 MB, throughput 0.96757
Reading from 6179: heap size 431 MB, throughput 0.977294
Reading from 6179: heap size 431 MB, throughput 0.964004
Reading from 6179: heap size 434 MB, throughput 0.948329
Reading from 6180: heap size 492 MB, throughput 0.983413
Reading from 6179: heap size 425 MB, throughput 0.975388
Reading from 6179: heap size 434 MB, throughput 0.984444
Reading from 6180: heap size 493 MB, throughput 0.988567
Reading from 6179: heap size 420 MB, throughput 0.987627
Reading from 6180: heap size 492 MB, throughput 0.990728
Reading from 6179: heap size 431 MB, throughput 0.990175
Reading from 6180: heap size 493 MB, throughput 0.98989
Numeric result:
Recommendation: 2 clients, utility 0.799349:
    h1: 432.252 MB (U(h) = 0.246572*h^0.207493)
    h2: 505.748 MB (U(h) = 0.20298*h^0.242773)
Recommendation: 2 clients, utility 0.799349:
    h1: 432.253 MB (U(h) = 0.246572*h^0.207493)
    h2: 505.747 MB (U(h) = 0.20298*h^0.242773)
Reading from 6179: heap size 430 MB, throughput 0.988463
Reading from 6180: heap size 490 MB, throughput 0.987839
Reading from 6179: heap size 432 MB, throughput 0.986777
Reading from 6180: heap size 492 MB, throughput 0.987472
Reading from 6179: heap size 418 MB, throughput 0.985583
Reading from 6179: heap size 426 MB, throughput 0.987219
Reading from 6180: heap size 492 MB, throughput 0.98697
Reading from 6179: heap size 428 MB, throughput 0.983924
Reading from 6180: heap size 493 MB, throughput 0.985547
Reading from 6179: heap size 428 MB, throughput 0.98777
Numeric result:
Recommendation: 2 clients, utility 0.801489:
    h1: 433.677 MB (U(h) = 0.245276*h^0.208792)
    h2: 504.323 MB (U(h) = 0.202951*h^0.242805)
Recommendation: 2 clients, utility 0.801489:
    h1: 433.677 MB (U(h) = 0.245276*h^0.208792)
    h2: 504.323 MB (U(h) = 0.202951*h^0.242805)
Reading from 6180: heap size 490 MB, throughput 0.983673
Reading from 6180: heap size 484 MB, throughput 0.97606
Reading from 6180: heap size 492 MB, throughput 0.964542
Reading from 6179: heap size 431 MB, throughput 0.985929
Reading from 6179: heap size 431 MB, throughput 0.971403
Reading from 6179: heap size 430 MB, throughput 0.961786
Reading from 6179: heap size 432 MB, throughput 0.964858
Reading from 6180: heap size 479 MB, throughput 0.984918
Reading from 6179: heap size 441 MB, throughput 0.984981
Reading from 6180: heap size 491 MB, throughput 0.98991
Reading from 6179: heap size 410 MB, throughput 0.988035
Reading from 6180: heap size 491 MB, throughput 0.990082
Reading from 6179: heap size 439 MB, throughput 0.987744
Reading from 6179: heap size 412 MB, throughput 0.98664
Reading from 6180: heap size 489 MB, throughput 0.990207
Numeric result:
Recommendation: 2 clients, utility 0.865085:
    h1: 336.882 MB (U(h) = 0.243225*h^0.210852)
    h2: 601.118 MB (U(h) = 0.0938838*h^0.376233)
Recommendation: 2 clients, utility 0.865085:
    h1: 336.884 MB (U(h) = 0.243225*h^0.210852)
    h2: 601.116 MB (U(h) = 0.0938838*h^0.376233)
Reading from 6179: heap size 435 MB, throughput 0.986066
Reading from 6180: heap size 490 MB, throughput 0.989434
Reading from 6179: heap size 414 MB, throughput 0.985961
Reading from 6180: heap size 488 MB, throughput 0.987717
Reading from 6179: heap size 420 MB, throughput 0.986861
Reading from 6180: heap size 490 MB, throughput 0.987635
Reading from 6179: heap size 411 MB, throughput 0.986643
Reading from 6179: heap size 416 MB, throughput 0.98351
Reading from 6180: heap size 492 MB, throughput 0.985194
Numeric result:
Recommendation: 2 clients, utility 0.866884:
    h1: 337.89 MB (U(h) = 0.242193*h^0.211904)
    h2: 600.11 MB (U(h) = 0.0938305*h^0.376354)
Recommendation: 2 clients, utility 0.866884:
    h1: 337.89 MB (U(h) = 0.242193*h^0.211904)
    h2: 600.11 MB (U(h) = 0.0938305*h^0.376354)
Reading from 6180: heap size 492 MB, throughput 0.984149
Reading from 6180: heap size 491 MB, throughput 0.974873
Reading from 6179: heap size 409 MB, throughput 0.98654
Reading from 6180: heap size 479 MB, throughput 0.963066
Reading from 6179: heap size 409 MB, throughput 0.976607
Reading from 6179: heap size 406 MB, throughput 0.964561
Reading from 6179: heap size 410 MB, throughput 0.946224
Reading from 6179: heap size 400 MB, throughput 0.96816
Reading from 6180: heap size 492 MB, throughput 0.983254
Reading from 6179: heap size 406 MB, throughput 0.981486
Reading from 6180: heap size 458 MB, throughput 0.987985
Reading from 6179: heap size 376 MB, throughput 0.986036
Reading from 6179: heap size 399 MB, throughput 0.987832
Reading from 6180: heap size 491 MB, throughput 0.989051
Reading from 6179: heap size 374 MB, throughput 0.988097
Reading from 6179: heap size 392 MB, throughput 0.985462
Reading from 6180: heap size 462 MB, throughput 0.990796
Numeric result:
Recommendation: 2 clients, utility 0.917657:
    h1: 303.503 MB (U(h) = 0.237296*h^0.2172)
    h2: 634.497 MB (U(h) = 0.059669*h^0.454076)
Recommendation: 2 clients, utility 0.917657:
    h1: 303.502 MB (U(h) = 0.237296*h^0.2172)
    h2: 634.498 MB (U(h) = 0.059669*h^0.454076)
Reading from 6179: heap size 373 MB, throughput 0.986023
Reading from 6180: heap size 492 MB, throughput 0.98751
Reading from 6179: heap size 384 MB, throughput 0.983295
Reading from 6179: heap size 371 MB, throughput 0.981693
Reading from 6180: heap size 490 MB, throughput 0.987688
Reading from 6179: heap size 380 MB, throughput 0.980216
Reading from 6179: heap size 370 MB, throughput 0.979037
Reading from 6180: heap size 491 MB, throughput 0.988781
Reading from 6179: heap size 377 MB, throughput 0.978511
Reading from 6180: heap size 490 MB, throughput 0.985715
Reading from 6179: heap size 368 MB, throughput 0.976668
Numeric result:
Recommendation: 2 clients, utility 0.926375:
    h1: 307.89 MB (U(h) = 0.233186*h^0.221895)
    h2: 630.11 MB (U(h) = 0.0596582*h^0.454114)
Recommendation: 2 clients, utility 0.926375:
    h1: 307.892 MB (U(h) = 0.233186*h^0.221895)
    h2: 630.108 MB (U(h) = 0.0596582*h^0.454114)
Reading from 6179: heap size 372 MB, throughput 0.975842
Reading from 6180: heap size 488 MB, throughput 0.985904
Reading from 6179: heap size 366 MB, throughput 0.963049
Reading from 6180: heap size 480 MB, throughput 0.976682
Reading from 6179: heap size 371 MB, throughput 0.938921
Reading from 6180: heap size 491 MB, throughput 0.96451
Reading from 6179: heap size 375 MB, throughput 0.904236
Reading from 6179: heap size 376 MB, throughput 0.867661
Reading from 6179: heap size 378 MB, throughput 0.82108
Reading from 6179: heap size 378 MB, throughput 0.981095
Reading from 6180: heap size 492 MB, throughput 0.983469
Reading from 6179: heap size 344 MB, throughput 0.977652
Reading from 6179: heap size 375 MB, throughput 0.98707
Reading from 6180: heap size 490 MB, throughput 0.986808
Reading from 6179: heap size 378 MB, throughput 0.989597
Reading from 6179: heap size 379 MB, throughput 0.988348
Reading from 6179: heap size 319 MB, throughput 0.986014
Reading from 6180: heap size 491 MB, throughput 0.9881
Reading from 6179: heap size 373 MB, throughput 0.985407
Reading from 6179: heap size 321 MB, throughput 0.9841
Reading from 6180: heap size 489 MB, throughput 0.98793
Numeric result:
Recommendation: 2 clients, utility 0.9998:
    h1: 446.498 MB (U(h) = 0.0573982*h^0.46601)
    h2: 491.502 MB (U(h) = 0.0422155*h^0.512981)
Recommendation: 2 clients, utility 0.9998:
    h1: 446.498 MB (U(h) = 0.0573982*h^0.46601)
    h2: 491.502 MB (U(h) = 0.0422155*h^0.512981)
Reading from 6179: heap size 365 MB, throughput 0.982385
Reading from 6179: heap size 369 MB, throughput 0.980084
Reading from 6180: heap size 490 MB, throughput 0.988001
Reading from 6179: heap size 362 MB, throughput 0.979906
Reading from 6179: heap size 332 MB, throughput 0.978833
Reading from 6180: heap size 488 MB, throughput 0.987799
Reading from 6179: heap size 360 MB, throughput 0.976616
Reading from 6179: heap size 362 MB, throughput 0.975078
Reading from 6179: heap size 363 MB, throughput 0.973391
Reading from 6180: heap size 490 MB, throughput 0.987034
Reading from 6179: heap size 363 MB, throughput 0.973457
Reading from 6179: heap size 366 MB, throughput 0.972247
Reading from 6180: heap size 491 MB, throughput 0.985876
Reading from 6179: heap size 367 MB, throughput 0.979531
Numeric result:
Recommendation: 2 clients, utility 1.12783:
    h1: 561.657 MB (U(h) = 0.010009*h^0.765651)
    h2: 376.343 MB (U(h) = 0.0422053*h^0.51303)
Recommendation: 2 clients, utility 1.12783:
    h1: 561.657 MB (U(h) = 0.010009*h^0.765651)
    h2: 376.343 MB (U(h) = 0.0422053*h^0.51303)
Reading from 6180: heap size 491 MB, throughput 0.985528
Reading from 6179: heap size 366 MB, throughput 0.975326
Reading from 6180: heap size 483 MB, throughput 0.976412
Reading from 6179: heap size 368 MB, throughput 0.957596
Reading from 6180: heap size 473 MB, throughput 0.95976
Reading from 6179: heap size 367 MB, throughput 0.931576
Reading from 6179: heap size 372 MB, throughput 0.904416
Reading from 6179: heap size 380 MB, throughput 0.857203
Reading from 6179: heap size 383 MB, throughput 0.827302
Reading from 6179: heap size 393 MB, throughput 0.956041
Reading from 6180: heap size 477 MB, throughput 0.98132
Reading from 6179: heap size 394 MB, throughput 0.976229
Reading from 6180: heap size 443 MB, throughput 0.986743
Reading from 6179: heap size 398 MB, throughput 0.981062
Reading from 6179: heap size 400 MB, throughput 0.982545
Reading from 6180: heap size 468 MB, throughput 0.989691
Reading from 6179: heap size 401 MB, throughput 0.983749
Reading from 6180: heap size 439 MB, throughput 0.988203
Reading from 6179: heap size 404 MB, throughput 0.983719
Numeric result:
Recommendation: 2 clients, utility 1.06326:
    h1: 396.059 MB (U(h) = 0.0625627*h^0.455357)
    h2: 541.941 MB (U(h) = 0.0220763*h^0.623087)
Recommendation: 2 clients, utility 1.06326:
    h1: 396.056 MB (U(h) = 0.0625627*h^0.455357)
    h2: 541.944 MB (U(h) = 0.0220763*h^0.623087)
Reading from 6179: heap size 403 MB, throughput 0.983
Reading from 6180: heap size 457 MB, throughput 0.987928
Reading from 6179: heap size 377 MB, throughput 0.982758
Reading from 6180: heap size 462 MB, throughput 0.986551
Reading from 6179: heap size 403 MB, throughput 0.983797
Reading from 6180: heap size 460 MB, throughput 0.986618
Reading from 6179: heap size 376 MB, throughput 0.980258
Reading from 6180: heap size 462 MB, throughput 0.985703
Reading from 6179: heap size 399 MB, throughput 0.981178
Reading from 6179: heap size 381 MB, throughput 0.982819
Reading from 6180: heap size 464 MB, throughput 0.984013
Reading from 6179: heap size 395 MB, throughput 0.977516
Reading from 6180: heap size 464 MB, throughput 0.984881
Reading from 6180: heap size 469 MB, throughput 0.976813
Numeric result:
Recommendation: 2 clients, utility 1.0151:
    h1: 489.679 MB (U(h) = 0.0564214*h^0.472936)
    h2: 448.321 MB (U(h) = 0.0683582*h^0.432993)
Recommendation: 2 clients, utility 1.0151:
    h1: 489.679 MB (U(h) = 0.0564214*h^0.472936)
    h2: 448.321 MB (U(h) = 0.0683582*h^0.432993)
Reading from 6180: heap size 469 MB, throughput 0.960348
Reading from 6179: heap size 395 MB, throughput 0.980818
Reading from 6180: heap size 460 MB, throughput 0.968824
Reading from 6179: heap size 401 MB, throughput 0.970129
Reading from 6179: heap size 402 MB, throughput 0.944666
Reading from 6179: heap size 409 MB, throughput 0.917656
Reading from 6179: heap size 413 MB, throughput 0.900377
Reading from 6179: heap size 425 MB, throughput 0.976605
Reading from 6180: heap size 431 MB, throughput 0.985903
Reading from 6179: heap size 425 MB, throughput 0.982891
Reading from 6180: heap size 466 MB, throughput 0.988438
Reading from 6179: heap size 429 MB, throughput 0.982741
Reading from 6180: heap size 434 MB, throughput 0.989562
Reading from 6179: heap size 432 MB, throughput 0.98679
Reading from 6180: heap size 463 MB, throughput 0.988546
Reading from 6179: heap size 433 MB, throughput 0.985085
Numeric result:
Recommendation: 2 clients, utility 1.03719:
    h1: 565.66 MB (U(h) = 0.0355692*h^0.550352)
    h2: 372.34 MB (U(h) = 0.104365*h^0.362262)
Recommendation: 2 clients, utility 1.03719:
    h1: 565.661 MB (U(h) = 0.0355692*h^0.550352)
    h2: 372.339 MB (U(h) = 0.104365*h^0.362262)
Reading from 6180: heap size 436 MB, throughput 0.987928
Reading from 6179: heap size 436 MB, throughput 0.985942
Reading from 6180: heap size 447 MB, throughput 0.988359
Reading from 6179: heap size 435 MB, throughput 0.987183
Reading from 6180: heap size 433 MB, throughput 0.987027
Reading from 6179: heap size 438 MB, throughput 0.985446
Reading from 6180: heap size 439 MB, throughput 0.983825
Reading from 6179: heap size 439 MB, throughput 0.984182
Reading from 6180: heap size 430 MB, throughput 0.987953
Reading from 6179: heap size 440 MB, throughput 0.986996
Reading from 6180: heap size 436 MB, throughput 0.98604
Numeric result:
Recommendation: 2 clients, utility 1.03679:
    h1: 546.852 MB (U(h) = 0.0355443*h^0.550476)
    h2: 391.148 MB (U(h) = 0.0865138*h^0.393739)
Recommendation: 2 clients, utility 1.03679:
    h1: 546.853 MB (U(h) = 0.0355443*h^0.550476)
    h2: 391.147 MB (U(h) = 0.0865138*h^0.393739)
Reading from 6180: heap size 410 MB, throughput 0.974245
Reading from 6180: heap size 432 MB, throughput 0.94307
Reading from 6180: heap size 424 MB, throughput 0.912984
Client 6180 died
Clients: 1
Reading from 6179: heap size 444 MB, throughput 0.975122
Reading from 6179: heap size 470 MB, throughput 0.98286
Reading from 6179: heap size 472 MB, throughput 0.984982
Client 6179 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
