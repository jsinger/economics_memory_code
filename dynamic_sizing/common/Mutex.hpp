#ifndef MUTEX_HPP
#define MUTEX_HPP

#include <pthread.h>

#if defined(__clang__) && (!defined(SWIG))
#define THREAD_ANNOTATION_ATTRIBUTE__(x)   __attribute__((x))
#else
#define THREAD_ANNOTATION_ATTRIBUTE__(x)   // no-op
#endif

#define GUARDED_BY(x) THREAD_ANNOTATION_ATTRIBUTE__(guarded_by(x))
#define EXCLUSIVE_LOCKS_REQUIRED(...) THREAD_ANNOTATION_ATTRIBUTE__(exclusive_locks_required(__VA_ARGS__))
#define LOCKABLE THREAD_ANNOTATION_ATTRIBUTE__(lockable)
#define EXCLUSIVE_LOCK_FUNCTION(...) THREAD_ANNOTATION_ATTRIBUTE__(exclusive_lock_function(__VA_ARGS__))
#define UNLOCK_FUNCTION(...) THREAD_ANNOTATION_ATTRIBUTE__(unlock_function(__VA_ARGS__))

/*! \addtogroup common
  @{
*/

namespace economem
{
    //! Wrapper class around pthread_mutex_t so the clang thread-safety annotations can be used.
    /*! Compile-time thread-safety checks == awesome! See http://clang.llvm.org/docs/ThreadSafetyAnalysis.html for details. */
    class LOCKABLE Mutex
    {
    public:
        Mutex(bool recursive=false);
        ~Mutex();

        void lock() EXCLUSIVE_LOCK_FUNCTION();
        void unlock() UNLOCK_FUNCTION();
        void condWaitOnLock(pthread_cond_t* cond) EXCLUSIVE_LOCKS_REQUIRED(this);

    private:
        pthread_mutex_t m_Mutex;
    };
}

/*! @} */

#endif
