economemd
    total memory: 6175 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub13_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 29874: heap size 9 MB, throughput 0.983181
Clients: 1
Client 29874 has a minimum heap size of 12 MB
Reading from 29873: heap size 9 MB, throughput 0.987069
Clients: 2
Client 29873 has a minimum heap size of 1223 MB
Reading from 29874: heap size 9 MB, throughput 0.974591
Reading from 29873: heap size 9 MB, throughput 0.965579
Reading from 29874: heap size 11 MB, throughput 0.981784
Reading from 29873: heap size 11 MB, throughput 0.981829
Reading from 29874: heap size 11 MB, throughput 0.966006
Reading from 29873: heap size 11 MB, throughput 0.974826
Reading from 29874: heap size 15 MB, throughput 0.974987
Reading from 29873: heap size 15 MB, throughput 0.811587
Reading from 29874: heap size 15 MB, throughput 0.965794
Reading from 29873: heap size 18 MB, throughput 0.966467
Reading from 29873: heap size 23 MB, throughput 0.951763
Reading from 29873: heap size 27 MB, throughput 0.859563
Reading from 29874: heap size 24 MB, throughput 0.853494
Reading from 29873: heap size 27 MB, throughput 0.345641
Reading from 29873: heap size 37 MB, throughput 0.80207
Reading from 29873: heap size 42 MB, throughput 0.763642
Reading from 29873: heap size 43 MB, throughput 0.766872
Reading from 29874: heap size 29 MB, throughput 0.962422
Reading from 29873: heap size 49 MB, throughput 0.305022
Reading from 29873: heap size 62 MB, throughput 0.844085
Reading from 29873: heap size 70 MB, throughput 0.776017
Reading from 29874: heap size 34 MB, throughput 0.987327
Reading from 29873: heap size 71 MB, throughput 0.214854
Reading from 29874: heap size 38 MB, throughput 0.97899
Reading from 29873: heap size 102 MB, throughput 0.852651
Reading from 29874: heap size 40 MB, throughput 0.983616
Reading from 29873: heap size 103 MB, throughput 0.784983
Reading from 29874: heap size 43 MB, throughput 0.983583
Reading from 29874: heap size 46 MB, throughput 0.975439
Reading from 29873: heap size 105 MB, throughput 0.185527
Reading from 29874: heap size 47 MB, throughput 0.986517
Reading from 29873: heap size 135 MB, throughput 0.775927
Reading from 29874: heap size 51 MB, throughput 0.965169
Reading from 29873: heap size 139 MB, throughput 0.770894
Reading from 29874: heap size 51 MB, throughput 0.949511
Reading from 29874: heap size 57 MB, throughput 0.835281
Reading from 29874: heap size 63 MB, throughput 0.984566
Reading from 29873: heap size 143 MB, throughput 0.153539
Reading from 29873: heap size 182 MB, throughput 0.649462
Reading from 29874: heap size 68 MB, throughput 0.957135
Reading from 29873: heap size 185 MB, throughput 0.739916
Reading from 29873: heap size 187 MB, throughput 0.592992
Reading from 29873: heap size 192 MB, throughput 0.701854
Reading from 29874: heap size 68 MB, throughput 0.996179
Reading from 29873: heap size 199 MB, throughput 0.76097
Reading from 29873: heap size 205 MB, throughput 0.686663
Reading from 29873: heap size 208 MB, throughput 0.678285
Reading from 29873: heap size 218 MB, throughput 0.607329
Reading from 29873: heap size 223 MB, throughput 0.611848
Reading from 29874: heap size 74 MB, throughput 0.997217
Reading from 29873: heap size 235 MB, throughput 0.521722
Reading from 29874: heap size 75 MB, throughput 0.994709
Reading from 29873: heap size 242 MB, throughput 0.112709
Reading from 29873: heap size 286 MB, throughput 0.539866
Reading from 29873: heap size 295 MB, throughput 0.517223
Reading from 29874: heap size 80 MB, throughput 0.995468
Reading from 29873: heap size 298 MB, throughput 0.113318
Reading from 29873: heap size 340 MB, throughput 0.474492
Reading from 29874: heap size 81 MB, throughput 0.996055
Reading from 29873: heap size 268 MB, throughput 0.584172
Reading from 29873: heap size 331 MB, throughput 0.515697
Reading from 29874: heap size 84 MB, throughput 0.99528
Reading from 29873: heap size 335 MB, throughput 0.129422
Reading from 29873: heap size 385 MB, throughput 0.511681
Reading from 29873: heap size 329 MB, throughput 0.729711
Reading from 29874: heap size 86 MB, throughput 0.99622
Reading from 29873: heap size 381 MB, throughput 0.722183
Reading from 29873: heap size 383 MB, throughput 0.673785
Reading from 29873: heap size 384 MB, throughput 0.612333
Reading from 29873: heap size 390 MB, throughput 0.603822
Reading from 29874: heap size 89 MB, throughput 0.997244
Reading from 29873: heap size 395 MB, throughput 0.627557
Reading from 29873: heap size 403 MB, throughput 0.608676
Reading from 29873: heap size 407 MB, throughput 0.597373
Reading from 29873: heap size 416 MB, throughput 0.535585
Reading from 29874: heap size 89 MB, throughput 0.995828
Reading from 29873: heap size 420 MB, throughput 0.453823
Reading from 29874: heap size 91 MB, throughput 0.995074
Equal recommendation: 3087 MB each
Reading from 29873: heap size 428 MB, throughput 0.0820097
Reading from 29874: heap size 92 MB, throughput 0.969498
Reading from 29873: heap size 477 MB, throughput 0.339836
Reading from 29873: heap size 483 MB, throughput 0.413305
Reading from 29873: heap size 488 MB, throughput 0.387442
Reading from 29874: heap size 95 MB, throughput 0.991428
Reading from 29874: heap size 95 MB, throughput 0.989823
Reading from 29873: heap size 491 MB, throughput 0.0793947
Reading from 29873: heap size 546 MB, throughput 0.43425
Reading from 29873: heap size 463 MB, throughput 0.527957
Reading from 29874: heap size 99 MB, throughput 0.996668
Reading from 29874: heap size 99 MB, throughput 0.993478
Reading from 29873: heap size 532 MB, throughput 0.0983232
Reading from 29873: heap size 524 MB, throughput 0.444846
Reading from 29873: heap size 586 MB, throughput 0.540708
Reading from 29874: heap size 102 MB, throughput 0.996829
Reading from 29873: heap size 591 MB, throughput 0.574198
Reading from 29873: heap size 592 MB, throughput 0.605287
Reading from 29873: heap size 593 MB, throughput 0.592809
Reading from 29874: heap size 103 MB, throughput 0.996708
Reading from 29873: heap size 598 MB, throughput 0.579028
Reading from 29873: heap size 600 MB, throughput 0.602565
Reading from 29874: heap size 106 MB, throughput 0.996455
Reading from 29873: heap size 605 MB, throughput 0.106854
Reading from 29874: heap size 106 MB, throughput 0.997594
Reading from 29873: heap size 667 MB, throughput 0.479425
Reading from 29874: heap size 107 MB, throughput 0.997757
Reading from 29873: heap size 677 MB, throughput 0.862725
Reading from 29874: heap size 108 MB, throughput 0.997681
Reading from 29873: heap size 677 MB, throughput 0.844649
Reading from 29874: heap size 110 MB, throughput 0.997439
Reading from 29873: heap size 676 MB, throughput 0.864697
Reading from 29873: heap size 688 MB, throughput 0.48308
Reading from 29874: heap size 110 MB, throughput 0.997731
Reading from 29873: heap size 698 MB, throughput 0.646699
Reading from 29873: heap size 706 MB, throughput 0.468632
Reading from 29874: heap size 111 MB, throughput 0.997559
Reading from 29874: heap size 111 MB, throughput 0.996393
Equal recommendation: 3087 MB each
Reading from 29873: heap size 715 MB, throughput 0.0276639
Reading from 29874: heap size 112 MB, throughput 0.997896
Reading from 29873: heap size 778 MB, throughput 0.271414
Reading from 29873: heap size 768 MB, throughput 0.439777
Reading from 29874: heap size 112 MB, throughput 0.998034
Reading from 29873: heap size 775 MB, throughput 0.847569
Reading from 29873: heap size 765 MB, throughput 0.522931
Reading from 29874: heap size 114 MB, throughput 0.997457
Reading from 29874: heap size 114 MB, throughput 0.994964
Reading from 29873: heap size 771 MB, throughput 0.0421125
Reading from 29874: heap size 115 MB, throughput 0.99734
Reading from 29873: heap size 845 MB, throughput 0.460058
Reading from 29874: heap size 115 MB, throughput 0.99398
Reading from 29873: heap size 845 MB, throughput 0.81781
Reading from 29874: heap size 116 MB, throughput 0.988427
Reading from 29873: heap size 852 MB, throughput 0.809675
Reading from 29873: heap size 852 MB, throughput 0.662372
Reading from 29874: heap size 117 MB, throughput 0.99123
Reading from 29874: heap size 120 MB, throughput 0.993769
Reading from 29874: heap size 120 MB, throughput 0.996013
Reading from 29873: heap size 857 MB, throughput 0.275367
Reading from 29873: heap size 950 MB, throughput 0.590531
Reading from 29874: heap size 124 MB, throughput 0.99735
Reading from 29873: heap size 955 MB, throughput 0.934037
Reading from 29873: heap size 961 MB, throughput 0.824887
Reading from 29873: heap size 951 MB, throughput 0.854138
Reading from 29873: heap size 820 MB, throughput 0.852019
Reading from 29874: heap size 124 MB, throughput 0.996842
Reading from 29873: heap size 943 MB, throughput 0.8626
Reading from 29873: heap size 825 MB, throughput 0.859315
Reading from 29873: heap size 936 MB, throughput 0.862401
Reading from 29873: heap size 942 MB, throughput 0.875882
Reading from 29874: heap size 127 MB, throughput 0.996289
Reading from 29873: heap size 933 MB, throughput 0.881712
Reading from 29873: heap size 938 MB, throughput 0.862535
Reading from 29873: heap size 929 MB, throughput 0.887745
Reading from 29874: heap size 127 MB, throughput 0.997077
Reading from 29874: heap size 129 MB, throughput 0.997056
Reading from 29873: heap size 934 MB, throughput 0.98491
Equal recommendation: 3087 MB each
Reading from 29874: heap size 129 MB, throughput 0.996785
Reading from 29873: heap size 926 MB, throughput 0.938375
Reading from 29873: heap size 860 MB, throughput 0.785832
Reading from 29874: heap size 131 MB, throughput 0.99706
Reading from 29873: heap size 919 MB, throughput 0.82461
Reading from 29873: heap size 859 MB, throughput 0.835808
Reading from 29873: heap size 914 MB, throughput 0.840015
Reading from 29874: heap size 131 MB, throughput 0.996741
Reading from 29873: heap size 919 MB, throughput 0.8323
Reading from 29873: heap size 912 MB, throughput 0.847916
Reading from 29873: heap size 916 MB, throughput 0.846447
Reading from 29873: heap size 912 MB, throughput 0.924881
Reading from 29874: heap size 133 MB, throughput 0.994131
Reading from 29873: heap size 916 MB, throughput 0.886866
Reading from 29873: heap size 920 MB, throughput 0.920811
Reading from 29874: heap size 133 MB, throughput 0.996516
Reading from 29873: heap size 922 MB, throughput 0.79735
Reading from 29873: heap size 927 MB, throughput 0.718698
Reading from 29874: heap size 134 MB, throughput 0.996299
Reading from 29874: heap size 135 MB, throughput 0.996544
Reading from 29873: heap size 927 MB, throughput 0.0643774
Reading from 29874: heap size 137 MB, throughput 0.997869
Reading from 29873: heap size 1065 MB, throughput 0.560837
Reading from 29873: heap size 1069 MB, throughput 0.750803
Reading from 29873: heap size 1070 MB, throughput 0.774554
Reading from 29873: heap size 1073 MB, throughput 0.752064
Reading from 29874: heap size 137 MB, throughput 0.997966
Reading from 29873: heap size 1078 MB, throughput 0.757531
Reading from 29873: heap size 1080 MB, throughput 0.74592
Reading from 29873: heap size 1094 MB, throughput 0.735759
Reading from 29873: heap size 1094 MB, throughput 0.79591
Reading from 29874: heap size 139 MB, throughput 0.998543
Reading from 29874: heap size 139 MB, throughput 0.99854
Reading from 29874: heap size 140 MB, throughput 0.998819
Equal recommendation: 3087 MB each
Reading from 29873: heap size 1107 MB, throughput 0.97711
Reading from 29874: heap size 140 MB, throughput 0.998242
Reading from 29874: heap size 142 MB, throughput 0.998709
Reading from 29874: heap size 142 MB, throughput 0.996703
Reading from 29874: heap size 143 MB, throughput 0.987709
Reading from 29874: heap size 143 MB, throughput 0.988526
Reading from 29873: heap size 1111 MB, throughput 0.967324
Reading from 29874: heap size 146 MB, throughput 0.9969
Reading from 29874: heap size 146 MB, throughput 0.996662
Reading from 29874: heap size 149 MB, throughput 0.997727
Reading from 29874: heap size 149 MB, throughput 0.997186
Reading from 29873: heap size 1125 MB, throughput 0.978348
Reading from 29874: heap size 151 MB, throughput 0.997567
Reading from 29874: heap size 152 MB, throughput 0.996333
Reading from 29874: heap size 153 MB, throughput 0.997422
Reading from 29874: heap size 154 MB, throughput 0.997195
Equal recommendation: 3087 MB each
Reading from 29873: heap size 1127 MB, throughput 0.979589
Reading from 29874: heap size 155 MB, throughput 0.995347
Reading from 29874: heap size 156 MB, throughput 0.997132
Reading from 29874: heap size 158 MB, throughput 0.996667
Reading from 29873: heap size 1129 MB, throughput 0.98016
Reading from 29874: heap size 158 MB, throughput 0.996203
Reading from 29874: heap size 160 MB, throughput 0.996798
Reading from 29874: heap size 160 MB, throughput 0.996097
Reading from 29874: heap size 163 MB, throughput 0.997796
Reading from 29873: heap size 1133 MB, throughput 0.978518
Reading from 29874: heap size 163 MB, throughput 0.92511
Reading from 29874: heap size 168 MB, throughput 0.998843
Reading from 29874: heap size 168 MB, throughput 0.997816
Equal recommendation: 3087 MB each
Reading from 29874: heap size 171 MB, throughput 0.992784
Reading from 29873: heap size 1127 MB, throughput 0.973012
Reading from 29874: heap size 171 MB, throughput 0.99228
Reading from 29874: heap size 174 MB, throughput 0.995262
Reading from 29874: heap size 175 MB, throughput 0.997489
Reading from 29874: heap size 178 MB, throughput 0.997948
Reading from 29873: heap size 1132 MB, throughput 0.979743
Reading from 29874: heap size 179 MB, throughput 0.997419
Reading from 29874: heap size 181 MB, throughput 0.995462
Reading from 29874: heap size 182 MB, throughput 0.997496
Reading from 29873: heap size 1134 MB, throughput 0.972855
Reading from 29874: heap size 185 MB, throughput 0.997782
Reading from 29874: heap size 185 MB, throughput 0.996676
Equal recommendation: 3087 MB each
Reading from 29874: heap size 188 MB, throughput 0.998036
Reading from 29873: heap size 1135 MB, throughput 0.978586
Reading from 29874: heap size 188 MB, throughput 0.996798
Reading from 29874: heap size 192 MB, throughput 0.997448
Reading from 29874: heap size 192 MB, throughput 0.996913
Reading from 29874: heap size 195 MB, throughput 0.997815
Reading from 29873: heap size 1140 MB, throughput 0.98064
Reading from 29874: heap size 195 MB, throughput 0.997574
Reading from 29874: heap size 198 MB, throughput 0.995387
Reading from 29874: heap size 198 MB, throughput 0.990768
Reading from 29874: heap size 202 MB, throughput 0.995545
Reading from 29873: heap size 1142 MB, throughput 0.97729
Equal recommendation: 3087 MB each
Reading from 29874: heap size 202 MB, throughput 0.997256
Reading from 29874: heap size 207 MB, throughput 0.997695
Reading from 29874: heap size 207 MB, throughput 0.997882
Reading from 29873: heap size 1145 MB, throughput 0.978449
Reading from 29874: heap size 210 MB, throughput 0.997977
Reading from 29874: heap size 211 MB, throughput 0.997186
Reading from 29873: heap size 1149 MB, throughput 0.979272
Reading from 29874: heap size 214 MB, throughput 0.998401
Reading from 29874: heap size 214 MB, throughput 0.995746
Reading from 29874: heap size 218 MB, throughput 0.997679
Equal recommendation: 3087 MB each
Reading from 29873: heap size 1152 MB, throughput 0.974859
Reading from 29874: heap size 218 MB, throughput 0.996846
Reading from 29874: heap size 222 MB, throughput 0.99787
Reading from 29874: heap size 222 MB, throughput 0.997812
Reading from 29873: heap size 1157 MB, throughput 0.978337
Reading from 29874: heap size 225 MB, throughput 0.996736
Reading from 29874: heap size 225 MB, throughput 0.991305
Reading from 29874: heap size 229 MB, throughput 0.995101
Reading from 29874: heap size 229 MB, throughput 0.996644
Reading from 29873: heap size 1161 MB, throughput 0.977676
Reading from 29874: heap size 234 MB, throughput 0.998256
Equal recommendation: 3087 MB each
Reading from 29874: heap size 235 MB, throughput 0.99814
Reading from 29873: heap size 1166 MB, throughput 0.977212
Reading from 29874: heap size 239 MB, throughput 0.998305
Reading from 29874: heap size 239 MB, throughput 0.997742
Reading from 29874: heap size 243 MB, throughput 0.997942
Reading from 29873: heap size 1170 MB, throughput 0.979828
Reading from 29874: heap size 243 MB, throughput 0.997838
Reading from 29874: heap size 246 MB, throughput 0.998035
Reading from 29873: heap size 1172 MB, throughput 0.977576
Reading from 29874: heap size 247 MB, throughput 0.997984
Equal recommendation: 3087 MB each
Reading from 29874: heap size 250 MB, throughput 0.9967
Reading from 29874: heap size 250 MB, throughput 0.99678
Reading from 29874: heap size 254 MB, throughput 0.990248
Reading from 29873: heap size 1176 MB, throughput 0.97613
Reading from 29874: heap size 254 MB, throughput 0.997614
Reading from 29874: heap size 260 MB, throughput 0.997705
Reading from 29873: heap size 1177 MB, throughput 0.977658
Reading from 29874: heap size 261 MB, throughput 0.997562
Reading from 29874: heap size 265 MB, throughput 0.998261
Equal recommendation: 3087 MB each
Reading from 29873: heap size 1181 MB, throughput 0.97744
Reading from 29874: heap size 266 MB, throughput 0.99792
Reading from 29874: heap size 270 MB, throughput 0.996776
Reading from 29874: heap size 270 MB, throughput 0.995634
Reading from 29873: heap size 1181 MB, throughput 0.977977
Reading from 29874: heap size 274 MB, throughput 0.997854
Reading from 29874: heap size 274 MB, throughput 0.997674
Equal recommendation: 3087 MB each
Reading from 29874: heap size 279 MB, throughput 0.996785
Reading from 29874: heap size 279 MB, throughput 0.992249
Reading from 29873: heap size 1185 MB, throughput 0.667493
Reading from 29874: heap size 284 MB, throughput 0.997555
Reading from 29874: heap size 284 MB, throughput 0.998023
Reading from 29873: heap size 1281 MB, throughput 0.968073
Reading from 29874: heap size 289 MB, throughput 0.998201
Reading from 29874: heap size 290 MB, throughput 0.998
Reading from 29873: heap size 1276 MB, throughput 0.987379
Equal recommendation: 3087 MB each
Reading from 29874: heap size 295 MB, throughput 0.998574
Reading from 29874: heap size 295 MB, throughput 0.998373
Reading from 29873: heap size 1284 MB, throughput 0.989147
Reading from 29874: heap size 298 MB, throughput 0.998695
Reading from 29874: heap size 298 MB, throughput 0.99793
Reading from 29873: heap size 1291 MB, throughput 0.988497
Reading from 29874: heap size 302 MB, throughput 0.998248
Reading from 29874: heap size 302 MB, throughput 0.990903
Equal recommendation: 3087 MB each
Reading from 29874: heap size 304 MB, throughput 0.99764
Reading from 29873: heap size 1292 MB, throughput 0.98461
Reading from 29874: heap size 305 MB, throughput 0.998079
Reading from 29874: heap size 309 MB, throughput 0.99762
Reading from 29873: heap size 1286 MB, throughput 0.985674
Reading from 29874: heap size 310 MB, throughput 0.997825
Reading from 29874: heap size 315 MB, throughput 0.998397
Reading from 29873: heap size 1195 MB, throughput 0.986356
Equal recommendation: 3087 MB each
Reading from 29874: heap size 315 MB, throughput 0.998523
Reading from 29874: heap size 320 MB, throughput 0.997779
Reading from 29873: heap size 1277 MB, throughput 0.981547
Reading from 29874: heap size 320 MB, throughput 0.997901
Reading from 29874: heap size 324 MB, throughput 0.911148
Reading from 29873: heap size 1211 MB, throughput 0.984353
Reading from 29874: heap size 333 MB, throughput 0.998417
Reading from 29874: heap size 339 MB, throughput 0.999189
Equal recommendation: 3087 MB each
Reading from 29873: heap size 1269 MB, throughput 0.982262
Reading from 29874: heap size 339 MB, throughput 0.998563
Reading from 29874: heap size 345 MB, throughput 0.999315
Reading from 29873: heap size 1274 MB, throughput 0.983319
Reading from 29874: heap size 345 MB, throughput 0.999006
Reading from 29874: heap size 350 MB, throughput 0.998847
Reading from 29873: heap size 1275 MB, throughput 0.981557
Equal recommendation: 3087 MB each
Reading from 29874: heap size 350 MB, throughput 0.999062
Reading from 29874: heap size 353 MB, throughput 0.99882
Reading from 29874: heap size 353 MB, throughput 0.99318
Reading from 29873: heap size 1275 MB, throughput 0.980315
Reading from 29874: heap size 356 MB, throughput 0.998393
Reading from 29874: heap size 357 MB, throughput 0.998013
Reading from 29873: heap size 1277 MB, throughput 0.977514
Equal recommendation: 3087 MB each
Reading from 29874: heap size 363 MB, throughput 0.998096
Reading from 29873: heap size 1281 MB, throughput 0.975854
Reading from 29874: heap size 364 MB, throughput 0.997536
Reading from 29874: heap size 370 MB, throughput 0.997733
Reading from 29873: heap size 1283 MB, throughput 0.975709
Reading from 29874: heap size 370 MB, throughput 0.997903
Reading from 29874: heap size 376 MB, throughput 0.996953
Equal recommendation: 3087 MB each
Reading from 29873: heap size 1290 MB, throughput 0.975183
Reading from 29874: heap size 376 MB, throughput 0.989237
Reading from 29874: heap size 383 MB, throughput 0.998265
Reading from 29873: heap size 1295 MB, throughput 0.978161
Reading from 29874: heap size 384 MB, throughput 0.998118
Reading from 29874: heap size 392 MB, throughput 0.998304
Reading from 29873: heap size 1298 MB, throughput 0.978473
Equal recommendation: 3087 MB each
Reading from 29874: heap size 393 MB, throughput 0.998367
Reading from 29874: heap size 400 MB, throughput 0.998503
Reading from 29873: heap size 1303 MB, throughput 0.975491
Reading from 29874: heap size 401 MB, throughput 0.998231
Reading from 29874: heap size 406 MB, throughput 0.997376
Reading from 29874: heap size 407 MB, throughput 0.997363
Equal recommendation: 3087 MB each
Reading from 29874: heap size 415 MB, throughput 0.998542
Reading from 29874: heap size 415 MB, throughput 0.998389
Reading from 29874: heap size 421 MB, throughput 0.998497
Reading from 29874: heap size 421 MB, throughput 0.99818
Equal recommendation: 3087 MB each
Reading from 29874: heap size 426 MB, throughput 0.998577
Reading from 29873: heap size 1305 MB, throughput 0.878877
Reading from 29874: heap size 427 MB, throughput 0.996696
Reading from 29874: heap size 432 MB, throughput 0.997701
Reading from 29874: heap size 433 MB, throughput 0.99869
Equal recommendation: 3087 MB each
Reading from 29874: heap size 438 MB, throughput 0.998724
Reading from 29874: heap size 439 MB, throughput 0.998414
Reading from 29874: heap size 445 MB, throughput 0.998634
Reading from 29873: heap size 1402 MB, throughput 0.989829
Reading from 29874: heap size 445 MB, throughput 0.998246
Equal recommendation: 3087 MB each
Reading from 29874: heap size 451 MB, throughput 0.995269
Reading from 29874: heap size 451 MB, throughput 0.997989
Reading from 29873: heap size 1346 MB, throughput 0.988817
Reading from 29873: heap size 1421 MB, throughput 0.888268
Reading from 29873: heap size 1431 MB, throughput 0.757407
Reading from 29873: heap size 1420 MB, throughput 0.657588
Reading from 29873: heap size 1454 MB, throughput 0.662656
Reading from 29874: heap size 459 MB, throughput 0.9983
Reading from 29873: heap size 1476 MB, throughput 0.686474
Reading from 29873: heap size 1490 MB, throughput 0.674408
Reading from 29873: heap size 1516 MB, throughput 0.7073
Reading from 29873: heap size 1522 MB, throughput 0.708932
Reading from 29873: heap size 1551 MB, throughput 0.750453
Reading from 29874: heap size 460 MB, throughput 0.997427
Reading from 29873: heap size 1552 MB, throughput 0.652389
Equal recommendation: 3087 MB each
Reading from 29874: heap size 468 MB, throughput 0.998595
Reading from 29873: heap size 1472 MB, throughput 0.996499
Reading from 29874: heap size 468 MB, throughput 0.99813
Reading from 29873: heap size 1478 MB, throughput 0.994913
Client 29874 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 29873: heap size 1512 MB, throughput 0.993223
Reading from 29873: heap size 1517 MB, throughput 0.992187
Recommendation: one client; give it all the memory
Reading from 29873: heap size 1520 MB, throughput 0.991395
Reading from 29873: heap size 1529 MB, throughput 0.990504
Recommendation: one client; give it all the memory
Reading from 29873: heap size 1510 MB, throughput 0.985067
Reading from 29873: heap size 1299 MB, throughput 0.989124
Reading from 29873: heap size 1492 MB, throughput 0.988449
Recommendation: one client; give it all the memory
Reading from 29873: heap size 1328 MB, throughput 0.986998
Reading from 29873: heap size 1470 MB, throughput 0.986618
Recommendation: one client; give it all the memory
Reading from 29873: heap size 1356 MB, throughput 0.985509
Reading from 29873: heap size 1470 MB, throughput 0.984353
Recommendation: one client; give it all the memory
Reading from 29873: heap size 1477 MB, throughput 0.984415
Recommendation: one client; give it all the memory
Reading from 29873: heap size 1481 MB, throughput 0.983343
Reading from 29873: heap size 1481 MB, throughput 0.982024
Recommendation: one client; give it all the memory
Reading from 29873: heap size 1489 MB, throughput 0.981633
Reading from 29873: heap size 1492 MB, throughput 0.980571
Recommendation: one client; give it all the memory
Reading from 29873: heap size 1500 MB, throughput 0.98146
Reading from 29873: heap size 1501 MB, throughput 0.981757
Recommendation: one client; give it all the memory
Reading from 29873: heap size 1510 MB, throughput 0.981639
Reading from 29873: heap size 1510 MB, throughput 0.979752
Recommendation: one client; give it all the memory
Reading from 29873: heap size 1518 MB, throughput 0.980665
Reading from 29873: heap size 1519 MB, throughput 0.980738
Recommendation: one client; give it all the memory
Reading from 29873: heap size 1527 MB, throughput 0.980891
Reading from 29873: heap size 1527 MB, throughput 0.979229
Recommendation: one client; give it all the memory
Reading from 29873: heap size 1534 MB, throughput 0.980393
Recommendation: one client; give it all the memory
Reading from 29873: heap size 1535 MB, throughput 0.981092
Reading from 29873: heap size 1542 MB, throughput 0.981531
Recommendation: one client; give it all the memory
Reading from 29873: heap size 1543 MB, throughput 0.981518
Recommendation: one client; give it all the memory
Reading from 29873: heap size 1550 MB, throughput 0.836076
Recommendation: one client; give it all the memory
Reading from 29873: heap size 1698 MB, throughput 0.992905
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 29873: heap size 1698 MB, throughput 0.996068
Reading from 29873: heap size 1702 MB, throughput 0.903753
Reading from 29873: heap size 1690 MB, throughput 0.83122
Reading from 29873: heap size 1705 MB, throughput 0.806088
Reading from 29873: heap size 1739 MB, throughput 0.836726
Reading from 29873: heap size 1750 MB, throughput 0.878019
Client 29873 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
