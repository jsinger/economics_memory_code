economemd
    total memory: 4876 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub8_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run09_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 20734: heap size 9 MB, throughput 0.988993
Clients: 1
Client 20734 has a minimum heap size of 8 MB
Reading from 20733: heap size 9 MB, throughput 0.99003
Clients: 2
Client 20733 has a minimum heap size of 1211 MB
Reading from 20733: heap size 9 MB, throughput 0.982289
Reading from 20734: heap size 9 MB, throughput 0.982478
Reading from 20733: heap size 9 MB, throughput 0.959826
Reading from 20734: heap size 11 MB, throughput 0.939915
Reading from 20734: heap size 11 MB, throughput 0.946494
Reading from 20733: heap size 9 MB, throughput 0.963427
Reading from 20733: heap size 11 MB, throughput 0.984859
Reading from 20734: heap size 15 MB, throughput 0.876876
Reading from 20734: heap size 20 MB, throughput 0.984276
Reading from 20733: heap size 11 MB, throughput 0.982876
Reading from 20734: heap size 24 MB, throughput 0.978807
Reading from 20734: heap size 29 MB, throughput 0.963374
Reading from 20733: heap size 17 MB, throughput 0.898936
Reading from 20734: heap size 32 MB, throughput 0.970128
Reading from 20734: heap size 32 MB, throughput 0.956868
Reading from 20734: heap size 36 MB, throughput 0.966575
Reading from 20734: heap size 36 MB, throughput 0.969677
Reading from 20733: heap size 17 MB, throughput 0.654126
Reading from 20734: heap size 42 MB, throughput 0.968195
Reading from 20734: heap size 42 MB, throughput 0.969597
Reading from 20733: heap size 30 MB, throughput 0.973691
Reading from 20734: heap size 49 MB, throughput 0.987205
Reading from 20733: heap size 31 MB, throughput 0.96293
Reading from 20734: heap size 49 MB, throughput 0.983332
Reading from 20734: heap size 58 MB, throughput 0.987112
Reading from 20733: heap size 34 MB, throughput 0.358226
Reading from 20734: heap size 58 MB, throughput 0.983708
Reading from 20733: heap size 47 MB, throughput 0.720445
Reading from 20734: heap size 63 MB, throughput 0.986569
Reading from 20733: heap size 51 MB, throughput 0.764474
Reading from 20734: heap size 64 MB, throughput 0.985019
Reading from 20734: heap size 69 MB, throughput 0.988586
Reading from 20734: heap size 70 MB, throughput 0.982072
Reading from 20733: heap size 51 MB, throughput 0.465847
Reading from 20734: heap size 76 MB, throughput 0.99365
Reading from 20733: heap size 71 MB, throughput 0.85683
Reading from 20733: heap size 72 MB, throughput 0.680079
Reading from 20734: heap size 76 MB, throughput 0.990236
Reading from 20734: heap size 83 MB, throughput 0.984373
Reading from 20733: heap size 78 MB, throughput 0.196598
Reading from 20734: heap size 83 MB, throughput 0.990331
Reading from 20733: heap size 98 MB, throughput 0.718135
Reading from 20734: heap size 88 MB, throughput 0.993036
Reading from 20733: heap size 104 MB, throughput 0.762697
Reading from 20733: heap size 106 MB, throughput 0.712977
Reading from 20734: heap size 88 MB, throughput 0.992829
Reading from 20734: heap size 93 MB, throughput 0.985425
Reading from 20734: heap size 93 MB, throughput 0.991021
Reading from 20733: heap size 109 MB, throughput 0.186507
Reading from 20734: heap size 99 MB, throughput 0.993595
Reading from 20733: heap size 136 MB, throughput 0.663774
Reading from 20733: heap size 142 MB, throughput 0.673131
Reading from 20734: heap size 99 MB, throughput 0.993052
Reading from 20734: heap size 103 MB, throughput 0.995161
Reading from 20733: heap size 146 MB, throughput 0.20263
Reading from 20734: heap size 103 MB, throughput 0.993429
Reading from 20733: heap size 185 MB, throughput 0.569387
Reading from 20733: heap size 188 MB, throughput 0.569296
Reading from 20734: heap size 107 MB, throughput 0.994861
Reading from 20733: heap size 190 MB, throughput 0.636248
Reading from 20734: heap size 107 MB, throughput 0.989505
Reading from 20733: heap size 195 MB, throughput 0.624847
Reading from 20733: heap size 201 MB, throughput 0.693727
Reading from 20734: heap size 111 MB, throughput 0.99589
Reading from 20733: heap size 209 MB, throughput 0.566895
Reading from 20734: heap size 111 MB, throughput 0.994948
Reading from 20733: heap size 215 MB, throughput 0.586944
Reading from 20734: heap size 114 MB, throughput 0.992019
Reading from 20734: heap size 114 MB, throughput 0.99077
Reading from 20734: heap size 118 MB, throughput 0.98946
Reading from 20734: heap size 118 MB, throughput 0.995249
Reading from 20733: heap size 222 MB, throughput 0.11131
Reading from 20734: heap size 122 MB, throughput 0.977654
Reading from 20734: heap size 122 MB, throughput 0.995145
Reading from 20733: heap size 276 MB, throughput 0.157013
Reading from 20734: heap size 127 MB, throughput 0.994727
Reading from 20733: heap size 324 MB, throughput 0.615702
Reading from 20734: heap size 127 MB, throughput 0.993393
Reading from 20733: heap size 319 MB, throughput 0.709027
Reading from 20733: heap size 323 MB, throughput 0.741069
Reading from 20734: heap size 132 MB, throughput 0.995464
Reading from 20733: heap size 323 MB, throughput 0.65941
Reading from 20734: heap size 132 MB, throughput 0.991533
Reading from 20733: heap size 324 MB, throughput 0.626483
Reading from 20733: heap size 325 MB, throughput 0.603616
Reading from 20734: heap size 136 MB, throughput 0.996974
Reading from 20733: heap size 333 MB, throughput 0.56641
Reading from 20734: heap size 136 MB, throughput 0.994349
Reading from 20733: heap size 338 MB, throughput 0.544985
Reading from 20733: heap size 350 MB, throughput 0.533467
Reading from 20734: heap size 139 MB, throughput 0.99604
Reading from 20733: heap size 357 MB, throughput 0.43468
Reading from 20734: heap size 140 MB, throughput 0.991317
Reading from 20733: heap size 369 MB, throughput 0.452765
Reading from 20734: heap size 143 MB, throughput 0.994738
Reading from 20734: heap size 143 MB, throughput 0.995696
Reading from 20733: heap size 374 MB, throughput 0.0640185
Reading from 20734: heap size 146 MB, throughput 0.996064
Reading from 20733: heap size 428 MB, throughput 0.36543
Reading from 20733: heap size 428 MB, throughput 0.5773
Reading from 20734: heap size 147 MB, throughput 0.990008
Reading from 20734: heap size 150 MB, throughput 0.993599
Reading from 20734: heap size 150 MB, throughput 0.994548
Reading from 20733: heap size 433 MB, throughput 0.0707417
Reading from 20734: heap size 154 MB, throughput 0.995225
Reading from 20733: heap size 475 MB, throughput 0.432565
Reading from 20734: heap size 154 MB, throughput 0.99469
Reading from 20733: heap size 481 MB, throughput 0.575748
Reading from 20734: heap size 158 MB, throughput 0.996317
Reading from 20733: heap size 470 MB, throughput 0.545524
Reading from 20734: heap size 158 MB, throughput 0.992754
Reading from 20733: heap size 475 MB, throughput 0.557984
Reading from 20733: heap size 473 MB, throughput 0.531191
Reading from 20734: heap size 161 MB, throughput 0.996086
Reading from 20733: heap size 474 MB, throughput 0.50008
Equal recommendation: 2438 MB each
Reading from 20734: heap size 161 MB, throughput 0.992108
Reading from 20734: heap size 165 MB, throughput 0.997288
Reading from 20734: heap size 165 MB, throughput 0.994865
Reading from 20733: heap size 480 MB, throughput 0.0984813
Reading from 20734: heap size 168 MB, throughput 0.995863
Reading from 20733: heap size 540 MB, throughput 0.45624
Reading from 20734: heap size 168 MB, throughput 0.991771
Reading from 20733: heap size 543 MB, throughput 0.542382
Reading from 20734: heap size 172 MB, throughput 0.992318
Reading from 20733: heap size 546 MB, throughput 0.465537
Reading from 20733: heap size 548 MB, throughput 0.495884
Reading from 20734: heap size 172 MB, throughput 0.995827
Reading from 20733: heap size 554 MB, throughput 0.508015
Reading from 20733: heap size 557 MB, throughput 0.512578
Reading from 20734: heap size 175 MB, throughput 0.996142
Reading from 20733: heap size 567 MB, throughput 0.476586
Reading from 20734: heap size 176 MB, throughput 0.995595
Reading from 20733: heap size 573 MB, throughput 0.474624
Reading from 20734: heap size 180 MB, throughput 0.994033
Reading from 20734: heap size 180 MB, throughput 0.994187
Reading from 20734: heap size 184 MB, throughput 0.99744
Reading from 20733: heap size 583 MB, throughput 0.0706768
Reading from 20734: heap size 184 MB, throughput 0.993747
Reading from 20733: heap size 653 MB, throughput 0.4295
Reading from 20733: heap size 658 MB, throughput 0.488087
Reading from 20734: heap size 188 MB, throughput 0.996701
Reading from 20733: heap size 657 MB, throughput 0.497076
Reading from 20734: heap size 188 MB, throughput 0.996907
Reading from 20734: heap size 191 MB, throughput 0.98211
Reading from 20733: heap size 659 MB, throughput 0.855752
Reading from 20734: heap size 192 MB, throughput 0.996487
Reading from 20734: heap size 197 MB, throughput 0.995368
Reading from 20733: heap size 659 MB, throughput 0.810133
Reading from 20734: heap size 197 MB, throughput 0.9951
Reading from 20734: heap size 202 MB, throughput 0.994048
Reading from 20734: heap size 202 MB, throughput 0.977277
Reading from 20733: heap size 665 MB, throughput 0.820275
Reading from 20734: heap size 207 MB, throughput 0.996302
Reading from 20734: heap size 207 MB, throughput 0.994779
Reading from 20734: heap size 213 MB, throughput 0.996739
Reading from 20734: heap size 214 MB, throughput 0.99715
Reading from 20733: heap size 666 MB, throughput 0.0813437
Reading from 20733: heap size 748 MB, throughput 0.293647
Reading from 20734: heap size 219 MB, throughput 0.997195
Reading from 20733: heap size 761 MB, throughput 0.526964
Reading from 20733: heap size 762 MB, throughput 0.269133
Reading from 20734: heap size 219 MB, throughput 0.997678
Reading from 20734: heap size 223 MB, throughput 0.996986
Equal recommendation: 2438 MB each
Reading from 20733: heap size 753 MB, throughput 0.0395506
Reading from 20734: heap size 224 MB, throughput 0.996529
Reading from 20733: heap size 737 MB, throughput 0.252887
Reading from 20733: heap size 830 MB, throughput 0.43064
Reading from 20734: heap size 228 MB, throughput 0.997921
Reading from 20733: heap size 831 MB, throughput 0.770394
Reading from 20734: heap size 228 MB, throughput 0.997055
Reading from 20733: heap size 831 MB, throughput 0.657783
Reading from 20733: heap size 833 MB, throughput 0.499514
Reading from 20734: heap size 216 MB, throughput 0.996629
Reading from 20733: heap size 831 MB, throughput 0.559852
Reading from 20734: heap size 206 MB, throughput 0.99557
Reading from 20734: heap size 197 MB, throughput 0.995566
Reading from 20734: heap size 188 MB, throughput 0.997327
Reading from 20734: heap size 179 MB, throughput 0.996045
Reading from 20733: heap size 833 MB, throughput 0.0777992
Reading from 20734: heap size 171 MB, throughput 0.996927
Reading from 20733: heap size 914 MB, throughput 0.625812
Reading from 20734: heap size 164 MB, throughput 0.996554
Reading from 20734: heap size 157 MB, throughput 0.995416
Reading from 20733: heap size 914 MB, throughput 0.880397
Reading from 20733: heap size 921 MB, throughput 0.722134
Reading from 20734: heap size 150 MB, throughput 0.996557
Reading from 20734: heap size 143 MB, throughput 0.993812
Reading from 20733: heap size 923 MB, throughput 0.935857
Reading from 20734: heap size 138 MB, throughput 0.99267
Reading from 20733: heap size 928 MB, throughput 0.886922
Reading from 20734: heap size 142 MB, throughput 0.945368
Reading from 20734: heap size 146 MB, throughput 0.995389
Reading from 20733: heap size 930 MB, throughput 0.901237
Reading from 20734: heap size 151 MB, throughput 0.995343
Reading from 20733: heap size 909 MB, throughput 0.737773
Reading from 20734: heap size 156 MB, throughput 0.995945
Reading from 20733: heap size 781 MB, throughput 0.787672
Reading from 20734: heap size 164 MB, throughput 0.996184
Reading from 20733: heap size 896 MB, throughput 0.765125
Reading from 20734: heap size 167 MB, throughput 0.995813
Reading from 20733: heap size 787 MB, throughput 0.789635
Reading from 20734: heap size 173 MB, throughput 0.996521
Reading from 20733: heap size 886 MB, throughput 0.829145
Reading from 20733: heap size 895 MB, throughput 0.815857
Reading from 20734: heap size 178 MB, throughput 0.997055
Reading from 20733: heap size 882 MB, throughput 0.825496
Reading from 20734: heap size 184 MB, throughput 0.996727
Reading from 20733: heap size 888 MB, throughput 0.803888
Reading from 20734: heap size 189 MB, throughput 0.996333
Reading from 20733: heap size 877 MB, throughput 0.825128
Reading from 20734: heap size 196 MB, throughput 0.996472
Reading from 20733: heap size 883 MB, throughput 0.833627
Reading from 20734: heap size 200 MB, throughput 0.995792
Reading from 20734: heap size 206 MB, throughput 0.995618
Reading from 20734: heap size 212 MB, throughput 0.996553
Reading from 20734: heap size 214 MB, throughput 0.99596
Reading from 20734: heap size 219 MB, throughput 0.997925
Reading from 20733: heap size 876 MB, throughput 0.970831
Reading from 20734: heap size 219 MB, throughput 0.997483
Reading from 20734: heap size 224 MB, throughput 0.993099
Reading from 20733: heap size 880 MB, throughput 0.884701
Reading from 20733: heap size 879 MB, throughput 0.718913
Reading from 20734: heap size 224 MB, throughput 0.997595
Reading from 20733: heap size 880 MB, throughput 0.776845
Reading from 20733: heap size 882 MB, throughput 0.759052
Reading from 20734: heap size 230 MB, throughput 0.998095
Reading from 20733: heap size 883 MB, throughput 0.783075
Reading from 20733: heap size 885 MB, throughput 0.760447
Equal recommendation: 2438 MB each
Reading from 20734: heap size 231 MB, throughput 0.998006
Reading from 20733: heap size 887 MB, throughput 0.775815
Reading from 20734: heap size 236 MB, throughput 0.996953
Reading from 20733: heap size 891 MB, throughput 0.801449
Reading from 20734: heap size 236 MB, throughput 0.996885
Reading from 20733: heap size 893 MB, throughput 0.875788
Reading from 20734: heap size 242 MB, throughput 0.996275
Reading from 20734: heap size 242 MB, throughput 0.997158
Reading from 20733: heap size 896 MB, throughput 0.895089
Reading from 20733: heap size 899 MB, throughput 0.715099
Reading from 20734: heap size 248 MB, throughput 0.997325
Reading from 20734: heap size 248 MB, throughput 0.99643
Reading from 20734: heap size 254 MB, throughput 0.997671
Reading from 20734: heap size 254 MB, throughput 0.996551
Reading from 20734: heap size 261 MB, throughput 0.995573
Reading from 20734: heap size 261 MB, throughput 0.996007
Reading from 20733: heap size 905 MB, throughput 0.0748286
Reading from 20733: heap size 1003 MB, throughput 0.451745
Reading from 20734: heap size 269 MB, throughput 0.99661
Reading from 20733: heap size 1030 MB, throughput 0.63123
Reading from 20734: heap size 269 MB, throughput 0.994773
Reading from 20733: heap size 1036 MB, throughput 0.634691
Reading from 20733: heap size 1057 MB, throughput 0.660795
Reading from 20734: heap size 278 MB, throughput 0.996191
Reading from 20733: heap size 1057 MB, throughput 0.63884
Reading from 20733: heap size 1074 MB, throughput 0.647163
Reading from 20734: heap size 278 MB, throughput 0.995686
Reading from 20733: heap size 1075 MB, throughput 0.643103
Reading from 20734: heap size 287 MB, throughput 0.993883
Reading from 20733: heap size 1094 MB, throughput 0.655503
Reading from 20734: heap size 287 MB, throughput 0.99404
Reading from 20734: heap size 297 MB, throughput 0.993904
Reading from 20734: heap size 298 MB, throughput 0.996173
Reading from 20733: heap size 1096 MB, throughput 0.930186
Reading from 20734: heap size 309 MB, throughput 0.996563
Reading from 20734: heap size 309 MB, throughput 0.996909
Reading from 20734: heap size 318 MB, throughput 0.996814
Reading from 20734: heap size 319 MB, throughput 0.996959
Equal recommendation: 2438 MB each
Reading from 20733: heap size 1120 MB, throughput 0.9675
Reading from 20734: heap size 328 MB, throughput 0.99755
Reading from 20734: heap size 329 MB, throughput 0.996319
Reading from 20734: heap size 337 MB, throughput 0.997529
Reading from 20734: heap size 337 MB, throughput 0.997567
Reading from 20734: heap size 345 MB, throughput 0.99661
Reading from 20733: heap size 1122 MB, throughput 0.938594
Reading from 20734: heap size 346 MB, throughput 0.998236
Reading from 20734: heap size 353 MB, throughput 0.997782
Reading from 20734: heap size 354 MB, throughput 0.996076
Reading from 20734: heap size 361 MB, throughput 0.99691
Reading from 20733: heap size 1128 MB, throughput 0.940938
Reading from 20734: heap size 361 MB, throughput 0.997433
Reading from 20734: heap size 370 MB, throughput 0.996732
Reading from 20734: heap size 371 MB, throughput 0.996992
Reading from 20733: heap size 1133 MB, throughput 0.941725
Reading from 20734: heap size 380 MB, throughput 0.997634
Reading from 20734: heap size 380 MB, throughput 0.995901
Reading from 20734: heap size 389 MB, throughput 0.996639
Reading from 20734: heap size 389 MB, throughput 0.997202
Reading from 20733: heap size 1129 MB, throughput 0.946328
Reading from 20734: heap size 399 MB, throughput 0.997048
Reading from 20734: heap size 399 MB, throughput 0.997157
Equal recommendation: 2438 MB each
Reading from 20734: heap size 409 MB, throughput 0.997406
Reading from 20734: heap size 409 MB, throughput 0.987574
Reading from 20733: heap size 1137 MB, throughput 0.945515
Reading from 20734: heap size 419 MB, throughput 0.996179
Reading from 20734: heap size 419 MB, throughput 0.996903
Reading from 20734: heap size 433 MB, throughput 0.979664
Reading from 20733: heap size 1131 MB, throughput 0.954311
Reading from 20734: heap size 438 MB, throughput 0.99617
Reading from 20734: heap size 452 MB, throughput 0.997526
Reading from 20734: heap size 452 MB, throughput 0.996725
Reading from 20733: heap size 1137 MB, throughput 0.946777
Reading from 20734: heap size 469 MB, throughput 0.99746
Reading from 20734: heap size 469 MB, throughput 0.997065
Reading from 20734: heap size 486 MB, throughput 0.996571
Reading from 20734: heap size 486 MB, throughput 0.99716
Reading from 20733: heap size 1143 MB, throughput 0.938898
Reading from 20734: heap size 503 MB, throughput 0.997612
Reading from 20734: heap size 504 MB, throughput 0.99765
Equal recommendation: 2438 MB each
Reading from 20734: heap size 520 MB, throughput 0.996856
Reading from 20733: heap size 1144 MB, throughput 0.946667
Reading from 20734: heap size 521 MB, throughput 0.997145
Reading from 20734: heap size 538 MB, throughput 0.997761
Reading from 20734: heap size 538 MB, throughput 0.997513
Reading from 20733: heap size 1153 MB, throughput 0.943991
Reading from 20734: heap size 555 MB, throughput 0.997334
Reading from 20734: heap size 555 MB, throughput 0.99732
Reading from 20734: heap size 572 MB, throughput 0.997719
Reading from 20733: heap size 1155 MB, throughput 0.938695
Reading from 20734: heap size 573 MB, throughput 0.997946
Reading from 20734: heap size 588 MB, throughput 0.99795
Reading from 20733: heap size 1164 MB, throughput 0.943398
Reading from 20734: heap size 589 MB, throughput 0.998059
Reading from 20734: heap size 604 MB, throughput 0.998468
Equal recommendation: 2438 MB each
Reading from 20734: heap size 605 MB, throughput 0.998197
Reading from 20733: heap size 1168 MB, throughput 0.939161
Reading from 20734: heap size 620 MB, throughput 0.997895
Reading from 20734: heap size 620 MB, throughput 0.998376
Reading from 20734: heap size 634 MB, throughput 0.998502
Reading from 20733: heap size 1177 MB, throughput 0.952278
Reading from 20734: heap size 635 MB, throughput 0.998404
Reading from 20734: heap size 648 MB, throughput 0.998406
Reading from 20733: heap size 1182 MB, throughput 0.948542
Reading from 20734: heap size 649 MB, throughput 0.99856
Reading from 20734: heap size 662 MB, throughput 0.997996
Reading from 20734: heap size 663 MB, throughput 0.998306
Reading from 20733: heap size 1192 MB, throughput 0.941952
Reading from 20734: heap size 677 MB, throughput 0.998419
Equal recommendation: 2438 MB each
Reading from 20734: heap size 677 MB, throughput 0.998377
Reading from 20734: heap size 691 MB, throughput 0.998307
Reading from 20734: heap size 691 MB, throughput 0.99878
Reading from 20733: heap size 1197 MB, throughput 0.497717
Reading from 20734: heap size 705 MB, throughput 0.99836
Reading from 20734: heap size 706 MB, throughput 0.998521
Reading from 20733: heap size 1295 MB, throughput 0.923604
Reading from 20734: heap size 720 MB, throughput 0.998942
Reading from 20734: heap size 720 MB, throughput 0.99774
Reading from 20733: heap size 1298 MB, throughput 0.963711
Reading from 20734: heap size 733 MB, throughput 0.998783
Reading from 20734: heap size 733 MB, throughput 0.998718
Equal recommendation: 2438 MB each
Reading from 20734: heap size 747 MB, throughput 0.998733
Reading from 20733: heap size 1305 MB, throughput 0.962307
Reading from 20734: heap size 747 MB, throughput 0.998429
Reading from 20734: heap size 761 MB, throughput 0.998645
Reading from 20733: heap size 1307 MB, throughput 0.95578
Reading from 20734: heap size 761 MB, throughput 0.998318
Reading from 20734: heap size 775 MB, throughput 0.998926
Reading from 20734: heap size 775 MB, throughput 0.998455
Reading from 20733: heap size 1305 MB, throughput 0.957156
Reading from 20734: heap size 789 MB, throughput 0.998529
Reading from 20734: heap size 789 MB, throughput 0.998886
Reading from 20733: heap size 1309 MB, throughput 0.952393
Equal recommendation: 2438 MB each
Reading from 20734: heap size 803 MB, throughput 0.998896
Reading from 20734: heap size 804 MB, throughput 0.998657
Reading from 20733: heap size 1302 MB, throughput 0.969111
Reading from 20734: heap size 817 MB, throughput 0.998439
Reading from 20734: heap size 817 MB, throughput 0.998808
Reading from 20733: heap size 1308 MB, throughput 0.9589
Reading from 20734: heap size 831 MB, throughput 0.99893
Reading from 20734: heap size 831 MB, throughput 0.998305
Reading from 20734: heap size 845 MB, throughput 0.998724
Reading from 20733: heap size 1310 MB, throughput 0.953283
Reading from 20734: heap size 845 MB, throughput 0.998856
Equal recommendation: 2438 MB each
Reading from 20734: heap size 860 MB, throughput 0.998645
Reading from 20733: heap size 1312 MB, throughput 0.946859
Reading from 20734: heap size 860 MB, throughput 0.998859
Reading from 20734: heap size 875 MB, throughput 0.998627
Reading from 20733: heap size 1320 MB, throughput 0.952647
Reading from 20734: heap size 875 MB, throughput 0.998866
Reading from 20734: heap size 889 MB, throughput 0.998726
Reading from 20734: heap size 890 MB, throughput 0.998992
Reading from 20733: heap size 1322 MB, throughput 0.953587
Reading from 20734: heap size 904 MB, throughput 0.999057
Equal recommendation: 2438 MB each
Reading from 20734: heap size 904 MB, throughput 0.99882
Reading from 20733: heap size 1331 MB, throughput 0.951986
Reading from 20734: heap size 918 MB, throughput 0.99893
Reading from 20734: heap size 918 MB, throughput 0.998907
Reading from 20733: heap size 1336 MB, throughput 0.950009
Reading from 20734: heap size 932 MB, throughput 0.99893
Reading from 20734: heap size 932 MB, throughput 0.998688
Reading from 20733: heap size 1345 MB, throughput 0.949388
Reading from 20734: heap size 946 MB, throughput 0.999125
Reading from 20734: heap size 946 MB, throughput 0.998939
Equal recommendation: 2438 MB each
Reading from 20733: heap size 1353 MB, throughput 0.948749
Reading from 20734: heap size 960 MB, throughput 0.998934
Reading from 20734: heap size 960 MB, throughput 0.998894
Reading from 20734: heap size 974 MB, throughput 0.999163
Reading from 20733: heap size 1366 MB, throughput 0.941631
Reading from 20734: heap size 974 MB, throughput 0.998883
Reading from 20734: heap size 988 MB, throughput 0.999039
Reading from 20733: heap size 1373 MB, throughput 0.947356
Reading from 20734: heap size 988 MB, throughput 0.999033
Reading from 20734: heap size 1001 MB, throughput 0.999095
Equal recommendation: 2438 MB each
Reading from 20734: heap size 1001 MB, throughput 0.998754
Reading from 20734: heap size 1015 MB, throughput 0.999184
Reading from 20734: heap size 1015 MB, throughput 0.999034
Reading from 20734: heap size 1028 MB, throughput 0.998926
Reading from 20734: heap size 1028 MB, throughput 0.999254
Reading from 20734: heap size 1042 MB, throughput 0.998953
Equal recommendation: 2438 MB each
Reading from 20734: heap size 1042 MB, throughput 0.998928
Reading from 20734: heap size 1056 MB, throughput 0.999196
Reading from 20734: heap size 1056 MB, throughput 0.999058
Reading from 20734: heap size 1069 MB, throughput 0.998889
Reading from 20734: heap size 1069 MB, throughput 0.998955
Reading from 20734: heap size 1084 MB, throughput 0.999138
Equal recommendation: 2438 MB each
Reading from 20734: heap size 1084 MB, throughput 0.998876
Reading from 20734: heap size 1098 MB, throughput 0.999012
Reading from 20734: heap size 1098 MB, throughput 0.998578
Reading from 20734: heap size 1113 MB, throughput 0.999334
Reading from 20734: heap size 1113 MB, throughput 0.999186
Reading from 20733: heap size 1386 MB, throughput 0.959073
Reading from 20734: heap size 1128 MB, throughput 0.999021
Equal recommendation: 2438 MB each
Reading from 20734: heap size 1128 MB, throughput 0.998983
Reading from 20734: heap size 1143 MB, throughput 0.999088
Reading from 20734: heap size 1143 MB, throughput 0.999183
Reading from 20733: heap size 1466 MB, throughput 0.990827
Reading from 20734: heap size 1157 MB, throughput 0.998525
Reading from 20733: heap size 1493 MB, throughput 0.889962
Reading from 20733: heap size 1493 MB, throughput 0.777117
Reading from 20733: heap size 1508 MB, throughput 0.7917
Reading from 20733: heap size 1509 MB, throughput 0.778724
Reading from 20733: heap size 1539 MB, throughput 0.81557
Reading from 20734: heap size 1158 MB, throughput 0.999116
Equal recommendation: 2438 MB each
Reading from 20734: heap size 1174 MB, throughput 0.99884
Reading from 20733: heap size 1539 MB, throughput 0.978391
Reading from 20734: heap size 1174 MB, throughput 0.999161
Reading from 20734: heap size 1190 MB, throughput 0.999013
Reading from 20733: heap size 1569 MB, throughput 0.977324
Reading from 20734: heap size 1191 MB, throughput 0.999268
Reading from 20734: heap size 1206 MB, throughput 0.999116
Reading from 20733: heap size 1572 MB, throughput 0.975597
Reading from 20734: heap size 1206 MB, throughput 0.999175
Equal recommendation: 2438 MB each
Reading from 20734: heap size 1221 MB, throughput 0.999174
Reading from 20733: heap size 1586 MB, throughput 0.971377
Reading from 20734: heap size 1221 MB, throughput 0.999111
Reading from 20734: heap size 1236 MB, throughput 0.999398
Reading from 20733: heap size 1593 MB, throughput 0.974527
Reading from 20734: heap size 1236 MB, throughput 0.999232
Reading from 20734: heap size 1250 MB, throughput 0.999136
Equal recommendation: 2438 MB each
Reading from 20734: heap size 1250 MB, throughput 0.999298
Reading from 20733: heap size 1590 MB, throughput 0.972112
Reading from 20734: heap size 1264 MB, throughput 0.999088
Reading from 20734: heap size 1264 MB, throughput 0.999238
Reading from 20733: heap size 1600 MB, throughput 0.967024
Reading from 20734: heap size 1278 MB, throughput 0.99906
Reading from 20734: heap size 1278 MB, throughput 0.998969
Equal recommendation: 2438 MB each
Reading from 20733: heap size 1602 MB, throughput 0.967196
Reading from 20734: heap size 1293 MB, throughput 0.999272
Reading from 20734: heap size 1293 MB, throughput 0.999163
Reading from 20733: heap size 1607 MB, throughput 0.964824
Reading from 20734: heap size 1308 MB, throughput 0.999312
Reading from 20734: heap size 1308 MB, throughput 0.999285
Reading from 20733: heap size 1621 MB, throughput 0.96475
Reading from 20734: heap size 1322 MB, throughput 0.999066
Equal recommendation: 2438 MB each
Reading from 20734: heap size 1322 MB, throughput 0.999157
Reading from 20734: heap size 1337 MB, throughput 0.999101
Reading from 20733: heap size 1625 MB, throughput 0.963195
Reading from 20734: heap size 1337 MB, throughput 0.999259
Reading from 20734: heap size 1352 MB, throughput 0.999101
Reading from 20733: heap size 1636 MB, throughput 0.960927
Reading from 20734: heap size 1352 MB, throughput 0.999344
Equal recommendation: 2438 MB each
Reading from 20734: heap size 1367 MB, throughput 0.999263
Reading from 20733: heap size 1637 MB, throughput 0.959865
Reading from 20734: heap size 1367 MB, throughput 0.999344
Reading from 20734: heap size 1382 MB, throughput 0.999224
Reading from 20734: heap size 1382 MB, throughput 0.99899
Reading from 20733: heap size 1649 MB, throughput 0.96323
Equal recommendation: 2438 MB each
Reading from 20734: heap size 1396 MB, throughput 0.999344
Reading from 20734: heap size 1396 MB, throughput 0.999296
Reading from 20733: heap size 1658 MB, throughput 0.951688
Reading from 20734: heap size 1411 MB, throughput 0.999237
Reading from 20734: heap size 1411 MB, throughput 0.999271
Reading from 20733: heap size 1672 MB, throughput 0.951704
Reading from 20734: heap size 1426 MB, throughput 0.999449
Equal recommendation: 2438 MB each
Reading from 20734: heap size 1426 MB, throughput 0.999325
Reading from 20733: heap size 1687 MB, throughput 0.954857
Reading from 20734: heap size 1440 MB, throughput 0.999392
Reading from 20734: heap size 1440 MB, throughput 0.999214
Reading from 20733: heap size 1707 MB, throughput 0.956509
Reading from 20734: heap size 1454 MB, throughput 0.999258
Equal recommendation: 2438 MB each
Reading from 20734: heap size 1454 MB, throughput 0.999359
Reading from 20733: heap size 1719 MB, throughput 0.953622
Reading from 20734: heap size 1468 MB, throughput 0.999344
Reading from 20734: heap size 1468 MB, throughput 0.999282
Reading from 20734: heap size 1482 MB, throughput 0.99926
Reading from 20733: heap size 1740 MB, throughput 0.950829
Reading from 20734: heap size 1482 MB, throughput 0.999344
Equal recommendation: 2438 MB each
Reading from 20734: heap size 1496 MB, throughput 0.99934
Reading from 20733: heap size 1749 MB, throughput 0.731599
Reading from 20734: heap size 1496 MB, throughput 0.999377
Reading from 20734: heap size 1510 MB, throughput 0.999321
Reading from 20733: heap size 1897 MB, throughput 0.954752
Reading from 20734: heap size 1511 MB, throughput 0.999406
Equal recommendation: 2438 MB each
Reading from 20734: heap size 1524 MB, throughput 0.999353
Reading from 20734: heap size 1524 MB, throughput 0.999464
Reading from 20734: heap size 1538 MB, throughput 0.999214
Reading from 20734: heap size 1538 MB, throughput 0.999374
Equal recommendation: 2438 MB each
Reading from 20734: heap size 1552 MB, throughput 0.999341
Reading from 20734: heap size 1552 MB, throughput 0.999203
Reading from 20734: heap size 1567 MB, throughput 0.999144
Reading from 20734: heap size 1490 MB, throughput 0.999417
Equal recommendation: 2438 MB each
Reading from 20734: heap size 1415 MB, throughput 0.999468
Reading from 20734: heap size 1347 MB, throughput 0.999546
Reading from 20734: heap size 1281 MB, throughput 0.999033
Reading from 20734: heap size 1221 MB, throughput 0.999358
Reading from 20734: heap size 1162 MB, throughput 0.999217
Equal recommendation: 2438 MB each
Reading from 20734: heap size 1105 MB, throughput 0.999213
Reading from 20734: heap size 1049 MB, throughput 0.999162
Reading from 20734: heap size 999 MB, throughput 0.99918
Reading from 20733: heap size 1904 MB, throughput 0.994227
Client 20734 died
Clients: 1
Reading from 20733: heap size 1914 MB, throughput 0.982953
Reading from 20733: heap size 1928 MB, throughput 0.85269
Reading from 20733: heap size 1948 MB, throughput 0.869278
Client 20733 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
