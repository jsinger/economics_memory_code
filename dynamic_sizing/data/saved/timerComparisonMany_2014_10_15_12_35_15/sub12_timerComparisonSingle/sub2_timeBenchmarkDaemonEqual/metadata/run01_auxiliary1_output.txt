economemd
    total memory: 3669 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub12_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 5718: heap size 9 MB, throughput 0.988457
Clients: 1
Client 5718 has a minimum heap size of 12 MB
Reading from 5717: heap size 9 MB, throughput 0.992561
Clients: 2
Client 5717 has a minimum heap size of 1211 MB
Reading from 5717: heap size 9 MB, throughput 0.96538
Reading from 5718: heap size 9 MB, throughput 0.988288
Reading from 5717: heap size 9 MB, throughput 0.952674
Reading from 5717: heap size 9 MB, throughput 0.943908
Reading from 5718: heap size 11 MB, throughput 0.983838
Reading from 5717: heap size 11 MB, throughput 0.971479
Reading from 5718: heap size 11 MB, throughput 0.977636
Reading from 5717: heap size 11 MB, throughput 0.973558
Reading from 5718: heap size 15 MB, throughput 0.980955
Reading from 5717: heap size 17 MB, throughput 0.957143
Reading from 5717: heap size 17 MB, throughput 0.505028
Reading from 5718: heap size 15 MB, throughput 0.977114
Reading from 5717: heap size 30 MB, throughput 0.861293
Reading from 5717: heap size 31 MB, throughput 0.940754
Reading from 5717: heap size 35 MB, throughput 0.535582
Reading from 5718: heap size 24 MB, throughput 0.851395
Reading from 5717: heap size 48 MB, throughput 0.830261
Reading from 5717: heap size 52 MB, throughput 0.499133
Reading from 5718: heap size 30 MB, throughput 0.973698
Reading from 5717: heap size 66 MB, throughput 0.815008
Reading from 5717: heap size 72 MB, throughput 0.838031
Reading from 5718: heap size 34 MB, throughput 0.979495
Reading from 5717: heap size 74 MB, throughput 0.293143
Reading from 5717: heap size 105 MB, throughput 0.788429
Reading from 5718: heap size 38 MB, throughput 0.988632
Reading from 5718: heap size 39 MB, throughput 0.991861
Reading from 5717: heap size 106 MB, throughput 0.29188
Reading from 5717: heap size 140 MB, throughput 0.756625
Reading from 5718: heap size 43 MB, throughput 0.989927
Reading from 5717: heap size 140 MB, throughput 0.845781
Reading from 5718: heap size 45 MB, throughput 0.974606
Reading from 5717: heap size 142 MB, throughput 0.855643
Reading from 5717: heap size 146 MB, throughput 0.736059
Reading from 5718: heap size 45 MB, throughput 0.98698
Reading from 5718: heap size 48 MB, throughput 0.990557
Reading from 5718: heap size 49 MB, throughput 0.984684
Reading from 5717: heap size 148 MB, throughput 0.135411
Reading from 5717: heap size 188 MB, throughput 0.674511
Reading from 5718: heap size 52 MB, throughput 0.980799
Reading from 5717: heap size 192 MB, throughput 0.740431
Reading from 5718: heap size 52 MB, throughput 0.583348
Reading from 5717: heap size 196 MB, throughput 0.712398
Reading from 5718: heap size 59 MB, throughput 0.989238
Reading from 5717: heap size 206 MB, throughput 0.774461
Reading from 5718: heap size 59 MB, throughput 0.983813
Reading from 5717: heap size 208 MB, throughput 0.651944
Reading from 5718: heap size 65 MB, throughput 0.995569
Reading from 5718: heap size 66 MB, throughput 0.996384
Reading from 5717: heap size 211 MB, throughput 0.13153
Reading from 5717: heap size 265 MB, throughput 0.656277
Reading from 5717: heap size 269 MB, throughput 0.736274
Reading from 5717: heap size 274 MB, throughput 0.654172
Reading from 5718: heap size 70 MB, throughput 0.996304
Reading from 5717: heap size 278 MB, throughput 0.598544
Reading from 5717: heap size 288 MB, throughput 0.536105
Reading from 5717: heap size 292 MB, throughput 0.526241
Reading from 5717: heap size 304 MB, throughput 0.516573
Reading from 5718: heap size 71 MB, throughput 0.996417
Reading from 5718: heap size 74 MB, throughput 0.996385
Reading from 5717: heap size 311 MB, throughput 0.103791
Reading from 5717: heap size 365 MB, throughput 0.501084
Reading from 5718: heap size 75 MB, throughput 0.993916
Reading from 5717: heap size 360 MB, throughput 0.0882465
Reading from 5718: heap size 78 MB, throughput 0.996256
Reading from 5717: heap size 360 MB, throughput 0.545275
Reading from 5717: heap size 415 MB, throughput 0.619913
Reading from 5717: heap size 353 MB, throughput 0.703828
Reading from 5717: heap size 410 MB, throughput 0.688256
Reading from 5718: heap size 78 MB, throughput 0.996357
Reading from 5717: heap size 414 MB, throughput 0.604796
Reading from 5717: heap size 415 MB, throughput 0.65244
Reading from 5717: heap size 418 MB, throughput 0.587492
Reading from 5718: heap size 79 MB, throughput 0.984736
Reading from 5717: heap size 421 MB, throughput 0.584413
Reading from 5717: heap size 429 MB, throughput 0.543354
Reading from 5717: heap size 433 MB, throughput 0.426007
Reading from 5717: heap size 444 MB, throughput 0.464454
Reading from 5718: heap size 80 MB, throughput 0.993553
Reading from 5717: heap size 449 MB, throughput 0.377111
Reading from 5717: heap size 458 MB, throughput 0.433547
Reading from 5718: heap size 83 MB, throughput 0.988306
Equal recommendation: 1834 MB each
Reading from 5718: heap size 83 MB, throughput 0.990981
Reading from 5717: heap size 463 MB, throughput 0.0643911
Reading from 5717: heap size 524 MB, throughput 0.364253
Reading from 5717: heap size 518 MB, throughput 0.525829
Reading from 5718: heap size 86 MB, throughput 0.996576
Reading from 5717: heap size 452 MB, throughput 0.59071
Reading from 5718: heap size 86 MB, throughput 0.993384
Reading from 5717: heap size 509 MB, throughput 0.0882348
Reading from 5718: heap size 88 MB, throughput 0.993779
Reading from 5717: heap size 514 MB, throughput 0.505333
Reading from 5717: heap size 570 MB, throughput 0.54161
Reading from 5717: heap size 573 MB, throughput 0.523811
Reading from 5717: heap size 565 MB, throughput 0.640991
Reading from 5718: heap size 89 MB, throughput 0.994865
Reading from 5717: heap size 569 MB, throughput 0.651684
Reading from 5717: heap size 570 MB, throughput 0.609917
Reading from 5717: heap size 572 MB, throughput 0.596201
Reading from 5717: heap size 578 MB, throughput 0.503488
Reading from 5718: heap size 91 MB, throughput 0.996474
Reading from 5718: heap size 91 MB, throughput 0.995725
Reading from 5717: heap size 584 MB, throughput 0.104429
Reading from 5718: heap size 93 MB, throughput 0.997588
Reading from 5717: heap size 651 MB, throughput 0.444804
Reading from 5717: heap size 658 MB, throughput 0.457
Reading from 5718: heap size 93 MB, throughput 0.997722
Reading from 5717: heap size 662 MB, throughput 0.847579
Reading from 5718: heap size 95 MB, throughput 0.997424
Reading from 5717: heap size 663 MB, throughput 0.829901
Reading from 5718: heap size 95 MB, throughput 0.997348
Reading from 5717: heap size 663 MB, throughput 0.847307
Reading from 5718: heap size 96 MB, throughput 0.997899
Reading from 5717: heap size 675 MB, throughput 0.756161
Reading from 5717: heap size 681 MB, throughput 0.431262
Reading from 5717: heap size 693 MB, throughput 0.654173
Reading from 5718: heap size 96 MB, throughput 0.997288
Reading from 5717: heap size 700 MB, throughput 0.517675
Reading from 5718: heap size 97 MB, throughput 0.997815
Reading from 5718: heap size 97 MB, throughput 0.997806
Reading from 5718: heap size 99 MB, throughput 0.997883
Reading from 5717: heap size 704 MB, throughput 0.0205783
Reading from 5717: heap size 769 MB, throughput 0.245728
Reading from 5717: heap size 688 MB, throughput 0.326916
Reading from 5717: heap size 759 MB, throughput 0.391378
Reading from 5718: heap size 99 MB, throughput 0.997431
Equal recommendation: 1834 MB each
Reading from 5718: heap size 100 MB, throughput 0.997079
Reading from 5717: heap size 764 MB, throughput 0.0489293
Reading from 5718: heap size 100 MB, throughput 0.997144
Reading from 5717: heap size 848 MB, throughput 0.81538
Reading from 5717: heap size 850 MB, throughput 0.655874
Reading from 5718: heap size 100 MB, throughput 0.99808
Reading from 5717: heap size 856 MB, throughput 0.705605
Reading from 5717: heap size 857 MB, throughput 0.671369
Reading from 5717: heap size 858 MB, throughput 0.818491
Reading from 5718: heap size 101 MB, throughput 0.997394
Reading from 5718: heap size 102 MB, throughput 0.997343
Reading from 5717: heap size 860 MB, throughput 0.187759
Reading from 5717: heap size 959 MB, throughput 0.597178
Reading from 5717: heap size 961 MB, throughput 0.961967
Reading from 5718: heap size 102 MB, throughput 0.998002
Reading from 5717: heap size 965 MB, throughput 0.956382
Reading from 5717: heap size 774 MB, throughput 0.896603
Reading from 5717: heap size 956 MB, throughput 0.866376
Reading from 5718: heap size 102 MB, throughput 0.995109
Reading from 5718: heap size 103 MB, throughput 0.988815
Reading from 5717: heap size 774 MB, throughput 0.847641
Reading from 5718: heap size 104 MB, throughput 0.991482
Reading from 5717: heap size 933 MB, throughput 0.675599
Reading from 5717: heap size 824 MB, throughput 0.737244
Reading from 5718: heap size 105 MB, throughput 0.990495
Reading from 5717: heap size 918 MB, throughput 0.697174
Reading from 5717: heap size 829 MB, throughput 0.731914
Reading from 5717: heap size 908 MB, throughput 0.782383
Reading from 5717: heap size 832 MB, throughput 0.788907
Reading from 5718: heap size 108 MB, throughput 0.996091
Reading from 5717: heap size 900 MB, throughput 0.75909
Reading from 5717: heap size 907 MB, throughput 0.806852
Reading from 5717: heap size 895 MB, throughput 0.840704
Reading from 5717: heap size 901 MB, throughput 0.839842
Reading from 5718: heap size 108 MB, throughput 0.995789
Reading from 5717: heap size 893 MB, throughput 0.803821
Reading from 5718: heap size 111 MB, throughput 0.996503
Reading from 5717: heap size 898 MB, throughput 0.97992
Reading from 5718: heap size 111 MB, throughput 0.996056
Reading from 5718: heap size 113 MB, throughput 0.996726
Reading from 5717: heap size 898 MB, throughput 0.964229
Reading from 5717: heap size 900 MB, throughput 0.80696
Reading from 5718: heap size 113 MB, throughput 0.99627
Reading from 5717: heap size 906 MB, throughput 0.812497
Reading from 5717: heap size 909 MB, throughput 0.816398
Reading from 5717: heap size 916 MB, throughput 0.817661
Reading from 5717: heap size 917 MB, throughput 0.735314
Reading from 5718: heap size 115 MB, throughput 0.99703
Reading from 5717: heap size 924 MB, throughput 0.828595
Reading from 5717: heap size 925 MB, throughput 0.789137
Reading from 5717: heap size 932 MB, throughput 0.834361
Equal recommendation: 1834 MB each
Reading from 5718: heap size 115 MB, throughput 0.993509
Reading from 5717: heap size 932 MB, throughput 0.913947
Reading from 5717: heap size 935 MB, throughput 0.892991
Reading from 5718: heap size 117 MB, throughput 0.976829
Reading from 5717: heap size 937 MB, throughput 0.911061
Reading from 5717: heap size 937 MB, throughput 0.808578
Reading from 5717: heap size 940 MB, throughput 0.727735
Reading from 5718: heap size 117 MB, throughput 0.98791
Reading from 5718: heap size 121 MB, throughput 0.993669
Reading from 5718: heap size 121 MB, throughput 0.995262
Reading from 5717: heap size 925 MB, throughput 0.0566928
Reading from 5717: heap size 1050 MB, throughput 0.575315
Reading from 5717: heap size 1048 MB, throughput 0.811745
Reading from 5718: heap size 124 MB, throughput 0.997126
Reading from 5717: heap size 1051 MB, throughput 0.799148
Reading from 5717: heap size 1051 MB, throughput 0.793889
Reading from 5717: heap size 1054 MB, throughput 0.77869
Reading from 5717: heap size 1055 MB, throughput 0.780623
Reading from 5717: heap size 1058 MB, throughput 0.743106
Reading from 5718: heap size 125 MB, throughput 0.99728
Reading from 5717: heap size 1063 MB, throughput 0.771544
Reading from 5717: heap size 1065 MB, throughput 0.766741
Reading from 5717: heap size 1077 MB, throughput 0.828663
Reading from 5718: heap size 128 MB, throughput 0.997691
Reading from 5717: heap size 1077 MB, throughput 0.943994
Reading from 5718: heap size 128 MB, throughput 0.998198
Reading from 5718: heap size 129 MB, throughput 0.997556
Reading from 5718: heap size 130 MB, throughput 0.998208
Reading from 5717: heap size 1091 MB, throughput 0.96949
Reading from 5718: heap size 132 MB, throughput 0.998625
Reading from 5718: heap size 132 MB, throughput 0.998288
Reading from 5717: heap size 1094 MB, throughput 0.961007
Equal recommendation: 1834 MB each
Reading from 5718: heap size 133 MB, throughput 0.998368
Reading from 5718: heap size 133 MB, throughput 0.998172
Reading from 5717: heap size 1101 MB, throughput 0.965302
Reading from 5718: heap size 134 MB, throughput 0.998423
Reading from 5718: heap size 134 MB, throughput 0.996093
Reading from 5718: heap size 135 MB, throughput 0.992236
Reading from 5717: heap size 1103 MB, throughput 0.942692
Reading from 5718: heap size 135 MB, throughput 0.987547
Reading from 5718: heap size 137 MB, throughput 0.996538
Reading from 5717: heap size 1100 MB, throughput 0.962489
Reading from 5718: heap size 137 MB, throughput 0.997306
Reading from 5718: heap size 140 MB, throughput 0.997328
Reading from 5717: heap size 1104 MB, throughput 0.960488
Reading from 5718: heap size 140 MB, throughput 0.997215
Reading from 5718: heap size 143 MB, throughput 0.997462
Reading from 5717: heap size 1102 MB, throughput 0.960264
Reading from 5718: heap size 143 MB, throughput 0.997247
Reading from 5718: heap size 144 MB, throughput 0.997321
Reading from 5718: heap size 144 MB, throughput 0.996521
Reading from 5717: heap size 1105 MB, throughput 0.95872
Equal recommendation: 1834 MB each
Reading from 5718: heap size 146 MB, throughput 0.997222
Reading from 5718: heap size 146 MB, throughput 0.997104
Reading from 5717: heap size 1110 MB, throughput 0.963715
Reading from 5718: heap size 148 MB, throughput 0.997301
Reading from 5718: heap size 148 MB, throughput 0.996365
Reading from 5717: heap size 1110 MB, throughput 0.943719
Reading from 5718: heap size 150 MB, throughput 0.996776
Reading from 5718: heap size 150 MB, throughput 0.997624
Reading from 5717: heap size 1115 MB, throughput 0.962155
Reading from 5718: heap size 152 MB, throughput 0.917989
Reading from 5718: heap size 156 MB, throughput 0.998047
Reading from 5717: heap size 1117 MB, throughput 0.96217
Reading from 5718: heap size 158 MB, throughput 0.998816
Reading from 5718: heap size 158 MB, throughput 0.998244
Reading from 5717: heap size 1123 MB, throughput 0.960142
Reading from 5718: heap size 159 MB, throughput 0.998651
Equal recommendation: 1834 MB each
Reading from 5718: heap size 160 MB, throughput 0.994194
Reading from 5718: heap size 161 MB, throughput 0.991751
Reading from 5717: heap size 1126 MB, throughput 0.947831
Reading from 5718: heap size 162 MB, throughput 0.995198
Reading from 5718: heap size 166 MB, throughput 0.997669
Reading from 5717: heap size 1131 MB, throughput 0.961775
Reading from 5718: heap size 166 MB, throughput 0.997145
Reading from 5718: heap size 169 MB, throughput 0.997788
Reading from 5717: heap size 1135 MB, throughput 0.960554
Reading from 5718: heap size 169 MB, throughput 0.997458
Reading from 5718: heap size 172 MB, throughput 0.997534
Reading from 5717: heap size 1141 MB, throughput 0.960058
Reading from 5718: heap size 172 MB, throughput 0.997528
Reading from 5718: heap size 175 MB, throughput 0.996878
Reading from 5717: heap size 1145 MB, throughput 0.954808
Reading from 5718: heap size 175 MB, throughput 0.996883
Reading from 5717: heap size 1151 MB, throughput 0.961776
Equal recommendation: 1834 MB each
Reading from 5718: heap size 178 MB, throughput 0.997588
Reading from 5718: heap size 178 MB, throughput 0.997123
Reading from 5717: heap size 1153 MB, throughput 0.959241
Reading from 5718: heap size 181 MB, throughput 0.997539
Reading from 5718: heap size 181 MB, throughput 0.996865
Reading from 5717: heap size 1158 MB, throughput 0.95866
Reading from 5718: heap size 183 MB, throughput 0.997564
Reading from 5718: heap size 183 MB, throughput 0.998
Reading from 5717: heap size 1159 MB, throughput 0.960637
Reading from 5718: heap size 185 MB, throughput 0.998355
Reading from 5718: heap size 186 MB, throughput 0.993724
Reading from 5718: heap size 188 MB, throughput 0.990971
Reading from 5717: heap size 1165 MB, throughput 0.9601
Reading from 5718: heap size 188 MB, throughput 0.996622
Reading from 5718: heap size 193 MB, throughput 0.997793
Equal recommendation: 1834 MB each
Reading from 5717: heap size 1165 MB, throughput 0.958908
Reading from 5718: heap size 193 MB, throughput 0.997339
Reading from 5718: heap size 196 MB, throughput 0.997864
Reading from 5717: heap size 1170 MB, throughput 0.961259
Reading from 5718: heap size 197 MB, throughput 0.997744
Reading from 5718: heap size 200 MB, throughput 0.997891
Reading from 5717: heap size 1170 MB, throughput 0.962599
Reading from 5718: heap size 200 MB, throughput 0.997483
Reading from 5718: heap size 203 MB, throughput 0.997623
Reading from 5718: heap size 203 MB, throughput 0.997336
Reading from 5717: heap size 1175 MB, throughput 0.539867
Reading from 5718: heap size 206 MB, throughput 0.997649
Equal recommendation: 1834 MB each
Reading from 5718: heap size 206 MB, throughput 0.99701
Reading from 5717: heap size 1270 MB, throughput 0.987043
Reading from 5718: heap size 208 MB, throughput 0.997628
Reading from 5718: heap size 209 MB, throughput 0.997436
Reading from 5717: heap size 1265 MB, throughput 0.990155
Reading from 5718: heap size 212 MB, throughput 0.996521
Reading from 5718: heap size 212 MB, throughput 0.992028
Reading from 5717: heap size 1275 MB, throughput 0.987707
Reading from 5718: heap size 215 MB, throughput 0.997087
Reading from 5718: heap size 215 MB, throughput 0.997668
Reading from 5717: heap size 1283 MB, throughput 0.985666
Reading from 5718: heap size 220 MB, throughput 0.998077
Reading from 5718: heap size 220 MB, throughput 0.997588
Reading from 5717: heap size 1285 MB, throughput 0.982845
Equal recommendation: 1834 MB each
Reading from 5718: heap size 224 MB, throughput 0.998053
Reading from 5717: heap size 1281 MB, throughput 0.980911
Reading from 5718: heap size 224 MB, throughput 0.997627
Reading from 5718: heap size 227 MB, throughput 0.997864
Reading from 5717: heap size 1187 MB, throughput 0.975501
Reading from 5718: heap size 227 MB, throughput 0.997724
Reading from 5718: heap size 230 MB, throughput 0.997924
Reading from 5717: heap size 1271 MB, throughput 0.976022
Reading from 5718: heap size 230 MB, throughput 0.99747
Reading from 5717: heap size 1205 MB, throughput 0.974776
Reading from 5718: heap size 234 MB, throughput 0.997964
Reading from 5718: heap size 234 MB, throughput 0.997289
Equal recommendation: 1834 MB each
Reading from 5718: heap size 237 MB, throughput 0.992963
Reading from 5717: heap size 1266 MB, throughput 0.969241
Reading from 5718: heap size 237 MB, throughput 0.994733
Reading from 5718: heap size 242 MB, throughput 0.997936
Reading from 5717: heap size 1270 MB, throughput 0.970602
Reading from 5718: heap size 242 MB, throughput 0.997777
Reading from 5717: heap size 1271 MB, throughput 0.970272
Reading from 5718: heap size 246 MB, throughput 0.998232
Reading from 5718: heap size 247 MB, throughput 0.997962
Reading from 5717: heap size 1273 MB, throughput 0.967764
Reading from 5718: heap size 250 MB, throughput 0.998121
Equal recommendation: 1834 MB each
Reading from 5717: heap size 1275 MB, throughput 0.964746
Reading from 5718: heap size 250 MB, throughput 0.997875
Reading from 5718: heap size 254 MB, throughput 0.998014
Reading from 5717: heap size 1280 MB, throughput 0.961476
Reading from 5718: heap size 254 MB, throughput 0.997843
Reading from 5717: heap size 1284 MB, throughput 0.961169
Reading from 5718: heap size 257 MB, throughput 0.998134
Reading from 5718: heap size 257 MB, throughput 0.997887
Reading from 5718: heap size 260 MB, throughput 0.9932
Reading from 5717: heap size 1291 MB, throughput 0.94848
Reading from 5718: heap size 260 MB, throughput 0.995963
Reading from 5717: heap size 1299 MB, throughput 0.961612
Reading from 5718: heap size 266 MB, throughput 0.998145
Equal recommendation: 1834 MB each
Reading from 5718: heap size 266 MB, throughput 0.99774
Reading from 5717: heap size 1303 MB, throughput 0.958495
Reading from 5718: heap size 271 MB, throughput 0.998267
Reading from 5717: heap size 1310 MB, throughput 0.960475
Reading from 5718: heap size 271 MB, throughput 0.998176
Reading from 5718: heap size 275 MB, throughput 0.998199
Reading from 5717: heap size 1312 MB, throughput 0.962566
Reading from 5718: heap size 275 MB, throughput 0.998123
Reading from 5718: heap size 277 MB, throughput 0.998326
Equal recommendation: 1834 MB each
Reading from 5718: heap size 278 MB, throughput 0.997858
Reading from 5718: heap size 281 MB, throughput 0.998294
Reading from 5718: heap size 281 MB, throughput 0.991912
Reading from 5718: heap size 284 MB, throughput 0.997781
Reading from 5718: heap size 285 MB, throughput 0.998003
Reading from 5718: heap size 290 MB, throughput 0.998234
Equal recommendation: 1834 MB each
Reading from 5718: heap size 290 MB, throughput 0.997997
Reading from 5718: heap size 293 MB, throughput 0.998192
Reading from 5718: heap size 293 MB, throughput 0.998045
Reading from 5718: heap size 297 MB, throughput 0.997121
Reading from 5717: heap size 1320 MB, throughput 0.864991
Reading from 5718: heap size 297 MB, throughput 0.998168
Reading from 5718: heap size 301 MB, throughput 0.998297
Equal recommendation: 1834 MB each
Reading from 5718: heap size 301 MB, throughput 0.874039
Reading from 5718: heap size 313 MB, throughput 0.99869
Reading from 5718: heap size 315 MB, throughput 0.998863
Reading from 5718: heap size 320 MB, throughput 0.998974
Reading from 5718: heap size 321 MB, throughput 0.998858
Reading from 5718: heap size 325 MB, throughput 0.999023
Reading from 5717: heap size 1482 MB, throughput 0.987876
Equal recommendation: 1834 MB each
Reading from 5718: heap size 326 MB, throughput 0.998859
Reading from 5718: heap size 329 MB, throughput 0.998862
Reading from 5718: heap size 330 MB, throughput 0.998878
Reading from 5717: heap size 1521 MB, throughput 0.988194
Reading from 5718: heap size 332 MB, throughput 0.997039
Reading from 5717: heap size 1528 MB, throughput 0.867732
Reading from 5717: heap size 1528 MB, throughput 0.783795
Reading from 5717: heap size 1529 MB, throughput 0.710655
Reading from 5717: heap size 1555 MB, throughput 0.759036
Reading from 5718: heap size 333 MB, throughput 0.997262
Reading from 5717: heap size 1570 MB, throughput 0.742963
Reading from 5717: heap size 1598 MB, throughput 0.769216
Reading from 5717: heap size 1607 MB, throughput 0.764477
Equal recommendation: 1834 MB each
Reading from 5718: heap size 338 MB, throughput 0.998674
Reading from 5717: heap size 1638 MB, throughput 0.96063
Reading from 5718: heap size 338 MB, throughput 0.998278
Reading from 5717: heap size 1641 MB, throughput 0.978112
Reading from 5718: heap size 343 MB, throughput 0.998622
Reading from 5717: heap size 1650 MB, throughput 0.977043
Reading from 5718: heap size 343 MB, throughput 0.99832
Reading from 5718: heap size 347 MB, throughput 0.998471
Reading from 5717: heap size 1658 MB, throughput 0.97695
Equal recommendation: 1834 MB each
Reading from 5718: heap size 347 MB, throughput 0.998312
Reading from 5717: heap size 1654 MB, throughput 0.976913
Reading from 5718: heap size 351 MB, throughput 0.998658
Reading from 5718: heap size 351 MB, throughput 0.994474
Reading from 5718: heap size 354 MB, throughput 0.998164
Reading from 5717: heap size 1665 MB, throughput 0.975058
Reading from 5718: heap size 355 MB, throughput 0.99832
Reading from 5717: heap size 1655 MB, throughput 0.973837
Reading from 5718: heap size 361 MB, throughput 0.998489
Equal recommendation: 1834 MB each
Reading from 5718: heap size 361 MB, throughput 0.998223
Reading from 5717: heap size 1665 MB, throughput 0.980735
Reading from 5718: heap size 366 MB, throughput 0.998414
Reading from 5717: heap size 1671 MB, throughput 0.980107
Reading from 5718: heap size 366 MB, throughput 0.998006
Reading from 5718: heap size 371 MB, throughput 0.998595
Reading from 5717: heap size 1675 MB, throughput 0.978927
Reading from 5718: heap size 371 MB, throughput 0.996047
Equal recommendation: 1834 MB each
Reading from 5718: heap size 375 MB, throughput 0.997785
Reading from 5717: heap size 1666 MB, throughput 0.97839
Reading from 5718: heap size 376 MB, throughput 0.998377
Reading from 5718: heap size 381 MB, throughput 0.998601
Reading from 5717: heap size 1674 MB, throughput 0.973535
Reading from 5718: heap size 382 MB, throughput 0.998218
Equal recommendation: 1834 MB each
Reading from 5717: heap size 1661 MB, throughput 0.972986
Reading from 5718: heap size 387 MB, throughput 0.998462
Reading from 5718: heap size 387 MB, throughput 0.998175
Reading from 5717: heap size 1668 MB, throughput 0.972751
Reading from 5718: heap size 392 MB, throughput 0.998401
Reading from 5718: heap size 392 MB, throughput 0.994281
Reading from 5717: heap size 1674 MB, throughput 0.972028
Reading from 5718: heap size 395 MB, throughput 0.998571
Equal recommendation: 1834 MB each
Reading from 5718: heap size 397 MB, throughput 0.998143
Reading from 5717: heap size 1676 MB, throughput 0.967966
Reading from 5718: heap size 402 MB, throughput 0.998627
Reading from 5717: heap size 1685 MB, throughput 0.969292
Reading from 5718: heap size 403 MB, throughput 0.998239
Reading from 5718: heap size 409 MB, throughput 0.998515
Reading from 5717: heap size 1691 MB, throughput 0.967339
Equal recommendation: 1834 MB each
Reading from 5718: heap size 409 MB, throughput 0.998052
Reading from 5718: heap size 414 MB, throughput 0.996352
Reading from 5717: heap size 1702 MB, throughput 0.758216
Reading from 5718: heap size 414 MB, throughput 0.997762
Reading from 5718: heap size 421 MB, throughput 0.998546
Reading from 5717: heap size 1639 MB, throughput 0.994174
Equal recommendation: 1834 MB each
Reading from 5718: heap size 421 MB, throughput 0.998572
Reading from 5717: heap size 1643 MB, throughput 0.990422
Reading from 5718: heap size 427 MB, throughput 0.998513
Reading from 5718: heap size 427 MB, throughput 0.998408
Reading from 5717: heap size 1655 MB, throughput 0.989021
Reading from 5718: heap size 432 MB, throughput 0.998443
Reading from 5717: heap size 1670 MB, throughput 0.988655
Equal recommendation: 1834 MB each
Client 5718 died
Clients: 1
Reading from 5717: heap size 1670 MB, throughput 0.986302
Reading from 5717: heap size 1660 MB, throughput 0.984648
Recommendation: one client; give it all the memory
Reading from 5717: heap size 1519 MB, throughput 0.9825
Reading from 5717: heap size 1644 MB, throughput 0.98142
Reading from 5717: heap size 1655 MB, throughput 0.979429
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 5717: heap size 1650 MB, throughput 0.990409
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 5717: heap size 1652 MB, throughput 0.992494
Reading from 5717: heap size 1668 MB, throughput 0.877268
Reading from 5717: heap size 1695 MB, throughput 0.753935
Reading from 5717: heap size 1731 MB, throughput 0.781523
Reading from 5717: heap size 1760 MB, throughput 0.763587
Reading from 5717: heap size 1803 MB, throughput 0.818635
Client 5717 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
