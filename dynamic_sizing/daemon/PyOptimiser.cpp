#include <Python.h>
#include <Utilities.hpp>
#include "UtilityModel.hpp"
#include "PyOptimiser.hpp"

#define OPTIMISER_MODULE "optimiser"
#define OPTIMISER_FUNC "optimise"
#define THIS_MODULE "economemd"

static PyObject* s_PyModule = NULL;
static PyObject* s_PyFunc = NULL;
static PyObject* s_ThisModule = NULL;

static std::function<double(const std::vector<double>&)> s_CombineUtilities = nullptr;
static PyObject* pyCombineUtilities(PyObject* self, PyObject* args);

static PyMethodDef economem_methods[] = {
    {"combineUtilities", pyCombineUtilities, METH_VARARGS, "Combine utilities"},
    {NULL, NULL, 0, NULL} // Sentinel
};


void PyOptimiser::init()
{
    economem::info("PyOptimiser", "initialising...");

    Py_Initialize();
    if ((s_ThisModule = Py_InitModule3(THIS_MODULE, economem_methods, "Economem optimiser.")) == NULL)
    {
        PyErr_Print();
        economem::die("Failed to initialise python: couldn't create this module");
    }

    PyObject* name = PyString_FromString(OPTIMISER_MODULE);
    if (name == NULL)
    {
        PyErr_Print();
        economem::die("Failed to initialise python: couldn't allocate string");
    }

    if (PyRun_SimpleString("import sys; import os; import os.path; sys.path.append(os.path.dirname(os.path.abspath(os.readlink(\"/proc/self/exe\"))))") != 0)
    {
        PyErr_Print();
        economem::die("Failed to initialise python; couldn't read /proc/self/exe");
    }

    s_PyModule = PyImport_Import(name);
    Py_DECREF(name);

    if (s_PyModule == NULL)
    {
        PyErr_Print();
        economem::die("Failed to initialise python: couldn't load module '" OPTIMISER_MODULE "'");
    }

    s_PyFunc = PyObject_GetAttrString(s_PyModule, OPTIMISER_FUNC);
    if (s_PyFunc == NULL || !PyCallable_Check(s_PyFunc))
    {
        PyErr_Print();
        economem::die("Failed to initialise python: couldn't load function '" OPTIMISER_FUNC "'");
    }

    economem::info("PyOptimiser", "initialised");
}

void PyOptimiser::deinit()
{
    Py_XDECREF(s_PyFunc);
    s_PyFunc = NULL;
    Py_XDECREF(s_PyModule);
    s_PyModule = NULL;
    Py_XDECREF(s_ThisModule);
    s_ThisModule = NULL;
    Py_Finalize();
}

PartitionResult* PyOptimiser::partitionMemoryNumeric(unsigned long long totalMem, const std::vector<unsigned long long>& minHeaps, std::function<double(const std::vector<double>&)> combineUtilities)
{
    s_CombineUtilities = combineUtilities;

    PyObject* args = PyTuple_New(2);
    PyTuple_SetItem(args, 0, PyInt_FromLong(totalMem));
    PyObject* list = PyList_New(minHeaps.size());
    for (size_t i = 0; i < minHeaps.size(); ++i)
        PyList_SET_ITEM(list, i, PyInt_FromLong(minHeaps[i]));
    PyTuple_SetItem(args, 1, list);

    PyObject* retval = PyObject_CallObject(s_PyFunc, args);

    Py_DECREF(args);

    PartitionResult* r = NULL;

    if (retval != NULL)
    {
        if (!PyList_Check(retval) || PyList_Size(retval) != (int)minHeaps.size())
            economem::die("Python returned an invalid list");

        r = new PartitionResult(minHeaps.size());
        for (size_t i = 0; i < minHeaps.size(); ++i)
            r->heapSizes[i] = (double)PyFloat_AsDouble(PyList_GetItem(retval, i));
        r->utility = combineUtilities(r->heapSizes);
        Py_DECREF(retval);
    }
    else
    {
        PyErr_Print();
        economem::die("Failed to call python function");
    }

    s_CombineUtilities = nullptr;
    return r;
}

static PyObject* pyCombineUtilities(PyObject* self, PyObject* args)
{
    (void)self;

    PyObject* l;
    if (!PyArg_ParseTuple(args, "O!", &PyList_Type, &l))
        economem::die("Python passed an invalid object");

    std::vector<double> heapSizes(PyList_Size(l));
    for (size_t i = 0; i < heapSizes.size(); ++i)
        heapSizes[i] = (double)PyFloat_AsDouble(PyList_GetItem(l, i));

    return Py_BuildValue("f", s_CombineUtilities(heapSizes));
}
