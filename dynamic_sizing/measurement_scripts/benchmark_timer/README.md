Benchmark Timer {#benchmarkTimer}
============

Various scripts for measuring and comparing the throughput of benchmarks using different throughput models.

All experiments can be resumed by using the corresponding `resume` script.


Basic scripts
-------------

All of these scripts run many benchmark VMs concurrently, and measure how long it takes for them all to finish. All are resumed by `resume_benchmark_timer`.

 - `time_benchmark_unconstrained` -- no daemon and no constraints; the VMs can use as much memory as they want.
 - `time_benchmark_cgroup_simple` -- run the VMs in a cgroup to constrain their memory. All VMs have a maximum heap size the same as the cgroup size.
 - `time_benchmark_cgroup_fair` -- run the VMs in a cgroup to constrain their memory. Each of N VMs has a maximum heap size of the cgroup size over N.
 - `time_benchmark_daemon` -- run the VMs with a daemon managing their heap sizes.

Combined scripts
----------------

These scripts run a combination of the basic scripts described above, and compare them to see which method gives the best overall throughput.

 - `timer_comparison_single` -- compare the methods for a single benchmark combination. Resumed by `resume_timer_comparison_single`.
 - `timer_comparison_many` -- compare the methods for many benchmark combinations. Resumed by `resume_timer_comparison_many`.

Postprocessing scripts
----------------------

 - `plot_timer_comparison_many.py` -- view the results of a `timer_comparison_many` run graphically.

Auxiliary scripts
-----------------

 - `benchmark_iteration_length` -- measure how long a single iteration takes for various benchmarks. Resumed by `resume_iteration_length`.
 - `benchmark_iteration_length_all` -- same as above, but for all available benchmarks. Can specify benchmarks to ignore. Resumed by `resume_iteration_length`.
 - `iterations_in_time` -- calculate the number of iterations a benchmark should be run for to last for the given time, based on the output of `benchmark_iteration_length`.
 - `iterations_in_time_all` -- same as above, but for every benchmark in the input file.
