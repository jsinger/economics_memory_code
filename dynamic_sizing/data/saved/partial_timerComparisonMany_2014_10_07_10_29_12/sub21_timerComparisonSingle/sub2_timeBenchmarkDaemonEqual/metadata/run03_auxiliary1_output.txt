economemd
    total memory: 1656 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub21_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 23289: heap size 9 MB, throughput 0.991616
Clients: 1
Client 23289 has a minimum heap size of 276 MB
Reading from 23290: heap size 9 MB, throughput 0.985769
Clients: 2
Client 23290 has a minimum heap size of 276 MB
Reading from 23290: heap size 9 MB, throughput 0.97834
Reading from 23289: heap size 9 MB, throughput 0.982357
Reading from 23289: heap size 9 MB, throughput 0.965448
Reading from 23290: heap size 11 MB, throughput 0.970341
Reading from 23289: heap size 9 MB, throughput 0.93124
Reading from 23290: heap size 11 MB, throughput 0.984283
Reading from 23289: heap size 11 MB, throughput 0.985368
Reading from 23289: heap size 11 MB, throughput 0.973134
Reading from 23290: heap size 15 MB, throughput 0.841357
Reading from 23289: heap size 17 MB, throughput 0.841089
Reading from 23290: heap size 18 MB, throughput 0.898369
Reading from 23289: heap size 17 MB, throughput 0.595049
Reading from 23290: heap size 23 MB, throughput 0.873552
Reading from 23289: heap size 29 MB, throughput 0.932343
Reading from 23290: heap size 28 MB, throughput 0.81904
Reading from 23289: heap size 31 MB, throughput 0.806783
Reading from 23290: heap size 30 MB, throughput 0.497981
Reading from 23289: heap size 34 MB, throughput 0.906109
Reading from 23290: heap size 39 MB, throughput 0.738957
Reading from 23290: heap size 46 MB, throughput 0.900263
Reading from 23289: heap size 36 MB, throughput 0.279828
Reading from 23290: heap size 47 MB, throughput 0.44731
Reading from 23290: heap size 62 MB, throughput 0.77639
Reading from 23289: heap size 53 MB, throughput 0.466398
Reading from 23290: heap size 65 MB, throughput 0.760214
Reading from 23289: heap size 66 MB, throughput 0.873796
Reading from 23289: heap size 71 MB, throughput 0.805628
Reading from 23290: heap size 69 MB, throughput 0.257254
Reading from 23290: heap size 88 MB, throughput 0.681465
Reading from 23289: heap size 72 MB, throughput 0.320481
Reading from 23290: heap size 96 MB, throughput 0.757432
Reading from 23289: heap size 94 MB, throughput 0.876964
Reading from 23290: heap size 98 MB, throughput 0.738204
Reading from 23289: heap size 98 MB, throughput 0.775448
Reading from 23290: heap size 101 MB, throughput 0.157092
Reading from 23289: heap size 99 MB, throughput 0.187519
Reading from 23290: heap size 129 MB, throughput 0.702406
Reading from 23289: heap size 122 MB, throughput 0.651145
Reading from 23290: heap size 136 MB, throughput 0.728523
Reading from 23289: heap size 127 MB, throughput 0.827543
Reading from 23289: heap size 129 MB, throughput 0.687209
Reading from 23290: heap size 138 MB, throughput 0.677489
Reading from 23289: heap size 132 MB, throughput 0.149875
Reading from 23290: heap size 143 MB, throughput 0.114282
Reading from 23289: heap size 164 MB, throughput 0.688999
Reading from 23290: heap size 176 MB, throughput 0.588735
Reading from 23289: heap size 170 MB, throughput 0.687235
Reading from 23290: heap size 184 MB, throughput 0.598572
Reading from 23290: heap size 186 MB, throughput 0.713317
Reading from 23290: heap size 189 MB, throughput 0.637347
Reading from 23289: heap size 174 MB, throughput 0.15923
Reading from 23289: heap size 217 MB, throughput 0.596568
Reading from 23289: heap size 220 MB, throughput 0.66735
Reading from 23290: heap size 195 MB, throughput 0.455334
Reading from 23289: heap size 222 MB, throughput 0.949247
Reading from 23289: heap size 225 MB, throughput 0.789947
Reading from 23290: heap size 232 MB, throughput 0.815082
Reading from 23289: heap size 235 MB, throughput 0.814321
Reading from 23289: heap size 239 MB, throughput 0.734157
Reading from 23290: heap size 241 MB, throughput 0.904108
Reading from 23289: heap size 243 MB, throughput 0.766541
Reading from 23289: heap size 246 MB, throughput 0.535354
Reading from 23290: heap size 248 MB, throughput 0.754405
Reading from 23290: heap size 252 MB, throughput 0.59988
Reading from 23289: heap size 251 MB, throughput 0.751255
Reading from 23290: heap size 258 MB, throughput 0.766986
Reading from 23289: heap size 252 MB, throughput 0.757012
Reading from 23290: heap size 261 MB, throughput 0.62657
Reading from 23289: heap size 254 MB, throughput 0.70829
Reading from 23290: heap size 267 MB, throughput 0.708088
Reading from 23289: heap size 259 MB, throughput 0.606176
Reading from 23289: heap size 265 MB, throughput 0.562229
Reading from 23290: heap size 268 MB, throughput 0.66471
Reading from 23289: heap size 268 MB, throughput 0.59382
Reading from 23290: heap size 272 MB, throughput 0.496271
Reading from 23290: heap size 276 MB, throughput 0.618423
Reading from 23289: heap size 276 MB, throughput 0.918418
Reading from 23290: heap size 280 MB, throughput 0.102723
Reading from 23290: heap size 320 MB, throughput 0.613448
Reading from 23289: heap size 276 MB, throughput 0.203008
Reading from 23289: heap size 316 MB, throughput 0.479568
Reading from 23289: heap size 318 MB, throughput 0.712037
Reading from 23289: heap size 320 MB, throughput 0.849707
Reading from 23290: heap size 320 MB, throughput 0.490168
Reading from 23290: heap size 366 MB, throughput 0.664521
Reading from 23289: heap size 321 MB, throughput 0.923016
Reading from 23290: heap size 370 MB, throughput 0.86133
Reading from 23290: heap size 371 MB, throughput 0.858696
Reading from 23289: heap size 324 MB, throughput 0.928722
Reading from 23290: heap size 375 MB, throughput 0.740683
Reading from 23289: heap size 325 MB, throughput 0.754035
Reading from 23289: heap size 324 MB, throughput 0.695805
Reading from 23290: heap size 376 MB, throughput 0.854785
Reading from 23289: heap size 327 MB, throughput 0.625441
Reading from 23290: heap size 375 MB, throughput 0.819866
Reading from 23289: heap size 329 MB, throughput 0.662909
Reading from 23289: heap size 330 MB, throughput 0.61438
Reading from 23290: heap size 378 MB, throughput 0.672887
Reading from 23290: heap size 372 MB, throughput 0.606676
Equal recommendation: 828 MB each
Reading from 23290: heap size 376 MB, throughput 0.695847
Reading from 23290: heap size 376 MB, throughput 0.650216
Reading from 23290: heap size 379 MB, throughput 0.598158
Reading from 23289: heap size 336 MB, throughput 0.972572
Reading from 23290: heap size 384 MB, throughput 0.949703
Reading from 23289: heap size 337 MB, throughput 0.977022
Reading from 23290: heap size 385 MB, throughput 0.984112
Reading from 23289: heap size 341 MB, throughput 0.985103
Reading from 23290: heap size 385 MB, throughput 0.946754
Reading from 23290: heap size 389 MB, throughput 0.95682
Reading from 23289: heap size 343 MB, throughput 0.650237
Reading from 23290: heap size 387 MB, throughput 0.970596
Reading from 23289: heap size 389 MB, throughput 0.969671
Reading from 23290: heap size 391 MB, throughput 0.962049
Reading from 23289: heap size 390 MB, throughput 0.985094
Equal recommendation: 828 MB each
Reading from 23289: heap size 394 MB, throughput 0.984559
Reading from 23290: heap size 389 MB, throughput 0.980906
Reading from 23290: heap size 392 MB, throughput 0.976601
Reading from 23289: heap size 396 MB, throughput 0.984576
Reading from 23290: heap size 389 MB, throughput 0.968121
Reading from 23289: heap size 393 MB, throughput 0.986368
Reading from 23289: heap size 396 MB, throughput 0.986254
Reading from 23290: heap size 391 MB, throughput 0.977137
Reading from 23289: heap size 391 MB, throughput 0.981749
Reading from 23290: heap size 394 MB, throughput 0.972831
Reading from 23289: heap size 394 MB, throughput 0.977846
Reading from 23290: heap size 394 MB, throughput 0.972149
Reading from 23289: heap size 394 MB, throughput 0.97446
Reading from 23290: heap size 397 MB, throughput 0.97438
Equal recommendation: 828 MB each
Reading from 23289: heap size 395 MB, throughput 0.983759
Reading from 23290: heap size 399 MB, throughput 0.983556
Reading from 23290: heap size 401 MB, throughput 0.869403
Reading from 23290: heap size 402 MB, throughput 0.750761
Reading from 23289: heap size 394 MB, throughput 0.951605
Reading from 23290: heap size 408 MB, throughput 0.685007
Reading from 23289: heap size 396 MB, throughput 0.676262
Reading from 23290: heap size 411 MB, throughput 0.892357
Reading from 23289: heap size 392 MB, throughput 0.812692
Reading from 23289: heap size 399 MB, throughput 0.737705
Reading from 23289: heap size 408 MB, throughput 0.946521
Reading from 23290: heap size 419 MB, throughput 0.983517
Reading from 23289: heap size 411 MB, throughput 0.985452
Reading from 23290: heap size 421 MB, throughput 0.984926
Reading from 23289: heap size 411 MB, throughput 0.985873
Reading from 23290: heap size 422 MB, throughput 0.988175
Reading from 23289: heap size 414 MB, throughput 0.985669
Reading from 23290: heap size 425 MB, throughput 0.987299
Reading from 23289: heap size 413 MB, throughput 0.990019
Equal recommendation: 828 MB each
Reading from 23289: heap size 416 MB, throughput 0.983981
Reading from 23290: heap size 424 MB, throughput 0.985434
Reading from 23290: heap size 426 MB, throughput 0.988329
Reading from 23289: heap size 413 MB, throughput 0.986615
Reading from 23290: heap size 422 MB, throughput 0.985109
Reading from 23289: heap size 416 MB, throughput 0.987962
Reading from 23290: heap size 425 MB, throughput 0.984976
Reading from 23289: heap size 416 MB, throughput 0.982087
Reading from 23290: heap size 425 MB, throughput 0.980052
Reading from 23289: heap size 417 MB, throughput 0.984612
Equal recommendation: 828 MB each
Reading from 23290: heap size 426 MB, throughput 0.98664
Reading from 23289: heap size 419 MB, throughput 0.981976
Reading from 23290: heap size 426 MB, throughput 0.968682
Reading from 23290: heap size 427 MB, throughput 0.852689
Reading from 23290: heap size 430 MB, throughput 0.8723
Reading from 23290: heap size 430 MB, throughput 0.980647
Reading from 23289: heap size 420 MB, throughput 0.989777
Reading from 23289: heap size 422 MB, throughput 0.901457
Reading from 23289: heap size 422 MB, throughput 0.844197
Reading from 23289: heap size 426 MB, throughput 0.872421
Reading from 23289: heap size 428 MB, throughput 0.98815
Reading from 23290: heap size 435 MB, throughput 0.989322
Reading from 23289: heap size 435 MB, throughput 0.993916
Reading from 23290: heap size 437 MB, throughput 0.989374
Reading from 23289: heap size 436 MB, throughput 0.988709
Reading from 23290: heap size 439 MB, throughput 0.986492
Equal recommendation: 828 MB each
Reading from 23289: heap size 438 MB, throughput 0.987617
Reading from 23290: heap size 440 MB, throughput 0.988207
Reading from 23289: heap size 440 MB, throughput 0.984992
Reading from 23290: heap size 439 MB, throughput 0.985393
Reading from 23289: heap size 439 MB, throughput 0.986476
Reading from 23290: heap size 441 MB, throughput 0.984713
Reading from 23289: heap size 441 MB, throughput 0.983606
Reading from 23290: heap size 438 MB, throughput 0.982509
Reading from 23289: heap size 440 MB, throughput 0.985456
Equal recommendation: 828 MB each
Reading from 23290: heap size 440 MB, throughput 0.983883
Reading from 23289: heap size 441 MB, throughput 0.982191
Reading from 23290: heap size 442 MB, throughput 0.987675
Reading from 23290: heap size 443 MB, throughput 0.886859
Reading from 23290: heap size 444 MB, throughput 0.859915
Reading from 23290: heap size 445 MB, throughput 0.985631
Reading from 23289: heap size 443 MB, throughput 0.991243
Reading from 23289: heap size 444 MB, throughput 0.906458
Reading from 23289: heap size 444 MB, throughput 0.879032
Reading from 23289: heap size 446 MB, throughput 0.918993
Reading from 23290: heap size 452 MB, throughput 0.99314
Reading from 23289: heap size 454 MB, throughput 0.99004
Reading from 23290: heap size 453 MB, throughput 0.992429
Reading from 23289: heap size 454 MB, throughput 0.99095
Equal recommendation: 828 MB each
Reading from 23290: heap size 453 MB, throughput 0.990581
Reading from 23289: heap size 456 MB, throughput 0.992332
Reading from 23290: heap size 455 MB, throughput 0.988521
Reading from 23289: heap size 458 MB, throughput 0.991531
Reading from 23290: heap size 452 MB, throughput 0.987236
Reading from 23289: heap size 456 MB, throughput 0.986331
Reading from 23290: heap size 455 MB, throughput 0.983883
Reading from 23289: heap size 459 MB, throughput 0.987088
Reading from 23290: heap size 456 MB, throughput 0.989041
Equal recommendation: 828 MB each
Reading from 23289: heap size 459 MB, throughput 0.989326
Reading from 23290: heap size 456 MB, throughput 0.990678
Reading from 23289: heap size 460 MB, throughput 0.984023
Reading from 23290: heap size 456 MB, throughput 0.906775
Reading from 23290: heap size 458 MB, throughput 0.858044
Reading from 23290: heap size 463 MB, throughput 0.98452
Reading from 23289: heap size 461 MB, throughput 0.986282
Reading from 23289: heap size 463 MB, throughput 0.901022
Reading from 23289: heap size 462 MB, throughput 0.883105
Reading from 23289: heap size 464 MB, throughput 0.991314
Reading from 23290: heap size 465 MB, throughput 0.993298
Reading from 23289: heap size 472 MB, throughput 0.991457
Reading from 23290: heap size 468 MB, throughput 0.993542
Equal recommendation: 828 MB each
Reading from 23289: heap size 472 MB, throughput 0.989295
Reading from 23290: heap size 470 MB, throughput 0.989084
Reading from 23289: heap size 474 MB, throughput 0.988515
Reading from 23290: heap size 469 MB, throughput 0.989916
Reading from 23289: heap size 476 MB, throughput 0.987869
Reading from 23290: heap size 471 MB, throughput 0.867527
Reading from 23289: heap size 475 MB, throughput 0.987551
Equal recommendation: 828 MB each
Reading from 23290: heap size 495 MB, throughput 0.995145
Reading from 23289: heap size 476 MB, throughput 0.985572
Reading from 23290: heap size 496 MB, throughput 0.992162
Reading from 23290: heap size 498 MB, throughput 0.970726
Reading from 23289: heap size 478 MB, throughput 0.988473
Reading from 23290: heap size 500 MB, throughput 0.869454
Reading from 23289: heap size 479 MB, throughput 0.959638
Reading from 23289: heap size 479 MB, throughput 0.922045
Reading from 23290: heap size 504 MB, throughput 0.97873
Reading from 23289: heap size 481 MB, throughput 0.967572
Reading from 23290: heap size 506 MB, throughput 0.991438
Reading from 23289: heap size 488 MB, throughput 0.991266
Reading from 23290: heap size 507 MB, throughput 0.991147
Equal recommendation: 828 MB each
Reading from 23289: heap size 488 MB, throughput 0.989482
Reading from 23290: heap size 510 MB, throughput 0.99081
Reading from 23289: heap size 490 MB, throughput 0.991577
Reading from 23290: heap size 508 MB, throughput 0.987838
Reading from 23289: heap size 491 MB, throughput 0.988664
Reading from 23290: heap size 511 MB, throughput 0.986606
Reading from 23289: heap size 489 MB, throughput 0.987691
Equal recommendation: 828 MB each
Reading from 23290: heap size 513 MB, throughput 0.986248
Reading from 23289: heap size 491 MB, throughput 0.987876
Reading from 23290: heap size 514 MB, throughput 0.991185
Reading from 23290: heap size 514 MB, throughput 0.915225
Reading from 23290: heap size 517 MB, throughput 0.874166
Reading from 23289: heap size 494 MB, throughput 0.992012
Reading from 23289: heap size 494 MB, throughput 0.926628
Reading from 23289: heap size 495 MB, throughput 0.904627
Reading from 23290: heap size 523 MB, throughput 0.987414
Reading from 23289: heap size 497 MB, throughput 0.987627
Reading from 23290: heap size 526 MB, throughput 0.994209
Reading from 23289: heap size 503 MB, throughput 0.992149
Equal recommendation: 828 MB each
Reading from 23290: heap size 528 MB, throughput 0.991287
Reading from 23289: heap size 504 MB, throughput 0.992197
Reading from 23290: heap size 531 MB, throughput 0.992521
Reading from 23289: heap size 504 MB, throughput 0.991289
Reading from 23290: heap size 529 MB, throughput 0.989418
Reading from 23289: heap size 506 MB, throughput 0.989317
Reading from 23290: heap size 532 MB, throughput 0.987024
Equal recommendation: 828 MB each
Reading from 23289: heap size 505 MB, throughput 0.990547
Reading from 23290: heap size 535 MB, throughput 0.992359
Reading from 23290: heap size 536 MB, throughput 0.926595
Reading from 23290: heap size 536 MB, throughput 0.951127
Reading from 23289: heap size 507 MB, throughput 0.992261
Reading from 23289: heap size 508 MB, throughput 0.973336
Reading from 23289: heap size 509 MB, throughput 0.900716
Reading from 23290: heap size 539 MB, throughput 0.993815
Reading from 23289: heap size 513 MB, throughput 0.988969
Reading from 23290: heap size 541 MB, throughput 0.992184
Equal recommendation: 828 MB each
Reading from 23289: heap size 515 MB, throughput 0.992095
Reading from 23290: heap size 543 MB, throughput 0.989325
Reading from 23289: heap size 518 MB, throughput 0.99048
Reading from 23290: heap size 541 MB, throughput 0.989985
Reading from 23289: heap size 519 MB, throughput 0.990842
Reading from 23290: heap size 544 MB, throughput 0.988231
Reading from 23289: heap size 516 MB, throughput 0.987995
Equal recommendation: 828 MB each
Reading from 23290: heap size 547 MB, throughput 0.99255
Reading from 23290: heap size 548 MB, throughput 0.970422
Reading from 23290: heap size 547 MB, throughput 0.930287
Reading from 23289: heap size 519 MB, throughput 0.992322
Reading from 23290: heap size 550 MB, throughput 0.991887
Reading from 23289: heap size 520 MB, throughput 0.995603
Reading from 23289: heap size 521 MB, throughput 0.93602
Reading from 23289: heap size 521 MB, throughput 0.930857
Reading from 23290: heap size 556 MB, throughput 0.992937
Reading from 23289: heap size 523 MB, throughput 0.989328
Equal recommendation: 828 MB each
Reading from 23290: heap size 557 MB, throughput 0.991922
Reading from 23289: heap size 529 MB, throughput 0.991349
Reading from 23290: heap size 557 MB, throughput 0.990571
Reading from 23289: heap size 530 MB, throughput 0.990654
Reading from 23290: heap size 559 MB, throughput 0.989716
Reading from 23289: heap size 530 MB, throughput 0.989969
Equal recommendation: 828 MB each
Reading from 23290: heap size 561 MB, throughput 0.99525
Reading from 23289: heap size 532 MB, throughput 0.990777
Reading from 23290: heap size 561 MB, throughput 0.973287
Reading from 23290: heap size 561 MB, throughput 0.87282
Reading from 23290: heap size 563 MB, throughput 0.992617
Reading from 23289: heap size 533 MB, throughput 0.990139
Reading from 23289: heap size 533 MB, throughput 0.979184
Reading from 23289: heap size 539 MB, throughput 0.910141
Reading from 23290: heap size 571 MB, throughput 0.993592
Reading from 23289: heap size 541 MB, throughput 0.991551
Equal recommendation: 828 MB each
Reading from 23289: heap size 546 MB, throughput 0.993034
Reading from 23290: heap size 573 MB, throughput 0.991516
Reading from 23289: heap size 547 MB, throughput 0.99121
Reading from 23290: heap size 574 MB, throughput 0.991662
Reading from 23289: heap size 547 MB, throughput 0.988903
Reading from 23290: heap size 576 MB, throughput 0.988965
Equal recommendation: 828 MB each
Reading from 23289: heap size 549 MB, throughput 0.989139
Reading from 23290: heap size 579 MB, throughput 0.993873
Reading from 23290: heap size 579 MB, throughput 0.93801
Reading from 23290: heap size 580 MB, throughput 0.990153
Reading from 23289: heap size 549 MB, throughput 0.987497
Reading from 23289: heap size 550 MB, throughput 0.826365
Reading from 23289: heap size 591 MB, throughput 0.996363
Reading from 23290: heap size 583 MB, throughput 0.993451
Equal recommendation: 828 MB each
Reading from 23289: heap size 592 MB, throughput 0.996077
Reading from 23290: heap size 584 MB, throughput 0.994799
Reading from 23289: heap size 597 MB, throughput 0.994919
Reading from 23290: heap size 586 MB, throughput 0.990886
Reading from 23289: heap size 599 MB, throughput 0.993218
Reading from 23290: heap size 586 MB, throughput 0.990089
Equal recommendation: 828 MB each
Reading from 23289: heap size 597 MB, throughput 0.992094
Reading from 23290: heap size 587 MB, throughput 0.994683
Reading from 23290: heap size 589 MB, throughput 0.944933
Reading from 23289: heap size 566 MB, throughput 0.988462
Reading from 23290: heap size 590 MB, throughput 0.972574
Reading from 23289: heap size 598 MB, throughput 0.993101
Reading from 23289: heap size 598 MB, throughput 0.88456
Reading from 23290: heap size 599 MB, throughput 0.990045
Reading from 23289: heap size 594 MB, throughput 0.962809
Equal recommendation: 828 MB each
Reading from 23290: heap size 599 MB, throughput 0.992345
Reading from 23289: heap size 599 MB, throughput 0.993456
Reading from 23290: heap size 599 MB, throughput 0.991892
Reading from 23289: heap size 598 MB, throughput 0.994544
Reading from 23290: heap size 601 MB, throughput 0.990788
Reading from 23289: heap size 602 MB, throughput 0.989955
Equal recommendation: 828 MB each
Reading from 23289: heap size 607 MB, throughput 0.988201
Reading from 23290: heap size 603 MB, throughput 0.993731
Reading from 23290: heap size 604 MB, throughput 0.928019
Reading from 23290: heap size 603 MB, throughput 0.988753
Reading from 23289: heap size 608 MB, throughput 0.989072
Reading from 23289: heap size 612 MB, throughput 0.985868
Reading from 23289: heap size 615 MB, throughput 0.883156
Reading from 23290: heap size 606 MB, throughput 0.993865
Equal recommendation: 828 MB each
Reading from 23289: heap size 621 MB, throughput 0.989772
Reading from 23290: heap size 608 MB, throughput 0.992818
Reading from 23289: heap size 624 MB, throughput 0.989285
Reading from 23290: heap size 610 MB, throughput 0.991219
Reading from 23289: heap size 624 MB, throughput 0.990619
Equal recommendation: 828 MB each
Reading from 23290: heap size 612 MB, throughput 0.991846
Reading from 23289: heap size 627 MB, throughput 0.988994
Reading from 23290: heap size 612 MB, throughput 0.989068
Reading from 23290: heap size 615 MB, throughput 0.923626
Client 23290 died
Clients: 1
Reading from 23289: heap size 626 MB, throughput 0.992486
Reading from 23289: heap size 628 MB, throughput 0.991735
Reading from 23289: heap size 631 MB, throughput 0.941274
Client 23289 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
