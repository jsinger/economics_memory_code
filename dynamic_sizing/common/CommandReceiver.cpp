#include "CommandReceiver.hpp"
#include "Utilities.hpp"

economem::CommandReceiver::CommandReceiver(const char* daemonName) : RecvSock(economem::allocDaemonSocketName(economem::TYPE_COMMAND, daemonName)) {}
