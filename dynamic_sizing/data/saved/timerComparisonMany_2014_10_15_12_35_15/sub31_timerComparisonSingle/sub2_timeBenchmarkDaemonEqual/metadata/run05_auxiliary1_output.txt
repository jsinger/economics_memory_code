economemd
    total memory: 2470 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub31_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run05_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
TimerThread: starting
ReadingThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 10919: heap size 9 MB, throughput 0.9817
Clients: 1
Client 10919 has a minimum heap size of 12 MB
Reading from 10917: heap size 9 MB, throughput 0.99188
Clients: 2
Client 10917 has a minimum heap size of 1223 MB
Reading from 10919: heap size 9 MB, throughput 0.983261
Reading from 10917: heap size 9 MB, throughput 0.968802
Reading from 10917: heap size 9 MB, throughput 0.966279
Reading from 10917: heap size 9 MB, throughput 0.948178
Reading from 10919: heap size 11 MB, throughput 0.979541
Reading from 10917: heap size 11 MB, throughput 0.968933
Reading from 10919: heap size 11 MB, throughput 0.979352
Reading from 10917: heap size 11 MB, throughput 0.979117
Reading from 10919: heap size 15 MB, throughput 0.98273
Reading from 10917: heap size 17 MB, throughput 0.966365
Reading from 10917: heap size 17 MB, throughput 0.616191
Reading from 10919: heap size 15 MB, throughput 0.969023
Reading from 10917: heap size 30 MB, throughput 0.86447
Reading from 10917: heap size 31 MB, throughput 0.90889
Reading from 10919: heap size 24 MB, throughput 0.92578
Reading from 10917: heap size 35 MB, throughput 0.512248
Reading from 10917: heap size 45 MB, throughput 0.799177
Reading from 10917: heap size 50 MB, throughput 0.275043
Reading from 10919: heap size 29 MB, throughput 0.959251
Reading from 10917: heap size 65 MB, throughput 0.812511
Reading from 10917: heap size 68 MB, throughput 0.828182
Reading from 10919: heap size 37 MB, throughput 0.982351
Reading from 10917: heap size 70 MB, throughput 0.29745
Reading from 10917: heap size 105 MB, throughput 0.735177
Reading from 10917: heap size 105 MB, throughput 0.732651
Reading from 10919: heap size 44 MB, throughput 0.986038
Reading from 10917: heap size 106 MB, throughput 0.578372
Reading from 10917: heap size 110 MB, throughput 0.767232
Reading from 10919: heap size 45 MB, throughput 0.984156
Reading from 10919: heap size 45 MB, throughput 0.985961
Reading from 10919: heap size 52 MB, throughput 0.971847
Reading from 10917: heap size 113 MB, throughput 0.190114
Reading from 10917: heap size 150 MB, throughput 0.656616
Reading from 10917: heap size 155 MB, throughput 0.792154
Reading from 10919: heap size 52 MB, throughput 0.987141
Reading from 10917: heap size 157 MB, throughput 0.668843
Reading from 10919: heap size 61 MB, throughput 0.97617
Reading from 10917: heap size 166 MB, throughput 0.627724
Reading from 10917: heap size 167 MB, throughput 0.658218
Reading from 10919: heap size 61 MB, throughput 0.973416
Reading from 10919: heap size 73 MB, throughput 0.650354
Reading from 10919: heap size 76 MB, throughput 0.990944
Reading from 10917: heap size 170 MB, throughput 0.139794
Reading from 10917: heap size 213 MB, throughput 0.568848
Reading from 10917: heap size 219 MB, throughput 0.705409
Reading from 10917: heap size 222 MB, throughput 0.626805
Reading from 10919: heap size 88 MB, throughput 0.997655
Reading from 10917: heap size 225 MB, throughput 0.635428
Reading from 10917: heap size 233 MB, throughput 0.655968
Reading from 10917: heap size 238 MB, throughput 0.579662
Reading from 10919: heap size 89 MB, throughput 0.996254
Reading from 10917: heap size 248 MB, throughput 0.16289
Reading from 10917: heap size 295 MB, throughput 0.508009
Reading from 10917: heap size 300 MB, throughput 0.521895
Reading from 10917: heap size 302 MB, throughput 0.585439
Reading from 10917: heap size 304 MB, throughput 0.50159
Reading from 10919: heap size 99 MB, throughput 0.997707
Reading from 10917: heap size 310 MB, throughput 0.133597
Reading from 10917: heap size 357 MB, throughput 0.505356
Reading from 10919: heap size 100 MB, throughput 0.997117
Reading from 10917: heap size 351 MB, throughput 0.713423
Reading from 10917: heap size 357 MB, throughput 0.631238
Reading from 10917: heap size 359 MB, throughput 0.594977
Reading from 10917: heap size 361 MB, throughput 0.650809
Reading from 10917: heap size 365 MB, throughput 0.628364
Reading from 10919: heap size 108 MB, throughput 0.995686
Reading from 10917: heap size 372 MB, throughput 0.580987
Reading from 10919: heap size 110 MB, throughput 0.997059
Reading from 10917: heap size 377 MB, throughput 0.10822
Reading from 10917: heap size 432 MB, throughput 0.525222
Reading from 10917: heap size 441 MB, throughput 0.650397
Reading from 10917: heap size 443 MB, throughput 0.592402
Reading from 10917: heap size 444 MB, throughput 0.465763
Reading from 10919: heap size 117 MB, throughput 0.996317
Reading from 10917: heap size 453 MB, throughput 0.538824
Equal recommendation: 1235 MB each
Reading from 10919: heap size 118 MB, throughput 0.996153
Reading from 10917: heap size 459 MB, throughput 0.103541
Reading from 10917: heap size 522 MB, throughput 0.356381
Reading from 10917: heap size 517 MB, throughput 0.565278
Reading from 10917: heap size 523 MB, throughput 0.567184
Reading from 10919: heap size 123 MB, throughput 0.997321
Reading from 10919: heap size 124 MB, throughput 0.997145
Reading from 10917: heap size 519 MB, throughput 0.124535
Reading from 10917: heap size 583 MB, throughput 0.489689
Reading from 10917: heap size 576 MB, throughput 0.579579
Reading from 10917: heap size 581 MB, throughput 0.666667
Reading from 10917: heap size 587 MB, throughput 0.645182
Reading from 10919: heap size 128 MB, throughput 0.997958
Reading from 10917: heap size 590 MB, throughput 0.514584
Reading from 10917: heap size 593 MB, throughput 0.617981
Reading from 10917: heap size 604 MB, throughput 0.556578
Reading from 10919: heap size 128 MB, throughput 0.997452
Reading from 10919: heap size 131 MB, throughput 0.997564
Reading from 10917: heap size 610 MB, throughput 0.0959398
Reading from 10917: heap size 683 MB, throughput 0.727637
Reading from 10919: heap size 132 MB, throughput 0.997487
Reading from 10917: heap size 695 MB, throughput 0.888957
Reading from 10917: heap size 697 MB, throughput 0.839607
Reading from 10919: heap size 134 MB, throughput 0.997082
Reading from 10917: heap size 695 MB, throughput 0.811972
Reading from 10917: heap size 713 MB, throughput 0.535519
Reading from 10917: heap size 723 MB, throughput 0.661568
Reading from 10919: heap size 135 MB, throughput 0.997406
Reading from 10917: heap size 732 MB, throughput 0.396969
Reading from 10917: heap size 741 MB, throughput 0.334421
Reading from 10919: heap size 138 MB, throughput 0.996893
Reading from 10919: heap size 138 MB, throughput 0.996987
Reading from 10917: heap size 743 MB, throughput 0.0189455
Equal recommendation: 1235 MB each
Reading from 10917: heap size 812 MB, throughput 0.226935
Reading from 10919: heap size 140 MB, throughput 0.994054
Reading from 10917: heap size 820 MB, throughput 0.399859
Reading from 10919: heap size 140 MB, throughput 0.997769
Reading from 10919: heap size 143 MB, throughput 0.997633
Reading from 10917: heap size 821 MB, throughput 0.206237
Reading from 10917: heap size 907 MB, throughput 0.458947
Reading from 10917: heap size 910 MB, throughput 0.675798
Reading from 10917: heap size 912 MB, throughput 0.704308
Reading from 10919: heap size 144 MB, throughput 0.998319
Reading from 10917: heap size 912 MB, throughput 0.897254
Reading from 10917: heap size 914 MB, throughput 0.706528
Reading from 10919: heap size 146 MB, throughput 0.998534
Reading from 10917: heap size 913 MB, throughput 0.225242
Reading from 10917: heap size 1000 MB, throughput 0.956948
Reading from 10919: heap size 147 MB, throughput 0.990795
Reading from 10917: heap size 1003 MB, throughput 0.960224
Reading from 10917: heap size 1006 MB, throughput 0.961121
Reading from 10919: heap size 148 MB, throughput 0.99281
Reading from 10917: heap size 1005 MB, throughput 0.876143
Reading from 10917: heap size 832 MB, throughput 0.816188
Reading from 10917: heap size 978 MB, throughput 0.79411
Reading from 10917: heap size 836 MB, throughput 0.834027
Reading from 10919: heap size 149 MB, throughput 0.99604
Reading from 10917: heap size 962 MB, throughput 0.824696
Reading from 10917: heap size 841 MB, throughput 0.788767
Reading from 10917: heap size 950 MB, throughput 0.803199
Reading from 10917: heap size 846 MB, throughput 0.759042
Reading from 10917: heap size 942 MB, throughput 0.850406
Reading from 10917: heap size 852 MB, throughput 0.848514
Reading from 10917: heap size 934 MB, throughput 0.855553
Reading from 10919: heap size 154 MB, throughput 0.997213
Reading from 10917: heap size 858 MB, throughput 0.832069
Reading from 10919: heap size 155 MB, throughput 0.997314
Reading from 10917: heap size 928 MB, throughput 0.987697
Reading from 10919: heap size 158 MB, throughput 0.997723
Reading from 10917: heap size 934 MB, throughput 0.951252
Reading from 10917: heap size 935 MB, throughput 0.806902
Reading from 10917: heap size 936 MB, throughput 0.813676
Reading from 10917: heap size 943 MB, throughput 0.773598
Reading from 10917: heap size 943 MB, throughput 0.819918
Reading from 10919: heap size 159 MB, throughput 0.997553
Reading from 10917: heap size 948 MB, throughput 0.826951
Reading from 10917: heap size 949 MB, throughput 0.82759
Equal recommendation: 1235 MB each
Reading from 10917: heap size 953 MB, throughput 0.838671
Reading from 10917: heap size 954 MB, throughput 0.833178
Reading from 10917: heap size 958 MB, throughput 0.896819
Reading from 10919: heap size 162 MB, throughput 0.997064
Reading from 10917: heap size 959 MB, throughput 0.901468
Reading from 10917: heap size 960 MB, throughput 0.884985
Reading from 10917: heap size 962 MB, throughput 0.777647
Reading from 10919: heap size 162 MB, throughput 0.996652
Reading from 10919: heap size 166 MB, throughput 0.997109
Reading from 10917: heap size 947 MB, throughput 0.0555435
Reading from 10919: heap size 166 MB, throughput 0.997038
Reading from 10917: heap size 1076 MB, throughput 0.497481
Reading from 10917: heap size 1080 MB, throughput 0.752071
Reading from 10917: heap size 1081 MB, throughput 0.739077
Reading from 10917: heap size 1088 MB, throughput 0.761061
Reading from 10917: heap size 1089 MB, throughput 0.741594
Reading from 10917: heap size 1092 MB, throughput 0.758868
Reading from 10919: heap size 169 MB, throughput 0.997713
Reading from 10917: heap size 1095 MB, throughput 0.69608
Reading from 10917: heap size 1097 MB, throughput 0.741098
Reading from 10917: heap size 1101 MB, throughput 0.637005
Reading from 10917: heap size 1104 MB, throughput 0.709125
Reading from 10919: heap size 169 MB, throughput 0.993643
Reading from 10919: heap size 173 MB, throughput 0.99573
Reading from 10919: heap size 173 MB, throughput 0.991325
Reading from 10917: heap size 1108 MB, throughput 0.968723
Reading from 10919: heap size 177 MB, throughput 0.996226
Equal recommendation: 1235 MB each
Reading from 10919: heap size 177 MB, throughput 0.997666
Reading from 10917: heap size 1116 MB, throughput 0.97395
Reading from 10919: heap size 181 MB, throughput 0.998517
Reading from 10919: heap size 181 MB, throughput 0.99683
Reading from 10919: heap size 185 MB, throughput 0.994865
Reading from 10919: heap size 185 MB, throughput 0.996685
Reading from 10917: heap size 1120 MB, throughput 0.97303
Reading from 10919: heap size 190 MB, throughput 0.998205
Reading from 10919: heap size 190 MB, throughput 0.99706
Reading from 10919: heap size 194 MB, throughput 0.997441
Reading from 10917: heap size 1131 MB, throughput 0.970146
Reading from 10919: heap size 194 MB, throughput 0.99774
Equal recommendation: 1235 MB each
Reading from 10919: heap size 197 MB, throughput 0.997871
Reading from 10917: heap size 1134 MB, throughput 0.98032
Reading from 10919: heap size 197 MB, throughput 0.996856
Reading from 10919: heap size 201 MB, throughput 0.997555
Reading from 10919: heap size 201 MB, throughput 0.997591
Reading from 10917: heap size 1134 MB, throughput 0.980638
Reading from 10919: heap size 204 MB, throughput 0.998445
Reading from 10919: heap size 204 MB, throughput 0.997795
Reading from 10917: heap size 1138 MB, throughput 0.978753
Reading from 10919: heap size 206 MB, throughput 0.997483
Reading from 10919: heap size 207 MB, throughput 0.997306
Equal recommendation: 1235 MB each
Reading from 10919: heap size 209 MB, throughput 0.996541
Reading from 10919: heap size 210 MB, throughput 0.991779
Reading from 10917: heap size 1136 MB, throughput 0.963266
Reading from 10919: heap size 214 MB, throughput 0.99786
Reading from 10919: heap size 214 MB, throughput 0.998219
Reading from 10917: heap size 1139 MB, throughput 0.977642
Reading from 10919: heap size 217 MB, throughput 0.998412
Reading from 10919: heap size 207 MB, throughput 0.996995
Reading from 10919: heap size 198 MB, throughput 0.997664
Reading from 10917: heap size 1144 MB, throughput 0.982048
Reading from 10919: heap size 190 MB, throughput 0.997761
Reading from 10919: heap size 182 MB, throughput 0.996972
Equal recommendation: 1235 MB each
Reading from 10919: heap size 174 MB, throughput 0.996144
Reading from 10917: heap size 1145 MB, throughput 0.979391
Reading from 10919: heap size 167 MB, throughput 0.997198
Reading from 10919: heap size 160 MB, throughput 0.997247
Reading from 10919: heap size 153 MB, throughput 0.9974
Reading from 10919: heap size 147 MB, throughput 0.996716
Reading from 10919: heap size 141 MB, throughput 0.996892
Reading from 10917: heap size 1149 MB, throughput 0.981416
Reading from 10919: heap size 135 MB, throughput 0.997281
Reading from 10919: heap size 129 MB, throughput 0.892946
Reading from 10919: heap size 126 MB, throughput 0.985212
Reading from 10919: heap size 133 MB, throughput 0.982252
Reading from 10919: heap size 141 MB, throughput 0.987909
Reading from 10919: heap size 149 MB, throughput 0.996694
Reading from 10917: heap size 1152 MB, throughput 0.978625
Equal recommendation: 1235 MB each
Reading from 10919: heap size 158 MB, throughput 0.997915
Reading from 10919: heap size 165 MB, throughput 0.9979
Reading from 10919: heap size 172 MB, throughput 0.997686
Reading from 10917: heap size 1156 MB, throughput 0.980658
Reading from 10919: heap size 179 MB, throughput 0.997795
Reading from 10919: heap size 185 MB, throughput 0.998061
Reading from 10919: heap size 190 MB, throughput 0.99769
Reading from 10917: heap size 1160 MB, throughput 0.978859
Reading from 10919: heap size 194 MB, throughput 0.996781
Reading from 10919: heap size 197 MB, throughput 0.998064
Reading from 10919: heap size 197 MB, throughput 0.996991
Equal recommendation: 1235 MB each
Reading from 10917: heap size 1163 MB, throughput 0.980535
Reading from 10919: heap size 201 MB, throughput 0.997713
Reading from 10919: heap size 201 MB, throughput 0.997639
Reading from 10919: heap size 206 MB, throughput 0.996916
Reading from 10917: heap size 1168 MB, throughput 0.978268
Reading from 10919: heap size 206 MB, throughput 0.997352
Reading from 10919: heap size 211 MB, throughput 0.994772
Reading from 10919: heap size 211 MB, throughput 0.990958
Reading from 10919: heap size 218 MB, throughput 0.997239
Reading from 10917: heap size 1172 MB, throughput 0.978448
Reading from 10919: heap size 218 MB, throughput 0.997921
Equal recommendation: 1235 MB each
Reading from 10919: heap size 224 MB, throughput 0.997106
Reading from 10917: heap size 1176 MB, throughput 0.977802
Reading from 10919: heap size 225 MB, throughput 0.997525
Reading from 10919: heap size 231 MB, throughput 0.998119
Reading from 10919: heap size 231 MB, throughput 0.997721
Reading from 10917: heap size 1180 MB, throughput 0.979007
Reading from 10919: heap size 237 MB, throughput 0.998038
Reading from 10919: heap size 237 MB, throughput 0.997933
Reading from 10919: heap size 242 MB, throughput 0.991843
Equal recommendation: 1235 MB each
Reading from 10919: heap size 242 MB, throughput 0.997933
Reading from 10917: heap size 1182 MB, throughput 0.696094
Reading from 10919: heap size 249 MB, throughput 0.998395
Reading from 10919: heap size 250 MB, throughput 0.993886
Reading from 10919: heap size 256 MB, throughput 0.996249
Reading from 10917: heap size 1244 MB, throughput 0.995468
Reading from 10919: heap size 257 MB, throughput 0.99768
Reading from 10919: heap size 264 MB, throughput 0.998504
Reading from 10917: heap size 1243 MB, throughput 0.994542
Reading from 10919: heap size 265 MB, throughput 0.99808
Equal recommendation: 1235 MB each
Reading from 10919: heap size 270 MB, throughput 0.997758
Reading from 10917: heap size 1248 MB, throughput 0.992134
Reading from 10919: heap size 271 MB, throughput 0.997994
Reading from 10919: heap size 276 MB, throughput 0.998672
Reading from 10917: heap size 1127 MB, throughput 0.990833
Reading from 10919: heap size 277 MB, throughput 0.998387
Reading from 10919: heap size 282 MB, throughput 0.997858
Reading from 10917: heap size 1252 MB, throughput 0.989533
Equal recommendation: 1235 MB each
Reading from 10919: heap size 282 MB, throughput 0.998222
Reading from 10919: heap size 288 MB, throughput 0.993098
Reading from 10919: heap size 288 MB, throughput 0.994627
Reading from 10917: heap size 1140 MB, throughput 0.987576
Reading from 10919: heap size 297 MB, throughput 0.998097
Reading from 10919: heap size 297 MB, throughput 0.995442
Reading from 10917: heap size 1241 MB, throughput 0.986726
Reading from 10919: heap size 305 MB, throughput 0.998284
Equal recommendation: 1235 MB each
Reading from 10917: heap size 1154 MB, throughput 0.987481
Reading from 10919: heap size 305 MB, throughput 0.998065
Reading from 10919: heap size 313 MB, throughput 0.998485
Reading from 10917: heap size 1226 MB, throughput 0.986092
Reading from 10919: heap size 313 MB, throughput 0.998073
Reading from 10919: heap size 320 MB, throughput 0.997726
Reading from 10917: heap size 1169 MB, throughput 0.984107
Reading from 10919: heap size 320 MB, throughput 0.990615
Reading from 10919: heap size 326 MB, throughput 0.996584
Equal recommendation: 1235 MB each
Reading from 10917: heap size 1224 MB, throughput 0.983597
Reading from 10919: heap size 327 MB, throughput 0.997191
Reading from 10919: heap size 337 MB, throughput 0.998266
Reading from 10917: heap size 1226 MB, throughput 0.982137
Reading from 10919: heap size 338 MB, throughput 0.997792
Reading from 10919: heap size 346 MB, throughput 0.997905
Reading from 10917: heap size 1226 MB, throughput 0.979323
Equal recommendation: 1235 MB each
Reading from 10919: heap size 346 MB, throughput 0.997505
Reading from 10917: heap size 1229 MB, throughput 0.979441
Reading from 10919: heap size 355 MB, throughput 0.998254
Reading from 10919: heap size 355 MB, throughput 0.998338
Reading from 10917: heap size 1230 MB, throughput 0.966172
Reading from 10919: heap size 362 MB, throughput 0.911042
Reading from 10919: heap size 370 MB, throughput 0.998712
Reading from 10917: heap size 1235 MB, throughput 0.977673
Equal recommendation: 1235 MB each
Reading from 10919: heap size 384 MB, throughput 0.999084
Reading from 10917: heap size 1240 MB, throughput 0.974463
Reading from 10919: heap size 385 MB, throughput 0.999047
Reading from 10919: heap size 395 MB, throughput 0.999395
Reading from 10917: heap size 1242 MB, throughput 0.976788
Reading from 10919: heap size 396 MB, throughput 0.999193
Equal recommendation: 1235 MB each
Reading from 10917: heap size 1244 MB, throughput 0.976532
Reading from 10919: heap size 403 MB, throughput 0.999245
Reading from 10919: heap size 405 MB, throughput 0.996948
Reading from 10917: heap size 1199 MB, throughput 0.974504
Reading from 10919: heap size 412 MB, throughput 0.998485
Reading from 10917: heap size 1244 MB, throughput 0.978215
Reading from 10919: heap size 412 MB, throughput 0.998733
Equal recommendation: 1235 MB each
Reading from 10919: heap size 419 MB, throughput 0.999028
Reading from 10917: heap size 1197 MB, throughput 0.976607
Reading from 10919: heap size 421 MB, throughput 0.998704
Reading from 10917: heap size 1244 MB, throughput 0.975335
Reading from 10919: heap size 428 MB, throughput 0.998523
Reading from 10917: heap size 1195 MB, throughput 0.976772
Reading from 10919: heap size 428 MB, throughput 0.998324
Equal recommendation: 1235 MB each
Reading from 10919: heap size 436 MB, throughput 0.996451
Reading from 10917: heap size 1241 MB, throughput 0.974801
Reading from 10919: heap size 436 MB, throughput 0.998328
Reading from 10917: heap size 1194 MB, throughput 0.976158
Reading from 10919: heap size 447 MB, throughput 0.99832
Reading from 10917: heap size 1237 MB, throughput 0.97733
Reading from 10919: heap size 447 MB, throughput 0.998545
Equal recommendation: 1235 MB each
Reading from 10919: heap size 456 MB, throughput 0.99863
Reading from 10917: heap size 1193 MB, throughput 0.978498
Reading from 10919: heap size 456 MB, throughput 0.998272
Reading from 10917: heap size 1233 MB, throughput 0.97725
Reading from 10919: heap size 465 MB, throughput 0.997687
Reading from 10919: heap size 465 MB, throughput 0.997305
Equal recommendation: 1235 MB each
Reading from 10919: heap size 475 MB, throughput 0.998489
Reading from 10919: heap size 475 MB, throughput 0.998617
Reading from 10919: heap size 484 MB, throughput 0.998663
Equal recommendation: 1235 MB each
Reading from 10919: heap size 485 MB, throughput 0.998453
Reading from 10919: heap size 494 MB, throughput 0.996707
Reading from 10917: heap size 1232 MB, throughput 0.860594
Reading from 10919: heap size 494 MB, throughput 0.997451
Equal recommendation: 1235 MB each
Reading from 10919: heap size 506 MB, throughput 0.998501
Reading from 10919: heap size 506 MB, throughput 0.998393
Reading from 10919: heap size 517 MB, throughput 0.998629
Reading from 10917: heap size 1355 MB, throughput 0.989714
Reading from 10919: heap size 518 MB, throughput 0.998678
Equal recommendation: 1235 MB each
Reading from 10919: heap size 527 MB, throughput 0.996686
Reading from 10917: heap size 1301 MB, throughput 0.963741
Reading from 10919: heap size 528 MB, throughput 0.998418
Reading from 10917: heap size 1347 MB, throughput 0.983853
Reading from 10917: heap size 1371 MB, throughput 0.87776
Reading from 10917: heap size 1375 MB, throughput 0.754538
Reading from 10919: heap size 540 MB, throughput 0.998627
Reading from 10917: heap size 1377 MB, throughput 0.68627
Reading from 10917: heap size 1367 MB, throughput 0.605382
Reading from 10917: heap size 1393 MB, throughput 0.60758
Reading from 10917: heap size 1397 MB, throughput 0.581884
Reading from 10917: heap size 1409 MB, throughput 0.600885
Reading from 10917: heap size 1412 MB, throughput 0.577532
Equal recommendation: 1235 MB each
Reading from 10917: heap size 1325 MB, throughput 0.11236
Reading from 10917: heap size 1385 MB, throughput 0.986657
Reading from 10917: heap size 1386 MB, throughput 0.986963
Reading from 10917: heap size 1389 MB, throughput 0.989314
Reading from 10917: heap size 1115 MB, throughput 0.987507
Reading from 10917: heap size 1389 MB, throughput 0.981212
Reading from 10917: heap size 1115 MB, throughput 0.971235
Reading from 10919: heap size 541 MB, throughput 0.998591
Reading from 10917: heap size 1388 MB, throughput 0.995001
Reading from 10917: heap size 1118 MB, throughput 0.99365
Reading from 10919: heap size 550 MB, throughput 0.998744
Reading from 10917: heap size 1386 MB, throughput 0.992856
Reading from 10917: heap size 1131 MB, throughput 0.990273
Equal recommendation: 1235 MB each
Client 10919 died
Clients: 1
Reading from 10917: heap size 1360 MB, throughput 0.988973
Reading from 10917: heap size 1143 MB, throughput 0.986571
Reading from 10917: heap size 1336 MB, throughput 0.986762
Reading from 10917: heap size 1155 MB, throughput 0.985229
Reading from 10917: heap size 1311 MB, throughput 0.984341
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1165 MB, throughput 0.98334
Reading from 10917: heap size 1289 MB, throughput 0.982247
Reading from 10917: heap size 1176 MB, throughput 0.981712
Reading from 10917: heap size 1270 MB, throughput 0.982565
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1187 MB, throughput 0.980651
Reading from 10917: heap size 1265 MB, throughput 0.979321
Reading from 10917: heap size 1270 MB, throughput 0.978288
Reading from 10917: heap size 1264 MB, throughput 0.979256
Reading from 10917: heap size 1267 MB, throughput 0.97932
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1262 MB, throughput 0.980871
Reading from 10917: heap size 1265 MB, throughput 0.976901
Reading from 10917: heap size 1260 MB, throughput 0.980527
Reading from 10917: heap size 1263 MB, throughput 0.977646
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1261 MB, throughput 0.979484
Reading from 10917: heap size 1262 MB, throughput 0.979776
Reading from 10917: heap size 1260 MB, throughput 0.979327
Reading from 10917: heap size 1262 MB, throughput 0.980887
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1262 MB, throughput 0.979046
Reading from 10917: heap size 1263 MB, throughput 0.97875
Reading from 10917: heap size 1264 MB, throughput 0.979688
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1265 MB, throughput 0.979432
Reading from 10917: heap size 1268 MB, throughput 0.978255
Reading from 10917: heap size 1268 MB, throughput 0.979682
Reading from 10917: heap size 1271 MB, throughput 0.978979
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1271 MB, throughput 0.978508
Reading from 10917: heap size 1275 MB, throughput 0.979567
Reading from 10917: heap size 1275 MB, throughput 0.978662
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1280 MB, throughput 0.978967
Reading from 10917: heap size 1280 MB, throughput 0.979155
Reading from 10917: heap size 1285 MB, throughput 0.978831
Reading from 10917: heap size 1285 MB, throughput 0.978966
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1290 MB, throughput 0.97919
Reading from 10917: heap size 1290 MB, throughput 0.978975
Reading from 10917: heap size 1294 MB, throughput 0.9785
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1295 MB, throughput 0.979563
Reading from 10917: heap size 1300 MB, throughput 0.978123
Reading from 10917: heap size 1300 MB, throughput 0.976865
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1305 MB, throughput 0.97897
Reading from 10917: heap size 1306 MB, throughput 0.977383
Reading from 10917: heap size 1311 MB, throughput 0.979022
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1311 MB, throughput 0.979143
Reading from 10917: heap size 1317 MB, throughput 0.756626
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1324 MB, throughput 0.995597
Reading from 10917: heap size 1321 MB, throughput 0.993917
Reading from 10917: heap size 1331 MB, throughput 0.992196
Reading from 10917: heap size 1339 MB, throughput 0.991542
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1341 MB, throughput 0.990752
Reading from 10917: heap size 1340 MB, throughput 0.989425
Reading from 10917: heap size 1260 MB, throughput 0.988126
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1333 MB, throughput 0.987392
Reading from 10917: heap size 1277 MB, throughput 0.987158
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1329 MB, throughput 0.985455
Reading from 10917: heap size 1333 MB, throughput 0.9842
Reading from 10917: heap size 1335 MB, throughput 0.983296
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1336 MB, throughput 0.991391
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1311 MB, throughput 0.992708
Reading from 10917: heap size 1344 MB, throughput 0.803466
Recommendation: one client; give it all the memory
Reading from 10917: heap size 1356 MB, throughput 0.996093
Reading from 10917: heap size 1401 MB, throughput 0.986927
Reading from 10917: heap size 1406 MB, throughput 0.920106
Reading from 10917: heap size 1282 MB, throughput 0.863532
Reading from 10917: heap size 1399 MB, throughput 0.849689
Reading from 10917: heap size 1401 MB, throughput 0.850715
Reading from 10917: heap size 1403 MB, throughput 0.862792
Reading from 10917: heap size 1407 MB, throughput 0.862186
Reading from 10917: heap size 1413 MB, throughput 0.877971
Reading from 10917: heap size 1418 MB, throughput 0.877932
Reading from 10917: heap size 1428 MB, throughput 0.887597
Client 10917 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
