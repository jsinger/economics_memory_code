%define api.prefix {ProgramSpec_}

%{
#include <cstdio>
#include <list>
#include <Utilities.hpp>
#include "ProgramSpec.hpp"
int ProgramSpec_lex(void);
void ProgramSpec_error(const char*);
static void validatePhaseLine(int allocRate, double length, int error);
%}

%union {
    int i;
    double f;
    char* s;
    Phase* phase;
    Transition* transition;
    std::list<Transition*>* transList;
    Program* program;
};

%token <i> INTEGER
%token <f> FLOAT
%token <s> STRING
%token INDENT
%token PHASE
%token MINHEAP
%token TRANSITION

%type <program> program
%type <phase> phase
%type <phase> phaseLine
%type <transition> transitionLine
%type <transList> transitionLines
%type <i> minHeap

%%

program:
                program phase                     { $1->addPhase($2); $$ = $1; }
        |       program minHeap                   { $1->setMinHeapMB($2); $$ = $1; }
        |                                         {$$ = g_BisonParsedProgram = new Program; }
                ;

phase:
                phaseLine transitionLines         { $1->addTransitions(*$2); delete $2; $$ = $1; }
                ;

transitionLines:
                transitionLines transitionLine    { $1->push_back($2); $$ = $1; }
        |       transitionLine                    { $$ = new std::list<Transition*>(1, $1); }

phaseLine:
                PHASE STRING INTEGER FLOAT INTEGER   { validatePhaseLine($3, $4, $5); $$ = new Phase($2, $3, $4, $5); free($2); }
        |       PHASE STRING INTEGER INTEGER INTEGER { validatePhaseLine($3, $4, $5); $$ = new Phase($2, $3, (double)$4, $5); free($2); }
        |       PHASE STRING INTEGER FLOAT           { validatePhaseLine($3, $4, 0); $$ = new Phase($2, $3, $4, 0); free($2); }
        |       PHASE STRING INTEGER INTEGER         { validatePhaseLine($3, $4, 0); $$ = new Phase($2, $3, (double)$4, 0); free($2); }
                ;

transitionLine:
                INDENT TRANSITION STRING INTEGER             { if ($4 < 1 || $4 > 100) ProgramSpec_error("Bad probability"); else { $$ = new Transition($3, $4); free($3); } }
                ;

minHeap:
                MINHEAP INTEGER                   { $$ = $2; }
                ;
%%

static void validatePhaseLine(int allocRate, double length, int error)
{
    if (allocRate <= 0 || length <= 0.0f || error > 100)
        ProgramSpec_error("Badly-formatted phase line");
}

void ProgramSpec_error(const char* err)
{
    economem::die("%s", err);
}
