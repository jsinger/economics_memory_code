module Economem.JavaCmdlineLauncher (launchJava) where

import Economem.Java
import Economem.Process
import Economem.Signals
import System.Environment

launchJava :: Java -> IO ()
launchJava j = do
  args <- getArgs
  p <- javaProcessLauncher
       j
       Nothing
       Nothing
       "."
       args
       Nothing
       Nothing
       Nothing
       []
       Nothing
       False
  h <- makeSignalHandler
  running <- start p
  killProcessOnSignal h running
  wait running
