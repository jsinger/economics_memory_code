Dynamic Heap Sizing
===================

Setup
-----

1. Install the dependencies
3. Set the environment variable ECONOMEM_ROOT to this folder (i.e. `economics_memory_code/dynamic_sizing`)
2. Add the python_common directory to your PYTHONPATH
3. `make`

The top-level makefile builds everything. The first run will take a very long time, because it downloads and builds three different versions of OpenJDK -- but subsequent builds will be much faster. Alternatively, just build the sub-directories you're interested in. To build the documentation, do `make docs` (the makefile also does this).

Documentation
-------------

Once the docs are built, look in `docs/html/index.html`.

Dependencies
------------

If you're on a recent Ubuntu-based system (recommended; tested on 13.10 and 14.04), just run `packages.sh`, then install [argtypes](https://github.com/CallumCameron/argtypes) manually. Otherwise, install as needed:

General:
 - Clang
 - GCC
 - Make
 - Scons
 - Python 2.7

For the daemon:
 - Python 2.7 development tools
 - wget (the build process pulls in some more libraries and builds them from source)
 - Boost
 - NumPy
 - SciPy

For the console:
 - Readline development headers

For the fake VM:
 - Flex
 - Bison

For the GC log parser:
 - Haskell platform

For all OpenJDK versions (see also the OpenJDK docs):
 - Mercurial
 - Ccache (optional, but makes things build faster)
 - An existing Java installation, preferably OpenJDK 7 (e.g. from your package manager)
 - X11 headers
 - CUPS headers
 - Freetype headers
 - Alsa headers

Additionally for OpenJDK 7:
 - Autoconf
 - Automake
 - Ant
 - Gawk
 - GCJ
 - Xsltproc
 - Rhino
 - Libjpeg headers
 - Libgif headers
 - Libpng headers
 - GTK 2 headers
 - Libattr1
 - Systemtap sdt headers

For JikesRVM:
 - Ant optional JAR files
 - GCC multilib (if on a 64-bit machine)
 - G++ multilib (if on a 64-bit machine)

For the measurement scripts:
 - the python packages: psutil, pytimeparse, future (can be installed through pip)
 - [Argtypes](https://github.com/CallumCameron/argtypes)
 - the cgroups commands (cgexec, cgcreate)
 - MatPlotLib
 - tkinter
 - the 'unbuffer' command (found in Ubuntu package expect-dev)
 - the Haskell packages: cassava, cmdlib, cond, download, missingh, zip-archive, hstats

For the docs:
 - Doxygen >= 1.8.6 (must be installed from source on Ubuntu 13.10)
 - Graphviz
