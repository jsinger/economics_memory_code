#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

DIST="${DIR}/gcbench/dist"
TARGET="${DIST}/gcbench.jar"

if [ ! -e "${TARGET}" ]; then
    SRC="$(readlink -f "$(ls -1 ${DIST}/gcbench-*.jar | head -n 1)")"
    mv "${SRC}" "${TARGET}"
fi
