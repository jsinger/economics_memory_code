economemd
    total memory: 8922 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub6_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run05_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 12953: heap size 9 MB, throughput 0.991902
Clients: 1
Client 12953 has a minimum heap size of 1211 MB
Reading from 12954: heap size 9 MB, throughput 0.992067
Clients: 2
Client 12954 has a minimum heap size of 276 MB
Reading from 12953: heap size 9 MB, throughput 0.987886
Reading from 12954: heap size 9 MB, throughput 0.988906
Reading from 12953: heap size 9 MB, throughput 0.982272
Reading from 12954: heap size 9 MB, throughput 0.98209
Reading from 12953: heap size 9 MB, throughput 0.97408
Reading from 12954: heap size 9 MB, throughput 0.975206
Reading from 12953: heap size 11 MB, throughput 0.956025
Reading from 12954: heap size 11 MB, throughput 0.975103
Reading from 12954: heap size 11 MB, throughput 0.979738
Reading from 12953: heap size 11 MB, throughput 0.971135
Reading from 12953: heap size 17 MB, throughput 0.912832
Reading from 12954: heap size 17 MB, throughput 0.915232
Reading from 12954: heap size 17 MB, throughput 0.866925
Reading from 12953: heap size 17 MB, throughput 0.886556
Reading from 12954: heap size 30 MB, throughput 0.68776
Reading from 12953: heap size 29 MB, throughput 0.747312
Reading from 12954: heap size 31 MB, throughput 0.189969
Reading from 12953: heap size 30 MB, throughput 0.839558
Reading from 12954: heap size 35 MB, throughput 0.725657
Reading from 12953: heap size 35 MB, throughput 0.554631
Reading from 12954: heap size 47 MB, throughput 0.471571
Reading from 12953: heap size 47 MB, throughput 0.338918
Reading from 12954: heap size 49 MB, throughput 0.551405
Reading from 12954: heap size 63 MB, throughput 0.318911
Reading from 12953: heap size 49 MB, throughput 0.601238
Reading from 12954: heap size 69 MB, throughput 0.278815
Reading from 12953: heap size 64 MB, throughput 0.318812
Reading from 12953: heap size 71 MB, throughput 0.440128
Reading from 12953: heap size 87 MB, throughput 0.370082
Reading from 12954: heap size 70 MB, throughput 0.435188
Reading from 12953: heap size 96 MB, throughput 0.386949
Reading from 12954: heap size 99 MB, throughput 0.556727
Reading from 12954: heap size 100 MB, throughput 0.461935
Reading from 12953: heap size 96 MB, throughput 0.386989
Reading from 12954: heap size 130 MB, throughput 0.674878
Reading from 12953: heap size 131 MB, throughput 0.524491
Reading from 12954: heap size 130 MB, throughput 0.5581
Reading from 12953: heap size 131 MB, throughput 0.257713
Reading from 12954: heap size 132 MB, throughput 0.278734
Reading from 12953: heap size 133 MB, throughput 0.433334
Reading from 12954: heap size 136 MB, throughput 0.417672
Reading from 12953: heap size 167 MB, throughput 0.343325
Reading from 12954: heap size 165 MB, throughput 0.0658921
Reading from 12953: heap size 173 MB, throughput 0.414115
Reading from 12954: heap size 169 MB, throughput 0.146883
Reading from 12953: heap size 174 MB, throughput 0.761305
Reading from 12954: heap size 172 MB, throughput 0.0166382
Reading from 12953: heap size 179 MB, throughput 0.775887
Reading from 12954: heap size 174 MB, throughput 0.412332
Reading from 12953: heap size 184 MB, throughput 0.486472
Reading from 12954: heap size 211 MB, throughput 0.753559
Reading from 12953: heap size 229 MB, throughput 0.735547
Reading from 12953: heap size 232 MB, throughput 0.735486
Reading from 12953: heap size 235 MB, throughput 0.719637
Reading from 12954: heap size 215 MB, throughput 0.868031
Reading from 12953: heap size 242 MB, throughput 0.647008
Reading from 12954: heap size 214 MB, throughput 0.890144
Reading from 12954: heap size 222 MB, throughput 0.786179
Reading from 12953: heap size 246 MB, throughput 0.371459
Reading from 12954: heap size 226 MB, throughput 0.854169
Reading from 12954: heap size 227 MB, throughput 0.785062
Reading from 12953: heap size 300 MB, throughput 0.640513
Reading from 12954: heap size 232 MB, throughput 0.758459
Reading from 12953: heap size 309 MB, throughput 0.608927
Reading from 12954: heap size 233 MB, throughput 0.758579
Reading from 12953: heap size 310 MB, throughput 0.643353
Reading from 12953: heap size 313 MB, throughput 0.63488
Reading from 12954: heap size 238 MB, throughput 0.848365
Reading from 12953: heap size 319 MB, throughput 0.623256
Reading from 12954: heap size 239 MB, throughput 0.835721
Reading from 12953: heap size 321 MB, throughput 0.607203
Reading from 12954: heap size 240 MB, throughput 0.81669
Reading from 12953: heap size 332 MB, throughput 0.545959
Reading from 12954: heap size 245 MB, throughput 0.765462
Reading from 12954: heap size 250 MB, throughput 0.738274
Reading from 12953: heap size 338 MB, throughput 0.504383
Reading from 12954: heap size 253 MB, throughput 0.732332
Reading from 12954: heap size 258 MB, throughput 0.819361
Reading from 12953: heap size 349 MB, throughput 0.422463
Reading from 12953: heap size 408 MB, throughput 0.441685
Reading from 12953: heap size 338 MB, throughput 0.483731
Reading from 12953: heap size 399 MB, throughput 0.186683
Reading from 12953: heap size 390 MB, throughput 0.450839
Reading from 12954: heap size 260 MB, throughput 0.644735
Reading from 12953: heap size 452 MB, throughput 0.505664
Reading from 12954: heap size 295 MB, throughput 0.757345
Reading from 12954: heap size 297 MB, throughput 0.705886
Reading from 12953: heap size 455 MB, throughput 0.538679
Reading from 12954: heap size 299 MB, throughput 0.760262
Reading from 12953: heap size 447 MB, throughput 0.569773
Reading from 12954: heap size 300 MB, throughput 0.804093
Reading from 12953: heap size 451 MB, throughput 0.531932
Reading from 12954: heap size 301 MB, throughput 0.86226
Reading from 12954: heap size 302 MB, throughput 0.812465
Reading from 12953: heap size 446 MB, throughput 0.511137
Reading from 12954: heap size 303 MB, throughput 0.807119
Reading from 12954: heap size 305 MB, throughput 0.717792
Reading from 12953: heap size 449 MB, throughput 0.557174
Reading from 12954: heap size 309 MB, throughput 0.730432
Reading from 12954: heap size 310 MB, throughput 0.667673
Reading from 12953: heap size 453 MB, throughput 0.568772
Reading from 12954: heap size 317 MB, throughput 0.691963
Reading from 12954: heap size 317 MB, throughput 0.706914
Numeric result:
Recommendation: 2 clients, utility 0.632702:
    h1: 1211 MB (U(h) = 0.892627*h^0.001)
    h2: 7711 MB (U(h) = 0.414533*h^0.0591408)
Recommendation: 2 clients, utility 0.632702:
    h1: 1211 MB (U(h) = 0.892627*h^0.001)
    h2: 7711 MB (U(h) = 0.414533*h^0.0591408)
Reading from 12953: heap size 455 MB, throughput 0.352389
Reading from 12953: heap size 513 MB, throughput 0.498773
Reading from 12953: heap size 519 MB, throughput 0.489359
Reading from 12953: heap size 525 MB, throughput 0.499496
Reading from 12954: heap size 325 MB, throughput 0.943402
Reading from 12953: heap size 528 MB, throughput 0.500111
Reading from 12953: heap size 536 MB, throughput 0.283646
Reading from 12953: heap size 609 MB, throughput 0.468858
Reading from 12953: heap size 618 MB, throughput 0.539506
Reading from 12954: heap size 326 MB, throughput 0.968082
Reading from 12953: heap size 618 MB, throughput 0.573833
Reading from 12953: heap size 620 MB, throughput 0.589939
Reading from 12953: heap size 630 MB, throughput 0.542847
Reading from 12953: heap size 638 MB, throughput 0.724506
Reading from 12954: heap size 329 MB, throughput 0.811361
Reading from 12953: heap size 650 MB, throughput 0.824389
Reading from 12953: heap size 650 MB, throughput 0.82976
Reading from 12953: heap size 672 MB, throughput 0.754238
Reading from 12953: heap size 681 MB, throughput 0.624903
Reading from 12954: heap size 371 MB, throughput 0.95704
Reading from 12953: heap size 702 MB, throughput 0.511576
Reading from 12954: heap size 376 MB, throughput 0.969813
Reading from 12953: heap size 714 MB, throughput 0.498216
Reading from 12953: heap size 794 MB, throughput 0.361122
Reading from 12954: heap size 376 MB, throughput 0.95824
Reading from 12953: heap size 769 MB, throughput 0.0448386
Reading from 12953: heap size 731 MB, throughput 0.23453
Reading from 12953: heap size 857 MB, throughput 0.282335
Reading from 12953: heap size 858 MB, throughput 0.324046
Reading from 12953: heap size 858 MB, throughput 0.376902
Reading from 12953: heap size 859 MB, throughput 0.41049
Numeric result:
Recommendation: 2 clients, utility 0.911059:
    h1: 1211 MB (U(h) = 1.04245*h^0.001)
    h2: 7711 MB (U(h) = 0.350152*h^0.101399)
Recommendation: 2 clients, utility 0.911059:
    h1: 1211 MB (U(h) = 1.04245*h^0.001)
    h2: 7711 MB (U(h) = 0.350152*h^0.101399)
Reading from 12954: heap size 377 MB, throughput 0.969439
Reading from 12953: heap size 858 MB, throughput 0.227442
Reading from 12953: heap size 949 MB, throughput 0.664654
Reading from 12953: heap size 948 MB, throughput 0.670505
Reading from 12953: heap size 950 MB, throughput 0.684874
Reading from 12953: heap size 947 MB, throughput 0.74685
Reading from 12953: heap size 721 MB, throughput 0.795484
Reading from 12954: heap size 379 MB, throughput 0.972399
Reading from 12953: heap size 938 MB, throughput 0.887506
Reading from 12953: heap size 733 MB, throughput 0.862188
Reading from 12953: heap size 922 MB, throughput 0.87443
Reading from 12953: heap size 735 MB, throughput 0.865674
Reading from 12953: heap size 906 MB, throughput 0.855824
Reading from 12953: heap size 740 MB, throughput 0.896835
Reading from 12953: heap size 888 MB, throughput 0.85067
Reading from 12953: heap size 750 MB, throughput 0.890271
Reading from 12954: heap size 377 MB, throughput 0.971836
Reading from 12953: heap size 869 MB, throughput 0.875613
Reading from 12953: heap size 881 MB, throughput 0.821799
Reading from 12953: heap size 870 MB, throughput 0.782611
Reading from 12953: heap size 783 MB, throughput 0.76375
Reading from 12953: heap size 860 MB, throughput 0.759491
Reading from 12953: heap size 787 MB, throughput 0.767966
Reading from 12953: heap size 852 MB, throughput 0.76953
Reading from 12953: heap size 792 MB, throughput 0.78458
Reading from 12953: heap size 846 MB, throughput 0.783541
Reading from 12953: heap size 794 MB, throughput 0.745151
Reading from 12953: heap size 842 MB, throughput 0.745566
Reading from 12953: heap size 847 MB, throughput 0.739535
Reading from 12954: heap size 379 MB, throughput 0.973493
Reading from 12953: heap size 842 MB, throughput 0.783978
Reading from 12953: heap size 845 MB, throughput 0.913212
Reading from 12954: heap size 379 MB, throughput 0.976574
Reading from 12953: heap size 845 MB, throughput 0.966672
Reading from 12953: heap size 849 MB, throughput 0.943621
Reading from 12953: heap size 845 MB, throughput 0.91127
Reading from 12953: heap size 850 MB, throughput 0.853469
Reading from 12953: heap size 856 MB, throughput 0.800107
Reading from 12953: heap size 859 MB, throughput 0.773922
Reading from 12953: heap size 865 MB, throughput 0.77381
Reading from 12954: heap size 380 MB, throughput 0.978412
Reading from 12953: heap size 867 MB, throughput 0.726261
Reading from 12953: heap size 875 MB, throughput 0.748318
Reading from 12953: heap size 875 MB, throughput 0.715334
Reading from 12953: heap size 882 MB, throughput 0.72774
Reading from 12953: heap size 882 MB, throughput 0.751215
Reading from 12953: heap size 888 MB, throughput 0.824472
Reading from 12954: heap size 382 MB, throughput 0.977323
Reading from 12953: heap size 889 MB, throughput 0.839358
Reading from 12953: heap size 891 MB, throughput 0.857458
Numeric result:
Recommendation: 2 clients, utility 0.609907:
    h1: 1437.33 MB (U(h) = 0.507392*h^0.0252607)
    h2: 7484.67 MB (U(h) = 0.309433*h^0.131535)
Recommendation: 2 clients, utility 0.609907:
    h1: 1437.38 MB (U(h) = 0.507392*h^0.0252607)
    h2: 7484.62 MB (U(h) = 0.309433*h^0.131535)
Reading from 12953: heap size 893 MB, throughput 0.713493
Reading from 12954: heap size 383 MB, throughput 0.9772
Reading from 12953: heap size 983 MB, throughput 0.645619
Reading from 12953: heap size 985 MB, throughput 0.652562
Reading from 12953: heap size 994 MB, throughput 0.647488
Reading from 12953: heap size 998 MB, throughput 0.603316
Reading from 12953: heap size 1008 MB, throughput 0.634593
Reading from 12953: heap size 1012 MB, throughput 0.63359
Reading from 12953: heap size 1025 MB, throughput 0.61981
Reading from 12954: heap size 380 MB, throughput 0.980205
Reading from 12953: heap size 1029 MB, throughput 0.636568
Reading from 12954: heap size 387 MB, throughput 0.967891
Reading from 12954: heap size 384 MB, throughput 0.938905
Reading from 12953: heap size 1044 MB, throughput 0.61183
Reading from 12954: heap size 390 MB, throughput 0.905392
Reading from 12954: heap size 398 MB, throughput 0.836005
Reading from 12953: heap size 1047 MB, throughput 0.580584
Reading from 12953: heap size 1064 MB, throughput 0.587675
Reading from 12954: heap size 401 MB, throughput 0.911873
Reading from 12953: heap size 1067 MB, throughput 0.80318
Reading from 12954: heap size 407 MB, throughput 0.971102
Reading from 12953: heap size 1086 MB, throughput 0.908218
Reading from 12954: heap size 410 MB, throughput 0.978976
Reading from 12953: heap size 1089 MB, throughput 0.927867
Reading from 12954: heap size 408 MB, throughput 0.983428
Reading from 12953: heap size 1104 MB, throughput 0.933048
Numeric result:
Recommendation: 2 clients, utility 1.00412:
    h1: 4722.04 MB (U(h) = 0.174664*h^0.194939)
    h2: 4199.96 MB (U(h) = 0.260069*h^0.173397)
Recommendation: 2 clients, utility 1.00412:
    h1: 4721.91 MB (U(h) = 0.174664*h^0.194939)
    h2: 4200.09 MB (U(h) = 0.260069*h^0.173397)
Reading from 12954: heap size 412 MB, throughput 0.984103
Reading from 12953: heap size 1109 MB, throughput 0.790675
Reading from 12954: heap size 409 MB, throughput 0.984339
Reading from 12953: heap size 1194 MB, throughput 0.962174
Reading from 12954: heap size 412 MB, throughput 0.979541
Reading from 12953: heap size 1196 MB, throughput 0.973735
Reading from 12954: heap size 410 MB, throughput 0.97975
Reading from 12953: heap size 1211 MB, throughput 0.974491
Reading from 12954: heap size 412 MB, throughput 0.981619
Reading from 12953: heap size 1215 MB, throughput 0.975863
Reading from 12954: heap size 409 MB, throughput 0.980845
Numeric result:
Recommendation: 2 clients, utility 1.21983:
    h1: 5339.5 MB (U(h) = 0.107735*h^0.270559)
    h2: 3582.5 MB (U(h) = 0.251346*h^0.181522)
Recommendation: 2 clients, utility 1.21983:
    h1: 5339.59 MB (U(h) = 0.107735*h^0.270559)
    h2: 3582.41 MB (U(h) = 0.251346*h^0.181522)
Reading from 12953: heap size 1216 MB, throughput 0.975609
Reading from 12954: heap size 411 MB, throughput 0.980702
Reading from 12953: heap size 1220 MB, throughput 0.973166
Reading from 12954: heap size 408 MB, throughput 0.986772
Reading from 12953: heap size 1210 MB, throughput 0.972775
Reading from 12954: heap size 410 MB, throughput 0.985233
Reading from 12954: heap size 413 MB, throughput 0.97432
Reading from 12954: heap size 413 MB, throughput 0.955679
Reading from 12954: heap size 419 MB, throughput 0.932601
Reading from 12953: heap size 1107 MB, throughput 0.96715
Reading from 12954: heap size 420 MB, throughput 0.976891
Reading from 12953: heap size 1199 MB, throughput 0.966828
Reading from 12954: heap size 427 MB, throughput 0.983969
Reading from 12953: heap size 1125 MB, throughput 0.966218
Reading from 12954: heap size 428 MB, throughput 0.987512
Numeric result:
Recommendation: 2 clients, utility 1.91401:
    h1: 6182.88 MB (U(h) = 0.032146*h^0.453551)
    h2: 2739.12 MB (U(h) = 0.231563*h^0.20091)
Recommendation: 2 clients, utility 1.91401:
    h1: 6183.08 MB (U(h) = 0.032146*h^0.453551)
    h2: 2738.92 MB (U(h) = 0.231563*h^0.20091)
Reading from 12953: heap size 1195 MB, throughput 0.969103
Reading from 12954: heap size 429 MB, throughput 0.987486
Reading from 12953: heap size 1200 MB, throughput 0.968297
Reading from 12954: heap size 431 MB, throughput 0.987366
Reading from 12953: heap size 1202 MB, throughput 0.960459
Reading from 12954: heap size 429 MB, throughput 0.987924
Reading from 12953: heap size 1203 MB, throughput 0.961679
Reading from 12954: heap size 431 MB, throughput 0.987074
Reading from 12953: heap size 1207 MB, throughput 0.958172
Reading from 12954: heap size 431 MB, throughput 0.986251
Numeric result:
Recommendation: 2 clients, utility 2.8354:
    h1: 6732.72 MB (U(h) = 0.00945146*h^0.635482)
    h2: 2189.28 MB (U(h) = 0.225929*h^0.206687)
Recommendation: 2 clients, utility 2.8354:
    h1: 6732.34 MB (U(h) = 0.00945146*h^0.635482)
    h2: 2189.66 MB (U(h) = 0.225929*h^0.206687)
Reading from 12953: heap size 1211 MB, throughput 0.954554
Reading from 12954: heap size 432 MB, throughput 0.981842
Reading from 12953: heap size 1218 MB, throughput 0.951734
Reading from 12954: heap size 434 MB, throughput 0.986082
Reading from 12953: heap size 1222 MB, throughput 0.95074
Reading from 12954: heap size 434 MB, throughput 0.980637
Reading from 12954: heap size 433 MB, throughput 0.96964
Reading from 12954: heap size 435 MB, throughput 0.953392
Reading from 12954: heap size 441 MB, throughput 0.978892
Reading from 12953: heap size 1230 MB, throughput 0.949333
Reading from 12954: heap size 442 MB, throughput 0.986815
Reading from 12953: heap size 1232 MB, throughput 0.948287
Numeric result:
Recommendation: 2 clients, utility 3.55614:
    h1: 6836.62 MB (U(h) = 0.00522065*h^0.723117)
    h2: 2085.38 MB (U(h) = 0.21291*h^0.220542)
Recommendation: 2 clients, utility 3.55614:
    h1: 6836.85 MB (U(h) = 0.00522065*h^0.723117)
    h2: 2085.15 MB (U(h) = 0.21291*h^0.220542)
Reading from 12954: heap size 447 MB, throughput 0.988166
Reading from 12953: heap size 1240 MB, throughput 0.95206
Reading from 12954: heap size 448 MB, throughput 0.988683
Reading from 12953: heap size 1241 MB, throughput 0.949357
Reading from 12954: heap size 448 MB, throughput 0.98903
Reading from 12953: heap size 1248 MB, throughput 0.949367
Reading from 12954: heap size 450 MB, throughput 0.987674
Reading from 12953: heap size 1249 MB, throughput 0.950314
Numeric result:
Recommendation: 2 clients, utility 3.53256:
    h1: 6757.58 MB (U(h) = 0.00580771*h^0.707948)
    h2: 2164.42 MB (U(h) = 0.207284*h^0.226732)
Recommendation: 2 clients, utility 3.53256:
    h1: 6757.73 MB (U(h) = 0.00580771*h^0.707948)
    h2: 2164.27 MB (U(h) = 0.207284*h^0.226732)
Reading from 12954: heap size 448 MB, throughput 0.987522
Reading from 12953: heap size 1257 MB, throughput 0.949988
Reading from 12954: heap size 450 MB, throughput 0.985734
Reading from 12953: heap size 1257 MB, throughput 0.952066
Reading from 12954: heap size 452 MB, throughput 0.989456
Reading from 12953: heap size 1264 MB, throughput 0.952076
Reading from 12954: heap size 453 MB, throughput 0.985264
Reading from 12954: heap size 451 MB, throughput 0.976145
Reading from 12954: heap size 454 MB, throughput 0.961113
Reading from 12953: heap size 1265 MB, throughput 0.951931
Reading from 12954: heap size 460 MB, throughput 0.983604
Numeric result:
Recommendation: 2 clients, utility 4.16013:
    h1: 6850.39 MB (U(h) = 0.00360312*h^0.777428)
    h2: 2071.61 MB (U(h) = 0.199882*h^0.235099)
Recommendation: 2 clients, utility 4.16013:
    h1: 6850.4 MB (U(h) = 0.00360312*h^0.777428)
    h2: 2071.6 MB (U(h) = 0.199882*h^0.235099)
Reading from 12954: heap size 461 MB, throughput 0.983587
Reading from 12953: heap size 1272 MB, throughput 0.951316
Reading from 12954: heap size 466 MB, throughput 0.987516
Reading from 12953: heap size 1272 MB, throughput 0.951281
Reading from 12954: heap size 467 MB, throughput 0.988875
Reading from 12953: heap size 1280 MB, throughput 0.929595
Reading from 12954: heap size 466 MB, throughput 0.989819
Numeric result:
Recommendation: 2 clients, utility 4.6071:
    h1: 6909.05 MB (U(h) = 0.00263781*h^0.822594)
    h2: 2012.95 MB (U(h) = 0.195918*h^0.239669)
Recommendation: 2 clients, utility 4.6071:
    h1: 6909.01 MB (U(h) = 0.00263781*h^0.822594)
    h2: 2012.99 MB (U(h) = 0.195918*h^0.239669)
Reading from 12953: heap size 1385 MB, throughput 0.950223
Reading from 12954: heap size 468 MB, throughput 0.988727
Reading from 12953: heap size 1384 MB, throughput 0.965446
Reading from 12954: heap size 468 MB, throughput 0.987187
Reading from 12953: heap size 1393 MB, throughput 0.971438
Reading from 12954: heap size 469 MB, throughput 0.990372
Reading from 12954: heap size 469 MB, throughput 0.986643
Reading from 12954: heap size 471 MB, throughput 0.976797
Reading from 12954: heap size 474 MB, throughput 0.971226
Reading from 12953: heap size 1404 MB, throughput 0.974123
Reading from 12954: heap size 476 MB, throughput 0.988561
Numeric result:
Recommendation: 2 clients, utility 4.95387:
    h1: 6922.34 MB (U(h) = 0.00214519*h^0.851869)
    h2: 1999.66 MB (U(h) = 0.190473*h^0.246065)
Recommendation: 2 clients, utility 4.95387:
    h1: 6922.43 MB (U(h) = 0.00214519*h^0.851869)
    h2: 1999.57 MB (U(h) = 0.190473*h^0.246065)
Reading from 12953: heap size 1405 MB, throughput 0.973674
Reading from 12954: heap size 480 MB, throughput 0.990459
Reading from 12953: heap size 1401 MB, throughput 0.971823
Reading from 12954: heap size 481 MB, throughput 0.990075
Reading from 12953: heap size 1406 MB, throughput 0.969449
Reading from 12954: heap size 480 MB, throughput 0.990087
Reading from 12953: heap size 1392 MB, throughput 0.968765
Numeric result:
Recommendation: 2 clients, utility 4.40639:
    h1: 6780.88 MB (U(h) = 0.00336276*h^0.786847)
    h2: 2141.12 MB (U(h) = 0.188456*h^0.248463)
Recommendation: 2 clients, utility 4.40639:
    h1: 6780.81 MB (U(h) = 0.00336276*h^0.786847)
    h2: 2141.19 MB (U(h) = 0.188456*h^0.248463)
Reading from 12954: heap size 483 MB, throughput 0.989371
Reading from 12953: heap size 1400 MB, throughput 0.972511
Reading from 12954: heap size 483 MB, throughput 0.988871
Reading from 12954: heap size 484 MB, throughput 0.99115
Reading from 12954: heap size 486 MB, throughput 0.986306
Reading from 12954: heap size 487 MB, throughput 0.975741
Reading from 12954: heap size 490 MB, throughput 0.971105
Numeric result:
Recommendation: 2 clients, utility 8.41782:
    h1: 5278.6 MB (U(h) = 0.00338955*h^0.785632)
    h2: 3643.4 MB (U(h) = 0.0345991*h^0.542316)
Recommendation: 2 clients, utility 8.41782:
    h1: 5278.38 MB (U(h) = 0.00338955*h^0.785632)
    h2: 3643.62 MB (U(h) = 0.0345991*h^0.542316)
Reading from 12954: heap size 492 MB, throughput 0.98862
Reading from 12954: heap size 495 MB, throughput 0.99061
Reading from 12954: heap size 497 MB, throughput 0.987397
Reading from 12953: heap size 1394 MB, throughput 0.916786
Reading from 12954: heap size 496 MB, throughput 0.989776
Numeric result:
Recommendation: 2 clients, utility 14.0965:
    h1: 4653.29 MB (U(h) = 0.00286551*h^0.809373)
    h2: 4268.71 MB (U(h) = 0.0106623*h^0.742494)
Recommendation: 2 clients, utility 14.0965:
    h1: 4653.25 MB (U(h) = 0.00286551*h^0.809373)
    h2: 4268.75 MB (U(h) = 0.0106623*h^0.742494)
Reading from 12954: heap size 498 MB, throughput 0.989537
Reading from 12954: heap size 500 MB, throughput 0.988773
Reading from 12954: heap size 500 MB, throughput 0.991048
Reading from 12954: heap size 503 MB, throughput 0.985588
Reading from 12954: heap size 504 MB, throughput 0.975096
Numeric result:
Recommendation: 2 clients, utility 21.1872:
    h1: 4202.63 MB (U(h) = 0.00286551*h^0.809373)
    h2: 4719.37 MB (U(h) = 0.00395553*h^0.908817)
Recommendation: 2 clients, utility 21.1872:
    h1: 4202.81 MB (U(h) = 0.00286551*h^0.809373)
    h2: 4719.19 MB (U(h) = 0.00395553*h^0.908817)
Reading from 12954: heap size 509 MB, throughput 0.987156
Reading from 12953: heap size 1542 MB, throughput 0.986326
Reading from 12954: heap size 511 MB, throughput 0.991086
Reading from 12954: heap size 513 MB, throughput 0.991465
Reading from 12953: heap size 1581 MB, throughput 0.984765
Reading from 12953: heap size 1499 MB, throughput 0.974302
Reading from 12953: heap size 1569 MB, throughput 0.956629
Reading from 12953: heap size 1594 MB, throughput 0.929063
Reading from 12953: heap size 1621 MB, throughput 0.887305
Reading from 12953: heap size 1636 MB, throughput 0.839194
Reading from 12953: heap size 1669 MB, throughput 0.80434
Numeric result:
Recommendation: 2 clients, utility 15.9922:
    h1: 2685.69 MB (U(h) = 0.0413328*h^0.430231)
    h2: 6236.31 MB (U(h) = 0.00209505*h^0.999)
Recommendation: 2 clients, utility 15.9922:
    h1: 2685.73 MB (U(h) = 0.0413328*h^0.430231)
    h2: 6236.27 MB (U(h) = 0.00209505*h^0.999)
Reading from 12954: heap size 515 MB, throughput 0.990459
Reading from 12953: heap size 1678 MB, throughput 0.913857
Reading from 12954: heap size 514 MB, throughput 0.990234
Reading from 12953: heap size 1696 MB, throughput 0.957874
Reading from 12954: heap size 516 MB, throughput 0.989185
Reading from 12953: heap size 1707 MB, throughput 0.964342
Reading from 12954: heap size 518 MB, throughput 0.982687
Reading from 12954: heap size 549 MB, throughput 0.987346
Reading from 12954: heap size 549 MB, throughput 0.987454
Numeric result:
Recommendation: 2 clients, utility 2.98718:
    h1: 4560.25 MB (U(h) = 0.0687009*h^0.359166)
    h2: 4361.75 MB (U(h) = 0.118515*h^0.343533)
Recommendation: 2 clients, utility 2.98718:
    h1: 4560.25 MB (U(h) = 0.0687009*h^0.359166)
    h2: 4361.75 MB (U(h) = 0.118515*h^0.343533)
Reading from 12953: heap size 1702 MB, throughput 0.966577
Reading from 12954: heap size 552 MB, throughput 0.993519
Reading from 12953: heap size 1715 MB, throughput 0.970602
Reading from 12954: heap size 555 MB, throughput 0.99385
Reading from 12953: heap size 1704 MB, throughput 0.970919
Reading from 12954: heap size 556 MB, throughput 0.989096
Reading from 12953: heap size 1717 MB, throughput 0.968355
Numeric result:
Recommendation: 2 clients, utility 3.13217:
    h1: 4373.66 MB (U(h) = 0.0712027*h^0.353994)
    h2: 4548.34 MB (U(h) = 0.101795*h^0.36819)
Recommendation: 2 clients, utility 3.13217:
    h1: 4373.31 MB (U(h) = 0.0712027*h^0.353994)
    h2: 4548.69 MB (U(h) = 0.101795*h^0.36819)
Reading from 12954: heap size 555 MB, throughput 0.989898
Reading from 12953: heap size 1707 MB, throughput 0.971009
Reading from 12954: heap size 556 MB, throughput 0.987577
Reading from 12953: heap size 1717 MB, throughput 0.971519
Reading from 12954: heap size 556 MB, throughput 0.98857
Numeric result:
Recommendation: 2 clients, utility 3.13275:
    h1: 4376.51 MB (U(h) = 0.0710189*h^0.354369)
    h2: 4545.49 MB (U(h) = 0.101879*h^0.368047)
Recommendation: 2 clients, utility 3.13275:
    h1: 4376.54 MB (U(h) = 0.0710189*h^0.354369)
    h2: 4545.46 MB (U(h) = 0.101879*h^0.368047)
Reading from 12954: heap size 560 MB, throughput 0.97719
Reading from 12953: heap size 1722 MB, throughput 0.969589
Reading from 12954: heap size 560 MB, throughput 0.968118
Reading from 12954: heap size 568 MB, throughput 0.981019
Reading from 12953: heap size 1727 MB, throughput 0.973308
Reading from 12954: heap size 576 MB, throughput 0.988231
Reading from 12953: heap size 1717 MB, throughput 0.973328
Reading from 12954: heap size 580 MB, throughput 0.989586
Numeric result:
Recommendation: 2 clients, utility 3.22465:
    h1: 4397.79 MB (U(h) = 0.0651399*h^0.366222)
    h2: 4524.21 MB (U(h) = 0.0961615*h^0.376798)
Recommendation: 2 clients, utility 3.22465:
    h1: 4397.5 MB (U(h) = 0.0651399*h^0.366222)
    h2: 4524.5 MB (U(h) = 0.0961615*h^0.376798)
Reading from 12953: heap size 1727 MB, throughput 0.970562
Reading from 12954: heap size 585 MB, throughput 0.989315
Reading from 12953: heap size 1734 MB, throughput 0.96749
Reading from 12954: heap size 586 MB, throughput 0.987921
Reading from 12953: heap size 1735 MB, throughput 0.95834
Reading from 12954: heap size 590 MB, throughput 0.973225
Numeric result:
Recommendation: 2 clients, utility 3.32923:
    h1: 4402.64 MB (U(h) = 0.0600469*h^0.377298)
    h2: 4519.36 MB (U(h) = 0.0898292*h^0.387316)
Recommendation: 2 clients, utility 3.32923:
    h1: 4402.56 MB (U(h) = 0.0600469*h^0.377298)
    h2: 4519.44 MB (U(h) = 0.0898292*h^0.387316)
Reading from 12954: heap size 593 MB, throughput 0.976416
Reading from 12954: heap size 599 MB, throughput 0.969083
Reading from 12953: heap size 1745 MB, throughput 0.959203
Reading from 12954: heap size 600 MB, throughput 0.979968
Reading from 12954: heap size 605 MB, throughput 0.988008
Reading from 12953: heap size 1752 MB, throughput 0.959225
Reading from 12954: heap size 608 MB, throughput 0.989635
Numeric result:
Recommendation: 2 clients, utility 3.23805:
    h1: 4684.97 MB (U(h) = 0.0497957*h^0.403066)
    h2: 4237.03 MB (U(h) = 0.102663*h^0.364521)
Recommendation: 2 clients, utility 3.23805:
    h1: 4685.01 MB (U(h) = 0.0497957*h^0.403066)
    h2: 4236.99 MB (U(h) = 0.102663*h^0.364521)
Reading from 12953: heap size 1768 MB, throughput 0.960416
Reading from 12954: heap size 608 MB, throughput 0.990781
Reading from 12953: heap size 1775 MB, throughput 0.959308
Reading from 12954: heap size 611 MB, throughput 0.990684
Reading from 12953: heap size 1792 MB, throughput 0.959136
Numeric result:
Recommendation: 2 clients, utility 3.00588:
    h1: 4901.25 MB (U(h) = 0.0491503*h^0.404328)
    h2: 4020.75 MB (U(h) = 0.125552*h^0.331694)
Recommendation: 2 clients, utility 3.00588:
    h1: 4901.23 MB (U(h) = 0.0491503*h^0.404328)
    h2: 4020.77 MB (U(h) = 0.125552*h^0.331694)
Reading from 12954: heap size 608 MB, throughput 0.993032
Reading from 12954: heap size 611 MB, throughput 0.988705
Reading from 12954: heap size 615 MB, throughput 0.985378
Reading from 12953: heap size 1798 MB, throughput 0.958602
Reading from 12954: heap size 615 MB, throughput 0.991695
Reading from 12953: heap size 1815 MB, throughput 0.951127
Reading from 12954: heap size 617 MB, throughput 0.99255
Numeric result:
Recommendation: 2 clients, utility 2.73808:
    h1: 5158.85 MB (U(h) = 0.0506654*h^0.399795)
    h2: 3763.15 MB (U(h) = 0.16048*h^0.291718)
Recommendation: 2 clients, utility 2.73808:
    h1: 5158.21 MB (U(h) = 0.0506654*h^0.399795)
    h2: 3763.79 MB (U(h) = 0.16048*h^0.291718)
Reading from 12953: heap size 1760 MB, throughput 0.975042
Reading from 12954: heap size 619 MB, throughput 0.992687
Reading from 12953: heap size 1768 MB, throughput 0.981523
Reading from 12954: heap size 617 MB, throughput 0.992082
Numeric result:
Recommendation: 2 clients, utility 2.65145:
    h1: 5304.23 MB (U(h) = 0.0496432*h^0.402558)
    h2: 3617.77 MB (U(h) = 0.178331*h^0.274564)
Recommendation: 2 clients, utility 2.65145:
    h1: 5304.25 MB (U(h) = 0.0496432*h^0.402558)
    h2: 3617.75 MB (U(h) = 0.178331*h^0.274564)
Reading from 12953: heap size 1782 MB, throughput 0.98353
Reading from 12954: heap size 620 MB, throughput 0.991538
Reading from 12954: heap size 617 MB, throughput 0.988424
Reading from 12954: heap size 619 MB, throughput 0.98387
Reading from 12953: heap size 1803 MB, throughput 0.983259
Reading from 12954: heap size 625 MB, throughput 0.991385
Numeric result:
Recommendation: 2 clients, utility 2.57449:
    h1: 5451.13 MB (U(h) = 0.0482944*h^0.406201)
    h2: 3470.87 MB (U(h) = 0.196576*h^0.258584)
Recommendation: 2 clients, utility 2.57449:
    h1: 5451.57 MB (U(h) = 0.0482944*h^0.406201)
    h2: 3470.43 MB (U(h) = 0.196576*h^0.258584)
Reading from 12954: heap size 625 MB, throughput 0.992387
Reading from 12954: heap size 625 MB, throughput 0.992082
Reading from 12954: heap size 628 MB, throughput 0.991558
Numeric result:
Recommendation: 2 clients, utility 2.51517:
    h1: 5549.36 MB (U(h) = 0.0482944*h^0.406201)
    h2: 3372.64 MB (U(h) = 0.211254*h^0.24687)
Recommendation: 2 clients, utility 2.51517:
    h1: 5549.36 MB (U(h) = 0.0482944*h^0.406201)
    h2: 3372.64 MB (U(h) = 0.211254*h^0.24687)
Reading from 12954: heap size 629 MB, throughput 0.993046
Reading from 12954: heap size 630 MB, throughput 0.9885
Client 12954 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 12953: heap size 1804 MB, throughput 0.995802
Recommendation: one client; give it all the memory
Reading from 12953: heap size 1787 MB, throughput 0.993932
Reading from 12953: heap size 1800 MB, throughput 0.986259
Reading from 12953: heap size 1791 MB, throughput 0.97536
Reading from 12953: heap size 1818 MB, throughput 0.954738
Client 12953 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
