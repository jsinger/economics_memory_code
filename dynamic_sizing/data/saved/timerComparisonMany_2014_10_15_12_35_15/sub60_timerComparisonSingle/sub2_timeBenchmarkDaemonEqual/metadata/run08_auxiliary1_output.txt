economemd
    total memory: 1728 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub60_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 19951: heap size 9 MB, throughput 0.988731
Clients: 1
Client 19951 has a minimum heap size of 12 MB
Reading from 19950: heap size 9 MB, throughput 0.991892
Clients: 2
Client 19950 has a minimum heap size of 276 MB
Reading from 19951: heap size 9 MB, throughput 0.992123
Reading from 19950: heap size 9 MB, throughput 0.983676
Reading from 19951: heap size 9 MB, throughput 0.948083
Reading from 19950: heap size 9 MB, throughput 0.937295
Reading from 19950: heap size 9 MB, throughput 0.946621
Reading from 19951: heap size 9 MB, throughput 0.973795
Reading from 19951: heap size 11 MB, throughput 0.978822
Reading from 19950: heap size 11 MB, throughput 0.984689
Reading from 19951: heap size 11 MB, throughput 0.97718
Reading from 19950: heap size 11 MB, throughput 0.975092
Reading from 19951: heap size 16 MB, throughput 0.982468
Reading from 19950: heap size 17 MB, throughput 0.913249
Reading from 19950: heap size 17 MB, throughput 0.586907
Reading from 19951: heap size 16 MB, throughput 0.98819
Reading from 19950: heap size 30 MB, throughput 0.948169
Reading from 19950: heap size 31 MB, throughput 0.883394
Reading from 19951: heap size 23 MB, throughput 0.881914
Reading from 19950: heap size 34 MB, throughput 0.475793
Reading from 19950: heap size 47 MB, throughput 0.863914
Reading from 19950: heap size 49 MB, throughput 0.784031
Reading from 19951: heap size 29 MB, throughput 0.98645
Reading from 19950: heap size 51 MB, throughput 0.235826
Reading from 19950: heap size 77 MB, throughput 0.821734
Reading from 19950: heap size 77 MB, throughput 0.776072
Reading from 19951: heap size 32 MB, throughput 0.988203
Reading from 19951: heap size 36 MB, throughput 0.989424
Reading from 19950: heap size 80 MB, throughput 0.20308
Reading from 19950: heap size 108 MB, throughput 0.799578
Reading from 19951: heap size 37 MB, throughput 0.988334
Reading from 19950: heap size 111 MB, throughput 0.735302
Reading from 19951: heap size 37 MB, throughput 0.986501
Reading from 19950: heap size 113 MB, throughput 0.785548
Reading from 19951: heap size 41 MB, throughput 0.978374
Reading from 19951: heap size 41 MB, throughput 0.985097
Reading from 19951: heap size 45 MB, throughput 0.989726
Reading from 19950: heap size 120 MB, throughput 0.254536
Reading from 19950: heap size 147 MB, throughput 0.730628
Reading from 19951: heap size 45 MB, throughput 0.973606
Reading from 19950: heap size 157 MB, throughput 0.822204
Reading from 19951: heap size 48 MB, throughput 0.982201
Reading from 19950: heap size 158 MB, throughput 0.715439
Reading from 19951: heap size 48 MB, throughput 0.978697
Reading from 19951: heap size 53 MB, throughput 0.512708
Reading from 19951: heap size 57 MB, throughput 0.989119
Reading from 19950: heap size 161 MB, throughput 0.159188
Reading from 19951: heap size 66 MB, throughput 0.991931
Reading from 19950: heap size 199 MB, throughput 0.578299
Reading from 19950: heap size 202 MB, throughput 0.675644
Reading from 19951: heap size 66 MB, throughput 0.995276
Reading from 19950: heap size 207 MB, throughput 0.894863
Reading from 19950: heap size 213 MB, throughput 0.894579
Reading from 19951: heap size 74 MB, throughput 0.995685
Reading from 19950: heap size 219 MB, throughput 0.801138
Reading from 19950: heap size 224 MB, throughput 0.807442
Reading from 19950: heap size 230 MB, throughput 0.581309
Reading from 19950: heap size 236 MB, throughput 0.729872
Reading from 19950: heap size 240 MB, throughput 0.761576
Reading from 19951: heap size 75 MB, throughput 0.997199
Reading from 19950: heap size 245 MB, throughput 0.845472
Reading from 19950: heap size 248 MB, throughput 0.763357
Reading from 19950: heap size 249 MB, throughput 0.587756
Reading from 19950: heap size 254 MB, throughput 0.668349
Reading from 19950: heap size 260 MB, throughput 0.597159
Reading from 19951: heap size 80 MB, throughput 0.994644
Reading from 19951: heap size 81 MB, throughput 0.994171
Reading from 19950: heap size 263 MB, throughput 0.106261
Reading from 19950: heap size 305 MB, throughput 0.653912
Reading from 19951: heap size 85 MB, throughput 0.996347
Reading from 19950: heap size 306 MB, throughput 0.951919
Reading from 19951: heap size 86 MB, throughput 0.995122
Reading from 19950: heap size 299 MB, throughput 0.146493
Reading from 19950: heap size 347 MB, throughput 0.636581
Reading from 19950: heap size 348 MB, throughput 0.84916
Reading from 19950: heap size 350 MB, throughput 0.810139
Reading from 19950: heap size 355 MB, throughput 0.810785
Reading from 19951: heap size 89 MB, throughput 0.995565
Reading from 19950: heap size 355 MB, throughput 0.855892
Reading from 19950: heap size 356 MB, throughput 0.757326
Reading from 19950: heap size 357 MB, throughput 0.616318
Reading from 19950: heap size 354 MB, throughput 0.728439
Reading from 19950: heap size 356 MB, throughput 0.749991
Reading from 19950: heap size 357 MB, throughput 0.651008
Reading from 19951: heap size 90 MB, throughput 0.996032
Reading from 19950: heap size 359 MB, throughput 0.556308
Equal recommendation: 864 MB each
Reading from 19951: heap size 93 MB, throughput 0.995769
Reading from 19950: heap size 366 MB, throughput 0.920046
Reading from 19951: heap size 93 MB, throughput 0.995535
Reading from 19951: heap size 95 MB, throughput 0.996221
Reading from 19950: heap size 366 MB, throughput 0.977955
Reading from 19951: heap size 96 MB, throughput 0.992743
Reading from 19951: heap size 98 MB, throughput 0.994081
Reading from 19951: heap size 98 MB, throughput 0.995725
Reading from 19950: heap size 368 MB, throughput 0.980964
Reading from 19951: heap size 101 MB, throughput 0.995747
Reading from 19951: heap size 101 MB, throughput 0.996324
Reading from 19950: heap size 371 MB, throughput 0.984164
Reading from 19951: heap size 103 MB, throughput 0.996625
Reading from 19951: heap size 104 MB, throughput 0.997413
Reading from 19951: heap size 105 MB, throughput 0.997532
Reading from 19950: heap size 372 MB, throughput 0.982963
Reading from 19951: heap size 105 MB, throughput 0.996962
Reading from 19951: heap size 107 MB, throughput 0.997835
Reading from 19950: heap size 375 MB, throughput 0.983183
Reading from 19951: heap size 107 MB, throughput 0.997967
Reading from 19951: heap size 108 MB, throughput 0.997687
Reading from 19950: heap size 372 MB, throughput 0.97605
Reading from 19951: heap size 109 MB, throughput 0.998174
Equal recommendation: 864 MB each
Reading from 19951: heap size 109 MB, throughput 0.998277
Reading from 19950: heap size 375 MB, throughput 0.977931
Reading from 19951: heap size 110 MB, throughput 0.997918
Reading from 19951: heap size 111 MB, throughput 0.998342
Reading from 19951: heap size 111 MB, throughput 0.99791
Reading from 19950: heap size 371 MB, throughput 0.980655
Reading from 19951: heap size 113 MB, throughput 0.998418
Reading from 19951: heap size 113 MB, throughput 0.990441
Reading from 19951: heap size 114 MB, throughput 0.992857
Reading from 19951: heap size 114 MB, throughput 0.986871
Reading from 19950: heap size 373 MB, throughput 0.962222
Reading from 19951: heap size 117 MB, throughput 0.995598
Reading from 19951: heap size 118 MB, throughput 0.996106
Reading from 19950: heap size 375 MB, throughput 0.97799
Reading from 19951: heap size 121 MB, throughput 0.997132
Reading from 19951: heap size 121 MB, throughput 0.996996
Reading from 19950: heap size 375 MB, throughput 0.979356
Reading from 19951: heap size 123 MB, throughput 0.99689
Reading from 19951: heap size 124 MB, throughput 0.996696
Reading from 19950: heap size 377 MB, throughput 0.978485
Reading from 19951: heap size 126 MB, throughput 0.997319
Reading from 19951: heap size 126 MB, throughput 0.996456
Equal recommendation: 864 MB each
Reading from 19950: heap size 378 MB, throughput 0.979078
Reading from 19951: heap size 129 MB, throughput 0.997211
Reading from 19951: heap size 129 MB, throughput 0.996466
Reading from 19951: heap size 131 MB, throughput 0.996881
Reading from 19950: heap size 380 MB, throughput 0.988954
Reading from 19950: heap size 382 MB, throughput 0.833825
Reading from 19950: heap size 378 MB, throughput 0.832349
Reading from 19950: heap size 382 MB, throughput 0.760951
Reading from 19951: heap size 131 MB, throughput 0.996819
Reading from 19950: heap size 389 MB, throughput 0.825833
Reading from 19950: heap size 391 MB, throughput 0.969946
Reading from 19951: heap size 134 MB, throughput 0.997041
Reading from 19951: heap size 134 MB, throughput 0.997446
Reading from 19950: heap size 397 MB, throughput 0.990937
Reading from 19951: heap size 136 MB, throughput 0.998194
Reading from 19951: heap size 136 MB, throughput 0.997863
Reading from 19951: heap size 138 MB, throughput 0.998598
Reading from 19950: heap size 398 MB, throughput 0.990862
Reading from 19951: heap size 138 MB, throughput 0.998427
Reading from 19951: heap size 139 MB, throughput 0.998621
Reading from 19950: heap size 398 MB, throughput 0.990285
Reading from 19951: heap size 139 MB, throughput 0.998722
Equal recommendation: 864 MB each
Reading from 19951: heap size 140 MB, throughput 0.998481
Reading from 19950: heap size 401 MB, throughput 0.987174
Reading from 19951: heap size 140 MB, throughput 0.997017
Reading from 19951: heap size 142 MB, throughput 0.993292
Reading from 19951: heap size 142 MB, throughput 0.989606
Reading from 19950: heap size 399 MB, throughput 0.988001
Reading from 19951: heap size 144 MB, throughput 0.995615
Reading from 19951: heap size 145 MB, throughput 0.997371
Reading from 19950: heap size 401 MB, throughput 0.985773
Reading from 19951: heap size 147 MB, throughput 0.997357
Reading from 19951: heap size 148 MB, throughput 0.997453
Reading from 19950: heap size 397 MB, throughput 0.986754
Reading from 19951: heap size 150 MB, throughput 0.997298
Reading from 19951: heap size 150 MB, throughput 0.995766
Reading from 19950: heap size 400 MB, throughput 0.987389
Reading from 19951: heap size 153 MB, throughput 0.995901
Reading from 19951: heap size 153 MB, throughput 0.997266
Equal recommendation: 864 MB each
Reading from 19950: heap size 395 MB, throughput 0.986879
Reading from 19951: heap size 156 MB, throughput 0.997726
Reading from 19951: heap size 156 MB, throughput 0.996484
Reading from 19951: heap size 158 MB, throughput 0.994476
Reading from 19950: heap size 398 MB, throughput 0.77623
Reading from 19951: heap size 158 MB, throughput 0.997279
Reading from 19951: heap size 161 MB, throughput 0.997345
Reading from 19950: heap size 434 MB, throughput 0.992072
Reading from 19951: heap size 161 MB, throughput 0.922244
Reading from 19950: heap size 435 MB, throughput 0.986112
Reading from 19950: heap size 439 MB, throughput 0.918175
Reading from 19950: heap size 439 MB, throughput 0.88614
Reading from 19950: heap size 444 MB, throughput 0.896925
Reading from 19951: heap size 165 MB, throughput 0.998456
Reading from 19951: heap size 166 MB, throughput 0.998719
Reading from 19950: heap size 444 MB, throughput 0.991895
Reading from 19951: heap size 168 MB, throughput 0.998808
Reading from 19951: heap size 169 MB, throughput 0.995934
Reading from 19950: heap size 450 MB, throughput 0.990667
Reading from 19951: heap size 171 MB, throughput 0.989268
Equal recommendation: 864 MB each
Reading from 19951: heap size 171 MB, throughput 0.992583
Reading from 19951: heap size 177 MB, throughput 0.997928
Reading from 19950: heap size 451 MB, throughput 0.991974
Reading from 19951: heap size 177 MB, throughput 0.997722
Reading from 19951: heap size 182 MB, throughput 0.998083
Reading from 19950: heap size 451 MB, throughput 0.991024
Reading from 19951: heap size 182 MB, throughput 0.996947
Reading from 19951: heap size 186 MB, throughput 0.998403
Reading from 19950: heap size 453 MB, throughput 0.990137
Reading from 19951: heap size 186 MB, throughput 0.997627
Reading from 19951: heap size 190 MB, throughput 0.997537
Reading from 19950: heap size 450 MB, throughput 0.990222
Reading from 19951: heap size 190 MB, throughput 0.997453
Equal recommendation: 864 MB each
Reading from 19950: heap size 453 MB, throughput 0.988286
Reading from 19951: heap size 192 MB, throughput 0.997069
Reading from 19951: heap size 193 MB, throughput 0.997588
Reading from 19950: heap size 452 MB, throughput 0.987923
Reading from 19951: heap size 197 MB, throughput 0.997866
Reading from 19951: heap size 197 MB, throughput 0.996773
Reading from 19950: heap size 453 MB, throughput 0.986969
Reading from 19951: heap size 201 MB, throughput 0.997728
Reading from 19951: heap size 201 MB, throughput 0.996036
Reading from 19951: heap size 205 MB, throughput 0.99204
Reading from 19950: heap size 455 MB, throughput 0.992085
Reading from 19951: heap size 205 MB, throughput 0.994732
Reading from 19950: heap size 456 MB, throughput 0.978045
Reading from 19950: heap size 457 MB, throughput 0.897472
Reading from 19950: heap size 457 MB, throughput 0.879375
Reading from 19951: heap size 211 MB, throughput 0.997882
Reading from 19950: heap size 463 MB, throughput 0.984727
Equal recommendation: 864 MB each
Reading from 19951: heap size 211 MB, throughput 0.998022
Reading from 19950: heap size 465 MB, throughput 0.993295
Reading from 19951: heap size 215 MB, throughput 0.998337
Reading from 19951: heap size 216 MB, throughput 0.997873
Reading from 19950: heap size 468 MB, throughput 0.992162
Reading from 19951: heap size 220 MB, throughput 0.997285
Reading from 19951: heap size 220 MB, throughput 0.997414
Reading from 19950: heap size 469 MB, throughput 0.992624
Reading from 19951: heap size 224 MB, throughput 0.997429
Reading from 19950: heap size 468 MB, throughput 0.991286
Reading from 19951: heap size 224 MB, throughput 0.995554
Equal recommendation: 864 MB each
Reading from 19951: heap size 229 MB, throughput 0.998159
Reading from 19950: heap size 470 MB, throughput 0.990077
Reading from 19951: heap size 229 MB, throughput 0.99794
Reading from 19951: heap size 233 MB, throughput 0.998151
Reading from 19950: heap size 469 MB, throughput 0.989762
Reading from 19951: heap size 233 MB, throughput 0.996542
Reading from 19951: heap size 237 MB, throughput 0.989197
Reading from 19950: heap size 470 MB, throughput 0.989637
Reading from 19951: heap size 237 MB, throughput 0.997739
Reading from 19951: heap size 243 MB, throughput 0.99828
Reading from 19950: heap size 472 MB, throughput 0.987142
Reading from 19951: heap size 244 MB, throughput 0.998093
Equal recommendation: 864 MB each
Reading from 19950: heap size 472 MB, throughput 0.993375
Reading from 19950: heap size 475 MB, throughput 0.926168
Reading from 19950: heap size 476 MB, throughput 0.887461
Reading from 19951: heap size 250 MB, throughput 0.99785
Reading from 19950: heap size 480 MB, throughput 0.982307
Reading from 19951: heap size 250 MB, throughput 0.998042
Reading from 19950: heap size 482 MB, throughput 0.99372
Reading from 19951: heap size 254 MB, throughput 0.998411
Reading from 19951: heap size 255 MB, throughput 0.997033
Reading from 19950: heap size 485 MB, throughput 0.991798
Reading from 19951: heap size 259 MB, throughput 0.998246
Reading from 19951: heap size 259 MB, throughput 0.99737
Reading from 19950: heap size 487 MB, throughput 0.992584
Equal recommendation: 864 MB each
Reading from 19951: heap size 263 MB, throughput 0.997416
Reading from 19950: heap size 485 MB, throughput 0.986221
Reading from 19951: heap size 263 MB, throughput 0.993687
Reading from 19951: heap size 267 MB, throughput 0.99619
Reading from 19950: heap size 487 MB, throughput 0.990788
Reading from 19951: heap size 268 MB, throughput 0.997286
Reading from 19951: heap size 275 MB, throughput 0.998098
Reading from 19950: heap size 486 MB, throughput 0.991427
Reading from 19951: heap size 275 MB, throughput 0.998216
Reading from 19950: heap size 488 MB, throughput 0.990461
Reading from 19951: heap size 280 MB, throughput 0.997952
Equal recommendation: 864 MB each
Reading from 19951: heap size 280 MB, throughput 0.998117
Reading from 19950: heap size 490 MB, throughput 0.994862
Reading from 19950: heap size 491 MB, throughput 0.933891
Reading from 19950: heap size 490 MB, throughput 0.91481
Reading from 19951: heap size 285 MB, throughput 0.998477
Reading from 19950: heap size 492 MB, throughput 0.985501
Reading from 19951: heap size 285 MB, throughput 0.998635
Reading from 19950: heap size 500 MB, throughput 0.994432
Reading from 19951: heap size 289 MB, throughput 0.998488
Reading from 19951: heap size 289 MB, throughput 0.997272
Reading from 19951: heap size 293 MB, throughput 0.992236
Reading from 19950: heap size 500 MB, throughput 0.993267
Equal recommendation: 864 MB each
Reading from 19951: heap size 293 MB, throughput 0.998229
Reading from 19950: heap size 500 MB, throughput 0.992943
Reading from 19951: heap size 299 MB, throughput 0.998211
Reading from 19950: heap size 502 MB, throughput 0.992249
Reading from 19951: heap size 300 MB, throughput 0.998373
Reading from 19951: heap size 305 MB, throughput 0.998164
Reading from 19950: heap size 500 MB, throughput 0.991639
Reading from 19951: heap size 306 MB, throughput 0.998247
Equal recommendation: 864 MB each
Reading from 19950: heap size 502 MB, throughput 0.991149
Reading from 19951: heap size 311 MB, throughput 0.99888
Reading from 19950: heap size 503 MB, throughput 0.987239
Reading from 19951: heap size 311 MB, throughput 0.998555
Reading from 19950: heap size 504 MB, throughput 0.98675
Reading from 19950: heap size 507 MB, throughput 0.919539
Reading from 19950: heap size 509 MB, throughput 0.945945
Reading from 19951: heap size 315 MB, throughput 0.998566
Reading from 19951: heap size 315 MB, throughput 0.918947
Reading from 19951: heap size 325 MB, throughput 0.997906
Reading from 19950: heap size 515 MB, throughput 0.994912
Reading from 19951: heap size 326 MB, throughput 0.999079
Equal recommendation: 864 MB each
Reading from 19950: heap size 515 MB, throughput 0.993329
Reading from 19951: heap size 332 MB, throughput 0.999118
Reading from 19951: heap size 333 MB, throughput 0.99906
Reading from 19950: heap size 516 MB, throughput 0.994349
Reading from 19951: heap size 337 MB, throughput 0.997246
Reading from 19950: heap size 518 MB, throughput 0.992309
Reading from 19951: heap size 338 MB, throughput 0.998314
Reading from 19950: heap size 515 MB, throughput 0.989893
Equal recommendation: 864 MB each
Reading from 19951: heap size 344 MB, throughput 0.998546
Reading from 19951: heap size 344 MB, throughput 0.999073
Reading from 19950: heap size 517 MB, throughput 0.986205
Reading from 19951: heap size 349 MB, throughput 0.994349
Reading from 19951: heap size 349 MB, throughput 0.99846
Reading from 19950: heap size 519 MB, throughput 0.995132
Reading from 19950: heap size 520 MB, throughput 0.979384
Reading from 19950: heap size 520 MB, throughput 0.921109
Reading from 19951: heap size 358 MB, throughput 0.998575
Reading from 19950: heap size 522 MB, throughput 0.985724
Equal recommendation: 864 MB each
Reading from 19951: heap size 359 MB, throughput 0.998534
Reading from 19950: heap size 528 MB, throughput 0.993914
Reading from 19951: heap size 365 MB, throughput 0.998645
Reading from 19950: heap size 529 MB, throughput 0.992472
Reading from 19951: heap size 365 MB, throughput 0.99748
Reading from 19951: heap size 372 MB, throughput 0.998269
Reading from 19950: heap size 528 MB, throughput 0.993969
Reading from 19951: heap size 372 MB, throughput 0.998323
Equal recommendation: 864 MB each
Reading from 19951: heap size 377 MB, throughput 0.993661
Reading from 19950: heap size 530 MB, throughput 0.993135
Reading from 19951: heap size 378 MB, throughput 0.998339
Reading from 19950: heap size 528 MB, throughput 0.992597
Reading from 19951: heap size 387 MB, throughput 0.998439
Reading from 19950: heap size 530 MB, throughput 0.990323
Reading from 19951: heap size 388 MB, throughput 0.998365
Reading from 19950: heap size 532 MB, throughput 0.991343
Reading from 19950: heap size 533 MB, throughput 0.916558
Equal recommendation: 864 MB each
Reading from 19951: heap size 395 MB, throughput 0.998519
Reading from 19950: heap size 536 MB, throughput 0.984569
Reading from 19951: heap size 396 MB, throughput 0.998304
Reading from 19950: heap size 538 MB, throughput 0.994564
Reading from 19951: heap size 402 MB, throughput 0.99825
Reading from 19950: heap size 540 MB, throughput 0.993218
Reading from 19951: heap size 402 MB, throughput 0.997302
Reading from 19951: heap size 408 MB, throughput 0.997891
Equal recommendation: 864 MB each
Reading from 19950: heap size 541 MB, throughput 0.993138
Reading from 19951: heap size 409 MB, throughput 0.998569
Reading from 19950: heap size 539 MB, throughput 0.992848
Reading from 19951: heap size 415 MB, throughput 0.998702
Reading from 19950: heap size 541 MB, throughput 0.991883
Reading from 19951: heap size 416 MB, throughput 0.997944
Reading from 19951: heap size 423 MB, throughput 0.998658
Reading from 19950: heap size 542 MB, throughput 0.98872
Equal recommendation: 864 MB each
Reading from 19950: heap size 543 MB, throughput 0.98226
Reading from 19950: heap size 547 MB, throughput 0.91433
Reading from 19951: heap size 423 MB, throughput 0.997527
Reading from 19950: heap size 551 MB, throughput 0.990668
Reading from 19951: heap size 430 MB, throughput 0.996835
Reading from 19951: heap size 430 MB, throughput 0.997828
Reading from 19950: heap size 555 MB, throughput 0.99404
Reading from 19951: heap size 438 MB, throughput 0.998651
Equal recommendation: 864 MB each
Reading from 19950: heap size 557 MB, throughput 0.993701
Reading from 19951: heap size 439 MB, throughput 0.998134
Reading from 19950: heap size 553 MB, throughput 0.991883
Reading from 19951: heap size 447 MB, throughput 0.998679
Reading from 19950: heap size 557 MB, throughput 0.991736
Reading from 19951: heap size 447 MB, throughput 0.998532
Equal recommendation: 864 MB each
Reading from 19950: heap size 559 MB, throughput 0.992099
Reading from 19951: heap size 454 MB, throughput 0.998808
Reading from 19951: heap size 454 MB, throughput 0.995388
Reading from 19950: heap size 559 MB, throughput 0.99399
Reading from 19950: heap size 561 MB, throughput 0.93133
Reading from 19950: heap size 562 MB, throughput 0.974867
Reading from 19951: heap size 460 MB, throughput 0.997923
Reading from 19951: heap size 461 MB, throughput 0.998562
Reading from 19950: heap size 570 MB, throughput 0.995061
Equal recommendation: 864 MB each
Reading from 19951: heap size 469 MB, throughput 0.998771
Reading from 19950: heap size 570 MB, throughput 0.994013
Reading from 19951: heap size 470 MB, throughput 0.998555
Reading from 19950: heap size 570 MB, throughput 0.993597
Reading from 19951: heap size 477 MB, throughput 0.998764
Reading from 19951: heap size 478 MB, throughput 0.995832
Reading from 19950: heap size 572 MB, throughput 0.992884
Equal recommendation: 864 MB each
Reading from 19951: heap size 483 MB, throughput 0.998621
Reading from 19950: heap size 569 MB, throughput 0.99267
Reading from 19951: heap size 485 MB, throughput 0.998779
Reading from 19950: heap size 572 MB, throughput 0.994528
Reading from 19950: heap size 573 MB, throughput 0.936941
Reading from 19950: heap size 574 MB, throughput 0.985041
Reading from 19951: heap size 492 MB, throughput 0.998723
Equal recommendation: 864 MB each
Reading from 19950: heap size 580 MB, throughput 0.995296
Reading from 19951: heap size 493 MB, throughput 0.998564
Reading from 19950: heap size 580 MB, throughput 0.993202
Reading from 19951: heap size 499 MB, throughput 0.999296
Client 19951 died
Clients: 1
Reading from 19950: heap size 580 MB, throughput 0.994532
Recommendation: one client; give it all the memory
Reading from 19950: heap size 582 MB, throughput 0.992539
Reading from 19950: heap size 582 MB, throughput 0.992105
Reading from 19950: heap size 582 MB, throughput 0.989902
Reading from 19950: heap size 584 MB, throughput 0.938496
Recommendation: one client; give it all the memory
Reading from 19950: heap size 586 MB, throughput 0.994073
Reading from 19950: heap size 591 MB, throughput 0.995386
Reading from 19950: heap size 592 MB, throughput 0.994013
Recommendation: one client; give it all the memory
Reading from 19950: heap size 591 MB, throughput 0.993894
Reading from 19950: heap size 593 MB, throughput 0.992432
Reading from 19950: heap size 595 MB, throughput 0.994112
Reading from 19950: heap size 595 MB, throughput 0.939935
Client 19950 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
