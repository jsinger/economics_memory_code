#!/usr/bin/env python

import staticutility as su
import defaultUtilityPostprocess

def postprocessAll():
    for d in su.allDefaultUtilityDirs():
        defaultUtilityPostprocess.postprocess(d)

if __name__ == "__main__":
    postprocessAll()
