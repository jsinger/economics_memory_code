#!/bin/bash
# Required packages for Ubuntu

sudo apt-get install gcc make clang scons python2.7 python2.7-dev wget libboost-dev python-numpy python-scipy libreadline-dev flex bison haskell-platform mercurial ccache openjdk-7-jdk libX11-dev libxext-dev libxrender-dev libxtst-dev libxt-dev libcups2-dev libfreetype6-dev libasound2-dev autoconf automake ant gawk gcj-jdk xsltproc rhino libjpeg-dev libgif-dev libpng12-dev libgtk2.0-dev libattr1-dev systemtap-sdt-dev python-pip cgroup-bin python-matplotlib python-tk expect-dev doxygen graphviz ant-optional gcc-multilib g++-multilib p7zip-full poppler-utils &&

sudo pip install psutil pytimeparse future &&

cabal update &&
cabal install cabal cabal-install cassava cmdlib cond download missingh zip-archive &&

TMPDIR="$(mktemp -d)" &&
cd "${TMPDIR}" &&
wget http://hackage.haskell.org/package/hstats-0.3/hstats-0.3.tar.gz &&
tar -xf hstats-0.3.tar.gz &&
cd hstats-0.3 &&
sed -i 's/base>=2.0, haskell98/base>=4.0/g' hstats.cabal &&
cabal install . &&
cd &&
rm -r "${TMPDIR}"
