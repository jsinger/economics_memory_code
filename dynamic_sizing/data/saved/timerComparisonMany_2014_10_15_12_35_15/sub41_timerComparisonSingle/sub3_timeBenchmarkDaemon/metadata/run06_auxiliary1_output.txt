economemd
    total memory: 1104 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub41_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 12298: heap size 9 MB, throughput 0.990744
Clients: 1
Client 12298 has a minimum heap size of 276 MB
Reading from 12299: heap size 9 MB, throughput 0.990052
Clients: 2
Client 12299 has a minimum heap size of 276 MB
Reading from 12299: heap size 9 MB, throughput 0.986591
Reading from 12298: heap size 9 MB, throughput 0.983118
Reading from 12299: heap size 9 MB, throughput 0.981118
Reading from 12298: heap size 9 MB, throughput 0.974202
Reading from 12299: heap size 9 MB, throughput 0.973213
Reading from 12298: heap size 9 MB, throughput 0.969788
Reading from 12299: heap size 11 MB, throughput 0.946409
Reading from 12298: heap size 11 MB, throughput 0.980106
Reading from 12299: heap size 11 MB, throughput 0.964108
Reading from 12298: heap size 11 MB, throughput 0.985511
Reading from 12299: heap size 17 MB, throughput 0.960788
Reading from 12298: heap size 17 MB, throughput 0.960787
Reading from 12299: heap size 17 MB, throughput 0.856442
Reading from 12298: heap size 17 MB, throughput 0.862194
Reading from 12299: heap size 29 MB, throughput 0.654509
Reading from 12299: heap size 30 MB, throughput 0.948347
Reading from 12298: heap size 30 MB, throughput 0.820359
Reading from 12298: heap size 31 MB, throughput 0.538621
Reading from 12299: heap size 35 MB, throughput 0.649982
Reading from 12299: heap size 48 MB, throughput 0.341884
Reading from 12298: heap size 34 MB, throughput 0.704453
Reading from 12298: heap size 46 MB, throughput 0.689674
Reading from 12299: heap size 50 MB, throughput 0.539603
Reading from 12298: heap size 50 MB, throughput 0.429779
Reading from 12299: heap size 65 MB, throughput 0.363214
Reading from 12298: heap size 50 MB, throughput 0.0525379
Reading from 12299: heap size 72 MB, throughput 0.464508
Reading from 12299: heap size 89 MB, throughput 0.386416
Reading from 12298: heap size 55 MB, throughput 0.53322
Reading from 12299: heap size 96 MB, throughput 0.444978
Reading from 12298: heap size 79 MB, throughput 0.326212
Reading from 12298: heap size 82 MB, throughput 0.409634
Reading from 12299: heap size 97 MB, throughput 0.409378
Reading from 12299: heap size 128 MB, throughput 0.51544
Reading from 12298: heap size 84 MB, throughput 0.349975
Reading from 12298: heap size 112 MB, throughput 0.468469
Reading from 12299: heap size 128 MB, throughput 0.40302
Reading from 12298: heap size 112 MB, throughput 0.355913
Reading from 12299: heap size 166 MB, throughput 0.78673
Reading from 12298: heap size 145 MB, throughput 0.734253
Reading from 12299: heap size 166 MB, throughput 0.747652
Reading from 12298: heap size 145 MB, throughput 0.675285
Reading from 12299: heap size 167 MB, throughput 0.466145
Reading from 12298: heap size 147 MB, throughput 0.428469
Reading from 12299: heap size 170 MB, throughput 0.358888
Reading from 12298: heap size 149 MB, throughput 0.359178
Reading from 12299: heap size 173 MB, throughput 0.479308
Reading from 12299: heap size 216 MB, throughput 0.328934
Reading from 12298: heap size 151 MB, throughput 0.409642
Reading from 12298: heap size 190 MB, throughput 0.292966
Reading from 12298: heap size 192 MB, throughput 0.186004
Reading from 12299: heap size 221 MB, throughput 0.434601
Reading from 12298: heap size 196 MB, throughput 0.683279
Reading from 12299: heap size 223 MB, throughput 0.889677
Reading from 12298: heap size 204 MB, throughput 0.849929
Reading from 12299: heap size 231 MB, throughput 0.8632
Reading from 12299: heap size 234 MB, throughput 0.83222
Reading from 12299: heap size 238 MB, throughput 0.791697
Reading from 12299: heap size 241 MB, throughput 0.814444
Reading from 12299: heap size 246 MB, throughput 0.825153
Reading from 12298: heap size 206 MB, throughput 0.621892
Reading from 12299: heap size 248 MB, throughput 0.841381
Reading from 12298: heap size 258 MB, throughput 0.83046
Reading from 12299: heap size 250 MB, throughput 0.7699
Reading from 12298: heap size 259 MB, throughput 0.836022
Reading from 12299: heap size 255 MB, throughput 0.753061
Reading from 12298: heap size 263 MB, throughput 0.810861
Reading from 12299: heap size 260 MB, throughput 0.751727
Reading from 12298: heap size 264 MB, throughput 0.816541
Reading from 12299: heap size 263 MB, throughput 0.723595
Reading from 12298: heap size 268 MB, throughput 0.846161
Reading from 12299: heap size 269 MB, throughput 0.859768
Reading from 12298: heap size 268 MB, throughput 0.849396
Reading from 12298: heap size 269 MB, throughput 0.814737
Reading from 12298: heap size 273 MB, throughput 0.808431
Reading from 12299: heap size 271 MB, throughput 0.881934
Reading from 12298: heap size 279 MB, throughput 0.751615
Reading from 12299: heap size 272 MB, throughput 0.824824
Reading from 12298: heap size 282 MB, throughput 0.723107
Reading from 12298: heap size 288 MB, throughput 0.895703
Reading from 12299: heap size 275 MB, throughput 0.688621
Reading from 12298: heap size 289 MB, throughput 0.852983
Reading from 12299: heap size 320 MB, throughput 0.740624
Reading from 12298: heap size 296 MB, throughput 0.817087
Reading from 12299: heap size 322 MB, throughput 0.793861
Reading from 12298: heap size 298 MB, throughput 0.792237
Reading from 12298: heap size 299 MB, throughput 0.79533
Reading from 12299: heap size 323 MB, throughput 0.847594
Reading from 12299: heap size 325 MB, throughput 0.836518
Reading from 12298: heap size 301 MB, throughput 0.884506
Reading from 12299: heap size 322 MB, throughput 0.750834
Reading from 12298: heap size 301 MB, throughput 0.869727
Reading from 12299: heap size 325 MB, throughput 0.716362
Reading from 12298: heap size 304 MB, throughput 0.807066
Reading from 12299: heap size 331 MB, throughput 0.641911
Reading from 12299: heap size 332 MB, throughput 0.641711
Reading from 12298: heap size 307 MB, throughput 0.717841
Reading from 12298: heap size 308 MB, throughput 0.660577
Reading from 12299: heap size 341 MB, throughput 0.600899
Reading from 12298: heap size 314 MB, throughput 0.618799
Reading from 12299: heap size 342 MB, throughput 0.585773
Reading from 12298: heap size 314 MB, throughput 0.652794
Numeric result:
Recommendation: 2 clients, utility 0.426541:
    h1: 828 MB (U(h) = 0.549317*h^0.0268284)
    h2: 276 MB (U(h) = 0.619446*h^0.00813151)
Recommendation: 2 clients, utility 0.426541:
    h1: 828 MB (U(h) = 0.549317*h^0.0268284)
    h2: 276 MB (U(h) = 0.619446*h^0.00813151)
Reading from 12298: heap size 323 MB, throughput 0.938941
Reading from 12299: heap size 349 MB, throughput 0.960268
Reading from 12298: heap size 323 MB, throughput 0.961662
Reading from 12299: heap size 308 MB, throughput 0.969935
Reading from 12298: heap size 328 MB, throughput 0.965659
Reading from 12299: heap size 342 MB, throughput 0.959452
Reading from 12299: heap size 298 MB, throughput 0.955526
Reading from 12298: heap size 330 MB, throughput 0.834945
Reading from 12299: heap size 340 MB, throughput 0.965953
Reading from 12299: heap size 301 MB, throughput 0.967951
Reading from 12298: heap size 375 MB, throughput 0.969415
Reading from 12299: heap size 334 MB, throughput 0.972617
Reading from 12298: heap size 377 MB, throughput 0.976644
Reading from 12299: heap size 304 MB, throughput 0.968038
Reading from 12298: heap size 379 MB, throughput 0.979664
Numeric result:
Recommendation: 2 clients, utility 0.532592:
    h1: 613.442 MB (U(h) = 0.463132*h^0.0705247)
    h2: 490.558 MB (U(h) = 0.515616*h^0.0564005)
Recommendation: 2 clients, utility 0.532592:
    h1: 613.426 MB (U(h) = 0.463132*h^0.0705247)
    h2: 490.574 MB (U(h) = 0.515616*h^0.0564005)
Reading from 12299: heap size 326 MB, throughput 0.969906
Reading from 12299: heap size 330 MB, throughput 0.968168
Reading from 12298: heap size 381 MB, throughput 0.977126
Reading from 12298: heap size 379 MB, throughput 0.969942
Reading from 12299: heap size 326 MB, throughput 0.902506
Reading from 12299: heap size 372 MB, throughput 0.96675
Reading from 12298: heap size 381 MB, throughput 0.977501
Reading from 12299: heap size 370 MB, throughput 0.979082
Reading from 12298: heap size 381 MB, throughput 0.978747
Reading from 12299: heap size 373 MB, throughput 0.983551
Reading from 12299: heap size 376 MB, throughput 0.983515
Reading from 12298: heap size 382 MB, throughput 0.979254
Reading from 12299: heap size 376 MB, throughput 0.980993
Reading from 12298: heap size 384 MB, throughput 0.977265
Reading from 12299: heap size 374 MB, throughput 0.981517
Numeric result:
Recommendation: 2 clients, utility 0.596743:
    h1: 555.485 MB (U(h) = 0.430701*h^0.0886552)
    h2: 548.515 MB (U(h) = 0.45549*h^0.0875452)
Recommendation: 2 clients, utility 0.596743:
    h1: 555.477 MB (U(h) = 0.430701*h^0.0886552)
    h2: 548.523 MB (U(h) = 0.45549*h^0.0875452)
Reading from 12299: heap size 375 MB, throughput 0.979112
Reading from 12298: heap size 386 MB, throughput 0.981661
Reading from 12299: heap size 373 MB, throughput 0.975581
Reading from 12299: heap size 375 MB, throughput 0.948756
Reading from 12299: heap size 371 MB, throughput 0.918886
Reading from 12299: heap size 377 MB, throughput 0.872909
Reading from 12299: heap size 384 MB, throughput 0.80184
Reading from 12299: heap size 389 MB, throughput 0.753201
Reading from 12298: heap size 386 MB, throughput 0.973637
Reading from 12298: heap size 388 MB, throughput 0.960216
Reading from 12298: heap size 384 MB, throughput 0.941883
Reading from 12299: heap size 397 MB, throughput 0.931808
Reading from 12298: heap size 390 MB, throughput 0.914984
Reading from 12298: heap size 397 MB, throughput 0.931971
Reading from 12299: heap size 400 MB, throughput 0.978372
Reading from 12298: heap size 399 MB, throughput 0.981588
Reading from 12299: heap size 397 MB, throughput 0.97531
Reading from 12298: heap size 401 MB, throughput 0.981668
Reading from 12299: heap size 401 MB, throughput 0.979026
Reading from 12299: heap size 398 MB, throughput 0.982538
Reading from 12298: heap size 403 MB, throughput 0.983807
Reading from 12299: heap size 401 MB, throughput 0.982795
Reading from 12298: heap size 403 MB, throughput 0.984363
Numeric result:
Recommendation: 2 clients, utility 0.665293:
    h1: 577.886 MB (U(h) = 0.383275*h^0.117503)
    h2: 526.114 MB (U(h) = 0.420615*h^0.106977)
Recommendation: 2 clients, utility 0.665293:
    h1: 577.884 MB (U(h) = 0.383275*h^0.117503)
    h2: 526.116 MB (U(h) = 0.420615*h^0.106977)
Reading from 12299: heap size 398 MB, throughput 0.982059
Reading from 12298: heap size 405 MB, throughput 0.984761
Reading from 12299: heap size 401 MB, throughput 0.980307
Reading from 12299: heap size 397 MB, throughput 0.981421
Reading from 12298: heap size 402 MB, throughput 0.984107
Reading from 12299: heap size 400 MB, throughput 0.980643
Reading from 12298: heap size 405 MB, throughput 0.983314
Reading from 12299: heap size 398 MB, throughput 0.981771
Reading from 12298: heap size 405 MB, throughput 0.981978
Reading from 12299: heap size 400 MB, throughput 0.981357
Reading from 12298: heap size 406 MB, throughput 0.980854
Reading from 12299: heap size 398 MB, throughput 0.978107
Numeric result:
Recommendation: 2 clients, utility 0.681355:
    h1: 600.276 MB (U(h) = 0.367669*h^0.1277)
    h2: 503.724 MB (U(h) = 0.420298*h^0.107159)
Recommendation: 2 clients, utility 0.681355:
    h1: 600.277 MB (U(h) = 0.367669*h^0.1277)
    h2: 503.723 MB (U(h) = 0.420298*h^0.107159)
Reading from 12299: heap size 399 MB, throughput 0.977065
Reading from 12298: heap size 408 MB, throughput 0.980506
Reading from 12299: heap size 400 MB, throughput 0.983633
Reading from 12298: heap size 408 MB, throughput 0.982329
Reading from 12299: heap size 401 MB, throughput 0.979904
Reading from 12299: heap size 401 MB, throughput 0.965318
Reading from 12299: heap size 401 MB, throughput 0.939951
Reading from 12299: heap size 406 MB, throughput 0.884051
Reading from 12298: heap size 409 MB, throughput 0.973413
Reading from 12298: heap size 410 MB, throughput 0.945239
Reading from 12299: heap size 410 MB, throughput 0.869821
Reading from 12298: heap size 414 MB, throughput 0.930649
Reading from 12298: heap size 416 MB, throughput 0.941798
Reading from 12299: heap size 421 MB, throughput 0.978068
Reading from 12298: heap size 424 MB, throughput 0.985026
Reading from 12299: heap size 421 MB, throughput 0.981585
Reading from 12298: heap size 425 MB, throughput 0.987789
Reading from 12299: heap size 424 MB, throughput 0.978407
Reading from 12299: heap size 426 MB, throughput 0.981268
Numeric result:
Recommendation: 2 clients, utility 0.724902:
    h1: 605.824 MB (U(h) = 0.343088*h^0.14451)
    h2: 498.176 MB (U(h) = 0.400176*h^0.118835)
Recommendation: 2 clients, utility 0.724902:
    h1: 605.818 MB (U(h) = 0.343088*h^0.14451)
    h2: 498.182 MB (U(h) = 0.400176*h^0.118835)
Reading from 12298: heap size 430 MB, throughput 0.988041
Reading from 12299: heap size 427 MB, throughput 0.982751
Reading from 12298: heap size 431 MB, throughput 0.989276
Reading from 12299: heap size 429 MB, throughput 0.983678
Reading from 12298: heap size 430 MB, throughput 0.988199
Reading from 12299: heap size 427 MB, throughput 0.980101
Reading from 12299: heap size 429 MB, throughput 0.981668
Reading from 12298: heap size 432 MB, throughput 0.986891
Reading from 12299: heap size 430 MB, throughput 0.984192
Reading from 12298: heap size 430 MB, throughput 0.986138
Numeric result:
Recommendation: 2 clients, utility 0.744914:
    h1: 603.736 MB (U(h) = 0.333763*h^0.151109)
    h2: 500.264 MB (U(h) = 0.389475*h^0.125213)
Recommendation: 2 clients, utility 0.744914:
    h1: 603.732 MB (U(h) = 0.333763*h^0.151109)
    h2: 500.268 MB (U(h) = 0.389475*h^0.125213)
Reading from 12299: heap size 431 MB, throughput 0.984309
Reading from 12298: heap size 432 MB, throughput 0.985579
Reading from 12299: heap size 434 MB, throughput 0.9846
Reading from 12299: heap size 434 MB, throughput 0.979512
Reading from 12299: heap size 435 MB, throughput 0.966774
Reading from 12299: heap size 437 MB, throughput 0.947554
Reading from 12298: heap size 434 MB, throughput 0.990107
Reading from 12299: heap size 444 MB, throughput 0.963535
Reading from 12298: heap size 435 MB, throughput 0.985873
Reading from 12298: heap size 435 MB, throughput 0.976627
Reading from 12298: heap size 437 MB, throughput 0.958428
Reading from 12299: heap size 445 MB, throughput 0.984803
Reading from 12298: heap size 443 MB, throughput 0.981585
Reading from 12299: heap size 450 MB, throughput 0.988443
Reading from 12298: heap size 444 MB, throughput 0.989676
Numeric result:
Recommendation: 2 clients, utility 0.776616:
    h1: 595.048 MB (U(h) = 0.321637*h^0.159913)
    h2: 508.952 MB (U(h) = 0.370637*h^0.136774)
Recommendation: 2 clients, utility 0.776616:
    h1: 595.051 MB (U(h) = 0.321637*h^0.159913)
    h2: 508.949 MB (U(h) = 0.370637*h^0.136774)
Reading from 12299: heap size 451 MB, throughput 0.98917
Reading from 12298: heap size 448 MB, throughput 0.989876
Reading from 12299: heap size 452 MB, throughput 0.989577
Reading from 12298: heap size 449 MB, throughput 0.989593
Reading from 12299: heap size 454 MB, throughput 0.98802
Reading from 12298: heap size 448 MB, throughput 0.989279
Reading from 12299: heap size 451 MB, throughput 0.985108
Reading from 12298: heap size 450 MB, throughput 0.987949
Reading from 12299: heap size 453 MB, throughput 0.986879
Reading from 12299: heap size 454 MB, throughput 0.984101
Reading from 12298: heap size 449 MB, throughput 0.988793
Numeric result:
Recommendation: 2 clients, utility 0.793486:
    h1: 591.441 MB (U(h) = 0.315199*h^0.164679)
    h2: 512.559 MB (U(h) = 0.361211*h^0.142714)
Recommendation: 2 clients, utility 0.793486:
    h1: 591.442 MB (U(h) = 0.315199*h^0.164679)
    h2: 512.558 MB (U(h) = 0.361211*h^0.142714)
Reading from 12298: heap size 451 MB, throughput 0.986801
Reading from 12299: heap size 455 MB, throughput 0.988725
Reading from 12299: heap size 458 MB, throughput 0.98384
Reading from 12299: heap size 458 MB, throughput 0.971906
Reading from 12299: heap size 462 MB, throughput 0.954904
Reading from 12299: heap size 463 MB, throughput 0.979751
Reading from 12298: heap size 452 MB, throughput 0.990489
Reading from 12298: heap size 453 MB, throughput 0.984572
Reading from 12298: heap size 452 MB, throughput 0.973751
Reading from 12298: heap size 455 MB, throughput 0.965647
Reading from 12299: heap size 471 MB, throughput 0.986919
Reading from 12298: heap size 462 MB, throughput 0.988764
Reading from 12299: heap size 472 MB, throughput 0.989678
Numeric result:
Recommendation: 2 clients, utility 0.814945:
    h1: 588.737 MB (U(h) = 0.306692*h^0.171094)
    h2: 515.263 MB (U(h) = 0.350269*h^0.149742)
Recommendation: 2 clients, utility 0.814945:
    h1: 588.736 MB (U(h) = 0.306692*h^0.171094)
    h2: 515.264 MB (U(h) = 0.350269*h^0.149742)
Reading from 12298: heap size 462 MB, throughput 0.990709
Reading from 12299: heap size 474 MB, throughput 0.989451
Reading from 12298: heap size 463 MB, throughput 0.988963
Reading from 12299: heap size 475 MB, throughput 0.987801
Reading from 12298: heap size 465 MB, throughput 0.990367
Reading from 12299: heap size 473 MB, throughput 0.988032
Reading from 12299: heap size 475 MB, throughput 0.987105
Reading from 12298: heap size 463 MB, throughput 0.989706
Numeric result:
Recommendation: 2 clients, utility 0.823957:
    h1: 586.67 MB (U(h) = 0.303515*h^0.173515)
    h2: 517.33 MB (U(h) = 0.345261*h^0.153004)
Recommendation: 2 clients, utility 0.823957:
    h1: 586.674 MB (U(h) = 0.303515*h^0.173515)
    h2: 517.326 MB (U(h) = 0.345261*h^0.153004)
Reading from 12299: heap size 477 MB, throughput 0.986237
Reading from 12298: heap size 465 MB, throughput 0.988506
Reading from 12299: heap size 477 MB, throughput 0.988836
Reading from 12298: heap size 468 MB, throughput 0.987468
Reading from 12299: heap size 479 MB, throughput 0.982146
Reading from 12299: heap size 480 MB, throughput 0.970358
Reading from 12299: heap size 485 MB, throughput 0.975655
Reading from 12298: heap size 468 MB, throughput 0.990033
Reading from 12298: heap size 469 MB, throughput 0.984439
Reading from 12299: heap size 487 MB, throughput 0.986531
Reading from 12298: heap size 470 MB, throughput 0.972881
Reading from 12298: heap size 475 MB, throughput 0.986391
Reading from 12299: heap size 491 MB, throughput 0.9889
Numeric result:
Recommendation: 2 clients, utility 0.858367:
    h1: 518.682 MB (U(h) = 0.298235*h^0.177572)
    h2: 585.318 MB (U(h) = 0.264536*h^0.200383)
Recommendation: 2 clients, utility 0.858367:
    h1: 518.685 MB (U(h) = 0.298235*h^0.177572)
    h2: 585.315 MB (U(h) = 0.264536*h^0.200383)
Reading from 12298: heap size 477 MB, throughput 0.988779
Reading from 12299: heap size 492 MB, throughput 0.987868
Reading from 12298: heap size 480 MB, throughput 0.991612
Reading from 12299: heap size 491 MB, throughput 0.988971
Reading from 12298: heap size 481 MB, throughput 0.990835
Reading from 12299: heap size 493 MB, throughput 0.988713
Reading from 12298: heap size 479 MB, throughput 0.98783
Numeric result:
Recommendation: 2 clients, utility 0.934478:
    h1: 403.829 MB (U(h) = 0.293382*h^0.181336)
    h2: 700.171 MB (U(h) = 0.136778*h^0.314396)
Recommendation: 2 clients, utility 0.934478:
    h1: 403.838 MB (U(h) = 0.293382*h^0.181336)
    h2: 700.162 MB (U(h) = 0.136778*h^0.314396)
Reading from 12299: heap size 493 MB, throughput 0.987449
Reading from 12298: heap size 482 MB, throughput 0.988284
Reading from 12299: heap size 494 MB, throughput 0.986162
Reading from 12299: heap size 495 MB, throughput 0.985973
Reading from 12299: heap size 498 MB, throughput 0.977553
Reading from 12298: heap size 466 MB, throughput 0.988061
Reading from 12299: heap size 501 MB, throughput 0.969545
Reading from 12299: heap size 503 MB, throughput 0.987387
Reading from 12298: heap size 460 MB, throughput 0.990348
Reading from 12298: heap size 459 MB, throughput 0.984439
Reading from 12298: heap size 454 MB, throughput 0.9733
Reading from 12298: heap size 455 MB, throughput 0.977565
Reading from 12299: heap size 506 MB, throughput 0.989413
Numeric result:
Recommendation: 2 clients, utility 1.10752:
    h1: 286.404 MB (U(h) = 0.288452*h^0.185282)
    h2: 817.596 MB (U(h) = 0.0387734*h^0.528924)
Recommendation: 2 clients, utility 1.10752:
    h1: 286.404 MB (U(h) = 0.288452*h^0.185282)
    h2: 817.596 MB (U(h) = 0.0387734*h^0.528924)
Reading from 12298: heap size 423 MB, throughput 0.987395
Reading from 12299: heap size 508 MB, throughput 0.990342
Reading from 12298: heap size 442 MB, throughput 0.991029
Reading from 12299: heap size 509 MB, throughput 0.990294
Reading from 12298: heap size 416 MB, throughput 0.99206
Reading from 12298: heap size 427 MB, throughput 0.991639
Reading from 12299: heap size 511 MB, throughput 0.989026
Numeric result:
Recommendation: 2 clients, utility 1.12295:
    h1: 329.85 MB (U(h) = 0.218266*h^0.23407)
    h2: 774.15 MB (U(h) = 0.0342726*h^0.549347)
Recommendation: 2 clients, utility 1.12295:
    h1: 329.854 MB (U(h) = 0.218266*h^0.23407)
    h2: 774.146 MB (U(h) = 0.0342726*h^0.549347)
Reading from 12298: heap size 410 MB, throughput 0.987963
Reading from 12299: heap size 511 MB, throughput 0.988087
Reading from 12298: heap size 417 MB, throughput 0.987186
Reading from 12298: heap size 406 MB, throughput 0.985157
Reading from 12299: heap size 512 MB, throughput 0.989769
Reading from 12299: heap size 513 MB, throughput 0.987768
Reading from 12299: heap size 514 MB, throughput 0.977869
Reading from 12298: heap size 412 MB, throughput 0.984762
Reading from 12299: heap size 518 MB, throughput 0.981641
Reading from 12298: heap size 403 MB, throughput 0.987803
Reading from 12298: heap size 402 MB, throughput 0.980513
Reading from 12298: heap size 400 MB, throughput 0.96918
Reading from 12299: heap size 520 MB, throughput 0.988326
Reading from 12298: heap size 404 MB, throughput 0.955656
Reading from 12298: heap size 393 MB, throughput 0.941425
Numeric result:
Recommendation: 2 clients, utility 1.17644:
    h1: 583.526 MB (U(h) = 0.0392429*h^0.527801)
    h2: 520.474 MB (U(h) = 0.0547119*h^0.470769)
Recommendation: 2 clients, utility 1.17644:
    h1: 583.527 MB (U(h) = 0.0392429*h^0.527801)
    h2: 520.473 MB (U(h) = 0.0547119*h^0.470769)
Reading from 12298: heap size 399 MB, throughput 0.983346
Reading from 12299: heap size 523 MB, throughput 0.990764
Reading from 12298: heap size 401 MB, throughput 0.985636
Reading from 12298: heap size 404 MB, throughput 0.985705
Reading from 12299: heap size 495 MB, throughput 0.990719
Reading from 12298: heap size 405 MB, throughput 0.986252
Reading from 12299: heap size 519 MB, throughput 0.991546
Reading from 12298: heap size 404 MB, throughput 0.987985
Numeric result:
Recommendation: 2 clients, utility 1.1581:
    h1: 656.843 MB (U(h) = 0.0391961*h^0.528059)
    h2: 447.157 MB (U(h) = 0.107132*h^0.359484)
Recommendation: 2 clients, utility 1.1581:
    h1: 656.843 MB (U(h) = 0.0391961*h^0.528059)
    h2: 447.157 MB (U(h) = 0.107132*h^0.359484)
Reading from 12298: heap size 406 MB, throughput 0.985784
Reading from 12299: heap size 518 MB, throughput 0.989919
Reading from 12298: heap size 404 MB, throughput 0.984321
Reading from 12299: heap size 503 MB, throughput 0.988088
Reading from 12298: heap size 406 MB, throughput 0.982302
Reading from 12299: heap size 497 MB, throughput 0.982117
Reading from 12299: heap size 493 MB, throughput 0.984822
Reading from 12299: heap size 457 MB, throughput 0.983156
Reading from 12298: heap size 438 MB, throughput 0.989062
Reading from 12299: heap size 491 MB, throughput 0.991894
Reading from 12298: heap size 438 MB, throughput 0.988198
Numeric result:
Recommendation: 2 clients, utility 1.21472:
    h1: 720.317 MB (U(h) = 0.0254904*h^0.600967)
    h2: 383.683 MB (U(h) = 0.13604*h^0.32011)
Recommendation: 2 clients, utility 1.21472:
    h1: 720.317 MB (U(h) = 0.0254904*h^0.600967)
    h2: 383.683 MB (U(h) = 0.13604*h^0.32011)
Reading from 12298: heap size 443 MB, throughput 0.989187
Reading from 12299: heap size 453 MB, throughput 0.992415
Reading from 12298: heap size 444 MB, throughput 0.983436
Reading from 12298: heap size 440 MB, throughput 0.970989
Reading from 12298: heap size 444 MB, throughput 0.949652
Reading from 12298: heap size 451 MB, throughput 0.968164
Reading from 12299: heap size 477 MB, throughput 0.99054
Reading from 12298: heap size 453 MB, throughput 0.98561
Reading from 12299: heap size 454 MB, throughput 0.989329
Reading from 12298: heap size 455 MB, throughput 0.986503
Reading from 12299: heap size 466 MB, throughput 0.987703
Reading from 12298: heap size 457 MB, throughput 0.986327
Numeric result:
Recommendation: 2 clients, utility 1.26016:
    h1: 734.31 MB (U(h) = 0.0180617*h^0.659036)
    h2: 369.69 MB (U(h) = 0.126754*h^0.331793)
Recommendation: 2 clients, utility 1.26016:
    h1: 734.31 MB (U(h) = 0.0180617*h^0.659036)
    h2: 369.69 MB (U(h) = 0.126754*h^0.331793)
Reading from 12298: heap size 455 MB, throughput 0.985929
Reading from 12299: heap size 455 MB, throughput 0.987062
Reading from 12298: heap size 458 MB, throughput 0.985724
Reading from 12299: heap size 463 MB, throughput 0.985365
Reading from 12298: heap size 459 MB, throughput 0.984974
Reading from 12299: heap size 462 MB, throughput 0.982499
Reading from 12298: heap size 460 MB, throughput 0.984388
Reading from 12299: heap size 462 MB, throughput 0.984207
Reading from 12299: heap size 444 MB, throughput 0.975258
Reading from 12299: heap size 455 MB, throughput 0.957495
Reading from 12299: heap size 460 MB, throughput 0.933621
Reading from 12298: heap size 462 MB, throughput 0.985728
Reading from 12299: heap size 456 MB, throughput 0.976834
Reading from 12299: heap size 428 MB, throughput 0.981086
Numeric result:
Recommendation: 2 clients, utility 1.25742:
    h1: 723.101 MB (U(h) = 0.0179781*h^0.659737)
    h2: 380.899 MB (U(h) = 0.115222*h^0.347519)
Recommendation: 2 clients, utility 1.25742:
    h1: 723.103 MB (U(h) = 0.0179781*h^0.659737)
    h2: 380.897 MB (U(h) = 0.115222*h^0.347519)
Reading from 12298: heap size 464 MB, throughput 0.98805
Reading from 12298: heap size 465 MB, throughput 0.984653
Reading from 12298: heap size 467 MB, throughput 0.970728
Reading from 12298: heap size 471 MB, throughput 0.935295
Reading from 12299: heap size 445 MB, throughput 0.980522
Reading from 12298: heap size 473 MB, throughput 0.966086
Reading from 12299: heap size 415 MB, throughput 0.983223
Reading from 12298: heap size 482 MB, throughput 0.984883
Reading from 12299: heap size 436 MB, throughput 0.983036
Reading from 12299: heap size 411 MB, throughput 0.981295
Reading from 12298: heap size 484 MB, throughput 0.988136
Reading from 12299: heap size 427 MB, throughput 0.981237
Reading from 12298: heap size 487 MB, throughput 0.988464
Numeric result:
Recommendation: 2 clients, utility 1.28551:
    h1: 728.833 MB (U(h) = 0.0135964*h^0.705655)
    h2: 375.167 MB (U(h) = 0.104853*h^0.363232)
Recommendation: 2 clients, utility 1.28551:
    h1: 728.836 MB (U(h) = 0.0135964*h^0.705655)
    h2: 375.164 MB (U(h) = 0.104853*h^0.363232)
Reading from 12299: heap size 407 MB, throughput 0.980565
Reading from 12299: heap size 420 MB, throughput 0.979593
Reading from 12298: heap size 489 MB, throughput 0.98851
Reading from 12299: heap size 404 MB, throughput 0.979091
Reading from 12298: heap size 488 MB, throughput 0.989221
Reading from 12299: heap size 413 MB, throughput 0.979592
Reading from 12299: heap size 408 MB, throughput 0.98133
Reading from 12299: heap size 414 MB, throughput 0.970117
Reading from 12299: heap size 400 MB, throughput 0.956978
Reading from 12299: heap size 409 MB, throughput 0.927775
Reading from 12299: heap size 397 MB, throughput 0.920971
Reading from 12298: heap size 491 MB, throughput 0.989132
Reading from 12299: heap size 408 MB, throughput 0.980034
Reading from 12298: heap size 493 MB, throughput 0.986868
Reading from 12299: heap size 373 MB, throughput 0.983716
Numeric result:
Recommendation: 2 clients, utility 1.10764:
    h1: 604.376 MB (U(h) = 0.0861552*h^0.398757)
    h2: 499.624 MB (U(h) = 0.128964*h^0.329642)
Recommendation: 2 clients, utility 1.10764:
    h1: 604.377 MB (U(h) = 0.0861552*h^0.398757)
    h2: 499.623 MB (U(h) = 0.128964*h^0.329642)
Reading from 12299: heap size 409 MB, throughput 0.985961
Reading from 12298: heap size 494 MB, throughput 0.98848
Reading from 12298: heap size 496 MB, throughput 0.983647
Reading from 12298: heap size 497 MB, throughput 0.971649
Reading from 12299: heap size 409 MB, throughput 0.984967
Reading from 12298: heap size 503 MB, throughput 0.982881
Reading from 12299: heap size 411 MB, throughput 0.98675
Reading from 12298: heap size 505 MB, throughput 0.988573
Reading from 12299: heap size 412 MB, throughput 0.985663
Reading from 12299: heap size 410 MB, throughput 0.984808
Reading from 12298: heap size 509 MB, throughput 0.989189
Reading from 12299: heap size 412 MB, throughput 0.983392
Reading from 12299: heap size 409 MB, throughput 0.981813
Numeric result:
Recommendation: 2 clients, utility 1.10831:
    h1: 630.914 MB (U(h) = 0.0799517*h^0.410457)
    h2: 473.086 MB (U(h) = 0.147666*h^0.307778)
Recommendation: 2 clients, utility 1.10831:
    h1: 630.914 MB (U(h) = 0.0799517*h^0.410457)
    h2: 473.086 MB (U(h) = 0.147666*h^0.307778)
Reading from 12298: heap size 511 MB, throughput 0.989663
Reading from 12299: heap size 411 MB, throughput 0.980483
Reading from 12299: heap size 411 MB, throughput 0.982608
Reading from 12298: heap size 510 MB, throughput 0.989433
Reading from 12299: heap size 412 MB, throughput 0.980488
Reading from 12298: heap size 512 MB, throughput 0.98821
Reading from 12299: heap size 414 MB, throughput 0.987066
Reading from 12299: heap size 415 MB, throughput 0.976661
Reading from 12299: heap size 414 MB, throughput 0.960524
Reading from 12299: heap size 417 MB, throughput 0.937753
Reading from 12299: heap size 426 MB, throughput 0.924725
Reading from 12298: heap size 515 MB, throughput 0.989087
Reading from 12299: heap size 427 MB, throughput 0.980342
Numeric result:
Recommendation: 2 clients, utility 1.10186:
    h1: 629.158 MB (U(h) = 0.0836122*h^0.402696)
    h2: 474.842 MB (U(h) = 0.151126*h^0.303925)
Recommendation: 2 clients, utility 1.10186:
    h1: 629.158 MB (U(h) = 0.0836122*h^0.402696)
    h2: 474.842 MB (U(h) = 0.151126*h^0.303925)
Reading from 12299: heap size 433 MB, throughput 0.985384
Reading from 12298: heap size 515 MB, throughput 0.990523
Reading from 12298: heap size 517 MB, throughput 0.984676
Reading from 12298: heap size 518 MB, throughput 0.972405
Reading from 12299: heap size 435 MB, throughput 0.986393
Reading from 12298: heap size 525 MB, throughput 0.985397
Reading from 12299: heap size 436 MB, throughput 0.979096
Reading from 12298: heap size 526 MB, throughput 0.990547
Reading from 12299: heap size 441 MB, throughput 0.992154
Reading from 12299: heap size 442 MB, throughput 0.992829
Reading from 12298: heap size 530 MB, throughput 0.990884
Reading from 12299: heap size 445 MB, throughput 0.991828
Numeric result:
Recommendation: 2 clients, utility 1.08575:
    h1: 681.533 MB (U(h) = 0.104448*h^0.365666)
    h2: 422.467 MB (U(h) = 0.24297*h^0.226661)
Recommendation: 2 clients, utility 1.08575:
    h1: 681.542 MB (U(h) = 0.104448*h^0.365666)
    h2: 422.458 MB (U(h) = 0.24297*h^0.226661)
Reading from 12298: heap size 532 MB, throughput 0.990852
Reading from 12299: heap size 448 MB, throughput 0.990694
Reading from 12299: heap size 410 MB, throughput 0.989543
Reading from 12298: heap size 530 MB, throughput 0.990525
Reading from 12299: heap size 446 MB, throughput 0.987128
Reading from 12299: heap size 418 MB, throughput 0.987587
Reading from 12298: heap size 533 MB, throughput 0.989483
Reading from 12299: heap size 440 MB, throughput 0.978405
Reading from 12299: heap size 438 MB, throughput 0.960119
Reading from 12299: heap size 439 MB, throughput 0.939555
Reading from 12299: heap size 441 MB, throughput 0.974963
Numeric result:
Recommendation: 2 clients, utility 1.11462:
    h1: 828 MB (U(h) = 0.113784*h^0.351461)
    h2: 276 MB (U(h) = 0.498898*h^0.109573)
Recommendation: 2 clients, utility 1.11462:
    h1: 828 MB (U(h) = 0.113784*h^0.351461)
    h2: 276 MB (U(h) = 0.498898*h^0.109573)
Reading from 12299: heap size 434 MB, throughput 0.981578
Reading from 12298: heap size 536 MB, throughput 0.992046
Reading from 12298: heap size 537 MB, throughput 0.986278
Reading from 12298: heap size 538 MB, throughput 0.978079
Reading from 12299: heap size 405 MB, throughput 0.983056
Reading from 12299: heap size 421 MB, throughput 0.982691
Reading from 12298: heap size 541 MB, throughput 0.98699
Reading from 12299: heap size 402 MB, throughput 0.983768
Reading from 12299: heap size 415 MB, throughput 0.982272
Reading from 12298: heap size 548 MB, throughput 0.990049
Reading from 12299: heap size 398 MB, throughput 0.982104
Numeric result:
Recommendation: 2 clients, utility 0.998826:
    h1: 487.23 MB (U(h) = 0.542312*h^0.0966121)
    h2: 616.77 MB (U(h) = 0.461664*h^0.122306)
Recommendation: 2 clients, utility 0.998826:
    h1: 487.214 MB (U(h) = 0.542312*h^0.0966121)
    h2: 616.786 MB (U(h) = 0.461664*h^0.122306)
Reading from 12299: heap size 409 MB, throughput 0.979044
Reading from 12298: heap size 550 MB, throughput 0.991072
Reading from 12299: heap size 412 MB, throughput 0.978884
Reading from 12298: heap size 526 MB, throughput 0.990524
Reading from 12299: heap size 415 MB, throughput 0.978429
Reading from 12299: heap size 415 MB, throughput 0.979199
Reading from 12298: heap size 517 MB, throughput 0.989844
Reading from 12299: heap size 419 MB, throughput 0.985048
Reading from 12299: heap size 419 MB, throughput 0.983605
Reading from 12299: heap size 423 MB, throughput 0.972343
Reading from 12299: heap size 423 MB, throughput 0.952184
Reading from 12299: heap size 429 MB, throughput 0.928587
Client 12299 died
Clients: 1
Reading from 12298: heap size 520 MB, throughput 0.990151
Recommendation: one client; give it all the memory
Reading from 12298: heap size 512 MB, throughput 0.991828
Reading from 12298: heap size 523 MB, throughput 0.987268
Reading from 12298: heap size 522 MB, throughput 0.979116
Client 12298 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
