module Economem.Run where

import Economem.Common
import Economem.Cgroup
import Economem.Files
import Economem.Process
import Economem.Records

data RunNum = RunNum Integer Integer deriving (Read, Show)
instance Pretty RunNum where
  pretty rn = prettyInt (getNum rn) (getMax rn)
instance NumAndMax RunNum where
  makeNumAndMax = RunNum
  getNum (RunNum n _) = n
  getMax (RunNum _ m) = m


data RunSetupAction = CgroupSize Cgroup DataSize deriving (Read, Show)
data RunCleanupAction = DummyCleanupAction deriving (Read, Show)

data RunType = RunToCompletion | KillAfter Seconds deriving (Read, Show)

data Run = Run { runNum :: RunNum,
                 runType :: RunType,
                 mainProcesses :: [ProcessLauncher],
                 auxiliaryProcesses :: [ProcessLauncher],
                 auxiliaryProcessDelay :: Seconds,
                 runSetupActions :: [RunSetupAction],
                 runCleanupActions :: [RunCleanupAction] } deriving (Read, Show)

prepareRun :: Run -> IO ()
prepareRun r = mapM_ oneAction (runSetupActions r)
  where
    oneAction :: RunSetupAction -> IO ()
    oneAction (CgroupSize cg size) = setCgroupMemoryLimit cg size


cleanupRun :: Run -> IO ()
cleanupRun r = mapM_ oneAction (runCleanupActions r)
  where
    oneAction :: RunCleanupAction -> IO ()
    oneAction _ = return ()


instance Fileable Run
