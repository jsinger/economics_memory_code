economemd
    total memory: 828 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_30_14_13_41/sub3_timerComparisonSingle/sub3_timeBenchmarkDaemonEqual/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 32347: heap size 9 MB, throughput 0.991832
Clients: 1
Client 32347 has a minimum heap size of 276 MB
Reading from 32348: heap size 9 MB, throughput 0.992406
Clients: 2
Client 32348 has a minimum heap size of 276 MB
Reading from 32348: heap size 9 MB, throughput 0.973249
Reading from 32347: heap size 9 MB, throughput 0.979034
Reading from 32348: heap size 9 MB, throughput 0.958171
Reading from 32347: heap size 9 MB, throughput 0.956216
Reading from 32348: heap size 9 MB, throughput 0.946702
Reading from 32347: heap size 9 MB, throughput 0.94926
Reading from 32348: heap size 11 MB, throughput 0.980304
Reading from 32347: heap size 11 MB, throughput 0.973064
Reading from 32348: heap size 11 MB, throughput 0.974621
Reading from 32347: heap size 11 MB, throughput 0.984712
Reading from 32348: heap size 17 MB, throughput 0.90637
Reading from 32347: heap size 17 MB, throughput 0.951055
Reading from 32348: heap size 17 MB, throughput 0.554217
Reading from 32347: heap size 17 MB, throughput 0.501673
Reading from 32348: heap size 30 MB, throughput 0.969176
Reading from 32347: heap size 30 MB, throughput 0.845908
Reading from 32348: heap size 31 MB, throughput 0.960071
Reading from 32347: heap size 31 MB, throughput 0.66264
Reading from 32348: heap size 34 MB, throughput 0.51204
Reading from 32347: heap size 36 MB, throughput 0.595562
Reading from 32348: heap size 48 MB, throughput 0.706579
Reading from 32347: heap size 48 MB, throughput 0.892353
Reading from 32348: heap size 51 MB, throughput 0.793273
Reading from 32348: heap size 52 MB, throughput 0.673821
Reading from 32347: heap size 54 MB, throughput 0.483345
Reading from 32347: heap size 66 MB, throughput 0.696635
Reading from 32348: heap size 57 MB, throughput 0.26534
Reading from 32348: heap size 81 MB, throughput 0.685875
Reading from 32347: heap size 75 MB, throughput 0.411877
Reading from 32348: heap size 86 MB, throughput 0.851924
Reading from 32347: heap size 90 MB, throughput 0.790633
Reading from 32348: heap size 88 MB, throughput 0.170264
Reading from 32347: heap size 99 MB, throughput 0.34219
Reading from 32348: heap size 120 MB, throughput 0.816241
Reading from 32347: heap size 119 MB, throughput 0.779357
Reading from 32348: heap size 120 MB, throughput 0.818555
Reading from 32348: heap size 123 MB, throughput 0.223968
Reading from 32347: heap size 127 MB, throughput 0.260746
Reading from 32348: heap size 156 MB, throughput 0.749641
Reading from 32347: heap size 152 MB, throughput 0.794896
Reading from 32348: heap size 160 MB, throughput 0.860428
Reading from 32347: heap size 158 MB, throughput 0.870967
Reading from 32348: heap size 162 MB, throughput 0.185997
Reading from 32347: heap size 160 MB, throughput 0.173163
Reading from 32348: heap size 205 MB, throughput 0.56349
Reading from 32347: heap size 210 MB, throughput 0.733842
Reading from 32348: heap size 207 MB, throughput 0.732243
Reading from 32348: heap size 208 MB, throughput 0.804215
Reading from 32347: heap size 209 MB, throughput 0.239545
Reading from 32348: heap size 213 MB, throughput 0.932489
Reading from 32347: heap size 263 MB, throughput 0.942332
Reading from 32347: heap size 263 MB, throughput 0.906871
Reading from 32347: heap size 267 MB, throughput 0.953715
Reading from 32348: heap size 213 MB, throughput 0.180328
Reading from 32347: heap size 270 MB, throughput 0.814654
Reading from 32348: heap size 267 MB, throughput 0.740992
Reading from 32347: heap size 275 MB, throughput 0.861408
Reading from 32348: heap size 266 MB, throughput 0.884573
Reading from 32348: heap size 268 MB, throughput 0.903316
Reading from 32347: heap size 277 MB, throughput 0.857266
Reading from 32347: heap size 278 MB, throughput 0.738952
Reading from 32348: heap size 266 MB, throughput 0.903039
Reading from 32347: heap size 284 MB, throughput 0.781199
Reading from 32348: heap size 268 MB, throughput 0.863858
Reading from 32347: heap size 291 MB, throughput 0.79827
Reading from 32348: heap size 268 MB, throughput 0.790811
Reading from 32348: heap size 271 MB, throughput 0.787481
Reading from 32348: heap size 276 MB, throughput 0.801006
Reading from 32348: heap size 278 MB, throughput 0.798215
Reading from 32347: heap size 295 MB, throughput 0.94309
Reading from 32348: heap size 283 MB, throughput 0.917742
Reading from 32347: heap size 298 MB, throughput 0.667473
Reading from 32347: heap size 303 MB, throughput 0.746822
Reading from 32347: heap size 310 MB, throughput 0.752704
Reading from 32348: heap size 284 MB, throughput 0.906871
Reading from 32348: heap size 285 MB, throughput 0.674442
Reading from 32347: heap size 313 MB, throughput 0.899824
Reading from 32348: heap size 288 MB, throughput 0.707006
Reading from 32348: heap size 296 MB, throughput 0.731736
Reading from 32348: heap size 296 MB, throughput 0.934028
Reading from 32347: heap size 319 MB, throughput 0.884735
Reading from 32347: heap size 320 MB, throughput 0.72575
Reading from 32348: heap size 298 MB, throughput 0.883429
Reading from 32347: heap size 323 MB, throughput 0.656279
Reading from 32348: heap size 299 MB, throughput 0.844996
Reading from 32347: heap size 324 MB, throughput 0.799891
Reading from 32348: heap size 302 MB, throughput 0.738433
Reading from 32347: heap size 329 MB, throughput 0.745435
Reading from 32348: heap size 303 MB, throughput 0.691139
Reading from 32347: heap size 330 MB, throughput 0.718441
Reading from 32348: heap size 310 MB, throughput 0.702537
Reading from 32348: heap size 311 MB, throughput 0.629702
Reading from 32348: heap size 318 MB, throughput 0.670237
Reading from 32348: heap size 319 MB, throughput 0.941193
Reading from 32347: heap size 335 MB, throughput 0.954128
Equal recommendation: 414 MB each
Reading from 32347: heap size 336 MB, throughput 0.972348
Reading from 32348: heap size 325 MB, throughput 0.982572
Reading from 32348: heap size 327 MB, throughput 0.959479
Reading from 32347: heap size 338 MB, throughput 0.981417
Reading from 32348: heap size 329 MB, throughput 0.971825
Reading from 32347: heap size 341 MB, throughput 0.977115
Reading from 32348: heap size 331 MB, throughput 0.973765
Reading from 32347: heap size 338 MB, throughput 0.666994
Reading from 32348: heap size 330 MB, throughput 0.978505
Reading from 32347: heap size 385 MB, throughput 0.978638
Reading from 32348: heap size 333 MB, throughput 0.966397
Reading from 32347: heap size 387 MB, throughput 0.984082
Reading from 32348: heap size 330 MB, throughput 0.973879
Equal recommendation: 414 MB each
Reading from 32347: heap size 388 MB, throughput 0.979781
Reading from 32348: heap size 332 MB, throughput 0.972815
Reading from 32347: heap size 387 MB, throughput 0.981879
Reading from 32348: heap size 334 MB, throughput 0.968287
Reading from 32347: heap size 389 MB, throughput 0.979331
Reading from 32348: heap size 334 MB, throughput 0.972644
Reading from 32347: heap size 384 MB, throughput 0.980793
Reading from 32348: heap size 337 MB, throughput 0.688801
Reading from 32347: heap size 387 MB, throughput 0.977646
Reading from 32348: heap size 376 MB, throughput 0.969231
Reading from 32347: heap size 386 MB, throughput 0.973347
Reading from 32348: heap size 379 MB, throughput 0.981327
Reading from 32347: heap size 387 MB, throughput 0.968258
Reading from 32348: heap size 379 MB, throughput 0.981366
Reading from 32347: heap size 387 MB, throughput 0.981313
Equal recommendation: 414 MB each
Reading from 32347: heap size 390 MB, throughput 0.861669
Reading from 32347: heap size 387 MB, throughput 0.742002
Reading from 32347: heap size 392 MB, throughput 0.761953
Reading from 32347: heap size 400 MB, throughput 0.786351
Reading from 32348: heap size 378 MB, throughput 0.986561
Reading from 32348: heap size 379 MB, throughput 0.892
Reading from 32347: heap size 402 MB, throughput 0.981466
Reading from 32348: heap size 376 MB, throughput 0.868879
Reading from 32348: heap size 380 MB, throughput 0.825337
Reading from 32348: heap size 386 MB, throughput 0.813368
Reading from 32348: heap size 389 MB, throughput 0.95624
Reading from 32347: heap size 407 MB, throughput 0.984453
Reading from 32348: heap size 395 MB, throughput 0.990707
Reading from 32347: heap size 410 MB, throughput 0.984744
Reading from 32348: heap size 397 MB, throughput 0.986379
Reading from 32347: heap size 409 MB, throughput 0.98767
Reading from 32348: heap size 397 MB, throughput 0.988504
Reading from 32347: heap size 412 MB, throughput 0.989465
Reading from 32348: heap size 400 MB, throughput 0.984011
Reading from 32347: heap size 408 MB, throughput 0.986467
Equal recommendation: 414 MB each
Reading from 32348: heap size 397 MB, throughput 0.988987
Reading from 32347: heap size 411 MB, throughput 0.9764
Reading from 32348: heap size 400 MB, throughput 0.982851
Reading from 32347: heap size 408 MB, throughput 0.981091
Reading from 32348: heap size 399 MB, throughput 0.986861
Reading from 32347: heap size 410 MB, throughput 0.982011
Reading from 32348: heap size 400 MB, throughput 0.986132
Reading from 32347: heap size 409 MB, throughput 0.981149
Reading from 32348: heap size 402 MB, throughput 0.982518
Reading from 32347: heap size 410 MB, throughput 0.977552
Reading from 32348: heap size 402 MB, throughput 0.981003
Reading from 32347: heap size 408 MB, throughput 0.987488
Reading from 32348: heap size 402 MB, throughput 0.979838
Equal recommendation: 414 MB each
Reading from 32347: heap size 410 MB, throughput 0.963673
Reading from 32347: heap size 409 MB, throughput 0.839798
Reading from 32347: heap size 411 MB, throughput 0.842122
Reading from 32347: heap size 416 MB, throughput 0.887842
Reading from 32348: heap size 403 MB, throughput 0.989219
Reading from 32348: heap size 404 MB, throughput 0.904306
Reading from 32348: heap size 405 MB, throughput 0.81812
Reading from 32348: heap size 410 MB, throughput 0.862268
Reading from 32347: heap size 385 MB, throughput 0.99115
Reading from 32348: heap size 412 MB, throughput 0.954245
Reading from 32347: heap size 415 MB, throughput 0.98617
Reading from 32348: heap size 421 MB, throughput 0.990644
Reading from 32347: heap size 384 MB, throughput 0.987216
Reading from 32348: heap size 389 MB, throughput 0.989778
Reading from 32347: heap size 414 MB, throughput 0.986568
Reading from 32348: heap size 420 MB, throughput 0.987156
Reading from 32347: heap size 413 MB, throughput 0.981868
Reading from 32348: heap size 391 MB, throughput 0.985767
Equal recommendation: 414 MB each
Reading from 32347: heap size 412 MB, throughput 0.982379
Reading from 32348: heap size 417 MB, throughput 0.987256
Reading from 32347: heap size 413 MB, throughput 0.983883
Reading from 32348: heap size 393 MB, throughput 0.984711
Reading from 32347: heap size 411 MB, throughput 0.978589
Reading from 32348: heap size 412 MB, throughput 0.983239
Reading from 32347: heap size 413 MB, throughput 0.981877
Reading from 32348: heap size 411 MB, throughput 0.982871
Reading from 32347: heap size 414 MB, throughput 0.97955
Reading from 32348: heap size 413 MB, throughput 0.981552
Reading from 32347: heap size 414 MB, throughput 0.989675
Reading from 32347: heap size 408 MB, throughput 0.914407
Reading from 32347: heap size 413 MB, throughput 0.860989
Reading from 32347: heap size 416 MB, throughput 0.859884
Equal recommendation: 414 MB each
Reading from 32348: heap size 413 MB, throughput 0.981956
Reading from 32347: heap size 406 MB, throughput 0.966088
Reading from 32348: heap size 414 MB, throughput 0.988252
Reading from 32348: heap size 387 MB, throughput 0.853584
Reading from 32348: heap size 412 MB, throughput 0.855941
Reading from 32348: heap size 414 MB, throughput 0.856107
Reading from 32347: heap size 416 MB, throughput 0.987947
Reading from 32348: heap size 407 MB, throughput 0.973568
Reading from 32347: heap size 383 MB, throughput 0.984055
Reading from 32348: heap size 414 MB, throughput 0.988077
Reading from 32347: heap size 414 MB, throughput 0.986357
Reading from 32348: heap size 402 MB, throughput 0.988089
Reading from 32347: heap size 385 MB, throughput 0.98786
Reading from 32348: heap size 412 MB, throughput 0.982088
Reading from 32347: heap size 411 MB, throughput 0.984294
Reading from 32348: heap size 412 MB, throughput 0.983868
Equal recommendation: 414 MB each
Reading from 32347: heap size 410 MB, throughput 0.980035
Reading from 32348: heap size 414 MB, throughput 0.988934
Reading from 32347: heap size 408 MB, throughput 0.983415
Reading from 32348: heap size 412 MB, throughput 0.987191
Reading from 32347: heap size 409 MB, throughput 0.981276
Reading from 32348: heap size 414 MB, throughput 0.983345
Reading from 32347: heap size 411 MB, throughput 0.981825
Reading from 32348: heap size 413 MB, throughput 0.987032
Reading from 32347: heap size 411 MB, throughput 0.975589
Reading from 32348: heap size 414 MB, throughput 0.9826
Reading from 32347: heap size 412 MB, throughput 0.987529
Reading from 32347: heap size 413 MB, throughput 0.892753
Reading from 32347: heap size 412 MB, throughput 0.859466
Reading from 32347: heap size 414 MB, throughput 0.854754
Equal recommendation: 414 MB each
Reading from 32347: heap size 408 MB, throughput 0.978937
Reading from 32348: heap size 416 MB, throughput 0.972759
Reading from 32348: heap size 414 MB, throughput 0.982768
Reading from 32348: heap size 420 MB, throughput 0.81571
Reading from 32348: heap size 417 MB, throughput 0.809154
Reading from 32348: heap size 420 MB, throughput 0.814628
Reading from 32347: heap size 415 MB, throughput 0.975776
Reading from 32348: heap size 421 MB, throughput 0.974869
Reading from 32347: heap size 405 MB, throughput 0.987124
Reading from 32348: heap size 417 MB, throughput 0.987477
Reading from 32347: heap size 413 MB, throughput 0.987303
Reading from 32348: heap size 389 MB, throughput 0.982541
Reading from 32347: heap size 413 MB, throughput 0.984611
Reading from 32348: heap size 418 MB, throughput 0.984867
Reading from 32347: heap size 414 MB, throughput 0.988889
Equal recommendation: 414 MB each
Reading from 32348: heap size 390 MB, throughput 0.985088
Reading from 32347: heap size 402 MB, throughput 0.983447
Reading from 32348: heap size 414 MB, throughput 0.984191
Reading from 32347: heap size 409 MB, throughput 0.982479
Reading from 32348: heap size 412 MB, throughput 0.983177
Reading from 32347: heap size 408 MB, throughput 0.98133
Reading from 32348: heap size 410 MB, throughput 0.853438
Reading from 32347: heap size 409 MB, throughput 0.976578
Reading from 32348: heap size 438 MB, throughput 0.995107
Reading from 32347: heap size 410 MB, throughput 0.976121
Reading from 32348: heap size 434 MB, throughput 0.990538
Reading from 32347: heap size 411 MB, throughput 0.98156
Reading from 32347: heap size 417 MB, throughput 0.810688
Reading from 32347: heap size 415 MB, throughput 0.776354
Reading from 32347: heap size 418 MB, throughput 0.838992
Equal recommendation: 414 MB each
Reading from 32348: heap size 437 MB, throughput 0.991674
Reading from 32347: heap size 419 MB, throughput 0.976545
Reading from 32347: heap size 414 MB, throughput 0.983886
Reading from 32348: heap size 437 MB, throughput 0.988392
Reading from 32348: heap size 401 MB, throughput 0.917088
Reading from 32348: heap size 431 MB, throughput 0.86436
Reading from 32348: heap size 415 MB, throughput 0.834664
Reading from 32348: heap size 431 MB, throughput 0.817577
Reading from 32348: heap size 411 MB, throughput 0.965849
Reading from 32347: heap size 418 MB, throughput 0.851289
Reading from 32348: heap size 432 MB, throughput 0.984741
Reading from 32347: heap size 412 MB, throughput 0.99471
Reading from 32348: heap size 392 MB, throughput 0.986408
Reading from 32347: heap size 422 MB, throughput 0.987756
Reading from 32348: heap size 430 MB, throughput 0.984782
Reading from 32347: heap size 421 MB, throughput 0.992892
Reading from 32348: heap size 396 MB, throughput 0.988482
Equal recommendation: 414 MB each
Reading from 32347: heap size 371 MB, throughput 0.987097
Reading from 32348: heap size 426 MB, throughput 0.982179
Reading from 32347: heap size 425 MB, throughput 0.989499
Reading from 32348: heap size 400 MB, throughput 0.981384
Reading from 32347: heap size 377 MB, throughput 0.98401
Reading from 32348: heap size 422 MB, throughput 0.982427
Reading from 32347: heap size 419 MB, throughput 0.982724
Reading from 32348: heap size 402 MB, throughput 0.981009
Reading from 32347: heap size 385 MB, throughput 0.978115
Reading from 32348: heap size 419 MB, throughput 0.98228
Reading from 32347: heap size 415 MB, throughput 0.977607
Reading from 32348: heap size 403 MB, throughput 0.980694
Reading from 32347: heap size 397 MB, throughput 0.951072
Reading from 32347: heap size 410 MB, throughput 0.782165
Reading from 32347: heap size 414 MB, throughput 0.785798
Reading from 32347: heap size 414 MB, throughput 0.796834
Equal recommendation: 414 MB each
Reading from 32347: heap size 418 MB, throughput 0.978931
Reading from 32348: heap size 417 MB, throughput 0.977929
Reading from 32347: heap size 413 MB, throughput 0.981308
Reading from 32348: heap size 404 MB, throughput 0.989751
Reading from 32348: heap size 416 MB, throughput 0.886083
Reading from 32348: heap size 414 MB, throughput 0.82029
Reading from 32348: heap size 415 MB, throughput 0.861931
Reading from 32347: heap size 416 MB, throughput 0.98318
Reading from 32348: heap size 415 MB, throughput 0.863118
Reading from 32348: heap size 416 MB, throughput 0.948021
Reading from 32347: heap size 399 MB, throughput 0.981998
Reading from 32348: heap size 381 MB, throughput 0.990011
Reading from 32347: heap size 410 MB, throughput 0.976326
Reading from 32348: heap size 416 MB, throughput 0.983297
Reading from 32347: heap size 406 MB, throughput 0.988507
Reading from 32348: heap size 382 MB, throughput 0.987364
Reading from 32348: heap size 415 MB, throughput 0.985407
Reading from 32347: heap size 409 MB, throughput 0.981996
Reading from 32348: heap size 384 MB, throughput 0.980224
Equal recommendation: 414 MB each
Reading from 32347: heap size 406 MB, throughput 0.978652
Reading from 32348: heap size 412 MB, throughput 0.978467
Reading from 32347: heap size 408 MB, throughput 0.980111
Reading from 32348: heap size 411 MB, throughput 0.982817
Reading from 32347: heap size 406 MB, throughput 0.980466
Reading from 32348: heap size 408 MB, throughput 0.982267
Reading from 32347: heap size 408 MB, throughput 0.976587
Reading from 32348: heap size 410 MB, throughput 0.979299
Reading from 32347: heap size 408 MB, throughput 0.977326
Reading from 32348: heap size 411 MB, throughput 0.976575
Reading from 32347: heap size 409 MB, throughput 0.989053
Reading from 32347: heap size 410 MB, throughput 0.909026
Reading from 32347: heap size 411 MB, throughput 0.816092
Reading from 32348: heap size 411 MB, throughput 0.97967
Reading from 32347: heap size 416 MB, throughput 0.852399
Reading from 32347: heap size 416 MB, throughput 0.87866
Reading from 32348: heap size 412 MB, throughput 0.976257
Reading from 32347: heap size 414 MB, throughput 0.983259
Equal recommendation: 414 MB each
Reading from 32347: heap size 416 MB, throughput 0.98531
Reading from 32348: heap size 412 MB, throughput 0.987466
Reading from 32348: heap size 413 MB, throughput 0.97727
Reading from 32348: heap size 415 MB, throughput 0.849521
Reading from 32348: heap size 412 MB, throughput 0.837249
Reading from 32348: heap size 416 MB, throughput 0.823817
Reading from 32347: heap size 404 MB, throughput 0.986403
Reading from 32348: heap size 411 MB, throughput 0.95744
Reading from 32348: heap size 418 MB, throughput 0.989958
Reading from 32347: heap size 413 MB, throughput 0.984705
Reading from 32348: heap size 408 MB, throughput 0.986565
Reading from 32347: heap size 412 MB, throughput 0.983516
Reading from 32348: heap size 418 MB, throughput 0.989467
Reading from 32347: heap size 415 MB, throughput 0.986987
Reading from 32348: heap size 407 MB, throughput 0.984308
Reading from 32347: heap size 402 MB, throughput 0.982784
Equal recommendation: 414 MB each
Reading from 32348: heap size 415 MB, throughput 0.981686
Reading from 32347: heap size 409 MB, throughput 0.981109
Reading from 32348: heap size 404 MB, throughput 0.98654
Reading from 32347: heap size 408 MB, throughput 0.978367
Reading from 32348: heap size 411 MB, throughput 0.987129
Reading from 32347: heap size 410 MB, throughput 0.978573
Reading from 32348: heap size 410 MB, throughput 0.979449
Reading from 32347: heap size 412 MB, throughput 0.978838
Reading from 32348: heap size 411 MB, throughput 0.98188
Reading from 32347: heap size 412 MB, throughput 0.98815
Reading from 32348: heap size 412 MB, throughput 0.978333
Reading from 32347: heap size 415 MB, throughput 0.916176
Reading from 32347: heap size 413 MB, throughput 0.857333
Reading from 32347: heap size 414 MB, throughput 0.847252
Reading from 32347: heap size 416 MB, throughput 0.964569
Reading from 32348: heap size 412 MB, throughput 0.984499
Reading from 32347: heap size 407 MB, throughput 0.988786
Equal recommendation: 414 MB each
Reading from 32348: heap size 413 MB, throughput 0.973414
Reading from 32347: heap size 416 MB, throughput 0.986694
Reading from 32348: heap size 414 MB, throughput 0.989246
Reading from 32348: heap size 417 MB, throughput 0.892544
Reading from 32348: heap size 416 MB, throughput 0.822506
Reading from 32348: heap size 416 MB, throughput 0.757726
Reading from 32348: heap size 417 MB, throughput 0.843742
Reading from 32347: heap size 405 MB, throughput 0.985558
Reading from 32348: heap size 418 MB, throughput 0.984245
Reading from 32347: heap size 413 MB, throughput 0.985009
Reading from 32348: heap size 386 MB, throughput 0.991269
Reading from 32347: heap size 412 MB, throughput 0.985123
Reading from 32348: heap size 419 MB, throughput 0.986251
Reading from 32347: heap size 414 MB, throughput 0.979804
Reading from 32348: heap size 388 MB, throughput 0.986047
Reading from 32347: heap size 402 MB, throughput 0.979156
Reading from 32348: heap size 417 MB, throughput 0.986122
Equal recommendation: 414 MB each
Reading from 32348: heap size 390 MB, throughput 0.982747
Reading from 32347: heap size 409 MB, throughput 0.983099
Reading from 32348: heap size 415 MB, throughput 0.979169
Reading from 32347: heap size 409 MB, throughput 0.982967
Reading from 32348: heap size 392 MB, throughput 0.979968
Reading from 32347: heap size 410 MB, throughput 0.981521
Reading from 32348: heap size 412 MB, throughput 0.981256
Reading from 32347: heap size 412 MB, throughput 0.977031
Reading from 32348: heap size 410 MB, throughput 0.981644
Reading from 32347: heap size 413 MB, throughput 0.976857
Reading from 32347: heap size 419 MB, throughput 0.337368
Reading from 32348: heap size 411 MB, throughput 0.977237
Reading from 32347: heap size 435 MB, throughput 0.994133
Reading from 32347: heap size 438 MB, throughput 0.992529
Reading from 32347: heap size 440 MB, throughput 0.992603
Reading from 32348: heap size 412 MB, throughput 0.978488
Equal recommendation: 414 MB each
Reading from 32347: heap size 441 MB, throughput 0.992891
Reading from 32348: heap size 414 MB, throughput 0.990185
Reading from 32347: heap size 379 MB, throughput 0.991619
Reading from 32348: heap size 414 MB, throughput 0.970757
Reading from 32348: heap size 409 MB, throughput 0.863249
Reading from 32348: heap size 413 MB, throughput 0.825315
Reading from 32348: heap size 418 MB, throughput 0.835903
Reading from 32348: heap size 420 MB, throughput 0.849734
Reading from 32347: heap size 440 MB, throughput 0.990047
Reading from 32348: heap size 418 MB, throughput 0.986555
Reading from 32347: heap size 385 MB, throughput 0.987766
Reading from 32348: heap size 389 MB, throughput 0.990558
Reading from 32347: heap size 434 MB, throughput 0.9918
Reading from 32348: heap size 420 MB, throughput 0.977481
Reading from 32347: heap size 393 MB, throughput 0.986471
Reading from 32348: heap size 391 MB, throughput 0.982328
Reading from 32347: heap size 428 MB, throughput 0.98364
Equal recommendation: 414 MB each
Reading from 32348: heap size 418 MB, throughput 0.986455
Reading from 32347: heap size 400 MB, throughput 0.98094
Reading from 32348: heap size 393 MB, throughput 0.982873
Reading from 32347: heap size 425 MB, throughput 0.986926
Reading from 32348: heap size 415 MB, throughput 0.980744
Reading from 32347: heap size 405 MB, throughput 0.978747
Reading from 32348: heap size 396 MB, throughput 0.979889
Reading from 32347: heap size 422 MB, throughput 0.974333
Reading from 32348: heap size 411 MB, throughput 0.980356
Reading from 32347: heap size 422 MB, throughput 0.985478
Reading from 32348: heap size 410 MB, throughput 0.981645
Reading from 32347: heap size 420 MB, throughput 0.893343
Reading from 32347: heap size 421 MB, throughput 0.747362
Reading from 32347: heap size 422 MB, throughput 0.790819
Reading from 32347: heap size 425 MB, throughput 0.800959
Reading from 32347: heap size 427 MB, throughput 0.957931
Reading from 32348: heap size 412 MB, throughput 0.822866
Reading from 32347: heap size 382 MB, throughput 0.979904
Equal recommendation: 414 MB each
Reading from 32348: heap size 424 MB, throughput 0.995911
Reading from 32347: heap size 428 MB, throughput 0.979337
Reading from 32347: heap size 384 MB, throughput 0.983787
Reading from 32348: heap size 423 MB, throughput 0.991829
Reading from 32348: heap size 399 MB, throughput 0.921928
Reading from 32348: heap size 421 MB, throughput 0.846181
Reading from 32348: heap size 422 MB, throughput 0.868193
Reading from 32348: heap size 423 MB, throughput 0.867837
Reading from 32348: heap size 410 MB, throughput 0.879871
Reading from 32347: heap size 424 MB, throughput 0.983533
Reading from 32348: heap size 423 MB, throughput 0.989027
Reading from 32347: heap size 386 MB, throughput 0.986636
Reading from 32348: heap size 388 MB, throughput 0.990182
Reading from 32347: heap size 419 MB, throughput 0.983521
Reading from 32348: heap size 422 MB, throughput 0.986056
Reading from 32347: heap size 388 MB, throughput 0.978852
Reading from 32348: heap size 391 MB, throughput 0.985515
Reading from 32347: heap size 414 MB, throughput 0.973477
Equal recommendation: 414 MB each
Reading from 32348: heap size 420 MB, throughput 0.986114
Reading from 32347: heap size 391 MB, throughput 0.979465
Reading from 32348: heap size 394 MB, throughput 0.982915
Reading from 32347: heap size 411 MB, throughput 0.976761
Reading from 32348: heap size 416 MB, throughput 0.977372
Reading from 32347: heap size 410 MB, throughput 0.974362
Reading from 32348: heap size 398 MB, throughput 0.978764
Reading from 32347: heap size 409 MB, throughput 0.977167
Reading from 32348: heap size 413 MB, throughput 0.978538
Reading from 32347: heap size 410 MB, throughput 0.985926
Reading from 32348: heap size 412 MB, throughput 0.975521
Reading from 32347: heap size 413 MB, throughput 0.977354
Reading from 32347: heap size 413 MB, throughput 0.849875
Reading from 32347: heap size 412 MB, throughput 0.857521
Reading from 32347: heap size 416 MB, throughput 0.80407
Reading from 32347: heap size 413 MB, throughput 0.870808
Reading from 32348: heap size 413 MB, throughput 0.977341
Reading from 32347: heap size 419 MB, throughput 0.983125
Reading from 32348: heap size 414 MB, throughput 0.96459
Equal recommendation: 414 MB each
Reading from 32348: heap size 410 MB, throughput 0.96247
Reading from 32347: heap size 405 MB, throughput 0.981134
Reading from 32347: heap size 417 MB, throughput 0.985186
Reading from 32348: heap size 414 MB, throughput 0.987022
Reading from 32348: heap size 417 MB, throughput 0.900984
Reading from 32348: heap size 417 MB, throughput 0.808092
Reading from 32348: heap size 418 MB, throughput 0.853974
Reading from 32348: heap size 419 MB, throughput 0.82048
Reading from 32348: heap size 420 MB, throughput 0.91417
Reading from 32347: heap size 403 MB, throughput 0.984084
Reading from 32348: heap size 387 MB, throughput 0.982844
Reading from 32347: heap size 413 MB, throughput 0.98323
Reading from 32348: heap size 420 MB, throughput 0.985749
Reading from 32347: heap size 412 MB, throughput 0.988318
Reading from 32348: heap size 388 MB, throughput 0.983829
Reading from 32347: heap size 415 MB, throughput 0.981328
Reading from 32348: heap size 419 MB, throughput 0.987629
Reading from 32347: heap size 403 MB, throughput 0.977234
Reading from 32348: heap size 390 MB, throughput 0.976131
Equal recommendation: 414 MB each
Reading from 32347: heap size 410 MB, throughput 0.978666
Reading from 32348: heap size 417 MB, throughput 0.979843
Reading from 32348: heap size 392 MB, throughput 0.955073
Reading from 32347: heap size 410 MB, throughput 0.970188
Reading from 32348: heap size 414 MB, throughput 0.986503
Reading from 32347: heap size 411 MB, throughput 0.980436
Reading from 32348: heap size 395 MB, throughput 0.976444
Reading from 32347: heap size 412 MB, throughput 0.980955
Reading from 32348: heap size 410 MB, throughput 0.979523
Reading from 32347: heap size 413 MB, throughput 0.987585
Reading from 32347: heap size 415 MB, throughput 0.966553
Reading from 32347: heap size 403 MB, throughput 0.837648
Reading from 32347: heap size 410 MB, throughput 0.826861
Reading from 32347: heap size 413 MB, throughput 0.670545
Reading from 32348: heap size 409 MB, throughput 0.974039
Reading from 32347: heap size 422 MB, throughput 0.96906
Reading from 32348: heap size 410 MB, throughput 0.975412
Reading from 32347: heap size 394 MB, throughput 0.985997
Reading from 32348: heap size 411 MB, throughput 0.979169
Equal recommendation: 414 MB each
Reading from 32347: heap size 422 MB, throughput 0.986272
Reading from 32348: heap size 414 MB, throughput 0.987064
Reading from 32347: heap size 395 MB, throughput 0.984379
Reading from 32348: heap size 415 MB, throughput 0.962446
Reading from 32348: heap size 407 MB, throughput 0.882158
Reading from 32348: heap size 412 MB, throughput 0.800412
Reading from 32348: heap size 420 MB, throughput 0.830958
Reading from 32348: heap size 421 MB, throughput 0.826282
Reading from 32348: heap size 421 MB, throughput 0.973614
Reading from 32347: heap size 421 MB, throughput 0.97933
Reading from 32347: heap size 397 MB, throughput 0.983213
Reading from 32348: heap size 392 MB, throughput 0.984306
Reading from 32348: heap size 421 MB, throughput 0.979602
Reading from 32347: heap size 419 MB, throughput 0.839474
Reading from 32348: heap size 393 MB, throughput 0.983496
Reading from 32347: heap size 387 MB, throughput 0.993959
Reading from 32348: heap size 420 MB, throughput 0.983344
Reading from 32347: heap size 398 MB, throughput 0.993562
Equal recommendation: 414 MB each
Reading from 32348: heap size 395 MB, throughput 0.982351
Reading from 32347: heap size 401 MB, throughput 0.98904
Reading from 32348: heap size 418 MB, throughput 0.981161
Reading from 32347: heap size 406 MB, throughput 0.989444
Reading from 32348: heap size 397 MB, throughput 0.982738
Reading from 32347: heap size 406 MB, throughput 0.9851
Reading from 32348: heap size 415 MB, throughput 0.977819
Reading from 32347: heap size 405 MB, throughput 0.987571
Reading from 32348: heap size 399 MB, throughput 0.97994
Reading from 32347: heap size 407 MB, throughput 0.965183
Reading from 32347: heap size 408 MB, throughput 0.813701
Reading from 32347: heap size 408 MB, throughput 0.782728
Reading from 32347: heap size 418 MB, throughput 0.836101
Client 32347 died
Clients: 1
Reading from 32348: heap size 412 MB, throughput 0.98498
Reading from 32348: heap size 411 MB, throughput 0.9851
Recommendation: one client; give it all the memory
Reading from 32348: heap size 413 MB, throughput 0.986347
Reading from 32348: heap size 413 MB, throughput 0.987606
Reading from 32348: heap size 417 MB, throughput 0.975814
Reading from 32348: heap size 417 MB, throughput 0.875191
Reading from 32348: heap size 416 MB, throughput 0.857539
Reading from 32348: heap size 420 MB, throughput 0.84385
Reading from 32348: heap size 428 MB, throughput 0.904432
Client 32348 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
CommandThread: finished
TimerThread: finished
ReadingThread: finished
Main: All threads killed, cleaning up
