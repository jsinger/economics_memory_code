economemd
    total memory: 6095 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub11_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 6150: heap size 9 MB, throughput 0.991737
Clients: 1
Client 6150 has a minimum heap size of 8 MB
Reading from 6149: heap size 9 MB, throughput 0.991811
Clients: 2
Client 6149 has a minimum heap size of 1211 MB
Reading from 6149: heap size 9 MB, throughput 0.975768
Reading from 6150: heap size 9 MB, throughput 0.983577
Reading from 6149: heap size 9 MB, throughput 0.957691
Reading from 6150: heap size 9 MB, throughput 0.964047
Reading from 6149: heap size 9 MB, throughput 0.926945
Reading from 6150: heap size 9 MB, throughput 0.886591
Reading from 6150: heap size 11 MB, throughput 0.901565
Reading from 6150: heap size 11 MB, throughput 0.972744
Reading from 6149: heap size 11 MB, throughput 0.970232
Reading from 6150: heap size 16 MB, throughput 0.961418
Reading from 6149: heap size 11 MB, throughput 0.954593
Reading from 6150: heap size 16 MB, throughput 0.95699
Reading from 6150: heap size 24 MB, throughput 0.963924
Reading from 6149: heap size 17 MB, throughput 0.933073
Reading from 6150: heap size 25 MB, throughput 0.723158
Reading from 6150: heap size 36 MB, throughput 0.986605
Reading from 6149: heap size 17 MB, throughput 0.475773
Reading from 6150: heap size 36 MB, throughput 0.979563
Reading from 6150: heap size 42 MB, throughput 0.974054
Reading from 6149: heap size 30 MB, throughput 0.950577
Reading from 6150: heap size 43 MB, throughput 0.982907
Reading from 6149: heap size 31 MB, throughput 0.871652
Reading from 6150: heap size 51 MB, throughput 0.986816
Reading from 6150: heap size 51 MB, throughput 0.977983
Reading from 6149: heap size 34 MB, throughput 0.426402
Reading from 6150: heap size 60 MB, throughput 0.983704
Reading from 6149: heap size 47 MB, throughput 0.865737
Reading from 6150: heap size 60 MB, throughput 0.977749
Reading from 6150: heap size 68 MB, throughput 0.981216
Reading from 6149: heap size 50 MB, throughput 0.912084
Reading from 6150: heap size 68 MB, throughput 0.979269
Reading from 6150: heap size 76 MB, throughput 0.991205
Reading from 6149: heap size 52 MB, throughput 0.412269
Reading from 6150: heap size 77 MB, throughput 0.987656
Reading from 6149: heap size 76 MB, throughput 0.773167
Reading from 6150: heap size 85 MB, throughput 0.975011
Reading from 6149: heap size 76 MB, throughput 0.279913
Reading from 6150: heap size 86 MB, throughput 0.992813
Reading from 6149: heap size 102 MB, throughput 0.725025
Reading from 6149: heap size 103 MB, throughput 0.757024
Reading from 6150: heap size 95 MB, throughput 0.99327
Reading from 6149: heap size 105 MB, throughput 0.851057
Reading from 6150: heap size 96 MB, throughput 0.990085
Reading from 6150: heap size 102 MB, throughput 0.99025
Reading from 6149: heap size 108 MB, throughput 0.277003
Reading from 6150: heap size 103 MB, throughput 0.991963
Reading from 6149: heap size 136 MB, throughput 0.569764
Reading from 6149: heap size 140 MB, throughput 0.820573
Reading from 6150: heap size 109 MB, throughput 0.996028
Reading from 6149: heap size 143 MB, throughput 0.773115
Reading from 6150: heap size 109 MB, throughput 0.994039
Reading from 6150: heap size 115 MB, throughput 0.994841
Reading from 6149: heap size 145 MB, throughput 0.135427
Reading from 6149: heap size 187 MB, throughput 0.675743
Reading from 6150: heap size 115 MB, throughput 0.997079
Reading from 6149: heap size 188 MB, throughput 0.705198
Reading from 6149: heap size 190 MB, throughput 0.686881
Reading from 6150: heap size 120 MB, throughput 0.994002
Reading from 6149: heap size 195 MB, throughput 0.660214
Reading from 6150: heap size 121 MB, throughput 0.97256
Reading from 6149: heap size 198 MB, throughput 0.676598
Reading from 6150: heap size 125 MB, throughput 0.994674
Reading from 6150: heap size 125 MB, throughput 0.994087
Reading from 6149: heap size 205 MB, throughput 0.147872
Reading from 6149: heap size 253 MB, throughput 0.659423
Reading from 6150: heap size 131 MB, throughput 0.996019
Reading from 6149: heap size 257 MB, throughput 0.70644
Reading from 6150: heap size 132 MB, throughput 0.994175
Reading from 6149: heap size 260 MB, throughput 0.620023
Reading from 6150: heap size 137 MB, throughput 0.994387
Reading from 6149: heap size 266 MB, throughput 0.60636
Reading from 6149: heap size 270 MB, throughput 0.506237
Reading from 6150: heap size 137 MB, throughput 0.995688
Reading from 6149: heap size 279 MB, throughput 0.568025
Reading from 6150: heap size 143 MB, throughput 0.991293
Reading from 6149: heap size 286 MB, throughput 0.488828
Reading from 6150: heap size 143 MB, throughput 0.991793
Reading from 6150: heap size 148 MB, throughput 0.99503
Reading from 6149: heap size 294 MB, throughput 0.110362
Reading from 6149: heap size 347 MB, throughput 0.424431
Reading from 6150: heap size 148 MB, throughput 0.996843
Reading from 6149: heap size 350 MB, throughput 0.559067
Reading from 6150: heap size 152 MB, throughput 0.996159
Reading from 6150: heap size 154 MB, throughput 0.994889
Reading from 6149: heap size 342 MB, throughput 0.123842
Reading from 6150: heap size 157 MB, throughput 0.9952
Reading from 6149: heap size 397 MB, throughput 0.46619
Reading from 6149: heap size 391 MB, throughput 0.668747
Reading from 6149: heap size 345 MB, throughput 0.62543
Reading from 6150: heap size 157 MB, throughput 0.996783
Reading from 6149: heap size 394 MB, throughput 0.602696
Reading from 6149: heap size 394 MB, throughput 0.528523
Reading from 6150: heap size 161 MB, throughput 0.99729
Reading from 6149: heap size 396 MB, throughput 0.573549
Reading from 6150: heap size 161 MB, throughput 0.993037
Reading from 6149: heap size 402 MB, throughput 0.514927
Reading from 6149: heap size 407 MB, throughput 0.53137
Reading from 6150: heap size 165 MB, throughput 0.993614
Reading from 6149: heap size 414 MB, throughput 0.544281
Reading from 6149: heap size 419 MB, throughput 0.501824
Reading from 6150: heap size 165 MB, throughput 0.996638
Reading from 6150: heap size 169 MB, throughput 0.996296
Reading from 6150: heap size 169 MB, throughput 0.995806
Reading from 6149: heap size 427 MB, throughput 0.0895964
Reading from 6150: heap size 173 MB, throughput 0.994081
Reading from 6149: heap size 484 MB, throughput 0.428721
Reading from 6149: heap size 490 MB, throughput 0.51676
Reading from 6150: heap size 173 MB, throughput 0.997136
Equal recommendation: 3047 MB each
Reading from 6149: heap size 494 MB, throughput 0.509845
Reading from 6150: heap size 177 MB, throughput 0.995956
Reading from 6150: heap size 178 MB, throughput 0.995766
Reading from 6150: heap size 181 MB, throughput 0.996072
Reading from 6149: heap size 494 MB, throughput 0.0727274
Reading from 6150: heap size 181 MB, throughput 0.988977
Reading from 6149: heap size 555 MB, throughput 0.529421
Reading from 6149: heap size 560 MB, throughput 0.540866
Reading from 6150: heap size 185 MB, throughput 0.997725
Reading from 6149: heap size 554 MB, throughput 0.511226
Reading from 6149: heap size 558 MB, throughput 0.561185
Reading from 6150: heap size 185 MB, throughput 0.99699
Reading from 6149: heap size 562 MB, throughput 0.552224
Reading from 6150: heap size 190 MB, throughput 0.997347
Reading from 6150: heap size 190 MB, throughput 0.996533
Reading from 6150: heap size 193 MB, throughput 0.997694
Reading from 6149: heap size 566 MB, throughput 0.110883
Reading from 6150: heap size 194 MB, throughput 0.994373
Reading from 6149: heap size 638 MB, throughput 0.457817
Reading from 6149: heap size 644 MB, throughput 0.486836
Reading from 6150: heap size 197 MB, throughput 0.995789
Reading from 6149: heap size 644 MB, throughput 0.475003
Reading from 6150: heap size 197 MB, throughput 0.995933
Reading from 6149: heap size 648 MB, throughput 0.474922
Reading from 6150: heap size 202 MB, throughput 0.996067
Reading from 6150: heap size 202 MB, throughput 0.994498
Reading from 6149: heap size 655 MB, throughput 0.82625
Reading from 6150: heap size 206 MB, throughput 0.996338
Reading from 6150: heap size 206 MB, throughput 0.995436
Reading from 6149: heap size 662 MB, throughput 0.827013
Reading from 6150: heap size 211 MB, throughput 0.995401
Reading from 6150: heap size 211 MB, throughput 0.995015
Reading from 6149: heap size 661 MB, throughput 0.797691
Reading from 6150: heap size 216 MB, throughput 0.991638
Reading from 6149: heap size 679 MB, throughput 0.433778
Reading from 6149: heap size 688 MB, throughput 0.244342
Reading from 6150: heap size 216 MB, throughput 0.996537
Reading from 6149: heap size 704 MB, throughput 0.466384
Reading from 6150: heap size 221 MB, throughput 0.996004
Reading from 6150: heap size 221 MB, throughput 0.995853
Reading from 6150: heap size 227 MB, throughput 0.989991
Reading from 6150: heap size 227 MB, throughput 0.996806
Reading from 6149: heap size 712 MB, throughput 0.0151543
Reading from 6150: heap size 233 MB, throughput 0.989644
Reading from 6149: heap size 705 MB, throughput 0.171727
Reading from 6149: heap size 781 MB, throughput 0.284379
Reading from 6150: heap size 233 MB, throughput 0.997775
Equal recommendation: 3047 MB each
Reading from 6150: heap size 239 MB, throughput 0.998015
Reading from 6149: heap size 679 MB, throughput 0.0359531
Reading from 6149: heap size 870 MB, throughput 0.266414
Reading from 6150: heap size 240 MB, throughput 0.997507
Reading from 6150: heap size 246 MB, throughput 0.996644
Reading from 6149: heap size 872 MB, throughput 0.894066
Reading from 6149: heap size 879 MB, throughput 0.625278
Reading from 6149: heap size 881 MB, throughput 0.555794
Reading from 6150: heap size 246 MB, throughput 0.996596
Reading from 6149: heap size 880 MB, throughput 0.59298
Reading from 6149: heap size 882 MB, throughput 0.595015
Reading from 6150: heap size 251 MB, throughput 0.997734
Reading from 6150: heap size 251 MB, throughput 0.998053
Reading from 6150: heap size 255 MB, throughput 0.997646
Reading from 6149: heap size 875 MB, throughput 0.147663
Reading from 6150: heap size 255 MB, throughput 0.997556
Reading from 6149: heap size 975 MB, throughput 0.427713
Reading from 6149: heap size 977 MB, throughput 0.928808
Reading from 6149: heap size 980 MB, throughput 0.773657
Reading from 6150: heap size 259 MB, throughput 0.997162
Reading from 6149: heap size 977 MB, throughput 0.919085
Reading from 6149: heap size 776 MB, throughput 0.802908
Reading from 6150: heap size 259 MB, throughput 0.972023
Reading from 6149: heap size 967 MB, throughput 0.892293
Reading from 6150: heap size 264 MB, throughput 0.996876
Reading from 6149: heap size 794 MB, throughput 0.848353
Reading from 6149: heap size 941 MB, throughput 0.718459
Reading from 6150: heap size 265 MB, throughput 0.995505
Reading from 6149: heap size 828 MB, throughput 0.724531
Reading from 6149: heap size 924 MB, throughput 0.695177
Reading from 6149: heap size 833 MB, throughput 0.677676
Reading from 6150: heap size 273 MB, throughput 0.997989
Reading from 6149: heap size 914 MB, throughput 0.75262
Reading from 6149: heap size 839 MB, throughput 0.749195
Reading from 6150: heap size 273 MB, throughput 0.99637
Reading from 6149: heap size 906 MB, throughput 0.811898
Reading from 6149: heap size 913 MB, throughput 0.736669
Reading from 6149: heap size 902 MB, throughput 0.747103
Reading from 6150: heap size 281 MB, throughput 0.997371
Reading from 6149: heap size 908 MB, throughput 0.747067
Reading from 6149: heap size 901 MB, throughput 0.7727
Reading from 6150: heap size 281 MB, throughput 0.997162
Reading from 6150: heap size 290 MB, throughput 0.996776
Reading from 6150: heap size 290 MB, throughput 0.996345
Reading from 6150: heap size 298 MB, throughput 0.997343
Reading from 6149: heap size 905 MB, throughput 0.968259
Reading from 6150: heap size 298 MB, throughput 0.996934
Reading from 6149: heap size 900 MB, throughput 0.88795
Reading from 6149: heap size 905 MB, throughput 0.694699
Reading from 6150: heap size 307 MB, throughput 0.996843
Reading from 6149: heap size 912 MB, throughput 0.73439
Reading from 6149: heap size 915 MB, throughput 0.710567
Reading from 6149: heap size 924 MB, throughput 0.73301
Reading from 6150: heap size 307 MB, throughput 0.997101
Reading from 6149: heap size 926 MB, throughput 0.74371
Reading from 6149: heap size 936 MB, throughput 0.724793
Reading from 6150: heap size 316 MB, throughput 0.998562
Reading from 6149: heap size 936 MB, throughput 0.720611
Equal recommendation: 3047 MB each
Reading from 6149: heap size 946 MB, throughput 0.780839
Reading from 6150: heap size 316 MB, throughput 0.997479
Reading from 6149: heap size 946 MB, throughput 0.851869
Reading from 6150: heap size 325 MB, throughput 0.998334
Reading from 6149: heap size 951 MB, throughput 0.869638
Reading from 6149: heap size 954 MB, throughput 0.723624
Reading from 6150: heap size 325 MB, throughput 0.996816
Reading from 6149: heap size 957 MB, throughput 0.639407
Reading from 6150: heap size 333 MB, throughput 0.996906
Reading from 6150: heap size 333 MB, throughput 0.998192
Reading from 6150: heap size 341 MB, throughput 0.997663
Reading from 6149: heap size 960 MB, throughput 0.0704312
Reading from 6150: heap size 341 MB, throughput 0.998094
Reading from 6149: heap size 1089 MB, throughput 0.499342
Reading from 6149: heap size 1092 MB, throughput 0.763841
Reading from 6150: heap size 350 MB, throughput 0.996734
Reading from 6149: heap size 1098 MB, throughput 0.763227
Reading from 6149: heap size 1101 MB, throughput 0.721291
Reading from 6149: heap size 1105 MB, throughput 0.726118
Reading from 6150: heap size 350 MB, throughput 0.997541
Reading from 6149: heap size 1109 MB, throughput 0.727794
Reading from 6149: heap size 1118 MB, throughput 0.735039
Reading from 6150: heap size 358 MB, throughput 0.998345
Reading from 6150: heap size 359 MB, throughput 0.99656
Reading from 6149: heap size 1120 MB, throughput 0.909346
Reading from 6150: heap size 367 MB, throughput 0.998802
Reading from 6150: heap size 367 MB, throughput 0.996518
Reading from 6150: heap size 376 MB, throughput 0.994659
Reading from 6149: heap size 1133 MB, throughput 0.965695
Reading from 6150: heap size 376 MB, throughput 0.997521
Reading from 6150: heap size 387 MB, throughput 0.998371
Reading from 6150: heap size 387 MB, throughput 0.997804
Equal recommendation: 3047 MB each
Reading from 6149: heap size 1135 MB, throughput 0.931524
Reading from 6150: heap size 398 MB, throughput 0.998265
Reading from 6150: heap size 398 MB, throughput 0.998032
Reading from 6150: heap size 407 MB, throughput 0.997656
Reading from 6149: heap size 1148 MB, throughput 0.931067
Reading from 6150: heap size 407 MB, throughput 0.997842
Reading from 6150: heap size 417 MB, throughput 0.998248
Reading from 6150: heap size 417 MB, throughput 0.998113
Reading from 6149: heap size 1153 MB, throughput 0.937476
Reading from 6150: heap size 426 MB, throughput 0.997601
Reading from 6150: heap size 426 MB, throughput 0.997522
Reading from 6150: heap size 436 MB, throughput 0.99824
Reading from 6149: heap size 1164 MB, throughput 0.928323
Reading from 6150: heap size 436 MB, throughput 0.997869
Reading from 6150: heap size 445 MB, throughput 0.998064
Reading from 6150: heap size 446 MB, throughput 0.997201
Reading from 6149: heap size 1172 MB, throughput 0.934519
Reading from 6150: heap size 455 MB, throughput 0.99802
Reading from 6150: heap size 455 MB, throughput 0.998156
Reading from 6150: heap size 465 MB, throughput 0.997654
Reading from 6149: heap size 1184 MB, throughput 0.9365
Equal recommendation: 3047 MB each
Reading from 6150: heap size 465 MB, throughput 0.998435
Reading from 6150: heap size 475 MB, throughput 0.997477
Reading from 6150: heap size 475 MB, throughput 0.995563
Reading from 6149: heap size 1188 MB, throughput 0.945674
Reading from 6150: heap size 486 MB, throughput 0.997925
Reading from 6150: heap size 486 MB, throughput 0.997854
Reading from 6149: heap size 1184 MB, throughput 0.932176
Reading from 6150: heap size 498 MB, throughput 0.997766
Reading from 6150: heap size 498 MB, throughput 0.997903
Reading from 6150: heap size 510 MB, throughput 0.997891
Reading from 6149: heap size 1189 MB, throughput 0.945444
Reading from 6150: heap size 511 MB, throughput 0.997554
Reading from 6150: heap size 523 MB, throughput 0.997477
Reading from 6150: heap size 523 MB, throughput 0.995736
Reading from 6149: heap size 1194 MB, throughput 0.950143
Reading from 6150: heap size 536 MB, throughput 0.998103
Reading from 6150: heap size 536 MB, throughput 0.997866
Equal recommendation: 3047 MB each
Reading from 6149: heap size 1195 MB, throughput 0.932504
Reading from 6150: heap size 550 MB, throughput 0.997842
Reading from 6150: heap size 550 MB, throughput 0.997738
Reading from 6149: heap size 1202 MB, throughput 0.932109
Reading from 6150: heap size 563 MB, throughput 0.998187
Reading from 6150: heap size 564 MB, throughput 0.998253
Reading from 6150: heap size 576 MB, throughput 0.997669
Reading from 6149: heap size 1204 MB, throughput 0.937462
Reading from 6150: heap size 576 MB, throughput 0.998072
Reading from 6150: heap size 587 MB, throughput 0.998263
Reading from 6149: heap size 1212 MB, throughput 0.942819
Reading from 6150: heap size 588 MB, throughput 0.997709
Reading from 6150: heap size 601 MB, throughput 0.998264
Reading from 6150: heap size 601 MB, throughput 0.998162
Reading from 6149: heap size 1216 MB, throughput 0.932915
Reading from 6150: heap size 613 MB, throughput 0.998713
Equal recommendation: 3047 MB each
Reading from 6150: heap size 613 MB, throughput 0.997453
Reading from 6150: heap size 625 MB, throughput 0.997821
Reading from 6149: heap size 1225 MB, throughput 0.511216
Reading from 6150: heap size 625 MB, throughput 0.998994
Reading from 6150: heap size 636 MB, throughput 0.997782
Reading from 6150: heap size 638 MB, throughput 0.998296
Reading from 6149: heap size 1299 MB, throughput 0.985188
Reading from 6150: heap size 650 MB, throughput 0.998387
Reading from 6150: heap size 650 MB, throughput 0.998081
Reading from 6149: heap size 1301 MB, throughput 0.978918
Reading from 6150: heap size 663 MB, throughput 0.998027
Reading from 6150: heap size 663 MB, throughput 0.998377
Reading from 6149: heap size 1311 MB, throughput 0.975746
Equal recommendation: 3047 MB each
Reading from 6150: heap size 675 MB, throughput 0.998493
Reading from 6150: heap size 675 MB, throughput 0.997967
Reading from 6149: heap size 1322 MB, throughput 0.97154
Reading from 6150: heap size 688 MB, throughput 0.998155
Reading from 6150: heap size 688 MB, throughput 0.99809
Reading from 6150: heap size 701 MB, throughput 0.992301
Reading from 6149: heap size 1324 MB, throughput 0.968443
Reading from 6150: heap size 701 MB, throughput 0.998397
Reading from 6150: heap size 722 MB, throughput 0.998108
Reading from 6149: heap size 1321 MB, throughput 0.966471
Reading from 6150: heap size 724 MB, throughput 0.998632
Reading from 6150: heap size 742 MB, throughput 0.998438
Reading from 6149: heap size 1325 MB, throughput 0.969514
Equal recommendation: 3047 MB each
Reading from 6150: heap size 742 MB, throughput 0.998591
Reading from 6150: heap size 757 MB, throughput 0.998652
Reading from 6149: heap size 1312 MB, throughput 0.956884
Reading from 6150: heap size 759 MB, throughput 0.998744
Reading from 6150: heap size 773 MB, throughput 0.998104
Reading from 6149: heap size 1319 MB, throughput 0.949611
Reading from 6150: heap size 773 MB, throughput 0.998502
Reading from 6150: heap size 786 MB, throughput 0.998772
Reading from 6149: heap size 1317 MB, throughput 0.95426
Reading from 6150: heap size 787 MB, throughput 0.998576
Reading from 6150: heap size 799 MB, throughput 0.998684
Reading from 6149: heap size 1318 MB, throughput 0.950802
Reading from 6150: heap size 800 MB, throughput 0.998812
Equal recommendation: 3047 MB each
Reading from 6150: heap size 812 MB, throughput 0.998665
Reading from 6149: heap size 1324 MB, throughput 0.949621
Reading from 6150: heap size 813 MB, throughput 0.998816
Reading from 6150: heap size 825 MB, throughput 0.999036
Reading from 6149: heap size 1327 MB, throughput 0.949254
Reading from 6150: heap size 825 MB, throughput 0.998779
Reading from 6150: heap size 836 MB, throughput 0.998493
Reading from 6149: heap size 1334 MB, throughput 0.935769
Reading from 6150: heap size 836 MB, throughput 0.998589
Reading from 6150: heap size 848 MB, throughput 0.998977
Reading from 6149: heap size 1342 MB, throughput 0.938575
Equal recommendation: 3047 MB each
Reading from 6150: heap size 848 MB, throughput 0.998417
Reading from 6150: heap size 860 MB, throughput 0.998758
Reading from 6149: heap size 1350 MB, throughput 0.931124
Reading from 6150: heap size 860 MB, throughput 0.998649
Reading from 6150: heap size 873 MB, throughput 0.998974
Reading from 6149: heap size 1360 MB, throughput 0.927372
Reading from 6150: heap size 873 MB, throughput 0.998564
Reading from 6150: heap size 885 MB, throughput 0.9988
Reading from 6149: heap size 1372 MB, throughput 0.93617
Reading from 6150: heap size 885 MB, throughput 0.999142
Reading from 6150: heap size 896 MB, throughput 0.998649
Reading from 6149: heap size 1378 MB, throughput 0.942692
Equal recommendation: 3047 MB each
Reading from 6150: heap size 897 MB, throughput 0.998815
Reading from 6150: heap size 909 MB, throughput 0.998603
Reading from 6149: heap size 1391 MB, throughput 0.936637
Reading from 6150: heap size 909 MB, throughput 0.999028
Reading from 6150: heap size 921 MB, throughput 0.99857
Reading from 6149: heap size 1395 MB, throughput 0.933161
Reading from 6150: heap size 922 MB, throughput 0.999032
Reading from 6150: heap size 934 MB, throughput 0.999095
Reading from 6149: heap size 1407 MB, throughput 0.942768
Reading from 6150: heap size 934 MB, throughput 0.99885
Equal recommendation: 3047 MB each
Reading from 6150: heap size 945 MB, throughput 0.998992
Reading from 6150: heap size 945 MB, throughput 0.998751
Reading from 6150: heap size 957 MB, throughput 0.999214
Reading from 6150: heap size 957 MB, throughput 0.9987
Reading from 6150: heap size 968 MB, throughput 0.998939
Reading from 6150: heap size 968 MB, throughput 0.998655
Equal recommendation: 3047 MB each
Reading from 6150: heap size 980 MB, throughput 0.998978
Reading from 6150: heap size 980 MB, throughput 0.998889
Reading from 6150: heap size 993 MB, throughput 0.999315
Reading from 6149: heap size 1409 MB, throughput 0.800601
Reading from 6150: heap size 993 MB, throughput 0.999006
Reading from 6150: heap size 1004 MB, throughput 0.999158
Reading from 6150: heap size 1004 MB, throughput 0.999099
Equal recommendation: 3047 MB each
Reading from 6150: heap size 1015 MB, throughput 0.99916
Reading from 6150: heap size 1015 MB, throughput 0.998876
Reading from 6150: heap size 1026 MB, throughput 0.998969
Reading from 6150: heap size 1026 MB, throughput 0.998723
Reading from 6149: heap size 1538 MB, throughput 0.974788
Reading from 6150: heap size 1037 MB, throughput 0.999216
Reading from 6150: heap size 1037 MB, throughput 0.998863
Equal recommendation: 3047 MB each
Reading from 6150: heap size 1049 MB, throughput 0.99909
Reading from 6149: heap size 1587 MB, throughput 0.971308
Reading from 6149: heap size 1589 MB, throughput 0.773095
Reading from 6150: heap size 1049 MB, throughput 0.99863
Reading from 6149: heap size 1594 MB, throughput 0.677226
Reading from 6149: heap size 1616 MB, throughput 0.700571
Reading from 6149: heap size 1629 MB, throughput 0.674377
Reading from 6149: heap size 1661 MB, throughput 0.724109
Reading from 6150: heap size 1061 MB, throughput 0.999148
Reading from 6149: heap size 1667 MB, throughput 0.761652
Reading from 6150: heap size 1061 MB, throughput 0.997854
Reading from 6149: heap size 1693 MB, throughput 0.954734
Reading from 6150: heap size 1073 MB, throughput 0.999048
Reading from 6150: heap size 1073 MB, throughput 0.998749
Reading from 6149: heap size 1699 MB, throughput 0.957029
Equal recommendation: 3047 MB each
Reading from 6150: heap size 1088 MB, throughput 0.99915
Reading from 6150: heap size 1088 MB, throughput 0.99893
Reading from 6149: heap size 1705 MB, throughput 0.960978
Reading from 6150: heap size 1102 MB, throughput 0.999023
Reading from 6150: heap size 1102 MB, throughput 0.99918
Reading from 6149: heap size 1716 MB, throughput 0.962442
Reading from 6150: heap size 1115 MB, throughput 0.999146
Reading from 6150: heap size 1115 MB, throughput 0.999122
Reading from 6149: heap size 1713 MB, throughput 0.959691
Equal recommendation: 3047 MB each
Reading from 6150: heap size 1127 MB, throughput 0.998829
Reading from 6150: heap size 1128 MB, throughput 0.998308
Reading from 6149: heap size 1725 MB, throughput 0.95983
Reading from 6150: heap size 1140 MB, throughput 0.999117
Reading from 6150: heap size 1140 MB, throughput 0.998929
Reading from 6149: heap size 1716 MB, throughput 0.95773
Reading from 6150: heap size 1155 MB, throughput 0.99917
Reading from 6150: heap size 1155 MB, throughput 0.998683
Reading from 6149: heap size 1728 MB, throughput 0.958275
Equal recommendation: 3047 MB each
Reading from 6150: heap size 1168 MB, throughput 0.999165
Reading from 6150: heap size 1168 MB, throughput 0.998994
Reading from 6149: heap size 1736 MB, throughput 0.965351
Reading from 6150: heap size 1182 MB, throughput 0.99927
Reading from 6150: heap size 1182 MB, throughput 0.999055
Reading from 6149: heap size 1739 MB, throughput 0.963667
Reading from 6150: heap size 1195 MB, throughput 0.999208
Equal recommendation: 3047 MB each
Reading from 6150: heap size 1195 MB, throughput 0.999164
Reading from 6149: heap size 1741 MB, throughput 0.961366
Reading from 6150: heap size 1208 MB, throughput 0.999237
Reading from 6150: heap size 1208 MB, throughput 0.999202
Reading from 6149: heap size 1748 MB, throughput 0.963248
Reading from 6150: heap size 1220 MB, throughput 0.999179
Reading from 6150: heap size 1220 MB, throughput 0.99927
Reading from 6149: heap size 1743 MB, throughput 0.961376
Equal recommendation: 3047 MB each
Reading from 6150: heap size 1231 MB, throughput 0.99901
Reading from 6150: heap size 1232 MB, throughput 0.999212
Reading from 6149: heap size 1750 MB, throughput 0.960672
Reading from 6150: heap size 1243 MB, throughput 0.999333
Reading from 6150: heap size 1243 MB, throughput 0.999216
Reading from 6149: heap size 1761 MB, throughput 0.953426
Reading from 6150: heap size 1255 MB, throughput 0.999285
Equal recommendation: 3047 MB each
Reading from 6150: heap size 1255 MB, throughput 0.999127
Reading from 6150: heap size 1266 MB, throughput 0.999226
Reading from 6149: heap size 1764 MB, throughput 0.945166
Reading from 6150: heap size 1266 MB, throughput 0.999203
Reading from 6150: heap size 1277 MB, throughput 0.999217
Reading from 6149: heap size 1780 MB, throughput 0.946461
Reading from 6150: heap size 1277 MB, throughput 0.999023
Equal recommendation: 3047 MB each
Reading from 6150: heap size 1289 MB, throughput 0.99917
Reading from 6149: heap size 1787 MB, throughput 0.948912
Reading from 6150: heap size 1226 MB, throughput 0.999266
Reading from 6150: heap size 1165 MB, throughput 0.999445
Reading from 6150: heap size 1109 MB, throughput 0.998729
Reading from 6149: heap size 1806 MB, throughput 0.713337
Reading from 6150: heap size 1055 MB, throughput 0.999388
Reading from 6150: heap size 1003 MB, throughput 0.998976
Equal recommendation: 3047 MB each
Reading from 6150: heap size 957 MB, throughput 0.99899
Reading from 6149: heap size 1778 MB, throughput 0.988459
Reading from 6150: heap size 910 MB, throughput 0.987044
Reading from 6150: heap size 867 MB, throughput 0.999261
Reading from 6150: heap size 878 MB, throughput 0.999297
Reading from 6149: heap size 1788 MB, throughput 0.983809
Reading from 6150: heap size 890 MB, throughput 0.999211
Reading from 6150: heap size 902 MB, throughput 0.998993
Reading from 6150: heap size 917 MB, throughput 0.999034
Reading from 6150: heap size 930 MB, throughput 0.998746
Reading from 6149: heap size 1803 MB, throughput 0.98154
Equal recommendation: 3047 MB each
Reading from 6150: heap size 945 MB, throughput 0.998638
Reading from 6150: heap size 960 MB, throughput 0.999144
Reading from 6150: heap size 975 MB, throughput 0.998808
Reading from 6149: heap size 1826 MB, throughput 0.977241
Reading from 6150: heap size 992 MB, throughput 0.999155
Reading from 6150: heap size 1008 MB, throughput 0.998913
Reading from 6150: heap size 1024 MB, throughput 0.999224
Reading from 6149: heap size 1827 MB, throughput 0.972351
Equal recommendation: 3047 MB each
Reading from 6150: heap size 1039 MB, throughput 0.998926
Reading from 6150: heap size 1055 MB, throughput 0.999325
Reading from 6150: heap size 1070 MB, throughput 0.999099
Reading from 6150: heap size 1085 MB, throughput 0.999236
Reading from 6150: heap size 1100 MB, throughput 0.999193
Reading from 6150: heap size 1114 MB, throughput 0.999328
Equal recommendation: 3047 MB each
Reading from 6150: heap size 1129 MB, throughput 0.999188
Reading from 6150: heap size 1143 MB, throughput 0.999408
Reading from 6150: heap size 1157 MB, throughput 0.999138
Reading from 6150: heap size 1171 MB, throughput 0.999275
Reading from 6150: heap size 1185 MB, throughput 0.99919
Reading from 6150: heap size 1200 MB, throughput 0.999316
Equal recommendation: 3047 MB each
Reading from 6150: heap size 1214 MB, throughput 0.999384
Reading from 6150: heap size 1228 MB, throughput 0.999211
Reading from 6149: heap size 1822 MB, throughput 0.994564
Reading from 6150: heap size 1239 MB, throughput 0.999265
Reading from 6150: heap size 1240 MB, throughput 0.998977
Reading from 6150: heap size 1255 MB, throughput 0.999029
Equal recommendation: 3047 MB each
Reading from 6150: heap size 1255 MB, throughput 0.999057
Reading from 6149: heap size 1830 MB, throughput 0.98432
Reading from 6149: heap size 1830 MB, throughput 0.813086
Reading from 6149: heap size 1832 MB, throughput 0.761301
Reading from 6150: heap size 1272 MB, throughput 0.998813
Reading from 6149: heap size 1868 MB, throughput 0.772559
Client 6149 died
Clients: 1
Reading from 6150: heap size 1272 MB, throughput 0.998473
Reading from 6150: heap size 1292 MB, throughput 0.998668
Reading from 6150: heap size 1292 MB, throughput 0.998513
Recommendation: one client; give it all the memory
Reading from 6150: heap size 1316 MB, throughput 0.998882
Reading from 6150: heap size 1316 MB, throughput 0.998568
Reading from 6150: heap size 1341 MB, throughput 0.998953
Reading from 6150: heap size 1341 MB, throughput 0.99885
Reading from 6150: heap size 1366 MB, throughput 0.998831
Recommendation: one client; give it all the memory
Reading from 6150: heap size 1367 MB, throughput 0.998839
Client 6150 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
