#!/usr/bin/env python
# Measure the utility of various heap size combinations of two or more benchmarks, on the local machine

import argparse
import argtypes
import staticutility as su
from utilitySpacePoint import utilitySpacePoint
from utilitySpacePostprocess import postprocess

RUNS = 5 # default number of runs


parser = argparse.ArgumentParser(description="Measure the utility of various heap size combinations of two or more benchmarks, on the local machine")

parser.add_argument("totalMem", type=argtypes.positive_int, help="total memory a combination is allowed to use")
parser.add_argument("time", help="how long to run for, in a format suitable for input to 'sleep'")

parser.add_argument("-r", "--runs", type=argtypes.positive_int, default=RUNS, help="how many times to run the measurement, for taking averages")
parser.add_argument("--ignoreCrossProduct", action="store_true", help="Only generate points on the budget line, not those from the cross product")
parser.add_argument("--pointsFile", type=argtypes.existing_absolute_path, help="Measure the points specified in this file rather than generating them in the usual way")

parser.add_argument("-b", "--benchmark", action="append", nargs=5, default=[], metavar=("benchmark", "inputSize", "threads", "time", "measureDir"), help="a benchmark to include in the measurement; at least two are required")

args = parser.parse_args()

totalMem = args.totalMem
time = args.time
runs = args.runs
ignoreCrossProduct = args.ignoreCrossProduct
pointsFile = args.pointsFile

if len(args.benchmark) < 2:
    parser.error("At least two benchmarks must be specified")

benchmarks = []
srcDirs = []
for b in args.benchmark:
    benchmarks.append(su.BenchmarkId(b[0], b[3], b[1], b[2]))
    srcDirs.append(su.MeasureDir(su.dirMustExist(b[4])))

minHeaps = map(lambda b: b.minHeap(), benchmarks)
if sum(minHeaps) > totalMem:
    parser.error("sum(minheaps) is greater than the available memory budget")


su.checkDaCapo()

outdir = su.UtilitySpaceDir(su.outputDir.join(su.utilitySpaceDirName(benchmarks, time)))
outdir.makedir()
outdir.writeBenchmarksFile(benchmarks)
outdir.writeRuns(runs)
outdir.writeTotalMem(totalMem)
outdir.writeRunTime(time)
outdir.machineInfo()

if pointsFile:
    measurepoints = outdir.pointsFromFile(benchmarks, srcDirs, minHeaps, totalMem, pointsFile)
else:
    measurepoints = outdir.pointsFromMeasure(benchmarks, srcDirs, minHeaps, totalMem, ignoreCrossProduct)

maxHeaps = outdir.maxHeaps(measurepoints)

starttime = outdir.timestampStartLocal(1, su.timeToSeconds(time), runs, len(measurepoints))

for point in measurepoints:
    utilitySpacePoint(time, runs, outdir, outdir.hostRawFile(su.me()), benchmarks, point, maxHeaps)

postprocess(outdir)

outdir.timestampEndLocal(starttime)
