import Economem.BenchmarkTimer
import Economem.ResumeExperimentCmdline

main :: IO ()
main = resume resumeBenchmarkTimer
