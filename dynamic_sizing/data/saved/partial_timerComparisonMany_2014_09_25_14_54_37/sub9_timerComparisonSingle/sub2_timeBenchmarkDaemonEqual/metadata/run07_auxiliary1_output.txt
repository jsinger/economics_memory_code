economemd
    total memory: 3657 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub9_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 28014: heap size 9 MB, throughput 0.988293
Clients: 1
Client 28014 has a minimum heap size of 8 MB
Reading from 28013: heap size 9 MB, throughput 0.991558
Clients: 2
Client 28013 has a minimum heap size of 1211 MB
Reading from 28013: heap size 9 MB, throughput 0.973151
Reading from 28014: heap size 9 MB, throughput 0.978064
Reading from 28013: heap size 9 MB, throughput 0.958926
Reading from 28014: heap size 11 MB, throughput 0.952392
Reading from 28014: heap size 11 MB, throughput 0.929159
Reading from 28013: heap size 9 MB, throughput 0.953984
Reading from 28013: heap size 11 MB, throughput 0.987138
Reading from 28014: heap size 15 MB, throughput 0.87335
Reading from 28014: heap size 20 MB, throughput 0.980623
Reading from 28013: heap size 11 MB, throughput 0.9741
Reading from 28014: heap size 24 MB, throughput 0.983964
Reading from 28014: heap size 29 MB, throughput 0.974984
Reading from 28013: heap size 17 MB, throughput 0.980947
Reading from 28014: heap size 31 MB, throughput 0.973698
Reading from 28014: heap size 31 MB, throughput 0.947028
Reading from 28014: heap size 34 MB, throughput 0.971016
Reading from 28014: heap size 34 MB, throughput 0.979905
Reading from 28014: heap size 38 MB, throughput 0.960883
Reading from 28013: heap size 17 MB, throughput 0.680092
Reading from 28014: heap size 38 MB, throughput 0.976364
Reading from 28014: heap size 43 MB, throughput 0.979188
Reading from 28013: heap size 30 MB, throughput 0.852865
Reading from 28014: heap size 43 MB, throughput 0.972546
Reading from 28014: heap size 50 MB, throughput 0.970168
Reading from 28014: heap size 50 MB, throughput 0.96803
Reading from 28013: heap size 31 MB, throughput 0.960797
Reading from 28014: heap size 55 MB, throughput 0.971498
Reading from 28014: heap size 55 MB, throughput 0.984187
Reading from 28013: heap size 34 MB, throughput 0.569869
Reading from 28014: heap size 60 MB, throughput 0.988791
Reading from 28013: heap size 48 MB, throughput 0.812729
Reading from 28014: heap size 61 MB, throughput 0.989865
Reading from 28013: heap size 52 MB, throughput 0.83164
Reading from 28014: heap size 66 MB, throughput 0.98733
Reading from 28014: heap size 66 MB, throughput 0.981776
Reading from 28013: heap size 53 MB, throughput 0.300854
Reading from 28014: heap size 72 MB, throughput 0.991143
Reading from 28013: heap size 73 MB, throughput 0.831426
Reading from 28014: heap size 72 MB, throughput 0.990603
Reading from 28014: heap size 75 MB, throughput 0.990597
Reading from 28013: heap size 74 MB, throughput 0.219825
Reading from 28014: heap size 76 MB, throughput 0.989028
Reading from 28013: heap size 102 MB, throughput 0.764453
Reading from 28014: heap size 80 MB, throughput 0.98733
Reading from 28013: heap size 102 MB, throughput 0.854409
Reading from 28014: heap size 80 MB, throughput 0.987669
Reading from 28014: heap size 84 MB, throughput 0.98982
Reading from 28013: heap size 104 MB, throughput 0.243104
Reading from 28014: heap size 84 MB, throughput 0.990485
Reading from 28013: heap size 132 MB, throughput 0.680446
Reading from 28014: heap size 88 MB, throughput 0.990624
Reading from 28013: heap size 138 MB, throughput 0.844637
Reading from 28013: heap size 139 MB, throughput 0.712216
Reading from 28014: heap size 88 MB, throughput 0.993831
Reading from 28013: heap size 145 MB, throughput 0.746774
Reading from 28014: heap size 91 MB, throughput 0.993802
Reading from 28014: heap size 91 MB, throughput 0.98797
Reading from 28014: heap size 94 MB, throughput 0.994498
Reading from 28013: heap size 148 MB, throughput 0.113564
Reading from 28014: heap size 94 MB, throughput 0.995079
Reading from 28013: heap size 192 MB, throughput 0.688554
Reading from 28014: heap size 97 MB, throughput 0.993597
Reading from 28013: heap size 193 MB, throughput 0.714947
Reading from 28014: heap size 97 MB, throughput 0.99293
Reading from 28013: heap size 195 MB, throughput 0.645644
Reading from 28014: heap size 100 MB, throughput 0.993483
Reading from 28013: heap size 203 MB, throughput 0.754305
Reading from 28014: heap size 100 MB, throughput 0.98596
Reading from 28013: heap size 206 MB, throughput 0.722223
Reading from 28014: heap size 103 MB, throughput 0.983394
Reading from 28013: heap size 212 MB, throughput 0.658866
Reading from 28014: heap size 103 MB, throughput 0.987593
Reading from 28013: heap size 216 MB, throughput 0.565079
Reading from 28014: heap size 107 MB, throughput 0.987239
Reading from 28014: heap size 107 MB, throughput 0.988301
Reading from 28014: heap size 111 MB, throughput 0.991164
Reading from 28014: heap size 111 MB, throughput 0.993517
Reading from 28013: heap size 226 MB, throughput 0.14174
Reading from 28014: heap size 114 MB, throughput 0.994223
Reading from 28013: heap size 268 MB, throughput 0.466844
Reading from 28013: heap size 278 MB, throughput 0.634859
Reading from 28014: heap size 114 MB, throughput 0.994618
Reading from 28013: heap size 281 MB, throughput 0.528019
Reading from 28014: heap size 117 MB, throughput 0.994678
Reading from 28013: heap size 287 MB, throughput 0.578246
Reading from 28013: heap size 294 MB, throughput 0.550338
Reading from 28014: heap size 118 MB, throughput 0.994135
Reading from 28014: heap size 120 MB, throughput 0.993862
Reading from 28014: heap size 120 MB, throughput 0.995727
Reading from 28014: heap size 123 MB, throughput 0.996712
Reading from 28013: heap size 298 MB, throughput 0.0891167
Reading from 28014: heap size 123 MB, throughput 0.993125
Reading from 28013: heap size 353 MB, throughput 0.44206
Reading from 28013: heap size 353 MB, throughput 0.599573
Reading from 28014: heap size 126 MB, throughput 0.996388
Reading from 28014: heap size 126 MB, throughput 0.995923
Reading from 28014: heap size 128 MB, throughput 0.995404
Reading from 28013: heap size 350 MB, throughput 0.127778
Reading from 28013: heap size 399 MB, throughput 0.540122
Reading from 28014: heap size 128 MB, throughput 0.996535
Reading from 28013: heap size 391 MB, throughput 0.66034
Reading from 28014: heap size 130 MB, throughput 0.996024
Reading from 28013: heap size 396 MB, throughput 0.594877
Reading from 28014: heap size 131 MB, throughput 0.991718
Reading from 28013: heap size 397 MB, throughput 0.629705
Reading from 28013: heap size 400 MB, throughput 0.539227
Reading from 28014: heap size 132 MB, throughput 0.994615
Reading from 28013: heap size 403 MB, throughput 0.546412
Reading from 28014: heap size 132 MB, throughput 0.9954
Reading from 28013: heap size 411 MB, throughput 0.540094
Reading from 28014: heap size 135 MB, throughput 0.991773
Reading from 28013: heap size 417 MB, throughput 0.534557
Reading from 28014: heap size 135 MB, throughput 0.98761
Reading from 28013: heap size 427 MB, throughput 0.504507
Reading from 28014: heap size 138 MB, throughput 0.993516
Reading from 28013: heap size 432 MB, throughput 0.53453
Reading from 28014: heap size 138 MB, throughput 0.989896
Reading from 28014: heap size 141 MB, throughput 0.994914
Reading from 28014: heap size 141 MB, throughput 0.994113
Reading from 28013: heap size 441 MB, throughput 0.0794687
Reading from 28014: heap size 145 MB, throughput 0.991836
Equal recommendation: 1828 MB each
Reading from 28013: heap size 497 MB, throughput 0.354848
Reading from 28014: heap size 145 MB, throughput 0.992028
Reading from 28013: heap size 503 MB, throughput 0.424907
Reading from 28014: heap size 148 MB, throughput 0.995671
Reading from 28014: heap size 148 MB, throughput 0.989059
Reading from 28014: heap size 152 MB, throughput 0.995252
Reading from 28013: heap size 500 MB, throughput 0.0840941
Reading from 28014: heap size 152 MB, throughput 0.992458
Reading from 28013: heap size 562 MB, throughput 0.378295
Reading from 28014: heap size 156 MB, throughput 0.995102
Reading from 28013: heap size 553 MB, throughput 0.515455
Reading from 28013: heap size 486 MB, throughput 0.545019
Reading from 28014: heap size 156 MB, throughput 0.991421
Reading from 28013: heap size 546 MB, throughput 0.522464
Reading from 28014: heap size 158 MB, throughput 0.992693
Reading from 28014: heap size 159 MB, throughput 0.993778
Reading from 28014: heap size 163 MB, throughput 0.997658
Reading from 28013: heap size 551 MB, throughput 0.098807
Reading from 28013: heap size 612 MB, throughput 0.504814
Reading from 28014: heap size 163 MB, throughput 0.983099
Reading from 28013: heap size 614 MB, throughput 0.674205
Reading from 28014: heap size 167 MB, throughput 0.993036
Reading from 28013: heap size 613 MB, throughput 0.609133
Reading from 28013: heap size 614 MB, throughput 0.572401
Reading from 28014: heap size 167 MB, throughput 0.995196
Reading from 28013: heap size 616 MB, throughput 0.54273
Reading from 28014: heap size 171 MB, throughput 0.994759
Reading from 28013: heap size 624 MB, throughput 0.569319
Reading from 28014: heap size 171 MB, throughput 0.995745
Reading from 28014: heap size 175 MB, throughput 0.995427
Reading from 28013: heap size 630 MB, throughput 0.793783
Reading from 28014: heap size 175 MB, throughput 0.996479
Reading from 28014: heap size 179 MB, throughput 0.995573
Reading from 28013: heap size 641 MB, throughput 0.783984
Reading from 28014: heap size 179 MB, throughput 0.994639
Reading from 28014: heap size 182 MB, throughput 0.995491
Reading from 28013: heap size 643 MB, throughput 0.732664
Reading from 28014: heap size 182 MB, throughput 0.993823
Reading from 28014: heap size 186 MB, throughput 0.994796
Reading from 28014: heap size 186 MB, throughput 0.993956
Reading from 28013: heap size 659 MB, throughput 0.813082
Reading from 28014: heap size 189 MB, throughput 0.994067
Reading from 28013: heap size 665 MB, throughput 0.594667
Reading from 28014: heap size 189 MB, throughput 0.996835
Reading from 28014: heap size 192 MB, throughput 0.997016
Reading from 28014: heap size 193 MB, throughput 0.994184
Reading from 28014: heap size 196 MB, throughput 0.996423
Reading from 28013: heap size 681 MB, throughput 0.034491
Reading from 28014: heap size 196 MB, throughput 0.995721
Reading from 28013: heap size 751 MB, throughput 0.345241
Reading from 28013: heap size 756 MB, throughput 0.261284
Reading from 28013: heap size 754 MB, throughput 0.223439
Reading from 28014: heap size 200 MB, throughput 0.99588
Reading from 28014: heap size 200 MB, throughput 0.996371
Equal recommendation: 1828 MB each
Reading from 28014: heap size 202 MB, throughput 0.995501
Reading from 28013: heap size 653 MB, throughput 0.0238722
Reading from 28014: heap size 203 MB, throughput 0.992318
Reading from 28013: heap size 828 MB, throughput 0.18661
Reading from 28013: heap size 831 MB, throughput 0.347223
Reading from 28014: heap size 206 MB, throughput 0.995647
Reading from 28014: heap size 206 MB, throughput 0.996066
Reading from 28014: heap size 209 MB, throughput 0.995336
Reading from 28013: heap size 835 MB, throughput 0.0316402
Reading from 28014: heap size 209 MB, throughput 0.995441
Reading from 28013: heap size 931 MB, throughput 0.586183
Reading from 28013: heap size 935 MB, throughput 0.919574
Reading from 28014: heap size 213 MB, throughput 0.997825
Reading from 28013: heap size 942 MB, throughput 0.686415
Reading from 28013: heap size 944 MB, throughput 0.668232
Reading from 28013: heap size 945 MB, throughput 0.607026
Reading from 28013: heap size 937 MB, throughput 0.621956
Reading from 28014: heap size 213 MB, throughput 0.997779
Reading from 28013: heap size 726 MB, throughput 0.881282
Reading from 28013: heap size 927 MB, throughput 0.863047
Reading from 28014: heap size 215 MB, throughput 0.997415
Reading from 28013: heap size 729 MB, throughput 0.757933
Reading from 28013: heap size 914 MB, throughput 0.898016
Reading from 28014: heap size 216 MB, throughput 0.996813
Reading from 28013: heap size 733 MB, throughput 0.76869
Reading from 28013: heap size 900 MB, throughput 0.906827
Reading from 28014: heap size 219 MB, throughput 0.995862
Reading from 28013: heap size 742 MB, throughput 0.830565
Reading from 28013: heap size 884 MB, throughput 0.835829
Reading from 28014: heap size 219 MB, throughput 0.995284
Reading from 28013: heap size 741 MB, throughput 0.884994
Reading from 28014: heap size 223 MB, throughput 0.996272
Reading from 28013: heap size 882 MB, throughput 0.681535
Reading from 28013: heap size 784 MB, throughput 0.631537
Reading from 28014: heap size 223 MB, throughput 0.995513
Reading from 28013: heap size 870 MB, throughput 0.710196
Reading from 28013: heap size 786 MB, throughput 0.668156
Reading from 28014: heap size 227 MB, throughput 0.996925
Reading from 28013: heap size 864 MB, throughput 0.720484
Reading from 28013: heap size 869 MB, throughput 0.720536
Reading from 28014: heap size 227 MB, throughput 0.988553
Reading from 28013: heap size 859 MB, throughput 0.698048
Reading from 28013: heap size 864 MB, throughput 0.730912
Reading from 28013: heap size 852 MB, throughput 0.766461
Reading from 28014: heap size 231 MB, throughput 0.996686
Reading from 28013: heap size 858 MB, throughput 0.765913
Reading from 28013: heap size 849 MB, throughput 0.780725
Reading from 28014: heap size 231 MB, throughput 0.994955
Reading from 28013: heap size 854 MB, throughput 0.785869
Reading from 28014: heap size 235 MB, throughput 0.996273
Reading from 28014: heap size 235 MB, throughput 0.994278
Reading from 28013: heap size 849 MB, throughput 0.963594
Reading from 28014: heap size 239 MB, throughput 0.99578
Reading from 28014: heap size 239 MB, throughput 0.994093
Reading from 28014: heap size 244 MB, throughput 0.996605
Reading from 28014: heap size 244 MB, throughput 0.996457
Reading from 28013: heap size 853 MB, throughput 0.960019
Reading from 28013: heap size 855 MB, throughput 0.695402
Reading from 28014: heap size 248 MB, throughput 0.994129
Reading from 28013: heap size 860 MB, throughput 0.752765
Reading from 28013: heap size 868 MB, throughput 0.697421
Reading from 28013: heap size 871 MB, throughput 0.737021
Reading from 28014: heap size 248 MB, throughput 0.933759
Reading from 28013: heap size 878 MB, throughput 0.727773
Reading from 28013: heap size 880 MB, throughput 0.723515
Reading from 28014: heap size 254 MB, throughput 0.997841
Reading from 28013: heap size 887 MB, throughput 0.724605
Reading from 28013: heap size 888 MB, throughput 0.703554
Reading from 28014: heap size 254 MB, throughput 0.995977
Equal recommendation: 1828 MB each
Reading from 28013: heap size 895 MB, throughput 0.719595
Reading from 28014: heap size 261 MB, throughput 0.99714
Reading from 28013: heap size 895 MB, throughput 0.864785
Reading from 28013: heap size 898 MB, throughput 0.823644
Reading from 28014: heap size 262 MB, throughput 0.997343
Reading from 28014: heap size 268 MB, throughput 0.995925
Reading from 28013: heap size 901 MB, throughput 0.881985
Reading from 28013: heap size 901 MB, throughput 0.721427
Reading from 28014: heap size 268 MB, throughput 0.997046
Reading from 28014: heap size 274 MB, throughput 0.997917
Reading from 28014: heap size 275 MB, throughput 0.997642
Reading from 28014: heap size 280 MB, throughput 0.998426
Reading from 28013: heap size 904 MB, throughput 0.117473
Reading from 28013: heap size 977 MB, throughput 0.338469
Reading from 28014: heap size 280 MB, throughput 0.996956
Reading from 28013: heap size 999 MB, throughput 0.552786
Reading from 28013: heap size 996 MB, throughput 0.533078
Reading from 28014: heap size 286 MB, throughput 0.995636
Reading from 28013: heap size 999 MB, throughput 0.46747
Reading from 28013: heap size 1005 MB, throughput 0.52241
Reading from 28014: heap size 286 MB, throughput 0.980851
Reading from 28013: heap size 1007 MB, throughput 0.484342
Reading from 28014: heap size 292 MB, throughput 0.99388
Reading from 28013: heap size 1021 MB, throughput 0.561225
Reading from 28013: heap size 1020 MB, throughput 0.455451
Reading from 28014: heap size 293 MB, throughput 0.994916
Reading from 28013: heap size 1037 MB, throughput 0.505966
Reading from 28013: heap size 1038 MB, throughput 0.482289
Reading from 28014: heap size 305 MB, throughput 0.994968
Reading from 28013: heap size 1057 MB, throughput 0.524238
Reading from 28014: heap size 306 MB, throughput 0.996349
Reading from 28014: heap size 316 MB, throughput 0.994457
Reading from 28014: heap size 317 MB, throughput 0.994505
Reading from 28014: heap size 328 MB, throughput 0.995804
Reading from 28013: heap size 1059 MB, throughput 0.960399
Reading from 28014: heap size 328 MB, throughput 0.996219
Reading from 28014: heap size 338 MB, throughput 0.997183
Reading from 28014: heap size 338 MB, throughput 0.995981
Reading from 28013: heap size 1083 MB, throughput 0.912618
Equal recommendation: 1828 MB each
Reading from 28014: heap size 347 MB, throughput 0.997142
Reading from 28014: heap size 348 MB, throughput 0.996782
Reading from 28014: heap size 356 MB, throughput 0.997006
Reading from 28014: heap size 357 MB, throughput 0.996416
Reading from 28013: heap size 1086 MB, throughput 0.908226
Reading from 28014: heap size 365 MB, throughput 0.998217
Reading from 28014: heap size 365 MB, throughput 0.998421
Reading from 28014: heap size 371 MB, throughput 0.99833
Reading from 28014: heap size 372 MB, throughput 0.99775
Reading from 28013: heap size 1105 MB, throughput 0.599586
Reading from 28014: heap size 379 MB, throughput 0.998019
Reading from 28014: heap size 379 MB, throughput 0.997225
Reading from 28014: heap size 385 MB, throughput 0.997967
Reading from 28013: heap size 1189 MB, throughput 0.985631
Reading from 28014: heap size 385 MB, throughput 0.997384
Reading from 28014: heap size 392 MB, throughput 0.997562
Reading from 28014: heap size 392 MB, throughput 0.996176
Reading from 28013: heap size 1198 MB, throughput 0.980302
Reading from 28014: heap size 398 MB, throughput 0.997923
Reading from 28014: heap size 398 MB, throughput 0.996859
Reading from 28014: heap size 406 MB, throughput 0.997655
Reading from 28013: heap size 1206 MB, throughput 0.976234
Equal recommendation: 1828 MB each
Reading from 28014: heap size 406 MB, throughput 0.997567
Reading from 28014: heap size 413 MB, throughput 0.997915
Reading from 28014: heap size 413 MB, throughput 0.996629
Reading from 28013: heap size 1220 MB, throughput 0.979254
Reading from 28014: heap size 421 MB, throughput 0.997337
Reading from 28014: heap size 421 MB, throughput 0.997237
Reading from 28014: heap size 429 MB, throughput 0.998013
Reading from 28013: heap size 1221 MB, throughput 0.967788
Reading from 28014: heap size 429 MB, throughput 0.996326
Reading from 28014: heap size 437 MB, throughput 0.997141
Reading from 28014: heap size 437 MB, throughput 0.997291
Reading from 28013: heap size 1219 MB, throughput 0.967914
Reading from 28014: heap size 445 MB, throughput 0.997542
Reading from 28014: heap size 445 MB, throughput 0.997092
Reading from 28014: heap size 454 MB, throughput 0.997007
Reading from 28013: heap size 1224 MB, throughput 0.961122
Reading from 28014: heap size 454 MB, throughput 0.997084
Reading from 28014: heap size 463 MB, throughput 0.997965
Reading from 28014: heap size 463 MB, throughput 0.997975
Reading from 28013: heap size 1213 MB, throughput 0.95961
Equal recommendation: 1828 MB each
Reading from 28014: heap size 470 MB, throughput 0.997848
Reading from 28014: heap size 471 MB, throughput 0.996997
Reading from 28014: heap size 479 MB, throughput 0.997575
Reading from 28013: heap size 1220 MB, throughput 0.957523
Reading from 28014: heap size 479 MB, throughput 0.997961
Reading from 28014: heap size 487 MB, throughput 0.997431
Reading from 28014: heap size 487 MB, throughput 0.997182
Reading from 28013: heap size 1213 MB, throughput 0.952859
Reading from 28014: heap size 496 MB, throughput 0.998139
Reading from 28014: heap size 496 MB, throughput 0.997446
Reading from 28013: heap size 1216 MB, throughput 0.950272
Reading from 28014: heap size 503 MB, throughput 0.998187
Reading from 28014: heap size 504 MB, throughput 0.997653
Reading from 28014: heap size 512 MB, throughput 0.997543
Reading from 28013: heap size 1221 MB, throughput 0.950653
Reading from 28014: heap size 512 MB, throughput 0.997849
Reading from 28014: heap size 521 MB, throughput 0.998042
Equal recommendation: 1828 MB each
Reading from 28014: heap size 521 MB, throughput 0.998025
Reading from 28013: heap size 1223 MB, throughput 0.940838
Reading from 28014: heap size 528 MB, throughput 0.997243
Reading from 28014: heap size 529 MB, throughput 0.998076
Reading from 28014: heap size 537 MB, throughput 0.997748
Reading from 28013: heap size 1229 MB, throughput 0.943169
Reading from 28014: heap size 537 MB, throughput 0.997984
Reading from 28014: heap size 545 MB, throughput 0.998129
Reading from 28013: heap size 1234 MB, throughput 0.935207
Reading from 28014: heap size 546 MB, throughput 0.998196
Reading from 28014: heap size 553 MB, throughput 0.998101
Reading from 28014: heap size 554 MB, throughput 0.997684
Reading from 28013: heap size 1241 MB, throughput 0.941545
Reading from 28014: heap size 562 MB, throughput 0.998668
Reading from 28014: heap size 562 MB, throughput 0.998304
Reading from 28013: heap size 1249 MB, throughput 0.932374
Reading from 28014: heap size 569 MB, throughput 0.998607
Equal recommendation: 1828 MB each
Reading from 28014: heap size 569 MB, throughput 0.997547
Reading from 28014: heap size 577 MB, throughput 0.998348
Reading from 28013: heap size 1260 MB, throughput 0.934959
Reading from 28014: heap size 577 MB, throughput 0.998242
Reading from 28014: heap size 584 MB, throughput 0.997694
Reading from 28014: heap size 585 MB, throughput 0.998088
Reading from 28013: heap size 1265 MB, throughput 0.945738
Reading from 28014: heap size 593 MB, throughput 0.998368
Reading from 28014: heap size 593 MB, throughput 0.998082
Reading from 28013: heap size 1275 MB, throughput 0.931975
Reading from 28014: heap size 602 MB, throughput 0.99817
Reading from 28014: heap size 602 MB, throughput 0.998233
Reading from 28014: heap size 610 MB, throughput 0.998454
Reading from 28013: heap size 1278 MB, throughput 0.934237
Reading from 28014: heap size 610 MB, throughput 0.998229
Equal recommendation: 1828 MB each
Reading from 28014: heap size 618 MB, throughput 0.998167
Reading from 28013: heap size 1288 MB, throughput 0.937913
Reading from 28014: heap size 618 MB, throughput 0.997663
Reading from 28014: heap size 626 MB, throughput 0.998259
Reading from 28014: heap size 626 MB, throughput 0.997949
Reading from 28013: heap size 1290 MB, throughput 0.934567
Reading from 28014: heap size 635 MB, throughput 0.998421
Reading from 28014: heap size 635 MB, throughput 0.998294
Reading from 28014: heap size 644 MB, throughput 0.998215
Reading from 28013: heap size 1300 MB, throughput 0.945367
Reading from 28014: heap size 644 MB, throughput 0.99841
Reading from 28014: heap size 652 MB, throughput 0.998732
Reading from 28013: heap size 1301 MB, throughput 0.932667
Reading from 28014: heap size 652 MB, throughput 0.998204
Equal recommendation: 1828 MB each
Reading from 28014: heap size 660 MB, throughput 0.998549
Reading from 28014: heap size 660 MB, throughput 0.998397
Reading from 28013: heap size 1311 MB, throughput 0.941597
Reading from 28014: heap size 668 MB, throughput 0.99841
Reading from 28014: heap size 668 MB, throughput 0.998177
Reading from 28014: heap size 676 MB, throughput 0.998748
Reading from 28013: heap size 1312 MB, throughput 0.943282
Reading from 28014: heap size 676 MB, throughput 0.998204
Reading from 28014: heap size 684 MB, throughput 0.998633
Reading from 28013: heap size 1322 MB, throughput 0.94605
Reading from 28014: heap size 684 MB, throughput 0.998684
Reading from 28014: heap size 692 MB, throughput 0.998569
Equal recommendation: 1828 MB each
Reading from 28014: heap size 692 MB, throughput 0.998471
Reading from 28013: heap size 1322 MB, throughput 0.953668
Reading from 28014: heap size 700 MB, throughput 0.998857
Reading from 28014: heap size 700 MB, throughput 0.998439
Reading from 28014: heap size 708 MB, throughput 0.998883
Reading from 28014: heap size 708 MB, throughput 0.998572
Reading from 28013: heap size 1331 MB, throughput 0.502676
Reading from 28014: heap size 715 MB, throughput 0.998893
Reading from 28014: heap size 715 MB, throughput 0.998701
Reading from 28014: heap size 723 MB, throughput 0.998784
Reading from 28013: heap size 1440 MB, throughput 0.916456
Reading from 28014: heap size 723 MB, throughput 0.998316
Equal recommendation: 1828 MB each
Reading from 28014: heap size 730 MB, throughput 0.998683
Reading from 28013: heap size 1442 MB, throughput 0.975736
Reading from 28014: heap size 730 MB, throughput 0.998859
Reading from 28014: heap size 737 MB, throughput 0.998596
Reading from 28014: heap size 737 MB, throughput 0.998437
Reading from 28013: heap size 1450 MB, throughput 0.965937
Reading from 28014: heap size 745 MB, throughput 0.998334
Reading from 28014: heap size 745 MB, throughput 0.998256
Reading from 28014: heap size 753 MB, throughput 0.993717
Reading from 28013: heap size 1463 MB, throughput 0.975393
Reading from 28014: heap size 753 MB, throughput 0.998686
Equal recommendation: 1828 MB each
Reading from 28014: heap size 766 MB, throughput 0.998468
Reading from 28013: heap size 1464 MB, throughput 0.963105
Reading from 28014: heap size 768 MB, throughput 0.998571
Reading from 28014: heap size 779 MB, throughput 0.998445
Reading from 28014: heap size 779 MB, throughput 0.998908
Reading from 28013: heap size 1459 MB, throughput 0.963528
Reading from 28014: heap size 789 MB, throughput 0.998801
Reading from 28014: heap size 790 MB, throughput 0.998299
Reading from 28013: heap size 1465 MB, throughput 0.962337
Reading from 28014: heap size 798 MB, throughput 0.999096
Reading from 28014: heap size 798 MB, throughput 0.998504
Reading from 28014: heap size 806 MB, throughput 0.998519
Equal recommendation: 1828 MB each
Reading from 28014: heap size 807 MB, throughput 0.998603
Reading from 28014: heap size 815 MB, throughput 0.998429
Reading from 28014: heap size 815 MB, throughput 0.998615
Reading from 28014: heap size 825 MB, throughput 0.998436
Reading from 28014: heap size 825 MB, throughput 0.998835
Reading from 28014: heap size 833 MB, throughput 0.998918
Reading from 28014: heap size 834 MB, throughput 0.998597
Equal recommendation: 1828 MB each
Reading from 28014: heap size 842 MB, throughput 0.998844
Reading from 28014: heap size 842 MB, throughput 0.998623
Reading from 28014: heap size 850 MB, throughput 0.998856
Reading from 28014: heap size 850 MB, throughput 0.998713
Reading from 28014: heap size 859 MB, throughput 0.998788
Reading from 28014: heap size 859 MB, throughput 0.99881
Reading from 28014: heap size 867 MB, throughput 0.998738
Reading from 28014: heap size 867 MB, throughput 0.998701
Equal recommendation: 1828 MB each
Reading from 28014: heap size 875 MB, throughput 0.998768
Reading from 28014: heap size 875 MB, throughput 0.999043
Reading from 28014: heap size 883 MB, throughput 0.999039
Reading from 28014: heap size 884 MB, throughput 0.998647
Reading from 28014: heap size 891 MB, throughput 0.999125
Reading from 28013: heap size 1452 MB, throughput 0.994371
Reading from 28014: heap size 891 MB, throughput 0.998867
Reading from 28014: heap size 898 MB, throughput 0.998921
Equal recommendation: 1828 MB each
Reading from 28014: heap size 898 MB, throughput 0.998838
Reading from 28014: heap size 906 MB, throughput 0.999009
Reading from 28014: heap size 906 MB, throughput 0.998839
Reading from 28013: heap size 1460 MB, throughput 0.975513
Reading from 28014: heap size 913 MB, throughput 0.998763
Reading from 28013: heap size 1452 MB, throughput 0.815839
Reading from 28013: heap size 1484 MB, throughput 0.663793
Reading from 28013: heap size 1507 MB, throughput 0.648419
Reading from 28014: heap size 913 MB, throughput 0.998777
Reading from 28013: heap size 1536 MB, throughput 0.638625
Reading from 28013: heap size 1569 MB, throughput 0.659249
Reading from 28013: heap size 1587 MB, throughput 0.643985
Reading from 28014: heap size 921 MB, throughput 0.998896
Reading from 28014: heap size 921 MB, throughput 0.998987
Reading from 28013: heap size 1624 MB, throughput 0.914671
Equal recommendation: 1828 MB each
Reading from 28014: heap size 929 MB, throughput 0.99863
Reading from 28014: heap size 929 MB, throughput 0.998888
Reading from 28013: heap size 1634 MB, throughput 0.936359
Reading from 28014: heap size 938 MB, throughput 0.998707
Reading from 28014: heap size 938 MB, throughput 0.998824
Reading from 28013: heap size 1670 MB, throughput 0.939697
Reading from 28014: heap size 946 MB, throughput 0.999122
Reading from 28014: heap size 946 MB, throughput 0.99886
Reading from 28013: heap size 1674 MB, throughput 0.937937
Reading from 28014: heap size 954 MB, throughput 0.998989
Equal recommendation: 1828 MB each
Reading from 28014: heap size 954 MB, throughput 0.998918
Reading from 28013: heap size 1704 MB, throughput 0.947389
Reading from 28014: heap size 962 MB, throughput 0.999056
Reading from 28014: heap size 962 MB, throughput 0.998789
Reading from 28014: heap size 970 MB, throughput 0.995708
Reading from 28013: heap size 1705 MB, throughput 0.956083
Reading from 28014: heap size 970 MB, throughput 0.998815
Reading from 28014: heap size 983 MB, throughput 0.999026
Reading from 28013: heap size 1725 MB, throughput 0.944734
Reading from 28014: heap size 984 MB, throughput 0.998668
Equal recommendation: 1828 MB each
Reading from 28014: heap size 994 MB, throughput 0.999121
Reading from 28014: heap size 994 MB, throughput 0.998177
Reading from 28013: heap size 1728 MB, throughput 0.730122
Reading from 28014: heap size 1004 MB, throughput 0.999261
Reading from 28014: heap size 1004 MB, throughput 0.998838
Reading from 28013: heap size 1709 MB, throughput 0.987756
Reading from 28014: heap size 1014 MB, throughput 0.999114
Reading from 28014: heap size 1014 MB, throughput 0.998715
Equal recommendation: 1828 MB each
Reading from 28014: heap size 1023 MB, throughput 0.999055
Reading from 28013: heap size 1725 MB, throughput 0.983564
Reading from 28014: heap size 1023 MB, throughput 0.998747
Reading from 28014: heap size 1032 MB, throughput 0.99906
Reading from 28013: heap size 1756 MB, throughput 0.979978
Reading from 28014: heap size 1032 MB, throughput 0.998762
Reading from 28014: heap size 1041 MB, throughput 0.99897
Reading from 28014: heap size 1041 MB, throughput 0.998836
Reading from 28013: heap size 1763 MB, throughput 0.976533
Equal recommendation: 1828 MB each
Reading from 28014: heap size 1050 MB, throughput 0.999014
Reading from 28014: heap size 1050 MB, throughput 0.998746
Reading from 28013: heap size 1766 MB, throughput 0.976333
Reading from 28014: heap size 1059 MB, throughput 0.999175
Reading from 28014: heap size 1059 MB, throughput 0.998968
Reading from 28014: heap size 1068 MB, throughput 0.999084
Reading from 28013: heap size 1771 MB, throughput 0.983501
Reading from 28014: heap size 1068 MB, throughput 0.998993
Equal recommendation: 1828 MB each
Reading from 28014: heap size 1076 MB, throughput 0.99912
Reading from 28013: heap size 1748 MB, throughput 0.972043
Reading from 28014: heap size 1076 MB, throughput 0.999073
Reading from 28014: heap size 1084 MB, throughput 0.999153
Reading from 28014: heap size 1085 MB, throughput 0.998764
Reading from 28013: heap size 1521 MB, throughput 0.966019
Reading from 28014: heap size 1092 MB, throughput 0.999166
Reading from 28014: heap size 1092 MB, throughput 0.999115
Equal recommendation: 1828 MB each
Reading from 28014: heap size 1100 MB, throughput 0.998781
Reading from 28013: heap size 1728 MB, throughput 0.967224
Reading from 28014: heap size 1101 MB, throughput 0.998867
Reading from 28014: heap size 1109 MB, throughput 0.999058
Reading from 28013: heap size 1562 MB, throughput 0.958229
Reading from 28014: heap size 1109 MB, throughput 0.999134
Reading from 28014: heap size 1118 MB, throughput 0.999042
Reading from 28014: heap size 1118 MB, throughput 0.998981
Equal recommendation: 1828 MB each
Reading from 28013: heap size 1718 MB, throughput 0.969407
Reading from 28014: heap size 1126 MB, throughput 0.999159
Reading from 28014: heap size 1126 MB, throughput 0.999253
Reading from 28013: heap size 1729 MB, throughput 0.960384
Reading from 28014: heap size 1134 MB, throughput 0.998947
Reading from 28014: heap size 1135 MB, throughput 0.999152
Reading from 28014: heap size 1143 MB, throughput 0.999241
Reading from 28013: heap size 1738 MB, throughput 0.961029
Equal recommendation: 1828 MB each
Reading from 28014: heap size 1143 MB, throughput 0.999126
Reading from 28014: heap size 1151 MB, throughput 0.999236
Reading from 28013: heap size 1739 MB, throughput 0.95663
Reading from 28014: heap size 1151 MB, throughput 0.99907
Reading from 28014: heap size 1158 MB, throughput 0.999152
Reading from 28014: heap size 1158 MB, throughput 0.999064
Reading from 28013: heap size 1752 MB, throughput 0.956458
Reading from 28014: heap size 1166 MB, throughput 0.999276
Equal recommendation: 1828 MB each
Reading from 28014: heap size 1166 MB, throughput 0.999018
Reading from 28013: heap size 1759 MB, throughput 0.956416
Reading from 28014: heap size 1174 MB, throughput 0.999246
Reading from 28014: heap size 1174 MB, throughput 0.998987
Reading from 28014: heap size 1182 MB, throughput 0.998909
Reading from 28013: heap size 1779 MB, throughput 0.946336
Reading from 28014: heap size 1182 MB, throughput 0.999274
Equal recommendation: 1828 MB each
Reading from 28014: heap size 1191 MB, throughput 0.999247
Reading from 28014: heap size 1191 MB, throughput 0.999236
Reading from 28014: heap size 1199 MB, throughput 0.999257
Reading from 28014: heap size 1199 MB, throughput 0.999181
Reading from 28014: heap size 1206 MB, throughput 0.9993
Reading from 28014: heap size 1206 MB, throughput 0.999142
Equal recommendation: 1828 MB each
Reading from 28014: heap size 1214 MB, throughput 0.999328
Reading from 28014: heap size 1214 MB, throughput 0.999022
Reading from 28014: heap size 1221 MB, throughput 0.999174
Reading from 28014: heap size 1221 MB, throughput 0.999102
Reading from 28014: heap size 1229 MB, throughput 0.998965
Equal recommendation: 1828 MB each
Reading from 28014: heap size 1229 MB, throughput 0.999509
Reading from 28014: heap size 1229 MB, throughput 0.999343
Reading from 28014: heap size 1229 MB, throughput 0.999321
Reading from 28014: heap size 1229 MB, throughput 0.999327
Reading from 28014: heap size 1229 MB, throughput 0.999313
Equal recommendation: 1828 MB each
Reading from 28013: heap size 1787 MB, throughput 0.992493
Reading from 28014: heap size 1229 MB, throughput 0.999423
Reading from 28014: heap size 1229 MB, throughput 0.999362
Client 28014 died
Clients: 1
Reading from 28013: heap size 1800 MB, throughput 0.977096
Reading from 28013: heap size 1765 MB, throughput 0.812199
Reading from 28013: heap size 1769 MB, throughput 0.801163
Reading from 28013: heap size 1689 MB, throughput 0.806963
Client 28013 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
