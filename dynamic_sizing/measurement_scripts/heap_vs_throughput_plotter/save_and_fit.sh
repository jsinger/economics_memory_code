#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") throughput_model prefix total_mem [daemon_args...]"
    exit 1
}

test -z "${1}" && usage
test -z "${2}" && usage
test -z "${3}" && usage

THROUGHPUT_MODEL="${1}"
PREFIX="${2}"
TOTAL_MEM="${3}"

function cleanup()
{
    test ! -z "${PLOT}" && kill "${PLOT}" &>/dev/null
    test ! -z "${DAEMON}" && kill "${DAEMON}" &>/dev/null
    wait
    rm "${FIFO}"
    exit
}

trap "cleanup" SIGINT SIGHUP SIGTERM


FIFO="$(mktemp --dry-run)"
mkfifo "${FIFO}"

python "${DIR}/demux_to_files.py" "${PREFIX}" "${THROUGHPUT_MODEL}" < "${FIFO}" &
PLOT="${!}"

"${DIR}/../../daemon/economemd" -c lru -c unbounded --reading-recv "${THROUGHPUT_MODEL}" --command-recv "${THROUGHPUT_MODEL}" --log-file "${FIFO}" --log-readings --log-functions "${@:4}" "${TOTAL_MEM}" &
DAEMON="${!}"

wait
cleanup
