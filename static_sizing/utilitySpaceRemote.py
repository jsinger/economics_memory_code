#!/usr/bin/env python
# Measure the utility of various heap size combinations, on remote machines

import argparse
import argtypes
import distributor.distribute as d
import os.path
import shutil
import staticutility as su
import tempfile

RUNS = 5 # default number of runs


parser = argparse.ArgumentParser(description="Measure the utility of various heap size combinations of two or more benchmarks, on remote machines")

parser.add_argument("hostsFile", type=argtypes.existing_absolute_path, help="hosts to run the remote job on")
parser.add_argument("gatewayFile", type=argtypes.existing_absolute_path, help="Machine for one-off setup actions")
parser.add_argument("totalMem", type=argtypes.positive_int, help="total memory a combination is allowed to use")
parser.add_argument("time", help="how long to run for, in a format suitable for input to 'sleep'")

parser.add_argument("-r", "--runs", type=argtypes.positive_int, default=RUNS, help="how many times to run the measurement, for taking averages")
parser.add_argument("--ignoreCrossProduct", action="store_true", help="Only generate points on the budget line, not those from the cross product")
parser.add_argument("--pointsFile", type=argtypes.existing_absolute_path, help="Measure the points specified in this file rather than generating them in the usual way")
parser.add_argument("-b", "--benchmark", action="append", nargs=5, default=[], metavar=("benchmark", "inputSize", "threads", "time", "measureDir"), help="a benchmark to include in the measurement; at least two are required")

parser.add_argument("-e", "--email", help="Email address to notify of completion")
parser.add_argument("--gatewayScpFile", type=argtypes.existing_absolute_path, help="Command to copy files to the gateway")
parser.add_argument("--gatewayUserFile", type=argtypes.existing_absolute_path, help="User to log into the gateway as")
parser.add_argument("--hostsSshFile", type=argtypes.existing_absolute_path, help="Command to log into the remote hosts")
parser.add_argument("--hostsUserFile", type=argtypes.existing_absolute_path, help="User to log into the remote hosts as")

args = parser.parse_args()
distributeArgs = {}
remoteMainDir = "economics_memory_code"
remoteThisDir = os.path.join(remoteMainDir, "static_sizing")

hostsFile = args.hostsFile
hosts = su.fileToList(hostsFile)
numHosts = len(hosts)
totalMem = args.totalMem
time = args.time
runs = args.runs
ignoreCrossProduct = args.ignoreCrossProduct
pointsFile = args.pointsFile
distributeArgs["gateway"] = su.firstLineOfFile(args.gatewayFile)
distributeArgs["name"] = "UtilitySpace"

if args.email:
    distributeArgs["notificationEmail"] = args.email
    distributeArgs["emailSenderConfig"] = os.path.join(remoteMainDir, "email.conf")

def fileOrNone(filename, arg):
    if filename:
        distributeArgs[arg] = su.firstLineOfFile(filename)

fileOrNone(args.gatewayScpFile, "gatewayScpCommand")
fileOrNone(args.gatewayUserFile, "gatewaySshUser")
fileOrNone(args.hostsSshFile, "hostsSshCommand")
fileOrNone(args.hostsUserFile, "hostsSshUser")

if len(args.benchmark) < 2:
    parser.error("At least two benchmarks must be specified")

benchmarks = []
srcDirs = []
for b in args.benchmark:
    benchmarks.append(su.BenchmarkId(b[0], b[3], b[1], b[2]))
    srcDirs.append(su.MeasureDir(su.dirMustExist(b[4])))

minHeaps = map(lambda b: b.minHeap(), benchmarks)
if sum(minHeaps) > totalMem:
    parser.error("sum(minheaps) is greater than the available memory budget")


tmpdir = su.UtilitySpaceDir(tempfile.mkdtemp())
tmpdir.writeBenchmarksFile(benchmarks)
tmpdir.writeRuns(runs)
tmpdir.writeTotalMem(totalMem)
tmpdir.writeRunTime(time)

if pointsFile:
    measurepoints = tmpdir.pointsFromFile(benchmarks, srcDirs, minHeaps, totalMem, pointsFile)
else:
    measurepoints = tmpdir.pointsFromMeasure(benchmarks, srcDirs, minHeaps, totalMem, ignoreCrossProduct)

maxHeaps = tmpdir.maxHeaps(measurepoints)

tmpdir.timestampStartRemote(numHosts, su.timeToSeconds(time), runs, len(measurepoints))

remoteOutdir = os.path.join(remoteThisDir, su.utilitySpaceDirName(benchmarks, time))

distributeArgs["hosts"] = hosts
distributeArgs["inputFile"] = tmpdir.measurepointsFile()
distributeArgs["remoteCommand"] = os.path.join(remoteThisDir, "utilitySpaceRemoteHarness.py")

a = [time, runs, remoteOutdir]
for (b, m) in zip(benchmarks, maxHeaps):
    a += ["--benchmark", b.name, b.time, b.inputSize, b.threads, m]
distributeArgs["remoteArgs"] = a

distributeArgs["gatewayFiles"] = [(str(tmpdir), remoteOutdir)]


try:
    d.distribute(**distributeArgs)
except d.DistributeException as e:
    print e


shutil.rmtree(str(tmpdir))

print "Don't forget to collect and process the data once it's finished"
