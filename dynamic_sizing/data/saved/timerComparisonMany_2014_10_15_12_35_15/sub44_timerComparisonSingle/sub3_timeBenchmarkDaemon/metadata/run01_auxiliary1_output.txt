economemd
    total memory: 2760 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub44_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 21012: heap size 9 MB, throughput 0.987735
Clients: 1
Reading from 21013: heap size 9 MB, throughput 0.989316
Clients: 2
Client 21013 has a minimum heap size of 276 MB
Client 21012 has a minimum heap size of 276 MB
Reading from 21012: heap size 9 MB, throughput 0.984107
Reading from 21013: heap size 9 MB, throughput 0.986659
Reading from 21012: heap size 11 MB, throughput 0.981088
Reading from 21013: heap size 11 MB, throughput 0.982987
Reading from 21012: heap size 11 MB, throughput 0.984119
Reading from 21013: heap size 11 MB, throughput 0.982466
Reading from 21012: heap size 15 MB, throughput 0.934161
Reading from 21013: heap size 15 MB, throughput 0.952779
Reading from 21012: heap size 18 MB, throughput 0.898042
Reading from 21013: heap size 18 MB, throughput 0.956302
Reading from 21012: heap size 24 MB, throughput 0.839889
Reading from 21013: heap size 23 MB, throughput 0.889408
Reading from 21012: heap size 28 MB, throughput 0.740312
Reading from 21013: heap size 28 MB, throughput 0.799075
Reading from 21013: heap size 39 MB, throughput 0.809052
Reading from 21012: heap size 32 MB, throughput 0.681539
Reading from 21013: heap size 40 MB, throughput 0.601825
Reading from 21013: heap size 42 MB, throughput 0.259397
Reading from 21012: heap size 42 MB, throughput 0.493955
Reading from 21013: heap size 44 MB, throughput 0.783881
Reading from 21012: heap size 47 MB, throughput 0.877848
Reading from 21013: heap size 49 MB, throughput 0.607014
Reading from 21013: heap size 63 MB, throughput 0.740348
Reading from 21012: heap size 48 MB, throughput 0.573511
Reading from 21012: heap size 68 MB, throughput 0.0510812
Reading from 21013: heap size 71 MB, throughput 0.317556
Reading from 21012: heap size 69 MB, throughput 0.341676
Reading from 21013: heap size 90 MB, throughput 0.0838998
Reading from 21012: heap size 95 MB, throughput 0.521116
Reading from 21013: heap size 95 MB, throughput 0.141746
Reading from 21012: heap size 96 MB, throughput 0.370065
Reading from 21012: heap size 100 MB, throughput 0.73477
Reading from 21012: heap size 103 MB, throughput 0.660085
Reading from 21013: heap size 97 MB, throughput 0.468905
Reading from 21013: heap size 129 MB, throughput 0.433628
Reading from 21013: heap size 130 MB, throughput 0.178997
Reading from 21013: heap size 133 MB, throughput 0.719995
Reading from 21012: heap size 106 MB, throughput 0.47283
Reading from 21012: heap size 138 MB, throughput 0.714772
Reading from 21013: heap size 138 MB, throughput 0.660763
Reading from 21012: heap size 142 MB, throughput 0.69613
Reading from 21013: heap size 141 MB, throughput 0.662035
Reading from 21012: heap size 145 MB, throughput 0.633265
Reading from 21012: heap size 150 MB, throughput 0.649326
Reading from 21013: heap size 146 MB, throughput 0.424165
Reading from 21013: heap size 195 MB, throughput 0.618364
Reading from 21012: heap size 156 MB, throughput 0.393486
Reading from 21012: heap size 194 MB, throughput 0.639122
Reading from 21013: heap size 197 MB, throughput 0.627881
Reading from 21012: heap size 198 MB, throughput 0.684907
Reading from 21013: heap size 200 MB, throughput 0.688348
Reading from 21012: heap size 199 MB, throughput 0.704014
Reading from 21012: heap size 202 MB, throughput 0.745964
Reading from 21013: heap size 205 MB, throughput 0.854794
Reading from 21012: heap size 206 MB, throughput 0.865958
Reading from 21013: heap size 207 MB, throughput 0.580572
Reading from 21013: heap size 256 MB, throughput 0.804882
Reading from 21012: heap size 212 MB, throughput 0.583057
Reading from 21012: heap size 263 MB, throughput 0.779709
Reading from 21013: heap size 264 MB, throughput 0.752095
Reading from 21012: heap size 266 MB, throughput 0.805582
Reading from 21013: heap size 265 MB, throughput 0.72004
Reading from 21012: heap size 266 MB, throughput 0.776975
Reading from 21013: heap size 270 MB, throughput 0.716526
Reading from 21012: heap size 268 MB, throughput 0.802595
Reading from 21013: heap size 271 MB, throughput 0.779588
Reading from 21012: heap size 270 MB, throughput 0.805906
Reading from 21013: heap size 276 MB, throughput 0.743008
Reading from 21012: heap size 271 MB, throughput 0.740894
Reading from 21013: heap size 276 MB, throughput 0.676262
Reading from 21012: heap size 273 MB, throughput 0.726716
Reading from 21013: heap size 284 MB, throughput 0.659559
Reading from 21012: heap size 275 MB, throughput 0.636322
Reading from 21013: heap size 287 MB, throughput 0.691183
Reading from 21012: heap size 283 MB, throughput 0.68571
Reading from 21013: heap size 285 MB, throughput 0.656613
Reading from 21012: heap size 284 MB, throughput 0.651954
Reading from 21012: heap size 285 MB, throughput 0.78906
Reading from 21012: heap size 288 MB, throughput 0.863227
Reading from 21013: heap size 289 MB, throughput 0.626434
Reading from 21013: heap size 327 MB, throughput 0.79839
Reading from 21013: heap size 331 MB, throughput 0.789426
Reading from 21012: heap size 286 MB, throughput 0.632448
Reading from 21013: heap size 337 MB, throughput 0.816872
Reading from 21012: heap size 331 MB, throughput 0.743725
Reading from 21013: heap size 337 MB, throughput 0.872129
Reading from 21012: heap size 335 MB, throughput 0.774558
Reading from 21013: heap size 341 MB, throughput 0.884477
Reading from 21013: heap size 343 MB, throughput 0.821375
Reading from 21012: heap size 336 MB, throughput 0.854611
Reading from 21013: heap size 349 MB, throughput 0.7814
Reading from 21012: heap size 343 MB, throughput 0.874709
Reading from 21013: heap size 349 MB, throughput 0.737044
Reading from 21012: heap size 344 MB, throughput 0.843324
Reading from 21013: heap size 358 MB, throughput 0.745896
Reading from 21012: heap size 343 MB, throughput 0.820691
Reading from 21013: heap size 358 MB, throughput 0.733118
Reading from 21012: heap size 346 MB, throughput 0.799976
Reading from 21012: heap size 345 MB, throughput 0.735296
Reading from 21012: heap size 347 MB, throughput 0.712361
Numeric result:
Recommendation: 2 clients, utility 0.446093:
    h1: 2484 MB (U(h) = 0.603515*h^0.0187682)
    h2: 276 MB (U(h) = 0.634711*h^0.001)
Recommendation: 2 clients, utility 0.446093:
    h1: 2484 MB (U(h) = 0.603515*h^0.0187682)
    h2: 276 MB (U(h) = 0.634711*h^0.001)
Reading from 21013: heap size 367 MB, throughput 0.972304
Reading from 21012: heap size 353 MB, throughput 0.946878
Reading from 21012: heap size 354 MB, throughput 0.9719
Reading from 21013: heap size 326 MB, throughput 0.974795
Reading from 21013: heap size 356 MB, throughput 0.968806
Reading from 21012: heap size 358 MB, throughput 0.976513
Reading from 21013: heap size 328 MB, throughput 0.97168
Reading from 21012: heap size 361 MB, throughput 0.978719
Reading from 21013: heap size 347 MB, throughput 0.968565
Reading from 21012: heap size 358 MB, throughput 0.977739
Reading from 21013: heap size 320 MB, throughput 0.970573
Reading from 21012: heap size 362 MB, throughput 0.980602
Reading from 21013: heap size 344 MB, throughput 0.969901
Numeric result:
Recommendation: 2 clients, utility 0.560128:
    h1: 1411.12 MB (U(h) = 0.535296*h^0.0498416)
    h2: 1348.88 MB (U(h) = 0.517073*h^0.047656)
Recommendation: 2 clients, utility 0.560128:
    h1: 1410.93 MB (U(h) = 0.535296*h^0.0498416)
    h2: 1349.07 MB (U(h) = 0.517073*h^0.047656)
Reading from 21012: heap size 360 MB, throughput 0.980823
Reading from 21013: heap size 318 MB, throughput 0.967281
Reading from 21012: heap size 363 MB, throughput 0.978004
Reading from 21013: heap size 345 MB, throughput 0.972101
Reading from 21013: heap size 344 MB, throughput 0.969227
Reading from 21012: heap size 366 MB, throughput 0.979123
Reading from 21013: heap size 343 MB, throughput 0.965831
Reading from 21012: heap size 366 MB, throughput 0.976074
Reading from 21013: heap size 344 MB, throughput 0.972295
Reading from 21012: heap size 369 MB, throughput 0.974088
Reading from 21013: heap size 346 MB, throughput 0.970394
Reading from 21012: heap size 371 MB, throughput 0.968617
Reading from 21013: heap size 346 MB, throughput 0.93108
Reading from 21012: heap size 374 MB, throughput 0.970456
Numeric result:
Recommendation: 2 clients, utility 0.649054:
    h1: 1496.31 MB (U(h) = 0.482947*h^0.0762917)
    h2: 1263.69 MB (U(h) = 0.485641*h^0.0644298)
Recommendation: 2 clients, utility 0.649054:
    h1: 1496.33 MB (U(h) = 0.482947*h^0.0762917)
    h2: 1263.67 MB (U(h) = 0.485641*h^0.0644298)
Reading from 21013: heap size 390 MB, throughput 0.965984
Reading from 21012: heap size 376 MB, throughput 0.974385
Reading from 21012: heap size 382 MB, throughput 0.962313
Reading from 21013: heap size 390 MB, throughput 0.975986
Reading from 21012: heap size 382 MB, throughput 0.938625
Reading from 21013: heap size 393 MB, throughput 0.964915
Reading from 21012: heap size 388 MB, throughput 0.916019
Reading from 21013: heap size 393 MB, throughput 0.947411
Reading from 21012: heap size 391 MB, throughput 0.896228
Reading from 21013: heap size 398 MB, throughput 0.927044
Reading from 21013: heap size 399 MB, throughput 0.905909
Reading from 21013: heap size 406 MB, throughput 0.95443
Reading from 21012: heap size 400 MB, throughput 0.976574
Reading from 21013: heap size 407 MB, throughput 0.982565
Reading from 21012: heap size 400 MB, throughput 0.982151
Reading from 21013: heap size 413 MB, throughput 0.984719
Reading from 21013: heap size 414 MB, throughput 0.985873
Reading from 21012: heap size 401 MB, throughput 0.984451
Reading from 21013: heap size 416 MB, throughput 0.985751
Reading from 21012: heap size 404 MB, throughput 0.984891
Numeric result:
Recommendation: 2 clients, utility 0.78045:
    h1: 1349.7 MB (U(h) = 0.445629*h^0.0965174)
    h2: 1410.3 MB (U(h) = 0.42044*h^0.10083)
Recommendation: 2 clients, utility 0.78045:
    h1: 1349.84 MB (U(h) = 0.445629*h^0.0965174)
    h2: 1410.16 MB (U(h) = 0.42044*h^0.10083)
Reading from 21013: heap size 418 MB, throughput 0.986068
Reading from 21012: heap size 402 MB, throughput 0.983694
Reading from 21013: heap size 417 MB, throughput 0.987173
Reading from 21012: heap size 405 MB, throughput 0.984197
Reading from 21013: heap size 419 MB, throughput 0.985418
Reading from 21012: heap size 401 MB, throughput 0.984872
Reading from 21013: heap size 417 MB, throughput 0.98517
Reading from 21012: heap size 404 MB, throughput 0.982354
Reading from 21013: heap size 419 MB, throughput 0.9857
Reading from 21013: heap size 420 MB, throughput 0.973088
Reading from 21012: heap size 402 MB, throughput 0.978048
Numeric result:
Recommendation: 2 clients, utility 0.826053:
    h1: 1307.92 MB (U(h) = 0.436221*h^0.10183)
    h2: 1452.08 MB (U(h) = 0.400334*h^0.113068)
Recommendation: 2 clients, utility 0.826053:
    h1: 1307.83 MB (U(h) = 0.436221*h^0.10183)
    h2: 1452.17 MB (U(h) = 0.400334*h^0.113068)
Reading from 21013: heap size 421 MB, throughput 0.966175
Reading from 21012: heap size 404 MB, throughput 0.958774
Reading from 21013: heap size 421 MB, throughput 0.974219
Reading from 21013: heap size 424 MB, throughput 0.95841
Reading from 21013: heap size 425 MB, throughput 0.945339
Reading from 21013: heap size 428 MB, throughput 0.919648
Reading from 21012: heap size 444 MB, throughput 0.975353
Reading from 21012: heap size 445 MB, throughput 0.966343
Reading from 21012: heap size 451 MB, throughput 0.953598
Reading from 21012: heap size 450 MB, throughput 0.944326
Reading from 21013: heap size 437 MB, throughput 0.970476
Reading from 21012: heap size 456 MB, throughput 0.982892
Reading from 21013: heap size 438 MB, throughput 0.981573
Reading from 21012: heap size 457 MB, throughput 0.986901
Reading from 21013: heap size 443 MB, throughput 0.984499
Reading from 21012: heap size 461 MB, throughput 0.989862
Reading from 21013: heap size 445 MB, throughput 0.9877
Numeric result:
Recommendation: 2 clients, utility 0.91205:
    h1: 1305.74 MB (U(h) = 0.410161*h^0.116631)
    h2: 1454.26 MB (U(h) = 0.373941*h^0.129909)
Recommendation: 2 clients, utility 0.91205:
    h1: 1305.67 MB (U(h) = 0.410161*h^0.116631)
    h2: 1454.33 MB (U(h) = 0.373941*h^0.129909)
Reading from 21013: heap size 446 MB, throughput 0.987761
Reading from 21012: heap size 463 MB, throughput 0.990883
Reading from 21013: heap size 448 MB, throughput 0.988748
Reading from 21012: heap size 462 MB, throughput 0.989841
Reading from 21013: heap size 446 MB, throughput 0.98567
Reading from 21012: heap size 465 MB, throughput 0.986154
Reading from 21013: heap size 449 MB, throughput 0.984096
Reading from 21012: heap size 465 MB, throughput 0.986395
Reading from 21013: heap size 451 MB, throughput 0.983103
Numeric result:
Recommendation: 2 clients, utility 0.949542:
    h1: 1300.51 MB (U(h) = 0.400527*h^0.122302)
    h2: 1459.49 MB (U(h) = 0.362847*h^0.137253)
Recommendation: 2 clients, utility 0.949542:
    h1: 1300.51 MB (U(h) = 0.400527*h^0.122302)
    h2: 1459.49 MB (U(h) = 0.362847*h^0.137253)
Reading from 21012: heap size 466 MB, throughput 0.98584
Reading from 21013: heap size 451 MB, throughput 0.985784
Reading from 21013: heap size 453 MB, throughput 0.980947
Reading from 21013: heap size 454 MB, throughput 0.969386
Reading from 21013: heap size 458 MB, throughput 0.954921
Reading from 21012: heap size 469 MB, throughput 0.989382
Reading from 21012: heap size 469 MB, throughput 0.981148
Reading from 21013: heap size 460 MB, throughput 0.982199
Reading from 21012: heap size 469 MB, throughput 0.971465
Reading from 21012: heap size 472 MB, throughput 0.969
Reading from 21013: heap size 468 MB, throughput 0.98931
Reading from 21012: heap size 480 MB, throughput 0.98223
Reading from 21013: heap size 469 MB, throughput 0.991077
Reading from 21012: heap size 481 MB, throughput 0.98814
Numeric result:
Recommendation: 2 clients, utility 0.997488:
    h1: 1299.87 MB (U(h) = 0.388002*h^0.129831)
    h2: 1460.13 MB (U(h) = 0.350158*h^0.145851)
Recommendation: 2 clients, utility 0.997488:
    h1: 1299.81 MB (U(h) = 0.388002*h^0.129831)
    h2: 1460.19 MB (U(h) = 0.350158*h^0.145851)
Reading from 21013: heap size 470 MB, throughput 0.990046
Reading from 21012: heap size 482 MB, throughput 0.988757
Reading from 21013: heap size 473 MB, throughput 0.989382
Reading from 21012: heap size 484 MB, throughput 0.989262
Reading from 21013: heap size 471 MB, throughput 0.987761
Reading from 21012: heap size 482 MB, throughput 0.987547
Reading from 21013: heap size 474 MB, throughput 0.986916
Reading from 21012: heap size 485 MB, throughput 0.987425
Numeric result:
Recommendation: 2 clients, utility 1.02558:
    h1: 1297.45 MB (U(h) = 0.381359*h^0.133895)
    h2: 1462.55 MB (U(h) = 0.342752*h^0.150967)
Recommendation: 2 clients, utility 1.02558:
    h1: 1297.3 MB (U(h) = 0.381359*h^0.133895)
    h2: 1462.7 MB (U(h) = 0.342752*h^0.150967)
Reading from 21013: heap size 476 MB, throughput 0.987202
Reading from 21012: heap size 488 MB, throughput 0.986741
Reading from 21013: heap size 476 MB, throughput 0.989149
Reading from 21013: heap size 480 MB, throughput 0.982731
Reading from 21013: heap size 480 MB, throughput 0.970056
Reading from 21012: heap size 488 MB, throughput 0.989514
Reading from 21013: heap size 487 MB, throughput 0.982881
Reading from 21012: heap size 490 MB, throughput 0.983584
Reading from 21012: heap size 491 MB, throughput 0.971756
Reading from 21012: heap size 498 MB, throughput 0.984914
Reading from 21013: heap size 488 MB, throughput 0.98665
Reading from 21012: heap size 499 MB, throughput 0.989393
Reading from 21013: heap size 493 MB, throughput 0.989789
Numeric result:
Recommendation: 2 clients, utility 1.05927:
    h1: 1301.67 MB (U(h) = 0.372371*h^0.139473)
    h2: 1458.33 MB (U(h) = 0.335197*h^0.156247)
Recommendation: 2 clients, utility 1.05927:
    h1: 1301.72 MB (U(h) = 0.372371*h^0.139473)
    h2: 1458.28 MB (U(h) = 0.335197*h^0.156247)
Reading from 21013: heap size 495 MB, throughput 0.9896
Reading from 21012: heap size 504 MB, throughput 0.990468
Reading from 21013: heap size 493 MB, throughput 0.98856
Reading from 21012: heap size 505 MB, throughput 0.990446
Reading from 21013: heap size 496 MB, throughput 0.988133
Reading from 21012: heap size 505 MB, throughput 0.989026
Reading from 21013: heap size 498 MB, throughput 0.987852
Numeric result:
Recommendation: 2 clients, utility 1.07479:
    h1: 1299.52 MB (U(h) = 0.369054*h^0.141549)
    h2: 1460.48 MB (U(h) = 0.331165*h^0.159089)
Recommendation: 2 clients, utility 1.07479:
    h1: 1299.49 MB (U(h) = 0.369054*h^0.141549)
    h2: 1460.51 MB (U(h) = 0.331165*h^0.159089)
Reading from 21012: heap size 507 MB, throughput 0.988907
Reading from 21012: heap size 509 MB, throughput 0.987989
Reading from 21013: heap size 499 MB, throughput 0.991053
Reading from 21013: heap size 502 MB, throughput 0.986145
Reading from 21013: heap size 502 MB, throughput 0.974944
Reading from 21012: heap size 509 MB, throughput 0.989156
Reading from 21012: heap size 512 MB, throughput 0.979791
Reading from 21013: heap size 508 MB, throughput 0.984575
Reading from 21012: heap size 513 MB, throughput 0.975668
Reading from 21013: heap size 510 MB, throughput 0.989211
Reading from 21012: heap size 523 MB, throughput 0.988136
Numeric result:
Recommendation: 2 clients, utility 1.15545:
    h1: 1182.31 MB (U(h) = 0.362225*h^0.145861)
    h2: 1577.69 MB (U(h) = 0.271088*h^0.194643)
Recommendation: 2 clients, utility 1.15545:
    h1: 1182.29 MB (U(h) = 0.362225*h^0.145861)
    h2: 1577.71 MB (U(h) = 0.271088*h^0.194643)
Reading from 21013: heap size 513 MB, throughput 0.990453
Reading from 21012: heap size 523 MB, throughput 0.990982
Reading from 21013: heap size 515 MB, throughput 0.989947
Reading from 21012: heap size 524 MB, throughput 0.99164
Reading from 21013: heap size 514 MB, throughput 0.990523
Reading from 21012: heap size 527 MB, throughput 0.989402
Reading from 21013: heap size 517 MB, throughput 0.98966
Numeric result:
Recommendation: 2 clients, utility 1.56857:
    h1: 781.047 MB (U(h) = 0.359641*h^0.147499)
    h2: 1978.95 MB (U(h) = 0.0957081*h^0.373743)
Recommendation: 2 clients, utility 1.56857:
    h1: 781.016 MB (U(h) = 0.359641*h^0.147499)
    h2: 1978.98 MB (U(h) = 0.0957081*h^0.373743)
Reading from 21012: heap size 527 MB, throughput 0.989567
Reading from 21013: heap size 519 MB, throughput 0.9889
Reading from 21012: heap size 529 MB, throughput 0.9884
Reading from 21013: heap size 520 MB, throughput 0.986954
Reading from 21013: heap size 527 MB, throughput 0.978379
Reading from 21012: heap size 532 MB, throughput 0.988861
Reading from 21013: heap size 528 MB, throughput 0.981352
Reading from 21012: heap size 533 MB, throughput 0.982057
Reading from 21012: heap size 537 MB, throughput 0.983686
Reading from 21013: heap size 535 MB, throughput 0.98924
Reading from 21012: heap size 539 MB, throughput 0.990926
Numeric result:
Recommendation: 2 clients, utility 2.62856:
    h1: 1024.69 MB (U(h) = 0.117884*h^0.33963)
    h2: 1735.31 MB (U(h) = 0.0290113*h^0.575175)
Recommendation: 2 clients, utility 2.62856:
    h1: 1024.68 MB (U(h) = 0.117884*h^0.33963)
    h2: 1735.32 MB (U(h) = 0.0290113*h^0.575175)
Reading from 21013: heap size 536 MB, throughput 0.990749
Reading from 21012: heap size 543 MB, throughput 0.991923
Reading from 21013: heap size 537 MB, throughput 0.992012
Reading from 21012: heap size 544 MB, throughput 0.991141
Reading from 21013: heap size 539 MB, throughput 0.990944
Reading from 21012: heap size 543 MB, throughput 0.990782
Numeric result:
Recommendation: 2 clients, utility 3.63422:
    h1: 1009.82 MB (U(h) = 0.0715553*h^0.424313)
    h2: 1750.18 MB (U(h) = 0.011117*h^0.735424)
Recommendation: 2 clients, utility 3.63422:
    h1: 1009.8 MB (U(h) = 0.0715553*h^0.424313)
    h2: 1750.2 MB (U(h) = 0.011117*h^0.735424)
Reading from 21013: heap size 539 MB, throughput 0.990657
Reading from 21012: heap size 545 MB, throughput 0.989936
Reading from 21013: heap size 540 MB, throughput 0.992566
Reading from 21013: heap size 542 MB, throughput 0.987849
Reading from 21013: heap size 543 MB, throughput 0.97837
Reading from 21012: heap size 547 MB, throughput 0.991839
Reading from 21012: heap size 548 MB, throughput 0.987848
Reading from 21012: heap size 549 MB, throughput 0.981263
Reading from 21013: heap size 549 MB, throughput 0.988725
Reading from 21012: heap size 551 MB, throughput 0.987555
Numeric result:
Recommendation: 2 clients, utility 2.58577:
    h1: 1189.55 MB (U(h) = 0.0861531*h^0.393383)
    h2: 1570.45 MB (U(h) = 0.0405127*h^0.519372)
Recommendation: 2 clients, utility 2.58577:
    h1: 1189.52 MB (U(h) = 0.0861531*h^0.393383)
    h2: 1570.48 MB (U(h) = 0.0405127*h^0.519372)
Reading from 21013: heap size 551 MB, throughput 0.992218
Reading from 21012: heap size 555 MB, throughput 0.992067
Reading from 21013: heap size 553 MB, throughput 0.992432
Reading from 21012: heap size 557 MB, throughput 0.991608
Reading from 21013: heap size 556 MB, throughput 0.991794
Reading from 21012: heap size 556 MB, throughput 0.991264
Numeric result:
Recommendation: 2 clients, utility 2.33962:
    h1: 1282.79 MB (U(h) = 0.0881591*h^0.389265)
    h2: 1477.21 MB (U(h) = 0.062121*h^0.448262)
Recommendation: 2 clients, utility 2.33962:
    h1: 1282.79 MB (U(h) = 0.0881591*h^0.389265)
    h2: 1477.21 MB (U(h) = 0.062121*h^0.448262)
Reading from 21013: heap size 555 MB, throughput 0.989736
Reading from 21012: heap size 558 MB, throughput 0.990972
Reading from 21013: heap size 557 MB, throughput 0.990662
Reading from 21013: heap size 560 MB, throughput 0.987276
Reading from 21013: heap size 561 MB, throughput 0.982944
Reading from 21012: heap size 561 MB, throughput 0.99183
Reading from 21012: heap size 562 MB, throughput 0.986375
Reading from 21012: heap size 562 MB, throughput 0.98465
Reading from 21013: heap size 567 MB, throughput 0.990027
Numeric result:
Recommendation: 2 clients, utility 2.21773:
    h1: 1417.07 MB (U(h) = 0.0780327*h^0.4091)
    h2: 1342.93 MB (U(h) = 0.0894622*h^0.387704)
Recommendation: 2 clients, utility 2.21773:
    h1: 1417.06 MB (U(h) = 0.0780327*h^0.4091)
    h2: 1342.94 MB (U(h) = 0.0894622*h^0.387704)
Reading from 21012: heap size 564 MB, throughput 0.98963
Reading from 21013: heap size 568 MB, throughput 0.991135
Reading from 21012: heap size 567 MB, throughput 0.991665
Reading from 21013: heap size 570 MB, throughput 0.992056
Reading from 21012: heap size 569 MB, throughput 0.992881
Reading from 21013: heap size 571 MB, throughput 0.991212
Numeric result:
Recommendation: 2 clients, utility 2.26774:
    h1: 1469.48 MB (U(h) = 0.0661436*h^0.43592)
    h2: 1290.52 MB (U(h) = 0.0919563*h^0.38283)
Recommendation: 2 clients, utility 2.26774:
    h1: 1469.48 MB (U(h) = 0.0661436*h^0.43592)
    h2: 1290.52 MB (U(h) = 0.0919563*h^0.38283)
Reading from 21012: heap size 569 MB, throughput 0.9914
Reading from 21013: heap size 572 MB, throughput 0.990861
Reading from 21013: heap size 573 MB, throughput 0.991547
Reading from 21012: heap size 571 MB, throughput 0.990748
Reading from 21013: heap size 574 MB, throughput 0.987405
Reading from 21013: heap size 576 MB, throughput 0.989945
Reading from 21012: heap size 574 MB, throughput 0.990219
Reading from 21012: heap size 575 MB, throughput 0.985534
Reading from 21012: heap size 580 MB, throughput 0.99075
Numeric result:
Recommendation: 2 clients, utility 2.00058:
    h1: 1437.65 MB (U(h) = 0.0982778*h^0.37056)
    h2: 1322.35 MB (U(h) = 0.118758*h^0.340858)
Recommendation: 2 clients, utility 2.00058:
    h1: 1437.62 MB (U(h) = 0.0982778*h^0.37056)
    h2: 1322.38 MB (U(h) = 0.118758*h^0.340858)
Reading from 21013: heap size 583 MB, throughput 0.992478
Reading from 21012: heap size 581 MB, throughput 0.993376
Reading from 21013: heap size 584 MB, throughput 0.992731
Reading from 21012: heap size 583 MB, throughput 0.992691
Reading from 21013: heap size 584 MB, throughput 0.992507
Numeric result:
Recommendation: 2 clients, utility 1.96349:
    h1: 1599.06 MB (U(h) = 0.0828218*h^0.398191)
    h2: 1160.94 MB (U(h) = 0.163356*h^0.289088)
Recommendation: 2 clients, utility 1.96349:
    h1: 1599.07 MB (U(h) = 0.0828218*h^0.398191)
    h2: 1160.93 MB (U(h) = 0.163356*h^0.289088)
Reading from 21012: heap size 585 MB, throughput 0.992493
Reading from 21013: heap size 586 MB, throughput 0.991579
Reading from 21012: heap size 584 MB, throughput 0.991421
Reading from 21013: heap size 588 MB, throughput 0.99303
Reading from 21013: heap size 589 MB, throughput 0.988301
Reading from 21012: heap size 586 MB, throughput 0.990826
Reading from 21013: heap size 589 MB, throughput 0.988925
Reading from 21012: heap size 589 MB, throughput 0.985772
Reading from 21012: heap size 590 MB, throughput 0.987791
Numeric result:
Recommendation: 2 clients, utility 1.72465:
    h1: 1894.79 MB (U(h) = 0.0979756*h^0.370256)
    h2: 865.214 MB (U(h) = 0.343139*h^0.169069)
Recommendation: 2 clients, utility 1.72465:
    h1: 1894.79 MB (U(h) = 0.0979756*h^0.370256)
    h2: 865.214 MB (U(h) = 0.343139*h^0.169069)
Reading from 21013: heap size 592 MB, throughput 0.992021
Reading from 21012: heap size 598 MB, throughput 0.992441
Reading from 21013: heap size 594 MB, throughput 0.992679
Reading from 21012: heap size 598 MB, throughput 0.992801
Reading from 21013: heap size 596 MB, throughput 0.992375
Numeric result:
Recommendation: 2 clients, utility 1.66743:
    h1: 2064.96 MB (U(h) = 0.103264*h^0.361657)
    h2: 695.043 MB (U(h) = 0.460559*h^0.121728)
Recommendation: 2 clients, utility 1.66743:
    h1: 2064.96 MB (U(h) = 0.103264*h^0.361657)
    h2: 695.036 MB (U(h) = 0.460559*h^0.121728)
Reading from 21012: heap size 598 MB, throughput 0.992102
Reading from 21013: heap size 597 MB, throughput 0.991698
Reading from 21012: heap size 600 MB, throughput 0.991194
Reading from 21013: heap size 598 MB, throughput 0.993105
Reading from 21013: heap size 599 MB, throughput 0.987711
Reading from 21012: heap size 603 MB, throughput 0.98997
Reading from 21013: heap size 601 MB, throughput 0.987908
Reading from 21012: heap size 603 MB, throughput 0.985422
Numeric result:
Recommendation: 2 clients, utility 1.55621:
    h1: 2296.41 MB (U(h) = 0.133099*h^0.320548)
    h2: 463.595 MB (U(h) = 0.65764*h^0.0647116)
Recommendation: 2 clients, utility 1.55621:
    h1: 2296.41 MB (U(h) = 0.133099*h^0.320548)
    h2: 463.595 MB (U(h) = 0.65764*h^0.0647116)
Reading from 21012: heap size 608 MB, throughput 0.98956
Reading from 21013: heap size 609 MB, throughput 0.992336
Reading from 21012: heap size 609 MB, throughput 0.992333
Reading from 21013: heap size 576 MB, throughput 0.993362
Reading from 21012: heap size 612 MB, throughput 0.99273
Reading from 21013: heap size 573 MB, throughput 0.992582
Numeric result:
Recommendation: 2 clients, utility 1.40918:
    h1: 2184.29 MB (U(h) = 0.199457*h^0.255262)
    h2: 575.707 MB (U(h) = 0.647147*h^0.0672821)
Recommendation: 2 clients, utility 1.40918:
    h1: 2184.27 MB (U(h) = 0.199457*h^0.255262)
    h2: 575.73 MB (U(h) = 0.647147*h^0.0672821)
Reading from 21012: heap size 614 MB, throughput 0.992494
Reading from 21013: heap size 564 MB, throughput 0.992993
Reading from 21013: heap size 573 MB, throughput 0.992378
Reading from 21013: heap size 575 MB, throughput 0.986814
Reading from 21012: heap size 614 MB, throughput 0.993554
Reading from 21012: heap size 615 MB, throughput 0.990449
Reading from 21013: heap size 579 MB, throughput 0.986804
Reading from 21012: heap size 617 MB, throughput 0.988806
Numeric result:
Recommendation: 2 clients, utility 1.28187:
    h1: 1998.01 MB (U(h) = 0.302723*h^0.188216)
    h2: 761.988 MB (U(h) = 0.629181*h^0.0717566)
Recommendation: 2 clients, utility 1.28187:
    h1: 1998.2 MB (U(h) = 0.302723*h^0.188216)
    h2: 761.804 MB (U(h) = 0.629181*h^0.0717566)
Reading from 21013: heap size 544 MB, throughput 0.99082
Reading from 21012: heap size 619 MB, throughput 0.992811
Reading from 21013: heap size 574 MB, throughput 0.991301
Reading from 21012: heap size 621 MB, throughput 0.993348
Reading from 21013: heap size 573 MB, throughput 0.990324
Numeric result:
Recommendation: 2 clients, utility 1.23851:
    h1: 1877.52 MB (U(h) = 0.359206*h^0.160834)
    h2: 882.483 MB (U(h) = 0.614262*h^0.0756015)
Recommendation: 2 clients, utility 1.23851:
    h1: 1877.48 MB (U(h) = 0.359206*h^0.160834)
    h2: 882.525 MB (U(h) = 0.614262*h^0.0756015)
Reading from 21012: heap size 622 MB, throughput 0.993181
Reading from 21013: heap size 574 MB, throughput 0.990729
Reading from 21012: heap size 622 MB, throughput 0.992473
Reading from 21013: heap size 575 MB, throughput 0.993021
Reading from 21013: heap size 578 MB, throughput 0.989059
Client 21013 died
Clients: 1
Reading from 21012: heap size 623 MB, throughput 0.992772
Reading from 21012: heap size 624 MB, throughput 0.989283
Client 21012 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
