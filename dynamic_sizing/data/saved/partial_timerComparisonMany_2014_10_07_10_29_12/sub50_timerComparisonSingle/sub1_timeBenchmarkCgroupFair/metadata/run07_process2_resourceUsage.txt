	Command being timed: "cgexec -g memory:economem /home/callum/economics_memory_code/dynamic_sizing/hotspot8/openjdk8/jdk8/build/linux-x86_64-normal-server-release/jdk/bin/java -XX:-EconomemVengerovThroughput -Xms10m -Xmx52m -XX:+DisableExplicitGC -XX:+UseAdaptiveSizePolicyWithSystemGC -jar /home/callum/economics_memory_code/dynamic_sizing/haskell_common/dacapo-9.12-bach.jar --no-pre-iteration-gc --scratch-directory /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub50_timerComparisonSingle/sub1_timeBenchmarkCgroupFair/scratch2 -t 1 -n 90 -s large pmd"
	User time (seconds): 473.28
	System time (seconds): 42.85
	Percent of CPU this job got: 62%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 13:43.17
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 169944
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 53078
	Minor (reclaiming a frame) page faults: 642193
	Voluntary context switches: 1786694
	Involuntary context switches: 173536
	Swaps: 0
	File system inputs: 2462864
	File system outputs: 60648
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
