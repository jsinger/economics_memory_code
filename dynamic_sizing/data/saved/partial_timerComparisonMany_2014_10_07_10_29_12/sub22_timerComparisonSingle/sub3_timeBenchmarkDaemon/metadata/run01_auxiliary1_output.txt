economemd
    total memory: 2208 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub22_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 27257: heap size 9 MB, throughput 0.990515
Clients: 1
Client 27257 has a minimum heap size of 276 MB
Reading from 27256: heap size 9 MB, throughput 0.989546
Clients: 2
Client 27256 has a minimum heap size of 276 MB
Reading from 27257: heap size 9 MB, throughput 0.986882
Reading from 27256: heap size 9 MB, throughput 0.98709
Reading from 27257: heap size 9 MB, throughput 0.981701
Reading from 27256: heap size 9 MB, throughput 0.980017
Reading from 27257: heap size 9 MB, throughput 0.972619
Reading from 27256: heap size 9 MB, throughput 0.970124
Reading from 27257: heap size 11 MB, throughput 0.977796
Reading from 27256: heap size 11 MB, throughput 0.958841
Reading from 27256: heap size 11 MB, throughput 0.972778
Reading from 27257: heap size 11 MB, throughput 0.983893
Reading from 27257: heap size 17 MB, throughput 0.929825
Reading from 27256: heap size 17 MB, throughput 0.969873
Reading from 27256: heap size 17 MB, throughput 0.898354
Reading from 27257: heap size 17 MB, throughput 0.85531
Reading from 27256: heap size 30 MB, throughput 0.797763
Reading from 27256: heap size 31 MB, throughput 0.503618
Reading from 27257: heap size 30 MB, throughput 0.748069
Reading from 27257: heap size 31 MB, throughput 0.176658
Reading from 27256: heap size 36 MB, throughput 0.682596
Reading from 27257: heap size 34 MB, throughput 0.677184
Reading from 27256: heap size 47 MB, throughput 0.510988
Reading from 27257: heap size 44 MB, throughput 0.200925
Reading from 27257: heap size 49 MB, throughput 0.0454455
Reading from 27256: heap size 49 MB, throughput 0.443934
Reading from 27257: heap size 50 MB, throughput 0.520226
Reading from 27257: heap size 68 MB, throughput 0.797017
Reading from 27256: heap size 52 MB, throughput 0.560108
Reading from 27256: heap size 76 MB, throughput 0.491735
Reading from 27257: heap size 70 MB, throughput 0.30674
Reading from 27256: heap size 76 MB, throughput 0.483532
Reading from 27257: heap size 96 MB, throughput 0.20073
Reading from 27256: heap size 101 MB, throughput 0.694354
Reading from 27257: heap size 97 MB, throughput 0.771617
Reading from 27256: heap size 102 MB, throughput 0.599404
Reading from 27256: heap size 104 MB, throughput 0.375777
Reading from 27256: heap size 107 MB, throughput 0.320465
Reading from 27257: heap size 99 MB, throughput 0.495875
Reading from 27257: heap size 127 MB, throughput 0.723563
Reading from 27257: heap size 131 MB, throughput 0.768852
Reading from 27257: heap size 133 MB, throughput 0.736337
Reading from 27257: heap size 135 MB, throughput 0.701542
Reading from 27256: heap size 111 MB, throughput 0.443474
Reading from 27257: heap size 141 MB, throughput 0.712029
Reading from 27257: heap size 144 MB, throughput 0.675408
Reading from 27256: heap size 138 MB, throughput 0.297866
Reading from 27257: heap size 149 MB, throughput 0.640177
Reading from 27256: heap size 161 MB, throughput 0.544035
Reading from 27257: heap size 152 MB, throughput 0.637071
Reading from 27256: heap size 165 MB, throughput 0.634001
Reading from 27256: heap size 167 MB, throughput 0.486122
Reading from 27256: heap size 169 MB, throughput 0.256514
Reading from 27257: heap size 160 MB, throughput 0.486065
Reading from 27256: heap size 177 MB, throughput 0.0256061
Reading from 27257: heap size 192 MB, throughput 0.551395
Reading from 27257: heap size 199 MB, throughput 0.532026
Reading from 27256: heap size 179 MB, throughput 0.471917
Reading from 27257: heap size 204 MB, throughput 0.279895
Reading from 27256: heap size 222 MB, throughput 0.860821
Reading from 27257: heap size 236 MB, throughput 0.84276
Reading from 27256: heap size 224 MB, throughput 0.896866
Reading from 27256: heap size 228 MB, throughput 0.865957
Reading from 27256: heap size 234 MB, throughput 0.814285
Reading from 27257: heap size 240 MB, throughput 0.862684
Reading from 27256: heap size 239 MB, throughput 0.739191
Reading from 27257: heap size 240 MB, throughput 0.825743
Reading from 27257: heap size 243 MB, throughput 0.722537
Reading from 27256: heap size 244 MB, throughput 0.741875
Reading from 27257: heap size 245 MB, throughput 0.72591
Reading from 27257: heap size 248 MB, throughput 0.621664
Reading from 27256: heap size 251 MB, throughput 0.768126
Reading from 27257: heap size 249 MB, throughput 0.663541
Reading from 27256: heap size 254 MB, throughput 0.799252
Reading from 27257: heap size 251 MB, throughput 0.703915
Reading from 27256: heap size 258 MB, throughput 0.762645
Reading from 27257: heap size 253 MB, throughput 0.706852
Reading from 27256: heap size 261 MB, throughput 0.72801
Reading from 27257: heap size 253 MB, throughput 0.778759
Reading from 27256: heap size 267 MB, throughput 0.726711
Reading from 27257: heap size 255 MB, throughput 0.741673
Reading from 27256: heap size 271 MB, throughput 0.676939
Reading from 27257: heap size 259 MB, throughput 0.716678
Reading from 27256: heap size 277 MB, throughput 0.678873
Reading from 27256: heap size 279 MB, throughput 0.684926
Reading from 27257: heap size 260 MB, throughput 0.610745
Reading from 27256: heap size 275 MB, throughput 0.88683
Reading from 27257: heap size 295 MB, throughput 0.586227
Reading from 27257: heap size 297 MB, throughput 0.824157
Reading from 27257: heap size 298 MB, throughput 0.845346
Reading from 27256: heap size 279 MB, throughput 0.700146
Reading from 27257: heap size 300 MB, throughput 0.836076
Reading from 27256: heap size 321 MB, throughput 0.733651
Reading from 27257: heap size 302 MB, throughput 0.815218
Reading from 27256: heap size 323 MB, throughput 0.722941
Reading from 27257: heap size 303 MB, throughput 0.791039
Reading from 27256: heap size 325 MB, throughput 0.753233
Reading from 27257: heap size 305 MB, throughput 0.8187
Reading from 27256: heap size 326 MB, throughput 0.856114
Reading from 27257: heap size 306 MB, throughput 0.7858
Reading from 27257: heap size 311 MB, throughput 0.793817
Reading from 27256: heap size 328 MB, throughput 0.863245
Reading from 27256: heap size 329 MB, throughput 0.820047
Reading from 27257: heap size 312 MB, throughput 0.707934
Reading from 27257: heap size 317 MB, throughput 0.67638
Reading from 27256: heap size 327 MB, throughput 0.725047
Reading from 27256: heap size 331 MB, throughput 0.67312
Reading from 27256: heap size 332 MB, throughput 0.59439
Reading from 27256: heap size 334 MB, throughput 0.613112
Reading from 27256: heap size 340 MB, throughput 0.595266
Numeric result:
Recommendation: 2 clients, utility 0.519593:
    h1: 1932 MB (U(h) = 0.310313*h^0.134199)
    h2: 276 MB (U(h) = 0.603176*h^0.001)
Recommendation: 2 clients, utility 0.519593:
    h1: 1932 MB (U(h) = 0.310313*h^0.134199)
    h2: 276 MB (U(h) = 0.603176*h^0.001)
Reading from 27257: heap size 318 MB, throughput 0.597338
Reading from 27257: heap size 363 MB, throughput 0.670679
Reading from 27257: heap size 365 MB, throughput 0.866553
Reading from 27256: heap size 341 MB, throughput 0.889611
Reading from 27257: heap size 372 MB, throughput 0.966928
Reading from 27256: heap size 327 MB, throughput 0.955321
Reading from 27257: heap size 374 MB, throughput 0.978071
Reading from 27256: heap size 301 MB, throughput 0.96656
Reading from 27256: heap size 334 MB, throughput 0.968403
Reading from 27257: heap size 381 MB, throughput 0.984291
Reading from 27256: heap size 294 MB, throughput 0.969737
Reading from 27257: heap size 382 MB, throughput 0.981235
Reading from 27256: heap size 328 MB, throughput 0.971447
Reading from 27257: heap size 381 MB, throughput 0.983072
Reading from 27256: heap size 296 MB, throughput 0.974171
Reading from 27257: heap size 384 MB, throughput 0.980156
Reading from 27256: heap size 322 MB, throughput 0.975713
Numeric result:
Recommendation: 2 clients, utility 0.631066:
    h1: 1769.26 MB (U(h) = 0.268965*h^0.169067)
    h2: 438.74 MB (U(h) = 0.513452*h^0.041929)
Recommendation: 2 clients, utility 0.631066:
    h1: 1769.23 MB (U(h) = 0.268965*h^0.169067)
    h2: 438.773 MB (U(h) = 0.513452*h^0.041929)
Reading from 27257: heap size 379 MB, throughput 0.975101
Reading from 27256: heap size 298 MB, throughput 0.971984
Reading from 27257: heap size 382 MB, throughput 0.973822
Reading from 27256: heap size 322 MB, throughput 0.965957
Reading from 27256: heap size 321 MB, throughput 0.963176
Reading from 27257: heap size 377 MB, throughput 0.973933
Reading from 27256: heap size 321 MB, throughput 0.967217
Reading from 27257: heap size 380 MB, throughput 0.973393
Reading from 27256: heap size 322 MB, throughput 0.96919
Reading from 27257: heap size 381 MB, throughput 0.978885
Reading from 27256: heap size 322 MB, throughput 0.974685
Reading from 27256: heap size 323 MB, throughput 0.973828
Reading from 27257: heap size 382 MB, throughput 0.978925
Reading from 27256: heap size 325 MB, throughput 0.974129
Reading from 27257: heap size 384 MB, throughput 0.978241
Reading from 27256: heap size 325 MB, throughput 0.972959
Numeric result:
Recommendation: 2 clients, utility 0.684765:
    h1: 1689.45 MB (U(h) = 0.253698*h^0.183342)
    h2: 518.553 MB (U(h) = 0.486063*h^0.0562665)
Recommendation: 2 clients, utility 0.684765:
    h1: 1689.5 MB (U(h) = 0.253698*h^0.183342)
    h2: 518.499 MB (U(h) = 0.486063*h^0.0562665)
Reading from 27256: heap size 328 MB, throughput 0.966696
Reading from 27257: heap size 385 MB, throughput 0.979403
Reading from 27257: heap size 389 MB, throughput 0.968239
Reading from 27257: heap size 389 MB, throughput 0.952747
Reading from 27257: heap size 385 MB, throughput 0.922467
Reading from 27257: heap size 390 MB, throughput 0.895572
Reading from 27257: heap size 397 MB, throughput 0.895133
Reading from 27256: heap size 328 MB, throughput 0.936993
Reading from 27256: heap size 370 MB, throughput 0.973941
Reading from 27256: heap size 371 MB, throughput 0.968086
Reading from 27256: heap size 375 MB, throughput 0.958484
Reading from 27256: heap size 375 MB, throughput 0.945959
Reading from 27256: heap size 377 MB, throughput 0.934496
Reading from 27257: heap size 400 MB, throughput 0.972705
Reading from 27256: heap size 378 MB, throughput 0.974254
Reading from 27256: heap size 379 MB, throughput 0.992173
Reading from 27257: heap size 400 MB, throughput 0.982308
Reading from 27256: heap size 381 MB, throughput 0.991617
Reading from 27257: heap size 403 MB, throughput 0.981202
Reading from 27256: heap size 383 MB, throughput 0.991161
Reading from 27257: heap size 401 MB, throughput 0.977195
Reading from 27256: heap size 384 MB, throughput 0.990854
Numeric result:
Recommendation: 2 clients, utility 0.822942:
    h1: 1468.69 MB (U(h) = 0.231146*h^0.205814)
    h2: 739.309 MB (U(h) = 0.400384*h^0.103599)
Recommendation: 2 clients, utility 0.822942:
    h1: 1468.71 MB (U(h) = 0.231146*h^0.205814)
    h2: 739.291 MB (U(h) = 0.400384*h^0.103599)
Reading from 27257: heap size 404 MB, throughput 0.980401
Reading from 27256: heap size 382 MB, throughput 0.989813
Reading from 27256: heap size 351 MB, throughput 0.9878
Reading from 27257: heap size 401 MB, throughput 0.982339
Reading from 27256: heap size 378 MB, throughput 0.985456
Reading from 27257: heap size 403 MB, throughput 0.984093
Reading from 27256: heap size 381 MB, throughput 0.986304
Reading from 27257: heap size 400 MB, throughput 0.983202
Reading from 27256: heap size 380 MB, throughput 0.984724
Reading from 27257: heap size 402 MB, throughput 0.980526
Reading from 27256: heap size 380 MB, throughput 0.985613
Reading from 27257: heap size 403 MB, throughput 0.98346
Reading from 27256: heap size 381 MB, throughput 0.98471
Numeric result:
Recommendation: 2 clients, utility 0.863328:
    h1: 1431.17 MB (U(h) = 0.225416*h^0.211837)
    h2: 776.832 MB (U(h) = 0.382258*h^0.114984)
Recommendation: 2 clients, utility 0.863328:
    h1: 1431.17 MB (U(h) = 0.225416*h^0.211837)
    h2: 776.833 MB (U(h) = 0.382258*h^0.114984)
Reading from 27257: heap size 403 MB, throughput 0.983389
Reading from 27256: heap size 382 MB, throughput 0.982097
Reading from 27256: heap size 384 MB, throughput 0.976281
Reading from 27257: heap size 404 MB, throughput 0.985359
Reading from 27257: heap size 404 MB, throughput 0.974792
Reading from 27257: heap size 404 MB, throughput 0.960612
Reading from 27257: heap size 406 MB, throughput 0.937242
Reading from 27257: heap size 412 MB, throughput 0.969736
Reading from 27256: heap size 386 MB, throughput 0.983695
Reading from 27256: heap size 386 MB, throughput 0.980148
Reading from 27256: heap size 389 MB, throughput 0.96417
Reading from 27256: heap size 383 MB, throughput 0.939464
Reading from 27256: heap size 390 MB, throughput 0.897252
Reading from 27256: heap size 397 MB, throughput 0.870734
Reading from 27257: heap size 413 MB, throughput 0.983867
Reading from 27256: heap size 401 MB, throughput 0.945435
Reading from 27256: heap size 405 MB, throughput 0.976686
Reading from 27257: heap size 417 MB, throughput 0.986804
Reading from 27256: heap size 408 MB, throughput 0.981452
Reading from 27257: heap size 418 MB, throughput 0.987202
Numeric result:
Recommendation: 2 clients, utility 0.936345:
    h1: 1384.65 MB (U(h) = 0.214503*h^0.223649)
    h2: 823.354 MB (U(h) = 0.354575*h^0.132988)
Recommendation: 2 clients, utility 0.936345:
    h1: 1384.65 MB (U(h) = 0.214503*h^0.223649)
    h2: 823.354 MB (U(h) = 0.354575*h^0.132988)
Reading from 27256: heap size 407 MB, throughput 0.983485
Reading from 27257: heap size 419 MB, throughput 0.987367
Reading from 27256: heap size 410 MB, throughput 0.984262
Reading from 27257: heap size 420 MB, throughput 0.986069
Reading from 27256: heap size 407 MB, throughput 0.982857
Reading from 27256: heap size 410 MB, throughput 0.982078
Reading from 27257: heap size 419 MB, throughput 0.9853
Reading from 27256: heap size 407 MB, throughput 0.983033
Reading from 27257: heap size 420 MB, throughput 0.986893
Reading from 27256: heap size 410 MB, throughput 0.983601
Reading from 27257: heap size 420 MB, throughput 0.984895
Numeric result:
Recommendation: 2 clients, utility 0.961477:
    h1: 1374.24 MB (U(h) = 0.210589*h^0.228011)
    h2: 833.758 MB (U(h) = 0.346664*h^0.138335)
Recommendation: 2 clients, utility 0.961477:
    h1: 1374.24 MB (U(h) = 0.210589*h^0.228011)
    h2: 833.756 MB (U(h) = 0.346664*h^0.138335)
Reading from 27256: heap size 409 MB, throughput 0.983263
Reading from 27257: heap size 421 MB, throughput 0.982211
Reading from 27256: heap size 410 MB, throughput 0.982371
Reading from 27257: heap size 423 MB, throughput 0.984889
Reading from 27257: heap size 424 MB, throughput 0.974766
Reading from 27257: heap size 424 MB, throughput 0.936548
Reading from 27256: heap size 411 MB, throughput 0.976577
Reading from 27257: heap size 426 MB, throughput 0.932501
Reading from 27257: heap size 436 MB, throughput 0.980897
Reading from 27256: heap size 411 MB, throughput 0.98417
Reading from 27256: heap size 414 MB, throughput 0.979612
Reading from 27256: heap size 414 MB, throughput 0.965484
Reading from 27256: heap size 417 MB, throughput 0.943863
Reading from 27256: heap size 420 MB, throughput 0.932281
Reading from 27257: heap size 436 MB, throughput 0.985025
Reading from 27256: heap size 429 MB, throughput 0.977849
Reading from 27257: heap size 439 MB, throughput 0.986384
Reading from 27256: heap size 429 MB, throughput 0.985093
Numeric result:
Recommendation: 2 clients, utility 1.02145:
    h1: 1351.19 MB (U(h) = 0.201925*h^0.237898)
    h2: 856.806 MB (U(h) = 0.328706*h^0.150851)
Recommendation: 2 clients, utility 1.02145:
    h1: 1351.2 MB (U(h) = 0.201925*h^0.237898)
    h2: 856.798 MB (U(h) = 0.328706*h^0.150851)
Reading from 27257: heap size 441 MB, throughput 0.986584
Reading from 27256: heap size 432 MB, throughput 0.987962
Reading from 27257: heap size 441 MB, throughput 0.986722
Reading from 27256: heap size 434 MB, throughput 0.988356
Reading from 27257: heap size 443 MB, throughput 0.98635
Reading from 27256: heap size 434 MB, throughput 0.987955
Reading from 27256: heap size 436 MB, throughput 0.987546
Reading from 27257: heap size 443 MB, throughput 0.986909
Numeric result:
Recommendation: 2 clients, utility 1.04615:
    h1: 1337.2 MB (U(h) = 0.199256*h^0.240996)
    h2: 870.798 MB (U(h) = 0.320182*h^0.156938)
Recommendation: 2 clients, utility 1.04615:
    h1: 1337.2 MB (U(h) = 0.199256*h^0.240996)
    h2: 870.797 MB (U(h) = 0.320182*h^0.156938)
Reading from 27256: heap size 435 MB, throughput 0.987062
Reading from 27257: heap size 444 MB, throughput 0.985336
Reading from 27256: heap size 437 MB, throughput 0.985511
Reading from 27257: heap size 446 MB, throughput 0.987902
Reading from 27257: heap size 447 MB, throughput 0.98049
Reading from 27257: heap size 447 MB, throughput 0.968902
Reading from 27257: heap size 450 MB, throughput 0.965412
Reading from 27256: heap size 439 MB, throughput 0.983748
Reading from 27257: heap size 457 MB, throughput 0.98669
Reading from 27256: heap size 439 MB, throughput 0.981101
Reading from 27256: heap size 437 MB, throughput 0.980405
Reading from 27256: heap size 442 MB, throughput 0.966701
Reading from 27256: heap size 438 MB, throughput 0.94511
Reading from 27256: heap size 445 MB, throughput 0.899403
Reading from 27257: heap size 458 MB, throughput 0.988416
Numeric result:
Recommendation: 2 clients, utility 1.0917:
    h1: 1325.27 MB (U(h) = 0.192877*h^0.248523)
    h2: 882.726 MB (U(h) = 0.308478*h^0.165533)
Recommendation: 2 clients, utility 1.0917:
    h1: 1325.28 MB (U(h) = 0.192877*h^0.248523)
    h2: 882.724 MB (U(h) = 0.308478*h^0.165533)
Reading from 27256: heap size 457 MB, throughput 0.97597
Reading from 27257: heap size 458 MB, throughput 0.989559
Reading from 27256: heap size 457 MB, throughput 0.981165
Reading from 27257: heap size 460 MB, throughput 0.98899
Reading from 27256: heap size 458 MB, throughput 0.983923
Reading from 27257: heap size 458 MB, throughput 0.988064
Reading from 27256: heap size 461 MB, throughput 0.985031
Reading from 27257: heap size 460 MB, throughput 0.9858
Reading from 27256: heap size 463 MB, throughput 0.978102
Numeric result:
Recommendation: 2 clients, utility 1.1102:
    h1: 1309.71 MB (U(h) = 0.191923*h^0.249662)
    h2: 898.291 MB (U(h) = 0.300824*h^0.171233)
Recommendation: 2 clients, utility 1.1102:
    h1: 1309.72 MB (U(h) = 0.191923*h^0.249662)
    h2: 898.281 MB (U(h) = 0.300824*h^0.171233)
Reading from 27257: heap size 461 MB, throughput 0.986154
Reading from 27256: heap size 479 MB, throughput 0.989707
Reading from 27257: heap size 462 MB, throughput 0.987772
Reading from 27257: heap size 463 MB, throughput 0.981655
Reading from 27257: heap size 464 MB, throughput 0.965724
Reading from 27256: heap size 479 MB, throughput 0.991002
Reading from 27257: heap size 469 MB, throughput 0.977412
Reading from 27256: heap size 483 MB, throughput 0.991396
Reading from 27257: heap size 471 MB, throughput 0.985918
Reading from 27256: heap size 489 MB, throughput 0.991589
Reading from 27256: heap size 490 MB, throughput 0.984153
Reading from 27256: heap size 489 MB, throughput 0.973549
Reading from 27257: heap size 474 MB, throughput 0.98868
Reading from 27256: heap size 491 MB, throughput 0.95947
Numeric result:
Recommendation: 2 clients, utility 1.14653:
    h1: 1304.62 MB (U(h) = 0.186584*h^0.256111)
    h2: 903.377 MB (U(h) = 0.292675*h^0.177347)
Recommendation: 2 clients, utility 1.14653:
    h1: 1304.61 MB (U(h) = 0.186584*h^0.256111)
    h2: 903.392 MB (U(h) = 0.292675*h^0.177347)
Reading from 27256: heap size 503 MB, throughput 0.985547
Reading from 27257: heap size 476 MB, throughput 0.988146
Reading from 27256: heap size 502 MB, throughput 0.990609
Reading from 27257: heap size 474 MB, throughput 0.990563
Reading from 27256: heap size 502 MB, throughput 0.990933
Reading from 27257: heap size 476 MB, throughput 0.990193
Numeric result:
Recommendation: 2 clients, utility 1.22022:
    h1: 1172.03 MB (U(h) = 0.185882*h^0.256966)
    h2: 1035.97 MB (U(h) = 0.220651*h^0.227132)
Recommendation: 2 clients, utility 1.22022:
    h1: 1172.04 MB (U(h) = 0.185882*h^0.256966)
    h2: 1035.96 MB (U(h) = 0.220651*h^0.227132)
Reading from 27256: heap size 506 MB, throughput 0.989746
Reading from 27257: heap size 478 MB, throughput 0.98826
Reading from 27256: heap size 505 MB, throughput 0.9887
Reading from 27257: heap size 478 MB, throughput 0.989793
Reading from 27257: heap size 481 MB, throughput 0.984324
Reading from 27257: heap size 481 MB, throughput 0.975129
Reading from 27256: heap size 507 MB, throughput 0.987547
Reading from 27257: heap size 486 MB, throughput 0.981422
Reading from 27256: heap size 511 MB, throughput 0.98647
Reading from 27257: heap size 488 MB, throughput 0.987762
Numeric result:
Recommendation: 2 clients, utility 1.89878:
    h1: 1040.76 MB (U(h) = 0.0851899*h^0.393688)
    h2: 1167.24 MB (U(h) = 0.0639663*h^0.44153)
Recommendation: 2 clients, utility 1.89878:
    h1: 1040.76 MB (U(h) = 0.0851899*h^0.393688)
    h2: 1167.24 MB (U(h) = 0.0639663*h^0.44153)
Reading from 27256: heap size 512 MB, throughput 0.988113
Reading from 27256: heap size 515 MB, throughput 0.981088
Reading from 27256: heap size 517 MB, throughput 0.969801
Reading from 27257: heap size 491 MB, throughput 0.990376
Reading from 27256: heap size 526 MB, throughput 0.98683
Reading from 27257: heap size 492 MB, throughput 0.9891
Reading from 27256: heap size 527 MB, throughput 0.98968
Reading from 27257: heap size 489 MB, throughput 0.989643
Reading from 27256: heap size 532 MB, throughput 0.990628
Reading from 27257: heap size 491 MB, throughput 0.988925
Numeric result:
Recommendation: 2 clients, utility 3.02387:
    h1: 905.443 MB (U(h) = 0.0449999*h^0.503923)
    h2: 1302.56 MB (U(h) = 0.0120034*h^0.724932)
Recommendation: 2 clients, utility 3.02387:
    h1: 905.446 MB (U(h) = 0.0449999*h^0.503923)
    h2: 1302.55 MB (U(h) = 0.0120034*h^0.724932)
Reading from 27256: heap size 534 MB, throughput 0.989568
Reading from 27257: heap size 493 MB, throughput 0.987432
Reading from 27257: heap size 494 MB, throughput 0.986624
Reading from 27256: heap size 532 MB, throughput 0.989868
Reading from 27257: heap size 495 MB, throughput 0.981442
Reading from 27257: heap size 497 MB, throughput 0.977467
Reading from 27256: heap size 535 MB, throughput 0.988242
Reading from 27257: heap size 504 MB, throughput 0.989472
Numeric result:
Recommendation: 2 clients, utility 3.14683:
    h1: 845.431 MB (U(h) = 0.0509615*h^0.482378)
    h2: 1362.57 MB (U(h) = 0.0087478*h^0.77744)
Recommendation: 2 clients, utility 3.14683:
    h1: 845.432 MB (U(h) = 0.0509615*h^0.482378)
    h2: 1362.57 MB (U(h) = 0.0087478*h^0.77744)
Reading from 27256: heap size 538 MB, throughput 0.990801
Reading from 27257: heap size 504 MB, throughput 0.990726
Reading from 27256: heap size 539 MB, throughput 0.984354
Reading from 27256: heap size 539 MB, throughput 0.975951
Reading from 27256: heap size 541 MB, throughput 0.985947
Reading from 27257: heap size 503 MB, throughput 0.990525
Reading from 27257: heap size 505 MB, throughput 0.98979
Reading from 27256: heap size 550 MB, throughput 0.991494
Reading from 27257: heap size 503 MB, throughput 0.989335
Reading from 27256: heap size 551 MB, throughput 0.991534
Numeric result:
Recommendation: 2 clients, utility 2.93925:
    h1: 737.505 MB (U(h) = 0.0861622*h^0.393821)
    h2: 1470.5 MB (U(h) = 0.00824748*h^0.785238)
Recommendation: 2 clients, utility 2.93925:
    h1: 737.5 MB (U(h) = 0.0861622*h^0.393821)
    h2: 1470.5 MB (U(h) = 0.00824748*h^0.785238)
Reading from 27257: heap size 505 MB, throughput 0.98936
Reading from 27256: heap size 552 MB, throughput 0.991291
Reading from 27257: heap size 507 MB, throughput 0.987725
Reading from 27257: heap size 508 MB, throughput 0.980338
Reading from 27257: heap size 511 MB, throughput 0.98365
Reading from 27256: heap size 555 MB, throughput 0.990412
Reading from 27257: heap size 512 MB, throughput 0.981204
Reading from 27256: heap size 556 MB, throughput 0.991452
Numeric result:
Recommendation: 2 clients, utility 1.9637:
    h1: 1252.3 MB (U(h) = 0.0550564*h^0.468508)
    h2: 955.697 MB (U(h) = 0.108485*h^0.357544)
Recommendation: 2 clients, utility 1.9637:
    h1: 1252.3 MB (U(h) = 0.0550564*h^0.468508)
    h2: 955.698 MB (U(h) = 0.108485*h^0.357544)
Reading from 27257: heap size 532 MB, throughput 0.994163
Reading from 27256: heap size 557 MB, throughput 0.991269
Reading from 27256: heap size 559 MB, throughput 0.986058
Reading from 27256: heap size 561 MB, throughput 0.987472
Reading from 27257: heap size 533 MB, throughput 0.994131
Reading from 27256: heap size 569 MB, throughput 0.991268
Reading from 27257: heap size 539 MB, throughput 0.993142
Numeric result:
Recommendation: 2 clients, utility 2.1023:
    h1: 1365.19 MB (U(h) = 0.0345743*h^0.54555)
    h2: 842.811 MB (U(h) = 0.122501*h^0.336803)
Recommendation: 2 clients, utility 2.1023:
    h1: 1365.18 MB (U(h) = 0.0345743*h^0.54555)
    h2: 842.816 MB (U(h) = 0.122501*h^0.336803)
Reading from 27257: heap size 539 MB, throughput 0.993422
Reading from 27256: heap size 570 MB, throughput 0.990877
Reading from 27257: heap size 534 MB, throughput 0.993154
Reading from 27257: heap size 537 MB, throughput 0.988417
Reading from 27256: heap size 571 MB, throughput 0.99139
Reading from 27257: heap size 534 MB, throughput 0.978719
Reading from 27257: heap size 539 MB, throughput 0.989211
Reading from 27256: heap size 573 MB, throughput 0.991146
Numeric result:
Recommendation: 2 clients, utility 2.19996:
    h1: 1427.17 MB (U(h) = 0.0259948*h^0.592527)
    h2: 780.826 MB (U(h) = 0.132046*h^0.324174)
Recommendation: 2 clients, utility 2.19996:
    h1: 1427.18 MB (U(h) = 0.0259948*h^0.592527)
    h2: 780.817 MB (U(h) = 0.132046*h^0.324174)
Reading from 27257: heap size 543 MB, throughput 0.989354
Reading from 27256: heap size 574 MB, throughput 0.990602
Reading from 27257: heap size 547 MB, throughput 0.989773
Reading from 27256: heap size 575 MB, throughput 0.989225
Reading from 27256: heap size 578 MB, throughput 0.983101
Reading from 27257: heap size 549 MB, throughput 0.989489
Reading from 27256: heap size 580 MB, throughput 0.988505
Numeric result:
Recommendation: 2 clients, utility 2.15513:
    h1: 1476.75 MB (U(h) = 0.0257957*h^0.593196)
    h2: 731.247 MB (U(h) = 0.158723*h^0.293732)
Recommendation: 2 clients, utility 2.15513:
    h1: 1476.76 MB (U(h) = 0.0257957*h^0.593196)
    h2: 731.244 MB (U(h) = 0.158723*h^0.293732)
Reading from 27257: heap size 550 MB, throughput 0.988276
Reading from 27256: heap size 589 MB, throughput 0.99086
Reading from 27257: heap size 553 MB, throughput 0.987081
Reading from 27257: heap size 555 MB, throughput 0.98471
Reading from 27256: heap size 590 MB, throughput 0.991714
Reading from 27257: heap size 562 MB, throughput 0.976615
Reading from 27257: heap size 565 MB, throughput 0.98566
Reading from 27256: heap size 590 MB, throughput 0.993154
Numeric result:
Recommendation: 2 clients, utility 1.86643:
    h1: 1459.33 MB (U(h) = 0.0461655*h^0.49622)
    h2: 748.673 MB (U(h) = 0.20177*h^0.254574)
Recommendation: 2 clients, utility 1.86643:
    h1: 1459.33 MB (U(h) = 0.0461655*h^0.49622)
    h2: 748.673 MB (U(h) = 0.20177*h^0.254574)
Reading from 27257: heap size 569 MB, throughput 0.989041
Reading from 27256: heap size 593 MB, throughput 0.991886
Reading from 27257: heap size 572 MB, throughput 0.990613
Reading from 27256: heap size 596 MB, throughput 0.994103
Reading from 27256: heap size 596 MB, throughput 0.989392
Reading from 27257: heap size 570 MB, throughput 0.989864
Reading from 27256: heap size 596 MB, throughput 0.98723
Numeric result:
Recommendation: 2 clients, utility 1.75789:
    h1: 1466.28 MB (U(h) = 0.0583048*h^0.457514)
    h2: 741.721 MB (U(h) = 0.232505*h^0.231432)
Recommendation: 2 clients, utility 1.75789:
    h1: 1466.29 MB (U(h) = 0.0583048*h^0.457514)
    h2: 741.714 MB (U(h) = 0.232505*h^0.231432)
Reading from 27257: heap size 573 MB, throughput 0.989478
Reading from 27256: heap size 599 MB, throughput 0.993266
Reading from 27257: heap size 570 MB, throughput 0.991076
Reading from 27257: heap size 573 MB, throughput 0.987835
Reading from 27257: heap size 574 MB, throughput 0.982756
Reading from 27256: heap size 602 MB, throughput 0.994345
Reading from 27257: heap size 575 MB, throughput 0.990348
Reading from 27256: heap size 604 MB, throughput 0.993087
Numeric result:
Recommendation: 2 clients, utility 1.68682:
    h1: 1535.37 MB (U(h) = 0.0652892*h^0.438532)
    h2: 672.63 MB (U(h) = 0.296284*h^0.192117)
Recommendation: 2 clients, utility 1.68682:
    h1: 1535.37 MB (U(h) = 0.0652892*h^0.438532)
    h2: 672.631 MB (U(h) = 0.296284*h^0.192117)
Reading from 27257: heap size 578 MB, throughput 0.99223
Reading from 27256: heap size 604 MB, throughput 0.993133
Reading from 27257: heap size 580 MB, throughput 0.991929
Reading from 27256: heap size 606 MB, throughput 0.993616
Reading from 27256: heap size 609 MB, throughput 0.989631
Reading from 27257: heap size 578 MB, throughput 0.991173
Reading from 27256: heap size 610 MB, throughput 0.988541
Numeric result:
Recommendation: 2 clients, utility 1.57077:
    h1: 1752.06 MB (U(h) = 0.087526*h^0.390574)
    h2: 455.94 MB (U(h) = 0.521052*h^0.101639)
Recommendation: 2 clients, utility 1.57077:
    h1: 1752.06 MB (U(h) = 0.087526*h^0.390574)
    h2: 455.937 MB (U(h) = 0.521052*h^0.101639)
Reading from 27257: heap size 581 MB, throughput 0.989935
Reading from 27256: heap size 617 MB, throughput 0.99208
Reading from 27257: heap size 582 MB, throughput 0.989133
Reading from 27257: heap size 583 MB, throughput 0.983166
Reading from 27257: heap size 587 MB, throughput 0.988365
Reading from 27256: heap size 582 MB, throughput 0.992747
Numeric result:
Recommendation: 2 clients, utility 1.4641:
    h1: 1754.8 MB (U(h) = 0.121084*h^0.33747)
    h2: 453.199 MB (U(h) = 0.570356*h^0.0871548)
Recommendation: 2 clients, utility 1.4641:
    h1: 1754.81 MB (U(h) = 0.121084*h^0.33747)
    h2: 453.194 MB (U(h) = 0.570356*h^0.0871548)
Reading from 27257: heap size 588 MB, throughput 0.989067
Reading from 27256: heap size 581 MB, throughput 0.993098
Reading from 27257: heap size 590 MB, throughput 0.991099
Reading from 27256: heap size 569 MB, throughput 0.991719
Reading from 27257: heap size 592 MB, throughput 0.99138
Reading from 27256: heap size 566 MB, throughput 0.991651
Reading from 27256: heap size 543 MB, throughput 0.987977
Reading from 27256: heap size 555 MB, throughput 0.981748
Numeric result:
Recommendation: 2 clients, utility 1.41282:
    h1: 1762.19 MB (U(h) = 0.14311*h^0.310133)
    h2: 445.814 MB (U(h) = 0.60236*h^0.0784609)
Recommendation: 2 clients, utility 1.41282:
    h1: 1762.18 MB (U(h) = 0.14311*h^0.310133)
    h2: 445.817 MB (U(h) = 0.60236*h^0.0784609)
Reading from 27257: heap size 591 MB, throughput 0.991135
Reading from 27256: heap size 526 MB, throughput 0.988902
Reading from 27257: heap size 593 MB, throughput 0.991009
Reading from 27257: heap size 595 MB, throughput 0.985974
Client 27257 died
Clients: 1
Reading from 27256: heap size 536 MB, throughput 0.992475
Reading from 27256: heap size 515 MB, throughput 0.993213
Recommendation: one client; give it all the memory
Reading from 27256: heap size 519 MB, throughput 0.992887
Reading from 27256: heap size 528 MB, throughput 0.992682
Reading from 27256: heap size 531 MB, throughput 0.992
Recommendation: one client; give it all the memory
Reading from 27256: heap size 531 MB, throughput 0.99284
Reading from 27256: heap size 535 MB, throughput 0.988283
Client 27256 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
