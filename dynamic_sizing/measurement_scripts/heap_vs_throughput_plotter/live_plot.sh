#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

SOCKET="default"

if [ ! -z "${1}" ]; then
    SOCKET="${1}"
fi

function cleanup()
{
    test ! -z "${UNBUFFER}" && kill "${UNBUFFER}" &>/dev/null
    test ! -z "${DEMUX}" && kill "${DEMUX}" &>/dev/null
    wait
    rm "${FIFO}"
    exit
}

trap "cleanup" SIGINT SIGTERM SIGHUP

FIFO="$(mktemp --dry-run)"
mkfifo "${FIFO}"

unbuffer "${DIR}/../../reading_pipe/rp" "${SOCKET}" > "${FIFO}" &
UNBUFFER="${!}"

python "${DIR}/demux_to_plotters.py" "${@}" < "${FIFO}" &
DEMUX="${!}"

wait
cleanup
