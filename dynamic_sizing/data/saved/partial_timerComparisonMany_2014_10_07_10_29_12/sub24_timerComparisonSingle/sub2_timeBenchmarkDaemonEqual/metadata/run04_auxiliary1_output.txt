economemd
    total memory: 3312 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub24_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 32735: heap size 9 MB, throughput 0.99161
Clients: 1
Client 32735 has a minimum heap size of 276 MB
Reading from 32736: heap size 9 MB, throughput 0.948109
Clients: 2
Client 32736 has a minimum heap size of 276 MB
Reading from 32735: heap size 9 MB, throughput 0.97473
Reading from 32736: heap size 9 MB, throughput 0.980663
Reading from 32735: heap size 9 MB, throughput 0.957852
Reading from 32735: heap size 9 MB, throughput 0.960123
Reading from 32736: heap size 11 MB, throughput 0.973638
Reading from 32735: heap size 11 MB, throughput 0.969711
Reading from 32736: heap size 11 MB, throughput 0.945114
Reading from 32735: heap size 11 MB, throughput 0.984123
Reading from 32736: heap size 15 MB, throughput 0.865204
Reading from 32735: heap size 17 MB, throughput 0.916577
Reading from 32736: heap size 18 MB, throughput 0.973099
Reading from 32735: heap size 17 MB, throughput 0.494951
Reading from 32736: heap size 24 MB, throughput 0.937655
Reading from 32735: heap size 29 MB, throughput 0.924305
Reading from 32735: heap size 30 MB, throughput 0.929932
Reading from 32736: heap size 28 MB, throughput 0.633792
Reading from 32736: heap size 41 MB, throughput 0.9065
Reading from 32736: heap size 42 MB, throughput 0.879237
Reading from 32736: heap size 45 MB, throughput 0.897884
Reading from 32735: heap size 35 MB, throughput 0.224649
Reading from 32735: heap size 47 MB, throughput 0.873953
Reading from 32736: heap size 46 MB, throughput 0.343331
Reading from 32736: heap size 63 MB, throughput 0.858383
Reading from 32735: heap size 49 MB, throughput 0.472236
Reading from 32736: heap size 66 MB, throughput 0.668911
Reading from 32735: heap size 64 MB, throughput 0.822353
Reading from 32736: heap size 71 MB, throughput 0.843609
Reading from 32735: heap size 71 MB, throughput 0.273398
Reading from 32735: heap size 88 MB, throughput 0.821178
Reading from 32736: heap size 73 MB, throughput 0.13753
Reading from 32735: heap size 95 MB, throughput 0.772371
Reading from 32736: heap size 110 MB, throughput 0.262167
Reading from 32735: heap size 96 MB, throughput 0.295637
Reading from 32736: heap size 132 MB, throughput 0.88835
Reading from 32735: heap size 131 MB, throughput 0.65699
Reading from 32736: heap size 133 MB, throughput 0.837974
Reading from 32736: heap size 136 MB, throughput 0.720456
Reading from 32735: heap size 131 MB, throughput 0.277462
Reading from 32735: heap size 170 MB, throughput 0.71979
Reading from 32736: heap size 139 MB, throughput 0.218223
Reading from 32735: heap size 170 MB, throughput 0.842863
Reading from 32736: heap size 174 MB, throughput 0.773073
Reading from 32735: heap size 172 MB, throughput 0.836053
Reading from 32736: heap size 181 MB, throughput 0.828468
Reading from 32735: heap size 175 MB, throughput 0.789311
Reading from 32736: heap size 182 MB, throughput 0.722194
Reading from 32736: heap size 191 MB, throughput 0.834024
Reading from 32735: heap size 178 MB, throughput 0.1918
Reading from 32735: heap size 222 MB, throughput 0.616236
Reading from 32736: heap size 195 MB, throughput 0.359328
Reading from 32736: heap size 245 MB, throughput 0.886004
Reading from 32735: heap size 226 MB, throughput 0.96347
Reading from 32736: heap size 248 MB, throughput 0.867379
Reading from 32735: heap size 229 MB, throughput 0.813418
Reading from 32735: heap size 241 MB, throughput 0.945088
Reading from 32736: heap size 253 MB, throughput 0.742957
Reading from 32735: heap size 242 MB, throughput 0.817769
Reading from 32736: heap size 258 MB, throughput 0.72733
Reading from 32735: heap size 244 MB, throughput 0.900965
Reading from 32736: heap size 265 MB, throughput 0.865211
Reading from 32735: heap size 245 MB, throughput 0.909436
Reading from 32736: heap size 268 MB, throughput 0.860364
Reading from 32735: heap size 249 MB, throughput 0.87725
Reading from 32736: heap size 272 MB, throughput 0.717547
Reading from 32735: heap size 250 MB, throughput 0.606666
Reading from 32735: heap size 255 MB, throughput 0.733821
Reading from 32736: heap size 278 MB, throughput 0.757838
Reading from 32735: heap size 258 MB, throughput 0.768945
Reading from 32736: heap size 285 MB, throughput 0.794232
Reading from 32735: heap size 264 MB, throughput 0.78212
Reading from 32736: heap size 289 MB, throughput 0.782737
Reading from 32735: heap size 266 MB, throughput 0.943812
Reading from 32735: heap size 270 MB, throughput 0.566679
Reading from 32736: heap size 296 MB, throughput 0.909347
Reading from 32735: heap size 274 MB, throughput 0.605963
Reading from 32736: heap size 298 MB, throughput 0.637748
Reading from 32736: heap size 305 MB, throughput 0.498265
Reading from 32736: heap size 308 MB, throughput 0.739718
Reading from 32735: heap size 281 MB, throughput 0.09948
Reading from 32735: heap size 324 MB, throughput 0.610029
Reading from 32736: heap size 307 MB, throughput 0.145494
Reading from 32735: heap size 328 MB, throughput 0.886582
Reading from 32736: heap size 353 MB, throughput 0.751998
Reading from 32735: heap size 329 MB, throughput 0.835051
Reading from 32735: heap size 333 MB, throughput 0.720243
Reading from 32736: heap size 356 MB, throughput 0.921668
Reading from 32735: heap size 334 MB, throughput 0.713961
Reading from 32736: heap size 357 MB, throughput 0.653383
Reading from 32735: heap size 342 MB, throughput 0.644277
Reading from 32736: heap size 357 MB, throughput 0.70703
Reading from 32735: heap size 342 MB, throughput 0.645333
Reading from 32736: heap size 359 MB, throughput 0.659001
Reading from 32735: heap size 351 MB, throughput 0.621137
Reading from 32736: heap size 362 MB, throughput 0.60423
Reading from 32736: heap size 364 MB, throughput 0.701766
Equal recommendation: 1656 MB each
Reading from 32735: heap size 352 MB, throughput 0.96885
Reading from 32736: heap size 374 MB, throughput 0.979574
Reading from 32735: heap size 355 MB, throughput 0.968802
Reading from 32736: heap size 374 MB, throughput 0.985723
Reading from 32735: heap size 358 MB, throughput 0.97702
Reading from 32735: heap size 357 MB, throughput 0.974057
Reading from 32736: heap size 378 MB, throughput 0.983183
Reading from 32735: heap size 360 MB, throughput 0.973349
Reading from 32736: heap size 381 MB, throughput 0.718194
Reading from 32735: heap size 363 MB, throughput 0.975671
Reading from 32735: heap size 365 MB, throughput 0.978517
Reading from 32736: heap size 428 MB, throughput 0.996906
Equal recommendation: 1656 MB each
Reading from 32736: heap size 429 MB, throughput 0.985478
Reading from 32735: heap size 363 MB, throughput 0.970301
Reading from 32735: heap size 365 MB, throughput 0.965172
Reading from 32736: heap size 439 MB, throughput 0.990138
Reading from 32735: heap size 366 MB, throughput 0.977234
Reading from 32736: heap size 440 MB, throughput 0.990039
Reading from 32735: heap size 367 MB, throughput 0.969846
Reading from 32736: heap size 441 MB, throughput 0.990716
Reading from 32735: heap size 369 MB, throughput 0.689868
Reading from 32736: heap size 443 MB, throughput 0.985702
Reading from 32735: heap size 412 MB, throughput 0.968271
Reading from 32736: heap size 437 MB, throughput 0.98385
Equal recommendation: 1656 MB each
Reading from 32735: heap size 415 MB, throughput 0.986201
Reading from 32735: heap size 416 MB, throughput 0.909717
Reading from 32735: heap size 416 MB, throughput 0.85846
Reading from 32735: heap size 418 MB, throughput 0.844462
Reading from 32735: heap size 425 MB, throughput 0.894453
Reading from 32736: heap size 441 MB, throughput 0.987632
Reading from 32736: heap size 436 MB, throughput 0.953364
Reading from 32736: heap size 441 MB, throughput 0.801422
Reading from 32736: heap size 445 MB, throughput 0.785578
Reading from 32735: heap size 426 MB, throughput 0.992356
Reading from 32736: heap size 452 MB, throughput 0.806207
Reading from 32736: heap size 462 MB, throughput 0.984207
Reading from 32735: heap size 430 MB, throughput 0.989071
Reading from 32735: heap size 432 MB, throughput 0.975079
Reading from 32736: heap size 466 MB, throughput 0.980723
Reading from 32735: heap size 432 MB, throughput 0.988636
Reading from 32736: heap size 462 MB, throughput 0.985801
Equal recommendation: 1656 MB each
Reading from 32735: heap size 434 MB, throughput 0.985643
Reading from 32736: heap size 467 MB, throughput 0.983752
Reading from 32735: heap size 431 MB, throughput 0.98514
Reading from 32736: heap size 465 MB, throughput 0.980873
Reading from 32735: heap size 434 MB, throughput 0.988695
Reading from 32736: heap size 468 MB, throughput 0.988096
Reading from 32735: heap size 434 MB, throughput 0.987917
Reading from 32736: heap size 465 MB, throughput 0.987364
Reading from 32735: heap size 435 MB, throughput 0.983327
Reading from 32736: heap size 468 MB, throughput 0.982352
Reading from 32735: heap size 437 MB, throughput 0.98035
Equal recommendation: 1656 MB each
Reading from 32736: heap size 466 MB, throughput 0.981354
Reading from 32735: heap size 438 MB, throughput 0.989984
Reading from 32735: heap size 441 MB, throughput 0.925312
Reading from 32735: heap size 441 MB, throughput 0.866908
Reading from 32735: heap size 446 MB, throughput 0.859447
Reading from 32736: heap size 468 MB, throughput 0.986749
Reading from 32735: heap size 447 MB, throughput 0.9882
Reading from 32736: heap size 466 MB, throughput 0.955861
Reading from 32736: heap size 468 MB, throughput 0.884536
Reading from 32736: heap size 472 MB, throughput 0.872592
Reading from 32736: heap size 471 MB, throughput 0.98633
Reading from 32735: heap size 454 MB, throughput 0.988741
Reading from 32735: heap size 455 MB, throughput 0.985087
Reading from 32736: heap size 477 MB, throughput 0.987819
Reading from 32735: heap size 456 MB, throughput 0.988473
Reading from 32736: heap size 479 MB, throughput 0.988558
Equal recommendation: 1656 MB each
Reading from 32735: heap size 458 MB, throughput 0.982546
Reading from 32736: heap size 484 MB, throughput 0.987786
Reading from 32735: heap size 456 MB, throughput 0.989446
Reading from 32736: heap size 485 MB, throughput 0.991019
Reading from 32735: heap size 459 MB, throughput 0.984403
Reading from 32736: heap size 485 MB, throughput 0.98731
Reading from 32735: heap size 458 MB, throughput 0.984721
Reading from 32736: heap size 487 MB, throughput 0.985805
Reading from 32735: heap size 459 MB, throughput 0.981107
Equal recommendation: 1656 MB each
Reading from 32736: heap size 486 MB, throughput 0.985432
Reading from 32735: heap size 462 MB, throughput 0.990185
Reading from 32735: heap size 463 MB, throughput 0.895329
Reading from 32735: heap size 464 MB, throughput 0.873988
Reading from 32735: heap size 466 MB, throughput 0.978843
Reading from 32736: heap size 487 MB, throughput 0.991805
Reading from 32736: heap size 490 MB, throughput 0.870993
Reading from 32736: heap size 490 MB, throughput 0.883583
Reading from 32735: heap size 473 MB, throughput 0.990904
Reading from 32736: heap size 498 MB, throughput 0.989475
Reading from 32735: heap size 474 MB, throughput 0.990724
Reading from 32736: heap size 499 MB, throughput 0.991467
Reading from 32735: heap size 475 MB, throughput 0.991917
Equal recommendation: 1656 MB each
Reading from 32736: heap size 504 MB, throughput 0.992963
Reading from 32735: heap size 477 MB, throughput 0.987883
Reading from 32736: heap size 506 MB, throughput 0.990161
Reading from 32735: heap size 474 MB, throughput 0.986174
Reading from 32735: heap size 477 MB, throughput 0.983291
Reading from 32736: heap size 506 MB, throughput 0.982504
Reading from 32735: heap size 479 MB, throughput 0.985865
Reading from 32736: heap size 508 MB, throughput 0.987583
Equal recommendation: 1656 MB each
Reading from 32735: heap size 479 MB, throughput 0.992614
Reading from 32735: heap size 482 MB, throughput 0.912831
Reading from 32736: heap size 509 MB, throughput 0.987305
Reading from 32735: heap size 482 MB, throughput 0.856489
Reading from 32735: heap size 488 MB, throughput 0.983236
Reading from 32735: heap size 490 MB, throughput 0.991514
Reading from 32736: heap size 510 MB, throughput 0.993449
Reading from 32736: heap size 511 MB, throughput 0.924383
Reading from 32736: heap size 513 MB, throughput 0.893973
Reading from 32735: heap size 494 MB, throughput 0.988039
Reading from 32736: heap size 519 MB, throughput 0.99134
Equal recommendation: 1656 MB each
Reading from 32735: heap size 496 MB, throughput 0.990207
Reading from 32736: heap size 521 MB, throughput 0.99235
Reading from 32735: heap size 495 MB, throughput 0.989958
Reading from 32736: heap size 525 MB, throughput 0.991887
Reading from 32735: heap size 497 MB, throughput 0.990192
Reading from 32736: heap size 527 MB, throughput 0.98961
Reading from 32735: heap size 499 MB, throughput 0.988053
Equal recommendation: 1656 MB each
Reading from 32736: heap size 525 MB, throughput 0.990592
Reading from 32735: heap size 499 MB, throughput 0.994797
Reading from 32735: heap size 501 MB, throughput 0.975715
Reading from 32735: heap size 502 MB, throughput 0.900295
Reading from 32736: heap size 528 MB, throughput 0.985744
Reading from 32735: heap size 506 MB, throughput 0.986132
Reading from 32735: heap size 507 MB, throughput 0.990744
Reading from 32736: heap size 531 MB, throughput 0.993291
Reading from 32736: heap size 532 MB, throughput 0.935308
Reading from 32736: heap size 533 MB, throughput 0.915984
Reading from 32735: heap size 510 MB, throughput 0.99062
Reading from 32736: heap size 535 MB, throughput 0.991564
Equal recommendation: 1656 MB each
Reading from 32735: heap size 512 MB, throughput 0.989118
Reading from 32736: heap size 542 MB, throughput 0.993374
Reading from 32735: heap size 510 MB, throughput 0.988942
Reading from 32736: heap size 544 MB, throughput 0.991865
Reading from 32735: heap size 512 MB, throughput 0.988467
Reading from 32735: heap size 514 MB, throughput 0.985133
Reading from 32736: heap size 544 MB, throughput 0.991824
Equal recommendation: 1656 MB each
Reading from 32735: heap size 514 MB, throughput 0.991558
Reading from 32735: heap size 517 MB, throughput 0.940463
Reading from 32735: heap size 518 MB, throughput 0.950511
Reading from 32736: heap size 547 MB, throughput 0.991638
Reading from 32735: heap size 525 MB, throughput 0.99178
Reading from 32736: heap size 548 MB, throughput 0.988433
Reading from 32735: heap size 525 MB, throughput 0.99212
Reading from 32736: heap size 549 MB, throughput 0.987778
Reading from 32736: heap size 551 MB, throughput 0.893564
Equal recommendation: 1656 MB each
Reading from 32736: heap size 553 MB, throughput 0.988107
Reading from 32735: heap size 524 MB, throughput 0.990406
Reading from 32735: heap size 527 MB, throughput 0.988658
Reading from 32736: heap size 562 MB, throughput 0.992833
Reading from 32735: heap size 527 MB, throughput 0.989465
Reading from 32736: heap size 562 MB, throughput 0.991461
Reading from 32735: heap size 528 MB, throughput 0.988841
Equal recommendation: 1656 MB each
Reading from 32736: heap size 563 MB, throughput 0.991781
Reading from 32735: heap size 530 MB, throughput 0.990446
Reading from 32735: heap size 531 MB, throughput 0.919607
Reading from 32735: heap size 530 MB, throughput 0.979022
Reading from 32736: heap size 565 MB, throughput 0.990916
Reading from 32735: heap size 533 MB, throughput 0.991666
Reading from 32736: heap size 567 MB, throughput 0.99272
Reading from 32735: heap size 535 MB, throughput 0.992586
Equal recommendation: 1656 MB each
Reading from 32736: heap size 567 MB, throughput 0.986621
Reading from 32736: heap size 571 MB, throughput 0.925987
Reading from 32735: heap size 537 MB, throughput 0.9915
Reading from 32736: heap size 572 MB, throughput 0.990623
Reading from 32735: heap size 535 MB, throughput 0.990193
Reading from 32736: heap size 579 MB, throughput 0.992922
Reading from 32735: heap size 537 MB, throughput 0.988465
Equal recommendation: 1656 MB each
Reading from 32736: heap size 580 MB, throughput 0.99219
Reading from 32735: heap size 540 MB, throughput 0.989059
Reading from 32735: heap size 540 MB, throughput 0.96777
Reading from 32735: heap size 541 MB, throughput 0.899451
Reading from 32736: heap size 580 MB, throughput 0.991883
Reading from 32735: heap size 543 MB, throughput 0.991107
Reading from 32736: heap size 582 MB, throughput 0.991203
Reading from 32735: heap size 550 MB, throughput 0.99303
Equal recommendation: 1656 MB each
Reading from 32736: heap size 584 MB, throughput 0.99466
Reading from 32735: heap size 551 MB, throughput 0.991626
Reading from 32736: heap size 584 MB, throughput 0.91531
Reading from 32736: heap size 585 MB, throughput 0.982044
Reading from 32735: heap size 552 MB, throughput 0.989683
Reading from 32736: heap size 587 MB, throughput 0.993389
Reading from 32735: heap size 554 MB, throughput 0.98874
Equal recommendation: 1656 MB each
Reading from 32736: heap size 590 MB, throughput 0.992998
Reading from 32735: heap size 556 MB, throughput 0.993142
Reading from 32735: heap size 556 MB, throughput 0.977052
Reading from 32735: heap size 556 MB, throughput 0.9246
Reading from 32736: heap size 592 MB, throughput 0.991359
Reading from 32735: heap size 558 MB, throughput 0.990763
Reading from 32736: heap size 592 MB, throughput 0.991586
Reading from 32735: heap size 565 MB, throughput 0.992527
Equal recommendation: 1656 MB each
Reading from 32735: heap size 566 MB, throughput 0.991961
Reading from 32736: heap size 594 MB, throughput 0.994748
Reading from 32736: heap size 597 MB, throughput 0.958436
Reading from 32736: heap size 597 MB, throughput 0.979195
Reading from 32735: heap size 565 MB, throughput 0.99409
Reading from 32736: heap size 604 MB, throughput 0.994625
Reading from 32735: heap size 567 MB, throughput 0.99001
Equal recommendation: 1656 MB each
Reading from 32736: heap size 605 MB, throughput 0.992971
Reading from 32735: heap size 569 MB, throughput 0.99238
Reading from 32735: heap size 570 MB, throughput 0.938682
Reading from 32735: heap size 569 MB, throughput 0.981054
Reading from 32736: heap size 603 MB, throughput 0.991463
Reading from 32735: heap size 572 MB, throughput 0.99507
Equal recommendation: 1656 MB each
Reading from 32736: heap size 606 MB, throughput 0.993357
Reading from 32735: heap size 575 MB, throughput 0.994226
Reading from 32736: heap size 608 MB, throughput 0.991191
Reading from 32735: heap size 576 MB, throughput 0.987302
Reading from 32736: heap size 609 MB, throughput 0.938141
Reading from 32736: heap size 609 MB, throughput 0.989401
Reading from 32735: heap size 575 MB, throughput 0.990718
Equal recommendation: 1656 MB each
Reading from 32736: heap size 611 MB, throughput 0.994068
Reading from 32735: heap size 577 MB, throughput 0.987808
Reading from 32735: heap size 577 MB, throughput 0.978161
Reading from 32735: heap size 581 MB, throughput 0.922489
Reading from 32736: heap size 613 MB, throughput 0.991417
Reading from 32735: heap size 585 MB, throughput 0.99315
Reading from 32736: heap size 615 MB, throughput 0.994227
Reading from 32735: heap size 586 MB, throughput 0.993605
Equal recommendation: 1656 MB each
Reading from 32736: heap size 617 MB, throughput 0.990548
Reading from 32735: heap size 589 MB, throughput 0.992353
Reading from 32736: heap size 618 MB, throughput 0.986386
Reading from 32736: heap size 620 MB, throughput 0.924947
Reading from 32735: heap size 590 MB, throughput 0.991472
Reading from 32736: heap size 623 MB, throughput 0.995781
Equal recommendation: 1656 MB each
Reading from 32735: heap size 590 MB, throughput 0.988774
Reading from 32735: heap size 591 MB, throughput 0.987976
Reading from 32735: heap size 594 MB, throughput 0.929731
Reading from 32736: heap size 629 MB, throughput 0.995289
Reading from 32735: heap size 595 MB, throughput 0.991163
Reading from 32736: heap size 631 MB, throughput 0.992623
Equal recommendation: 1656 MB each
Reading from 32735: heap size 602 MB, throughput 0.995119
Reading from 32736: heap size 631 MB, throughput 0.992117
Reading from 32735: heap size 602 MB, throughput 0.992569
Reading from 32736: heap size 633 MB, throughput 0.995214
Reading from 32736: heap size 635 MB, throughput 0.942452
Reading from 32735: heap size 602 MB, throughput 0.988327
Reading from 32736: heap size 636 MB, throughput 0.992049
Equal recommendation: 1656 MB each
Reading from 32735: heap size 604 MB, throughput 0.990589
Reading from 32735: heap size 606 MB, throughput 0.988767
Reading from 32735: heap size 607 MB, throughput 0.950938
Client 32735 died
Clients: 1
Reading from 32736: heap size 644 MB, throughput 0.995728
Reading from 32736: heap size 645 MB, throughput 0.994594
Recommendation: one client; give it all the memory
Reading from 32736: heap size 644 MB, throughput 0.994124
Reading from 32736: heap size 646 MB, throughput 0.996841
Reading from 32736: heap size 648 MB, throughput 0.962149
Client 32736 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
