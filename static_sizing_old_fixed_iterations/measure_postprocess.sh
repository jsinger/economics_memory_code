#!/bin/bash
# Process an existing measure dataset
#
# Usage: measure_postprocess.sh data_dir
#

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") data_dir"
    exit 1
}

test -z "${1}" && usage
test ! -d "${1}" && usage

DATA_DIR="$(readlink -f "${1}")"

cd "${DATA_DIR}"

BENCHMARKS_FILE="${DATA_DIR}/benchmarks.txt"
ITERATIONS="$(cat "${DATA_DIR}/iterations.txt")"

while read BENCHMARK; do
    RESULTS="${DATA_DIR}/${BENCHMARK}_results.txt"
    HEAP_SIZES_FILE="${DATA_DIR}/${BENCHMARK}_heapSizes.txt"
    AVG_TIMES_FILE="${DATA_DIR}/${BENCHMARK}_avgTimes.txt"
    STDDEV_FILE="${DATA_DIR}/${BENCHMARK}_stddev.txt"
    THROUGHPUT_FILE="${DATA_DIR}/${BENCHMARK}_throughput.txt"
    COEFFICIENT_FILE="${DATA_DIR}/${BENCHMARK}_fit_coefficient.txt"
    EXPONENT_FILE="${DATA_DIR}/${BENCHMARK}_fit_exponent.txt"

    cat ${BENCHMARK}_*_results.txt | sort -n > "${RESULTS}"

    cut '--delimiter= ' -f 1 "${RESULTS}" > "${HEAP_SIZES_FILE}"
    cut '--delimiter= ' -f 2 "${RESULTS}" > "${AVG_TIMES_FILE}"
    cut '--delimiter= ' -f 3 "${RESULTS}" > "${STDDEV_FILE}"
    cut '--delimiter= ' -f 4 "${RESULTS}" > "${THROUGHPUT_FILE}"

    octave -qf "${DIR}/measure_plot.m" "${BENCHMARK} (${ITERATIONS} iterations)" "${HEAP_SIZES_FILE}" "${AVG_TIMES_FILE}" "${STDDEV_FILE}" "${THROUGHPUT_FILE}" "${DATA_DIR}/${BENCHMARK}_runtime.pdf" "${DATA_DIR}/${BENCHMARK}_throughput.pdf" "${COEFFICIENT_FILE}" "${EXPONENT_FILE}"

done < "${BENCHMARKS_FILE}"
