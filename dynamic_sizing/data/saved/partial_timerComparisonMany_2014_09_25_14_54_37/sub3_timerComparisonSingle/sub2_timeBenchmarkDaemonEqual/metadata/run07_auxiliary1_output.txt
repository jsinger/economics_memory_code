economemd
    total memory: 4461 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub3_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
TimerThread: starting
ReadingThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 4847: heap size 9 MB, throughput 0.991841
Clients: 1
Client 4847 has a minimum heap size of 1211 MB
Reading from 4848: heap size 9 MB, throughput 0.985358
Clients: 2
Client 4848 has a minimum heap size of 276 MB
Reading from 4848: heap size 9 MB, throughput 0.970583
Reading from 4847: heap size 9 MB, throughput 0.979402
Reading from 4847: heap size 9 MB, throughput 0.954276
Reading from 4848: heap size 11 MB, throughput 0.97257
Reading from 4847: heap size 9 MB, throughput 0.947127
Reading from 4847: heap size 11 MB, throughput 0.969471
Reading from 4848: heap size 11 MB, throughput 0.982901
Reading from 4847: heap size 11 MB, throughput 0.97587
Reading from 4847: heap size 17 MB, throughput 0.939019
Reading from 4848: heap size 15 MB, throughput 0.812148
Reading from 4847: heap size 17 MB, throughput 0.606042
Reading from 4848: heap size 19 MB, throughput 0.971596
Reading from 4848: heap size 23 MB, throughput 0.960296
Reading from 4847: heap size 30 MB, throughput 0.958653
Reading from 4847: heap size 31 MB, throughput 0.911894
Reading from 4848: heap size 26 MB, throughput 0.528198
Reading from 4848: heap size 37 MB, throughput 0.933195
Reading from 4847: heap size 34 MB, throughput 0.379509
Reading from 4848: heap size 39 MB, throughput 0.916187
Reading from 4847: heap size 48 MB, throughput 0.932493
Reading from 4848: heap size 40 MB, throughput 0.731421
Reading from 4848: heap size 41 MB, throughput 0.822101
Reading from 4847: heap size 47 MB, throughput 0.921799
Reading from 4848: heap size 45 MB, throughput 0.808199
Reading from 4848: heap size 47 MB, throughput 0.212728
Reading from 4848: heap size 66 MB, throughput 0.822318
Reading from 4847: heap size 50 MB, throughput 0.157804
Reading from 4848: heap size 68 MB, throughput 0.732242
Reading from 4847: heap size 71 MB, throughput 0.830566
Reading from 4848: heap size 72 MB, throughput 0.806756
Reading from 4847: heap size 71 MB, throughput 0.828586
Reading from 4848: heap size 75 MB, throughput 0.680754
Reading from 4847: heap size 77 MB, throughput 0.200718
Reading from 4848: heap size 79 MB, throughput 0.180756
Reading from 4847: heap size 97 MB, throughput 0.681414
Reading from 4848: heap size 107 MB, throughput 0.683307
Reading from 4847: heap size 101 MB, throughput 0.75418
Reading from 4848: heap size 109 MB, throughput 0.706455
Reading from 4847: heap size 102 MB, throughput 0.202519
Reading from 4848: heap size 112 MB, throughput 0.17662
Reading from 4847: heap size 137 MB, throughput 0.821986
Reading from 4848: heap size 143 MB, throughput 0.676741
Reading from 4847: heap size 137 MB, throughput 0.837141
Reading from 4848: heap size 144 MB, throughput 0.791568
Reading from 4847: heap size 139 MB, throughput 0.768666
Reading from 4848: heap size 148 MB, throughput 0.792952
Reading from 4847: heap size 142 MB, throughput 0.803035
Reading from 4848: heap size 150 MB, throughput 0.71852
Reading from 4848: heap size 156 MB, throughput 0.712275
Reading from 4847: heap size 144 MB, throughput 0.0999508
Reading from 4847: heap size 184 MB, throughput 0.635194
Reading from 4848: heap size 160 MB, throughput 0.122284
Reading from 4847: heap size 186 MB, throughput 0.742355
Reading from 4848: heap size 208 MB, throughput 0.680753
Reading from 4847: heap size 189 MB, throughput 0.786567
Reading from 4847: heap size 196 MB, throughput 0.732907
Reading from 4848: heap size 210 MB, throughput 0.731439
Reading from 4847: heap size 198 MB, throughput 0.743149
Reading from 4848: heap size 214 MB, throughput 0.939523
Reading from 4847: heap size 200 MB, throughput 0.137201
Reading from 4847: heap size 250 MB, throughput 0.463411
Reading from 4847: heap size 255 MB, throughput 0.552722
Reading from 4847: heap size 257 MB, throughput 0.52363
Reading from 4848: heap size 218 MB, throughput 0.216391
Reading from 4847: heap size 259 MB, throughput 0.556412
Reading from 4848: heap size 270 MB, throughput 0.530199
Reading from 4847: heap size 266 MB, throughput 0.576444
Reading from 4848: heap size 272 MB, throughput 0.784296
Reading from 4847: heap size 271 MB, throughput 0.577562
Reading from 4847: heap size 278 MB, throughput 0.508769
Reading from 4848: heap size 271 MB, throughput 0.809103
Reading from 4847: heap size 284 MB, throughput 0.590948
Reading from 4848: heap size 272 MB, throughput 0.850864
Reading from 4848: heap size 276 MB, throughput 0.794195
Reading from 4848: heap size 276 MB, throughput 0.561387
Reading from 4848: heap size 283 MB, throughput 0.843821
Reading from 4847: heap size 289 MB, throughput 0.155142
Reading from 4848: heap size 285 MB, throughput 0.655892
Reading from 4847: heap size 341 MB, throughput 0.424351
Reading from 4848: heap size 292 MB, throughput 0.789456
Reading from 4847: heap size 343 MB, throughput 0.595306
Reading from 4847: heap size 345 MB, throughput 0.667321
Reading from 4847: heap size 346 MB, throughput 0.67741
Reading from 4848: heap size 294 MB, throughput 0.916042
Reading from 4848: heap size 297 MB, throughput 0.580222
Reading from 4847: heap size 347 MB, throughput 0.129058
Reading from 4848: heap size 301 MB, throughput 0.829778
Reading from 4848: heap size 304 MB, throughput 0.590526
Reading from 4847: heap size 399 MB, throughput 0.466285
Reading from 4847: heap size 397 MB, throughput 0.601774
Reading from 4847: heap size 401 MB, throughput 0.512488
Reading from 4847: heap size 403 MB, throughput 0.579898
Reading from 4848: heap size 306 MB, throughput 0.137996
Reading from 4847: heap size 407 MB, throughput 0.602716
Reading from 4847: heap size 411 MB, throughput 0.456948
Reading from 4848: heap size 353 MB, throughput 0.892364
Reading from 4847: heap size 419 MB, throughput 0.576332
Reading from 4848: heap size 354 MB, throughput 0.795919
Reading from 4848: heap size 357 MB, throughput 0.670492
Reading from 4848: heap size 360 MB, throughput 0.616843
Reading from 4848: heap size 366 MB, throughput 0.761943
Reading from 4848: heap size 366 MB, throughput 0.639976
Reading from 4847: heap size 428 MB, throughput 0.0914465
Equal recommendation: 2230 MB each
Reading from 4847: heap size 490 MB, throughput 0.379822
Reading from 4848: heap size 376 MB, throughput 0.939435
Reading from 4847: heap size 488 MB, throughput 0.503683
Reading from 4847: heap size 493 MB, throughput 0.107141
Reading from 4847: heap size 556 MB, throughput 0.497891
Reading from 4847: heap size 559 MB, throughput 0.65746
Reading from 4847: heap size 558 MB, throughput 0.609577
Reading from 4847: heap size 560 MB, throughput 0.610857
Reading from 4848: heap size 376 MB, throughput 0.984651
Reading from 4847: heap size 559 MB, throughput 0.643935
Reading from 4847: heap size 570 MB, throughput 0.583425
Reading from 4847: heap size 579 MB, throughput 0.623262
Reading from 4847: heap size 590 MB, throughput 0.543033
Reading from 4847: heap size 594 MB, throughput 0.577716
Reading from 4847: heap size 609 MB, throughput 0.555152
Reading from 4847: heap size 616 MB, throughput 0.516112
Reading from 4848: heap size 381 MB, throughput 0.970215
Reading from 4847: heap size 627 MB, throughput 0.196998
Reading from 4848: heap size 384 MB, throughput 0.979026
Reading from 4847: heap size 700 MB, throughput 0.726177
Reading from 4847: heap size 708 MB, throughput 0.768866
Reading from 4847: heap size 707 MB, throughput 0.799997
Reading from 4848: heap size 384 MB, throughput 0.962673
Reading from 4847: heap size 714 MB, throughput 0.809353
Reading from 4847: heap size 717 MB, throughput 0.306886
Reading from 4847: heap size 728 MB, throughput 0.260409
Reading from 4848: heap size 387 MB, throughput 0.969274
Reading from 4847: heap size 729 MB, throughput 0.0421104
Reading from 4847: heap size 706 MB, throughput 0.298635
Equal recommendation: 2230 MB each
Reading from 4847: heap size 799 MB, throughput 0.449947
Reading from 4847: heap size 687 MB, throughput 0.322363
Reading from 4848: heap size 389 MB, throughput 0.937483
Reading from 4847: heap size 796 MB, throughput 0.0448978
Reading from 4847: heap size 894 MB, throughput 0.23319
Reading from 4847: heap size 894 MB, throughput 0.640635
Reading from 4847: heap size 898 MB, throughput 0.763464
Reading from 4847: heap size 902 MB, throughput 0.902289
Reading from 4847: heap size 743 MB, throughput 0.594897
Reading from 4847: heap size 892 MB, throughput 0.588306
Reading from 4847: heap size 897 MB, throughput 0.595798
Reading from 4848: heap size 391 MB, throughput 0.960671
Reading from 4847: heap size 898 MB, throughput 0.800683
Reading from 4847: heap size 898 MB, throughput 0.963818
Reading from 4847: heap size 893 MB, throughput 0.902686
Reading from 4847: heap size 712 MB, throughput 0.936838
Reading from 4847: heap size 885 MB, throughput 0.810745
Reading from 4847: heap size 718 MB, throughput 0.917292
Reading from 4847: heap size 871 MB, throughput 0.898914
Reading from 4847: heap size 726 MB, throughput 0.851837
Reading from 4848: heap size 391 MB, throughput 0.975298
Reading from 4847: heap size 860 MB, throughput 0.903339
Reading from 4847: heap size 771 MB, throughput 0.795598
Reading from 4847: heap size 860 MB, throughput 0.780374
Reading from 4847: heap size 774 MB, throughput 0.77441
Reading from 4847: heap size 848 MB, throughput 0.799178
Reading from 4847: heap size 771 MB, throughput 0.78504
Reading from 4847: heap size 841 MB, throughput 0.818358
Reading from 4847: heap size 776 MB, throughput 0.820233
Reading from 4847: heap size 835 MB, throughput 0.778368
Reading from 4848: heap size 393 MB, throughput 0.955522
Reading from 4847: heap size 778 MB, throughput 0.83208
Reading from 4847: heap size 828 MB, throughput 0.844797
Reading from 4847: heap size 834 MB, throughput 0.845956
Reading from 4847: heap size 827 MB, throughput 0.868737
Reading from 4847: heap size 831 MB, throughput 0.984136
Reading from 4848: heap size 397 MB, throughput 0.974764
Reading from 4847: heap size 832 MB, throughput 0.967518
Reading from 4847: heap size 834 MB, throughput 0.753157
Reading from 4848: heap size 397 MB, throughput 0.967599
Reading from 4847: heap size 839 MB, throughput 0.72757
Reading from 4847: heap size 843 MB, throughput 0.75183
Reading from 4847: heap size 849 MB, throughput 0.771543
Reading from 4847: heap size 851 MB, throughput 0.762939
Reading from 4847: heap size 859 MB, throughput 0.713975
Reading from 4847: heap size 860 MB, throughput 0.778519
Reading from 4847: heap size 868 MB, throughput 0.772721
Reading from 4847: heap size 868 MB, throughput 0.790798
Reading from 4847: heap size 873 MB, throughput 0.880235
Equal recommendation: 2230 MB each
Reading from 4848: heap size 401 MB, throughput 0.975041
Reading from 4847: heap size 874 MB, throughput 0.834612
Reading from 4847: heap size 875 MB, throughput 0.865198
Reading from 4847: heap size 879 MB, throughput 0.697556
Reading from 4848: heap size 403 MB, throughput 0.929052
Reading from 4848: heap size 403 MB, throughput 0.798616
Reading from 4848: heap size 405 MB, throughput 0.694951
Reading from 4848: heap size 413 MB, throughput 0.821697
Reading from 4848: heap size 416 MB, throughput 0.967647
Reading from 4847: heap size 884 MB, throughput 0.0823447
Reading from 4847: heap size 980 MB, throughput 0.500903
Reading from 4847: heap size 1009 MB, throughput 0.745656
Reading from 4847: heap size 1010 MB, throughput 0.663414
Reading from 4847: heap size 1020 MB, throughput 0.724397
Reading from 4847: heap size 1020 MB, throughput 0.61557
Reading from 4847: heap size 1033 MB, throughput 0.70206
Reading from 4847: heap size 1034 MB, throughput 0.679182
Reading from 4847: heap size 1050 MB, throughput 0.717672
Reading from 4848: heap size 424 MB, throughput 0.978025
Reading from 4847: heap size 1052 MB, throughput 0.690757
Reading from 4847: heap size 1069 MB, throughput 0.743419
Reading from 4848: heap size 426 MB, throughput 0.986049
Reading from 4847: heap size 1071 MB, throughput 0.962519
Reading from 4848: heap size 429 MB, throughput 0.983878
Reading from 4847: heap size 1090 MB, throughput 0.965683
Equal recommendation: 2230 MB each
Reading from 4847: heap size 1092 MB, throughput 0.952717
Reading from 4848: heap size 431 MB, throughput 0.98481
Reading from 4847: heap size 1098 MB, throughput 0.949244
Reading from 4848: heap size 431 MB, throughput 0.979822
Reading from 4847: heap size 1102 MB, throughput 0.964652
Reading from 4848: heap size 433 MB, throughput 0.982399
Reading from 4847: heap size 1096 MB, throughput 0.94802
Reading from 4848: heap size 431 MB, throughput 0.983241
Reading from 4847: heap size 1103 MB, throughput 0.95398
Reading from 4848: heap size 433 MB, throughput 0.979869
Reading from 4847: heap size 1095 MB, throughput 0.954519
Equal recommendation: 2230 MB each
Reading from 4848: heap size 434 MB, throughput 0.990079
Reading from 4848: heap size 435 MB, throughput 0.920036
Reading from 4848: heap size 438 MB, throughput 0.871095
Reading from 4848: heap size 438 MB, throughput 0.87329
Reading from 4847: heap size 1101 MB, throughput 0.961062
Reading from 4848: heap size 445 MB, throughput 0.989033
Reading from 4847: heap size 1102 MB, throughput 0.941063
Reading from 4848: heap size 445 MB, throughput 0.991375
Reading from 4847: heap size 1104 MB, throughput 0.944244
Reading from 4848: heap size 450 MB, throughput 0.988273
Reading from 4847: heap size 1111 MB, throughput 0.957558
Reading from 4848: heap size 451 MB, throughput 0.987497
Reading from 4847: heap size 1112 MB, throughput 0.954172
Equal recommendation: 2230 MB each
Reading from 4848: heap size 451 MB, throughput 0.987252
Reading from 4847: heap size 1118 MB, throughput 0.957415
Reading from 4847: heap size 1121 MB, throughput 0.964601
Reading from 4848: heap size 453 MB, throughput 0.987714
Reading from 4847: heap size 1126 MB, throughput 0.956409
Reading from 4848: heap size 451 MB, throughput 0.986762
Reading from 4847: heap size 1131 MB, throughput 0.954829
Reading from 4848: heap size 453 MB, throughput 0.984448
Equal recommendation: 2230 MB each
Reading from 4847: heap size 1138 MB, throughput 0.956
Reading from 4848: heap size 455 MB, throughput 0.988536
Reading from 4848: heap size 455 MB, throughput 0.888711
Reading from 4848: heap size 456 MB, throughput 0.880573
Reading from 4847: heap size 1142 MB, throughput 0.925054
Reading from 4848: heap size 458 MB, throughput 0.67541
Reading from 4848: heap size 483 MB, throughput 0.976073
Reading from 4847: heap size 1149 MB, throughput 0.513116
Reading from 4848: heap size 483 MB, throughput 0.993239
Reading from 4847: heap size 1247 MB, throughput 0.946262
Reading from 4848: heap size 492 MB, throughput 0.991227
Equal recommendation: 2230 MB each
Reading from 4847: heap size 1253 MB, throughput 0.971696
Reading from 4848: heap size 493 MB, throughput 0.989349
Reading from 4847: heap size 1256 MB, throughput 0.971091
Reading from 4848: heap size 492 MB, throughput 0.989165
Reading from 4847: heap size 1259 MB, throughput 0.96927
Reading from 4848: heap size 494 MB, throughput 0.987937
Reading from 4847: heap size 1261 MB, throughput 0.967447
Equal recommendation: 2230 MB each
Reading from 4848: heap size 495 MB, throughput 0.990302
Reading from 4847: heap size 1257 MB, throughput 0.967219
Reading from 4848: heap size 496 MB, throughput 0.960771
Reading from 4848: heap size 494 MB, throughput 0.87819
Reading from 4848: heap size 499 MB, throughput 0.966398
Reading from 4847: heap size 1261 MB, throughput 0.96614
Reading from 4848: heap size 506 MB, throughput 0.991002
Reading from 4847: heap size 1251 MB, throughput 0.964572
Reading from 4848: heap size 509 MB, throughput 0.990354
Reading from 4847: heap size 1257 MB, throughput 0.961265
Equal recommendation: 2230 MB each
Reading from 4847: heap size 1259 MB, throughput 0.957927
Reading from 4848: heap size 508 MB, throughput 0.985793
Reading from 4847: heap size 1259 MB, throughput 0.949488
Reading from 4848: heap size 510 MB, throughput 0.983101
Reading from 4848: heap size 514 MB, throughput 0.978487
Reading from 4847: heap size 1265 MB, throughput 0.95628
Reading from 4848: heap size 515 MB, throughput 0.98389
Reading from 4847: heap size 1268 MB, throughput 0.956453
Equal recommendation: 2230 MB each
Reading from 4848: heap size 520 MB, throughput 0.989707
Reading from 4848: heap size 522 MB, throughput 0.784592
Reading from 4847: heap size 1275 MB, throughput 0.950747
Reading from 4848: heap size 522 MB, throughput 0.898994
Reading from 4847: heap size 1280 MB, throughput 0.945689
Reading from 4848: heap size 524 MB, throughput 0.99263
Reading from 4847: heap size 1288 MB, throughput 0.949722
Reading from 4848: heap size 533 MB, throughput 0.990627
Reading from 4847: heap size 1295 MB, throughput 0.950341
Reading from 4848: heap size 536 MB, throughput 0.989749
Equal recommendation: 2230 MB each
Reading from 4847: heap size 1305 MB, throughput 0.956957
Reading from 4848: heap size 539 MB, throughput 0.990832
Reading from 4847: heap size 1310 MB, throughput 0.954821
Reading from 4848: heap size 541 MB, throughput 0.988862
Reading from 4847: heap size 1320 MB, throughput 0.955063
Reading from 4848: heap size 542 MB, throughput 0.987788
Equal recommendation: 2230 MB each
Reading from 4848: heap size 544 MB, throughput 0.986791
Reading from 4848: heap size 548 MB, throughput 0.913825
Reading from 4847: heap size 1323 MB, throughput 0.9526
Reading from 4848: heap size 549 MB, throughput 0.983472
Reading from 4848: heap size 556 MB, throughput 0.979098
Reading from 4847: heap size 1332 MB, throughput 0.54704
Reading from 4848: heap size 557 MB, throughput 0.992529
Equal recommendation: 2230 MB each
Reading from 4848: heap size 557 MB, throughput 0.99124
Reading from 4848: heap size 560 MB, throughput 0.989885
Reading from 4848: heap size 560 MB, throughput 0.989022
Reading from 4848: heap size 562 MB, throughput 0.986264
Equal recommendation: 2230 MB each
Reading from 4848: heap size 565 MB, throughput 0.916262
Reading from 4848: heap size 566 MB, throughput 0.988883
Reading from 4848: heap size 573 MB, throughput 0.992834
Reading from 4848: heap size 574 MB, throughput 0.991604
Equal recommendation: 2230 MB each
Reading from 4848: heap size 574 MB, throughput 0.990695
Reading from 4848: heap size 577 MB, throughput 0.99045
Reading from 4847: heap size 1446 MB, throughput 0.994416
Reading from 4848: heap size 579 MB, throughput 0.992934
Reading from 4848: heap size 579 MB, throughput 0.924386
Equal recommendation: 2230 MB each
Reading from 4848: heap size 581 MB, throughput 0.987523
Reading from 4848: heap size 583 MB, throughput 0.9934
Reading from 4847: heap size 1448 MB, throughput 0.987962
Reading from 4847: heap size 1452 MB, throughput 0.88131
Reading from 4847: heap size 1464 MB, throughput 0.762307
Reading from 4847: heap size 1473 MB, throughput 0.72092
Reading from 4847: heap size 1518 MB, throughput 0.744696
Reading from 4848: heap size 585 MB, throughput 0.995264
Reading from 4847: heap size 1530 MB, throughput 0.755741
Reading from 4847: heap size 1563 MB, throughput 0.804467
Equal recommendation: 2230 MB each
Reading from 4847: heap size 1570 MB, throughput 0.951002
Reading from 4848: heap size 587 MB, throughput 0.991175
Reading from 4847: heap size 1581 MB, throughput 0.971599
Reading from 4848: heap size 586 MB, throughput 0.991633
Reading from 4847: heap size 1594 MB, throughput 0.970009
Reading from 4848: heap size 588 MB, throughput 0.994093
Reading from 4848: heap size 590 MB, throughput 0.936997
Equal recommendation: 2230 MB each
Reading from 4848: heap size 591 MB, throughput 0.973625
Reading from 4847: heap size 1589 MB, throughput 0.959914
Reading from 4847: heap size 1601 MB, throughput 0.966265
Reading from 4848: heap size 600 MB, throughput 0.993814
Reading from 4847: heap size 1604 MB, throughput 0.969847
Reading from 4848: heap size 600 MB, throughput 0.99234
Equal recommendation: 2230 MB each
Reading from 4847: heap size 1611 MB, throughput 0.969472
Reading from 4848: heap size 599 MB, throughput 0.990701
Reading from 4848: heap size 602 MB, throughput 0.986237
Reading from 4847: heap size 1623 MB, throughput 0.77713
Reading from 4848: heap size 605 MB, throughput 0.991729
Reading from 4848: heap size 606 MB, throughput 0.924682
Reading from 4847: heap size 1616 MB, throughput 0.991696
Equal recommendation: 2230 MB each
Reading from 4848: heap size 611 MB, throughput 0.992823
Reading from 4847: heap size 1612 MB, throughput 0.988719
Reading from 4848: heap size 613 MB, throughput 0.993444
Reading from 4847: heap size 1629 MB, throughput 0.986065
Reading from 4848: heap size 616 MB, throughput 0.992097
Equal recommendation: 2230 MB each
Reading from 4847: heap size 1650 MB, throughput 0.984308
Reading from 4848: heap size 618 MB, throughput 0.991987
Reading from 4847: heap size 1653 MB, throughput 0.981809
Reading from 4848: heap size 620 MB, throughput 0.993987
Reading from 4848: heap size 620 MB, throughput 0.972072
Reading from 4848: heap size 620 MB, throughput 0.983894
Reading from 4847: heap size 1650 MB, throughput 0.979571
Equal recommendation: 2230 MB each
Reading from 4848: heap size 623 MB, throughput 0.994349
Reading from 4847: heap size 1657 MB, throughput 0.979142
Reading from 4848: heap size 626 MB, throughput 0.993204
Reading from 4847: heap size 1638 MB, throughput 0.975536
Equal recommendation: 2230 MB each
Reading from 4848: heap size 627 MB, throughput 0.992875
Reading from 4847: heap size 1649 MB, throughput 0.972467
Reading from 4848: heap size 627 MB, throughput 0.990892
Reading from 4847: heap size 1642 MB, throughput 0.971314
Reading from 4848: heap size 628 MB, throughput 0.987028
Reading from 4848: heap size 631 MB, throughput 0.952117
Equal recommendation: 2230 MB each
Reading from 4847: heap size 1646 MB, throughput 0.971161
Reading from 4848: heap size 633 MB, throughput 0.993722
Reading from 4847: heap size 1654 MB, throughput 0.968221
Reading from 4848: heap size 637 MB, throughput 0.993381
Reading from 4847: heap size 1659 MB, throughput 0.965431
Equal recommendation: 2230 MB each
Reading from 4848: heap size 639 MB, throughput 0.991989
Reading from 4847: heap size 1670 MB, throughput 0.963505
Reading from 4848: heap size 640 MB, throughput 0.991819
Reading from 4848: heap size 641 MB, throughput 0.98786
Reading from 4848: heap size 643 MB, throughput 0.951206
Client 4848 died
Clients: 1
Reading from 4847: heap size 1681 MB, throughput 0.969686
Recommendation: one client; give it all the memory
Reading from 4847: heap size 1695 MB, throughput 0.969945
Reading from 4847: heap size 1710 MB, throughput 0.967979
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 4847: heap size 1728 MB, throughput 0.996826
Recommendation: one client; give it all the memory
Reading from 4847: heap size 1738 MB, throughput 0.986963
Reading from 4847: heap size 1736 MB, throughput 0.844984
Reading from 4847: heap size 1738 MB, throughput 0.782025
Reading from 4847: heap size 1772 MB, throughput 0.819016
Client 4847 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
