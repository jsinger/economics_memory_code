#ifndef SIMPLETHROUGHPUT_HPP
#define SIMPLETHROUGHPUT_HPP

#include "Stopwatch.hpp"
#include "ThroughputCalculator.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
//! Throughput-calculation algorithm that treats all GCs the same.
/*! This algorithm applies the basic definition of throughput directly: for every period of mutation followed by GC, the calculated throughput is `mutatorTime / (mutatorTime + gcTime)`, or equivalently, `1 - (gcTime / (mutatorTime + gcTime))`. The same formula is used regardless of whether the GC is major or minor.

  The disadvantage of this algorithm in a generational GC system is that the throughput numbers for major GCs tend to be a lot lower than for minor GCs (because major GCs are longer). This messes up the curve fitting done by the daemon, and can lead to poor results. Other ThroughputCalculator subclasses attempt to address this.

  For a system with only one kind of GC, however, this algorithm is ideal.

  To use, subclass, and implement getHeapSizeB() and logReading() as necessary for your VM/application. */
    class SimpleThroughput : public ThroughputCalculator
    {
    public:
        SimpleThroughput();

        virtual void gcBegin();
        virtual ThroughputCalculator::Result minorGCEnd();
        virtual ThroughputCalculator::Result majorGCEnd();

    protected:
        //! Return the current heap size in bytes.
        virtual unsigned long long getHeapSizeB() = 0;

        //! Use if you want to log the calculated throughputs in your client.
        virtual void logReading(unsigned long long heapSizeB, double throughput, unsigned long long gcTime, unsigned long long interGCTime) = 0;

    private:
        ThroughputCalculator::Result gcEnd();
        economem::Stopwatch m_GCTimer;
        economem::Stopwatch m_InterGCTimer;
        unsigned long long m_HeapSizeAtBeginCollectionB;
    };
}

/*! @} */

#endif
