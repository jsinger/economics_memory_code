#ifndef SIMPLESMOOTHINGTHROUGHPUT_HPP
#define SIMPLESMOOTHINGTHROUGHPUT_HPP

#include "Stopwatch.hpp"
#include "ThroughputCalculator.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
//! Throughput-calculation algorithm that attempts to smooth the difference between major and minor GCs.
/*! This algorithm applies the throughput definition to both major and minor GCs, but treats the two kinds of GCs differently when choosing the GC time and mutation time parameters to use with that definition.

  For minor GCs, GC time is the length of the minor GC, and mutation time is the length of the mutation period immediately preceding this minor GC.

  For major GCs, GC time is the total amount of time spent in GC since the end of the previous major GC, and mutation time is the total amount of time spent mutating since the end of the previous major GC.

  To use, subclass, and implement getHeapSizeB() and logReading() as necessary for your VM/application. */
    class SimpleSmoothingThroughput : public ThroughputCalculator
    {
    public:
        SimpleSmoothingThroughput();

        virtual void gcBegin();
        virtual ThroughputCalculator::Result minorGCEnd();
        virtual ThroughputCalculator::Result majorGCEnd();

    protected:
        //! Return the current heap size in bytes.
        virtual unsigned long long getHeapSizeB() = 0;

        //! Use if you want to log the calculated throughputs in your client.
        virtual void logReading(unsigned long long heapSizeB, double throughput, unsigned long long gcTime, unsigned long long interGCTime) = 0;

    private:
        economem::Stopwatch m_GCTimer;
        economem::Stopwatch m_InterGCTimer;
        unsigned long long m_TotalGCTimer;
        economem::Stopwatch m_InterMajorGCTimer;
        unsigned long long m_HeapSizeAtBeginCollectionB;
    };
}

/*! @} */

#endif
