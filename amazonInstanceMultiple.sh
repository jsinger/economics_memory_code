#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") keyfile instances_file"
    exit 1
}

test -z "${1}" && usage
test ! -f "${1}" && usage
test -z "${2}" && usage
test ! -f "${2}" && usage

KEYFILE="$(readlink -f "${1}")"
INSTANCES="$(readlink -f "${2}")"

exec 3<"${INSTANCES}"
while read -u 3 line; do
    "${DIR}/amazonInstanceSetup.sh" "${KEYFILE}" "${line}"
done
