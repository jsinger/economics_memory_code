#!/usr/bin/env python

import random
import sys

l = []

for line in sys.stdin:
    l.append(line.strip())

def makeBM(s):
    s2 = s.split(",")
    return (s2[0], s2[1], int(s2[2]), int(s2[3]))

data = []

for line1 in l:
    for line2 in l:
        bm1 = makeBM(line1)
        bm2 = makeBM(line2)
        if bm1 < bm2:
            x = (bm1, bm2, bm1[3] + bm2[3])
        else:
            x = (bm2, bm1, bm1[3] + bm2[3])

        if x not in data:
            data.append(x)

data.sort()


same = []
half = []

bad = ["gcbench", "jbb2005", "jbb2013c"]
i = 0
while i < len(data):
    if data[i][0][2] + data[i][1][2] > 4:
        del data[i]
    elif 6 * data[i][2] > 10000:
        del data[i]
    elif data[i][0][0] in bad or data[i][1][0] in bad:
        del data[i]
    elif data[i][0] == data[i][1]:
        same.append(data[i])
        del data[i]
    else:
        i += 1

def do(bm):
    return "%s:%s:%d:%d" % (bm[0], bm[1], bm[2], bm[3])

def show(l):
    for line in l:
        print "%s|%s" % (do(line[0]), do(line[1]))

out = random.sample(data, 16) + random.sample(same, 4)
out.sort()
show(out)
