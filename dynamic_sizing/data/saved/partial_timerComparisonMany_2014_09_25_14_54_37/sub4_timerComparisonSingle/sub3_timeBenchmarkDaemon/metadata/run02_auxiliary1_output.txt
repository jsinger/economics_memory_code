economemd
    total memory: 5948 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub4_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
ReadingThread: starting
TimerThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 7653: heap size 9 MB, throughput 0.991934
Clients: 1
Client 7653 has a minimum heap size of 1211 MB
Reading from 7654: heap size 9 MB, throughput 0.990083
Clients: 2
Client 7654 has a minimum heap size of 276 MB
Reading from 7654: heap size 9 MB, throughput 0.985911
Reading from 7653: heap size 9 MB, throughput 0.987384
Reading from 7653: heap size 9 MB, throughput 0.981641
Reading from 7654: heap size 9 MB, throughput 0.98023
Reading from 7654: heap size 9 MB, throughput 0.971086
Reading from 7653: heap size 9 MB, throughput 0.974568
Reading from 7654: heap size 11 MB, throughput 0.981691
Reading from 7653: heap size 11 MB, throughput 0.976265
Reading from 7654: heap size 11 MB, throughput 0.980902
Reading from 7653: heap size 11 MB, throughput 0.981078
Reading from 7653: heap size 17 MB, throughput 0.973293
Reading from 7654: heap size 17 MB, throughput 0.969876
Reading from 7653: heap size 17 MB, throughput 0.875677
Reading from 7654: heap size 17 MB, throughput 0.909969
Reading from 7654: heap size 30 MB, throughput 0.83961
Reading from 7653: heap size 30 MB, throughput 0.764594
Reading from 7654: heap size 31 MB, throughput 0.567672
Reading from 7653: heap size 31 MB, throughput 0.210217
Reading from 7654: heap size 35 MB, throughput 0.686997
Reading from 7653: heap size 35 MB, throughput 0.632584
Reading from 7654: heap size 47 MB, throughput 0.437934
Reading from 7653: heap size 46 MB, throughput 0.273986
Reading from 7654: heap size 49 MB, throughput 0.466147
Reading from 7653: heap size 50 MB, throughput 0.353323
Reading from 7654: heap size 65 MB, throughput 0.388753
Reading from 7653: heap size 63 MB, throughput 0.175761
Reading from 7654: heap size 69 MB, throughput 0.368874
Reading from 7653: heap size 67 MB, throughput 0.375363
Reading from 7653: heap size 85 MB, throughput 0.206745
Reading from 7654: heap size 71 MB, throughput 0.400642
Reading from 7653: heap size 93 MB, throughput 0.277982
Reading from 7654: heap size 100 MB, throughput 0.474314
Reading from 7654: heap size 100 MB, throughput 0.352817
Reading from 7653: heap size 93 MB, throughput 0.428541
Reading from 7654: heap size 133 MB, throughput 0.67304
Reading from 7653: heap size 127 MB, throughput 0.495876
Reading from 7654: heap size 132 MB, throughput 0.639623
Reading from 7653: heap size 127 MB, throughput 0.232392
Reading from 7654: heap size 134 MB, throughput 0.363218
Reading from 7654: heap size 137 MB, throughput 0.313441
Reading from 7653: heap size 129 MB, throughput 0.392474
Reading from 7653: heap size 161 MB, throughput 0.27651
Reading from 7654: heap size 140 MB, throughput 0.495668
Reading from 7653: heap size 166 MB, throughput 0.304591
Reading from 7654: heap size 177 MB, throughput 0.451971
Reading from 7653: heap size 167 MB, throughput 0.765213
Reading from 7654: heap size 181 MB, throughput 0.370759
Reading from 7653: heap size 172 MB, throughput 0.764002
Reading from 7654: heap size 184 MB, throughput 0.0192758
Reading from 7654: heap size 191 MB, throughput 0.749846
Reading from 7653: heap size 176 MB, throughput 0.483929
Reading from 7653: heap size 217 MB, throughput 0.72722
Reading from 7654: heap size 194 MB, throughput 0.883539
Reading from 7653: heap size 220 MB, throughput 0.743113
Reading from 7653: heap size 221 MB, throughput 0.715952
Reading from 7653: heap size 228 MB, throughput 0.664916
Reading from 7653: heap size 238 MB, throughput 0.66815
Reading from 7653: heap size 242 MB, throughput 0.66959
Reading from 7653: heap size 246 MB, throughput 0.676797
Reading from 7654: heap size 196 MB, throughput 0.598117
Reading from 7653: heap size 254 MB, throughput 0.680398
Reading from 7653: heap size 258 MB, throughput 0.602124
Reading from 7654: heap size 249 MB, throughput 0.816187
Reading from 7654: heap size 254 MB, throughput 0.782293
Reading from 7653: heap size 269 MB, throughput 0.566798
Reading from 7654: heap size 255 MB, throughput 0.793225
Reading from 7654: heap size 260 MB, throughput 0.760547
Reading from 7654: heap size 261 MB, throughput 0.773819
Reading from 7653: heap size 274 MB, throughput 0.494882
Reading from 7654: heap size 266 MB, throughput 0.816017
Reading from 7653: heap size 323 MB, throughput 0.467777
Reading from 7654: heap size 267 MB, throughput 0.774107
Reading from 7653: heap size 331 MB, throughput 0.465582
Reading from 7654: heap size 268 MB, throughput 0.704171
Reading from 7654: heap size 272 MB, throughput 0.63853
Reading from 7654: heap size 278 MB, throughput 0.640255
Reading from 7654: heap size 281 MB, throughput 0.616985
Reading from 7653: heap size 335 MB, throughput 0.177966
Reading from 7653: heap size 379 MB, throughput 0.466453
Reading from 7653: heap size 301 MB, throughput 0.554513
Reading from 7654: heap size 286 MB, throughput 0.847708
Reading from 7653: heap size 367 MB, throughput 0.620281
Reading from 7653: heap size 373 MB, throughput 0.622895
Reading from 7653: heap size 372 MB, throughput 0.620134
Reading from 7654: heap size 288 MB, throughput 0.885287
Reading from 7653: heap size 373 MB, throughput 0.560009
Reading from 7654: heap size 289 MB, throughput 0.823669
Reading from 7653: heap size 374 MB, throughput 0.276172
Reading from 7654: heap size 290 MB, throughput 0.553492
Reading from 7653: heap size 428 MB, throughput 0.558289
Reading from 7653: heap size 426 MB, throughput 0.539021
Reading from 7654: heap size 337 MB, throughput 0.674513
Reading from 7653: heap size 431 MB, throughput 0.530281
Reading from 7654: heap size 338 MB, throughput 0.809688
Reading from 7653: heap size 433 MB, throughput 0.515162
Reading from 7654: heap size 342 MB, throughput 0.833348
Reading from 7653: heap size 434 MB, throughput 0.586921
Reading from 7654: heap size 344 MB, throughput 0.750689
Reading from 7653: heap size 438 MB, throughput 0.546306
Reading from 7654: heap size 349 MB, throughput 0.754512
Reading from 7653: heap size 441 MB, throughput 0.527744
Reading from 7654: heap size 349 MB, throughput 0.662539
Reading from 7654: heap size 355 MB, throughput 0.650994
Reading from 7653: heap size 444 MB, throughput 0.508496
Reading from 7654: heap size 356 MB, throughput 0.710568
Reading from 7654: heap size 364 MB, throughput 0.685769
Numeric result:
Recommendation: 2 clients, utility 0.320364:
    h1: 1854.35 MB (U(h) = 0.531476*h^0.001)
    h2: 4093.65 MB (U(h) = 0.587536*h^0.00217498)
Recommendation: 2 clients, utility 0.320364:
    h1: 1873.4 MB (U(h) = 0.531476*h^0.001)
    h2: 4074.6 MB (U(h) = 0.587536*h^0.00217498)
Reading from 7653: heap size 449 MB, throughput 0.314123
Reading from 7653: heap size 499 MB, throughput 0.420818
Reading from 7653: heap size 506 MB, throughput 0.434326
Reading from 7653: heap size 510 MB, throughput 0.458797
Reading from 7653: heap size 513 MB, throughput 0.434321
Reading from 7654: heap size 365 MB, throughput 0.948061
Reading from 7653: heap size 518 MB, throughput 0.229383
Reading from 7653: heap size 572 MB, throughput 0.393777
Reading from 7653: heap size 575 MB, throughput 0.496245
Reading from 7654: heap size 367 MB, throughput 0.945442
Reading from 7653: heap size 577 MB, throughput 0.202452
Reading from 7653: heap size 639 MB, throughput 0.438588
Reading from 7653: heap size 640 MB, throughput 0.510398
Reading from 7654: heap size 370 MB, throughput 0.962777
Reading from 7653: heap size 638 MB, throughput 0.554878
Reading from 7653: heap size 639 MB, throughput 0.565677
Reading from 7653: heap size 641 MB, throughput 0.743229
Reading from 7654: heap size 371 MB, throughput 0.96966
Reading from 7653: heap size 649 MB, throughput 0.796086
Reading from 7653: heap size 650 MB, throughput 0.825939
Reading from 7654: heap size 374 MB, throughput 0.965832
Reading from 7653: heap size 666 MB, throughput 0.475964
Reading from 7653: heap size 748 MB, throughput 0.612786
Reading from 7654: heap size 374 MB, throughput 0.966611
Reading from 7653: heap size 756 MB, throughput 0.66145
Reading from 7653: heap size 762 MB, throughput 0.56284
Reading from 7653: heap size 762 MB, throughput 0.493422
Reading from 7653: heap size 753 MB, throughput 0.460383
Reading from 7653: heap size 683 MB, throughput 0.474291
Numeric result:
Recommendation: 2 clients, utility 0.383052:
    h1: 1211 MB (U(h) = 0.525061*h^0.001)
    h2: 4737 MB (U(h) = 0.495667*h^0.0448307)
Recommendation: 2 clients, utility 0.383052:
    h1: 1211 MB (U(h) = 0.525061*h^0.001)
    h2: 4737 MB (U(h) = 0.495667*h^0.0448307)
Reading from 7654: heap size 376 MB, throughput 0.962309
Reading from 7653: heap size 749 MB, throughput 0.304696
Reading from 7653: heap size 820 MB, throughput 0.630712
Reading from 7653: heap size 821 MB, throughput 0.622315
Reading from 7653: heap size 821 MB, throughput 0.556246
Reading from 7654: heap size 374 MB, throughput 0.963288
Reading from 7653: heap size 827 MB, throughput 0.709892
Reading from 7653: heap size 828 MB, throughput 0.35882
Reading from 7653: heap size 909 MB, throughput 0.670769
Reading from 7653: heap size 912 MB, throughput 0.783298
Reading from 7654: heap size 377 MB, throughput 0.965088
Reading from 7653: heap size 919 MB, throughput 0.811109
Reading from 7653: heap size 923 MB, throughput 0.885362
Reading from 7653: heap size 914 MB, throughput 0.859603
Reading from 7653: heap size 782 MB, throughput 0.831061
Reading from 7653: heap size 904 MB, throughput 0.814712
Reading from 7653: heap size 788 MB, throughput 0.798239
Reading from 7653: heap size 895 MB, throughput 0.781243
Reading from 7654: heap size 379 MB, throughput 0.960059
Reading from 7653: heap size 794 MB, throughput 0.771839
Reading from 7653: heap size 892 MB, throughput 0.789555
Reading from 7653: heap size 897 MB, throughput 0.807281
Reading from 7653: heap size 888 MB, throughput 0.822586
Reading from 7653: heap size 893 MB, throughput 0.830121
Reading from 7654: heap size 380 MB, throughput 0.966978
Reading from 7653: heap size 887 MB, throughput 0.952528
Reading from 7653: heap size 891 MB, throughput 0.949497
Reading from 7654: heap size 383 MB, throughput 0.962256
Reading from 7653: heap size 891 MB, throughput 0.91973
Reading from 7653: heap size 894 MB, throughput 0.875649
Reading from 7653: heap size 891 MB, throughput 0.831805
Reading from 7653: heap size 894 MB, throughput 0.833093
Reading from 7653: heap size 892 MB, throughput 0.82693
Reading from 7653: heap size 895 MB, throughput 0.821044
Reading from 7653: heap size 895 MB, throughput 0.834281
Reading from 7654: heap size 384 MB, throughput 0.970047
Numeric result:
Recommendation: 2 clients, utility 0.523418:
    h1: 2867.66 MB (U(h) = 0.348197*h^0.0757185)
    h2: 3080.34 MB (U(h) = 0.427943*h^0.0813619)
Recommendation: 2 clients, utility 0.523418:
    h1: 2867.15 MB (U(h) = 0.348197*h^0.0757185)
    h2: 3080.85 MB (U(h) = 0.427943*h^0.0813619)
Reading from 7653: heap size 898 MB, throughput 0.876849
Reading from 7653: heap size 902 MB, throughput 0.886255
Reading from 7653: heap size 903 MB, throughput 0.907692
Reading from 7653: heap size 902 MB, throughput 0.863993
Reading from 7654: heap size 387 MB, throughput 0.97531
Reading from 7654: heap size 389 MB, throughput 0.954878
Reading from 7654: heap size 384 MB, throughput 0.935524
Reading from 7654: heap size 389 MB, throughput 0.892469
Reading from 7654: heap size 396 MB, throughput 0.851643
Reading from 7653: heap size 905 MB, throughput 0.659774
Reading from 7654: heap size 399 MB, throughput 0.916303
Reading from 7653: heap size 976 MB, throughput 0.641613
Reading from 7653: heap size 997 MB, throughput 0.673162
Reading from 7653: heap size 1005 MB, throughput 0.677385
Reading from 7653: heap size 1005 MB, throughput 0.691859
Reading from 7653: heap size 1003 MB, throughput 0.708909
Reading from 7653: heap size 1007 MB, throughput 0.70167
Reading from 7653: heap size 1016 MB, throughput 0.710228
Reading from 7653: heap size 1017 MB, throughput 0.676719
Reading from 7653: heap size 1031 MB, throughput 0.67599
Reading from 7653: heap size 1031 MB, throughput 0.67258
Reading from 7654: heap size 406 MB, throughput 0.967458
Reading from 7653: heap size 1046 MB, throughput 0.912739
Reading from 7654: heap size 408 MB, throughput 0.979626
Reading from 7653: heap size 1050 MB, throughput 0.934823
Reading from 7654: heap size 410 MB, throughput 0.983469
Numeric result:
Recommendation: 2 clients, utility 0.869951:
    h1: 4209.82 MB (U(h) = 0.110639*h^0.264794)
    h2: 1738.18 MB (U(h) = 0.381642*h^0.109332)
Recommendation: 2 clients, utility 0.869951:
    h1: 4209.79 MB (U(h) = 0.110639*h^0.264794)
    h2: 1738.21 MB (U(h) = 0.381642*h^0.109332)
Reading from 7653: heap size 1066 MB, throughput 0.954787
Reading from 7654: heap size 412 MB, throughput 0.986467
Reading from 7653: heap size 1068 MB, throughput 0.953725
Reading from 7653: heap size 1070 MB, throughput 0.946186
Reading from 7654: heap size 411 MB, throughput 0.98256
Reading from 7653: heap size 1075 MB, throughput 0.947657
Reading from 7654: heap size 414 MB, throughput 0.983788
Reading from 7653: heap size 1069 MB, throughput 0.956101
Reading from 7654: heap size 411 MB, throughput 0.983166
Reading from 7653: heap size 1075 MB, throughput 0.95467
Reading from 7654: heap size 413 MB, throughput 0.982741
Numeric result:
Recommendation: 2 clients, utility 0.983257:
    h1: 4177.53 MB (U(h) = 0.0946143*h^0.292409)
    h2: 1770.47 MB (U(h) = 0.359266*h^0.12392)
Recommendation: 2 clients, utility 0.983257:
    h1: 4177.58 MB (U(h) = 0.0946143*h^0.292409)
    h2: 1770.42 MB (U(h) = 0.359266*h^0.12392)
Reading from 7653: heap size 1075 MB, throughput 0.958981
Reading from 7654: heap size 413 MB, throughput 0.982256
Reading from 7653: heap size 1078 MB, throughput 0.955776
Reading from 7654: heap size 414 MB, throughput 0.98042
Reading from 7654: heap size 414 MB, throughput 0.981996
Reading from 7654: heap size 416 MB, throughput 0.969007
Reading from 7653: heap size 1083 MB, throughput 0.956315
Reading from 7654: heap size 418 MB, throughput 0.95567
Reading from 7654: heap size 419 MB, throughput 0.944005
Reading from 7653: heap size 1085 MB, throughput 0.95204
Reading from 7654: heap size 425 MB, throughput 0.980458
Reading from 7653: heap size 1091 MB, throughput 0.952898
Reading from 7654: heap size 425 MB, throughput 0.985842
Numeric result:
Recommendation: 2 clients, utility 1.0303:
    h1: 4055.56 MB (U(h) = 0.0978857*h^0.289449)
    h2: 1892.44 MB (U(h) = 0.342907*h^0.135099)
Recommendation: 2 clients, utility 1.0303:
    h1: 4055.23 MB (U(h) = 0.0978857*h^0.289449)
    h2: 1892.77 MB (U(h) = 0.342907*h^0.135099)
Reading from 7653: heap size 1094 MB, throughput 0.950747
Reading from 7654: heap size 427 MB, throughput 0.988553
Reading from 7653: heap size 1100 MB, throughput 0.951684
Reading from 7654: heap size 429 MB, throughput 0.987582
Reading from 7653: heap size 1104 MB, throughput 0.952984
Reading from 7654: heap size 427 MB, throughput 0.987513
Reading from 7654: heap size 429 MB, throughput 0.975744
Reading from 7653: heap size 1111 MB, throughput 0.884704
Numeric result:
Recommendation: 2 clients, utility 1.07755:
    h1: 4039.32 MB (U(h) = 0.0940156*h^0.297491)
    h2: 1908.68 MB (U(h) = 0.335089*h^0.140588)
Recommendation: 2 clients, utility 1.07755:
    h1: 4039.17 MB (U(h) = 0.0940156*h^0.297491)
    h2: 1908.83 MB (U(h) = 0.335089*h^0.140588)
Reading from 7654: heap size 427 MB, throughput 0.960241
Reading from 7653: heap size 1204 MB, throughput 0.939347
Reading from 7654: heap size 460 MB, throughput 0.985208
Reading from 7653: heap size 1213 MB, throughput 0.957779
Reading from 7654: heap size 459 MB, throughput 0.988613
Reading from 7653: heap size 1214 MB, throughput 0.96285
Reading from 7654: heap size 462 MB, throughput 0.988607
Reading from 7654: heap size 460 MB, throughput 0.980226
Reading from 7654: heap size 463 MB, throughput 0.962724
Reading from 7654: heap size 469 MB, throughput 0.957364
Reading from 7653: heap size 1217 MB, throughput 0.966944
Reading from 7654: heap size 471 MB, throughput 0.984259
Reading from 7653: heap size 1219 MB, throughput 0.967227
Numeric result:
Recommendation: 2 clients, utility 1.43159:
    h1: 4385.7 MB (U(h) = 0.0382899*h^0.433875)
    h2: 1562.3 MB (U(h) = 0.315448*h^0.154557)
Recommendation: 2 clients, utility 1.43159:
    h1: 4385.7 MB (U(h) = 0.0382899*h^0.433875)
    h2: 1562.3 MB (U(h) = 0.315448*h^0.154557)
Reading from 7654: heap size 473 MB, throughput 0.987998
Reading from 7653: heap size 1213 MB, throughput 0.966296
Reading from 7654: heap size 475 MB, throughput 0.988456
Reading from 7653: heap size 1218 MB, throughput 0.965251
Reading from 7654: heap size 472 MB, throughput 0.98791
Reading from 7653: heap size 1210 MB, throughput 0.969381
Reading from 7654: heap size 475 MB, throughput 0.988186
Reading from 7653: heap size 1214 MB, throughput 0.970574
Numeric result:
Recommendation: 2 clients, utility 1.62172:
    h1: 4489.49 MB (U(h) = 0.0254853*h^0.494989)
    h2: 1458.51 MB (U(h) = 0.306984*h^0.160805)
Recommendation: 2 clients, utility 1.62172:
    h1: 4489.51 MB (U(h) = 0.0254853*h^0.494989)
    h2: 1458.49 MB (U(h) = 0.306984*h^0.160805)
Reading from 7654: heap size 477 MB, throughput 0.98749
Reading from 7653: heap size 1219 MB, throughput 0.965881
Reading from 7654: heap size 478 MB, throughput 0.98582
Reading from 7653: heap size 1219 MB, throughput 0.960874
Reading from 7654: heap size 480 MB, throughput 0.984801
Reading from 7653: heap size 1224 MB, throughput 0.959117
Reading from 7654: heap size 482 MB, throughput 0.98612
Reading from 7654: heap size 484 MB, throughput 0.978131
Reading from 7654: heap size 485 MB, throughput 0.964349
Reading from 7653: heap size 1227 MB, throughput 0.956947
Reading from 7654: heap size 492 MB, throughput 0.978001
Reading from 7653: heap size 1233 MB, throughput 0.960887
Numeric result:
Recommendation: 2 clients, utility 2.01929:
    h1: 4627.45 MB (U(h) = 0.012237*h^0.604161)
    h2: 1320.55 MB (U(h) = 0.291782*h^0.172397)
Recommendation: 2 clients, utility 2.01929:
    h1: 4627.54 MB (U(h) = 0.012237*h^0.604161)
    h2: 1320.46 MB (U(h) = 0.291782*h^0.172397)
Reading from 7654: heap size 494 MB, throughput 0.98687
Reading from 7653: heap size 1239 MB, throughput 0.96167
Reading from 7654: heap size 496 MB, throughput 0.98832
Reading from 7653: heap size 1245 MB, throughput 0.956836
Reading from 7654: heap size 498 MB, throughput 0.988803
Reading from 7653: heap size 1253 MB, throughput 0.954549
Reading from 7654: heap size 495 MB, throughput 0.988229
Reading from 7653: heap size 1261 MB, throughput 0.953794
Numeric result:
Recommendation: 2 clients, utility 2.46482:
    h1: 4759.68 MB (U(h) = 0.0058371*h^0.71346)
    h2: 1188.32 MB (U(h) = 0.284469*h^0.178133)
Recommendation: 2 clients, utility 2.46482:
    h1: 4759.64 MB (U(h) = 0.0058371*h^0.71346)
    h2: 1188.36 MB (U(h) = 0.284469*h^0.178133)
Reading from 7654: heap size 498 MB, throughput 0.986932
Reading from 7653: heap size 1266 MB, throughput 0.953069
Reading from 7654: heap size 499 MB, throughput 0.986641
Reading from 7653: heap size 1274 MB, throughput 0.952763
Reading from 7654: heap size 500 MB, throughput 0.988612
Reading from 7654: heap size 500 MB, throughput 0.984661
Reading from 7654: heap size 502 MB, throughput 0.974369
Reading from 7654: heap size 506 MB, throughput 0.973883
Reading from 7653: heap size 1277 MB, throughput 0.921513
Numeric result:
Recommendation: 2 clients, utility 2.39421:
    h1: 4699.4 MB (U(h) = 0.00693387*h^0.688194)
    h2: 1248.6 MB (U(h) = 0.278568*h^0.182849)
Recommendation: 2 clients, utility 2.39421:
    h1: 4699.4 MB (U(h) = 0.00693387*h^0.688194)
    h2: 1248.6 MB (U(h) = 0.278568*h^0.182849)
Reading from 7654: heap size 508 MB, throughput 0.989758
Reading from 7653: heap size 1389 MB, throughput 0.944219
Reading from 7654: heap size 512 MB, throughput 0.991015
Reading from 7653: heap size 1390 MB, throughput 0.957315
Reading from 7654: heap size 513 MB, throughput 0.99032
Reading from 7653: heap size 1398 MB, throughput 0.963435
Reading from 7654: heap size 512 MB, throughput 0.989109
Numeric result:
Recommendation: 2 clients, utility 2.96599:
    h1: 4571.78 MB (U(h) = 0.00372511*h^0.778212)
    h2: 1376.22 MB (U(h) = 0.207704*h^0.234259)
Recommendation: 2 clients, utility 2.96599:
    h1: 4571.79 MB (U(h) = 0.00372511*h^0.778212)
    h2: 1376.21 MB (U(h) = 0.207704*h^0.234259)
Reading from 7653: heap size 1400 MB, throughput 0.966251
Reading from 7654: heap size 514 MB, throughput 0.988441
Reading from 7654: heap size 515 MB, throughput 0.988005
Reading from 7654: heap size 516 MB, throughput 0.9892
Reading from 7654: heap size 519 MB, throughput 0.983551
Reading from 7654: heap size 519 MB, throughput 0.97218
Reading from 7654: heap size 525 MB, throughput 0.983414
Numeric result:
Recommendation: 2 clients, utility 5.1554:
    h1: 3571.75 MB (U(h) = 0.00303781*h^0.807622)
    h2: 2376.25 MB (U(h) = 0.0351806*h^0.537343)
Recommendation: 2 clients, utility 5.1554:
    h1: 3571.64 MB (U(h) = 0.00303781*h^0.807622)
    h2: 2376.36 MB (U(h) = 0.0351806*h^0.537343)
Reading from 7654: heap size 527 MB, throughput 0.990059
Reading from 7653: heap size 1398 MB, throughput 0.983464
Reading from 7654: heap size 529 MB, throughput 0.990098
Reading from 7654: heap size 531 MB, throughput 0.990053
Reading from 7654: heap size 530 MB, throughput 0.989351
Numeric result:
Recommendation: 2 clients, utility 6.60168:
    h1: 3254.38 MB (U(h) = 0.00302919*h^0.808058)
    h2: 2693.62 MB (U(h) = 0.0160607*h^0.668822)
Recommendation: 2 clients, utility 6.60168:
    h1: 3254.38 MB (U(h) = 0.00302919*h^0.808058)
    h2: 2693.62 MB (U(h) = 0.0160607*h^0.668822)
Reading from 7654: heap size 532 MB, throughput 0.989396
Reading from 7653: heap size 1402 MB, throughput 0.985913
Reading from 7654: heap size 535 MB, throughput 0.990318
Reading from 7654: heap size 535 MB, throughput 0.985732
Reading from 7654: heap size 534 MB, throughput 0.977392
Reading from 7654: heap size 537 MB, throughput 0.984617
Reading from 7653: heap size 1405 MB, throughput 0.984136
Numeric result:
Recommendation: 2 clients, utility 8.64947:
    h1: 2947.73 MB (U(h) = 0.00316007*h^0.801538)
    h2: 3000.27 MB (U(h) = 0.00660092*h^0.815827)
Recommendation: 2 clients, utility 8.64947:
    h1: 2947.73 MB (U(h) = 0.00316007*h^0.801538)
    h2: 3000.27 MB (U(h) = 0.00660092*h^0.815827)
Reading from 7653: heap size 1434 MB, throughput 0.970648
Reading from 7653: heap size 1424 MB, throughput 0.950642
Reading from 7653: heap size 1459 MB, throughput 0.911962
Reading from 7653: heap size 1484 MB, throughput 0.8654
Reading from 7654: heap size 544 MB, throughput 0.982037
Reading from 7653: heap size 1502 MB, throughput 0.793102
Reading from 7653: heap size 1534 MB, throughput 0.756118
Reading from 7653: heap size 1543 MB, throughput 0.75004
Reading from 7653: heap size 1555 MB, throughput 0.916136
Reading from 7654: heap size 545 MB, throughput 0.987489
Reading from 7653: heap size 1569 MB, throughput 0.950743
Reading from 7654: heap size 546 MB, throughput 0.988571
Numeric result:
Recommendation: 2 clients, utility 8.61219:
    h1: 3000.48 MB (U(h) = 0.00246574*h^0.832263)
    h2: 2947.52 MB (U(h) = 0.00649702*h^0.817567)
Recommendation: 2 clients, utility 8.61219:
    h1: 3000.49 MB (U(h) = 0.00246574*h^0.832263)
    h2: 2947.51 MB (U(h) = 0.00649702*h^0.817567)
Reading from 7653: heap size 1556 MB, throughput 0.94751
Reading from 7654: heap size 548 MB, throughput 0.989207
Reading from 7653: heap size 1541 MB, throughput 0.977187
Reading from 7654: heap size 549 MB, throughput 0.988536
Reading from 7654: heap size 550 MB, throughput 0.989012
Reading from 7653: heap size 1544 MB, throughput 0.983425
Reading from 7654: heap size 552 MB, throughput 0.983177
Reading from 7654: heap size 554 MB, throughput 0.983776
Reading from 7653: heap size 1557 MB, throughput 0.984821
Numeric result:
Recommendation: 2 clients, utility 3.96947:
    h1: 3690.04 MB (U(h) = 0.00480501*h^0.736179)
    h2: 2257.96 MB (U(h) = 0.0603028*h^0.450464)
Recommendation: 2 clients, utility 3.96947:
    h1: 3690.07 MB (U(h) = 0.00480501*h^0.736179)
    h2: 2257.93 MB (U(h) = 0.0603028*h^0.450464)
Reading from 7654: heap size 562 MB, throughput 0.990491
Reading from 7653: heap size 1580 MB, throughput 0.983958
Reading from 7654: heap size 562 MB, throughput 0.990636
Reading from 7653: heap size 1583 MB, throughput 0.985319
Reading from 7654: heap size 562 MB, throughput 0.990469
Reading from 7653: heap size 1576 MB, throughput 0.981433
Numeric result:
Recommendation: 2 clients, utility 3.31506:
    h1: 3450.29 MB (U(h) = 0.0123695*h^0.601705)
    h2: 2497.71 MB (U(h) = 0.0659828*h^0.435599)
Recommendation: 2 clients, utility 3.31506:
    h1: 3450.23 MB (U(h) = 0.0123695*h^0.601705)
    h2: 2497.77 MB (U(h) = 0.0659828*h^0.435599)
Reading from 7654: heap size 564 MB, throughput 0.989575
Reading from 7653: heap size 1586 MB, throughput 0.980793
Reading from 7654: heap size 564 MB, throughput 0.989134
Reading from 7653: heap size 1562 MB, throughput 0.977324
Reading from 7654: heap size 565 MB, throughput 0.985955
Reading from 7654: heap size 568 MB, throughput 0.980579
Reading from 7654: heap size 570 MB, throughput 0.983328
Reading from 7653: heap size 1388 MB, throughput 0.975398
Numeric result:
Recommendation: 2 clients, utility 3.24738:
    h1: 3282.16 MB (U(h) = 0.0168806*h^0.557677)
    h2: 2665.84 MB (U(h) = 0.0590901*h^0.452955)
Recommendation: 2 clients, utility 3.24738:
    h1: 3282.17 MB (U(h) = 0.0168806*h^0.557677)
    h2: 2665.83 MB (U(h) = 0.0590901*h^0.452955)
Reading from 7654: heap size 576 MB, throughput 0.990595
Reading from 7653: heap size 1542 MB, throughput 0.975763
Reading from 7654: heap size 577 MB, throughput 0.990837
Reading from 7653: heap size 1558 MB, throughput 0.972672
Reading from 7654: heap size 577 MB, throughput 0.991
Reading from 7653: heap size 1547 MB, throughput 0.970316
Numeric result:
Recommendation: 2 clients, utility 3.49243:
    h1: 3399.88 MB (U(h) = 0.010996*h^0.617615)
    h2: 2548.12 MB (U(h) = 0.0554762*h^0.462889)
Recommendation: 2 clients, utility 3.49243:
    h1: 3399.87 MB (U(h) = 0.010996*h^0.617615)
    h2: 2548.13 MB (U(h) = 0.0554762*h^0.462889)
Reading from 7654: heap size 579 MB, throughput 0.990055
Reading from 7653: heap size 1553 MB, throughput 0.96881
Reading from 7654: heap size 581 MB, throughput 0.992004
Reading from 7654: heap size 581 MB, throughput 0.988637
Reading from 7654: heap size 582 MB, throughput 0.982278
Reading from 7653: heap size 1562 MB, throughput 0.966869
Reading from 7654: heap size 584 MB, throughput 0.9892
Numeric result:
Recommendation: 2 clients, utility 3.05242:
    h1: 3542.65 MB (U(h) = 0.0133597*h^0.590145)
    h2: 2405.35 MB (U(h) = 0.081177*h^0.400688)
Recommendation: 2 clients, utility 3.05242:
    h1: 3542.66 MB (U(h) = 0.0133597*h^0.590145)
    h2: 2405.34 MB (U(h) = 0.081177*h^0.400688)
Reading from 7653: heap size 1564 MB, throughput 0.963299
Reading from 7654: heap size 590 MB, throughput 0.991289
Reading from 7653: heap size 1576 MB, throughput 0.963166
Reading from 7654: heap size 591 MB, throughput 0.991297
Reading from 7653: heap size 1583 MB, throughput 0.961879
Reading from 7654: heap size 589 MB, throughput 0.991136
Numeric result:
Recommendation: 2 clients, utility 2.91508:
    h1: 3586.4 MB (U(h) = 0.0144475*h^0.579003)
    h2: 2361.6 MB (U(h) = 0.0913312*h^0.381264)
Recommendation: 2 clients, utility 2.91508:
    h1: 3586.41 MB (U(h) = 0.0144475*h^0.579003)
    h2: 2361.59 MB (U(h) = 0.0913312*h^0.381264)
Reading from 7653: heap size 1599 MB, throughput 0.962054
Reading from 7654: heap size 592 MB, throughput 0.989916
Reading from 7654: heap size 595 MB, throughput 0.989873
Reading from 7654: heap size 596 MB, throughput 0.984332
Reading from 7653: heap size 1605 MB, throughput 0.959957
Reading from 7654: heap size 600 MB, throughput 0.989058
Numeric result:
Recommendation: 2 clients, utility 2.38411:
    h1: 3994.98 MB (U(h) = 0.0177707*h^0.549763)
    h2: 1953.02 MB (U(h) = 0.1833*h^0.26878)
Recommendation: 2 clients, utility 2.38411:
    h1: 3994.89 MB (U(h) = 0.0177707*h^0.549763)
    h2: 1953.11 MB (U(h) = 0.1833*h^0.26878)
Reading from 7653: heap size 1622 MB, throughput 0.963245
Reading from 7654: heap size 601 MB, throughput 0.991609
Reading from 7653: heap size 1625 MB, throughput 0.962432
Reading from 7654: heap size 604 MB, throughput 0.991794
Reading from 7653: heap size 1643 MB, throughput 0.961861
Reading from 7654: heap size 605 MB, throughput 0.991229
Numeric result:
Recommendation: 2 clients, utility 1.82725:
    h1: 3942.7 MB (U(h) = 0.0532786*h^0.396212)
    h2: 2005.3 MB (U(h) = 0.278666*h^0.201523)
Recommendation: 2 clients, utility 1.82725:
    h1: 3942.66 MB (U(h) = 0.0532786*h^0.396212)
    h2: 2005.34 MB (U(h) = 0.278666*h^0.201523)
Reading from 7653: heap size 1645 MB, throughput 0.961936
Reading from 7654: heap size 606 MB, throughput 0.990671
Reading from 7654: heap size 607 MB, throughput 0.989915
Reading from 7654: heap size 609 MB, throughput 0.98396
Reading from 7653: heap size 1663 MB, throughput 0.961645
Reading from 7654: heap size 611 MB, throughput 0.988661
Numeric result:
Recommendation: 2 clients, utility 1.52102:
    h1: 4901.14 MB (U(h) = 0.0704357*h^0.357297)
    h2: 1046.86 MB (U(h) = 0.609992*h^0.0763228)
Recommendation: 2 clients, utility 1.52102:
    h1: 4901.07 MB (U(h) = 0.0704357*h^0.357297)
    h2: 1046.93 MB (U(h) = 0.609992*h^0.0763228)
Reading from 7654: heap size 618 MB, throughput 0.992217
Reading from 7654: heap size 618 MB, throughput 0.991486
Reading from 7654: heap size 618 MB, throughput 0.99136
Numeric result:
Recommendation: 2 clients, utility 1.51878:
    h1: 4924.97 MB (U(h) = 0.0704357*h^0.357297)
    h2: 1023.03 MB (U(h) = 0.618055*h^0.0742189)
Recommendation: 2 clients, utility 1.51878:
    h1: 4924.97 MB (U(h) = 0.0704357*h^0.357297)
    h2: 1023.03 MB (U(h) = 0.618055*h^0.0742189)
Reading from 7654: heap size 620 MB, throughput 0.990936
Reading from 7654: heap size 621 MB, throughput 0.986882
Client 7654 died
Clients: 1
Reading from 7653: heap size 1664 MB, throughput 0.951824
Recommendation: one client; give it all the memory
Reading from 7653: heap size 1808 MB, throughput 0.986107
Reading from 7653: heap size 1824 MB, throughput 0.980528
Reading from 7653: heap size 1829 MB, throughput 0.970331
Reading from 7653: heap size 1831 MB, throughput 0.954069
Reading from 7653: heap size 1868 MB, throughput 0.942124
Client 7653 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
