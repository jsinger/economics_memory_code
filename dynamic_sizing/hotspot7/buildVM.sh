#!/bin/bash

export ECONOMEM_LIB_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ICEDTEA_BUILD_ROOT="${ECONOMEM_LIB_DIR}/icedtea7/icedtea-build"

if [ ! -e "${ICEDTEA_BUILD_ROOT}" ]; then
    echo 'Icedtea not set up'
    exit 1
fi

export LD_LIBRARY_PATH="${LD_LIBRARY_PATH}:${ECONOMEM_LIB_DIR}"

function stamp()
{
    REALFILE="$(readlink -f "${1}")"
    SYMLINK="$(realpath -s "${2}")"

    if [ ! -e "${REALFILE}.stamp" ]; then
        stat -c '%Y' "${REALFILE}" > "${REALFILE}.stamp"
        touch "${SYMLINK}"
    else
        STAMP="$(cat "${REALFILE}.stamp")"
        TIME="$(stat -c '%Y' "${REALFILE}")"

        if (( STAMP < TIME )); then
            echo "${TIME}" > "${REALFILE}.stamp"
            touch "${SYMLINK}"
        fi
    fi
}

stamp "${ECONOMEM_LIB_DIR}/vm.make" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/make/linux/makefiles/vm.make"
stamp "${ECONOMEM_LIB_DIR}/launcher.make" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/make/linux/makefiles/launcher.make"
stamp "${ECONOMEM_LIB_DIR}/buildtree.make" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/make/linux/makefiles/buildtree.make"
stamp "${ECONOMEM_LIB_DIR}/gcc.make" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/make/linux/makefiles/gcc.make"
stamp "${ECONOMEM_LIB_DIR}/psAdaptiveSizePolicy.cpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/parallelScavenge/psAdaptiveSizePolicy.cpp"
stamp "${ECONOMEM_LIB_DIR}/psAdaptiveSizePolicy.hpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/parallelScavenge/psAdaptiveSizePolicy.hpp"
stamp "${ECONOMEM_LIB_DIR}/adaptiveSizePolicy.hpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/shared/adaptiveSizePolicy.hpp"
stamp "${ECONOMEM_LIB_DIR}/adaptiveSizePolicy.cpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/shared/adaptiveSizePolicy.cpp"
stamp "${ECONOMEM_LIB_DIR}/mapfile-vers-product" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/make/linux/makefiles/mapfile-vers-product"
stamp "${ECONOMEM_LIB_DIR}/psScavenge.cpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/parallelScavenge/psScavenge.cpp"
stamp "${ECONOMEM_LIB_DIR}/psScavenge.hpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/parallelScavenge/psScavenge.hpp"
stamp "${ECONOMEM_LIB_DIR}/psMarkSweep.cpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/parallelScavenge/psMarkSweep.cpp"
stamp "${ECONOMEM_LIB_DIR}/psMarkSweep.hpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/parallelScavenge/psMarkSweep.hpp"
stamp "${ECONOMEM_LIB_DIR}/psParallelCompact.cpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/parallelScavenge/psParallelCompact.cpp"
stamp "${ECONOMEM_LIB_DIR}/psParallelCompact.hpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/parallelScavenge/psParallelCompact.hpp"

cd "${ICEDTEA_BUILD_ROOT}"
make hotspot 2>&1 | tee "${ECONOMEM_LIB_DIR}/build.log"
make 2>&1 | tee -a "${ECONOMEM_LIB_DIR}/build.log"
make 2>&1 | tee -a "${ECONOMEM_LIB_DIR}/build.log"
