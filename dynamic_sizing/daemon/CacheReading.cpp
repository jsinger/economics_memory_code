#include "CacheReading.hpp"

#define AVG_WEIGHT 50

CacheReading::CacheReading(unsigned long long heapSizeMB, double throughput) : m_HeapSizeMB(heapSizeMB)
#ifdef USE_AVERAGES
                                                                             , m_Throughput(AVG_WEIGHT, throughput)
#else
                                                                             , m_Throughput(throughput)
#endif
{

}

CacheReading& CacheReading::operator=(const CacheReading& r)
{
    m_HeapSizeMB = r.m_HeapSizeMB;
    m_Throughput = r.m_Throughput;
    return *this;
}

unsigned long long CacheReading::getHeapSizeMB() const
{
    return m_HeapSizeMB;
}

double CacheReading::getThroughput() const
{
#ifdef USE_AVERAGES
    return m_Throughput.average();
#else
    return m_Throughput;
#endif
}

void CacheReading::updateReading(double throughput)
{
#ifdef USE_AVERAGES
    m_Throughput.sample(throughput);
#else
    m_Throughput = throughput;
#endif
}

void CacheReading::dumpTo(FILE* fd) const
{
#ifdef USE_AVERAGES
    fprintf(fd, "        (size: %llu MB, throughput: %g)\n", m_HeapSizeMB, m_Throughput.average());
#else
    fprintf(fd, "        (size: %llu MB, throughput: %g)\n", m_HeapSizeMB, m_Throughput);
#endif
}
