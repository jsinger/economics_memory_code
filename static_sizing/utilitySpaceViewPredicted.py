#!/usr/bin/env python
# View a predicted utility space graph interactively

import argparse
import argtypes
import os.path
import staticutility as su
import utilitymodel
from utilitySpacePostprocess import postprocess

parser = argparse.ArgumentParser(description="View a predicted utility space graph interactively")
parser.add_argument("dir", type=argtypes.existing_absolute_dir, help="Directory containing data")
parser.add_argument("model", nargs="?", default=utilitymodel.defaultModel, type=su.validateModel, help="Model to view")
args = parser.parse_args()

dataDir = su.UtilitySpaceDir(args.dir)
model = args.model

if not os.path.exists(dataDir.predictedGraph(model)):
    postprocess(dataDir)

bids = dataDir.parseBenchmarksFile()
results = su.UtilityResultsColumns(su.loadUtilityFullResultsFile(su.fileMustExist(dataDir.resultsFile(model))))

if not dataDir.viewPredicted(results.hs, results.predictedU, bids, dataDir.totalMem(), dataDir.runs(), model):
    print "Cannot visualise results for this number of VMs"
