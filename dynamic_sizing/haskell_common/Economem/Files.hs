{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeSynonymInstances #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FunctionalDependencies #-}
{-# LANGUAGE LambdaCase #-}


module Economem.Files (Path(..),
                       exists,
                       (</>),
                       absPath,
                       Dir(..),
                       asAbsDir,
                       ensureExists,
                       allExistInDir,
                       postprocessDir,
                       is,
                       ofFile,
                       Fileable(..),
                       File,
                       get,
                       getOrDefault,
                       put,
                       AppendableFile,
                       append,
                       (|>),
                       (|>>),
                       WholeFile(..),
                       StringFile,
                       IntFile,
                       DoubleFile,
                       LinesFile(..),
                       CsvFile(..),
                       CsvFields(..),
                       Input(..),
                       fromFile,
                       loadInput,
                       loadOrDefault,
                       maybeLoad,
                       maybeGet,
                       maybePut) where

import Control.Applicative
import Control.Conditional((<||>), ifM)
import Control.Exception
import Control.Monad
import qualified Data.ByteString.Lazy as B
import qualified Data.ByteString as BS
import Data.Char
import qualified Data.Csv as C
import Data.Maybe
import qualified Data.Vector as V
import System.Directory
import qualified System.FilePath as FP
import System.Path
import System.Posix.Files


class Path a where
  getPath :: Path a => a -> FilePath

exists :: Path a => a -> IO Bool
exists p = doesFileExist path <||> doesDirectoryExist path
  where
    path = getPath p

(</>) :: (Path a, Path b) => a -> b -> FilePath
a </> b = getPath a FP.</> getPath b

absPath :: Path a => a -> IO FilePath
absPath path = do
  dot <- canonicalizePath "."
  return (fromMaybe (getPath path) (absNormPath dot (getPath path)))

infixr 5 </>

instance Path FilePath where
  getPath = id

class Path a => Dir a where
  asDir :: (Dir a, Path b) => b -> a
  postprocessingActions :: Dir a => [a -> IO ()]
  postprocessingActions = []

asAbsDir :: (Dir a, Path b) => b -> IO a
asAbsDir path = asDir <$> absPath path

ensureExists :: Dir a => a -> IO ()
ensureExists = createDirectoryIfMissing True . getPath

allExistInDir :: Dir a => a -> [a -> FilePath] -> IO Bool
allExistInDir d files = and <$> mapM doesFileExist (map ($ d) files)

postprocessDir :: Dir a => a -> IO ()
postprocessDir d = mapM_ ($ d) postprocessingActions

is :: (Dir a, File b c) => (a -> b) -> (a -> IO (Maybe c)) -> a -> IO ()
is outfile getter d = getter d >>= \case
  Just val -> val |> outfile d
  Nothing -> return ()

ofFile :: (Dir a, File b c) => (c -> d) -> (a -> b) -> (a -> IO (Maybe d))
ofFile func infile d =
  ifM
  (exists (infile d))
  (Just . func <$> get (infile d))
  (return Nothing)

infix 9 `ofFile`
infix 8 `is`

class Path a => File a b | a -> b where
  getInternal :: File a b => a -> IO b
  putInternal :: File a b => a -> b -> IO ()

class (File a [b]) => AppendableFile a b where
  appendInternal :: AppendableFile a b => a -> b -> IO ()


get :: File a b => a -> IO b
get f =
  catch
  (getInternal f)
  (\e -> fail ("An error occurred reading the file '" ++ getPath f ++ "': " ++ show (e::IOError)))

getOrDefault :: File a b => a -> b -> IO b
getOrDefault f def =
  catch
  (get f)
  (handler def)
  where
    handler :: b -> IOError -> IO b
    handler d _ = return d

put :: File a b => a -> b -> IO ()
put f a =
  catch
  (putInternal f a)
  (\e -> fail ("An error occurred writing to the file '" ++ getPath f ++ "': " ++ show (e::IOError)))

(|>) :: File a b => b -> a -> IO ()
(|>) = flip put

append :: AppendableFile a b => a -> b -> IO ()
append f a =
  catch
  (appendInternal f a)
  (\e -> fail ("An error occurred appending to the file '" ++ getPath f ++ "': " ++ show (e::IOError)))

(|>>) :: AppendableFile a b => b -> a -> IO ()
(|>>) = flip append

class (Read a, Show a) => Fileable a where
  showForFile :: Fileable a => a -> String
  showForFile = show

  readForFile :: Fileable a => String -> IO a
  readForFile = readIO

instance Fileable Integer
instance Fileable Double
instance Fileable Bool

-- String needs special treatment so it doesn't get converted to a Haskell string literal
instance Fileable String where
  showForFile = trim
  readForFile = return


data WholeFile a = WholeFile FilePath
instance Path (WholeFile a) where
  getPath (WholeFile path) = path
instance Fileable a => File (WholeFile a) a where
  getInternal f = trim `liftM` readFile (getPath f) >>= readForFile
  putInternal f d = writeFile (getPath f) (showForFile d ++ "\n")

type StringFile = WholeFile String
type IntFile = WholeFile Integer
type DoubleFile = WholeFile Double


data LinesFile a = LinesFile FilePath
instance Path (LinesFile a) where
  getPath (LinesFile path) = path
instance Fileable a => File (LinesFile a) [a] where
  getInternal f = lines <$> readFile (getPath f) >>= mapM readForFile
  putInternal f xs = writeFile (getPath f) (unlines (map showForFile xs))
instance Fileable a => AppendableFile (LinesFile a) a where
  appendInternal f x = appendFile (getPath f) (showForFile x ++ "\n")


data CsvFile a = CsvFile FilePath
instance Path (CsvFile a) where
  getPath (CsvFile path) = path
instance (C.FromNamedRecord a, C.ToNamedRecord a, CsvFields a) => File (CsvFile a) [a] where
  getInternal f = do
    t <- B.readFile (getPath f)
    case C.decodeByName t of
      Left err -> fail err
      Right (_, v) -> return (V.toList v)
  putInternal f = outputCsvInternal B.writeFile f True
instance (C.FromNamedRecord a, C.ToNamedRecord a, CsvFields a) => AppendableFile (CsvFile a) a where
  appendInternal f x = do
    writeHeaders <- fileEmpty
    outputCsvInternal B.appendFile f writeHeaders [x]
    where
      fileEmpty :: IO Bool
      fileEmpty =
        ifM (exists f)
          (do stat <- getFileStatus (getPath f)
              return (fileSize stat == 0))
          (return True)

class CsvFields a where
  getCsvFieldNames :: CsvFields a => Maybe a -> [C.Name]
  getNamedRecordInput :: CsvFields a => a -> [(BS.ByteString, BS.ByteString)]

outputCsvInternal :: (C.ToNamedRecord a, CsvFields a) =>
                     (FilePath -> B.ByteString -> IO ()) ->
                     CsvFile a ->
                     Bool ->
                     [a] ->
                     IO ()
outputCsvInternal action f writeHeaders xs = do
  let t = C.encodeByName (V.fromList (getCsvFieldNames (if null xs then Nothing else Just (head xs)))) xs
  action (getPath f)
    (if writeHeaders then t
     else B.intercalate "\n" (tail (B.split (fromIntegral (ord '\n')) t)))


data Input a = Raw [a]
             | FromFile (CsvFile a)

fromFile :: FilePath -> Input a
fromFile path = FromFile (CsvFile path)

loadInput :: (CsvFields a, C.FromNamedRecord a, C.ToNamedRecord a) => Input a -> IO [a]
loadInput (Raw xs) = return xs
loadInput (FromFile f) = get f

loadOrDefault :: (CsvFields a, C.FromNamedRecord a, C.ToNamedRecord a) => Input a -> [a] -> IO [a]
loadOrDefault (Raw xs) _ = return xs
loadOrDefault (FromFile f) def = getOrDefault f def

maybeLoad :: (CsvFields a, C.FromNamedRecord a, C.ToNamedRecord a) => Input a -> IO (Maybe [a])
maybeLoad (Raw xs) = return (Just xs)
maybeLoad (FromFile f) =
  catch (Just <$> get f) handler
  where
    handler :: IOError -> IO (Maybe [a])
    handler _ = return Nothing

maybeGet :: (CsvFields a, C.FromNamedRecord a, C.ToNamedRecord a) => CsvFile a -> IO (Maybe (Input a))
maybeGet f =
  ifM
  (exists f)
  (return (Just (FromFile f)))
  (return Nothing)

maybePut :: (CsvFields a, C.FromNamedRecord a, C.ToNamedRecord a) => CsvFile a -> Maybe (Input a) -> IO ()
maybePut _ Nothing = return ()
maybePut f (Just input) = put f =<< loadInput input

trim :: String -> String
trim "" = ""
trim s = if last s == '\n' then
           init s
           else
           s
