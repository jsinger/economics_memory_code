Using scaled threading model. 4 processors detected, 1 threads used to drive the workload, in a possible range of [1,256000]
===== DaCapo 9.12 h2 starting warmup 1 =====
................................................................................................................................................................................................................................................................
Completed 256000 transactions
	Stock level ............. 10178 ( 4.0%)
	Order status by name ....  6096 ( 2.4%)
	Order status by ID ......  4109 ( 1.6%)
	Payment by name ......... 66251 (25.9%)
	Payment by ID ........... 43986 (17.2%)
	Delivery schedule ....... 10282 ( 4.0%)
	New order ...............113955 (44.5%)
	New order rollback ......  1143 ( 0.4%)
Resetting database to initial state
===== DaCapo 9.12 h2 completed warmup 1 in 1088521 msec =====
===== DaCapo 9.12 h2 starting =====
................................................................................................................................................................................................................................................................
Completed 256000 transactions
	Stock level ............. 10178 ( 4.0%)
	Order status by name ....  6096 ( 2.4%)
	Order status by ID ......  4109 ( 1.6%)
	Payment by name ......... 66251 (25.9%)
	Payment by ID ........... 43986 (17.2%)
	Delivery schedule ....... 10282 ( 4.0%)
	New order ...............113955 (44.5%)
	New order rollback ......  1143 ( 0.4%)
Resetting database to initial state
java.lang.reflect.InvocationTargetException
java.lang.reflect.InvocationTargetException
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:483)
	at org.dacapo.harness.H2.iterate(H2.java:87)
	at org.dacapo.harness.Benchmark.run(Benchmark.java:166)
	at org.dacapo.harness.TestHarness.runBenchmark(TestHarness.java:218)
	at org.dacapo.harness.TestHarness.main(TestHarness.java:171)
	at Harness.main(Harness.java:17)
Caused by: org.h2.jdbc.JdbcSQLException: Out of memory.; SQL statement:
DELETE FROM ORDERLINE WHERE OL_INITIAL = FALSE [90108-123]
	at org.h2.message.Message.getSQLException(Message.java:111)
	at org.h2.message.Message.convertThrowable(Message.java:303)
	at org.h2.command.Command.executeUpdate(Command.java:231)
	at org.h2.jdbc.JdbcPreparedStatement.execute(JdbcPreparedStatement.java:176)
	at org.dacapo.h2.TPCC.resetToInitialData(TPCC.java:485)
	at org.dacapo.h2.TPCC.iteration(TPCC.java:310)
	... 9 more
Caused by: java.lang.OutOfMemoryError: Java heap space
	at org.h2.util.ObjectArray.createArray(ObjectArray.java:84)
	at org.h2.util.ObjectArray.ensureCapacity(ObjectArray.java:165)
	at org.h2.util.ObjectArray.add(ObjectArray.java:98)
	at org.h2.log.UndoLog.add(UndoLog.java:119)
	at org.h2.engine.Session.log(Session.java:606)
	at org.h2.engine.Session.log(Session.java:591)
	at org.h2.command.dml.Delete.update(Delete.java:72)
	at org.h2.command.CommandContainer.update(CommandContainer.java:72)
	at org.h2.command.Command.executeUpdate(Command.java:209)
	... 12 more
