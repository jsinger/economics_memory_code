economemd
    total memory: 3705 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub32_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 13873: heap size 9 MB, throughput 0.98948
Clients: 1
Client 13873 has a minimum heap size of 12 MB
Reading from 13872: heap size 9 MB, throughput 0.987513
Clients: 2
Client 13872 has a minimum heap size of 1223 MB
Reading from 13872: heap size 9 MB, throughput 0.958864
Reading from 13873: heap size 9 MB, throughput 0.990489
Reading from 13873: heap size 9 MB, throughput 0.943984
Reading from 13872: heap size 11 MB, throughput 0.98097
Reading from 13873: heap size 9 MB, throughput 0.972881
Reading from 13873: heap size 11 MB, throughput 0.95778
Reading from 13872: heap size 11 MB, throughput 0.981063
Reading from 13873: heap size 11 MB, throughput 0.897326
Reading from 13872: heap size 15 MB, throughput 0.84761
Reading from 13873: heap size 16 MB, throughput 0.986183
Reading from 13872: heap size 19 MB, throughput 0.949975
Reading from 13872: heap size 25 MB, throughput 0.942971
Reading from 13873: heap size 16 MB, throughput 0.979953
Reading from 13872: heap size 29 MB, throughput 0.896
Reading from 13872: heap size 31 MB, throughput 0.566383
Reading from 13872: heap size 41 MB, throughput 0.898162
Reading from 13872: heap size 47 MB, throughput 0.876091
Reading from 13873: heap size 23 MB, throughput 0.924961
Reading from 13872: heap size 48 MB, throughput 0.675073
Reading from 13873: heap size 28 MB, throughput 0.989583
Reading from 13872: heap size 53 MB, throughput 0.344195
Reading from 13872: heap size 68 MB, throughput 0.662167
Reading from 13873: heap size 34 MB, throughput 0.989787
Reading from 13872: heap size 76 MB, throughput 0.296037
Reading from 13872: heap size 96 MB, throughput 0.692222
Reading from 13873: heap size 37 MB, throughput 0.989948
Reading from 13872: heap size 104 MB, throughput 0.843771
Reading from 13873: heap size 43 MB, throughput 0.990574
Reading from 13873: heap size 42 MB, throughput 0.971214
Reading from 13872: heap size 105 MB, throughput 0.195192
Reading from 13873: heap size 51 MB, throughput 0.987576
Reading from 13872: heap size 139 MB, throughput 0.880545
Reading from 13873: heap size 51 MB, throughput 0.99168
Reading from 13873: heap size 61 MB, throughput 0.989141
Reading from 13872: heap size 140 MB, throughput 0.143634
Reading from 13873: heap size 61 MB, throughput 0.990016
Reading from 13872: heap size 181 MB, throughput 0.812213
Reading from 13873: heap size 69 MB, throughput 0.390302
Reading from 13872: heap size 182 MB, throughput 0.727036
Reading from 13873: heap size 73 MB, throughput 0.988806
Reading from 13872: heap size 186 MB, throughput 0.684822
Reading from 13872: heap size 189 MB, throughput 0.677333
Reading from 13872: heap size 197 MB, throughput 0.729718
Reading from 13873: heap size 87 MB, throughput 0.993706
Reading from 13872: heap size 203 MB, throughput 0.201074
Reading from 13872: heap size 250 MB, throughput 0.73811
Reading from 13873: heap size 87 MB, throughput 0.990029
Reading from 13872: heap size 255 MB, throughput 0.722743
Reading from 13872: heap size 259 MB, throughput 0.754933
Reading from 13872: heap size 267 MB, throughput 0.672791
Reading from 13872: heap size 272 MB, throughput 0.619958
Reading from 13872: heap size 284 MB, throughput 0.635378
Reading from 13872: heap size 289 MB, throughput 0.565117
Reading from 13872: heap size 304 MB, throughput 0.489463
Reading from 13873: heap size 101 MB, throughput 0.997658
Reading from 13872: heap size 311 MB, throughput 0.0886857
Reading from 13872: heap size 367 MB, throughput 0.401956
Reading from 13873: heap size 102 MB, throughput 0.998043
Reading from 13872: heap size 366 MB, throughput 0.104314
Reading from 13872: heap size 347 MB, throughput 0.496349
Reading from 13873: heap size 114 MB, throughput 0.997586
Reading from 13872: heap size 412 MB, throughput 0.634129
Reading from 13872: heap size 335 MB, throughput 0.61495
Reading from 13872: heap size 403 MB, throughput 0.596738
Reading from 13872: heap size 410 MB, throughput 0.579164
Reading from 13872: heap size 411 MB, throughput 0.557422
Reading from 13872: heap size 413 MB, throughput 0.52852
Reading from 13873: heap size 117 MB, throughput 0.998131
Reading from 13872: heap size 419 MB, throughput 0.11654
Reading from 13873: heap size 125 MB, throughput 0.998034
Reading from 13872: heap size 474 MB, throughput 0.445737
Equal recommendation: 1852 MB each
Reading from 13872: heap size 479 MB, throughput 0.596611
Reading from 13872: heap size 481 MB, throughput 0.561656
Reading from 13872: heap size 485 MB, throughput 0.514958
Reading from 13873: heap size 126 MB, throughput 0.997705
Reading from 13872: heap size 490 MB, throughput 0.523465
Reading from 13872: heap size 494 MB, throughput 0.468235
Reading from 13872: heap size 503 MB, throughput 0.513751
Reading from 13873: heap size 133 MB, throughput 0.998107
Reading from 13872: heap size 511 MB, throughput 0.0946101
Reading from 13873: heap size 134 MB, throughput 0.997274
Reading from 13872: heap size 575 MB, throughput 0.417817
Reading from 13872: heap size 583 MB, throughput 0.611757
Reading from 13872: heap size 584 MB, throughput 0.55812
Reading from 13873: heap size 140 MB, throughput 0.99785
Reading from 13872: heap size 591 MB, throughput 0.147733
Reading from 13873: heap size 141 MB, throughput 0.998129
Reading from 13872: heap size 660 MB, throughput 0.49468
Reading from 13872: heap size 662 MB, throughput 0.601416
Reading from 13872: heap size 666 MB, throughput 0.616619
Reading from 13873: heap size 146 MB, throughput 0.998753
Reading from 13872: heap size 668 MB, throughput 0.827086
Reading from 13872: heap size 676 MB, throughput 0.845838
Reading from 13873: heap size 147 MB, throughput 0.998409
Reading from 13872: heap size 674 MB, throughput 0.831093
Reading from 13872: heap size 696 MB, throughput 0.487948
Reading from 13872: heap size 704 MB, throughput 0.56437
Reading from 13873: heap size 150 MB, throughput 0.997911
Reading from 13873: heap size 151 MB, throughput 0.997471
Reading from 13872: heap size 716 MB, throughput 0.0375161
Reading from 13872: heap size 799 MB, throughput 0.248648
Reading from 13872: heap size 701 MB, throughput 0.345548
Reading from 13873: heap size 155 MB, throughput 0.998456
Reading from 13872: heap size 782 MB, throughput 0.394588
Equal recommendation: 1852 MB each
Reading from 13872: heap size 684 MB, throughput 0.0492768
Reading from 13873: heap size 155 MB, throughput 0.998308
Reading from 13872: heap size 864 MB, throughput 0.79962
Reading from 13872: heap size 867 MB, throughput 0.59809
Reading from 13872: heap size 869 MB, throughput 0.614521
Reading from 13872: heap size 871 MB, throughput 0.632256
Reading from 13873: heap size 159 MB, throughput 0.998628
Reading from 13872: heap size 871 MB, throughput 0.833567
Reading from 13872: heap size 873 MB, throughput 0.766452
Reading from 13873: heap size 159 MB, throughput 0.998389
Reading from 13872: heap size 878 MB, throughput 0.116182
Reading from 13873: heap size 162 MB, throughput 0.995691
Reading from 13872: heap size 975 MB, throughput 0.697733
Reading from 13872: heap size 982 MB, throughput 0.847621
Reading from 13873: heap size 162 MB, throughput 0.9944
Reading from 13872: heap size 984 MB, throughput 0.8927
Reading from 13873: heap size 166 MB, throughput 0.994529
Reading from 13872: heap size 979 MB, throughput 0.884982
Reading from 13872: heap size 824 MB, throughput 0.780638
Reading from 13872: heap size 959 MB, throughput 0.81808
Reading from 13872: heap size 829 MB, throughput 0.80289
Reading from 13872: heap size 947 MB, throughput 0.7857
Reading from 13872: heap size 834 MB, throughput 0.824681
Reading from 13872: heap size 939 MB, throughput 0.842218
Reading from 13873: heap size 167 MB, throughput 0.997413
Reading from 13872: heap size 946 MB, throughput 0.83056
Reading from 13872: heap size 932 MB, throughput 0.856482
Reading from 13872: heap size 939 MB, throughput 0.849034
Reading from 13872: heap size 928 MB, throughput 0.97518
Reading from 13873: heap size 174 MB, throughput 0.997508
Reading from 13873: heap size 175 MB, throughput 0.997116
Reading from 13872: heap size 934 MB, throughput 0.973429
Reading from 13872: heap size 933 MB, throughput 0.809405
Reading from 13872: heap size 936 MB, throughput 0.823734
Reading from 13872: heap size 930 MB, throughput 0.753172
Reading from 13872: heap size 934 MB, throughput 0.803106
Reading from 13873: heap size 180 MB, throughput 0.997417
Reading from 13872: heap size 930 MB, throughput 0.791156
Reading from 13872: heap size 934 MB, throughput 0.841644
Reading from 13872: heap size 931 MB, throughput 0.848859
Equal recommendation: 1852 MB each
Reading from 13872: heap size 935 MB, throughput 0.830483
Reading from 13872: heap size 934 MB, throughput 0.906908
Reading from 13873: heap size 180 MB, throughput 0.997426
Reading from 13872: heap size 937 MB, throughput 0.89727
Reading from 13872: heap size 937 MB, throughput 0.872116
Reading from 13872: heap size 940 MB, throughput 0.743679
Reading from 13872: heap size 926 MB, throughput 0.671952
Reading from 13872: heap size 946 MB, throughput 0.640996
Reading from 13873: heap size 186 MB, throughput 0.997489
Reading from 13873: heap size 186 MB, throughput 0.996522
Reading from 13872: heap size 956 MB, throughput 0.0548023
Reading from 13872: heap size 1056 MB, throughput 0.565922
Reading from 13873: heap size 191 MB, throughput 0.997384
Reading from 13872: heap size 1053 MB, throughput 0.737389
Reading from 13872: heap size 1058 MB, throughput 0.734918
Reading from 13872: heap size 1059 MB, throughput 0.717026
Reading from 13872: heap size 1062 MB, throughput 0.722518
Reading from 13872: heap size 1072 MB, throughput 0.725213
Reading from 13872: heap size 1073 MB, throughput 0.730222
Reading from 13873: heap size 191 MB, throughput 0.997607
Reading from 13872: heap size 1084 MB, throughput 0.958655
Reading from 13873: heap size 197 MB, throughput 0.997923
Reading from 13873: heap size 197 MB, throughput 0.99678
Reading from 13873: heap size 202 MB, throughput 0.997754
Reading from 13872: heap size 1089 MB, throughput 0.974937
Equal recommendation: 1852 MB each
Reading from 13873: heap size 202 MB, throughput 0.997036
Reading from 13873: heap size 207 MB, throughput 0.943895
Reading from 13872: heap size 1104 MB, throughput 0.967973
Reading from 13873: heap size 209 MB, throughput 0.990926
Reading from 13873: heap size 214 MB, throughput 0.997018
Reading from 13873: heap size 214 MB, throughput 0.997898
Reading from 13872: heap size 1105 MB, throughput 0.977555
Reading from 13873: heap size 223 MB, throughput 0.998402
Reading from 13873: heap size 223 MB, throughput 0.998174
Reading from 13872: heap size 1106 MB, throughput 0.9782
Reading from 13873: heap size 229 MB, throughput 0.997707
Equal recommendation: 1852 MB each
Reading from 13873: heap size 230 MB, throughput 0.997647
Reading from 13872: heap size 1111 MB, throughput 0.97718
Reading from 13873: heap size 238 MB, throughput 0.998349
Reading from 13873: heap size 238 MB, throughput 0.997464
Reading from 13872: heap size 1104 MB, throughput 0.971454
Reading from 13873: heap size 245 MB, throughput 0.998165
Reading from 13873: heap size 245 MB, throughput 0.997876
Reading from 13872: heap size 1109 MB, throughput 0.975431
Reading from 13873: heap size 252 MB, throughput 0.998209
Equal recommendation: 1852 MB each
Reading from 13873: heap size 252 MB, throughput 0.99647
Reading from 13872: heap size 1103 MB, throughput 0.961479
Reading from 13873: heap size 258 MB, throughput 0.992234
Reading from 13873: heap size 259 MB, throughput 0.996872
Reading from 13872: heap size 1107 MB, throughput 0.978717
Reading from 13873: heap size 270 MB, throughput 0.997999
Reading from 13873: heap size 271 MB, throughput 0.998122
Reading from 13872: heap size 1109 MB, throughput 0.976631
Reading from 13873: heap size 279 MB, throughput 0.998217
Equal recommendation: 1852 MB each
Reading from 13873: heap size 280 MB, throughput 0.998121
Reading from 13872: heap size 1110 MB, throughput 0.978585
Reading from 13873: heap size 289 MB, throughput 0.99777
Reading from 13873: heap size 289 MB, throughput 0.998115
Reading from 13872: heap size 1113 MB, throughput 0.978304
Reading from 13873: heap size 297 MB, throughput 0.998411
Reading from 13873: heap size 297 MB, throughput 0.998397
Reading from 13872: heap size 1115 MB, throughput 0.952268
Reading from 13873: heap size 305 MB, throughput 0.992918
Equal recommendation: 1852 MB each
Reading from 13873: heap size 305 MB, throughput 0.997961
Reading from 13872: heap size 1118 MB, throughput 0.977089
Reading from 13873: heap size 317 MB, throughput 0.99827
Reading from 13872: heap size 1121 MB, throughput 0.976289
Reading from 13873: heap size 318 MB, throughput 0.998162
Reading from 13873: heap size 326 MB, throughput 0.997744
Reading from 13872: heap size 1124 MB, throughput 0.97851
Reading from 13873: heap size 327 MB, throughput 0.970951
Equal recommendation: 1852 MB each
Reading from 13873: heap size 342 MB, throughput 0.998873
Reading from 13872: heap size 1128 MB, throughput 0.975834
Reading from 13873: heap size 342 MB, throughput 0.999211
Reading from 13872: heap size 1132 MB, throughput 0.975699
Reading from 13873: heap size 352 MB, throughput 0.998175
Reading from 13873: heap size 353 MB, throughput 0.990859
Reading from 13872: heap size 1135 MB, throughput 0.976816
Reading from 13873: heap size 363 MB, throughput 0.998599
Equal recommendation: 1852 MB each
Reading from 13873: heap size 363 MB, throughput 0.998609
Reading from 13872: heap size 1139 MB, throughput 0.976263
Reading from 13873: heap size 376 MB, throughput 0.998499
Reading from 13872: heap size 1140 MB, throughput 0.976574
Reading from 13873: heap size 378 MB, throughput 0.998615
Reading from 13873: heap size 390 MB, throughput 0.998378
Equal recommendation: 1852 MB each
Reading from 13872: heap size 1143 MB, throughput 0.974831
Reading from 13873: heap size 391 MB, throughput 0.998501
Reading from 13873: heap size 403 MB, throughput 0.995523
Reading from 13872: heap size 1144 MB, throughput 0.967324
Reading from 13873: heap size 404 MB, throughput 0.997385
Reading from 13873: heap size 420 MB, throughput 0.997721
Reading from 13872: heap size 1147 MB, throughput 0.672608
Equal recommendation: 1852 MB each
Reading from 13873: heap size 421 MB, throughput 0.998501
Reading from 13872: heap size 1239 MB, throughput 0.96786
Reading from 13873: heap size 435 MB, throughput 0.998013
Reading from 13872: heap size 1235 MB, throughput 0.989987
Reading from 13873: heap size 436 MB, throughput 0.998067
Equal recommendation: 1852 MB each
Reading from 13872: heap size 1243 MB, throughput 0.989921
Reading from 13873: heap size 449 MB, throughput 0.998566
Reading from 13873: heap size 450 MB, throughput 0.993378
Reading from 13872: heap size 1250 MB, throughput 0.986238
Reading from 13873: heap size 462 MB, throughput 0.997962
Reading from 13872: heap size 1251 MB, throughput 0.987673
Reading from 13873: heap size 464 MB, throughput 0.998178
Equal recommendation: 1852 MB each
Reading from 13872: heap size 1246 MB, throughput 0.986892
Reading from 13873: heap size 480 MB, throughput 0.99874
Reading from 13873: heap size 482 MB, throughput 0.998482
Reading from 13872: heap size 1160 MB, throughput 0.984928
Reading from 13873: heap size 496 MB, throughput 0.998273
Reading from 13872: heap size 1236 MB, throughput 0.980468
Reading from 13873: heap size 498 MB, throughput 0.99528
Equal recommendation: 1852 MB each
Reading from 13872: heap size 1175 MB, throughput 0.981514
Reading from 13873: heap size 512 MB, throughput 0.998002
Reading from 13872: heap size 1230 MB, throughput 0.9809
Reading from 13873: heap size 513 MB, throughput 0.998637
Reading from 13872: heap size 1234 MB, throughput 0.981937
Equal recommendation: 1852 MB each
Reading from 13873: heap size 529 MB, throughput 0.998518
Reading from 13873: heap size 531 MB, throughput 0.998588
Reading from 13872: heap size 1235 MB, throughput 0.980266
Reading from 13873: heap size 545 MB, throughput 0.997218
Reading from 13872: heap size 1236 MB, throughput 0.977394
Reading from 13873: heap size 547 MB, throughput 0.998345
Equal recommendation: 1852 MB each
Reading from 13872: heap size 1238 MB, throughput 0.980828
Reading from 13873: heap size 565 MB, throughput 0.998339
Reading from 13872: heap size 1241 MB, throughput 0.975946
Reading from 13873: heap size 566 MB, throughput 0.998395
Reading from 13872: heap size 1244 MB, throughput 0.977202
Equal recommendation: 1852 MB each
Reading from 13873: heap size 581 MB, throughput 0.99819
Reading from 13872: heap size 1250 MB, throughput 0.974888
Reading from 13873: heap size 582 MB, throughput 0.997424
Reading from 13872: heap size 1255 MB, throughput 0.975098
Reading from 13873: heap size 599 MB, throughput 0.998124
Equal recommendation: 1852 MB each
Reading from 13872: heap size 1258 MB, throughput 0.975108
Reading from 13873: heap size 600 MB, throughput 0.997953
Reading from 13872: heap size 1263 MB, throughput 0.978516
Reading from 13873: heap size 619 MB, throughput 0.998402
Reading from 13872: heap size 1265 MB, throughput 0.977577
Equal recommendation: 1852 MB each
Reading from 13873: heap size 619 MB, throughput 0.99836
Reading from 13873: heap size 635 MB, throughput 0.997414
Reading from 13872: heap size 1269 MB, throughput 0.976534
Reading from 13872: heap size 1270 MB, throughput 0.975446
Reading from 13873: heap size 637 MB, throughput 0.998516
Equal recommendation: 1852 MB each
Reading from 13872: heap size 1272 MB, throughput 0.974553
Reading from 13873: heap size 656 MB, throughput 0.998681
Reading from 13873: heap size 657 MB, throughput 0.99861
Reading from 13873: heap size 674 MB, throughput 0.95811
Equal recommendation: 1852 MB each
Reading from 13873: heap size 678 MB, throughput 0.998981
Reading from 13873: heap size 642 MB, throughput 0.999274
Reading from 13872: heap size 1273 MB, throughput 0.836395
Equal recommendation: 1852 MB each
Reading from 13873: heap size 611 MB, throughput 0.999189
Reading from 13873: heap size 582 MB, throughput 0.999206
Reading from 13873: heap size 555 MB, throughput 0.996965
Reading from 13872: heap size 1439 MB, throughput 0.985225
Reading from 13873: heap size 535 MB, throughput 0.998919
Equal recommendation: 1852 MB each
Reading from 13873: heap size 507 MB, throughput 0.998844
Reading from 13872: heap size 1384 MB, throughput 0.989619
Reading from 13872: heap size 1480 MB, throughput 0.886354
Reading from 13872: heap size 1482 MB, throughput 0.768439
Reading from 13872: heap size 1459 MB, throughput 0.690083
Reading from 13872: heap size 1483 MB, throughput 0.676021
Reading from 13873: heap size 483 MB, throughput 0.998828
Reading from 13872: heap size 1503 MB, throughput 0.704526
Reading from 13872: heap size 1514 MB, throughput 0.682752
Reading from 13872: heap size 1537 MB, throughput 0.711234
Reading from 13872: heap size 1543 MB, throughput 0.711834
Reading from 13872: heap size 1569 MB, throughput 0.743835
Reading from 13872: heap size 1570 MB, throughput 0.857706
Reading from 13873: heap size 461 MB, throughput 0.998143
Equal recommendation: 1852 MB each
Reading from 13873: heap size 440 MB, throughput 0.998407
Reading from 13872: heap size 1593 MB, throughput 0.980641
Reading from 13873: heap size 421 MB, throughput 0.996104
Reading from 13873: heap size 404 MB, throughput 0.998338
Reading from 13872: heap size 1595 MB, throughput 0.984319
Reading from 13873: heap size 383 MB, throughput 0.998225
Equal recommendation: 1852 MB each
Reading from 13872: heap size 1609 MB, throughput 0.986182
Reading from 13873: heap size 367 MB, throughput 0.998
Reading from 13873: heap size 350 MB, throughput 0.99751
Reading from 13872: heap size 1615 MB, throughput 0.980824
Reading from 13873: heap size 335 MB, throughput 0.998032
Reading from 13873: heap size 320 MB, throughput 0.997804
Reading from 13872: heap size 1627 MB, throughput 0.861595
Equal recommendation: 1852 MB each
Client 13873 died
Clients: 1
Reading from 13872: heap size 1471 MB, throughput 0.996138
Reading from 13872: heap size 1482 MB, throughput 0.994783
Recommendation: one client; give it all the memory
Reading from 13872: heap size 1486 MB, throughput 0.994122
Reading from 13872: heap size 1485 MB, throughput 0.992568
Recommendation: one client; give it all the memory
Reading from 13872: heap size 1492 MB, throughput 0.991346
Reading from 13872: heap size 1472 MB, throughput 0.990585
Recommendation: one client; give it all the memory
Reading from 13872: heap size 1312 MB, throughput 0.989258
Reading from 13872: heap size 1453 MB, throughput 0.988604
Recommendation: one client; give it all the memory
Reading from 13872: heap size 1337 MB, throughput 0.987514
Reading from 13872: heap size 1441 MB, throughput 0.986452
Recommendation: one client; give it all the memory
Reading from 13872: heap size 1450 MB, throughput 0.986124
Reading from 13872: heap size 1451 MB, throughput 0.985281
Recommendation: one client; give it all the memory
Reading from 13872: heap size 1451 MB, throughput 0.984262
Reading from 13872: heap size 1454 MB, throughput 0.982577
Recommendation: one client; give it all the memory
Reading from 13872: heap size 1459 MB, throughput 0.980943
Reading from 13872: heap size 1463 MB, throughput 0.980202
Recommendation: one client; give it all the memory
Reading from 13872: heap size 1471 MB, throughput 0.981093
Reading from 13872: heap size 1479 MB, throughput 0.980391
Recommendation: one client; give it all the memory
Reading from 13872: heap size 1484 MB, throughput 0.979616
Reading from 13872: heap size 1492 MB, throughput 0.978836
Recommendation: one client; give it all the memory
Reading from 13872: heap size 1495 MB, throughput 0.979215
Reading from 13872: heap size 1504 MB, throughput 0.98084
Recommendation: one client; give it all the memory
Reading from 13872: heap size 1504 MB, throughput 0.979232
Reading from 13872: heap size 1512 MB, throughput 0.979814
Recommendation: one client; give it all the memory
Reading from 13872: heap size 1512 MB, throughput 0.979446
Reading from 13872: heap size 1516 MB, throughput 0.980494
Recommendation: one client; give it all the memory
Reading from 13872: heap size 1518 MB, throughput 0.980431
Reading from 13872: heap size 1522 MB, throughput 0.980028
Recommendation: one client; give it all the memory
Reading from 13872: heap size 1523 MB, throughput 0.988644
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 13872: heap size 1521 MB, throughput 0.992282
Reading from 13872: heap size 1530 MB, throughput 0.984908
Reading from 13872: heap size 1532 MB, throughput 0.334073
Reading from 13872: heap size 1604 MB, throughput 0.995802
Reading from 13872: heap size 1636 MB, throughput 0.99468
Reading from 13872: heap size 1652 MB, throughput 0.994723
Recommendation: one client; give it all the memory
Reading from 13872: heap size 1673 MB, throughput 0.98956
Client 13872 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
