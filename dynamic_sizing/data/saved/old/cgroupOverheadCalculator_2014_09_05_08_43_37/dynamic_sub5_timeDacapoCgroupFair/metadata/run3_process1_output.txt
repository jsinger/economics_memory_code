GCBench: 1 threads, 149 iters



##########
GCBench iteration 1
##########

[0] Garbage Collector Test
[0]  Stretching memory with a binary tree of depth 22
 Total memory available=9961472 bytes  Free memory=8928336 bytes
[0]  Creating a long-lived binary tree of depth 22
[0]  Creating a long-lived array of 700000 doubles
 Total memory available=451411968 bytes  Free memory=176245440 bytes
Creating 8196 trees of depth 10
	Top down construction took 149msecs
	Bottom up construction took 115msecs
Creating 2048 trees of depth 12
	Top down construction took 111msecs
	Bottom up construction took 105msecs
Creating 512 trees of depth 14
	Top down construction took 90msecs
	Bottom up construction took 99msecs
Creating 128 trees of depth 16
	Top down construction took 87msecs
	Bottom up construction took 99msecs
Creating 32 trees of depth 18
	Top down construction took 96msecs
	Bottom up construction took 89msecs
Creating 8 trees of depth 20
	Top down construction took 97msecs
	Bottom up construction took 124msecs
Creating 2 trees of depth 22
Pool did not terminate
[harness] Finished all threads


##########
GCBench iteration 2
##########

Exception in thread "main" Exception in thread "pool-1-thread-1" java.lang.OutOfMemoryError: Java heap space
	at GCBench.Populate(GCBench.java:71)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:73)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:73)
	at GCBench.Populate(GCBench.java:73)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:73)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:73)
	at GCBench.Populate(GCBench.java:73)
	at GCBench.Populate(GCBench.java:73)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:73)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.Populate(GCBench.java:74)
	at GCBench.TimeConstruction(GCBench.java:108)
	at GCBenchRunner.run(GCBenchRunner.java:111)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)
	at java.lang.Thread.run(Thread.java:745)
java.lang.OutOfMemoryError: Java heap space
	at java.util.HashMap.newNode(HashMap.java:1734)
	at java.util.HashMap.putVal(HashMap.java:630)
	at java.util.HashMap.put(HashMap.java:611)
	at java.util.HashSet.add(HashSet.java:219)
	at java.util.concurrent.ThreadPoolExecutor.addWorker(ThreadPoolExecutor.java:940)
	at java.util.concurrent.ThreadPoolExecutor.execute(ThreadPoolExecutor.java:1357)
	at GCBenchMT.start(GCBenchMT.java:158)
	at GCBenchWrapper.main(GCBenchWrapper.java:14)
