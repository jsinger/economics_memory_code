economemd
    total memory: 1656 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub21_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 23608: heap size 9 MB, throughput 0.990974
Clients: 1
Client 23608 has a minimum heap size of 276 MB
Reading from 23609: heap size 9 MB, throughput 0.986067
Clients: 2
Client 23609 has a minimum heap size of 276 MB
Reading from 23608: heap size 9 MB, throughput 0.979097
Reading from 23609: heap size 9 MB, throughput 0.956599
Reading from 23608: heap size 9 MB, throughput 0.95905
Reading from 23608: heap size 9 MB, throughput 0.956312
Reading from 23609: heap size 11 MB, throughput 0.975819
Reading from 23608: heap size 11 MB, throughput 0.981658
Reading from 23609: heap size 11 MB, throughput 0.984411
Reading from 23608: heap size 11 MB, throughput 0.970546
Reading from 23608: heap size 17 MB, throughput 0.925465
Reading from 23609: heap size 15 MB, throughput 0.812244
Reading from 23609: heap size 19 MB, throughput 0.953737
Reading from 23608: heap size 17 MB, throughput 0.428565
Reading from 23609: heap size 23 MB, throughput 0.925119
Reading from 23608: heap size 30 MB, throughput 0.926902
Reading from 23609: heap size 28 MB, throughput 0.830981
Reading from 23608: heap size 31 MB, throughput 0.948173
Reading from 23609: heap size 28 MB, throughput 0.27104
Reading from 23609: heap size 37 MB, throughput 0.910724
Reading from 23608: heap size 34 MB, throughput 0.305625
Reading from 23609: heap size 42 MB, throughput 0.895823
Reading from 23608: heap size 47 MB, throughput 0.927393
Reading from 23609: heap size 43 MB, throughput 0.773664
Reading from 23608: heap size 49 MB, throughput 0.916672
Reading from 23608: heap size 50 MB, throughput 0.852966
Reading from 23609: heap size 48 MB, throughput 0.406763
Reading from 23609: heap size 61 MB, throughput 0.656462
Reading from 23608: heap size 54 MB, throughput 0.163224
Reading from 23608: heap size 69 MB, throughput 0.636885
Reading from 23608: heap size 74 MB, throughput 0.734526
Reading from 23609: heap size 68 MB, throughput 0.193871
Reading from 23608: heap size 76 MB, throughput 0.188274
Reading from 23609: heap size 84 MB, throughput 0.24626
Reading from 23608: heap size 97 MB, throughput 0.611336
Reading from 23609: heap size 110 MB, throughput 0.713232
Reading from 23608: heap size 101 MB, throughput 0.696251
Reading from 23608: heap size 103 MB, throughput 0.611494
Reading from 23609: heap size 113 MB, throughput 0.892963
Reading from 23608: heap size 106 MB, throughput 0.724241
Reading from 23609: heap size 114 MB, throughput 0.781093
Reading from 23608: heap size 113 MB, throughput 0.740815
Reading from 23609: heap size 116 MB, throughput 0.746594
Reading from 23608: heap size 116 MB, throughput 0.660345
Reading from 23608: heap size 120 MB, throughput 0.785712
Reading from 23609: heap size 119 MB, throughput 0.134383
Reading from 23609: heap size 156 MB, throughput 0.648682
Reading from 23608: heap size 126 MB, throughput 0.125326
Reading from 23609: heap size 160 MB, throughput 0.828403
Reading from 23608: heap size 164 MB, throughput 0.708782
Reading from 23609: heap size 162 MB, throughput 0.766593
Reading from 23608: heap size 168 MB, throughput 0.69012
Reading from 23609: heap size 170 MB, throughput 0.644502
Reading from 23608: heap size 170 MB, throughput 0.507195
Reading from 23608: heap size 174 MB, throughput 0.623827
Reading from 23609: heap size 172 MB, throughput 0.178121
Reading from 23609: heap size 216 MB, throughput 0.694277
Reading from 23608: heap size 179 MB, throughput 0.108602
Reading from 23608: heap size 215 MB, throughput 0.458657
Reading from 23609: heap size 219 MB, throughput 0.907313
Reading from 23608: heap size 218 MB, throughput 0.840921
Reading from 23609: heap size 220 MB, throughput 0.897063
Reading from 23608: heap size 220 MB, throughput 0.900066
Reading from 23609: heap size 227 MB, throughput 0.795277
Reading from 23608: heap size 221 MB, throughput 0.673249
Reading from 23609: heap size 232 MB, throughput 0.846866
Reading from 23608: heap size 226 MB, throughput 0.585502
Reading from 23609: heap size 235 MB, throughput 0.71462
Reading from 23608: heap size 230 MB, throughput 0.601082
Reading from 23609: heap size 240 MB, throughput 0.844776
Reading from 23608: heap size 233 MB, throughput 0.682745
Reading from 23609: heap size 243 MB, throughput 0.753819
Reading from 23608: heap size 237 MB, throughput 0.531574
Reading from 23609: heap size 247 MB, throughput 0.753279
Reading from 23608: heap size 239 MB, throughput 0.517007
Reading from 23608: heap size 243 MB, throughput 0.712138
Reading from 23609: heap size 249 MB, throughput 0.663844
Reading from 23609: heap size 251 MB, throughput 0.685813
Reading from 23609: heap size 256 MB, throughput 0.556001
Reading from 23609: heap size 261 MB, throughput 0.693251
Reading from 23609: heap size 264 MB, throughput 0.579604
Reading from 23608: heap size 244 MB, throughput 0.146044
Reading from 23608: heap size 272 MB, throughput 0.62379
Reading from 23608: heap size 276 MB, throughput 0.692296
Reading from 23608: heap size 272 MB, throughput 0.600202
Reading from 23608: heap size 275 MB, throughput 0.67837
Reading from 23609: heap size 270 MB, throughput 0.250776
Reading from 23608: heap size 275 MB, throughput 0.747546
Reading from 23609: heap size 311 MB, throughput 0.859172
Reading from 23608: heap size 277 MB, throughput 0.304089
Reading from 23609: heap size 307 MB, throughput 0.815223
Reading from 23609: heap size 310 MB, throughput 0.844622
Reading from 23609: heap size 314 MB, throughput 0.885422
Reading from 23609: heap size 314 MB, throughput 0.879057
Reading from 23608: heap size 319 MB, throughput 0.88503
Reading from 23609: heap size 316 MB, throughput 0.900537
Reading from 23608: heap size 321 MB, throughput 0.840679
Reading from 23608: heap size 325 MB, throughput 0.788615
Reading from 23609: heap size 318 MB, throughput 0.880464
Reading from 23608: heap size 326 MB, throughput 0.880163
Reading from 23609: heap size 321 MB, throughput 0.851939
Reading from 23608: heap size 328 MB, throughput 0.787471
Reading from 23609: heap size 321 MB, throughput 0.621449
Reading from 23609: heap size 319 MB, throughput 0.578733
Reading from 23608: heap size 329 MB, throughput 0.844136
Reading from 23609: heap size 321 MB, throughput 0.633936
Reading from 23609: heap size 323 MB, throughput 0.639277
Reading from 23608: heap size 331 MB, throughput 0.821006
Reading from 23608: heap size 332 MB, throughput 0.686966
Reading from 23609: heap size 325 MB, throughput 0.642062
Reading from 23608: heap size 329 MB, throughput 0.639535
Reading from 23608: heap size 332 MB, throughput 0.688057
Reading from 23608: heap size 334 MB, throughput 0.698605
Reading from 23608: heap size 336 MB, throughput 0.727368
Equal recommendation: 828 MB each
Reading from 23608: heap size 343 MB, throughput 0.93827
Reading from 23609: heap size 331 MB, throughput 0.977226
Reading from 23609: heap size 332 MB, throughput 0.952958
Reading from 23608: heap size 343 MB, throughput 0.969513
Reading from 23608: heap size 347 MB, throughput 0.964113
Reading from 23609: heap size 333 MB, throughput 0.667426
Reading from 23608: heap size 349 MB, throughput 0.976545
Reading from 23609: heap size 377 MB, throughput 0.95996
Reading from 23608: heap size 349 MB, throughput 0.97455
Reading from 23609: heap size 382 MB, throughput 0.984935
Reading from 23608: heap size 351 MB, throughput 0.979111
Reading from 23609: heap size 382 MB, throughput 0.985409
Reading from 23608: heap size 349 MB, throughput 0.975919
Reading from 23609: heap size 382 MB, throughput 0.982311
Reading from 23608: heap size 352 MB, throughput 0.979147
Equal recommendation: 828 MB each
Reading from 23609: heap size 385 MB, throughput 0.982971
Reading from 23608: heap size 352 MB, throughput 0.972044
Reading from 23609: heap size 381 MB, throughput 0.973515
Reading from 23608: heap size 353 MB, throughput 0.971647
Reading from 23609: heap size 384 MB, throughput 0.979507
Reading from 23608: heap size 355 MB, throughput 0.963787
Reading from 23609: heap size 384 MB, throughput 0.982819
Reading from 23608: heap size 356 MB, throughput 0.974504
Reading from 23608: heap size 358 MB, throughput 0.971587
Reading from 23609: heap size 384 MB, throughput 0.978271
Reading from 23608: heap size 360 MB, throughput 0.962302
Reading from 23609: heap size 386 MB, throughput 0.973847
Reading from 23608: heap size 362 MB, throughput 0.975587
Reading from 23608: heap size 364 MB, throughput 0.961603
Reading from 23609: heap size 387 MB, throughput 0.973196
Reading from 23608: heap size 363 MB, throughput 0.81053
Reading from 23608: heap size 365 MB, throughput 0.731307
Reading from 23608: heap size 371 MB, throughput 0.810819
Equal recommendation: 828 MB each
Reading from 23608: heap size 374 MB, throughput 0.93417
Reading from 23609: heap size 389 MB, throughput 0.970531
Reading from 23609: heap size 390 MB, throughput 0.784706
Reading from 23609: heap size 386 MB, throughput 0.844193
Reading from 23609: heap size 393 MB, throughput 0.837151
Reading from 23609: heap size 402 MB, throughput 0.83112
Reading from 23608: heap size 381 MB, throughput 0.986726
Reading from 23609: heap size 405 MB, throughput 0.97792
Reading from 23608: heap size 383 MB, throughput 0.979084
Reading from 23609: heap size 405 MB, throughput 0.989629
Reading from 23608: heap size 383 MB, throughput 0.982958
Reading from 23609: heap size 408 MB, throughput 0.986077
Reading from 23608: heap size 386 MB, throughput 0.988015
Reading from 23609: heap size 406 MB, throughput 0.97913
Reading from 23608: heap size 384 MB, throughput 0.98356
Reading from 23609: heap size 409 MB, throughput 0.983731
Equal recommendation: 828 MB each
Reading from 23608: heap size 387 MB, throughput 0.98253
Reading from 23609: heap size 407 MB, throughput 0.983055
Reading from 23608: heap size 383 MB, throughput 0.724762
Reading from 23609: heap size 409 MB, throughput 0.984005
Reading from 23608: heap size 422 MB, throughput 0.980646
Reading from 23609: heap size 411 MB, throughput 0.982589
Reading from 23608: heap size 422 MB, throughput 0.992041
Reading from 23609: heap size 412 MB, throughput 0.981812
Reading from 23608: heap size 424 MB, throughput 0.988301
Reading from 23609: heap size 414 MB, throughput 0.976606
Reading from 23608: heap size 427 MB, throughput 0.991567
Reading from 23609: heap size 415 MB, throughput 0.987126
Equal recommendation: 828 MB each
Reading from 23608: heap size 427 MB, throughput 0.984003
Reading from 23609: heap size 416 MB, throughput 0.948135
Reading from 23609: heap size 417 MB, throughput 0.820682
Reading from 23608: heap size 424 MB, throughput 0.832743
Reading from 23608: heap size 426 MB, throughput 0.834888
Reading from 23609: heap size 420 MB, throughput 0.759342
Reading from 23609: heap size 422 MB, throughput 0.799431
Reading from 23608: heap size 433 MB, throughput 0.774428
Reading from 23608: heap size 435 MB, throughput 0.98669
Reading from 23609: heap size 433 MB, throughput 0.989531
Reading from 23608: heap size 438 MB, throughput 0.992338
Reading from 23609: heap size 433 MB, throughput 0.987543
Reading from 23608: heap size 441 MB, throughput 0.989318
Reading from 23609: heap size 438 MB, throughput 0.9875
Reading from 23608: heap size 441 MB, throughput 0.991673
Reading from 23609: heap size 440 MB, throughput 0.985242
Equal recommendation: 828 MB each
Reading from 23608: heap size 444 MB, throughput 0.985547
Reading from 23609: heap size 440 MB, throughput 0.979926
Reading from 23608: heap size 444 MB, throughput 0.987953
Reading from 23609: heap size 443 MB, throughput 0.985822
Reading from 23608: heap size 446 MB, throughput 0.979338
Reading from 23609: heap size 443 MB, throughput 0.976552
Reading from 23608: heap size 449 MB, throughput 0.983175
Reading from 23609: heap size 445 MB, throughput 0.983214
Reading from 23608: heap size 450 MB, throughput 0.983419
Reading from 23609: heap size 448 MB, throughput 0.990662
Equal recommendation: 828 MB each
Reading from 23609: heap size 449 MB, throughput 0.969342
Reading from 23609: heap size 449 MB, throughput 0.870131
Reading from 23609: heap size 451 MB, throughput 0.870253
Reading from 23608: heap size 453 MB, throughput 0.989557
Reading from 23608: heap size 454 MB, throughput 0.909437
Reading from 23608: heap size 454 MB, throughput 0.872041
Reading from 23608: heap size 457 MB, throughput 0.966815
Reading from 23609: heap size 458 MB, throughput 0.990723
Reading from 23608: heap size 465 MB, throughput 0.98997
Reading from 23609: heap size 459 MB, throughput 0.990923
Reading from 23608: heap size 465 MB, throughput 0.991383
Reading from 23609: heap size 465 MB, throughput 0.991251
Reading from 23608: heap size 465 MB, throughput 0.987369
Reading from 23609: heap size 466 MB, throughput 0.987961
Equal recommendation: 828 MB each
Reading from 23608: heap size 468 MB, throughput 0.984617
Reading from 23609: heap size 466 MB, throughput 0.987784
Reading from 23608: heap size 466 MB, throughput 0.990584
Reading from 23609: heap size 468 MB, throughput 0.98833
Reading from 23608: heap size 468 MB, throughput 0.985095
Reading from 23609: heap size 468 MB, throughput 0.987554
Reading from 23608: heap size 471 MB, throughput 0.986199
Reading from 23609: heap size 469 MB, throughput 0.992608
Equal recommendation: 828 MB each
Reading from 23608: heap size 471 MB, throughput 0.991631
Reading from 23609: heap size 470 MB, throughput 0.972427
Reading from 23609: heap size 471 MB, throughput 0.876941
Reading from 23609: heap size 475 MB, throughput 0.918079
Reading from 23608: heap size 472 MB, throughput 0.974651
Reading from 23608: heap size 473 MB, throughput 0.876694
Reading from 23608: heap size 476 MB, throughput 0.927254
Reading from 23609: heap size 477 MB, throughput 0.990208
Reading from 23608: heap size 479 MB, throughput 0.991111
Reading from 23609: heap size 482 MB, throughput 0.991402
Reading from 23608: heap size 482 MB, throughput 0.991601
Reading from 23609: heap size 484 MB, throughput 0.991046
Reading from 23608: heap size 484 MB, throughput 0.989862
Equal recommendation: 828 MB each
Reading from 23609: heap size 483 MB, throughput 0.989122
Reading from 23608: heap size 483 MB, throughput 0.987312
Reading from 23609: heap size 486 MB, throughput 0.977684
Reading from 23608: heap size 486 MB, throughput 0.980751
Reading from 23608: heap size 487 MB, throughput 0.985845
Reading from 23609: heap size 487 MB, throughput 0.986943
Reading from 23608: heap size 488 MB, throughput 0.985295
Reading from 23609: heap size 488 MB, throughput 0.992138
Reading from 23609: heap size 490 MB, throughput 0.974855
Reading from 23609: heap size 491 MB, throughput 0.855951
Equal recommendation: 828 MB each
Reading from 23608: heap size 491 MB, throughput 0.992752
Reading from 23608: heap size 492 MB, throughput 0.906319
Reading from 23608: heap size 492 MB, throughput 0.857515
Reading from 23609: heap size 495 MB, throughput 0.982727
Reading from 23608: heap size 495 MB, throughput 0.987901
Reading from 23609: heap size 497 MB, throughput 0.993038
Reading from 23608: heap size 502 MB, throughput 0.992344
Reading from 23609: heap size 501 MB, throughput 0.993583
Reading from 23608: heap size 504 MB, throughput 0.990863
Reading from 23609: heap size 502 MB, throughput 0.990834
Equal recommendation: 828 MB each
Reading from 23608: heap size 505 MB, throughput 0.992181
Reading from 23609: heap size 501 MB, throughput 0.988469
Reading from 23608: heap size 508 MB, throughput 0.989365
Reading from 23609: heap size 503 MB, throughput 0.988865
Reading from 23608: heap size 508 MB, throughput 0.9894
Reading from 23609: heap size 505 MB, throughput 0.987462
Reading from 23608: heap size 509 MB, throughput 0.984049
Reading from 23609: heap size 506 MB, throughput 0.986191
Reading from 23609: heap size 508 MB, throughput 0.878725
Reading from 23608: heap size 509 MB, throughput 0.970805
Reading from 23609: heap size 510 MB, throughput 0.961089
Equal recommendation: 828 MB each
Reading from 23608: heap size 514 MB, throughput 0.901631
Reading from 23608: heap size 518 MB, throughput 0.978333
Reading from 23609: heap size 518 MB, throughput 0.993836
Reading from 23608: heap size 520 MB, throughput 0.991657
Reading from 23608: heap size 522 MB, throughput 0.988496
Reading from 23609: heap size 519 MB, throughput 0.988577
Reading from 23608: heap size 524 MB, throughput 0.99074
Reading from 23609: heap size 519 MB, throughput 0.992939
Equal recommendation: 828 MB each
Reading from 23609: heap size 522 MB, throughput 0.988632
Reading from 23608: heap size 522 MB, throughput 0.989706
Reading from 23609: heap size 522 MB, throughput 0.990682
Reading from 23608: heap size 525 MB, throughput 0.991026
Reading from 23609: heap size 523 MB, throughput 0.992504
Reading from 23608: heap size 527 MB, throughput 0.985998
Reading from 23609: heap size 525 MB, throughput 0.970399
Reading from 23609: heap size 526 MB, throughput 0.902847
Reading from 23608: heap size 528 MB, throughput 0.979557
Reading from 23608: heap size 534 MB, throughput 0.90967
Equal recommendation: 828 MB each
Reading from 23609: heap size 531 MB, throughput 0.98946
Reading from 23608: heap size 535 MB, throughput 0.986004
Reading from 23608: heap size 541 MB, throughput 0.989772
Reading from 23609: heap size 532 MB, throughput 0.988965
Reading from 23608: heap size 542 MB, throughput 0.991802
Reading from 23609: heap size 534 MB, throughput 0.990838
Equal recommendation: 828 MB each
Reading from 23608: heap size 543 MB, throughput 0.990718
Reading from 23609: heap size 536 MB, throughput 0.990861
Reading from 23608: heap size 545 MB, throughput 0.989767
Reading from 23609: heap size 535 MB, throughput 0.988475
Reading from 23608: heap size 542 MB, throughput 0.988864
Reading from 23609: heap size 537 MB, throughput 0.985996
Reading from 23609: heap size 539 MB, throughput 0.982217
Reading from 23609: heap size 541 MB, throughput 0.91358
Reading from 23608: heap size 545 MB, throughput 0.992032
Reading from 23608: heap size 547 MB, throughput 0.93668
Equal recommendation: 828 MB each
Reading from 23608: heap size 548 MB, throughput 0.982892
Reading from 23609: heap size 545 MB, throughput 0.99111
Reading from 23608: heap size 554 MB, throughput 0.992782
Reading from 23609: heap size 546 MB, throughput 0.991711
Reading from 23608: heap size 555 MB, throughput 0.992657
Reading from 23609: heap size 549 MB, throughput 0.991421
Equal recommendation: 828 MB each
Reading from 23608: heap size 554 MB, throughput 0.991854
Reading from 23609: heap size 551 MB, throughput 0.990221
Reading from 23608: heap size 556 MB, throughput 0.99022
Reading from 23609: heap size 549 MB, throughput 0.986511
Reading from 23608: heap size 556 MB, throughput 0.98972
Reading from 23609: heap size 551 MB, throughput 0.992544
Reading from 23609: heap size 550 MB, throughput 0.969394
Reading from 23609: heap size 552 MB, throughput 0.943258
Reading from 23608: heap size 557 MB, throughput 0.990798
Equal recommendation: 828 MB each
Reading from 23608: heap size 560 MB, throughput 0.9121
Reading from 23608: heap size 561 MB, throughput 0.988236
Reading from 23609: heap size 559 MB, throughput 0.993933
Reading from 23608: heap size 568 MB, throughput 0.989312
Reading from 23609: heap size 559 MB, throughput 0.897643
Reading from 23608: heap size 569 MB, throughput 0.9947
Reading from 23609: heap size 575 MB, throughput 0.995744
Equal recommendation: 828 MB each
Reading from 23609: heap size 577 MB, throughput 0.993298
Reading from 23608: heap size 569 MB, throughput 0.991316
Reading from 23609: heap size 583 MB, throughput 0.991652
Reading from 23608: heap size 571 MB, throughput 0.990206
Reading from 23609: heap size 583 MB, throughput 0.986738
Reading from 23609: heap size 581 MB, throughput 0.911042
Reading from 23608: heap size 573 MB, throughput 0.993162
Equal recommendation: 828 MB each
Reading from 23608: heap size 573 MB, throughput 0.97163
Reading from 23608: heap size 572 MB, throughput 0.898734
Reading from 23609: heap size 585 MB, throughput 0.984964
Reading from 23609: heap size 591 MB, throughput 0.991762
Reading from 23608: heap size 575 MB, throughput 0.99431
Reading from 23609: heap size 594 MB, throughput 0.991193
Reading from 23608: heap size 579 MB, throughput 0.990547
Equal recommendation: 828 MB each
Reading from 23609: heap size 596 MB, throughput 0.989326
Reading from 23608: heap size 581 MB, throughput 0.992372
Reading from 23609: heap size 597 MB, throughput 0.988469
Reading from 23608: heap size 582 MB, throughput 0.991052
Reading from 23609: heap size 601 MB, throughput 0.989337
Reading from 23609: heap size 604 MB, throughput 0.909834
Reading from 23608: heap size 584 MB, throughput 0.988635
Equal recommendation: 828 MB each
Reading from 23609: heap size 609 MB, throughput 0.988855
Reading from 23608: heap size 585 MB, throughput 0.982598
Reading from 23608: heap size 588 MB, throughput 0.949961
Reading from 23609: heap size 612 MB, throughput 0.99087
Reading from 23608: heap size 594 MB, throughput 0.994733
Reading from 23609: heap size 611 MB, throughput 0.991226
Reading from 23608: heap size 594 MB, throughput 0.993649
Equal recommendation: 828 MB each
Reading from 23609: heap size 615 MB, throughput 0.992268
Reading from 23608: heap size 595 MB, throughput 0.992648
Reading from 23609: heap size 619 MB, throughput 0.988622
Reading from 23608: heap size 597 MB, throughput 0.991815
Reading from 23609: heap size 619 MB, throughput 0.987699
Reading from 23609: heap size 624 MB, throughput 0.942683
Equal recommendation: 828 MB each
Reading from 23608: heap size 598 MB, throughput 0.995762
Reading from 23608: heap size 599 MB, throughput 0.980012
Reading from 23609: heap size 625 MB, throughput 0.992775
Reading from 23608: heap size 598 MB, throughput 0.960522
Reading from 23608: heap size 601 MB, throughput 0.992681
Reading from 23609: heap size 629 MB, throughput 0.993148
Reading from 23608: heap size 604 MB, throughput 0.992667
Reading from 23609: heap size 631 MB, throughput 0.991213
Equal recommendation: 828 MB each
Reading from 23608: heap size 605 MB, throughput 0.991963
Reading from 23609: heap size 629 MB, throughput 0.990839
Reading from 23608: heap size 605 MB, throughput 0.991118
Reading from 23609: heap size 632 MB, throughput 0.993914
Reading from 23609: heap size 634 MB, throughput 0.928254
Client 23609 died
Clients: 1
Reading from 23608: heap size 606 MB, throughput 0.995592
Reading from 23608: heap size 608 MB, throughput 0.948886
Recommendation: one client; give it all the memory
Client 23608 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
