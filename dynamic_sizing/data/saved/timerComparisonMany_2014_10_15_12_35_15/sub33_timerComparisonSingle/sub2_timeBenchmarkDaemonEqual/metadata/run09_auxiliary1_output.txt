economemd
    total memory: 4940 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub33_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run09_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
TimerThread: starting
ReadingThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 17456: heap size 9 MB, throughput 0.987768
Clients: 1
Client 17456 has a minimum heap size of 12 MB
Reading from 17454: heap size 9 MB, throughput 0.992658
Clients: 2
Client 17454 has a minimum heap size of 1223 MB
Reading from 17454: heap size 9 MB, throughput 0.974506
Reading from 17456: heap size 9 MB, throughput 0.98583
Reading from 17454: heap size 9 MB, throughput 0.753745
Reading from 17454: heap size 9 MB, throughput 0.964458
Reading from 17456: heap size 11 MB, throughput 0.979002
Reading from 17454: heap size 11 MB, throughput 0.96876
Reading from 17456: heap size 11 MB, throughput 0.971914
Reading from 17454: heap size 11 MB, throughput 0.977883
Reading from 17454: heap size 17 MB, throughput 0.957726
Reading from 17456: heap size 15 MB, throughput 0.98063
Reading from 17454: heap size 17 MB, throughput 0.523747
Reading from 17456: heap size 15 MB, throughput 0.950294
Reading from 17454: heap size 30 MB, throughput 0.959572
Reading from 17454: heap size 31 MB, throughput 0.910071
Reading from 17454: heap size 34 MB, throughput 0.412441
Reading from 17454: heap size 48 MB, throughput 0.796104
Reading from 17454: heap size 49 MB, throughput 0.880144
Reading from 17456: heap size 24 MB, throughput 0.932343
Reading from 17454: heap size 51 MB, throughput 0.190623
Reading from 17454: heap size 75 MB, throughput 0.786051
Reading from 17456: heap size 29 MB, throughput 0.989052
Reading from 17454: heap size 75 MB, throughput 0.372747
Reading from 17454: heap size 101 MB, throughput 0.757694
Reading from 17456: heap size 36 MB, throughput 0.987727
Reading from 17454: heap size 102 MB, throughput 0.652758
Reading from 17454: heap size 103 MB, throughput 0.605448
Reading from 17456: heap size 43 MB, throughput 0.982722
Reading from 17454: heap size 106 MB, throughput 0.68585
Reading from 17456: heap size 44 MB, throughput 0.981061
Reading from 17456: heap size 45 MB, throughput 0.943438
Reading from 17456: heap size 52 MB, throughput 0.913671
Reading from 17454: heap size 110 MB, throughput 0.17584
Reading from 17454: heap size 140 MB, throughput 0.603708
Reading from 17456: heap size 52 MB, throughput 0.963626
Reading from 17454: heap size 145 MB, throughput 0.636445
Reading from 17454: heap size 148 MB, throughput 0.666887
Reading from 17456: heap size 66 MB, throughput 0.98246
Reading from 17454: heap size 158 MB, throughput 0.645988
Reading from 17456: heap size 66 MB, throughput 0.770099
Reading from 17454: heap size 160 MB, throughput 0.160689
Reading from 17456: heap size 84 MB, throughput 0.993146
Reading from 17454: heap size 194 MB, throughput 0.561531
Reading from 17454: heap size 202 MB, throughput 0.630919
Reading from 17454: heap size 205 MB, throughput 0.716509
Reading from 17456: heap size 86 MB, throughput 0.995996
Reading from 17454: heap size 208 MB, throughput 0.665963
Reading from 17454: heap size 212 MB, throughput 0.674277
Reading from 17454: heap size 220 MB, throughput 0.553954
Reading from 17454: heap size 224 MB, throughput 0.507958
Reading from 17456: heap size 95 MB, throughput 0.996858
Reading from 17454: heap size 235 MB, throughput 0.110358
Reading from 17454: heap size 280 MB, throughput 0.506576
Reading from 17456: heap size 97 MB, throughput 0.996169
Reading from 17454: heap size 286 MB, throughput 0.129498
Reading from 17454: heap size 334 MB, throughput 0.584377
Reading from 17454: heap size 281 MB, throughput 0.614752
Reading from 17454: heap size 330 MB, throughput 0.629913
Reading from 17454: heap size 332 MB, throughput 0.685295
Reading from 17456: heap size 104 MB, throughput 0.99575
Reading from 17454: heap size 335 MB, throughput 0.67618
Reading from 17454: heap size 340 MB, throughput 0.62115
Reading from 17454: heap size 343 MB, throughput 0.454528
Reading from 17454: heap size 352 MB, throughput 0.538814
Reading from 17454: heap size 359 MB, throughput 0.509528
Reading from 17456: heap size 106 MB, throughput 0.995859
Reading from 17454: heap size 367 MB, throughput 0.0923827
Reading from 17454: heap size 420 MB, throughput 0.463451
Reading from 17454: heap size 425 MB, throughput 0.612288
Reading from 17456: heap size 113 MB, throughput 0.994802
Reading from 17454: heap size 423 MB, throughput 0.568666
Reading from 17454: heap size 425 MB, throughput 0.57261
Reading from 17454: heap size 430 MB, throughput 0.613536
Reading from 17456: heap size 114 MB, throughput 0.996555
Reading from 17454: heap size 430 MB, throughput 0.103576
Reading from 17454: heap size 488 MB, throughput 0.458453
Reading from 17454: heap size 488 MB, throughput 0.566257
Equal recommendation: 2470 MB each
Reading from 17454: heap size 487 MB, throughput 0.519167
Reading from 17456: heap size 120 MB, throughput 0.997112
Reading from 17454: heap size 488 MB, throughput 0.437246
Reading from 17454: heap size 494 MB, throughput 0.513775
Reading from 17456: heap size 121 MB, throughput 0.996701
Reading from 17454: heap size 496 MB, throughput 0.115862
Reading from 17454: heap size 559 MB, throughput 0.475072
Reading from 17456: heap size 126 MB, throughput 0.996373
Reading from 17454: heap size 562 MB, throughput 0.553731
Reading from 17454: heap size 563 MB, throughput 0.599999
Reading from 17454: heap size 564 MB, throughput 0.628142
Reading from 17454: heap size 569 MB, throughput 0.614404
Reading from 17454: heap size 576 MB, throughput 0.594665
Reading from 17456: heap size 126 MB, throughput 0.995617
Reading from 17454: heap size 582 MB, throughput 0.495653
Reading from 17456: heap size 130 MB, throughput 0.996249
Reading from 17454: heap size 591 MB, throughput 0.0950884
Reading from 17454: heap size 656 MB, throughput 0.373473
Reading from 17456: heap size 130 MB, throughput 0.99684
Reading from 17454: heap size 666 MB, throughput 0.794255
Reading from 17456: heap size 134 MB, throughput 0.997919
Reading from 17454: heap size 665 MB, throughput 0.837837
Reading from 17456: heap size 134 MB, throughput 0.99677
Reading from 17454: heap size 671 MB, throughput 0.840428
Reading from 17454: heap size 669 MB, throughput 0.757739
Reading from 17456: heap size 137 MB, throughput 0.997776
Reading from 17456: heap size 137 MB, throughput 0.996717
Reading from 17454: heap size 688 MB, throughput 0.0532919
Reading from 17454: heap size 765 MB, throughput 0.402834
Reading from 17454: heap size 772 MB, throughput 0.604098
Reading from 17454: heap size 765 MB, throughput 0.307238
Reading from 17454: heap size 678 MB, throughput 0.328858
Reading from 17456: heap size 140 MB, throughput 0.99706
Reading from 17454: heap size 753 MB, throughput 0.0424751
Reading from 17456: heap size 140 MB, throughput 0.997675
Reading from 17454: heap size 839 MB, throughput 0.203512
Equal recommendation: 2470 MB each
Reading from 17454: heap size 840 MB, throughput 0.693244
Reading from 17454: heap size 842 MB, throughput 0.897699
Reading from 17454: heap size 849 MB, throughput 0.692722
Reading from 17454: heap size 850 MB, throughput 0.659954
Reading from 17454: heap size 847 MB, throughput 0.678032
Reading from 17456: heap size 143 MB, throughput 0.998771
Reading from 17456: heap size 143 MB, throughput 0.997833
Reading from 17454: heap size 850 MB, throughput 0.13176
Reading from 17454: heap size 941 MB, throughput 0.554704
Reading from 17456: heap size 146 MB, throughput 0.998448
Reading from 17454: heap size 942 MB, throughput 0.944091
Reading from 17454: heap size 948 MB, throughput 0.862233
Reading from 17454: heap size 949 MB, throughput 0.954368
Reading from 17454: heap size 942 MB, throughput 0.897944
Reading from 17454: heap size 749 MB, throughput 0.933757
Reading from 17456: heap size 146 MB, throughput 0.997524
Reading from 17454: heap size 924 MB, throughput 0.828823
Reading from 17454: heap size 796 MB, throughput 0.731012
Reading from 17454: heap size 903 MB, throughput 0.717659
Reading from 17456: heap size 147 MB, throughput 0.994276
Reading from 17454: heap size 801 MB, throughput 0.669684
Reading from 17454: heap size 890 MB, throughput 0.69551
Reading from 17454: heap size 800 MB, throughput 0.74045
Reading from 17456: heap size 148 MB, throughput 0.992096
Reading from 17454: heap size 884 MB, throughput 0.79178
Reading from 17454: heap size 891 MB, throughput 0.728792
Reading from 17454: heap size 879 MB, throughput 0.784373
Reading from 17454: heap size 886 MB, throughput 0.770258
Reading from 17456: heap size 150 MB, throughput 0.996862
Reading from 17454: heap size 880 MB, throughput 0.858663
Reading from 17456: heap size 151 MB, throughput 0.997351
Reading from 17454: heap size 885 MB, throughput 0.985059
Reading from 17456: heap size 155 MB, throughput 0.99759
Reading from 17454: heap size 886 MB, throughput 0.963819
Reading from 17454: heap size 887 MB, throughput 0.776578
Reading from 17454: heap size 893 MB, throughput 0.82363
Reading from 17454: heap size 893 MB, throughput 0.817601
Reading from 17454: heap size 899 MB, throughput 0.829969
Reading from 17456: heap size 155 MB, throughput 0.997378
Reading from 17454: heap size 899 MB, throughput 0.82212
Reading from 17454: heap size 904 MB, throughput 0.84333
Reading from 17454: heap size 905 MB, throughput 0.835602
Reading from 17454: heap size 908 MB, throughput 0.842581
Reading from 17456: heap size 159 MB, throughput 0.996926
Reading from 17454: heap size 910 MB, throughput 0.904568
Reading from 17454: heap size 911 MB, throughput 0.923234
Equal recommendation: 2470 MB each
Reading from 17454: heap size 914 MB, throughput 0.796944
Reading from 17456: heap size 159 MB, throughput 0.995642
Reading from 17456: heap size 162 MB, throughput 0.995165
Reading from 17454: heap size 920 MB, throughput 0.0920987
Reading from 17454: heap size 1007 MB, throughput 0.573375
Reading from 17456: heap size 162 MB, throughput 0.996936
Reading from 17454: heap size 1029 MB, throughput 0.807098
Reading from 17454: heap size 1034 MB, throughput 0.787353
Reading from 17454: heap size 1036 MB, throughput 0.809989
Reading from 17454: heap size 1038 MB, throughput 0.787844
Reading from 17454: heap size 1041 MB, throughput 0.785215
Reading from 17454: heap size 1044 MB, throughput 0.746203
Reading from 17456: heap size 165 MB, throughput 0.997447
Reading from 17454: heap size 1054 MB, throughput 0.781069
Reading from 17454: heap size 1055 MB, throughput 0.79108
Reading from 17456: heap size 166 MB, throughput 0.996242
Reading from 17456: heap size 168 MB, throughput 0.997019
Reading from 17456: heap size 169 MB, throughput 0.99707
Reading from 17454: heap size 1068 MB, throughput 0.983939
Reading from 17456: heap size 172 MB, throughput 0.997241
Reading from 17456: heap size 172 MB, throughput 0.997526
Equal recommendation: 2470 MB each
Reading from 17456: heap size 175 MB, throughput 0.998136
Reading from 17454: heap size 1072 MB, throughput 0.983448
Reading from 17456: heap size 175 MB, throughput 0.997751
Reading from 17456: heap size 178 MB, throughput 0.998125
Reading from 17456: heap size 178 MB, throughput 0.993244
Reading from 17456: heap size 180 MB, throughput 0.99562
Reading from 17454: heap size 1084 MB, throughput 0.979812
Reading from 17456: heap size 181 MB, throughput 0.997223
Reading from 17456: heap size 185 MB, throughput 0.997858
Reading from 17456: heap size 185 MB, throughput 0.99744
Reading from 17454: heap size 1087 MB, throughput 0.978684
Reading from 17456: heap size 187 MB, throughput 0.998109
Reading from 17456: heap size 188 MB, throughput 0.997599
Equal recommendation: 2470 MB each
Reading from 17454: heap size 1086 MB, throughput 0.9788
Reading from 17456: heap size 191 MB, throughput 0.998041
Reading from 17456: heap size 191 MB, throughput 0.996822
Reading from 17456: heap size 193 MB, throughput 0.997868
Reading from 17454: heap size 1091 MB, throughput 0.980398
Reading from 17456: heap size 193 MB, throughput 0.9975
Reading from 17456: heap size 196 MB, throughput 0.997946
Reading from 17456: heap size 196 MB, throughput 0.997719
Reading from 17454: heap size 1088 MB, throughput 0.979212
Reading from 17456: heap size 199 MB, throughput 0.997223
Reading from 17456: heap size 199 MB, throughput 0.99756
Equal recommendation: 2470 MB each
Reading from 17456: heap size 202 MB, throughput 0.996452
Reading from 17456: heap size 202 MB, throughput 0.991831
Reading from 17454: heap size 1091 MB, throughput 0.967273
Reading from 17456: heap size 204 MB, throughput 0.996511
Reading from 17456: heap size 205 MB, throughput 0.997808
Reading from 17456: heap size 209 MB, throughput 0.99835
Reading from 17454: heap size 1095 MB, throughput 0.97849
Reading from 17456: heap size 210 MB, throughput 0.997566
Reading from 17456: heap size 213 MB, throughput 0.998154
Reading from 17454: heap size 1097 MB, throughput 0.977536
Reading from 17456: heap size 203 MB, throughput 0.997376
Reading from 17456: heap size 194 MB, throughput 0.99825
Equal recommendation: 2470 MB each
Reading from 17456: heap size 186 MB, throughput 0.995593
Reading from 17454: heap size 1101 MB, throughput 0.979491
Reading from 17456: heap size 178 MB, throughput 0.994475
Reading from 17456: heap size 181 MB, throughput 0.997739
Reading from 17456: heap size 174 MB, throughput 0.997498
Reading from 17456: heap size 166 MB, throughput 0.997944
Reading from 17454: heap size 1104 MB, throughput 0.973167
Reading from 17456: heap size 159 MB, throughput 0.997418
Reading from 17456: heap size 153 MB, throughput 0.901866
Reading from 17456: heap size 149 MB, throughput 0.996364
Reading from 17456: heap size 153 MB, throughput 0.984586
Reading from 17456: heap size 161 MB, throughput 0.991633
Reading from 17454: heap size 1108 MB, throughput 0.977758
Reading from 17456: heap size 168 MB, throughput 0.997455
Equal recommendation: 2470 MB each
Reading from 17456: heap size 172 MB, throughput 0.997676
Reading from 17456: heap size 182 MB, throughput 0.998288
Reading from 17454: heap size 1112 MB, throughput 0.976904
Reading from 17456: heap size 185 MB, throughput 0.998337
Reading from 17456: heap size 190 MB, throughput 0.998756
Reading from 17456: heap size 194 MB, throughput 0.998234
Reading from 17454: heap size 1116 MB, throughput 0.977267
Reading from 17456: heap size 194 MB, throughput 0.998539
Reading from 17456: heap size 197 MB, throughput 0.998044
Reading from 17456: heap size 197 MB, throughput 0.997195
Equal recommendation: 2470 MB each
Reading from 17454: heap size 1121 MB, throughput 0.976844
Reading from 17456: heap size 201 MB, throughput 0.998429
Reading from 17456: heap size 201 MB, throughput 0.99781
Reading from 17456: heap size 204 MB, throughput 0.998244
Reading from 17454: heap size 1125 MB, throughput 0.976481
Reading from 17456: heap size 204 MB, throughput 0.99825
Reading from 17456: heap size 207 MB, throughput 0.997206
Reading from 17456: heap size 207 MB, throughput 0.990734
Reading from 17456: heap size 210 MB, throughput 0.996614
Reading from 17456: heap size 211 MB, throughput 0.996571
Reading from 17454: heap size 1130 MB, throughput 0.651203
Reading from 17456: heap size 218 MB, throughput 0.997925
Equal recommendation: 2470 MB each
Reading from 17456: heap size 218 MB, throughput 0.997637
Reading from 17456: heap size 223 MB, throughput 0.995701
Reading from 17454: heap size 1230 MB, throughput 0.968076
Reading from 17456: heap size 224 MB, throughput 0.997288
Reading from 17456: heap size 229 MB, throughput 0.998279
Reading from 17456: heap size 229 MB, throughput 0.997319
Reading from 17454: heap size 1232 MB, throughput 0.989899
Reading from 17456: heap size 235 MB, throughput 0.998201
Reading from 17456: heap size 235 MB, throughput 0.997759
Equal recommendation: 2470 MB each
Reading from 17454: heap size 1239 MB, throughput 0.98899
Reading from 17456: heap size 240 MB, throughput 0.997813
Reading from 17456: heap size 240 MB, throughput 0.997879
Reading from 17456: heap size 244 MB, throughput 0.994563
Reading from 17456: heap size 244 MB, throughput 0.993965
Reading from 17454: heap size 1241 MB, throughput 0.986467
Reading from 17456: heap size 251 MB, throughput 0.997639
Reading from 17456: heap size 251 MB, throughput 0.998292
Reading from 17454: heap size 1239 MB, throughput 0.985744
Reading from 17456: heap size 257 MB, throughput 0.99845
Equal recommendation: 2470 MB each
Reading from 17456: heap size 258 MB, throughput 0.997727
Reading from 17454: heap size 1148 MB, throughput 0.9811
Reading from 17456: heap size 262 MB, throughput 0.998524
Reading from 17456: heap size 263 MB, throughput 0.998047
Reading from 17454: heap size 1231 MB, throughput 0.98378
Reading from 17456: heap size 267 MB, throughput 0.997484
Reading from 17456: heap size 267 MB, throughput 0.99793
Reading from 17456: heap size 272 MB, throughput 0.997837
Reading from 17454: heap size 1164 MB, throughput 0.981123
Equal recommendation: 2470 MB each
Reading from 17456: heap size 272 MB, throughput 0.993483
Reading from 17456: heap size 277 MB, throughput 0.991734
Reading from 17456: heap size 277 MB, throughput 0.997394
Reading from 17454: heap size 1222 MB, throughput 0.981979
Reading from 17456: heap size 285 MB, throughput 0.998362
Reading from 17456: heap size 287 MB, throughput 0.997994
Reading from 17454: heap size 1228 MB, throughput 0.980952
Reading from 17456: heap size 294 MB, throughput 0.998308
Equal recommendation: 2470 MB each
Reading from 17456: heap size 294 MB, throughput 0.998287
Reading from 17454: heap size 1229 MB, throughput 0.981277
Reading from 17456: heap size 299 MB, throughput 0.998463
Reading from 17456: heap size 300 MB, throughput 0.997627
Reading from 17454: heap size 1229 MB, throughput 0.978656
Reading from 17456: heap size 306 MB, throughput 0.998477
Reading from 17456: heap size 306 MB, throughput 0.99724
Reading from 17454: heap size 1231 MB, throughput 0.975466
Reading from 17456: heap size 311 MB, throughput 0.993899
Equal recommendation: 2470 MB each
Reading from 17456: heap size 311 MB, throughput 0.998072
Reading from 17454: heap size 1234 MB, throughput 0.979189
Reading from 17456: heap size 318 MB, throughput 0.99778
Reading from 17456: heap size 319 MB, throughput 0.997551
Reading from 17454: heap size 1237 MB, throughput 0.976682
Reading from 17456: heap size 326 MB, throughput 0.998021
Reading from 17456: heap size 326 MB, throughput 0.99789
Equal recommendation: 2470 MB each
Reading from 17454: heap size 1243 MB, throughput 0.975121
Reading from 17456: heap size 332 MB, throughput 0.998323
Reading from 17456: heap size 333 MB, throughput 0.998267
Reading from 17454: heap size 1248 MB, throughput 0.977463
Reading from 17456: heap size 338 MB, throughput 0.9975
Reading from 17456: heap size 338 MB, throughput 0.996856
Reading from 17454: heap size 1253 MB, throughput 0.975691
Reading from 17456: heap size 345 MB, throughput 0.998506
Equal recommendation: 2470 MB each
Reading from 17456: heap size 345 MB, throughput 0.998019
Reading from 17454: heap size 1258 MB, throughput 0.977796
Reading from 17456: heap size 352 MB, throughput 0.998311
Reading from 17456: heap size 352 MB, throughput 0.998349
Reading from 17454: heap size 1261 MB, throughput 0.975291
Reading from 17456: heap size 357 MB, throughput 0.998057
Equal recommendation: 2470 MB each
Reading from 17456: heap size 357 MB, throughput 0.997937
Reading from 17454: heap size 1266 MB, throughput 0.977207
Reading from 17456: heap size 363 MB, throughput 0.99818
Reading from 17456: heap size 363 MB, throughput 0.991488
Reading from 17454: heap size 1268 MB, throughput 0.972969
Reading from 17456: heap size 369 MB, throughput 0.99858
Reading from 17456: heap size 369 MB, throughput 0.998025
Reading from 17454: heap size 1273 MB, throughput 0.978121
Equal recommendation: 2470 MB each
Reading from 17456: heap size 378 MB, throughput 0.998554
Reading from 17456: heap size 378 MB, throughput 0.997953
Reading from 17454: heap size 1273 MB, throughput 0.676186
Reading from 17456: heap size 384 MB, throughput 0.998547
Reading from 17456: heap size 385 MB, throughput 0.998197
Equal recommendation: 2470 MB each
Reading from 17456: heap size 392 MB, throughput 0.941593
Reading from 17454: heap size 1389 MB, throughput 0.963425
Reading from 17456: heap size 400 MB, throughput 0.997953
Reading from 17454: heap size 1389 MB, throughput 0.987158
Reading from 17456: heap size 408 MB, throughput 0.998853
Reading from 17456: heap size 408 MB, throughput 0.99911
Equal recommendation: 2470 MB each
Reading from 17456: heap size 415 MB, throughput 0.999038
Reading from 17456: heap size 416 MB, throughput 0.999014
Reading from 17456: heap size 423 MB, throughput 0.999132
Reading from 17456: heap size 423 MB, throughput 0.996637
Reading from 17454: heap size 1398 MB, throughput 0.992424
Reading from 17456: heap size 429 MB, throughput 0.998083
Equal recommendation: 2470 MB each
Reading from 17456: heap size 430 MB, throughput 0.998542
Reading from 17456: heap size 438 MB, throughput 0.998831
Reading from 17456: heap size 438 MB, throughput 0.998455
Equal recommendation: 2470 MB each
Reading from 17456: heap size 446 MB, throughput 0.998603
Reading from 17454: heap size 1400 MB, throughput 0.992873
Reading from 17456: heap size 446 MB, throughput 0.998505
Reading from 17456: heap size 453 MB, throughput 0.995458
Reading from 17456: heap size 453 MB, throughput 0.998241
Reading from 17454: heap size 1400 MB, throughput 0.986133
Reading from 17454: heap size 1402 MB, throughput 0.873136
Reading from 17456: heap size 463 MB, throughput 0.998543
Equal recommendation: 2470 MB each
Reading from 17454: heap size 1416 MB, throughput 0.783676
Reading from 17454: heap size 1428 MB, throughput 0.710404
Reading from 17454: heap size 1466 MB, throughput 0.744012
Reading from 17454: heap size 1483 MB, throughput 0.727836
Reading from 17454: heap size 1511 MB, throughput 0.755622
Reading from 17454: heap size 1521 MB, throughput 0.744225
Reading from 17456: heap size 465 MB, throughput 0.998485
Reading from 17454: heap size 1553 MB, throughput 0.967712
Reading from 17456: heap size 473 MB, throughput 0.998055
Reading from 17454: heap size 1557 MB, throughput 0.984317
Reading from 17456: heap size 474 MB, throughput 0.998456
Equal recommendation: 2470 MB each
Reading from 17456: heap size 482 MB, throughput 0.995714
Reading from 17456: heap size 483 MB, throughput 0.998559
Reading from 17454: heap size 1543 MB, throughput 0.985052
Reading from 17456: heap size 494 MB, throughput 0.997568
Reading from 17454: heap size 1559 MB, throughput 0.982823
Equal recommendation: 2470 MB each
Reading from 17456: heap size 495 MB, throughput 0.998311
Reading from 17456: heap size 505 MB, throughput 0.998533
Reading from 17454: heap size 1547 MB, throughput 0.983348
Reading from 17456: heap size 506 MB, throughput 0.998202
Client 17456 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 17454: heap size 1559 MB, throughput 0.871115
Reading from 17454: heap size 1512 MB, throughput 0.996054
Recommendation: one client; give it all the memory
Reading from 17454: heap size 1521 MB, throughput 0.994246
Reading from 17454: heap size 1544 MB, throughput 0.993202
Recommendation: one client; give it all the memory
Reading from 17454: heap size 1553 MB, throughput 0.992088
Reading from 17454: heap size 1560 MB, throughput 0.991271
Recommendation: one client; give it all the memory
Reading from 17454: heap size 1563 MB, throughput 0.989832
Reading from 17454: heap size 1548 MB, throughput 0.989605
Recommendation: one client; give it all the memory
Reading from 17454: heap size 1397 MB, throughput 0.988017
Reading from 17454: heap size 1531 MB, throughput 0.987478
Recommendation: one client; give it all the memory
Reading from 17454: heap size 1426 MB, throughput 0.986132
Recommendation: one client; give it all the memory
Reading from 17454: heap size 1528 MB, throughput 0.985166
Reading from 17454: heap size 1533 MB, throughput 0.98393
Recommendation: one client; give it all the memory
Reading from 17454: heap size 1536 MB, throughput 0.983073
Reading from 17454: heap size 1539 MB, throughput 0.982405
Recommendation: one client; give it all the memory
Reading from 17454: heap size 1544 MB, throughput 0.980806
Reading from 17454: heap size 1552 MB, throughput 0.978976
Recommendation: one client; give it all the memory
Reading from 17454: heap size 1563 MB, throughput 0.979902
Recommendation: one client; give it all the memory
Reading from 17454: heap size 1570 MB, throughput 0.978845
Reading from 17454: heap size 1581 MB, throughput 0.979665
Recommendation: one client; give it all the memory
Reading from 17454: heap size 1586 MB, throughput 0.979316
Reading from 17454: heap size 1597 MB, throughput 0.980848
Recommendation: one client; give it all the memory
Reading from 17454: heap size 1599 MB, throughput 0.980933
Recommendation: one client; give it all the memory
Reading from 17454: heap size 1610 MB, throughput 0.981072
Recommendation: one client; give it all the memory
Reading from 17454: heap size 1610 MB, throughput 0.988431
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 17454: heap size 1598 MB, throughput 0.993387
Reading from 17454: heap size 1620 MB, throughput 0.879715
Reading from 17454: heap size 1603 MB, throughput 0.776738
Reading from 17454: heap size 1626 MB, throughput 0.768381
Reading from 17454: heap size 1661 MB, throughput 0.794507
Reading from 17454: heap size 1673 MB, throughput 0.837008
Client 17454 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
