#include <algorithm>
#include "AdaptiveWeightedAverage.hpp"

economem::AdaptiveWeightedAverage::AdaptiveWeightedAverage(unsigned weight) : m_Average(0), m_LastSample(0), m_SampleCount(0), m_Weight(weight), m_IsOld(false)
{
}

economem::AdaptiveWeightedAverage::AdaptiveWeightedAverage(unsigned weight, double firstSample) : m_Average(0), m_LastSample(0), m_SampleCount(0), m_Weight(weight), m_IsOld(false)
{
    sample(firstSample);
}

economem::AdaptiveWeightedAverage& economem::AdaptiveWeightedAverage::operator=(const economem::AdaptiveWeightedAverage& a)
{
    m_Average = a.m_Average;
    m_LastSample = a.m_LastSample;
    m_SampleCount = a.m_SampleCount;
    m_Weight = a.m_Weight;
    m_IsOld = a.m_IsOld;
    return *this;
}

void economem::AdaptiveWeightedAverage::incrementCount()
{
    m_SampleCount++;
    if (!m_IsOld && m_SampleCount > OLD_THRESHOLD)
        m_IsOld = true;
}

void economem::AdaptiveWeightedAverage::setAverage(double avg)
{
    m_Average = avg;
}

double economem::AdaptiveWeightedAverage::average() const
{
    return m_Average;
}

unsigned int economem::AdaptiveWeightedAverage::weight() const
{
    return m_Weight;
}

unsigned int economem::AdaptiveWeightedAverage::count() const
{
    return m_SampleCount;
}

double economem::AdaptiveWeightedAverage::lastSample() const
{
    return m_LastSample;
}

bool economem::AdaptiveWeightedAverage::isOld() const
{
    return m_IsOld;
}

double economem::AdaptiveWeightedAverage::expAvg(double avg, double sample, unsigned int weight)
{
    return (100.0f - weight) * avg / 100.0f + weight * sample / 100.0f;
}

unsigned long long economem::AdaptiveWeightedAverage::expAvg(unsigned long long avg, unsigned long long sample, unsigned int weight)
{
    // Convert to double and back to avoid integer overflow.
    return (unsigned long long)expAvg((double)avg, (double)sample, weight);
}

double economem::AdaptiveWeightedAverage::computeAdaptiveAverage(double newSample, double average)
{
    // We smooth the samples by not using weight() directly until we've
    // had enough data to make it meaningful. We'd like the first weight
    // used to be 1, the second to be 1/2, etc until we have
    // OLD_THRESHOLD/weight samples.
    unsigned int countWeight = 0;

    // Avoid division by zero if the counter wraps (7158457)
    if (!isOld())
        countWeight = OLD_THRESHOLD/count();

    unsigned int adaptiveWeight = std::max(weight(), countWeight);

    return expAvg(average, newSample, adaptiveWeight);
}

void economem::AdaptiveWeightedAverage::sample(double newSample)
{
    incrementCount();

    // Compute the new weighted average
    double newAvg = computeAdaptiveAverage(newSample, average());
    setAverage(newAvg);
    m_LastSample = newSample;
}
