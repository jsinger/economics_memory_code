import Economem.Files
import Economem.PostprocessCmdlineLauncher
import Economem.StaggeredStart

main :: IO ()
main = postprocess (asDir :: FilePath -> StaggeredStartDir)
