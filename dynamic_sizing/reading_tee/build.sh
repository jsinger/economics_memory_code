#!/bin/bash
# For the static analyser to work, scons has to be wrapped in a scan-build invocation

if [ "${1}" = 'clean' ]; then
    scons -f SConstruct.main -Q -c
else
	scan-build -V --use-cc=clang --use-c++=clang++ scons -f SConstruct.main -Q "${@}"
fi
