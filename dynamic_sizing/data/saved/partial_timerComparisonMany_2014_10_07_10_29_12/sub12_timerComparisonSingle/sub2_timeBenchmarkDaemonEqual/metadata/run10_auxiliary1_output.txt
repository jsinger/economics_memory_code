economemd
    total memory: 4940 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub12_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run10_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 26925: heap size 9 MB, throughput 0.982087
Clients: 1
Client 26925 has a minimum heap size of 12 MB
Reading from 26924: heap size 9 MB, throughput 0.988552
Clients: 2
Client 26924 has a minimum heap size of 1223 MB
Reading from 26924: heap size 9 MB, throughput 0.985104
Reading from 26925: heap size 9 MB, throughput 0.989689
Reading from 26924: heap size 11 MB, throughput 0.971818
Reading from 26925: heap size 11 MB, throughput 0.979529
Reading from 26925: heap size 11 MB, throughput 0.939401
Reading from 26924: heap size 11 MB, throughput 0.984937
Reading from 26925: heap size 15 MB, throughput 0.987447
Reading from 26924: heap size 15 MB, throughput 0.79468
Reading from 26924: heap size 18 MB, throughput 0.963486
Reading from 26925: heap size 15 MB, throughput 0.979415
Reading from 26924: heap size 23 MB, throughput 0.849017
Reading from 26924: heap size 28 MB, throughput 0.420568
Reading from 26924: heap size 38 MB, throughput 0.967247
Reading from 26925: heap size 24 MB, throughput 0.933041
Reading from 26924: heap size 40 MB, throughput 0.877035
Reading from 26924: heap size 42 MB, throughput 0.777645
Reading from 26924: heap size 44 MB, throughput 0.788622
Reading from 26924: heap size 48 MB, throughput 0.31662
Reading from 26925: heap size 29 MB, throughput 0.973665
Reading from 26924: heap size 63 MB, throughput 0.798442
Reading from 26924: heap size 68 MB, throughput 0.676258
Reading from 26924: heap size 71 MB, throughput 0.237567
Reading from 26925: heap size 37 MB, throughput 0.989521
Reading from 26924: heap size 92 MB, throughput 0.728744
Reading from 26924: heap size 96 MB, throughput 0.725693
Reading from 26924: heap size 98 MB, throughput 0.670548
Reading from 26925: heap size 44 MB, throughput 0.95665
Reading from 26925: heap size 45 MB, throughput 0.987935
Reading from 26924: heap size 101 MB, throughput 0.243696
Reading from 26924: heap size 131 MB, throughput 0.609778
Reading from 26925: heap size 45 MB, throughput 0.986508
Reading from 26924: heap size 133 MB, throughput 0.687073
Reading from 26924: heap size 137 MB, throughput 0.818199
Reading from 26925: heap size 52 MB, throughput 0.990548
Reading from 26924: heap size 141 MB, throughput 0.596878
Reading from 26925: heap size 53 MB, throughput 0.989361
Reading from 26925: heap size 60 MB, throughput 0.988547
Reading from 26924: heap size 145 MB, throughput 0.097037
Reading from 26925: heap size 61 MB, throughput 0.862376
Reading from 26924: heap size 180 MB, throughput 0.675089
Reading from 26924: heap size 188 MB, throughput 0.59791
Reading from 26925: heap size 77 MB, throughput 0.990096
Reading from 26924: heap size 190 MB, throughput 0.523694
Reading from 26924: heap size 194 MB, throughput 0.547481
Reading from 26925: heap size 77 MB, throughput 0.994043
Reading from 26924: heap size 201 MB, throughput 0.657627
Reading from 26925: heap size 85 MB, throughput 0.997769
Reading from 26924: heap size 206 MB, throughput 0.10294
Reading from 26924: heap size 243 MB, throughput 0.596389
Reading from 26924: heap size 251 MB, throughput 0.448594
Reading from 26924: heap size 252 MB, throughput 0.683454
Reading from 26925: heap size 86 MB, throughput 0.997051
Reading from 26924: heap size 257 MB, throughput 0.144744
Reading from 26924: heap size 297 MB, throughput 0.664432
Reading from 26924: heap size 300 MB, throughput 0.70712
Reading from 26925: heap size 92 MB, throughput 0.996823
Reading from 26924: heap size 303 MB, throughput 0.632856
Reading from 26924: heap size 305 MB, throughput 0.617877
Reading from 26924: heap size 310 MB, throughput 0.622794
Reading from 26924: heap size 314 MB, throughput 0.598108
Reading from 26924: heap size 324 MB, throughput 0.565684
Reading from 26925: heap size 93 MB, throughput 0.997219
Reading from 26924: heap size 331 MB, throughput 0.575256
Reading from 26924: heap size 342 MB, throughput 0.469979
Reading from 26925: heap size 98 MB, throughput 0.995898
Reading from 26924: heap size 348 MB, throughput 0.0867848
Reading from 26924: heap size 400 MB, throughput 0.409598
Reading from 26924: heap size 408 MB, throughput 0.582788
Reading from 26925: heap size 99 MB, throughput 0.99492
Reading from 26925: heap size 105 MB, throughput 0.995179
Reading from 26924: heap size 408 MB, throughput 0.0811268
Reading from 26924: heap size 457 MB, throughput 0.481554
Reading from 26924: heap size 387 MB, throughput 0.67989
Reading from 26924: heap size 451 MB, throughput 0.579837
Equal recommendation: 2470 MB each
Reading from 26924: heap size 454 MB, throughput 0.635573
Reading from 26925: heap size 105 MB, throughput 0.9966
Reading from 26924: heap size 455 MB, throughput 0.504009
Reading from 26924: heap size 459 MB, throughput 0.438718
Reading from 26924: heap size 466 MB, throughput 0.446733
Reading from 26925: heap size 109 MB, throughput 0.995881
Reading from 26925: heap size 110 MB, throughput 0.996325
Reading from 26924: heap size 471 MB, throughput 0.0915978
Reading from 26924: heap size 531 MB, throughput 0.489251
Reading from 26924: heap size 533 MB, throughput 0.553588
Reading from 26924: heap size 531 MB, throughput 0.597531
Reading from 26925: heap size 115 MB, throughput 0.996845
Reading from 26924: heap size 533 MB, throughput 0.538614
Reading from 26925: heap size 115 MB, throughput 0.992951
Reading from 26924: heap size 538 MB, throughput 0.100021
Reading from 26924: heap size 595 MB, throughput 0.469634
Reading from 26925: heap size 119 MB, throughput 0.991293
Reading from 26924: heap size 607 MB, throughput 0.630004
Reading from 26924: heap size 608 MB, throughput 0.564614
Reading from 26924: heap size 614 MB, throughput 0.558131
Reading from 26924: heap size 620 MB, throughput 0.441295
Reading from 26925: heap size 119 MB, throughput 0.995014
Reading from 26925: heap size 125 MB, throughput 0.995771
Reading from 26924: heap size 626 MB, throughput 0.0941885
Reading from 26925: heap size 126 MB, throughput 0.996016
Reading from 26924: heap size 692 MB, throughput 0.819241
Reading from 26925: heap size 131 MB, throughput 0.998147
Reading from 26924: heap size 699 MB, throughput 0.869469
Reading from 26925: heap size 131 MB, throughput 0.9972
Reading from 26924: heap size 705 MB, throughput 0.856317
Reading from 26924: heap size 711 MB, throughput 0.541705
Reading from 26924: heap size 723 MB, throughput 0.642515
Reading from 26925: heap size 135 MB, throughput 0.997829
Reading from 26925: heap size 136 MB, throughput 0.99748
Equal recommendation: 2470 MB each
Reading from 26925: heap size 139 MB, throughput 0.997585
Reading from 26924: heap size 732 MB, throughput 0.0316212
Reading from 26924: heap size 798 MB, throughput 0.34153
Reading from 26924: heap size 784 MB, throughput 0.481086
Reading from 26925: heap size 139 MB, throughput 0.997598
Reading from 26924: heap size 794 MB, throughput 0.85181
Reading from 26924: heap size 778 MB, throughput 0.633069
Reading from 26925: heap size 143 MB, throughput 0.997951
Reading from 26925: heap size 143 MB, throughput 0.996776
Reading from 26924: heap size 786 MB, throughput 0.062123
Reading from 26924: heap size 853 MB, throughput 0.40365
Reading from 26925: heap size 147 MB, throughput 0.996109
Reading from 26924: heap size 856 MB, throughput 0.787796
Reading from 26924: heap size 863 MB, throughput 0.772394
Reading from 26924: heap size 864 MB, throughput 0.617459
Reading from 26925: heap size 147 MB, throughput 0.988641
Reading from 26925: heap size 150 MB, throughput 0.978478
Reading from 26924: heap size 870 MB, throughput 0.269754
Reading from 26924: heap size 950 MB, throughput 0.636861
Reading from 26925: heap size 151 MB, throughput 0.997313
Reading from 26924: heap size 950 MB, throughput 0.925866
Reading from 26924: heap size 959 MB, throughput 0.842366
Reading from 26924: heap size 958 MB, throughput 0.845048
Reading from 26924: heap size 961 MB, throughput 0.846014
Reading from 26924: heap size 953 MB, throughput 0.847604
Reading from 26925: heap size 159 MB, throughput 0.997628
Reading from 26924: heap size 958 MB, throughput 0.853065
Reading from 26924: heap size 946 MB, throughput 0.858912
Reading from 26924: heap size 952 MB, throughput 0.844141
Reading from 26924: heap size 938 MB, throughput 0.830071
Reading from 26924: heap size 945 MB, throughput 0.866494
Reading from 26925: heap size 160 MB, throughput 0.996915
Reading from 26924: heap size 932 MB, throughput 0.980583
Reading from 26925: heap size 167 MB, throughput 0.997583
Reading from 26924: heap size 939 MB, throughput 0.970665
Equal recommendation: 2470 MB each
Reading from 26924: heap size 928 MB, throughput 0.791108
Reading from 26925: heap size 168 MB, throughput 0.99715
Reading from 26924: heap size 845 MB, throughput 0.824136
Reading from 26924: heap size 921 MB, throughput 0.823739
Reading from 26924: heap size 928 MB, throughput 0.83709
Reading from 26924: heap size 918 MB, throughput 0.829807
Reading from 26924: heap size 923 MB, throughput 0.83264
Reading from 26925: heap size 173 MB, throughput 0.997829
Reading from 26924: heap size 916 MB, throughput 0.850562
Reading from 26924: heap size 921 MB, throughput 0.846332
Reading from 26924: heap size 916 MB, throughput 0.897844
Reading from 26925: heap size 174 MB, throughput 0.996675
Reading from 26924: heap size 920 MB, throughput 0.910528
Reading from 26924: heap size 922 MB, throughput 0.808225
Reading from 26925: heap size 179 MB, throughput 0.99578
Reading from 26924: heap size 924 MB, throughput 0.114564
Reading from 26924: heap size 996 MB, throughput 0.548163
Reading from 26924: heap size 1015 MB, throughput 0.725057
Reading from 26925: heap size 179 MB, throughput 0.997155
Reading from 26924: heap size 1024 MB, throughput 0.713667
Reading from 26924: heap size 1026 MB, throughput 0.737481
Reading from 26924: heap size 1023 MB, throughput 0.737145
Reading from 26924: heap size 1028 MB, throughput 0.714926
Reading from 26924: heap size 1035 MB, throughput 0.75835
Reading from 26924: heap size 1037 MB, throughput 0.712058
Reading from 26925: heap size 185 MB, throughput 0.997822
Reading from 26924: heap size 1051 MB, throughput 0.742275
Reading from 26924: heap size 1051 MB, throughput 0.900057
Reading from 26925: heap size 185 MB, throughput 0.997137
Reading from 26925: heap size 190 MB, throughput 0.997992
Reading from 26925: heap size 190 MB, throughput 0.996117
Equal recommendation: 2470 MB each
Reading from 26924: heap size 1067 MB, throughput 0.970789
Reading from 26925: heap size 194 MB, throughput 0.997236
Reading from 26925: heap size 194 MB, throughput 0.996814
Reading from 26925: heap size 199 MB, throughput 0.99423
Reading from 26924: heap size 1068 MB, throughput 0.949661
Reading from 26925: heap size 199 MB, throughput 0.994992
Reading from 26925: heap size 205 MB, throughput 0.997543
Reading from 26925: heap size 205 MB, throughput 0.997663
Reading from 26924: heap size 1081 MB, throughput 0.977159
Reading from 26925: heap size 210 MB, throughput 0.997591
Reading from 26925: heap size 210 MB, throughput 0.997952
Reading from 26924: heap size 1086 MB, throughput 0.971677
Equal recommendation: 2470 MB each
Reading from 26925: heap size 214 MB, throughput 0.997552
Reading from 26925: heap size 215 MB, throughput 0.997868
Reading from 26925: heap size 204 MB, throughput 0.997839
Reading from 26924: heap size 1101 MB, throughput 0.786905
Reading from 26925: heap size 195 MB, throughput 0.996967
Reading from 26925: heap size 187 MB, throughput 0.996737
Reading from 26925: heap size 179 MB, throughput 0.997611
Reading from 26924: heap size 1205 MB, throughput 0.995572
Reading from 26925: heap size 171 MB, throughput 0.997617
Reading from 26925: heap size 164 MB, throughput 0.996648
Reading from 26925: heap size 158 MB, throughput 0.997009
Equal recommendation: 2470 MB each
Reading from 26925: heap size 151 MB, throughput 0.990494
Reading from 26924: heap size 1205 MB, throughput 0.990314
Reading from 26925: heap size 157 MB, throughput 0.989039
Reading from 26925: heap size 164 MB, throughput 0.815174
Reading from 26925: heap size 173 MB, throughput 0.998181
Reading from 26925: heap size 181 MB, throughput 0.998306
Reading from 26924: heap size 1215 MB, throughput 0.993055
Reading from 26925: heap size 189 MB, throughput 0.999037
Reading from 26925: heap size 200 MB, throughput 0.998458
Reading from 26924: heap size 1225 MB, throughput 0.990407
Reading from 26925: heap size 204 MB, throughput 0.999002
Reading from 26925: heap size 204 MB, throughput 0.998108
Reading from 26925: heap size 209 MB, throughput 0.998575
Equal recommendation: 2470 MB each
Reading from 26924: heap size 1227 MB, throughput 0.988739
Reading from 26925: heap size 209 MB, throughput 0.998798
Reading from 26925: heap size 213 MB, throughput 0.998778
Reading from 26925: heap size 214 MB, throughput 0.998532
Reading from 26924: heap size 1223 MB, throughput 0.989701
Reading from 26925: heap size 217 MB, throughput 0.999043
Reading from 26925: heap size 218 MB, throughput 0.997666
Reading from 26924: heap size 1116 MB, throughput 0.988835
Reading from 26925: heap size 222 MB, throughput 0.998045
Reading from 26925: heap size 222 MB, throughput 0.989202
Reading from 26925: heap size 225 MB, throughput 0.995464
Equal recommendation: 2470 MB each
Reading from 26925: heap size 226 MB, throughput 0.998047
Reading from 26924: heap size 1212 MB, throughput 0.984004
Reading from 26925: heap size 234 MB, throughput 0.998073
Reading from 26925: heap size 235 MB, throughput 0.997443
Reading from 26924: heap size 1133 MB, throughput 0.985397
Reading from 26925: heap size 243 MB, throughput 0.997965
Reading from 26925: heap size 243 MB, throughput 0.997698
Reading from 26924: heap size 1201 MB, throughput 0.985699
Reading from 26925: heap size 250 MB, throughput 0.997951
Equal recommendation: 2470 MB each
Reading from 26925: heap size 250 MB, throughput 0.997996
Reading from 26925: heap size 257 MB, throughput 0.99815
Reading from 26924: heap size 1150 MB, throughput 0.98055
Reading from 26925: heap size 257 MB, throughput 0.997526
Reading from 26925: heap size 264 MB, throughput 0.994764
Reading from 26924: heap size 1208 MB, throughput 0.973473
Reading from 26925: heap size 264 MB, throughput 0.994605
Reading from 26925: heap size 271 MB, throughput 0.993167
Reading from 26925: heap size 272 MB, throughput 0.997938
Reading from 26924: heap size 1208 MB, throughput 0.979505
Equal recommendation: 2470 MB each
Reading from 26925: heap size 282 MB, throughput 0.997793
Reading from 26925: heap size 283 MB, throughput 0.997716
Reading from 26924: heap size 1209 MB, throughput 0.981139
Reading from 26925: heap size 291 MB, throughput 0.99779
Reading from 26925: heap size 292 MB, throughput 0.998095
Reading from 26924: heap size 1213 MB, throughput 0.97917
Reading from 26925: heap size 299 MB, throughput 0.998154
Reading from 26925: heap size 300 MB, throughput 0.99801
Equal recommendation: 2470 MB each
Reading from 26924: heap size 1214 MB, throughput 0.978835
Reading from 26925: heap size 308 MB, throughput 0.997764
Reading from 26925: heap size 308 MB, throughput 0.996584
Reading from 26925: heap size 315 MB, throughput 0.993315
Reading from 26924: heap size 1221 MB, throughput 0.978283
Reading from 26925: heap size 316 MB, throughput 0.997732
Reading from 26924: heap size 1225 MB, throughput 0.976867
Reading from 26925: heap size 326 MB, throughput 0.998515
Equal recommendation: 2470 MB each
Reading from 26925: heap size 327 MB, throughput 0.998014
Reading from 26924: heap size 1229 MB, throughput 0.972617
Reading from 26925: heap size 335 MB, throughput 0.998172
Reading from 26925: heap size 336 MB, throughput 0.998042
Reading from 26924: heap size 1233 MB, throughput 0.977646
Reading from 26925: heap size 345 MB, throughput 0.998476
Reading from 26924: heap size 1235 MB, throughput 0.976055
Reading from 26925: heap size 345 MB, throughput 0.997884
Equal recommendation: 2470 MB each
Reading from 26925: heap size 353 MB, throughput 0.995887
Reading from 26925: heap size 353 MB, throughput 0.997169
Reading from 26924: heap size 1240 MB, throughput 0.976566
Reading from 26925: heap size 365 MB, throughput 0.995637
Reading from 26924: heap size 1240 MB, throughput 0.976489
Reading from 26925: heap size 365 MB, throughput 0.998259
Equal recommendation: 2470 MB each
Reading from 26925: heap size 374 MB, throughput 0.998028
Reading from 26924: heap size 1245 MB, throughput 0.977675
Reading from 26925: heap size 376 MB, throughput 0.997646
Reading from 26925: heap size 385 MB, throughput 0.997327
Reading from 26924: heap size 1245 MB, throughput 0.97907
Reading from 26925: heap size 386 MB, throughput 0.998381
Reading from 26925: heap size 395 MB, throughput 0.994848
Reading from 26924: heap size 1247 MB, throughput 0.976961
Equal recommendation: 2470 MB each
Reading from 26925: heap size 396 MB, throughput 0.998398
Reading from 26924: heap size 1248 MB, throughput 0.976399
Reading from 26925: heap size 408 MB, throughput 0.998271
Reading from 26925: heap size 409 MB, throughput 0.998018
Reading from 26924: heap size 1250 MB, throughput 0.979098
Reading from 26925: heap size 420 MB, throughput 0.997307
Equal recommendation: 2470 MB each
Reading from 26924: heap size 1251 MB, throughput 0.977662
Reading from 26925: heap size 420 MB, throughput 0.997998
Reading from 26925: heap size 430 MB, throughput 0.998416
Reading from 26924: heap size 1252 MB, throughput 0.975758
Reading from 26925: heap size 430 MB, throughput 0.99486
Reading from 26925: heap size 440 MB, throughput 0.99805
Equal recommendation: 2470 MB each
Reading from 26924: heap size 1253 MB, throughput 0.685899
Reading from 26925: heap size 441 MB, throughput 0.998386
Reading from 26925: heap size 452 MB, throughput 0.998228
Reading from 26924: heap size 1367 MB, throughput 0.966739
Reading from 26925: heap size 453 MB, throughput 0.998209
Equal recommendation: 2470 MB each
Reading from 26924: heap size 1368 MB, throughput 0.991342
Reading from 26925: heap size 463 MB, throughput 0.998416
Reading from 26925: heap size 463 MB, throughput 0.995773
Reading from 26924: heap size 1378 MB, throughput 0.988196
Reading from 26925: heap size 473 MB, throughput 0.968213
Reading from 26924: heap size 1383 MB, throughput 0.989637
Reading from 26925: heap size 479 MB, throughput 0.99918
Equal recommendation: 2470 MB each
Reading from 26925: heap size 491 MB, throughput 0.99938
Reading from 26924: heap size 1385 MB, throughput 0.988258
Reading from 26925: heap size 491 MB, throughput 0.999164
Reading from 26924: heap size 1387 MB, throughput 0.985912
Reading from 26925: heap size 502 MB, throughput 0.99897
Equal recommendation: 2470 MB each
Reading from 26925: heap size 503 MB, throughput 0.997139
Reading from 26924: heap size 1378 MB, throughput 0.984807
Reading from 26925: heap size 512 MB, throughput 0.998755
Reading from 26924: heap size 1293 MB, throughput 0.985878
Reading from 26925: heap size 513 MB, throughput 0.998612
Equal recommendation: 2470 MB each
Reading from 26925: heap size 524 MB, throughput 0.998787
Reading from 26924: heap size 1365 MB, throughput 0.983819
Reading from 26925: heap size 525 MB, throughput 0.99874
Reading from 26925: heap size 535 MB, throughput 0.998225
Reading from 26925: heap size 536 MB, throughput 0.997819
Equal recommendation: 2470 MB each
Reading from 26925: heap size 548 MB, throughput 0.998803
Reading from 26924: heap size 1311 MB, throughput 0.987564
Reading from 26925: heap size 548 MB, throughput 0.99876
Equal recommendation: 2470 MB each
Reading from 26925: heap size 559 MB, throughput 0.998585
Reading from 26925: heap size 560 MB, throughput 0.99859
Reading from 26925: heap size 572 MB, throughput 0.9971
Reading from 26924: heap size 1340 MB, throughput 0.991966
Reading from 26925: heap size 572 MB, throughput 0.998664
Equal recommendation: 2470 MB each
Reading from 26925: heap size 586 MB, throughput 0.998596
Reading from 26924: heap size 1372 MB, throughput 0.981577
Reading from 26924: heap size 1362 MB, throughput 0.845
Reading from 26924: heap size 1394 MB, throughput 0.244299
Reading from 26924: heap size 1414 MB, throughput 0.992611
Reading from 26924: heap size 1461 MB, throughput 0.992515
Reading from 26924: heap size 1479 MB, throughput 0.992567
Reading from 26924: heap size 1480 MB, throughput 0.993068
Reading from 26925: heap size 587 MB, throughput 0.998519
Reading from 26924: heap size 1477 MB, throughput 0.989413
Reading from 26924: heap size 1483 MB, throughput 0.98943
Reading from 26924: heap size 1466 MB, throughput 0.993513
Reading from 26924: heap size 1203 MB, throughput 0.983141
Equal recommendation: 2470 MB each
Reading from 26925: heap size 600 MB, throughput 0.997919
Reading from 26924: heap size 1449 MB, throughput 0.995001
Reading from 26925: heap size 601 MB, throughput 0.997709
Reading from 26924: heap size 1215 MB, throughput 0.994672
Reading from 26925: heap size 615 MB, throughput 0.998742
Reading from 26924: heap size 1426 MB, throughput 0.993201
Equal recommendation: 2470 MB each
Reading from 26924: heap size 1232 MB, throughput 0.990699
Reading from 26925: heap size 616 MB, throughput 0.998324
Reading from 26924: heap size 1402 MB, throughput 0.990268
Reading from 26925: heap size 631 MB, throughput 0.998236
Reading from 26924: heap size 1249 MB, throughput 0.990062
Client 26925 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 26924: heap size 1376 MB, throughput 0.98718
Reading from 26924: heap size 1266 MB, throughput 0.986933
Reading from 26924: heap size 1368 MB, throughput 0.984852
Recommendation: one client; give it all the memory
Reading from 26924: heap size 1282 MB, throughput 0.983952
Reading from 26924: heap size 1370 MB, throughput 0.983468
Reading from 26924: heap size 1372 MB, throughput 0.982506
Recommendation: one client; give it all the memory
Reading from 26924: heap size 1375 MB, throughput 0.981758
Reading from 26924: heap size 1377 MB, throughput 0.979547
Reading from 26924: heap size 1381 MB, throughput 0.980572
Recommendation: one client; give it all the memory
Reading from 26924: heap size 1385 MB, throughput 0.97885
Reading from 26924: heap size 1391 MB, throughput 0.977442
Reading from 26924: heap size 1396 MB, throughput 0.977019
Recommendation: one client; give it all the memory
Reading from 26924: heap size 1403 MB, throughput 0.978663
Reading from 26924: heap size 1405 MB, throughput 0.97909
Reading from 26924: heap size 1411 MB, throughput 0.979161
Recommendation: one client; give it all the memory
Reading from 26924: heap size 1412 MB, throughput 0.978818
Reading from 26924: heap size 1417 MB, throughput 0.979409
Recommendation: one client; give it all the memory
Reading from 26924: heap size 1418 MB, throughput 0.978136
Reading from 26924: heap size 1421 MB, throughput 0.978142
Reading from 26924: heap size 1422 MB, throughput 0.978075
Recommendation: one client; give it all the memory
Reading from 26924: heap size 1426 MB, throughput 0.979478
Reading from 26924: heap size 1427 MB, throughput 0.978807
Recommendation: one client; give it all the memory
Reading from 26924: heap size 1431 MB, throughput 0.979018
Reading from 26924: heap size 1432 MB, throughput 0.978987
Reading from 26924: heap size 1436 MB, throughput 0.978794
Recommendation: one client; give it all the memory
Reading from 26924: heap size 1437 MB, throughput 0.978133
Reading from 26924: heap size 1443 MB, throughput 0.980266
Recommendation: one client; give it all the memory
Reading from 26924: heap size 1443 MB, throughput 0.978666
Reading from 26924: heap size 1449 MB, throughput 0.808192
Recommendation: one client; give it all the memory
Reading from 26924: heap size 1483 MB, throughput 0.995594
Reading from 26924: heap size 1480 MB, throughput 0.994018
Reading from 26924: heap size 1491 MB, throughput 0.992637
Recommendation: one client; give it all the memory
Reading from 26924: heap size 1502 MB, throughput 0.991614
Reading from 26924: heap size 1503 MB, throughput 0.99065
Recommendation: one client; give it all the memory
Reading from 26924: heap size 1498 MB, throughput 0.989124
Reading from 26924: heap size 1386 MB, throughput 0.988357
Recommendation: one client; give it all the memory
Reading from 26924: heap size 1486 MB, throughput 0.987445
Recommendation: one client; give it all the memory
Reading from 26924: heap size 1410 MB, throughput 0.986686
Recommendation: one client; give it all the memory
Reading from 26924: heap size 1453 MB, throughput 0.989973
Recommendation: one client; give it all the memory
Reading from 26924: heap size 1492 MB, throughput 0.98013
Reading from 26924: heap size 1509 MB, throughput 0.839211
Reading from 26924: heap size 1529 MB, throughput 0.683817
Reading from 26924: heap size 1544 MB, throughput 0.698105
Reading from 26924: heap size 1575 MB, throughput 0.674692
Reading from 26924: heap size 1614 MB, throughput 0.711372
Reading from 26924: heap size 1630 MB, throughput 0.722559
Reading from 26924: heap size 1674 MB, throughput 0.763085
Client 26924 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
