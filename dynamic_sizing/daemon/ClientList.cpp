#include <pthread.h>
#include <Log.hpp>
#include "UtilityModel.hpp"
#include "Client.hpp"
#include "ClientList.hpp"

ClientList::ClientList(const std::vector<std::string>& readingCacheNames, bool forceFunctionValidity, economem::Log& log)
    : m_ReadingCacheNames(readingCacheNames),
      m_ForceFunctionValidity(forceFunctionValidity),
      m_Log(log)
{

}

ClientList::~ClientList()
{
    clear();
}

void ClientList::lock()
{
    m_Lock.lock();
}

void ClientList::unlock()
{
    m_Lock.unlock();
}

void ClientList::addReading(const economem::Reading& reading)
{
    for (Client* c : m_Clients)
    {
        if (c->getPid() == reading.sender)
        {
            c->addReading(reading);
            m_Log.logReading(reading.sender, reading.heapSizeMB, reading.throughput);
            return;
        }
    }

    Client* c;
    m_Clients.push_back(c = new Client(reading, m_ReadingCacheNames, m_ForceFunctionValidity, m_Log));
    m_Log.logCreation(reading.sender);
    m_Log.logReading(reading.sender, reading.heapSizeMB, reading.throughput);
    m_Log.logMinHeap(reading.sender, c->getMinHeapMB());
}

void ClientList::clientDied(pid_t pid)
{
    for (std::vector<Client*>::iterator it = m_Clients.begin(); it != m_Clients.end(); ++it)
    {
        if ((*it)->getPid() == pid)
        {
            m_Log.logDeath(pid, (*it)->getTotalReadingsReceived());
            delete *it;
            m_Clients.erase(it);
            return;
        }
    }
}

size_t ClientList::count()
{
    return m_Clients.size();
}

Client* ClientList::getClient(size_t i)
{
    if (i >= m_Clients.size())
        return NULL;
    else
        return m_Clients[i];
}

Client* ClientList::getClientByPid(pid_t pid)
{
    for (Client* c : m_Clients)
    {
        if (c->getPid() == pid)
            return c;
    }
    return NULL;
}

void ClientList::calculateAllFunctions(const UtilityModel& u)
{
    for (Client* c : m_Clients)
    {
        c->calculateFunction(u);
        c->logUtilityFunctions();
    }
}

bool ClientList::allValid()
{
    for (Client* c : m_Clients)
    {
        if (!c->isValid())
            return false;
    }
    return true;
}

void ClientList::makeRecommendations(std::vector<economem::Recommendation>& l, economem::RecommendationSender& sender)
{
    size_t len = std::min(l.size(), m_Clients.size());
    std::vector<Client*>::iterator itC = m_Clients.begin();
    std::vector<economem::Recommendation>::iterator itR = l.begin();
    size_t i = 0;

    while (i < len)
    {
        if (sender.sendRecommendation(*itR, (*itC)->getPid()))
        {
            m_Log.logRecommendation((*itC)->getPid(), itR->getHeapSizeMB());
            ++i;
            ++itC;
            ++itR;
        }
        else
        {
            m_Log.logDeath((*itC)->getPid(), (*itC)->getTotalReadingsReceived());
            delete *itC;
            itC = m_Clients.erase(itC);
            itR = l.erase(itR);
            --len;
        }
    }
}

bool ClientList::recommendToClient(size_t i, const economem::Recommendation& r, economem::RecommendationSender& sender)
{
    if (i >= m_Clients.size())
        return false;

    std::vector<Client*>::iterator it = m_Clients.begin() + i;

    if (sender.sendRecommendation(r, (*it)->getPid()))
    {
        m_Log.logRecommendation((*it)->getPid(), r.getHeapSizeMB());
        return true;
    }
    else
    {
        m_Log.logDeath((*it)->getPid(), (*it)->getTotalReadingsReceived());
        delete *it;
        m_Clients.erase(it);
        return false;
    }
}

void ClientList::recommendToAll(const economem::Recommendation& r, economem::RecommendationSender& sender)
{
    if (!m_Clients.empty())
    {
        // We're doing it this rather odd way to avoid having to duplicate the code in makeRecommendations
        // that deletes clients to whom sending fails
        std::vector<economem::Recommendation> l(m_Clients.size());
        for (size_t i = 0; i < m_Clients.size(); ++i)
            l[i] = r;
        makeRecommendations(l, sender);
    }
}

void ClientList::dumpTo(FILE* fd)
{
    fprintf(fd, "Clients: %zu\n", m_Clients.size());
    for (Client* c : m_Clients)
        c->dumpTo(fd);
}

void ClientList::clear()
{
    for (Client* c : m_Clients)
        delete c;
    m_Clients.clear();
}
