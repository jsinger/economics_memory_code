#include <cstdio>
#include <Utilities.hpp>
#include "RootUtilityModel.hpp"
#include "PyOptimiser.hpp"


// Use this to manually test the numerical optimisation in UtilityModel.cpp

void usage()
{
    fprintf(stderr, "Usage: optimisertest p1_c p1_e p2_c p2_e\n");
    exit(1);
}


int main(int argc, char** argv)
{
    if (argc < 5)
        usage();

    economem::makeInfoVerbose();
    PyOptimiser::init();
    RootUtilityModel model(false);
    RootUtilityFunction f1(atof(argv[1]), atof(argv[2]));
    RootUtilityFunction f2(atof(argv[3]), atof(argv[4]));
    printf("%s\n%s\n", f1.toString().c_str(), f2.toString().c_str());

    PartitionResult* r = model.partitionMemory(1000, {&f1, &f2}, {2, 2});
    printf("Analytic result:\n");
    r->print({&f1, &f2});
    delete r;

    PyOptimiser::deinit();
    return 0;
}
