economemd
    total memory: 4904 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub17_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 9964: heap size 9 MB, throughput 0.989111
Clients: 1
Client 9964 has a minimum heap size of 3 MB
Reading from 9963: heap size 9 MB, throughput 0.990442
Clients: 2
Client 9963 has a minimum heap size of 1223 MB
Reading from 9964: heap size 9 MB, throughput 0.9785
Reading from 9963: heap size 9 MB, throughput 0.982948
Reading from 9964: heap size 11 MB, throughput 0.961031
Reading from 9963: heap size 9 MB, throughput 0.959968
Reading from 9963: heap size 9 MB, throughput 0.948448
Reading from 9964: heap size 11 MB, throughput 0.976715
Reading from 9963: heap size 11 MB, throughput 0.980689
Reading from 9963: heap size 11 MB, throughput 0.976471
Reading from 9964: heap size 15 MB, throughput 0.929712
Reading from 9963: heap size 17 MB, throughput 0.958598
Reading from 9964: heap size 17 MB, throughput 0.994006
Reading from 9963: heap size 17 MB, throughput 0.541975
Reading from 9964: heap size 21 MB, throughput 0.986523
Reading from 9963: heap size 30 MB, throughput 0.970877
Reading from 9964: heap size 26 MB, throughput 0.992444
Reading from 9964: heap size 27 MB, throughput 0.990539
Reading from 9963: heap size 31 MB, throughput 0.886574
Reading from 9964: heap size 27 MB, throughput 0.992156
Reading from 9964: heap size 29 MB, throughput 0.993151
Reading from 9963: heap size 34 MB, throughput 0.459451
Reading from 9964: heap size 29 MB, throughput 0.988369
Reading from 9963: heap size 47 MB, throughput 0.886322
Reading from 9963: heap size 50 MB, throughput 0.822287
Reading from 9964: heap size 32 MB, throughput 0.992211
Reading from 9964: heap size 32 MB, throughput 0.989162
Reading from 9964: heap size 36 MB, throughput 0.991861
Reading from 9963: heap size 51 MB, throughput 0.278005
Reading from 9964: heap size 36 MB, throughput 0.984472
Reading from 9963: heap size 71 MB, throughput 0.800962
Reading from 9963: heap size 72 MB, throughput 0.734883
Reading from 9964: heap size 40 MB, throughput 0.989753
Reading from 9964: heap size 40 MB, throughput 0.976859
Reading from 9964: heap size 43 MB, throughput 0.989101
Reading from 9964: heap size 43 MB, throughput 0.9823
Reading from 9963: heap size 75 MB, throughput 0.296291
Reading from 9964: heap size 47 MB, throughput 0.992154
Reading from 9963: heap size 98 MB, throughput 0.678556
Reading from 9964: heap size 47 MB, throughput 0.993009
Reading from 9963: heap size 101 MB, throughput 0.685858
Reading from 9964: heap size 50 MB, throughput 0.994308
Reading from 9963: heap size 103 MB, throughput 0.169947
Reading from 9964: heap size 51 MB, throughput 0.972111
Reading from 9963: heap size 135 MB, throughput 0.791708
Reading from 9964: heap size 55 MB, throughput 0.994834
Reading from 9963: heap size 137 MB, throughput 0.705632
Reading from 9964: heap size 55 MB, throughput 0.991149
Reading from 9963: heap size 140 MB, throughput 0.816119
Reading from 9964: heap size 59 MB, throughput 0.994857
Reading from 9963: heap size 143 MB, throughput 0.528833
Reading from 9964: heap size 59 MB, throughput 0.985957
Reading from 9964: heap size 63 MB, throughput 0.992342
Reading from 9963: heap size 150 MB, throughput 0.79525
Reading from 9964: heap size 63 MB, throughput 0.993086
Reading from 9963: heap size 154 MB, throughput 0.58058
Reading from 9964: heap size 67 MB, throughput 0.994902
Reading from 9964: heap size 67 MB, throughput 0.993356
Reading from 9964: heap size 71 MB, throughput 0.994985
Reading from 9963: heap size 157 MB, throughput 0.15251
Reading from 9964: heap size 71 MB, throughput 0.990181
Reading from 9963: heap size 196 MB, throughput 0.591002
Reading from 9963: heap size 203 MB, throughput 0.720426
Reading from 9964: heap size 74 MB, throughput 0.994856
Reading from 9963: heap size 205 MB, throughput 0.678095
Reading from 9964: heap size 74 MB, throughput 0.989339
Reading from 9963: heap size 208 MB, throughput 0.684955
Reading from 9964: heap size 77 MB, throughput 0.993488
Reading from 9963: heap size 214 MB, throughput 0.546077
Reading from 9964: heap size 77 MB, throughput 0.990771
Reading from 9964: heap size 81 MB, throughput 0.993993
Reading from 9964: heap size 81 MB, throughput 0.994232
Reading from 9963: heap size 220 MB, throughput 0.120492
Reading from 9964: heap size 84 MB, throughput 0.99366
Reading from 9964: heap size 84 MB, throughput 0.985532
Reading from 9963: heap size 260 MB, throughput 0.62591
Reading from 9964: heap size 88 MB, throughput 0.988764
Reading from 9963: heap size 267 MB, throughput 0.664475
Reading from 9964: heap size 88 MB, throughput 0.99074
Reading from 9963: heap size 268 MB, throughput 0.620005
Reading from 9964: heap size 93 MB, throughput 0.99382
Reading from 9963: heap size 271 MB, throughput 0.63493
Reading from 9964: heap size 93 MB, throughput 0.993239
Reading from 9963: heap size 277 MB, throughput 0.61634
Reading from 9964: heap size 96 MB, throughput 0.990964
Reading from 9964: heap size 97 MB, throughput 0.991186
Reading from 9964: heap size 101 MB, throughput 0.995274
Reading from 9964: heap size 101 MB, throughput 0.991991
Reading from 9964: heap size 105 MB, throughput 0.993317
Reading from 9963: heap size 282 MB, throughput 0.136974
Reading from 9964: heap size 105 MB, throughput 0.902354
Reading from 9963: heap size 326 MB, throughput 0.579291
Reading from 9964: heap size 109 MB, throughput 0.989062
Reading from 9963: heap size 333 MB, throughput 0.603978
Reading from 9963: heap size 335 MB, throughput 0.617639
Reading from 9964: heap size 109 MB, throughput 0.992862
Reading from 9963: heap size 340 MB, throughput 0.634672
Reading from 9964: heap size 118 MB, throughput 0.993671
Reading from 9963: heap size 345 MB, throughput 0.593746
Reading from 9964: heap size 119 MB, throughput 0.993108
Reading from 9964: heap size 126 MB, throughput 0.993706
Reading from 9964: heap size 127 MB, throughput 0.985989
Reading from 9964: heap size 134 MB, throughput 0.996243
Reading from 9963: heap size 351 MB, throughput 0.114665
Reading from 9964: heap size 134 MB, throughput 0.993
Reading from 9963: heap size 398 MB, throughput 0.464551
Reading from 9963: heap size 402 MB, throughput 0.67111
Reading from 9964: heap size 140 MB, throughput 0.992597
Reading from 9963: heap size 405 MB, throughput 0.616814
Reading from 9964: heap size 141 MB, throughput 0.993681
Reading from 9963: heap size 407 MB, throughput 0.576916
Reading from 9964: heap size 146 MB, throughput 0.9915
Reading from 9963: heap size 412 MB, throughput 0.518815
Reading from 9964: heap size 147 MB, throughput 0.992742
Equal recommendation: 2452 MB each
Reading from 9964: heap size 153 MB, throughput 0.994709
Reading from 9964: heap size 153 MB, throughput 0.995166
Reading from 9963: heap size 416 MB, throughput 0.102604
Reading from 9964: heap size 159 MB, throughput 0.995238
Reading from 9964: heap size 159 MB, throughput 0.992881
Reading from 9963: heap size 472 MB, throughput 0.390638
Reading from 9964: heap size 164 MB, throughput 0.993041
Reading from 9963: heap size 477 MB, throughput 0.555037
Reading from 9963: heap size 480 MB, throughput 0.581478
Reading from 9964: heap size 164 MB, throughput 0.994885
Reading from 9964: heap size 169 MB, throughput 0.993155
Reading from 9964: heap size 170 MB, throughput 0.994477
Reading from 9964: heap size 175 MB, throughput 0.994561
Reading from 9963: heap size 484 MB, throughput 0.115544
Reading from 9963: heap size 538 MB, throughput 0.427467
Reading from 9964: heap size 176 MB, throughput 0.994992
Reading from 9963: heap size 544 MB, throughput 0.576593
Reading from 9964: heap size 181 MB, throughput 0.993391
Reading from 9963: heap size 545 MB, throughput 0.504325
Reading from 9964: heap size 181 MB, throughput 0.993392
Reading from 9963: heap size 547 MB, throughput 0.619592
Reading from 9964: heap size 187 MB, throughput 0.99594
Reading from 9963: heap size 555 MB, throughput 0.521558
Reading from 9964: heap size 187 MB, throughput 0.99351
Reading from 9964: heap size 193 MB, throughput 0.994093
Reading from 9964: heap size 193 MB, throughput 0.993867
Reading from 9964: heap size 200 MB, throughput 0.993702
Reading from 9963: heap size 563 MB, throughput 0.0944521
Reading from 9964: heap size 200 MB, throughput 0.994708
Reading from 9963: heap size 630 MB, throughput 0.370781
Reading from 9964: heap size 206 MB, throughput 0.995813
Reading from 9963: heap size 644 MB, throughput 0.636662
Reading from 9964: heap size 206 MB, throughput 0.993357
Reading from 9963: heap size 645 MB, throughput 0.521313
Reading from 9964: heap size 213 MB, throughput 0.994274
Reading from 9963: heap size 647 MB, throughput 0.503548
Reading from 9964: heap size 213 MB, throughput 0.994195
Reading from 9964: heap size 220 MB, throughput 0.993742
Reading from 9963: heap size 656 MB, throughput 0.798147
Reading from 9964: heap size 220 MB, throughput 0.994509
Reading from 9964: heap size 227 MB, throughput 0.994962
Reading from 9963: heap size 661 MB, throughput 0.805607
Reading from 9964: heap size 227 MB, throughput 0.993693
Reading from 9964: heap size 234 MB, throughput 0.994453
Reading from 9964: heap size 234 MB, throughput 0.994451
Reading from 9963: heap size 672 MB, throughput 0.844653
Reading from 9964: heap size 242 MB, throughput 0.995647
Reading from 9964: heap size 242 MB, throughput 0.996042
Reading from 9964: heap size 248 MB, throughput 0.996333
Equal recommendation: 2452 MB each
Reading from 9964: heap size 249 MB, throughput 0.995323
Reading from 9963: heap size 675 MB, throughput 0.0496548
Reading from 9963: heap size 757 MB, throughput 0.198734
Reading from 9964: heap size 255 MB, throughput 0.994155
Reading from 9963: heap size 773 MB, throughput 0.544836
Reading from 9963: heap size 774 MB, throughput 0.272913
Reading from 9964: heap size 255 MB, throughput 0.993538
Reading from 9963: heap size 765 MB, throughput 0.307151
Reading from 9964: heap size 263 MB, throughput 0.987666
Reading from 9964: heap size 263 MB, throughput 0.99464
Reading from 9964: heap size 272 MB, throughput 0.995053
Reading from 9964: heap size 272 MB, throughput 0.992463
Reading from 9963: heap size 664 MB, throughput 0.0248127
Reading from 9964: heap size 282 MB, throughput 0.994574
Reading from 9963: heap size 843 MB, throughput 0.238746
Reading from 9964: heap size 282 MB, throughput 0.993135
Reading from 9964: heap size 292 MB, throughput 0.990177
Reading from 9963: heap size 847 MB, throughput 0.861234
Reading from 9963: heap size 847 MB, throughput 0.525813
Reading from 9964: heap size 292 MB, throughput 0.992842
Reading from 9963: heap size 849 MB, throughput 0.460265
Reading from 9963: heap size 846 MB, throughput 0.527322
Reading from 9964: heap size 304 MB, throughput 0.994306
Reading from 9964: heap size 304 MB, throughput 0.9933
Reading from 9964: heap size 314 MB, throughput 0.995093
Reading from 9964: heap size 314 MB, throughput 0.99352
Reading from 9963: heap size 849 MB, throughput 0.190282
Reading from 9964: heap size 325 MB, throughput 0.993286
Reading from 9963: heap size 930 MB, throughput 0.501607
Reading from 9963: heap size 932 MB, throughput 0.839331
Reading from 9964: heap size 325 MB, throughput 0.994613
Reading from 9964: heap size 336 MB, throughput 0.994957
Reading from 9964: heap size 336 MB, throughput 0.995813
Reading from 9963: heap size 939 MB, throughput 0.265615
Reading from 9963: heap size 1026 MB, throughput 0.976872
Reading from 9964: heap size 346 MB, throughput 0.995684
Reading from 9963: heap size 1031 MB, throughput 0.927344
Reading from 9964: heap size 347 MB, throughput 0.995179
Reading from 9963: heap size 832 MB, throughput 0.848617
Reading from 9963: heap size 1017 MB, throughput 0.79494
Reading from 9964: heap size 356 MB, throughput 0.995287
Reading from 9963: heap size 842 MB, throughput 0.827261
Reading from 9963: heap size 1005 MB, throughput 0.802268
Reading from 9964: heap size 357 MB, throughput 0.994981
Reading from 9963: heap size 847 MB, throughput 0.831052
Reading from 9964: heap size 367 MB, throughput 0.992931
Reading from 9963: heap size 993 MB, throughput 0.852101
Reading from 9963: heap size 853 MB, throughput 0.833305
Reading from 9964: heap size 367 MB, throughput 0.994771
Reading from 9963: heap size 983 MB, throughput 0.866572
Reading from 9963: heap size 859 MB, throughput 0.846729
Reading from 9964: heap size 379 MB, throughput 0.995684
Reading from 9963: heap size 974 MB, throughput 0.837742
Equal recommendation: 2452 MB each
Reading from 9964: heap size 379 MB, throughput 0.995045
Reading from 9964: heap size 360 MB, throughput 0.994672
Reading from 9964: heap size 343 MB, throughput 0.995493
Reading from 9963: heap size 865 MB, throughput 0.981517
Reading from 9964: heap size 327 MB, throughput 0.994695
Reading from 9964: heap size 312 MB, throughput 0.994229
Reading from 9964: heap size 297 MB, throughput 0.992661
Reading from 9963: heap size 970 MB, throughput 0.954737
Reading from 9964: heap size 307 MB, throughput 0.99355
Reading from 9963: heap size 975 MB, throughput 0.743424
Reading from 9963: heap size 974 MB, throughput 0.763312
Reading from 9964: heap size 317 MB, throughput 0.992897
Reading from 9963: heap size 976 MB, throughput 0.748246
Reading from 9963: heap size 975 MB, throughput 0.749524
Reading from 9964: heap size 328 MB, throughput 0.995034
Reading from 9963: heap size 977 MB, throughput 0.750717
Reading from 9964: heap size 338 MB, throughput 0.994615
Reading from 9963: heap size 977 MB, throughput 0.774098
Reading from 9963: heap size 980 MB, throughput 0.782461
Reading from 9964: heap size 323 MB, throughput 0.994912
Reading from 9963: heap size 979 MB, throughput 0.911081
Reading from 9964: heap size 307 MB, throughput 0.994677
Reading from 9963: heap size 983 MB, throughput 0.895518
Reading from 9964: heap size 293 MB, throughput 0.994064
Reading from 9964: heap size 280 MB, throughput 0.994007
Reading from 9963: heap size 979 MB, throughput 0.899374
Reading from 9964: heap size 267 MB, throughput 0.988937
Reading from 9963: heap size 985 MB, throughput 0.706061
Reading from 9963: heap size 982 MB, throughput 0.615013
Reading from 9964: heap size 276 MB, throughput 0.994274
Reading from 9963: heap size 989 MB, throughput 0.582811
Reading from 9964: heap size 287 MB, throughput 0.99453
Reading from 9963: heap size 1015 MB, throughput 0.63304
Reading from 9963: heap size 1020 MB, throughput 0.641655
Reading from 9964: heap size 297 MB, throughput 0.994729
Reading from 9963: heap size 1038 MB, throughput 0.673503
Reading from 9964: heap size 283 MB, throughput 0.994876
Reading from 9963: heap size 1038 MB, throughput 0.606668
Reading from 9964: heap size 269 MB, throughput 0.994096
Reading from 9963: heap size 1053 MB, throughput 0.632495
Reading from 9964: heap size 257 MB, throughput 0.994201
Reading from 9963: heap size 1054 MB, throughput 0.674829
Reading from 9963: heap size 1071 MB, throughput 0.664947
Reading from 9964: heap size 245 MB, throughput 0.996105
Reading from 9963: heap size 1073 MB, throughput 0.711598
Reading from 9964: heap size 233 MB, throughput 0.994533
Reading from 9964: heap size 223 MB, throughput 0.983326
Reading from 9964: heap size 232 MB, throughput 0.992088
Reading from 9964: heap size 242 MB, throughput 0.995014
Reading from 9964: heap size 251 MB, throughput 0.994746
Reading from 9964: heap size 260 MB, throughput 0.991983
Reading from 9964: heap size 270 MB, throughput 0.994421
Reading from 9964: heap size 281 MB, throughput 0.994185
Equal recommendation: 2452 MB each
Reading from 9964: heap size 268 MB, throughput 0.983828
Reading from 9964: heap size 279 MB, throughput 0.995738
Reading from 9963: heap size 1091 MB, throughput 0.961936
Reading from 9964: heap size 291 MB, throughput 0.993147
Reading from 9964: heap size 303 MB, throughput 0.995207
Reading from 9964: heap size 314 MB, throughput 0.995073
Reading from 9964: heap size 298 MB, throughput 0.994686
Reading from 9964: heap size 285 MB, throughput 0.994211
Reading from 9964: heap size 272 MB, throughput 0.994555
Reading from 9964: heap size 259 MB, throughput 0.994479
Reading from 9964: heap size 246 MB, throughput 0.993097
Reading from 9963: heap size 1093 MB, throughput 0.965087
Reading from 9964: heap size 236 MB, throughput 0.994609
Reading from 9964: heap size 224 MB, throughput 0.992093
Reading from 9964: heap size 214 MB, throughput 0.917717
Reading from 9964: heap size 205 MB, throughput 0.993402
Reading from 9964: heap size 217 MB, throughput 0.994425
Reading from 9964: heap size 230 MB, throughput 0.995004
Reading from 9964: heap size 242 MB, throughput 0.995121
Reading from 9964: heap size 255 MB, throughput 0.99456
Reading from 9964: heap size 269 MB, throughput 0.994596
Reading from 9964: heap size 282 MB, throughput 0.994979
Reading from 9964: heap size 298 MB, throughput 0.99441
Reading from 9963: heap size 1101 MB, throughput 0.962183
Reading from 9964: heap size 313 MB, throughput 0.995265
Reading from 9964: heap size 329 MB, throughput 0.994416
Reading from 9964: heap size 345 MB, throughput 0.995136
Reading from 9964: heap size 358 MB, throughput 0.994213
Reading from 9964: heap size 364 MB, throughput 0.994901
Reading from 9964: heap size 364 MB, throughput 0.993706
Reading from 9964: heap size 383 MB, throughput 0.994519
Reading from 9963: heap size 1105 MB, throughput 0.961388
Reading from 9964: heap size 383 MB, throughput 0.99555
Reading from 9964: heap size 402 MB, throughput 0.995459
Equal recommendation: 2452 MB each
Reading from 9964: heap size 402 MB, throughput 0.994209
Reading from 9964: heap size 422 MB, throughput 0.995567
Reading from 9964: heap size 422 MB, throughput 0.99513
Reading from 9964: heap size 443 MB, throughput 0.995436
Reading from 9963: heap size 1103 MB, throughput 0.96509
Reading from 9964: heap size 443 MB, throughput 0.995216
Reading from 9964: heap size 465 MB, throughput 0.995472
Reading from 9964: heap size 465 MB, throughput 0.995412
Reading from 9964: heap size 487 MB, throughput 0.99551
Reading from 9964: heap size 487 MB, throughput 0.994192
Reading from 9964: heap size 510 MB, throughput 0.995196
Reading from 9963: heap size 1109 MB, throughput 0.967192
Reading from 9964: heap size 510 MB, throughput 0.994928
Reading from 9964: heap size 536 MB, throughput 0.994481
Reading from 9964: heap size 536 MB, throughput 0.994721
Reading from 9964: heap size 563 MB, throughput 0.995343
Reading from 9964: heap size 563 MB, throughput 0.99509
Reading from 9963: heap size 1097 MB, throughput 0.957692
Reading from 9964: heap size 591 MB, throughput 0.996234
Reading from 9964: heap size 591 MB, throughput 0.995317
Reading from 9964: heap size 619 MB, throughput 0.995283
Equal recommendation: 2452 MB each
Reading from 9964: heap size 619 MB, throughput 0.995355
Reading from 9963: heap size 1105 MB, throughput 0.96025
Reading from 9964: heap size 649 MB, throughput 0.995662
Reading from 9964: heap size 649 MB, throughput 0.994859
Reading from 9964: heap size 679 MB, throughput 0.995606
Reading from 9964: heap size 679 MB, throughput 0.995389
Reading from 9963: heap size 1095 MB, throughput 0.962574
Reading from 9964: heap size 711 MB, throughput 0.99452
Reading from 9964: heap size 711 MB, throughput 0.997224
Reading from 9964: heap size 743 MB, throughput 0.995579
Reading from 9964: heap size 745 MB, throughput 0.993961
Reading from 9963: heap size 1100 MB, throughput 0.968846
Reading from 9964: heap size 777 MB, throughput 0.995707
Reading from 9964: heap size 777 MB, throughput 0.995666
Equal recommendation: 2452 MB each
Reading from 9964: heap size 813 MB, throughput 0.994816
Reading from 9964: heap size 814 MB, throughput 0.995848
Reading from 9963: heap size 1100 MB, throughput 0.966534
Reading from 9964: heap size 852 MB, throughput 0.995917
Reading from 9964: heap size 853 MB, throughput 0.995015
Reading from 9964: heap size 892 MB, throughput 0.996054
Reading from 9963: heap size 1102 MB, throughput 0.958549
Reading from 9964: heap size 892 MB, throughput 0.995524
Reading from 9964: heap size 933 MB, throughput 0.995726
Reading from 9964: heap size 933 MB, throughput 0.995455
Reading from 9963: heap size 1107 MB, throughput 0.953931
Reading from 9964: heap size 976 MB, throughput 0.995857
Reading from 9964: heap size 976 MB, throughput 0.995449
Equal recommendation: 2452 MB each
Reading from 9964: heap size 1021 MB, throughput 0.996101
Reading from 9963: heap size 1108 MB, throughput 0.961049
Reading from 9964: heap size 1021 MB, throughput 0.99521
Reading from 9964: heap size 1067 MB, throughput 0.995732
Reading from 9964: heap size 1067 MB, throughput 0.995872
Reading from 9963: heap size 1113 MB, throughput 0.960082
Reading from 9964: heap size 1115 MB, throughput 0.995678
Reading from 9964: heap size 1116 MB, throughput 0.995744
Reading from 9964: heap size 1166 MB, throughput 0.996292
Reading from 9963: heap size 1116 MB, throughput 0.969178
Reading from 9964: heap size 1166 MB, throughput 0.995615
Equal recommendation: 2452 MB each
Reading from 9964: heap size 1217 MB, throughput 0.995939
Reading from 9964: heap size 1217 MB, throughput 0.995899
Reading from 9963: heap size 1122 MB, throughput 0.562587
Reading from 9964: heap size 1271 MB, throughput 0.997257
Reading from 9964: heap size 1271 MB, throughput 0.995687
Reading from 9964: heap size 1322 MB, throughput 0.996116
Reading from 9963: heap size 1216 MB, throughput 0.952463
Reading from 9964: heap size 1323 MB, throughput 0.995857
Reading from 9964: heap size 1378 MB, throughput 0.996349
Equal recommendation: 2452 MB each
Reading from 9963: heap size 1224 MB, throughput 0.976614
Reading from 9964: heap size 1378 MB, throughput 0.995548
Reading from 9964: heap size 1436 MB, throughput 0.996092
Reading from 9963: heap size 1224 MB, throughput 0.970368
Reading from 9964: heap size 1436 MB, throughput 0.995854
Reading from 9964: heap size 1498 MB, throughput 0.995441
Reading from 9963: heap size 1221 MB, throughput 0.974721
Reading from 9964: heap size 1498 MB, throughput 0.995843
Reading from 9964: heap size 1565 MB, throughput 0.995696
Equal recommendation: 2452 MB each
Reading from 9963: heap size 1224 MB, throughput 0.976862
Reading from 9964: heap size 1565 MB, throughput 0.996088
Reading from 9964: heap size 1632 MB, throughput 0.996295
Reading from 9963: heap size 1215 MB, throughput 0.966741
Reading from 9964: heap size 1633 MB, throughput 0.996106
Reading from 9964: heap size 1636 MB, throughput 0.996076
Reading from 9963: heap size 1220 MB, throughput 0.963856
Reading from 9964: heap size 1636 MB, throughput 0.995939
Equal recommendation: 2452 MB each
Reading from 9964: heap size 1636 MB, throughput 0.996155
Reading from 9963: heap size 1214 MB, throughput 0.969563
Reading from 9964: heap size 1635 MB, throughput 0.996318
Reading from 9964: heap size 1636 MB, throughput 0.996218
Reading from 9963: heap size 1217 MB, throughput 0.964794
Reading from 9964: heap size 1636 MB, throughput 0.996217
Reading from 9964: heap size 1636 MB, throughput 0.995986
Reading from 9963: heap size 1221 MB, throughput 0.968006
Reading from 9964: heap size 1634 MB, throughput 0.996514
Equal recommendation: 2452 MB each
Reading from 9964: heap size 1635 MB, throughput 0.997194
Reading from 9963: heap size 1221 MB, throughput 0.972121
Reading from 9964: heap size 1635 MB, throughput 0.996476
Reading from 9964: heap size 1636 MB, throughput 0.99646
Reading from 9963: heap size 1226 MB, throughput 0.967888
Reading from 9964: heap size 1636 MB, throughput 0.996355
Reading from 9964: heap size 1636 MB, throughput 0.996178
Equal recommendation: 2452 MB each
Reading from 9963: heap size 1229 MB, throughput 0.959391
Reading from 9964: heap size 1636 MB, throughput 0.996198
Reading from 9964: heap size 1635 MB, throughput 0.996102
Reading from 9963: heap size 1233 MB, throughput 0.964914
Reading from 9964: heap size 1636 MB, throughput 0.996221
Reading from 9964: heap size 1637 MB, throughput 0.996048
Reading from 9963: heap size 1238 MB, throughput 0.957881
Reading from 9964: heap size 1637 MB, throughput 0.995982
Equal recommendation: 2452 MB each
Reading from 9964: heap size 1637 MB, throughput 0.99607
Reading from 9963: heap size 1242 MB, throughput 0.959129
Reading from 9964: heap size 1637 MB, throughput 0.99598
Reading from 9964: heap size 1637 MB, throughput 0.996099
Reading from 9963: heap size 1248 MB, throughput 0.961826
Reading from 9964: heap size 1637 MB, throughput 0.996123
Reading from 9964: heap size 1637 MB, throughput 0.995999
Reading from 9963: heap size 1256 MB, throughput 0.954469
Reading from 9964: heap size 1637 MB, throughput 0.996295
Equal recommendation: 2452 MB each
Reading from 9964: heap size 1636 MB, throughput 0.996037
Reading from 9964: heap size 1636 MB, throughput 0.996218
Reading from 9963: heap size 1259 MB, throughput 0.590391
Reading from 9964: heap size 1637 MB, throughput 0.997512
Reading from 9964: heap size 1637 MB, throughput 0.996051
Reading from 9964: heap size 1637 MB, throughput 0.99592
Reading from 9963: heap size 1370 MB, throughput 0.947339
Equal recommendation: 2452 MB each
Reading from 9964: heap size 1637 MB, throughput 0.996123
Reading from 9964: heap size 1637 MB, throughput 0.996064
Reading from 9963: heap size 1372 MB, throughput 0.9755
Reading from 9964: heap size 1637 MB, throughput 0.996081
Reading from 9964: heap size 1637 MB, throughput 0.995938
Reading from 9963: heap size 1380 MB, throughput 0.974742
Reading from 9964: heap size 1637 MB, throughput 0.995937
Equal recommendation: 2452 MB each
Reading from 9964: heap size 1637 MB, throughput 0.996104
Reading from 9963: heap size 1381 MB, throughput 0.968667
Reading from 9964: heap size 1637 MB, throughput 0.995979
Reading from 9964: heap size 1637 MB, throughput 0.99608
Reading from 9964: heap size 1637 MB, throughput 0.996022
Reading from 9963: heap size 1381 MB, throughput 0.965987
Reading from 9964: heap size 1637 MB, throughput 0.996032
Reading from 9964: heap size 1637 MB, throughput 0.995946
Equal recommendation: 2452 MB each
Reading from 9964: heap size 1637 MB, throughput 0.996086
Reading from 9964: heap size 1637 MB, throughput 0.99601
Reading from 9964: heap size 1637 MB, throughput 0.995986
Reading from 9964: heap size 1637 MB, throughput 0.996036
Reading from 9964: heap size 1637 MB, throughput 0.996007
Equal recommendation: 2452 MB each
Reading from 9964: heap size 1637 MB, throughput 0.996159
Reading from 9964: heap size 1637 MB, throughput 0.995913
Reading from 9964: heap size 1637 MB, throughput 0.995878
Reading from 9964: heap size 1637 MB, throughput 0.995884
Reading from 9964: heap size 1637 MB, throughput 0.997305
Reading from 9963: heap size 1384 MB, throughput 0.859854
Equal recommendation: 2452 MB each
Reading from 9964: heap size 1637 MB, throughput 0.996743
Reading from 9964: heap size 1637 MB, throughput 0.995996
Reading from 9964: heap size 1637 MB, throughput 0.995983
Reading from 9964: heap size 1637 MB, throughput 0.996321
Reading from 9964: heap size 1637 MB, throughput 0.996261
Reading from 9964: heap size 1637 MB, throughput 0.996282
Equal recommendation: 2452 MB each
Reading from 9964: heap size 1637 MB, throughput 0.996419
Reading from 9964: heap size 1637 MB, throughput 0.99632
Reading from 9963: heap size 1488 MB, throughput 0.981944
Reading from 9964: heap size 1637 MB, throughput 0.995997
Reading from 9964: heap size 1637 MB, throughput 0.995902
Reading from 9964: heap size 1638 MB, throughput 0.996105
Reading from 9963: heap size 1526 MB, throughput 0.981337
Reading from 9964: heap size 1638 MB, throughput 0.99513
Reading from 9963: heap size 1535 MB, throughput 0.829528
Equal recommendation: 2452 MB each
Reading from 9963: heap size 1540 MB, throughput 0.654291
Reading from 9963: heap size 1558 MB, throughput 0.65081
Reading from 9963: heap size 1577 MB, throughput 0.682507
Reading from 9963: heap size 1608 MB, throughput 0.715892
Reading from 9963: heap size 1620 MB, throughput 0.671614
Reading from 9964: heap size 1637 MB, throughput 0.996665
Reading from 9963: heap size 1655 MB, throughput 0.871258
Reading from 9964: heap size 1637 MB, throughput 0.996592
Reading from 9964: heap size 1637 MB, throughput 0.99616
Reading from 9963: heap size 1661 MB, throughput 0.968077
Reading from 9964: heap size 1637 MB, throughput 0.99635
Reading from 9964: heap size 1637 MB, throughput 0.996229
Equal recommendation: 2452 MB each
Reading from 9964: heap size 1637 MB, throughput 0.996331
Reading from 9963: heap size 1665 MB, throughput 0.97332
Reading from 9964: heap size 1637 MB, throughput 0.996312
Reading from 9964: heap size 1637 MB, throughput 0.996025
Reading from 9963: heap size 1677 MB, throughput 0.972723
Reading from 9964: heap size 1637 MB, throughput 0.99614
Reading from 9964: heap size 1637 MB, throughput 0.995966
Equal recommendation: 2452 MB each
Reading from 9964: heap size 1637 MB, throughput 0.996352
Reading from 9963: heap size 1671 MB, throughput 0.969596
Reading from 9964: heap size 1637 MB, throughput 0.996119
Reading from 9964: heap size 1637 MB, throughput 0.996141
Reading from 9963: heap size 1684 MB, throughput 0.969008
Reading from 9964: heap size 1637 MB, throughput 0.996273
Reading from 9964: heap size 1637 MB, throughput 0.996046
Reading from 9964: heap size 1637 MB, throughput 0.996411
Equal recommendation: 2452 MB each
Reading from 9963: heap size 1672 MB, throughput 0.973894
Reading from 9964: heap size 1637 MB, throughput 0.995893
Reading from 9964: heap size 1637 MB, throughput 0.996052
Reading from 9964: heap size 1637 MB, throughput 0.996215
Reading from 9963: heap size 1685 MB, throughput 0.968827
Reading from 9964: heap size 1637 MB, throughput 0.996094
Reading from 9964: heap size 1637 MB, throughput 0.996159
Equal recommendation: 2452 MB each
Reading from 9964: heap size 1637 MB, throughput 0.996025
Reading from 9963: heap size 1693 MB, throughput 0.979644
Reading from 9964: heap size 1637 MB, throughput 0.995921
Reading from 9964: heap size 1637 MB, throughput 0.996165
Reading from 9964: heap size 1637 MB, throughput 0.994734
Reading from 9963: heap size 1695 MB, throughput 0.975553
Reading from 9964: heap size 1637 MB, throughput 0.995968
Reading from 9964: heap size 1637 MB, throughput 0.996302
Equal recommendation: 2452 MB each
Reading from 9964: heap size 1637 MB, throughput 0.996337
Reading from 9963: heap size 1696 MB, throughput 0.979728
Reading from 9964: heap size 1637 MB, throughput 0.99685
Reading from 9964: heap size 1637 MB, throughput 0.996387
Reading from 9964: heap size 1637 MB, throughput 0.996122
Reading from 9963: heap size 1702 MB, throughput 0.971981
Reading from 9964: heap size 1637 MB, throughput 0.996216
Equal recommendation: 2452 MB each
Reading from 9964: heap size 1637 MB, throughput 0.995899
Reading from 9964: heap size 1637 MB, throughput 0.996113
Reading from 9963: heap size 1690 MB, throughput 0.976059
Reading from 9964: heap size 1637 MB, throughput 0.996553
Reading from 9964: heap size 1637 MB, throughput 0.995975
Reading from 9964: heap size 1637 MB, throughput 0.996143
Reading from 9963: heap size 1699 MB, throughput 0.973648
Reading from 9964: heap size 1637 MB, throughput 0.99614
Equal recommendation: 2452 MB each
Reading from 9964: heap size 1637 MB, throughput 0.996282
Client 9964 died
Clients: 1
Reading from 9963: heap size 1699 MB, throughput 0.983075
Recommendation: one client; give it all the memory
Reading from 9963: heap size 1701 MB, throughput 0.982239
Reading from 9963: heap size 1709 MB, throughput 0.982463
Recommendation: one client; give it all the memory
Reading from 9963: heap size 1714 MB, throughput 0.980955
Recommendation: one client; give it all the memory
Reading from 9963: heap size 1723 MB, throughput 0.982358
Reading from 9963: heap size 1728 MB, throughput 0.846477
Recommendation: one client; give it all the memory
Reading from 9963: heap size 1704 MB, throughput 0.996491
Reading from 9963: heap size 1708 MB, throughput 0.995108
Recommendation: one client; give it all the memory
Reading from 9963: heap size 1724 MB, throughput 0.993405
Recommendation: one client; give it all the memory
Reading from 9963: heap size 1730 MB, throughput 0.992848
Reading from 9963: heap size 1729 MB, throughput 0.991536
Recommendation: one client; give it all the memory
Reading from 9963: heap size 1555 MB, throughput 0.990908
Recommendation: one client; give it all the memory
Reading from 9963: heap size 1713 MB, throughput 0.989538
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 9963: heap size 1587 MB, throughput 0.991861
Recommendation: one client; give it all the memory
Reading from 9963: heap size 1694 MB, throughput 0.995299
Recommendation: one client; give it all the memory
Reading from 9963: heap size 1716 MB, throughput 0.887479
Reading from 9963: heap size 1696 MB, throughput 0.806537
Reading from 9963: heap size 1729 MB, throughput 0.785904
Reading from 9963: heap size 1763 MB, throughput 0.813147
Reading from 9963: heap size 1783 MB, throughput 0.851557
Client 9963 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
