
SPECjbb2013 Java Business Benchmark
 (c) Standard Performance Evaluation Corporation, 2013

Preparing to launch All-in-one SPECjbb2013.

Reading property file: /home/callum/economics_memory_code/dynamic_sizing/specjbb2013/composite.props

     0s: Enumerating plugins...
     0s:    Connectivity:
     0s:             HTTP_Grizzly: Grizzly HTTP server, JDK HTTP client
     0s:              NIO_Grizzly: Grizzly NIO server, Grizzly NIO client
     0s:               HTTP_Jetty: Jetty HTTP server, JDK HTTP client
     0s:    Snapshot:
     0s:                 InMemory: Stores snapshots in heap memory
     0s:    Data Writers:
     0s:                     Demo: Send all frame to listener
     0s:                   InFile: Writing Data into file
     0s:                   Silent: Drop all frames
     0s: 
     0s: Validating kit integrity...
     0s:   Exception while reading the bundle: Java heap space
     1s: 