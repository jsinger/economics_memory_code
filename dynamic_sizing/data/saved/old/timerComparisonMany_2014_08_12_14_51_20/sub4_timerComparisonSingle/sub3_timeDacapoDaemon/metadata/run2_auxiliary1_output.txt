economemd
    total memory: 100 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_08_12_14_51_20/sub4_timerComparisonSingle/sub3_timeDacapoDaemon/daemon_run2_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 24255: heap size 9 MB, throughput 0.981743
Clients: 1
Reading from 24257: heap size 9 MB, throughput 0.991563
Clients: 2
Reading from 24255: heap size 9 MB, throughput 0.967689
Reading from 24257: heap size 9 MB, throughput 0.987902
Reading from 24255: heap size 11 MB, throughput 0.972428
Reading from 24257: heap size 9 MB, throughput 0.982965
Reading from 24257: heap size 9 MB, throughput 0.943879
Reading from 24255: heap size 11 MB, throughput 0.943562
Reading from 24257: heap size 11 MB, throughput 0.97278
Reading from 24257: heap size 11 MB, throughput 0.97353
Reading from 24255: heap size 15 MB, throughput 0.824755
Reading from 24257: heap size 17 MB, throughput 0.980499
Reading from 24257: heap size 17 MB, throughput 0.850232
Reading from 24257: heap size 30 MB, throughput 0.742977
Reading from 24257: heap size 31 MB, throughput 0.0627716
Reading from 24257: heap size 36 MB, throughput 0.773468
Reading from 24257: heap size 48 MB, throughput 0.419916
Reading from 24255: heap size 23 MB, throughput 0.977298
Reading from 24257: heap size 52 MB, throughput 0.55345
Reading from 24257: heap size 65 MB, throughput 0.376289
Reading from 24257: heap size 72 MB, throughput 0.370516
Reading from 24257: heap size 89 MB, throughput 0.243847
Reading from 24257: heap size 95 MB, throughput 0.321412
Reading from 24255: heap size 26 MB, throughput 0.990116
Reading from 24257: heap size 97 MB, throughput 0.366281
Reading from 24257: heap size 132 MB, throughput 0.422307
Reading from 24257: heap size 132 MB, throughput 0.419543
Reading from 24257: heap size 171 MB, throughput 0.717741
Reading from 24257: heap size 171 MB, throughput 0.662469
Reading from 24257: heap size 172 MB, throughput 0.427924
Reading from 24257: heap size 175 MB, throughput 0.317453
Reading from 24255: heap size 29 MB, throughput 0.994035
Reading from 24257: heap size 177 MB, throughput 0.501547
Reading from 24257: heap size 222 MB, throughput 0.387088
Reading from 24257: heap size 224 MB, throughput 0.290309
Reading from 24255: heap size 32 MB, throughput 0.996044
Reading from 24257: heap size 228 MB, throughput 0.552588
Reading from 24257: heap size 286 MB, throughput 0.165795
Reading from 24257: heap size 286 MB, throughput 0.337895
Reading from 24255: heap size 32 MB, throughput 0.996692
Reading from 24257: heap size 286 MB, throughput 0.597238
Reading from 24257: heap size 287 MB, throughput 0.758914
Reading from 24257: heap size 285 MB, throughput 0.868412
Reading from 24257: heap size 288 MB, throughput 0.855353
Reading from 24257: heap size 291 MB, throughput 0.856449
Reading from 24257: heap size 292 MB, throughput 0.874583
Reading from 24255: heap size 33 MB, throughput 0.996605
Reading from 24257: heap size 296 MB, throughput 0.941351
Reading from 24257: heap size 296 MB, throughput 0.94407
Reading from 24257: heap size 295 MB, throughput 0.91142
Reading from 24257: heap size 298 MB, throughput 0.878845
Reading from 24257: heap size 303 MB, throughput 0.182425
Reading from 24257: heap size 304 MB, throughput 0.146493
Reading from 24257: heap size 306 MB, throughput 0.297657
Reading from 24257: heap size 307 MB, throughput 0.29048
Reading from 24255: heap size 33 MB, throughput 0.994182
Reading from 24257: heap size 307 MB, throughput 0.135225
Reading from 24257: heap size 310 MB, throughput 0.715297
Reading from 24257: heap size 317 MB, throughput 0.639275
Reading from 24257: heap size 320 MB, throughput 0.592575
Reading from 24257: heap size 329 MB, throughput 0.671945
Numeric result:
Recommendation: 2 clients, utility 0.575069:
    h1: 98 MB (U(h) = 0.8238*h^0.0517057)
    h2: 2 MB (U(h) = 0.550351*h^0.001)
Recommendation: 2 clients, utility 0.575069:
    h1: 98 MB (U(h) = 0.8238*h^0.0517057)
    h2: 2 MB (U(h) = 0.550351*h^0.001)
Reading from 24255: heap size 34 MB, throughput 0.994227
Reading from 24257: heap size 330 MB, throughput 0.936732
Reading from 24255: heap size 34 MB, throughput 0.990418
Reading from 24257: heap size 339 MB, throughput 0.961494
Reading from 24255: heap size 35 MB, throughput 0.990828
Reading from 24257: heap size 338 MB, throughput 0.967671
Reading from 24255: heap size 35 MB, throughput 0.993556
Reading from 24255: heap size 37 MB, throughput 0.994743
Reading from 24255: heap size 37 MB, throughput 0.995076
Reading from 24255: heap size 37 MB, throughput 0.99379
Numeric result:
Recommendation: 2 clients, utility 0.513102:
    h1: 74.9592 MB (U(h) = 0.821273*h^0.052892)
    h2: 25.0408 MB (U(h) = 0.469721*h^0.0176691)
Recommendation: 2 clients, utility 0.513102:
    h1: 74.9592 MB (U(h) = 0.821273*h^0.052892)
    h2: 25.0408 MB (U(h) = 0.469721*h^0.0176691)
Reading from 24255: heap size 38 MB, throughput 0.993701
Reading from 24255: heap size 38 MB, throughput 0.939276
Reading from 24255: heap size 39 MB, throughput 0.855983
Reading from 24255: heap size 37 MB, throughput 0.993625
Reading from 24255: heap size 39 MB, throughput 0.99228
Reading from 24255: heap size 40 MB, throughput 0.993778
Reading from 24255: heap size 40 MB, throughput 0.995478
Numeric result:
Recommendation: 2 clients, utility 0.501966:
    h1: 68.7856 MB (U(h) = 0.852872*h^0.0389369)
    h2: 31.2144 MB (U(h) = 0.469721*h^0.0176691)
Recommendation: 2 clients, utility 0.501966:
    h1: 68.7859 MB (U(h) = 0.852872*h^0.0389369)
    h2: 31.2141 MB (U(h) = 0.469721*h^0.0176691)
Reading from 24255: heap size 41 MB, throughput 0.996457
Reading from 24255: heap size 41 MB, throughput 0.996175
Reading from 24255: heap size 42 MB, throughput 0.996726
Reading from 24255: heap size 42 MB, throughput 0.99701
Reading from 24255: heap size 42 MB, throughput 0.996817
Reading from 24255: heap size 42 MB, throughput 0.996834
Reading from 24255: heap size 43 MB, throughput 0.996207
Reading from 24255: heap size 43 MB, throughput 0.996888
Numeric result:
Recommendation: 2 clients, utility 0.504342:
    h1: 70.4902 MB (U(h) = 0.845109*h^0.0422045)
    h2: 29.5098 MB (U(h) = 0.469721*h^0.0176691)
Recommendation: 2 clients, utility 0.504342:
    h1: 70.4894 MB (U(h) = 0.845109*h^0.0422045)
    h2: 29.5106 MB (U(h) = 0.469721*h^0.0176691)
Reading from 24255: heap size 42 MB, throughput 0.996867
Reading from 24255: heap size 42 MB, throughput 0.997153
Reading from 24255: heap size 42 MB, throughput 0.997449
Reading from 24255: heap size 42 MB, throughput 0.997741
Reading from 24255: heap size 42 MB, throughput 0.9977
Reading from 24255: heap size 42 MB, throughput 0.997212
Numeric result:
Recommendation: 2 clients, utility 0.504386:
    h1: 70.5201 MB (U(h) = 0.844965*h^0.0422652)
    h2: 29.4799 MB (U(h) = 0.469721*h^0.0176691)
Recommendation: 2 clients, utility 0.504386:
    h1: 70.5193 MB (U(h) = 0.844965*h^0.0422652)
    h2: 29.4807 MB (U(h) = 0.469721*h^0.0176691)
Reading from 24255: heap size 42 MB, throughput 0.997375
Reading from 24255: heap size 42 MB, throughput 0.997571
Reading from 24255: heap size 42 MB, throughput 0.997878
Reading from 24255: heap size 42 MB, throughput 0.997961
Reading from 24255: heap size 43 MB, throughput 0.99779
Reading from 24255: heap size 43 MB, throughput 0.998001
Numeric result:
Recommendation: 2 clients, utility 0.504504:
    h1: 70.5991 MB (U(h) = 0.844575*h^0.0424287)
    h2: 29.4009 MB (U(h) = 0.469721*h^0.0176691)
Recommendation: 2 clients, utility 0.504504:
    h1: 70.5995 MB (U(h) = 0.844575*h^0.0424287)
    h2: 29.4005 MB (U(h) = 0.469721*h^0.0176691)
Reading from 24255: heap size 43 MB, throughput 0.998078
Reading from 24255: heap size 43 MB, throughput 0.998043
Reading from 24255: heap size 42 MB, throughput 0.99811
Reading from 24255: heap size 43 MB, throughput 0.997091
Reading from 24255: heap size 43 MB, throughput 0.997976
Reading from 24255: heap size 43 MB, throughput 0.997648
Numeric result:
Recommendation: 2 clients, utility 0.504525:
    h1: 70.6131 MB (U(h) = 0.844508*h^0.042457)
    h2: 29.3869 MB (U(h) = 0.469721*h^0.0176691)
Recommendation: 2 clients, utility 0.504525:
    h1: 70.6133 MB (U(h) = 0.844508*h^0.042457)
    h2: 29.3867 MB (U(h) = 0.469721*h^0.0176691)
Reading from 24255: heap size 44 MB, throughput 0.997663
Reading from 24255: heap size 44 MB, throughput 0.998116
Reading from 24255: heap size 44 MB, throughput 0.996008
Reading from 24255: heap size 44 MB, throughput 0.996694
Reading from 24255: heap size 44 MB, throughput 0.997318
Numeric result:
Recommendation: 2 clients, utility 0.504938:
    h1: 70.8958 MB (U(h) = 0.843098*h^0.0430414)
    h2: 29.1042 MB (U(h) = 0.469721*h^0.0176691)
Recommendation: 2 clients, utility 0.504938:
    h1: 70.8962 MB (U(h) = 0.843098*h^0.0430414)
    h2: 29.1038 MB (U(h) = 0.469721*h^0.0176691)
Reading from 24255: heap size 44 MB, throughput 0.997899
Reading from 24255: heap size 45 MB, throughput 0.995504
Reading from 24257: heap size 5471 MB, throughput 0.998246
Reading from 24255: heap size 45 MB, throughput 0.996618
Reading from 24255: heap size 45 MB, throughput 0.997772
Reading from 24255: heap size 45 MB, throughput 0.998125
Numeric result:
Recommendation: 2 clients, utility 0.487714:
    h1: 43.3857 MB (U(h) = 0.84191*h^0.0435306)
    h2: 56.6143 MB (U(h) = 0.390888*h^0.0568036)
Recommendation: 2 clients, utility 0.487714:
    h1: 43.3856 MB (U(h) = 0.84191*h^0.0435306)
    h2: 56.6144 MB (U(h) = 0.390888*h^0.0568036)
Reading from 24255: heap size 45 MB, throughput 0.997997
Reading from 24255: heap size 44 MB, throughput 0.997968
Reading from 24255: heap size 44 MB, throughput 0.997634
Reading from 24255: heap size 43 MB, throughput 0.997839
Reading from 24255: heap size 43 MB, throughput 0.995741
Numeric result:
Recommendation: 2 clients, utility 0.487693:
    h1: 43.3611 MB (U(h) = 0.84201*h^0.0434876)
    h2: 56.6389 MB (U(h) = 0.390888*h^0.0568036)
Recommendation: 2 clients, utility 0.487693:
    h1: 43.3613 MB (U(h) = 0.84201*h^0.0434876)
    h2: 56.6387 MB (U(h) = 0.390888*h^0.0568036)
Reading from 24255: heap size 43 MB, throughput 0.99656
Reading from 24255: heap size 42 MB, throughput 0.997305
Reading from 24255: heap size 43 MB, throughput 0.99771
Reading from 24255: heap size 43 MB, throughput 0.996636
Reading from 24255: heap size 43 MB, throughput 0.997452
Reading from 24255: heap size 42 MB, throughput 0.997814
Numeric result:
Recommendation: 2 clients, utility 0.487702:
    h1: 43.3718 MB (U(h) = 0.841965*h^0.0435062)
    h2: 56.6282 MB (U(h) = 0.390888*h^0.0568036)
Recommendation: 2 clients, utility 0.487702:
    h1: 43.3718 MB (U(h) = 0.841965*h^0.0435062)
    h2: 56.6282 MB (U(h) = 0.390888*h^0.0568036)
Reading from 24255: heap size 41 MB, throughput 0.997839
Reading from 24255: heap size 41 MB, throughput 0.996306
Reading from 24255: heap size 42 MB, throughput 0.996968
Reading from 24255: heap size 40 MB, throughput 0.997553
Reading from 24255: heap size 40 MB, throughput 0.997876
Numeric result:
Recommendation: 2 clients, utility 0.487794:
    h1: 43.4686 MB (U(h) = 0.841579*h^0.043678)
    h2: 56.5314 MB (U(h) = 0.390888*h^0.0568036)
Recommendation: 2 clients, utility 0.487794:
    h1: 43.4686 MB (U(h) = 0.841579*h^0.043678)
    h2: 56.5314 MB (U(h) = 0.390888*h^0.0568036)
Reading from 24255: heap size 39 MB, throughput 0.997965
Reading from 24255: heap size 39 MB, throughput 0.997925
Reading from 24255: heap size 38 MB, throughput 0.997632
Reading from 24255: heap size 38 MB, throughput 0.998222
Reading from 24255: heap size 38 MB, throughput 0.997824
Reading from 24255: heap size 38 MB, throughput 0.997731
Reading from 24255: heap size 37 MB, throughput 0.997599
Reading from 24255: heap size 36 MB, throughput 0.997438
Numeric result:
Recommendation: 2 clients, utility 0.491546:
    h1: 46.7392 MB (U(h) = 0.82835*h^0.0498485)
    h2: 53.2608 MB (U(h) = 0.390888*h^0.0568036)
Recommendation: 2 clients, utility 0.491546:
    h1: 46.7393 MB (U(h) = 0.82835*h^0.0498485)
    h2: 53.2607 MB (U(h) = 0.390888*h^0.0568036)
Reading from 24255: heap size 36 MB, throughput 0.995564
Reading from 24255: heap size 35 MB, throughput 0.996574
Reading from 24255: heap size 35 MB, throughput 0.997076
Reading from 24255: heap size 34 MB, throughput 0.993351
Reading from 24255: heap size 35 MB, throughput 0.994979
Reading from 24255: heap size 35 MB, throughput 0.99584
Reading from 24255: heap size 35 MB, throughput 0.996265
Reading from 24255: heap size 34 MB, throughput 0.996795
Numeric result:
Recommendation: 2 clients, utility 0.491715:
    h1: 46.8265 MB (U(h) = 0.828077*h^0.0500236)
    h2: 53.1735 MB (U(h) = 0.390888*h^0.0568036)
Recommendation: 2 clients, utility 0.491715:
    h1: 46.8266 MB (U(h) = 0.828077*h^0.0500236)
    h2: 53.1734 MB (U(h) = 0.390888*h^0.0568036)
Reading from 24255: heap size 35 MB, throughput 0.99717
Reading from 24255: heap size 35 MB, throughput 0.996996
Reading from 24255: heap size 35 MB, throughput 0.996399
Reading from 24255: heap size 36 MB, throughput 0.997846
Reading from 24255: heap size 35 MB, throughput 0.99821
Reading from 24255: heap size 36 MB, throughput 0.997904
Reading from 24255: heap size 36 MB, throughput 0.997963
Reading from 24255: heap size 37 MB, throughput 0.997332
Reading from 24255: heap size 37 MB, throughput 0.997075
Reading from 24255: heap size 37 MB, throughput 0.996836
Numeric result:
Recommendation: 2 clients, utility 0.491843:
    h1: 46.908 MB (U(h) = 0.82777*h^0.0501878)
    h2: 53.092 MB (U(h) = 0.390888*h^0.0568036)
Recommendation: 2 clients, utility 0.491843:
    h1: 46.9082 MB (U(h) = 0.82777*h^0.0501878)
    h2: 53.0918 MB (U(h) = 0.390888*h^0.0568036)
Reading from 24255: heap size 38 MB, throughput 0.996423
Reading from 24255: heap size 38 MB, throughput 0.986836
Reading from 24257: heap size 5470 MB, throughput 0.989231
Reading from 24255: heap size 39 MB, throughput 0.991976
Reading from 24255: heap size 39 MB, throughput 0.994957
Reading from 24255: heap size 39 MB, throughput 0.996185
Reading from 24255: heap size 39 MB, throughput 0.996668
Reading from 24255: heap size 39 MB, throughput 0.994342
Reading from 24255: heap size 39 MB, throughput 0.995602
Numeric result:
Recommendation: 2 clients, utility 0.48505:
    h1: 39.0313 MB (U(h) = 0.826088*h^0.0509535)
    h2: 60.9687 MB (U(h) = 0.351231*h^0.0795918)
Recommendation: 2 clients, utility 0.48505:
    h1: 39.0313 MB (U(h) = 0.826088*h^0.0509535)
    h2: 60.9687 MB (U(h) = 0.351231*h^0.0795918)
Reading from 24255: heap size 40 MB, throughput 0.989528
Reading from 24255: heap size 40 MB, throughput 0.962523
Reading from 24255: heap size 40 MB, throughput 0.886028
Reading from 24255: heap size 40 MB, throughput 0.809509
Reading from 24255: heap size 40 MB, throughput 0.911678
Reading from 24255: heap size 37 MB, throughput 0.978398
Reading from 24255: heap size 40 MB, throughput 0.987815
Numeric result:
Recommendation: 2 clients, utility 0.482867:
    h1: 36.7665 MB (U(h) = 0.836466*h^0.0462783)
    h2: 63.2335 MB (U(h) = 0.351231*h^0.0795918)
Recommendation: 2 clients, utility 0.482867:
    h1: 36.7667 MB (U(h) = 0.836466*h^0.0462783)
    h2: 63.2333 MB (U(h) = 0.351231*h^0.0795918)
Reading from 24255: heap size 40 MB, throughput 0.992718
Reading from 24255: heap size 40 MB, throughput 0.995219
Reading from 24255: heap size 40 MB, throughput 0.996482
Reading from 24255: heap size 40 MB, throughput 0.996407
Reading from 24255: heap size 40 MB, throughput 0.99557
Reading from 24255: heap size 39 MB, throughput 0.99655
Reading from 24255: heap size 39 MB, throughput 0.993389
Reading from 24255: heap size 39 MB, throughput 0.995083
Numeric result:
Recommendation: 2 clients, utility 0.484659:
    h1: 38.7005 MB (U(h) = 0.827552*h^0.0502491)
    h2: 61.2995 MB (U(h) = 0.351231*h^0.0795918)
Recommendation: 2 clients, utility 0.484659:
    h1: 38.7005 MB (U(h) = 0.827552*h^0.0502491)
    h2: 61.2995 MB (U(h) = 0.351231*h^0.0795918)
Reading from 24255: heap size 39 MB, throughput 0.99551
Reading from 24255: heap size 38 MB, throughput 0.996718
Reading from 24255: heap size 39 MB, throughput 0.997056
Reading from 24255: heap size 39 MB, throughput 0.994302
Reading from 24255: heap size 39 MB, throughput 0.996032
Reading from 24255: heap size 40 MB, throughput 0.996641
Reading from 24255: heap size 38 MB, throughput 0.997524
Numeric result:
Recommendation: 2 clients, utility 0.484864:
    h1: 38.8865 MB (U(h) = 0.826706*h^0.0506442)
    h2: 61.1135 MB (U(h) = 0.351231*h^0.0795918)
Recommendation: 2 clients, utility 0.484864:
    h1: 38.8865 MB (U(h) = 0.826706*h^0.0506442)
    h2: 61.1135 MB (U(h) = 0.351231*h^0.0795918)
Reading from 24255: heap size 39 MB, throughput 0.997003
Reading from 24255: heap size 39 MB, throughput 0.994551
Reading from 24255: heap size 39 MB, throughput 0.995383
Reading from 24255: heap size 39 MB, throughput 0.996679
Reading from 24255: heap size 38 MB, throughput 0.994303
Reading from 24255: heap size 39 MB, throughput 0.996081
Reading from 24255: heap size 39 MB, throughput 0.996983
Numeric result:
Recommendation: 2 clients, utility 0.484869:
    h1: 38.8932 MB (U(h) = 0.826672*h^0.0506585)
    h2: 61.1068 MB (U(h) = 0.351231*h^0.0795918)
Recommendation: 2 clients, utility 0.484869:
    h1: 38.8932 MB (U(h) = 0.826672*h^0.0506585)
    h2: 61.1068 MB (U(h) = 0.351231*h^0.0795918)
Reading from 24255: heap size 39 MB, throughput 0.99721
Reading from 24255: heap size 39 MB, throughput 0.997683
Reading from 24255: heap size 39 MB, throughput 0.997842
Reading from 24255: heap size 39 MB, throughput 0.997789
Reading from 24255: heap size 38 MB, throughput 0.994057
Reading from 24255: heap size 39 MB, throughput 0.995714
Reading from 24255: heap size 39 MB, throughput 0.996325
Reading from 24255: heap size 39 MB, throughput 0.997088
Numeric result:
Recommendation: 2 clients, utility 0.484862:
    h1: 38.8879 MB (U(h) = 0.826694*h^0.0506472)
    h2: 61.1121 MB (U(h) = 0.351231*h^0.0795918)
Recommendation: 2 clients, utility 0.484862:
    h1: 38.8879 MB (U(h) = 0.826694*h^0.0506472)
    h2: 61.1121 MB (U(h) = 0.351231*h^0.0795918)
Reading from 24255: heap size 39 MB, throughput 0.996647
Reading from 24257: heap size 5489 MB, throughput 0.998987
Reading from 24255: heap size 39 MB, throughput 0.99481
Reading from 24255: heap size 39 MB, throughput 0.996447
Reading from 24255: heap size 39 MB, throughput 0.997445
Reading from 24255: heap size 39 MB, throughput 0.997073
Reading from 24255: heap size 39 MB, throughput 0.99769
Numeric result:
Recommendation: 2 clients, utility 0.48077:
    h1: 34.7609 MB (U(h) = 0.826623*h^0.0506801)
    h2: 65.2391 MB (U(h) = 0.326541*h^0.0951167)
Recommendation: 2 clients, utility 0.48077:
    h1: 34.7608 MB (U(h) = 0.826623*h^0.0506801)
    h2: 65.2392 MB (U(h) = 0.326541*h^0.0951167)
Reading from 24255: heap size 39 MB, throughput 0.997712
Reading from 24255: heap size 39 MB, throughput 0.998111
Reading from 24255: heap size 39 MB, throughput 0.997675
Reading from 24255: heap size 39 MB, throughput 0.997696
Reading from 24255: heap size 39 MB, throughput 0.993579
Reading from 24255: heap size 38 MB, throughput 0.995363
Reading from 24255: heap size 39 MB, throughput 0.994619
Numeric result:
Recommendation: 2 clients, utility 0.480719:
    h1: 34.7083 MB (U(h) = 0.826879*h^0.050563)
    h2: 65.2917 MB (U(h) = 0.326541*h^0.0951167)
Recommendation: 2 clients, utility 0.480719:
    h1: 34.7083 MB (U(h) = 0.826879*h^0.050563)
    h2: 65.2917 MB (U(h) = 0.326541*h^0.0951167)
Reading from 24255: heap size 38 MB, throughput 0.995813
Reading from 24255: heap size 39 MB, throughput 0.994938
Reading from 24255: heap size 38 MB, throughput 0.99551
Reading from 24255: heap size 39 MB, throughput 0.993978
Reading from 24255: heap size 38 MB, throughput 0.993881
Reading from 24255: heap size 39 MB, throughput 0.99365
Reading from 24255: heap size 38 MB, throughput 0.994538
Reading from 24255: heap size 39 MB, throughput 0.991991
Numeric result:
Recommendation: 2 clients, utility 0.480649:
    h1: 34.6379 MB (U(h) = 0.827219*h^0.0504061)
    h2: 65.3621 MB (U(h) = 0.326541*h^0.0951167)
Recommendation: 2 clients, utility 0.480649:
    h1: 34.6379 MB (U(h) = 0.827219*h^0.0504061)
    h2: 65.3621 MB (U(h) = 0.326541*h^0.0951167)
Reading from 24255: heap size 39 MB, throughput 0.992714
Reading from 24255: heap size 39 MB, throughput 0.9953
Reading from 24255: heap size 39 MB, throughput 0.992192
Reading from 24255: heap size 40 MB, throughput 0.993427
Client 24255 died
Clients: 1
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 24257: heap size 5511 MB, throughput 0.999232
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Client 24257 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
