	Command being timed: "cgexec -g memory:economem /home/callum/economics_memory_code/dynamic_sizing/hotspot8/openjdk8/jdk8/build/linux-x86_64-normal-server-release/jdk/bin/java -XX:-EconomemVengerovThroughput -Xms10m -Xmx298m -XX:+DisableExplicitGC -XX:+UseAdaptiveSizePolicyWithSystemGC -jar /home/callum/economics_memory_code/dynamic_sizing/haskell_common/dacapo-9.12-bach.jar --no-pre-iteration-gc --scratch-directory /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub25_timerComparisonSingle/sub1_timeBenchmarkCgroupFair/scratch1 -t 2 -n 13 -s large h2"
	User time (seconds): 468.66
	System time (seconds): 3.98
	Percent of CPU this job got: 127%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 6:11.64
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 394428
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 5780
	Minor (reclaiming a frame) page faults: 272189
	Voluntary context switches: 232432
	Involuntary context switches: 217339
	Swaps: 0
	File system inputs: 381080
	File system outputs: 16256
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
