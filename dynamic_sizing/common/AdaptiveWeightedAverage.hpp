#ifndef ADAPTIVEWEIGHTEDAVERAGE_HPP
#define ADAPTIVEWEIGHTEDAVERAGE_HPP

#include <cstdlib>
#include <stdint.h>

/*! \addtogroup common
  @{
*/

namespace economem
{
    //! An exponentially-decaying average
    /*! Based on the OpenJDK sources. */
    class AdaptiveWeightedAverage
    {
    public:
        //! Input weight must be between 0 and 100, where a higher weight favours the most recent data.
        AdaptiveWeightedAverage(unsigned weight);

        //! Input weight must be between 0 and 100, where a higher weight favours the most recent data.
        AdaptiveWeightedAverage(unsigned weight, double firstSample);

        AdaptiveWeightedAverage& operator=(const AdaptiveWeightedAverage& a);

        double average() const;
        unsigned int weight() const;
        unsigned int count() const;
        double lastSample() const;
        bool isOld() const;

        void sample(double newSample);

    private:
        double m_Average;             // The last computed average
        double m_LastSample;          // The last value sampled.
        unsigned int m_SampleCount;   // How often we've sampled this average
        unsigned int m_Weight;        // The weight used to smooth the averages, between 0 and 100; a higher weight favours the most recent data.
        bool m_IsOld;                 // Has enough historical data

        const static unsigned int OLD_THRESHOLD = 100;

        void incrementCount();
        void setAverage(double avg);
        double computeAdaptiveAverage(double new_sample, double average);
        static double expAvg(double avg, double sample, unsigned int weight);
        static unsigned long long expAvg(unsigned long long avg, unsigned long long sample, unsigned int weight);
    };
}

/*! @} */

#endif
