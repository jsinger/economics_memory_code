import Economem.ResumeExperimentCmdline
import Economem.StaggeredStart

main :: IO ()
main = resume resumeStaggeredStart
