#!/usr/bin/env python
# Measure utility space points on a remote machine

import argparse
import argtypes
import glob
import distributor.distribute as d
import os.path
import shutil
import staticutility_core as su
import sys
import tempfile
from utilitySpacePoint import utilitySpacePoint


parser = argparse.ArgumentParser(description="Measure utility space points on a remote machine")

parser.add_argument("time", help="how long to run for, in a format suitable for input to 'sleep'")
parser.add_argument("runs", type=argtypes.positive_int, help="how many times to run the measurement, for taking averages")
parser.add_argument("outDir", help="where to save the output")

parser.add_argument("-b", "--benchmark", action="append", nargs=5, default=[], metavar=("benchmark", "time", "inputSize", "threads", "maxHeap"), help="a benchmark to include in the measurement; at least two are required")

args = parser.parse_args()
outDir = su.UtilitySpaceDir(args.outDir)

if len(args.benchmark) < 2:
    parser.error("At least two benchmarks must be specified")

bids = []
maxHeaps = []
for b in args.benchmark:
    bids.append(su.BenchmarkId(b[0], b[1], b[2], b[3]))
    maxHeaps.append(int(b[4]))

su.checkDaCapo()
outDir.makedir()
outDir.machineInfo()

resultsFile = outDir.hostRawFile(su.me())
tempResults = tempfile.mktemp()
tempDir = su.UtilitySpaceDir(tempfile.mkdtemp())
distributeRunDir = d.runDirFromEnv()

for line in sys.stdin:
    if distributeRunDir.aborted():
        break

    utilitySpacePoint(args.time, args.runs, tempDir, tempResults, bids, map(int, line.strip().split()), maxHeaps)

    for f in glob.glob(os.path.join(str(tempDir), "*")):
        shutil.move(f, str(outDir))

shutil.move(tempResults, resultsFile)
shutil.rmtree(str(tempDir))
