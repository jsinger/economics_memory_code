#!/bin/bash
# Collect the data from a remote minHeap run onto this machine. Assumes the remote data is not in a shared filesystem.
# If it is, it will be easier just to scp or git commit that data directly.
#
# Usage: minHeapCollect.sh distribute_dir
#
# distribute_dir should be the directory containing the distribute logs for the remote run.
#

echo "THIS HASN'T BEEN UPDATED TO THE NEW DISTRIBUTE!"
exit 1

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") distribute_dir"
    exit 1
}

test -z "${1}" && usage
test ! -d "${1}" && usage

DISTRIBUTE_DIR="$(readlink -f "${1}")"

RUNNING_HOSTS="${DISTRIBUTE_DIR}/runningHosts.txt"
REMOTE_ARGS_FILE="${DISTRIBUTE_DIR}/remoteArgs.txt"

function check()
{
    if [ ! -f "${1}" ]; then
        echo "$(basename "${1}") not found"
        exit 1
    fi
}

check "${RUNNING_HOSTS}"
check "${REMOTE_ARGS_FILE}"

REMOTE_DIR="$(cut '--delimiter= ' -f 1 "${REMOTE_ARGS_FILE}")"

OUTDIR="${DIR}/$(basename "${REMOTE_DIR}")"

if [ -e "${OUTDIR}" ]; then
    echo "${OUTDIR} already exists"
    exit 1
fi

mkdir -p "${OUTDIR}"

TEMPDIR="$(mktemp -d)"

while read line; do
    scp -r "${line}:${REMOTE_DIR}" "${TEMPDIR}"
    cd "${TEMPDIR}/$(basename "${REMOTE_DIR}")"
    mv -t "${OUTDIR}" *
    cd
    rmdir "${TEMPDIR}/$(basename "${REMOTE_DIR}")"
done < "${RUNNING_HOSTS}"

rmdir "${TEMPDIR}"
