#!/bin/bash
# For the static analyser to work, scons has to be wrapped in a scan-build invocation

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ "${1}" = 'clean' ]; then
    scons -f SConstruct.main -Q -c
else
    # Something odd is going on with scons and MCT on Ubuntu 14.04, so install it here instead
    if [ ! -e "${DIR}/mct" ]; then
        "${DIR}/installMCT.sh"
    fi

	scan-build -V --use-cc=clang --use-c++=clang++ scons -f SConstruct.main -Q "${@}"
fi
