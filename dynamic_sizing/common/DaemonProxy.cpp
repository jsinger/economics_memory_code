#include <cstdlib>
#include <list>
#include "DaemonProxy.hpp"
#include "ReadingSender.hpp"
#include "RecommendationReceiver.hpp"
#include "Utilities.hpp"

economem::DaemonProxy::DaemonProxy()
    : m_MinHeapMB(0),
      m_NewRecommendation(false),
      m_Receiver(),
      m_ReceiverThread(this, m_Receiver)
{
    if (!m_ReceiverThread.start())
        economem::warning("Failed to start DaemonProxy receiver thread");
}

economem::DaemonProxy::~DaemonProxy()
{
    sendDeath();
    m_ReceiverThread.kill();
    m_ReceiverThread.wait();

    for (std::list<Target>::iterator it = m_Targets.begin(); it != m_Targets.end(); ++it)
    {
        delete it->calc;
        delete it->sender;
    }
}

void economem::DaemonProxy::addTarget(ThroughputCalculator* calc, ReadingSender* sender)
{
    m_Targets.push_back(Target(calc, sender));
}

void economem::DaemonProxy::setMinHeapMB(unsigned long long minHeapMB)
{
    m_MinHeapMB = minHeapMB;
}

void economem::DaemonProxy::gcBegin()
{
    for (std::list<Target>::iterator it = m_Targets.begin(); it != m_Targets.end(); ++it)
        it->calc->gcBegin();
}

void economem::DaemonProxy::minorGCEnd()
{
    gcEnd(&ThroughputCalculator::minorGCEnd);
}

void economem::DaemonProxy::majorGCEnd()
{
    gcEnd(&ThroughputCalculator::majorGCEnd);
}

void economem::DaemonProxy::gcEnd(GCEndFunc func)
{
    std::list<Target>::iterator it = m_Targets.begin();

    while (it != m_Targets.end())
    {
        Target& t = *it;
        ThroughputCalculator::Result result = (t.calc->*func)();

        if (result.isValid())
        {
            if (t.sender->sendReading(result.heapSizeMB, result.throughput) &&
                (t.sentMinHeapMsg || m_MinHeapMB == 0 || t.sender->sendMinHeapMB(m_MinHeapMB)))
            {
                t.sentMinHeapMsg = true;
                ++it;
            }
            else
            {
                delete t.calc;
                delete t.sender;
                it = m_Targets.erase(it);
            }
        }
        else
            ++it;
    }
}

void economem::DaemonProxy::sendDeath()
{
    std::list<Target>::iterator it = m_Targets.begin();

    while (it != m_Targets.end())
    {
        Target& t = *it;
        if (t.sender->sendDeathNotification())
            ++it;
        else
        {
            economem::warning("failed to send death notification");
            delete t.calc;
            delete t.sender;
            it = m_Targets.erase(it);
        }
    }
}

bool economem::DaemonProxy::getRecommendation(economem::Recommendation& r)
{
    bool newRecommendation;
    m_Lock.lock();
    newRecommendation = m_NewRecommendation;
    if (newRecommendation)
    {
        r = m_Recommendation;
        m_NewRecommendation = false;
    }
    m_Lock.unlock();
    return newRecommendation;
}

void economem::DaemonProxy::setRecommendation(const economem::Recommendation& r)
{
    m_Lock.lock();
    m_Recommendation = r;
    m_NewRecommendation = true;
    m_Lock.unlock();
}

economem::DaemonProxy::Target::Target(ThroughputCalculator* calc, ReadingSender* sender)
    : calc(calc), sender(sender), sentMinHeapMsg(false) {}

economem::DaemonProxy::IncomingThread::IncomingThread(DaemonProxy* parent, RecommendationReceiver& receiver) : ReceiverThread<economem::Recommendation>(receiver, "RecommendationReceiver"), m_Parent(parent) {}

void economem::DaemonProxy::IncomingThread::received(const economem::Recommendation& msg)
{
    m_Parent->setRecommendation(msg);
}
