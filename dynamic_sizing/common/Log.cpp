#include <cstdarg>
#include "Utilities.hpp"
#include "Log.hpp"

economem::Log::Log(const std::string& filename, bool logReadings, bool logFunctions)
    : m_LogReadings(logReadings),
      m_LogFunctions(logFunctions),
      m_CloseFd(true),
      m_Fd(NULL)
{
    if ((m_Fd = fopen(filename.c_str(), "w")) == NULL)
        economem::die("Couldn't open reading log file '%s' for writing", filename.c_str());
    setlinebuf(m_Fd);
}

economem::Log::Log(FILE* fd, bool logReadings, bool logFunctions)
    : m_LogReadings(logReadings),
      m_LogFunctions(logFunctions),
      m_CloseFd(false),
      m_Fd(fd)
{

}

economem::Log::~Log()
{
    if (m_CloseFd)
        fclose(m_Fd);
}

void economem::Log::logCreation(pid_t pid)
{
    m_Lock.lock();
    if (m_LogReadings)
        log(pid, "CREATION");
    m_Lock.unlock();
}

void economem::Log::logReading(pid_t pid, unsigned long long heapSizeMB, double throughput)
{
    m_Lock.lock();
    if (m_LogReadings)
        log(pid, "READING", "%llu %f", heapSizeMB, throughput);
    m_Lock.unlock();
}

void economem::Log::logMinHeap(pid_t pid, unsigned long long heapSizeMB)
{
    m_Lock.lock();
    if (m_LogReadings)
        log(pid, "MINHEAP", "%llu", heapSizeMB);
    m_Lock.unlock();
}

void economem::Log::logFunction(pid_t pid, const std::string& cacheName, const std::string& func)
{
    m_Lock.lock();
    if (m_LogFunctions)
        log(pid, "FUNCTION", "%s %s", cacheName.c_str(), func.c_str());
    m_Lock.unlock();
}

void economem::Log::logRecommendation(pid_t pid, unsigned long long heapSizeMB)
{
    m_Lock.lock();
    if (m_LogReadings)
        log(pid, "RECOMMEND", "%llu", heapSizeMB);
    m_Lock.unlock();
}

void economem::Log::logDeath(pid_t pid, unsigned long long totalReadingsReceived)
{
    m_Lock.lock();
    if (m_LogReadings)
        log(pid, "DEATH", "%llu", totalReadingsReceived);
    m_Lock.unlock();
}

void economem::Log::logTotalMemMB(unsigned int totalMemMB)
{
    m_Lock.lock();
    if (m_LogReadings)
        log(0, "TOTAL_MEM", "%u", totalMemMB);
    m_Lock.unlock();
}

bool economem::Log::logsReadings()
{
    bool b;
    m_Lock.lock();
    b = m_LogReadings;
    m_Lock.unlock();
    return b;
}

void economem::Log::allowReadingLogging(bool allow)
{
    m_Lock.lock();
    m_LogReadings = allow;
    m_Lock.unlock();
}

bool economem::Log::logsFunctions()
{
    bool b;
    m_Lock.lock();
    b = m_LogFunctions;
    m_Lock.unlock();
    return b;
}

void economem::Log::allowFunctionLogging(bool allow)
{
    m_Lock.lock();
    m_LogFunctions = allow;
    m_Lock.unlock();
}

void economem::Log::log(pid_t pid, const char* event)
{
    fprintf(m_Fd, "%llu %u %s\n", economem::milliTime(), pid, event);
}

void economem::Log::log(pid_t pid, const char* event, const char* fmt, ...)
{
    fprintf(m_Fd, "%llu %u %s ", economem::milliTime(), pid, event);
    va_list args;
    va_start(args, fmt);
    vfprintf(m_Fd, fmt, args);
    va_end(args);
    fprintf(m_Fd, "\n");
}
