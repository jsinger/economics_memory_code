#include <cmath>
#include <sstream>
#include "LogUtilityModel.hpp"
#include "UtilityFunction.hpp"

const char* LogUtilityModel::NAME = "log";

class LogUtilityModel::LogUtilityFunction : public UtilityFunction
{
public:
    // U(h) = a ln(bh)
    double a;
    double b;

    LogUtilityFunction(double a, double b) : a(a), b(b) {}
    virtual double evaluate(double heapSize) const;
    virtual bool isValid() const;
    virtual void forceValidity();
    virtual std::string toString() const;
    virtual std::string toPython() const;
};

double LogUtilityModel::LogUtilityFunction::evaluate(double heapSize) const
{
    return a * std::log(b * heapSize);
}

bool LogUtilityModel::LogUtilityFunction::isValid() const
{
    return a > 0.0 && b > 0.0;
}

void LogUtilityModel::LogUtilityFunction::forceValidity()
{
    a = std::max(0.0, a);
    b = std::max(0.0, b);
}

std::string LogUtilityModel::LogUtilityFunction::toString() const
{
    std::stringstream s;
    s << "U(h) = " << a << " ln(" << b << "*h)";
    if (!isValid())
        s << " (INVALID)";
    return s.str();
}

std::string LogUtilityModel::LogUtilityFunction::toPython() const
{
    std::stringstream s;
    s << "lambda h: " << a << " * math.log(" << b << " * h)";
    return s.str();
}

UtilityFunction* LogUtilityModel::calculateFuncFromData(ReadingIterator& it) const
{
    std::pair<double, double> p = UtilityModel::fitStraightLine(it, log);
    return new LogUtilityFunction(p.second, std::exp(p.first/p.second));
}

PartitionResult* LogUtilityModel::partitionMemory(unsigned long long totalMem, const std::vector<const UtilityFunction*>& functions, const std::vector<unsigned long long>& minHeaps) const
{
    return partitionMemoryNumeric(totalMem, functions, minHeaps);
}

double LogUtilityModel::combineRawUtilities(const std::vector<double>& utilities) const
{
    double result = 1;
    for (double d : utilities)
        result *= d;
    return result;
}
