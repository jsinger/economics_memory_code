economemd
    total memory: 3759 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub22_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 21822: heap size 9 MB, throughput 0.992291
Clients: 1
Client 21822 has a minimum heap size of 30 MB
Reading from 21820: heap size 9 MB, throughput 0.992195
Clients: 2
Client 21820 has a minimum heap size of 1223 MB
Reading from 21822: heap size 9 MB, throughput 0.989124
Reading from 21822: heap size 9 MB, throughput 0.98415
Reading from 21820: heap size 9 MB, throughput 0.988936
Reading from 21822: heap size 9 MB, throughput 0.974837
Reading from 21822: heap size 11 MB, throughput 0.955847
Reading from 21822: heap size 11 MB, throughput 0.883888
Reading from 21820: heap size 9 MB, throughput 0.981596
Reading from 21822: heap size 15 MB, throughput 0.861917
Reading from 21822: heap size 16 MB, throughput 0.83267
Reading from 21822: heap size 23 MB, throughput 0.835035
Reading from 21822: heap size 23 MB, throughput 0.822593
Reading from 21822: heap size 32 MB, throughput 0.860736
Reading from 21822: heap size 32 MB, throughput 0.877064
Reading from 21822: heap size 47 MB, throughput 0.915615
Reading from 21820: heap size 9 MB, throughput 0.96192
Reading from 21822: heap size 47 MB, throughput 0.930729
Reading from 21822: heap size 70 MB, throughput 0.949034
Reading from 21822: heap size 70 MB, throughput 0.943475
Reading from 21822: heap size 107 MB, throughput 0.902304
Reading from 21822: heap size 107 MB, throughput 0.92357
Reading from 21820: heap size 11 MB, throughput 0.981601
Reading from 21820: heap size 11 MB, throughput 0.980183
Reading from 21820: heap size 17 MB, throughput 0.927951
Reading from 21820: heap size 17 MB, throughput 0.916125
Reading from 21820: heap size 30 MB, throughput 0.846416
Reading from 21820: heap size 31 MB, throughput 0.463599
Reading from 21822: heap size 147 MB, throughput 0.960786
Reading from 21820: heap size 35 MB, throughput 0.768143
Reading from 21820: heap size 44 MB, throughput 0.483723
Reading from 21820: heap size 61 MB, throughput 0.807974
Reading from 21820: heap size 63 MB, throughput 0.534722
Reading from 21822: heap size 154 MB, throughput 0.835121
Reading from 21820: heap size 64 MB, throughput 0.478101
Reading from 21820: heap size 85 MB, throughput 0.542134
Reading from 21820: heap size 91 MB, throughput 0.3967
Reading from 21820: heap size 92 MB, throughput 0.144378
Reading from 21820: heap size 98 MB, throughput 0.77409
Reading from 21822: heap size 208 MB, throughput 0.994069
Reading from 21820: heap size 101 MB, throughput 0.463768
Reading from 21820: heap size 134 MB, throughput 0.729827
Reading from 21820: heap size 136 MB, throughput 0.737592
Reading from 21820: heap size 139 MB, throughput 0.722007
Reading from 21820: heap size 145 MB, throughput 0.678631
Reading from 21822: heap size 221 MB, throughput 0.967127
Reading from 21820: heap size 149 MB, throughput 0.647417
Reading from 21820: heap size 155 MB, throughput 0.338347
Reading from 21820: heap size 189 MB, throughput 0.582314
Reading from 21822: heap size 237 MB, throughput 0.973659
Reading from 21820: heap size 196 MB, throughput 0.585592
Reading from 21820: heap size 198 MB, throughput 0.58858
Reading from 21820: heap size 204 MB, throughput 0.625749
Reading from 21820: heap size 210 MB, throughput 0.334288
Reading from 21820: heap size 249 MB, throughput 0.539512
Reading from 21822: heap size 241 MB, throughput 0.967238
Reading from 21820: heap size 257 MB, throughput 0.614132
Reading from 21820: heap size 258 MB, throughput 0.661438
Reading from 21820: heap size 260 MB, throughput 0.612118
Reading from 21820: heap size 265 MB, throughput 0.624681
Reading from 21820: heap size 271 MB, throughput 0.591084
Reading from 21822: heap size 260 MB, throughput 0.933228
Reading from 21820: heap size 276 MB, throughput 0.397705
Reading from 21820: heap size 320 MB, throughput 0.535009
Reading from 21820: heap size 325 MB, throughput 0.571446
Reading from 21822: heap size 277 MB, throughput 0.970723
Reading from 21820: heap size 327 MB, throughput 0.546325
Reading from 21820: heap size 330 MB, throughput 0.54361
Reading from 21820: heap size 335 MB, throughput 0.287916
Reading from 21820: heap size 380 MB, throughput 0.528594
Reading from 21820: heap size 376 MB, throughput 0.57789
Reading from 21822: heap size 293 MB, throughput 0.981514
Reading from 21820: heap size 381 MB, throughput 0.539067
Reading from 21820: heap size 383 MB, throughput 0.571637
Reading from 21820: heap size 387 MB, throughput 0.57616
Numeric result:
Recommendation: 2 clients, utility 1.02221:
    h1: 2536 MB (U(h) = 0.826978*h^0.0255508)
    h2: 1223 MB (U(h) = 1.00457*h^0.001)
Recommendation: 2 clients, utility 1.02221:
    h1: 2536 MB (U(h) = 0.826978*h^0.0255508)
    h2: 1223 MB (U(h) = 1.00457*h^0.001)
Reading from 21822: heap size 294 MB, throughput 0.876736
Reading from 21820: heap size 392 MB, throughput 0.295109
Reading from 21820: heap size 444 MB, throughput 0.541025
Reading from 21822: heap size 307 MB, throughput 0.960898
Reading from 21820: heap size 454 MB, throughput 0.573601
Reading from 21820: heap size 455 MB, throughput 0.575029
Reading from 21820: heap size 458 MB, throughput 0.55994
Reading from 21820: heap size 466 MB, throughput 0.552347
Reading from 21820: heap size 474 MB, throughput 0.450486
Reading from 21822: heap size 321 MB, throughput 0.957822
Reading from 21820: heap size 482 MB, throughput 0.295614
Reading from 21820: heap size 546 MB, throughput 0.398151
Reading from 21822: heap size 337 MB, throughput 0.973764
Reading from 21820: heap size 549 MB, throughput 0.422591
Reading from 21820: heap size 549 MB, throughput 0.467419
Reading from 21822: heap size 338 MB, throughput 0.979628
Reading from 21820: heap size 551 MB, throughput 0.238282
Reading from 21820: heap size 613 MB, throughput 0.456708
Reading from 21820: heap size 616 MB, throughput 0.532235
Reading from 21820: heap size 614 MB, throughput 0.605938
Reading from 21822: heap size 357 MB, throughput 0.984555
Reading from 21820: heap size 615 MB, throughput 0.602801
Reading from 21820: heap size 618 MB, throughput 0.538152
Reading from 21820: heap size 626 MB, throughput 0.441129
Reading from 21822: heap size 359 MB, throughput 0.917461
Reading from 21822: heap size 368 MB, throughput 0.976263
Reading from 21820: heap size 634 MB, throughput 0.389728
Reading from 21820: heap size 714 MB, throughput 0.702273
Reading from 21822: heap size 374 MB, throughput 0.97683
Numeric result:
Recommendation: 2 clients, utility 1.02535:
    h1: 2536 MB (U(h) = 0.828442*h^0.0248749)
    h2: 1223 MB (U(h) = 1.01122*h^0.001)
Recommendation: 2 clients, utility 1.02535:
    h1: 2536 MB (U(h) = 0.828442*h^0.0248749)
    h2: 1223 MB (U(h) = 1.01122*h^0.001)
Reading from 21820: heap size 727 MB, throughput 0.800736
Reading from 21822: heap size 395 MB, throughput 0.97914
Reading from 21820: heap size 733 MB, throughput 0.740401
Reading from 21820: heap size 745 MB, throughput 0.685391
Reading from 21820: heap size 752 MB, throughput 0.587451
Reading from 21820: heap size 762 MB, throughput 0.525218
Reading from 21822: heap size 397 MB, throughput 0.976684
Reading from 21822: heap size 420 MB, throughput 0.979823
Reading from 21820: heap size 762 MB, throughput 0.395329
Reading from 21820: heap size 824 MB, throughput 0.58874
Reading from 21820: heap size 831 MB, throughput 0.664954
Reading from 21820: heap size 834 MB, throughput 0.648571
Reading from 21820: heap size 836 MB, throughput 0.627214
Reading from 21822: heap size 421 MB, throughput 0.983728
Reading from 21822: heap size 446 MB, throughput 0.984587
Reading from 21820: heap size 835 MB, throughput 0.353496
Reading from 21820: heap size 921 MB, throughput 0.734026
Reading from 21820: heap size 926 MB, throughput 0.729344
Reading from 21822: heap size 448 MB, throughput 0.985171
Reading from 21820: heap size 927 MB, throughput 0.822199
Reading from 21820: heap size 932 MB, throughput 0.858987
Reading from 21822: heap size 467 MB, throughput 0.985655
Reading from 21820: heap size 934 MB, throughput 0.519326
Reading from 21820: heap size 1026 MB, throughput 0.764002
Reading from 21820: heap size 1029 MB, throughput 0.78475
Reading from 21822: heap size 469 MB, throughput 0.984673
Reading from 21820: heap size 1032 MB, throughput 0.780747
Reading from 21820: heap size 1034 MB, throughput 0.807349
Numeric result:
Recommendation: 2 clients, utility 0.691566:
    h1: 2536 MB (U(h) = 0.819357*h^0.0279037)
    h2: 1223 MB (U(h) = 0.673418*h^0.001)
Recommendation: 2 clients, utility 0.691566:
    h1: 2536 MB (U(h) = 0.819357*h^0.0279037)
    h2: 1223 MB (U(h) = 0.673418*h^0.001)
Reading from 21820: heap size 1029 MB, throughput 0.828194
Reading from 21820: heap size 1034 MB, throughput 0.837299
Reading from 21820: heap size 1026 MB, throughput 0.837499
Reading from 21822: heap size 489 MB, throughput 0.984719
Reading from 21820: heap size 1031 MB, throughput 0.835787
Reading from 21822: heap size 490 MB, throughput 0.982829
Reading from 21820: heap size 1022 MB, throughput 0.940125
Reading from 21820: heap size 1028 MB, throughput 0.895972
Reading from 21820: heap size 1033 MB, throughput 0.847154
Reading from 21822: heap size 509 MB, throughput 0.986414
Reading from 21820: heap size 1037 MB, throughput 0.81796
Reading from 21820: heap size 1048 MB, throughput 0.733609
Reading from 21820: heap size 1050 MB, throughput 0.709032
Reading from 21820: heap size 1042 MB, throughput 0.74171
Reading from 21822: heap size 510 MB, throughput 0.988067
Reading from 21820: heap size 1052 MB, throughput 0.78501
Reading from 21820: heap size 1040 MB, throughput 0.86867
Reading from 21820: heap size 1049 MB, throughput 0.86464
Reading from 21820: heap size 1041 MB, throughput 0.848113
Reading from 21822: heap size 526 MB, throughput 0.988327
Reading from 21820: heap size 1049 MB, throughput 0.816918
Reading from 21820: heap size 1043 MB, throughput 0.770998
Reading from 21820: heap size 1056 MB, throughput 0.713465
Reading from 21820: heap size 1071 MB, throughput 0.67345
Reading from 21822: heap size 528 MB, throughput 0.987396
Reading from 21820: heap size 1081 MB, throughput 0.694607
Numeric result:
Recommendation: 2 clients, utility 0.628477:
    h1: 2066.14 MB (U(h) = 0.815619*h^0.0290904)
    h2: 1692.86 MB (U(h) = 0.516878*h^0.0238411)
Recommendation: 2 clients, utility 0.628477:
    h1: 2065.89 MB (U(h) = 0.815619*h^0.0290904)
    h2: 1693.11 MB (U(h) = 0.516878*h^0.0238411)
Reading from 21820: heap size 1199 MB, throughput 0.597078
Reading from 21822: heap size 547 MB, throughput 0.987974
Reading from 21820: heap size 1205 MB, throughput 0.669641
Reading from 21820: heap size 1217 MB, throughput 0.729443
Reading from 21820: heap size 1220 MB, throughput 0.760551
Reading from 21822: heap size 547 MB, throughput 0.990201
Reading from 21820: heap size 1224 MB, throughput 0.906503
Reading from 21822: heap size 564 MB, throughput 0.990035
Reading from 21820: heap size 1229 MB, throughput 0.964231
Reading from 21822: heap size 566 MB, throughput 0.989577
Reading from 21822: heap size 583 MB, throughput 0.991189
Reading from 21820: heap size 1221 MB, throughput 0.972538
Reading from 21822: heap size 584 MB, throughput 0.988751
Numeric result:
Recommendation: 2 clients, utility 0.75722:
    h1: 702.113 MB (U(h) = 0.813348*h^0.0297878)
    h2: 3056.89 MB (U(h) = 0.27052*h^0.129676)
Recommendation: 2 clients, utility 0.75722:
    h1: 702.18 MB (U(h) = 0.813348*h^0.0297878)
    h2: 3056.82 MB (U(h) = 0.27052*h^0.129676)
Reading from 21822: heap size 600 MB, throughput 0.991618
Reading from 21820: heap size 1231 MB, throughput 0.97163
Reading from 21822: heap size 602 MB, throughput 0.98832
Reading from 21822: heap size 616 MB, throughput 0.987685
Reading from 21820: heap size 1222 MB, throughput 0.97275
Reading from 21822: heap size 618 MB, throughput 0.991315
Reading from 21822: heap size 632 MB, throughput 0.992507
Reading from 21820: heap size 1229 MB, throughput 0.968956
Numeric result:
Recommendation: 2 clients, utility 0.807811:
    h1: 586.54 MB (U(h) = 0.812026*h^0.0301843)
    h2: 3172.46 MB (U(h) = 0.220047*h^0.163267)
Recommendation: 2 clients, utility 0.807811:
    h1: 586.517 MB (U(h) = 0.812026*h^0.0301843)
    h2: 3172.48 MB (U(h) = 0.220047*h^0.163267)
Reading from 21822: heap size 635 MB, throughput 0.990495
Reading from 21822: heap size 601 MB, throughput 0.991163
Reading from 21820: heap size 1224 MB, throughput 0.968895
Reading from 21822: heap size 567 MB, throughput 0.99225
Reading from 21822: heap size 584 MB, throughput 0.989891
Reading from 21820: heap size 1231 MB, throughput 0.97351
Reading from 21822: heap size 608 MB, throughput 0.992099
Reading from 21822: heap size 567 MB, throughput 0.989179
Numeric result:
Recommendation: 2 clients, utility 0.809356:
    h1: 590.084 MB (U(h) = 0.81103*h^0.0304868)
    h2: 3168.92 MB (U(h) = 0.219511*h^0.16372)
Recommendation: 2 clients, utility 0.809356:
    h1: 590.092 MB (U(h) = 0.81103*h^0.0304868)
    h2: 3168.91 MB (U(h) = 0.219511*h^0.16372)
Reading from 21820: heap size 1227 MB, throughput 0.974079
Reading from 21822: heap size 600 MB, throughput 0.989102
Reading from 21822: heap size 570 MB, throughput 0.991986
Reading from 21820: heap size 1232 MB, throughput 0.971265
Reading from 21822: heap size 594 MB, throughput 0.990377
Reading from 21822: heap size 561 MB, throughput 0.990565
Reading from 21820: heap size 1225 MB, throughput 0.97171
Reading from 21822: heap size 584 MB, throughput 0.992293
Reading from 21822: heap size 587 MB, throughput 0.98853
Numeric result:
Recommendation: 2 clients, utility 0.887046:
    h1: 472.564 MB (U(h) = 0.810231*h^0.0307342)
    h2: 3286.44 MB (U(h) = 0.16046*h^0.213771)
Recommendation: 2 clients, utility 0.887046:
    h1: 472.504 MB (U(h) = 0.810231*h^0.0307342)
    h2: 3286.5 MB (U(h) = 0.16046*h^0.213771)
Reading from 21820: heap size 1229 MB, throughput 0.970832
Reading from 21822: heap size 586 MB, throughput 0.990987
Reading from 21822: heap size 540 MB, throughput 0.988182
Reading from 21822: heap size 531 MB, throughput 0.98863
Reading from 21820: heap size 1233 MB, throughput 0.968642
Reading from 21822: heap size 499 MB, throughput 0.990644
Reading from 21822: heap size 476 MB, throughput 0.990402
Reading from 21820: heap size 1234 MB, throughput 0.969888
Reading from 21822: heap size 463 MB, throughput 0.99204
Numeric result:
Recommendation: 2 clients, utility 0.930128:
    h1: 432.799 MB (U(h) = 0.80911*h^0.0311195)
    h2: 3326.2 MB (U(h) = 0.136814*h^0.239176)
Recommendation: 2 clients, utility 0.930128:
    h1: 432.779 MB (U(h) = 0.80911*h^0.0311195)
    h2: 3326.22 MB (U(h) = 0.136814*h^0.239176)
Reading from 21822: heap size 470 MB, throughput 0.990616
Reading from 21822: heap size 457 MB, throughput 0.991934
Reading from 21820: heap size 1239 MB, throughput 0.971218
Reading from 21822: heap size 435 MB, throughput 0.988974
Reading from 21822: heap size 423 MB, throughput 0.989544
Reading from 21822: heap size 436 MB, throughput 0.989435
Reading from 21820: heap size 1242 MB, throughput 0.972858
Reading from 21822: heap size 418 MB, throughput 0.990951
Reading from 21822: heap size 428 MB, throughput 0.992312
Reading from 21822: heap size 442 MB, throughput 0.986142
Numeric result:
Recommendation: 2 clients, utility 0.896498:
    h1: 495.5 MB (U(h) = 0.807971*h^0.0316046)
    h2: 3263.5 MB (U(h) = 0.16927*h^0.208157)
Recommendation: 2 clients, utility 0.896498:
    h1: 495.5 MB (U(h) = 0.807971*h^0.0316046)
    h2: 3263.5 MB (U(h) = 0.16927*h^0.208157)
Reading from 21822: heap size 425 MB, throughput 0.990339
Reading from 21820: heap size 1246 MB, throughput 0.972331
Reading from 21822: heap size 430 MB, throughput 0.987056
Reading from 21822: heap size 440 MB, throughput 0.989792
Reading from 21820: heap size 1251 MB, throughput 0.970441
Reading from 21822: heap size 453 MB, throughput 0.987869
Reading from 21822: heap size 455 MB, throughput 0.988537
Reading from 21822: heap size 468 MB, throughput 0.988829
Reading from 21822: heap size 468 MB, throughput 0.982236
Numeric result:
Recommendation: 2 clients, utility 0.949509:
    h1: 436.7 MB (U(h) = 0.807405*h^0.0318407)
    h2: 3322.3 MB (U(h) = 0.135912*h^0.242255)
Recommendation: 2 clients, utility 0.949509:
    h1: 436.669 MB (U(h) = 0.807405*h^0.0318407)
    h2: 3322.33 MB (U(h) = 0.135912*h^0.242255)
Reading from 21820: heap size 1257 MB, throughput 0.943673
Reading from 21822: heap size 481 MB, throughput 0.986431
Reading from 21822: heap size 452 MB, throughput 0.989437
Reading from 21822: heap size 433 MB, throughput 0.655533
Reading from 21822: heap size 458 MB, throughput 0.989419
Reading from 21820: heap size 1331 MB, throughput 0.981597
Reading from 21822: heap size 434 MB, throughput 0.99283
Reading from 21822: heap size 451 MB, throughput 0.989042
Reading from 21822: heap size 433 MB, throughput 0.958253
Reading from 21820: heap size 1327 MB, throughput 0.985932
Reading from 21822: heap size 437 MB, throughput 0.985443
Numeric result:
Recommendation: 2 clients, utility 1.05892:
    h1: 340.247 MB (U(h) = 0.808575*h^0.0312957)
    h2: 3418.75 MB (U(h) = 0.0844663*h^0.31445)
Recommendation: 2 clients, utility 1.05892:
    h1: 340.252 MB (U(h) = 0.808575*h^0.0312957)
    h2: 3418.75 MB (U(h) = 0.0844663*h^0.31445)
Reading from 21822: heap size 446 MB, throughput 0.989884
Reading from 21820: heap size 1338 MB, throughput 0.986818
Reading from 21822: heap size 414 MB, throughput 0.980228
Reading from 21822: heap size 406 MB, throughput 0.990258
Reading from 21822: heap size 381 MB, throughput 0.988165
Reading from 21822: heap size 368 MB, throughput 0.990492
Reading from 21820: heap size 1346 MB, throughput 0.988607
Reading from 21822: heap size 354 MB, throughput 0.270516
Reading from 21822: heap size 342 MB, throughput 0.987692
Reading from 21822: heap size 336 MB, throughput 0.989332
Reading from 21822: heap size 340 MB, throughput 0.728056
Reading from 21820: heap size 1347 MB, throughput 0.98805
Numeric result:
Recommendation: 2 clients, utility 1.1245:
    h1: 285.489 MB (U(h) = 0.802453*h^0.0298482)
    h2: 3473.51 MB (U(h) = 0.0612664*h^0.363204)
Recommendation: 2 clients, utility 1.1245:
    h1: 285.457 MB (U(h) = 0.802453*h^0.0298482)
    h2: 3473.54 MB (U(h) = 0.0612664*h^0.363204)
Reading from 21822: heap size 329 MB, throughput 0.984765
Reading from 21822: heap size 324 MB, throughput 0.689676
Reading from 21822: heap size 306 MB, throughput 0.990123
Reading from 21822: heap size 296 MB, throughput 0.985336
Reading from 21822: heap size 290 MB, throughput 0.984981
Reading from 21820: heap size 1340 MB, throughput 0.987952
Reading from 21822: heap size 281 MB, throughput 0.988407
Reading from 21822: heap size 293 MB, throughput 0.99016
Reading from 21822: heap size 274 MB, throughput 0.969259
Reading from 21822: heap size 287 MB, throughput 0.974753
Reading from 21822: heap size 269 MB, throughput 0.981822
Reading from 21820: heap size 1232 MB, throughput 0.986762
Reading from 21822: heap size 283 MB, throughput 0.98837
Reading from 21822: heap size 287 MB, throughput 0.959025
Reading from 21822: heap size 280 MB, throughput 0.972891
Numeric result:
Recommendation: 2 clients, utility 1.1518:
    h1: 405.034 MB (U(h) = 0.728308*h^0.0460245)
    h2: 3353.97 MB (U(h) = 0.0543684*h^0.381133)
Recommendation: 2 clients, utility 1.1518:
    h1: 405.017 MB (U(h) = 0.728308*h^0.0460245)
    h2: 3353.98 MB (U(h) = 0.0543684*h^0.381133)
Reading from 21822: heap size 282 MB, throughput 0.949442
Reading from 21822: heap size 287 MB, throughput 0.0387642
Reading from 21822: heap size 298 MB, throughput 0.974968
Reading from 21820: heap size 1326 MB, throughput 0.983661
Reading from 21822: heap size 298 MB, throughput 0.973929
Reading from 21822: heap size 313 MB, throughput 0.9784
Reading from 21822: heap size 316 MB, throughput 0.975817
Reading from 21822: heap size 330 MB, throughput 0.978295
Reading from 21822: heap size 331 MB, throughput 0.969627
Reading from 21820: heap size 1253 MB, throughput 0.981603
Reading from 21822: heap size 347 MB, throughput 0.97457
Reading from 21822: heap size 346 MB, throughput 0.987012
Reading from 21822: heap size 361 MB, throughput 0.98391
Numeric result:
Recommendation: 2 clients, utility 1.24127:
    h1: 813.751 MB (U(h) = 0.474241*h^0.116228)
    h2: 2945.25 MB (U(h) = 0.041711*h^0.420665)
Recommendation: 2 clients, utility 1.24127:
    h1: 813.756 MB (U(h) = 0.474241*h^0.116228)
    h2: 2945.24 MB (U(h) = 0.041711*h^0.420665)
Reading from 21820: heap size 1323 MB, throughput 0.978468
Reading from 21822: heap size 362 MB, throughput 0.989383
Reading from 21822: heap size 378 MB, throughput 0.988836
Reading from 21822: heap size 380 MB, throughput 0.988205
Reading from 21822: heap size 393 MB, throughput 0.984754
Reading from 21820: heap size 1327 MB, throughput 0.977095
Reading from 21822: heap size 394 MB, throughput 0.976461
Reading from 21822: heap size 414 MB, throughput 0.984615
Reading from 21822: heap size 413 MB, throughput 0.980284
Reading from 21822: heap size 431 MB, throughput 0.986674
Reading from 21820: heap size 1328 MB, throughput 0.977355
Reading from 21822: heap size 433 MB, throughput 0.982059
Numeric result:
Recommendation: 2 clients, utility 1.296:
    h1: 894.46 MB (U(h) = 0.416548*h^0.137814)
    h2: 2864.54 MB (U(h) = 0.0363406*h^0.441353)
Recommendation: 2 clients, utility 1.296:
    h1: 894.462 MB (U(h) = 0.416548*h^0.137814)
    h2: 2864.54 MB (U(h) = 0.0363406*h^0.441353)
Reading from 21822: heap size 451 MB, throughput 0.986535
Reading from 21822: heap size 453 MB, throughput 0.98566
Reading from 21820: heap size 1330 MB, throughput 0.977074
Reading from 21822: heap size 473 MB, throughput 0.988926
Reading from 21822: heap size 474 MB, throughput 0.987274
Reading from 21822: heap size 492 MB, throughput 0.989769
Reading from 21820: heap size 1333 MB, throughput 0.974188
Reading from 21822: heap size 493 MB, throughput 0.986449
Reading from 21822: heap size 513 MB, throughput 0.990321
Numeric result:
Recommendation: 2 clients, utility 1.35576:
    h1: 879.902 MB (U(h) = 0.397153*h^0.145491)
    h2: 2879.1 MB (U(h) = 0.028712*h^0.476048)
Recommendation: 2 clients, utility 1.35576:
    h1: 879.915 MB (U(h) = 0.397153*h^0.145491)
    h2: 2879.08 MB (U(h) = 0.028712*h^0.476048)
Reading from 21822: heap size 513 MB, throughput 0.987078
Reading from 21820: heap size 1339 MB, throughput 0.970987
Reading from 21822: heap size 533 MB, throughput 0.990715
Reading from 21822: heap size 534 MB, throughput 0.988126
Reading from 21820: heap size 1342 MB, throughput 0.969573
Reading from 21822: heap size 554 MB, throughput 0.991898
Reading from 21822: heap size 554 MB, throughput 0.988792
Reading from 21822: heap size 574 MB, throughput 0.989531
Reading from 21820: heap size 1351 MB, throughput 0.969791
Numeric result:
Recommendation: 2 clients, utility 1.47898:
    h1: 783.539 MB (U(h) = 0.395502*h^0.145957)
    h2: 2975.46 MB (U(h) = 0.0167928*h^0.554269)
Recommendation: 2 clients, utility 1.47898:
    h1: 783.535 MB (U(h) = 0.395502*h^0.145957)
    h2: 2975.46 MB (U(h) = 0.0167928*h^0.554269)
Reading from 21822: heap size 574 MB, throughput 0.991598
Reading from 21822: heap size 590 MB, throughput 0.989675
Reading from 21820: heap size 1359 MB, throughput 0.970631
Reading from 21822: heap size 593 MB, throughput 0.989794
Reading from 21822: heap size 614 MB, throughput 0.99242
Reading from 21822: heap size 615 MB, throughput 0.983838
Reading from 21820: heap size 1364 MB, throughput 0.971784
Reading from 21822: heap size 634 MB, throughput 0.987793
Numeric result:
Recommendation: 2 clients, utility 1.52798:
    h1: 723.974 MB (U(h) = 0.409269*h^0.140068)
    h2: 3035.03 MB (U(h) = 0.013393*h^0.587185)
Recommendation: 2 clients, utility 1.52798:
    h1: 723.978 MB (U(h) = 0.409269*h^0.140068)
    h2: 3035.02 MB (U(h) = 0.013393*h^0.587185)
Reading from 21822: heap size 635 MB, throughput 0.989907
Reading from 21820: heap size 1371 MB, throughput 0.971865
Reading from 21822: heap size 657 MB, throughput 0.988592
Reading from 21822: heap size 659 MB, throughput 0.987325
Reading from 21820: heap size 1373 MB, throughput 0.970736
Reading from 21822: heap size 686 MB, throughput 0.989928
Reading from 21822: heap size 687 MB, throughput 0.992256
Numeric result:
Recommendation: 2 clients, utility 1.58822:
    h1: 655.67 MB (U(h) = 0.428285*h^0.132395)
    h2: 3103.33 MB (U(h) = 0.010192*h^0.626607)
Recommendation: 2 clients, utility 1.58822:
    h1: 655.694 MB (U(h) = 0.428285*h^0.132395)
    h2: 3103.31 MB (U(h) = 0.010192*h^0.626607)
Reading from 21822: heap size 707 MB, throughput 0.990167
Reading from 21822: heap size 676 MB, throughput 0.986888
Reading from 21822: heap size 648 MB, throughput 0.993189
Reading from 21822: heap size 696 MB, throughput 0.992931
Reading from 21822: heap size 654 MB, throughput 0.989518
Numeric result:
Recommendation: 2 clients, utility 1.58294:
    h1: 629.498 MB (U(h) = 0.444757*h^0.126042)
    h2: 3129.5 MB (U(h) = 0.010192*h^0.626607)
Recommendation: 2 clients, utility 1.58294:
    h1: 629.501 MB (U(h) = 0.444757*h^0.126042)
    h2: 3129.5 MB (U(h) = 0.010192*h^0.626607)
Reading from 21822: heap size 683 MB, throughput 0.988619
Reading from 21822: heap size 651 MB, throughput 0.989661
Reading from 21820: heap size 1380 MB, throughput 0.986327
Reading from 21822: heap size 618 MB, throughput 0.987515
Reading from 21822: heap size 654 MB, throughput 0.986247
Reading from 21822: heap size 634 MB, throughput 0.988205
Reading from 21822: heap size 603 MB, throughput 0.986121
Numeric result:
Recommendation: 2 clients, utility 1.62111:
    h1: 604.607 MB (U(h) = 0.449256*h^0.124347)
    h2: 3154.39 MB (U(h) = 0.00874161*h^0.648732)
Recommendation: 2 clients, utility 1.62111:
    h1: 604.62 MB (U(h) = 0.449256*h^0.124347)
    h2: 3154.38 MB (U(h) = 0.00874161*h^0.648732)
Reading from 21822: heap size 642 MB, throughput 0.984446
Reading from 21822: heap size 613 MB, throughput 0.987905
Reading from 21822: heap size 580 MB, throughput 0.984004
Reading from 21822: heap size 609 MB, throughput 0.985312
Reading from 21822: heap size 581 MB, throughput 0.99188
Reading from 21822: heap size 581 MB, throughput 0.99188
Reading from 21822: heap size 581 MB, throughput 0.975141
Reading from 21822: heap size 600 MB, throughput 0.976197
Reading from 21820: heap size 1381 MB, throughput 0.989563
Numeric result:
Recommendation: 2 clients, utility 1.66284:
    h1: 574.934 MB (U(h) = 0.456785*h^0.12151)
    h2: 3184.07 MB (U(h) = 0.00739069*h^0.672893)
Recommendation: 2 clients, utility 1.66284:
    h1: 574.966 MB (U(h) = 0.456785*h^0.12151)
    h2: 3184.03 MB (U(h) = 0.00739069*h^0.672893)
Reading from 21822: heap size 602 MB, throughput 0.982895
Reading from 21822: heap size 557 MB, throughput 0.987189
Reading from 21822: heap size 614 MB, throughput 0.985773
Reading from 21822: heap size 577 MB, throughput 0.989691
Reading from 21822: heap size 544 MB, throughput 0.985462
Reading from 21820: heap size 1398 MB, throughput 0.986468
Reading from 21822: heap size 572 MB, throughput 0.987814
Reading from 21820: heap size 1413 MB, throughput 0.975697
Reading from 21822: heap size 573 MB, throughput 0.988866
Reading from 21820: heap size 1426 MB, throughput 0.972093
Reading from 21820: heap size 1541 MB, throughput 0.958288
Reading from 21820: heap size 1589 MB, throughput 0.96317
Numeric result:
Recommendation: 2 clients, utility 1.78049:
    h1: 526.297 MB (U(h) = 0.459237*h^0.120566)
    h2: 3232.7 MB (U(h) = 0.00458491*h^0.740571)
Recommendation: 2 clients, utility 1.78049:
    h1: 526.289 MB (U(h) = 0.459237*h^0.120566)
    h2: 3232.71 MB (U(h) = 0.00458491*h^0.740571)
Reading from 21820: heap size 1599 MB, throughput 0.969231
Reading from 21820: heap size 1617 MB, throughput 0.976267
Reading from 21820: heap size 1619 MB, throughput 0.981224
Reading from 21822: heap size 600 MB, throughput 0.987353
Reading from 21822: heap size 563 MB, throughput 0.991843
Reading from 21820: heap size 1613 MB, throughput 0.988702
Reading from 21822: heap size 530 MB, throughput 0.988618
Reading from 21822: heap size 522 MB, throughput 0.991848
Reading from 21822: heap size 546 MB, throughput 0.988926
Reading from 21820: heap size 1620 MB, throughput 0.992269
Reading from 21822: heap size 511 MB, throughput 0.989936
Reading from 21822: heap size 525 MB, throughput 0.989017
Numeric result:
Recommendation: 2 clients, utility 1.8844:
    h1: 496.031 MB (U(h) = 0.45379*h^0.122329)
    h2: 3262.97 MB (U(h) = 0.00289182*h^0.804701)
Recommendation: 2 clients, utility 1.8844:
    h1: 496.03 MB (U(h) = 0.45379*h^0.122329)
    h2: 3262.97 MB (U(h) = 0.00289182*h^0.804701)
Reading from 21822: heap size 530 MB, throughput 0.98922
Reading from 21820: heap size 1594 MB, throughput 0.99127
Reading from 21822: heap size 490 MB, throughput 0.991098
Client 21822 died
Clients: 1
Reading from 21820: heap size 1334 MB, throughput 0.991884
Recommendation: one client; give it all the memory
Reading from 21820: heap size 1567 MB, throughput 0.991362
Reading from 21820: heap size 1357 MB, throughput 0.990653
Recommendation: one client; give it all the memory
Reading from 21820: heap size 1537 MB, throughput 0.990107
Reading from 21820: heap size 1380 MB, throughput 0.989222
Reading from 21820: heap size 1516 MB, throughput 0.988336
Recommendation: one client; give it all the memory
Reading from 21820: heap size 1402 MB, throughput 0.987275
Reading from 21820: heap size 1516 MB, throughput 0.986069
Recommendation: one client; give it all the memory
Reading from 21820: heap size 1520 MB, throughput 0.985268
Reading from 21820: heap size 1522 MB, throughput 0.984649
Recommendation: one client; give it all the memory
Reading from 21820: heap size 1524 MB, throughput 0.983894
Reading from 21820: heap size 1527 MB, throughput 0.983184
Recommendation: one client; give it all the memory
Reading from 21820: heap size 1532 MB, throughput 0.982175
Reading from 21820: heap size 1539 MB, throughput 0.981067
Recommendation: one client; give it all the memory
Reading from 21820: heap size 1545 MB, throughput 0.980708
Reading from 21820: heap size 1553 MB, throughput 0.980192
Recommendation: one client; give it all the memory
Reading from 21820: heap size 1556 MB, throughput 0.97965
Reading from 21820: heap size 1563 MB, throughput 0.979815
Recommendation: one client; give it all the memory
Reading from 21820: heap size 1564 MB, throughput 0.980329
Reading from 21820: heap size 1570 MB, throughput 0.979846
Recommendation: one client; give it all the memory
Reading from 21820: heap size 1570 MB, throughput 0.979516
Recommendation: one client; give it all the memory
Reading from 21820: heap size 1575 MB, throughput 0.979533
Reading from 21820: heap size 1575 MB, throughput 0.979448
Recommendation: one client; give it all the memory
Reading from 21820: heap size 1577 MB, throughput 0.979743
Reading from 21820: heap size 1579 MB, throughput 0.976891
Recommendation: one client; give it all the memory
Reading from 21820: heap size 1635 MB, throughput 0.987868
Reading from 21820: heap size 1637 MB, throughput 0.991436
Recommendation: one client; give it all the memory
Reading from 21820: heap size 1650 MB, throughput 0.992488
Recommendation: one client; give it all the memory
Reading from 21820: heap size 1655 MB, throughput 0.992256
Reading from 21820: heap size 1653 MB, throughput 0.991755
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 21820: heap size 1483 MB, throughput 0.990923
Recommendation: one client; give it all the memory
Reading from 21820: heap size 1637 MB, throughput 0.990004
Recommendation: one client; give it all the memory
Reading from 21820: heap size 1655 MB, throughput 0.98723
Reading from 21820: heap size 1685 MB, throughput 0.978518
Reading from 21820: heap size 1685 MB, throughput 0.958071
Reading from 21820: heap size 1710 MB, throughput 0.934367
Reading from 21820: heap size 1731 MB, throughput 0.897499
Reading from 21820: heap size 1771 MB, throughput 0.873974
Client 21820 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
