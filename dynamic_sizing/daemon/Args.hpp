#ifndef ARGS_HPP
#define ARGS_HPP

#include <string>
#include <vector>

class Args
{
public:
    Args(int argc, char** argv);

    unsigned long long totalMemMB() const;
    unsigned long long timerIntervalSeconds() const;
    bool timerRunsAtStart() const;
    const std::string& utilityModel() const;
    const std::vector<std::string>& readingCacheNames() const;
    bool forceFunctionValidity() const;
    bool alwaysNumericOptimisation() const;
    bool makeRecommendations() const;
    bool equalRecommendations() const;
    bool randomRecommendations() const;

    const std::string& readingReceiverName() const;
    const std::string& commandReceiverName() const;

    const std::string& dumpFileName() const;
    const std::string& logFileName() const;
    bool logReadings() const;
    bool logFunctions() const;

private:
    unsigned long long m_TotalMemMB;
    unsigned long long m_TimerIntervalSeconds;
    bool m_TimerRunsAtStart;
    std::string m_UtilityModel;
    std::vector<std::string> m_ReadingCacheNames;
    bool m_ForceFunctionValidity;
    bool m_AlwaysNumericOptimisation;
    bool m_MakeRecommendations;
    bool m_EqualRecommendations;
    bool m_RandomRecommendations;

    std::string m_ReadingReceiverName;
    std::string m_CommandReceiverName;

    std::string m_DumpFileName;
    std::string m_LogFileName;
    bool m_LogReadings;
    bool m_LogFunctions;
};

#endif
