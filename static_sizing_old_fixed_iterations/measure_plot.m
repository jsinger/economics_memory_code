args = argv();

plotTitle = args{1};
heap = load(args{2});
times = load(args{3});
stddevs = load(args{4});
throughputs = load(args{5});
runtimePlot = args{6};
throughputPlot = args{7};
c_file = args{8};
q_file = args{9};


# Runtime graph
errorbar(heap, times, stddevs);
xlabel("Heap size (MB)");
ylabel("Runtime (s)");
title(plotTitle);

print(runtimePlot);

close all;


# Throughput graph
P = polyfit(log(heap), log(throughputs), 1);
q = P(1);
c = exp(P(2));

fitx = linspace(min(heap), max(heap), 100);
fity = c * fitx .^ q;

plot(heap, throughputs, '-', fitx, fity);
xlabel("Heap size (MB)");
ylabel("Throughput (s^{-1})");
legend("Original data", sprintf("Fitted: y = %gx^{%g}", c, q));
title(plotTitle);

print(throughputPlot);

# Save c
[fd, msg] = fopen(c_file, "w");
fprintf(fd, "%f\n", c);
fclose(fd);

# Save q
[fd, msg] = fopen(q_file, "w");
fprintf(fd, "%f\n", q);
fclose(fd);
