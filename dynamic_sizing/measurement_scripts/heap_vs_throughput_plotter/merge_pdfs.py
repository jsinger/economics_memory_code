#!/usr/bin/env python

import argparse
import glob
import shutil
import subprocess
import sys

def merge_pdfs(files, outfile):
    if files:
        if len(files) > 1:
            return subprocess.call(["pdfunite"] + sorted(files) + [outfile]) == 0
        elif len(files) == 1:
            shutil.copy(files[0], outfile)
            return True
    else:
        return False

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Merge pdf files from glob, safely (i.e. doesn't crash if only one file matches, unlike pdfunite (which it wraps))")
    parser.add_argument("in_file_glob", help="Glob to find input files")
    parser.add_argument("out_file", help="Save to this file")
    args = parser.parse_args()

    ret = merge_pdfs(glob.glob(args.in_file_glob), args.out_file)
    if ret:
        sys.exit(0)
    else:
        sys.exit(1)
