module EconoGCLog where

import Text.Regex.Posix


data GCType = MAJOR | MINOR deriving (Eq, Show)

gcTypeFromStr :: String -> GCType
gcTypeFromStr "major" = MAJOR
gcTypeFromStr _       = MINOR



data GCPeriod = GCPeriod { gcSource :: String,
                           gcType :: GCType,
                           heapSizeBytes :: Integer,
                           heapSizeMB :: Double,
                           throughput :: Double,
                           gcTimeTicks :: Integer,
                           gcTimeS :: Double,
                           interGCTimeTicks :: Integer,
                           interGCTimeS :: Double } deriving (Eq, Show)

gcFromStr :: String -> Maybe GCPeriod
gcFromStr s =
  case m of
    []   -> Nothing
    [xs] -> Just (GCPeriod {
                     gcSource = (xs !! 1),
                     gcType = (gcTypeFromStr (xs !! 2)),
                     heapSizeBytes = (read (xs !! 3)),
                     heapSizeMB = (read (xs !! 4)),
                     throughput = (read (xs !! 5)),
                     gcTimeTicks = (read (xs !! 6)),
                     gcTimeS = (read (xs !! 7)),
                     interGCTimeTicks = (read (xs !! 8)),
                     interGCTimeS = (read (xs !! 9)) }
                 )
  where m = s =~ "^#END (.*) \\((major|minor)\\): heapSize ([0-9]+) \\(([0-9]+\\.[0-9]+) MB\\), throughput ([0-9]+\\.[0-9]+), gcTime ([0-9]+) \\(([0-9]+\\.[0-9]+) s\\), interGCTime ([0-9]+) \\(([0-9]+\\.[0-9]+) s\\)$" ::[[String]]

parseGCs :: [String] -> [GCPeriod]
parseGCs xs = foldr f [] (map gcFromStr xs)
  where f :: Maybe GCPeriod -> [GCPeriod] -> [GCPeriod]
        f (Just y) ys = y:ys
        f Nothing ys = ys

majorOnly :: [GCPeriod] -> [GCPeriod]
majorOnly gs = filter (\g -> gcType g == MAJOR) gs

minorOnly :: [GCPeriod] -> [GCPeriod]
minorOnly gs = filter (\g -> gcType g == MINOR) gs
