economemd
    total memory: 5012 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub2_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 12768: heap size 9 MB, throughput 0.988176
Clients: 1
Client 12768 has a minimum heap size of 30 MB
Reading from 12766: heap size 9 MB, throughput 0.986957
Clients: 2
Client 12766 has a minimum heap size of 1223 MB
Reading from 12768: heap size 9 MB, throughput 0.985388
Reading from 12766: heap size 9 MB, throughput 0.983502
Reading from 12768: heap size 11 MB, throughput 0.984006
Reading from 12768: heap size 11 MB, throughput 0.97462
Reading from 12768: heap size 15 MB, throughput 0.955148
Reading from 12768: heap size 15 MB, throughput 0.92306
Reading from 12768: heap size 24 MB, throughput 0.87964
Reading from 12768: heap size 24 MB, throughput 0.849054
Reading from 12766: heap size 11 MB, throughput 0.979128
Reading from 12768: heap size 40 MB, throughput 0.89624
Reading from 12768: heap size 40 MB, throughput 0.901338
Reading from 12768: heap size 60 MB, throughput 0.929826
Reading from 12768: heap size 60 MB, throughput 0.937174
Reading from 12768: heap size 91 MB, throughput 0.957896
Reading from 12768: heap size 91 MB, throughput 0.959287
Reading from 12768: heap size 140 MB, throughput 0.964465
Reading from 12766: heap size 11 MB, throughput 0.983962
Reading from 12766: heap size 15 MB, throughput 0.936799
Reading from 12766: heap size 18 MB, throughput 0.917746
Reading from 12766: heap size 24 MB, throughput 0.886636
Reading from 12766: heap size 28 MB, throughput 0.778686
Reading from 12766: heap size 30 MB, throughput 0.709074
Reading from 12766: heap size 39 MB, throughput 0.327304
Reading from 12768: heap size 140 MB, throughput 0.960567
Reading from 12766: heap size 45 MB, throughput 0.868375
Reading from 12766: heap size 46 MB, throughput 0.537824
Reading from 12766: heap size 65 MB, throughput 0.783636
Reading from 12766: heap size 66 MB, throughput 0.429634
Reading from 12768: heap size 224 MB, throughput 0.905517
Reading from 12766: heap size 91 MB, throughput 0.451995
Reading from 12766: heap size 92 MB, throughput 0.746734
Reading from 12766: heap size 96 MB, throughput 0.734714
Reading from 12766: heap size 100 MB, throughput 0.708419
Reading from 12766: heap size 103 MB, throughput 0.440866
Reading from 12768: heap size 240 MB, throughput 0.980723
Reading from 12766: heap size 132 MB, throughput 0.68839
Reading from 12766: heap size 139 MB, throughput 0.658424
Reading from 12766: heap size 141 MB, throughput 0.699591
Reading from 12766: heap size 146 MB, throughput 0.290471
Reading from 12768: heap size 278 MB, throughput 0.986692
Reading from 12766: heap size 183 MB, throughput 0.633418
Reading from 12766: heap size 191 MB, throughput 0.658178
Reading from 12766: heap size 193 MB, throughput 0.660991
Reading from 12766: heap size 197 MB, throughput 0.654048
Reading from 12766: heap size 204 MB, throughput 0.661221
Reading from 12766: heap size 208 MB, throughput 0.606262
Reading from 12766: heap size 219 MB, throughput 0.568611
Reading from 12768: heap size 290 MB, throughput 0.950654
Reading from 12766: heap size 228 MB, throughput 0.389715
Reading from 12766: heap size 271 MB, throughput 0.527864
Reading from 12766: heap size 283 MB, throughput 0.611335
Reading from 12766: heap size 284 MB, throughput 0.622116
Reading from 12768: heap size 325 MB, throughput 0.981382
Reading from 12766: heap size 287 MB, throughput 0.288028
Reading from 12766: heap size 332 MB, throughput 0.536574
Reading from 12766: heap size 334 MB, throughput 0.642278
Reading from 12766: heap size 337 MB, throughput 0.618737
Reading from 12766: heap size 340 MB, throughput 0.587703
Reading from 12768: heap size 330 MB, throughput 0.980746
Reading from 12766: heap size 345 MB, throughput 0.54583
Reading from 12766: heap size 352 MB, throughput 0.402456
Reading from 12766: heap size 402 MB, throughput 0.495493
Reading from 12766: heap size 408 MB, throughput 0.480009
Reading from 12766: heap size 410 MB, throughput 0.476031
Reading from 12768: heap size 369 MB, throughput 0.983142
Reading from 12766: heap size 415 MB, throughput 0.525383
Numeric result:
Recommendation: 2 clients, utility 1.21979:
    h1: 3789 MB (U(h) = 0.916633*h^0.00844112)
    h2: 1223 MB (U(h) = 1.23252*h^0.001)
Recommendation: 2 clients, utility 1.21979:
    h1: 3789 MB (U(h) = 0.916633*h^0.00844112)
    h2: 1223 MB (U(h) = 1.23252*h^0.001)
Reading from 12766: heap size 418 MB, throughput 0.261104
Reading from 12768: heap size 371 MB, throughput 0.966886
Reading from 12766: heap size 475 MB, throughput 0.464483
Reading from 12766: heap size 476 MB, throughput 0.491168
Reading from 12766: heap size 478 MB, throughput 0.520259
Reading from 12766: heap size 479 MB, throughput 0.317073
Reading from 12766: heap size 537 MB, throughput 0.526215
Reading from 12768: heap size 419 MB, throughput 0.990578
Reading from 12766: heap size 539 MB, throughput 0.530021
Reading from 12766: heap size 542 MB, throughput 0.561659
Reading from 12766: heap size 547 MB, throughput 0.493922
Reading from 12766: heap size 554 MB, throughput 0.531409
Reading from 12768: heap size 421 MB, throughput 0.988764
Reading from 12766: heap size 563 MB, throughput 0.324737
Reading from 12766: heap size 628 MB, throughput 0.513351
Reading from 12768: heap size 450 MB, throughput 0.98705
Reading from 12766: heap size 638 MB, throughput 0.513921
Reading from 12766: heap size 645 MB, throughput 0.212301
Reading from 12768: heap size 451 MB, throughput 0.977664
Reading from 12766: heap size 708 MB, throughput 0.453003
Reading from 12766: heap size 709 MB, throughput 0.77718
Reading from 12768: heap size 480 MB, throughput 0.986616
Reading from 12766: heap size 713 MB, throughput 0.818643
Reading from 12766: heap size 709 MB, throughput 0.766189
Numeric result:
Recommendation: 2 clients, utility 1.23246:
    h1: 3789 MB (U(h) = 0.905577*h^0.0119712)
    h2: 1223 MB (U(h) = 1.22439*h^0.001)
Recommendation: 2 clients, utility 1.23246:
    h1: 3789 MB (U(h) = 0.905577*h^0.0119712)
    h2: 1223 MB (U(h) = 1.22439*h^0.001)
Reading from 12766: heap size 729 MB, throughput 0.737265
Reading from 12768: heap size 484 MB, throughput 0.980117
Reading from 12766: heap size 735 MB, throughput 0.598547
Reading from 12766: heap size 750 MB, throughput 0.509084
Reading from 12768: heap size 515 MB, throughput 0.986781
Reading from 12766: heap size 764 MB, throughput 0.492348
Reading from 12766: heap size 833 MB, throughput 0.387367
Reading from 12766: heap size 823 MB, throughput 0.682218
Reading from 12766: heap size 829 MB, throughput 0.64063
Reading from 12766: heap size 816 MB, throughput 0.599054
Reading from 12768: heap size 518 MB, throughput 0.988058
Reading from 12766: heap size 823 MB, throughput 0.315251
Reading from 12766: heap size 896 MB, throughput 0.549858
Reading from 12768: heap size 553 MB, throughput 0.989683
Reading from 12766: heap size 899 MB, throughput 0.684337
Reading from 12766: heap size 908 MB, throughput 0.770227
Reading from 12766: heap size 909 MB, throughput 0.779911
Reading from 12768: heap size 555 MB, throughput 0.982006
Reading from 12766: heap size 914 MB, throughput 0.480896
Reading from 12766: heap size 1009 MB, throughput 0.689932
Reading from 12766: heap size 1005 MB, throughput 0.723826
Reading from 12766: heap size 1009 MB, throughput 0.733864
Reading from 12766: heap size 1002 MB, throughput 0.748658
Reading from 12766: heap size 1007 MB, throughput 0.78427
Reading from 12766: heap size 997 MB, throughput 0.815907
Reading from 12768: heap size 587 MB, throughput 0.985652
Reading from 12766: heap size 1003 MB, throughput 0.824692
Reading from 12766: heap size 993 MB, throughput 0.820139
Reading from 12766: heap size 999 MB, throughput 0.826494
Numeric result:
Recommendation: 2 clients, utility 0.847425:
    h1: 3789 MB (U(h) = 0.901261*h^0.0132757)
    h2: 1223 MB (U(h) = 0.836865*h^0.001)
Recommendation: 2 clients, utility 0.847425:
    h1: 3789 MB (U(h) = 0.901261*h^0.0132757)
    h2: 1223 MB (U(h) = 0.836865*h^0.001)
Reading from 12768: heap size 585 MB, throughput 0.987757
Reading from 12766: heap size 989 MB, throughput 0.960465
Reading from 12766: heap size 886 MB, throughput 0.936434
Reading from 12766: heap size 978 MB, throughput 0.899513
Reading from 12766: heap size 986 MB, throughput 0.860704
Reading from 12768: heap size 617 MB, throughput 0.991068
Reading from 12766: heap size 972 MB, throughput 0.843892
Reading from 12766: heap size 979 MB, throughput 0.810831
Reading from 12766: heap size 971 MB, throughput 0.813549
Reading from 12766: heap size 976 MB, throughput 0.820909
Reading from 12766: heap size 971 MB, throughput 0.8599
Reading from 12766: heap size 976 MB, throughput 0.877854
Reading from 12768: heap size 621 MB, throughput 0.98741
Reading from 12766: heap size 974 MB, throughput 0.87139
Reading from 12766: heap size 979 MB, throughput 0.809857
Reading from 12766: heap size 971 MB, throughput 0.755851
Reading from 12766: heap size 989 MB, throughput 0.685744
Reading from 12766: heap size 1002 MB, throughput 0.666068
Reading from 12768: heap size 649 MB, throughput 0.989694
Reading from 12766: heap size 1011 MB, throughput 0.654037
Reading from 12766: heap size 1110 MB, throughput 0.604601
Reading from 12766: heap size 1117 MB, throughput 0.675993
Reading from 12766: heap size 1119 MB, throughput 0.735584
Reading from 12768: heap size 652 MB, throughput 0.991638
Reading from 12766: heap size 1124 MB, throughput 0.752534
Reading from 12766: heap size 1125 MB, throughput 0.75604
Numeric result:
Recommendation: 2 clients, utility 0.693142:
    h1: 3789 MB (U(h) = 0.898483*h^0.0140739)
    h2: 1223 MB (U(h) = 0.682121*h^0.001)
Recommendation: 2 clients, utility 0.693142:
    h1: 3789 MB (U(h) = 0.898483*h^0.0140739)
    h2: 1223 MB (U(h) = 0.682121*h^0.001)
Reading from 12766: heap size 1130 MB, throughput 0.941257
Reading from 12768: heap size 677 MB, throughput 0.989451
Reading from 12768: heap size 681 MB, throughput 0.988557
Reading from 12766: heap size 1135 MB, throughput 0.961284
Reading from 12768: heap size 713 MB, throughput 0.990416
Reading from 12766: heap size 1141 MB, throughput 0.967621
Reading from 12768: heap size 714 MB, throughput 0.992141
Reading from 12768: heap size 735 MB, throughput 0.990931
Reading from 12766: heap size 1153 MB, throughput 0.969108
Numeric result:
Recommendation: 2 clients, utility 0.64579:
    h1: 3789 MB (U(h) = 0.896892*h^0.0145133)
    h2: 1223 MB (U(h) = 0.634348*h^0.001)
Recommendation: 2 clients, utility 0.64579:
    h1: 3789 MB (U(h) = 0.896892*h^0.0145133)
    h2: 1223 MB (U(h) = 0.634348*h^0.001)
Reading from 12768: heap size 739 MB, throughput 0.988972
Reading from 12766: heap size 1155 MB, throughput 0.971138
Reading from 12768: heap size 767 MB, throughput 0.990324
Reading from 12768: heap size 769 MB, throughput 0.986454
Reading from 12766: heap size 1164 MB, throughput 0.912401
Reading from 12768: heap size 800 MB, throughput 0.988319
Numeric result:
Recommendation: 2 clients, utility 0.659384:
    h1: 1595.53 MB (U(h) = 0.896699*h^0.0145656)
    h2: 3416.47 MB (U(h) = 0.51233*h^0.031212)
Recommendation: 2 clients, utility 0.659384:
    h1: 1594.73 MB (U(h) = 0.896699*h^0.0145656)
    h2: 3417.27 MB (U(h) = 0.51233*h^0.031212)
Reading from 12766: heap size 1239 MB, throughput 0.982797
Reading from 12768: heap size 801 MB, throughput 0.989618
Reading from 12766: heap size 1240 MB, throughput 0.986513
Reading from 12768: heap size 829 MB, throughput 0.992234
Reading from 12766: heap size 1250 MB, throughput 0.985689
Reading from 12768: heap size 831 MB, throughput 0.990629
Reading from 12768: heap size 859 MB, throughput 0.990667
Numeric result:
Recommendation: 2 clients, utility 0.758415:
    h1: 665.745 MB (U(h) = 0.896274*h^0.0146767)
    h2: 4346.25 MB (U(h) = 0.344726*h^0.0958067)
Recommendation: 2 clients, utility 0.758415:
    h1: 665.796 MB (U(h) = 0.896274*h^0.0146767)
    h2: 4346.2 MB (U(h) = 0.344726*h^0.0958067)
Reading from 12766: heap size 1259 MB, throughput 0.986906
Reading from 12768: heap size 860 MB, throughput 0.990211
Reading from 12766: heap size 1261 MB, throughput 0.985474
Reading from 12768: heap size 806 MB, throughput 0.990461
Reading from 12766: heap size 1255 MB, throughput 0.984716
Reading from 12768: heap size 765 MB, throughput 0.989842
Reading from 12768: heap size 724 MB, throughput 0.988061
Numeric result:
Recommendation: 2 clients, utility 0.822445:
    h1: 509.838 MB (U(h) = 0.896052*h^0.0147362)
    h2: 4502.16 MB (U(h) = 0.280155*h^0.130146)
Recommendation: 2 clients, utility 0.822445:
    h1: 509.778 MB (U(h) = 0.896052*h^0.0147362)
    h2: 4502.22 MB (U(h) = 0.280155*h^0.130146)
Reading from 12766: heap size 1144 MB, throughput 0.984169
Reading from 12768: heap size 703 MB, throughput 0.987333
Reading from 12766: heap size 1244 MB, throughput 0.982419
Reading from 12768: heap size 669 MB, throughput 0.991639
Reading from 12768: heap size 625 MB, throughput 0.992476
Reading from 12766: heap size 1163 MB, throughput 0.98222
Reading from 12768: heap size 606 MB, throughput 0.988789
Reading from 12768: heap size 590 MB, throughput 0.991331
Numeric result:
Recommendation: 2 clients, utility 0.939421:
    h1: 358.013 MB (U(h) = 0.895465*h^0.0149234)
    h2: 4653.99 MB (U(h) = 0.18674*h^0.193974)
Recommendation: 2 clients, utility 0.939421:
    h1: 358.051 MB (U(h) = 0.895465*h^0.0149234)
    h2: 4653.95 MB (U(h) = 0.18674*h^0.193974)
Reading from 12766: heap size 1235 MB, throughput 0.981009
Reading from 12768: heap size 556 MB, throughput 0.987248
Reading from 12768: heap size 539 MB, throughput 0.987616
Reading from 12768: heap size 500 MB, throughput 0.988611
Reading from 12766: heap size 1241 MB, throughput 0.978436
Reading from 12768: heap size 474 MB, throughput 0.986846
Reading from 12768: heap size 455 MB, throughput 0.988176
Reading from 12766: heap size 1241 MB, throughput 0.977388
Reading from 12768: heap size 437 MB, throughput 0.987648
Reading from 12768: heap size 415 MB, throughput 0.987865
Reading from 12768: heap size 400 MB, throughput 0.989192
Numeric result:
Recommendation: 2 clients, utility 0.963167:
    h1: 345.933 MB (U(h) = 0.895499*h^0.0150572)
    h2: 4666.07 MB (U(h) = 0.17715*h^0.203073)
Recommendation: 2 clients, utility 0.963167:
    h1: 345.97 MB (U(h) = 0.895499*h^0.0150572)
    h2: 4666.03 MB (U(h) = 0.17715*h^0.203073)
Reading from 12768: heap size 386 MB, throughput 0.973602
Reading from 12766: heap size 1242 MB, throughput 0.972202
Reading from 12768: heap size 382 MB, throughput 0.976702
Reading from 12768: heap size 362 MB, throughput 0.982835
Reading from 12768: heap size 351 MB, throughput 0.978959
Reading from 12766: heap size 1244 MB, throughput 0.971223
Reading from 12768: heap size 344 MB, throughput 0.980439
Reading from 12768: heap size 354 MB, throughput 0.98599
Reading from 12768: heap size 343 MB, throughput 0.985667
Reading from 12768: heap size 366 MB, throughput 0.987264
Reading from 12766: heap size 1248 MB, throughput 0.973062
Reading from 12768: heap size 351 MB, throughput 0.990494
Reading from 12768: heap size 339 MB, throughput 0.984961
Numeric result:
Recommendation: 2 clients, utility 1.05834:
    h1: 282.169 MB (U(h) = 0.896219*h^0.0150096)
    h2: 4729.83 MB (U(h) = 0.12907*h^0.251603)
Recommendation: 2 clients, utility 1.05834:
    h1: 282.163 MB (U(h) = 0.896219*h^0.0150096)
    h2: 4729.84 MB (U(h) = 0.12907*h^0.251603)
Reading from 12768: heap size 368 MB, throughput 0.989463
Reading from 12768: heap size 343 MB, throughput 0.991146
Reading from 12766: heap size 1252 MB, throughput 0.971683
Reading from 12768: heap size 331 MB, throughput 0.984433
Reading from 12768: heap size 330 MB, throughput 0.988382
Reading from 12768: heap size 309 MB, throughput 0.990036
Reading from 12768: heap size 300 MB, throughput 0.983313
Reading from 12766: heap size 1258 MB, throughput 0.971042
Reading from 12768: heap size 300 MB, throughput 0.986581
Reading from 12768: heap size 282 MB, throughput 0.983758
Reading from 12768: heap size 275 MB, throughput 0.987147
Reading from 12768: heap size 288 MB, throughput 0.97933
Reading from 12768: heap size 292 MB, throughput 0.985317
Reading from 12766: heap size 1265 MB, throughput 0.971091
Reading from 12768: heap size 272 MB, throughput 0.986042
Numeric result:
Recommendation: 2 clients, utility 1.18594:
    h1: 227.367 MB (U(h) = 0.898581*h^0.0147577)
    h2: 4784.63 MB (U(h) = 0.0876901*h^0.310549)
Recommendation: 2 clients, utility 1.18594:
    h1: 227.372 MB (U(h) = 0.898581*h^0.0147577)
    h2: 4784.63 MB (U(h) = 0.0876901*h^0.310549)
Reading from 12768: heap size 286 MB, throughput 0.989677
Reading from 12768: heap size 270 MB, throughput 0.988239
Reading from 12768: heap size 270 MB, throughput 0.988228
Reading from 12768: heap size 262 MB, throughput 0.986781
Reading from 12766: heap size 1269 MB, throughput 0.970553
Reading from 12768: heap size 259 MB, throughput 0.989708
Reading from 12768: heap size 244 MB, throughput 0.98134
Reading from 12768: heap size 249 MB, throughput 0.979917
Reading from 12768: heap size 234 MB, throughput 0.985386
Reading from 12768: heap size 234 MB, throughput 0.983921
Reading from 12768: heap size 222 MB, throughput 0.986042
Reading from 12768: heap size 236 MB, throughput 0.979878
Reading from 12766: heap size 1275 MB, throughput 0.967964
Reading from 12768: heap size 222 MB, throughput 0.978466
Reading from 12768: heap size 231 MB, throughput 0.980358
Reading from 12768: heap size 219 MB, throughput 0.984343
Reading from 12768: heap size 226 MB, throughput 0.986611
Reading from 12768: heap size 226 MB, throughput 0.980519
Reading from 12768: heap size 236 MB, throughput 0.980874
Reading from 12768: heap size 223 MB, throughput 0.983278
Numeric result:
Recommendation: 2 clients, utility 1.30927:
    h1: 187.32 MB (U(h) = 0.903144*h^0.0141298)
    h2: 4824.68 MB (U(h) = 0.0614175*h^0.364024)
Recommendation: 2 clients, utility 1.30927:
    h1: 187.274 MB (U(h) = 0.903144*h^0.0141298)
    h2: 4824.73 MB (U(h) = 0.0614175*h^0.364024)
Reading from 12766: heap size 1277 MB, throughput 0.969557
Reading from 12768: heap size 230 MB, throughput 0.98426
Reading from 12768: heap size 216 MB, throughput 0.986494
Reading from 12768: heap size 213 MB, throughput 0.988596
Reading from 12768: heap size 206 MB, throughput 0.980438
Reading from 12768: heap size 204 MB, throughput 0.978016
Reading from 12768: heap size 198 MB, throughput 0.981753
Reading from 12768: heap size 196 MB, throughput 0.984347
Reading from 12768: heap size 191 MB, throughput 0.978012
Reading from 12768: heap size 188 MB, throughput 0.983736
Reading from 12766: heap size 1283 MB, throughput 0.967475
Reading from 12768: heap size 184 MB, throughput 0.971542
Reading from 12768: heap size 189 MB, throughput 0.979333
Reading from 12768: heap size 175 MB, throughput 0.969868
Reading from 12768: heap size 187 MB, throughput 0.977924
Reading from 12768: heap size 186 MB, throughput 0.978062
Reading from 12768: heap size 195 MB, throughput 0.980691
Reading from 12768: heap size 187 MB, throughput 0.983725
Reading from 12768: heap size 184 MB, throughput 0.986617
Reading from 12768: heap size 188 MB, throughput 0.988171
Reading from 12768: heap size 176 MB, throughput 0.976212
Reading from 12766: heap size 1284 MB, throughput 0.968543
Reading from 12768: heap size 188 MB, throughput 0.980694
Reading from 12768: heap size 179 MB, throughput 0.977553
Reading from 12768: heap size 192 MB, throughput 0.979937
Reading from 12768: heap size 183 MB, throughput 0.979446
Reading from 12768: heap size 190 MB, throughput 0.981965
Numeric result:
Recommendation: 2 clients, utility 1.41445:
    h1: 81.5867 MB (U(h) = 0.947304*h^0.00661778)
    h2: 4930.41 MB (U(h) = 0.0484189*h^0.399805)
Recommendation: 2 clients, utility 1.41445:
    h1: 81.6104 MB (U(h) = 0.947304*h^0.00661778)
    h2: 4930.39 MB (U(h) = 0.0484189*h^0.399805)
Reading from 12768: heap size 181 MB, throughput 0.973985
Reading from 12768: heap size 177 MB, throughput 0.984021
Reading from 12768: heap size 138 MB, throughput 0.976521
Reading from 12768: heap size 128 MB, throughput 0.982278
Reading from 12768: heap size 131 MB, throughput 0.973089
Reading from 12766: heap size 1291 MB, throughput 0.968801
Reading from 12768: heap size 126 MB, throughput 0.975125
Reading from 12768: heap size 127 MB, throughput 0.97836
Reading from 12768: heap size 117 MB, throughput 0.965273
Reading from 12768: heap size 120 MB, throughput 0.972875
Reading from 12768: heap size 112 MB, throughput 0.979741
Reading from 12768: heap size 115 MB, throughput 0.976932
Reading from 12768: heap size 108 MB, throughput 0.325035
Reading from 12768: heap size 109 MB, throughput 0.555646
Reading from 12768: heap size 108 MB, throughput 0.950385
Reading from 12768: heap size 107 MB, throughput 0.955633
Reading from 12768: heap size 100 MB, throughput 0.951122
Reading from 12768: heap size 112 MB, throughput 0.687755
Reading from 12768: heap size 100 MB, throughput 0.438269
Reading from 12768: heap size 109 MB, throughput 0.439758
Reading from 12768: heap size 93 MB, throughput 0.986839
Reading from 12768: heap size 107 MB, throughput 0.98815
Reading from 12768: heap size 91 MB, throughput 0.990474
Reading from 12768: heap size 104 MB, throughput 0.869419
Reading from 12768: heap size 89 MB, throughput 0.959949
Reading from 12766: heap size 1291 MB, throughput 0.970584
Reading from 12768: heap size 101 MB, throughput 0.984575
Reading from 12768: heap size 89 MB, throughput 0.981565
Reading from 12768: heap size 97 MB, throughput 0.980118
Reading from 12768: heap size 89 MB, throughput 0.968503
Reading from 12768: heap size 96 MB, throughput 0.77784
Reading from 12768: heap size 96 MB, throughput 0.314857
Reading from 12768: heap size 99 MB, throughput 0.949185
Reading from 12768: heap size 90 MB, throughput 0.951045
Reading from 12768: heap size 97 MB, throughput 0.951109
Reading from 12768: heap size 84 MB, throughput 0.338287
Reading from 12768: heap size 95 MB, throughput 0.441294
Reading from 12768: heap size 81 MB, throughput 0.455761
Reading from 12768: heap size 95 MB, throughput 0.490054
Reading from 12768: heap size 79 MB, throughput 0.63514
Reading from 12768: heap size 95 MB, throughput 0.80124
Reading from 12768: heap size 80 MB, throughput 0.881971
Reading from 12768: heap size 94 MB, throughput 0.92606
Reading from 12768: heap size 80 MB, throughput 0.966112
Reading from 12768: heap size 93 MB, throughput 0.976586
Reading from 12768: heap size 80 MB, throughput 0.983518
Reading from 12768: heap size 92 MB, throughput 0.987811
Reading from 12768: heap size 80 MB, throughput 0.991045
Reading from 12768: heap size 91 MB, throughput 0.983974
Reading from 12768: heap size 85 MB, throughput 0.982004
Reading from 12768: heap size 89 MB, throughput 0.980576
Reading from 12768: heap size 83 MB, throughput 0.895552
Reading from 12768: heap size 86 MB, throughput 0.736438
Reading from 12768: heap size 88 MB, throughput 0.517459
Reading from 12766: heap size 1295 MB, throughput 0.969839
Reading from 12768: heap size 89 MB, throughput 0.903721
Reading from 12768: heap size 90 MB, throughput 0.913921
Reading from 12768: heap size 90 MB, throughput 0.945914
Reading from 12768: heap size 77 MB, throughput 0.956112
Reading from 12768: heap size 88 MB, throughput 0.974297
Reading from 12768: heap size 70 MB, throughput 0.966682
Numeric result:
Recommendation: 2 clients, utility 1.6851:
    h1: 1142.42 MB (U(h) = 0.455798*h^0.134683)
    h2: 3869.58 MB (U(h) = 0.0330625*h^0.456196)
Recommendation: 2 clients, utility 1.6851:
    h1: 1142.42 MB (U(h) = 0.455798*h^0.134683)
    h2: 3869.58 MB (U(h) = 0.0330625*h^0.456196)
Reading from 12768: heap size 88 MB, throughput 0.965099
Reading from 12768: heap size 87 MB, throughput 0.974264
Reading from 12768: heap size 90 MB, throughput 0.981503
Reading from 12768: heap size 90 MB, throughput 0.983277
Reading from 12768: heap size 92 MB, throughput 0.985179
Reading from 12768: heap size 93 MB, throughput 0.689641
Reading from 12768: heap size 95 MB, throughput 0.918487
Reading from 12768: heap size 96 MB, throughput 0.965638
Reading from 12768: heap size 97 MB, throughput 0.98358
Reading from 12768: heap size 98 MB, throughput 0.98761
Reading from 12768: heap size 100 MB, throughput 0.990022
Reading from 12768: heap size 101 MB, throughput 0.982207
Reading from 12768: heap size 104 MB, throughput 0.979193
Reading from 12768: heap size 104 MB, throughput 0.766333
Reading from 12768: heap size 107 MB, throughput 0.95605
Reading from 12768: heap size 110 MB, throughput 0.958223
Reading from 12768: heap size 117 MB, throughput 0.959184
Reading from 12768: heap size 118 MB, throughput 0.953476
Reading from 12768: heap size 129 MB, throughput 0.974726
Reading from 12768: heap size 129 MB, throughput 0.956327
Reading from 12766: heap size 1296 MB, throughput 0.969973
Reading from 12768: heap size 138 MB, throughput 0.976187
Reading from 12768: heap size 138 MB, throughput 0.985025
Reading from 12768: heap size 145 MB, throughput 0.988995
Reading from 12768: heap size 146 MB, throughput 0.983899
Reading from 12768: heap size 152 MB, throughput 0.321945
Reading from 12768: heap size 153 MB, throughput 0.947791
Reading from 12768: heap size 170 MB, throughput 0.940648
Reading from 12768: heap size 170 MB, throughput 0.924114
Reading from 12768: heap size 178 MB, throughput 0.962379
Reading from 12768: heap size 179 MB, throughput 0.9728
Reading from 12766: heap size 1299 MB, throughput 0.970345
Reading from 12768: heap size 188 MB, throughput 0.982756
Reading from 12768: heap size 189 MB, throughput 0.901568
Reading from 12768: heap size 196 MB, throughput 0.779653
Reading from 12768: heap size 200 MB, throughput 0.867445
Reading from 12768: heap size 212 MB, throughput 0.0465489
Reading from 12768: heap size 214 MB, throughput 0.858558
Numeric result:
Recommendation: 2 clients, utility 1.73754:
    h1: 870.86 MB (U(h) = 0.508931*h^0.109349)
    h2: 4141.14 MB (U(h) = 0.0214225*h^0.520006)
Recommendation: 2 clients, utility 1.73754:
    h1: 870.824 MB (U(h) = 0.508931*h^0.109349)
    h2: 4141.18 MB (U(h) = 0.0214225*h^0.520006)
Reading from 12768: heap size 226 MB, throughput 0.929433
Reading from 12768: heap size 226 MB, throughput 0.837243
Reading from 12766: heap size 1300 MB, throughput 0.967446
Reading from 12768: heap size 238 MB, throughput 0.960056
Reading from 12768: heap size 254 MB, throughput 0.98402
Reading from 12768: heap size 272 MB, throughput 0.991322
Reading from 12768: heap size 273 MB, throughput 0.982856
Reading from 12768: heap size 290 MB, throughput 0.985064
Reading from 12768: heap size 291 MB, throughput 0.985724
Reading from 12766: heap size 1303 MB, throughput 0.968479
Reading from 12768: heap size 308 MB, throughput 0.982026
Reading from 12768: heap size 309 MB, throughput 0.980815
Reading from 12768: heap size 331 MB, throughput 0.985314
Reading from 12768: heap size 331 MB, throughput 0.987286
Reading from 12768: heap size 349 MB, throughput 0.981086
Numeric result:
Recommendation: 2 clients, utility 1.8434:
    h1: 821.128 MB (U(h) = 0.508892*h^0.109423)
    h2: 4190.87 MB (U(h) = 0.0164872*h^0.558469)
Recommendation: 2 clients, utility 1.8434:
    h1: 821.132 MB (U(h) = 0.508892*h^0.109423)
    h2: 4190.87 MB (U(h) = 0.0164872*h^0.558469)
Reading from 12768: heap size 351 MB, throughput 0.973119
Reading from 12768: heap size 377 MB, throughput 0.974943
Reading from 12766: heap size 1304 MB, throughput 0.95442
Reading from 12768: heap size 379 MB, throughput 0.975493
Reading from 12768: heap size 404 MB, throughput 0.983365
Reading from 12768: heap size 408 MB, throughput 0.981566
Reading from 12768: heap size 435 MB, throughput 0.984791
Reading from 12766: heap size 1419 MB, throughput 0.965287
Reading from 12768: heap size 436 MB, throughput 0.985891
Reading from 12768: heap size 461 MB, throughput 0.983754
Reading from 12768: heap size 463 MB, throughput 0.986467
Numeric result:
Recommendation: 2 clients, utility 1.94391:
    h1: 710.593 MB (U(h) = 0.53118*h^0.100267)
    h2: 4301.41 MB (U(h) = 0.0118073*h^0.606934)
Recommendation: 2 clients, utility 1.94391:
    h1: 710.601 MB (U(h) = 0.53118*h^0.100267)
    h2: 4301.4 MB (U(h) = 0.0118073*h^0.606934)
Reading from 12766: heap size 1420 MB, throughput 0.976169
Reading from 12768: heap size 493 MB, throughput 0.982589
Reading from 12768: heap size 495 MB, throughput 0.987277
Reading from 12768: heap size 524 MB, throughput 0.986003
Reading from 12766: heap size 1433 MB, throughput 0.980302
Reading from 12768: heap size 528 MB, throughput 0.98974
Reading from 12768: heap size 555 MB, throughput 0.984495
Reading from 12768: heap size 558 MB, throughput 0.98647
Reading from 12766: heap size 1438 MB, throughput 0.98238
Numeric result:
Recommendation: 2 clients, utility 2.03089:
    h1: 638.075 MB (U(h) = 0.54549*h^0.0942382)
    h2: 4373.93 MB (U(h) = 0.00900861*h^0.645976)
Recommendation: 2 clients, utility 2.03089:
    h1: 638.088 MB (U(h) = 0.54549*h^0.0942382)
    h2: 4373.91 MB (U(h) = 0.00900861*h^0.645976)
Reading from 12768: heap size 591 MB, throughput 0.990713
Reading from 12768: heap size 593 MB, throughput 0.988383
Reading from 12766: heap size 1441 MB, throughput 0.982242
Reading from 12768: heap size 626 MB, throughput 0.992525
Reading from 12768: heap size 626 MB, throughput 0.985662
Reading from 12768: heap size 658 MB, throughput 0.987026
Reading from 12766: heap size 1443 MB, throughput 0.979651
Reading from 12768: heap size 624 MB, throughput 0.991966
Numeric result:
Recommendation: 2 clients, utility 2.21027:
    h1: 565.814 MB (U(h) = 0.553583*h^0.0905532)
    h2: 4446.19 MB (U(h) = 0.00570373*h^0.711581)
Recommendation: 2 clients, utility 2.21027:
    h1: 565.806 MB (U(h) = 0.553583*h^0.0905532)
    h2: 4446.19 MB (U(h) = 0.00570373*h^0.711581)
Reading from 12768: heap size 634 MB, throughput 0.99292
Reading from 12768: heap size 599 MB, throughput 0.988968
Reading from 12768: heap size 588 MB, throughput 0.991789
Reading from 12768: heap size 553 MB, throughput 0.988572
Reading from 12768: heap size 589 MB, throughput 0.987625
Reading from 12768: heap size 557 MB, throughput 0.990696
Numeric result:
Recommendation: 2 clients, utility 2.19982:
    h1: 568.518 MB (U(h) = 0.549247*h^0.0910461)
    h2: 4443.48 MB (U(h) = 0.00570373*h^0.711581)
Recommendation: 2 clients, utility 2.19982:
    h1: 568.537 MB (U(h) = 0.549247*h^0.0910461)
    h2: 4443.46 MB (U(h) = 0.00570373*h^0.711581)
Reading from 12768: heap size 571 MB, throughput 0.988355
Reading from 12768: heap size 545 MB, throughput 0.991501
Reading from 12768: heap size 559 MB, throughput 0.987454
Reading from 12768: heap size 565 MB, throughput 0.994009
Reading from 12768: heap size 565 MB, throughput 0.994008
Reading from 12768: heap size 565 MB, throughput 0.969909
Reading from 12768: heap size 596 MB, throughput 0.951769
Reading from 12768: heap size 555 MB, throughput 0.988058
Reading from 12768: heap size 574 MB, throughput 0.986077
Reading from 12768: heap size 548 MB, throughput 0.990603
Numeric result:
Recommendation: 2 clients, utility 2.18987:
    h1: 581.203 MB (U(h) = 0.538842*h^0.0933421)
    h2: 4430.8 MB (U(h) = 0.00570373*h^0.711581)
Recommendation: 2 clients, utility 2.18987:
    h1: 581.212 MB (U(h) = 0.538842*h^0.0933421)
    h2: 4430.79 MB (U(h) = 0.00570373*h^0.711581)
Reading from 12768: heap size 559 MB, throughput 0.988486
Reading from 12768: heap size 568 MB, throughput 0.988071
Reading from 12768: heap size 633 MB, throughput 0.989407
Reading from 12766: heap size 1434 MB, throughput 0.993577
Reading from 12768: heap size 594 MB, throughput 0.987573
Reading from 12768: heap size 581 MB, throughput 0.988883
Reading from 12768: heap size 552 MB, throughput 0.991031
Reading from 12768: heap size 587 MB, throughput 0.987448
Numeric result:
Recommendation: 2 clients, utility 2.29789:
    h1: 592.929 MB (U(h) = 0.515917*h^0.100076)
    h2: 4419.07 MB (U(h) = 0.00449079*h^0.745863)
Recommendation: 2 clients, utility 2.29789:
    h1: 592.927 MB (U(h) = 0.515917*h^0.100076)
    h2: 4419.07 MB (U(h) = 0.00449079*h^0.745863)
Reading from 12768: heap size 566 MB, throughput 0.99108
Reading from 12766: heap size 1439 MB, throughput 0.989712
Reading from 12766: heap size 1423 MB, throughput 0.981468
Reading from 12768: heap size 573 MB, throughput 0.988784
Reading from 12766: heap size 1452 MB, throughput 0.962538
Reading from 12766: heap size 1438 MB, throughput 0.935081
Reading from 12766: heap size 1480 MB, throughput 0.890532
Reading from 12766: heap size 1509 MB, throughput 0.840699
Reading from 12766: heap size 1527 MB, throughput 0.782497
Reading from 12768: heap size 587 MB, throughput 0.989356
Reading from 12766: heap size 1560 MB, throughput 0.741685
Reading from 12766: heap size 1570 MB, throughput 0.739362
Reading from 12768: heap size 624 MB, throughput 0.991497
Reading from 12768: heap size 585 MB, throughput 0.988597
Reading from 12766: heap size 1577 MB, throughput 0.949821
Numeric result:
Recommendation: 2 clients, utility 2.41129:
    h1: 589.139 MB (U(h) = 0.496525*h^0.106285)
    h2: 4422.86 MB (U(h) = 0.00303941*h^0.797949)
Recommendation: 2 clients, utility 2.41129:
    h1: 589.119 MB (U(h) = 0.496525*h^0.106285)
    h2: 4422.88 MB (U(h) = 0.00303941*h^0.797949)
Reading from 12768: heap size 603 MB, throughput 0.98932
Reading from 12768: heap size 570 MB, throughput 0.99126
Reading from 12766: heap size 1593 MB, throughput 0.9703
Reading from 12768: heap size 586 MB, throughput 0.988261
Reading from 12768: heap size 592 MB, throughput 0.991645
Reading from 12768: heap size 554 MB, throughput 0.989607
Reading from 12766: heap size 1581 MB, throughput 0.975319
Reading from 12768: heap size 597 MB, throughput 0.989583
Numeric result:
Recommendation: 2 clients, utility 2.34346:
    h1: 413.888 MB (U(h) = 0.619804*h^0.0702813)
    h2: 4598.11 MB (U(h) = 0.00342087*h^0.780747)
Recommendation: 2 clients, utility 2.34346:
    h1: 413.911 MB (U(h) = 0.619804*h^0.0702813)
    h2: 4598.09 MB (U(h) = 0.00342087*h^0.780747)
Reading from 12768: heap size 564 MB, throughput 0.991838
Reading from 12768: heap size 534 MB, throughput 0.98855
Reading from 12766: heap size 1598 MB, throughput 0.97799
Reading from 12768: heap size 519 MB, throughput 0.991655
Client 12768 died
Clients: 1
Reading from 12766: heap size 1581 MB, throughput 0.981027
Recommendation: one client; give it all the memory
Reading from 12766: heap size 1597 MB, throughput 0.967989
Reading from 12766: heap size 1561 MB, throughput 0.98938
Recommendation: one client; give it all the memory
Reading from 12766: heap size 1567 MB, throughput 0.992346
Reading from 12766: heap size 1592 MB, throughput 0.992947
Recommendation: one client; give it all the memory
Reading from 12766: heap size 1597 MB, throughput 0.99263
Recommendation: one client; give it all the memory
Reading from 12766: heap size 1597 MB, throughput 0.992161
Reading from 12766: heap size 1604 MB, throughput 0.991244
Recommendation: one client; give it all the memory
Reading from 12766: heap size 1583 MB, throughput 0.990523
Reading from 12766: heap size 1429 MB, throughput 0.989486
Recommendation: one client; give it all the memory
Reading from 12766: heap size 1562 MB, throughput 0.988477
Reading from 12766: heap size 1460 MB, throughput 0.98745
Recommendation: one client; give it all the memory
Reading from 12766: heap size 1566 MB, throughput 0.986948
Recommendation: one client; give it all the memory
Reading from 12766: heap size 1570 MB, throughput 0.985863
Reading from 12766: heap size 1574 MB, throughput 0.985055
Recommendation: one client; give it all the memory
Reading from 12766: heap size 1577 MB, throughput 0.983701
Reading from 12766: heap size 1582 MB, throughput 0.98292
Recommendation: one client; give it all the memory
Reading from 12766: heap size 1591 MB, throughput 0.981765
Recommendation: one client; give it all the memory
Reading from 12766: heap size 1601 MB, throughput 0.980754
Reading from 12766: heap size 1610 MB, throughput 0.98019
Recommendation: one client; give it all the memory
Reading from 12766: heap size 1620 MB, throughput 0.979995
Reading from 12766: heap size 1625 MB, throughput 0.979906
Recommendation: one client; give it all the memory
Reading from 12766: heap size 1636 MB, throughput 0.979869
Recommendation: one client; give it all the memory
Reading from 12766: heap size 1638 MB, throughput 0.979685
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 12766: heap size 1649 MB, throughput 0.991728
Recommendation: one client; give it all the memory
Reading from 12766: heap size 1539 MB, throughput 0.989976
Reading from 12766: heap size 1640 MB, throughput 0.982137
Reading from 12766: heap size 1648 MB, throughput 0.96514
Reading from 12766: heap size 1676 MB, throughput 0.943233
Reading from 12766: heap size 1695 MB, throughput 0.909939
Client 12766 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
