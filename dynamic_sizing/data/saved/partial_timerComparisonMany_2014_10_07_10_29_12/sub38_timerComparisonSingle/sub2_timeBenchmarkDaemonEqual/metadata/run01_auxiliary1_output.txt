economemd
    total memory: 1440 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub38_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 24986: heap size 9 MB, throughput 0.984511
Clients: 1
Client 24986 has a minimum heap size of 12 MB
Reading from 24985: heap size 9 MB, throughput 0.99293
Clients: 2
Client 24985 has a minimum heap size of 276 MB
Reading from 24985: heap size 9 MB, throughput 0.97438
Reading from 24986: heap size 9 MB, throughput 0.970281
Reading from 24985: heap size 9 MB, throughput 0.93216
Reading from 24985: heap size 9 MB, throughput 0.97095
Reading from 24986: heap size 11 MB, throughput 0.985158
Reading from 24986: heap size 11 MB, throughput 0.971577
Reading from 24985: heap size 11 MB, throughput 0.988907
Reading from 24986: heap size 15 MB, throughput 0.944937
Reading from 24985: heap size 11 MB, throughput 0.98794
Reading from 24986: heap size 15 MB, throughput 0.985887
Reading from 24985: heap size 17 MB, throughput 0.943715
Reading from 24985: heap size 17 MB, throughput 0.572566
Reading from 24985: heap size 30 MB, throughput 0.91268
Reading from 24985: heap size 31 MB, throughput 0.940757
Reading from 24986: heap size 24 MB, throughput 0.948778
Reading from 24985: heap size 36 MB, throughput 0.313898
Reading from 24985: heap size 48 MB, throughput 0.85531
Reading from 24986: heap size 30 MB, throughput 0.985609
Reading from 24985: heap size 49 MB, throughput 0.425265
Reading from 24985: heap size 66 MB, throughput 0.865342
Reading from 24985: heap size 71 MB, throughput 0.860879
Reading from 24986: heap size 39 MB, throughput 0.978339
Reading from 24985: heap size 72 MB, throughput 0.213458
Reading from 24986: heap size 46 MB, throughput 0.977392
Reading from 24985: heap size 100 MB, throughput 0.7953
Reading from 24985: heap size 101 MB, throughput 0.655079
Reading from 24986: heap size 49 MB, throughput 0.985902
Reading from 24985: heap size 102 MB, throughput 0.235095
Reading from 24985: heap size 132 MB, throughput 0.72896
Reading from 24985: heap size 137 MB, throughput 0.827522
Reading from 24986: heap size 49 MB, throughput 0.74124
Reading from 24985: heap size 137 MB, throughput 0.651877
Reading from 24985: heap size 143 MB, throughput 0.703991
Reading from 24986: heap size 61 MB, throughput 0.992131
Reading from 24986: heap size 61 MB, throughput 0.981925
Reading from 24986: heap size 70 MB, throughput 0.992404
Reading from 24985: heap size 146 MB, throughput 0.120765
Reading from 24986: heap size 70 MB, throughput 0.967595
Reading from 24985: heap size 188 MB, throughput 0.71744
Reading from 24985: heap size 190 MB, throughput 0.708892
Reading from 24986: heap size 80 MB, throughput 0.986766
Reading from 24985: heap size 192 MB, throughput 0.595282
Reading from 24985: heap size 196 MB, throughput 0.802728
Reading from 24986: heap size 80 MB, throughput 0.993357
Reading from 24985: heap size 203 MB, throughput 0.9489
Reading from 24986: heap size 86 MB, throughput 0.997034
Reading from 24985: heap size 209 MB, throughput 0.161655
Reading from 24986: heap size 87 MB, throughput 0.995617
Reading from 24985: heap size 272 MB, throughput 0.722831
Reading from 24985: heap size 272 MB, throughput 0.90749
Reading from 24985: heap size 273 MB, throughput 0.841351
Reading from 24986: heap size 93 MB, throughput 0.995919
Reading from 24985: heap size 275 MB, throughput 0.90134
Reading from 24985: heap size 278 MB, throughput 0.856282
Reading from 24985: heap size 278 MB, throughput 0.78425
Reading from 24985: heap size 284 MB, throughput 0.855927
Reading from 24986: heap size 94 MB, throughput 0.995162
Reading from 24985: heap size 287 MB, throughput 0.770072
Reading from 24985: heap size 292 MB, throughput 0.921096
Reading from 24986: heap size 99 MB, throughput 0.996273
Reading from 24985: heap size 294 MB, throughput 0.945284
Reading from 24985: heap size 293 MB, throughput 0.724171
Reading from 24985: heap size 297 MB, throughput 0.75094
Reading from 24985: heap size 305 MB, throughput 0.806515
Reading from 24986: heap size 100 MB, throughput 0.995314
Reading from 24985: heap size 305 MB, throughput 0.158698
Reading from 24986: heap size 105 MB, throughput 0.985371
Reading from 24985: heap size 352 MB, throughput 0.776042
Equal recommendation: 720 MB each
Reading from 24985: heap size 353 MB, throughput 0.850853
Reading from 24985: heap size 355 MB, throughput 0.738572
Reading from 24985: heap size 358 MB, throughput 0.786549
Reading from 24985: heap size 356 MB, throughput 0.756081
Reading from 24985: heap size 359 MB, throughput 0.773413
Reading from 24986: heap size 105 MB, throughput 0.996074
Reading from 24985: heap size 360 MB, throughput 0.925532
Reading from 24986: heap size 109 MB, throughput 0.996204
Reading from 24986: heap size 110 MB, throughput 0.993269
Reading from 24985: heap size 363 MB, throughput 0.990978
Reading from 24986: heap size 115 MB, throughput 0.994216
Reading from 24986: heap size 115 MB, throughput 0.993222
Reading from 24985: heap size 369 MB, throughput 0.983266
Reading from 24986: heap size 120 MB, throughput 0.996229
Reading from 24986: heap size 120 MB, throughput 0.995673
Reading from 24985: heap size 370 MB, throughput 0.986898
Reading from 24986: heap size 124 MB, throughput 0.995813
Reading from 24986: heap size 125 MB, throughput 0.99757
Reading from 24985: heap size 370 MB, throughput 0.981131
Reading from 24986: heap size 128 MB, throughput 0.997454
Reading from 24986: heap size 128 MB, throughput 0.997188
Reading from 24985: heap size 373 MB, throughput 0.985722
Reading from 24986: heap size 131 MB, throughput 0.998491
Reading from 24986: heap size 131 MB, throughput 0.997734
Equal recommendation: 720 MB each
Reading from 24985: heap size 368 MB, throughput 0.978025
Reading from 24986: heap size 133 MB, throughput 0.998401
Reading from 24986: heap size 133 MB, throughput 0.997983
Reading from 24985: heap size 372 MB, throughput 0.980115
Reading from 24986: heap size 136 MB, throughput 0.998728
Reading from 24986: heap size 136 MB, throughput 0.997124
Reading from 24985: heap size 373 MB, throughput 0.964083
Reading from 24986: heap size 138 MB, throughput 0.99319
Reading from 24986: heap size 138 MB, throughput 0.990863
Reading from 24985: heap size 373 MB, throughput 0.976441
Reading from 24986: heap size 140 MB, throughput 0.994759
Reading from 24986: heap size 141 MB, throughput 0.997352
Reading from 24985: heap size 376 MB, throughput 0.975526
Reading from 24986: heap size 145 MB, throughput 0.997417
Reading from 24986: heap size 145 MB, throughput 0.99707
Reading from 24985: heap size 377 MB, throughput 0.974171
Reading from 24986: heap size 148 MB, throughput 0.996696
Reading from 24986: heap size 148 MB, throughput 0.997604
Reading from 24985: heap size 380 MB, throughput 0.980015
Equal recommendation: 720 MB each
Reading from 24986: heap size 151 MB, throughput 0.99795
Reading from 24985: heap size 382 MB, throughput 0.980314
Reading from 24986: heap size 151 MB, throughput 0.996977
Reading from 24986: heap size 154 MB, throughput 0.997641
Reading from 24986: heap size 154 MB, throughput 0.99694
Reading from 24985: heap size 384 MB, throughput 0.989756
Reading from 24985: heap size 386 MB, throughput 0.908994
Reading from 24985: heap size 382 MB, throughput 0.83564
Reading from 24985: heap size 386 MB, throughput 0.816893
Reading from 24985: heap size 392 MB, throughput 0.806856
Reading from 24986: heap size 155 MB, throughput 0.997432
Reading from 24985: heap size 395 MB, throughput 0.942683
Reading from 24986: heap size 156 MB, throughput 0.997013
Reading from 24986: heap size 158 MB, throughput 0.99509
Reading from 24985: heap size 400 MB, throughput 0.988574
Reading from 24986: heap size 158 MB, throughput 0.997284
Reading from 24986: heap size 161 MB, throughput 0.998079
Reading from 24985: heap size 402 MB, throughput 0.99031
Reading from 24986: heap size 161 MB, throughput 0.998674
Equal recommendation: 720 MB each
Reading from 24985: heap size 401 MB, throughput 0.990234
Reading from 24986: heap size 164 MB, throughput 0.998849
Reading from 24986: heap size 164 MB, throughput 0.998666
Reading from 24986: heap size 166 MB, throughput 0.993963
Reading from 24985: heap size 403 MB, throughput 0.981972
Reading from 24986: heap size 166 MB, throughput 0.987058
Reading from 24986: heap size 169 MB, throughput 0.997003
Reading from 24985: heap size 401 MB, throughput 0.987922
Reading from 24986: heap size 169 MB, throughput 0.997792
Reading from 24985: heap size 403 MB, throughput 0.98761
Reading from 24986: heap size 172 MB, throughput 0.997984
Reading from 24986: heap size 173 MB, throughput 0.9972
Reading from 24985: heap size 399 MB, throughput 0.986571
Reading from 24986: heap size 176 MB, throughput 0.997897
Reading from 24986: heap size 176 MB, throughput 0.997021
Reading from 24985: heap size 402 MB, throughput 0.980624
Equal recommendation: 720 MB each
Reading from 24986: heap size 178 MB, throughput 0.996698
Reading from 24985: heap size 398 MB, throughput 0.985023
Reading from 24986: heap size 179 MB, throughput 0.997632
Reading from 24986: heap size 182 MB, throughput 0.997814
Reading from 24985: heap size 400 MB, throughput 0.981678
Reading from 24986: heap size 182 MB, throughput 0.997254
Reading from 24985: heap size 398 MB, throughput 0.986838
Reading from 24986: heap size 184 MB, throughput 0.997839
Reading from 24986: heap size 184 MB, throughput 0.997443
Reading from 24985: heap size 399 MB, throughput 0.992806
Reading from 24986: heap size 186 MB, throughput 0.997732
Reading from 24985: heap size 400 MB, throughput 0.894524
Reading from 24985: heap size 400 MB, throughput 0.862681
Reading from 24985: heap size 404 MB, throughput 0.846626
Reading from 24985: heap size 406 MB, throughput 0.911498
Reading from 24986: heap size 187 MB, throughput 0.997619
Reading from 24986: heap size 176 MB, throughput 0.936259
Reading from 24985: heap size 413 MB, throughput 0.985392
Reading from 24986: heap size 171 MB, throughput 0.99473
Equal recommendation: 720 MB each
Reading from 24986: heap size 177 MB, throughput 0.992723
Reading from 24986: heap size 182 MB, throughput 0.998067
Reading from 24985: heap size 414 MB, throughput 0.992096
Reading from 24986: heap size 184 MB, throughput 0.997659
Reading from 24986: heap size 186 MB, throughput 0.996781
Reading from 24985: heap size 416 MB, throughput 0.991216
Reading from 24986: heap size 190 MB, throughput 0.997706
Reading from 24985: heap size 417 MB, throughput 0.990177
Reading from 24986: heap size 190 MB, throughput 0.99722
Reading from 24986: heap size 195 MB, throughput 0.998136
Reading from 24985: heap size 416 MB, throughput 0.990141
Reading from 24986: heap size 195 MB, throughput 0.996923
Reading from 24986: heap size 198 MB, throughput 0.99743
Reading from 24985: heap size 418 MB, throughput 0.987549
Equal recommendation: 720 MB each
Reading from 24986: heap size 198 MB, throughput 0.997562
Reading from 24985: heap size 415 MB, throughput 0.988882
Reading from 24986: heap size 202 MB, throughput 0.997661
Reading from 24986: heap size 202 MB, throughput 0.997936
Reading from 24985: heap size 417 MB, throughput 0.988134
Reading from 24986: heap size 206 MB, throughput 0.998175
Reading from 24986: heap size 206 MB, throughput 0.998082
Reading from 24985: heap size 417 MB, throughput 0.987056
Reading from 24986: heap size 209 MB, throughput 0.9966
Reading from 24986: heap size 210 MB, throughput 0.991272
Reading from 24986: heap size 214 MB, throughput 0.997441
Reading from 24985: heap size 418 MB, throughput 0.993547
Reading from 24985: heap size 420 MB, throughput 0.978378
Equal recommendation: 720 MB each
Reading from 24985: heap size 420 MB, throughput 0.876471
Reading from 24986: heap size 214 MB, throughput 0.997862
Reading from 24985: heap size 423 MB, throughput 0.880388
Reading from 24985: heap size 424 MB, throughput 0.975345
Reading from 24986: heap size 219 MB, throughput 0.998387
Reading from 24986: heap size 220 MB, throughput 0.99769
Reading from 24985: heap size 430 MB, throughput 0.993235
Reading from 24986: heap size 224 MB, throughput 0.998381
Reading from 24985: heap size 431 MB, throughput 0.991187
Reading from 24986: heap size 224 MB, throughput 0.997973
Reading from 24986: heap size 229 MB, throughput 0.998491
Reading from 24985: heap size 432 MB, throughput 0.991833
Reading from 24986: heap size 229 MB, throughput 0.997921
Equal recommendation: 720 MB each
Reading from 24985: heap size 433 MB, throughput 0.990855
Reading from 24986: heap size 233 MB, throughput 0.998483
Reading from 24986: heap size 233 MB, throughput 0.998212
Reading from 24985: heap size 431 MB, throughput 0.989864
Reading from 24986: heap size 236 MB, throughput 0.997945
Reading from 24986: heap size 236 MB, throughput 0.997424
Reading from 24985: heap size 433 MB, throughput 0.989769
Reading from 24986: heap size 240 MB, throughput 0.992122
Reading from 24986: heap size 240 MB, throughput 0.997378
Reading from 24985: heap size 431 MB, throughput 0.989675
Reading from 24986: heap size 245 MB, throughput 0.997139
Reading from 24986: heap size 246 MB, throughput 0.998098
Reading from 24985: heap size 432 MB, throughput 0.987499
Equal recommendation: 720 MB each
Reading from 24986: heap size 251 MB, throughput 0.998414
Reading from 24985: heap size 433 MB, throughput 0.990624
Reading from 24986: heap size 252 MB, throughput 0.998054
Reading from 24985: heap size 434 MB, throughput 0.949549
Reading from 24985: heap size 432 MB, throughput 0.901725
Reading from 24985: heap size 434 MB, throughput 0.852905
Reading from 24986: heap size 257 MB, throughput 0.997901
Reading from 24985: heap size 439 MB, throughput 0.992173
Reading from 24986: heap size 257 MB, throughput 0.998077
Reading from 24985: heap size 440 MB, throughput 0.993437
Reading from 24986: heap size 261 MB, throughput 0.997779
Reading from 24986: heap size 261 MB, throughput 0.998029
Reading from 24985: heap size 444 MB, throughput 0.992556
Equal recommendation: 720 MB each
Reading from 24986: heap size 266 MB, throughput 0.997106
Reading from 24986: heap size 266 MB, throughput 0.994422
Reading from 24985: heap size 445 MB, throughput 0.988021
Reading from 24986: heap size 269 MB, throughput 0.99647
Reading from 24985: heap size 444 MB, throughput 0.991884
Reading from 24986: heap size 270 MB, throughput 0.998071
Reading from 24986: heap size 276 MB, throughput 0.998052
Reading from 24985: heap size 446 MB, throughput 0.991291
Reading from 24986: heap size 276 MB, throughput 0.997841
Equal recommendation: 720 MB each
Reading from 24985: heap size 445 MB, throughput 0.991613
Reading from 24986: heap size 281 MB, throughput 0.998591
Reading from 24986: heap size 282 MB, throughput 0.998198
Reading from 24985: heap size 446 MB, throughput 0.990073
Reading from 24986: heap size 287 MB, throughput 0.998427
Reading from 24985: heap size 448 MB, throughput 0.993735
Reading from 24986: heap size 287 MB, throughput 0.998167
Reading from 24985: heap size 448 MB, throughput 0.936453
Reading from 24985: heap size 448 MB, throughput 0.908421
Reading from 24985: heap size 450 MB, throughput 0.964316
Reading from 24986: heap size 291 MB, throughput 0.998496
Reading from 24986: heap size 291 MB, throughput 0.996944
Reading from 24985: heap size 456 MB, throughput 0.990184
Reading from 24986: heap size 295 MB, throughput 0.995526
Equal recommendation: 720 MB each
Reading from 24985: heap size 457 MB, throughput 0.992598
Reading from 24986: heap size 295 MB, throughput 0.997799
Reading from 24986: heap size 302 MB, throughput 0.998285
Reading from 24985: heap size 457 MB, throughput 0.9926
Reading from 24986: heap size 302 MB, throughput 0.998206
Reading from 24985: heap size 459 MB, throughput 0.993349
Reading from 24986: heap size 307 MB, throughput 0.998294
Reading from 24986: heap size 307 MB, throughput 0.997562
Reading from 24985: heap size 455 MB, throughput 0.991577
Equal recommendation: 720 MB each
Reading from 24986: heap size 312 MB, throughput 0.997856
Reading from 24985: heap size 458 MB, throughput 0.991223
Reading from 24986: heap size 312 MB, throughput 0.998173
Reading from 24985: heap size 459 MB, throughput 0.990537
Reading from 24986: heap size 317 MB, throughput 0.998738
Reading from 24986: heap size 317 MB, throughput 0.890317
Reading from 24986: heap size 331 MB, throughput 0.99908
Reading from 24985: heap size 459 MB, throughput 0.994963
Reading from 24985: heap size 460 MB, throughput 0.946522
Reading from 24985: heap size 461 MB, throughput 0.896896
Reading from 24985: heap size 465 MB, throughput 0.979979
Equal recommendation: 720 MB each
Reading from 24986: heap size 332 MB, throughput 0.998738
Reading from 24986: heap size 339 MB, throughput 0.999287
Reading from 24985: heap size 467 MB, throughput 0.892426
Reading from 24986: heap size 340 MB, throughput 0.998514
Reading from 24985: heap size 485 MB, throughput 0.996693
Reading from 24986: heap size 346 MB, throughput 0.999274
Reading from 24985: heap size 486 MB, throughput 0.994961
Reading from 24986: heap size 346 MB, throughput 0.999065
Equal recommendation: 720 MB each
Reading from 24986: heap size 351 MB, throughput 0.999369
Reading from 24985: heap size 491 MB, throughput 0.994746
Reading from 24986: heap size 352 MB, throughput 0.997825
Reading from 24986: heap size 356 MB, throughput 0.997296
Reading from 24985: heap size 492 MB, throughput 0.993799
Reading from 24986: heap size 356 MB, throughput 0.998602
Reading from 24985: heap size 488 MB, throughput 0.992393
Reading from 24986: heap size 363 MB, throughput 0.998851
Equal recommendation: 720 MB each
Reading from 24985: heap size 491 MB, throughput 0.9905
Reading from 24986: heap size 363 MB, throughput 0.998416
Reading from 24985: heap size 491 MB, throughput 0.987447
Reading from 24985: heap size 492 MB, throughput 0.874112
Reading from 24985: heap size 496 MB, throughput 0.933108
Reading from 24986: heap size 369 MB, throughput 0.998705
Reading from 24985: heap size 500 MB, throughput 0.993055
Reading from 24986: heap size 369 MB, throughput 0.997927
Reading from 24985: heap size 497 MB, throughput 0.992329
Reading from 24986: heap size 375 MB, throughput 0.998693
Equal recommendation: 720 MB each
Reading from 24986: heap size 375 MB, throughput 0.996614
Reading from 24985: heap size 501 MB, throughput 0.992858
Reading from 24986: heap size 381 MB, throughput 0.994352
Reading from 24986: heap size 381 MB, throughput 0.997534
Reading from 24985: heap size 502 MB, throughput 0.991909
Reading from 24986: heap size 391 MB, throughput 0.998484
Reading from 24985: heap size 503 MB, throughput 0.990613
Reading from 24986: heap size 392 MB, throughput 0.997815
Equal recommendation: 720 MB each
Reading from 24985: heap size 506 MB, throughput 0.990817
Reading from 24986: heap size 401 MB, throughput 0.998149
Reading from 24985: heap size 508 MB, throughput 0.985705
Reading from 24986: heap size 401 MB, throughput 0.998278
Reading from 24985: heap size 507 MB, throughput 0.984782
Reading from 24985: heap size 513 MB, throughput 0.903691
Reading from 24985: heap size 517 MB, throughput 0.96913
Reading from 24986: heap size 408 MB, throughput 0.99793
Reading from 24986: heap size 409 MB, throughput 0.993681
Reading from 24985: heap size 518 MB, throughput 0.993675
Equal recommendation: 720 MB each
Reading from 24986: heap size 416 MB, throughput 0.998596
Reading from 24985: heap size 520 MB, throughput 0.994796
Reading from 24986: heap size 417 MB, throughput 0.998671
Reading from 24985: heap size 522 MB, throughput 0.991104
Reading from 24986: heap size 425 MB, throughput 0.998196
Reading from 24985: heap size 519 MB, throughput 0.992381
Reading from 24986: heap size 427 MB, throughput 0.998359
Equal recommendation: 720 MB each
Reading from 24985: heap size 522 MB, throughput 0.992089
Reading from 24986: heap size 435 MB, throughput 0.998357
Reading from 24985: heap size 518 MB, throughput 0.990643
Reading from 24986: heap size 435 MB, throughput 0.998256
Reading from 24986: heap size 443 MB, throughput 0.996199
Reading from 24985: heap size 520 MB, throughput 0.993917
Reading from 24985: heap size 519 MB, throughput 0.944651
Reading from 24985: heap size 521 MB, throughput 0.918686
Reading from 24986: heap size 443 MB, throughput 0.998393
Reading from 24985: heap size 526 MB, throughput 0.993816
Equal recommendation: 720 MB each
Reading from 24986: heap size 453 MB, throughput 0.998415
Reading from 24985: heap size 527 MB, throughput 0.994138
Reading from 24986: heap size 454 MB, throughput 0.998585
Reading from 24985: heap size 528 MB, throughput 0.993553
Reading from 24986: heap size 461 MB, throughput 0.998525
Reading from 24985: heap size 530 MB, throughput 0.992712
Equal recommendation: 720 MB each
Reading from 24986: heap size 462 MB, throughput 0.997778
Reading from 24986: heap size 470 MB, throughput 0.995707
Reading from 24985: heap size 528 MB, throughput 0.993217
Reading from 24986: heap size 470 MB, throughput 0.997889
Reading from 24985: heap size 530 MB, throughput 0.993189
Reading from 24986: heap size 482 MB, throughput 0.99868
Reading from 24985: heap size 532 MB, throughput 0.994858
Reading from 24985: heap size 532 MB, throughput 0.934482
Reading from 24985: heap size 532 MB, throughput 0.968499
Equal recommendation: 720 MB each
Reading from 24986: heap size 483 MB, throughput 0.998702
Reading from 24985: heap size 534 MB, throughput 0.995697
Reading from 24986: heap size 491 MB, throughput 0.9987
Reading from 24985: heap size 537 MB, throughput 0.994612
Reading from 24986: heap size 492 MB, throughput 0.998337
Reading from 24986: heap size 499 MB, throughput 0.996606
Equal recommendation: 720 MB each
Reading from 24985: heap size 538 MB, throughput 0.994198
Reading from 24986: heap size 499 MB, throughput 0.998618
Reading from 24985: heap size 535 MB, throughput 0.99343
Reading from 24986: heap size 499 MB, throughput 0.998031
Reading from 24985: heap size 538 MB, throughput 0.993633
Reading from 24986: heap size 499 MB, throughput 0.998666
Equal recommendation: 720 MB each
Reading from 24985: heap size 539 MB, throughput 0.995708
Reading from 24986: heap size 499 MB, throughput 0.998737
Reading from 24985: heap size 540 MB, throughput 0.977857
Reading from 24985: heap size 538 MB, throughput 0.933978
Reading from 24985: heap size 540 MB, throughput 0.994442
Client 24986 died
Clients: 1
Reading from 24985: heap size 546 MB, throughput 0.995237
Recommendation: one client; give it all the memory
Reading from 24985: heap size 547 MB, throughput 0.994333
Reading from 24985: heap size 546 MB, throughput 0.994083
Reading from 24985: heap size 548 MB, throughput 0.992947
Recommendation: one client; give it all the memory
Reading from 24985: heap size 549 MB, throughput 0.990876
Reading from 24985: heap size 549 MB, throughput 0.983514
Reading from 24985: heap size 555 MB, throughput 0.933536
Reading from 24985: heap size 556 MB, throughput 0.994451
Recommendation: one client; give it all the memory
Reading from 24985: heap size 561 MB, throughput 0.99503
Reading from 24985: heap size 562 MB, throughput 0.994416
Reading from 24985: heap size 561 MB, throughput 0.993438
Recommendation: one client; give it all the memory
Reading from 24985: heap size 563 MB, throughput 0.992995
Reading from 24985: heap size 563 MB, throughput 0.995696
Reading from 24985: heap size 564 MB, throughput 0.956466
Reading from 24985: heap size 563 MB, throughput 0.95753
Client 24985 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
