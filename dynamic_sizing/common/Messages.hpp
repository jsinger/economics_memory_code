#ifndef MESSAGES_HPP
#define MESSAGES_HPP

#include <cstdio>
#include <sys/types.h>

/*! \addtogroup common
  @{
*/

namespace economem
{
    //! Message type used with Reading messages.
    enum ReadingType
    {
        READINGTYPE_READING, //!< A normal reading
        READINGTYPE_MINHEAP, //!< Notification of the client's minimum heap size; heapSizeMB used
        READINGTYPE_DEATH //!< Client died; fields unused
    };

    //! A reading sent from a client to the daemon.
    struct Reading
    {
        Reading() : type(READINGTYPE_READING), sender(0), heapSizeMB(0), throughput(0) {}
        Reading(ReadingType type, pid_t sender, unsigned long long heapSizeMB, double throughput) : type(type), sender(sender), heapSizeMB(heapSizeMB), throughput(throughput) {}

        ReadingType type;
        pid_t sender;
        unsigned long long heapSizeMB; //!< in binary MB
        double throughput; //!< fraction of time not spent GCing, between 0 and 1

        void print() const
        {
            printf("Reading from %u: heap size %llu MB, throughput %g\n", (unsigned int)sender, heapSizeMB, throughput);
        }
    };

    //! A recommendation sent from the daemon to a client.
    struct Recommendation
    {
    public:
        Recommendation() : m_HeapSizeMB(0) {}
        Recommendation(unsigned long long heapSizeMB) : m_HeapSizeMB(heapSizeMB) {}

        unsigned long long getHeapSizeMB() const
        {
            return m_HeapSizeMB;
        }

        unsigned long long getHeapSizeBytes() const
        {
            return m_HeapSizeMB * 1024 * 1024;
        }

        void setHeapSizeMB(unsigned long long s)
        {
            m_HeapSizeMB = s;
        }

    private:
        unsigned long long m_HeapSizeMB; // binary MB
    };

    //! Message type used with Command messages
    enum CommandType
    {
        COMMANDTYPE_CLIENTS, //!< Ask how many clients are connected; fields unused
        COMMANDTYPE_PID, //!< Ask for a client's pid; i holds the 1-based index of a client
        COMMANDTYPE_GETMINHEAP, //!< Ask for a client's minheap; i holds the 1-based index of a client
        COMMANDTYPE_SETMINHEAP, //!< Set a client's minheap; i holds the 1-based index of a client, j holds size in binary MB
        COMMANDTYPE_GETTOTALMEM, //!< Ask for the total memory managed by the daemon; fields unused
        COMMANDTYPE_SETTOTALMEM, //!< Set the total memory managed by the daemon; i holds size in binary MB
        COMMANDTYPE_TIMERSTATE, //!< Ask whether the timer is running; fields unused
        COMMANDTYPE_PAUSE, //!< Tell the daemon to pause the timer; fields unused
        COMMANDTYPE_UNPAUSE, //!< Tell the daemon to unpause the timer; fields unused
        COMMANDTYPE_GETTIMER, //!< Ask for the current timer interval; fields unused
        COMMANDTYPE_SETTIMER, //!< Set the timer interval; i holds the new timer value in seconds
        COMMANDTYPE_LOGSREADINGS, //!< Ask whether the daemon logs readings; fields unused
        COMMANDTYPE_SETLOGREADINGS, //!< Tell the daemon whether to log readings; i is 0/1 boolean
        COMMANDTYPE_LOGSFUNCTIONS, //!< Ask whether the daemon logs functions; fields unused
        COMMANDTYPE_SETLOGFUNCTIONS, //!< Tell the daemon whether to log functions; i is 0/1 boolean
        COMMANDTYPE_RECOMMEND, //!< Tell the daemon to make an explicit recommendation; i holds the 1-based index of a client (0 for all), j holds size in binary MB
        COMMANDTYPE_DUMP, //!< Tell the daemon to dump its state to file; fields unused
        COMMANDTYPE_CLEAR, //!< Tell the daemon to discard all state; fields unused
        COMMANDTYPE_EXIT //!< Tell the daemon to exit; fields unused
    };

    //! A command sent from the interactive console (or similar program) to the daemon.
    struct Command
    {
        Command() : type(economem::COMMANDTYPE_CLIENTS), sender(0), i(0), j(0) {}
        Command(CommandType type, pid_t sender, unsigned long long i, unsigned long long j) : type(type), sender(sender), i(i), j(j) {}

        CommandType type;
        pid_t sender;

        unsigned long long i; //!< command-specific; see CommandType for meanings
        unsigned long long j; //!< command-specific; see CommandType for meanings
    };

    //! Message type used with CommandResult messages.
    enum CommandResultType
    {
        CMDRESULTTYPE_CLIENTS, //!< number of clients known by the daemon
        CMDRESULTTYPE_PID, //!< pid of requested client; 0 if invalid client index
        CMDRESULTTYPE_GETMINHEAP, //!< in binary MB, 0 means unknown (or invalid index)
        CMDRESULTTYPE_SETMINHEAP, //!< bool: success
        CMDRESULTTYPE_GETTOTALMEM, //!< total memory managed by the daemon in binary MB
        CMDRESULTTYPE_SETTOTALMEM, //!< bool: success
        CMDRESULTTYPE_TIMERSTATE, //!< bool: paused
        CMDRESULTTYPE_PAUSE, //!< bool: paused (false if already paused)
        CMDRESULTTYPE_UNPAUSE, //!< bool: unpaused (false if already running)
        CMDRESULTTYPE_GETTIMER, //!< timer period in seconds
        CMDRESULTTYPE_SETTIMER, //!< bool: success
        CMDRESULTTYPE_LOGSREADINGS, //!< bool: whether logs readings
        CMDRESULTTYPE_LOGSFUNCTIONS, //!< bool: whether logs functions
        CMDRESULTTYPE_RECOMMEND, //!< (no return value)
        CMDRESULTTYPE_DUMP, //!< bool: success
        CMDRESULTTYPE_CLEAR, //!< (no return value)
        CMDRESULTTYPE_EXIT //!< (no return value)
    };

    //! The response to a command, sent from the daemon to the command's original sender.
    struct CommandResult
    {
        CommandResult() : type(CMDRESULTTYPE_CLIENTS), i(0) {}
        CommandResult(CommandResultType type, unsigned long long i) : type(type), i(i) {}

        CommandResultType type;
        unsigned long long i; //!< command-specific; see CommandResultType for meanings
    };
}

/*! @} */

#endif
