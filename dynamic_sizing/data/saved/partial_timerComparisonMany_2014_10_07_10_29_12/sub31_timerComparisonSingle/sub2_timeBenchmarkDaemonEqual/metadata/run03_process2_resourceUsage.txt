	Command being timed: "cgexec -g memory:economem /home/callum/economics_memory_code/dynamic_sizing/hotspot8/openjdk8/jdk8/build/linux-x86_64-normal-server-release/jdk/bin/java -XX:+EconomemSimpleThroughput -XX:-EconomemVengerovThroughput -XX:EconomemMinHeap=8 -Xms10m -Xmx852m -XX:+DisableExplicitGC -XX:+UseAdaptiveSizePolicyWithSystemGC -jar /home/callum/economics_memory_code/dynamic_sizing/haskell_common/dacapo-9.12-bach.jar --no-pre-iteration-gc --scratch-directory /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub31_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/scratch2 -t 2 -n 32 -s large sunflow"
	User time (seconds): 581.18
	System time (seconds): 1.90
	Percent of CPU this job got: 191%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 5:04.14
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 361456
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 2120
	Minor (reclaiming a frame) page faults: 86692
	Voluntary context switches: 28008
	Involuntary context switches: 62590
	Swaps: 0
	File system inputs: 141304
	File system outputs: 17832
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
