#ifndef SENDSOCK_HPP
#define SENDSOCK_HPP

#include <sys/un.h>
#include "Mutex.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
//! Base class for sending messages.
/*! This class uses an anonymous Unix datagram socket for sending. There is no concept of a connection or a session; messages can be sent anywhere (it is up to subclasses to decide where they will allow you to send). */
    class SendSock
    {
    public:
        virtual ~SendSock();

    protected:
        SendSock();

        bool sendToDaemon(const char* type, const char* daemonName, const void* buf, size_t len);
        bool sendToClient(const char* type, pid_t dest, const void* buf, size_t len);

    private:
        int m_Socket GUARDED_BY(m_Lock);
        economem::Mutex m_Lock;

        bool send(const void* buf, size_t len, const sockaddr_un& addr);

        static bool makeDaemonAddr(sockaddr_un& addr, const char* type, const char* name);
        static bool makeClientAddr(sockaddr_un& addr, pid_t pid, const char* type);
        static bool makeAddr(sockaddr_un& addr, char* sockName);
    };
}

/*! @} */

#endif
