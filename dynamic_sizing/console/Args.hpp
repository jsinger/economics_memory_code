#ifndef ARGS_HPP
#define ARGS_HPP

class Args
{
public:
    static const char* getCommandTarget();
    static bool singleCommandMode();
    static char* getSingleCommand();
    static void parse(int argc, char** argv);

private:
    static void usage();

    static const char* s_CommandTarget;
    static char* s_SingleCommand;
};

#endif
