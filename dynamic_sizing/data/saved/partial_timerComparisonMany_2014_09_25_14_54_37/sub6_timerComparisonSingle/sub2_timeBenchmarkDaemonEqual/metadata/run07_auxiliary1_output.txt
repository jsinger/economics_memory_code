economemd
    total memory: 8922 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub6_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 12291: heap size 9 MB, throughput 0.991734
Clients: 1
Client 12291 has a minimum heap size of 1211 MB
Reading from 12292: heap size 9 MB, throughput 0.990922
Clients: 2
Client 12292 has a minimum heap size of 276 MB
Reading from 12292: heap size 9 MB, throughput 0.977408
Reading from 12291: heap size 9 MB, throughput 0.984411
Reading from 12292: heap size 9 MB, throughput 0.966979
Reading from 12291: heap size 9 MB, throughput 0.963311
Reading from 12292: heap size 9 MB, throughput 0.956535
Reading from 12291: heap size 9 MB, throughput 0.957579
Reading from 12292: heap size 11 MB, throughput 0.970595
Reading from 12291: heap size 11 MB, throughput 0.968681
Reading from 12292: heap size 11 MB, throughput 0.971041
Reading from 12291: heap size 11 MB, throughput 0.976026
Reading from 12292: heap size 17 MB, throughput 0.89358
Reading from 12291: heap size 17 MB, throughput 0.96111
Reading from 12292: heap size 17 MB, throughput 0.685157
Reading from 12291: heap size 17 MB, throughput 0.560457
Reading from 12292: heap size 30 MB, throughput 0.853757
Reading from 12291: heap size 30 MB, throughput 0.943809
Reading from 12291: heap size 31 MB, throughput 0.876586
Reading from 12292: heap size 31 MB, throughput 0.917602
Reading from 12292: heap size 35 MB, throughput 0.588902
Reading from 12291: heap size 33 MB, throughput 0.523041
Reading from 12292: heap size 46 MB, throughput 0.890486
Reading from 12291: heap size 46 MB, throughput 0.930039
Reading from 12291: heap size 48 MB, throughput 0.874726
Reading from 12292: heap size 49 MB, throughput 0.403728
Reading from 12292: heap size 64 MB, throughput 0.660405
Reading from 12292: heap size 72 MB, throughput 0.80874
Reading from 12291: heap size 49 MB, throughput 0.376786
Reading from 12291: heap size 67 MB, throughput 0.742645
Reading from 12291: heap size 68 MB, throughput 0.646618
Reading from 12291: heap size 70 MB, throughput 0.74259
Reading from 12292: heap size 73 MB, throughput 0.18799
Reading from 12292: heap size 106 MB, throughput 0.79976
Reading from 12291: heap size 74 MB, throughput 0.137866
Reading from 12291: heap size 106 MB, throughput 0.700901
Reading from 12291: heap size 106 MB, throughput 0.859163
Reading from 12292: heap size 106 MB, throughput 0.217905
Reading from 12291: heap size 108 MB, throughput 0.766429
Reading from 12292: heap size 139 MB, throughput 0.598934
Reading from 12292: heap size 139 MB, throughput 0.805059
Reading from 12292: heap size 142 MB, throughput 0.717808
Reading from 12291: heap size 110 MB, throughput 0.215231
Reading from 12291: heap size 140 MB, throughput 0.740044
Reading from 12291: heap size 142 MB, throughput 0.746982
Reading from 12291: heap size 146 MB, throughput 0.719015
Reading from 12291: heap size 149 MB, throughput 0.681099
Reading from 12292: heap size 147 MB, throughput 0.215498
Reading from 12292: heap size 180 MB, throughput 0.639206
Reading from 12292: heap size 185 MB, throughput 0.78773
Reading from 12292: heap size 187 MB, throughput 0.77838
Reading from 12291: heap size 156 MB, throughput 0.19081
Reading from 12291: heap size 189 MB, throughput 0.557424
Reading from 12291: heap size 193 MB, throughput 0.573706
Reading from 12291: heap size 198 MB, throughput 0.577028
Reading from 12291: heap size 202 MB, throughput 0.690234
Reading from 12292: heap size 190 MB, throughput 0.276203
Reading from 12291: heap size 211 MB, throughput 0.156009
Reading from 12291: heap size 252 MB, throughput 0.582304
Reading from 12292: heap size 237 MB, throughput 0.947606
Reading from 12291: heap size 256 MB, throughput 0.714284
Reading from 12291: heap size 258 MB, throughput 0.747976
Reading from 12292: heap size 239 MB, throughput 0.904866
Reading from 12291: heap size 261 MB, throughput 0.676741
Reading from 12292: heap size 249 MB, throughput 0.879687
Reading from 12291: heap size 264 MB, throughput 0.619323
Reading from 12292: heap size 250 MB, throughput 0.859707
Reading from 12291: heap size 270 MB, throughput 0.583332
Reading from 12291: heap size 277 MB, throughput 0.460563
Reading from 12292: heap size 255 MB, throughput 0.820825
Reading from 12292: heap size 255 MB, throughput 0.88917
Reading from 12292: heap size 256 MB, throughput 0.766579
Reading from 12292: heap size 260 MB, throughput 0.727005
Reading from 12291: heap size 284 MB, throughput 0.102312
Reading from 12292: heap size 266 MB, throughput 0.837222
Reading from 12291: heap size 335 MB, throughput 0.521248
Reading from 12292: heap size 269 MB, throughput 0.846032
Reading from 12291: heap size 336 MB, throughput 0.677593
Reading from 12292: heap size 275 MB, throughput 0.895515
Reading from 12291: heap size 341 MB, throughput 0.134598
Reading from 12292: heap size 276 MB, throughput 0.753378
Reading from 12291: heap size 388 MB, throughput 0.626616
Reading from 12292: heap size 283 MB, throughput 0.780056
Reading from 12291: heap size 381 MB, throughput 0.725178
Reading from 12292: heap size 286 MB, throughput 0.776153
Reading from 12291: heap size 387 MB, throughput 0.708703
Reading from 12292: heap size 294 MB, throughput 0.879908
Reading from 12291: heap size 388 MB, throughput 0.512652
Reading from 12291: heap size 390 MB, throughput 0.68393
Reading from 12291: heap size 392 MB, throughput 0.572043
Reading from 12291: heap size 400 MB, throughput 0.463596
Reading from 12291: heap size 404 MB, throughput 0.447036
Reading from 12292: heap size 295 MB, throughput 0.181303
Reading from 12291: heap size 415 MB, throughput 0.527384
Reading from 12292: heap size 343 MB, throughput 0.754484
Reading from 12292: heap size 344 MB, throughput 0.564342
Reading from 12292: heap size 349 MB, throughput 0.73762
Reading from 12292: heap size 348 MB, throughput 0.638389
Reading from 12292: heap size 355 MB, throughput 0.613982
Reading from 12291: heap size 424 MB, throughput 0.0871265
Equal recommendation: 4461 MB each
Reading from 12291: heap size 483 MB, throughput 0.326343
Reading from 12291: heap size 486 MB, throughput 0.495641
Reading from 12292: heap size 355 MB, throughput 0.964137
Reading from 12291: heap size 488 MB, throughput 0.091028
Reading from 12291: heap size 542 MB, throughput 0.401779
Reading from 12291: heap size 547 MB, throughput 0.56301
Reading from 12291: heap size 539 MB, throughput 0.632799
Reading from 12291: heap size 544 MB, throughput 0.608529
Reading from 12292: heap size 363 MB, throughput 0.962961
Reading from 12291: heap size 535 MB, throughput 0.121924
Reading from 12291: heap size 604 MB, throughput 0.529836
Reading from 12291: heap size 601 MB, throughput 0.665303
Reading from 12291: heap size 603 MB, throughput 0.554502
Reading from 12291: heap size 605 MB, throughput 0.613274
Reading from 12291: heap size 613 MB, throughput 0.596973
Reading from 12291: heap size 616 MB, throughput 0.50264
Reading from 12292: heap size 364 MB, throughput 0.961536
Reading from 12291: heap size 630 MB, throughput 0.430202
Reading from 12291: heap size 637 MB, throughput 0.787958
Reading from 12292: heap size 367 MB, throughput 0.948592
Reading from 12291: heap size 648 MB, throughput 0.824034
Reading from 12291: heap size 650 MB, throughput 0.773036
Reading from 12291: heap size 666 MB, throughput 0.728399
Reading from 12292: heap size 369 MB, throughput 0.961695
Reading from 12291: heap size 670 MB, throughput 0.747897
Reading from 12291: heap size 686 MB, throughput 0.0379311
Reading from 12291: heap size 759 MB, throughput 0.21935
Reading from 12291: heap size 664 MB, throughput 0.257131
Reading from 12292: heap size 371 MB, throughput 0.981927
Reading from 12291: heap size 749 MB, throughput 0.0976611
Reading from 12291: heap size 717 MB, throughput 0.241924
Reading from 12291: heap size 829 MB, throughput 0.32391
Reading from 12291: heap size 829 MB, throughput 0.394103
Equal recommendation: 4461 MB each
Reading from 12291: heap size 830 MB, throughput 0.405855
Reading from 12292: heap size 372 MB, throughput 0.959267
Reading from 12291: heap size 831 MB, throughput 0.0320116
Reading from 12291: heap size 917 MB, throughput 0.25009
Reading from 12291: heap size 918 MB, throughput 0.75176
Reading from 12291: heap size 925 MB, throughput 0.894902
Reading from 12291: heap size 926 MB, throughput 0.942975
Reading from 12291: heap size 914 MB, throughput 0.656054
Reading from 12291: heap size 708 MB, throughput 0.76639
Reading from 12291: heap size 910 MB, throughput 0.770702
Reading from 12291: heap size 710 MB, throughput 0.791917
Reading from 12291: heap size 897 MB, throughput 0.717874
Reading from 12292: heap size 371 MB, throughput 0.967982
Reading from 12291: heap size 705 MB, throughput 0.927723
Reading from 12291: heap size 885 MB, throughput 0.855893
Reading from 12291: heap size 714 MB, throughput 0.926898
Reading from 12291: heap size 870 MB, throughput 0.803647
Reading from 12291: heap size 720 MB, throughput 0.907558
Reading from 12291: heap size 854 MB, throughput 0.843176
Reading from 12291: heap size 728 MB, throughput 0.869663
Reading from 12291: heap size 842 MB, throughput 0.90698
Reading from 12291: heap size 762 MB, throughput 0.777584
Reading from 12292: heap size 373 MB, throughput 0.96043
Reading from 12291: heap size 846 MB, throughput 0.629392
Reading from 12291: heap size 848 MB, throughput 0.748124
Reading from 12291: heap size 840 MB, throughput 0.739628
Reading from 12291: heap size 844 MB, throughput 0.787386
Reading from 12291: heap size 837 MB, throughput 0.764084
Reading from 12291: heap size 841 MB, throughput 0.764688
Reading from 12291: heap size 835 MB, throughput 0.774057
Reading from 12291: heap size 839 MB, throughput 0.7838
Reading from 12291: heap size 830 MB, throughput 0.770693
Reading from 12291: heap size 835 MB, throughput 0.798694
Reading from 12291: heap size 824 MB, throughput 0.799265
Reading from 12291: heap size 830 MB, throughput 0.816391
Reading from 12292: heap size 376 MB, throughput 0.976111
Reading from 12291: heap size 825 MB, throughput 0.974248
Reading from 12292: heap size 376 MB, throughput 0.973954
Reading from 12291: heap size 828 MB, throughput 0.962696
Reading from 12291: heap size 829 MB, throughput 0.718213
Reading from 12291: heap size 835 MB, throughput 0.743422
Reading from 12291: heap size 842 MB, throughput 0.730755
Reading from 12291: heap size 846 MB, throughput 0.74808
Reading from 12291: heap size 853 MB, throughput 0.75281
Reading from 12291: heap size 855 MB, throughput 0.735207
Reading from 12291: heap size 863 MB, throughput 0.772456
Reading from 12292: heap size 379 MB, throughput 0.957279
Reading from 12291: heap size 863 MB, throughput 0.73965
Reading from 12291: heap size 871 MB, throughput 0.746249
Reading from 12291: heap size 871 MB, throughput 0.772755
Reading from 12291: heap size 876 MB, throughput 0.877852
Reading from 12291: heap size 877 MB, throughput 0.831587
Reading from 12291: heap size 876 MB, throughput 0.888911
Equal recommendation: 4461 MB each
Reading from 12292: heap size 381 MB, throughput 0.972067
Reading from 12291: heap size 880 MB, throughput 0.0632286
Reading from 12291: heap size 965 MB, throughput 0.573465
Reading from 12291: heap size 966 MB, throughput 0.666797
Reading from 12291: heap size 974 MB, throughput 0.673572
Reading from 12291: heap size 979 MB, throughput 0.634397
Reading from 12291: heap size 990 MB, throughput 0.698267
Reading from 12291: heap size 995 MB, throughput 0.682786
Reading from 12291: heap size 1007 MB, throughput 0.655628
Reading from 12292: heap size 384 MB, throughput 0.985097
Reading from 12291: heap size 1011 MB, throughput 0.668023
Reading from 12291: heap size 1025 MB, throughput 0.658384
Reading from 12292: heap size 386 MB, throughput 0.794174
Reading from 12292: heap size 383 MB, throughput 0.770943
Reading from 12291: heap size 1028 MB, throughput 0.642367
Reading from 12292: heap size 388 MB, throughput 0.837812
Reading from 12291: heap size 1043 MB, throughput 0.615876
Reading from 12292: heap size 396 MB, throughput 0.775399
Reading from 12291: heap size 1047 MB, throughput 0.71239
Reading from 12292: heap size 399 MB, throughput 0.974559
Reading from 12291: heap size 1064 MB, throughput 0.960068
Reading from 12292: heap size 406 MB, throughput 0.97643
Reading from 12291: heap size 1067 MB, throughput 0.954519
Reading from 12291: heap size 1079 MB, throughput 0.944259
Reading from 12292: heap size 407 MB, throughput 0.9832
Reading from 12291: heap size 1081 MB, throughput 0.955734
Equal recommendation: 4461 MB each
Reading from 12292: heap size 408 MB, throughput 0.98695
Reading from 12291: heap size 1079 MB, throughput 0.948322
Reading from 12292: heap size 411 MB, throughput 0.986023
Reading from 12291: heap size 1085 MB, throughput 0.951798
Reading from 12291: heap size 1075 MB, throughput 0.950403
Reading from 12292: heap size 410 MB, throughput 0.985562
Reading from 12291: heap size 1082 MB, throughput 0.945998
Reading from 12292: heap size 412 MB, throughput 0.986261
Reading from 12291: heap size 1071 MB, throughput 0.953415
Reading from 12292: heap size 409 MB, throughput 0.984966
Reading from 12291: heap size 1078 MB, throughput 0.949179
Equal recommendation: 4461 MB each
Reading from 12292: heap size 412 MB, throughput 0.981582
Reading from 12291: heap size 1074 MB, throughput 0.952135
Reading from 12292: heap size 410 MB, throughput 0.979955
Reading from 12291: heap size 1077 MB, throughput 0.952604
Reading from 12292: heap size 411 MB, throughput 0.990747
Reading from 12292: heap size 413 MB, throughput 0.908728
Reading from 12291: heap size 1081 MB, throughput 0.950151
Reading from 12292: heap size 413 MB, throughput 0.837226
Reading from 12292: heap size 417 MB, throughput 0.844217
Reading from 12291: heap size 1081 MB, throughput 0.949277
Reading from 12292: heap size 418 MB, throughput 0.984097
Reading from 12291: heap size 1086 MB, throughput 0.951511
Reading from 12292: heap size 424 MB, throughput 0.986458
Equal recommendation: 4461 MB each
Reading from 12291: heap size 1088 MB, throughput 0.949607
Reading from 12292: heap size 425 MB, throughput 0.989177
Reading from 12291: heap size 1094 MB, throughput 0.950002
Reading from 12292: heap size 428 MB, throughput 0.98803
Reading from 12291: heap size 1097 MB, throughput 0.957358
Reading from 12292: heap size 429 MB, throughput 0.987817
Reading from 12291: heap size 1103 MB, throughput 0.956989
Reading from 12292: heap size 428 MB, throughput 0.985852
Reading from 12291: heap size 1105 MB, throughput 0.950411
Reading from 12292: heap size 430 MB, throughput 0.987213
Equal recommendation: 4461 MB each
Reading from 12292: heap size 429 MB, throughput 0.977466
Reading from 12291: heap size 1112 MB, throughput 0.480181
Reading from 12291: heap size 1202 MB, throughput 0.92408
Reading from 12292: heap size 430 MB, throughput 0.990646
Reading from 12292: heap size 432 MB, throughput 0.979529
Reading from 12292: heap size 433 MB, throughput 0.877822
Reading from 12292: heap size 436 MB, throughput 0.868793
Reading from 12291: heap size 1207 MB, throughput 0.969967
Reading from 12292: heap size 437 MB, throughput 0.9863
Reading from 12291: heap size 1209 MB, throughput 0.977912
Equal recommendation: 4461 MB each
Reading from 12292: heap size 443 MB, throughput 0.992064
Reading from 12291: heap size 1213 MB, throughput 0.967917
Reading from 12292: heap size 444 MB, throughput 0.989712
Reading from 12291: heap size 1214 MB, throughput 0.963683
Reading from 12291: heap size 1210 MB, throughput 0.954836
Reading from 12292: heap size 445 MB, throughput 0.99014
Reading from 12291: heap size 1214 MB, throughput 0.969981
Reading from 12292: heap size 447 MB, throughput 0.989474
Reading from 12291: heap size 1206 MB, throughput 0.964178
Reading from 12292: heap size 445 MB, throughput 0.987901
Equal recommendation: 4461 MB each
Reading from 12291: heap size 1211 MB, throughput 0.958057
Reading from 12292: heap size 447 MB, throughput 0.986442
Reading from 12291: heap size 1212 MB, throughput 0.962459
Reading from 12292: heap size 447 MB, throughput 0.986239
Reading from 12291: heap size 1213 MB, throughput 0.960115
Reading from 12292: heap size 448 MB, throughput 0.990815
Reading from 12292: heap size 451 MB, throughput 0.907397
Reading from 12292: heap size 451 MB, throughput 0.868307
Reading from 12291: heap size 1218 MB, throughput 0.954024
Reading from 12292: heap size 457 MB, throughput 0.784024
Equal recommendation: 4461 MB each
Reading from 12291: heap size 1221 MB, throughput 0.959316
Reading from 12292: heap size 481 MB, throughput 0.995635
Reading from 12291: heap size 1227 MB, throughput 0.959818
Reading from 12292: heap size 485 MB, throughput 0.994454
Reading from 12291: heap size 1232 MB, throughput 0.949699
Reading from 12292: heap size 487 MB, throughput 0.991622
Reading from 12291: heap size 1238 MB, throughput 0.955435
Equal recommendation: 4461 MB each
Reading from 12292: heap size 489 MB, throughput 0.99119
Reading from 12291: heap size 1245 MB, throughput 0.954418
Reading from 12291: heap size 1253 MB, throughput 0.957784
Reading from 12292: heap size 491 MB, throughput 0.991238
Reading from 12291: heap size 1258 MB, throughput 0.958084
Reading from 12292: heap size 488 MB, throughput 0.987716
Reading from 12291: heap size 1267 MB, throughput 0.951612
Reading from 12292: heap size 490 MB, throughput 0.992075
Reading from 12292: heap size 489 MB, throughput 0.91928
Reading from 12292: heap size 491 MB, throughput 0.860076
Equal recommendation: 4461 MB each
Reading from 12292: heap size 498 MB, throughput 0.979341
Reading from 12291: heap size 1269 MB, throughput 0.540784
Reading from 12292: heap size 503 MB, throughput 0.991186
Reading from 12291: heap size 1381 MB, throughput 0.925564
Reading from 12292: heap size 501 MB, throughput 0.989843
Reading from 12291: heap size 1383 MB, throughput 0.967177
Equal recommendation: 4461 MB each
Reading from 12292: heap size 505 MB, throughput 0.988546
Reading from 12291: heap size 1393 MB, throughput 0.966741
Reading from 12292: heap size 509 MB, throughput 0.989525
Reading from 12292: heap size 509 MB, throughput 0.98726
Reading from 12292: heap size 513 MB, throughput 0.984146
Equal recommendation: 4461 MB each
Reading from 12292: heap size 515 MB, throughput 0.985031
Reading from 12292: heap size 519 MB, throughput 0.880792
Reading from 12292: heap size 522 MB, throughput 0.963392
Reading from 12291: heap size 1393 MB, throughput 0.988115
Reading from 12292: heap size 530 MB, throughput 0.991414
Reading from 12292: heap size 530 MB, throughput 0.99081
Equal recommendation: 4461 MB each
Reading from 12292: heap size 531 MB, throughput 0.990861
Reading from 12292: heap size 533 MB, throughput 0.988975
Reading from 12292: heap size 531 MB, throughput 0.988793
Reading from 12291: heap size 1388 MB, throughput 0.99017
Equal recommendation: 4461 MB each
Reading from 12292: heap size 533 MB, throughput 0.987204
Reading from 12292: heap size 533 MB, throughput 0.984895
Reading from 12292: heap size 536 MB, throughput 0.90915
Reading from 12292: heap size 540 MB, throughput 0.984596
Reading from 12291: heap size 1400 MB, throughput 0.979529
Reading from 12291: heap size 1398 MB, throughput 0.801504
Reading from 12291: heap size 1419 MB, throughput 0.710965
Reading from 12292: heap size 540 MB, throughput 0.988621
Reading from 12291: heap size 1412 MB, throughput 0.63729
Reading from 12291: heap size 1451 MB, throughput 0.663104
Reading from 12291: heap size 1479 MB, throughput 0.625491
Reading from 12291: heap size 1495 MB, throughput 0.664111
Reading from 12291: heap size 1528 MB, throughput 0.648896
Reading from 12291: heap size 1536 MB, throughput 0.913289
Reading from 12292: heap size 543 MB, throughput 0.988925
Equal recommendation: 4461 MB each
Reading from 12291: heap size 1544 MB, throughput 0.957471
Reading from 12292: heap size 545 MB, throughput 0.990545
Reading from 12291: heap size 1559 MB, throughput 0.955673
Reading from 12292: heap size 543 MB, throughput 0.990121
Reading from 12291: heap size 1548 MB, throughput 0.961953
Reading from 12292: heap size 546 MB, throughput 0.989572
Equal recommendation: 4461 MB each
Reading from 12291: heap size 1563 MB, throughput 0.957293
Reading from 12292: heap size 547 MB, throughput 0.992374
Reading from 12292: heap size 547 MB, throughput 0.90925
Reading from 12292: heap size 547 MB, throughput 0.97619
Reading from 12291: heap size 1550 MB, throughput 0.750218
Reading from 12291: heap size 1542 MB, throughput 0.989724
Reading from 12292: heap size 549 MB, throughput 0.993029
Reading from 12291: heap size 1543 MB, throughput 0.987411
Equal recommendation: 4461 MB each
Reading from 12292: heap size 552 MB, throughput 0.99227
Reading from 12291: heap size 1557 MB, throughput 0.984551
Reading from 12292: heap size 554 MB, throughput 0.991602
Reading from 12291: heap size 1580 MB, throughput 0.981048
Reading from 12292: heap size 552 MB, throughput 0.991092
Reading from 12291: heap size 1581 MB, throughput 0.978383
Equal recommendation: 4461 MB each
Reading from 12292: heap size 555 MB, throughput 0.989087
Reading from 12292: heap size 557 MB, throughput 0.990569
Reading from 12291: heap size 1576 MB, throughput 0.97621
Reading from 12292: heap size 558 MB, throughput 0.915734
Reading from 12292: heap size 561 MB, throughput 0.991153
Reading from 12291: heap size 1584 MB, throughput 0.974158
Reading from 12292: heap size 563 MB, throughput 0.992957
Equal recommendation: 4461 MB each
Reading from 12291: heap size 1562 MB, throughput 0.971018
Reading from 12292: heap size 566 MB, throughput 0.991769
Reading from 12291: heap size 1412 MB, throughput 0.975063
Reading from 12292: heap size 567 MB, throughput 0.991998
Reading from 12291: heap size 1546 MB, throughput 0.975928
Reading from 12292: heap size 566 MB, throughput 0.990553
Equal recommendation: 4461 MB each
Reading from 12291: heap size 1560 MB, throughput 0.964862
Reading from 12292: heap size 568 MB, throughput 0.995248
Reading from 12292: heap size 570 MB, throughput 0.935861
Reading from 12292: heap size 571 MB, throughput 0.976748
Reading from 12291: heap size 1558 MB, throughput 0.967391
Reading from 12292: heap size 578 MB, throughput 0.993381
Reading from 12291: heap size 1560 MB, throughput 0.963496
Equal recommendation: 4461 MB each
Reading from 12292: heap size 579 MB, throughput 0.992383
Reading from 12291: heap size 1570 MB, throughput 0.965491
Reading from 12292: heap size 579 MB, throughput 0.991847
Reading from 12291: heap size 1574 MB, throughput 0.965518
Reading from 12292: heap size 581 MB, throughput 0.99034
Reading from 12291: heap size 1585 MB, throughput 0.963547
Equal recommendation: 4461 MB each
Reading from 12292: heap size 583 MB, throughput 0.99322
Reading from 12291: heap size 1595 MB, throughput 0.954452
Reading from 12292: heap size 583 MB, throughput 0.968982
Reading from 12292: heap size 584 MB, throughput 0.973183
Reading from 12291: heap size 1610 MB, throughput 0.955309
Reading from 12292: heap size 586 MB, throughput 0.993822
Equal recommendation: 4461 MB each
Reading from 12291: heap size 1617 MB, throughput 0.959349
Reading from 12292: heap size 588 MB, throughput 0.993145
Reading from 12291: heap size 1634 MB, throughput 0.96219
Reading from 12292: heap size 590 MB, throughput 0.992123
Reading from 12291: heap size 1638 MB, throughput 0.95464
Reading from 12292: heap size 590 MB, throughput 0.990965
Equal recommendation: 4461 MB each
Reading from 12291: heap size 1656 MB, throughput 0.95693
Reading from 12292: heap size 591 MB, throughput 0.995476
Reading from 12292: heap size 593 MB, throughput 0.939264
Reading from 12292: heap size 594 MB, throughput 0.987844
Equal recommendation: 4461 MB each
Reading from 12292: heap size 601 MB, throughput 0.993936
Reading from 12292: heap size 602 MB, throughput 0.993182
Reading from 12292: heap size 601 MB, throughput 0.991795
Reading from 12291: heap size 1659 MB, throughput 0.983432
Equal recommendation: 4461 MB each
Reading from 12292: heap size 603 MB, throughput 0.990923
Reading from 12292: heap size 605 MB, throughput 0.988145
Reading from 12292: heap size 606 MB, throughput 0.922425
Client 12292 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 12291: heap size 1643 MB, throughput 0.993868
Reading from 12291: heap size 1679 MB, throughput 0.886991
Reading from 12291: heap size 1664 MB, throughput 0.786135
Reading from 12291: heap size 1685 MB, throughput 0.774487
Reading from 12291: heap size 1721 MB, throughput 0.800359
Client 12291 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
