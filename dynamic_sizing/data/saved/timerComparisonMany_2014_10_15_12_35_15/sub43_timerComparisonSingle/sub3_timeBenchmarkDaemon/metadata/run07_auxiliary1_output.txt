economemd
    total memory: 2208 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub43_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 18564: heap size 9 MB, throughput 0.99032
Clients: 1
Client 18564 has a minimum heap size of 276 MB
Reading from 18563: heap size 9 MB, throughput 0.991725
Clients: 2
Client 18563 has a minimum heap size of 276 MB
Reading from 18563: heap size 9 MB, throughput 0.988901
Reading from 18564: heap size 9 MB, throughput 0.983034
Reading from 18563: heap size 9 MB, throughput 0.981706
Reading from 18564: heap size 9 MB, throughput 0.978957
Reading from 18563: heap size 9 MB, throughput 0.975508
Reading from 18564: heap size 9 MB, throughput 0.962032
Reading from 18564: heap size 11 MB, throughput 0.977642
Reading from 18563: heap size 11 MB, throughput 0.973499
Reading from 18564: heap size 11 MB, throughput 0.982386
Reading from 18563: heap size 11 MB, throughput 0.967224
Reading from 18563: heap size 17 MB, throughput 0.932357
Reading from 18564: heap size 17 MB, throughput 0.942861
Reading from 18564: heap size 17 MB, throughput 0.889695
Reading from 18563: heap size 17 MB, throughput 0.894995
Reading from 18563: heap size 30 MB, throughput 0.703532
Reading from 18564: heap size 30 MB, throughput 0.845484
Reading from 18564: heap size 31 MB, throughput 0.529251
Reading from 18563: heap size 31 MB, throughput 0.513643
Reading from 18564: heap size 35 MB, throughput 0.684073
Reading from 18563: heap size 36 MB, throughput 0.677697
Reading from 18564: heap size 47 MB, throughput 0.457315
Reading from 18563: heap size 48 MB, throughput 0.48806
Reading from 18564: heap size 50 MB, throughput 0.430983
Reading from 18563: heap size 51 MB, throughput 0.475126
Reading from 18564: heap size 52 MB, throughput 0.471456
Reading from 18563: heap size 67 MB, throughput 0.393907
Reading from 18564: heap size 77 MB, throughput 0.462835
Reading from 18563: heap size 73 MB, throughput 0.318112
Reading from 18564: heap size 77 MB, throughput 0.358534
Reading from 18563: heap size 74 MB, throughput 0.4658
Reading from 18563: heap size 104 MB, throughput 0.532429
Reading from 18564: heap size 104 MB, throughput 0.698137
Reading from 18564: heap size 105 MB, throughput 0.535407
Reading from 18564: heap size 106 MB, throughput 0.358289
Reading from 18563: heap size 105 MB, throughput 0.38337
Reading from 18564: heap size 109 MB, throughput 0.266904
Reading from 18563: heap size 138 MB, throughput 0.739111
Reading from 18563: heap size 138 MB, throughput 0.64178
Reading from 18563: heap size 139 MB, throughput 0.392181
Reading from 18563: heap size 143 MB, throughput 0.346201
Reading from 18564: heap size 112 MB, throughput 0.548333
Reading from 18564: heap size 150 MB, throughput 0.378725
Reading from 18564: heap size 153 MB, throughput 0.338082
Reading from 18564: heap size 155 MB, throughput 0.708878
Reading from 18563: heap size 146 MB, throughput 0.473291
Reading from 18564: heap size 164 MB, throughput 0.767145
Reading from 18563: heap size 183 MB, throughput 0.350756
Reading from 18563: heap size 189 MB, throughput 0.227027
Reading from 18563: heap size 192 MB, throughput 0.683987
Reading from 18564: heap size 166 MB, throughput 0.475802
Reading from 18564: heap size 210 MB, throughput 0.718607
Reading from 18563: heap size 201 MB, throughput 0.79785
Reading from 18564: heap size 212 MB, throughput 0.808557
Reading from 18564: heap size 215 MB, throughput 0.89461
Reading from 18563: heap size 204 MB, throughput 0.604399
Reading from 18564: heap size 222 MB, throughput 0.862723
Reading from 18564: heap size 227 MB, throughput 0.792447
Reading from 18563: heap size 255 MB, throughput 0.856042
Reading from 18564: heap size 232 MB, throughput 0.830487
Reading from 18564: heap size 237 MB, throughput 0.73811
Reading from 18563: heap size 259 MB, throughput 0.853976
Reading from 18563: heap size 263 MB, throughput 0.8073
Reading from 18564: heap size 241 MB, throughput 0.763572
Reading from 18564: heap size 247 MB, throughput 0.813675
Reading from 18563: heap size 266 MB, throughput 0.848099
Reading from 18564: heap size 249 MB, throughput 0.767105
Reading from 18563: heap size 273 MB, throughput 0.836695
Reading from 18564: heap size 250 MB, throughput 0.762397
Reading from 18564: heap size 256 MB, throughput 0.711128
Reading from 18563: heap size 275 MB, throughput 0.798435
Reading from 18564: heap size 262 MB, throughput 0.721774
Reading from 18563: heap size 276 MB, throughput 0.799591
Reading from 18564: heap size 265 MB, throughput 0.723402
Reading from 18563: heap size 281 MB, throughput 0.798675
Reading from 18563: heap size 288 MB, throughput 0.721986
Reading from 18563: heap size 291 MB, throughput 0.735832
Reading from 18564: heap size 271 MB, throughput 0.690503
Reading from 18563: heap size 294 MB, throughput 0.887299
Reading from 18563: heap size 297 MB, throughput 0.838355
Reading from 18564: heap size 313 MB, throughput 0.848448
Reading from 18563: heap size 304 MB, throughput 0.774361
Reading from 18564: heap size 310 MB, throughput 0.80405
Reading from 18563: heap size 308 MB, throughput 0.74787
Reading from 18564: heap size 313 MB, throughput 0.733879
Reading from 18564: heap size 312 MB, throughput 0.754757
Reading from 18564: heap size 314 MB, throughput 0.684685
Reading from 18564: heap size 315 MB, throughput 0.789923
Reading from 18564: heap size 317 MB, throughput 0.841862
Reading from 18563: heap size 307 MB, throughput 0.667433
Reading from 18564: heap size 321 MB, throughput 0.794989
Reading from 18564: heap size 323 MB, throughput 0.665879
Reading from 18563: heap size 358 MB, throughput 0.808887
Reading from 18563: heap size 365 MB, throughput 0.74654
Reading from 18563: heap size 365 MB, throughput 0.687738
Reading from 18563: heap size 364 MB, throughput 0.684568
Reading from 18563: heap size 367 MB, throughput 0.692814
Reading from 18563: heap size 371 MB, throughput 0.686646
Reading from 18564: heap size 330 MB, throughput 0.587972
Numeric result:
Recommendation: 2 clients, utility 0.467041:
    h1: 973.208 MB (U(h) = 0.580134*h^0.0251608)
    h2: 1234.79 MB (U(h) = 0.539453*h^0.0319213)
Recommendation: 2 clients, utility 0.467041:
    h1: 973.248 MB (U(h) = 0.580134*h^0.0251608)
    h2: 1234.75 MB (U(h) = 0.539453*h^0.0319213)
Reading from 18564: heap size 374 MB, throughput 0.617856
Reading from 18564: heap size 380 MB, throughput 0.681679
Reading from 18564: heap size 381 MB, throughput 0.928096
Reading from 18563: heap size 373 MB, throughput 0.930588
Reading from 18564: heap size 385 MB, throughput 0.963186
Reading from 18563: heap size 381 MB, throughput 0.964239
Reading from 18564: heap size 388 MB, throughput 0.975423
Reading from 18563: heap size 382 MB, throughput 0.975783
Reading from 18564: heap size 387 MB, throughput 0.979325
Reading from 18563: heap size 384 MB, throughput 0.978491
Reading from 18564: heap size 390 MB, throughput 0.976129
Reading from 18563: heap size 387 MB, throughput 0.976118
Reading from 18564: heap size 388 MB, throughput 0.9785
Numeric result:
Recommendation: 2 clients, utility 0.579083:
    h1: 1007.62 MB (U(h) = 0.506149*h^0.0587675)
    h2: 1200.38 MB (U(h) = 0.46385*h^0.0700119)
Recommendation: 2 clients, utility 0.579083:
    h1: 1007.6 MB (U(h) = 0.506149*h^0.0587675)
    h2: 1200.4 MB (U(h) = 0.46385*h^0.0700119)
Reading from 18563: heap size 387 MB, throughput 0.973291
Reading from 18564: heap size 391 MB, throughput 0.970529
Reading from 18564: heap size 391 MB, throughput 0.968635
Reading from 18563: heap size 390 MB, throughput 0.974948
Reading from 18564: heap size 392 MB, throughput 0.974195
Reading from 18563: heap size 388 MB, throughput 0.976408
Reading from 18563: heap size 391 MB, throughput 0.978833
Reading from 18564: heap size 395 MB, throughput 0.977276
Reading from 18564: heap size 397 MB, throughput 0.976731
Reading from 18563: heap size 394 MB, throughput 0.980907
Reading from 18563: heap size 395 MB, throughput 0.9778
Reading from 18564: heap size 397 MB, throughput 0.978594
Reading from 18563: heap size 398 MB, throughput 0.974755
Numeric result:
Recommendation: 2 clients, utility 0.67833:
    h1: 970.405 MB (U(h) = 0.464475*h^0.0798334)
    h2: 1237.59 MB (U(h) = 0.408469*h^0.101812)
Recommendation: 2 clients, utility 0.67833:
    h1: 970.422 MB (U(h) = 0.464475*h^0.0798334)
    h2: 1237.58 MB (U(h) = 0.408469*h^0.101812)
Reading from 18564: heap size 399 MB, throughput 0.97665
Reading from 18563: heap size 400 MB, throughput 0.979033
Reading from 18564: heap size 400 MB, throughput 0.981605
Reading from 18563: heap size 403 MB, throughput 0.971498
Reading from 18563: heap size 405 MB, throughput 0.953178
Reading from 18563: heap size 410 MB, throughput 0.931772
Reading from 18563: heap size 413 MB, throughput 0.892407
Reading from 18564: heap size 400 MB, throughput 0.977062
Reading from 18564: heap size 400 MB, throughput 0.958715
Reading from 18564: heap size 401 MB, throughput 0.923763
Reading from 18564: heap size 407 MB, throughput 0.896888
Reading from 18564: heap size 410 MB, throughput 0.950895
Reading from 18563: heap size 422 MB, throughput 0.969451
Reading from 18564: heap size 419 MB, throughput 0.979289
Reading from 18563: heap size 423 MB, throughput 0.980372
Reading from 18564: heap size 420 MB, throughput 0.980867
Reading from 18563: heap size 425 MB, throughput 0.974648
Reading from 18564: heap size 422 MB, throughput 0.985316
Reading from 18563: heap size 427 MB, throughput 0.979473
Numeric result:
Recommendation: 2 clients, utility 0.798795:
    h1: 997.916 MB (U(h) = 0.414299*h^0.107534)
    h2: 1210.08 MB (U(h) = 0.363604*h^0.130397)
Recommendation: 2 clients, utility 0.798795:
    h1: 997.917 MB (U(h) = 0.414299*h^0.107534)
    h2: 1210.08 MB (U(h) = 0.363604*h^0.130397)
Reading from 18564: heap size 424 MB, throughput 0.98429
Reading from 18563: heap size 427 MB, throughput 0.981846
Reading from 18564: heap size 424 MB, throughput 0.984056
Reading from 18563: heap size 429 MB, throughput 0.983354
Reading from 18564: heap size 426 MB, throughput 0.982936
Reading from 18563: heap size 427 MB, throughput 0.981759
Reading from 18564: heap size 424 MB, throughput 0.982917
Reading from 18563: heap size 429 MB, throughput 0.982389
Reading from 18564: heap size 426 MB, throughput 0.982657
Reading from 18563: heap size 426 MB, throughput 0.982595
Numeric result:
Recommendation: 2 clients, utility 0.825168:
    h1: 1005.6 MB (U(h) = 0.404007*h^0.113557)
    h2: 1202.4 MB (U(h) = 0.355627*h^0.13578)
Recommendation: 2 clients, utility 0.825168:
    h1: 1005.6 MB (U(h) = 0.404007*h^0.113557)
    h2: 1202.4 MB (U(h) = 0.355627*h^0.13578)
Reading from 18564: heap size 425 MB, throughput 0.982581
Reading from 18563: heap size 428 MB, throughput 0.981766
Reading from 18563: heap size 430 MB, throughput 0.983557
Reading from 18563: heap size 430 MB, throughput 0.956031
Reading from 18564: heap size 427 MB, throughput 0.965914
Reading from 18563: heap size 433 MB, throughput 0.945054
Reading from 18563: heap size 433 MB, throughput 0.939218
Reading from 18564: heap size 444 MB, throughput 0.984209
Reading from 18564: heap size 444 MB, throughput 0.980303
Reading from 18564: heap size 447 MB, throughput 0.970999
Reading from 18564: heap size 447 MB, throughput 0.981972
Reading from 18563: heap size 441 MB, throughput 0.983382
Reading from 18564: heap size 450 MB, throughput 0.991245
Reading from 18563: heap size 442 MB, throughput 0.987445
Reading from 18564: heap size 453 MB, throughput 0.99185
Numeric result:
Recommendation: 2 clients, utility 0.886772:
    h1: 1031.54 MB (U(h) = 0.379281*h^0.128505)
    h2: 1176.46 MB (U(h) = 0.340082*h^0.146559)
Recommendation: 2 clients, utility 0.886772:
    h1: 1031.54 MB (U(h) = 0.379281*h^0.128505)
    h2: 1176.46 MB (U(h) = 0.340082*h^0.146559)
Reading from 18563: heap size 445 MB, throughput 0.98995
Reading from 18564: heap size 456 MB, throughput 0.99067
Reading from 18563: heap size 447 MB, throughput 0.990515
Reading from 18564: heap size 457 MB, throughput 0.990651
Reading from 18563: heap size 445 MB, throughput 0.988679
Reading from 18564: heap size 454 MB, throughput 0.989533
Reading from 18563: heap size 448 MB, throughput 0.987319
Reading from 18564: heap size 456 MB, throughput 0.988606
Reading from 18563: heap size 447 MB, throughput 0.986132
Numeric result:
Recommendation: 2 clients, utility 0.917625:
    h1: 1036.29 MB (U(h) = 0.369075*h^0.134891)
    h2: 1171.71 MB (U(h) = 0.331691*h^0.15252)
Recommendation: 2 clients, utility 0.917625:
    h1: 1036.28 MB (U(h) = 0.369075*h^0.134891)
    h2: 1171.72 MB (U(h) = 0.331691*h^0.15252)
Reading from 18564: heap size 456 MB, throughput 0.986719
Reading from 18563: heap size 448 MB, throughput 0.984714
Reading from 18564: heap size 456 MB, throughput 0.984363
Reading from 18563: heap size 450 MB, throughput 0.987847
Reading from 18563: heap size 451 MB, throughput 0.979884
Reading from 18563: heap size 450 MB, throughput 0.968408
Reading from 18563: heap size 452 MB, throughput 0.973466
Reading from 18564: heap size 458 MB, throughput 0.9882
Reading from 18564: heap size 460 MB, throughput 0.979244
Reading from 18564: heap size 458 MB, throughput 0.967228
Reading from 18564: heap size 462 MB, throughput 0.950256
Reading from 18563: heap size 460 MB, throughput 0.987047
Reading from 18564: heap size 472 MB, throughput 0.979836
Numeric result:
Recommendation: 2 clients, utility 0.950719:
    h1: 1039 MB (U(h) = 0.359044*h^0.141308)
    h2: 1169 MB (U(h) = 0.322772*h^0.158985)
Recommendation: 2 clients, utility 0.950719:
    h1: 1039.01 MB (U(h) = 0.359044*h^0.141308)
    h2: 1168.99 MB (U(h) = 0.322772*h^0.158985)
Reading from 18564: heap size 474 MB, throughput 0.989097
Reading from 18563: heap size 460 MB, throughput 0.989321
Reading from 18564: heap size 474 MB, throughput 0.990911
Reading from 18563: heap size 462 MB, throughput 0.991081
Reading from 18564: heap size 478 MB, throughput 0.988792
Reading from 18563: heap size 463 MB, throughput 0.98983
Reading from 18564: heap size 475 MB, throughput 0.987372
Reading from 18563: heap size 460 MB, throughput 0.988134
Reading from 18564: heap size 478 MB, throughput 0.987121
Numeric result:
Recommendation: 2 clients, utility 0.971099:
    h1: 1046.56 MB (U(h) = 0.351714*h^0.146068)
    h2: 1161.44 MB (U(h) = 0.318524*h^0.162102)
Recommendation: 2 clients, utility 0.971099:
    h1: 1046.56 MB (U(h) = 0.351714*h^0.146068)
    h2: 1161.44 MB (U(h) = 0.318524*h^0.162102)
Reading from 18563: heap size 463 MB, throughput 0.986997
Reading from 18564: heap size 479 MB, throughput 0.9868
Reading from 18563: heap size 465 MB, throughput 0.984937
Reading from 18564: heap size 480 MB, throughput 0.985944
Reading from 18563: heap size 465 MB, throughput 0.98807
Reading from 18563: heap size 468 MB, throughput 0.981957
Reading from 18563: heap size 469 MB, throughput 0.969813
Reading from 18564: heap size 483 MB, throughput 0.986357
Reading from 18564: heap size 484 MB, throughput 0.980823
Reading from 18564: heap size 485 MB, throughput 0.972839
Reading from 18563: heap size 474 MB, throughput 0.984669
Reading from 18564: heap size 487 MB, throughput 0.985294
Numeric result:
Recommendation: 2 clients, utility 1.00476:
    h1: 1057.44 MB (U(h) = 0.340214*h^0.153699)
    h2: 1150.56 MB (U(h) = 0.311631*h^0.16722)
Recommendation: 2 clients, utility 1.00476:
    h1: 1057.49 MB (U(h) = 0.340214*h^0.153699)
    h2: 1150.51 MB (U(h) = 0.311631*h^0.16722)
Reading from 18563: heap size 476 MB, throughput 0.989765
Reading from 18564: heap size 494 MB, throughput 0.990742
Reading from 18563: heap size 479 MB, throughput 0.990011
Reading from 18564: heap size 495 MB, throughput 0.990577
Reading from 18563: heap size 480 MB, throughput 0.990141
Reading from 18564: heap size 495 MB, throughput 0.990419
Reading from 18563: heap size 478 MB, throughput 0.988936
Reading from 18564: heap size 498 MB, throughput 0.989916
Numeric result:
Recommendation: 2 clients, utility 1.02696:
    h1: 1054.19 MB (U(h) = 0.335118*h^0.157132)
    h2: 1153.81 MB (U(h) = 0.3053*h^0.171982)
Recommendation: 2 clients, utility 1.02696:
    h1: 1054.19 MB (U(h) = 0.335118*h^0.157132)
    h2: 1153.81 MB (U(h) = 0.3053*h^0.171982)
Reading from 18563: heap size 480 MB, throughput 0.985711
Reading from 18564: heap size 496 MB, throughput 0.985614
Reading from 18564: heap size 498 MB, throughput 0.984992
Reading from 18563: heap size 483 MB, throughput 0.986113
Reading from 18564: heap size 501 MB, throughput 0.985649
Reading from 18563: heap size 483 MB, throughput 0.987713
Reading from 18563: heap size 485 MB, throughput 0.981179
Reading from 18564: heap size 502 MB, throughput 0.980178
Reading from 18563: heap size 486 MB, throughput 0.974073
Reading from 18564: heap size 502 MB, throughput 0.967309
Reading from 18564: heap size 505 MB, throughput 0.980248
Reading from 18563: heap size 494 MB, throughput 0.987505
Numeric result:
Recommendation: 2 clients, utility 1.04739:
    h1: 1054.73 MB (U(h) = 0.329747*h^0.160793)
    h2: 1153.27 MB (U(h) = 0.300268*h^0.17581)
Recommendation: 2 clients, utility 1.04739:
    h1: 1054.75 MB (U(h) = 0.329747*h^0.160793)
    h2: 1153.25 MB (U(h) = 0.300268*h^0.17581)
Reading from 18564: heap size 513 MB, throughput 0.989869
Reading from 18563: heap size 494 MB, throughput 0.990164
Reading from 18564: heap size 514 MB, throughput 0.990859
Reading from 18563: heap size 494 MB, throughput 0.990333
Reading from 18564: heap size 515 MB, throughput 0.992296
Reading from 18563: heap size 497 MB, throughput 0.990307
Numeric result:
Recommendation: 2 clients, utility 1.05724:
    h1: 1060.91 MB (U(h) = 0.325803*h^0.163494)
    h2: 1147.09 MB (U(h) = 0.298995*h^0.17678)
Recommendation: 2 clients, utility 1.05724:
    h1: 1060.89 MB (U(h) = 0.325803*h^0.163494)
    h2: 1147.11 MB (U(h) = 0.298995*h^0.17678)
Reading from 18564: heap size 518 MB, throughput 0.99226
Reading from 18563: heap size 496 MB, throughput 0.938725
Reading from 18564: heap size 518 MB, throughput 0.990935
Reading from 18563: heap size 498 MB, throughput 0.892377
Reading from 18564: heap size 519 MB, throughput 0.989417
Reading from 18563: heap size 499 MB, throughput 0.990784
Reading from 18563: heap size 501 MB, throughput 0.984918
Reading from 18563: heap size 500 MB, throughput 0.976157
Reading from 18564: heap size 522 MB, throughput 0.988976
Reading from 18564: heap size 523 MB, throughput 0.980799
Reading from 18564: heap size 527 MB, throughput 0.980567
Numeric result:
Recommendation: 2 clients, utility 1.20826:
    h1: 1305.47 MB (U(h) = 0.187027*h^0.259716)
    h2: 902.526 MB (U(h) = 0.295378*h^0.179554)
Recommendation: 2 clients, utility 1.20826:
    h1: 1305.47 MB (U(h) = 0.187027*h^0.259716)
    h2: 902.531 MB (U(h) = 0.295378*h^0.179554)
Reading from 18563: heap size 502 MB, throughput 0.986624
Reading from 18564: heap size 529 MB, throughput 0.990028
Reading from 18563: heap size 507 MB, throughput 0.990201
Reading from 18564: heap size 532 MB, throughput 0.992117
Reading from 18563: heap size 509 MB, throughput 0.991003
Reading from 18564: heap size 534 MB, throughput 0.991682
Reading from 18563: heap size 509 MB, throughput 0.990287
Numeric result:
Recommendation: 2 clients, utility 1.43938:
    h1: 1497.55 MB (U(h) = 0.0904621*h^0.383318)
    h2: 710.448 MB (U(h) = 0.292401*h^0.181849)
Recommendation: 2 clients, utility 1.43938:
    h1: 1497.55 MB (U(h) = 0.0904621*h^0.383318)
    h2: 710.449 MB (U(h) = 0.292401*h^0.181849)
Reading from 18564: heap size 533 MB, throughput 0.992329
Reading from 18563: heap size 511 MB, throughput 0.990543
Reading from 18564: heap size 536 MB, throughput 0.990278
Reading from 18563: heap size 512 MB, throughput 0.989424
Reading from 18563: heap size 513 MB, throughput 0.989797
Reading from 18564: heap size 538 MB, throughput 0.991796
Reading from 18563: heap size 515 MB, throughput 0.984368
Reading from 18563: heap size 517 MB, throughput 0.975937
Reading from 18564: heap size 539 MB, throughput 0.983415
Reading from 18564: heap size 539 MB, throughput 0.974743
Numeric result:
Recommendation: 2 clients, utility 1.98695:
    h1: 1224.96 MB (U(h) = 0.0482182*h^0.488989)
    h2: 983.04 MB (U(h) = 0.0852314*h^0.392411)
Recommendation: 2 clients, utility 1.98695:
    h1: 1224.97 MB (U(h) = 0.0482182*h^0.488989)
    h2: 983.031 MB (U(h) = 0.0852314*h^0.392411)
Reading from 18564: heap size 541 MB, throughput 0.984272
Reading from 18563: heap size 524 MB, throughput 0.989838
Reading from 18564: heap size 549 MB, throughput 0.989563
Reading from 18563: heap size 524 MB, throughput 0.991536
Reading from 18564: heap size 550 MB, throughput 0.990745
Numeric result:
Recommendation: 2 clients, utility 2.13449:
    h1: 1225.6 MB (U(h) = 0.0377529*h^0.529172)
    h2: 982.396 MB (U(h) = 0.0706075*h^0.424165)
Recommendation: 2 clients, utility 2.13449:
    h1: 1225.6 MB (U(h) = 0.0377529*h^0.529172)
    h2: 982.398 MB (U(h) = 0.0706075*h^0.424165)
Reading from 18563: heap size 524 MB, throughput 0.991441
Reading from 18564: heap size 551 MB, throughput 0.990857
Reading from 18563: heap size 527 MB, throughput 0.990487
Reading from 18564: heap size 554 MB, throughput 0.990861
Reading from 18563: heap size 526 MB, throughput 0.989785
Reading from 18564: heap size 556 MB, throughput 0.990295
Numeric result:
Recommendation: 2 clients, utility 2.17526:
    h1: 1041.01 MB (U(h) = 0.0577493*h^0.458354)
    h2: 1166.99 MB (U(h) = 0.0413984*h^0.513819)
Recommendation: 2 clients, utility 2.17526:
    h1: 1041.01 MB (U(h) = 0.0577493*h^0.458354)
    h2: 1166.99 MB (U(h) = 0.0413984*h^0.513819)
Reading from 18563: heap size 527 MB, throughput 0.991421
Reading from 18563: heap size 529 MB, throughput 0.980051
Reading from 18564: heap size 557 MB, throughput 0.987668
Reading from 18564: heap size 560 MB, throughput 0.980792
Reading from 18563: heap size 530 MB, throughput 0.980068
Reading from 18564: heap size 562 MB, throughput 0.986642
Reading from 18563: heap size 551 MB, throughput 0.990146
Reading from 18564: heap size 569 MB, throughput 0.992167
Reading from 18563: heap size 552 MB, throughput 0.993044
Numeric result:
Recommendation: 2 clients, utility 2.21035:
    h1: 879.214 MB (U(h) = 0.0894525*h^0.385911)
    h2: 1328.79 MB (U(h) = 0.0272252*h^0.583244)
Recommendation: 2 clients, utility 2.21035:
    h1: 879.211 MB (U(h) = 0.0894525*h^0.385911)
    h2: 1328.79 MB (U(h) = 0.0272252*h^0.583244)
Reading from 18564: heap size 570 MB, throughput 0.992351
Reading from 18563: heap size 559 MB, throughput 0.993577
Reading from 18564: heap size 571 MB, throughput 0.99237
Reading from 18563: heap size 560 MB, throughput 0.992412
Reading from 18564: heap size 573 MB, throughput 0.991712
Numeric result:
Recommendation: 2 clients, utility 2.2017:
    h1: 820.918 MB (U(h) = 0.107662*h^0.355249)
    h2: 1387.08 MB (U(h) = 0.024508*h^0.600262)
Recommendation: 2 clients, utility 2.2017:
    h1: 820.912 MB (U(h) = 0.107662*h^0.355249)
    h2: 1387.09 MB (U(h) = 0.024508*h^0.600262)
Reading from 18563: heap size 560 MB, throughput 0.991142
Reading from 18564: heap size 575 MB, throughput 0.993172
Reading from 18564: heap size 576 MB, throughput 0.988657
Reading from 18564: heap size 577 MB, throughput 0.984677
Reading from 18563: heap size 562 MB, throughput 0.98877
Reading from 18563: heap size 561 MB, throughput 0.982789
Reading from 18563: heap size 567 MB, throughput 0.986957
Reading from 18564: heap size 579 MB, throughput 0.990843
Reading from 18563: heap size 573 MB, throughput 0.989925
Numeric result:
Recommendation: 2 clients, utility 2.1561:
    h1: 939.255 MB (U(h) = 0.0756156*h^0.412238)
    h2: 1268.74 MB (U(h) = 0.0317304*h^0.556842)
Recommendation: 2 clients, utility 2.1561:
    h1: 939.263 MB (U(h) = 0.0756156*h^0.412238)
    h2: 1268.74 MB (U(h) = 0.0317304*h^0.556842)
Reading from 18564: heap size 582 MB, throughput 0.991642
Reading from 18563: heap size 576 MB, throughput 0.99139
Reading from 18564: heap size 584 MB, throughput 0.99163
Reading from 18563: heap size 578 MB, throughput 0.990914
Reading from 18564: heap size 584 MB, throughput 0.990633
Numeric result:
Recommendation: 2 clients, utility 1.78566:
    h1: 1186.39 MB (U(h) = 0.0738783*h^0.415758)
    h2: 1021.61 MB (U(h) = 0.106607*h^0.358011)
Recommendation: 2 clients, utility 1.78566:
    h1: 1186.39 MB (U(h) = 0.0738783*h^0.415758)
    h2: 1021.61 MB (U(h) = 0.106607*h^0.358011)
Reading from 18563: heap size 579 MB, throughput 0.98983
Reading from 18564: heap size 586 MB, throughput 0.99011
Reading from 18564: heap size 587 MB, throughput 0.988592
Reading from 18564: heap size 591 MB, throughput 0.982403
Reading from 18563: heap size 583 MB, throughput 0.992319
Reading from 18563: heap size 585 MB, throughput 0.986607
Reading from 18563: heap size 583 MB, throughput 0.983248
Reading from 18564: heap size 595 MB, throughput 0.990309
Reading from 18563: heap size 588 MB, throughput 0.988932
Numeric result:
Recommendation: 2 clients, utility 1.76683:
    h1: 1221.5 MB (U(h) = 0.0697679*h^0.424465)
    h2: 986.502 MB (U(h) = 0.116646*h^0.342805)
Recommendation: 2 clients, utility 1.76683:
    h1: 1221.5 MB (U(h) = 0.0697679*h^0.424465)
    h2: 986.502 MB (U(h) = 0.116646*h^0.342805)
Reading from 18564: heap size 596 MB, throughput 0.992145
Reading from 18563: heap size 587 MB, throughput 0.992026
Reading from 18564: heap size 599 MB, throughput 0.993297
Reading from 18563: heap size 590 MB, throughput 0.991312
Reading from 18564: heap size 601 MB, throughput 0.992562
Numeric result:
Recommendation: 2 clients, utility 1.71298:
    h1: 1158.89 MB (U(h) = 0.0878316*h^0.386981)
    h2: 1049.11 MB (U(h) = 0.1112*h^0.350325)
Recommendation: 2 clients, utility 1.71298:
    h1: 1158.89 MB (U(h) = 0.0878316*h^0.386981)
    h2: 1049.11 MB (U(h) = 0.1112*h^0.350325)
Reading from 18563: heap size 588 MB, throughput 0.99212
Reading from 18564: heap size 602 MB, throughput 0.991132
Reading from 18564: heap size 603 MB, throughput 0.988983
Reading from 18564: heap size 610 MB, throughput 0.984235
Reading from 18563: heap size 590 MB, throughput 0.993431
Reading from 18563: heap size 590 MB, throughput 0.988061
Reading from 18563: heap size 592 MB, throughput 0.9861
Reading from 18564: heap size 611 MB, throughput 0.990314
Numeric result:
Recommendation: 2 clients, utility 1.63481:
    h1: 1054.42 MB (U(h) = 0.128433*h^0.325544)
    h2: 1153.58 MB (U(h) = 0.107184*h^0.356147)
Recommendation: 2 clients, utility 1.63481:
    h1: 1054.44 MB (U(h) = 0.128433*h^0.325544)
    h2: 1153.56 MB (U(h) = 0.107184*h^0.356147)
Reading from 18563: heap size 598 MB, throughput 0.991941
Reading from 18564: heap size 616 MB, throughput 0.992133
Reading from 18563: heap size 599 MB, throughput 0.992518
Reading from 18564: heap size 617 MB, throughput 0.99189
Reading from 18563: heap size 600 MB, throughput 0.992672
Numeric result:
Recommendation: 2 clients, utility 1.60661:
    h1: 1026.96 MB (U(h) = 0.142918*h^0.308215)
    h2: 1181.04 MB (U(h) = 0.108045*h^0.354466)
Recommendation: 2 clients, utility 1.60661:
    h1: 1026.95 MB (U(h) = 0.142918*h^0.308215)
    h2: 1181.05 MB (U(h) = 0.108045*h^0.354466)
Reading from 18564: heap size 616 MB, throughput 0.991647
Reading from 18563: heap size 602 MB, throughput 0.991593
Reading from 18564: heap size 619 MB, throughput 0.993221
Reading from 18564: heap size 620 MB, throughput 0.989527
Reading from 18563: heap size 604 MB, throughput 0.992984
Reading from 18563: heap size 604 MB, throughput 0.98826
Reading from 18564: heap size 621 MB, throughput 0.989883
Reading from 18563: heap size 605 MB, throughput 0.987301
Numeric result:
Recommendation: 2 clients, utility 1.41625:
    h1: 760.335 MB (U(h) = 0.354092*h^0.163017)
    h2: 1447.66 MB (U(h) = 0.141698*h^0.31038)
Recommendation: 2 clients, utility 1.41625:
    h1: 760.337 MB (U(h) = 0.354092*h^0.163017)
    h2: 1447.66 MB (U(h) = 0.141698*h^0.31038)
Reading from 18564: heap size 629 MB, throughput 0.992187
Reading from 18563: heap size 607 MB, throughput 0.991635
Reading from 18564: heap size 629 MB, throughput 0.9928
Reading from 18563: heap size 610 MB, throughput 0.99257
Numeric result:
Recommendation: 2 clients, utility 1.40674:
    h1: 551.421 MB (U(h) = 0.509189*h^0.105208)
    h2: 1656.58 MB (U(h) = 0.136593*h^0.316065)
Recommendation: 2 clients, utility 1.40674:
    h1: 551.42 MB (U(h) = 0.509189*h^0.105208)
    h2: 1656.58 MB (U(h) = 0.136593*h^0.316065)
Reading from 18564: heap size 628 MB, throughput 0.992492
Reading from 18563: heap size 612 MB, throughput 0.991128
Reading from 18564: heap size 607 MB, throughput 0.991788
Reading from 18563: heap size 613 MB, throughput 0.991124
Reading from 18564: heap size 602 MB, throughput 0.990706
Reading from 18564: heap size 594 MB, throughput 0.987227
Client 18564 died
Clients: 1
Reading from 18563: heap size 614 MB, throughput 0.990759
Reading from 18563: heap size 617 MB, throughput 0.987741
Client 18563 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
