#ifndef SIGNALHANDLER_HPP
#define SIGNALHANDLER_HPP

#include <functional>
#include "Mutex.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
    //! A simple way to get signal handling to play nicely with pthreads.
    /*! The callback is called in the context of a normal pthread, so you don't have to worry about signal-safe functions and all the usual stuff. */
    class SignalHandler
    {
    public:
        //! Set up the signal handler.
        /*! Should be called from the main thread (or at least, from the parent thread of all future pthread_create calls), because of the way signal masking works. The \p callback will only be called once; subsequent signals will be ignored. Also, \p callback should be threadsafe. */
        static void init(std::function<void()> callback=nullptr);

    private:
        static void* threadFunc(void* arg);
        static void dummySignalHandler(int i);

        static economem::Mutex s_CallbackLock;
        static std::function<void()> s_Callback GUARDED_BY(s_CallbackLock);
        static sigset_t s_Signals;
    };
}

/*! @} */

#endif
