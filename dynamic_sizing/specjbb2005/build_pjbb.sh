#!/bin/bash
# Patch SPECjbb 2005 to create pjbb2005

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SRC_TARBALL="${DIR}/specjbb2005.tar.gz"
SRC_DIR="${DIR}/SpecJBB05"

if [ ! -e "${SRC_TARBALL}" ]; then
    echo "Original SPECjbb 2005 tarball not present; cannot continue"
    exit 0
fi

rm -rf "${SRC_DIR}" &&
tar xf "${SRC_TARBALL}" &&
cd "${SRC_DIR}" &&
chmod -R u+w src &&
patch -p1 < ../pjbb2005-1.1.patch &&
ant
