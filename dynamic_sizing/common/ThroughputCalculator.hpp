#ifndef THROUGHPUTCALCULATOR_HPP
#define THROUGHPUTCALCULATOR_HPP

#include <sys/types.h>
#include <stdint.h>

/*! \addtogroup common
  @{
*/

namespace economem
{
//! Base class for throughput-calculation algorithms.
/*! Subclasses of this class are used by DaemonProxy to calculate the throughput that should be reported to the daemon whenever a GC ends. Different subclasses can implement completely different algorithms for calculating throughput.

  Throughput is the fraction of time spent doing mutator work, between 0 and 1. */
    class ThroughputCalculator
    {
    public:

        class Result
        {
        public:
            Result() : heapSizeMB(0), throughput(0) {}
            Result(unsigned long long heapSizeMB, double throughput) : heapSizeMB(heapSizeMB), throughput(throughput) {}

            bool isValid() const { return heapSizeMB > 0 && throughput > 0; }

            unsigned long long heapSizeMB;
            double throughput;
        };

        virtual ~ThroughputCalculator() {}

        //! Called by DaemonProxy::gcBegin().
        virtual void gcBegin() = 0;

        //! Called by DaemonProxy::minorGCEnd().
        /*! Returns the heap size and calculated throughput that should be reported to the daemon. Heap size should typically be the heap size at the beginning of the GC (since that was the size the previous mutation period used). */
        virtual Result minorGCEnd() = 0;

        //! Called by DaemonProxy::majorGCEnd().
        /*! Returns the heap size and calculated throughput that should be reported to the daemon. Heap size should typically be the heap size at the beginning of the GC (since that was the size the previous mutation period used).*/
        virtual Result majorGCEnd() = 0;

    protected:
        ThroughputCalculator() {}
    };
}

/*! @} */

#endif
