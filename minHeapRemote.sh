#!/bin/bash
# Calculate the min heap values for the DaCapo benchmarks, on remote machines

function usage()
{
    echo "Usage: $(basename "${0}") hosts_file ssh_command_file gateway_host_file notify_email"
    exit 1
}

test -z "${1}" && usage
test ! -f "${1}" && usage
test -z "${2}" && usage
test ! -f "${2}" && usage
test -z "${3}" && usage
test ! -f "${3}" && usage
test -z "${4}" && usage

HOSTS_FILE="$(readlink -f "${1}")"
SSH_COMMAND_FILE="$(readlink -f "${2}")"
GATEWAY_HOST_FILE="$(readlink -f "${3}")"
NOTIFY_EMAIL="${4}"

GLOBAL_MAX_THREADS='4'
ME="$(whoami)@$(hostname)"
SSH_COMMAND="$(cat "${SSH_COMMAND_FILE}")"
HOST="$(cat "${GATEWAY_HOST_FILE}")"

REMOTE_DIR="economics_memory_code"
REMOTE_OUTDIR="${REMOTE_DIR}/minHeap_${ME}_$(date '+%Y_%m_%d_%H_%M_%S')"
INPUT="$(readlink -f "$(mktemp)")"

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DACAPO='dacapo-9.12-bach.jar'

"${DIR}/checkDacapo.sh" || exit 1

DACAPODIR="$(readlink -f "$(mktemp -d)")"
cp "${DIR}/${DACAPO}" "${DACAPODIR}/${DACAPO}"
cd "${DACAPODIR}"
unzip "${DACAPO}"

cd 'cnf'

# Any benchmarks to ignore get rm'd here
# batik doesn't work in Java 7
rm batik.cnf
rm tradebeans.cnf
rm tradesoap.cnf

for i in *.cnf; do
    FILE="$(readlink -f "${i}")"
    NAME="$(grep benchmark "${FILE}" | cut '--delimiter= ' -f 2)"

    if grep 'thread-model per_cpu' "${FILE}" >/dev/null; then
        MAX_THREADS="${GLOBAL_MAX_THREADS}"
    else
        MAX_THREADS='1'
    fi

    grep 'size ' "${FILE}" | cut '--delimiter= ' -f 2 | while read line; do
        j='1'
        while (( j <= MAX_THREADS )); do
            echo "${NAME} ${line} ${j}" >> "${INPUT}"
            j=$(( j + 1 ))
        done
    done
done

cd
rm -r "${DACAPODIR}"

ssh "${HOST}" mkdir -p "${REMOTE_OUTDIR}"
ssh "${HOST}" "${REMOTE_DIR}/checkDacapo.sh"

scp -q "${INPUT}" "${HOST}:${REMOTE_OUTDIR}/input.txt"

distribute --email "${NOTIFY_EMAIL}" --senderConfig "${REMOTE_DIR}/email.conf" --hostsSsh "${SSH_COMMAND}" "${HOSTS_FILE}" "${INPUT}" "${REMOTE_DIR}/minHeap_remoteHarness.sh" "${REMOTE_OUTDIR}"

rm "${INPUT}"

echo "Don't forget to collect and process the data once it's finished"
