economemd
    total memory: 1656 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub42_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 13798: heap size 9 MB, throughput 0.991928
Clients: 1
Client 13798 has a minimum heap size of 276 MB
Reading from 13799: heap size 9 MB, throughput 0.990156
Clients: 2
Client 13799 has a minimum heap size of 276 MB
Reading from 13798: heap size 9 MB, throughput 0.974098
Reading from 13799: heap size 9 MB, throughput 0.974845
Reading from 13798: heap size 9 MB, throughput 0.963058
Reading from 13799: heap size 9 MB, throughput 0.96419
Reading from 13798: heap size 9 MB, throughput 0.955145
Reading from 13799: heap size 9 MB, throughput 0.961591
Reading from 13798: heap size 11 MB, throughput 0.980706
Reading from 13799: heap size 11 MB, throughput 0.986715
Reading from 13798: heap size 11 MB, throughput 0.980523
Reading from 13799: heap size 11 MB, throughput 0.988768
Reading from 13799: heap size 17 MB, throughput 0.908357
Reading from 13798: heap size 17 MB, throughput 0.941084
Reading from 13798: heap size 17 MB, throughput 0.417725
Reading from 13799: heap size 17 MB, throughput 0.655432
Reading from 13799: heap size 30 MB, throughput 0.927193
Reading from 13798: heap size 30 MB, throughput 0.960025
Reading from 13799: heap size 31 MB, throughput 0.936301
Reading from 13798: heap size 31 MB, throughput 0.785701
Reading from 13799: heap size 34 MB, throughput 0.502001
Reading from 13798: heap size 35 MB, throughput 0.356095
Reading from 13799: heap size 47 MB, throughput 0.915669
Reading from 13798: heap size 46 MB, throughput 0.792614
Reading from 13799: heap size 49 MB, throughput 0.919335
Reading from 13798: heap size 50 MB, throughput 0.455043
Reading from 13799: heap size 51 MB, throughput 0.248587
Reading from 13798: heap size 64 MB, throughput 0.840772
Reading from 13798: heap size 70 MB, throughput 0.659331
Reading from 13799: heap size 75 MB, throughput 0.805118
Reading from 13799: heap size 75 MB, throughput 0.261603
Reading from 13799: heap size 97 MB, throughput 0.769598
Reading from 13798: heap size 72 MB, throughput 0.168026
Reading from 13799: heap size 100 MB, throughput 0.73531
Reading from 13798: heap size 104 MB, throughput 0.826284
Reading from 13798: heap size 104 MB, throughput 0.213374
Reading from 13799: heap size 102 MB, throughput 0.197376
Reading from 13799: heap size 129 MB, throughput 0.696399
Reading from 13798: heap size 136 MB, throughput 0.71726
Reading from 13799: heap size 133 MB, throughput 0.82469
Reading from 13798: heap size 136 MB, throughput 0.859745
Reading from 13799: heap size 135 MB, throughput 0.752295
Reading from 13798: heap size 138 MB, throughput 0.798784
Reading from 13798: heap size 142 MB, throughput 0.764565
Reading from 13799: heap size 143 MB, throughput 0.212736
Reading from 13799: heap size 174 MB, throughput 0.594732
Reading from 13798: heap size 145 MB, throughput 0.127341
Reading from 13799: heap size 181 MB, throughput 0.831077
Reading from 13799: heap size 183 MB, throughput 0.708055
Reading from 13798: heap size 176 MB, throughput 0.276725
Reading from 13798: heap size 205 MB, throughput 0.758108
Reading from 13798: heap size 210 MB, throughput 0.760434
Reading from 13799: heap size 184 MB, throughput 0.189611
Reading from 13798: heap size 212 MB, throughput 0.820957
Reading from 13799: heap size 226 MB, throughput 0.909919
Reading from 13798: heap size 214 MB, throughput 0.949155
Reading from 13799: heap size 229 MB, throughput 0.922595
Reading from 13799: heap size 234 MB, throughput 0.743141
Reading from 13799: heap size 239 MB, throughput 0.804541
Reading from 13799: heap size 245 MB, throughput 0.571155
Reading from 13798: heap size 217 MB, throughput 0.269788
Reading from 13799: heap size 251 MB, throughput 0.650815
Reading from 13798: heap size 267 MB, throughput 0.618288
Reading from 13798: heap size 267 MB, throughput 0.800959
Reading from 13799: heap size 255 MB, throughput 0.767075
Reading from 13798: heap size 268 MB, throughput 0.946832
Reading from 13798: heap size 265 MB, throughput 0.878583
Reading from 13799: heap size 259 MB, throughput 0.802103
Reading from 13799: heap size 262 MB, throughput 0.659588
Reading from 13798: heap size 267 MB, throughput 0.862186
Reading from 13799: heap size 269 MB, throughput 0.578136
Reading from 13799: heap size 273 MB, throughput 0.658642
Reading from 13798: heap size 268 MB, throughput 0.786325
Reading from 13799: heap size 280 MB, throughput 0.655591
Reading from 13798: heap size 269 MB, throughput 0.658487
Reading from 13798: heap size 274 MB, throughput 0.651917
Reading from 13799: heap size 283 MB, throughput 0.663287
Reading from 13798: heap size 276 MB, throughput 0.808794
Reading from 13798: heap size 281 MB, throughput 0.825492
Reading from 13799: heap size 286 MB, throughput 0.934268
Reading from 13799: heap size 288 MB, throughput 0.704186
Reading from 13798: heap size 282 MB, throughput 0.966163
Reading from 13798: heap size 285 MB, throughput 0.687102
Reading from 13798: heap size 288 MB, throughput 0.723361
Reading from 13798: heap size 295 MB, throughput 0.71561
Reading from 13798: heap size 296 MB, throughput 0.768182
Reading from 13798: heap size 295 MB, throughput 0.916165
Reading from 13799: heap size 285 MB, throughput 0.0988836
Reading from 13798: heap size 298 MB, throughput 0.873963
Reading from 13799: heap size 332 MB, throughput 0.641877
Reading from 13798: heap size 300 MB, throughput 0.807498
Reading from 13799: heap size 335 MB, throughput 0.866318
Reading from 13798: heap size 301 MB, throughput 0.699557
Reading from 13798: heap size 307 MB, throughput 0.665095
Reading from 13799: heap size 336 MB, throughput 0.869238
Reading from 13798: heap size 309 MB, throughput 0.652479
Reading from 13798: heap size 316 MB, throughput 0.602698
Reading from 13799: heap size 335 MB, throughput 0.844439
Reading from 13798: heap size 318 MB, throughput 0.749833
Reading from 13799: heap size 338 MB, throughput 0.691394
Reading from 13799: heap size 342 MB, throughput 0.609472
Reading from 13799: heap size 342 MB, throughput 0.58811
Reading from 13799: heap size 350 MB, throughput 0.715346
Reading from 13799: heap size 350 MB, throughput 0.549628
Reading from 13799: heap size 358 MB, throughput 0.694672
Equal recommendation: 828 MB each
Reading from 13798: heap size 326 MB, throughput 0.961862
Reading from 13799: heap size 359 MB, throughput 0.983685
Reading from 13798: heap size 327 MB, throughput 0.98727
Reading from 13798: heap size 327 MB, throughput 0.979205
Reading from 13799: heap size 361 MB, throughput 0.964734
Reading from 13798: heap size 330 MB, throughput 0.976003
Reading from 13799: heap size 364 MB, throughput 0.970258
Reading from 13798: heap size 330 MB, throughput 0.970217
Reading from 13799: heap size 368 MB, throughput 0.968236
Reading from 13798: heap size 332 MB, throughput 0.97323
Reading from 13799: heap size 369 MB, throughput 0.982224
Reading from 13798: heap size 329 MB, throughput 0.973977
Reading from 13798: heap size 332 MB, throughput 0.958485
Reading from 13799: heap size 369 MB, throughput 0.97215
Equal recommendation: 828 MB each
Reading from 13798: heap size 332 MB, throughput 0.971035
Reading from 13799: heap size 372 MB, throughput 0.969666
Reading from 13798: heap size 333 MB, throughput 0.970338
Reading from 13799: heap size 369 MB, throughput 0.967546
Reading from 13798: heap size 334 MB, throughput 0.969025
Reading from 13799: heap size 372 MB, throughput 0.97997
Reading from 13798: heap size 336 MB, throughput 0.971245
Reading from 13799: heap size 371 MB, throughput 0.975529
Reading from 13798: heap size 337 MB, throughput 0.969468
Reading from 13798: heap size 338 MB, throughput 0.964593
Reading from 13799: heap size 372 MB, throughput 0.979634
Reading from 13798: heap size 340 MB, throughput 0.981838
Reading from 13799: heap size 375 MB, throughput 0.975612
Reading from 13798: heap size 342 MB, throughput 0.977455
Equal recommendation: 828 MB each
Reading from 13798: heap size 345 MB, throughput 0.925233
Reading from 13799: heap size 376 MB, throughput 0.689782
Reading from 13798: heap size 345 MB, throughput 0.119314
Reading from 13798: heap size 378 MB, throughput 0.527202
Reading from 13798: heap size 383 MB, throughput 0.794309
Reading from 13798: heap size 389 MB, throughput 0.840417
Reading from 13799: heap size 416 MB, throughput 0.963257
Reading from 13799: heap size 421 MB, throughput 0.8418
Reading from 13798: heap size 389 MB, throughput 0.962782
Reading from 13799: heap size 425 MB, throughput 0.830773
Reading from 13799: heap size 424 MB, throughput 0.856879
Reading from 13799: heap size 428 MB, throughput 0.938518
Reading from 13798: heap size 393 MB, throughput 0.974633
Reading from 13799: heap size 429 MB, throughput 0.98641
Reading from 13798: heap size 394 MB, throughput 0.98225
Reading from 13799: heap size 436 MB, throughput 0.988995
Reading from 13798: heap size 392 MB, throughput 0.978872
Reading from 13798: heap size 395 MB, throughput 0.972204
Reading from 13799: heap size 437 MB, throughput 0.987446
Reading from 13798: heap size 395 MB, throughput 0.974287
Reading from 13799: heap size 439 MB, throughput 0.985039
Equal recommendation: 828 MB each
Reading from 13798: heap size 396 MB, throughput 0.979971
Reading from 13799: heap size 441 MB, throughput 0.984868
Reading from 13798: heap size 399 MB, throughput 0.959257
Reading from 13799: heap size 438 MB, throughput 0.982417
Reading from 13798: heap size 401 MB, throughput 0.966059
Reading from 13799: heap size 441 MB, throughput 0.983834
Reading from 13798: heap size 403 MB, throughput 0.976987
Reading from 13798: heap size 406 MB, throughput 0.965518
Reading from 13799: heap size 440 MB, throughput 0.986745
Reading from 13798: heap size 409 MB, throughput 0.963942
Reading from 13799: heap size 441 MB, throughput 0.981339
Reading from 13798: heap size 413 MB, throughput 0.97459
Equal recommendation: 828 MB each
Reading from 13799: heap size 443 MB, throughput 0.982212
Reading from 13798: heap size 412 MB, throughput 0.980966
Reading from 13798: heap size 413 MB, throughput 0.857298
Reading from 13798: heap size 411 MB, throughput 0.817298
Reading from 13798: heap size 413 MB, throughput 0.796156
Reading from 13798: heap size 421 MB, throughput 0.845881
Reading from 13799: heap size 444 MB, throughput 0.984592
Reading from 13799: heap size 449 MB, throughput 0.889351
Reading from 13799: heap size 450 MB, throughput 0.863823
Reading from 13799: heap size 456 MB, throughput 0.966916
Reading from 13798: heap size 421 MB, throughput 0.98243
Reading from 13799: heap size 457 MB, throughput 0.991076
Reading from 13798: heap size 425 MB, throughput 0.986672
Reading from 13798: heap size 427 MB, throughput 0.987853
Reading from 13799: heap size 459 MB, throughput 0.992701
Reading from 13798: heap size 428 MB, throughput 0.984204
Reading from 13799: heap size 461 MB, throughput 0.991394
Reading from 13798: heap size 430 MB, throughput 0.989128
Equal recommendation: 828 MB each
Reading from 13799: heap size 460 MB, throughput 0.986793
Reading from 13798: heap size 428 MB, throughput 0.981262
Reading from 13798: heap size 430 MB, throughput 0.984895
Reading from 13799: heap size 462 MB, throughput 0.989132
Reading from 13798: heap size 427 MB, throughput 0.98199
Reading from 13799: heap size 459 MB, throughput 0.985605
Reading from 13798: heap size 429 MB, throughput 0.980835
Reading from 13799: heap size 462 MB, throughput 0.985053
Reading from 13798: heap size 427 MB, throughput 0.965574
Reading from 13798: heap size 429 MB, throughput 0.985713
Reading from 13799: heap size 464 MB, throughput 0.9816
Reading from 13798: heap size 428 MB, throughput 0.962507
Reading from 13798: heap size 429 MB, throughput 0.865652
Reading from 13798: heap size 432 MB, throughput 0.797593
Equal recommendation: 828 MB each
Reading from 13798: heap size 433 MB, throughput 0.959955
Reading from 13799: heap size 464 MB, throughput 0.992211
Reading from 13799: heap size 466 MB, throughput 0.940114
Reading from 13799: heap size 467 MB, throughput 0.876101
Reading from 13799: heap size 471 MB, throughput 0.862766
Reading from 13798: heap size 439 MB, throughput 0.99036
Reading from 13799: heap size 473 MB, throughput 0.98623
Reading from 13798: heap size 441 MB, throughput 0.984997
Reading from 13798: heap size 443 MB, throughput 0.983654
Reading from 13799: heap size 478 MB, throughput 0.994591
Reading from 13798: heap size 445 MB, throughput 0.98776
Reading from 13799: heap size 480 MB, throughput 0.98974
Equal recommendation: 828 MB each
Reading from 13798: heap size 444 MB, throughput 0.982709
Reading from 13799: heap size 480 MB, throughput 0.989064
Reading from 13798: heap size 446 MB, throughput 0.98913
Reading from 13799: heap size 482 MB, throughput 0.988827
Reading from 13798: heap size 445 MB, throughput 0.984075
Reading from 13798: heap size 446 MB, throughput 0.981655
Reading from 13799: heap size 482 MB, throughput 0.987435
Reading from 13798: heap size 448 MB, throughput 0.987378
Reading from 13799: heap size 483 MB, throughput 0.987762
Reading from 13798: heap size 448 MB, throughput 0.962685
Reading from 13798: heap size 449 MB, throughput 0.842456
Reading from 13798: heap size 451 MB, throughput 0.866246
Equal recommendation: 828 MB each
Reading from 13798: heap size 456 MB, throughput 0.987735
Reading from 13799: heap size 486 MB, throughput 0.988796
Reading from 13799: heap size 487 MB, throughput 0.964694
Reading from 13799: heap size 486 MB, throughput 0.885443
Reading from 13799: heap size 489 MB, throughput 0.947243
Reading from 13798: heap size 458 MB, throughput 0.992125
Reading from 13798: heap size 461 MB, throughput 0.992518
Reading from 13799: heap size 496 MB, throughput 0.994596
Reading from 13798: heap size 463 MB, throughput 0.989046
Reading from 13799: heap size 497 MB, throughput 0.992053
Reading from 13798: heap size 462 MB, throughput 0.986543
Equal recommendation: 828 MB each
Reading from 13799: heap size 499 MB, throughput 0.990347
Reading from 13798: heap size 464 MB, throughput 0.984587
Reading from 13799: heap size 501 MB, throughput 0.992465
Reading from 13798: heap size 463 MB, throughput 0.88092
Reading from 13799: heap size 500 MB, throughput 0.988653
Reading from 13798: heap size 482 MB, throughput 0.994437
Reading from 13799: heap size 501 MB, throughput 0.98785
Reading from 13798: heap size 481 MB, throughput 0.99128
Reading from 13798: heap size 484 MB, throughput 0.924166
Reading from 13798: heap size 484 MB, throughput 0.882081
Reading from 13798: heap size 486 MB, throughput 0.871802
Equal recommendation: 828 MB each
Reading from 13799: heap size 504 MB, throughput 0.992756
Reading from 13799: heap size 504 MB, throughput 0.979085
Reading from 13799: heap size 504 MB, throughput 0.898051
Reading from 13798: heap size 496 MB, throughput 0.99389
Reading from 13799: heap size 506 MB, throughput 0.982805
Reading from 13798: heap size 495 MB, throughput 0.987576
Reading from 13799: heap size 513 MB, throughput 0.994509
Reading from 13798: heap size 496 MB, throughput 0.98842
Reading from 13799: heap size 514 MB, throughput 0.992218
Reading from 13798: heap size 499 MB, throughput 0.98743
Equal recommendation: 828 MB each
Reading from 13799: heap size 515 MB, throughput 0.989533
Reading from 13798: heap size 499 MB, throughput 0.986293
Reading from 13799: heap size 516 MB, throughput 0.989788
Reading from 13798: heap size 501 MB, throughput 0.985842
Reading from 13799: heap size 515 MB, throughput 0.987813
Reading from 13798: heap size 504 MB, throughput 0.983798
Reading from 13798: heap size 505 MB, throughput 0.990276
Reading from 13798: heap size 506 MB, throughput 0.927416
Reading from 13799: heap size 516 MB, throughput 0.987841
Reading from 13798: heap size 509 MB, throughput 0.882196
Equal recommendation: 828 MB each
Reading from 13798: heap size 515 MB, throughput 0.987992
Reading from 13799: heap size 519 MB, throughput 0.989811
Reading from 13799: heap size 520 MB, throughput 0.910164
Reading from 13799: heap size 520 MB, throughput 0.92978
Reading from 13798: heap size 518 MB, throughput 0.990623
Reading from 13799: heap size 522 MB, throughput 0.990059
Reading from 13798: heap size 520 MB, throughput 0.992005
Reading from 13799: heap size 526 MB, throughput 0.992191
Reading from 13798: heap size 522 MB, throughput 0.989142
Equal recommendation: 828 MB each
Reading from 13799: heap size 527 MB, throughput 0.992119
Reading from 13798: heap size 521 MB, throughput 0.98853
Reading from 13799: heap size 526 MB, throughput 0.990078
Reading from 13798: heap size 523 MB, throughput 0.987542
Reading from 13798: heap size 526 MB, throughput 0.986032
Reading from 13799: heap size 529 MB, throughput 0.988938
Reading from 13798: heap size 526 MB, throughput 0.986818
Reading from 13798: heap size 529 MB, throughput 0.889751
Equal recommendation: 828 MB each
Reading from 13798: heap size 531 MB, throughput 0.967521
Reading from 13799: heap size 532 MB, throughput 0.9912
Reading from 13799: heap size 532 MB, throughput 0.988225
Reading from 13799: heap size 535 MB, throughput 0.903244
Reading from 13798: heap size 539 MB, throughput 0.989397
Reading from 13799: heap size 537 MB, throughput 0.983365
Reading from 13798: heap size 540 MB, throughput 0.990074
Reading from 13799: heap size 544 MB, throughput 0.990988
Reading from 13798: heap size 540 MB, throughput 0.989923
Reading from 13799: heap size 545 MB, throughput 0.993921
Equal recommendation: 828 MB each
Reading from 13798: heap size 543 MB, throughput 0.988543
Reading from 13799: heap size 544 MB, throughput 0.991453
Reading from 13798: heap size 542 MB, throughput 0.988526
Reading from 13799: heap size 547 MB, throughput 0.989829
Reading from 13798: heap size 544 MB, throughput 0.990015
Reading from 13798: heap size 546 MB, throughput 0.984061
Reading from 13798: heap size 548 MB, throughput 0.905623
Reading from 13799: heap size 547 MB, throughput 0.990061
Equal recommendation: 828 MB each
Reading from 13798: heap size 552 MB, throughput 0.983059
Reading from 13799: heap size 548 MB, throughput 0.994366
Reading from 13799: heap size 550 MB, throughput 0.941764
Reading from 13799: heap size 551 MB, throughput 0.941242
Reading from 13798: heap size 553 MB, throughput 0.991389
Reading from 13798: heap size 556 MB, throughput 0.991245
Reading from 13799: heap size 558 MB, throughput 0.993995
Reading from 13798: heap size 558 MB, throughput 0.986445
Reading from 13799: heap size 558 MB, throughput 0.994569
Equal recommendation: 828 MB each
Reading from 13798: heap size 556 MB, throughput 0.988653
Reading from 13799: heap size 559 MB, throughput 0.991854
Reading from 13798: heap size 559 MB, throughput 0.992099
Reading from 13799: heap size 561 MB, throughput 0.99022
Reading from 13798: heap size 561 MB, throughput 0.992042
Reading from 13798: heap size 562 MB, throughput 0.91481
Reading from 13798: heap size 562 MB, throughput 0.958553
Equal recommendation: 828 MB each
Reading from 13799: heap size 562 MB, throughput 0.990367
Reading from 13798: heap size 564 MB, throughput 0.992059
Reading from 13799: heap size 562 MB, throughput 0.99538
Reading from 13799: heap size 566 MB, throughput 0.931881
Reading from 13799: heap size 566 MB, throughput 0.988606
Reading from 13798: heap size 567 MB, throughput 0.992049
Reading from 13799: heap size 573 MB, throughput 0.994152
Reading from 13798: heap size 569 MB, throughput 0.990907
Equal recommendation: 828 MB each
Reading from 13799: heap size 574 MB, throughput 0.992403
Reading from 13798: heap size 568 MB, throughput 0.990479
Reading from 13799: heap size 573 MB, throughput 0.987491
Reading from 13798: heap size 571 MB, throughput 0.988993
Reading from 13799: heap size 575 MB, throughput 0.989683
Reading from 13798: heap size 573 MB, throughput 0.992235
Reading from 13798: heap size 574 MB, throughput 0.934122
Reading from 13798: heap size 574 MB, throughput 0.981368
Equal recommendation: 828 MB each
Reading from 13799: heap size 577 MB, throughput 0.98986
Reading from 13798: heap size 577 MB, throughput 0.994724
Reading from 13799: heap size 577 MB, throughput 0.981911
Reading from 13799: heap size 584 MB, throughput 0.925854
Reading from 13798: heap size 578 MB, throughput 0.99197
Reading from 13799: heap size 586 MB, throughput 0.994976
Reading from 13798: heap size 580 MB, throughput 0.990511
Equal recommendation: 828 MB each
Reading from 13799: heap size 591 MB, throughput 0.99364
Reading from 13798: heap size 580 MB, throughput 0.989872
Reading from 13799: heap size 592 MB, throughput 0.99139
Reading from 13798: heap size 581 MB, throughput 0.99194
Reading from 13799: heap size 590 MB, throughput 0.991743
Reading from 13798: heap size 583 MB, throughput 0.986581
Reading from 13798: heap size 585 MB, throughput 0.914708
Equal recommendation: 828 MB each
Reading from 13798: heap size 588 MB, throughput 0.990503
Reading from 13799: heap size 593 MB, throughput 0.99126
Reading from 13799: heap size 595 MB, throughput 0.991036
Reading from 13799: heap size 596 MB, throughput 0.920919
Reading from 13798: heap size 590 MB, throughput 0.99313
Reading from 13799: heap size 599 MB, throughput 0.993614
Reading from 13798: heap size 592 MB, throughput 0.99211
Equal recommendation: 828 MB each
Reading from 13799: heap size 600 MB, throughput 0.993958
Reading from 13798: heap size 594 MB, throughput 0.991144
Reading from 13798: heap size 594 MB, throughput 0.987808
Reading from 13799: heap size 602 MB, throughput 0.992873
Reading from 13798: heap size 595 MB, throughput 0.991575
Reading from 13798: heap size 599 MB, throughput 0.895574
Reading from 13799: heap size 604 MB, throughput 0.991519
Reading from 13798: heap size 599 MB, throughput 0.986857
Equal recommendation: 828 MB each
Reading from 13799: heap size 603 MB, throughput 0.988334
Reading from 13798: heap size 607 MB, throughput 0.992825
Reading from 13799: heap size 604 MB, throughput 0.986767
Reading from 13799: heap size 607 MB, throughput 0.964041
Reading from 13798: heap size 608 MB, throughput 0.99016
Reading from 13799: heap size 608 MB, throughput 0.994119
Equal recommendation: 828 MB each
Reading from 13798: heap size 609 MB, throughput 0.991525
Reading from 13799: heap size 611 MB, throughput 0.992716
Reading from 13798: heap size 611 MB, throughput 0.990049
Reading from 13799: heap size 612 MB, throughput 0.991909
Reading from 13798: heap size 614 MB, throughput 0.993823
Reading from 13798: heap size 615 MB, throughput 0.927561
Client 13798 died
Clients: 1
Reading from 13799: heap size 611 MB, throughput 0.993205
Recommendation: one client; give it all the memory
Reading from 13799: heap size 613 MB, throughput 0.996605
Reading from 13799: heap size 614 MB, throughput 0.959493
Client 13799 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
