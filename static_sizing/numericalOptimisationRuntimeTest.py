#!/usr/bin/env python
# Test whether varying the number of dimensions has any effect on the runtime of numerical optimisation

import staticutility as su
import utilitymodel as u
import time

def printAndLog(s, *fs):
    print s
    log(s, *fs)

def log(s, *fs):
    for f in fs:
        f.write(s + "\n")
        f.flush()

results = []
dimRange = range(2, 20)

for model in u.models:
    m = u.models[model]
    p = m.testingParams()
    avgs = []

    for dims in dimRange:
        print "%s: %d dimensions..." % (model, dims),
        times = []

        for i in range(1000):
            starttime = time.clock()

            m.calculateBestNumeric(1000, [p]*dims, [2]*dims)

            runtime = time.clock() - starttime
            times.append(runtime)

        avgs.append((dims, su.average(times), su.stddev(times)))
        print "avg %.2g s" % su.average(times)

    results.append((model, avgs))

with open("runtimeTest.txt", "w") as f:
    t = []
    headings = [""] + map(str, dimRange)
    t.append(headings)

    for r in results:
        t.append([r[0]] + map(lambda (d, a, s): "%.2g" % a, r[1]))

    maxLens = map(lambda l: max(map(len, l)), zip(*t))
    template = "   ".join(map(lambda (x, y): "{%d:%d}" % (x, y), zip(range(len(maxLens)), maxLens)))
    print
    log("Average runtime of numerical optimisation with N dimensions\n", f)
    printAndLog("\n".join(map(lambda l: template.format(*l), t)), f)
