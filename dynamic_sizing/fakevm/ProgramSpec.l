%option prefix="ProgramSpec_"
%option outfile="lex.ProgramSpec.c"
%option header-file="lex.ProgramSpec.h"
%option noyywrap
%option nounput

%{
#include "ProgramSpec.hpp"
#include "ProgramSpec.tab.hpp"

void ProgramSpec_error(const char*);
%}

text       [A-Za-z0-9]
whitespace [ \t]

%%

[1-9][0-9]*           { ProgramSpec_lval.i = atoi(yytext); return INTEGER; }
[0-9]+\.[0-9]+        { ProgramSpec_lval.f = atof(yytext); return FLOAT; }
^phase                { return PHASE; }
^minheap              { return MINHEAP; }
transition            { return TRANSITION; }
{text}+               { ProgramSpec_lval.s = strdup(yytext); return STRING; }
^#.*\n                ; /* Ignore comments */
^{whitespace}+        { return INDENT; }
{whitespace}|\n       ;
.                     { ProgramSpec_error("Invalid character in input"); }
