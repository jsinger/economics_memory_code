module Economem.Java (Java(..),
                      systemJava,
                      hotspot7,
                      hotspot8,
                      hotspot8Unmodified,
                      jikes,
                      validJavaNames,
                      defaultJava,
                      javaFromName,
                      javaVersion,
                      javaProcessLauncher) where

import Control.Applicative
import Control.Monad
import qualified Data.ByteString.Char8 as B
import qualified Data.Csv as C
import Data.List
import qualified Data.Text as T
import Economem.Common
import Economem.Cgroup
import Economem.Daemon
import Economem.DelayedStart
import Economem.Files hiding ((</>))
import qualified Economem.Process as P
import Economem.ResourceUsageFile
import System.FilePath
import System.Process

data Java = Java { javaName :: String,
                   javaPrettyName :: String,
                   javaExecutable :: IO FilePath,
                   javaLibraryPaths :: IO [FilePath],
                   javaStartingHeapToArgs :: DataSize -> [String],
                   javaMaxHeapToArgs :: DataSize -> [String],
                   javaDisableExplicitGCArgs :: [String],
                   javaGCLogToArgs :: FilePath -> [String],
                   javaForceGrowthToArgs :: Bool -> [String],
                   javaSupportedThroughputs :: [ThroughputType],
                   javaDefaultThroughputs :: [ThroughputType],
                   javaThroughputToArgs :: [ThroughputType] -> IO [String],
                   javaThroughputToEnv :: [ThroughputType] -> [P.ExtraEnv],
                   javaSupportsDaemonMinHeap :: Bool,
                   javaDaemonMinHeapToArgs :: DataSize -> IO [String],
                   javaDaemonMinHeapToEnv :: Maybe DataSize -> [P.ExtraEnv] }

hotspotHeapArg :: String -> DataSize -> [String]
hotspotHeapArg name size = [name ++ show (sizeToInt (toMB (size))) ++ "m"]

hotspotXms :: DataSize -> [String]
hotspotXms = hotspotHeapArg "-Xms"

hotspotXmx :: DataSize -> [String]
hotspotXmx = hotspotHeapArg "-Xmx"

hotspotDisableExplicitGC :: [String]
hotspotDisableExplicitGC = ["-XX:+DisableExplicitGC", "-XX:+UseAdaptiveSizePolicyWithSystemGC"]

hotspotGCLog :: FilePath -> [String]
hotspotGCLog path =
  ["-Xloggc:" ++ path ,
   "-XX:+PrintGCDetails",
   "-XX:+PrintGCTimeStamps",
   "-XX:+PrintGCApplicationStoppedTime",
   "-XX:+PrintGCApplicationConcurrentTime",
   "-XX:+PrintAdaptiveSizePolicy"]

hotspotForceGrowth :: Bool -> [String]
hotspotForceGrowth True = ["-XX:GCTimeRatio=999"] -- Goal is to spend 1/1000 of the time GCing, i.e. highly unlikely to be met
hotspotForceGrowth False = []




systemJava :: Java
systemJava =
  Java { javaName = "SystemJava",
         javaPrettyName = "System Java",
         javaExecutable = return "java",
         javaLibraryPaths = return [],
         javaStartingHeapToArgs = hotspotXms,
         javaMaxHeapToArgs = hotspotXmx,
         javaDisableExplicitGCArgs = hotspotDisableExplicitGC,
         javaGCLogToArgs = hotspotGCLog,
         javaForceGrowthToArgs = hotspotForceGrowth,
         javaSupportedThroughputs = [],
         javaDefaultThroughputs = [],
         javaThroughputToArgs = hardcodedThroughput systemJava,
         javaThroughputToEnv = const [],
         javaSupportsDaemonMinHeap = False,
         javaDaemonMinHeapToArgs = unsupportedMinHeap systemJava,
         javaDaemonMinHeapToEnv = const [] }


hotspot7 :: Java
hotspot7 =
  Java { javaName = "Hotspot7",
         javaPrettyName = "HotSpot 7",
         javaExecutable = (</> "bin" </> "java") <$> path,
         javaLibraryPaths = libPaths,
         javaStartingHeapToArgs = hotspotXms,
         javaMaxHeapToArgs = hotspotXmx,
         javaDisableExplicitGCArgs = hotspotDisableExplicitGC,
         javaGCLogToArgs = hotspotGCLog,
         javaForceGrowthToArgs = hotspotForceGrowth,
         -- In HotSpot 7, simple throughput is hardcoded to be on
         javaSupportedThroughputs = [simpleThroughput],
         javaDefaultThroughputs = [simpleThroughput],
         javaThroughputToArgs = hardcodedThroughput hotspot7,
         javaThroughputToEnv = const [],
         javaSupportsDaemonMinHeap = False,
         javaDaemonMinHeapToArgs = unsupportedMinHeap hotspot7,
         javaDaemonMinHeapToEnv = const [] }
  where
    libPaths :: IO [FilePath]
    libPaths = do
      r <- hotspot7Root
      p <- path
      amd64 <- is64bit
      return [r,
              p </> "lib" </> (if amd64 then "amd64" else "i386") </> "server"]

    hotspot7Root :: IO FilePath
    hotspot7Root = (</> "hotspot7") <$> economemRoot

    path :: IO FilePath
    path = (</> "icedtea7" </> "icedtea-build" </> "openjdk.build") <$> hotspot7Root


makeHotspot8 :: String ->
                String ->
                FilePath ->
                [ThroughputType] ->
                [ThroughputType] ->
                ([ThroughputType] -> IO [String]) ->
                Bool ->
                (DataSize -> IO [String]) ->
                Java
makeHotspot8 name prettyName directory supported defaultThroughputs throughputToArgs supportsMinHeap minHeapToArgs =
  Java { javaName = name,
         javaPrettyName = prettyName,
         javaExecutable = (</> "bin" </> "java") <$> path,
         javaLibraryPaths = libPaths,
         javaStartingHeapToArgs = hotspotXms,
         javaMaxHeapToArgs = hotspotXmx,
         javaDisableExplicitGCArgs = hotspotDisableExplicitGC,
         javaGCLogToArgs = hotspotGCLog,
         javaForceGrowthToArgs = hotspotForceGrowth,
         javaSupportedThroughputs = supported,
         javaDefaultThroughputs = defaultThroughputs,
         javaThroughputToArgs = throughputToArgs,
         javaThroughputToEnv = const [],
         javaSupportsDaemonMinHeap = supportsMinHeap,
         javaDaemonMinHeapToArgs = minHeapToArgs,
         javaDaemonMinHeapToEnv = const [] }
  where
    path :: IO FilePath
    path = do
      root <- economemRoot
      amd64 <- is64bit
      return (root </> directory </> "openjdk8" </> "jdk8" </> "build" </>
              ("linux-x86" ++ (if amd64 then "_64" else "") ++ "-normal-server-release") </> "jdk")

    libPaths :: IO [FilePath]
    libPaths = do
      p <- path
      amd64 <- is64bit
      return [p </> "lib" </> (if amd64 then "amd64" else "i386") </> "server"]


hotspot8 :: Java
hotspot8 = makeHotspot8 "Hotspot8" "HotSpot 8" "hotspot8"
           [simpleThroughput, vengerov, avgVengerov, majorOnly, simpleSmoothing, proportional]
           [vengerov]
           (\ts -> do
               checkThroughputsSupported hotspot8 ts
               let cleaned = javaSupportedThroughputs hotspot8 `intersect` nub ts
               args <- mapM toArg cleaned
               return $ args ++ if vengerov `notElem` cleaned then
                                  ["-XX:-EconomemVengerovThroughput"]
                                else
                                  [])
           True
           (\mh -> return ["-XX:EconomemMinHeap=" ++ show (sizeToInt (toMB mh))])
  where
    toArg :: ThroughputType -> IO String
    toArg t
      | t == simpleThroughput = return "-XX:+EconomemSimpleThroughput"
      | t == vengerov = return "-XX:+EconomemVengerovThroughput"
      | t == avgVengerov = return "-XX:+EconomemAvgVengerovThroughput"
      | t == majorOnly = return "-XX:+EconomemMajorOnlyThroughput"
      | t == simpleSmoothing = return "-XX:+EconomemSimpleSmoothingThroughput"
      | t == proportional = return "-XX:+EconomemProportionalThroughput"
      | otherwise = fail $ "Unsupported throughput type: " ++ throughputSocketName t


hotspot8Unmodified :: Java
hotspot8Unmodified = makeHotspot8 "Hotspot8Unmodified" "HotSpot 8 Unmodified" "hotspot8_unmodified" [] []
                     (hardcodedThroughput hotspot8Unmodified)
                     False
                     (unsupportedMinHeap hotspot8Unmodified)


jikes :: Java
jikes =
  Java { javaName = "jikes",
         javaPrettyName = "Jikes RVM",
         javaExecutable =
           (</> "jikes"
            </> "jikesrvm"
            </> "jikesrvm"
            </> "dist"
            </> "production_x86_64-linux"
            </> "rvm") <$> economemRoot,
         javaLibraryPaths = return [],
         javaStartingHeapToArgs = const [],
         javaMaxHeapToArgs = hotspotXmx, -- not a typo; jikes uses the same format as hotspot
         javaDisableExplicitGCArgs = ["-X:gc:ignoreSystemGC=true"],
         javaGCLogToArgs = const [], -- doesn't appear to be available
         javaForceGrowthToArgs = const [],
         javaSupportedThroughputs = [simpleThroughput, -- these are all 'fake', in that they just redirect
                                                       -- majorOnly to a different name
                                     vengerov,
                                     avgVengerov,
                                     majorOnly,
                                     simpleSmoothing,
                                     proportional],
         javaDefaultThroughputs = [simpleThroughput],
         javaThroughputToArgs = hardcodedThroughput jikes,
         javaThroughputToEnv = throughputEnv,
         javaSupportsDaemonMinHeap = True,
         javaDaemonMinHeapToArgs = const (return []),
         javaDaemonMinHeapToEnv = minHeapEnv }
  where
    throughputEnv :: [ThroughputType] -> [P.ExtraEnv]
    throughputEnv [] = []
    throughputEnv (x:_) = [P.ExtraEnv "ECONOMEM_JIKES_DAEMON_NAME" (P.Simple (throughputSocketName x))]

    minHeapEnv :: Maybe DataSize -> [P.ExtraEnv]
    minHeapEnv Nothing = []
    minHeapEnv (Just s) = [P.ExtraEnv "ECONOMEM_JIKES_MIN_HEAP" (P.Simple (show (sizeToInt (toMB s))))]


hardcodedThroughput :: Java -> [ThroughputType] -> IO [String]
hardcodedThroughput j ts = do
  checkThroughputsSupported j ts
  return []

checkThroughputsSupported :: Java -> [ThroughputType] -> IO ()
checkThroughputsSupported j =
  mapM_ (\t -> unless (t `elem` javaSupportedThroughputs j)
               (fail $ "Throughput type '" ++ throughputSocketName t ++ "' is not supported by " ++ javaName j))

unsupportedMinHeap :: Java -> (DataSize -> IO [String])
unsupportedMinHeap j = \_ -> fail $ "Daemon min heap specification is not supported by " ++ javaName j

javas :: [Java]
javas = [systemJava, hotspot7, hotspot8, hotspot8Unmodified, jikes]

defaultJava :: Java
defaultJava = hotspot8

validJavaNames :: [String]
validJavaNames = map javaName javas

javaFromName :: Monad m => String -> m Java
javaFromName s = case lookup s (zip validJavaNames javas) of
  Just java -> return java
  Nothing -> fail ("Unrecognised Java version: " ++ s)

instance C.FromNamedRecord Java where
  parseNamedRecord r = javaFromName =<<
                       r C..: B.pack "JAVA"
instance C.ToNamedRecord Java where
  toNamedRecord r = C.namedRecord (getNamedRecordInput r)
instance CsvFields Java where
  getCsvFieldNames _ = [B.pack "JAVA"]
  getNamedRecordInput java = [B.pack "JAVA" C..= javaName java]


javaProcessLauncher :: Java ->
                       Maybe FilePath ->
                       Maybe FilePath ->
                       FilePath ->
                       [String] ->
                       Maybe Cgroup ->
                       Maybe ResourceUsageFile ->
                       Maybe DelayedStart ->
                       [ThroughputType] ->
                       Maybe DataSize ->
                       Bool ->
                       IO P.ProcessLauncher
javaProcessLauncher j infile outfile workingDir args cg res delay throughputs minHeap forceGrowth = do
  exe <- javaExecutable j
  libPaths <- javaLibraryPaths j
  throughputArgs <- (javaThroughputToArgs j) throughputs
  minHeapArgs <- case minHeap of
    Just m -> (javaDaemonMinHeapToArgs j) m
    Nothing -> return []
  (realCmd, realArgs) <- P.modifyArgs delay =<<
                         P.modifyArgs res =<<
                         P.modifyArgs cg (exe, throughputArgs ++ minHeapArgs ++ javaForceGrowthToArgs j forceGrowth ++ args)
  let environment = case libPaths of
        [] -> []
        xs -> [P.ExtraEnv "LD_LIBRARY_PATH" (P.PathLike xs)]
  return P.ProcessLauncher { P.procCmd = realCmd,
                             P.procArgs = realArgs,
                             P.procWorkingDir = workingDir,
                             P.procExtraEnvironment = environment ++
                                                      javaThroughputToEnv j throughputs ++
                                                      javaDaemonMinHeapToEnv j minHeap,
                             P.procInfile = infile,
                             P.procOutfile = outfile,
                             P.procSetupActions = [],
                             P.procStartedActions = [],
                             P.procCleanupActions = [] }


is64bit :: IO Bool
is64bit = do
  output <- readProcess "uname" ["-m"] ""
  return (T.unpack (T.strip (T.pack output)) == "x86_64")

javaVersion :: Java -> FilePath -> IO ()
javaVersion j outfile = do
  l <- javaProcessLauncher j (Just devnull) (Just outfile) "." ["-version"] Nothing Nothing Nothing [] Nothing False
  r <- P.start l
  P.wait r
