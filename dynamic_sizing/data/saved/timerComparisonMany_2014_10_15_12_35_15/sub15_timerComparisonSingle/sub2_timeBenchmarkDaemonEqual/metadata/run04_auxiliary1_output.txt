economemd
    total memory: 7338 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub15_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 14999: heap size 9 MB, throughput 0.98
Clients: 1
Client 14999 has a minimum heap size of 12 MB
Reading from 14998: heap size 9 MB, throughput 0.989626
Clients: 2
Client 14998 has a minimum heap size of 1211 MB
Reading from 14998: heap size 9 MB, throughput 0.960525
Reading from 14999: heap size 9 MB, throughput 0.984265
Reading from 14998: heap size 9 MB, throughput 0.96485
Reading from 14998: heap size 9 MB, throughput 0.942423
Reading from 14999: heap size 11 MB, throughput 0.985375
Reading from 14998: heap size 11 MB, throughput 0.939248
Reading from 14999: heap size 11 MB, throughput 0.97181
Reading from 14998: heap size 11 MB, throughput 0.979639
Reading from 14999: heap size 15 MB, throughput 0.978188
Reading from 14998: heap size 17 MB, throughput 0.850339
Reading from 14998: heap size 17 MB, throughput 0.524577
Reading from 14999: heap size 15 MB, throughput 0.983027
Reading from 14998: heap size 30 MB, throughput 0.964005
Reading from 14998: heap size 31 MB, throughput 0.890155
Reading from 14998: heap size 34 MB, throughput 0.384566
Reading from 14999: heap size 24 MB, throughput 0.938357
Reading from 14998: heap size 46 MB, throughput 0.890125
Reading from 14998: heap size 48 MB, throughput 0.884887
Reading from 14998: heap size 49 MB, throughput 0.916914
Reading from 14999: heap size 29 MB, throughput 0.990222
Reading from 14998: heap size 52 MB, throughput 0.17314
Reading from 14998: heap size 77 MB, throughput 0.611203
Reading from 14998: heap size 80 MB, throughput 0.773084
Reading from 14998: heap size 81 MB, throughput 0.722661
Reading from 14999: heap size 35 MB, throughput 0.988469
Reading from 14998: heap size 87 MB, throughput 0.696063
Reading from 14999: heap size 41 MB, throughput 0.992043
Reading from 14998: heap size 88 MB, throughput 0.23128
Reading from 14998: heap size 121 MB, throughput 0.757866
Reading from 14999: heap size 42 MB, throughput 0.976748
Reading from 14998: heap size 122 MB, throughput 0.628022
Reading from 14999: heap size 45 MB, throughput 0.969942
Reading from 14998: heap size 125 MB, throughput 0.67847
Reading from 14998: heap size 129 MB, throughput 0.650863
Reading from 14999: heap size 52 MB, throughput 0.985152
Reading from 14999: heap size 52 MB, throughput 0.980807
Reading from 14998: heap size 133 MB, throughput 0.17406
Reading from 14999: heap size 61 MB, throughput 0.933246
Reading from 14998: heap size 163 MB, throughput 0.543675
Reading from 14998: heap size 169 MB, throughput 0.687094
Reading from 14999: heap size 61 MB, throughput 0.980119
Reading from 14998: heap size 172 MB, throughput 0.671225
Reading from 14999: heap size 75 MB, throughput 0.534897
Reading from 14998: heap size 176 MB, throughput 0.572258
Reading from 14999: heap size 80 MB, throughput 0.990703
Reading from 14998: heap size 183 MB, throughput 0.137156
Reading from 14998: heap size 220 MB, throughput 0.580955
Reading from 14999: heap size 94 MB, throughput 0.99701
Reading from 14998: heap size 224 MB, throughput 0.534997
Reading from 14998: heap size 227 MB, throughput 0.71645
Reading from 14998: heap size 230 MB, throughput 0.632737
Reading from 14998: heap size 232 MB, throughput 0.62753
Reading from 14999: heap size 95 MB, throughput 0.997388
Reading from 14998: heap size 237 MB, throughput 0.128605
Reading from 14998: heap size 275 MB, throughput 0.595427
Reading from 14998: heap size 279 MB, throughput 0.622175
Reading from 14998: heap size 281 MB, throughput 0.564654
Reading from 14998: heap size 284 MB, throughput 0.60019
Reading from 14998: heap size 288 MB, throughput 0.564782
Reading from 14999: heap size 109 MB, throughput 0.998151
Reading from 14998: heap size 295 MB, throughput 0.707552
Reading from 14998: heap size 298 MB, throughput 0.114703
Reading from 14998: heap size 342 MB, throughput 0.520618
Reading from 14999: heap size 110 MB, throughput 0.997111
Reading from 14998: heap size 349 MB, throughput 0.601472
Reading from 14998: heap size 350 MB, throughput 0.602377
Reading from 14998: heap size 354 MB, throughput 0.489264
Reading from 14999: heap size 122 MB, throughput 0.99724
Reading from 14998: heap size 356 MB, throughput 0.103587
Reading from 14998: heap size 407 MB, throughput 0.564424
Reading from 14998: heap size 407 MB, throughput 0.589161
Reading from 14998: heap size 408 MB, throughput 0.641004
Reading from 14998: heap size 408 MB, throughput 0.56471
Reading from 14999: heap size 124 MB, throughput 0.997057
Reading from 14998: heap size 411 MB, throughput 0.567005
Reading from 14998: heap size 417 MB, throughput 0.481957
Equal recommendation: 3669 MB each
Reading from 14999: heap size 133 MB, throughput 0.995614
Reading from 14998: heap size 422 MB, throughput 0.100487
Reading from 14998: heap size 476 MB, throughput 0.52675
Reading from 14998: heap size 484 MB, throughput 0.551677
Reading from 14998: heap size 486 MB, throughput 0.55908
Reading from 14999: heap size 135 MB, throughput 0.997794
Reading from 14998: heap size 491 MB, throughput 0.138426
Reading from 14998: heap size 543 MB, throughput 0.500382
Reading from 14999: heap size 141 MB, throughput 0.998161
Reading from 14998: heap size 548 MB, throughput 0.567858
Reading from 14998: heap size 550 MB, throughput 0.555367
Reading from 14998: heap size 555 MB, throughput 0.611422
Reading from 14998: heap size 560 MB, throughput 0.587756
Reading from 14999: heap size 142 MB, throughput 0.997687
Reading from 14998: heap size 566 MB, throughput 0.132516
Reading from 14999: heap size 147 MB, throughput 0.998241
Reading from 14998: heap size 631 MB, throughput 0.53653
Reading from 14998: heap size 643 MB, throughput 0.619271
Reading from 14998: heap size 645 MB, throughput 0.557864
Reading from 14999: heap size 148 MB, throughput 0.997764
Reading from 14998: heap size 648 MB, throughput 0.816705
Reading from 14998: heap size 658 MB, throughput 0.861217
Reading from 14999: heap size 152 MB, throughput 0.998402
Reading from 14998: heap size 656 MB, throughput 0.816199
Reading from 14998: heap size 677 MB, throughput 0.825921
Reading from 14999: heap size 153 MB, throughput 0.998052
Reading from 14999: heap size 156 MB, throughput 0.997442
Reading from 14998: heap size 686 MB, throughput 0.0398438
Reading from 14998: heap size 763 MB, throughput 0.228928
Reading from 14998: heap size 771 MB, throughput 0.611293
Reading from 14998: heap size 774 MB, throughput 0.313263
Reading from 14998: heap size 759 MB, throughput 0.308248
Reading from 14999: heap size 157 MB, throughput 0.997544
Equal recommendation: 3669 MB each
Reading from 14998: heap size 659 MB, throughput 0.0340244
Reading from 14999: heap size 161 MB, throughput 0.997718
Reading from 14998: heap size 833 MB, throughput 0.274951
Reading from 14998: heap size 837 MB, throughput 0.48132
Reading from 14999: heap size 161 MB, throughput 0.998222
Reading from 14998: heap size 842 MB, throughput 0.227889
Reading from 14999: heap size 164 MB, throughput 0.998311
Reading from 14998: heap size 926 MB, throughput 0.434105
Reading from 14998: heap size 929 MB, throughput 0.706043
Reading from 14998: heap size 931 MB, throughput 0.709726
Reading from 14998: heap size 932 MB, throughput 0.882959
Reading from 14998: heap size 934 MB, throughput 0.835124
Reading from 14998: heap size 938 MB, throughput 0.944349
Reading from 14998: heap size 938 MB, throughput 0.883801
Reading from 14999: heap size 164 MB, throughput 0.997095
Reading from 14998: heap size 935 MB, throughput 0.936195
Reading from 14998: heap size 723 MB, throughput 0.72747
Reading from 14999: heap size 167 MB, throughput 0.994464
Reading from 14998: heap size 923 MB, throughput 0.866017
Reading from 14998: heap size 741 MB, throughput 0.841729
Reading from 14999: heap size 167 MB, throughput 0.993311
Reading from 14998: heap size 896 MB, throughput 0.750138
Reading from 14998: heap size 775 MB, throughput 0.792807
Reading from 14998: heap size 878 MB, throughput 0.810154
Reading from 14998: heap size 780 MB, throughput 0.806154
Reading from 14998: heap size 867 MB, throughput 0.826956
Reading from 14998: heap size 777 MB, throughput 0.755166
Reading from 14999: heap size 173 MB, throughput 0.997135
Reading from 14998: heap size 859 MB, throughput 0.849422
Reading from 14998: heap size 782 MB, throughput 0.83895
Reading from 14998: heap size 854 MB, throughput 0.862403
Reading from 14998: heap size 860 MB, throughput 0.862444
Reading from 14998: heap size 852 MB, throughput 0.981808
Reading from 14999: heap size 173 MB, throughput 0.997573
Reading from 14999: heap size 179 MB, throughput 0.997911
Reading from 14999: heap size 179 MB, throughput 0.989428
Reading from 14998: heap size 856 MB, throughput 0.620152
Reading from 14998: heap size 937 MB, throughput 0.740244
Reading from 14998: heap size 879 MB, throughput 0.818258
Equal recommendation: 3669 MB each
Reading from 14998: heap size 930 MB, throughput 0.822262
Reading from 14998: heap size 935 MB, throughput 0.796288
Reading from 14999: heap size 183 MB, throughput 0.998115
Reading from 14998: heap size 929 MB, throughput 0.839889
Reading from 14998: heap size 932 MB, throughput 0.81065
Reading from 14998: heap size 929 MB, throughput 0.84004
Reading from 14998: heap size 932 MB, throughput 0.850349
Reading from 14998: heap size 931 MB, throughput 0.808132
Reading from 14999: heap size 183 MB, throughput 0.997142
Reading from 14998: heap size 934 MB, throughput 0.829053
Reading from 14998: heap size 936 MB, throughput 0.84196
Reading from 14998: heap size 940 MB, throughput 0.8063
Reading from 14999: heap size 190 MB, throughput 0.995582
Reading from 14999: heap size 190 MB, throughput 0.987821
Reading from 14998: heap size 955 MB, throughput 0.104415
Reading from 14998: heap size 1063 MB, throughput 0.575577
Reading from 14998: heap size 1098 MB, throughput 0.840125
Reading from 14998: heap size 1101 MB, throughput 0.795717
Reading from 14998: heap size 1112 MB, throughput 0.822262
Reading from 14998: heap size 1112 MB, throughput 0.790671
Reading from 14998: heap size 1116 MB, throughput 0.792343
Reading from 14999: heap size 196 MB, throughput 0.998257
Reading from 14998: heap size 1119 MB, throughput 0.78863
Reading from 14998: heap size 1120 MB, throughput 0.76293
Reading from 14998: heap size 1124 MB, throughput 0.747238
Reading from 14998: heap size 1125 MB, throughput 0.754292
Reading from 14999: heap size 196 MB, throughput 0.997147
Reading from 14999: heap size 201 MB, throughput 0.997341
Reading from 14998: heap size 1129 MB, throughput 0.969502
Reading from 14999: heap size 202 MB, throughput 0.997193
Reading from 14998: heap size 1131 MB, throughput 0.961636
Equal recommendation: 3669 MB each
Reading from 14999: heap size 208 MB, throughput 0.997867
Reading from 14998: heap size 1136 MB, throughput 0.958677
Reading from 14999: heap size 208 MB, throughput 0.998024
Reading from 14999: heap size 212 MB, throughput 0.990897
Reading from 14999: heap size 213 MB, throughput 0.996174
Reading from 14998: heap size 1146 MB, throughput 0.964955
Reading from 14999: heap size 220 MB, throughput 0.998071
Reading from 14998: heap size 1148 MB, throughput 0.952528
Reading from 14999: heap size 221 MB, throughput 0.998148
Reading from 14998: heap size 1156 MB, throughput 0.962132
Reading from 14999: heap size 225 MB, throughput 0.998383
Reading from 14999: heap size 226 MB, throughput 0.998081
Reading from 14998: heap size 1159 MB, throughput 0.963823
Equal recommendation: 3669 MB each
Reading from 14999: heap size 229 MB, throughput 0.998306
Reading from 14998: heap size 1166 MB, throughput 0.962157
Reading from 14999: heap size 230 MB, throughput 0.997806
Reading from 14998: heap size 1167 MB, throughput 0.957023
Reading from 14999: heap size 233 MB, throughput 0.998101
Reading from 14999: heap size 222 MB, throughput 0.99753
Reading from 14998: heap size 1172 MB, throughput 0.963313
Reading from 14999: heap size 213 MB, throughput 0.997767
Reading from 14998: heap size 1175 MB, throughput 0.956713
Reading from 14999: heap size 203 MB, throughput 0.99781
Reading from 14999: heap size 194 MB, throughput 0.997492
Reading from 14998: heap size 1181 MB, throughput 0.943592
Reading from 14999: heap size 186 MB, throughput 0.996735
Equal recommendation: 3669 MB each
Reading from 14999: heap size 178 MB, throughput 0.989148
Reading from 14999: heap size 184 MB, throughput 0.894607
Reading from 14998: heap size 1185 MB, throughput 0.959303
Reading from 14999: heap size 191 MB, throughput 0.998913
Reading from 14999: heap size 197 MB, throughput 0.998709
Reading from 14998: heap size 1191 MB, throughput 0.963145
Reading from 14999: heap size 203 MB, throughput 0.999077
Reading from 14998: heap size 1195 MB, throughput 0.959827
Reading from 14999: heap size 213 MB, throughput 0.998901
Reading from 14999: heap size 214 MB, throughput 0.999072
Reading from 14998: heap size 1200 MB, throughput 0.961671
Reading from 14999: heap size 218 MB, throughput 0.999036
Reading from 14998: heap size 1206 MB, throughput 0.959873
Equal recommendation: 3669 MB each
Reading from 14999: heap size 221 MB, throughput 0.999029
Reading from 14999: heap size 222 MB, throughput 0.99885
Reading from 14998: heap size 1213 MB, throughput 0.956511
Reading from 14999: heap size 224 MB, throughput 0.999105
Reading from 14999: heap size 225 MB, throughput 0.99891
Reading from 14998: heap size 1216 MB, throughput 0.957906
Reading from 14999: heap size 227 MB, throughput 0.998965
Reading from 14998: heap size 1222 MB, throughput 0.962037
Reading from 14999: heap size 228 MB, throughput 0.998273
Reading from 14999: heap size 230 MB, throughput 0.994205
Reading from 14999: heap size 230 MB, throughput 0.996761
Reading from 14999: heap size 236 MB, throughput 0.998
Equal recommendation: 3669 MB each
Reading from 14998: heap size 1224 MB, throughput 0.563131
Reading from 14999: heap size 237 MB, throughput 0.997909
Reading from 14998: heap size 1290 MB, throughput 0.991618
Reading from 14999: heap size 242 MB, throughput 0.998427
Reading from 14999: heap size 243 MB, throughput 0.998033
Reading from 14998: heap size 1290 MB, throughput 0.989199
Reading from 14999: heap size 247 MB, throughput 0.998053
Reading from 14998: heap size 1300 MB, throughput 0.9869
Reading from 14999: heap size 248 MB, throughput 0.997628
Reading from 14999: heap size 253 MB, throughput 0.998071
Reading from 14998: heap size 1304 MB, throughput 0.982346
Equal recommendation: 3669 MB each
Reading from 14999: heap size 253 MB, throughput 0.997825
Reading from 14998: heap size 1304 MB, throughput 0.980574
Reading from 14999: heap size 259 MB, throughput 0.998205
Reading from 14999: heap size 259 MB, throughput 0.997912
Reading from 14998: heap size 1190 MB, throughput 0.977852
Reading from 14999: heap size 264 MB, throughput 0.993072
Reading from 14999: heap size 264 MB, throughput 0.997159
Reading from 14998: heap size 1294 MB, throughput 0.977613
Reading from 14999: heap size 274 MB, throughput 0.997994
Reading from 14998: heap size 1210 MB, throughput 0.976806
Reading from 14999: heap size 274 MB, throughput 0.997947
Equal recommendation: 3669 MB each
Reading from 14999: heap size 280 MB, throughput 0.998487
Reading from 14998: heap size 1284 MB, throughput 0.973982
Reading from 14999: heap size 281 MB, throughput 0.99804
Reading from 14998: heap size 1290 MB, throughput 0.972292
Reading from 14999: heap size 287 MB, throughput 0.998148
Reading from 14998: heap size 1290 MB, throughput 0.96885
Reading from 14999: heap size 287 MB, throughput 0.998021
Reading from 14999: heap size 294 MB, throughput 0.998167
Reading from 14998: heap size 1291 MB, throughput 0.970325
Reading from 14999: heap size 294 MB, throughput 0.998041
Equal recommendation: 3669 MB each
Reading from 14999: heap size 300 MB, throughput 0.995759
Reading from 14998: heap size 1294 MB, throughput 0.962347
Reading from 14999: heap size 300 MB, throughput 0.995996
Reading from 14998: heap size 1298 MB, throughput 0.965134
Reading from 14999: heap size 309 MB, throughput 0.998144
Reading from 14999: heap size 309 MB, throughput 0.99785
Reading from 14998: heap size 1301 MB, throughput 0.961869
Reading from 14999: heap size 316 MB, throughput 0.998144
Reading from 14998: heap size 1309 MB, throughput 0.958897
Reading from 14999: heap size 317 MB, throughput 0.997776
Equal recommendation: 3669 MB each
Reading from 14998: heap size 1317 MB, throughput 0.961729
Reading from 14999: heap size 324 MB, throughput 0.998277
Reading from 14998: heap size 1323 MB, throughput 0.959705
Reading from 14999: heap size 324 MB, throughput 0.998356
Reading from 14999: heap size 331 MB, throughput 0.998076
Reading from 14998: heap size 1331 MB, throughput 0.954956
Reading from 14999: heap size 331 MB, throughput 0.996616
Reading from 14999: heap size 337 MB, throughput 0.996073
Reading from 14998: heap size 1334 MB, throughput 0.961445
Equal recommendation: 3669 MB each
Reading from 14999: heap size 338 MB, throughput 0.998061
Reading from 14998: heap size 1342 MB, throughput 0.962637
Reading from 14999: heap size 346 MB, throughput 0.998202
Reading from 14998: heap size 1344 MB, throughput 0.961217
Reading from 14999: heap size 347 MB, throughput 0.998095
Reading from 14998: heap size 1351 MB, throughput 0.961139
Reading from 14999: heap size 354 MB, throughput 0.998068
Reading from 14999: heap size 355 MB, throughput 0.998057
Equal recommendation: 3669 MB each
Reading from 14999: heap size 363 MB, throughput 0.998246
Reading from 14999: heap size 363 MB, throughput 0.997818
Reading from 14999: heap size 371 MB, throughput 0.996339
Reading from 14999: heap size 371 MB, throughput 0.998187
Reading from 14999: heap size 380 MB, throughput 0.998357
Equal recommendation: 3669 MB each
Reading from 14999: heap size 380 MB, throughput 0.998136
Reading from 14999: heap size 388 MB, throughput 0.99831
Reading from 14999: heap size 389 MB, throughput 0.998011
Reading from 14998: heap size 1352 MB, throughput 0.884067
Reading from 14999: heap size 396 MB, throughput 0.998377
Equal recommendation: 3669 MB each
Reading from 14999: heap size 396 MB, throughput 0.994404
Reading from 14999: heap size 403 MB, throughput 0.998575
Reading from 14999: heap size 404 MB, throughput 0.998595
Reading from 14999: heap size 413 MB, throughput 0.996623
Equal recommendation: 3669 MB each
Reading from 14998: heap size 1479 MB, throughput 0.98669
Reading from 14999: heap size 414 MB, throughput 0.998509
Reading from 14999: heap size 422 MB, throughput 0.998445
Reading from 14998: heap size 1522 MB, throughput 0.98511
Reading from 14999: heap size 423 MB, throughput 0.998508
Reading from 14998: heap size 1514 MB, throughput 0.868563
Reading from 14998: heap size 1522 MB, throughput 0.73701
Reading from 14998: heap size 1509 MB, throughput 0.635858
Reading from 14998: heap size 1530 MB, throughput 0.700368
Reading from 14999: heap size 432 MB, throughput 0.940243
Reading from 14998: heap size 1556 MB, throughput 0.716346
Reading from 14998: heap size 1568 MB, throughput 0.720311
Reading from 14998: heap size 1598 MB, throughput 0.731487
Reading from 14998: heap size 1604 MB, throughput 0.904114
Equal recommendation: 3669 MB each
Reading from 14999: heap size 440 MB, throughput 0.999319
Reading from 14998: heap size 1627 MB, throughput 0.977189
Reading from 14999: heap size 452 MB, throughput 0.99927
Reading from 14998: heap size 1632 MB, throughput 0.975104
Reading from 14999: heap size 452 MB, throughput 0.999202
Reading from 14998: heap size 1636 MB, throughput 0.973954
Reading from 14999: heap size 462 MB, throughput 0.99919
Equal recommendation: 3669 MB each
Reading from 14998: heap size 1646 MB, throughput 0.975047
Reading from 14999: heap size 462 MB, throughput 0.999337
Reading from 14998: heap size 1639 MB, throughput 0.971457
Reading from 14999: heap size 468 MB, throughput 0.997729
Reading from 14999: heap size 470 MB, throughput 0.99842
Reading from 14998: heap size 1650 MB, throughput 0.973218
Reading from 14999: heap size 481 MB, throughput 0.998812
Equal recommendation: 3669 MB each
Reading from 14998: heap size 1658 MB, throughput 0.979059
Reading from 14999: heap size 481 MB, throughput 0.998634
Reading from 14998: heap size 1660 MB, throughput 0.979159
Reading from 14999: heap size 489 MB, throughput 0.998878
Reading from 14998: heap size 1663 MB, throughput 0.972633
Reading from 14999: heap size 490 MB, throughput 0.998675
Equal recommendation: 3669 MB each
Reading from 14998: heap size 1668 MB, throughput 0.97329
Reading from 14999: heap size 498 MB, throughput 0.996973
Reading from 14999: heap size 499 MB, throughput 0.998415
Reading from 14998: heap size 1655 MB, throughput 0.976182
Reading from 14999: heap size 511 MB, throughput 0.998608
Reading from 14998: heap size 1664 MB, throughput 0.973545
Equal recommendation: 3669 MB each
Reading from 14999: heap size 512 MB, throughput 0.998394
Reading from 14998: heap size 1649 MB, throughput 0.971967
Reading from 14999: heap size 522 MB, throughput 0.998542
Reading from 14998: heap size 1657 MB, throughput 0.970607
Reading from 14999: heap size 523 MB, throughput 0.998559
Reading from 14999: heap size 534 MB, throughput 0.996489
Reading from 14998: heap size 1663 MB, throughput 0.971291
Equal recommendation: 3669 MB each
Reading from 14999: heap size 534 MB, throughput 0.998338
Reading from 14998: heap size 1664 MB, throughput 0.965946
Reading from 14999: heap size 548 MB, throughput 0.998376
Reading from 14998: heap size 1675 MB, throughput 0.968447
Reading from 14999: heap size 549 MB, throughput 0.998466
Equal recommendation: 3669 MB each
Reading from 14998: heap size 1678 MB, throughput 0.766061
Reading from 14999: heap size 561 MB, throughput 0.998534
Reading from 14999: heap size 562 MB, throughput 0.996572
Reading from 14998: heap size 1632 MB, throughput 0.993274
Reading from 14999: heap size 574 MB, throughput 0.998579
Reading from 14998: heap size 1635 MB, throughput 0.990382
Equal recommendation: 3669 MB each
Reading from 14999: heap size 575 MB, throughput 0.998372
Reading from 14998: heap size 1652 MB, throughput 0.988489
Reading from 14999: heap size 588 MB, throughput 0.998433
Reading from 14998: heap size 1658 MB, throughput 0.98646
Reading from 14999: heap size 589 MB, throughput 0.998365
Equal recommendation: 3669 MB each
Reading from 14998: heap size 1657 MB, throughput 0.986472
Client 14999 died
Clients: 1
Reading from 14998: heap size 1662 MB, throughput 0.983828
Reading from 14998: heap size 1643 MB, throughput 0.98232
Recommendation: one client; give it all the memory
Reading from 14998: heap size 1531 MB, throughput 0.980086
Reading from 14998: heap size 1637 MB, throughput 0.97903
Reading from 14998: heap size 1644 MB, throughput 0.977047
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 14998: heap size 1649 MB, throughput 0.989349
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 14998: heap size 1651 MB, throughput 0.991394
Reading from 14998: heap size 1658 MB, throughput 0.868005
Reading from 14998: heap size 1693 MB, throughput 0.738257
Reading from 14998: heap size 1734 MB, throughput 0.766569
Reading from 14998: heap size 1762 MB, throughput 0.754555
Reading from 14998: heap size 1806 MB, throughput 0.806205
Client 14998 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
