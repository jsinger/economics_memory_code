#!/usr/bin/env python

import staticutility as su

d = su.UtilityAggregateDir(str(su.outputDir), su.allUtilitySpaceDirs())
d.tabulateAndSave()
