economemd
    total memory: 1728 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub60_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run09_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 20078: heap size 9 MB, throughput 0.988036
Clients: 1
Client 20078 has a minimum heap size of 12 MB
Reading from 20077: heap size 9 MB, throughput 0.985192
Clients: 2
Client 20077 has a minimum heap size of 276 MB
Reading from 20078: heap size 9 MB, throughput 0.98469
Reading from 20077: heap size 9 MB, throughput 0.962934
Reading from 20077: heap size 11 MB, throughput 0.977583
Reading from 20078: heap size 11 MB, throughput 0.986285
Reading from 20078: heap size 11 MB, throughput 0.959049
Reading from 20077: heap size 11 MB, throughput 0.985156
Reading from 20077: heap size 15 MB, throughput 0.805138
Reading from 20078: heap size 15 MB, throughput 0.973139
Reading from 20077: heap size 19 MB, throughput 0.960103
Reading from 20077: heap size 23 MB, throughput 0.9479
Reading from 20078: heap size 15 MB, throughput 0.97351
Reading from 20077: heap size 27 MB, throughput 0.886393
Reading from 20077: heap size 28 MB, throughput 0.371368
Reading from 20077: heap size 38 MB, throughput 0.865575
Reading from 20077: heap size 42 MB, throughput 0.814442
Reading from 20077: heap size 43 MB, throughput 0.841104
Reading from 20078: heap size 24 MB, throughput 0.892247
Reading from 20077: heap size 48 MB, throughput 0.347197
Reading from 20077: heap size 62 MB, throughput 0.822016
Reading from 20077: heap size 69 MB, throughput 0.728085
Reading from 20078: heap size 30 MB, throughput 0.989875
Reading from 20077: heap size 70 MB, throughput 0.792491
Reading from 20077: heap size 74 MB, throughput 0.230208
Reading from 20078: heap size 35 MB, throughput 0.985971
Reading from 20077: heap size 95 MB, throughput 0.77474
Reading from 20077: heap size 101 MB, throughput 0.762696
Reading from 20077: heap size 103 MB, throughput 0.601742
Reading from 20078: heap size 40 MB, throughput 0.979344
Reading from 20078: heap size 42 MB, throughput 0.985989
Reading from 20077: heap size 107 MB, throughput 0.150116
Reading from 20077: heap size 135 MB, throughput 0.631318
Reading from 20078: heap size 45 MB, throughput 0.964281
Reading from 20078: heap size 50 MB, throughput 0.839056
Reading from 20077: heap size 141 MB, throughput 0.717001
Reading from 20077: heap size 142 MB, throughput 0.729408
Reading from 20078: heap size 50 MB, throughput 0.953886
Reading from 20077: heap size 147 MB, throughput 0.640936
Reading from 20078: heap size 61 MB, throughput 0.989802
Reading from 20078: heap size 62 MB, throughput 0.760027
Reading from 20077: heap size 150 MB, throughput 0.118516
Reading from 20078: heap size 76 MB, throughput 0.987996
Reading from 20077: heap size 183 MB, throughput 0.497636
Reading from 20077: heap size 187 MB, throughput 0.676069
Reading from 20078: heap size 77 MB, throughput 0.989342
Reading from 20077: heap size 189 MB, throughput 0.696831
Reading from 20077: heap size 192 MB, throughput 0.855115
Reading from 20078: heap size 86 MB, throughput 0.997749
Reading from 20077: heap size 195 MB, throughput 0.867309
Reading from 20078: heap size 87 MB, throughput 0.996825
Reading from 20077: heap size 204 MB, throughput 0.184994
Reading from 20077: heap size 252 MB, throughput 0.619256
Reading from 20078: heap size 93 MB, throughput 0.996123
Reading from 20077: heap size 256 MB, throughput 0.780967
Reading from 20077: heap size 259 MB, throughput 0.661455
Reading from 20077: heap size 262 MB, throughput 0.75538
Reading from 20077: heap size 266 MB, throughput 0.765122
Reading from 20077: heap size 268 MB, throughput 0.743565
Reading from 20078: heap size 95 MB, throughput 0.987712
Reading from 20077: heap size 269 MB, throughput 0.566942
Reading from 20077: heap size 274 MB, throughput 0.646916
Reading from 20077: heap size 280 MB, throughput 0.601026
Reading from 20077: heap size 283 MB, throughput 0.708065
Reading from 20078: heap size 101 MB, throughput 0.994704
Reading from 20077: heap size 280 MB, throughput 0.10425
Reading from 20078: heap size 102 MB, throughput 0.994679
Reading from 20077: heap size 326 MB, throughput 0.904406
Reading from 20077: heap size 324 MB, throughput 0.863954
Reading from 20077: heap size 326 MB, throughput 0.748858
Reading from 20077: heap size 327 MB, throughput 0.783915
Reading from 20077: heap size 328 MB, throughput 0.847271
Reading from 20077: heap size 329 MB, throughput 0.859723
Reading from 20078: heap size 108 MB, throughput 0.994993
Reading from 20077: heap size 331 MB, throughput 0.829393
Reading from 20077: heap size 334 MB, throughput 0.610057
Reading from 20077: heap size 335 MB, throughput 0.681785
Equal recommendation: 864 MB each
Reading from 20078: heap size 108 MB, throughput 0.994603
Reading from 20077: heap size 335 MB, throughput 0.108244
Reading from 20077: heap size 381 MB, throughput 0.501175
Reading from 20077: heap size 385 MB, throughput 0.82398
Reading from 20077: heap size 386 MB, throughput 0.758644
Reading from 20078: heap size 112 MB, throughput 0.992187
Reading from 20078: heap size 113 MB, throughput 0.994274
Reading from 20077: heap size 394 MB, throughput 0.983305
Reading from 20078: heap size 117 MB, throughput 0.995353
Reading from 20078: heap size 117 MB, throughput 0.992783
Reading from 20077: heap size 394 MB, throughput 0.983862
Reading from 20078: heap size 121 MB, throughput 0.996517
Reading from 20078: heap size 121 MB, throughput 0.995562
Reading from 20077: heap size 393 MB, throughput 0.97693
Reading from 20078: heap size 124 MB, throughput 0.995139
Reading from 20078: heap size 124 MB, throughput 0.996402
Reading from 20077: heap size 397 MB, throughput 0.984713
Reading from 20078: heap size 127 MB, throughput 0.997384
Reading from 20077: heap size 393 MB, throughput 0.977606
Reading from 20078: heap size 127 MB, throughput 0.997468
Reading from 20078: heap size 130 MB, throughput 0.997626
Reading from 20078: heap size 130 MB, throughput 0.998094
Reading from 20077: heap size 396 MB, throughput 0.980263
Equal recommendation: 864 MB each
Reading from 20078: heap size 131 MB, throughput 0.998193
Reading from 20078: heap size 132 MB, throughput 0.997986
Reading from 20077: heap size 392 MB, throughput 0.982492
Reading from 20078: heap size 134 MB, throughput 0.998434
Reading from 20078: heap size 134 MB, throughput 0.99832
Reading from 20077: heap size 395 MB, throughput 0.980034
Reading from 20078: heap size 135 MB, throughput 0.99684
Reading from 20078: heap size 135 MB, throughput 0.992648
Reading from 20077: heap size 397 MB, throughput 0.940819
Reading from 20078: heap size 137 MB, throughput 0.988318
Reading from 20078: heap size 137 MB, throughput 0.996097
Reading from 20077: heap size 398 MB, throughput 0.968968
Reading from 20078: heap size 142 MB, throughput 0.997413
Reading from 20078: heap size 142 MB, throughput 0.996989
Reading from 20077: heap size 396 MB, throughput 0.980956
Reading from 20078: heap size 144 MB, throughput 0.995988
Reading from 20078: heap size 145 MB, throughput 0.996168
Reading from 20077: heap size 399 MB, throughput 0.977302
Equal recommendation: 864 MB each
Reading from 20078: heap size 148 MB, throughput 0.995091
Reading from 20078: heap size 148 MB, throughput 0.997058
Reading from 20077: heap size 400 MB, throughput 0.975263
Reading from 20078: heap size 150 MB, throughput 0.997298
Reading from 20077: heap size 400 MB, throughput 0.973587
Reading from 20078: heap size 150 MB, throughput 0.997228
Reading from 20078: heap size 152 MB, throughput 0.995592
Reading from 20077: heap size 399 MB, throughput 0.981055
Reading from 20077: heap size 403 MB, throughput 0.84302
Reading from 20077: heap size 401 MB, throughput 0.833921
Reading from 20077: heap size 404 MB, throughput 0.82731
Reading from 20077: heap size 411 MB, throughput 0.850262
Reading from 20078: heap size 152 MB, throughput 0.997076
Reading from 20077: heap size 412 MB, throughput 0.978097
Reading from 20078: heap size 155 MB, throughput 0.996374
Reading from 20078: heap size 155 MB, throughput 0.996114
Reading from 20077: heap size 418 MB, throughput 0.985955
Reading from 20078: heap size 157 MB, throughput 0.997349
Reading from 20078: heap size 157 MB, throughput 0.997606
Reading from 20077: heap size 419 MB, throughput 0.990058
Reading from 20078: heap size 159 MB, throughput 0.99862
Equal recommendation: 864 MB each
Reading from 20078: heap size 159 MB, throughput 0.998612
Reading from 20077: heap size 420 MB, throughput 0.987074
Reading from 20078: heap size 161 MB, throughput 0.998619
Reading from 20078: heap size 161 MB, throughput 0.992741
Reading from 20078: heap size 162 MB, throughput 0.993371
Reading from 20077: heap size 422 MB, throughput 0.979423
Reading from 20078: heap size 163 MB, throughput 0.993099
Reading from 20078: heap size 166 MB, throughput 0.99776
Reading from 20077: heap size 421 MB, throughput 0.986844
Reading from 20078: heap size 166 MB, throughput 0.99752
Reading from 20078: heap size 168 MB, throughput 0.997452
Reading from 20077: heap size 423 MB, throughput 0.984637
Reading from 20078: heap size 169 MB, throughput 0.99653
Reading from 20077: heap size 419 MB, throughput 0.985666
Reading from 20078: heap size 171 MB, throughput 0.996733
Reading from 20078: heap size 171 MB, throughput 0.997138
Equal recommendation: 864 MB each
Reading from 20077: heap size 421 MB, throughput 0.985821
Reading from 20078: heap size 173 MB, throughput 0.997854
Reading from 20078: heap size 173 MB, throughput 0.996384
Reading from 20077: heap size 418 MB, throughput 0.983214
Reading from 20078: heap size 176 MB, throughput 0.997514
Reading from 20078: heap size 176 MB, throughput 0.99717
Reading from 20077: heap size 420 MB, throughput 0.984209
Reading from 20078: heap size 177 MB, throughput 0.997457
Reading from 20078: heap size 178 MB, throughput 0.997066
Reading from 20077: heap size 417 MB, throughput 0.990711
Reading from 20077: heap size 419 MB, throughput 0.941482
Reading from 20077: heap size 419 MB, throughput 0.885126
Reading from 20077: heap size 420 MB, throughput 0.873441
Reading from 20077: heap size 425 MB, throughput 0.91921
Reading from 20078: heap size 180 MB, throughput 0.997048
Reading from 20078: heap size 180 MB, throughput 0.998079
Reading from 20077: heap size 426 MB, throughput 0.992287
Reading from 20078: heap size 182 MB, throughput 0.934653
Equal recommendation: 864 MB each
Reading from 20078: heap size 184 MB, throughput 0.992745
Reading from 20078: heap size 186 MB, throughput 0.992653
Reading from 20077: heap size 432 MB, throughput 0.991604
Reading from 20078: heap size 187 MB, throughput 0.997775
Reading from 20078: heap size 190 MB, throughput 0.998044
Reading from 20077: heap size 432 MB, throughput 0.989672
Reading from 20078: heap size 191 MB, throughput 0.996249
Reading from 20078: heap size 194 MB, throughput 0.998399
Reading from 20077: heap size 434 MB, throughput 0.990054
Reading from 20078: heap size 195 MB, throughput 0.997638
Reading from 20077: heap size 435 MB, throughput 0.991005
Reading from 20078: heap size 198 MB, throughput 0.998196
Reading from 20078: heap size 198 MB, throughput 0.997584
Reading from 20077: heap size 432 MB, throughput 0.989206
Equal recommendation: 864 MB each
Reading from 20078: heap size 201 MB, throughput 0.998077
Reading from 20078: heap size 201 MB, throughput 0.997718
Reading from 20077: heap size 434 MB, throughput 0.98747
Reading from 20078: heap size 204 MB, throughput 0.998086
Reading from 20077: heap size 432 MB, throughput 0.987421
Reading from 20078: heap size 204 MB, throughput 0.996666
Reading from 20078: heap size 207 MB, throughput 0.998047
Reading from 20077: heap size 433 MB, throughput 0.985884
Reading from 20078: heap size 207 MB, throughput 0.997698
Reading from 20078: heap size 210 MB, throughput 0.993899
Reading from 20078: heap size 210 MB, throughput 0.990708
Reading from 20077: heap size 435 MB, throughput 0.987957
Reading from 20078: heap size 214 MB, throughput 0.998242
Reading from 20077: heap size 435 MB, throughput 0.967646
Reading from 20077: heap size 434 MB, throughput 0.897997
Reading from 20077: heap size 436 MB, throughput 0.894748
Equal recommendation: 864 MB each
Reading from 20077: heap size 440 MB, throughput 0.981277
Reading from 20078: heap size 215 MB, throughput 0.998008
Reading from 20078: heap size 219 MB, throughput 0.998152
Reading from 20077: heap size 441 MB, throughput 0.992128
Reading from 20078: heap size 220 MB, throughput 0.997295
Reading from 20078: heap size 223 MB, throughput 0.997295
Reading from 20077: heap size 445 MB, throughput 0.992986
Reading from 20078: heap size 223 MB, throughput 0.997876
Reading from 20078: heap size 227 MB, throughput 0.9983
Reading from 20077: heap size 446 MB, throughput 0.991937
Reading from 20078: heap size 227 MB, throughput 0.996527
Equal recommendation: 864 MB each
Reading from 20077: heap size 446 MB, throughput 0.991735
Reading from 20078: heap size 230 MB, throughput 0.99813
Reading from 20078: heap size 230 MB, throughput 0.997574
Reading from 20077: heap size 447 MB, throughput 0.991006
Reading from 20078: heap size 234 MB, throughput 0.997721
Reading from 20078: heap size 234 MB, throughput 0.996697
Reading from 20078: heap size 237 MB, throughput 0.992314
Reading from 20077: heap size 444 MB, throughput 0.990834
Reading from 20078: heap size 237 MB, throughput 0.997342
Reading from 20077: heap size 446 MB, throughput 0.987474
Reading from 20078: heap size 242 MB, throughput 0.997974
Reading from 20078: heap size 243 MB, throughput 0.997012
Equal recommendation: 864 MB each
Reading from 20077: heap size 447 MB, throughput 0.98777
Reading from 20078: heap size 248 MB, throughput 0.998463
Reading from 20077: heap size 447 MB, throughput 0.991717
Reading from 20077: heap size 449 MB, throughput 0.936558
Reading from 20078: heap size 248 MB, throughput 0.998145
Reading from 20077: heap size 449 MB, throughput 0.257515
Reading from 20077: heap size 478 MB, throughput 0.995952
Reading from 20078: heap size 250 MB, throughput 0.998377
Reading from 20077: heap size 479 MB, throughput 0.995759
Reading from 20078: heap size 251 MB, throughput 0.998166
Reading from 20077: heap size 487 MB, throughput 0.993533
Reading from 20078: heap size 254 MB, throughput 0.997731
Reading from 20078: heap size 255 MB, throughput 0.997995
Reading from 20077: heap size 489 MB, throughput 0.994741
Equal recommendation: 864 MB each
Reading from 20078: heap size 258 MB, throughput 0.998413
Reading from 20078: heap size 258 MB, throughput 0.996184
Reading from 20077: heap size 489 MB, throughput 0.99174
Reading from 20078: heap size 262 MB, throughput 0.994074
Reading from 20078: heap size 262 MB, throughput 0.997801
Reading from 20077: heap size 491 MB, throughput 0.991916
Reading from 20078: heap size 267 MB, throughput 0.997357
Reading from 20077: heap size 485 MB, throughput 0.991836
Reading from 20078: heap size 267 MB, throughput 0.998044
Reading from 20078: heap size 271 MB, throughput 0.997795
Equal recommendation: 864 MB each
Reading from 20077: heap size 488 MB, throughput 0.989666
Reading from 20078: heap size 272 MB, throughput 0.998209
Reading from 20077: heap size 489 MB, throughput 0.989064
Reading from 20078: heap size 274 MB, throughput 0.998342
Reading from 20077: heap size 490 MB, throughput 0.989527
Reading from 20077: heap size 493 MB, throughput 0.911876
Reading from 20078: heap size 275 MB, throughput 0.998241
Reading from 20077: heap size 494 MB, throughput 0.868689
Reading from 20077: heap size 500 MB, throughput 0.979049
Reading from 20078: heap size 279 MB, throughput 0.998436
Reading from 20077: heap size 505 MB, throughput 0.99214
Reading from 20078: heap size 279 MB, throughput 0.99821
Reading from 20078: heap size 283 MB, throughput 0.993238
Equal recommendation: 864 MB each
Reading from 20078: heap size 283 MB, throughput 0.996458
Reading from 20077: heap size 501 MB, throughput 0.990666
Reading from 20078: heap size 288 MB, throughput 0.998227
Reading from 20077: heap size 505 MB, throughput 0.990939
Reading from 20078: heap size 289 MB, throughput 0.997848
Reading from 20077: heap size 507 MB, throughput 0.990204
Reading from 20078: heap size 293 MB, throughput 0.997667
Reading from 20078: heap size 293 MB, throughput 0.998126
Reading from 20077: heap size 508 MB, throughput 0.988892
Reading from 20078: heap size 298 MB, throughput 0.99842
Equal recommendation: 864 MB each
Reading from 20077: heap size 511 MB, throughput 0.988006
Reading from 20078: heap size 298 MB, throughput 0.997359
Reading from 20077: heap size 512 MB, throughput 0.98838
Reading from 20078: heap size 302 MB, throughput 0.998498
Reading from 20077: heap size 515 MB, throughput 0.991752
Reading from 20078: heap size 302 MB, throughput 0.998196
Reading from 20077: heap size 516 MB, throughput 0.831875
Reading from 20077: heap size 515 MB, throughput 0.885328
Reading from 20078: heap size 305 MB, throughput 0.99309
Reading from 20077: heap size 516 MB, throughput 0.988061
Reading from 20078: heap size 305 MB, throughput 0.997573
Equal recommendation: 864 MB each
Reading from 20078: heap size 312 MB, throughput 0.998428
Reading from 20077: heap size 525 MB, throughput 0.994577
Reading from 20078: heap size 312 MB, throughput 0.998063
Reading from 20077: heap size 526 MB, throughput 0.993091
Reading from 20078: heap size 316 MB, throughput 0.998461
Reading from 20078: heap size 317 MB, throughput 0.998072
Reading from 20077: heap size 528 MB, throughput 0.992061
Reading from 20078: heap size 321 MB, throughput 0.998503
Reading from 20077: heap size 530 MB, throughput 0.992782
Equal recommendation: 864 MB each
Reading from 20078: heap size 321 MB, throughput 0.998406
Reading from 20077: heap size 528 MB, throughput 0.991522
Reading from 20078: heap size 325 MB, throughput 0.995827
Reading from 20078: heap size 325 MB, throughput 0.927127
Reading from 20077: heap size 531 MB, throughput 0.990017
Reading from 20078: heap size 338 MB, throughput 0.998732
Reading from 20077: heap size 532 MB, throughput 0.992432
Reading from 20077: heap size 533 MB, throughput 0.928537
Reading from 20078: heap size 339 MB, throughput 0.999054
Reading from 20077: heap size 534 MB, throughput 0.928439
Reading from 20078: heap size 344 MB, throughput 0.998651
Reading from 20077: heap size 536 MB, throughput 0.992258
Equal recommendation: 864 MB each
Reading from 20078: heap size 345 MB, throughput 0.998944
Reading from 20077: heap size 541 MB, throughput 0.994038
Reading from 20078: heap size 349 MB, throughput 0.998845
Reading from 20078: heap size 350 MB, throughput 0.999072
Reading from 20077: heap size 543 MB, throughput 0.994213
Reading from 20078: heap size 354 MB, throughput 0.999227
Reading from 20077: heap size 542 MB, throughput 0.99331
Equal recommendation: 864 MB each
Reading from 20078: heap size 354 MB, throughput 0.998197
Reading from 20078: heap size 358 MB, throughput 0.99621
Reading from 20077: heap size 544 MB, throughput 0.991967
Reading from 20078: heap size 358 MB, throughput 0.998458
Reading from 20077: heap size 545 MB, throughput 0.991096
Reading from 20078: heap size 364 MB, throughput 0.99851
Reading from 20078: heap size 365 MB, throughput 0.998482
Reading from 20077: heap size 545 MB, throughput 0.994862
Reading from 20077: heap size 545 MB, throughput 0.945478
Reading from 20077: heap size 547 MB, throughput 0.916156
Equal recommendation: 864 MB each
Reading from 20078: heap size 370 MB, throughput 0.998355
Reading from 20077: heap size 553 MB, throughput 0.992313
Reading from 20078: heap size 370 MB, throughput 0.9984
Reading from 20077: heap size 554 MB, throughput 0.993856
Reading from 20078: heap size 375 MB, throughput 0.998553
Reading from 20078: heap size 375 MB, throughput 0.997637
Reading from 20078: heap size 380 MB, throughput 0.996081
Reading from 20077: heap size 556 MB, throughput 0.993337
Equal recommendation: 864 MB each
Reading from 20078: heap size 380 MB, throughput 0.998403
Reading from 20077: heap size 558 MB, throughput 0.992404
Reading from 20078: heap size 387 MB, throughput 0.998683
Reading from 20077: heap size 557 MB, throughput 0.993174
Reading from 20078: heap size 387 MB, throughput 0.998418
Reading from 20078: heap size 393 MB, throughput 0.998704
Reading from 20077: heap size 559 MB, throughput 0.992038
Equal recommendation: 864 MB each
Reading from 20077: heap size 561 MB, throughput 0.989442
Reading from 20078: heap size 393 MB, throughput 0.9983
Reading from 20077: heap size 562 MB, throughput 0.925711
Reading from 20077: heap size 566 MB, throughput 0.989289
Reading from 20078: heap size 398 MB, throughput 0.997992
Reading from 20078: heap size 398 MB, throughput 0.993001
Reading from 20077: heap size 567 MB, throughput 0.994913
Reading from 20078: heap size 402 MB, throughput 0.998308
Reading from 20077: heap size 570 MB, throughput 0.993126
Reading from 20078: heap size 403 MB, throughput 0.997557
Equal recommendation: 864 MB each
Reading from 20078: heap size 411 MB, throughput 0.998049
Reading from 20077: heap size 572 MB, throughput 0.994299
Reading from 20078: heap size 411 MB, throughput 0.997383
Reading from 20077: heap size 570 MB, throughput 0.993362
Reading from 20078: heap size 419 MB, throughput 0.998491
Equal recommendation: 864 MB each
Reading from 20077: heap size 572 MB, throughput 0.992527
Reading from 20078: heap size 419 MB, throughput 0.998175
Reading from 20078: heap size 425 MB, throughput 0.996253
Reading from 20077: heap size 574 MB, throughput 0.992009
Reading from 20077: heap size 574 MB, throughput 0.918468
Reading from 20078: heap size 426 MB, throughput 0.996866
Reading from 20077: heap size 577 MB, throughput 0.991519
Reading from 20078: heap size 435 MB, throughput 0.998727
Reading from 20077: heap size 579 MB, throughput 0.995548
Equal recommendation: 864 MB each
Reading from 20078: heap size 435 MB, throughput 0.998454
Reading from 20077: heap size 582 MB, throughput 0.994485
Reading from 20078: heap size 442 MB, throughput 0.998652
Reading from 20078: heap size 442 MB, throughput 0.99844
Reading from 20077: heap size 583 MB, throughput 0.993893
Reading from 20078: heap size 448 MB, throughput 0.998482
Reading from 20077: heap size 582 MB, throughput 0.987502
Reading from 20078: heap size 449 MB, throughput 0.994417
Equal recommendation: 864 MB each
Reading from 20077: heap size 584 MB, throughput 0.991927
Reading from 20078: heap size 454 MB, throughput 0.998513
Reading from 20077: heap size 586 MB, throughput 0.990625
Reading from 20077: heap size 587 MB, throughput 0.92759
Reading from 20078: heap size 455 MB, throughput 0.998659
Reading from 20077: heap size 590 MB, throughput 0.994051
Reading from 20078: heap size 461 MB, throughput 0.998197
Equal recommendation: 864 MB each
Reading from 20077: heap size 592 MB, throughput 0.99542
Reading from 20078: heap size 462 MB, throughput 0.99797
Reading from 20078: heap size 469 MB, throughput 0.997768
Reading from 20077: heap size 594 MB, throughput 0.994965
Client 20078 died
Clients: 1
Reading from 20077: heap size 596 MB, throughput 0.994241
Recommendation: one client; give it all the memory
Reading from 20077: heap size 594 MB, throughput 0.993547
Reading from 20077: heap size 596 MB, throughput 0.993859
Reading from 20077: heap size 597 MB, throughput 0.951548
Reading from 20077: heap size 598 MB, throughput 0.985841
Recommendation: one client; give it all the memory
Reading from 20077: heap size 604 MB, throughput 0.995762
Reading from 20077: heap size 605 MB, throughput 0.994294
Reading from 20077: heap size 604 MB, throughput 0.994219
Recommendation: one client; give it all the memory
Reading from 20077: heap size 606 MB, throughput 0.992585
Reading from 20077: heap size 608 MB, throughput 0.989615
Reading from 20077: heap size 608 MB, throughput 0.965047
Client 20077 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
