#include "gc_implementation/shared/HotspotMajorOnly.hpp"
#include "gc_implementation/parallelScavenge/parallelScavengeHeap.hpp"

HotspotMajorOnly::HotspotMajorOnly() : MajorOnlyThroughput() {}

unsigned long long HotspotMajorOnly::getHeapSizeB()
{
    return ParallelScavengeHeap::heap()->capacity();
}

void HotspotMajorOnly::logReading(unsigned long long heapSizeB, double throughput, unsigned long long gcTime, unsigned long long interGCTime)
{
    if (PrintGC)
        gclog_or_tty->print("#END MajorOnlyThroughput: heapSize %llu (%f MB), throughput %f, gcTime %llu (%f s), interGCTime %llu (%f s)\n",
                            heapSizeB,
                            heapSizeB/(1024.0*1024.0),
                            throughput,
                            gcTime,
                            gcTime / 1000000.0,
                            interGCTime,
                            interGCTime / 1000000.0);
}
