economemd
    total memory: 1000 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: no
    equal recommendations: no

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/gcFactor_2014_09_23_09_34_59/daemon_log_5.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Reading from 4755: heap size 9 MB, throughput 0.99196
Clients: 1
Reading from 4755: heap size 9 MB, throughput 0.953836
Reading from 4755: heap size 9 MB, throughput 0.961523
Reading from 4755: heap size 9 MB, throughput 0.972719
Reading from 4755: heap size 11 MB, throughput 0.960029
Reading from 4755: heap size 11 MB, throughput 0.953864
Reading from 4755: heap size 16 MB, throughput 0.912472
Reading from 4755: heap size 22 MB, throughput 0.99682
Reading from 4755: heap size 20 MB, throughput 0.997877
Reading from 4755: heap size 21 MB, throughput 0.997689
Reading from 4755: heap size 21 MB, throughput 0.997655
Reading from 4755: heap size 21 MB, throughput 0.998229
Reading from 4755: heap size 21 MB, throughput 0.998069
Reading from 4755: heap size 21 MB, throughput 0.998293
Reading from 4755: heap size 21 MB, throughput 0.998486
Reading from 4755: heap size 21 MB, throughput 0.998639
Reading from 4755: heap size 20 MB, throughput 0.997893
Reading from 4755: heap size 21 MB, throughput 0.997606
Reading from 4755: heap size 21 MB, throughput 0.997531
Reading from 4755: heap size 21 MB, throughput 0.998282
Reading from 4755: heap size 21 MB, throughput 0.99804
Reading from 4755: heap size 21 MB, throughput 0.997249
Reading from 4755: heap size 21 MB, throughput 0.998378
Reading from 4755: heap size 21 MB, throughput 0.997874
Reading from 4755: heap size 22 MB, throughput 0.997845
Reading from 4755: heap size 22 MB, throughput 0.997265
Reading from 4755: heap size 22 MB, throughput 0.997152
Reading from 4755: heap size 22 MB, throughput 0.997303
Reading from 4755: heap size 22 MB, throughput 0.997438
Reading from 4755: heap size 22 MB, throughput 0.997534
Reading from 4755: heap size 21 MB, throughput 0.996612
Reading from 4755: heap size 22 MB, throughput 0.997374
Reading from 4755: heap size 22 MB, throughput 0.997303
Reading from 4755: heap size 22 MB, throughput 0.996713
Reading from 4755: heap size 22 MB, throughput 0.997876
Reading from 4755: heap size 22 MB, throughput 0.996774
Reading from 4755: heap size 22 MB, throughput 0.997312
Reading from 4755: heap size 22 MB, throughput 0.996529
Reading from 4755: heap size 22 MB, throughput 0.997418
Reading from 4755: heap size 22 MB, throughput 0.997083
Reading from 4755: heap size 22 MB, throughput 0.996858
Reading from 4755: heap size 22 MB, throughput 0.996883
Reading from 4755: heap size 21 MB, throughput 0.996796
Reading from 4755: heap size 21 MB, throughput 0.996572
Reading from 4755: heap size 22 MB, throughput 0.997273
Client 4755 died
Clients: 0
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
