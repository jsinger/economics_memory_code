economemd
    total memory: 3657 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub9_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 27458: heap size 9 MB, throughput 0.983776
Clients: 1
Client 27458 has a minimum heap size of 8 MB
Reading from 27457: heap size 9 MB, throughput 0.991514
Clients: 2
Client 27457 has a minimum heap size of 1211 MB
Reading from 27457: heap size 9 MB, throughput 0.978746
Reading from 27458: heap size 9 MB, throughput 0.97926
Reading from 27457: heap size 9 MB, throughput 0.961787
Reading from 27458: heap size 11 MB, throughput 0.952348
Reading from 27457: heap size 9 MB, throughput 0.952234
Reading from 27458: heap size 11 MB, throughput 0.946049
Reading from 27457: heap size 11 MB, throughput 0.987041
Reading from 27458: heap size 15 MB, throughput 0.863707
Reading from 27458: heap size 20 MB, throughput 0.981259
Reading from 27457: heap size 11 MB, throughput 0.976719
Reading from 27458: heap size 24 MB, throughput 0.979358
Reading from 27457: heap size 17 MB, throughput 0.887199
Reading from 27458: heap size 29 MB, throughput 0.951174
Reading from 27458: heap size 30 MB, throughput 0.962831
Reading from 27458: heap size 30 MB, throughput 0.961319
Reading from 27457: heap size 17 MB, throughput 0.702338
Reading from 27458: heap size 34 MB, throughput 0.889385
Reading from 27458: heap size 34 MB, throughput 0.969876
Reading from 27457: heap size 29 MB, throughput 0.87645
Reading from 27458: heap size 38 MB, throughput 0.979173
Reading from 27458: heap size 39 MB, throughput 0.974624
Reading from 27457: heap size 31 MB, throughput 0.905493
Reading from 27458: heap size 46 MB, throughput 0.984775
Reading from 27458: heap size 46 MB, throughput 0.971983
Reading from 27458: heap size 54 MB, throughput 0.985412
Reading from 27457: heap size 35 MB, throughput 0.304606
Reading from 27458: heap size 54 MB, throughput 0.978162
Reading from 27457: heap size 47 MB, throughput 0.800312
Reading from 27458: heap size 59 MB, throughput 0.979883
Reading from 27458: heap size 59 MB, throughput 0.981206
Reading from 27457: heap size 49 MB, throughput 0.356213
Reading from 27458: heap size 65 MB, throughput 0.983884
Reading from 27457: heap size 64 MB, throughput 0.878583
Reading from 27458: heap size 65 MB, throughput 0.989563
Reading from 27457: heap size 68 MB, throughput 0.740244
Reading from 27458: heap size 71 MB, throughput 0.991303
Reading from 27458: heap size 71 MB, throughput 0.978295
Reading from 27457: heap size 70 MB, throughput 0.267198
Reading from 27458: heap size 77 MB, throughput 0.988283
Reading from 27457: heap size 100 MB, throughput 0.807281
Reading from 27458: heap size 77 MB, throughput 0.989912
Reading from 27458: heap size 81 MB, throughput 0.992515
Reading from 27457: heap size 100 MB, throughput 0.213714
Reading from 27458: heap size 81 MB, throughput 0.990379
Reading from 27457: heap size 131 MB, throughput 0.774515
Reading from 27458: heap size 85 MB, throughput 0.986052
Reading from 27457: heap size 131 MB, throughput 0.793698
Reading from 27457: heap size 132 MB, throughput 0.82993
Reading from 27458: heap size 85 MB, throughput 0.990997
Reading from 27457: heap size 136 MB, throughput 0.707768
Reading from 27458: heap size 89 MB, throughput 0.991841
Reading from 27458: heap size 89 MB, throughput 0.970187
Reading from 27458: heap size 93 MB, throughput 0.990737
Reading from 27457: heap size 138 MB, throughput 0.173429
Reading from 27458: heap size 93 MB, throughput 0.994611
Reading from 27457: heap size 177 MB, throughput 0.646732
Reading from 27458: heap size 97 MB, throughput 0.994763
Reading from 27457: heap size 181 MB, throughput 0.680023
Reading from 27457: heap size 184 MB, throughput 0.664842
Reading from 27458: heap size 97 MB, throughput 0.9944
Reading from 27457: heap size 192 MB, throughput 0.805677
Reading from 27458: heap size 101 MB, throughput 0.994402
Reading from 27458: heap size 101 MB, throughput 0.992097
Reading from 27457: heap size 194 MB, throughput 0.179104
Reading from 27458: heap size 104 MB, throughput 0.996012
Reading from 27458: heap size 104 MB, throughput 0.992303
Reading from 27457: heap size 233 MB, throughput 0.567414
Reading from 27457: heap size 241 MB, throughput 0.689594
Reading from 27458: heap size 106 MB, throughput 0.995051
Reading from 27457: heap size 242 MB, throughput 0.637482
Reading from 27458: heap size 107 MB, throughput 0.992912
Reading from 27458: heap size 109 MB, throughput 0.988274
Reading from 27457: heap size 246 MB, throughput 0.625188
Reading from 27458: heap size 109 MB, throughput 0.988205
Reading from 27457: heap size 249 MB, throughput 0.647077
Reading from 27458: heap size 112 MB, throughput 0.984756
Reading from 27457: heap size 257 MB, throughput 0.593805
Reading from 27457: heap size 262 MB, throughput 0.526036
Reading from 27458: heap size 112 MB, throughput 0.992845
Reading from 27457: heap size 272 MB, throughput 0.463216
Reading from 27458: heap size 115 MB, throughput 0.993082
Reading from 27458: heap size 116 MB, throughput 0.990783
Reading from 27458: heap size 119 MB, throughput 0.991284
Reading from 27457: heap size 279 MB, throughput 0.113482
Reading from 27458: heap size 119 MB, throughput 0.989349
Reading from 27457: heap size 330 MB, throughput 0.434516
Reading from 27458: heap size 122 MB, throughput 0.993676
Reading from 27458: heap size 122 MB, throughput 0.993507
Reading from 27457: heap size 336 MB, throughput 0.100365
Reading from 27458: heap size 126 MB, throughput 0.993453
Reading from 27457: heap size 386 MB, throughput 0.557911
Reading from 27457: heap size 380 MB, throughput 0.663027
Reading from 27458: heap size 126 MB, throughput 0.995184
Reading from 27457: heap size 319 MB, throughput 0.675117
Reading from 27457: heap size 375 MB, throughput 0.685372
Reading from 27458: heap size 129 MB, throughput 0.996809
Reading from 27457: heap size 379 MB, throughput 0.594209
Reading from 27458: heap size 129 MB, throughput 0.99436
Reading from 27457: heap size 380 MB, throughput 0.561426
Reading from 27457: heap size 383 MB, throughput 0.52409
Reading from 27458: heap size 132 MB, throughput 0.996332
Reading from 27457: heap size 386 MB, throughput 0.602511
Reading from 27457: heap size 393 MB, throughput 0.510229
Reading from 27458: heap size 132 MB, throughput 0.996149
Reading from 27457: heap size 399 MB, throughput 0.503518
Reading from 27458: heap size 134 MB, throughput 0.994468
Reading from 27458: heap size 134 MB, throughput 0.992183
Reading from 27458: heap size 137 MB, throughput 0.99475
Reading from 27458: heap size 137 MB, throughput 0.996857
Reading from 27457: heap size 407 MB, throughput 0.0763683
Reading from 27458: heap size 139 MB, throughput 0.994417
Reading from 27457: heap size 463 MB, throughput 0.387815
Reading from 27458: heap size 139 MB, throughput 0.99349
Reading from 27457: heap size 470 MB, throughput 0.51029
Reading from 27457: heap size 473 MB, throughput 0.493191
Reading from 27458: heap size 142 MB, throughput 0.994249
Reading from 27457: heap size 475 MB, throughput 0.477412
Reading from 27458: heap size 142 MB, throughput 0.995212
Reading from 27457: heap size 480 MB, throughput 0.475358
Reading from 27458: heap size 144 MB, throughput 0.996112
Reading from 27458: heap size 145 MB, throughput 0.994691
Equal recommendation: 1828 MB each
Reading from 27458: heap size 147 MB, throughput 0.993973
Reading from 27457: heap size 484 MB, throughput 0.0833397
Reading from 27458: heap size 147 MB, throughput 0.995729
Reading from 27457: heap size 540 MB, throughput 0.365264
Reading from 27458: heap size 149 MB, throughput 0.996778
Reading from 27457: heap size 473 MB, throughput 0.561586
Reading from 27458: heap size 150 MB, throughput 0.993473
Reading from 27457: heap size 531 MB, throughput 0.60181
Reading from 27457: heap size 537 MB, throughput 0.496712
Reading from 27458: heap size 152 MB, throughput 0.995994
Reading from 27457: heap size 534 MB, throughput 0.568426
Reading from 27458: heap size 152 MB, throughput 0.995183
Reading from 27458: heap size 154 MB, throughput 0.996068
Reading from 27458: heap size 154 MB, throughput 0.994995
Reading from 27457: heap size 536 MB, throughput 0.107406
Reading from 27458: heap size 156 MB, throughput 0.996442
Reading from 27457: heap size 602 MB, throughput 0.463649
Reading from 27458: heap size 157 MB, throughput 0.993775
Reading from 27457: heap size 605 MB, throughput 0.615277
Reading from 27458: heap size 159 MB, throughput 0.995459
Reading from 27457: heap size 606 MB, throughput 0.542396
Reading from 27458: heap size 159 MB, throughput 0.994087
Reading from 27457: heap size 609 MB, throughput 0.524943
Reading from 27458: heap size 162 MB, throughput 0.993155
Reading from 27457: heap size 612 MB, throughput 0.545936
Reading from 27457: heap size 622 MB, throughput 0.499995
Reading from 27458: heap size 162 MB, throughput 0.99543
Reading from 27458: heap size 165 MB, throughput 0.993989
Reading from 27458: heap size 165 MB, throughput 0.994314
Reading from 27457: heap size 629 MB, throughput 0.785626
Reading from 27458: heap size 168 MB, throughput 0.996957
Reading from 27458: heap size 168 MB, throughput 0.994166
Reading from 27457: heap size 637 MB, throughput 0.78185
Reading from 27458: heap size 171 MB, throughput 0.996286
Reading from 27457: heap size 637 MB, throughput 0.736705
Reading from 27458: heap size 171 MB, throughput 0.995294
Reading from 27458: heap size 174 MB, throughput 0.994701
Reading from 27458: heap size 174 MB, throughput 0.990621
Reading from 27458: heap size 177 MB, throughput 0.994286
Reading from 27458: heap size 177 MB, throughput 0.994291
Reading from 27458: heap size 180 MB, throughput 0.996754
Reading from 27458: heap size 180 MB, throughput 0.995616
Reading from 27458: heap size 183 MB, throughput 0.994951
Reading from 27457: heap size 654 MB, throughput 0.24197
Reading from 27458: heap size 183 MB, throughput 0.994495
Reading from 27457: heap size 724 MB, throughput 0.305616
Reading from 27457: heap size 738 MB, throughput 0.355489
Reading from 27458: heap size 187 MB, throughput 0.996912
Reading from 27457: heap size 740 MB, throughput 0.456617
Reading from 27457: heap size 741 MB, throughput 0.201674
Reading from 27458: heap size 187 MB, throughput 0.994289
Reading from 27458: heap size 189 MB, throughput 0.997024
Reading from 27458: heap size 189 MB, throughput 0.99361
Reading from 27457: heap size 727 MB, throughput 0.027202
Reading from 27458: heap size 192 MB, throughput 0.997112
Reading from 27457: heap size 710 MB, throughput 0.179285
Reading from 27457: heap size 805 MB, throughput 0.344622
Reading from 27458: heap size 192 MB, throughput 0.995745
Equal recommendation: 1828 MB each
Reading from 27458: heap size 195 MB, throughput 0.994502
Reading from 27458: heap size 195 MB, throughput 0.993799
Reading from 27457: heap size 806 MB, throughput 0.0393853
Reading from 27457: heap size 894 MB, throughput 0.232777
Reading from 27458: heap size 199 MB, throughput 0.997153
Reading from 27458: heap size 199 MB, throughput 0.996142
Reading from 27457: heap size 896 MB, throughput 0.934475
Reading from 27457: heap size 906 MB, throughput 0.704265
Reading from 27457: heap size 906 MB, throughput 0.576043
Reading from 27458: heap size 201 MB, throughput 0.997522
Reading from 27457: heap size 902 MB, throughput 0.631183
Reading from 27457: heap size 905 MB, throughput 0.575166
Reading from 27458: heap size 202 MB, throughput 0.996719
Reading from 27457: heap size 902 MB, throughput 0.909516
Reading from 27458: heap size 205 MB, throughput 0.996643
Reading from 27457: heap size 904 MB, throughput 0.784184
Reading from 27457: heap size 897 MB, throughput 0.8948
Reading from 27458: heap size 205 MB, throughput 0.995948
Reading from 27457: heap size 727 MB, throughput 0.696692
Reading from 27458: heap size 208 MB, throughput 0.996826
Reading from 27457: heap size 891 MB, throughput 0.953418
Reading from 27457: heap size 728 MB, throughput 0.817786
Reading from 27458: heap size 208 MB, throughput 0.994189
Reading from 27457: heap size 881 MB, throughput 0.858312
Reading from 27458: heap size 210 MB, throughput 0.995814
Reading from 27458: heap size 211 MB, throughput 0.992268
Reading from 27457: heap size 742 MB, throughput 0.859698
Reading from 27457: heap size 869 MB, throughput 0.711111
Reading from 27458: heap size 214 MB, throughput 0.996272
Reading from 27457: heap size 779 MB, throughput 0.718508
Reading from 27457: heap size 863 MB, throughput 0.744188
Reading from 27458: heap size 214 MB, throughput 0.994946
Reading from 27457: heap size 774 MB, throughput 0.771125
Reading from 27458: heap size 218 MB, throughput 0.996183
Reading from 27457: heap size 848 MB, throughput 0.787433
Reading from 27457: heap size 780 MB, throughput 0.772497
Reading from 27458: heap size 218 MB, throughput 0.995223
Reading from 27457: heap size 841 MB, throughput 0.779988
Reading from 27457: heap size 848 MB, throughput 0.789213
Reading from 27458: heap size 222 MB, throughput 0.996563
Reading from 27457: heap size 837 MB, throughput 0.800825
Reading from 27458: heap size 222 MB, throughput 0.995058
Reading from 27457: heap size 843 MB, throughput 0.812003
Reading from 27457: heap size 838 MB, throughput 0.833206
Reading from 27458: heap size 225 MB, throughput 0.996121
Reading from 27458: heap size 225 MB, throughput 0.994385
Reading from 27458: heap size 229 MB, throughput 0.99588
Reading from 27458: heap size 229 MB, throughput 0.994941
Reading from 27458: heap size 232 MB, throughput 0.99621
Reading from 27457: heap size 841 MB, throughput 0.974952
Reading from 27458: heap size 232 MB, throughput 0.995477
Reading from 27458: heap size 236 MB, throughput 0.970598
Reading from 27457: heap size 840 MB, throughput 0.933449
Reading from 27457: heap size 842 MB, throughput 0.729878
Reading from 27458: heap size 238 MB, throughput 0.996779
Reading from 27457: heap size 849 MB, throughput 0.732157
Reading from 27457: heap size 851 MB, throughput 0.738927
Reading from 27457: heap size 859 MB, throughput 0.747771
Reading from 27458: heap size 241 MB, throughput 0.995376
Reading from 27457: heap size 860 MB, throughput 0.708254
Reading from 27458: heap size 241 MB, throughput 0.996136
Reading from 27457: heap size 869 MB, throughput 0.703525
Reading from 27457: heap size 869 MB, throughput 0.749518
Reading from 27458: heap size 248 MB, throughput 0.997349
Reading from 27457: heap size 877 MB, throughput 0.743299
Reading from 27458: heap size 248 MB, throughput 0.995783
Reading from 27457: heap size 878 MB, throughput 0.891752
Equal recommendation: 1828 MB each
Reading from 27458: heap size 254 MB, throughput 0.996338
Reading from 27457: heap size 883 MB, throughput 0.789723
Reading from 27458: heap size 254 MB, throughput 0.996468
Reading from 27457: heap size 885 MB, throughput 0.879202
Reading from 27458: heap size 259 MB, throughput 0.992333
Reading from 27458: heap size 260 MB, throughput 0.996072
Reading from 27458: heap size 266 MB, throughput 0.997615
Reading from 27458: heap size 267 MB, throughput 0.994967
Reading from 27458: heap size 274 MB, throughput 0.99608
Reading from 27457: heap size 889 MB, throughput 0.0659668
Reading from 27458: heap size 274 MB, throughput 0.991254
Reading from 27457: heap size 972 MB, throughput 0.446663
Reading from 27457: heap size 994 MB, throughput 0.730666
Reading from 27458: heap size 281 MB, throughput 0.991607
Reading from 27457: heap size 995 MB, throughput 0.701986
Reading from 27457: heap size 1006 MB, throughput 0.686252
Reading from 27457: heap size 1006 MB, throughput 0.671568
Reading from 27458: heap size 282 MB, throughput 0.995324
Reading from 27457: heap size 1018 MB, throughput 0.666522
Reading from 27458: heap size 292 MB, throughput 0.993171
Reading from 27457: heap size 1020 MB, throughput 0.655914
Reading from 27457: heap size 1034 MB, throughput 0.657854
Reading from 27458: heap size 293 MB, throughput 0.994694
Reading from 27457: heap size 1037 MB, throughput 0.639228
Reading from 27457: heap size 1054 MB, throughput 0.689591
Reading from 27458: heap size 303 MB, throughput 0.996122
Reading from 27458: heap size 304 MB, throughput 0.993562
Reading from 27458: heap size 314 MB, throughput 0.995301
Reading from 27458: heap size 314 MB, throughput 0.995273
Reading from 27458: heap size 324 MB, throughput 0.995868
Reading from 27457: heap size 1057 MB, throughput 0.967404
Reading from 27458: heap size 324 MB, throughput 0.997031
Reading from 27458: heap size 333 MB, throughput 0.996705
Reading from 27458: heap size 334 MB, throughput 0.997058
Reading from 27457: heap size 1077 MB, throughput 0.948323
Reading from 27458: heap size 342 MB, throughput 0.996877
Equal recommendation: 1828 MB each
Reading from 27458: heap size 342 MB, throughput 0.996131
Reading from 27458: heap size 350 MB, throughput 0.998289
Reading from 27458: heap size 350 MB, throughput 0.998027
Reading from 27457: heap size 1080 MB, throughput 0.935142
Reading from 27458: heap size 357 MB, throughput 0.998601
Reading from 27458: heap size 357 MB, throughput 0.998412
Reading from 27458: heap size 362 MB, throughput 0.996647
Reading from 27458: heap size 363 MB, throughput 0.996116
Reading from 27457: heap size 1086 MB, throughput 0.937189
Reading from 27458: heap size 369 MB, throughput 0.996807
Reading from 27458: heap size 369 MB, throughput 0.997131
Reading from 27458: heap size 377 MB, throughput 0.997563
Reading from 27457: heap size 1090 MB, throughput 0.932045
Reading from 27458: heap size 377 MB, throughput 0.996376
Reading from 27458: heap size 384 MB, throughput 0.996512
Reading from 27458: heap size 384 MB, throughput 0.995607
Reading from 27458: heap size 393 MB, throughput 0.997447
Reading from 27457: heap size 1087 MB, throughput 0.928751
Reading from 27458: heap size 392 MB, throughput 0.996997
Reading from 27458: heap size 401 MB, throughput 0.998202
Reading from 27458: heap size 401 MB, throughput 0.996981
Reading from 27457: heap size 1093 MB, throughput 0.941474
Reading from 27458: heap size 409 MB, throughput 0.997554
Equal recommendation: 1828 MB each
Reading from 27458: heap size 409 MB, throughput 0.997
Reading from 27458: heap size 417 MB, throughput 0.997777
Reading from 27458: heap size 417 MB, throughput 0.99322
Reading from 27457: heap size 1089 MB, throughput 0.94734
Reading from 27458: heap size 423 MB, throughput 0.997629
Reading from 27458: heap size 424 MB, throughput 0.996782
Reading from 27458: heap size 433 MB, throughput 0.997276
Reading from 27457: heap size 1094 MB, throughput 0.94248
Reading from 27458: heap size 433 MB, throughput 0.997261
Reading from 27458: heap size 441 MB, throughput 0.997719
Reading from 27458: heap size 442 MB, throughput 0.996702
Reading from 27457: heap size 1101 MB, throughput 0.929066
Reading from 27458: heap size 451 MB, throughput 0.996987
Reading from 27458: heap size 451 MB, throughput 0.997051
Reading from 27458: heap size 460 MB, throughput 0.99779
Reading from 27457: heap size 1102 MB, throughput 0.939645
Reading from 27458: heap size 460 MB, throughput 0.9977
Reading from 27458: heap size 468 MB, throughput 0.997567
Reading from 27458: heap size 469 MB, throughput 0.997718
Reading from 27458: heap size 477 MB, throughput 0.997626
Reading from 27457: heap size 1110 MB, throughput 0.938966
Equal recommendation: 1828 MB each
Reading from 27458: heap size 477 MB, throughput 0.997656
Reading from 27458: heap size 484 MB, throughput 0.997368
Reading from 27458: heap size 485 MB, throughput 0.997588
Reading from 27457: heap size 1112 MB, throughput 0.940844
Reading from 27458: heap size 493 MB, throughput 0.997724
Reading from 27458: heap size 493 MB, throughput 0.997876
Reading from 27458: heap size 500 MB, throughput 0.997401
Reading from 27457: heap size 1120 MB, throughput 0.950702
Reading from 27458: heap size 501 MB, throughput 0.997524
Reading from 27458: heap size 509 MB, throughput 0.998223
Reading from 27458: heap size 509 MB, throughput 0.997554
Reading from 27457: heap size 1124 MB, throughput 0.937908
Reading from 27458: heap size 517 MB, throughput 0.998462
Reading from 27458: heap size 517 MB, throughput 0.997536
Reading from 27458: heap size 525 MB, throughput 0.996205
Reading from 27457: heap size 1131 MB, throughput 0.93145
Reading from 27458: heap size 525 MB, throughput 0.997762
Equal recommendation: 1828 MB each
Reading from 27458: heap size 535 MB, throughput 0.997286
Reading from 27458: heap size 535 MB, throughput 0.996132
Reading from 27457: heap size 1137 MB, throughput 0.937943
Reading from 27458: heap size 544 MB, throughput 0.998306
Reading from 27458: heap size 544 MB, throughput 0.998249
Reading from 27458: heap size 553 MB, throughput 0.997846
Reading from 27458: heap size 554 MB, throughput 0.997985
Reading from 27457: heap size 1146 MB, throughput 0.47403
Reading from 27458: heap size 562 MB, throughput 0.998727
Reading from 27458: heap size 562 MB, throughput 0.998254
Reading from 27458: heap size 570 MB, throughput 0.998332
Reading from 27457: heap size 1239 MB, throughput 0.910449
Reading from 27458: heap size 570 MB, throughput 0.997672
Reading from 27458: heap size 578 MB, throughput 0.998365
Equal recommendation: 1828 MB each
Reading from 27458: heap size 578 MB, throughput 0.998056
Reading from 27457: heap size 1250 MB, throughput 0.963675
Reading from 27458: heap size 586 MB, throughput 0.998034
Reading from 27458: heap size 586 MB, throughput 0.998222
Reading from 27458: heap size 594 MB, throughput 0.99697
Reading from 27457: heap size 1251 MB, throughput 0.968568
Reading from 27458: heap size 594 MB, throughput 0.997845
Reading from 27458: heap size 602 MB, throughput 0.997999
Reading from 27457: heap size 1252 MB, throughput 0.958656
Reading from 27458: heap size 603 MB, throughput 0.998403
Reading from 27458: heap size 611 MB, throughput 0.998233
Reading from 27458: heap size 611 MB, throughput 0.997808
Reading from 27457: heap size 1255 MB, throughput 0.962187
Reading from 27458: heap size 620 MB, throughput 0.998227
Reading from 27458: heap size 620 MB, throughput 0.998427
Equal recommendation: 1828 MB each
Reading from 27458: heap size 628 MB, throughput 0.997873
Reading from 27457: heap size 1249 MB, throughput 0.955659
Reading from 27458: heap size 628 MB, throughput 0.998653
Reading from 27458: heap size 636 MB, throughput 0.998358
Reading from 27458: heap size 637 MB, throughput 0.99752
Reading from 27457: heap size 1254 MB, throughput 0.955559
Reading from 27458: heap size 644 MB, throughput 0.998421
Reading from 27458: heap size 644 MB, throughput 0.998707
Reading from 27457: heap size 1250 MB, throughput 0.947788
Reading from 27458: heap size 652 MB, throughput 0.998497
Reading from 27458: heap size 653 MB, throughput 0.998029
Reading from 27458: heap size 660 MB, throughput 0.998415
Reading from 27457: heap size 1253 MB, throughput 0.952071
Reading from 27458: heap size 660 MB, throughput 0.998496
Reading from 27458: heap size 668 MB, throughput 0.998356
Equal recommendation: 1828 MB each
Reading from 27458: heap size 668 MB, throughput 0.998489
Reading from 27457: heap size 1259 MB, throughput 0.943536
Reading from 27458: heap size 676 MB, throughput 0.998561
Reading from 27458: heap size 676 MB, throughput 0.997849
Reading from 27458: heap size 684 MB, throughput 0.997899
Reading from 27457: heap size 1260 MB, throughput 0.945252
Reading from 27458: heap size 684 MB, throughput 0.998417
Reading from 27458: heap size 693 MB, throughput 0.998294
Reading from 27457: heap size 1268 MB, throughput 0.954535
Reading from 27458: heap size 693 MB, throughput 0.998041
Reading from 27458: heap size 702 MB, throughput 0.998613
Reading from 27458: heap size 702 MB, throughput 0.998397
Reading from 27457: heap size 1271 MB, throughput 0.95156
Equal recommendation: 1828 MB each
Reading from 27458: heap size 711 MB, throughput 0.998641
Reading from 27458: heap size 711 MB, throughput 0.998332
Reading from 27458: heap size 719 MB, throughput 0.99841
Reading from 27457: heap size 1279 MB, throughput 0.94558
Reading from 27458: heap size 719 MB, throughput 0.99838
Reading from 27458: heap size 728 MB, throughput 0.9988
Reading from 27457: heap size 1285 MB, throughput 0.950281
Reading from 27458: heap size 728 MB, throughput 0.998565
Reading from 27458: heap size 736 MB, throughput 0.998773
Reading from 27458: heap size 736 MB, throughput 0.998102
Reading from 27457: heap size 1294 MB, throughput 0.946484
Reading from 27458: heap size 744 MB, throughput 0.998642
Equal recommendation: 1828 MB each
Reading from 27458: heap size 744 MB, throughput 0.998739
Reading from 27458: heap size 752 MB, throughput 0.99741
Reading from 27457: heap size 1302 MB, throughput 0.948674
Reading from 27458: heap size 752 MB, throughput 0.99843
Reading from 27458: heap size 761 MB, throughput 0.998777
Reading from 27457: heap size 1312 MB, throughput 0.940191
Reading from 27458: heap size 761 MB, throughput 0.998282
Reading from 27458: heap size 770 MB, throughput 0.998677
Reading from 27458: heap size 770 MB, throughput 0.998395
Reading from 27457: heap size 1318 MB, throughput 0.952298
Reading from 27458: heap size 779 MB, throughput 0.998738
Reading from 27458: heap size 779 MB, throughput 0.998535
Equal recommendation: 1828 MB each
Reading from 27458: heap size 788 MB, throughput 0.998307
Reading from 27458: heap size 788 MB, throughput 0.999121
Reading from 27457: heap size 1329 MB, throughput 0.528941
Reading from 27458: heap size 796 MB, throughput 0.998471
Reading from 27458: heap size 797 MB, throughput 0.998232
Reading from 27457: heap size 1435 MB, throughput 0.923067
Reading from 27458: heap size 804 MB, throughput 0.998826
Reading from 27458: heap size 804 MB, throughput 0.998554
Reading from 27458: heap size 813 MB, throughput 0.998789
Equal recommendation: 1828 MB each
Reading from 27458: heap size 813 MB, throughput 0.998794
Reading from 27458: heap size 821 MB, throughput 0.998736
Reading from 27458: heap size 821 MB, throughput 0.998735
Reading from 27458: heap size 829 MB, throughput 0.998908
Reading from 27458: heap size 829 MB, throughput 0.998481
Reading from 27458: heap size 836 MB, throughput 0.998868
Reading from 27458: heap size 836 MB, throughput 0.998265
Reading from 27458: heap size 844 MB, throughput 0.998785
Reading from 27458: heap size 844 MB, throughput 0.998554
Equal recommendation: 1828 MB each
Reading from 27457: heap size 1447 MB, throughput 0.986861
Reading from 27458: heap size 853 MB, throughput 0.9989
Reading from 27458: heap size 853 MB, throughput 0.998798
Reading from 27458: heap size 861 MB, throughput 0.998708
Reading from 27458: heap size 861 MB, throughput 0.998669
Reading from 27458: heap size 869 MB, throughput 0.998642
Reading from 27458: heap size 869 MB, throughput 0.998483
Reading from 27458: heap size 878 MB, throughput 0.99876
Equal recommendation: 1828 MB each
Reading from 27458: heap size 878 MB, throughput 0.998947
Reading from 27458: heap size 886 MB, throughput 0.999004
Reading from 27458: heap size 886 MB, throughput 0.998911
Reading from 27458: heap size 894 MB, throughput 0.998895
Reading from 27458: heap size 894 MB, throughput 0.998495
Reading from 27457: heap size 1448 MB, throughput 0.987954
Reading from 27458: heap size 902 MB, throughput 0.99909
Reading from 27458: heap size 902 MB, throughput 0.998838
Reading from 27458: heap size 910 MB, throughput 0.9988
Equal recommendation: 1828 MB each
Reading from 27458: heap size 910 MB, throughput 0.999006
Reading from 27458: heap size 917 MB, throughput 0.99765
Reading from 27457: heap size 1445 MB, throughput 0.97789
Reading from 27457: heap size 1465 MB, throughput 0.758093
Reading from 27457: heap size 1456 MB, throughput 0.673253
Reading from 27457: heap size 1491 MB, throughput 0.649369
Reading from 27458: heap size 918 MB, throughput 0.998723
Reading from 27457: heap size 1519 MB, throughput 0.733495
Reading from 27457: heap size 1534 MB, throughput 0.678119
Reading from 27457: heap size 1568 MB, throughput 0.69106
Reading from 27458: heap size 927 MB, throughput 0.998798
Reading from 27458: heap size 927 MB, throughput 0.999073
Reading from 27457: heap size 1576 MB, throughput 0.934665
Reading from 27458: heap size 936 MB, throughput 0.998571
Reading from 27458: heap size 937 MB, throughput 0.998946
Equal recommendation: 1828 MB each
Reading from 27457: heap size 1616 MB, throughput 0.95606
Reading from 27458: heap size 945 MB, throughput 0.99885
Reading from 27458: heap size 945 MB, throughput 0.998622
Reading from 27458: heap size 953 MB, throughput 0.998736
Reading from 27457: heap size 1619 MB, throughput 0.949992
Reading from 27458: heap size 953 MB, throughput 0.999019
Reading from 27458: heap size 962 MB, throughput 0.999029
Reading from 27457: heap size 1651 MB, throughput 0.958647
Reading from 27458: heap size 962 MB, throughput 0.998849
Reading from 27458: heap size 970 MB, throughput 0.999172
Equal recommendation: 1828 MB each
Reading from 27458: heap size 970 MB, throughput 0.998437
Reading from 27457: heap size 1655 MB, throughput 0.95331
Reading from 27458: heap size 978 MB, throughput 0.99911
Reading from 27458: heap size 978 MB, throughput 0.998856
Reading from 27457: heap size 1681 MB, throughput 0.953042
Reading from 27458: heap size 986 MB, throughput 0.99893
Reading from 27458: heap size 986 MB, throughput 0.998775
Reading from 27457: heap size 1685 MB, throughput 0.94737
Reading from 27458: heap size 994 MB, throughput 0.999232
Equal recommendation: 1828 MB each
Reading from 27458: heap size 994 MB, throughput 0.998847
Reading from 27458: heap size 1002 MB, throughput 0.999091
Reading from 27457: heap size 1697 MB, throughput 0.746509
Reading from 27458: heap size 1002 MB, throughput 0.9989
Reading from 27458: heap size 1010 MB, throughput 0.999261
Reading from 27458: heap size 1010 MB, throughput 0.998535
Reading from 27457: heap size 1723 MB, throughput 0.988454
Reading from 27458: heap size 1017 MB, throughput 0.999146
Reading from 27458: heap size 1017 MB, throughput 0.998675
Equal recommendation: 1828 MB each
Reading from 27457: heap size 1714 MB, throughput 0.984843
Reading from 27458: heap size 1025 MB, throughput 0.998769
Reading from 27458: heap size 1025 MB, throughput 0.99877
Reading from 27458: heap size 1034 MB, throughput 0.999171
Reading from 27457: heap size 1733 MB, throughput 0.980741
Reading from 27458: heap size 1034 MB, throughput 0.999001
Reading from 27458: heap size 1043 MB, throughput 0.999015
Reading from 27458: heap size 1043 MB, throughput 0.999043
Reading from 27457: heap size 1758 MB, throughput 0.976216
Equal recommendation: 1828 MB each
Reading from 27458: heap size 1051 MB, throughput 0.999064
Reading from 27458: heap size 1051 MB, throughput 0.99872
Reading from 27457: heap size 1761 MB, throughput 0.976295
Reading from 27458: heap size 1059 MB, throughput 0.998926
Reading from 27458: heap size 1059 MB, throughput 0.998793
Reading from 27458: heap size 1067 MB, throughput 0.999167
Reading from 27457: heap size 1754 MB, throughput 0.973535
Reading from 27458: heap size 1067 MB, throughput 0.998973
Reading from 27458: heap size 1076 MB, throughput 0.999083
Equal recommendation: 1828 MB each
Reading from 27457: heap size 1763 MB, throughput 0.964757
Reading from 27458: heap size 1076 MB, throughput 0.998824
Reading from 27458: heap size 1084 MB, throughput 0.999259
Reading from 27458: heap size 1084 MB, throughput 0.998826
Reading from 27457: heap size 1739 MB, throughput 0.966375
Reading from 27458: heap size 1092 MB, throughput 0.99923
Reading from 27458: heap size 1092 MB, throughput 0.998821
Reading from 27458: heap size 1100 MB, throughput 0.999
Reading from 27457: heap size 1532 MB, throughput 0.967149
Equal recommendation: 1828 MB each
Reading from 27458: heap size 1100 MB, throughput 0.999047
Reading from 27458: heap size 1108 MB, throughput 0.998988
Reading from 27457: heap size 1717 MB, throughput 0.963911
Reading from 27458: heap size 1108 MB, throughput 0.999136
Reading from 27458: heap size 1116 MB, throughput 0.998929
Reading from 27458: heap size 1117 MB, throughput 0.999049
Reading from 27457: heap size 1734 MB, throughput 0.95586
Reading from 27458: heap size 1125 MB, throughput 0.998973
Equal recommendation: 1828 MB each
Reading from 27458: heap size 1125 MB, throughput 0.99903
Reading from 27458: heap size 1133 MB, throughput 0.999014
Reading from 27457: heap size 1721 MB, throughput 0.959603
Reading from 27458: heap size 1133 MB, throughput 0.99907
Reading from 27458: heap size 1142 MB, throughput 0.999171
Reading from 27458: heap size 1142 MB, throughput 0.99878
Reading from 27457: heap size 1728 MB, throughput 0.955279
Reading from 27458: heap size 1150 MB, throughput 0.999145
Equal recommendation: 1828 MB each
Reading from 27458: heap size 1150 MB, throughput 0.99902
Reading from 27457: heap size 1740 MB, throughput 0.952776
Reading from 27458: heap size 1158 MB, throughput 0.999164
Reading from 27458: heap size 1158 MB, throughput 0.999125
Reading from 27458: heap size 1166 MB, throughput 0.999015
Reading from 27457: heap size 1742 MB, throughput 0.949622
Reading from 27458: heap size 1167 MB, throughput 0.998942
Equal recommendation: 1828 MB each
Reading from 27458: heap size 1175 MB, throughput 0.998871
Reading from 27458: heap size 1175 MB, throughput 0.998352
Reading from 27457: heap size 1759 MB, throughput 0.956441
Reading from 27458: heap size 1184 MB, throughput 0.999179
Reading from 27458: heap size 1184 MB, throughput 0.999056
Reading from 27457: heap size 1767 MB, throughput 0.958541
Reading from 27458: heap size 1193 MB, throughput 0.998926
Reading from 27458: heap size 1193 MB, throughput 0.999094
Equal recommendation: 1828 MB each
Reading from 27458: heap size 1202 MB, throughput 0.999082
Reading from 27458: heap size 1203 MB, throughput 0.999086
Reading from 27458: heap size 1211 MB, throughput 0.999037
Reading from 27458: heap size 1211 MB, throughput 0.998856
Reading from 27458: heap size 1220 MB, throughput 0.999188
Equal recommendation: 1828 MB each
Reading from 27458: heap size 1220 MB, throughput 0.999053
Reading from 27458: heap size 1229 MB, throughput 0.999143
Reading from 27458: heap size 1229 MB, throughput 0.999368
Reading from 27458: heap size 1229 MB, throughput 0.999218
Reading from 27458: heap size 1229 MB, throughput 0.999321
Reading from 27458: heap size 1229 MB, throughput 0.999393
Equal recommendation: 1828 MB each
Reading from 27458: heap size 1229 MB, throughput 0.999334
Reading from 27458: heap size 1229 MB, throughput 0.999382
Client 27458 died
Clients: 1
Reading from 27457: heap size 1789 MB, throughput 0.995477
Recommendation: one client; give it all the memory
Reading from 27457: heap size 1795 MB, throughput 0.972658
Reading from 27457: heap size 1785 MB, throughput 0.809896
Reading from 27457: heap size 1735 MB, throughput 0.814647
Reading from 27457: heap size 1770 MB, throughput 0.803623
Client 27457 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
