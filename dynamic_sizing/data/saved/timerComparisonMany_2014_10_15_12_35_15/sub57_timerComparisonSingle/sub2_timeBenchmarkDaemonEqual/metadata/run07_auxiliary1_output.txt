economemd
    total memory: 864 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub57_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 8687: heap size 9 MB, throughput 0.990732
Clients: 1
Client 8687 has a minimum heap size of 12 MB
Reading from 8686: heap size 9 MB, throughput 0.992963
Clients: 2
Client 8686 has a minimum heap size of 276 MB
Reading from 8687: heap size 9 MB, throughput 0.988918
Reading from 8686: heap size 9 MB, throughput 0.986669
Reading from 8687: heap size 9 MB, throughput 0.959897
Reading from 8686: heap size 9 MB, throughput 0.956081
Reading from 8686: heap size 9 MB, throughput 0.960999
Reading from 8687: heap size 9 MB, throughput 0.970174
Reading from 8686: heap size 11 MB, throughput 0.989646
Reading from 8687: heap size 11 MB, throughput 0.981535
Reading from 8687: heap size 11 MB, throughput 0.976284
Reading from 8686: heap size 11 MB, throughput 0.963229
Reading from 8687: heap size 16 MB, throughput 0.971909
Reading from 8686: heap size 17 MB, throughput 0.928365
Reading from 8686: heap size 17 MB, throughput 0.595979
Reading from 8687: heap size 16 MB, throughput 0.975865
Reading from 8686: heap size 29 MB, throughput 0.963367
Reading from 8686: heap size 30 MB, throughput 0.840688
Reading from 8686: heap size 34 MB, throughput 0.509741
Reading from 8686: heap size 48 MB, throughput 0.847561
Reading from 8687: heap size 23 MB, throughput 0.986885
Reading from 8686: heap size 50 MB, throughput 0.830371
Reading from 8687: heap size 24 MB, throughput 0.878684
Reading from 8686: heap size 52 MB, throughput 0.321083
Reading from 8686: heap size 76 MB, throughput 0.825013
Reading from 8687: heap size 38 MB, throughput 0.989554
Reading from 8686: heap size 76 MB, throughput 0.359708
Reading from 8686: heap size 96 MB, throughput 0.837157
Reading from 8687: heap size 39 MB, throughput 0.981429
Reading from 8686: heap size 99 MB, throughput 0.854383
Reading from 8687: heap size 46 MB, throughput 0.987629
Reading from 8687: heap size 46 MB, throughput 0.98543
Reading from 8686: heap size 101 MB, throughput 0.193406
Reading from 8686: heap size 131 MB, throughput 0.694159
Reading from 8687: heap size 54 MB, throughput 0.983581
Reading from 8686: heap size 135 MB, throughput 0.743912
Reading from 8686: heap size 136 MB, throughput 0.686959
Reading from 8687: heap size 55 MB, throughput 0.991629
Reading from 8687: heap size 64 MB, throughput 0.963809
Reading from 8687: heap size 65 MB, throughput 0.969762
Reading from 8686: heap size 144 MB, throughput 0.136862
Reading from 8687: heap size 75 MB, throughput 0.983131
Reading from 8686: heap size 173 MB, throughput 0.723109
Reading from 8686: heap size 181 MB, throughput 0.747694
Reading from 8686: heap size 183 MB, throughput 0.666099
Reading from 8687: heap size 75 MB, throughput 0.989218
Reading from 8686: heap size 185 MB, throughput 0.189997
Reading from 8687: heap size 85 MB, throughput 0.996837
Reading from 8686: heap size 226 MB, throughput 0.945331
Reading from 8686: heap size 227 MB, throughput 0.91694
Reading from 8686: heap size 234 MB, throughput 0.669252
Reading from 8687: heap size 87 MB, throughput 0.997284
Reading from 8686: heap size 239 MB, throughput 0.871569
Reading from 8686: heap size 238 MB, throughput 0.797425
Reading from 8686: heap size 244 MB, throughput 0.898545
Reading from 8686: heap size 244 MB, throughput 0.898496
Reading from 8686: heap size 245 MB, throughput 0.809183
Reading from 8686: heap size 248 MB, throughput 0.809399
Reading from 8687: heap size 96 MB, throughput 0.997552
Reading from 8686: heap size 254 MB, throughput 0.832803
Reading from 8686: heap size 256 MB, throughput 0.771735
Reading from 8686: heap size 262 MB, throughput 0.918087
Reading from 8687: heap size 98 MB, throughput 0.996811
Reading from 8686: heap size 263 MB, throughput 0.930684
Reading from 8686: heap size 261 MB, throughput 0.692396
Reading from 8687: heap size 106 MB, throughput 0.995784
Reading from 8686: heap size 265 MB, throughput 0.118348
Reading from 8686: heap size 311 MB, throughput 0.679556
Reading from 8686: heap size 311 MB, throughput 0.867662
Reading from 8686: heap size 315 MB, throughput 0.953641
Reading from 8686: heap size 315 MB, throughput 0.826006
Reading from 8686: heap size 315 MB, throughput 0.869037
Reading from 8687: heap size 107 MB, throughput 0.997345
Reading from 8686: heap size 317 MB, throughput 0.746986
Reading from 8686: heap size 317 MB, throughput 0.814634
Reading from 8686: heap size 318 MB, throughput 0.708236
Reading from 8686: heap size 322 MB, throughput 0.717697
Reading from 8686: heap size 322 MB, throughput 0.99999
Reading from 8686: heap size 322 MB, throughput 1
Reading from 8686: heap size 322 MB, throughput 0.146051
Reading from 8687: heap size 113 MB, throughput 0.996349
Equal recommendation: 432 MB each
Reading from 8687: heap size 114 MB, throughput 0.996841
Reading from 8686: heap size 328 MB, throughput 0.990449
Reading from 8687: heap size 119 MB, throughput 0.996418
Reading from 8687: heap size 119 MB, throughput 0.995278
Reading from 8686: heap size 329 MB, throughput 0.984217
Reading from 8687: heap size 125 MB, throughput 0.997098
Reading from 8687: heap size 125 MB, throughput 0.99712
Reading from 8686: heap size 331 MB, throughput 0.982249
Reading from 8687: heap size 130 MB, throughput 0.997307
Reading from 8687: heap size 130 MB, throughput 0.996916
Reading from 8686: heap size 333 MB, throughput 0.984356
Reading from 8687: heap size 134 MB, throughput 0.997233
Reading from 8687: heap size 134 MB, throughput 0.997647
Reading from 8686: heap size 332 MB, throughput 0.98593
Reading from 8687: heap size 138 MB, throughput 0.996656
Reading from 8687: heap size 138 MB, throughput 0.997687
Reading from 8686: heap size 335 MB, throughput 0.981508
Equal recommendation: 432 MB each
Reading from 8687: heap size 142 MB, throughput 0.998155
Reading from 8686: heap size 333 MB, throughput 0.982345
Reading from 8687: heap size 142 MB, throughput 0.998286
Reading from 8687: heap size 145 MB, throughput 0.998593
Reading from 8686: heap size 335 MB, throughput 0.976064
Reading from 8687: heap size 146 MB, throughput 0.996876
Reading from 8687: heap size 149 MB, throughput 0.836209
Reading from 8687: heap size 152 MB, throughput 0.988743
Reading from 8686: heap size 337 MB, throughput 0.966728
Reading from 8687: heap size 158 MB, throughput 0.997247
Reading from 8686: heap size 337 MB, throughput 0.973431
Reading from 8687: heap size 159 MB, throughput 0.99784
Reading from 8687: heap size 167 MB, throughput 0.998362
Reading from 8686: heap size 339 MB, throughput 0.974646
Reading from 8687: heap size 168 MB, throughput 0.997272
Reading from 8686: heap size 341 MB, throughput 0.983398
Reading from 8687: heap size 176 MB, throughput 0.998517
Equal recommendation: 432 MB each
Reading from 8687: heap size 177 MB, throughput 0.9982
Reading from 8686: heap size 343 MB, throughput 0.980748
Reading from 8687: heap size 183 MB, throughput 0.997881
Reading from 8686: heap size 345 MB, throughput 0.975695
Reading from 8687: heap size 184 MB, throughput 0.996596
Reading from 8687: heap size 190 MB, throughput 0.99828
Reading from 8686: heap size 347 MB, throughput 0.985942
Reading from 8686: heap size 349 MB, throughput 0.954205
Reading from 8686: heap size 351 MB, throughput 0.840594
Reading from 8687: heap size 190 MB, throughput 0.995464
Reading from 8686: heap size 351 MB, throughput 0.133464
Reading from 8686: heap size 390 MB, throughput 0.740642
Reading from 8686: heap size 393 MB, throughput 0.913661
Reading from 8687: heap size 197 MB, throughput 0.998167
Reading from 8686: heap size 397 MB, throughput 0.992227
Reading from 8687: heap size 197 MB, throughput 0.997957
Reading from 8686: heap size 398 MB, throughput 0.989125
Reading from 8687: heap size 205 MB, throughput 0.997631
Equal recommendation: 432 MB each
Reading from 8687: heap size 205 MB, throughput 0.997873
Reading from 8686: heap size 396 MB, throughput 0.991382
Reading from 8687: heap size 212 MB, throughput 0.997024
Reading from 8686: heap size 399 MB, throughput 0.977615
Reading from 8687: heap size 212 MB, throughput 0.992812
Reading from 8687: heap size 218 MB, throughput 0.997271
Reading from 8686: heap size 397 MB, throughput 0.990642
Reading from 8687: heap size 219 MB, throughput 0.997874
Reading from 8686: heap size 399 MB, throughput 0.988101
Reading from 8687: heap size 227 MB, throughput 0.99831
Reading from 8687: heap size 228 MB, throughput 0.997607
Reading from 8686: heap size 401 MB, throughput 0.97948
Reading from 8687: heap size 237 MB, throughput 0.997957
Reading from 8686: heap size 402 MB, throughput 0.982464
Equal recommendation: 432 MB each
Reading from 8687: heap size 237 MB, throughput 0.997724
Reading from 8686: heap size 404 MB, throughput 0.985418
Reading from 8687: heap size 244 MB, throughput 0.997812
Reading from 8686: heap size 407 MB, throughput 0.983049
Reading from 8687: heap size 245 MB, throughput 0.997869
Reading from 8686: heap size 409 MB, throughput 0.980434
Reading from 8687: heap size 253 MB, throughput 0.997909
Reading from 8686: heap size 413 MB, throughput 0.976268
Reading from 8687: heap size 253 MB, throughput 0.997958
Reading from 8686: heap size 414 MB, throughput 0.987502
Reading from 8686: heap size 415 MB, throughput 0.869832
Reading from 8686: heap size 414 MB, throughput 0.844979
Reading from 8686: heap size 415 MB, throughput 0.839963
Reading from 8687: heap size 261 MB, throughput 0.997928
Reading from 8686: heap size 422 MB, throughput 0.940717
Reading from 8687: heap size 261 MB, throughput 0.99363
Equal recommendation: 432 MB each
Reading from 8686: heap size 422 MB, throughput 0.990229
Reading from 8687: heap size 268 MB, throughput 0.996088
Reading from 8687: heap size 269 MB, throughput 0.997637
Reading from 8686: heap size 428 MB, throughput 0.987486
Reading from 8687: heap size 279 MB, throughput 0.998124
Reading from 8686: heap size 429 MB, throughput 0.989858
Reading from 8687: heap size 280 MB, throughput 0.998421
Reading from 8686: heap size 429 MB, throughput 0.990434
Reading from 8687: heap size 288 MB, throughput 0.997903
Reading from 8686: heap size 432 MB, throughput 0.903191
Equal recommendation: 432 MB each
Reading from 8687: heap size 289 MB, throughput 0.997323
Reading from 8686: heap size 450 MB, throughput 0.996508
Reading from 8687: heap size 298 MB, throughput 0.997692
Reading from 8686: heap size 450 MB, throughput 0.995431
Reading from 8687: heap size 296 MB, throughput 0.997743
Reading from 8686: heap size 450 MB, throughput 0.994107
Reading from 8687: heap size 298 MB, throughput 0.998583
Reading from 8687: heap size 298 MB, throughput 0.906316
Reading from 8686: heap size 396 MB, throughput 0.990172
Reading from 8687: heap size 303 MB, throughput 0.998225
Reading from 8686: heap size 450 MB, throughput 0.989491
Equal recommendation: 432 MB each
Reading from 8687: heap size 288 MB, throughput 0.99893
Reading from 8686: heap size 403 MB, throughput 0.992035
Reading from 8686: heap size 443 MB, throughput 0.921101
Reading from 8686: heap size 441 MB, throughput 0.870769
Reading from 8687: heap size 275 MB, throughput 0.998985
Reading from 8686: heap size 442 MB, throughput 0.872284
Reading from 8686: heap size 422 MB, throughput 0.974519
Reading from 8687: heap size 263 MB, throughput 0.998793
Reading from 8686: heap size 446 MB, throughput 0.990147
Reading from 8687: heap size 251 MB, throughput 0.998678
Reading from 8686: heap size 403 MB, throughput 0.990939
Reading from 8687: heap size 241 MB, throughput 0.998498
Reading from 8687: heap size 231 MB, throughput 0.998357
Reading from 8686: heap size 442 MB, throughput 0.990136
Equal recommendation: 432 MB each
Reading from 8687: heap size 221 MB, throughput 0.99818
Reading from 8687: heap size 211 MB, throughput 0.997669
Reading from 8686: heap size 408 MB, throughput 0.991319
Reading from 8687: heap size 206 MB, throughput 0.998586
Reading from 8686: heap size 436 MB, throughput 0.986409
Reading from 8687: heap size 194 MB, throughput 0.998313
Reading from 8687: heap size 186 MB, throughput 0.994255
Reading from 8687: heap size 198 MB, throughput 0.988289
Reading from 8686: heap size 412 MB, throughput 0.984126
Reading from 8687: heap size 211 MB, throughput 0.997596
Reading from 8686: heap size 432 MB, throughput 0.987698
Reading from 8687: heap size 220 MB, throughput 0.998618
Reading from 8687: heap size 233 MB, throughput 0.998707
Reading from 8686: heap size 412 MB, throughput 0.985839
Equal recommendation: 432 MB each
Reading from 8687: heap size 244 MB, throughput 0.997989
Reading from 8686: heap size 429 MB, throughput 0.98647
Reading from 8687: heap size 256 MB, throughput 0.998214
Reading from 8686: heap size 429 MB, throughput 0.986037
Reading from 8687: heap size 245 MB, throughput 0.998782
Reading from 8687: heap size 239 MB, throughput 0.998762
Reading from 8686: heap size 431 MB, throughput 0.993404
Reading from 8686: heap size 431 MB, throughput 0.930159
Reading from 8686: heap size 431 MB, throughput 0.87489
Reading from 8686: heap size 433 MB, throughput 0.869416
Reading from 8686: heap size 426 MB, throughput 0.912266
Reading from 8687: heap size 224 MB, throughput 0.998494
Reading from 8687: heap size 215 MB, throughput 0.998228
Reading from 8686: heap size 433 MB, throughput 0.989566
Reading from 8687: heap size 206 MB, throughput 0.998254
Equal recommendation: 432 MB each
Reading from 8686: heap size 421 MB, throughput 0.989747
Reading from 8687: heap size 197 MB, throughput 0.998135
Reading from 8687: heap size 189 MB, throughput 0.995282
Reading from 8687: heap size 183 MB, throughput 0.98746
Reading from 8686: heap size 431 MB, throughput 0.978139
Reading from 8687: heap size 197 MB, throughput 0.989791
Reading from 8687: heap size 212 MB, throughput 0.997074
Reading from 8686: heap size 433 MB, throughput 0.990937
Reading from 8687: heap size 226 MB, throughput 0.998209
Reading from 8686: heap size 406 MB, throughput 0.987262
Reading from 8687: heap size 240 MB, throughput 0.998525
Reading from 8686: heap size 433 MB, throughput 0.989333
Reading from 8687: heap size 245 MB, throughput 0.998498
Reading from 8687: heap size 245 MB, throughput 0.9979
Reading from 8686: heap size 409 MB, throughput 0.988242
Equal recommendation: 432 MB each
Reading from 8687: heap size 257 MB, throughput 0.998184
Reading from 8686: heap size 428 MB, throughput 0.986974
Reading from 8687: heap size 257 MB, throughput 0.997044
Reading from 8686: heap size 427 MB, throughput 0.98765
Reading from 8687: heap size 268 MB, throughput 0.99796
Reading from 8686: heap size 429 MB, throughput 0.984498
Reading from 8687: heap size 268 MB, throughput 0.997923
Reading from 8687: heap size 255 MB, throughput 0.998002
Reading from 8686: heap size 429 MB, throughput 0.991817
Reading from 8687: heap size 244 MB, throughput 0.995494
Reading from 8686: heap size 432 MB, throughput 0.978159
Reading from 8686: heap size 432 MB, throughput 0.819572
Reading from 8686: heap size 429 MB, throughput 0.881835
Reading from 8686: heap size 432 MB, throughput 0.91239
Reading from 8687: heap size 256 MB, throughput 0.991927
Equal recommendation: 432 MB each
Reading from 8686: heap size 442 MB, throughput 0.990558
Reading from 8687: heap size 273 MB, throughput 0.998188
Reading from 8687: heap size 273 MB, throughput 0.997763
Reading from 8686: heap size 411 MB, throughput 0.993214
Reading from 8687: heap size 276 MB, throughput 0.998254
Reading from 8686: heap size 440 MB, throughput 0.991909
Reading from 8687: heap size 287 MB, throughput 0.997344
Reading from 8686: heap size 412 MB, throughput 0.99119
Reading from 8687: heap size 289 MB, throughput 0.998076
Equal recommendation: 432 MB each
Reading from 8686: heap size 437 MB, throughput 0.991044
Reading from 8687: heap size 301 MB, throughput 0.998329
Reading from 8686: heap size 415 MB, throughput 0.988223
Reading from 8687: heap size 287 MB, throughput 0.998555
Reading from 8687: heap size 274 MB, throughput 0.99714
Reading from 8686: heap size 433 MB, throughput 0.987072
Reading from 8687: heap size 262 MB, throughput 0.998205
Reading from 8686: heap size 417 MB, throughput 0.979873
Reading from 8687: heap size 251 MB, throughput 0.992328
Reading from 8687: heap size 266 MB, throughput 0.936844
Reading from 8686: heap size 429 MB, throughput 0.985126
Reading from 8687: heap size 279 MB, throughput 0.998962
Reading from 8686: heap size 429 MB, throughput 0.985486
Equal recommendation: 432 MB each
Reading from 8687: heap size 265 MB, throughput 0.999006
Reading from 8686: heap size 432 MB, throughput 0.991076
Reading from 8686: heap size 433 MB, throughput 0.923451
Reading from 8687: heap size 254 MB, throughput 0.999132
Reading from 8686: heap size 431 MB, throughput 0.886348
Reading from 8686: heap size 433 MB, throughput 0.879129
Reading from 8686: heap size 426 MB, throughput 0.982913
Reading from 8687: heap size 248 MB, throughput 0.999009
Reading from 8687: heap size 232 MB, throughput 0.99871
Reading from 8686: heap size 434 MB, throughput 0.992961
Reading from 8687: heap size 228 MB, throughput 0.998821
Reading from 8686: heap size 422 MB, throughput 0.985134
Reading from 8687: heap size 213 MB, throughput 0.99893
Reading from 8686: heap size 432 MB, throughput 0.989441
Reading from 8687: heap size 209 MB, throughput 0.998332
Equal recommendation: 432 MB each
Reading from 8687: heap size 195 MB, throughput 0.998392
Reading from 8686: heap size 432 MB, throughput 0.990482
Reading from 8687: heap size 192 MB, throughput 0.998687
Reading from 8687: heap size 180 MB, throughput 0.997905
Reading from 8686: heap size 434 MB, throughput 0.973689
Reading from 8687: heap size 178 MB, throughput 0.99175
Reading from 8687: heap size 169 MB, throughput 0.990844
Reading from 8687: heap size 182 MB, throughput 0.99768
Reading from 8686: heap size 421 MB, throughput 0.98631
Reading from 8687: heap size 174 MB, throughput 0.997227
Reading from 8687: heap size 166 MB, throughput 0.997836
Reading from 8686: heap size 428 MB, throughput 0.988333
Reading from 8687: heap size 160 MB, throughput 0.997656
Reading from 8687: heap size 154 MB, throughput 0.997386
Reading from 8686: heap size 428 MB, throughput 0.989202
Reading from 8687: heap size 148 MB, throughput 0.997264
Equal recommendation: 432 MB each
Reading from 8687: heap size 142 MB, throughput 0.99688
Reading from 8686: heap size 429 MB, throughput 0.985365
Reading from 8687: heap size 137 MB, throughput 0.997127
Reading from 8687: heap size 132 MB, throughput 0.996421
Reading from 8687: heap size 127 MB, throughput 0.996096
Reading from 8686: heap size 432 MB, throughput 0.983087
Reading from 8687: heap size 122 MB, throughput 0.996129
Reading from 8687: heap size 118 MB, throughput 0.995929
Reading from 8687: heap size 114 MB, throughput 0.995876
Reading from 8686: heap size 422 MB, throughput 0.992337
Reading from 8686: heap size 432 MB, throughput 0.916058
Reading from 8686: heap size 431 MB, throughput 0.880686
Reading from 8687: heap size 109 MB, throughput 0.996484
Reading from 8686: heap size 436 MB, throughput 0.88874
Reading from 8687: heap size 105 MB, throughput 0.995113
Reading from 8687: heap size 101 MB, throughput 0.996462
Reading from 8686: heap size 426 MB, throughput 0.986492
Reading from 8687: heap size 97 MB, throughput 0.997357
Reading from 8687: heap size 94 MB, throughput 0.997314
Reading from 8687: heap size 95 MB, throughput 0.997128
Reading from 8687: heap size 88 MB, throughput 0.996044
Reading from 8686: heap size 436 MB, throughput 0.991862
Reading from 8687: heap size 90 MB, throughput 0.997028
Reading from 8687: heap size 83 MB, throughput 0.99707
Reading from 8687: heap size 84 MB, throughput 0.996745
Reading from 8687: heap size 78 MB, throughput 0.996964
Reading from 8687: heap size 80 MB, throughput 0.992677
Equal recommendation: 432 MB each
Reading from 8687: heap size 76 MB, throughput 0.979395
Reading from 8687: heap size 82 MB, throughput 0.981681
Reading from 8686: heap size 404 MB, throughput 0.989336
Reading from 8687: heap size 90 MB, throughput 0.987448
Reading from 8687: heap size 99 MB, throughput 0.986973
Reading from 8687: heap size 109 MB, throughput 0.994635
Reading from 8687: heap size 119 MB, throughput 0.996493
Reading from 8686: heap size 435 MB, throughput 0.991542
Reading from 8687: heap size 130 MB, throughput 0.997248
Reading from 8687: heap size 141 MB, throughput 0.997272
Reading from 8687: heap size 153 MB, throughput 0.997259
Reading from 8686: heap size 406 MB, throughput 0.989424
Reading from 8687: heap size 164 MB, throughput 0.997459
Reading from 8687: heap size 176 MB, throughput 0.997684
Reading from 8686: heap size 432 MB, throughput 0.989905
Reading from 8687: heap size 169 MB, throughput 0.997009
Reading from 8687: heap size 163 MB, throughput 0.997811
Reading from 8686: heap size 410 MB, throughput 0.988307
Reading from 8687: heap size 156 MB, throughput 0.995963
Reading from 8687: heap size 150 MB, throughput 0.997072
Equal recommendation: 432 MB each
Reading from 8686: heap size 429 MB, throughput 0.987762
Reading from 8687: heap size 144 MB, throughput 0.996606
Reading from 8687: heap size 139 MB, throughput 0.997516
Reading from 8687: heap size 133 MB, throughput 0.997534
Reading from 8686: heap size 427 MB, throughput 0.988906
Reading from 8687: heap size 128 MB, throughput 0.997736
Reading from 8687: heap size 123 MB, throughput 0.997868
Reading from 8686: heap size 429 MB, throughput 0.986493
Reading from 8687: heap size 118 MB, throughput 0.998412
Reading from 8687: heap size 118 MB, throughput 0.99777
Reading from 8687: heap size 110 MB, throughput 0.998014
Reading from 8687: heap size 110 MB, throughput 0.997859
Reading from 8686: heap size 429 MB, throughput 0.984187
Reading from 8687: heap size 103 MB, throughput 0.997829
Reading from 8687: heap size 103 MB, throughput 0.990907
Reading from 8687: heap size 98 MB, throughput 0.985815
Reading from 8687: heap size 106 MB, throughput 0.987419
Reading from 8687: heap size 116 MB, throughput 0.98698
Reading from 8686: heap size 428 MB, throughput 0.985665
Reading from 8686: heap size 433 MB, throughput 0.876385
Reading from 8687: heap size 128 MB, throughput 0.996928
Reading from 8686: heap size 429 MB, throughput 0.856908
Reading from 8686: heap size 434 MB, throughput 0.836879
Reading from 8687: heap size 139 MB, throughput 0.997045
Reading from 8686: heap size 431 MB, throughput 0.983323
Equal recommendation: 432 MB each
Reading from 8687: heap size 150 MB, throughput 0.996554
Reading from 8687: heap size 158 MB, throughput 0.997415
Reading from 8686: heap size 438 MB, throughput 0.991863
Reading from 8687: heap size 158 MB, throughput 0.997137
Reading from 8687: heap size 167 MB, throughput 0.996718
Reading from 8687: heap size 160 MB, throughput 0.995764
Reading from 8686: heap size 422 MB, throughput 0.990407
Reading from 8687: heap size 154 MB, throughput 0.997222
Reading from 8687: heap size 148 MB, throughput 0.997198
Reading from 8686: heap size 434 MB, throughput 0.985013
Reading from 8687: heap size 142 MB, throughput 0.997238
Reading from 8687: heap size 137 MB, throughput 0.996733
Reading from 8686: heap size 420 MB, throughput 0.987592
Reading from 8687: heap size 132 MB, throughput 0.995299
Reading from 8687: heap size 126 MB, throughput 0.997357
Reading from 8687: heap size 121 MB, throughput 0.997156
Reading from 8686: heap size 429 MB, throughput 0.987288
Equal recommendation: 432 MB each
Reading from 8687: heap size 116 MB, throughput 0.996859
Reading from 8687: heap size 112 MB, throughput 0.997641
Reading from 8687: heap size 111 MB, throughput 0.997643
Reading from 8686: heap size 427 MB, throughput 0.988642
Reading from 8687: heap size 104 MB, throughput 0.996861
Reading from 8687: heap size 104 MB, throughput 0.996023
Reading from 8687: heap size 97 MB, throughput 0.996493
Reading from 8687: heap size 98 MB, throughput 0.996031
Reading from 8687: heap size 91 MB, throughput 0.997145
Reading from 8686: heap size 429 MB, throughput 0.985358
Reading from 8687: heap size 92 MB, throughput 0.995534
Reading from 8687: heap size 87 MB, throughput 0.985053
Reading from 8687: heap size 94 MB, throughput 0.987237
Reading from 8687: heap size 102 MB, throughput 0.986394
Reading from 8687: heap size 112 MB, throughput 0.988771
Reading from 8687: heap size 123 MB, throughput 0.997394
Reading from 8686: heap size 428 MB, throughput 0.988
Reading from 8687: heap size 131 MB, throughput 0.995551
Reading from 8687: heap size 136 MB, throughput 0.997101
Reading from 8687: heap size 136 MB, throughput 0.995845
Reading from 8686: heap size 429 MB, throughput 0.986528
Reading from 8687: heap size 147 MB, throughput 0.996128
Reading from 8687: heap size 147 MB, throughput 0.996291
Equal recommendation: 432 MB each
Reading from 8686: heap size 429 MB, throughput 0.984918
Reading from 8687: heap size 158 MB, throughput 0.997584
Reading from 8687: heap size 150 MB, throughput 0.997423
Reading from 8687: heap size 145 MB, throughput 0.997107
Reading from 8686: heap size 430 MB, throughput 0.993647
Reading from 8686: heap size 431 MB, throughput 0.912979
Reading from 8686: heap size 432 MB, throughput 0.845341
Reading from 8687: heap size 139 MB, throughput 0.99739
Reading from 8686: heap size 426 MB, throughput 0.885113
Reading from 8687: heap size 134 MB, throughput 0.996849
Reading from 8686: heap size 432 MB, throughput 0.983582
Reading from 8687: heap size 129 MB, throughput 0.99544
Reading from 8687: heap size 124 MB, throughput 0.997037
Reading from 8686: heap size 422 MB, throughput 0.992727
Reading from 8687: heap size 119 MB, throughput 0.99561
Reading from 8687: heap size 115 MB, throughput 0.996993
Reading from 8687: heap size 110 MB, throughput 0.996718
Reading from 8687: heap size 106 MB, throughput 0.997586
Reading from 8686: heap size 432 MB, throughput 0.992466
Reading from 8687: heap size 106 MB, throughput 0.997596
Reading from 8687: heap size 99 MB, throughput 0.997864
Reading from 8687: heap size 100 MB, throughput 0.997493
Equal recommendation: 432 MB each
Reading from 8687: heap size 93 MB, throughput 0.997021
Reading from 8686: heap size 436 MB, throughput 0.991666
Reading from 8687: heap size 94 MB, throughput 0.997098
Reading from 8687: heap size 87 MB, throughput 0.996977
Reading from 8687: heap size 88 MB, throughput 0.99711
Reading from 8687: heap size 82 MB, throughput 0.987274
Reading from 8687: heap size 91 MB, throughput 0.984624
Reading from 8687: heap size 98 MB, throughput 0.984167
Reading from 8687: heap size 108 MB, throughput 0.702641
Reading from 8686: heap size 407 MB, throughput 0.991489
Reading from 8687: heap size 117 MB, throughput 0.996377
Reading from 8687: heap size 121 MB, throughput 0.997385
Reading from 8687: heap size 132 MB, throughput 0.99786
Reading from 8686: heap size 435 MB, throughput 0.989827
Reading from 8687: heap size 132 MB, throughput 0.997817
Reading from 8687: heap size 124 MB, throughput 0.997439
Reading from 8687: heap size 123 MB, throughput 0.997105
Reading from 8686: heap size 410 MB, throughput 0.988632
Reading from 8687: heap size 116 MB, throughput 0.9974
Reading from 8687: heap size 112 MB, throughput 0.997531
Reading from 8687: heap size 108 MB, throughput 0.996043
Reading from 8686: heap size 431 MB, throughput 0.989089
Reading from 8687: heap size 104 MB, throughput 0.997765
Reading from 8687: heap size 101 MB, throughput 0.997232
Reading from 8687: heap size 97 MB, throughput 0.996968
Equal recommendation: 432 MB each
Reading from 8687: heap size 94 MB, throughput 0.99496
Reading from 8687: heap size 91 MB, throughput 0.997042
Reading from 8686: heap size 430 MB, throughput 0.990454
Reading from 8687: heap size 88 MB, throughput 0.996783
Reading from 8687: heap size 86 MB, throughput 0.996497
Reading from 8687: heap size 83 MB, throughput 0.997544
Reading from 8687: heap size 82 MB, throughput 0.995327
Reading from 8687: heap size 78 MB, throughput 0.996449
Reading from 8686: heap size 432 MB, throughput 0.986302
Reading from 8687: heap size 77 MB, throughput 0.997206
Reading from 8687: heap size 74 MB, throughput 0.997218
Reading from 8687: heap size 73 MB, throughput 0.996616
Reading from 8687: heap size 70 MB, throughput 0.996577
Reading from 8687: heap size 69 MB, throughput 0.994194
Reading from 8687: heap size 66 MB, throughput 0.99641
Reading from 8686: heap size 432 MB, throughput 0.987275
Reading from 8687: heap size 66 MB, throughput 0.996223
Reading from 8687: heap size 63 MB, throughput 0.996151
Reading from 8687: heap size 63 MB, throughput 0.99594
Reading from 8687: heap size 60 MB, throughput 0.995754
Reading from 8687: heap size 60 MB, throughput 0.99528
Reading from 8687: heap size 58 MB, throughput 0.995568
Reading from 8687: heap size 58 MB, throughput 0.994925
Reading from 8687: heap size 56 MB, throughput 0.995473
Reading from 8687: heap size 55 MB, throughput 0.994704
Reading from 8686: heap size 433 MB, throughput 0.991839
Reading from 8686: heap size 412 MB, throughput 0.916528
Reading from 8687: heap size 54 MB, throughput 0.995504
Reading from 8686: heap size 429 MB, throughput 0.865468
Reading from 8687: heap size 53 MB, throughput 0.993745
Reading from 8686: heap size 431 MB, throughput 0.872892
Reading from 8687: heap size 52 MB, throughput 0.992377
Reading from 8687: heap size 51 MB, throughput 0.994692
Reading from 8687: heap size 50 MB, throughput 0.994893
Reading from 8687: heap size 49 MB, throughput 0.99394
Reading from 8687: heap size 48 MB, throughput 0.986217
Reading from 8687: heap size 52 MB, throughput 0.973271
Reading from 8686: heap size 438 MB, throughput 0.980579
Reading from 8687: heap size 56 MB, throughput 0.981368
Reading from 8687: heap size 62 MB, throughput 0.971101
Reading from 8687: heap size 68 MB, throughput 0.975567
Reading from 8687: heap size 77 MB, throughput 0.980679
Reading from 8687: heap size 86 MB, throughput 0.979251
Reading from 8687: heap size 98 MB, throughput 0.990852
Equal recommendation: 432 MB each
Reading from 8687: heap size 109 MB, throughput 0.996101
Reading from 8687: heap size 111 MB, throughput 0.996238
Reading from 8686: heap size 406 MB, throughput 0.992245
Reading from 8687: heap size 124 MB, throughput 0.997377
Reading from 8687: heap size 124 MB, throughput 0.997366
Reading from 8686: heap size 438 MB, throughput 0.991488
Reading from 8687: heap size 135 MB, throughput 0.997425
Reading from 8687: heap size 136 MB, throughput 0.99647
Reading from 8687: heap size 128 MB, throughput 0.997797
Reading from 8686: heap size 407 MB, throughput 0.99147
Reading from 8687: heap size 126 MB, throughput 0.998003
Reading from 8687: heap size 119 MB, throughput 0.997611
Reading from 8687: heap size 118 MB, throughput 0.997305
Reading from 8687: heap size 111 MB, throughput 0.997312
Reading from 8686: heap size 436 MB, throughput 0.992746
Reading from 8687: heap size 109 MB, throughput 0.99779
Reading from 8687: heap size 103 MB, throughput 0.996773
Reading from 8687: heap size 102 MB, throughput 0.996054
Reading from 8687: heap size 97 MB, throughput 0.995977
Reading from 8686: heap size 409 MB, throughput 0.984777
Equal recommendation: 432 MB each
Reading from 8687: heap size 96 MB, throughput 0.99727
Reading from 8687: heap size 91 MB, throughput 0.996368
Reading from 8687: heap size 90 MB, throughput 0.995131
Reading from 8687: heap size 85 MB, throughput 0.997317
Reading from 8686: heap size 432 MB, throughput 0.989547
Reading from 8687: heap size 84 MB, throughput 0.996676
Reading from 8687: heap size 80 MB, throughput 0.994683
Reading from 8687: heap size 79 MB, throughput 0.996714
Reading from 8687: heap size 76 MB, throughput 0.996428
Reading from 8687: heap size 75 MB, throughput 0.996888
Reading from 8686: heap size 412 MB, throughput 0.987646
Reading from 8687: heap size 72 MB, throughput 0.996953
Reading from 8687: heap size 71 MB, throughput 0.996491
Reading from 8687: heap size 68 MB, throughput 0.996313
Reading from 8687: heap size 67 MB, throughput 0.995734
Reading from 8687: heap size 65 MB, throughput 0.996148
Reading from 8687: heap size 64 MB, throughput 0.995569
Reading from 8686: heap size 428 MB, throughput 0.987033
Reading from 8687: heap size 62 MB, throughput 0.996254
Reading from 8687: heap size 61 MB, throughput 0.995405
Reading from 8686: heap size 427 MB, throughput 0.984835
Client 8687 died
Clients: 1
Reading from 8686: heap size 429 MB, throughput 0.87433
Recommendation: one client; give it all the memory
Reading from 8686: heap size 455 MB, throughput 0.990625
Reading from 8686: heap size 461 MB, throughput 0.923878
Reading from 8686: heap size 463 MB, throughput 0.905386
Reading from 8686: heap size 467 MB, throughput 0.911198
Reading from 8686: heap size 466 MB, throughput 0.9936
Reading from 8686: heap size 472 MB, throughput 0.99368
Reading from 8686: heap size 474 MB, throughput 0.993001
Reading from 8686: heap size 478 MB, throughput 0.992293
Recommendation: one client; give it all the memory
Reading from 8686: heap size 479 MB, throughput 0.990021
Reading from 8686: heap size 479 MB, throughput 0.990486
Reading from 8686: heap size 481 MB, throughput 0.989761
Reading from 8686: heap size 482 MB, throughput 0.99119
Reading from 8686: heap size 483 MB, throughput 0.986642
Recommendation: one client; give it all the memory
Reading from 8686: heap size 486 MB, throughput 0.992098
Reading from 8686: heap size 487 MB, throughput 0.924091
Reading from 8686: heap size 485 MB, throughput 0.900908
Reading from 8686: heap size 489 MB, throughput 0.925414
Reading from 8686: heap size 497 MB, throughput 0.993175
Reading from 8686: heap size 468 MB, throughput 0.992862
Reading from 8686: heap size 496 MB, throughput 0.992013
Recommendation: one client; give it all the memory
Reading from 8686: heap size 495 MB, throughput 0.992148
Reading from 8686: heap size 497 MB, throughput 0.989578
Reading from 8686: heap size 496 MB, throughput 0.990366
Reading from 8686: heap size 499 MB, throughput 0.989434
Recommendation: one client; give it all the memory
Reading from 8686: heap size 499 MB, throughput 0.987727
Reading from 8686: heap size 499 MB, throughput 0.992163
Reading from 8686: heap size 500 MB, throughput 0.91735
Reading from 8686: heap size 495 MB, throughput 0.902866
Client 8686 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
