economemd
    total memory: 6115 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub14_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 11701: heap size 9 MB, throughput 0.989764
Clients: 1
Client 11701 has a minimum heap size of 12 MB
Reading from 11700: heap size 9 MB, throughput 0.992322
Clients: 2
Client 11700 has a minimum heap size of 1211 MB
Reading from 11701: heap size 9 MB, throughput 0.987166
Reading from 11700: heap size 9 MB, throughput 0.986819
Reading from 11701: heap size 9 MB, throughput 0.970462
Reading from 11700: heap size 9 MB, throughput 0.960425
Reading from 11700: heap size 9 MB, throughput 0.92221
Reading from 11701: heap size 9 MB, throughput 0.977387
Reading from 11701: heap size 11 MB, throughput 0.976066
Reading from 11701: heap size 11 MB, throughput 0.969894
Reading from 11701: heap size 16 MB, throughput 0.983033
Reading from 11701: heap size 16 MB, throughput 0.980213
Reading from 11700: heap size 11 MB, throughput 0.996958
Reading from 11701: heap size 24 MB, throughput 0.984757
Reading from 11700: heap size 11 MB, throughput 0.867456
Reading from 11700: heap size 17 MB, throughput 0.929117
Reading from 11700: heap size 17 MB, throughput 0.647417
Reading from 11700: heap size 29 MB, throughput 0.93657
Reading from 11701: heap size 25 MB, throughput 0.860736
Reading from 11700: heap size 30 MB, throughput 0.911161
Reading from 11700: heap size 35 MB, throughput 0.413691
Reading from 11700: heap size 48 MB, throughput 0.85683
Reading from 11701: heap size 40 MB, throughput 0.991124
Reading from 11700: heap size 50 MB, throughput 0.283091
Reading from 11701: heap size 41 MB, throughput 0.971769
Reading from 11700: heap size 66 MB, throughput 0.866512
Reading from 11701: heap size 45 MB, throughput 0.987049
Reading from 11700: heap size 73 MB, throughput 0.320499
Reading from 11701: heap size 45 MB, throughput 0.983894
Reading from 11700: heap size 89 MB, throughput 0.884082
Reading from 11701: heap size 49 MB, throughput 0.973034
Reading from 11700: heap size 97 MB, throughput 0.836525
Reading from 11701: heap size 50 MB, throughput 0.987249
Reading from 11700: heap size 98 MB, throughput 0.224063
Reading from 11700: heap size 130 MB, throughput 0.704647
Reading from 11701: heap size 57 MB, throughput 0.94465
Reading from 11700: heap size 131 MB, throughput 0.700497
Reading from 11701: heap size 57 MB, throughput 0.983336
Reading from 11701: heap size 64 MB, throughput 0.986058
Reading from 11700: heap size 133 MB, throughput 0.171706
Reading from 11701: heap size 64 MB, throughput 0.98088
Reading from 11700: heap size 167 MB, throughput 0.732649
Reading from 11700: heap size 174 MB, throughput 0.63062
Reading from 11700: heap size 175 MB, throughput 0.692533
Reading from 11701: heap size 72 MB, throughput 0.993134
Reading from 11700: heap size 179 MB, throughput 0.654377
Reading from 11700: heap size 186 MB, throughput 0.698167
Reading from 11701: heap size 72 MB, throughput 0.987198
Reading from 11700: heap size 191 MB, throughput 0.155932
Reading from 11700: heap size 232 MB, throughput 0.739749
Reading from 11700: heap size 239 MB, throughput 0.730036
Reading from 11700: heap size 244 MB, throughput 0.716905
Reading from 11700: heap size 247 MB, throughput 0.636722
Reading from 11701: heap size 79 MB, throughput 0.993457
Reading from 11700: heap size 257 MB, throughput 0.622351
Reading from 11700: heap size 262 MB, throughput 0.642733
Reading from 11700: heap size 274 MB, throughput 0.522223
Reading from 11700: heap size 278 MB, throughput 0.478077
Reading from 11701: heap size 80 MB, throughput 0.977974
Reading from 11700: heap size 290 MB, throughput 0.082952
Reading from 11700: heap size 343 MB, throughput 0.444665
Reading from 11701: heap size 87 MB, throughput 0.990544
Reading from 11700: heap size 273 MB, throughput 0.0928501
Reading from 11700: heap size 381 MB, throughput 0.53011
Reading from 11700: heap size 386 MB, throughput 0.639564
Reading from 11700: heap size 376 MB, throughput 0.535916
Reading from 11701: heap size 87 MB, throughput 0.99384
Reading from 11700: heap size 321 MB, throughput 0.64675
Reading from 11700: heap size 375 MB, throughput 0.59075
Reading from 11700: heap size 377 MB, throughput 0.574344
Reading from 11700: heap size 379 MB, throughput 0.594468
Reading from 11700: heap size 382 MB, throughput 0.5813
Reading from 11701: heap size 91 MB, throughput 0.995389
Reading from 11700: heap size 387 MB, throughput 0.593614
Reading from 11700: heap size 390 MB, throughput 0.544315
Reading from 11701: heap size 92 MB, throughput 0.99366
Reading from 11700: heap size 394 MB, throughput 0.10013
Reading from 11700: heap size 444 MB, throughput 0.42563
Reading from 11701: heap size 96 MB, throughput 0.9949
Reading from 11700: heap size 453 MB, throughput 0.625955
Equal recommendation: 3057 MB each
Reading from 11700: heap size 454 MB, throughput 0.57769
Reading from 11700: heap size 458 MB, throughput 0.556674
Reading from 11700: heap size 464 MB, throughput 0.489774
Reading from 11701: heap size 96 MB, throughput 0.995014
Reading from 11700: heap size 469 MB, throughput 0.452066
Reading from 11700: heap size 477 MB, throughput 0.459056
Reading from 11701: heap size 100 MB, throughput 0.994271
Reading from 11701: heap size 100 MB, throughput 0.994852
Reading from 11700: heap size 484 MB, throughput 0.0975543
Reading from 11700: heap size 540 MB, throughput 0.423124
Reading from 11700: heap size 540 MB, throughput 0.516951
Reading from 11701: heap size 104 MB, throughput 0.994515
Reading from 11700: heap size 544 MB, throughput 0.106471
Reading from 11701: heap size 104 MB, throughput 0.995271
Reading from 11700: heap size 599 MB, throughput 0.471273
Reading from 11700: heap size 602 MB, throughput 0.671838
Reading from 11700: heap size 603 MB, throughput 0.553159
Reading from 11700: heap size 604 MB, throughput 0.590009
Reading from 11701: heap size 107 MB, throughput 0.996688
Reading from 11700: heap size 607 MB, throughput 0.625583
Reading from 11700: heap size 615 MB, throughput 0.582547
Reading from 11700: heap size 621 MB, throughput 0.477863
Reading from 11700: heap size 629 MB, throughput 0.561609
Reading from 11701: heap size 107 MB, throughput 0.99677
Reading from 11701: heap size 109 MB, throughput 0.997671
Reading from 11701: heap size 110 MB, throughput 0.997041
Reading from 11700: heap size 634 MB, throughput 0.33384
Reading from 11701: heap size 112 MB, throughput 0.998032
Reading from 11700: heap size 702 MB, throughput 0.803222
Reading from 11701: heap size 112 MB, throughput 0.997928
Reading from 11700: heap size 708 MB, throughput 0.884178
Reading from 11700: heap size 715 MB, throughput 0.525504
Reading from 11700: heap size 724 MB, throughput 0.398107
Reading from 11701: heap size 114 MB, throughput 0.998034
Reading from 11700: heap size 730 MB, throughput 0.612292
Reading from 11700: heap size 737 MB, throughput 0.366915
Reading from 11701: heap size 114 MB, throughput 0.997867
Reading from 11701: heap size 115 MB, throughput 0.998067
Equal recommendation: 3057 MB each
Reading from 11701: heap size 115 MB, throughput 0.99792
Reading from 11700: heap size 737 MB, throughput 0.0226954
Reading from 11700: heap size 792 MB, throughput 0.288269
Reading from 11700: heap size 801 MB, throughput 0.451622
Reading from 11701: heap size 116 MB, throughput 0.998312
Reading from 11700: heap size 798 MB, throughput 0.864502
Reading from 11700: heap size 801 MB, throughput 0.517072
Reading from 11701: heap size 116 MB, throughput 0.99837
Reading from 11701: heap size 118 MB, throughput 0.996772
Reading from 11700: heap size 798 MB, throughput 0.052712
Reading from 11700: heap size 876 MB, throughput 0.510364
Reading from 11700: heap size 879 MB, throughput 0.864889
Reading from 11701: heap size 118 MB, throughput 0.997762
Reading from 11700: heap size 882 MB, throughput 0.786602
Reading from 11700: heap size 888 MB, throughput 0.634507
Reading from 11701: heap size 118 MB, throughput 0.993337
Reading from 11701: heap size 119 MB, throughput 0.433607
Reading from 11701: heap size 123 MB, throughput 0.995108
Reading from 11701: heap size 123 MB, throughput 0.994257
Reading from 11700: heap size 889 MB, throughput 0.162144
Reading from 11701: heap size 131 MB, throughput 0.992428
Reading from 11700: heap size 981 MB, throughput 0.621132
Reading from 11700: heap size 984 MB, throughput 0.910814
Reading from 11700: heap size 976 MB, throughput 0.822666
Reading from 11700: heap size 832 MB, throughput 0.806312
Reading from 11701: heap size 132 MB, throughput 0.997242
Reading from 11700: heap size 967 MB, throughput 0.795464
Reading from 11700: heap size 838 MB, throughput 0.827629
Reading from 11700: heap size 957 MB, throughput 0.831654
Reading from 11700: heap size 843 MB, throughput 0.82795
Reading from 11700: heap size 952 MB, throughput 0.85236
Reading from 11701: heap size 137 MB, throughput 0.998001
Reading from 11700: heap size 958 MB, throughput 0.853641
Reading from 11700: heap size 946 MB, throughput 0.838225
Reading from 11700: heap size 952 MB, throughput 0.844675
Reading from 11701: heap size 138 MB, throughput 0.997751
Reading from 11701: heap size 143 MB, throughput 0.996924
Reading from 11700: heap size 942 MB, throughput 0.983015
Equal recommendation: 3057 MB each
Reading from 11700: heap size 856 MB, throughput 0.906706
Reading from 11701: heap size 144 MB, throughput 0.996977
Reading from 11700: heap size 941 MB, throughput 0.811487
Reading from 11700: heap size 944 MB, throughput 0.828613
Reading from 11700: heap size 936 MB, throughput 0.827954
Reading from 11700: heap size 940 MB, throughput 0.828345
Reading from 11700: heap size 936 MB, throughput 0.828956
Reading from 11701: heap size 148 MB, throughput 0.998082
Reading from 11700: heap size 939 MB, throughput 0.839504
Reading from 11700: heap size 936 MB, throughput 0.840206
Reading from 11700: heap size 940 MB, throughput 0.88494
Reading from 11701: heap size 148 MB, throughput 0.997216
Reading from 11700: heap size 941 MB, throughput 0.864533
Reading from 11700: heap size 943 MB, throughput 0.917961
Reading from 11700: heap size 945 MB, throughput 0.745414
Reading from 11700: heap size 946 MB, throughput 0.671504
Reading from 11701: heap size 152 MB, throughput 0.997951
Reading from 11700: heap size 966 MB, throughput 0.701776
Reading from 11701: heap size 152 MB, throughput 0.996639
Reading from 11701: heap size 156 MB, throughput 0.989322
Reading from 11700: heap size 974 MB, throughput 0.0641932
Reading from 11700: heap size 1083 MB, throughput 0.596216
Reading from 11700: heap size 1088 MB, throughput 0.777359
Reading from 11700: heap size 1088 MB, throughput 0.785041
Reading from 11701: heap size 156 MB, throughput 0.995446
Reading from 11700: heap size 1092 MB, throughput 0.767334
Reading from 11700: heap size 1092 MB, throughput 0.772358
Reading from 11700: heap size 1097 MB, throughput 0.756331
Reading from 11700: heap size 1103 MB, throughput 0.798054
Reading from 11701: heap size 161 MB, throughput 0.996394
Reading from 11701: heap size 162 MB, throughput 0.997771
Reading from 11700: heap size 1105 MB, throughput 0.966478
Reading from 11701: heap size 166 MB, throughput 0.99841
Equal recommendation: 3057 MB each
Reading from 11700: heap size 1122 MB, throughput 0.968048
Reading from 11701: heap size 167 MB, throughput 0.998515
Reading from 11701: heap size 170 MB, throughput 0.998724
Reading from 11700: heap size 1125 MB, throughput 0.949518
Reading from 11701: heap size 171 MB, throughput 0.997314
Reading from 11701: heap size 175 MB, throughput 0.994193
Reading from 11701: heap size 175 MB, throughput 0.994818
Reading from 11700: heap size 1133 MB, throughput 0.970417
Reading from 11701: heap size 180 MB, throughput 0.997251
Reading from 11701: heap size 181 MB, throughput 0.997346
Reading from 11700: heap size 1135 MB, throughput 0.957257
Reading from 11701: heap size 185 MB, throughput 0.998097
Reading from 11700: heap size 1131 MB, throughput 0.960013
Reading from 11701: heap size 186 MB, throughput 0.997742
Reading from 11701: heap size 190 MB, throughput 0.997967
Reading from 11700: heap size 1136 MB, throughput 0.962783
Equal recommendation: 3057 MB each
Reading from 11701: heap size 190 MB, throughput 0.997411
Reading from 11700: heap size 1130 MB, throughput 0.963552
Reading from 11701: heap size 195 MB, throughput 0.997832
Reading from 11701: heap size 195 MB, throughput 0.997506
Reading from 11700: heap size 1134 MB, throughput 0.959723
Reading from 11701: heap size 199 MB, throughput 0.997949
Reading from 11700: heap size 1140 MB, throughput 0.945795
Reading from 11701: heap size 199 MB, throughput 0.997543
Reading from 11701: heap size 203 MB, throughput 0.997774
Reading from 11700: heap size 1140 MB, throughput 0.959789
Reading from 11701: heap size 203 MB, throughput 0.997791
Reading from 11701: heap size 206 MB, throughput 0.997788
Reading from 11700: heap size 1145 MB, throughput 0.960898
Equal recommendation: 3057 MB each
Reading from 11701: heap size 206 MB, throughput 0.994002
Reading from 11701: heap size 209 MB, throughput 0.992548
Reading from 11700: heap size 1148 MB, throughput 0.963117
Reading from 11701: heap size 210 MB, throughput 0.997986
Reading from 11701: heap size 216 MB, throughput 0.998254
Reading from 11700: heap size 1153 MB, throughput 0.956483
Reading from 11701: heap size 217 MB, throughput 0.997823
Reading from 11700: heap size 1156 MB, throughput 0.954189
Reading from 11701: heap size 221 MB, throughput 0.998249
Reading from 11701: heap size 222 MB, throughput 0.997837
Reading from 11700: heap size 1161 MB, throughput 0.957393
Reading from 11701: heap size 227 MB, throughput 0.998116
Reading from 11700: heap size 1166 MB, throughput 0.95883
Equal recommendation: 3057 MB each
Reading from 11701: heap size 227 MB, throughput 0.997751
Reading from 11700: heap size 1172 MB, throughput 0.95521
Reading from 11701: heap size 231 MB, throughput 0.998108
Reading from 11701: heap size 231 MB, throughput 0.997756
Reading from 11700: heap size 1175 MB, throughput 0.961573
Reading from 11701: heap size 235 MB, throughput 0.998185
Reading from 11700: heap size 1182 MB, throughput 0.957088
Reading from 11701: heap size 235 MB, throughput 0.997862
Reading from 11701: heap size 239 MB, throughput 0.995536
Reading from 11701: heap size 239 MB, throughput 0.993771
Reading from 11700: heap size 1184 MB, throughput 0.95803
Reading from 11701: heap size 245 MB, throughput 0.998124
Equal recommendation: 3057 MB each
Reading from 11700: heap size 1190 MB, throughput 0.959714
Reading from 11701: heap size 245 MB, throughput 0.998181
Reading from 11701: heap size 251 MB, throughput 0.998537
Reading from 11700: heap size 1191 MB, throughput 0.959143
Reading from 11701: heap size 251 MB, throughput 0.998136
Reading from 11700: heap size 1197 MB, throughput 0.962394
Reading from 11701: heap size 256 MB, throughput 0.998344
Reading from 11701: heap size 256 MB, throughput 0.997969
Reading from 11700: heap size 1197 MB, throughput 0.961251
Reading from 11701: heap size 261 MB, throughput 0.998306
Equal recommendation: 3057 MB each
Reading from 11701: heap size 261 MB, throughput 0.997848
Reading from 11700: heap size 1202 MB, throughput 0.536932
Reading from 11701: heap size 265 MB, throughput 0.998248
Reading from 11701: heap size 265 MB, throughput 0.997098
Reading from 11700: heap size 1297 MB, throughput 0.942283
Reading from 11701: heap size 270 MB, throughput 0.863706
Reading from 11701: heap size 277 MB, throughput 0.99897
Reading from 11700: heap size 1295 MB, throughput 0.98257
Reading from 11701: heap size 285 MB, throughput 0.999023
Reading from 11700: heap size 1303 MB, throughput 0.983062
Reading from 11701: heap size 285 MB, throughput 0.999107
Equal recommendation: 3057 MB each
Reading from 11700: heap size 1312 MB, throughput 0.979721
Reading from 11701: heap size 292 MB, throughput 0.999224
Reading from 11701: heap size 292 MB, throughput 0.999087
Reading from 11700: heap size 1312 MB, throughput 0.977996
Reading from 11701: heap size 297 MB, throughput 0.999199
Reading from 11700: heap size 1307 MB, throughput 0.974517
Reading from 11701: heap size 298 MB, throughput 0.99908
Reading from 11700: heap size 1218 MB, throughput 0.976108
Reading from 11701: heap size 301 MB, throughput 0.999175
Equal recommendation: 3057 MB each
Reading from 11701: heap size 302 MB, throughput 0.998378
Reading from 11701: heap size 306 MB, throughput 0.991195
Reading from 11700: heap size 1298 MB, throughput 0.970075
Reading from 11701: heap size 306 MB, throughput 0.998417
Reading from 11700: heap size 1237 MB, throughput 0.972271
Reading from 11701: heap size 316 MB, throughput 0.998628
Reading from 11700: heap size 1297 MB, throughput 0.970345
Reading from 11701: heap size 317 MB, throughput 0.998267
Reading from 11700: heap size 1300 MB, throughput 0.968053
Reading from 11701: heap size 325 MB, throughput 0.998847
Equal recommendation: 3057 MB each
Reading from 11701: heap size 325 MB, throughput 0.99832
Reading from 11700: heap size 1302 MB, throughput 0.967809
Reading from 11701: heap size 332 MB, throughput 0.998301
Reading from 11700: heap size 1304 MB, throughput 0.966768
Reading from 11701: heap size 333 MB, throughput 0.998263
Reading from 11700: heap size 1308 MB, throughput 0.964848
Reading from 11701: heap size 340 MB, throughput 0.998465
Reading from 11701: heap size 340 MB, throughput 0.992987
Reading from 11700: heap size 1313 MB, throughput 0.962299
Reading from 11701: heap size 346 MB, throughput 0.99852
Equal recommendation: 3057 MB each
Reading from 11700: heap size 1318 MB, throughput 0.963393
Reading from 11701: heap size 347 MB, throughput 0.998158
Reading from 11701: heap size 356 MB, throughput 0.998358
Reading from 11700: heap size 1325 MB, throughput 0.961213
Reading from 11701: heap size 356 MB, throughput 0.998074
Reading from 11700: heap size 1332 MB, throughput 0.962058
Reading from 11701: heap size 364 MB, throughput 0.998233
Equal recommendation: 3057 MB each
Reading from 11701: heap size 365 MB, throughput 0.998054
Reading from 11701: heap size 373 MB, throughput 0.998546
Reading from 11701: heap size 373 MB, throughput 0.993121
Reading from 11701: heap size 380 MB, throughput 0.998432
Reading from 11701: heap size 381 MB, throughput 0.998148
Equal recommendation: 3057 MB each
Reading from 11701: heap size 390 MB, throughput 0.99838
Reading from 11700: heap size 1337 MB, throughput 0.990786
Reading from 11701: heap size 392 MB, throughput 0.998181
Reading from 11701: heap size 400 MB, throughput 0.998257
Reading from 11701: heap size 401 MB, throughput 0.998155
Reading from 11701: heap size 411 MB, throughput 0.99611
Equal recommendation: 3057 MB each
Reading from 11701: heap size 411 MB, throughput 0.998
Reading from 11701: heap size 422 MB, throughput 0.998442
Reading from 11700: heap size 1317 MB, throughput 0.918278
Reading from 11701: heap size 422 MB, throughput 0.998314
Equal recommendation: 3057 MB each
Reading from 11701: heap size 432 MB, throughput 0.998453
Reading from 11700: heap size 1456 MB, throughput 0.993764
Reading from 11700: heap size 1503 MB, throughput 0.937263
Reading from 11700: heap size 1350 MB, throughput 0.862865
Reading from 11700: heap size 1492 MB, throughput 0.875684
Reading from 11700: heap size 1498 MB, throughput 0.87431
Reading from 11700: heap size 1488 MB, throughput 0.861456
Reading from 11700: heap size 1495 MB, throughput 0.887692
Reading from 11700: heap size 1491 MB, throughput 0.963648
Reading from 11701: heap size 432 MB, throughput 0.998391
Reading from 11700: heap size 1498 MB, throughput 0.990918
Reading from 11701: heap size 440 MB, throughput 0.998374
Reading from 11701: heap size 441 MB, throughput 0.994709
Reading from 11700: heap size 1519 MB, throughput 0.989178
Equal recommendation: 3057 MB each
Reading from 11701: heap size 449 MB, throughput 0.998398
Reading from 11700: heap size 1522 MB, throughput 0.985482
Reading from 11701: heap size 451 MB, throughput 0.998493
Reading from 11700: heap size 1522 MB, throughput 0.984779
Reading from 11701: heap size 461 MB, throughput 0.998534
Reading from 11700: heap size 1528 MB, throughput 0.982728
Reading from 11701: heap size 462 MB, throughput 0.998424
Reading from 11700: heap size 1513 MB, throughput 0.981578
Equal recommendation: 3057 MB each
Reading from 11701: heap size 472 MB, throughput 0.998474
Reading from 11700: heap size 1410 MB, throughput 0.976569
Reading from 11701: heap size 472 MB, throughput 0.995844
Reading from 11700: heap size 1502 MB, throughput 0.976156
Reading from 11701: heap size 481 MB, throughput 0.998335
Reading from 11700: heap size 1511 MB, throughput 0.975734
Reading from 11701: heap size 482 MB, throughput 0.998565
Equal recommendation: 3057 MB each
Reading from 11700: heap size 1503 MB, throughput 0.975325
Reading from 11701: heap size 491 MB, throughput 0.998592
Reading from 11700: heap size 1507 MB, throughput 0.974082
Reading from 11701: heap size 493 MB, throughput 0.998488
Reading from 11700: heap size 1512 MB, throughput 0.97317
Reading from 11701: heap size 503 MB, throughput 0.998532
Equal recommendation: 3057 MB each
Reading from 11701: heap size 503 MB, throughput 0.947868
Reading from 11700: heap size 1515 MB, throughput 0.969356
Reading from 11701: heap size 520 MB, throughput 0.999424
Reading from 11700: heap size 1522 MB, throughput 0.968955
Reading from 11701: heap size 521 MB, throughput 0.99939
Reading from 11700: heap size 1529 MB, throughput 0.965563
Equal recommendation: 3057 MB each
Reading from 11701: heap size 532 MB, throughput 0.999468
Reading from 11700: heap size 1537 MB, throughput 0.965826
Reading from 11701: heap size 534 MB, throughput 0.999393
Reading from 11700: heap size 1548 MB, throughput 0.963288
Reading from 11701: heap size 542 MB, throughput 0.99832
Reading from 11700: heap size 1558 MB, throughput 0.963125
Reading from 11701: heap size 543 MB, throughput 0.998591
Equal recommendation: 3057 MB each
Reading from 11700: heap size 1566 MB, throughput 0.965622
Reading from 11701: heap size 555 MB, throughput 0.999083
Reading from 11700: heap size 1576 MB, throughput 0.965506
Reading from 11701: heap size 555 MB, throughput 0.998872
Reading from 11700: heap size 1581 MB, throughput 0.965379
Reading from 11701: heap size 565 MB, throughput 0.999024
Equal recommendation: 3057 MB each
Reading from 11700: heap size 1591 MB, throughput 0.965502
Reading from 11701: heap size 565 MB, throughput 0.99824
Reading from 11700: heap size 1594 MB, throughput 0.964893
Reading from 11701: heap size 575 MB, throughput 0.998299
Reading from 11700: heap size 1604 MB, throughput 0.963598
Reading from 11701: heap size 575 MB, throughput 0.99883
Equal recommendation: 3057 MB each
Reading from 11700: heap size 1605 MB, throughput 0.965266
Reading from 11701: heap size 586 MB, throughput 0.998758
Reading from 11700: heap size 1616 MB, throughput 0.965877
Reading from 11701: heap size 587 MB, throughput 0.998664
Equal recommendation: 3057 MB each
Reading from 11700: heap size 1616 MB, throughput 0.755877
Client 11701 died
Clients: 1
Reading from 11700: heap size 1671 MB, throughput 0.993781
Reading from 11700: heap size 1673 MB, throughput 0.99157
Recommendation: one client; give it all the memory
Reading from 11700: heap size 1690 MB, throughput 0.989658
Reading from 11700: heap size 1697 MB, throughput 0.987776
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 11700: heap size 1698 MB, throughput 0.997291
Recommendation: one client; give it all the memory
Reading from 11700: heap size 1561 MB, throughput 0.98835
Reading from 11700: heap size 1684 MB, throughput 0.869987
Reading from 11700: heap size 1696 MB, throughput 0.770547
Reading from 11700: heap size 1719 MB, throughput 0.806176
Reading from 11700: heap size 1742 MB, throughput 0.786759
Client 11700 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
