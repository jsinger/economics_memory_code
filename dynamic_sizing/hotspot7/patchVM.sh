#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ICEDTEA_BUILD_ROOT="${DIR}/icedtea7/icedtea-build"

function doFile()
{
    NEWFILE="$(readlink -f "${1}")"
    REPLACEFILE="$(realpath -s "${2}")"

    if [ -e "${REPLACEFILE}.original" ]; then
        echo "${REPLACEFILE}.original already exists!"
        return 1
    fi

    mv "${REPLACEFILE}" "${REPLACEFILE}.original"
    ln -s "${NEWFILE}" "${REPLACEFILE}"
    touch "${REPLACEFILE}"
}

doFile "${DIR}/vm.make" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/make/linux/makefiles/vm.make"
doFile "${DIR}/launcher.make" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/make/linux/makefiles/launcher.make"
doFile "${DIR}/buildtree.make" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/make/linux/makefiles/buildtree.make"
doFile "${DIR}/gcc.make" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/make/linux/makefiles/gcc.make"
doFile "${DIR}/psAdaptiveSizePolicy.cpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/parallelScavenge/psAdaptiveSizePolicy.cpp"
doFile "${DIR}/psAdaptiveSizePolicy.hpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/parallelScavenge/psAdaptiveSizePolicy.hpp"
doFile "${DIR}/adaptiveSizePolicy.hpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/shared/adaptiveSizePolicy.hpp"
doFile "${DIR}/adaptiveSizePolicy.cpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/shared/adaptiveSizePolicy.cpp"
doFile "${DIR}/mapfile-vers-product" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/make/linux/makefiles/mapfile-vers-product"
doFile "${DIR}/psScavenge.cpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/parallelScavenge/psScavenge.cpp"
doFile "${DIR}/psScavenge.hpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/parallelScavenge/psScavenge.hpp"
doFile "${DIR}/psMarkSweep.cpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/parallelScavenge/psMarkSweep.cpp"
doFile "${DIR}/psMarkSweep.hpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/parallelScavenge/psMarkSweep.hpp"
doFile "${DIR}/psParallelCompact.cpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/parallelScavenge/psParallelCompact.cpp"
doFile "${DIR}/psParallelCompact.hpp" "${ICEDTEA_BUILD_ROOT}/openjdk/hotspot/src/share/vm/gc_implementation/parallelScavenge/psParallelCompact.hpp"

touch "${DIR}/icedtea7/patch.done"
