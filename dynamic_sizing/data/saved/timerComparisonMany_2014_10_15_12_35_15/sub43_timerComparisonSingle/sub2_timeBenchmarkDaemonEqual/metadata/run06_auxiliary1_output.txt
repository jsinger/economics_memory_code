economemd
    total memory: 2208 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub43_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 17405: heap size 9 MB, throughput 0.992055
Clients: 1
Client 17405 has a minimum heap size of 276 MB
Reading from 17404: heap size 9 MB, throughput 0.968301
Clients: 2
Client 17404 has a minimum heap size of 276 MB
Reading from 17405: heap size 9 MB, throughput 0.975979
Reading from 17404: heap size 9 MB, throughput 0.980237
Reading from 17405: heap size 9 MB, throughput 0.962813
Reading from 17405: heap size 9 MB, throughput 0.950937
Reading from 17404: heap size 11 MB, throughput 0.971676
Reading from 17405: heap size 11 MB, throughput 0.978247
Reading from 17404: heap size 11 MB, throughput 0.95946
Reading from 17405: heap size 11 MB, throughput 0.975852
Reading from 17405: heap size 17 MB, throughput 0.965401
Reading from 17404: heap size 15 MB, throughput 0.812541
Reading from 17405: heap size 17 MB, throughput 0.536863
Reading from 17404: heap size 19 MB, throughput 0.949778
Reading from 17405: heap size 29 MB, throughput 0.850296
Reading from 17404: heap size 23 MB, throughput 0.955977
Reading from 17405: heap size 30 MB, throughput 0.87192
Reading from 17404: heap size 27 MB, throughput 0.935957
Reading from 17405: heap size 35 MB, throughput 0.329898
Reading from 17404: heap size 27 MB, throughput 0.458611
Reading from 17405: heap size 47 MB, throughput 0.796827
Reading from 17404: heap size 37 MB, throughput 0.896953
Reading from 17404: heap size 41 MB, throughput 0.711148
Reading from 17404: heap size 42 MB, throughput 0.792914
Reading from 17405: heap size 48 MB, throughput 0.557809
Reading from 17405: heap size 64 MB, throughput 0.785672
Reading from 17404: heap size 46 MB, throughput 0.274109
Reading from 17405: heap size 69 MB, throughput 0.938297
Reading from 17404: heap size 61 MB, throughput 0.768079
Reading from 17404: heap size 68 MB, throughput 0.855
Reading from 17405: heap size 69 MB, throughput 0.212842
Reading from 17404: heap size 69 MB, throughput 0.242623
Reading from 17405: heap size 98 MB, throughput 0.71838
Reading from 17404: heap size 91 MB, throughput 0.655379
Reading from 17404: heap size 94 MB, throughput 0.873276
Reading from 17405: heap size 98 MB, throughput 0.794962
Reading from 17404: heap size 98 MB, throughput 0.743931
Reading from 17405: heap size 100 MB, throughput 0.725712
Reading from 17404: heap size 100 MB, throughput 0.165743
Reading from 17405: heap size 103 MB, throughput 0.250335
Reading from 17404: heap size 127 MB, throughput 0.747003
Reading from 17405: heap size 129 MB, throughput 0.747631
Reading from 17405: heap size 133 MB, throughput 0.715703
Reading from 17404: heap size 131 MB, throughput 0.664634
Reading from 17405: heap size 136 MB, throughput 0.759979
Reading from 17404: heap size 134 MB, throughput 0.705797
Reading from 17404: heap size 138 MB, throughput 0.618521
Reading from 17405: heap size 138 MB, throughput 0.119369
Reading from 17404: heap size 143 MB, throughput 0.131003
Reading from 17405: heap size 176 MB, throughput 0.584723
Reading from 17404: heap size 176 MB, throughput 0.53399
Reading from 17405: heap size 178 MB, throughput 0.654476
Reading from 17404: heap size 184 MB, throughput 0.497216
Reading from 17405: heap size 181 MB, throughput 0.557568
Reading from 17404: heap size 186 MB, throughput 0.63164
Reading from 17405: heap size 186 MB, throughput 0.627895
Reading from 17404: heap size 192 MB, throughput 0.160558
Reading from 17405: heap size 196 MB, throughput 0.411604
Reading from 17404: heap size 228 MB, throughput 0.85215
Reading from 17404: heap size 237 MB, throughput 0.940842
Reading from 17405: heap size 234 MB, throughput 0.917593
Reading from 17404: heap size 238 MB, throughput 0.803087
Reading from 17405: heap size 240 MB, throughput 0.7541
Reading from 17405: heap size 244 MB, throughput 0.650927
Reading from 17404: heap size 244 MB, throughput 0.762268
Reading from 17404: heap size 247 MB, throughput 0.545728
Reading from 17405: heap size 250 MB, throughput 0.886064
Reading from 17404: heap size 252 MB, throughput 0.851991
Reading from 17405: heap size 253 MB, throughput 0.806767
Reading from 17404: heap size 255 MB, throughput 0.811192
Reading from 17405: heap size 258 MB, throughput 0.740843
Reading from 17404: heap size 260 MB, throughput 0.79053
Reading from 17405: heap size 260 MB, throughput 0.848822
Reading from 17404: heap size 262 MB, throughput 0.714632
Reading from 17405: heap size 265 MB, throughput 0.725174
Reading from 17404: heap size 268 MB, throughput 0.714616
Reading from 17405: heap size 267 MB, throughput 0.517965
Reading from 17404: heap size 272 MB, throughput 0.617709
Reading from 17405: heap size 273 MB, throughput 0.717911
Reading from 17404: heap size 278 MB, throughput 0.774712
Reading from 17405: heap size 277 MB, throughput 0.72136
Reading from 17405: heap size 286 MB, throughput 0.759018
Reading from 17405: heap size 286 MB, throughput 0.920268
Reading from 17404: heap size 279 MB, throughput 0.289104
Reading from 17404: heap size 312 MB, throughput 0.848974
Reading from 17405: heap size 281 MB, throughput 0.163491
Reading from 17404: heap size 315 MB, throughput 0.765588
Reading from 17405: heap size 324 MB, throughput 0.703643
Reading from 17404: heap size 314 MB, throughput 0.813802
Reading from 17405: heap size 326 MB, throughput 0.850308
Reading from 17405: heap size 326 MB, throughput 0.85057
Reading from 17405: heap size 332 MB, throughput 0.897229
Reading from 17404: heap size 315 MB, throughput 0.159399
Reading from 17405: heap size 332 MB, throughput 0.788961
Reading from 17404: heap size 363 MB, throughput 0.84863
Reading from 17405: heap size 330 MB, throughput 0.746478
Reading from 17405: heap size 333 MB, throughput 0.653913
Reading from 17404: heap size 363 MB, throughput 0.92972
Reading from 17405: heap size 340 MB, throughput 0.671981
Reading from 17404: heap size 367 MB, throughput 0.79787
Reading from 17405: heap size 340 MB, throughput 0.710419
Reading from 17404: heap size 368 MB, throughput 0.751175
Reading from 17405: heap size 349 MB, throughput 0.604814
Reading from 17404: heap size 364 MB, throughput 0.635097
Reading from 17404: heap size 367 MB, throughput 0.688438
Equal recommendation: 1104 MB each
Reading from 17404: heap size 364 MB, throughput 0.700662
Reading from 17404: heap size 368 MB, throughput 0.940289
Reading from 17405: heap size 349 MB, throughput 0.970848
Reading from 17404: heap size 373 MB, throughput 0.979497
Reading from 17405: heap size 353 MB, throughput 0.979776
Reading from 17404: heap size 374 MB, throughput 0.947306
Reading from 17405: heap size 356 MB, throughput 0.971873
Reading from 17404: heap size 375 MB, throughput 0.977985
Reading from 17405: heap size 354 MB, throughput 0.687451
Reading from 17404: heap size 378 MB, throughput 0.974681
Reading from 17405: heap size 403 MB, throughput 0.975474
Reading from 17404: heap size 377 MB, throughput 0.978765
Reading from 17405: heap size 406 MB, throughput 0.993091
Reading from 17404: heap size 379 MB, throughput 0.972717
Equal recommendation: 1104 MB each
Reading from 17405: heap size 409 MB, throughput 0.993157
Reading from 17404: heap size 376 MB, throughput 0.977315
Reading from 17405: heap size 413 MB, throughput 0.988257
Reading from 17404: heap size 379 MB, throughput 0.981529
Reading from 17405: heap size 414 MB, throughput 0.986039
Reading from 17404: heap size 377 MB, throughput 0.978557
Reading from 17405: heap size 411 MB, throughput 0.983411
Reading from 17404: heap size 379 MB, throughput 0.977638
Reading from 17405: heap size 414 MB, throughput 0.983973
Reading from 17404: heap size 381 MB, throughput 0.971056
Reading from 17404: heap size 382 MB, throughput 0.972444
Reading from 17405: heap size 409 MB, throughput 0.984538
Equal recommendation: 1104 MB each
Reading from 17404: heap size 384 MB, throughput 0.972863
Reading from 17405: heap size 412 MB, throughput 0.981922
Reading from 17404: heap size 385 MB, throughput 0.977626
Reading from 17405: heap size 411 MB, throughput 0.984723
Reading from 17404: heap size 385 MB, throughput 0.876607
Reading from 17405: heap size 411 MB, throughput 0.802792
Reading from 17404: heap size 387 MB, throughput 0.691467
Reading from 17405: heap size 408 MB, throughput 0.713674
Reading from 17404: heap size 393 MB, throughput 0.682525
Reading from 17405: heap size 417 MB, throughput 0.670438
Reading from 17404: heap size 396 MB, throughput 0.788592
Reading from 17405: heap size 427 MB, throughput 0.829941
Reading from 17404: heap size 405 MB, throughput 0.979063
Reading from 17405: heap size 431 MB, throughput 0.986268
Reading from 17404: heap size 407 MB, throughput 0.982919
Reading from 17405: heap size 429 MB, throughput 0.973061
Reading from 17405: heap size 434 MB, throughput 0.96552
Reading from 17404: heap size 410 MB, throughput 0.981926
Reading from 17404: heap size 412 MB, throughput 0.98382
Reading from 17405: heap size 432 MB, throughput 0.98469
Equal recommendation: 1104 MB each
Reading from 17404: heap size 413 MB, throughput 0.984153
Reading from 17405: heap size 436 MB, throughput 0.980792
Reading from 17404: heap size 415 MB, throughput 0.982695
Reading from 17405: heap size 434 MB, throughput 0.988077
Reading from 17404: heap size 414 MB, throughput 0.984184
Reading from 17405: heap size 437 MB, throughput 0.982631
Reading from 17404: heap size 416 MB, throughput 0.986896
Reading from 17405: heap size 437 MB, throughput 0.984779
Reading from 17404: heap size 412 MB, throughput 0.983419
Reading from 17405: heap size 439 MB, throughput 0.98339
Equal recommendation: 1104 MB each
Reading from 17405: heap size 439 MB, throughput 0.979865
Reading from 17404: heap size 415 MB, throughput 0.981477
Reading from 17404: heap size 415 MB, throughput 0.98355
Reading from 17405: heap size 440 MB, throughput 0.991137
Reading from 17405: heap size 443 MB, throughput 0.912211
Reading from 17404: heap size 416 MB, throughput 0.955344
Reading from 17404: heap size 416 MB, throughput 0.865199
Reading from 17405: heap size 444 MB, throughput 0.782056
Reading from 17405: heap size 449 MB, throughput 0.804562
Reading from 17404: heap size 417 MB, throughput 0.660988
Reading from 17404: heap size 422 MB, throughput 0.979146
Reading from 17405: heap size 450 MB, throughput 0.988727
Reading from 17404: heap size 422 MB, throughput 0.9901
Reading from 17405: heap size 459 MB, throughput 0.991017
Reading from 17404: heap size 429 MB, throughput 0.988482
Equal recommendation: 1104 MB each
Reading from 17405: heap size 460 MB, throughput 0.990734
Reading from 17404: heap size 430 MB, throughput 0.988143
Reading from 17405: heap size 464 MB, throughput 0.989972
Reading from 17404: heap size 433 MB, throughput 0.991941
Reading from 17405: heap size 466 MB, throughput 0.988647
Reading from 17404: heap size 434 MB, throughput 0.985568
Reading from 17405: heap size 466 MB, throughput 0.986195
Reading from 17404: heap size 433 MB, throughput 0.985378
Reading from 17405: heap size 468 MB, throughput 0.985885
Equal recommendation: 1104 MB each
Reading from 17404: heap size 435 MB, throughput 0.984361
Reading from 17405: heap size 469 MB, throughput 0.98568
Reading from 17404: heap size 435 MB, throughput 0.98526
Reading from 17405: heap size 470 MB, throughput 0.992381
Reading from 17404: heap size 436 MB, throughput 0.991214
Reading from 17405: heap size 473 MB, throughput 0.933518
Reading from 17405: heap size 473 MB, throughput 0.754777
Reading from 17405: heap size 479 MB, throughput 0.91093
Reading from 17404: heap size 439 MB, throughput 0.284499
Reading from 17404: heap size 478 MB, throughput 0.795044
Reading from 17404: heap size 481 MB, throughput 0.979373
Reading from 17404: heap size 485 MB, throughput 0.993784
Reading from 17405: heap size 480 MB, throughput 0.986893
Equal recommendation: 1104 MB each
Reading from 17404: heap size 491 MB, throughput 0.993803
Reading from 17405: heap size 487 MB, throughput 0.993541
Reading from 17404: heap size 492 MB, throughput 0.991057
Reading from 17405: heap size 489 MB, throughput 0.991226
Reading from 17404: heap size 491 MB, throughput 0.99002
Reading from 17405: heap size 490 MB, throughput 0.989784
Reading from 17404: heap size 493 MB, throughput 0.988799
Reading from 17405: heap size 493 MB, throughput 0.991235
Equal recommendation: 1104 MB each
Reading from 17404: heap size 488 MB, throughput 0.989272
Reading from 17405: heap size 494 MB, throughput 0.985275
Reading from 17404: heap size 490 MB, throughput 0.985686
Reading from 17405: heap size 495 MB, throughput 0.988679
Reading from 17404: heap size 491 MB, throughput 0.983765
Reading from 17405: heap size 499 MB, throughput 0.988035
Reading from 17405: heap size 500 MB, throughput 0.897543
Reading from 17405: heap size 504 MB, throughput 0.912521
Reading from 17404: heap size 493 MB, throughput 0.987856
Reading from 17404: heap size 496 MB, throughput 0.886503
Reading from 17404: heap size 497 MB, throughput 0.847325
Reading from 17405: heap size 506 MB, throughput 0.992986
Reading from 17404: heap size 505 MB, throughput 0.982979
Equal recommendation: 1104 MB each
Reading from 17404: heap size 510 MB, throughput 0.986927
Reading from 17405: heap size 511 MB, throughput 0.993857
Reading from 17404: heap size 507 MB, throughput 0.988208
Reading from 17405: heap size 513 MB, throughput 0.991065
Reading from 17404: heap size 510 MB, throughput 0.987241
Reading from 17405: heap size 513 MB, throughput 0.991955
Reading from 17404: heap size 513 MB, throughput 0.987099
Equal recommendation: 1104 MB each
Reading from 17405: heap size 515 MB, throughput 0.98934
Reading from 17404: heap size 514 MB, throughput 0.982572
Reading from 17405: heap size 517 MB, throughput 0.989187
Reading from 17404: heap size 518 MB, throughput 0.981756
Reading from 17405: heap size 517 MB, throughput 0.992774
Reading from 17405: heap size 518 MB, throughput 0.933391
Reading from 17405: heap size 520 MB, throughput 0.891849
Reading from 17404: heap size 519 MB, throughput 0.993419
Reading from 17404: heap size 519 MB, throughput 0.971868
Reading from 17404: heap size 521 MB, throughput 0.895189
Reading from 17404: heap size 526 MB, throughput 0.938535
Equal recommendation: 1104 MB each
Reading from 17405: heap size 526 MB, throughput 0.991852
Reading from 17404: heap size 527 MB, throughput 0.990483
Reading from 17405: heap size 528 MB, throughput 0.993367
Reading from 17404: heap size 530 MB, throughput 0.993742
Reading from 17405: heap size 532 MB, throughput 0.992102
Reading from 17404: heap size 532 MB, throughput 0.989827
Reading from 17404: heap size 532 MB, throughput 0.987774
Equal recommendation: 1104 MB each
Reading from 17405: heap size 533 MB, throughput 0.991753
Reading from 17404: heap size 534 MB, throughput 0.99088
Reading from 17405: heap size 533 MB, throughput 0.989527
Reading from 17404: heap size 531 MB, throughput 0.98731
Reading from 17405: heap size 535 MB, throughput 0.989339
Reading from 17404: heap size 533 MB, throughput 0.993417
Reading from 17405: heap size 538 MB, throughput 0.988909
Reading from 17405: heap size 539 MB, throughput 0.902871
Reading from 17404: heap size 535 MB, throughput 0.915806
Reading from 17404: heap size 536 MB, throughput 0.898212
Equal recommendation: 1104 MB each
Reading from 17405: heap size 543 MB, throughput 0.986607
Reading from 17404: heap size 542 MB, throughput 0.989608
Reading from 17405: heap size 544 MB, throughput 0.992572
Reading from 17404: heap size 543 MB, throughput 0.994482
Reading from 17405: heap size 548 MB, throughput 0.992311
Reading from 17404: heap size 545 MB, throughput 0.990525
Reading from 17405: heap size 549 MB, throughput 0.993262
Equal recommendation: 1104 MB each
Reading from 17404: heap size 548 MB, throughput 0.990746
Reading from 17405: heap size 547 MB, throughput 0.989124
Reading from 17404: heap size 546 MB, throughput 0.991319
Reading from 17405: heap size 550 MB, throughput 0.989254
Reading from 17404: heap size 549 MB, throughput 0.988579
Reading from 17405: heap size 552 MB, throughput 0.989891
Reading from 17405: heap size 553 MB, throughput 0.915723
Reading from 17404: heap size 552 MB, throughput 0.993558
Reading from 17404: heap size 552 MB, throughput 0.925013
Equal recommendation: 1104 MB each
Reading from 17404: heap size 553 MB, throughput 0.905695
Reading from 17405: heap size 554 MB, throughput 0.985283
Reading from 17404: heap size 555 MB, throughput 0.991974
Reading from 17405: heap size 556 MB, throughput 0.993168
Reading from 17404: heap size 562 MB, throughput 0.994377
Reading from 17405: heap size 559 MB, throughput 0.993059
Reading from 17404: heap size 563 MB, throughput 0.993172
Equal recommendation: 1104 MB each
Reading from 17405: heap size 561 MB, throughput 0.991493
Reading from 17404: heap size 563 MB, throughput 0.989833
Reading from 17405: heap size 560 MB, throughput 0.990127
Reading from 17404: heap size 566 MB, throughput 0.989998
Reading from 17405: heap size 562 MB, throughput 0.989377
Reading from 17404: heap size 567 MB, throughput 0.991954
Reading from 17405: heap size 564 MB, throughput 0.987195
Reading from 17405: heap size 565 MB, throughput 0.917343
Equal recommendation: 1104 MB each
Reading from 17404: heap size 568 MB, throughput 0.978045
Reading from 17404: heap size 572 MB, throughput 0.920829
Reading from 17405: heap size 569 MB, throughput 0.992015
Reading from 17404: heap size 574 MB, throughput 0.990559
Reading from 17405: heap size 571 MB, throughput 0.988209
Reading from 17404: heap size 580 MB, throughput 0.990557
Equal recommendation: 1104 MB each
Reading from 17404: heap size 581 MB, throughput 0.989467
Reading from 17405: heap size 573 MB, throughput 0.992022
Reading from 17404: heap size 581 MB, throughput 0.990319
Reading from 17405: heap size 575 MB, throughput 0.991786
Reading from 17404: heap size 583 MB, throughput 0.990965
Reading from 17405: heap size 574 MB, throughput 0.991251
Equal recommendation: 1104 MB each
Reading from 17404: heap size 583 MB, throughput 0.99231
Reading from 17405: heap size 576 MB, throughput 0.992884
Reading from 17405: heap size 579 MB, throughput 0.927954
Reading from 17404: heap size 584 MB, throughput 0.96891
Reading from 17404: heap size 584 MB, throughput 0.935264
Reading from 17405: heap size 580 MB, throughput 0.991395
Reading from 17404: heap size 586 MB, throughput 0.992056
Reading from 17405: heap size 586 MB, throughput 0.99296
Reading from 17404: heap size 592 MB, throughput 0.992443
Equal recommendation: 1104 MB each
Reading from 17405: heap size 587 MB, throughput 0.992242
Reading from 17404: heap size 593 MB, throughput 0.993991
Reading from 17405: heap size 586 MB, throughput 0.989307
Reading from 17404: heap size 593 MB, throughput 0.989967
Reading from 17405: heap size 589 MB, throughput 0.9897
Reading from 17404: heap size 595 MB, throughput 0.989317
Equal recommendation: 1104 MB each
Reading from 17405: heap size 591 MB, throughput 0.994255
Reading from 17405: heap size 592 MB, throughput 0.929449
Reading from 17404: heap size 597 MB, throughput 0.991015
Reading from 17404: heap size 598 MB, throughput 0.8969
Reading from 17405: heap size 592 MB, throughput 0.98953
Reading from 17404: heap size 601 MB, throughput 0.98774
Reading from 17405: heap size 595 MB, throughput 0.993416
Reading from 17404: heap size 602 MB, throughput 0.993195
Equal recommendation: 1104 MB each
Reading from 17405: heap size 596 MB, throughput 0.994286
Reading from 17404: heap size 606 MB, throughput 0.993331
Reading from 17405: heap size 598 MB, throughput 0.991592
Reading from 17404: heap size 608 MB, throughput 0.991845
Reading from 17405: heap size 599 MB, throughput 0.991562
Reading from 17404: heap size 606 MB, throughput 0.991141
Equal recommendation: 1104 MB each
Reading from 17404: heap size 609 MB, throughput 0.992321
Reading from 17405: heap size 600 MB, throughput 0.991778
Reading from 17404: heap size 612 MB, throughput 0.935388
Reading from 17405: heap size 603 MB, throughput 0.945885
Reading from 17404: heap size 613 MB, throughput 0.988443
Reading from 17405: heap size 604 MB, throughput 0.99304
Reading from 17404: heap size 619 MB, throughput 0.993777
Equal recommendation: 1104 MB each
Reading from 17405: heap size 610 MB, throughput 0.940496
Reading from 17404: heap size 620 MB, throughput 0.990836
Reading from 17405: heap size 623 MB, throughput 0.996372
Reading from 17404: heap size 619 MB, throughput 0.991315
Reading from 17405: heap size 624 MB, throughput 0.994479
Equal recommendation: 1104 MB each
Reading from 17404: heap size 621 MB, throughput 0.990212
Reading from 17404: heap size 624 MB, throughput 0.991604
Reading from 17404: heap size 625 MB, throughput 0.924938
Reading from 17405: heap size 626 MB, throughput 0.989128
Reading from 17405: heap size 629 MB, throughput 0.983283
Reading from 17405: heap size 630 MB, throughput 0.978824
Reading from 17404: heap size 629 MB, throughput 0.992605
Reading from 17405: heap size 637 MB, throughput 0.993726
Equal recommendation: 1104 MB each
Reading from 17404: heap size 630 MB, throughput 0.993575
Reading from 17405: heap size 638 MB, throughput 0.992907
Reading from 17404: heap size 632 MB, throughput 0.993944
Reading from 17405: heap size 638 MB, throughput 0.993573
Reading from 17404: heap size 634 MB, throughput 0.991909
Equal recommendation: 1104 MB each
Reading from 17405: heap size 639 MB, throughput 0.992025
Reading from 17404: heap size 636 MB, throughput 0.994663
Reading from 17404: heap size 636 MB, throughput 0.977945
Client 17404 died
Clients: 1
Reading from 17405: heap size 642 MB, throughput 0.993061
Reading from 17405: heap size 644 MB, throughput 0.929426
Client 17405 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
