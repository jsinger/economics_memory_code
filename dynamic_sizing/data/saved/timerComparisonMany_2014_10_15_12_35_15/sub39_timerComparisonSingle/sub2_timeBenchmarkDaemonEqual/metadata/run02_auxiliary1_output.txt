economemd
    total memory: 6130 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub39_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 3541: heap size 9 MB, throughput 0.987666
Clients: 1
Client 3541 has a minimum heap size of 3 MB
Reading from 3540: heap size 9 MB, throughput 0.990444
Clients: 2
Client 3540 has a minimum heap size of 1223 MB
Reading from 3541: heap size 9 MB, throughput 0.967256
Reading from 3540: heap size 9 MB, throughput 0.965003
Reading from 3541: heap size 11 MB, throughput 0.969655
Reading from 3540: heap size 9 MB, throughput 0.955783
Reading from 3540: heap size 9 MB, throughput 0.912272
Reading from 3541: heap size 11 MB, throughput 0.976683
Reading from 3540: heap size 11 MB, throughput 0.985022
Reading from 3540: heap size 11 MB, throughput 0.982436
Reading from 3540: heap size 17 MB, throughput 0.96291
Reading from 3541: heap size 15 MB, throughput 0.921409
Reading from 3540: heap size 17 MB, throughput 0.579361
Reading from 3540: heap size 30 MB, throughput 0.810222
Reading from 3541: heap size 18 MB, throughput 0.990882
Reading from 3541: heap size 21 MB, throughput 0.989444
Reading from 3540: heap size 31 MB, throughput 0.910107
Reading from 3541: heap size 24 MB, throughput 0.989922
Reading from 3541: heap size 26 MB, throughput 0.984437
Reading from 3540: heap size 35 MB, throughput 0.443101
Reading from 3541: heap size 26 MB, throughput 0.985623
Reading from 3540: heap size 45 MB, throughput 0.904681
Reading from 3541: heap size 29 MB, throughput 0.993212
Reading from 3541: heap size 29 MB, throughput 0.985046
Reading from 3541: heap size 30 MB, throughput 0.987149
Reading from 3540: heap size 48 MB, throughput 0.4054
Reading from 3541: heap size 31 MB, throughput 0.988098
Reading from 3540: heap size 62 MB, throughput 0.853047
Reading from 3541: heap size 34 MB, throughput 0.993744
Reading from 3540: heap size 67 MB, throughput 0.852842
Reading from 3541: heap size 34 MB, throughput 0.988229
Reading from 3541: heap size 37 MB, throughput 0.983352
Reading from 3541: heap size 37 MB, throughput 0.98698
Reading from 3541: heap size 39 MB, throughput 0.986558
Reading from 3541: heap size 39 MB, throughput 0.988689
Reading from 3540: heap size 68 MB, throughput 0.232489
Reading from 3541: heap size 41 MB, throughput 0.986937
Reading from 3540: heap size 99 MB, throughput 0.897687
Reading from 3541: heap size 41 MB, throughput 0.980954
Reading from 3541: heap size 44 MB, throughput 0.991665
Reading from 3541: heap size 44 MB, throughput 0.99186
Reading from 3540: heap size 100 MB, throughput 0.303351
Reading from 3541: heap size 47 MB, throughput 0.992895
Reading from 3541: heap size 47 MB, throughput 0.986896
Reading from 3540: heap size 131 MB, throughput 0.929768
Reading from 3541: heap size 49 MB, throughput 0.991153
Reading from 3540: heap size 131 MB, throughput 0.904977
Reading from 3541: heap size 49 MB, throughput 0.993611
Reading from 3541: heap size 52 MB, throughput 0.987672
Reading from 3540: heap size 133 MB, throughput 0.78557
Reading from 3540: heap size 136 MB, throughput 0.686654
Reading from 3541: heap size 52 MB, throughput 0.991988
Reading from 3541: heap size 54 MB, throughput 0.991879
Reading from 3541: heap size 54 MB, throughput 0.991067
Reading from 3541: heap size 57 MB, throughput 0.99367
Reading from 3541: heap size 57 MB, throughput 0.990837
Reading from 3540: heap size 137 MB, throughput 0.128218
Reading from 3541: heap size 59 MB, throughput 0.99422
Reading from 3540: heap size 176 MB, throughput 0.579646
Reading from 3541: heap size 59 MB, throughput 0.990742
Reading from 3541: heap size 61 MB, throughput 0.986592
Reading from 3541: heap size 61 MB, throughput 0.98249
Reading from 3540: heap size 179 MB, throughput 0.770587
Reading from 3541: heap size 63 MB, throughput 0.98671
Reading from 3540: heap size 182 MB, throughput 0.703128
Reading from 3541: heap size 63 MB, throughput 0.988218
Reading from 3540: heap size 190 MB, throughput 0.782969
Reading from 3541: heap size 65 MB, throughput 0.992543
Reading from 3540: heap size 192 MB, throughput 0.662093
Reading from 3541: heap size 66 MB, throughput 0.974572
Reading from 3541: heap size 68 MB, throughput 0.992185
Reading from 3541: heap size 68 MB, throughput 0.914017
Reading from 3541: heap size 72 MB, throughput 0.993456
Reading from 3540: heap size 195 MB, throughput 0.126083
Reading from 3541: heap size 72 MB, throughput 0.987875
Reading from 3540: heap size 243 MB, throughput 0.555873
Reading from 3541: heap size 76 MB, throughput 0.992191
Reading from 3540: heap size 247 MB, throughput 0.691375
Reading from 3541: heap size 77 MB, throughput 0.98765
Reading from 3541: heap size 81 MB, throughput 0.991424
Reading from 3540: heap size 252 MB, throughput 0.747024
Reading from 3541: heap size 81 MB, throughput 0.990065
Reading from 3540: heap size 255 MB, throughput 0.70785
Reading from 3541: heap size 85 MB, throughput 0.937306
Reading from 3540: heap size 263 MB, throughput 0.622253
Reading from 3540: heap size 266 MB, throughput 0.536797
Reading from 3541: heap size 86 MB, throughput 0.99472
Reading from 3540: heap size 277 MB, throughput 0.568372
Reading from 3541: heap size 91 MB, throughput 0.992125
Reading from 3540: heap size 284 MB, throughput 0.406996
Reading from 3541: heap size 91 MB, throughput 0.991788
Reading from 3541: heap size 96 MB, throughput 0.989334
Reading from 3541: heap size 96 MB, throughput 0.994607
Reading from 3541: heap size 100 MB, throughput 0.993053
Reading from 3540: heap size 291 MB, throughput 0.13872
Reading from 3541: heap size 101 MB, throughput 0.993705
Reading from 3540: heap size 346 MB, throughput 0.456763
Reading from 3541: heap size 104 MB, throughput 0.99399
Reading from 3540: heap size 286 MB, throughput 0.611262
Reading from 3541: heap size 104 MB, throughput 0.991037
Reading from 3541: heap size 108 MB, throughput 0.994614
Reading from 3541: heap size 108 MB, throughput 0.992389
Reading from 3541: heap size 111 MB, throughput 0.991006
Reading from 3540: heap size 335 MB, throughput 0.105175
Reading from 3541: heap size 111 MB, throughput 0.994782
Reading from 3540: heap size 392 MB, throughput 0.458649
Reading from 3541: heap size 115 MB, throughput 0.993195
Reading from 3540: heap size 386 MB, throughput 0.720309
Reading from 3541: heap size 115 MB, throughput 0.992186
Reading from 3540: heap size 389 MB, throughput 0.529594
Reading from 3541: heap size 119 MB, throughput 0.990747
Reading from 3540: heap size 382 MB, throughput 0.642833
Reading from 3541: heap size 119 MB, throughput 0.989246
Reading from 3540: heap size 386 MB, throughput 0.604861
Reading from 3541: heap size 122 MB, throughput 0.979615
Reading from 3540: heap size 387 MB, throughput 0.513962
Reading from 3540: heap size 390 MB, throughput 0.604912
Reading from 3541: heap size 122 MB, throughput 0.9935
Reading from 3541: heap size 127 MB, throughput 0.99197
Reading from 3540: heap size 395 MB, throughput 0.525937
Reading from 3541: heap size 127 MB, throughput 0.987851
Reading from 3540: heap size 401 MB, throughput 0.527191
Reading from 3540: heap size 404 MB, throughput 0.495563
Reading from 3541: heap size 132 MB, throughput 0.990284
Reading from 3541: heap size 132 MB, throughput 0.99076
Reading from 3540: heap size 412 MB, throughput 0.432213
Equal recommendation: 3065 MB each
Reading from 3540: heap size 417 MB, throughput 0.445031
Reading from 3541: heap size 137 MB, throughput 0.994405
Reading from 3541: heap size 137 MB, throughput 0.990728
Reading from 3541: heap size 142 MB, throughput 0.994668
Reading from 3541: heap size 142 MB, throughput 0.994667
Reading from 3540: heap size 426 MB, throughput 0.0749248
Reading from 3541: heap size 147 MB, throughput 0.996337
Reading from 3540: heap size 490 MB, throughput 0.46021
Reading from 3541: heap size 147 MB, throughput 0.994442
Reading from 3540: heap size 492 MB, throughput 0.491176
Reading from 3541: heap size 151 MB, throughput 0.991469
Reading from 3540: heap size 484 MB, throughput 0.563333
Reading from 3541: heap size 151 MB, throughput 0.990036
Reading from 3541: heap size 155 MB, throughput 0.996071
Reading from 3541: heap size 155 MB, throughput 0.993096
Reading from 3541: heap size 160 MB, throughput 0.995912
Reading from 3540: heap size 488 MB, throughput 0.0879029
Reading from 3541: heap size 160 MB, throughput 0.993896
Reading from 3540: heap size 543 MB, throughput 0.418784
Reading from 3540: heap size 546 MB, throughput 0.63509
Reading from 3541: heap size 164 MB, throughput 0.995364
Reading from 3540: heap size 541 MB, throughput 0.607661
Reading from 3541: heap size 164 MB, throughput 0.993604
Reading from 3540: heap size 545 MB, throughput 0.532153
Reading from 3541: heap size 168 MB, throughput 0.994535
Reading from 3540: heap size 546 MB, throughput 0.533811
Reading from 3541: heap size 168 MB, throughput 0.993258
Reading from 3540: heap size 549 MB, throughput 0.59892
Reading from 3541: heap size 172 MB, throughput 0.994601
Reading from 3540: heap size 555 MB, throughput 0.535572
Reading from 3541: heap size 172 MB, throughput 0.993079
Reading from 3541: heap size 176 MB, throughput 0.993012
Reading from 3541: heap size 176 MB, throughput 0.993184
Reading from 3541: heap size 181 MB, throughput 0.996097
Reading from 3540: heap size 561 MB, throughput 0.0939707
Reading from 3541: heap size 181 MB, throughput 0.993376
Reading from 3540: heap size 629 MB, throughput 0.437748
Reading from 3541: heap size 185 MB, throughput 0.9932
Reading from 3540: heap size 637 MB, throughput 0.546062
Reading from 3541: heap size 185 MB, throughput 0.992599
Reading from 3540: heap size 638 MB, throughput 0.509618
Reading from 3541: heap size 190 MB, throughput 0.988689
Reading from 3540: heap size 642 MB, throughput 0.495887
Reading from 3541: heap size 190 MB, throughput 0.993113
Reading from 3541: heap size 196 MB, throughput 0.994448
Reading from 3540: heap size 649 MB, throughput 0.879461
Reading from 3541: heap size 196 MB, throughput 0.994885
Reading from 3541: heap size 202 MB, throughput 0.993728
Reading from 3541: heap size 202 MB, throughput 0.993311
Reading from 3540: heap size 654 MB, throughput 0.776186
Reading from 3541: heap size 207 MB, throughput 0.994842
Reading from 3541: heap size 207 MB, throughput 0.99327
Reading from 3541: heap size 213 MB, throughput 0.963287
Reading from 3540: heap size 653 MB, throughput 0.859954
Reading from 3540: heap size 671 MB, throughput 0.520838
Reading from 3540: heap size 682 MB, throughput 0.375536
Reading from 3541: heap size 213 MB, throughput 0.997124
Reading from 3541: heap size 222 MB, throughput 0.996156
Reading from 3541: heap size 223 MB, throughput 0.994586
Reading from 3541: heap size 231 MB, throughput 0.994494
Equal recommendation: 3065 MB each
Reading from 3541: heap size 231 MB, throughput 0.994585
Reading from 3540: heap size 695 MB, throughput 0.0997859
Reading from 3541: heap size 238 MB, throughput 0.991671
Reading from 3540: heap size 769 MB, throughput 0.345387
Reading from 3540: heap size 681 MB, throughput 0.206108
Reading from 3540: heap size 753 MB, throughput 0.275788
Reading from 3541: heap size 238 MB, throughput 0.994792
Reading from 3541: heap size 245 MB, throughput 0.994818
Reading from 3541: heap size 245 MB, throughput 0.991985
Reading from 3540: heap size 762 MB, throughput 0.0285028
Reading from 3540: heap size 839 MB, throughput 0.190676
Reading from 3541: heap size 252 MB, throughput 0.995832
Reading from 3540: heap size 841 MB, throughput 0.510736
Reading from 3541: heap size 252 MB, throughput 0.994178
Reading from 3540: heap size 846 MB, throughput 0.842444
Reading from 3540: heap size 848 MB, throughput 0.835745
Reading from 3541: heap size 259 MB, throughput 0.993798
Reading from 3540: heap size 850 MB, throughput 0.539331
Reading from 3540: heap size 852 MB, throughput 0.553244
Reading from 3541: heap size 259 MB, throughput 0.991291
Reading from 3541: heap size 266 MB, throughput 0.995899
Reading from 3541: heap size 266 MB, throughput 0.994201
Reading from 3541: heap size 273 MB, throughput 0.994513
Reading from 3541: heap size 273 MB, throughput 0.995468
Reading from 3540: heap size 845 MB, throughput 0.0335273
Reading from 3540: heap size 940 MB, throughput 0.348564
Reading from 3541: heap size 279 MB, throughput 0.994993
Reading from 3540: heap size 942 MB, throughput 0.928499
Reading from 3540: heap size 945 MB, throughput 0.838728
Reading from 3541: heap size 279 MB, throughput 0.994407
Reading from 3540: heap size 940 MB, throughput 0.902846
Reading from 3540: heap size 752 MB, throughput 0.777892
Reading from 3541: heap size 286 MB, throughput 0.995091
Reading from 3540: heap size 931 MB, throughput 0.925433
Reading from 3540: heap size 761 MB, throughput 0.825608
Reading from 3541: heap size 286 MB, throughput 0.994011
Reading from 3540: heap size 917 MB, throughput 0.888169
Reading from 3541: heap size 292 MB, throughput 0.995585
Reading from 3540: heap size 791 MB, throughput 0.787295
Reading from 3541: heap size 292 MB, throughput 0.992079
Reading from 3540: heap size 903 MB, throughput 0.702694
Reading from 3540: heap size 812 MB, throughput 0.664631
Reading from 3540: heap size 896 MB, throughput 0.683908
Reading from 3541: heap size 299 MB, throughput 0.994685
Reading from 3540: heap size 812 MB, throughput 0.688005
Reading from 3540: heap size 887 MB, throughput 0.714633
Reading from 3541: heap size 299 MB, throughput 0.994971
Reading from 3540: heap size 894 MB, throughput 0.730702
Reading from 3540: heap size 882 MB, throughput 0.730149
Reading from 3541: heap size 305 MB, throughput 0.995419
Reading from 3540: heap size 888 MB, throughput 0.723104
Reading from 3540: heap size 877 MB, throughput 0.75082
Reading from 3541: heap size 305 MB, throughput 0.994218
Reading from 3540: heap size 883 MB, throughput 0.718087
Reading from 3540: heap size 878 MB, throughput 0.79281
Reading from 3541: heap size 312 MB, throughput 0.995562
Reading from 3541: heap size 312 MB, throughput 0.995212
Reading from 3541: heap size 319 MB, throughput 0.995512
Reading from 3541: heap size 319 MB, throughput 0.994428
Reading from 3540: heap size 882 MB, throughput 0.972245
Reading from 3541: heap size 326 MB, throughput 0.995304
Equal recommendation: 3065 MB each
Reading from 3541: heap size 326 MB, throughput 0.99488
Reading from 3540: heap size 882 MB, throughput 0.926487
Reading from 3541: heap size 333 MB, throughput 0.995476
Reading from 3540: heap size 884 MB, throughput 0.701734
Reading from 3540: heap size 890 MB, throughput 0.725214
Reading from 3541: heap size 333 MB, throughput 0.993689
Reading from 3540: heap size 894 MB, throughput 0.713606
Reading from 3540: heap size 902 MB, throughput 0.693828
Reading from 3540: heap size 904 MB, throughput 0.80942
Reading from 3540: heap size 913 MB, throughput 0.824331
Reading from 3540: heap size 914 MB, throughput 0.822744
Reading from 3540: heap size 921 MB, throughput 0.833162
Reading from 3541: heap size 341 MB, throughput 0.998288
Reading from 3540: heap size 922 MB, throughput 0.844866
Reading from 3540: heap size 926 MB, throughput 0.812969
Reading from 3541: heap size 341 MB, throughput 0.994673
Reading from 3540: heap size 928 MB, throughput 0.88747
Reading from 3541: heap size 347 MB, throughput 0.982362
Reading from 3540: heap size 931 MB, throughput 0.654243
Reading from 3541: heap size 348 MB, throughput 0.993673
Reading from 3541: heap size 359 MB, throughput 0.995921
Reading from 3541: heap size 359 MB, throughput 0.995292
Reading from 3541: heap size 369 MB, throughput 0.995823
Reading from 3540: heap size 933 MB, throughput 0.0601876
Reading from 3540: heap size 1062 MB, throughput 0.435008
Reading from 3541: heap size 369 MB, throughput 0.994105
Reading from 3540: heap size 1066 MB, throughput 0.748649
Reading from 3540: heap size 1069 MB, throughput 0.739039
Reading from 3541: heap size 378 MB, throughput 0.991658
Reading from 3540: heap size 1071 MB, throughput 0.744371
Reading from 3540: heap size 1074 MB, throughput 0.718055
Reading from 3541: heap size 378 MB, throughput 0.990739
Reading from 3540: heap size 1078 MB, throughput 0.66312
Reading from 3540: heap size 1089 MB, throughput 0.731336
Reading from 3541: heap size 389 MB, throughput 0.9861
Reading from 3540: heap size 1090 MB, throughput 0.695521
Reading from 3541: heap size 389 MB, throughput 0.993775
Reading from 3541: heap size 401 MB, throughput 0.995853
Reading from 3540: heap size 1107 MB, throughput 0.942106
Reading from 3541: heap size 402 MB, throughput 0.995457
Reading from 3541: heap size 413 MB, throughput 0.995654
Reading from 3541: heap size 413 MB, throughput 0.994296
Equal recommendation: 3065 MB each
Reading from 3541: heap size 423 MB, throughput 0.995078
Reading from 3541: heap size 424 MB, throughput 0.995228
Reading from 3540: heap size 1107 MB, throughput 0.970616
Reading from 3541: heap size 434 MB, throughput 0.995867
Reading from 3541: heap size 434 MB, throughput 0.994531
Reading from 3541: heap size 444 MB, throughput 0.995434
Reading from 3541: heap size 444 MB, throughput 0.994684
Reading from 3541: heap size 454 MB, throughput 0.995474
Reading from 3541: heap size 454 MB, throughput 0.994539
Reading from 3540: heap size 1126 MB, throughput 0.958567
Reading from 3541: heap size 464 MB, throughput 0.995595
Reading from 3541: heap size 464 MB, throughput 0.994573
Reading from 3541: heap size 474 MB, throughput 0.994997
Reading from 3541: heap size 474 MB, throughput 0.994249
Reading from 3541: heap size 485 MB, throughput 0.995072
Reading from 3540: heap size 1128 MB, throughput 0.969474
Reading from 3541: heap size 485 MB, throughput 0.994918
Reading from 3541: heap size 496 MB, throughput 0.994716
Reading from 3541: heap size 472 MB, throughput 0.99612
Reading from 3541: heap size 447 MB, throughput 0.995624
Reading from 3541: heap size 429 MB, throughput 0.995351
Reading from 3541: heap size 407 MB, throughput 0.995016
Equal recommendation: 3065 MB each
Reading from 3540: heap size 1132 MB, throughput 0.963869
Reading from 3541: heap size 388 MB, throughput 0.995514
Reading from 3541: heap size 368 MB, throughput 0.994701
Reading from 3541: heap size 352 MB, throughput 0.995037
Reading from 3541: heap size 335 MB, throughput 0.994413
Reading from 3541: heap size 319 MB, throughput 0.994051
Reading from 3541: heap size 304 MB, throughput 0.994464
Reading from 3541: heap size 290 MB, throughput 0.994074
Reading from 3541: heap size 277 MB, throughput 0.994483
Reading from 3540: heap size 1136 MB, throughput 0.965422
Reading from 3541: heap size 264 MB, throughput 0.995529
Reading from 3541: heap size 252 MB, throughput 0.991351
Reading from 3541: heap size 240 MB, throughput 0.994371
Reading from 3541: heap size 229 MB, throughput 0.994162
Reading from 3541: heap size 218 MB, throughput 0.99322
Reading from 3541: heap size 208 MB, throughput 0.993731
Reading from 3541: heap size 199 MB, throughput 0.993149
Reading from 3541: heap size 204 MB, throughput 0.992928
Reading from 3541: heap size 209 MB, throughput 0.993663
Reading from 3541: heap size 215 MB, throughput 0.9938
Reading from 3541: heap size 220 MB, throughput 0.993763
Reading from 3541: heap size 210 MB, throughput 0.993083
Reading from 3541: heap size 216 MB, throughput 0.992002
Reading from 3540: heap size 1133 MB, throughput 0.964517
Reading from 3541: heap size 222 MB, throughput 0.994143
Reading from 3541: heap size 228 MB, throughput 0.993393
Reading from 3541: heap size 235 MB, throughput 0.992758
Reading from 3541: heap size 241 MB, throughput 0.925689
Reading from 3541: heap size 250 MB, throughput 0.993796
Reading from 3541: heap size 262 MB, throughput 0.99446
Reading from 3541: heap size 274 MB, throughput 0.994689
Reading from 3541: heap size 286 MB, throughput 0.994693
Reading from 3541: heap size 299 MB, throughput 0.994856
Reading from 3541: heap size 311 MB, throughput 0.995085
Reading from 3541: heap size 324 MB, throughput 0.994966
Reading from 3540: heap size 1138 MB, throughput 0.963355
Reading from 3541: heap size 337 MB, throughput 0.995548
Equal recommendation: 3065 MB each
Reading from 3541: heap size 349 MB, throughput 0.994671
Reading from 3541: heap size 364 MB, throughput 0.995272
Reading from 3541: heap size 378 MB, throughput 0.995225
Reading from 3541: heap size 392 MB, throughput 0.99536
Reading from 3541: heap size 407 MB, throughput 0.995109
Reading from 3541: heap size 423 MB, throughput 0.995248
Reading from 3540: heap size 1144 MB, throughput 0.96286
Reading from 3541: heap size 438 MB, throughput 0.995595
Reading from 3541: heap size 455 MB, throughput 0.995515
Reading from 3541: heap size 458 MB, throughput 0.99549
Reading from 3541: heap size 459 MB, throughput 0.994213
Reading from 3541: heap size 475 MB, throughput 0.995083
Reading from 3541: heap size 476 MB, throughput 0.994084
Reading from 3540: heap size 1145 MB, throughput 0.957288
Reading from 3541: heap size 494 MB, throughput 0.995473
Reading from 3541: heap size 494 MB, throughput 0.994842
Reading from 3541: heap size 514 MB, throughput 0.998531
Reading from 3541: heap size 514 MB, throughput 0.994621
Reading from 3540: heap size 1150 MB, throughput 0.967123
Equal recommendation: 3065 MB each
Reading from 3541: heap size 532 MB, throughput 0.994824
Reading from 3541: heap size 532 MB, throughput 0.994638
Reading from 3541: heap size 552 MB, throughput 0.99507
Reading from 3541: heap size 552 MB, throughput 0.994592
Reading from 3541: heap size 573 MB, throughput 0.994829
Reading from 3540: heap size 1154 MB, throughput 0.960482
Reading from 3541: heap size 573 MB, throughput 0.995119
Reading from 3541: heap size 595 MB, throughput 0.994764
Reading from 3541: heap size 596 MB, throughput 0.994838
Reading from 3541: heap size 620 MB, throughput 0.994907
Reading from 3541: heap size 620 MB, throughput 0.994739
Reading from 3540: heap size 1160 MB, throughput 0.949373
Reading from 3541: heap size 645 MB, throughput 0.995506
Reading from 3541: heap size 645 MB, throughput 0.995266
Reading from 3541: heap size 670 MB, throughput 0.995413
Reading from 3541: heap size 670 MB, throughput 0.995428
Reading from 3540: heap size 1164 MB, throughput 0.96113
Equal recommendation: 3065 MB each
Reading from 3541: heap size 696 MB, throughput 0.995668
Reading from 3541: heap size 696 MB, throughput 0.993733
Reading from 3541: heap size 722 MB, throughput 0.995629
Reading from 3541: heap size 722 MB, throughput 0.995234
Reading from 3540: heap size 1169 MB, throughput 0.955246
Reading from 3541: heap size 750 MB, throughput 0.995484
Reading from 3541: heap size 751 MB, throughput 0.995428
Reading from 3541: heap size 779 MB, throughput 0.99528
Reading from 3541: heap size 779 MB, throughput 0.995306
Reading from 3540: heap size 1175 MB, throughput 0.961969
Reading from 3541: heap size 809 MB, throughput 0.99534
Reading from 3541: heap size 809 MB, throughput 0.99535
Reading from 3541: heap size 839 MB, throughput 0.995282
Reading from 3540: heap size 1180 MB, throughput 0.961266
Equal recommendation: 3065 MB each
Reading from 3541: heap size 840 MB, throughput 0.995528
Reading from 3541: heap size 871 MB, throughput 0.995291
Reading from 3541: heap size 871 MB, throughput 0.995427
Reading from 3540: heap size 1186 MB, throughput 0.953429
Reading from 3541: heap size 904 MB, throughput 0.994765
Reading from 3541: heap size 904 MB, throughput 0.995603
Reading from 3541: heap size 938 MB, throughput 0.995548
Reading from 3541: heap size 939 MB, throughput 0.9952
Reading from 3540: heap size 1192 MB, throughput 0.961471
Reading from 3541: heap size 975 MB, throughput 0.995665
Reading from 3541: heap size 975 MB, throughput 0.995615
Reading from 3541: heap size 1010 MB, throughput 0.995492
Reading from 3540: heap size 1196 MB, throughput 0.958115
Equal recommendation: 3065 MB each
Reading from 3541: heap size 1011 MB, throughput 0.995633
Reading from 3541: heap size 1047 MB, throughput 0.995573
Reading from 3541: heap size 1048 MB, throughput 0.994567
Reading from 3540: heap size 1202 MB, throughput 0.960562
Reading from 3541: heap size 1086 MB, throughput 0.995655
Reading from 3541: heap size 1086 MB, throughput 0.995531
Reading from 3540: heap size 1204 MB, throughput 0.963257
Reading from 3541: heap size 1126 MB, throughput 0.995584
Reading from 3541: heap size 1127 MB, throughput 0.995466
Reading from 3541: heap size 1169 MB, throughput 0.995872
Equal recommendation: 3065 MB each
Reading from 3540: heap size 1209 MB, throughput 0.961599
Reading from 3541: heap size 1169 MB, throughput 0.995829
Reading from 3541: heap size 1211 MB, throughput 0.995943
Reading from 3540: heap size 1210 MB, throughput 0.959702
Reading from 3541: heap size 1211 MB, throughput 0.995759
Reading from 3541: heap size 1254 MB, throughput 0.994854
Reading from 3541: heap size 1254 MB, throughput 0.995621
Reading from 3541: heap size 1301 MB, throughput 0.996884
Reading from 3540: heap size 1216 MB, throughput 0.592876
Equal recommendation: 3065 MB each
Reading from 3541: heap size 1301 MB, throughput 0.999999
Reading from 3541: heap size 1301 MB, throughput 0.95
Reading from 3541: heap size 1301 MB, throughput 0.0092563
Reading from 3541: heap size 1343 MB, throughput 0.995918
Reading from 3540: heap size 1313 MB, throughput 0.95131
Reading from 3541: heap size 1345 MB, throughput 0.995735
Reading from 3541: heap size 1390 MB, throughput 0.995895
Reading from 3540: heap size 1311 MB, throughput 0.979438
Reading from 3541: heap size 1390 MB, throughput 0.995743
Reading from 3541: heap size 1437 MB, throughput 0.99522
Equal recommendation: 3065 MB each
Reading from 3541: heap size 1438 MB, throughput 0.995871
Reading from 3540: heap size 1319 MB, throughput 0.977269
Reading from 3541: heap size 1488 MB, throughput 0.995756
Reading from 3541: heap size 1489 MB, throughput 0.995911
Reading from 3540: heap size 1328 MB, throughput 0.976477
Reading from 3541: heap size 1541 MB, throughput 0.996999
Reading from 3540: heap size 1329 MB, throughput 0.977729
Reading from 3541: heap size 1541 MB, throughput 0.995985
Reading from 3541: heap size 1593 MB, throughput 0.995961
Equal recommendation: 3065 MB each
Reading from 3540: heap size 1324 MB, throughput 0.971998
Reading from 3541: heap size 1593 MB, throughput 0.995802
Reading from 3541: heap size 1645 MB, throughput 0.99585
Reading from 3540: heap size 1227 MB, throughput 0.978679
Reading from 3541: heap size 1646 MB, throughput 0.99595
Reading from 3541: heap size 1702 MB, throughput 0.995871
Reading from 3540: heap size 1314 MB, throughput 0.975904
Reading from 3541: heap size 1702 MB, throughput 0.995838
Equal recommendation: 3065 MB each
Reading from 3541: heap size 1760 MB, throughput 0.995881
Reading from 3540: heap size 1247 MB, throughput 0.973286
Reading from 3541: heap size 1760 MB, throughput 0.995871
Reading from 3541: heap size 1821 MB, throughput 0.996072
Reading from 3540: heap size 1310 MB, throughput 0.968394
Reading from 3541: heap size 1821 MB, throughput 0.995884
Reading from 3541: heap size 1883 MB, throughput 0.994884
Reading from 3540: heap size 1314 MB, throughput 0.967107
Equal recommendation: 3065 MB each
Reading from 3541: heap size 1883 MB, throughput 0.995856
Reading from 3540: heap size 1317 MB, throughput 0.964064
Reading from 3541: heap size 1951 MB, throughput 0.996192
Reading from 3541: heap size 1951 MB, throughput 0.995928
Reading from 3540: heap size 1318 MB, throughput 0.964659
Reading from 3541: heap size 2019 MB, throughput 0.996134
Equal recommendation: 3065 MB each
Reading from 3541: heap size 2019 MB, throughput 0.995991
Reading from 3540: heap size 1323 MB, throughput 0.96531
Reading from 3541: heap size 2045 MB, throughput 0.995977
Reading from 3540: heap size 1328 MB, throughput 0.968837
Reading from 3541: heap size 2045 MB, throughput 0.996335
Reading from 3541: heap size 2045 MB, throughput 0.996197
Reading from 3540: heap size 1333 MB, throughput 0.965072
Equal recommendation: 3065 MB each
Reading from 3541: heap size 2045 MB, throughput 0.996481
Reading from 3541: heap size 2045 MB, throughput 0.996117
Reading from 3540: heap size 1341 MB, throughput 0.96389
Reading from 3541: heap size 2045 MB, throughput 0.996311
Reading from 3540: heap size 1348 MB, throughput 0.954971
Reading from 3541: heap size 2045 MB, throughput 0.996281
Equal recommendation: 3065 MB each
Reading from 3541: heap size 2045 MB, throughput 0.996144
Reading from 3541: heap size 2045 MB, throughput 0.996205
Reading from 3541: heap size 2045 MB, throughput 0.996176
Reading from 3541: heap size 2045 MB, throughput 0.996238
Reading from 3541: heap size 2045 MB, throughput 0.996161
Equal recommendation: 3065 MB each
Reading from 3541: heap size 2045 MB, throughput 0.996193
Reading from 3541: heap size 2045 MB, throughput 0.996183
Reading from 3541: heap size 2044 MB, throughput 0.995518
Reading from 3541: heap size 2045 MB, throughput 0.99624
Equal recommendation: 3065 MB each
Reading from 3541: heap size 2045 MB, throughput 0.996216
Reading from 3541: heap size 2045 MB, throughput 0.996113
Reading from 3541: heap size 2046 MB, throughput 0.996222
Reading from 3540: heap size 1353 MB, throughput 0.993569
Reading from 3541: heap size 2046 MB, throughput 0.996568
Reading from 3541: heap size 2046 MB, throughput 0.996206
Equal recommendation: 3065 MB each
Reading from 3541: heap size 2046 MB, throughput 0.996204
Reading from 3540: heap size 1349 MB, throughput 0.781072
Reading from 3541: heap size 2045 MB, throughput 0.996928
Reading from 3540: heap size 1486 MB, throughput 0.993829
Reading from 3541: heap size 2045 MB, throughput 0.996331
Reading from 3540: heap size 1540 MB, throughput 0.900911
Reading from 3540: heap size 1547 MB, throughput 0.849433
Reading from 3540: heap size 1536 MB, throughput 0.853042
Reading from 3540: heap size 1542 MB, throughput 0.837121
Reading from 3540: heap size 1537 MB, throughput 0.891714
Reading from 3540: heap size 1544 MB, throughput 0.982244
Reading from 3541: heap size 2045 MB, throughput 0.996135
Equal recommendation: 3065 MB each
Reading from 3540: heap size 1551 MB, throughput 0.989289
Reading from 3541: heap size 2046 MB, throughput 0.996284
Reading from 3541: heap size 2046 MB, throughput 0.995983
Reading from 3540: heap size 1557 MB, throughput 0.99191
Reading from 3541: heap size 2046 MB, throughput 0.996116
Reading from 3541: heap size 2046 MB, throughput 0.996241
Equal recommendation: 3065 MB each
Reading from 3540: heap size 1566 MB, throughput 0.986648
Reading from 3541: heap size 2046 MB, throughput 0.99627
Reading from 3541: heap size 2046 MB, throughput 0.996225
Reading from 3540: heap size 1571 MB, throughput 0.98324
Reading from 3541: heap size 2046 MB, throughput 0.996271
Reading from 3541: heap size 2046 MB, throughput 0.996266
Reading from 3540: heap size 1559 MB, throughput 0.978619
Equal recommendation: 3065 MB each
Reading from 3541: heap size 2046 MB, throughput 0.99624
Reading from 3541: heap size 2046 MB, throughput 0.996254
Reading from 3540: heap size 1568 MB, throughput 0.977931
Reading from 3541: heap size 2046 MB, throughput 0.996288
Reading from 3540: heap size 1544 MB, throughput 0.984248
Reading from 3541: heap size 2046 MB, throughput 0.996981
Equal recommendation: 3065 MB each
Reading from 3541: heap size 2046 MB, throughput 0.99607
Reading from 3540: heap size 1450 MB, throughput 0.97688
Reading from 3541: heap size 2046 MB, throughput 1
Reading from 3541: heap size 2046 MB, throughput 0.952381
Reading from 3541: heap size 2046 MB, throughput 0.00625
Reading from 3541: heap size 2046 MB, throughput 0.996137
Reading from 3540: heap size 1543 MB, throughput 0.976997
Reading from 3541: heap size 2046 MB, throughput 0.996268
Equal recommendation: 3065 MB each
Reading from 3541: heap size 2046 MB, throughput 0.996127
Reading from 3540: heap size 1549 MB, throughput 0.974466
Reading from 3541: heap size 2046 MB, throughput 0.996341
Reading from 3541: heap size 2046 MB, throughput 0.996294
Reading from 3540: heap size 1553 MB, throughput 0.971309
Reading from 3541: heap size 2046 MB, throughput 0.99641
Reading from 3541: heap size 2046 MB, throughput 0.996129
Equal recommendation: 3065 MB each
Reading from 3540: heap size 1554 MB, throughput 0.972842
Reading from 3541: heap size 2046 MB, throughput 0.9963
Reading from 3541: heap size 2046 MB, throughput 0.996217
Reading from 3541: heap size 2046 MB, throughput 0.996312
Reading from 3540: heap size 1560 MB, throughput 0.966044
Reading from 3541: heap size 2046 MB, throughput 0.996368
Equal recommendation: 3065 MB each
Reading from 3541: heap size 2046 MB, throughput 0.996226
Reading from 3540: heap size 1567 MB, throughput 0.972549
Reading from 3541: heap size 2046 MB, throughput 0.996355
Reading from 3541: heap size 2046 MB, throughput 0.996129
Reading from 3540: heap size 1574 MB, throughput 0.969385
Reading from 3541: heap size 2046 MB, throughput 0.996356
Equal recommendation: 3065 MB each
Reading from 3541: heap size 2046 MB, throughput 0.996142
Reading from 3540: heap size 1584 MB, throughput 0.965509
Reading from 3541: heap size 2046 MB, throughput 0.996458
Reading from 3541: heap size 2046 MB, throughput 0.996135
Reading from 3540: heap size 1594 MB, throughput 0.962551
Reading from 3541: heap size 2046 MB, throughput 0.996394
Equal recommendation: 3065 MB each
Reading from 3541: heap size 2046 MB, throughput 0.996229
Reading from 3540: heap size 1605 MB, throughput 0.963177
Reading from 3541: heap size 2046 MB, throughput 0.996405
Client 3541 died
Clients: 1
Reading from 3540: heap size 1617 MB, throughput 0.979285
Recommendation: one client; give it all the memory
Reading from 3540: heap size 1623 MB, throughput 0.981203
Reading from 3540: heap size 1633 MB, throughput 0.978795
Recommendation: one client; give it all the memory
Reading from 3540: heap size 1636 MB, throughput 0.979214
Reading from 3540: heap size 1645 MB, throughput 0.980632
Recommendation: one client; give it all the memory
Reading from 3540: heap size 1646 MB, throughput 0.980019
Recommendation: one client; give it all the memory
Reading from 3540: heap size 1653 MB, throughput 0.980419
Reading from 3540: heap size 1654 MB, throughput 0.979946
Recommendation: one client; give it all the memory
Reading from 3540: heap size 1657 MB, throughput 0.980864
Reading from 3540: heap size 1659 MB, throughput 0.831409
Recommendation: one client; give it all the memory
Reading from 3540: heap size 1705 MB, throughput 0.996229
Recommendation: one client; give it all the memory
Reading from 3540: heap size 1707 MB, throughput 0.994704
Recommendation: one client; give it all the memory
Reading from 3540: heap size 1721 MB, throughput 0.990525
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 3540: heap size 1727 MB, throughput 0.995688
Reading from 3540: heap size 1721 MB, throughput 0.910542
Reading from 3540: heap size 1728 MB, throughput 0.789064
Reading from 3540: heap size 1737 MB, throughput 0.816489
Reading from 3540: heap size 1755 MB, throughput 0.7976
Reading from 3540: heap size 1790 MB, throughput 0.867949
Client 3540 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
