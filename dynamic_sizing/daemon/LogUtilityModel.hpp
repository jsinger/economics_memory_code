#ifndef LOGUTILITYMODEL_HPP
#define LOGUTILITYMODEL_HPP

#include "UtilityModel.hpp"

// A utility model based on U(h) = a ln(bh)
// Combined utility for clients is found by multiplying the individual utilities

class LogUtilityModel : public UtilityModel
{
public:
    static const char* NAME;

    virtual UtilityFunction* calculateFuncFromData(ReadingIterator& it) const;
    virtual PartitionResult* partitionMemory(unsigned long long totalMem, const std::vector<const UtilityFunction*>& functions, const std::vector<unsigned long long>& minHeaps) const;

protected:
    virtual double combineRawUtilities(const std::vector<double>& utilities) const;

private:
    class LogUtilityFunction;
};

#endif
