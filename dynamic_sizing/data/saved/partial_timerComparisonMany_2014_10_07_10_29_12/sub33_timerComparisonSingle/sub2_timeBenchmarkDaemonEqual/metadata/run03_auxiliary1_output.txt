economemd
    total memory: 1420 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub33_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
TimerThread: starting
CommandThread: starting
ReadingThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 2513: heap size 9 MB, throughput 0.990325
Clients: 1
Client 2513 has a minimum heap size of 8 MB
Reading from 2512: heap size 9 MB, throughput 0.991907
Clients: 2
Client 2512 has a minimum heap size of 276 MB
Reading from 2512: heap size 9 MB, throughput 0.981036
Reading from 2513: heap size 9 MB, throughput 0.971697
Reading from 2512: heap size 9 MB, throughput 0.958021
Reading from 2513: heap size 9 MB, throughput 0.9618
Reading from 2512: heap size 9 MB, throughput 0.949456
Reading from 2513: heap size 9 MB, throughput 0.632769
Reading from 2513: heap size 11 MB, throughput 0.954779
Reading from 2513: heap size 11 MB, throughput 0.975195
Reading from 2513: heap size 16 MB, throughput 0.984929
Reading from 2513: heap size 16 MB, throughput 0.967698
Reading from 2513: heap size 23 MB, throughput 0.954981
Reading from 2513: heap size 24 MB, throughput 0.588172
Reading from 2513: heap size 35 MB, throughput 0.981865
Reading from 2513: heap size 35 MB, throughput 0.966982
Reading from 2513: heap size 43 MB, throughput 0.98369
Reading from 2513: heap size 44 MB, throughput 0.980371
Reading from 2512: heap size 11 MB, throughput 0.996954
Reading from 2513: heap size 52 MB, throughput 0.985305
Reading from 2513: heap size 53 MB, throughput 0.980266
Reading from 2512: heap size 11 MB, throughput 0.986165
Reading from 2513: heap size 65 MB, throughput 0.986016
Reading from 2512: heap size 17 MB, throughput 0.952951
Reading from 2513: heap size 65 MB, throughput 0.985752
Reading from 2512: heap size 17 MB, throughput 0.528278
Reading from 2513: heap size 74 MB, throughput 0.989965
Reading from 2512: heap size 29 MB, throughput 0.965884
Reading from 2513: heap size 74 MB, throughput 0.989572
Reading from 2512: heap size 30 MB, throughput 0.89638
Reading from 2513: heap size 82 MB, throughput 0.994625
Reading from 2513: heap size 83 MB, throughput 0.986191
Reading from 2512: heap size 35 MB, throughput 0.406775
Reading from 2512: heap size 48 MB, throughput 0.832273
Reading from 2513: heap size 91 MB, throughput 0.992861
Reading from 2513: heap size 92 MB, throughput 0.994966
Reading from 2512: heap size 50 MB, throughput 0.361837
Reading from 2512: heap size 66 MB, throughput 0.891108
Reading from 2513: heap size 100 MB, throughput 0.993257
Reading from 2512: heap size 72 MB, throughput 0.72933
Reading from 2513: heap size 101 MB, throughput 0.990746
Reading from 2512: heap size 73 MB, throughput 0.26539
Reading from 2513: heap size 107 MB, throughput 0.99333
Reading from 2512: heap size 103 MB, throughput 0.751472
Reading from 2513: heap size 107 MB, throughput 0.988963
Reading from 2512: heap size 103 MB, throughput 0.351545
Reading from 2513: heap size 113 MB, throughput 0.993114
Reading from 2512: heap size 135 MB, throughput 0.6918
Reading from 2512: heap size 135 MB, throughput 0.806793
Reading from 2513: heap size 113 MB, throughput 0.996134
Reading from 2512: heap size 138 MB, throughput 0.782639
Reading from 2513: heap size 119 MB, throughput 0.995425
Reading from 2513: heap size 120 MB, throughput 0.994402
Reading from 2512: heap size 142 MB, throughput 0.178833
Reading from 2513: heap size 125 MB, throughput 0.993888
Reading from 2512: heap size 174 MB, throughput 0.637674
Reading from 2512: heap size 179 MB, throughput 0.804753
Reading from 2513: heap size 126 MB, throughput 0.993404
Reading from 2512: heap size 181 MB, throughput 0.763591
Reading from 2513: heap size 131 MB, throughput 0.996073
Reading from 2513: heap size 131 MB, throughput 0.992835
Reading from 2512: heap size 184 MB, throughput 0.194533
Reading from 2513: heap size 136 MB, throughput 0.99634
Reading from 2513: heap size 136 MB, throughput 0.99385
Reading from 2513: heap size 141 MB, throughput 0.99239
Reading from 2512: heap size 228 MB, throughput 0.896374
Reading from 2513: heap size 141 MB, throughput 0.991791
Reading from 2513: heap size 147 MB, throughput 0.993791
Reading from 2512: heap size 231 MB, throughput 0.920466
Reading from 2512: heap size 241 MB, throughput 0.716909
Reading from 2513: heap size 147 MB, throughput 0.995055
Reading from 2512: heap size 243 MB, throughput 0.835616
Reading from 2512: heap size 249 MB, throughput 0.833487
Reading from 2513: heap size 153 MB, throughput 0.997151
Reading from 2512: heap size 250 MB, throughput 0.816952
Reading from 2512: heap size 256 MB, throughput 0.840781
Reading from 2513: heap size 153 MB, throughput 0.995836
Reading from 2512: heap size 257 MB, throughput 0.69701
Reading from 2512: heap size 264 MB, throughput 0.73382
Reading from 2513: heap size 158 MB, throughput 0.996415
Reading from 2512: heap size 268 MB, throughput 0.693047
Reading from 2513: heap size 158 MB, throughput 0.996782
Reading from 2513: heap size 162 MB, throughput 0.997749
Reading from 2512: heap size 275 MB, throughput 0.924512
Reading from 2512: heap size 277 MB, throughput 0.717847
Reading from 2513: heap size 162 MB, throughput 0.996215
Reading from 2512: heap size 283 MB, throughput 0.697768
Reading from 2512: heap size 287 MB, throughput 0.68425
Reading from 2513: heap size 166 MB, throughput 0.996635
Reading from 2512: heap size 295 MB, throughput 0.758905
Reading from 2513: heap size 166 MB, throughput 0.996883
Reading from 2513: heap size 170 MB, throughput 0.995712
Reading from 2513: heap size 170 MB, throughput 0.997538
Reading from 2512: heap size 297 MB, throughput 0.235053
Reading from 2512: heap size 343 MB, throughput 0.644289
Reading from 2513: heap size 174 MB, throughput 0.996004
Reading from 2512: heap size 345 MB, throughput 0.729025
Reading from 2512: heap size 349 MB, throughput 0.681914
Reading from 2513: heap size 174 MB, throughput 0.99595
Reading from 2512: heap size 350 MB, throughput 0.624623
Reading from 2512: heap size 356 MB, throughput 0.697059
Reading from 2513: heap size 178 MB, throughput 0.997495
Reading from 2513: heap size 178 MB, throughput 0.996558
Reading from 2512: heap size 356 MB, throughput 0.947669
Reading from 2513: heap size 182 MB, throughput 0.996045
Equal recommendation: 710 MB each
Reading from 2513: heap size 182 MB, throughput 0.995553
Reading from 2513: heap size 186 MB, throughput 0.998089
Reading from 2513: heap size 186 MB, throughput 0.995918
Reading from 2513: heap size 190 MB, throughput 0.993618
Reading from 2512: heap size 364 MB, throughput 0.979196
Reading from 2513: heap size 190 MB, throughput 0.995543
Reading from 2513: heap size 194 MB, throughput 0.996572
Reading from 2513: heap size 195 MB, throughput 0.994524
Reading from 2513: heap size 200 MB, throughput 0.993178
Reading from 2513: heap size 200 MB, throughput 0.996951
Reading from 2512: heap size 365 MB, throughput 0.970333
Reading from 2513: heap size 205 MB, throughput 0.996472
Reading from 2513: heap size 205 MB, throughput 0.995873
Reading from 2513: heap size 210 MB, throughput 0.996191
Reading from 2513: heap size 210 MB, throughput 0.996273
Reading from 2512: heap size 367 MB, throughput 0.972609
Reading from 2513: heap size 215 MB, throughput 0.996748
Reading from 2513: heap size 215 MB, throughput 0.994447
Reading from 2513: heap size 220 MB, throughput 0.994453
Reading from 2513: heap size 220 MB, throughput 0.995829
Reading from 2513: heap size 226 MB, throughput 0.993181
Reading from 2512: heap size 370 MB, throughput 0.974819
Reading from 2513: heap size 226 MB, throughput 0.995998
Reading from 2513: heap size 233 MB, throughput 0.997278
Reading from 2513: heap size 233 MB, throughput 0.996999
Reading from 2513: heap size 238 MB, throughput 0.997472
Reading from 2512: heap size 371 MB, throughput 0.970945
Reading from 2513: heap size 238 MB, throughput 0.997292
Reading from 2513: heap size 243 MB, throughput 0.997799
Reading from 2513: heap size 243 MB, throughput 0.997548
Reading from 2513: heap size 248 MB, throughput 0.997099
Reading from 2512: heap size 373 MB, throughput 0.965769
Reading from 2513: heap size 248 MB, throughput 0.996574
Reading from 2513: heap size 252 MB, throughput 0.997638
Reading from 2513: heap size 252 MB, throughput 0.996316
Reading from 2513: heap size 257 MB, throughput 0.998237
Equal recommendation: 710 MB each
Reading from 2512: heap size 372 MB, throughput 0.963905
Reading from 2513: heap size 257 MB, throughput 0.997515
Reading from 2513: heap size 261 MB, throughput 0.99757
Reading from 2513: heap size 261 MB, throughput 0.967979
Reading from 2513: heap size 268 MB, throughput 0.996626
Reading from 2512: heap size 374 MB, throughput 0.96697
Reading from 2513: heap size 268 MB, throughput 0.99608
Reading from 2513: heap size 277 MB, throughput 0.997337
Reading from 2513: heap size 277 MB, throughput 0.99706
Reading from 2513: heap size 285 MB, throughput 0.995661
Reading from 2512: heap size 377 MB, throughput 0.955637
Reading from 2513: heap size 285 MB, throughput 0.997278
Reading from 2513: heap size 295 MB, throughput 0.997475
Reading from 2513: heap size 295 MB, throughput 0.996189
Reading from 2513: heap size 305 MB, throughput 0.996624
Reading from 2512: heap size 377 MB, throughput 0.964499
Reading from 2513: heap size 305 MB, throughput 0.997126
Reading from 2513: heap size 315 MB, throughput 0.998344
Reading from 2513: heap size 315 MB, throughput 0.996753
Reading from 2512: heap size 380 MB, throughput 0.957652
Reading from 2513: heap size 325 MB, throughput 0.997869
Reading from 2513: heap size 325 MB, throughput 0.997661
Reading from 2513: heap size 334 MB, throughput 0.997814
Reading from 2512: heap size 382 MB, throughput 0.965823
Reading from 2513: heap size 334 MB, throughput 0.99769
Reading from 2513: heap size 343 MB, throughput 0.997659
Reading from 2513: heap size 343 MB, throughput 0.998366
Reading from 2512: heap size 381 MB, throughput 0.967641
Reading from 2512: heap size 387 MB, throughput 0.774918
Reading from 2513: heap size 351 MB, throughput 0.99783
Reading from 2512: heap size 384 MB, throughput 0.713137
Reading from 2512: heap size 389 MB, throughput 0.760083
Equal recommendation: 710 MB each
Reading from 2513: heap size 352 MB, throughput 0.998358
Reading from 2512: heap size 398 MB, throughput 0.928929
Reading from 2513: heap size 361 MB, throughput 0.996485
Reading from 2513: heap size 361 MB, throughput 0.997191
Reading from 2513: heap size 371 MB, throughput 0.997503
Reading from 2512: heap size 400 MB, throughput 0.983404
Reading from 2513: heap size 371 MB, throughput 0.997624
Reading from 2513: heap size 381 MB, throughput 0.997947
Reading from 2513: heap size 382 MB, throughput 0.995571
Reading from 2512: heap size 403 MB, throughput 0.985761
Reading from 2513: heap size 392 MB, throughput 0.99715
Reading from 2513: heap size 392 MB, throughput 0.997043
Reading from 2513: heap size 404 MB, throughput 0.99788
Reading from 2512: heap size 406 MB, throughput 0.978601
Reading from 2513: heap size 404 MB, throughput 0.99794
Reading from 2513: heap size 416 MB, throughput 0.9975
Reading from 2513: heap size 416 MB, throughput 0.996664
Reading from 2512: heap size 405 MB, throughput 0.977921
Reading from 2513: heap size 428 MB, throughput 0.997485
Reading from 2513: heap size 428 MB, throughput 0.99708
Reading from 2513: heap size 441 MB, throughput 0.998557
Reading from 2512: heap size 408 MB, throughput 0.981891
Reading from 2513: heap size 441 MB, throughput 0.996957
Equal recommendation: 710 MB each
Reading from 2513: heap size 453 MB, throughput 0.997414
Reading from 2513: heap size 453 MB, throughput 0.997494
Reading from 2512: heap size 405 MB, throughput 0.975207
Reading from 2513: heap size 466 MB, throughput 0.997567
Reading from 2513: heap size 466 MB, throughput 0.997489
Reading from 2513: heap size 479 MB, throughput 0.996738
Reading from 2512: heap size 408 MB, throughput 0.973604
Reading from 2513: heap size 480 MB, throughput 0.998164
Reading from 2513: heap size 480 MB, throughput 0.998475
Reading from 2513: heap size 480 MB, throughput 0.998585
Reading from 2512: heap size 405 MB, throughput 0.976872
Reading from 2513: heap size 480 MB, throughput 0.998286
Reading from 2513: heap size 480 MB, throughput 0.998048
Reading from 2513: heap size 480 MB, throughput 0.998339
Reading from 2512: heap size 408 MB, throughput 0.719082
Reading from 2513: heap size 480 MB, throughput 0.998713
Reading from 2513: heap size 480 MB, throughput 0.997964
Reading from 2513: heap size 480 MB, throughput 0.998258
Reading from 2513: heap size 480 MB, throughput 0.998612
Reading from 2512: heap size 445 MB, throughput 0.970262
Equal recommendation: 710 MB each
Reading from 2513: heap size 480 MB, throughput 0.998097
Reading from 2512: heap size 446 MB, throughput 0.9537
Reading from 2512: heap size 452 MB, throughput 0.788023
Reading from 2512: heap size 452 MB, throughput 0.847995
Reading from 2513: heap size 480 MB, throughput 0.998148
Reading from 2513: heap size 480 MB, throughput 0.998099
Reading from 2512: heap size 461 MB, throughput 0.976729
Reading from 2513: heap size 479 MB, throughput 0.998012
Reading from 2513: heap size 479 MB, throughput 0.998394
Reading from 2512: heap size 460 MB, throughput 0.98646
Reading from 2513: heap size 480 MB, throughput 0.998506
Reading from 2513: heap size 480 MB, throughput 0.998358
Reading from 2513: heap size 480 MB, throughput 0.997952
Reading from 2513: heap size 480 MB, throughput 0.998264
Reading from 2512: heap size 467 MB, throughput 0.987641
Reading from 2513: heap size 480 MB, throughput 0.99847
Reading from 2513: heap size 480 MB, throughput 0.998286
Reading from 2513: heap size 480 MB, throughput 0.998277
Reading from 2512: heap size 469 MB, throughput 0.986048
Reading from 2513: heap size 479 MB, throughput 0.998334
Reading from 2513: heap size 480 MB, throughput 0.998552
Equal recommendation: 710 MB each
Reading from 2513: heap size 480 MB, throughput 0.998468
Reading from 2512: heap size 471 MB, throughput 0.980673
Reading from 2513: heap size 481 MB, throughput 0.997946
Reading from 2513: heap size 481 MB, throughput 0.998393
Reading from 2513: heap size 481 MB, throughput 0.998428
Reading from 2512: heap size 473 MB, throughput 0.978618
Reading from 2513: heap size 481 MB, throughput 0.998493
Reading from 2513: heap size 481 MB, throughput 0.998206
Reading from 2513: heap size 481 MB, throughput 0.997839
Reading from 2512: heap size 472 MB, throughput 0.982476
Reading from 2513: heap size 481 MB, throughput 0.998059
Reading from 2513: heap size 481 MB, throughput 0.998684
Reading from 2513: heap size 481 MB, throughput 0.997783
Reading from 2513: heap size 481 MB, throughput 0.998396
Reading from 2512: heap size 474 MB, throughput 0.979965
Reading from 2513: heap size 481 MB, throughput 0.998447
Reading from 2513: heap size 481 MB, throughput 0.998106
Reading from 2513: heap size 481 MB, throughput 0.998379
Reading from 2512: heap size 477 MB, throughput 0.98805
Reading from 2512: heap size 478 MB, throughput 0.891754
Reading from 2513: heap size 481 MB, throughput 0.997926
Equal recommendation: 710 MB each
Reading from 2512: heap size 479 MB, throughput 0.861232
Reading from 2512: heap size 481 MB, throughput 0.950078
Reading from 2513: heap size 481 MB, throughput 0.998164
Reading from 2513: heap size 481 MB, throughput 0.99839
Reading from 2513: heap size 481 MB, throughput 0.99822
Reading from 2512: heap size 491 MB, throughput 0.989057
Reading from 2513: heap size 480 MB, throughput 0.997433
Reading from 2513: heap size 480 MB, throughput 0.997715
Reading from 2513: heap size 480 MB, throughput 0.998262
Reading from 2513: heap size 481 MB, throughput 0.998184
Reading from 2512: heap size 492 MB, throughput 0.987447
Reading from 2513: heap size 481 MB, throughput 0.997991
Reading from 2513: heap size 480 MB, throughput 0.998153
Reading from 2513: heap size 481 MB, throughput 0.99831
Reading from 2512: heap size 494 MB, throughput 0.985971
Reading from 2513: heap size 480 MB, throughput 0.998601
Reading from 2513: heap size 481 MB, throughput 0.998331
Reading from 2513: heap size 481 MB, throughput 0.998113
Reading from 2513: heap size 481 MB, throughput 0.998176
Equal recommendation: 710 MB each
Reading from 2512: heap size 497 MB, throughput 0.983107
Reading from 2513: heap size 481 MB, throughput 0.998337
Reading from 2513: heap size 481 MB, throughput 0.998714
Reading from 2513: heap size 481 MB, throughput 0.998495
Reading from 2512: heap size 497 MB, throughput 0.981209
Reading from 2513: heap size 481 MB, throughput 0.997909
Reading from 2513: heap size 481 MB, throughput 0.998149
Reading from 2513: heap size 481 MB, throughput 0.998279
Reading from 2513: heap size 481 MB, throughput 0.99804
Reading from 2512: heap size 499 MB, throughput 0.97742
Reading from 2513: heap size 481 MB, throughput 0.997932
Reading from 2513: heap size 481 MB, throughput 0.998351
Reading from 2513: heap size 481 MB, throughput 0.998333
Reading from 2513: heap size 481 MB, throughput 0.998325
Reading from 2512: heap size 502 MB, throughput 0.991227
Reading from 2513: heap size 481 MB, throughput 0.997546
Reading from 2512: heap size 503 MB, throughput 0.963461
Reading from 2512: heap size 504 MB, throughput 0.821492
Reading from 2513: heap size 481 MB, throughput 0.997522
Reading from 2512: heap size 507 MB, throughput 0.972408
Reading from 2513: heap size 481 MB, throughput 0.998501
Equal recommendation: 710 MB each
Reading from 2513: heap size 481 MB, throughput 0.998407
Reading from 2513: heap size 481 MB, throughput 0.998545
Reading from 2513: heap size 481 MB, throughput 0.997839
Reading from 2512: heap size 516 MB, throughput 0.994598
Reading from 2513: heap size 481 MB, throughput 0.998529
Reading from 2513: heap size 481 MB, throughput 0.99833
Reading from 2513: heap size 481 MB, throughput 0.99788
Reading from 2513: heap size 481 MB, throughput 0.998333
Reading from 2513: heap size 481 MB, throughput 0.998286
Reading from 2512: heap size 517 MB, throughput 0.989505
Reading from 2513: heap size 481 MB, throughput 0.998367
Reading from 2513: heap size 481 MB, throughput 0.998036
Reading from 2513: heap size 481 MB, throughput 0.997596
Reading from 2513: heap size 481 MB, throughput 0.998195
Reading from 2512: heap size 519 MB, throughput 0.98753
Reading from 2513: heap size 481 MB, throughput 0.998192
Reading from 2513: heap size 481 MB, throughput 0.998574
Equal recommendation: 710 MB each
Reading from 2513: heap size 481 MB, throughput 0.998276
Reading from 2513: heap size 481 MB, throughput 0.998024
Reading from 2512: heap size 522 MB, throughput 0.986019
Reading from 2513: heap size 481 MB, throughput 0.998428
Reading from 2513: heap size 481 MB, throughput 0.998338
Reading from 2513: heap size 481 MB, throughput 0.997951
Reading from 2513: heap size 481 MB, throughput 0.998067
Reading from 2512: heap size 523 MB, throughput 0.988785
Reading from 2513: heap size 481 MB, throughput 0.998449
Reading from 2513: heap size 481 MB, throughput 0.998556
Reading from 2513: heap size 481 MB, throughput 0.998135
Reading from 2513: heap size 481 MB, throughput 0.998193
Reading from 2512: heap size 524 MB, throughput 0.980439
Reading from 2513: heap size 481 MB, throughput 0.998458
Reading from 2513: heap size 481 MB, throughput 0.998441
Reading from 2512: heap size 524 MB, throughput 0.975988
Reading from 2512: heap size 529 MB, throughput 0.876346
Reading from 2513: heap size 481 MB, throughput 0.998477
Reading from 2513: heap size 481 MB, throughput 0.998247
Reading from 2513: heap size 481 MB, throughput 0.998103
Equal recommendation: 710 MB each
Reading from 2512: heap size 534 MB, throughput 0.982878
Reading from 2513: heap size 481 MB, throughput 0.998277
Reading from 2513: heap size 481 MB, throughput 0.998006
Reading from 2513: heap size 481 MB, throughput 0.997939
Reading from 2513: heap size 481 MB, throughput 0.998049
Reading from 2512: heap size 536 MB, throughput 0.990175
Reading from 2513: heap size 481 MB, throughput 0.99854
Reading from 2513: heap size 481 MB, throughput 0.998314
Reading from 2513: heap size 481 MB, throughput 0.997892
Reading from 2513: heap size 481 MB, throughput 0.99741
Reading from 2513: heap size 481 MB, throughput 0.998229
Reading from 2512: heap size 539 MB, throughput 0.991461
Reading from 2513: heap size 481 MB, throughput 0.998414
Reading from 2513: heap size 481 MB, throughput 0.998414
Reading from 2513: heap size 481 MB, throughput 0.998401
Reading from 2513: heap size 481 MB, throughput 0.997948
Reading from 2513: heap size 481 MB, throughput 0.998615
Reading from 2512: heap size 541 MB, throughput 0.989855
Equal recommendation: 710 MB each
Reading from 2513: heap size 481 MB, throughput 0.997881
Reading from 2513: heap size 481 MB, throughput 0.99791
Reading from 2513: heap size 481 MB, throughput 0.998426
Reading from 2513: heap size 481 MB, throughput 0.998391
Reading from 2512: heap size 540 MB, throughput 0.987812
Reading from 2513: heap size 481 MB, throughput 0.998326
Reading from 2513: heap size 481 MB, throughput 0.997649
Reading from 2513: heap size 481 MB, throughput 0.998149
Reading from 2513: heap size 481 MB, throughput 0.998677
Reading from 2512: heap size 542 MB, throughput 0.981226
Reading from 2513: heap size 481 MB, throughput 0.998358
Reading from 2513: heap size 481 MB, throughput 0.998552
Reading from 2513: heap size 481 MB, throughput 0.998093
Reading from 2513: heap size 481 MB, throughput 0.99812
Reading from 2512: heap size 545 MB, throughput 0.989815
Reading from 2512: heap size 546 MB, throughput 0.864088
Reading from 2513: heap size 481 MB, throughput 0.99804
Reading from 2512: heap size 547 MB, throughput 0.950535
Reading from 2513: heap size 481 MB, throughput 0.998381
Equal recommendation: 710 MB each
Reading from 2513: heap size 481 MB, throughput 0.998229
Reading from 2513: heap size 481 MB, throughput 0.997839
Reading from 2513: heap size 481 MB, throughput 0.99823
Reading from 2513: heap size 481 MB, throughput 0.99844
Reading from 2512: heap size 549 MB, throughput 0.990817
Reading from 2513: heap size 481 MB, throughput 0.998483
Reading from 2513: heap size 481 MB, throughput 0.997243
Reading from 2513: heap size 481 MB, throughput 0.998275
Reading from 2513: heap size 481 MB, throughput 0.998365
Reading from 2513: heap size 481 MB, throughput 0.998432
Reading from 2512: heap size 555 MB, throughput 0.98998
Reading from 2513: heap size 481 MB, throughput 0.998236
Reading from 2513: heap size 481 MB, throughput 0.998153
Reading from 2513: heap size 481 MB, throughput 0.99834
Reading from 2513: heap size 481 MB, throughput 0.998389
Reading from 2513: heap size 481 MB, throughput 0.997643
Reading from 2512: heap size 557 MB, throughput 0.9876
Reading from 2513: heap size 481 MB, throughput 0.998285
Equal recommendation: 710 MB each
Reading from 2513: heap size 481 MB, throughput 0.998355
Reading from 2513: heap size 481 MB, throughput 0.998642
Reading from 2513: heap size 481 MB, throughput 0.997963
Reading from 2512: heap size 558 MB, throughput 0.985004
Reading from 2513: heap size 481 MB, throughput 0.99792
Reading from 2513: heap size 481 MB, throughput 0.998381
Reading from 2513: heap size 481 MB, throughput 0.998406
Reading from 2513: heap size 481 MB, throughput 0.998586
Reading from 2512: heap size 560 MB, throughput 0.983738
Reading from 2513: heap size 481 MB, throughput 0.998196
Reading from 2513: heap size 481 MB, throughput 0.998238
Reading from 2513: heap size 481 MB, throughput 0.99834
Reading from 2513: heap size 481 MB, throughput 0.998193
Reading from 2512: heap size 563 MB, throughput 0.991059
Reading from 2513: heap size 481 MB, throughput 0.997673
Reading from 2512: heap size 563 MB, throughput 0.910299
Reading from 2513: heap size 481 MB, throughput 0.998201
Reading from 2512: heap size 567 MB, throughput 0.984597
Reading from 2513: heap size 481 MB, throughput 0.998266
Equal recommendation: 710 MB each
Reading from 2513: heap size 481 MB, throughput 0.998553
Reading from 2513: heap size 481 MB, throughput 0.998064
Reading from 2513: heap size 481 MB, throughput 0.998396
Reading from 2513: heap size 481 MB, throughput 0.99849
Reading from 2512: heap size 568 MB, throughput 0.990827
Reading from 2513: heap size 481 MB, throughput 0.998225
Reading from 2513: heap size 481 MB, throughput 0.99846
Reading from 2513: heap size 481 MB, throughput 0.998121
Reading from 2513: heap size 481 MB, throughput 0.998021
Reading from 2513: heap size 481 MB, throughput 0.997918
Reading from 2512: heap size 572 MB, throughput 0.989959
Reading from 2513: heap size 481 MB, throughput 0.998666
Reading from 2513: heap size 481 MB, throughput 0.998214
Reading from 2513: heap size 481 MB, throughput 0.998342
Reading from 2513: heap size 481 MB, throughput 0.998236
Reading from 2512: heap size 574 MB, throughput 0.987393
Reading from 2513: heap size 481 MB, throughput 0.998353
Equal recommendation: 710 MB each
Reading from 2513: heap size 481 MB, throughput 0.9982
Reading from 2513: heap size 481 MB, throughput 0.998345
Reading from 2513: heap size 481 MB, throughput 0.998194
Reading from 2513: heap size 481 MB, throughput 0.998349
Reading from 2512: heap size 573 MB, throughput 0.988159
Reading from 2513: heap size 481 MB, throughput 0.998386
Reading from 2513: heap size 481 MB, throughput 0.998271
Reading from 2513: heap size 481 MB, throughput 0.998098
Reading from 2513: heap size 481 MB, throughput 0.998428
Reading from 2513: heap size 481 MB, throughput 0.998314
Reading from 2512: heap size 575 MB, throughput 0.993388
Reading from 2513: heap size 481 MB, throughput 0.997764
Reading from 2513: heap size 481 MB, throughput 0.997681
Reading from 2512: heap size 579 MB, throughput 0.971182
Reading from 2512: heap size 580 MB, throughput 0.908634
Reading from 2513: heap size 481 MB, throughput 0.998387
Reading from 2513: heap size 481 MB, throughput 0.998039
Reading from 2513: heap size 480 MB, throughput 0.997925
Reading from 2513: heap size 480 MB, throughput 0.998269
Equal recommendation: 710 MB each
Reading from 2513: heap size 480 MB, throughput 0.9985
Reading from 2512: heap size 586 MB, throughput 0.990473
Reading from 2513: heap size 481 MB, throughput 0.998407
Reading from 2513: heap size 481 MB, throughput 0.998479
Reading from 2513: heap size 481 MB, throughput 0.998125
Reading from 2513: heap size 481 MB, throughput 0.997906
Reading from 2513: heap size 481 MB, throughput 0.998393
Reading from 2513: heap size 481 MB, throughput 0.970526
Reading from 2512: heap size 586 MB, throughput 0.993464
Reading from 2513: heap size 481 MB, throughput 0.998023
Reading from 2513: heap size 481 MB, throughput 0.998235
Reading from 2513: heap size 481 MB, throughput 0.998328
Reading from 2513: heap size 481 MB, throughput 0.998557
Reading from 2513: heap size 481 MB, throughput 0.997892
Reading from 2513: heap size 481 MB, throughput 0.998075
Reading from 2512: heap size 587 MB, throughput 0.991858
Reading from 2513: heap size 481 MB, throughput 0.998575
Equal recommendation: 710 MB each
Reading from 2513: heap size 481 MB, throughput 0.998658
Reading from 2513: heap size 481 MB, throughput 0.998543
Reading from 2513: heap size 481 MB, throughput 0.998027
Reading from 2512: heap size 590 MB, throughput 0.987174
Reading from 2513: heap size 481 MB, throughput 0.998057
Reading from 2513: heap size 481 MB, throughput 0.998196
Reading from 2513: heap size 481 MB, throughput 0.998107
Reading from 2513: heap size 481 MB, throughput 0.998272
Reading from 2513: heap size 481 MB, throughput 0.998436
Reading from 2513: heap size 481 MB, throughput 0.998277
Reading from 2512: heap size 593 MB, throughput 0.992446
Reading from 2513: heap size 481 MB, throughput 0.998334
Reading from 2513: heap size 481 MB, throughput 0.997825
Reading from 2512: heap size 594 MB, throughput 0.969465
Reading from 2513: heap size 481 MB, throughput 0.998045
Reading from 2512: heap size 594 MB, throughput 0.943059
Reading from 2513: heap size 481 MB, throughput 0.998552
Reading from 2513: heap size 481 MB, throughput 0.998575
Equal recommendation: 710 MB each
Reading from 2513: heap size 481 MB, throughput 0.998437
Reading from 2513: heap size 481 MB, throughput 0.99792
Reading from 2512: heap size 597 MB, throughput 0.991076
Reading from 2513: heap size 481 MB, throughput 0.998163
Reading from 2513: heap size 481 MB, throughput 0.998374
Reading from 2513: heap size 481 MB, throughput 0.998154
Reading from 2513: heap size 481 MB, throughput 0.998269
Reading from 2513: heap size 481 MB, throughput 0.998494
Reading from 2513: heap size 481 MB, throughput 0.998178
Reading from 2512: heap size 600 MB, throughput 0.991562
Reading from 2513: heap size 481 MB, throughput 0.998561
Reading from 2513: heap size 481 MB, throughput 0.997486
Reading from 2513: heap size 481 MB, throughput 0.997792
Reading from 2513: heap size 481 MB, throughput 0.998684
Reading from 2513: heap size 481 MB, throughput 0.99836
Reading from 2512: heap size 602 MB, throughput 0.989271
Reading from 2513: heap size 481 MB, throughput 0.998677
Reading from 2513: heap size 458 MB, throughput 0.976035
Equal recommendation: 710 MB each
Reading from 2513: heap size 437 MB, throughput 0.998097
Reading from 2513: heap size 451 MB, throughput 0.998828
Reading from 2513: heap size 463 MB, throughput 0.998666
Reading from 2512: heap size 604 MB, throughput 0.987132
Reading from 2513: heap size 478 MB, throughput 0.997946
Reading from 2513: heap size 478 MB, throughput 0.998111
Reading from 2513: heap size 478 MB, throughput 0.99665
Reading from 2513: heap size 483 MB, throughput 0.998358
Reading from 2513: heap size 483 MB, throughput 0.997995
Reading from 2512: heap size 605 MB, throughput 0.99274
Reading from 2513: heap size 483 MB, throughput 0.997687
Reading from 2512: heap size 607 MB, throughput 0.913164
Reading from 2513: heap size 483 MB, throughput 0.998232
Reading from 2513: heap size 483 MB, throughput 0.998072
Reading from 2512: heap size 609 MB, throughput 0.986329
Reading from 2513: heap size 483 MB, throughput 0.998355
Reading from 2513: heap size 483 MB, throughput 0.997816
Reading from 2513: heap size 483 MB, throughput 0.997656
Equal recommendation: 710 MB each
Reading from 2513: heap size 483 MB, throughput 0.998109
Reading from 2513: heap size 483 MB, throughput 0.997784
Reading from 2513: heap size 482 MB, throughput 0.998321
Reading from 2512: heap size 618 MB, throughput 0.992235
Reading from 2513: heap size 483 MB, throughput 0.99844
Reading from 2513: heap size 484 MB, throughput 0.998369
Reading from 2513: heap size 484 MB, throughput 0.998537
Reading from 2513: heap size 484 MB, throughput 0.997739
Reading from 2513: heap size 484 MB, throughput 0.997729
Reading from 2513: heap size 484 MB, throughput 0.998465
Reading from 2513: heap size 483 MB, throughput 0.99855
Reading from 2512: heap size 619 MB, throughput 0.991281
Reading from 2513: heap size 483 MB, throughput 0.998617
Reading from 2513: heap size 483 MB, throughput 0.998161
Reading from 2513: heap size 484 MB, throughput 0.998414
Reading from 2513: heap size 484 MB, throughput 0.9983
Reading from 2513: heap size 484 MB, throughput 0.998443
Equal recommendation: 710 MB each
Reading from 2512: heap size 620 MB, throughput 0.987085
Reading from 2513: heap size 484 MB, throughput 0.998214
Reading from 2513: heap size 484 MB, throughput 0.998276
Reading from 2513: heap size 484 MB, throughput 0.99827
Reading from 2513: heap size 484 MB, throughput 0.998561
Reading from 2513: heap size 484 MB, throughput 0.998158
Reading from 2513: heap size 484 MB, throughput 0.998322
Reading from 2512: heap size 622 MB, throughput 0.986373
Reading from 2513: heap size 484 MB, throughput 0.998006
Reading from 2513: heap size 484 MB, throughput 0.997975
Reading from 2512: heap size 623 MB, throughput 0.977693
Reading from 2513: heap size 484 MB, throughput 0.998325
Reading from 2512: heap size 627 MB, throughput 0.978479
Reading from 2513: heap size 484 MB, throughput 0.997916
Reading from 2513: heap size 484 MB, throughput 0.997979
Reading from 2513: heap size 484 MB, throughput 0.998582
Reading from 2513: heap size 484 MB, throughput 0.998565
Reading from 2513: heap size 485 MB, throughput 0.998233
Equal recommendation: 710 MB each
Reading from 2513: heap size 485 MB, throughput 0.998333
Reading from 2512: heap size 634 MB, throughput 0.994618
Reading from 2513: heap size 485 MB, throughput 0.998625
Reading from 2513: heap size 485 MB, throughput 0.998614
Reading from 2513: heap size 485 MB, throughput 0.998324
Reading from 2513: heap size 485 MB, throughput 0.998287
Reading from 2513: heap size 485 MB, throughput 0.998407
Reading from 2513: heap size 485 MB, throughput 0.998214
Reading from 2512: heap size 634 MB, throughput 0.990761
Reading from 2513: heap size 485 MB, throughput 0.998645
Reading from 2513: heap size 485 MB, throughput 0.998285
Reading from 2513: heap size 485 MB, throughput 0.998136
Reading from 2513: heap size 485 MB, throughput 0.998295
Reading from 2513: heap size 485 MB, throughput 0.99816
Reading from 2513: heap size 485 MB, throughput 0.997807
Reading from 2512: heap size 633 MB, throughput 0.989851
Reading from 2513: heap size 485 MB, throughput 0.998258
Equal recommendation: 710 MB each
Reading from 2513: heap size 485 MB, throughput 0.998222
Reading from 2513: heap size 485 MB, throughput 0.998526
Reading from 2513: heap size 485 MB, throughput 0.998368
Reading from 2513: heap size 485 MB, throughput 0.997928
Reading from 2513: heap size 485 MB, throughput 0.998409
Reading from 2512: heap size 635 MB, throughput 0.987604
Reading from 2513: heap size 485 MB, throughput 0.998505
Reading from 2513: heap size 485 MB, throughput 0.998578
Reading from 2513: heap size 485 MB, throughput 0.998175
Reading from 2512: heap size 638 MB, throughput 0.983341
Reading from 2513: heap size 485 MB, throughput 0.99786
Reading from 2512: heap size 639 MB, throughput 0.954343
Reading from 2513: heap size 485 MB, throughput 0.998594
Reading from 2513: heap size 485 MB, throughput 0.998454
Reading from 2513: heap size 485 MB, throughput 0.998245
Reading from 2513: heap size 485 MB, throughput 0.998627
Reading from 2513: heap size 485 MB, throughput 0.998489
Reading from 2513: heap size 485 MB, throughput 0.998499
Equal recommendation: 710 MB each
Reading from 2512: heap size 644 MB, throughput 0.991848
Reading from 2513: heap size 485 MB, throughput 0.997911
Reading from 2513: heap size 485 MB, throughput 0.997895
Reading from 2513: heap size 485 MB, throughput 0.998327
Reading from 2513: heap size 485 MB, throughput 0.998508
Reading from 2513: heap size 485 MB, throughput 0.998195
Reading from 2513: heap size 485 MB, throughput 0.998065
Reading from 2512: heap size 644 MB, throughput 0.990805
Reading from 2513: heap size 485 MB, throughput 0.998001
Reading from 2513: heap size 485 MB, throughput 0.99839
Reading from 2513: heap size 485 MB, throughput 0.998235
Reading from 2513: heap size 485 MB, throughput 0.998219
Reading from 2513: heap size 485 MB, throughput 0.998336
Reading from 2513: heap size 485 MB, throughput 0.998141
Reading from 2512: heap size 645 MB, throughput 0.988459
Reading from 2513: heap size 485 MB, throughput 0.998374
Reading from 2513: heap size 485 MB, throughput 0.998123
Equal recommendation: 710 MB each
Reading from 2513: heap size 485 MB, throughput 0.998168
Reading from 2513: heap size 485 MB, throughput 0.998564
Reading from 2513: heap size 485 MB, throughput 0.998475
Reading from 2512: heap size 646 MB, throughput 0.98599
Reading from 2513: heap size 485 MB, throughput 0.998382
Reading from 2513: heap size 485 MB, throughput 0.997973
Reading from 2513: heap size 485 MB, throughput 0.997765
Reading from 2512: heap size 649 MB, throughput 0.983007
Reading from 2513: heap size 485 MB, throughput 0.998167
Client 2512 died
Clients: 1
Reading from 2513: heap size 485 MB, throughput 0.997689
Reading from 2513: heap size 485 MB, throughput 0.997722
Reading from 2513: heap size 485 MB, throughput 0.997972
Reading from 2513: heap size 485 MB, throughput 0.997281
Reading from 2513: heap size 485 MB, throughput 0.998244
Reading from 2513: heap size 485 MB, throughput 0.997248
Reading from 2513: heap size 485 MB, throughput 0.998034
Reading from 2513: heap size 485 MB, throughput 0.997975
Recommendation: one client; give it all the memory
Reading from 2513: heap size 485 MB, throughput 0.998364
Reading from 2513: heap size 485 MB, throughput 0.99771
Reading from 2513: heap size 485 MB, throughput 0.997986
Reading from 2513: heap size 485 MB, throughput 0.997612
Reading from 2513: heap size 485 MB, throughput 0.997929
Reading from 2513: heap size 485 MB, throughput 0.997552
Reading from 2513: heap size 485 MB, throughput 0.998242
Reading from 2513: heap size 484 MB, throughput 0.99823
Reading from 2513: heap size 484 MB, throughput 0.998495
Reading from 2513: heap size 484 MB, throughput 0.997927
Reading from 2513: heap size 484 MB, throughput 0.998122
Reading from 2513: heap size 485 MB, throughput 0.997225
Reading from 2513: heap size 485 MB, throughput 0.99854
Reading from 2513: heap size 485 MB, throughput 0.998205
Recommendation: one client; give it all the memory
Reading from 2513: heap size 485 MB, throughput 0.998085
Reading from 2513: heap size 485 MB, throughput 0.997428
Reading from 2513: heap size 485 MB, throughput 0.997678
Reading from 2513: heap size 485 MB, throughput 0.997794
Reading from 2513: heap size 485 MB, throughput 0.99726
Reading from 2513: heap size 485 MB, throughput 0.997683
Reading from 2513: heap size 485 MB, throughput 0.997468
Reading from 2513: heap size 485 MB, throughput 0.998281
Reading from 2513: heap size 485 MB, throughput 0.998133
Reading from 2513: heap size 485 MB, throughput 0.997322
Reading from 2513: heap size 485 MB, throughput 0.997934
Reading from 2513: heap size 485 MB, throughput 0.99785
Reading from 2513: heap size 485 MB, throughput 0.998175
Reading from 2513: heap size 485 MB, throughput 0.998065
Reading from 2513: heap size 485 MB, throughput 0.997135
Reading from 2513: heap size 485 MB, throughput 0.997806
Recommendation: one client; give it all the memory
Reading from 2513: heap size 485 MB, throughput 0.997804
Reading from 2513: heap size 485 MB, throughput 0.99882
Client 2513 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
