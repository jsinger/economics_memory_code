#include <cmath>
#include <cstdio>
#include <mct/hash-map.hpp>
#include "LRUCache.hpp"
#include "CacheReading.hpp"
#include "ReadingIterator.hpp"

const char* LRUCache::NAME = "lru";

class LRUCache::Cache
{
public:
    mct::linked_hash_map<unsigned long long, CacheReading> c;
};

class LRUCache::Iterator : public ReadingIterator
{
private:
    const LRUCache::Cache& m_Cache;
    mct::linked_hash_map<unsigned long long, CacheReading>::const_iterator m_It;

public:
    Iterator(const LRUCache::Cache& c);
    virtual bool hasNext() const;
    virtual std::pair<unsigned long long, double> getNext();
};

LRUCache::Iterator::Iterator(const LRUCache::Cache& c) : m_Cache(c)
{
    m_It = m_Cache.c.begin();
}

bool LRUCache::Iterator::hasNext() const
{
    return m_It != m_Cache.c.end();
}

std::pair<unsigned long long, double> LRUCache::Iterator::getNext()
{
    std::pair<unsigned long long, double> p(m_It->second.getHeapSizeMB(), m_It->second.getThroughput());
    ++m_It;
    return p;
}

LRUCache::LRUCache(size_t capacity) : ReadingCache(), m_Capacity(capacity)
{
    m_Cache = new Cache;
}

LRUCache::~LRUCache()
{
    delete m_Cache;
}

std::string LRUCache::getName() const
{
    return NAME;
}

bool LRUCache::isValid() const
{
    return m_Cache->c.size() >= LRUCache::MIN_READINGS_VALID;
}

void LRUCache::addReading(unsigned long long heapSizeMB, double throughput)
{
    mct::linked_hash_map<unsigned long long, CacheReading>::iterator it = m_Cache->c.find(heapSizeMB);
    if (it == m_Cache->c.end())
    {
        // This heap size is not in the cache
        m_Cache->c.push_front(std::pair<unsigned long long, CacheReading>(heapSizeMB, CacheReading(heapSizeMB, throughput)));

        if (m_Cache->c.size() > m_Capacity)
            m_Cache->c.pop_back();
    }
    else
    {
        // This heap size is in the cache, so should be updated and moved to the front
        it->second.updateReading(throughput);
        m_Cache->c.relink(m_Cache->c.begin(), it);
    }
}

ReadingIterator* LRUCache::newIterator() const
{
    return new Iterator(*m_Cache);
}

void LRUCache::dumpTo(FILE* fd) const
{
    fprintf(fd, "    Readings: %zu (max %zu) (%s)\n", m_Cache->c.size(), m_Capacity, (isValid() ? "valid" : "invalid"));

    for (mct::linked_hash_map<unsigned long long, CacheReading>::const_iterator it = m_Cache->c.begin(); it != m_Cache->c.end(); ++it)
        it->second.dumpTo(fd);
}
