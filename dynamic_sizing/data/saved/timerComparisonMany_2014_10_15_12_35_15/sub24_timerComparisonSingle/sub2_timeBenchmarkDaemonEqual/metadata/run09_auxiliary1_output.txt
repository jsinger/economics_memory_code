economemd
    total memory: 6265 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub24_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run09_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 13062: heap size 9 MB, throughput 0.992242
Clients: 1
Client 13062 has a minimum heap size of 30 MB
Reading from 13061: heap size 9 MB, throughput 0.977395
Clients: 2
Client 13061 has a minimum heap size of 1223 MB
Reading from 13062: heap size 9 MB, throughput 0.972467
Reading from 13062: heap size 9 MB, throughput 0.968327
Reading from 13061: heap size 9 MB, throughput 0.960175
Reading from 13062: heap size 9 MB, throughput 0.974703
Reading from 13062: heap size 11 MB, throughput 0.609039
Reading from 13062: heap size 11 MB, throughput 0.449423
Reading from 13062: heap size 16 MB, throughput 0.72562
Reading from 13062: heap size 16 MB, throughput 0.680391
Reading from 13062: heap size 24 MB, throughput 0.88781
Reading from 13062: heap size 24 MB, throughput 0.800487
Reading from 13062: heap size 34 MB, throughput 0.892613
Reading from 13062: heap size 34 MB, throughput 0.895527
Reading from 13062: heap size 50 MB, throughput 0.947782
Reading from 13061: heap size 11 MB, throughput 0.974619
Reading from 13062: heap size 50 MB, throughput 0.928139
Reading from 13062: heap size 75 MB, throughput 0.961509
Reading from 13062: heap size 75 MB, throughput 0.941088
Reading from 13062: heap size 116 MB, throughput 0.967061
Reading from 13062: heap size 116 MB, throughput 0.94383
Reading from 13061: heap size 11 MB, throughput 0.984117
Reading from 13061: heap size 15 MB, throughput 0.839372
Reading from 13061: heap size 19 MB, throughput 0.906388
Reading from 13061: heap size 25 MB, throughput 0.960288
Reading from 13061: heap size 29 MB, throughput 0.855641
Reading from 13061: heap size 32 MB, throughput 0.458055
Reading from 13061: heap size 41 MB, throughput 0.936878
Reading from 13061: heap size 47 MB, throughput 0.907073
Reading from 13062: heap size 159 MB, throughput 0.970274
Reading from 13061: heap size 49 MB, throughput 0.280686
Reading from 13061: heap size 64 MB, throughput 0.890052
Reading from 13061: heap size 68 MB, throughput 0.732577
Reading from 13062: heap size 165 MB, throughput 0.859852
Reading from 13061: heap size 70 MB, throughput 0.200471
Reading from 13061: heap size 90 MB, throughput 0.738043
Reading from 13061: heap size 96 MB, throughput 0.780543
Reading from 13061: heap size 98 MB, throughput 0.712743
Reading from 13061: heap size 101 MB, throughput 0.724262
Reading from 13061: heap size 106 MB, throughput 0.725555
Reading from 13061: heap size 110 MB, throughput 0.67294
Reading from 13062: heap size 221 MB, throughput 0.965519
Reading from 13061: heap size 114 MB, throughput 0.0972138
Reading from 13061: heap size 141 MB, throughput 0.418275
Reading from 13062: heap size 236 MB, throughput 0.932757
Reading from 13061: heap size 146 MB, throughput 0.286856
Reading from 13061: heap size 177 MB, throughput 0.558058
Reading from 13061: heap size 178 MB, throughput 0.63053
Reading from 13061: heap size 179 MB, throughput 0.700911
Reading from 13061: heap size 182 MB, throughput 0.585521
Reading from 13061: heap size 185 MB, throughput 0.547996
Reading from 13061: heap size 192 MB, throughput 0.612667
Reading from 13061: heap size 199 MB, throughput 0.607289
Reading from 13062: heap size 264 MB, throughput 0.99535
Reading from 13061: heap size 204 MB, throughput 0.108026
Reading from 13061: heap size 244 MB, throughput 0.495481
Reading from 13061: heap size 247 MB, throughput 0.114605
Reading from 13062: heap size 263 MB, throughput 0.845569
Reading from 13061: heap size 287 MB, throughput 0.668112
Reading from 13061: heap size 288 MB, throughput 0.580079
Reading from 13061: heap size 288 MB, throughput 0.694162
Reading from 13061: heap size 288 MB, throughput 0.618306
Reading from 13061: heap size 291 MB, throughput 0.646574
Reading from 13062: heap size 308 MB, throughput 0.995132
Reading from 13061: heap size 297 MB, throughput 0.627469
Reading from 13061: heap size 298 MB, throughput 0.527846
Reading from 13061: heap size 310 MB, throughput 0.487775
Reading from 13061: heap size 316 MB, throughput 0.418488
Reading from 13062: heap size 312 MB, throughput 0.996583
Reading from 13061: heap size 326 MB, throughput 0.107401
Reading from 13061: heap size 374 MB, throughput 0.41386
Reading from 13061: heap size 380 MB, throughput 0.54073
Equal recommendation: 3132 MB each
Reading from 13062: heap size 328 MB, throughput 0.989695
Reading from 13061: heap size 379 MB, throughput 0.0822045
Reading from 13061: heap size 367 MB, throughput 0.433999
Reading from 13061: heap size 421 MB, throughput 0.558376
Reading from 13061: heap size 364 MB, throughput 0.6054
Reading from 13061: heap size 417 MB, throughput 0.572453
Reading from 13061: heap size 421 MB, throughput 0.655766
Reading from 13061: heap size 422 MB, throughput 0.637522
Reading from 13062: heap size 333 MB, throughput 0.986905
Reading from 13061: heap size 426 MB, throughput 0.512677
Reading from 13061: heap size 428 MB, throughput 0.580125
Reading from 13061: heap size 436 MB, throughput 0.0970705
Reading from 13061: heap size 495 MB, throughput 0.4376
Reading from 13061: heap size 500 MB, throughput 0.512428
Reading from 13062: heap size 357 MB, throughput 0.991615
Reading from 13061: heap size 502 MB, throughput 0.437234
Reading from 13061: heap size 502 MB, throughput 0.524278
Reading from 13061: heap size 506 MB, throughput 0.476639
Reading from 13062: heap size 358 MB, throughput 0.962552
Reading from 13061: heap size 508 MB, throughput 0.0791818
Reading from 13061: heap size 570 MB, throughput 0.398329
Reading from 13062: heap size 374 MB, throughput 0.926247
Reading from 13061: heap size 571 MB, throughput 0.57787
Reading from 13061: heap size 569 MB, throughput 0.583581
Reading from 13061: heap size 571 MB, throughput 0.129737
Reading from 13061: heap size 636 MB, throughput 0.435176
Reading from 13061: heap size 639 MB, throughput 0.623803
Reading from 13062: heap size 380 MB, throughput 0.997662
Reading from 13061: heap size 640 MB, throughput 0.630659
Reading from 13061: heap size 643 MB, throughput 0.560906
Reading from 13062: heap size 404 MB, throughput 0.985385
Reading from 13061: heap size 646 MB, throughput 0.866831
Reading from 13061: heap size 656 MB, throughput 0.836846
Reading from 13062: heap size 405 MB, throughput 0.992288
Reading from 13061: heap size 664 MB, throughput 0.796342
Reading from 13061: heap size 680 MB, throughput 0.401933
Equal recommendation: 3132 MB each
Reading from 13062: heap size 426 MB, throughput 0.965013
Reading from 13061: heap size 692 MB, throughput 0.0830606
Reading from 13061: heap size 768 MB, throughput 0.226918
Reading from 13062: heap size 428 MB, throughput 0.991962
Reading from 13061: heap size 770 MB, throughput 0.348087
Reading from 13061: heap size 693 MB, throughput 0.352212
Reading from 13061: heap size 760 MB, throughput 0.0436278
Reading from 13061: heap size 841 MB, throughput 0.440388
Reading from 13062: heap size 454 MB, throughput 0.986138
Reading from 13061: heap size 844 MB, throughput 0.8737
Reading from 13061: heap size 846 MB, throughput 0.611749
Reading from 13061: heap size 843 MB, throughput 0.554537
Reading from 13061: heap size 846 MB, throughput 0.545702
Reading from 13061: heap size 848 MB, throughput 0.890004
Reading from 13062: heap size 458 MB, throughput 0.982713
Reading from 13061: heap size 849 MB, throughput 0.106329
Reading from 13061: heap size 936 MB, throughput 0.673043
Reading from 13062: heap size 489 MB, throughput 0.985912
Reading from 13061: heap size 936 MB, throughput 0.960018
Reading from 13061: heap size 945 MB, throughput 0.948876
Reading from 13061: heap size 949 MB, throughput 0.941335
Reading from 13061: heap size 940 MB, throughput 0.892111
Reading from 13061: heap size 790 MB, throughput 0.833357
Reading from 13062: heap size 489 MB, throughput 0.992721
Reading from 13061: heap size 925 MB, throughput 0.862195
Reading from 13061: heap size 795 MB, throughput 0.829914
Reading from 13061: heap size 914 MB, throughput 0.834598
Reading from 13061: heap size 800 MB, throughput 0.825436
Reading from 13061: heap size 905 MB, throughput 0.876221
Reading from 13061: heap size 805 MB, throughput 0.858184
Reading from 13061: heap size 897 MB, throughput 0.852767
Reading from 13062: heap size 512 MB, throughput 0.987147
Reading from 13061: heap size 811 MB, throughput 0.8725
Reading from 13061: heap size 891 MB, throughput 0.980471
Equal recommendation: 3132 MB each
Reading from 13062: heap size 517 MB, throughput 0.993131
Reading from 13061: heap size 897 MB, throughput 0.967756
Reading from 13061: heap size 895 MB, throughput 0.849572
Reading from 13061: heap size 899 MB, throughput 0.77328
Reading from 13061: heap size 902 MB, throughput 0.752559
Reading from 13062: heap size 536 MB, throughput 0.970916
Reading from 13061: heap size 902 MB, throughput 0.79761
Reading from 13061: heap size 904 MB, throughput 0.832919
Reading from 13061: heap size 906 MB, throughput 0.816767
Reading from 13061: heap size 906 MB, throughput 0.798524
Reading from 13061: heap size 909 MB, throughput 0.827724
Reading from 13061: heap size 910 MB, throughput 0.913358
Reading from 13062: heap size 540 MB, throughput 0.987022
Reading from 13061: heap size 913 MB, throughput 0.844602
Reading from 13061: heap size 911 MB, throughput 0.904585
Reading from 13061: heap size 915 MB, throughput 0.795306
Reading from 13061: heap size 910 MB, throughput 0.672579
Reading from 13062: heap size 568 MB, throughput 0.985573
Reading from 13061: heap size 920 MB, throughput 0.0819533
Reading from 13061: heap size 1054 MB, throughput 0.529283
Reading from 13061: heap size 1057 MB, throughput 0.618287
Reading from 13061: heap size 1061 MB, throughput 0.673809
Reading from 13062: heap size 570 MB, throughput 0.981435
Reading from 13061: heap size 1063 MB, throughput 0.675511
Reading from 13061: heap size 1074 MB, throughput 0.703095
Reading from 13061: heap size 1075 MB, throughput 0.705581
Reading from 13061: heap size 1092 MB, throughput 0.752729
Reading from 13061: heap size 1092 MB, throughput 0.722046
Reading from 13062: heap size 597 MB, throughput 0.982815
Reading from 13061: heap size 1110 MB, throughput 0.938546
Equal recommendation: 3132 MB each
Reading from 13062: heap size 599 MB, throughput 0.98434
Reading from 13061: heap size 1112 MB, throughput 0.963817
Reading from 13062: heap size 629 MB, throughput 0.986588
Reading from 13062: heap size 631 MB, throughput 0.995848
Reading from 13061: heap size 1132 MB, throughput 0.965125
Reading from 13062: heap size 654 MB, throughput 0.989413
Reading from 13062: heap size 659 MB, throughput 0.986968
Reading from 13061: heap size 1133 MB, throughput 0.979279
Equal recommendation: 3132 MB each
Reading from 13062: heap size 689 MB, throughput 0.991635
Reading from 13061: heap size 1138 MB, throughput 0.974754
Reading from 13062: heap size 689 MB, throughput 0.994748
Reading from 13062: heap size 708 MB, throughput 0.988235
Reading from 13061: heap size 1143 MB, throughput 0.966642
Reading from 13062: heap size 712 MB, throughput 0.988053
Reading from 13061: heap size 1136 MB, throughput 0.972545
Reading from 13062: heap size 740 MB, throughput 0.989685
Equal recommendation: 3132 MB each
Reading from 13062: heap size 741 MB, throughput 0.983836
Reading from 13061: heap size 1143 MB, throughput 0.969866
Reading from 13062: heap size 771 MB, throughput 0.992572
Reading from 13061: heap size 1138 MB, throughput 0.975256
Reading from 13062: heap size 770 MB, throughput 0.993644
Reading from 13061: heap size 1142 MB, throughput 0.967876
Reading from 13062: heap size 793 MB, throughput 0.990491
Equal recommendation: 3132 MB each
Reading from 13062: heap size 797 MB, throughput 0.990698
Reading from 13061: heap size 1146 MB, throughput 0.973834
Reading from 13062: heap size 826 MB, throughput 0.98983
Reading from 13061: heap size 1147 MB, throughput 0.969831
Reading from 13062: heap size 826 MB, throughput 0.991623
Reading from 13062: heap size 856 MB, throughput 0.991379
Reading from 13061: heap size 1152 MB, throughput 0.971321
Equal recommendation: 3132 MB each
Reading from 13062: heap size 857 MB, throughput 0.990778
Reading from 13061: heap size 1154 MB, throughput 0.974664
Reading from 13062: heap size 887 MB, throughput 0.99198
Reading from 13061: heap size 1158 MB, throughput 0.969249
Reading from 13062: heap size 887 MB, throughput 0.991503
Equal recommendation: 3132 MB each
Reading from 13062: heap size 915 MB, throughput 0.986656
Reading from 13061: heap size 1163 MB, throughput 0.969268
Reading from 13062: heap size 916 MB, throughput 0.990753
Reading from 13061: heap size 1167 MB, throughput 0.971235
Reading from 13062: heap size 948 MB, throughput 0.987118
Reading from 13061: heap size 1172 MB, throughput 0.969981
Reading from 13062: heap size 949 MB, throughput 0.990668
Equal recommendation: 3132 MB each
Reading from 13061: heap size 1178 MB, throughput 0.972127
Reading from 13062: heap size 984 MB, throughput 0.993197
Reading from 13061: heap size 1181 MB, throughput 0.96657
Reading from 13062: heap size 985 MB, throughput 0.99232
Reading from 13061: heap size 1187 MB, throughput 0.969769
Reading from 13062: heap size 1017 MB, throughput 0.990279
Equal recommendation: 3132 MB each
Reading from 13061: heap size 1188 MB, throughput 0.967459
Reading from 13062: heap size 1018 MB, throughput 0.989908
Reading from 13062: heap size 1054 MB, throughput 0.992555
Reading from 13061: heap size 1194 MB, throughput 0.969895
Reading from 13062: heap size 1055 MB, throughput 0.992353
Equal recommendation: 3132 MB each
Reading from 13061: heap size 1194 MB, throughput 0.635809
Reading from 13062: heap size 1086 MB, throughput 0.993954
Reading from 13061: heap size 1291 MB, throughput 0.95824
Reading from 13062: heap size 1088 MB, throughput 0.99046
Equal recommendation: 3132 MB each
Reading from 13061: heap size 1291 MB, throughput 0.98224
Reading from 13062: heap size 1122 MB, throughput 0.992594
Reading from 13061: heap size 1301 MB, throughput 0.984027
Reading from 13062: heap size 1122 MB, throughput 0.990773
Reading from 13061: heap size 1303 MB, throughput 0.981992
Reading from 13062: heap size 1158 MB, throughput 0.990696
Equal recommendation: 3132 MB each
Reading from 13062: heap size 1158 MB, throughput 0.996257
Reading from 13061: heap size 1303 MB, throughput 0.982755
Reading from 13062: heap size 1190 MB, throughput 0.993736
Reading from 13061: heap size 1306 MB, throughput 0.981593
Equal recommendation: 3132 MB each
Reading from 13062: heap size 1194 MB, throughput 0.989581
Reading from 13061: heap size 1297 MB, throughput 0.981648
Reading from 13062: heap size 1225 MB, throughput 0.991868
Reading from 13061: heap size 1221 MB, throughput 0.97973
Reading from 13062: heap size 1228 MB, throughput 0.990045
Reading from 13061: heap size 1286 MB, throughput 0.979433
Equal recommendation: 3132 MB each
Reading from 13062: heap size 1266 MB, throughput 0.991794
Reading from 13061: heap size 1293 MB, throughput 0.975722
Reading from 13062: heap size 1269 MB, throughput 0.992526
Reading from 13061: heap size 1293 MB, throughput 0.979825
Equal recommendation: 3132 MB each
Reading from 13062: heap size 1312 MB, throughput 0.992114
Reading from 13061: heap size 1294 MB, throughput 0.97212
Reading from 13062: heap size 1313 MB, throughput 0.990591
Reading from 13061: heap size 1296 MB, throughput 0.973199
Reading from 13062: heap size 1356 MB, throughput 0.990855
Reading from 13061: heap size 1300 MB, throughput 0.971394
Equal recommendation: 3132 MB each
Reading from 13061: heap size 1303 MB, throughput 0.97141
Reading from 13062: heap size 1355 MB, throughput 0.990934
Reading from 13061: heap size 1310 MB, throughput 0.968178
Reading from 13062: heap size 1402 MB, throughput 0.99317
Equal recommendation: 3132 MB each
Reading from 13061: heap size 1317 MB, throughput 0.971561
Reading from 13062: heap size 1403 MB, throughput 0.989133
Reading from 13061: heap size 1323 MB, throughput 0.971883
Reading from 13062: heap size 1444 MB, throughput 0.991603
Equal recommendation: 3132 MB each
Reading from 13062: heap size 1448 MB, throughput 0.991262
Reading from 13062: heap size 1495 MB, throughput 0.990206
Equal recommendation: 3132 MB each
Reading from 13062: heap size 1496 MB, throughput 0.989021
Reading from 13062: heap size 1552 MB, throughput 0.987398
Reading from 13061: heap size 1330 MB, throughput 0.863352
Equal recommendation: 3132 MB each
Reading from 13062: heap size 1551 MB, throughput 0.993317
Reading from 13062: heap size 1602 MB, throughput 0.991703
Reading from 13062: heap size 1607 MB, throughput 0.989093
Equal recommendation: 3132 MB each
Reading from 13061: heap size 1483 MB, throughput 0.987215
Reading from 13062: heap size 1662 MB, throughput 0.991974
Reading from 13061: heap size 1525 MB, throughput 0.983375
Reading from 13061: heap size 1530 MB, throughput 0.838589
Reading from 13061: heap size 1523 MB, throughput 0.735894
Reading from 13061: heap size 1545 MB, throughput 0.681884
Reading from 13062: heap size 1664 MB, throughput 0.9888
Reading from 13061: heap size 1572 MB, throughput 0.731182
Equal recommendation: 3132 MB each
Reading from 13061: heap size 1592 MB, throughput 0.67934
Reading from 13061: heap size 1624 MB, throughput 0.71017
Reading from 13061: heap size 1635 MB, throughput 0.775228
Reading from 13061: heap size 1653 MB, throughput 0.968835
Reading from 13062: heap size 1723 MB, throughput 0.992868
Reading from 13061: heap size 1664 MB, throughput 0.979457
Equal recommendation: 3132 MB each
Reading from 13062: heap size 1723 MB, throughput 0.992293
Reading from 13061: heap size 1658 MB, throughput 0.97975
Client 13062 died
Clients: 1
Reading from 13061: heap size 1672 MB, throughput 0.984305
Recommendation: one client; give it all the memory
Reading from 13061: heap size 1658 MB, throughput 0.98483
Reading from 13061: heap size 1673 MB, throughput 0.983579
Recommendation: one client; give it all the memory
Reading from 13061: heap size 1669 MB, throughput 0.873898
Recommendation: one client; give it all the memory
Reading from 13061: heap size 1599 MB, throughput 0.996573
Reading from 13061: heap size 1604 MB, throughput 0.994925
Recommendation: one client; give it all the memory
Reading from 13061: heap size 1616 MB, throughput 0.994037
Reading from 13061: heap size 1629 MB, throughput 0.992958
Recommendation: one client; give it all the memory
Reading from 13061: heap size 1631 MB, throughput 0.991916
Reading from 13061: heap size 1616 MB, throughput 0.990985
Recommendation: one client; give it all the memory
Reading from 13061: heap size 1427 MB, throughput 0.989676
Recommendation: one client; give it all the memory
Reading from 13061: heap size 1596 MB, throughput 0.989172
Reading from 13061: heap size 1456 MB, throughput 0.987437
Recommendation: one client; give it all the memory
Reading from 13061: heap size 1580 MB, throughput 0.98717
Recommendation: one client; give it all the memory
Reading from 13061: heap size 1591 MB, throughput 0.985983
Reading from 13061: heap size 1589 MB, throughput 0.985708
Recommendation: one client; give it all the memory
Reading from 13061: heap size 1590 MB, throughput 0.983832
Reading from 13061: heap size 1594 MB, throughput 0.983474
Recommendation: one client; give it all the memory
Reading from 13061: heap size 1600 MB, throughput 0.981956
Recommendation: one client; give it all the memory
Reading from 13061: heap size 1605 MB, throughput 0.981453
Reading from 13061: heap size 1615 MB, throughput 0.980762
Recommendation: one client; give it all the memory
Reading from 13061: heap size 1626 MB, throughput 0.981343
Reading from 13061: heap size 1632 MB, throughput 0.980996
Recommendation: one client; give it all the memory
Reading from 13061: heap size 1642 MB, throughput 0.981595
Recommendation: one client; give it all the memory
Reading from 13061: heap size 1645 MB, throughput 0.980487
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 13061: heap size 1656 MB, throughput 0.996968
Reading from 13061: heap size 1530 MB, throughput 0.985856
Reading from 13061: heap size 1646 MB, throughput 0.856603
Reading from 13061: heap size 1655 MB, throughput 0.769676
Reading from 13061: heap size 1682 MB, throughput 0.805544
Recommendation: one client; give it all the memory
Reading from 13061: heap size 1702 MB, throughput 0.794889
Client 13061 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
