{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, DeriveDataTypeable #-}
{-# OPTIONS_GHC -fno-warn-missing-fields -fno-warn-missing-methods #-}

import Control.Applicative
import Economem.ArgHelper
import Economem.Common
import Economem.Experiment
import Economem.Files
import Economem.Records
import Economem.Signals
import qualified Economem.StaggeredStart as S
import System.Console.CmdLib

data Args = Args { inputFile :: FilePath,
                   maxHeap :: Int,
                   minHeapsFile :: FilePath,
                   dontRecommend :: Bool,
                   forceGrowth :: Bool}
          deriving (Typeable, Data, Eq, Show)

instance Attributes Args where
  attributes _ = group "Options" [
    inputFile %> [ Positional 0,
                   Required True,
                   Help "Specifies which combinations of benchmarks to run",
                   ArgHelp "INPUT_FILE" ],
    maxHeap %> [ Positional 1,
                   Required True,
                   Help "Memory budget of the daemon",
                   ArgHelp "MAX_HEAP" ],
    minHeapsFile %> [ Positional 2,
                      Required True,
                      Help "File containing min heaps",
                      ArgHelp "MIN_HEAPS_FILE" ],
    dontRecommend %> [ Long ["dont-recommend"],
                       Default False,
                       Help "Tell the daemon not to make recommendations"],
    forceGrowth %> [ Long ["force-growth"],
                     Default False,
                     Help "Tell the VM to grow the heap whenever possible"]]


instance RecordCommand Args where
  mode_summary _ = "Observe the behaviour of benchmarks over time"

main :: IO ()
main = getArgs >>= executeR Args {} >>= \opts -> do
  (i, m, minHeaps, d, f) <- validate opts
  s <- S.newStaggeredStart i m minHeaps (not d) f Nothing
  h <- makeSignalHandler
  runExperiment s h

  where
    validate :: Args -> IO (Input S.StaggeredStartInput,
                            DataSize,
                            Input BenchmarkThreadsMB,
                            Bool,
                            Bool)
    validate a = do
      i <- fromFile <$> existingAbsoluteFile (inputFile a)
      m <- (`DataSize` MB) <$> positive (fromIntegral (maxHeap a))
      minHeaps <- fromFile <$> existingAbsoluteFile (minHeapsFile a)
      return (i, m, minHeaps, dontRecommend a, forceGrowth a)
