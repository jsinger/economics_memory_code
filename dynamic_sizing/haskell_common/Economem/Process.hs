{-# LANGUAGE LambdaCase #-}

module Economem.Process (ProcessNum(..),
                         ProcSetupAction(..),
                         ProcStartedAction(..),
                         ProcCleanupAction(..),
                         EnvData(..),
                         ExtraEnv(..),
                         ProcessLauncher(..),
                         ArgsModifier(..),
                         modifyArgs,
                         addSetupAction,
                         addStartedAction,
                         addCleanupAction,
                         RunningProcess,
                         start,
                         kill,
                         wait,
                         prepareProcess,
                         cleanupProcess) where

import Control.Applicative
import Control.Exception
import Data.List
import Economem.Common
import System.Directory
import System.Environment
import System.Exit
import System.IO
import System.Process
import System.Process.Internals (ProcessHandle__(..), PHANDLE, withProcessHandle)


data ProcessNum = ProcessNum Integer Integer
instance Pretty ProcessNum where
  pretty pn = prettyInt (getNum pn) (getMax pn)
instance NumAndMax ProcessNum where
  makeNumAndMax = ProcessNum
  getNum (ProcessNum n _) = n
  getMax (ProcessNum _ m) = m



data ProcSetupAction = MkDir FilePath | Command FilePath [String] deriving (Read, Show)
data ProcStartedAction = SavePid FilePath deriving (Read, Show)
data ProcCleanupAction = RmFile FilePath | RmDir FilePath | SaveExitCode FilePath deriving (Read, Show)

data EnvData = PathLike [String] | Simple String deriving (Read, Show)
data ExtraEnv = ExtraEnv String EnvData deriving (Read, Show)


data ProcessLauncher = ProcessLauncher { procCmd :: String,
                                         procArgs :: [String],
                                         procWorkingDir :: FilePath,
                                         procExtraEnvironment :: [ExtraEnv],
                                         procInfile :: Maybe FilePath,
                                         procOutfile :: Maybe FilePath,
                                         procSetupActions :: [ProcSetupAction],
                                         procStartedActions :: [ProcStartedAction],
                                         procCleanupActions :: [ProcCleanupAction] }
                     deriving (Read, Show)

class ArgsModifier a where
  modifyArgsInternal :: a -> (FilePath, [String]) -> IO (FilePath, [String])

modifyArgs :: ArgsModifier a => Maybe a -> (FilePath, [String]) -> IO (FilePath, [String])
modifyArgs (Just a) = modifyArgsInternal a
modifyArgs Nothing = return


addSetupAction :: ProcessLauncher -> ProcSetupAction -> ProcessLauncher
addSetupAction p a = p { procSetupActions = procSetupActions p ++ [a] }

addStartedAction :: ProcessLauncher -> ProcStartedAction -> ProcessLauncher
addStartedAction p a = p { procStartedActions = procStartedActions p ++ [a] }

addCleanupAction :: ProcessLauncher -> ProcCleanupAction -> ProcessLauncher
addCleanupAction p a = p { procCleanupActions = procCleanupActions p ++ [a] }


data RunningProcess = RunningProcess { launcher :: ProcessLauncher,
                                       processHandle :: ProcessHandle }


start :: ProcessLauncher -> IO RunningProcess
start l = bracket
          (do input <- setupHandle procInfile ReadMode
              output <- setupHandle procOutfile WriteMode
              return (input, output))
          (\(input, output) -> do
              cleanupHandle input
              cleanupHandle output)
          (\(input, output) -> do
              e <- setupEnvironment (procExtraEnvironment l)
              (_, _, _, h) <- createProcess CreateProcess { cmdspec = RawCommand (procCmd l) (procArgs l),
                                                            cwd = Just (procWorkingDir l),
                                                            env = Just e,
                                                            std_in = input,
                                                            std_out = output,
                                                            std_err = output,
                                                            close_fds = True,
                                                            create_group = False }
              let r = RunningProcess l h
              afterStart r
              return r)
  where
    setupHandle :: (ProcessLauncher -> Maybe FilePath) -> IOMode -> IO StdStream
    setupHandle f mode = case f l of
      Just path -> UseHandle <$> openFile path mode
      Nothing -> return Inherit

    cleanupHandle :: StdStream -> IO ()
    cleanupHandle (UseHandle h) = hClose h
    cleanupHandle _ = return ()

    afterStart :: RunningProcess -> IO ()
    afterStart p = mapM_ oneAction (procStartedActions (launcher p))
      where
        oneAction :: ProcStartedAction -> IO ()
        oneAction (SavePid path) = withProcessHandle (processHandle p) getPid >>= \case
          Just pid -> writeFile path (show pid)
          Nothing -> fail "Couldn't get PID of child process"

        getPid :: ProcessHandle__ -> IO (ProcessHandle__, Maybe PHANDLE)
        getPid h@(OpenHandle t) = return (h, Just t)
        getPid h@(ClosedHandle _) = return (h, Nothing)


-- Killing a dead process is a no-op, not an error
kill :: RunningProcess -> IO ()
kill p = catch (terminateProcess (processHandle p)) ignore

-- Waiting on a dead process is a no-op, not an error
wait :: RunningProcess -> IO ()
wait p = catch (do _ <- waitForProcess (processHandle p)
                   return ())
         ignore


prepareProcess :: ProcessLauncher -> IO ()
prepareProcess p = mapM_ oneAction (procSetupActions p)
  where
    oneAction :: ProcSetupAction -> IO ()
    oneAction (MkDir path) = createDirectoryIfMissing True path
    oneAction (Command cmd args) = wait =<< start
                                   (ProcessLauncher { procCmd = cmd,
                                                      procArgs = args,
                                                      procWorkingDir = ".",
                                                      procExtraEnvironment = [],
                                                      procInfile = Just devnull,
                                                      procOutfile = Nothing,
                                                      procSetupActions = [],
                                                      procStartedActions = [],
                                                      procCleanupActions = [] })


cleanupProcess :: RunningProcess -> IO ()
cleanupProcess p = mapM_ oneAction (procCleanupActions (launcher p))
  where
    oneAction :: ProcCleanupAction -> IO ()
    oneAction (RmFile path) = removeFile path
    oneAction (RmDir path) = removeDirectoryRecursive path
    oneAction (SaveExitCode path) = withProcessHandle (processHandle p) getExitCode >>= \case
      Just exit -> writeFile path (show exit)
      Nothing -> fail "Couldn't get exit code of child process"

    getExitCode :: ProcessHandle__ -> IO (ProcessHandle__, Maybe Integer)
    getExitCode h@(OpenHandle _) = return (h, Nothing)
    getExitCode h@(ClosedHandle e) =
      return (h, Just (case e of
                        ExitSuccess -> 0
                        ExitFailure i -> fromIntegral i))



setupEnvironment :: [ExtraEnv] -> IO [(String, String)]
setupEnvironment es = do
  e <- getEnvironment
  return (foldr oneVar e es)
  where
    oneVar :: ExtraEnv -> [(String, String)] -> [(String, String)]
    oneVar (ExtraEnv name edata) current =
      let maybeExisting = lookup name current in
      case maybeExisting of
        Nothing -> case edata of
          PathLike ps -> (name, intercalate ":" ps):current
          Simple s -> (name, s):current
        Just val -> let without = filter (\(x, _) -> x /= name) current in
          case edata of
            PathLike ps -> (name, intercalate ":" ps ++ ":" ++ val):without
            Simple s -> (name, s):without

ignore :: IOError -> IO ()
ignore _ = return ()
