#include "SimpleSmoothingThroughput.hpp"

economem::SimpleSmoothingThroughput::SimpleSmoothingThroughput()
    : ThroughputCalculator(),
      m_TotalGCTimer(0),
      m_HeapSizeAtBeginCollectionB(0)
{
    m_InterGCTimer.start();
    m_InterMajorGCTimer.start();
}

void economem::SimpleSmoothingThroughput::gcBegin()
{
    m_HeapSizeAtBeginCollectionB = getHeapSizeB();
    m_GCTimer.start();
}

economem::ThroughputCalculator::Result economem::SimpleSmoothingThroughput::minorGCEnd()
{
    m_GCTimer.stop();
    m_InterGCTimer.stop();

    unsigned long long gcTime = m_GCTimer.microseconds();
    m_TotalGCTimer += gcTime;

    unsigned long long totalTime = m_InterGCTimer.microseconds();
    double throughput = 1.0 - ((double)gcTime/(double)totalTime);

    logReading(m_HeapSizeAtBeginCollectionB, throughput, gcTime, totalTime);
    m_InterGCTimer.start();

    return ThroughputCalculator::Result(m_HeapSizeAtBeginCollectionB / (1024 * 1024), throughput);
}

economem::ThroughputCalculator::Result economem::SimpleSmoothingThroughput::majorGCEnd()
{
    m_GCTimer.stop();
    m_InterGCTimer.stop();
    m_InterMajorGCTimer.stop();

    m_TotalGCTimer += m_GCTimer.microseconds();
    unsigned long long totalTime = m_InterMajorGCTimer.microseconds();
    double throughput = 1.0 - ((double)m_TotalGCTimer/(double)totalTime);

    logReading(m_HeapSizeAtBeginCollectionB, throughput, m_TotalGCTimer, totalTime);
    m_InterGCTimer.start();
    m_InterMajorGCTimer.start();
    m_TotalGCTimer = 0;

    return ThroughputCalculator::Result(m_HeapSizeAtBeginCollectionB / (1024 * 1024), throughput);
}
