economemd
    total memory: 3705 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub11_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 22989: heap size 9 MB, throughput 0.987277
Clients: 1
Client 22989 has a minimum heap size of 12 MB
Reading from 22988: heap size 9 MB, throughput 0.978814
Clients: 2
Client 22988 has a minimum heap size of 1223 MB
Reading from 22989: heap size 9 MB, throughput 0.988378
Reading from 22988: heap size 9 MB, throughput 0.981156
Reading from 22988: heap size 11 MB, throughput 0.972157
Reading from 22989: heap size 11 MB, throughput 0.96511
Reading from 22989: heap size 11 MB, throughput 0.964188
Reading from 22988: heap size 11 MB, throughput 0.986237
Reading from 22989: heap size 15 MB, throughput 0.985092
Reading from 22988: heap size 15 MB, throughput 0.825862
Reading from 22989: heap size 15 MB, throughput 0.983367
Reading from 22988: heap size 18 MB, throughput 0.968654
Reading from 22988: heap size 24 MB, throughput 0.922822
Reading from 22988: heap size 28 MB, throughput 0.953501
Reading from 22989: heap size 24 MB, throughput 0.940893
Reading from 22988: heap size 32 MB, throughput 0.433991
Reading from 22988: heap size 42 MB, throughput 0.895832
Reading from 22988: heap size 47 MB, throughput 0.806943
Reading from 22989: heap size 29 MB, throughput 0.940788
Reading from 22988: heap size 49 MB, throughput 0.201941
Reading from 22988: heap size 68 MB, throughput 0.706083
Reading from 22989: heap size 38 MB, throughput 0.987795
Reading from 22988: heap size 69 MB, throughput 0.792583
Reading from 22989: heap size 45 MB, throughput 0.990784
Reading from 22988: heap size 73 MB, throughput 0.162524
Reading from 22989: heap size 47 MB, throughput 0.97424
Reading from 22988: heap size 98 MB, throughput 0.680478
Reading from 22988: heap size 104 MB, throughput 0.726775
Reading from 22989: heap size 47 MB, throughput 0.986326
Reading from 22988: heap size 107 MB, throughput 0.727061
Reading from 22988: heap size 115 MB, throughput 0.833891
Reading from 22989: heap size 54 MB, throughput 0.96257
Reading from 22989: heap size 55 MB, throughput 0.989568
Reading from 22988: heap size 117 MB, throughput 0.145209
Reading from 22989: heap size 64 MB, throughput 0.969638
Reading from 22988: heap size 144 MB, throughput 0.588497
Reading from 22988: heap size 151 MB, throughput 0.70831
Reading from 22989: heap size 65 MB, throughput 0.62604
Reading from 22988: heap size 154 MB, throughput 0.638735
Reading from 22988: heap size 158 MB, throughput 0.62799
Reading from 22989: heap size 84 MB, throughput 0.992351
Reading from 22988: heap size 162 MB, throughput 0.537571
Reading from 22988: heap size 170 MB, throughput 0.519997
Reading from 22989: heap size 84 MB, throughput 0.993103
Reading from 22988: heap size 176 MB, throughput 0.100159
Reading from 22988: heap size 214 MB, throughput 0.564209
Reading from 22989: heap size 93 MB, throughput 0.995712
Reading from 22988: heap size 224 MB, throughput 0.139814
Reading from 22988: heap size 262 MB, throughput 0.56984
Reading from 22988: heap size 260 MB, throughput 0.753302
Reading from 22989: heap size 94 MB, throughput 0.995622
Reading from 22988: heap size 264 MB, throughput 0.635987
Reading from 22988: heap size 265 MB, throughput 0.687171
Reading from 22988: heap size 269 MB, throughput 0.633872
Reading from 22988: heap size 271 MB, throughput 0.600682
Reading from 22988: heap size 278 MB, throughput 0.617383
Reading from 22988: heap size 281 MB, throughput 0.570346
Reading from 22989: heap size 102 MB, throughput 0.992873
Reading from 22988: heap size 291 MB, throughput 0.514261
Reading from 22988: heap size 294 MB, throughput 0.420868
Reading from 22988: heap size 305 MB, throughput 0.488711
Reading from 22989: heap size 103 MB, throughput 0.99512
Reading from 22988: heap size 311 MB, throughput 0.0810306
Reading from 22988: heap size 357 MB, throughput 0.413599
Reading from 22988: heap size 357 MB, throughput 0.503036
Reading from 22989: heap size 110 MB, throughput 0.997407
Reading from 22988: heap size 293 MB, throughput 0.10021
Reading from 22988: heap size 394 MB, throughput 0.513436
Reading from 22988: heap size 319 MB, throughput 0.498969
Reading from 22988: heap size 393 MB, throughput 0.565244
Reading from 22988: heap size 337 MB, throughput 0.555876
Reading from 22988: heap size 385 MB, throughput 0.615551
Reading from 22989: heap size 111 MB, throughput 0.996677
Reading from 22988: heap size 343 MB, throughput 0.0956383
Reading from 22988: heap size 435 MB, throughput 0.531562
Reading from 22989: heap size 117 MB, throughput 0.996381
Reading from 22988: heap size 438 MB, throughput 0.580422
Equal recommendation: 1852 MB each
Reading from 22988: heap size 433 MB, throughput 0.609469
Reading from 22988: heap size 436 MB, throughput 0.638335
Reading from 22988: heap size 439 MB, throughput 0.483146
Reading from 22988: heap size 440 MB, throughput 0.517224
Reading from 22989: heap size 118 MB, throughput 0.991068
Reading from 22988: heap size 444 MB, throughput 0.596151
Reading from 22988: heap size 449 MB, throughput 0.570971
Reading from 22988: heap size 452 MB, throughput 0.492252
Reading from 22988: heap size 459 MB, throughput 0.498731
Reading from 22988: heap size 464 MB, throughput 0.499822
Reading from 22989: heap size 122 MB, throughput 0.997043
Reading from 22989: heap size 122 MB, throughput 0.996205
Reading from 22988: heap size 472 MB, throughput 0.0940867
Reading from 22988: heap size 527 MB, throughput 0.353282
Reading from 22988: heap size 535 MB, throughput 0.469557
Reading from 22988: heap size 537 MB, throughput 0.483286
Reading from 22989: heap size 126 MB, throughput 0.995687
Reading from 22988: heap size 541 MB, throughput 0.0819385
Reading from 22989: heap size 127 MB, throughput 0.996474
Reading from 22988: heap size 611 MB, throughput 0.502126
Reading from 22988: heap size 543 MB, throughput 0.604439
Reading from 22988: heap size 604 MB, throughput 0.627878
Reading from 22988: heap size 607 MB, throughput 0.605409
Reading from 22988: heap size 609 MB, throughput 0.604207
Reading from 22989: heap size 130 MB, throughput 0.996985
Reading from 22988: heap size 613 MB, throughput 0.53998
Reading from 22989: heap size 131 MB, throughput 0.995323
Reading from 22988: heap size 619 MB, throughput 0.0783799
Reading from 22989: heap size 135 MB, throughput 0.997373
Reading from 22988: heap size 689 MB, throughput 0.833398
Reading from 22989: heap size 135 MB, throughput 0.992355
Reading from 22988: heap size 697 MB, throughput 0.862546
Reading from 22988: heap size 697 MB, throughput 0.869098
Reading from 22988: heap size 699 MB, throughput 0.547644
Reading from 22989: heap size 139 MB, throughput 0.998325
Reading from 22988: heap size 708 MB, throughput 0.647361
Reading from 22988: heap size 715 MB, throughput 0.506337
Reading from 22989: heap size 139 MB, throughput 0.997663
Equal recommendation: 1852 MB each
Reading from 22989: heap size 141 MB, throughput 0.996106
Reading from 22988: heap size 716 MB, throughput 0.0313677
Reading from 22988: heap size 783 MB, throughput 0.346994
Reading from 22988: heap size 786 MB, throughput 0.48425
Reading from 22988: heap size 784 MB, throughput 0.875057
Reading from 22989: heap size 142 MB, throughput 0.998271
Reading from 22988: heap size 786 MB, throughput 0.566856
Reading from 22988: heap size 788 MB, throughput 0.628881
Reading from 22988: heap size 789 MB, throughput 0.653156
Reading from 22989: heap size 145 MB, throughput 0.997983
Reading from 22989: heap size 145 MB, throughput 0.997599
Reading from 22988: heap size 795 MB, throughput 0.1205
Reading from 22989: heap size 147 MB, throughput 0.990886
Reading from 22988: heap size 872 MB, throughput 0.616382
Reading from 22988: heap size 879 MB, throughput 0.561213
Reading from 22989: heap size 148 MB, throughput 0.994294
Reading from 22988: heap size 880 MB, throughput 0.797703
Reading from 22988: heap size 886 MB, throughput 0.821391
Reading from 22989: heap size 151 MB, throughput 0.992732
Reading from 22988: heap size 889 MB, throughput 0.93664
Reading from 22988: heap size 887 MB, throughput 0.74112
Reading from 22988: heap size 894 MB, throughput 0.812187
Reading from 22988: heap size 882 MB, throughput 0.817759
Reading from 22989: heap size 152 MB, throughput 0.997004
Reading from 22988: heap size 888 MB, throughput 0.78137
Reading from 22988: heap size 878 MB, throughput 0.778662
Reading from 22989: heap size 157 MB, throughput 0.997817
Reading from 22988: heap size 884 MB, throughput 0.129575
Reading from 22988: heap size 966 MB, throughput 0.579535
Reading from 22989: heap size 157 MB, throughput 0.997161
Reading from 22988: heap size 971 MB, throughput 0.778967
Reading from 22988: heap size 974 MB, throughput 0.795624
Reading from 22989: heap size 161 MB, throughput 0.997712
Reading from 22988: heap size 976 MB, throughput 0.958793
Reading from 22989: heap size 161 MB, throughput 0.997051
Equal recommendation: 1852 MB each
Reading from 22988: heap size 974 MB, throughput 0.928793
Reading from 22988: heap size 978 MB, throughput 0.778297
Reading from 22988: heap size 987 MB, throughput 0.779436
Reading from 22988: heap size 994 MB, throughput 0.745273
Reading from 22989: heap size 165 MB, throughput 0.997849
Reading from 22988: heap size 1004 MB, throughput 0.77776
Reading from 22988: heap size 1007 MB, throughput 0.826843
Reading from 22988: heap size 997 MB, throughput 0.846471
Reading from 22988: heap size 1003 MB, throughput 0.778706
Reading from 22989: heap size 165 MB, throughput 0.997438
Reading from 22988: heap size 997 MB, throughput 0.913247
Reading from 22988: heap size 1002 MB, throughput 0.914926
Reading from 22988: heap size 997 MB, throughput 0.863543
Reading from 22989: heap size 168 MB, throughput 0.997702
Reading from 22988: heap size 1003 MB, throughput 0.753999
Reading from 22988: heap size 996 MB, throughput 0.659869
Reading from 22988: heap size 1011 MB, throughput 0.629401
Reading from 22989: heap size 168 MB, throughput 0.996692
Reading from 22989: heap size 171 MB, throughput 0.996736
Reading from 22988: heap size 1024 MB, throughput 0.0658675
Reading from 22988: heap size 1145 MB, throughput 0.54728
Reading from 22988: heap size 1151 MB, throughput 0.740484
Reading from 22988: heap size 1152 MB, throughput 0.741828
Reading from 22989: heap size 171 MB, throughput 0.997373
Reading from 22988: heap size 1157 MB, throughput 0.749789
Reading from 22988: heap size 1161 MB, throughput 0.691562
Reading from 22988: heap size 1163 MB, throughput 0.744428
Reading from 22989: heap size 173 MB, throughput 0.994474
Reading from 22988: heap size 1168 MB, throughput 0.871997
Reading from 22989: heap size 173 MB, throughput 0.996718
Reading from 22989: heap size 177 MB, throughput 0.996659
Equal recommendation: 1852 MB each
Reading from 22988: heap size 1165 MB, throughput 0.976669
Reading from 22989: heap size 177 MB, throughput 0.997791
Reading from 22989: heap size 181 MB, throughput 0.998647
Reading from 22989: heap size 181 MB, throughput 0.977976
Reading from 22989: heap size 183 MB, throughput 0.990264
Reading from 22988: heap size 1172 MB, throughput 0.968717
Reading from 22989: heap size 184 MB, throughput 0.997016
Reading from 22989: heap size 190 MB, throughput 0.998323
Reading from 22989: heap size 191 MB, throughput 0.997642
Reading from 22988: heap size 1167 MB, throughput 0.977022
Reading from 22989: heap size 196 MB, throughput 0.997624
Reading from 22989: heap size 196 MB, throughput 0.997839
Equal recommendation: 1852 MB each
Reading from 22988: heap size 1174 MB, throughput 0.96755
Reading from 22989: heap size 199 MB, throughput 0.997051
Reading from 22989: heap size 200 MB, throughput 0.996513
Reading from 22989: heap size 204 MB, throughput 0.996429
Reading from 22988: heap size 1179 MB, throughput 0.973193
Reading from 22989: heap size 204 MB, throughput 0.997277
Reading from 22989: heap size 207 MB, throughput 0.998044
Reading from 22989: heap size 208 MB, throughput 0.997202
Reading from 22988: heap size 1181 MB, throughput 0.980545
Reading from 22989: heap size 211 MB, throughput 0.99741
Reading from 22989: heap size 211 MB, throughput 0.997733
Equal recommendation: 1852 MB each
Reading from 22988: heap size 1182 MB, throughput 0.963181
Reading from 22989: heap size 215 MB, throughput 0.996259
Reading from 22989: heap size 215 MB, throughput 0.989539
Reading from 22989: heap size 218 MB, throughput 0.997675
Reading from 22988: heap size 1185 MB, throughput 0.97988
Reading from 22989: heap size 219 MB, throughput 0.996572
Reading from 22989: heap size 224 MB, throughput 0.997393
Reading from 22989: heap size 224 MB, throughput 0.99717
Reading from 22988: heap size 1176 MB, throughput 0.98003
Reading from 22989: heap size 228 MB, throughput 0.998059
Equal recommendation: 1852 MB each
Reading from 22989: heap size 229 MB, throughput 0.997195
Reading from 22988: heap size 1182 MB, throughput 0.979226
Reading from 22989: heap size 233 MB, throughput 0.998141
Reading from 22989: heap size 222 MB, throughput 0.997594
Reading from 22989: heap size 211 MB, throughput 0.998392
Reading from 22988: heap size 1180 MB, throughput 0.97989
Reading from 22989: heap size 203 MB, throughput 0.997833
Reading from 22989: heap size 194 MB, throughput 0.997792
Reading from 22989: heap size 186 MB, throughput 0.996782
Reading from 22989: heap size 178 MB, throughput 0.988871
Reading from 22988: heap size 1182 MB, throughput 0.968814
Reading from 22989: heap size 184 MB, throughput 0.826077
Equal recommendation: 1852 MB each
Reading from 22989: heap size 193 MB, throughput 0.998826
Reading from 22989: heap size 198 MB, throughput 0.998787
Reading from 22988: heap size 1186 MB, throughput 0.977761
Reading from 22989: heap size 203 MB, throughput 0.99883
Reading from 22989: heap size 213 MB, throughput 0.998652
Reading from 22989: heap size 216 MB, throughput 0.998571
Reading from 22988: heap size 1187 MB, throughput 0.979428
Reading from 22989: heap size 218 MB, throughput 0.998904
Reading from 22989: heap size 221 MB, throughput 0.998609
Reading from 22989: heap size 221 MB, throughput 0.997783
Reading from 22988: heap size 1190 MB, throughput 0.977671
Equal recommendation: 1852 MB each
Reading from 22989: heap size 224 MB, throughput 0.998837
Reading from 22989: heap size 224 MB, throughput 0.998709
Reading from 22988: heap size 1193 MB, throughput 0.976597
Reading from 22989: heap size 227 MB, throughput 0.999132
Reading from 22989: heap size 227 MB, throughput 0.99874
Reading from 22989: heap size 230 MB, throughput 0.996662
Reading from 22989: heap size 230 MB, throughput 0.968519
Reading from 22988: heap size 1197 MB, throughput 0.975141
Reading from 22989: heap size 234 MB, throughput 0.998547
Reading from 22989: heap size 235 MB, throughput 0.997964
Equal recommendation: 1852 MB each
Reading from 22988: heap size 1201 MB, throughput 0.977803
Reading from 22989: heap size 243 MB, throughput 0.997652
Reading from 22989: heap size 245 MB, throughput 0.997601
Reading from 22989: heap size 252 MB, throughput 0.998494
Reading from 22989: heap size 254 MB, throughput 0.996733
Reading from 22988: heap size 1205 MB, throughput 0.710786
Reading from 22989: heap size 260 MB, throughput 0.998196
Reading from 22989: heap size 261 MB, throughput 0.997775
Equal recommendation: 1852 MB each
Reading from 22988: heap size 1274 MB, throughput 0.996182
Reading from 22989: heap size 267 MB, throughput 0.998373
Reading from 22989: heap size 267 MB, throughput 0.998112
Reading from 22989: heap size 272 MB, throughput 0.996297
Reading from 22988: heap size 1269 MB, throughput 0.994442
Reading from 22989: heap size 273 MB, throughput 0.993927
Reading from 22989: heap size 279 MB, throughput 0.997877
Reading from 22988: heap size 1279 MB, throughput 0.993119
Reading from 22989: heap size 280 MB, throughput 0.997568
Reading from 22989: heap size 286 MB, throughput 0.998632
Equal recommendation: 1852 MB each
Reading from 22989: heap size 286 MB, throughput 0.997836
Reading from 22988: heap size 1285 MB, throughput 0.990521
Reading from 22989: heap size 292 MB, throughput 0.997892
Reading from 22989: heap size 292 MB, throughput 0.998175
Reading from 22988: heap size 1287 MB, throughput 0.990164
Reading from 22989: heap size 297 MB, throughput 0.995727
Reading from 22989: heap size 297 MB, throughput 0.998103
Reading from 22988: heap size 1281 MB, throughput 0.989307
Equal recommendation: 1852 MB each
Reading from 22989: heap size 303 MB, throughput 0.997504
Reading from 22989: heap size 304 MB, throughput 0.994294
Reading from 22988: heap size 1183 MB, throughput 0.987952
Reading from 22989: heap size 310 MB, throughput 0.997715
Reading from 22989: heap size 310 MB, throughput 0.998002
Reading from 22988: heap size 1269 MB, throughput 0.987442
Reading from 22989: heap size 316 MB, throughput 0.998176
Reading from 22989: heap size 317 MB, throughput 0.997294
Equal recommendation: 1852 MB each
Reading from 22988: heap size 1200 MB, throughput 0.985207
Reading from 22989: heap size 324 MB, throughput 0.998253
Reading from 22989: heap size 324 MB, throughput 0.998053
Reading from 22988: heap size 1262 MB, throughput 0.981928
Reading from 22989: heap size 331 MB, throughput 0.998532
Reading from 22989: heap size 331 MB, throughput 0.998197
Reading from 22989: heap size 336 MB, throughput 0.993965
Reading from 22988: heap size 1266 MB, throughput 0.984096
Equal recommendation: 1852 MB each
Reading from 22989: heap size 336 MB, throughput 0.99816
Reading from 22988: heap size 1267 MB, throughput 0.981279
Reading from 22989: heap size 344 MB, throughput 0.997848
Reading from 22989: heap size 345 MB, throughput 0.997914
Reading from 22988: heap size 1268 MB, throughput 0.980515
Reading from 22989: heap size 352 MB, throughput 0.99773
Equal recommendation: 1852 MB each
Reading from 22989: heap size 352 MB, throughput 0.997701
Reading from 22988: heap size 1270 MB, throughput 0.980336
Reading from 22989: heap size 358 MB, throughput 0.998369
Reading from 22988: heap size 1275 MB, throughput 0.977803
Reading from 22989: heap size 359 MB, throughput 0.998188
Reading from 22989: heap size 365 MB, throughput 0.995477
Reading from 22989: heap size 365 MB, throughput 0.997935
Reading from 22988: heap size 1277 MB, throughput 0.976441
Equal recommendation: 1852 MB each
Reading from 22989: heap size 373 MB, throughput 0.99852
Reading from 22988: heap size 1284 MB, throughput 0.978613
Reading from 22989: heap size 374 MB, throughput 0.998332
Reading from 22989: heap size 380 MB, throughput 0.997755
Reading from 22988: heap size 1290 MB, throughput 0.972006
Reading from 22989: heap size 381 MB, throughput 0.997413
Equal recommendation: 1852 MB each
Reading from 22988: heap size 1294 MB, throughput 0.97793
Reading from 22989: heap size 388 MB, throughput 0.998183
Reading from 22989: heap size 388 MB, throughput 0.998015
Reading from 22989: heap size 394 MB, throughput 0.996088
Reading from 22988: heap size 1299 MB, throughput 0.977138
Reading from 22989: heap size 394 MB, throughput 0.998051
Reading from 22988: heap size 1301 MB, throughput 0.977072
Reading from 22989: heap size 402 MB, throughput 0.998293
Equal recommendation: 1852 MB each
Reading from 22989: heap size 403 MB, throughput 0.998261
Reading from 22988: heap size 1306 MB, throughput 0.977746
Reading from 22989: heap size 410 MB, throughput 0.998433
Reading from 22988: heap size 1306 MB, throughput 0.97937
Reading from 22989: heap size 410 MB, throughput 0.998205
Reading from 22989: heap size 416 MB, throughput 0.99751
Equal recommendation: 1852 MB each
Reading from 22988: heap size 1309 MB, throughput 0.977666
Reading from 22989: heap size 417 MB, throughput 0.994655
Reading from 22989: heap size 423 MB, throughput 0.99861
Reading from 22988: heap size 1310 MB, throughput 0.978691
Reading from 22989: heap size 424 MB, throughput 0.99845
Reading from 22989: heap size 432 MB, throughput 0.953006
Reading from 22988: heap size 1311 MB, throughput 0.975108
Equal recommendation: 1852 MB each
Reading from 22989: heap size 436 MB, throughput 0.999292
Reading from 22989: heap size 444 MB, throughput 0.999435
Reading from 22989: heap size 444 MB, throughput 0.998772
Reading from 22989: heap size 451 MB, throughput 0.997067
Equal recommendation: 1852 MB each
Reading from 22989: heap size 452 MB, throughput 0.998734
Reading from 22989: heap size 460 MB, throughput 0.998752
Reading from 22988: heap size 1312 MB, throughput 0.855576
Reading from 22989: heap size 461 MB, throughput 0.998629
Equal recommendation: 1852 MB each
Reading from 22989: heap size 469 MB, throughput 0.99871
Reading from 22989: heap size 469 MB, throughput 0.998796
Reading from 22989: heap size 476 MB, throughput 0.996353
Reading from 22989: heap size 477 MB, throughput 0.998596
Equal recommendation: 1852 MB each
Reading from 22988: heap size 1426 MB, throughput 0.989907
Reading from 22989: heap size 487 MB, throughput 0.998805
Reading from 22989: heap size 488 MB, throughput 0.998544
Reading from 22989: heap size 495 MB, throughput 0.998441
Reading from 22988: heap size 1363 MB, throughput 0.988565
Reading from 22988: heap size 1445 MB, throughput 0.895835
Reading from 22988: heap size 1454 MB, throughput 0.758507
Reading from 22988: heap size 1434 MB, throughput 0.71013
Reading from 22988: heap size 1466 MB, throughput 0.685124
Reading from 22988: heap size 1488 MB, throughput 0.713659
Reading from 22988: heap size 1499 MB, throughput 0.699384
Reading from 22988: heap size 1525 MB, throughput 0.723586
Reading from 22989: heap size 496 MB, throughput 0.998193
Equal recommendation: 1852 MB each
Reading from 22988: heap size 1531 MB, throughput 0.679883
Reading from 22988: heap size 1560 MB, throughput 0.751615
Reading from 22989: heap size 505 MB, throughput 0.996232
Reading from 22988: heap size 1561 MB, throughput 0.977412
Reading from 22989: heap size 505 MB, throughput 0.997909
Reading from 22989: heap size 518 MB, throughput 0.998472
Reading from 22988: heap size 1571 MB, throughput 0.857717
Equal recommendation: 1852 MB each
Reading from 22989: heap size 518 MB, throughput 0.998207
Reading from 22988: heap size 1474 MB, throughput 0.995607
Reading from 22989: heap size 528 MB, throughput 0.99772
Reading from 22988: heap size 1493 MB, throughput 0.994854
Client 22989 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 22988: heap size 1503 MB, throughput 0.992949
Reading from 22988: heap size 1522 MB, throughput 0.992681
Recommendation: one client; give it all the memory
Reading from 22988: heap size 1526 MB, throughput 0.99094
Reading from 22988: heap size 1519 MB, throughput 0.990222
Recommendation: one client; give it all the memory
Reading from 22988: heap size 1529 MB, throughput 0.989684
Reading from 22988: heap size 1504 MB, throughput 0.988472
Recommendation: one client; give it all the memory
Reading from 22988: heap size 1352 MB, throughput 0.987666
Reading from 22988: heap size 1485 MB, throughput 0.986558
Recommendation: one client; give it all the memory
Reading from 22988: heap size 1380 MB, throughput 0.985586
Reading from 22988: heap size 1487 MB, throughput 0.98507
Recommendation: one client; give it all the memory
Reading from 22988: heap size 1493 MB, throughput 0.98453
Reading from 22988: heap size 1497 MB, throughput 0.982893
Recommendation: one client; give it all the memory
Reading from 22988: heap size 1499 MB, throughput 0.982497
Reading from 22988: heap size 1504 MB, throughput 0.980552
Recommendation: one client; give it all the memory
Reading from 22988: heap size 1510 MB, throughput 0.980953
Reading from 22988: heap size 1519 MB, throughput 0.981333
Recommendation: one client; give it all the memory
Reading from 22988: heap size 1524 MB, throughput 0.980728
Reading from 22988: heap size 1533 MB, throughput 0.979403
Recommendation: one client; give it all the memory
Reading from 22988: heap size 1535 MB, throughput 0.979322
Recommendation: one client; give it all the memory
Reading from 22988: heap size 1545 MB, throughput 0.98137
Reading from 22988: heap size 1546 MB, throughput 0.980398
Recommendation: one client; give it all the memory
Reading from 22988: heap size 1555 MB, throughput 0.980191
Reading from 22988: heap size 1556 MB, throughput 0.979345
Recommendation: one client; give it all the memory
Reading from 22988: heap size 1565 MB, throughput 0.979896
Reading from 22988: heap size 1565 MB, throughput 0.980512
Recommendation: one client; give it all the memory
Reading from 22988: heap size 1573 MB, throughput 0.981554
Recommendation: one client; give it all the memory
Reading from 22988: heap size 1573 MB, throughput 0.980856
Recommendation: one client; give it all the memory
Reading from 22988: heap size 1581 MB, throughput 0.905953
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 22988: heap size 1782 MB, throughput 0.990261
Reading from 22988: heap size 1831 MB, throughput 0.910997
Reading from 22988: heap size 1832 MB, throughput 0.797502
Reading from 22988: heap size 1853 MB, throughput 0.835598
Reading from 22988: heap size 1852 MB, throughput 0.825091
Client 22988 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
TimerThread: finished
ReadingThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
