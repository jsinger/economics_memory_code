	Command being timed: "cgexec -g memory:economem /home/callum/economics_memory_code/dynamic_sizing/hotspot8/openjdk8/jdk8/build/linux-x86_64-normal-server-release/jdk/bin/java -XX:+EconomemSimpleThroughput -XX:-EconomemVengerovThroughput -XX:EconomemMinHeap=12 -Xms10m -Xmx2470m -XX:+DisableExplicitGC -XX:+UseAdaptiveSizePolicyWithSystemGC -jar /home/callum/economics_memory_code/dynamic_sizing/haskell_common/dacapo-9.12-bach.jar --no-pre-iteration-gc --scratch-directory /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub10_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/scratch2 -t 2 -n 15 -s large tomcat"
	User time (seconds): 155.11
	System time (seconds): 30.11
	Percent of CPU this job got: 67%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 4:32.88
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 741492
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 2
	Minor (reclaiming a frame) page faults: 173544
	Voluntary context switches: 2205142
	Involuntary context switches: 153770
	Swaps: 0
	File system inputs: 27360
	File system outputs: 91064
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
