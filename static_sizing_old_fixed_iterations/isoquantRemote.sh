#!/bin/bash
# Measure isoquant points on remote machines
#
# Usage: isoquantRemote.sh hosts_file measurepoints total_mem [runs] [iterations]
#

function usage()
{
    echo "Usage: $(basename "${0}") hosts_file measurepoints total_mem [runs] [iterations]"
    exit 1
}

test -z "${1}" && usage
test ! -f "${1}" && usage
test -z "${2}" && usage
test ! -f "${2}" && usage
test -z "${3}" && usage

HOSTS_FILE="$(readlink -f "${1}")"
MEASUREPOINTS="$(readlink -f "${2}")"
TOTAL="${3}"

B1='pmd'
B2='xalan'

if [ -z "${4}" ]; then
    RUNS='5'
else
    RUNS="${4}"
fi

if [ -z "${5}" ]; then
    ITERATIONS='10'
else
    ITERATIONS="${5}"
fi

MAXHEAP1="$(cut '--delimiter= ' -f 1 "${MEASUREPOINTS}" | sort -nr | head -n 1)"
MAXHEAP2="$(cut '--delimiter= ' -f 2 "${MEASUREPOINTS}" | sort -nr | head -n 1)"

REMOTE_DIR="economics_memory_code/static_sizing_old_fixed_iterations"
REMOTE_OUTDIR="${REMOTE_DIR}/isoquant_$(whoami)@$(hostname)_$(date '+%Y_%m_%d_%H_%M_%S')"

TIMESTAMPS_FILE="$(readlink -f "$(mktemp --tmpdir=.)")"

echo "Remote run started by $(whoami)@$(hostname)" >> "${TIMESTAMPS_FILE}"
echo "Started: $(date '+%Y %m %d, %H:%M:%S')" >> "${TIMESTAMPS_FILE}"

ssh callumc@sibu.dcs.gla.ac.uk "${REMOTE_DIR}/isoquant_remoteSetup.sh" "${B1}" "${B2}" "${TOTAL}" "${ITERATIONS}" "${RUNS}" "${REMOTE_OUTDIR}"
scp "${MEASUREPOINTS}" "callumc@sibu.dcs.gla.ac.uk:${REMOTE_OUTDIR}/input.txt"
scp "${TIMESTAMPS_FILE}" "callumc@sibu.dcs.gla.ac.uk:${REMOTE_OUTDIR}/timestamps.txt"

distribute "${HOSTS_FILE}" "${MEASUREPOINTS}" 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -A callumc@sibu.dcs.gla.ac.uk ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no' "${REMOTE_DIR}/isoquant_remoteHarness.sh" "${B1}" "${B2}" "${MAXHEAP1}" "${MAXHEAP2}" "${RUNS}" "${ITERATIONS}" "${REMOTE_OUTDIR}"

rm "${TIMESTAMPS_FILE}"

echo "Don't forget to collect and process the data once it's finished"
