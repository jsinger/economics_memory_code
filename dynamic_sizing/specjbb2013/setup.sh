#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ISO="${DIR}/SPECjbb2013-1.00.iso"
TARGET_DIR="${DIR}/specjbb2013"

if [ ! -e "${ISO}" ]; then
    echo "Original SPECjbb 2013 ISO not present; cannot continue"
    exit 0
fi

mkdir -p "${TARGET_DIR}"
7z x "${ISO}" "-o${TARGET_DIR}"
