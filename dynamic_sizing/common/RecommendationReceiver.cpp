#include <unistd.h>
#include "RecommendationReceiver.hpp"
#include "Utilities.hpp"

economem::RecommendationReceiver::RecommendationReceiver() : RecvSock(economem::allocClientSocketName(getpid(), economem::TYPE_RECOMMENDATION)) {}
