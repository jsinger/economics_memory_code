#!/usr/bin/env python

import argparse
import argtypes
import csv
import matplotlib.pyplot as plt

class CircularList(object):
    def __init__(self, l):
        super(object, self)
        self.l = l
        self.i = 0

    def __iter__(self):
        return self

    def next(self):
        item = self.l[self.i]
        self.i = (self.i + 1) % len(self.l)
        return item


def plot_timer_comparison(infile, outfile, legend_location):
    def read_data():
        with open(infile) as f:
            data = csv.DictReader(f)
            combinations = []
            for line in data:
                bms = line["BENCHMARKS"]
                heap = int(line["MAX_HEAP"])
                del line["BENCHMARKS"]
                del line["MAX_HEAP"]
                del line["FASTEST"]
                combinations.append((bms, heap, line))
            combinations.sort()
        return combinations

    def remove_unused_columns(data):
        used_columns = []
        for combination in data:
            for key in combination[2].keys():
                if combination[2][key] and key not in used_columns:
                    used_columns.append(key)

        new_data = []
        for (bms, heap, results) in data:
            d = []
            for key in used_columns:
                if key in results:
                    d.append((float(results[key].split()[0]), float(results[key].split()[2])))
                else:
                    d.append((0.0, 0.0))
            new_data.append((bms, heap, d))

        styles = CircularList([("r", "/"), ("g", "\\"), ("b", "+"), ("r", "\\")])
        return (new_data, zip(used_columns, styles))

    (data, columns) = remove_unused_columns(read_data())

    ax = plt.subplot(111)

    base_inc = 2
    base_pos = base_inc
    for combination in data:
        i = 0
        for (height, (name, (colour, style))) in zip(combination[2], columns):
            ax.bar(base_pos + i, height[0], 1, yerr=height[1], color="w", ecolor="k", edgecolor=colour, hatch=style)
            i += 1
        ax.text(base_pos + len(columns) / 2.0, -80, "%d MB" % combination[1], ha="center", va="center", size=10)
        ax.text(base_pos + len(columns) / 2.0, -40, "\n".join(combination[0].split("|")), ha="center", va="center", size=4)

        base_pos += len(columns) + base_inc

    ax.set_ylabel("Average runtime (s)")
    ax.set_title("Memory partition method comparison (smaller is better)")
    ax.xaxis.set_tick_params(size=0)
    ax.set_xticklabels([])
    ax.legend(map(lambda c: c[0], columns), loc=int(legend_location))

    plt.savefig(outfile)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot the results from a timer_comparison_many run")
    parser.add_argument("inFile", type=argtypes.existing_path, help="File containing results to plot")
    parser.add_argument("outFile", help="File to save graph in")
    parser.add_argument("legendLocation", nargs="?", type=int, choices=[1,2,3,4], default=1, help="Corner of the graph in which to display the legend")

    args = parser.parse_args()
    plot_timer_comparison(args.inFile, args.outFile, args.legendLocation)
