import economem.common

CREATION = "CREATION"
READING = "READING"
MINHEAP = "MINHEAP"
FUNCTION = "FUNCTION"
RECOMMENDATION = "RECOMMEND"
DEATH = "DEATH"
TOTAL_MEM = "TOTAL_MEM"

class Event(object):
    def __init__(self, timestamp, pid, desc):
        super(Event, self).__init__()
        self.timestamp = int(timestamp)
        self.pid = int(pid)
        self.desc = desc

    def __str__(self):
        return "%d %u %s" % (self.timestamp, self.pid, self.desc)

class Creation(Event):
    def __init__(self, timestamp, pid):
        super(Creation, self).__init__(timestamp, pid, CREATION)

    def __str__(self):
        return super(Creation, self).__str__()

class Reading(Event):
    def __init__(self, timestamp, pid, heap_size_MB, throughput):
        super(Reading, self).__init__(timestamp, pid, READING)
        self.heap_size_MB = int(heap_size_MB)
        self.throughput = float(throughput)

    def __str__(self):
        return super(Reading, self).__str__() + (" %u %f" % (self.heap_size_MB, self.throughput))

class MinHeap(Event):
    def __init__(self, timestamp, pid, heap_size_MB):
        super(MinHeap, self).__init__(timestamp, pid, MINHEAP)
        self.heap_size_MB = int(heap_size_MB)

    def __str__(self):
        return super(MinHeap, self).__str__() + (" %u" % self.heap_size_MB)

class Function(Event):
    def __init__(self, timestamp, pid, cache_name, func_str):
        super(Function, self).__init__(timestamp, pid, FUNCTION)
        self.cache_name = cache_name
        self.func_str = func_str

    def __str__(self):
        return super(Function, self).__str__() + (" %s %s" % (self.cache_name, self.func_str))

class Recommendation(Event):
    def __init__(self, timestamp, pid, heap_size_MB):
        super(Recommendation, self).__init__(timestamp, pid, RECOMMENDATION)
        self.heap_size_MB = int(heap_size_MB)

    def __str__(self):
        return super(Recommendation, self).__str__() + (" %u" % self.heap_size_MB)

class Death(Event):
    def __init__(self, timestamp, pid, total_readings):
        super(Death, self).__init__(timestamp, pid, DEATH)
        self.total_readings = int(total_readings)

    def __str__(self):
        return super(Death, self).__str__() + (" %u" % self.total_readings)

class TotalMem(Event):
    def __init__(self, timestamp, total_mem_MB):
        super(TotalMem, self).__init__(timestamp, 0, TOTAL_MEM)
        self.total_mem_MB = int(total_mem_MB)

    def __str__(self):
        return super(TotalMem, self).__str__() + (" %u" % self.total_mem_MB)

def event_from_str(s):
    s = s.strip("\n").split(" ")
    timestamp = s[0]
    pid = s[1]
    event = s[2]

    if event == CREATION:
        return Creation(timestamp, pid)
    elif event == READING:
        return Reading(timestamp, pid, s[3], s[4])
    elif event == MINHEAP:
        return MinHeap(timestamp, pid, s[3])
    elif event == FUNCTION:
        return Function(timestamp, pid, s[3], " ".join(s[4:]))
    elif event == RECOMMENDATION:
        return Recommendation(timestamp, pid, s[3])
    elif event == DEATH:
        return Death(timestamp, pid, s[3])
    elif event == TOTAL_MEM:
        return TotalMem(timestamp, s[3])
    else:
        raise economem.common.EconomemException("Unknown log line: '%s'" % " ".join(s))
