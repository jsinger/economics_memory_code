#ifndef HOTSPOTSIMPLETHROUGHPUT_HPP
#define HOTSPOTSIMPLETHROUGHPUT_HPP

#include <SimpleThroughput.hpp>

class HotspotSimpleThroughput : public economem::SimpleThroughput
{
public:
    HotspotSimpleThroughput();

private:
    virtual unsigned long long getHeapSizeB();
    virtual void logReading(unsigned long long heapSizeB, double throughput, unsigned long long gcTime, unsigned long long interGCTime);
};

#endif
