economemd
    total memory: 6115 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub14_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 11905: heap size 9 MB, throughput 0.989499
Clients: 1
Client 11905 has a minimum heap size of 12 MB
Reading from 11904: heap size 9 MB, throughput 0.990703
Clients: 2
Client 11904 has a minimum heap size of 1211 MB
Reading from 11904: heap size 9 MB, throughput 0.977247
Reading from 11905: heap size 9 MB, throughput 0.98265
Reading from 11905: heap size 9 MB, throughput 0.965105
Reading from 11904: heap size 9 MB, throughput 0.971329
Reading from 11904: heap size 9 MB, throughput 0.96162
Reading from 11905: heap size 9 MB, throughput 0.978937
Reading from 11904: heap size 11 MB, throughput 0.971615
Reading from 11905: heap size 11 MB, throughput 0.979513
Reading from 11904: heap size 11 MB, throughput 0.936459
Reading from 11905: heap size 11 MB, throughput 0.987873
Reading from 11904: heap size 17 MB, throughput 0.95156
Reading from 11905: heap size 16 MB, throughput 0.983749
Reading from 11904: heap size 17 MB, throughput 0.809514
Reading from 11904: heap size 30 MB, throughput 0.893177
Reading from 11905: heap size 16 MB, throughput 0.976233
Reading from 11904: heap size 31 MB, throughput 0.939323
Reading from 11904: heap size 36 MB, throughput 0.438803
Reading from 11905: heap size 23 MB, throughput 0.98285
Reading from 11904: heap size 48 MB, throughput 0.883394
Reading from 11904: heap size 49 MB, throughput 0.909922
Reading from 11905: heap size 24 MB, throughput 0.734472
Reading from 11904: heap size 53 MB, throughput 0.236383
Reading from 11904: heap size 75 MB, throughput 0.773252
Reading from 11904: heap size 76 MB, throughput 0.332
Reading from 11905: heap size 39 MB, throughput 0.993107
Reading from 11904: heap size 105 MB, throughput 0.779878
Reading from 11904: heap size 105 MB, throughput 0.922049
Reading from 11905: heap size 40 MB, throughput 0.9862
Reading from 11904: heap size 106 MB, throughput 0.76946
Reading from 11905: heap size 45 MB, throughput 0.984986
Reading from 11904: heap size 109 MB, throughput 0.829837
Reading from 11905: heap size 45 MB, throughput 0.985609
Reading from 11905: heap size 50 MB, throughput 0.965365
Reading from 11904: heap size 111 MB, throughput 0.138611
Reading from 11905: heap size 50 MB, throughput 0.987197
Reading from 11904: heap size 147 MB, throughput 0.650885
Reading from 11905: heap size 56 MB, throughput 0.979668
Reading from 11904: heap size 151 MB, throughput 0.87774
Reading from 11905: heap size 57 MB, throughput 0.967716
Reading from 11904: heap size 153 MB, throughput 0.808165
Reading from 11904: heap size 161 MB, throughput 0.778552
Reading from 11905: heap size 62 MB, throughput 0.970637
Reading from 11905: heap size 62 MB, throughput 0.964318
Reading from 11904: heap size 163 MB, throughput 0.136342
Reading from 11905: heap size 69 MB, throughput 0.986717
Reading from 11904: heap size 208 MB, throughput 0.711047
Reading from 11904: heap size 210 MB, throughput 0.644776
Reading from 11904: heap size 213 MB, throughput 0.768079
Reading from 11904: heap size 218 MB, throughput 0.709723
Reading from 11904: heap size 227 MB, throughput 0.712897
Reading from 11905: heap size 69 MB, throughput 0.993507
Reading from 11904: heap size 232 MB, throughput 0.700284
Reading from 11904: heap size 236 MB, throughput 0.592616
Reading from 11904: heap size 245 MB, throughput 0.623628
Reading from 11905: heap size 75 MB, throughput 0.994406
Reading from 11904: heap size 251 MB, throughput 0.0979843
Reading from 11905: heap size 76 MB, throughput 0.968396
Reading from 11904: heap size 301 MB, throughput 0.529484
Reading from 11904: heap size 308 MB, throughput 0.658583
Reading from 11905: heap size 82 MB, throughput 0.993797
Reading from 11904: heap size 310 MB, throughput 0.137634
Reading from 11904: heap size 360 MB, throughput 0.480921
Reading from 11904: heap size 361 MB, throughput 0.68993
Reading from 11905: heap size 82 MB, throughput 0.993593
Reading from 11904: heap size 363 MB, throughput 0.576156
Reading from 11904: heap size 363 MB, throughput 0.595042
Reading from 11904: heap size 365 MB, throughput 0.658054
Reading from 11904: heap size 372 MB, throughput 0.611816
Reading from 11905: heap size 86 MB, throughput 0.994312
Reading from 11904: heap size 375 MB, throughput 0.58891
Reading from 11904: heap size 386 MB, throughput 0.6147
Reading from 11904: heap size 393 MB, throughput 0.481002
Reading from 11905: heap size 87 MB, throughput 0.991691
Reading from 11904: heap size 400 MB, throughput 0.106483
Reading from 11905: heap size 89 MB, throughput 0.994018
Reading from 11904: heap size 455 MB, throughput 0.427758
Reading from 11904: heap size 461 MB, throughput 0.533312
Reading from 11904: heap size 463 MB, throughput 0.553585
Reading from 11904: heap size 464 MB, throughput 0.525921
Reading from 11905: heap size 90 MB, throughput 0.994144
Equal recommendation: 3057 MB each
Reading from 11905: heap size 93 MB, throughput 0.992039
Reading from 11904: heap size 469 MB, throughput 0.10791
Reading from 11904: heap size 527 MB, throughput 0.375692
Reading from 11905: heap size 94 MB, throughput 0.994739
Reading from 11904: heap size 516 MB, throughput 0.550596
Reading from 11904: heap size 522 MB, throughput 0.580015
Reading from 11904: heap size 518 MB, throughput 0.627263
Reading from 11905: heap size 97 MB, throughput 0.996172
Reading from 11905: heap size 97 MB, throughput 0.990484
Reading from 11904: heap size 521 MB, throughput 0.115311
Reading from 11904: heap size 588 MB, throughput 0.452315
Reading from 11904: heap size 588 MB, throughput 0.56341
Reading from 11905: heap size 100 MB, throughput 0.996493
Reading from 11904: heap size 587 MB, throughput 0.622062
Reading from 11904: heap size 591 MB, throughput 0.616088
Reading from 11904: heap size 594 MB, throughput 0.560096
Reading from 11905: heap size 100 MB, throughput 0.994847
Reading from 11904: heap size 606 MB, throughput 0.500775
Reading from 11904: heap size 613 MB, throughput 0.548005
Reading from 11904: heap size 626 MB, throughput 0.556644
Reading from 11905: heap size 102 MB, throughput 0.995651
Reading from 11904: heap size 633 MB, throughput 0.728527
Reading from 11905: heap size 102 MB, throughput 0.995311
Reading from 11904: heap size 643 MB, throughput 0.831294
Reading from 11905: heap size 105 MB, throughput 0.99681
Reading from 11905: heap size 105 MB, throughput 0.996687
Reading from 11904: heap size 646 MB, throughput 0.226087
Reading from 11905: heap size 107 MB, throughput 0.997354
Reading from 11904: heap size 722 MB, throughput 0.749835
Reading from 11904: heap size 733 MB, throughput 0.794206
Reading from 11905: heap size 107 MB, throughput 0.996922
Reading from 11904: heap size 738 MB, throughput 0.429046
Reading from 11904: heap size 742 MB, throughput 0.390637
Reading from 11904: heap size 743 MB, throughput 0.63335
Reading from 11905: heap size 107 MB, throughput 0.996644
Reading from 11905: heap size 108 MB, throughput 0.997146
Reading from 11905: heap size 109 MB, throughput 0.997892
Equal recommendation: 3057 MB each
Reading from 11904: heap size 729 MB, throughput 0.0212698
Reading from 11905: heap size 109 MB, throughput 0.997857
Reading from 11904: heap size 710 MB, throughput 0.275364
Reading from 11904: heap size 803 MB, throughput 0.439698
Reading from 11904: heap size 806 MB, throughput 0.409467
Reading from 11904: heap size 806 MB, throughput 0.476792
Reading from 11905: heap size 110 MB, throughput 0.998473
Reading from 11905: heap size 110 MB, throughput 0.997827
Reading from 11905: heap size 111 MB, throughput 0.997281
Reading from 11904: heap size 807 MB, throughput 0.194617
Reading from 11904: heap size 897 MB, throughput 0.464101
Reading from 11905: heap size 111 MB, throughput 0.996013
Reading from 11904: heap size 899 MB, throughput 0.630512
Reading from 11904: heap size 904 MB, throughput 0.594216
Reading from 11904: heap size 904 MB, throughput 0.796699
Reading from 11905: heap size 113 MB, throughput 0.865352
Reading from 11904: heap size 909 MB, throughput 0.819316
Reading from 11905: heap size 117 MB, throughput 0.981921
Reading from 11904: heap size 909 MB, throughput 0.917443
Reading from 11904: heap size 912 MB, throughput 0.716124
Reading from 11905: heap size 119 MB, throughput 0.993121
Reading from 11904: heap size 914 MB, throughput 0.905276
Reading from 11904: heap size 905 MB, throughput 0.872451
Reading from 11905: heap size 119 MB, throughput 0.995426
Reading from 11904: heap size 726 MB, throughput 0.923528
Reading from 11904: heap size 884 MB, throughput 0.779668
Reading from 11904: heap size 776 MB, throughput 0.826956
Reading from 11905: heap size 124 MB, throughput 0.997589
Reading from 11904: heap size 870 MB, throughput 0.814772
Reading from 11904: heap size 781 MB, throughput 0.774409
Reading from 11905: heap size 124 MB, throughput 0.99679
Reading from 11905: heap size 128 MB, throughput 0.995589
Reading from 11904: heap size 859 MB, throughput 0.122255
Reading from 11904: heap size 962 MB, throughput 0.628019
Reading from 11904: heap size 958 MB, throughput 0.723494
Reading from 11905: heap size 128 MB, throughput 0.997525
Reading from 11904: heap size 960 MB, throughput 0.750267
Reading from 11904: heap size 960 MB, throughput 0.810665
Reading from 11904: heap size 962 MB, throughput 0.78252
Reading from 11905: heap size 131 MB, throughput 0.99801
Reading from 11904: heap size 958 MB, throughput 0.899718
Reading from 11905: heap size 131 MB, throughput 0.997117
Equal recommendation: 3057 MB each
Reading from 11905: heap size 134 MB, throughput 0.997582
Reading from 11904: heap size 962 MB, throughput 0.971427
Reading from 11904: heap size 954 MB, throughput 0.802869
Reading from 11905: heap size 134 MB, throughput 0.997415
Reading from 11904: heap size 964 MB, throughput 0.760986
Reading from 11904: heap size 972 MB, throughput 0.658023
Reading from 11904: heap size 981 MB, throughput 0.772003
Reading from 11904: heap size 969 MB, throughput 0.814767
Reading from 11905: heap size 136 MB, throughput 0.99759
Reading from 11904: heap size 975 MB, throughput 0.802471
Reading from 11904: heap size 966 MB, throughput 0.824204
Reading from 11904: heap size 972 MB, throughput 0.82163
Reading from 11904: heap size 967 MB, throughput 0.835662
Reading from 11905: heap size 137 MB, throughput 0.996937
Reading from 11904: heap size 971 MB, throughput 0.870556
Reading from 11904: heap size 971 MB, throughput 0.855142
Reading from 11904: heap size 973 MB, throughput 0.900551
Reading from 11905: heap size 138 MB, throughput 0.997654
Reading from 11904: heap size 974 MB, throughput 0.759968
Reading from 11904: heap size 976 MB, throughput 0.664264
Reading from 11905: heap size 139 MB, throughput 0.997498
Reading from 11905: heap size 141 MB, throughput 0.997
Reading from 11904: heap size 980 MB, throughput 0.0672124
Reading from 11904: heap size 1109 MB, throughput 0.524648
Reading from 11904: heap size 1117 MB, throughput 0.777608
Reading from 11905: heap size 141 MB, throughput 0.996074
Reading from 11904: heap size 1119 MB, throughput 0.700678
Reading from 11904: heap size 1129 MB, throughput 0.74094
Reading from 11904: heap size 1130 MB, throughput 0.738227
Reading from 11904: heap size 1133 MB, throughput 0.767936
Reading from 11904: heap size 1137 MB, throughput 0.727818
Reading from 11905: heap size 144 MB, throughput 0.998332
Reading from 11904: heap size 1139 MB, throughput 0.683434
Reading from 11904: heap size 1143 MB, throughput 0.764066
Reading from 11905: heap size 144 MB, throughput 0.997621
Reading from 11905: heap size 146 MB, throughput 0.997762
Reading from 11904: heap size 1141 MB, throughput 0.969143
Equal recommendation: 3057 MB each
Reading from 11905: heap size 146 MB, throughput 0.998192
Reading from 11905: heap size 149 MB, throughput 0.998652
Reading from 11904: heap size 1148 MB, throughput 0.966256
Reading from 11905: heap size 149 MB, throughput 0.996047
Reading from 11905: heap size 151 MB, throughput 0.991912
Reading from 11905: heap size 151 MB, throughput 0.990675
Reading from 11904: heap size 1147 MB, throughput 0.965167
Reading from 11905: heap size 155 MB, throughput 0.99697
Reading from 11905: heap size 156 MB, throughput 0.997116
Reading from 11904: heap size 1152 MB, throughput 0.949648
Reading from 11905: heap size 160 MB, throughput 0.997795
Reading from 11905: heap size 160 MB, throughput 0.997341
Reading from 11904: heap size 1157 MB, throughput 0.965915
Reading from 11905: heap size 163 MB, throughput 0.99771
Reading from 11905: heap size 164 MB, throughput 0.996448
Reading from 11904: heap size 1160 MB, throughput 0.959994
Reading from 11905: heap size 167 MB, throughput 0.997729
Equal recommendation: 3057 MB each
Reading from 11904: heap size 1156 MB, throughput 0.963374
Reading from 11905: heap size 167 MB, throughput 0.997486
Reading from 11905: heap size 171 MB, throughput 0.997574
Reading from 11904: heap size 1161 MB, throughput 0.965531
Reading from 11905: heap size 171 MB, throughput 0.997217
Reading from 11905: heap size 174 MB, throughput 0.997666
Reading from 11904: heap size 1164 MB, throughput 0.958436
Reading from 11905: heap size 174 MB, throughput 0.997434
Reading from 11905: heap size 176 MB, throughput 0.997516
Reading from 11904: heap size 1166 MB, throughput 0.962723
Reading from 11905: heap size 176 MB, throughput 0.997165
Reading from 11905: heap size 179 MB, throughput 0.998298
Reading from 11904: heap size 1172 MB, throughput 0.964242
Reading from 11905: heap size 179 MB, throughput 0.998454
Reading from 11905: heap size 181 MB, throughput 0.995024
Equal recommendation: 3057 MB each
Reading from 11905: heap size 182 MB, throughput 0.990823
Reading from 11904: heap size 1174 MB, throughput 0.963598
Reading from 11905: heap size 185 MB, throughput 0.997615
Reading from 11905: heap size 186 MB, throughput 0.997745
Reading from 11904: heap size 1179 MB, throughput 0.958306
Reading from 11905: heap size 190 MB, throughput 0.997682
Reading from 11904: heap size 1183 MB, throughput 0.962684
Reading from 11905: heap size 190 MB, throughput 0.997598
Reading from 11905: heap size 193 MB, throughput 0.997978
Reading from 11904: heap size 1189 MB, throughput 0.963797
Reading from 11905: heap size 194 MB, throughput 0.997582
Reading from 11905: heap size 197 MB, throughput 0.998002
Reading from 11904: heap size 1193 MB, throughput 0.960497
Reading from 11905: heap size 197 MB, throughput 0.99763
Equal recommendation: 3057 MB each
Reading from 11904: heap size 1199 MB, throughput 0.96075
Reading from 11905: heap size 200 MB, throughput 0.995952
Reading from 11905: heap size 200 MB, throughput 0.997411
Reading from 11904: heap size 1204 MB, throughput 0.960526
Reading from 11905: heap size 204 MB, throughput 0.997853
Reading from 11905: heap size 204 MB, throughput 0.997371
Reading from 11904: heap size 1210 MB, throughput 0.961991
Reading from 11905: heap size 208 MB, throughput 0.99785
Reading from 11905: heap size 208 MB, throughput 0.996618
Reading from 11904: heap size 1213 MB, throughput 0.940388
Reading from 11905: heap size 211 MB, throughput 0.99155
Reading from 11905: heap size 211 MB, throughput 0.997043
Reading from 11904: heap size 1219 MB, throughput 0.962875
Reading from 11905: heap size 217 MB, throughput 0.998176
Equal recommendation: 3057 MB each
Reading from 11905: heap size 217 MB, throughput 0.997827
Reading from 11905: heap size 221 MB, throughput 0.997897
Reading from 11904: heap size 1221 MB, throughput 0.569305
Reading from 11905: heap size 221 MB, throughput 0.997823
Reading from 11905: heap size 225 MB, throughput 0.998264
Reading from 11904: heap size 1288 MB, throughput 0.992567
Reading from 11905: heap size 225 MB, throughput 0.997664
Reading from 11904: heap size 1289 MB, throughput 0.990205
Reading from 11905: heap size 228 MB, throughput 0.998132
Reading from 11905: heap size 229 MB, throughput 0.997372
Reading from 11904: heap size 1299 MB, throughput 0.986843
Equal recommendation: 3057 MB each
Reading from 11905: heap size 232 MB, throughput 0.998059
Reading from 11904: heap size 1304 MB, throughput 0.982286
Reading from 11905: heap size 232 MB, throughput 0.997877
Reading from 11905: heap size 235 MB, throughput 0.997723
Reading from 11905: heap size 235 MB, throughput 0.991517
Reading from 11904: heap size 1306 MB, throughput 0.982169
Reading from 11905: heap size 238 MB, throughput 0.997123
Reading from 11905: heap size 239 MB, throughput 0.997998
Reading from 11904: heap size 1307 MB, throughput 0.979901
Reading from 11905: heap size 243 MB, throughput 0.998319
Reading from 11904: heap size 1296 MB, throughput 0.979233
Reading from 11905: heap size 244 MB, throughput 0.998065
Equal recommendation: 3057 MB each
Reading from 11905: heap size 248 MB, throughput 0.998467
Reading from 11904: heap size 1215 MB, throughput 0.977249
Reading from 11905: heap size 248 MB, throughput 0.998003
Reading from 11904: heap size 1286 MB, throughput 0.975087
Reading from 11905: heap size 252 MB, throughput 0.998296
Reading from 11905: heap size 252 MB, throughput 0.997913
Reading from 11904: heap size 1292 MB, throughput 0.973287
Reading from 11905: heap size 255 MB, throughput 0.998297
Reading from 11904: heap size 1293 MB, throughput 0.97299
Reading from 11905: heap size 255 MB, throughput 0.998084
Equal recommendation: 3057 MB each
Reading from 11905: heap size 259 MB, throughput 0.99702
Reading from 11905: heap size 259 MB, throughput 0.843294
Reading from 11904: heap size 1293 MB, throughput 0.972241
Reading from 11905: heap size 269 MB, throughput 0.999171
Reading from 11904: heap size 1296 MB, throughput 0.96942
Reading from 11905: heap size 270 MB, throughput 0.999086
Reading from 11905: heap size 276 MB, throughput 0.999147
Reading from 11904: heap size 1300 MB, throughput 0.966942
Reading from 11905: heap size 277 MB, throughput 0.999046
Reading from 11904: heap size 1304 MB, throughput 0.964969
Reading from 11905: heap size 281 MB, throughput 0.999196
Equal recommendation: 3057 MB each
Reading from 11905: heap size 282 MB, throughput 0.998809
Reading from 11904: heap size 1311 MB, throughput 0.958463
Reading from 11905: heap size 286 MB, throughput 0.999179
Reading from 11904: heap size 1319 MB, throughput 0.959562
Reading from 11905: heap size 286 MB, throughput 0.999074
Reading from 11904: heap size 1325 MB, throughput 0.962085
Reading from 11905: heap size 289 MB, throughput 0.999049
Reading from 11905: heap size 290 MB, throughput 0.996082
Reading from 11905: heap size 293 MB, throughput 0.996949
Reading from 11904: heap size 1333 MB, throughput 0.963792
Reading from 11905: heap size 293 MB, throughput 0.998457
Equal recommendation: 3057 MB each
Reading from 11904: heap size 1336 MB, throughput 0.964016
Reading from 11905: heap size 300 MB, throughput 0.998625
Reading from 11905: heap size 300 MB, throughput 0.998212
Reading from 11904: heap size 1344 MB, throughput 0.964912
Reading from 11905: heap size 305 MB, throughput 0.998532
Reading from 11904: heap size 1346 MB, throughput 0.963518
Reading from 11905: heap size 305 MB, throughput 0.998251
Reading from 11905: heap size 309 MB, throughput 0.998075
Reading from 11904: heap size 1353 MB, throughput 0.958557
Equal recommendation: 3057 MB each
Reading from 11905: heap size 310 MB, throughput 0.998195
Reading from 11905: heap size 315 MB, throughput 0.998469
Reading from 11905: heap size 315 MB, throughput 0.989341
Reading from 11905: heap size 320 MB, throughput 0.997858
Reading from 11905: heap size 321 MB, throughput 0.998208
Reading from 11905: heap size 329 MB, throughput 0.99823
Equal recommendation: 3057 MB each
Reading from 11905: heap size 330 MB, throughput 0.998067
Reading from 11905: heap size 336 MB, throughput 0.998182
Reading from 11905: heap size 337 MB, throughput 0.998011
Reading from 11905: heap size 343 MB, throughput 0.998594
Reading from 11905: heap size 343 MB, throughput 0.998275
Equal recommendation: 3057 MB each
Reading from 11904: heap size 1354 MB, throughput 0.883425
Reading from 11905: heap size 350 MB, throughput 0.995107
Reading from 11905: heap size 350 MB, throughput 0.99829
Reading from 11905: heap size 357 MB, throughput 0.9984
Reading from 11905: heap size 358 MB, throughput 0.998114
Reading from 11905: heap size 364 MB, throughput 0.998315
Equal recommendation: 3057 MB each
Reading from 11905: heap size 365 MB, throughput 0.998119
Reading from 11904: heap size 1473 MB, throughput 0.987551
Reading from 11905: heap size 372 MB, throughput 0.998339
Reading from 11905: heap size 372 MB, throughput 0.997644
Reading from 11905: heap size 378 MB, throughput 0.994916
Reading from 11904: heap size 1513 MB, throughput 0.988051
Reading from 11904: heap size 1508 MB, throughput 0.884111
Reading from 11904: heap size 1514 MB, throughput 0.779136
Reading from 11904: heap size 1504 MB, throughput 0.737287
Reading from 11905: heap size 378 MB, throughput 0.998501
Reading from 11904: heap size 1524 MB, throughput 0.720846
Equal recommendation: 3057 MB each
Reading from 11904: heap size 1548 MB, throughput 0.747744
Reading from 11904: heap size 1561 MB, throughput 0.73871
Reading from 11904: heap size 1588 MB, throughput 0.764266
Reading from 11904: heap size 1595 MB, throughput 0.917054
Reading from 11905: heap size 386 MB, throughput 0.998486
Reading from 11904: heap size 1616 MB, throughput 0.97554
Reading from 11905: heap size 387 MB, throughput 0.998509
Reading from 11904: heap size 1623 MB, throughput 0.974184
Reading from 11905: heap size 393 MB, throughput 0.998418
Reading from 11905: heap size 394 MB, throughput 0.998211
Reading from 11904: heap size 1621 MB, throughput 0.973546
Equal recommendation: 3057 MB each
Reading from 11905: heap size 401 MB, throughput 0.998453
Reading from 11904: heap size 1631 MB, throughput 0.973204
Reading from 11905: heap size 401 MB, throughput 0.996294
Reading from 11904: heap size 1620 MB, throughput 0.969921
Reading from 11905: heap size 407 MB, throughput 0.998234
Reading from 11905: heap size 408 MB, throughput 0.998466
Reading from 11904: heap size 1632 MB, throughput 0.971859
Equal recommendation: 3057 MB each
Reading from 11905: heap size 415 MB, throughput 0.998531
Reading from 11904: heap size 1643 MB, throughput 0.976844
Reading from 11905: heap size 415 MB, throughput 0.998284
Reading from 11904: heap size 1645 MB, throughput 0.979766
Reading from 11905: heap size 421 MB, throughput 0.998557
Reading from 11904: heap size 1650 MB, throughput 0.976467
Reading from 11905: heap size 422 MB, throughput 0.998329
Equal recommendation: 3057 MB each
Reading from 11905: heap size 429 MB, throughput 0.995937
Reading from 11904: heap size 1654 MB, throughput 0.976867
Reading from 11905: heap size 429 MB, throughput 0.997976
Reading from 11904: heap size 1642 MB, throughput 0.976265
Reading from 11905: heap size 438 MB, throughput 0.998697
Reading from 11905: heap size 438 MB, throughput 0.998295
Reading from 11904: heap size 1651 MB, throughput 0.974014
Equal recommendation: 3057 MB each
Reading from 11905: heap size 446 MB, throughput 0.998659
Reading from 11904: heap size 1632 MB, throughput 0.973134
Reading from 11905: heap size 446 MB, throughput 0.998281
Reading from 11904: heap size 1642 MB, throughput 0.97068
Reading from 11905: heap size 454 MB, throughput 0.99813
Reading from 11905: heap size 454 MB, throughput 0.996564
Reading from 11904: heap size 1645 MB, throughput 0.969308
Equal recommendation: 3057 MB each
Reading from 11905: heap size 460 MB, throughput 0.998509
Reading from 11904: heap size 1646 MB, throughput 0.966878
Reading from 11905: heap size 461 MB, throughput 0.998563
Reading from 11905: heap size 467 MB, throughput 0.897463
Reading from 11904: heap size 1656 MB, throughput 0.967803
Reading from 11905: heap size 472 MB, throughput 0.999358
Reading from 11904: heap size 1658 MB, throughput 0.966529
Equal recommendation: 3057 MB each
Reading from 11905: heap size 482 MB, throughput 0.999407
Reading from 11905: heap size 482 MB, throughput 0.998368
Reading from 11904: heap size 1670 MB, throughput 0.765744
Reading from 11905: heap size 492 MB, throughput 0.9984
Reading from 11904: heap size 1630 MB, throughput 0.993628
Reading from 11905: heap size 492 MB, throughput 0.998892
Equal recommendation: 3057 MB each
Reading from 11905: heap size 500 MB, throughput 0.99883
Reading from 11904: heap size 1631 MB, throughput 0.99072
Reading from 11905: heap size 501 MB, throughput 0.998815
Reading from 11904: heap size 1643 MB, throughput 0.988819
Reading from 11905: heap size 508 MB, throughput 0.998901
Reading from 11904: heap size 1658 MB, throughput 0.988085
Equal recommendation: 3057 MB each
Client 11905 died
Clients: 1
Reading from 11904: heap size 1659 MB, throughput 0.985298
Reading from 11904: heap size 1649 MB, throughput 0.983334
Recommendation: one client; give it all the memory
Reading from 11904: heap size 1511 MB, throughput 0.982254
Reading from 11904: heap size 1635 MB, throughput 0.980597
Reading from 11904: heap size 1645 MB, throughput 0.979092
Recommendation: one client; give it all the memory
Reading from 11904: heap size 1640 MB, throughput 0.97736
Recommendation: one client; give it all the memory
Reading from 11904: heap size 1643 MB, throughput 0.991613
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 11904: heap size 1626 MB, throughput 0.992936
Reading from 11904: heap size 1654 MB, throughput 0.855355
Reading from 11904: heap size 1640 MB, throughput 0.772061
Reading from 11904: heap size 1686 MB, throughput 0.753059
Reading from 11904: heap size 1721 MB, throughput 0.781664
Reading from 11904: heap size 1740 MB, throughput 0.812124
Client 11904 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
