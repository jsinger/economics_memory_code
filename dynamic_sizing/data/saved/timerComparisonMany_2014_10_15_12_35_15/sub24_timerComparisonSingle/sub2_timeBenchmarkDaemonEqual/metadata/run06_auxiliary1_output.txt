economemd
    total memory: 6265 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub24_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
ReadingThread: starting
TimerThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 11807: heap size 9 MB, throughput 0.990163
Clients: 1
Client 11807 has a minimum heap size of 30 MB
Reading from 11806: heap size 9 MB, throughput 0.991556
Clients: 2
Client 11806 has a minimum heap size of 1223 MB
Reading from 11807: heap size 9 MB, throughput 0.967328
Reading from 11807: heap size 9 MB, throughput 0.969162
Reading from 11806: heap size 9 MB, throughput 0.963475
Reading from 11807: heap size 9 MB, throughput 0.951959
Reading from 11807: heap size 11 MB, throughput 0.735806
Reading from 11807: heap size 11 MB, throughput 0.55547
Reading from 11807: heap size 16 MB, throughput 0.700655
Reading from 11807: heap size 16 MB, throughput 0.642447
Reading from 11807: heap size 24 MB, throughput 0.851841
Reading from 11807: heap size 24 MB, throughput 0.839179
Reading from 11807: heap size 34 MB, throughput 0.920473
Reading from 11807: heap size 34 MB, throughput 0.819612
Reading from 11806: heap size 9 MB, throughput 0.969659
Reading from 11807: heap size 50 MB, throughput 0.932985
Reading from 11807: heap size 50 MB, throughput 0.921033
Reading from 11806: heap size 9 MB, throughput 0.96035
Reading from 11807: heap size 75 MB, throughput 0.949174
Reading from 11807: heap size 75 MB, throughput 0.958107
Reading from 11807: heap size 116 MB, throughput 0.962419
Reading from 11807: heap size 116 MB, throughput 0.963521
Reading from 11806: heap size 11 MB, throughput 0.98433
Reading from 11806: heap size 11 MB, throughput 0.983213
Reading from 11806: heap size 17 MB, throughput 0.927914
Reading from 11806: heap size 17 MB, throughput 0.612545
Reading from 11806: heap size 30 MB, throughput 0.971204
Reading from 11806: heap size 31 MB, throughput 0.956983
Reading from 11806: heap size 34 MB, throughput 0.448579
Reading from 11806: heap size 46 MB, throughput 0.890768
Reading from 11806: heap size 49 MB, throughput 0.772015
Reading from 11807: heap size 160 MB, throughput 0.922037
Reading from 11806: heap size 50 MB, throughput 0.553338
Reading from 11806: heap size 55 MB, throughput 0.104659
Reading from 11806: heap size 78 MB, throughput 0.752457
Reading from 11807: heap size 167 MB, throughput 0.844559
Reading from 11806: heap size 81 MB, throughput 0.87312
Reading from 11806: heap size 84 MB, throughput 0.376391
Reading from 11806: heap size 115 MB, throughput 0.830455
Reading from 11806: heap size 115 MB, throughput 0.820134
Reading from 11807: heap size 213 MB, throughput 0.992932
Reading from 11806: heap size 118 MB, throughput 0.221743
Reading from 11806: heap size 148 MB, throughput 0.713344
Reading from 11806: heap size 155 MB, throughput 0.846876
Reading from 11807: heap size 239 MB, throughput 0.994503
Reading from 11806: heap size 156 MB, throughput 0.584412
Reading from 11806: heap size 162 MB, throughput 0.705248
Reading from 11806: heap size 165 MB, throughput 0.171613
Reading from 11806: heap size 213 MB, throughput 0.767307
Reading from 11806: heap size 214 MB, throughput 0.782831
Reading from 11806: heap size 217 MB, throughput 0.617091
Reading from 11807: heap size 242 MB, throughput 0.995211
Reading from 11806: heap size 224 MB, throughput 0.729071
Reading from 11806: heap size 227 MB, throughput 0.648776
Reading from 11806: heap size 235 MB, throughput 0.66872
Reading from 11806: heap size 240 MB, throughput 0.680315
Reading from 11806: heap size 251 MB, throughput 0.600866
Reading from 11807: heap size 242 MB, throughput 0.849369
Reading from 11806: heap size 255 MB, throughput 0.0916142
Reading from 11806: heap size 305 MB, throughput 0.357512
Reading from 11807: heap size 270 MB, throughput 0.989625
Reading from 11806: heap size 317 MB, throughput 0.197208
Reading from 11807: heap size 275 MB, throughput 0.983719
Reading from 11806: heap size 362 MB, throughput 0.465925
Reading from 11806: heap size 352 MB, throughput 0.509489
Reading from 11806: heap size 359 MB, throughput 0.646393
Reading from 11806: heap size 360 MB, throughput 0.673836
Reading from 11806: heap size 362 MB, throughput 0.630429
Reading from 11806: heap size 364 MB, throughput 0.625207
Reading from 11806: heap size 371 MB, throughput 0.448658
Reading from 11806: heap size 374 MB, throughput 0.438075
Reading from 11807: heap size 290 MB, throughput 0.991914
Equal recommendation: 3132 MB each
Reading from 11807: heap size 291 MB, throughput 0.961063
Reading from 11806: heap size 384 MB, throughput 0.0764424
Reading from 11806: heap size 442 MB, throughput 0.447387
Reading from 11806: heap size 445 MB, throughput 0.542759
Reading from 11806: heap size 444 MB, throughput 0.656767
Reading from 11806: heap size 446 MB, throughput 0.525859
Reading from 11807: heap size 301 MB, throughput 0.963862
Reading from 11806: heap size 452 MB, throughput 0.547682
Reading from 11807: heap size 306 MB, throughput 0.987489
Reading from 11806: heap size 453 MB, throughput 0.116405
Reading from 11806: heap size 510 MB, throughput 0.493324
Reading from 11806: heap size 511 MB, throughput 0.499632
Reading from 11806: heap size 512 MB, throughput 0.555153
Reading from 11806: heap size 513 MB, throughput 0.460945
Reading from 11807: heap size 327 MB, throughput 0.978955
Reading from 11806: heap size 518 MB, throughput 0.11244
Reading from 11807: heap size 328 MB, throughput 0.882253
Reading from 11806: heap size 579 MB, throughput 0.478027
Reading from 11806: heap size 574 MB, throughput 0.660029
Reading from 11806: heap size 580 MB, throughput 0.631072
Reading from 11806: heap size 583 MB, throughput 0.635786
Reading from 11806: heap size 591 MB, throughput 0.606492
Reading from 11807: heap size 355 MB, throughput 0.99231
Reading from 11806: heap size 595 MB, throughput 0.495529
Reading from 11806: heap size 609 MB, throughput 0.510874
Reading from 11806: heap size 617 MB, throughput 0.461203
Reading from 11807: heap size 357 MB, throughput 0.977986
Reading from 11807: heap size 374 MB, throughput 0.983568
Reading from 11806: heap size 628 MB, throughput 0.267119
Reading from 11806: heap size 694 MB, throughput 0.784466
Reading from 11807: heap size 375 MB, throughput 0.982047
Reading from 11806: heap size 704 MB, throughput 0.799979
Reading from 11806: heap size 703 MB, throughput 0.84138
Reading from 11807: heap size 392 MB, throughput 0.977168
Equal recommendation: 3132 MB each
Reading from 11806: heap size 714 MB, throughput 0.503029
Reading from 11806: heap size 724 MB, throughput 0.355083
Reading from 11807: heap size 393 MB, throughput 0.98145
Reading from 11806: heap size 728 MB, throughput 0.0794667
Reading from 11806: heap size 789 MB, throughput 0.234848
Reading from 11806: heap size 691 MB, throughput 0.342789
Reading from 11807: heap size 411 MB, throughput 0.976559
Reading from 11806: heap size 778 MB, throughput 0.0470033
Reading from 11806: heap size 857 MB, throughput 0.261383
Reading from 11806: heap size 855 MB, throughput 0.469788
Reading from 11807: heap size 412 MB, throughput 0.992679
Reading from 11806: heap size 859 MB, throughput 0.885083
Reading from 11806: heap size 862 MB, throughput 0.653847
Reading from 11806: heap size 864 MB, throughput 0.512218
Reading from 11807: heap size 432 MB, throughput 0.986048
Reading from 11806: heap size 864 MB, throughput 0.0492508
Reading from 11806: heap size 945 MB, throughput 0.424362
Reading from 11806: heap size 946 MB, throughput 0.933371
Reading from 11807: heap size 436 MB, throughput 0.988886
Reading from 11806: heap size 949 MB, throughput 0.893849
Reading from 11806: heap size 951 MB, throughput 0.888104
Reading from 11806: heap size 952 MB, throughput 0.779482
Reading from 11806: heap size 944 MB, throughput 0.930307
Reading from 11806: heap size 744 MB, throughput 0.918234
Reading from 11806: heap size 930 MB, throughput 0.91149
Reading from 11807: heap size 452 MB, throughput 0.98801
Reading from 11806: heap size 790 MB, throughput 0.802135
Reading from 11806: heap size 902 MB, throughput 0.811158
Reading from 11806: heap size 794 MB, throughput 0.735351
Reading from 11806: heap size 888 MB, throughput 0.775021
Reading from 11806: heap size 791 MB, throughput 0.823264
Reading from 11806: heap size 876 MB, throughput 0.829638
Reading from 11807: heap size 453 MB, throughput 0.99097
Reading from 11806: heap size 796 MB, throughput 0.847553
Reading from 11806: heap size 868 MB, throughput 0.852004
Reading from 11806: heap size 801 MB, throughput 0.809006
Reading from 11806: heap size 865 MB, throughput 0.880044
Reading from 11806: heap size 870 MB, throughput 0.878055
Reading from 11807: heap size 466 MB, throughput 0.99268
Equal recommendation: 3132 MB each
Reading from 11806: heap size 861 MB, throughput 0.979837
Reading from 11807: heap size 468 MB, throughput 0.982403
Reading from 11806: heap size 866 MB, throughput 0.974386
Reading from 11806: heap size 869 MB, throughput 0.789361
Reading from 11806: heap size 871 MB, throughput 0.7679
Reading from 11807: heap size 480 MB, throughput 0.981905
Reading from 11806: heap size 876 MB, throughput 0.759435
Reading from 11806: heap size 877 MB, throughput 0.786903
Reading from 11806: heap size 884 MB, throughput 0.695135
Reading from 11806: heap size 884 MB, throughput 0.754055
Reading from 11806: heap size 890 MB, throughput 0.798721
Reading from 11806: heap size 891 MB, throughput 0.754448
Reading from 11806: heap size 896 MB, throughput 0.820073
Reading from 11807: heap size 483 MB, throughput 0.97925
Reading from 11806: heap size 897 MB, throughput 0.865054
Reading from 11806: heap size 899 MB, throughput 0.874201
Reading from 11806: heap size 902 MB, throughput 0.87676
Reading from 11807: heap size 499 MB, throughput 0.985893
Reading from 11806: heap size 904 MB, throughput 0.102384
Reading from 11806: heap size 990 MB, throughput 0.470731
Reading from 11807: heap size 501 MB, throughput 0.992246
Reading from 11806: heap size 1015 MB, throughput 0.7264
Reading from 11806: heap size 1017 MB, throughput 0.729668
Reading from 11806: heap size 1020 MB, throughput 0.718775
Reading from 11806: heap size 1023 MB, throughput 0.698388
Reading from 11806: heap size 1025 MB, throughput 0.705291
Reading from 11806: heap size 1029 MB, throughput 0.728188
Reading from 11806: heap size 1035 MB, throughput 0.71465
Reading from 11807: heap size 515 MB, throughput 0.988957
Reading from 11806: heap size 1038 MB, throughput 0.675551
Reading from 11806: heap size 1054 MB, throughput 0.636668
Equal recommendation: 3132 MB each
Reading from 11807: heap size 518 MB, throughput 0.990196
Reading from 11807: heap size 533 MB, throughput 0.987534
Reading from 11806: heap size 1054 MB, throughput 0.975409
Reading from 11807: heap size 535 MB, throughput 0.999999
Reading from 11807: heap size 535 MB, throughput 1
Reading from 11807: heap size 535 MB, throughput 0.284106
Reading from 11807: heap size 556 MB, throughput 0.982863
Reading from 11806: heap size 1074 MB, throughput 0.971906
Reading from 11807: heap size 556 MB, throughput 0.996878
Reading from 11807: heap size 575 MB, throughput 0.993864
Reading from 11806: heap size 1076 MB, throughput 0.969535
Equal recommendation: 3132 MB each
Reading from 11807: heap size 577 MB, throughput 0.98376
Reading from 11806: heap size 1091 MB, throughput 0.958754
Reading from 11807: heap size 589 MB, throughput 0.994585
Reading from 11807: heap size 592 MB, throughput 0.983788
Reading from 11807: heap size 605 MB, throughput 0.97495
Reading from 11806: heap size 1096 MB, throughput 0.748542
Reading from 11807: heap size 608 MB, throughput 0.993652
Reading from 11806: heap size 1201 MB, throughput 0.994745
Reading from 11807: heap size 624 MB, throughput 0.986629
Equal recommendation: 3132 MB each
Reading from 11807: heap size 628 MB, throughput 0.987354
Reading from 11806: heap size 1204 MB, throughput 0.991433
Reading from 11807: heap size 652 MB, throughput 0.995767
Reading from 11807: heap size 653 MB, throughput 0.98761
Reading from 11806: heap size 1218 MB, throughput 0.991489
Reading from 11807: heap size 671 MB, throughput 0.985814
Reading from 11806: heap size 1222 MB, throughput 0.988216
Reading from 11807: heap size 672 MB, throughput 0.991063
Equal recommendation: 3132 MB each
Reading from 11807: heap size 690 MB, throughput 0.994975
Reading from 11806: heap size 1224 MB, throughput 0.986902
Reading from 11807: heap size 692 MB, throughput 0.986809
Reading from 11807: heap size 711 MB, throughput 0.988048
Reading from 11806: heap size 1227 MB, throughput 0.985428
Reading from 11807: heap size 711 MB, throughput 0.991266
Reading from 11806: heap size 1216 MB, throughput 0.987622
Equal recommendation: 3132 MB each
Reading from 11807: heap size 731 MB, throughput 0.994068
Reading from 11807: heap size 733 MB, throughput 0.988401
Reading from 11806: heap size 1148 MB, throughput 0.979031
Reading from 11807: heap size 752 MB, throughput 0.992825
Reading from 11807: heap size 752 MB, throughput 0.989645
Reading from 11806: heap size 1210 MB, throughput 0.981109
Reading from 11807: heap size 771 MB, throughput 0.990099
Equal recommendation: 3132 MB each
Reading from 11806: heap size 1215 MB, throughput 0.980397
Reading from 11807: heap size 772 MB, throughput 0.992466
Reading from 11807: heap size 791 MB, throughput 0.99139
Reading from 11806: heap size 1216 MB, throughput 0.979629
Reading from 11807: heap size 792 MB, throughput 0.9953
Reading from 11806: heap size 1217 MB, throughput 0.977103
Reading from 11807: heap size 808 MB, throughput 0.991153
Equal recommendation: 3132 MB each
Reading from 11807: heap size 810 MB, throughput 0.99372
Reading from 11806: heap size 1219 MB, throughput 0.981008
Reading from 11807: heap size 827 MB, throughput 0.992142
Reading from 11806: heap size 1224 MB, throughput 0.968954
Reading from 11807: heap size 829 MB, throughput 0.990557
Reading from 11806: heap size 1227 MB, throughput 0.976807
Reading from 11807: heap size 849 MB, throughput 0.991533
Equal recommendation: 3132 MB each
Reading from 11807: heap size 849 MB, throughput 0.990467
Reading from 11806: heap size 1235 MB, throughput 0.969227
Reading from 11807: heap size 873 MB, throughput 0.991605
Reading from 11806: heap size 1241 MB, throughput 0.971225
Reading from 11807: heap size 873 MB, throughput 0.990548
Equal recommendation: 3132 MB each
Reading from 11806: heap size 1247 MB, throughput 0.968194
Reading from 11807: heap size 896 MB, throughput 0.99613
Reading from 11807: heap size 896 MB, throughput 0.990148
Reading from 11806: heap size 1253 MB, throughput 0.9693
Reading from 11807: heap size 916 MB, throughput 0.991709
Reading from 11806: heap size 1257 MB, throughput 0.97171
Reading from 11807: heap size 916 MB, throughput 0.988536
Equal recommendation: 3132 MB each
Reading from 11806: heap size 1263 MB, throughput 0.97078
Reading from 11807: heap size 938 MB, throughput 0.991252
Reading from 11807: heap size 939 MB, throughput 0.991443
Reading from 11806: heap size 1265 MB, throughput 0.965655
Reading from 11807: heap size 964 MB, throughput 0.992984
Reading from 11806: heap size 1270 MB, throughput 0.972564
Equal recommendation: 3132 MB each
Reading from 11807: heap size 965 MB, throughput 0.991024
Reading from 11806: heap size 1271 MB, throughput 0.968081
Reading from 11807: heap size 989 MB, throughput 0.991912
Reading from 11806: heap size 1276 MB, throughput 0.969921
Reading from 11807: heap size 989 MB, throughput 0.98831
Equal recommendation: 3132 MB each
Reading from 11807: heap size 1013 MB, throughput 0.991914
Reading from 11806: heap size 1276 MB, throughput 0.971738
Reading from 11807: heap size 1014 MB, throughput 0.991026
Reading from 11806: heap size 1278 MB, throughput 0.968596
Reading from 11807: heap size 1041 MB, throughput 0.992459
Reading from 11806: heap size 1279 MB, throughput 0.970162
Equal recommendation: 3132 MB each
Reading from 11807: heap size 1042 MB, throughput 0.989957
Reading from 11807: heap size 1070 MB, throughput 0.995165
Reading from 11806: heap size 1280 MB, throughput 0.635637
Reading from 11807: heap size 1070 MB, throughput 0.988963
Reading from 11806: heap size 1391 MB, throughput 0.958425
Equal recommendation: 3132 MB each
Reading from 11807: heap size 1094 MB, throughput 0.996066
Reading from 11806: heap size 1386 MB, throughput 0.985305
Reading from 11807: heap size 1096 MB, throughput 0.996345
Reading from 11806: heap size 1395 MB, throughput 0.986189
Reading from 11807: heap size 1116 MB, throughput 0.992642
Equal recommendation: 3132 MB each
Reading from 11807: heap size 1119 MB, throughput 0.991574
Reading from 11806: heap size 1404 MB, throughput 0.97783
Reading from 11807: heap size 1142 MB, throughput 0.995371
Reading from 11806: heap size 1405 MB, throughput 0.984098
Reading from 11807: heap size 1143 MB, throughput 0.99333
Reading from 11806: heap size 1398 MB, throughput 0.980419
Equal recommendation: 3132 MB each
Reading from 11807: heap size 1165 MB, throughput 0.989846
Reading from 11806: heap size 1295 MB, throughput 0.981737
Reading from 11807: heap size 1166 MB, throughput 0.990453
Reading from 11806: heap size 1389 MB, throughput 0.980094
Reading from 11807: heap size 1194 MB, throughput 0.995287
Equal recommendation: 3132 MB each
Reading from 11807: heap size 1197 MB, throughput 0.982383
Reading from 11806: heap size 1315 MB, throughput 0.978157
Reading from 11807: heap size 1218 MB, throughput 0.990859
Reading from 11807: heap size 1223 MB, throughput 0.985593
Equal recommendation: 3132 MB each
Reading from 11807: heap size 1258 MB, throughput 0.990169
Reading from 11807: heap size 1262 MB, throughput 0.98964
Equal recommendation: 3132 MB each
Reading from 11807: heap size 1302 MB, throughput 0.987165
Reading from 11807: heap size 1304 MB, throughput 0.989743
Reading from 11807: heap size 1350 MB, throughput 0.987717
Equal recommendation: 3132 MB each
Reading from 11807: heap size 1351 MB, throughput 0.990312
Reading from 11806: heap size 1384 MB, throughput 0.996537
Reading from 11807: heap size 1395 MB, throughput 0.990173
Reading from 11806: heap size 1388 MB, throughput 0.973504
Reading from 11807: heap size 1397 MB, throughput 0.987619
Equal recommendation: 3132 MB each
Reading from 11806: heap size 1364 MB, throughput 0.925796
Reading from 11806: heap size 1407 MB, throughput 0.239486
Reading from 11806: heap size 1427 MB, throughput 0.989714
Reading from 11806: heap size 1464 MB, throughput 0.987228
Reading from 11806: heap size 1482 MB, throughput 0.990466
Reading from 11806: heap size 1485 MB, throughput 0.991072
Reading from 11806: heap size 1485 MB, throughput 0.991196
Reading from 11806: heap size 1490 MB, throughput 0.991315
Reading from 11807: heap size 1443 MB, throughput 0.970034
Reading from 11806: heap size 1477 MB, throughput 0.987485
Reading from 11806: heap size 1246 MB, throughput 0.990901
Reading from 11807: heap size 1461 MB, throughput 0.994376
Equal recommendation: 3132 MB each
Client 11807 died
Clients: 1
Reading from 11806: heap size 1461 MB, throughput 0.994287
Reading from 11806: heap size 1268 MB, throughput 0.99263
Reading from 11806: heap size 1438 MB, throughput 0.991229
Recommendation: one client; give it all the memory
Reading from 11806: heap size 1288 MB, throughput 0.989923
Reading from 11806: heap size 1414 MB, throughput 0.989425
Reading from 11806: heap size 1307 MB, throughput 0.988278
Recommendation: one client; give it all the memory
Reading from 11806: heap size 1407 MB, throughput 0.986938
Reading from 11806: heap size 1414 MB, throughput 0.986367
Recommendation: one client; give it all the memory
Reading from 11806: heap size 1412 MB, throughput 0.985636
Reading from 11806: heap size 1413 MB, throughput 0.984476
Reading from 11806: heap size 1416 MB, throughput 0.983947
Recommendation: one client; give it all the memory
Reading from 11806: heap size 1419 MB, throughput 0.982394
Reading from 11806: heap size 1422 MB, throughput 0.981625
Recommendation: one client; give it all the memory
Reading from 11806: heap size 1428 MB, throughput 0.980513
Reading from 11806: heap size 1433 MB, throughput 0.980581
Reading from 11806: heap size 1441 MB, throughput 0.97969
Recommendation: one client; give it all the memory
Reading from 11806: heap size 1448 MB, throughput 0.980244
Reading from 11806: heap size 1452 MB, throughput 0.979664
Recommendation: one client; give it all the memory
Reading from 11806: heap size 1458 MB, throughput 0.979624
Reading from 11806: heap size 1460 MB, throughput 0.978139
Recommendation: one client; give it all the memory
Reading from 11806: heap size 1466 MB, throughput 0.979176
Reading from 11806: heap size 1466 MB, throughput 0.979529
Reading from 11806: heap size 1470 MB, throughput 0.980785
Recommendation: one client; give it all the memory
Reading from 11806: heap size 1471 MB, throughput 0.979812
Reading from 11806: heap size 1473 MB, throughput 0.98013
Recommendation: one client; give it all the memory
Reading from 11806: heap size 1475 MB, throughput 0.978704
Reading from 11806: heap size 1476 MB, throughput 0.979264
Recommendation: one client; give it all the memory
Reading from 11806: heap size 1478 MB, throughput 0.979578
Reading from 11806: heap size 1479 MB, throughput 0.980212
Recommendation: one client; give it all the memory
Reading from 11806: heap size 1481 MB, throughput 0.980777
Reading from 11806: heap size 1483 MB, throughput 0.809415
Recommendation: one client; give it all the memory
Reading from 11806: heap size 1524 MB, throughput 0.995526
Reading from 11806: heap size 1518 MB, throughput 0.994528
Recommendation: one client; give it all the memory
Reading from 11806: heap size 1530 MB, throughput 0.992834
Reading from 11806: heap size 1540 MB, throughput 0.991899
Recommendation: one client; give it all the memory
Reading from 11806: heap size 1542 MB, throughput 0.991103
Reading from 11806: heap size 1535 MB, throughput 0.989652
Recommendation: one client; give it all the memory
Reading from 11806: heap size 1409 MB, throughput 0.994468
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 11806: heap size 1530 MB, throughput 0.993975
Reading from 11806: heap size 1534 MB, throughput 0.987197
Reading from 11806: heap size 1526 MB, throughput 0.875364
Reading from 11806: heap size 1541 MB, throughput 0.740776
Reading from 11806: heap size 1562 MB, throughput 0.765689
Reading from 11806: heap size 1590 MB, throughput 0.744515
Recommendation: one client; give it all the memory
Reading from 11806: heap size 1627 MB, throughput 0.779582
Reading from 11806: heap size 1645 MB, throughput 0.760316
Client 11806 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
