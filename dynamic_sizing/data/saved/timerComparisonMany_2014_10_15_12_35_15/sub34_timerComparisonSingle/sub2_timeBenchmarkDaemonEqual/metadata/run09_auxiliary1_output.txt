economemd
    total memory: 6175 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub34_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run09_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
TimerThread: starting
ReadingThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 20789: heap size 9 MB, throughput 0.988079
Clients: 1
Client 20789 has a minimum heap size of 12 MB
Reading from 20788: heap size 9 MB, throughput 0.990495
Clients: 2
Client 20788 has a minimum heap size of 1223 MB
Reading from 20789: heap size 9 MB, throughput 0.98263
Reading from 20788: heap size 9 MB, throughput 0.987349
Reading from 20788: heap size 9 MB, throughput 0.935301
Reading from 20789: heap size 11 MB, throughput 0.97976
Reading from 20788: heap size 9 MB, throughput 0.950094
Reading from 20789: heap size 11 MB, throughput 0.970195
Reading from 20788: heap size 11 MB, throughput 0.970073
Reading from 20789: heap size 15 MB, throughput 0.9867
Reading from 20788: heap size 11 MB, throughput 0.983039
Reading from 20788: heap size 17 MB, throughput 0.91591
Reading from 20789: heap size 15 MB, throughput 0.957063
Reading from 20788: heap size 17 MB, throughput 0.705231
Reading from 20788: heap size 30 MB, throughput 0.982642
Reading from 20788: heap size 31 MB, throughput 0.933643
Reading from 20789: heap size 24 MB, throughput 0.910164
Reading from 20788: heap size 33 MB, throughput 0.406897
Reading from 20788: heap size 43 MB, throughput 0.834242
Reading from 20788: heap size 48 MB, throughput 0.918957
Reading from 20789: heap size 30 MB, throughput 0.98644
Reading from 20788: heap size 49 MB, throughput 0.241709
Reading from 20788: heap size 65 MB, throughput 0.265602
Reading from 20789: heap size 35 MB, throughput 0.988171
Reading from 20788: heap size 88 MB, throughput 0.843471
Reading from 20788: heap size 89 MB, throughput 0.758393
Reading from 20788: heap size 90 MB, throughput 0.671946
Reading from 20789: heap size 40 MB, throughput 0.972843
Reading from 20788: heap size 95 MB, throughput 0.831168
Reading from 20789: heap size 42 MB, throughput 0.990007
Reading from 20788: heap size 96 MB, throughput 0.268181
Reading from 20789: heap size 46 MB, throughput 0.976245
Reading from 20788: heap size 128 MB, throughput 0.734894
Reading from 20789: heap size 51 MB, throughput 0.980898
Reading from 20788: heap size 129 MB, throughput 0.730339
Reading from 20788: heap size 131 MB, throughput 0.680477
Reading from 20788: heap size 135 MB, throughput 0.774945
Reading from 20788: heap size 139 MB, throughput 0.700342
Reading from 20789: heap size 51 MB, throughput 0.991969
Reading from 20789: heap size 57 MB, throughput 0.990059
Reading from 20789: heap size 58 MB, throughput 0.97103
Reading from 20789: heap size 65 MB, throughput 0.691515
Reading from 20788: heap size 142 MB, throughput 0.0822695
Reading from 20789: heap size 69 MB, throughput 0.990913
Reading from 20788: heap size 185 MB, throughput 0.699346
Reading from 20788: heap size 186 MB, throughput 0.588009
Reading from 20788: heap size 188 MB, throughput 0.627332
Reading from 20789: heap size 76 MB, throughput 0.995628
Reading from 20788: heap size 194 MB, throughput 0.550563
Reading from 20788: heap size 197 MB, throughput 0.607047
Reading from 20788: heap size 206 MB, throughput 0.404983
Reading from 20788: heap size 212 MB, throughput 0.424317
Reading from 20789: heap size 76 MB, throughput 0.997227
Reading from 20788: heap size 219 MB, throughput 0.107512
Reading from 20788: heap size 265 MB, throughput 0.535521
Reading from 20788: heap size 217 MB, throughput 0.626817
Reading from 20788: heap size 257 MB, throughput 0.614936
Reading from 20789: heap size 82 MB, throughput 0.99772
Reading from 20788: heap size 262 MB, throughput 0.157667
Reading from 20789: heap size 83 MB, throughput 0.99643
Reading from 20788: heap size 298 MB, throughput 0.636232
Reading from 20788: heap size 302 MB, throughput 0.611866
Reading from 20788: heap size 302 MB, throughput 0.700794
Reading from 20788: heap size 302 MB, throughput 0.59895
Reading from 20788: heap size 304 MB, throughput 0.686214
Reading from 20788: heap size 308 MB, throughput 0.636712
Reading from 20789: heap size 88 MB, throughput 0.997324
Reading from 20788: heap size 311 MB, throughput 0.509802
Reading from 20788: heap size 320 MB, throughput 0.515048
Reading from 20788: heap size 326 MB, throughput 0.50145
Reading from 20788: heap size 335 MB, throughput 0.511751
Reading from 20788: heap size 339 MB, throughput 0.483055
Reading from 20788: heap size 346 MB, throughput 0.500876
Reading from 20789: heap size 89 MB, throughput 0.997031
Reading from 20788: heap size 351 MB, throughput 0.0596963
Reading from 20789: heap size 93 MB, throughput 0.996566
Reading from 20788: heap size 401 MB, throughput 0.419772
Reading from 20788: heap size 393 MB, throughput 0.673573
Reading from 20789: heap size 94 MB, throughput 0.99534
Reading from 20788: heap size 336 MB, throughput 0.0632847
Reading from 20788: heap size 444 MB, throughput 0.42607
Reading from 20788: heap size 448 MB, throughput 0.59663
Reading from 20788: heap size 444 MB, throughput 0.499829
Reading from 20789: heap size 97 MB, throughput 0.997139
Reading from 20788: heap size 390 MB, throughput 0.568974
Reading from 20788: heap size 439 MB, throughput 0.479249
Equal recommendation: 3087 MB each
Reading from 20788: heap size 443 MB, throughput 0.486885
Reading from 20788: heap size 445 MB, throughput 0.582589
Reading from 20789: heap size 97 MB, throughput 0.994884
Reading from 20788: heap size 446 MB, throughput 0.543862
Reading from 20788: heap size 449 MB, throughput 0.467704
Reading from 20788: heap size 450 MB, throughput 0.542739
Reading from 20788: heap size 454 MB, throughput 0.525945
Reading from 20789: heap size 100 MB, throughput 0.986738
Reading from 20788: heap size 457 MB, throughput 0.435951
Reading from 20788: heap size 461 MB, throughput 0.519318
Reading from 20789: heap size 100 MB, throughput 0.995885
Reading from 20788: heap size 467 MB, throughput 0.0824004
Reading from 20788: heap size 524 MB, throughput 0.383098
Reading from 20788: heap size 533 MB, throughput 0.452531
Reading from 20789: heap size 104 MB, throughput 0.996841
Reading from 20788: heap size 537 MB, throughput 0.40906
Reading from 20788: heap size 543 MB, throughput 0.484304
Reading from 20789: heap size 105 MB, throughput 0.995122
Reading from 20788: heap size 552 MB, throughput 0.0821289
Reading from 20789: heap size 108 MB, throughput 0.994631
Reading from 20788: heap size 619 MB, throughput 0.326913
Reading from 20788: heap size 612 MB, throughput 0.516452
Reading from 20788: heap size 551 MB, throughput 0.540201
Reading from 20788: heap size 608 MB, throughput 0.511642
Reading from 20789: heap size 108 MB, throughput 0.996455
Reading from 20789: heap size 112 MB, throughput 0.996892
Reading from 20788: heap size 613 MB, throughput 0.0970393
Reading from 20788: heap size 674 MB, throughput 0.49415
Reading from 20788: heap size 678 MB, throughput 0.733534
Reading from 20789: heap size 112 MB, throughput 0.997431
Reading from 20788: heap size 675 MB, throughput 0.908814
Reading from 20789: heap size 115 MB, throughput 0.998108
Reading from 20788: heap size 678 MB, throughput 0.865654
Reading from 20789: heap size 115 MB, throughput 0.995821
Reading from 20788: heap size 675 MB, throughput 0.887236
Reading from 20788: heap size 689 MB, throughput 0.507044
Reading from 20788: heap size 695 MB, throughput 0.581497
Reading from 20789: heap size 117 MB, throughput 0.997906
Reading from 20788: heap size 702 MB, throughput 0.527536
Reading from 20788: heap size 706 MB, throughput 0.402675
Reading from 20788: heap size 709 MB, throughput 0.392103
Reading from 20789: heap size 117 MB, throughput 0.997535
Reading from 20789: heap size 120 MB, throughput 0.996675
Equal recommendation: 3087 MB each
Reading from 20788: heap size 711 MB, throughput 0.0255428
Reading from 20789: heap size 120 MB, throughput 0.997032
Reading from 20788: heap size 784 MB, throughput 0.307643
Reading from 20788: heap size 784 MB, throughput 0.872703
Reading from 20788: heap size 784 MB, throughput 0.578933
Reading from 20788: heap size 780 MB, throughput 0.587046
Reading from 20789: heap size 122 MB, throughput 0.99846
Reading from 20788: heap size 783 MB, throughput 0.604849
Reading from 20788: heap size 787 MB, throughput 0.867783
Reading from 20789: heap size 122 MB, throughput 0.997849
Reading from 20789: heap size 123 MB, throughput 0.997214
Reading from 20788: heap size 787 MB, throughput 0.0824498
Reading from 20789: heap size 124 MB, throughput 0.99729
Reading from 20788: heap size 877 MB, throughput 0.619425
Reading from 20788: heap size 879 MB, throughput 0.693944
Reading from 20789: heap size 126 MB, throughput 0.996518
Reading from 20788: heap size 886 MB, throughput 0.93354
Reading from 20788: heap size 890 MB, throughput 0.884968
Reading from 20789: heap size 126 MB, throughput 0.992557
Reading from 20789: heap size 128 MB, throughput 0.964525
Reading from 20788: heap size 895 MB, throughput 0.885331
Reading from 20788: heap size 779 MB, throughput 0.778024
Reading from 20788: heap size 877 MB, throughput 0.812064
Reading from 20788: heap size 795 MB, throughput 0.83506
Reading from 20789: heap size 129 MB, throughput 0.995816
Reading from 20788: heap size 866 MB, throughput 0.842723
Reading from 20788: heap size 800 MB, throughput 0.800526
Reading from 20788: heap size 864 MB, throughput 0.828987
Reading from 20788: heap size 869 MB, throughput 0.83615
Reading from 20789: heap size 136 MB, throughput 0.996393
Reading from 20788: heap size 862 MB, throughput 0.862568
Reading from 20788: heap size 866 MB, throughput 0.844233
Reading from 20788: heap size 860 MB, throughput 0.858514
Reading from 20788: heap size 863 MB, throughput 0.88106
Reading from 20789: heap size 136 MB, throughput 0.99678
Reading from 20789: heap size 141 MB, throughput 0.996611
Reading from 20788: heap size 862 MB, throughput 0.980409
Reading from 20789: heap size 142 MB, throughput 0.996764
Reading from 20788: heap size 864 MB, throughput 0.957928
Reading from 20788: heap size 868 MB, throughput 0.812819
Reading from 20788: heap size 869 MB, throughput 0.81549
Equal recommendation: 3087 MB each
Reading from 20789: heap size 146 MB, throughput 0.994213
Reading from 20788: heap size 873 MB, throughput 0.814366
Reading from 20788: heap size 873 MB, throughput 0.775965
Reading from 20788: heap size 878 MB, throughput 0.823542
Reading from 20788: heap size 879 MB, throughput 0.826193
Reading from 20788: heap size 883 MB, throughput 0.837259
Reading from 20789: heap size 146 MB, throughput 0.996891
Reading from 20788: heap size 884 MB, throughput 0.821135
Reading from 20788: heap size 888 MB, throughput 0.897031
Reading from 20788: heap size 889 MB, throughput 0.895905
Reading from 20789: heap size 150 MB, throughput 0.997686
Reading from 20788: heap size 892 MB, throughput 0.877406
Reading from 20789: heap size 150 MB, throughput 0.996609
Reading from 20789: heap size 154 MB, throughput 0.997208
Reading from 20788: heap size 894 MB, throughput 0.0757044
Reading from 20789: heap size 154 MB, throughput 0.996593
Reading from 20788: heap size 972 MB, throughput 0.539632
Reading from 20788: heap size 992 MB, throughput 0.788835
Reading from 20788: heap size 989 MB, throughput 0.781748
Reading from 20788: heap size 992 MB, throughput 0.793969
Reading from 20788: heap size 993 MB, throughput 0.774634
Reading from 20788: heap size 996 MB, throughput 0.777888
Reading from 20788: heap size 998 MB, throughput 0.756714
Reading from 20788: heap size 1001 MB, throughput 0.754068
Reading from 20789: heap size 155 MB, throughput 0.997942
Reading from 20788: heap size 1009 MB, throughput 0.765102
Reading from 20788: heap size 1010 MB, throughput 0.74744
Reading from 20788: heap size 1023 MB, throughput 0.756702
Reading from 20789: heap size 156 MB, throughput 0.99479
Reading from 20788: heap size 1024 MB, throughput 0.941724
Reading from 20789: heap size 159 MB, throughput 0.99659
Reading from 20789: heap size 159 MB, throughput 0.99788
Equal recommendation: 3087 MB each
Reading from 20789: heap size 162 MB, throughput 0.998711
Reading from 20788: heap size 1035 MB, throughput 0.97816
Reading from 20789: heap size 162 MB, throughput 0.998117
Reading from 20789: heap size 165 MB, throughput 0.998845
Reading from 20789: heap size 165 MB, throughput 0.995699
Reading from 20788: heap size 1040 MB, throughput 0.969267
Reading from 20789: heap size 167 MB, throughput 0.994114
Reading from 20789: heap size 167 MB, throughput 0.994493
Reading from 20789: heap size 171 MB, throughput 0.997702
Reading from 20789: heap size 171 MB, throughput 0.994677
Reading from 20788: heap size 1051 MB, throughput 0.980485
Reading from 20789: heap size 174 MB, throughput 0.997725
Reading from 20789: heap size 174 MB, throughput 0.997775
Reading from 20788: heap size 1053 MB, throughput 0.974058
Reading from 20789: heap size 178 MB, throughput 0.996746
Equal recommendation: 3087 MB each
Reading from 20789: heap size 178 MB, throughput 0.99737
Reading from 20789: heap size 181 MB, throughput 0.998042
Reading from 20788: heap size 1053 MB, throughput 0.977714
Reading from 20789: heap size 181 MB, throughput 0.996804
Reading from 20789: heap size 184 MB, throughput 0.994472
Reading from 20789: heap size 184 MB, throughput 0.997281
Reading from 20788: heap size 1056 MB, throughput 0.977751
Reading from 20789: heap size 186 MB, throughput 0.996903
Reading from 20789: heap size 187 MB, throughput 0.99682
Reading from 20788: heap size 1049 MB, throughput 0.977499
Reading from 20789: heap size 190 MB, throughput 0.996887
Equal recommendation: 3087 MB each
Reading from 20789: heap size 190 MB, throughput 0.917272
Reading from 20789: heap size 197 MB, throughput 0.99661
Reading from 20789: heap size 197 MB, throughput 0.991788
Reading from 20788: heap size 1054 MB, throughput 0.970325
Reading from 20789: heap size 201 MB, throughput 0.99678
Reading from 20789: heap size 202 MB, throughput 0.997845
Reading from 20789: heap size 207 MB, throughput 0.994301
Reading from 20788: heap size 1054 MB, throughput 0.978041
Reading from 20789: heap size 207 MB, throughput 0.996594
Reading from 20789: heap size 213 MB, throughput 0.998333
Reading from 20788: heap size 1055 MB, throughput 0.980002
Reading from 20789: heap size 213 MB, throughput 0.998093
Equal recommendation: 3087 MB each
Reading from 20789: heap size 218 MB, throughput 0.997421
Reading from 20788: heap size 1059 MB, throughput 0.978383
Reading from 20789: heap size 218 MB, throughput 0.997207
Reading from 20789: heap size 222 MB, throughput 0.998088
Reading from 20789: heap size 223 MB, throughput 0.997821
Reading from 20788: heap size 1060 MB, throughput 0.975927
Reading from 20789: heap size 227 MB, throughput 0.99684
Reading from 20789: heap size 227 MB, throughput 0.997786
Reading from 20789: heap size 231 MB, throughput 0.994991
Reading from 20788: heap size 1063 MB, throughput 0.960206
Reading from 20789: heap size 232 MB, throughput 0.985976
Equal recommendation: 3087 MB each
Reading from 20789: heap size 237 MB, throughput 0.996735
Reading from 20788: heap size 1066 MB, throughput 0.977227
Reading from 20789: heap size 238 MB, throughput 0.998112
Reading from 20789: heap size 244 MB, throughput 0.998494
Reading from 20788: heap size 1069 MB, throughput 0.976993
Reading from 20789: heap size 245 MB, throughput 0.996161
Reading from 20789: heap size 252 MB, throughput 0.998298
Reading from 20789: heap size 252 MB, throughput 0.997093
Reading from 20788: heap size 1073 MB, throughput 0.978238
Equal recommendation: 3087 MB each
Reading from 20789: heap size 259 MB, throughput 0.998279
Reading from 20789: heap size 259 MB, throughput 0.996403
Reading from 20788: heap size 1077 MB, throughput 0.975961
Reading from 20789: heap size 265 MB, throughput 0.997755
Reading from 20789: heap size 265 MB, throughput 0.997875
Reading from 20789: heap size 270 MB, throughput 0.996527
Reading from 20788: heap size 1081 MB, throughput 0.601274
Reading from 20789: heap size 270 MB, throughput 0.996449
Reading from 20789: heap size 276 MB, throughput 0.997824
Equal recommendation: 3087 MB each
Reading from 20788: heap size 1182 MB, throughput 0.968487
Reading from 20789: heap size 277 MB, throughput 0.998336
Reading from 20789: heap size 282 MB, throughput 0.998649
Reading from 20788: heap size 1184 MB, throughput 0.989108
Reading from 20789: heap size 283 MB, throughput 0.9979
Reading from 20789: heap size 287 MB, throughput 0.998547
Reading from 20788: heap size 1190 MB, throughput 0.988778
Reading from 20789: heap size 288 MB, throughput 0.997947
Equal recommendation: 3087 MB each
Reading from 20789: heap size 292 MB, throughput 0.99853
Reading from 20788: heap size 1192 MB, throughput 0.987308
Reading from 20789: heap size 292 MB, throughput 0.998268
Reading from 20789: heap size 297 MB, throughput 0.998005
Reading from 20788: heap size 1191 MB, throughput 0.983984
Reading from 20789: heap size 297 MB, throughput 0.993789
Reading from 20789: heap size 301 MB, throughput 0.998305
Reading from 20788: heap size 1112 MB, throughput 0.983962
Reading from 20789: heap size 302 MB, throughput 0.998147
Equal recommendation: 3087 MB each
Reading from 20789: heap size 307 MB, throughput 0.9982
Reading from 20788: heap size 1184 MB, throughput 0.984603
Reading from 20789: heap size 307 MB, throughput 0.997829
Reading from 20789: heap size 312 MB, throughput 0.998488
Reading from 20788: heap size 1127 MB, throughput 0.984156
Reading from 20789: heap size 313 MB, throughput 0.998523
Reading from 20789: heap size 316 MB, throughput 0.99839
Reading from 20788: heap size 1176 MB, throughput 0.982795
Equal recommendation: 3087 MB each
Reading from 20789: heap size 317 MB, throughput 0.998288
Reading from 20789: heap size 322 MB, throughput 0.994802
Reading from 20788: heap size 1181 MB, throughput 0.979619
Reading from 20789: heap size 322 MB, throughput 0.997672
Reading from 20789: heap size 330 MB, throughput 0.997404
Reading from 20788: heap size 1182 MB, throughput 0.978738
Reading from 20789: heap size 330 MB, throughput 0.997502
Equal recommendation: 3087 MB each
Reading from 20788: heap size 1182 MB, throughput 0.981466
Reading from 20789: heap size 337 MB, throughput 0.998544
Reading from 20789: heap size 337 MB, throughput 0.997443
Reading from 20788: heap size 1184 MB, throughput 0.978273
Reading from 20789: heap size 343 MB, throughput 0.99769
Reading from 20789: heap size 343 MB, throughput 0.998394
Reading from 20788: heap size 1187 MB, throughput 0.981214
Reading from 20789: heap size 349 MB, throughput 0.99811
Equal recommendation: 3087 MB each
Reading from 20789: heap size 349 MB, throughput 0.994839
Reading from 20788: heap size 1189 MB, throughput 0.977066
Reading from 20789: heap size 355 MB, throughput 0.99825
Reading from 20788: heap size 1194 MB, throughput 0.975546
Reading from 20789: heap size 355 MB, throughput 0.997875
Reading from 20789: heap size 362 MB, throughput 0.997813
Reading from 20788: heap size 1199 MB, throughput 0.975953
Reading from 20789: heap size 362 MB, throughput 0.998206
Equal recommendation: 3087 MB each
Reading from 20788: heap size 1204 MB, throughput 0.976009
Reading from 20789: heap size 368 MB, throughput 0.997284
Reading from 20789: heap size 369 MB, throughput 0.997965
Reading from 20788: heap size 1208 MB, throughput 0.977363
Reading from 20789: heap size 376 MB, throughput 0.997704
Reading from 20789: heap size 376 MB, throughput 0.996482
Reading from 20788: heap size 1211 MB, throughput 0.973261
Equal recommendation: 3087 MB each
Reading from 20789: heap size 382 MB, throughput 0.998008
Reading from 20789: heap size 383 MB, throughput 0.994534
Reading from 20788: heap size 1215 MB, throughput 0.977534
Reading from 20789: heap size 391 MB, throughput 0.99876
Reading from 20788: heap size 1216 MB, throughput 0.976834
Reading from 20789: heap size 391 MB, throughput 0.998257
Equal recommendation: 3087 MB each
Reading from 20788: heap size 1221 MB, throughput 0.975866
Reading from 20789: heap size 397 MB, throughput 0.997521
Reading from 20789: heap size 399 MB, throughput 0.997816
Reading from 20789: heap size 406 MB, throughput 0.840882
Reading from 20788: heap size 1221 MB, throughput 0.963017
Reading from 20789: heap size 411 MB, throughput 0.997522
Reading from 20788: heap size 1225 MB, throughput 0.976255
Reading from 20789: heap size 424 MB, throughput 0.999206
Equal recommendation: 3087 MB each
Reading from 20789: heap size 424 MB, throughput 0.998927
Reading from 20789: heap size 435 MB, throughput 0.999085
Reading from 20788: heap size 1225 MB, throughput 0.641415
Reading from 20789: heap size 436 MB, throughput 0.998876
Reading from 20788: heap size 1343 MB, throughput 0.968234
Equal recommendation: 3087 MB each
Reading from 20789: heap size 444 MB, throughput 0.999014
Reading from 20789: heap size 445 MB, throughput 0.99582
Reading from 20788: heap size 1343 MB, throughput 0.985922
Reading from 20789: heap size 453 MB, throughput 0.998115
Reading from 20788: heap size 1352 MB, throughput 0.987516
Reading from 20789: heap size 454 MB, throughput 0.998607
Equal recommendation: 3087 MB each
Reading from 20789: heap size 462 MB, throughput 0.998581
Reading from 20788: heap size 1354 MB, throughput 0.986044
Reading from 20789: heap size 464 MB, throughput 0.9985
Reading from 20788: heap size 1353 MB, throughput 0.98641
Reading from 20789: heap size 472 MB, throughput 0.998468
Reading from 20789: heap size 473 MB, throughput 0.997179
Equal recommendation: 3087 MB each
Reading from 20789: heap size 481 MB, throughput 0.997831
Reading from 20789: heap size 482 MB, throughput 0.998363
Reading from 20789: heap size 492 MB, throughput 0.998486
Equal recommendation: 3087 MB each
Reading from 20789: heap size 492 MB, throughput 0.998529
Reading from 20788: heap size 1356 MB, throughput 0.993225
Reading from 20789: heap size 500 MB, throughput 0.99865
Reading from 20789: heap size 501 MB, throughput 0.996315
Reading from 20789: heap size 509 MB, throughput 0.99832
Equal recommendation: 3087 MB each
Reading from 20789: heap size 510 MB, throughput 0.998506
Reading from 20789: heap size 519 MB, throughput 0.998597
Reading from 20788: heap size 1340 MB, throughput 0.994044
Reading from 20789: heap size 520 MB, throughput 0.998595
Equal recommendation: 3087 MB each
Reading from 20789: heap size 529 MB, throughput 0.99897
Reading from 20789: heap size 529 MB, throughput 0.996273
Reading from 20788: heap size 1356 MB, throughput 0.983914
Reading from 20788: heap size 1345 MB, throughput 0.860003
Reading from 20788: heap size 1370 MB, throughput 0.744164
Reading from 20788: heap size 1369 MB, throughput 0.669192
Reading from 20788: heap size 1403 MB, throughput 0.611048
Reading from 20788: heap size 1427 MB, throughput 0.641034
Reading from 20789: heap size 537 MB, throughput 0.998805
Reading from 20788: heap size 1452 MB, throughput 0.619119
Reading from 20788: heap size 1478 MB, throughput 0.644721
Reading from 20788: heap size 1492 MB, throughput 0.64307
Reading from 20788: heap size 1523 MB, throughput 0.691754
Reading from 20788: heap size 1525 MB, throughput 0.177132
Reading from 20788: heap size 1565 MB, throughput 0.990614
Equal recommendation: 3087 MB each
Reading from 20789: heap size 538 MB, throughput 0.998555
Reading from 20788: heap size 1576 MB, throughput 0.995288
Reading from 20789: heap size 547 MB, throughput 0.99864
Reading from 20789: heap size 547 MB, throughput 0.998443
Reading from 20788: heap size 1613 MB, throughput 0.993861
Equal recommendation: 3087 MB each
Client 20789 died
Clients: 1
Reading from 20788: heap size 1618 MB, throughput 0.993044
Reading from 20788: heap size 1623 MB, throughput 0.992209
Recommendation: one client; give it all the memory
Reading from 20788: heap size 1633 MB, throughput 0.991162
Reading from 20788: heap size 1613 MB, throughput 0.990504
Reading from 20788: heap size 1628 MB, throughput 0.989803
Recommendation: one client; give it all the memory
Reading from 20788: heap size 1593 MB, throughput 0.9888
Reading from 20788: heap size 1380 MB, throughput 0.988254
Recommendation: one client; give it all the memory
Reading from 20788: heap size 1568 MB, throughput 0.987002
Reading from 20788: heap size 1407 MB, throughput 0.986412
Recommendation: one client; give it all the memory
Reading from 20788: heap size 1548 MB, throughput 0.984862
Reading from 20788: heap size 1433 MB, throughput 0.985088
Recommendation: one client; give it all the memory
Reading from 20788: heap size 1550 MB, throughput 0.984169
Reading from 20788: heap size 1556 MB, throughput 0.98317
Recommendation: one client; give it all the memory
Reading from 20788: heap size 1561 MB, throughput 0.983497
Reading from 20788: heap size 1561 MB, throughput 0.982735
Recommendation: one client; give it all the memory
Reading from 20788: heap size 1569 MB, throughput 0.981992
Reading from 20788: heap size 1569 MB, throughput 0.981507
Recommendation: one client; give it all the memory
Reading from 20788: heap size 1576 MB, throughput 0.981286
Reading from 20788: heap size 1576 MB, throughput 0.981571
Recommendation: one client; give it all the memory
Reading from 20788: heap size 1582 MB, throughput 0.982223
Reading from 20788: heap size 1583 MB, throughput 0.980373
Recommendation: one client; give it all the memory
Reading from 20788: heap size 1589 MB, throughput 0.981192
Recommendation: one client; give it all the memory
Reading from 20788: heap size 1589 MB, throughput 0.981161
Reading from 20788: heap size 1596 MB, throughput 0.981412
Recommendation: one client; give it all the memory
Reading from 20788: heap size 1596 MB, throughput 0.981592
Reading from 20788: heap size 1602 MB, throughput 0.981584
Recommendation: one client; give it all the memory
Reading from 20788: heap size 1603 MB, throughput 0.980768
Reading from 20788: heap size 1608 MB, throughput 0.981981
Recommendation: one client; give it all the memory
Reading from 20788: heap size 1609 MB, throughput 0.838173
Recommendation: one client; give it all the memory
Reading from 20788: heap size 1673 MB, throughput 0.996509
Reading from 20788: heap size 1674 MB, throughput 0.994445
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 20788: heap size 1690 MB, throughput 0.997797
Recommendation: one client; give it all the memory
Reading from 20788: heap size 1561 MB, throughput 0.98923
Reading from 20788: heap size 1690 MB, throughput 0.876367
Reading from 20788: heap size 1694 MB, throughput 0.788326
Reading from 20788: heap size 1719 MB, throughput 0.82497
Reading from 20788: heap size 1738 MB, throughput 0.803356
Reading from 20788: heap size 1779 MB, throughput 0.874641
Client 20788 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
