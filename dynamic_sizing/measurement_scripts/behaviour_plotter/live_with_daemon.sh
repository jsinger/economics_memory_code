#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") throughput_model total_mem [daemon_args...]"
    exit 1
}

test -z "${1}" && usage
test -z "${2}" && usage

THROUGHPUT_MODEL="${1}"
TOTAL_MEM="${2}"

function cleanup()
{
    test ! -z "${PLOT}" && kill "${PLOT}" &>/dev/null
    test ! -z "${TEE}" && kill "${TEE}" &>/dev/null
    test ! -z "${DAEMON}" && kill "${DAEMON}" &>/dev/null
    wait
    rm "${FIFO_TEE_PLOT}"
    rm "${FIFO_DAEMON_TEE}"
    exit
}

trap "cleanup" SIGINT SIGHUP SIGTERM


FIFO_TEE_PLOT="$(mktemp --dry-run)"
mkfifo "${FIFO_TEE_PLOT}"

FIFO_DAEMON_TEE="$(mktemp --dry-run)"
mkfifo "${FIFO_DAEMON_TEE}"

LOGFILE="$(readlink -f "./daemon_${THROUGHPUT_MODEL}_$(date '+%Y_%m_%d_%H_%M_%S').log")"

python "${DIR}/plot_behaviour_live.py" "${THROUGHPUT_MODEL}" < "${FIFO_TEE_PLOT}" &
PLOT="${!}"

tee "${LOGFILE}" < "${FIFO_DAEMON_TEE}" > "${FIFO_TEE_PLOT}" &
TEE="${!}"

"${DIR}/../../daemon/economemd" --reading-recv "${THROUGHPUT_MODEL}" --command-recv "${THROUGHPUT_MODEL}" --log-file "${FIFO_DAEMON_TEE}" --log-readings --log-functions "${@:3}" "${TOTAL_MEM}" &
DAEMON="${!}"

wait
cleanup
