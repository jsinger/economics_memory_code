#ifndef RECOMMENDATIONSENDER_HPP
#define RECOMMENDATIONSENDER_HPP

#include "Messages.hpp"
#include "SendSock.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
    //! Send recommendations from a daemon to a client.
    class RecommendationSender : public SendSock
    {
    public:
        RecommendationSender();

        bool sendRecommendation(const economem::Recommendation& r, pid_t dest);
    };
}

/*! @} */

#endif
