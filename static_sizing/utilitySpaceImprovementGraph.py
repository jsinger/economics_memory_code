#!/usr/bin/env python
# Generate an 'improvement' graph based on the averages of all utility space runs

import staticutility as su

d = su.UtilityAggregateDir(str(su.outputDir), su.allUtilitySpaceDirs())
d.plotImprovementGraph()
