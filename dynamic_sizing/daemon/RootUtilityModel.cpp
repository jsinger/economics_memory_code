#include <cmath>
#include <limits>
#include <sstream>
#include "RootUtilityModel.hpp"
#include "UtilityFunction.hpp"

const char* RootUtilityModel::NAME = "root";

double RootUtilityFunction::evaluate(double heapSize) const
{
    return c * std::pow(heapSize, e);
}

bool RootUtilityFunction::isValid() const
{
    return c > 0.0 && e > 0.0 && e < 1.0;
}

void RootUtilityFunction::forceValidity()
{
    // I'm really not sure about this. The problem is, the model only works
    // if 0 < e < 1, i.e. it's an open interval. Setting it to 0 or 1 makes
    // it break... so how to enforce this? Using numbers much smaller than
    // this gives nonsense results, presumably because of rounding errors.
    c = std::max(0.001, c);
    e = std::max(0.001, std::min(1.0 - 0.001, e));

    // c = std::max(std::numeric_limits<double>::min(), c);
    // e = std::max(std::numeric_limits<double>::min(), std::min(1.0 - std::numeric_limits<double>::min(), e));
}

std::string RootUtilityFunction::toString() const
{
    std::stringstream s;
    s << "U(h) = " << c << "*h^" << e;
    if (!isValid())
        s << " (INVALID)";
    return s.str();
}

std::string RootUtilityFunction::toPython() const
{
    std::stringstream s;
    s << "lambda h: " << c << " * (h ** " << e << ")";
    return s.str();
}

RootUtilityModel::RootUtilityModel(bool alwaysNumericOptimisation)
    : UtilityModel(),
      m_AlwaysNumericOptimisation(alwaysNumericOptimisation)
{

}

UtilityFunction* RootUtilityModel::calculateFuncFromData(ReadingIterator& it) const
{
    std::pair<double, double> p = UtilityModel::fitStraightLine(it, log, log);
    return new RootUtilityFunction(std::exp(p.first), p.second);
}

PartitionResult* RootUtilityModel::partitionMemory(unsigned long long totalMem, const std::vector<const UtilityFunction*>& functions, const std::vector<unsigned long long>& minHeaps) const
{
    if (functions.size() == 2)
    {
        PartitionResult* a = partitionMemoryAnalytic(totalMem, functions[0], functions[1], minHeaps[0], minHeaps[1]);
        PartitionResult* n = partitionMemoryNumeric(totalMem, functions, minHeaps);

        if (m_AlwaysNumericOptimisation)
        {
            printf("Analytic result (unused):\n");
            a->print(functions);
            delete a;
            return n;
        }
        else
        {
            printf("Numeric result (unused):\n");
            n->print(functions);
            delete n;
            return a;
        }
    }
    else
        return partitionMemoryNumeric(totalMem, functions, minHeaps);
}

PartitionResult* RootUtilityModel::partitionMemoryAnalytic(unsigned long long totalMem, const UtilityFunction* func1, const UtilityFunction* func2, unsigned long long minHeap1, unsigned long long minHeap2) const
{
    RootUtilityFunction* f1 = (RootUtilityFunction*)func1;
    RootUtilityFunction* f2 = (RootUtilityFunction*)func2;
    double b = f1->e;
    double d = f2->e;
    PartitionResult* r = new PartitionResult(2);

    double h1max = (b*totalMem)/(b+d);

    if (h1max < minHeap1)
        r->heapSizes[0] = minHeap1;
    else if (h1max > totalMem - minHeap2)
        r->heapSizes[0] = totalMem - minHeap2;
    else
        r->heapSizes[0] =  h1max;

    r->heapSizes[1] = totalMem - r->heapSizes[0];
    r->utility = combineRawUtilities({f1->evaluate(r->heapSizes[0]), f2->evaluate(r->heapSizes[1])});
    return r;
}

double RootUtilityModel::combineRawUtilities(const std::vector<double>& utilities) const
{
    double result = 1;
    for (double d : utilities)
        result *= d;
    return result;
}
