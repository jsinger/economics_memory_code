#include <cstring>
#include <pthread.h>
#include <signal.h>
#include "Mutex.hpp"
#include "SignalHandler.hpp"
#include "Thread.hpp"
#include "Utilities.hpp"

economem::Mutex economem::SignalHandler::s_CallbackLock;
std::function<void()> economem::SignalHandler::s_Callback = nullptr;
sigset_t economem::SignalHandler::s_Signals;


void economem::SignalHandler::init(std::function<void()> callback)
{
    s_CallbackLock.lock();

    static bool once = false;
    if (!once)
    {
        once = true;

        // (1) Set up a signal handler for THREAD_INTERRUPT_SIG so we can
        //     send it to threads without terminating the program. The
        //     signal handler itself does nothing; this is just a way to
        //     forcibly wake threads that are in blocking calls like read
        //     or recvfrom.
        struct sigaction a;
        memset(&a, 0, sizeof(a));
        a.sa_handler = dummySignalHandler;
        if (sigaction(economem::Thread::THREAD_INTERRUPT_SIG, &a, NULL) == -1)
            economem::die("Signal handler couldn't establish dummy signal handler");


        // (2) Set up a thread-safe way of catching external signals
        //     so that we can do cleanup when Ctrl-C'd or killed.
        s_Callback = callback;

        if (sigemptyset(&s_Signals) != 0
            || sigaddset(&s_Signals, SIGINT) != 0
            || sigaddset(&s_Signals, SIGTERM) != 0
            || sigaddset(&s_Signals, SIGHUP) != 0
            || sigaddset(&s_Signals, SIGPIPE) != 0)
            economem::die("Signal handler couldn't initialise signal mask");

        if (pthread_sigmask(SIG_BLOCK, &s_Signals, NULL) != 0)
            economem::die("Signal handler couldn't set signal mask");

        pthread_t thread;
        if (pthread_create(&thread, NULL, threadFunc, NULL) != 0)
            economem::die("Signal handler couldn't start the signal handling thread");
    }

    s_CallbackLock.unlock();
}

void* economem::SignalHandler::threadFunc(void* arg)
{
    (void)arg;
    int sig;

    while (true)
    {
        if (sigwait(&s_Signals, &sig) == 0)
        {
            s_CallbackLock.lock();
            if (s_Callback)
                s_Callback();
            s_CallbackLock.unlock();
            break;
        }
        else
            economem::warning("Something went wrong handling signals");
    }
    return NULL;
}

void economem::SignalHandler::dummySignalHandler(int i)
{
    (void)i;
}
