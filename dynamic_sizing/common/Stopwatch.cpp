#include <cstdlib>
#include "Stopwatch.hpp"
#include "Utilities.hpp"

economem::Stopwatch::Stopwatch() {}

void economem::Stopwatch::start()
{
    gettimeofday(&m_StartTime, NULL);
    m_StopTime = m_StartTime;
}

void economem::Stopwatch::stop()
{
    gettimeofday(&m_StopTime, NULL);
}

unsigned long long economem::Stopwatch::microseconds() const
{
    timeval diff;
    timersub(&m_StopTime, &m_StartTime, &diff);
    return diff.tv_sec * 1000000 + diff.tv_usec;
}

double economem::Stopwatch::seconds() const
{
    return microseconds() / 1000000.0;
}
