economemd
    total memory: 3312 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub45_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run09_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 23878: heap size 9 MB, throughput 0.991599
Clients: 1
Client 23878 has a minimum heap size of 276 MB
Reading from 23877: heap size 9 MB, throughput 0.984105
Clients: 2
Client 23877 has a minimum heap size of 276 MB
Reading from 23878: heap size 9 MB, throughput 0.980294
Reading from 23877: heap size 9 MB, throughput 0.974393
Reading from 23878: heap size 9 MB, throughput 0.964045
Reading from 23878: heap size 9 MB, throughput 0.944784
Reading from 23877: heap size 11 MB, throughput 0.970703
Reading from 23878: heap size 11 MB, throughput 0.983484
Reading from 23877: heap size 11 MB, throughput 0.96213
Reading from 23878: heap size 11 MB, throughput 0.981975
Reading from 23877: heap size 15 MB, throughput 0.849324
Reading from 23878: heap size 17 MB, throughput 0.909271
Reading from 23877: heap size 19 MB, throughput 0.953295
Reading from 23878: heap size 17 MB, throughput 0.415386
Reading from 23877: heap size 24 MB, throughput 0.961671
Reading from 23878: heap size 30 MB, throughput 0.942754
Reading from 23877: heap size 29 MB, throughput 0.894841
Reading from 23878: heap size 31 MB, throughput 0.817784
Reading from 23877: heap size 31 MB, throughput 0.317387
Reading from 23877: heap size 40 MB, throughput 0.908665
Reading from 23878: heap size 34 MB, throughput 0.371617
Reading from 23878: heap size 46 MB, throughput 0.861747
Reading from 23877: heap size 46 MB, throughput 0.774571
Reading from 23878: heap size 49 MB, throughput 0.77441
Reading from 23878: heap size 50 MB, throughput 0.206789
Reading from 23877: heap size 48 MB, throughput 0.213639
Reading from 23878: heap size 69 MB, throughput 0.781591
Reading from 23878: heap size 71 MB, throughput 0.794135
Reading from 23877: heap size 67 MB, throughput 0.40851
Reading from 23877: heap size 88 MB, throughput 0.83612
Reading from 23878: heap size 76 MB, throughput 0.380843
Reading from 23877: heap size 91 MB, throughput 0.864214
Reading from 23878: heap size 96 MB, throughput 0.608945
Reading from 23878: heap size 103 MB, throughput 0.735071
Reading from 23877: heap size 92 MB, throughput 0.805753
Reading from 23878: heap size 103 MB, throughput 0.629683
Reading from 23877: heap size 98 MB, throughput 0.252143
Reading from 23877: heap size 122 MB, throughput 0.807433
Reading from 23878: heap size 106 MB, throughput 0.160045
Reading from 23877: heap size 128 MB, throughput 0.822577
Reading from 23878: heap size 134 MB, throughput 0.693831
Reading from 23877: heap size 130 MB, throughput 0.706834
Reading from 23878: heap size 139 MB, throughput 0.824903
Reading from 23878: heap size 143 MB, throughput 0.608819
Reading from 23878: heap size 150 MB, throughput 0.633481
Reading from 23878: heap size 153 MB, throughput 0.62681
Reading from 23877: heap size 133 MB, throughput 0.160591
Reading from 23877: heap size 167 MB, throughput 0.634669
Reading from 23877: heap size 173 MB, throughput 0.67942
Reading from 23878: heap size 157 MB, throughput 0.119808
Reading from 23877: heap size 176 MB, throughput 0.735164
Reading from 23878: heap size 196 MB, throughput 0.684816
Reading from 23877: heap size 184 MB, throughput 0.779818
Reading from 23878: heap size 203 MB, throughput 0.746634
Reading from 23877: heap size 188 MB, throughput 0.703245
Reading from 23878: heap size 206 MB, throughput 0.922315
Reading from 23877: heap size 191 MB, throughput 0.905525
Reading from 23878: heap size 208 MB, throughput 0.886649
Reading from 23878: heap size 217 MB, throughput 0.706585
Reading from 23878: heap size 222 MB, throughput 0.551628
Reading from 23878: heap size 230 MB, throughput 0.568008
Reading from 23878: heap size 234 MB, throughput 0.507353
Reading from 23877: heap size 199 MB, throughput 0.351548
Reading from 23877: heap size 251 MB, throughput 0.653019
Reading from 23877: heap size 255 MB, throughput 0.665285
Reading from 23877: heap size 259 MB, throughput 0.598844
Reading from 23878: heap size 240 MB, throughput 0.0460837
Reading from 23877: heap size 262 MB, throughput 0.790468
Reading from 23878: heap size 275 MB, throughput 0.667994
Reading from 23877: heap size 268 MB, throughput 0.735907
Reading from 23877: heap size 270 MB, throughput 0.824188
Reading from 23878: heap size 279 MB, throughput 0.176148
Reading from 23877: heap size 273 MB, throughput 0.715851
Reading from 23878: heap size 322 MB, throughput 0.773603
Reading from 23877: heap size 275 MB, throughput 0.694655
Reading from 23878: heap size 324 MB, throughput 0.927177
Reading from 23878: heap size 323 MB, throughput 0.689454
Reading from 23877: heap size 281 MB, throughput 0.707973
Reading from 23878: heap size 326 MB, throughput 0.856698
Reading from 23877: heap size 284 MB, throughput 0.71782
Reading from 23878: heap size 321 MB, throughput 0.86431
Reading from 23878: heap size 324 MB, throughput 0.744351
Reading from 23877: heap size 288 MB, throughput 0.731989
Reading from 23878: heap size 319 MB, throughput 0.876203
Reading from 23878: heap size 322 MB, throughput 0.939868
Reading from 23878: heap size 318 MB, throughput 0.881803
Reading from 23878: heap size 321 MB, throughput 0.799888
Reading from 23878: heap size 315 MB, throughput 0.829307
Reading from 23878: heap size 318 MB, throughput 0.831204
Reading from 23877: heap size 290 MB, throughput 0.413766
Reading from 23878: heap size 313 MB, throughput 0.872744
Reading from 23877: heap size 327 MB, throughput 0.676025
Reading from 23878: heap size 316 MB, throughput 0.927448
Reading from 23877: heap size 331 MB, throughput 0.753727
Reading from 23878: heap size 311 MB, throughput 0.894177
Reading from 23877: heap size 335 MB, throughput 0.813907
Reading from 23878: heap size 314 MB, throughput 0.790935
Reading from 23877: heap size 335 MB, throughput 0.869127
Reading from 23878: heap size 315 MB, throughput 0.736296
Reading from 23878: heap size 316 MB, throughput 0.628375
Reading from 23878: heap size 321 MB, throughput 0.596201
Reading from 23877: heap size 337 MB, throughput 0.913537
Reading from 23878: heap size 322 MB, throughput 0.610169
Reading from 23878: heap size 329 MB, throughput 0.565709
Reading from 23877: heap size 339 MB, throughput 0.820848
Reading from 23877: heap size 340 MB, throughput 0.724471
Reading from 23877: heap size 341 MB, throughput 0.692271
Equal recommendation: 1656 MB each
Reading from 23877: heap size 344 MB, throughput 0.667293
Reading from 23878: heap size 330 MB, throughput 0.929701
Reading from 23877: heap size 345 MB, throughput 0.681911
Reading from 23877: heap size 350 MB, throughput 0.573944
Reading from 23877: heap size 351 MB, throughput 0.915339
Reading from 23878: heap size 334 MB, throughput 0.946295
Reading from 23877: heap size 356 MB, throughput 0.97234
Reading from 23878: heap size 336 MB, throughput 0.974797
Reading from 23877: heap size 358 MB, throughput 0.973333
Reading from 23878: heap size 339 MB, throughput 0.974921
Reading from 23878: heap size 341 MB, throughput 0.96031
Reading from 23877: heap size 357 MB, throughput 0.975659
Reading from 23878: heap size 339 MB, throughput 0.973732
Reading from 23877: heap size 361 MB, throughput 0.731462
Reading from 23878: heap size 341 MB, throughput 0.964097
Reading from 23877: heap size 410 MB, throughput 0.971344
Reading from 23878: heap size 337 MB, throughput 0.968292
Reading from 23877: heap size 411 MB, throughput 0.991972
Reading from 23878: heap size 340 MB, throughput 0.978276
Equal recommendation: 1656 MB each
Reading from 23877: heap size 418 MB, throughput 0.980286
Reading from 23878: heap size 338 MB, throughput 0.967702
Reading from 23878: heap size 340 MB, throughput 0.962121
Reading from 23877: heap size 418 MB, throughput 0.985622
Reading from 23878: heap size 342 MB, throughput 0.970529
Reading from 23877: heap size 418 MB, throughput 0.98822
Reading from 23878: heap size 342 MB, throughput 0.973094
Reading from 23877: heap size 420 MB, throughput 0.983677
Reading from 23878: heap size 344 MB, throughput 0.971323
Reading from 23878: heap size 345 MB, throughput 0.966343
Reading from 23877: heap size 414 MB, throughput 0.985175
Reading from 23878: heap size 347 MB, throughput 0.974403
Reading from 23877: heap size 382 MB, throughput 0.983658
Equal recommendation: 1656 MB each
Reading from 23878: heap size 348 MB, throughput 0.985773
Reading from 23878: heap size 348 MB, throughput 0.974564
Reading from 23877: heap size 412 MB, throughput 0.97632
Reading from 23878: heap size 350 MB, throughput 0.739649
Reading from 23878: heap size 344 MB, throughput 0.778572
Reading from 23878: heap size 350 MB, throughput 0.781929
Reading from 23878: heap size 356 MB, throughput 0.723798
Reading from 23878: heap size 359 MB, throughput 0.878837
Reading from 23877: heap size 414 MB, throughput 0.982769
Reading from 23877: heap size 417 MB, throughput 0.831345
Reading from 23877: heap size 419 MB, throughput 0.768544
Reading from 23877: heap size 426 MB, throughput 0.79492
Reading from 23878: heap size 366 MB, throughput 0.98528
Reading from 23877: heap size 433 MB, throughput 0.895108
Reading from 23878: heap size 367 MB, throughput 0.98374
Reading from 23877: heap size 439 MB, throughput 0.985974
Reading from 23878: heap size 367 MB, throughput 0.985175
Reading from 23877: heap size 443 MB, throughput 0.98394
Reading from 23878: heap size 369 MB, throughput 0.982986
Reading from 23877: heap size 441 MB, throughput 0.984758
Reading from 23878: heap size 366 MB, throughput 0.983092
Equal recommendation: 1656 MB each
Reading from 23878: heap size 369 MB, throughput 0.978683
Reading from 23877: heap size 445 MB, throughput 0.983286
Reading from 23878: heap size 367 MB, throughput 0.982852
Reading from 23877: heap size 441 MB, throughput 0.982524
Reading from 23878: heap size 369 MB, throughput 0.97956
Reading from 23877: heap size 445 MB, throughput 0.982021
Reading from 23878: heap size 366 MB, throughput 0.724125
Reading from 23877: heap size 442 MB, throughput 0.986805
Reading from 23878: heap size 407 MB, throughput 0.974327
Reading from 23877: heap size 445 MB, throughput 0.985347
Reading from 23878: heap size 404 MB, throughput 0.987837
Reading from 23877: heap size 442 MB, throughput 0.98218
Reading from 23878: heap size 407 MB, throughput 0.988114
Equal recommendation: 1656 MB each
Reading from 23877: heap size 444 MB, throughput 0.979976
Reading from 23878: heap size 411 MB, throughput 0.988161
Reading from 23878: heap size 385 MB, throughput 0.971245
Reading from 23878: heap size 406 MB, throughput 0.869593
Reading from 23878: heap size 408 MB, throughput 0.830151
Reading from 23878: heap size 413 MB, throughput 0.849517
Reading from 23877: heap size 446 MB, throughput 0.988074
Reading from 23877: heap size 446 MB, throughput 0.90862
Reading from 23878: heap size 416 MB, throughput 0.959127
Reading from 23877: heap size 450 MB, throughput 0.776365
Reading from 23877: heap size 450 MB, throughput 0.911252
Reading from 23878: heap size 422 MB, throughput 0.981068
Reading from 23877: heap size 457 MB, throughput 0.989497
Reading from 23878: heap size 424 MB, throughput 0.988103
Reading from 23877: heap size 458 MB, throughput 0.989857
Reading from 23878: heap size 422 MB, throughput 0.991778
Equal recommendation: 1656 MB each
Reading from 23877: heap size 463 MB, throughput 0.988831
Reading from 23878: heap size 425 MB, throughput 0.987315
Reading from 23878: heap size 422 MB, throughput 0.988039
Reading from 23877: heap size 464 MB, throughput 0.989803
Reading from 23878: heap size 424 MB, throughput 0.985537
Reading from 23877: heap size 463 MB, throughput 0.988605
Reading from 23878: heap size 425 MB, throughput 0.98182
Reading from 23877: heap size 466 MB, throughput 0.985337
Reading from 23878: heap size 426 MB, throughput 0.982096
Reading from 23877: heap size 463 MB, throughput 0.985917
Reading from 23878: heap size 428 MB, throughput 0.979045
Equal recommendation: 1656 MB each
Reading from 23878: heap size 428 MB, throughput 0.979612
Reading from 23877: heap size 466 MB, throughput 0.983898
Reading from 23878: heap size 430 MB, throughput 0.989566
Reading from 23878: heap size 431 MB, throughput 0.912144
Reading from 23878: heap size 430 MB, throughput 0.865955
Reading from 23878: heap size 433 MB, throughput 0.830278
Reading from 23877: heap size 468 MB, throughput 0.992559
Reading from 23878: heap size 439 MB, throughput 0.983588
Reading from 23877: heap size 468 MB, throughput 0.927962
Reading from 23877: heap size 469 MB, throughput 0.910514
Reading from 23877: heap size 471 MB, throughput 0.91787
Reading from 23878: heap size 440 MB, throughput 0.981271
Reading from 23877: heap size 478 MB, throughput 0.993294
Reading from 23878: heap size 444 MB, throughput 0.988652
Reading from 23877: heap size 478 MB, throughput 0.99005
Equal recommendation: 1656 MB each
Reading from 23878: heap size 445 MB, throughput 0.985237
Reading from 23877: heap size 480 MB, throughput 0.98648
Reading from 23878: heap size 444 MB, throughput 0.981866
Reading from 23878: heap size 447 MB, throughput 0.98491
Reading from 23877: heap size 482 MB, throughput 0.987227
Reading from 23878: heap size 445 MB, throughput 0.984787
Reading from 23877: heap size 481 MB, throughput 0.988128
Reading from 23878: heap size 447 MB, throughput 0.982935
Reading from 23878: heap size 449 MB, throughput 0.980542
Reading from 23877: heap size 483 MB, throughput 0.987672
Equal recommendation: 1656 MB each
Reading from 23878: heap size 449 MB, throughput 0.97623
Reading from 23877: heap size 485 MB, throughput 0.987115
Reading from 23878: heap size 451 MB, throughput 0.988968
Reading from 23878: heap size 452 MB, throughput 0.893075
Reading from 23878: heap size 452 MB, throughput 0.878232
Reading from 23878: heap size 455 MB, throughput 0.955126
Reading from 23877: heap size 485 MB, throughput 0.993946
Reading from 23878: heap size 462 MB, throughput 0.979498
Reading from 23877: heap size 487 MB, throughput 0.915967
Reading from 23877: heap size 488 MB, throughput 0.887023
Reading from 23877: heap size 494 MB, throughput 0.984078
Reading from 23878: heap size 463 MB, throughput 0.992838
Reading from 23877: heap size 495 MB, throughput 0.992145
Equal recommendation: 1656 MB each
Reading from 23878: heap size 463 MB, throughput 0.992669
Reading from 23877: heap size 499 MB, throughput 0.991702
Reading from 23878: heap size 465 MB, throughput 0.988222
Reading from 23878: heap size 464 MB, throughput 0.988099
Reading from 23877: heap size 501 MB, throughput 0.988807
Reading from 23878: heap size 466 MB, throughput 0.982639
Reading from 23877: heap size 499 MB, throughput 0.992961
Reading from 23878: heap size 467 MB, throughput 0.99008
Equal recommendation: 1656 MB each
Reading from 23877: heap size 502 MB, throughput 0.988282
Reading from 23878: heap size 467 MB, throughput 0.983106
Reading from 23878: heap size 469 MB, throughput 0.990085
Reading from 23877: heap size 502 MB, throughput 0.987574
Reading from 23878: heap size 470 MB, throughput 0.892597
Reading from 23878: heap size 470 MB, throughput 0.893769
Reading from 23878: heap size 472 MB, throughput 0.978451
Reading from 23877: heap size 503 MB, throughput 0.993632
Reading from 23878: heap size 480 MB, throughput 0.991777
Reading from 23877: heap size 505 MB, throughput 0.936819
Reading from 23877: heap size 506 MB, throughput 0.89521
Reading from 23877: heap size 511 MB, throughput 0.988776
Reading from 23878: heap size 480 MB, throughput 0.990217
Equal recommendation: 1656 MB each
Reading from 23877: heap size 513 MB, throughput 0.994543
Reading from 23878: heap size 480 MB, throughput 0.990281
Reading from 23878: heap size 482 MB, throughput 0.98816
Reading from 23877: heap size 515 MB, throughput 0.990962
Reading from 23878: heap size 479 MB, throughput 0.988114
Reading from 23877: heap size 517 MB, throughput 0.990703
Reading from 23878: heap size 482 MB, throughput 0.986323
Equal recommendation: 1656 MB each
Reading from 23877: heap size 516 MB, throughput 0.991835
Reading from 23878: heap size 484 MB, throughput 0.985158
Reading from 23877: heap size 518 MB, throughput 0.988079
Reading from 23878: heap size 484 MB, throughput 0.991615
Reading from 23878: heap size 486 MB, throughput 0.918539
Reading from 23878: heap size 487 MB, throughput 0.886218
Reading from 23878: heap size 492 MB, throughput 0.987171
Reading from 23877: heap size 521 MB, throughput 0.993539
Reading from 23877: heap size 521 MB, throughput 0.977713
Reading from 23877: heap size 520 MB, throughput 0.911529
Reading from 23878: heap size 494 MB, throughput 0.990713
Reading from 23877: heap size 523 MB, throughput 0.988686
Equal recommendation: 1656 MB each
Reading from 23878: heap size 497 MB, throughput 0.990446
Reading from 23877: heap size 530 MB, throughput 0.994441
Reading from 23878: heap size 498 MB, throughput 0.989383
Reading from 23877: heap size 531 MB, throughput 0.991949
Reading from 23878: heap size 497 MB, throughput 0.987343
Reading from 23877: heap size 530 MB, throughput 0.991617
Reading from 23878: heap size 499 MB, throughput 0.992056
Equal recommendation: 1656 MB each
Reading from 23878: heap size 500 MB, throughput 0.988035
Reading from 23877: heap size 532 MB, throughput 0.992157
Reading from 23878: heap size 501 MB, throughput 0.995466
Reading from 23877: heap size 532 MB, throughput 0.989436
Reading from 23878: heap size 503 MB, throughput 0.931623
Reading from 23878: heap size 504 MB, throughput 0.879572
Reading from 23878: heap size 508 MB, throughput 0.986396
Reading from 23877: heap size 533 MB, throughput 0.993833
Reading from 23877: heap size 535 MB, throughput 0.931858
Reading from 23877: heap size 536 MB, throughput 0.917684
Reading from 23878: heap size 510 MB, throughput 0.992585
Equal recommendation: 1656 MB each
Reading from 23877: heap size 541 MB, throughput 0.994354
Reading from 23878: heap size 513 MB, throughput 0.991886
Reading from 23878: heap size 515 MB, throughput 0.989574
Reading from 23877: heap size 543 MB, throughput 0.992803
Reading from 23878: heap size 513 MB, throughput 0.987899
Reading from 23877: heap size 545 MB, throughput 0.991396
Reading from 23878: heap size 515 MB, throughput 0.990678
Equal recommendation: 1656 MB each
Reading from 23877: heap size 547 MB, throughput 0.990729
Reading from 23878: heap size 517 MB, throughput 0.986379
Reading from 23878: heap size 518 MB, throughput 0.986231
Reading from 23878: heap size 519 MB, throughput 0.907266
Reading from 23877: heap size 546 MB, throughput 0.990135
Reading from 23878: heap size 522 MB, throughput 0.95896
Reading from 23878: heap size 528 MB, throughput 0.994553
Reading from 23877: heap size 548 MB, throughput 0.995968
Reading from 23877: heap size 551 MB, throughput 0.979503
Reading from 23877: heap size 551 MB, throughput 0.911264
Equal recommendation: 1656 MB each
Reading from 23878: heap size 529 MB, throughput 0.991823
Reading from 23877: heap size 555 MB, throughput 0.994139
Reading from 23878: heap size 529 MB, throughput 0.990683
Reading from 23877: heap size 557 MB, throughput 0.993249
Reading from 23878: heap size 531 MB, throughput 0.989741
Reading from 23877: heap size 559 MB, throughput 0.992159
Reading from 23878: heap size 530 MB, throughput 0.992472
Equal recommendation: 1656 MB each
Reading from 23878: heap size 531 MB, throughput 0.984872
Reading from 23877: heap size 561 MB, throughput 0.984108
Reading from 23878: heap size 533 MB, throughput 0.988038
Reading from 23878: heap size 534 MB, throughput 0.904533
Reading from 23878: heap size 537 MB, throughput 0.980842
Reading from 23877: heap size 561 MB, throughput 0.988828
Reading from 23878: heap size 539 MB, throughput 0.992722
Equal recommendation: 1656 MB each
Reading from 23877: heap size 562 MB, throughput 0.991241
Reading from 23877: heap size 566 MB, throughput 0.966639
Reading from 23877: heap size 566 MB, throughput 0.956895
Reading from 23878: heap size 542 MB, throughput 0.992525
Reading from 23878: heap size 543 MB, throughput 0.990129
Reading from 23877: heap size 572 MB, throughput 0.995853
Reading from 23878: heap size 542 MB, throughput 0.990841
Reading from 23877: heap size 572 MB, throughput 0.991599
Equal recommendation: 1656 MB each
Reading from 23878: heap size 544 MB, throughput 0.986952
Reading from 23877: heap size 573 MB, throughput 0.991666
Reading from 23878: heap size 546 MB, throughput 0.994449
Reading from 23878: heap size 546 MB, throughput 0.921614
Reading from 23878: heap size 546 MB, throughput 0.917737
Reading from 23877: heap size 575 MB, throughput 0.9917
Reading from 23878: heap size 548 MB, throughput 0.991433
Equal recommendation: 1656 MB each
Reading from 23877: heap size 576 MB, throughput 0.989104
Reading from 23878: heap size 555 MB, throughput 0.991952
Reading from 23877: heap size 576 MB, throughput 0.990595
Reading from 23877: heap size 578 MB, throughput 0.919484
Reading from 23878: heap size 556 MB, throughput 0.990989
Reading from 23877: heap size 580 MB, throughput 0.991281
Reading from 23878: heap size 555 MB, throughput 0.990196
Reading from 23877: heap size 586 MB, throughput 0.993018
Equal recommendation: 1656 MB each
Reading from 23878: heap size 558 MB, throughput 0.99049
Reading from 23877: heap size 587 MB, throughput 0.992418
Reading from 23878: heap size 560 MB, throughput 0.991868
Reading from 23878: heap size 560 MB, throughput 0.960288
Reading from 23878: heap size 562 MB, throughput 0.90033
Reading from 23877: heap size 587 MB, throughput 0.987958
Reading from 23878: heap size 564 MB, throughput 0.992512
Equal recommendation: 1656 MB each
Reading from 23877: heap size 589 MB, throughput 0.992854
Reading from 23878: heap size 568 MB, throughput 0.991956
Reading from 23877: heap size 592 MB, throughput 0.994394
Reading from 23877: heap size 592 MB, throughput 0.910262
Reading from 23877: heap size 593 MB, throughput 0.990167
Reading from 23878: heap size 570 MB, throughput 0.992289
Reading from 23877: heap size 595 MB, throughput 0.992744
Reading from 23878: heap size 569 MB, throughput 0.992194
Equal recommendation: 1656 MB each
Reading from 23878: heap size 571 MB, throughput 0.990969
Reading from 23877: heap size 598 MB, throughput 0.992203
Reading from 23878: heap size 574 MB, throughput 0.995505
Reading from 23878: heap size 575 MB, throughput 0.929093
Reading from 23877: heap size 600 MB, throughput 0.991423
Reading from 23878: heap size 575 MB, throughput 0.989911
Equal recommendation: 1656 MB each
Reading from 23877: heap size 600 MB, throughput 0.990801
Reading from 23878: heap size 578 MB, throughput 0.993958
Reading from 23877: heap size 601 MB, throughput 0.990958
Reading from 23877: heap size 603 MB, throughput 0.475977
Reading from 23878: heap size 579 MB, throughput 0.991407
Reading from 23877: heap size 630 MB, throughput 0.996385
Reading from 23878: heap size 581 MB, throughput 0.991196
Equal recommendation: 1656 MB each
Reading from 23878: heap size 580 MB, throughput 0.992713
Reading from 23877: heap size 635 MB, throughput 0.995279
Reading from 23878: heap size 581 MB, throughput 0.994816
Reading from 23877: heap size 638 MB, throughput 0.994118
Reading from 23878: heap size 582 MB, throughput 0.973311
Client 23878 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 23877: heap size 640 MB, throughput 0.99476
Reading from 23877: heap size 642 MB, throughput 0.992957
Reading from 23877: heap size 643 MB, throughput 0.983458
Client 23877 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
