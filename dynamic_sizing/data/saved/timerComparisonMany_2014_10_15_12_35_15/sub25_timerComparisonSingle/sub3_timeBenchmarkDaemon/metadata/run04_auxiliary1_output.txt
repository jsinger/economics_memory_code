economemd
    total memory: 7518 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub25_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 27573: heap size 9 MB, throughput 0.991718
Clients: 1
Client 27573 has a minimum heap size of 30 MB
Reading from 27572: heap size 9 MB, throughput 0.992152
Clients: 2
Client 27572 has a minimum heap size of 1223 MB
Reading from 27573: heap size 9 MB, throughput 0.987252
Reading from 27573: heap size 9 MB, throughput 0.981378
Reading from 27572: heap size 9 MB, throughput 0.987871
Reading from 27573: heap size 9 MB, throughput 0.977915
Reading from 27573: heap size 11 MB, throughput 0.963093
Reading from 27573: heap size 11 MB, throughput 0.926899
Reading from 27573: heap size 15 MB, throughput 0.893018
Reading from 27573: heap size 16 MB, throughput 0.861418
Reading from 27573: heap size 23 MB, throughput 0.858546
Reading from 27572: heap size 9 MB, throughput 0.981608
Reading from 27573: heap size 23 MB, throughput 0.861537
Reading from 27573: heap size 32 MB, throughput 0.890949
Reading from 27573: heap size 32 MB, throughput 0.88925
Reading from 27573: heap size 47 MB, throughput 0.92434
Reading from 27573: heap size 47 MB, throughput 0.916045
Reading from 27572: heap size 9 MB, throughput 0.971958
Reading from 27573: heap size 70 MB, throughput 0.939746
Reading from 27573: heap size 70 MB, throughput 0.938078
Reading from 27573: heap size 107 MB, throughput 0.842558
Reading from 27573: heap size 107 MB, throughput 0.885541
Reading from 27572: heap size 11 MB, throughput 0.971877
Reading from 27572: heap size 11 MB, throughput 0.977956
Reading from 27572: heap size 17 MB, throughput 0.955993
Reading from 27572: heap size 17 MB, throughput 0.857179
Reading from 27572: heap size 30 MB, throughput 0.7049
Reading from 27572: heap size 31 MB, throughput 0.299443
Reading from 27572: heap size 34 MB, throughput 0.646852
Reading from 27573: heap size 147 MB, throughput 0.965481
Reading from 27572: heap size 48 MB, throughput 0.617986
Reading from 27572: heap size 51 MB, throughput 0.498381
Reading from 27573: heap size 153 MB, throughput 0.888149
Reading from 27572: heap size 52 MB, throughput 0.59989
Reading from 27572: heap size 76 MB, throughput 0.524863
Reading from 27572: heap size 76 MB, throughput 0.556514
Reading from 27572: heap size 102 MB, throughput 0.72128
Reading from 27572: heap size 102 MB, throughput 0.521361
Reading from 27573: heap size 209 MB, throughput 0.971868
Reading from 27572: heap size 104 MB, throughput 0.251786
Reading from 27572: heap size 108 MB, throughput 0.546417
Reading from 27572: heap size 135 MB, throughput 0.0374396
Reading from 27572: heap size 139 MB, throughput 0.0622308
Reading from 27572: heap size 142 MB, throughput 0.754835
Reading from 27573: heap size 218 MB, throughput 0.939478
Reading from 27572: heap size 144 MB, throughput 0.32863
Reading from 27572: heap size 182 MB, throughput 0.68769
Reading from 27572: heap size 184 MB, throughput 0.65677
Reading from 27572: heap size 185 MB, throughput 0.615165
Reading from 27573: heap size 255 MB, throughput 0.98599
Reading from 27572: heap size 193 MB, throughput 0.659952
Reading from 27572: heap size 196 MB, throughput 0.689278
Reading from 27572: heap size 202 MB, throughput 0.646093
Reading from 27572: heap size 206 MB, throughput 0.650378
Reading from 27572: heap size 215 MB, throughput 0.424792
Reading from 27573: heap size 255 MB, throughput 0.958167
Reading from 27572: heap size 255 MB, throughput 0.518563
Reading from 27572: heap size 264 MB, throughput 0.538802
Reading from 27572: heap size 267 MB, throughput 0.621594
Reading from 27573: heap size 301 MB, throughput 0.983872
Reading from 27572: heap size 270 MB, throughput 0.302403
Reading from 27572: heap size 315 MB, throughput 0.626887
Reading from 27572: heap size 316 MB, throughput 0.597871
Reading from 27572: heap size 317 MB, throughput 0.588108
Reading from 27572: heap size 317 MB, throughput 0.586074
Reading from 27573: heap size 306 MB, throughput 0.987038
Reading from 27572: heap size 322 MB, throughput 0.595607
Reading from 27572: heap size 326 MB, throughput 0.371813
Reading from 27572: heap size 370 MB, throughput 0.566626
Reading from 27572: heap size 374 MB, throughput 0.55127
Reading from 27572: heap size 375 MB, throughput 0.515994
Reading from 27573: heap size 329 MB, throughput 0.958495
Reading from 27572: heap size 378 MB, throughput 0.518918
Reading from 27572: heap size 383 MB, throughput 0.606853
Reading from 27572: heap size 386 MB, throughput 0.610214
Numeric result:
Recommendation: 2 clients, utility 0.792594:
    h1: 6295 MB (U(h) = 0.863177*h^0.0172639)
    h2: 1223 MB (U(h) = 0.783931*h^0.001)
Recommendation: 2 clients, utility 0.792594:
    h1: 6295 MB (U(h) = 0.863177*h^0.0172639)
    h2: 1223 MB (U(h) = 0.783931*h^0.001)
Reading from 27573: heap size 336 MB, throughput 0.988813
Reading from 27572: heap size 391 MB, throughput 0.354955
Reading from 27572: heap size 441 MB, throughput 0.527907
Reading from 27572: heap size 449 MB, throughput 0.594648
Reading from 27572: heap size 450 MB, throughput 0.595084
Reading from 27572: heap size 453 MB, throughput 0.612418
Reading from 27572: heap size 460 MB, throughput 0.594221
Reading from 27572: heap size 466 MB, throughput 0.530198
Reading from 27573: heap size 370 MB, throughput 0.990662
Reading from 27572: heap size 474 MB, throughput 0.500555
Reading from 27573: heap size 370 MB, throughput 0.981665
Reading from 27572: heap size 479 MB, throughput 0.272558
Reading from 27572: heap size 539 MB, throughput 0.419235
Reading from 27572: heap size 543 MB, throughput 0.467955
Reading from 27572: heap size 545 MB, throughput 0.474732
Reading from 27573: heap size 392 MB, throughput 0.980182
Reading from 27572: heap size 547 MB, throughput 0.226053
Reading from 27572: heap size 606 MB, throughput 0.491186
Reading from 27572: heap size 607 MB, throughput 0.466458
Reading from 27572: heap size 611 MB, throughput 0.471312
Reading from 27573: heap size 397 MB, throughput 0.98178
Reading from 27572: heap size 614 MB, throughput 0.461457
Reading from 27572: heap size 619 MB, throughput 0.468777
Reading from 27573: heap size 429 MB, throughput 0.974474
Reading from 27572: heap size 625 MB, throughput 0.272837
Reading from 27572: heap size 693 MB, throughput 0.586328
Reading from 27573: heap size 432 MB, throughput 0.978515
Reading from 27572: heap size 696 MB, throughput 0.74898
Reading from 27572: heap size 700 MB, throughput 0.816925
Reading from 27573: heap size 465 MB, throughput 0.980714
Numeric result:
Recommendation: 2 clients, utility 0.732608:
    h1: 6295 MB (U(h) = 0.8486*h^0.0223432)
    h2: 1223 MB (U(h) = 0.705017*h^0.001)
Recommendation: 2 clients, utility 0.732608:
    h1: 6295 MB (U(h) = 0.8486*h^0.0223432)
    h2: 1223 MB (U(h) = 0.705017*h^0.001)
Reading from 27572: heap size 696 MB, throughput 0.779921
Reading from 27572: heap size 709 MB, throughput 0.71992
Reading from 27573: heap size 467 MB, throughput 0.98306
Reading from 27572: heap size 717 MB, throughput 0.49183
Reading from 27572: heap size 794 MB, throughput 0.467915
Reading from 27572: heap size 784 MB, throughput 0.392557
Reading from 27572: heap size 793 MB, throughput 0.377161
Reading from 27572: heap size 787 MB, throughput 0.717358
Reading from 27573: heap size 499 MB, throughput 0.981209
Reading from 27572: heap size 791 MB, throughput 0.281388
Reading from 27572: heap size 867 MB, throughput 0.518249
Reading from 27572: heap size 869 MB, throughput 0.647168
Reading from 27573: heap size 500 MB, throughput 0.983009
Reading from 27572: heap size 877 MB, throughput 0.667555
Reading from 27572: heap size 878 MB, throughput 0.733372
Reading from 27572: heap size 883 MB, throughput 0.807475
Reading from 27572: heap size 884 MB, throughput 0.795386
Reading from 27573: heap size 538 MB, throughput 0.975543
Reading from 27572: heap size 889 MB, throughput 0.479017
Reading from 27572: heap size 975 MB, throughput 0.678478
Reading from 27572: heap size 971 MB, throughput 0.693371
Reading from 27572: heap size 975 MB, throughput 0.728865
Reading from 27573: heap size 539 MB, throughput 0.980865
Reading from 27572: heap size 971 MB, throughput 0.755409
Reading from 27572: heap size 975 MB, throughput 0.790632
Reading from 27572: heap size 968 MB, throughput 0.810565
Reading from 27572: heap size 973 MB, throughput 0.803356
Reading from 27572: heap size 969 MB, throughput 0.824079
Reading from 27573: heap size 576 MB, throughput 0.984966
Reading from 27572: heap size 973 MB, throughput 0.925189
Numeric result:
Recommendation: 2 clients, utility 0.541447:
    h1: 6295 MB (U(h) = 0.846229*h^0.0231207)
    h2: 1223 MB (U(h) = 0.518974*h^0.001)
Recommendation: 2 clients, utility 0.541447:
    h1: 6295 MB (U(h) = 0.846229*h^0.0231207)
    h2: 1223 MB (U(h) = 0.518974*h^0.001)
Reading from 27572: heap size 975 MB, throughput 0.963398
Reading from 27573: heap size 577 MB, throughput 0.983853
Reading from 27572: heap size 977 MB, throughput 0.936683
Reading from 27572: heap size 968 MB, throughput 0.891018
Reading from 27572: heap size 973 MB, throughput 0.85567
Reading from 27572: heap size 961 MB, throughput 0.845219
Reading from 27572: heap size 968 MB, throughput 0.818263
Reading from 27572: heap size 960 MB, throughput 0.794328
Reading from 27572: heap size 965 MB, throughput 0.79319
Reading from 27573: heap size 607 MB, throughput 0.987165
Reading from 27572: heap size 962 MB, throughput 0.840938
Reading from 27572: heap size 966 MB, throughput 0.861147
Reading from 27572: heap size 973 MB, throughput 0.823245
Reading from 27572: heap size 975 MB, throughput 0.761514
Reading from 27572: heap size 979 MB, throughput 0.73423
Reading from 27572: heap size 996 MB, throughput 0.693414
Reading from 27573: heap size 609 MB, throughput 0.987195
Reading from 27572: heap size 1011 MB, throughput 0.676104
Reading from 27573: heap size 636 MB, throughput 0.982204
Reading from 27572: heap size 1021 MB, throughput 0.659847
Reading from 27572: heap size 1125 MB, throughput 0.604448
Reading from 27572: heap size 1131 MB, throughput 0.682137
Reading from 27572: heap size 1130 MB, throughput 0.705013
Reading from 27572: heap size 1136 MB, throughput 0.718321
Reading from 27573: heap size 638 MB, throughput 0.98382
Reading from 27572: heap size 1139 MB, throughput 0.923406
Numeric result:
Recommendation: 2 clients, utility 0.639051:
    h1: 2346.43 MB (U(h) = 0.845213*h^0.0234377)
    h2: 5171.57 MB (U(h) = 0.405597*h^0.0515615)
Recommendation: 2 clients, utility 0.639051:
    h1: 2349.42 MB (U(h) = 0.845213*h^0.0234377)
    h2: 5168.58 MB (U(h) = 0.405597*h^0.0515615)
Reading from 27573: heap size 668 MB, throughput 0.989008
Reading from 27572: heap size 1144 MB, throughput 0.962558
Reading from 27573: heap size 670 MB, throughput 0.98826
Reading from 27573: heap size 700 MB, throughput 0.98821
Reading from 27572: heap size 1150 MB, throughput 0.970701
Reading from 27573: heap size 701 MB, throughput 0.990427
Reading from 27572: heap size 1153 MB, throughput 0.970441
Reading from 27573: heap size 725 MB, throughput 0.992855
Numeric result:
Recommendation: 2 clients, utility 0.674833:
    h1: 1998.71 MB (U(h) = 0.84392*h^0.023825)
    h2: 5519.29 MB (U(h) = 0.378394*h^0.0658254)
Recommendation: 2 clients, utility 0.674833:
    h1: 1997.94 MB (U(h) = 0.84392*h^0.023825)
    h2: 5520.06 MB (U(h) = 0.378394*h^0.0658254)
Reading from 27573: heap size 729 MB, throughput 0.991074
Reading from 27572: heap size 1164 MB, throughput 0.972675
Reading from 27573: heap size 754 MB, throughput 0.990145
Reading from 27572: heap size 1168 MB, throughput 0.967689
Reading from 27573: heap size 754 MB, throughput 0.989277
Reading from 27573: heap size 785 MB, throughput 0.986258
Reading from 27572: heap size 1178 MB, throughput 0.920209
Numeric result:
Recommendation: 2 clients, utility 0.830822:
    h1: 1064.25 MB (U(h) = 0.843815*h^0.0238571)
    h2: 6453.75 MB (U(h) = 0.234304*h^0.144696)
Recommendation: 2 clients, utility 0.830822:
    h1: 1064.1 MB (U(h) = 0.843815*h^0.0238571)
    h2: 6453.9 MB (U(h) = 0.234304*h^0.144696)
Reading from 27573: heap size 784 MB, throughput 0.989375
Reading from 27572: heap size 1257 MB, throughput 0.981344
Reading from 27573: heap size 810 MB, throughput 0.990776
Reading from 27572: heap size 1257 MB, throughput 0.985851
Reading from 27573: heap size 814 MB, throughput 0.991232
Reading from 27573: heap size 838 MB, throughput 0.993931
Reading from 27572: heap size 1267 MB, throughput 0.987835
Numeric result:
Recommendation: 2 clients, utility 0.998715:
    h1: 748.421 MB (U(h) = 0.843545*h^0.0239338)
    h2: 6769.58 MB (U(h) = 0.149876*h^0.216368)
Recommendation: 2 clients, utility 0.998715:
    h1: 748.785 MB (U(h) = 0.843545*h^0.0239338)
    h2: 6769.22 MB (U(h) = 0.149876*h^0.216368)
Reading from 27573: heap size 841 MB, throughput 0.99168
Reading from 27572: heap size 1277 MB, throughput 0.986437
Reading from 27573: heap size 793 MB, throughput 0.991858
Reading from 27572: heap size 1278 MB, throughput 0.986961
Reading from 27573: heap size 758 MB, throughput 0.991409
Reading from 27573: heap size 723 MB, throughput 0.988827
Numeric result:
Recommendation: 2 clients, utility 1.09879:
    h1: 652.896 MB (U(h) = 0.843259*h^0.0240173)
    h2: 6865.1 MB (U(h) = 0.119748*h^0.252585)
Recommendation: 2 clients, utility 1.09879:
    h1: 652.785 MB (U(h) = 0.843259*h^0.0240173)
    h2: 6865.22 MB (U(h) = 0.119748*h^0.252585)
Reading from 27572: heap size 1272 MB, throughput 0.983712
Reading from 27573: heap size 763 MB, throughput 0.985894
Reading from 27572: heap size 1161 MB, throughput 0.984063
Reading from 27573: heap size 718 MB, throughput 0.991717
Reading from 27573: heap size 692 MB, throughput 0.992746
Reading from 27572: heap size 1260 MB, throughput 0.982594
Reading from 27573: heap size 662 MB, throughput 0.991663
Numeric result:
Recommendation: 2 clients, utility 1.3857:
    h1: 489.453 MB (U(h) = 0.84287*h^0.0241393)
    h2: 7028.55 MB (U(h) = 0.0657142*h^0.3466)
Recommendation: 2 clients, utility 1.3857:
    h1: 489.507 MB (U(h) = 0.84287*h^0.0241393)
    h2: 7028.49 MB (U(h) = 0.0657142*h^0.3466)
Reading from 27573: heap size 646 MB, throughput 0.989452
Reading from 27572: heap size 1180 MB, throughput 0.980677
Reading from 27573: heap size 616 MB, throughput 0.9879
Reading from 27573: heap size 581 MB, throughput 0.990238
Reading from 27572: heap size 1252 MB, throughput 0.979249
Reading from 27573: heap size 552 MB, throughput 0.987869
Reading from 27573: heap size 540 MB, throughput 0.991099
Reading from 27572: heap size 1258 MB, throughput 0.974172
Reading from 27573: heap size 509 MB, throughput 0.986695
Numeric result:
Recommendation: 2 clients, utility 1.58903:
    h1: 429.268 MB (U(h) = 0.84224*h^0.0243777)
    h2: 7088.73 MB (U(h) = 0.0458714*h^0.402532)
Recommendation: 2 clients, utility 1.58903:
    h1: 429.298 MB (U(h) = 0.84224*h^0.0243777)
    h2: 7088.7 MB (U(h) = 0.0458714*h^0.402532)
Reading from 27573: heap size 500 MB, throughput 0.987884
Reading from 27573: heap size 470 MB, throughput 0.985795
Reading from 27572: heap size 1259 MB, throughput 0.975211
Reading from 27573: heap size 463 MB, throughput 0.988954
Reading from 27573: heap size 437 MB, throughput 0.989966
Reading from 27573: heap size 423 MB, throughput 0.991222
Reading from 27572: heap size 1259 MB, throughput 0.97664
Reading from 27573: heap size 438 MB, throughput 0.98719
Reading from 27573: heap size 433 MB, throughput 0.989999
Reading from 27573: heap size 405 MB, throughput 0.991951
Reading from 27572: heap size 1261 MB, throughput 0.976019
Numeric result:
Recommendation: 2 clients, utility 1.43207:
    h1: 492.633 MB (U(h) = 0.842138*h^0.0246275)
    h2: 7025.37 MB (U(h) = 0.0650108*h^0.351287)
Recommendation: 2 clients, utility 1.43207:
    h1: 492.53 MB (U(h) = 0.842138*h^0.0246275)
    h2: 7025.47 MB (U(h) = 0.0650108*h^0.351287)
Reading from 27573: heap size 416 MB, throughput 0.991495
Reading from 27573: heap size 438 MB, throughput 0.988859
Reading from 27573: heap size 458 MB, throughput 0.989685
Reading from 27572: heap size 1266 MB, throughput 0.974308
Reading from 27573: heap size 486 MB, throughput 0.988977
Reading from 27573: heap size 497 MB, throughput 0.988825
Reading from 27572: heap size 1269 MB, throughput 0.974938
Reading from 27573: heap size 463 MB, throughput 0.989108
Reading from 27573: heap size 481 MB, throughput 0.990921
Numeric result:
Recommendation: 2 clients, utility 1.32606:
    h1: 552.605 MB (U(h) = 0.84193*h^0.0247932)
    h2: 6965.39 MB (U(h) = 0.0847283*h^0.31259)
Recommendation: 2 clients, utility 1.32606:
    h1: 552.473 MB (U(h) = 0.84193*h^0.0247932)
    h2: 6965.53 MB (U(h) = 0.0847283*h^0.31259)
Reading from 27573: heap size 498 MB, throughput 0.989586
Reading from 27572: heap size 1276 MB, throughput 0.968301
Reading from 27573: heap size 498 MB, throughput 0.991858
Reading from 27573: heap size 513 MB, throughput 0.990522
Reading from 27572: heap size 1282 MB, throughput 0.968288
Reading from 27573: heap size 515 MB, throughput 0.99191
Reading from 27573: heap size 531 MB, throughput 0.988618
Reading from 27573: heap size 533 MB, throughput 0.991673
Reading from 27572: heap size 1286 MB, throughput 0.969234
Reading from 27573: heap size 552 MB, throughput 0.98822
Numeric result:
Recommendation: 2 clients, utility 1.45265:
    h1: 497.754 MB (U(h) = 0.841541*h^0.0249564)
    h2: 7020.25 MB (U(h) = 0.065469*h^0.351955)
Recommendation: 2 clients, utility 1.45265:
    h1: 497.789 MB (U(h) = 0.841541*h^0.0249564)
    h2: 7020.21 MB (U(h) = 0.065469*h^0.351955)
Reading from 27573: heap size 529 MB, throughput 0.991548
Reading from 27572: heap size 1293 MB, throughput 0.966777
Reading from 27573: heap size 498 MB, throughput 0.987234
Reading from 27573: heap size 493 MB, throughput 0.988689
Reading from 27573: heap size 511 MB, throughput 0.987471
Reading from 27572: heap size 1295 MB, throughput 0.966269
Reading from 27573: heap size 485 MB, throughput 0.988835
Reading from 27573: heap size 502 MB, throughput 0.990868
Reading from 27572: heap size 1301 MB, throughput 0.968096
Numeric result:
Recommendation: 2 clients, utility 1.71635:
    h1: 415.316 MB (U(h) = 0.841289*h^0.0250714)
    h2: 7102.68 MB (U(h) = 0.0391432*h^0.428766)
Recommendation: 2 clients, utility 1.71635:
    h1: 415.318 MB (U(h) = 0.841289*h^0.0250714)
    h2: 7102.68 MB (U(h) = 0.0391432*h^0.428766)
Reading from 27573: heap size 471 MB, throughput 0.990432
Reading from 27573: heap size 453 MB, throughput 0.99011
Reading from 27573: heap size 435 MB, throughput 0.988088
Reading from 27572: heap size 1302 MB, throughput 0.968787
Reading from 27573: heap size 422 MB, throughput 0.989505
Reading from 27573: heap size 406 MB, throughput 0.991484
Reading from 27573: heap size 425 MB, throughput 0.986531
Reading from 27572: heap size 1308 MB, throughput 0.970561
Reading from 27573: heap size 418 MB, throughput 0.989675
Reading from 27573: heap size 392 MB, throughput 0.986559
Reading from 27573: heap size 417 MB, throughput 0.98955
Numeric result:
Recommendation: 2 clients, utility 1.95774:
    h1: 367.381 MB (U(h) = 0.841419*h^0.0252016)
    h2: 7150.62 MB (U(h) = 0.025826*h^0.490366)
Recommendation: 2 clients, utility 1.95774:
    h1: 367.49 MB (U(h) = 0.841419*h^0.0252016)
    h2: 7150.51 MB (U(h) = 0.025826*h^0.490366)
Reading from 27573: heap size 390 MB, throughput 0.992736
Reading from 27572: heap size 1308 MB, throughput 0.969934
Reading from 27573: heap size 368 MB, throughput 0.991086
Reading from 27573: heap size 364 MB, throughput 0.993004
Reading from 27573: heap size 384 MB, throughput 0.989242
Reading from 27573: heap size 359 MB, throughput 0.988949
Reading from 27572: heap size 1312 MB, throughput 0.969688
Reading from 27573: heap size 379 MB, throughput 0.990477
Reading from 27573: heap size 355 MB, throughput 0.985452
Reading from 27573: heap size 372 MB, throughput 0.988025
Reading from 27573: heap size 350 MB, throughput 0.99038
Reading from 27572: heap size 1313 MB, throughput 0.971184
Numeric result:
Recommendation: 2 clients, utility 2.08299:
    h1: 348.001 MB (U(h) = 0.842536*h^0.0251842)
    h2: 7170 MB (U(h) = 0.0213159*h^0.518838)
Recommendation: 2 clients, utility 2.08299:
    h1: 348.028 MB (U(h) = 0.842536*h^0.0251842)
    h2: 7169.97 MB (U(h) = 0.0213159*h^0.518838)
Reading from 27573: heap size 373 MB, throughput 0.983293
Reading from 27573: heap size 359 MB, throughput 0.987662
Reading from 27573: heap size 338 MB, throughput 0.987706
Reading from 27573: heap size 352 MB, throughput 0.986855
Reading from 27573: heap size 337 MB, throughput 0.984954
Reading from 27572: heap size 1316 MB, throughput 0.971622
Reading from 27573: heap size 348 MB, throughput 0.986286
Reading from 27573: heap size 326 MB, throughput 0.98806
Reading from 27573: heap size 338 MB, throughput 0.990923
Reading from 27573: heap size 337 MB, throughput 0.992313
Reading from 27572: heap size 1317 MB, throughput 0.969159
Reading from 27573: heap size 347 MB, throughput 0.985113
Reading from 27573: heap size 349 MB, throughput 0.987541
Numeric result:
Recommendation: 2 clients, utility 2.29734:
    h1: 396.007 MB (U(h) = 0.811143*h^0.0315137)
    h2: 7121.99 MB (U(h) = 0.0153714*h^0.566773)
Recommendation: 2 clients, utility 2.29734:
    h1: 395.998 MB (U(h) = 0.811143*h^0.0315137)
    h2: 7122 MB (U(h) = 0.0153714*h^0.566773)
Reading from 27573: heap size 329 MB, throughput 0.988422
Reading from 27573: heap size 344 MB, throughput 0.983407
Reading from 27573: heap size 353 MB, throughput 0.988087
Reading from 27573: heap size 355 MB, throughput 0.987863
Reading from 27573: heap size 369 MB, throughput 0.983706
Reading from 27572: heap size 1321 MB, throughput 0.955119
Reading from 27573: heap size 369 MB, throughput 0.987673
Reading from 27573: heap size 383 MB, throughput 0.987523
Reading from 27573: heap size 385 MB, throughput 0.988289
Reading from 27573: heap size 400 MB, throughput 0.990932
Reading from 27572: heap size 1439 MB, throughput 0.969924
Numeric result:
Recommendation: 2 clients, utility 2.42814:
    h1: 44.8756 MB (U(h) = 0.966858*h^0.00348339)
    h2: 7473.12 MB (U(h) = 0.0140222*h^0.580183)
Recommendation: 2 clients, utility 2.42814:
    h1: 44.8683 MB (U(h) = 0.966858*h^0.00348339)
    h2: 7473.13 MB (U(h) = 0.0140222*h^0.580183)
Reading from 27573: heap size 379 MB, throughput 0.761066
Reading from 27573: heap size 280 MB, throughput 0.983374
Reading from 27573: heap size 239 MB, throughput 0.98828
Reading from 27573: heap size 185 MB, throughput 0.985117
Reading from 27573: heap size 161 MB, throughput 0.989793
Reading from 27573: heap size 150 MB, throughput 0.989922
Reading from 27573: heap size 140 MB, throughput 0.979667
Reading from 27573: heap size 141 MB, throughput 0.979149
Reading from 27572: heap size 1434 MB, throughput 0.980256
Reading from 27573: heap size 134 MB, throughput 0.980285
Reading from 27573: heap size 127 MB, throughput 0.964247
Reading from 27573: heap size 126 MB, throughput 0.970434
Reading from 27573: heap size 119 MB, throughput 0.968603
Reading from 27573: heap size 114 MB, throughput 0.966545
Reading from 27573: heap size 111 MB, throughput 0.954362
Reading from 27573: heap size 108 MB, throughput 0.95201
Reading from 27573: heap size 105 MB, throughput 0.957503
Reading from 27573: heap size 88 MB, throughput 0.981214
Reading from 27573: heap size 87 MB, throughput 0.980903
Reading from 27573: heap size 88 MB, throughput 0.98208
Reading from 27573: heap size 86 MB, throughput 0.978504
Reading from 27573: heap size 86 MB, throughput 0.950161
Reading from 27573: heap size 91 MB, throughput 0.924258
Reading from 27573: heap size 88 MB, throughput 0.922894
Reading from 27573: heap size 87 MB, throughput 0.935795
Reading from 27573: heap size 81 MB, throughput 0.950071
Reading from 27573: heap size 86 MB, throughput 0.968872
Reading from 27573: heap size 75 MB, throughput 0.980165
Reading from 27573: heap size 91 MB, throughput 0.97913
Reading from 27573: heap size 75 MB, throughput 0.929393
Reading from 27573: heap size 92 MB, throughput 0.9801
Reading from 27573: heap size 74 MB, throughput 0.991096
Reading from 27573: heap size 74 MB, throughput 0.991097
Reading from 27573: heap size 74 MB, throughput 0.969814
Reading from 27573: heap size 90 MB, throughput 0.980174
Reading from 27573: heap size 74 MB, throughput 0.978776
Reading from 27573: heap size 88 MB, throughput 0.985068
Reading from 27573: heap size 72 MB, throughput 0.974188
Reading from 27573: heap size 85 MB, throughput 0.976417
Reading from 27573: heap size 72 MB, throughput 0.98203
Reading from 27572: heap size 1444 MB, throughput 0.983552
Reading from 27573: heap size 83 MB, throughput 0.978834
Reading from 27573: heap size 71 MB, throughput 0.977458
Reading from 27573: heap size 80 MB, throughput 0.973858
Reading from 27573: heap size 71 MB, throughput 0.979423
Reading from 27573: heap size 78 MB, throughput 0.979486
Reading from 27573: heap size 69 MB, throughput 0.990097
Reading from 27573: heap size 76 MB, throughput 0.979815
Reading from 27573: heap size 71 MB, throughput 0.958896
Reading from 27573: heap size 75 MB, throughput 0.96077
Reading from 27573: heap size 76 MB, throughput 0.916224
Reading from 27573: heap size 75 MB, throughput 0.938231
Reading from 27573: heap size 76 MB, throughput 0.93341
Reading from 27573: heap size 76 MB, throughput 0.797218
Reading from 27573: heap size 76 MB, throughput 0.588701
Reading from 27573: heap size 75 MB, throughput 0.378188
Reading from 27573: heap size 77 MB, throughput 0.185625
Reading from 27573: heap size 78 MB, throughput 0.877643
Reading from 27573: heap size 79 MB, throughput 0.884692
Reading from 27573: heap size 78 MB, throughput 0.927215
Reading from 27573: heap size 73 MB, throughput 0.941196
Reading from 27573: heap size 72 MB, throughput 0.939845
Reading from 27573: heap size 57 MB, throughput 0.94942
Reading from 27573: heap size 72 MB, throughput 0.967469
Reading from 27573: heap size 57 MB, throughput 0.974683
Reading from 27573: heap size 70 MB, throughput 0.978893
Reading from 27573: heap size 57 MB, throughput 0.979974
Reading from 27573: heap size 69 MB, throughput 0.465899
Reading from 27573: heap size 58 MB, throughput 0.414539
Reading from 27573: heap size 68 MB, throughput 0.419238
Reading from 27573: heap size 57 MB, throughput 0.550429
Reading from 27573: heap size 67 MB, throughput 0.786493
Reading from 27573: heap size 57 MB, throughput 0.896166
Numeric result:
Recommendation: 2 clients, utility 2.74216:
    h1: 1121.79 MB (U(h) = 0.512533*h^0.110011)
    h2: 6396.21 MB (U(h) = 0.0101288*h^0.627257)
Recommendation: 2 clients, utility 2.74216:
    h1: 1121.79 MB (U(h) = 0.512533*h^0.110011)
    h2: 6396.21 MB (U(h) = 0.0101288*h^0.627257)
Reading from 27573: heap size 66 MB, throughput 0.925564
Reading from 27573: heap size 66 MB, throughput 0.946127
Reading from 27573: heap size 67 MB, throughput 0.950419
Reading from 27573: heap size 67 MB, throughput 0.949228
Reading from 27573: heap size 67 MB, throughput 0.963227
Reading from 27573: heap size 68 MB, throughput 0.971506
Reading from 27573: heap size 68 MB, throughput 0.980674
Reading from 27573: heap size 69 MB, throughput 0.983797
Reading from 27573: heap size 69 MB, throughput 0.98621
Reading from 27573: heap size 70 MB, throughput 0.986694
Reading from 27573: heap size 71 MB, throughput 0.98301
Reading from 27573: heap size 72 MB, throughput 0.983445
Reading from 27573: heap size 73 MB, throughput 0.985776
Reading from 27573: heap size 74 MB, throughput 0.991474
Reading from 27573: heap size 75 MB, throughput 0.985463
Reading from 27573: heap size 76 MB, throughput 0.991616
Reading from 27573: heap size 76 MB, throughput 0.991617
Reading from 27573: heap size 76 MB, throughput 0.942693
Reading from 27573: heap size 78 MB, throughput 0.97028
Reading from 27573: heap size 79 MB, throughput 0.835137
Reading from 27573: heap size 80 MB, throughput 0.946094
Reading from 27572: heap size 1454 MB, throughput 0.983588
Reading from 27573: heap size 84 MB, throughput 0.778899
Reading from 27573: heap size 92 MB, throughput 0.907324
Reading from 27573: heap size 92 MB, throughput 0.942419
Reading from 27573: heap size 95 MB, throughput 0.9617
Reading from 27573: heap size 96 MB, throughput 0.958668
Reading from 27573: heap size 100 MB, throughput 0.951092
Reading from 27573: heap size 101 MB, throughput 0.919611
Reading from 27573: heap size 107 MB, throughput 0.708102
Reading from 27573: heap size 108 MB, throughput 0.843039
Reading from 27573: heap size 112 MB, throughput 0.799914
Reading from 27573: heap size 113 MB, throughput 0.888049
Reading from 27573: heap size 118 MB, throughput 0.946416
Reading from 27573: heap size 119 MB, throughput 0.974661
Reading from 27573: heap size 124 MB, throughput 0.980476
Reading from 27573: heap size 124 MB, throughput 0.949343
Reading from 27573: heap size 129 MB, throughput 0.980011
Reading from 27573: heap size 133 MB, throughput 0.984717
Reading from 27573: heap size 139 MB, throughput 0.982743
Reading from 27573: heap size 140 MB, throughput 0.888054
Reading from 27572: heap size 1456 MB, throughput 0.982291
Reading from 27573: heap size 148 MB, throughput 0.935977
Reading from 27573: heap size 148 MB, throughput 0.964329
Reading from 27573: heap size 155 MB, throughput 0.981656
Reading from 27573: heap size 156 MB, throughput 0.9785
Reading from 27573: heap size 163 MB, throughput 0.759547
Reading from 27573: heap size 164 MB, throughput 0.960737
Reading from 27573: heap size 179 MB, throughput 0.898827
Reading from 27573: heap size 179 MB, throughput 0.944401
Reading from 27573: heap size 188 MB, throughput 0.97272
Numeric result:
Recommendation: 2 clients, utility 2.83336:
    h1: 847.323 MB (U(h) = 0.606809*h^0.0830711)
    h2: 6670.68 MB (U(h) = 0.00841476*h^0.653991)
Recommendation: 2 clients, utility 2.83336:
    h1: 847.322 MB (U(h) = 0.606809*h^0.0830711)
    h2: 6670.68 MB (U(h) = 0.00841476*h^0.653991)
Reading from 27573: heap size 189 MB, throughput 0.981217
Reading from 27573: heap size 199 MB, throughput 0.906592
Reading from 27573: heap size 200 MB, throughput 0.850826
Reading from 27573: heap size 215 MB, throughput 0.815072
Reading from 27573: heap size 216 MB, throughput 0.976242
Reading from 27573: heap size 231 MB, throughput 0.977553
Reading from 27573: heap size 231 MB, throughput 0.974191
Reading from 27573: heap size 244 MB, throughput 0.974332
Reading from 27573: heap size 248 MB, throughput 0.974197
Reading from 27573: heap size 267 MB, throughput 0.978925
Reading from 27573: heap size 268 MB, throughput 0.975568
Reading from 27573: heap size 286 MB, throughput 0.976959
Reading from 27573: heap size 287 MB, throughput 0.9796
Reading from 27573: heap size 305 MB, throughput 0.983196
Reading from 27573: heap size 306 MB, throughput 0.967274
Reading from 27573: heap size 323 MB, throughput 0.970002
Reading from 27572: heap size 1451 MB, throughput 0.98846
Reading from 27573: heap size 335 MB, throughput 0.982702
Numeric result:
Recommendation: 2 clients, utility 2.98066:
    h1: 896.328 MB (U(h) = 0.583428*h^0.0911374)
    h2: 6621.67 MB (U(h) = 0.00735591*h^0.673287)
Recommendation: 2 clients, utility 2.98066:
    h1: 896.323 MB (U(h) = 0.583428*h^0.0911374)
    h2: 6621.68 MB (U(h) = 0.00735591*h^0.673287)
Reading from 27573: heap size 361 MB, throughput 0.985808
Reading from 27573: heap size 362 MB, throughput 0.986307
Reading from 27573: heap size 381 MB, throughput 0.987847
Reading from 27573: heap size 383 MB, throughput 0.979073
Reading from 27573: heap size 405 MB, throughput 0.9859
Reading from 27573: heap size 407 MB, throughput 0.993301
Reading from 27573: heap size 407 MB, throughput 0.9933
Reading from 27573: heap size 407 MB, throughput 0.974898
Reading from 27573: heap size 429 MB, throughput 0.982859
Reading from 27573: heap size 431 MB, throughput 0.984968
Reading from 27573: heap size 456 MB, throughput 0.988544
Reading from 27573: heap size 457 MB, throughput 0.989772
Numeric result:
Recommendation: 2 clients, utility 2.9607:
    h1: 868.03 MB (U(h) = 0.59244*h^0.087887)
    h2: 6649.97 MB (U(h) = 0.00735591*h^0.673287)
Recommendation: 2 clients, utility 2.9607:
    h1: 868.046 MB (U(h) = 0.59244*h^0.087887)
    h2: 6649.95 MB (U(h) = 0.00735591*h^0.673287)
Reading from 27573: heap size 480 MB, throughput 0.990796
Reading from 27573: heap size 482 MB, throughput 0.989393
Reading from 27572: heap size 1455 MB, throughput 0.989127
Reading from 27573: heap size 509 MB, throughput 0.985772
Reading from 27573: heap size 509 MB, throughput 0.982928
Reading from 27573: heap size 544 MB, throughput 0.985577
Reading from 27573: heap size 546 MB, throughput 0.982328
Reading from 27573: heap size 579 MB, throughput 0.986816
Reading from 27572: heap size 1452 MB, throughput 0.986518
Numeric result:
Recommendation: 2 clients, utility 3.12303:
    h1: 778.055 MB (U(h) = 0.609633*h^0.0818586)
    h2: 6739.95 MB (U(h) = 0.00572762*h^0.709094)
Recommendation: 2 clients, utility 3.12303:
    h1: 778.065 MB (U(h) = 0.609633*h^0.0818586)
    h2: 6739.94 MB (U(h) = 0.00572762*h^0.709094)
Reading from 27572: heap size 1462 MB, throughput 0.975983
Reading from 27572: heap size 1469 MB, throughput 0.953946
Reading from 27572: heap size 1492 MB, throughput 0.918747
Reading from 27573: heap size 584 MB, throughput 0.984338
Reading from 27572: heap size 1529 MB, throughput 0.864475
Reading from 27572: heap size 1555 MB, throughput 0.807808
Reading from 27572: heap size 1590 MB, throughput 0.769244
Reading from 27572: heap size 1605 MB, throughput 0.754288
Reading from 27573: heap size 620 MB, throughput 0.986535
Reading from 27573: heap size 623 MB, throughput 0.988387
Reading from 27572: heap size 1615 MB, throughput 0.946833
Reading from 27573: heap size 659 MB, throughput 0.987709
Reading from 27573: heap size 662 MB, throughput 0.986712
Numeric result:
Recommendation: 2 clients, utility 3.82315:
    h1: 622.741 MB (U(h) = 0.627163*h^0.0759072)
    h2: 6895.26 MB (U(h) = 0.00222183*h^0.840479)
Recommendation: 2 clients, utility 3.82315:
    h1: 622.74 MB (U(h) = 0.627163*h^0.0759072)
    h2: 6895.26 MB (U(h) = 0.00222183*h^0.840479)
Reading from 27572: heap size 1633 MB, throughput 0.969975
Reading from 27573: heap size 709 MB, throughput 0.987715
Reading from 27573: heap size 662 MB, throughput 0.989489
Reading from 27573: heap size 633 MB, throughput 0.988493
Reading from 27572: heap size 1617 MB, throughput 0.97495
Reading from 27573: heap size 618 MB, throughput 0.990914
Reading from 27573: heap size 646 MB, throughput 0.993499
Numeric result:
Recommendation: 2 clients, utility 4.14884:
    h1: 574.97 MB (U(h) = 0.633555*h^0.0737801)
    h2: 6943.03 MB (U(h) = 0.0015491*h^0.890905)
Recommendation: 2 clients, utility 4.14884:
    h1: 574.984 MB (U(h) = 0.633555*h^0.0737801)
    h2: 6943.02 MB (U(h) = 0.0015491*h^0.890905)
Reading from 27572: heap size 1635 MB, throughput 0.977685
Reading from 27573: heap size 610 MB, throughput 0.991123
Reading from 27573: heap size 595 MB, throughput 0.989292
Reading from 27573: heap size 575 MB, throughput 0.992495
Client 27573 died
Clients: 1
Reading from 27572: heap size 1615 MB, throughput 0.981513
Recommendation: one client; give it all the memory
Reading from 27572: heap size 1632 MB, throughput 0.982251
Reading from 27572: heap size 1617 MB, throughput 0.96817
Recommendation: one client; give it all the memory
Reading from 27572: heap size 1598 MB, throughput 0.989772
Reading from 27572: heap size 1601 MB, throughput 0.9923
Recommendation: one client; give it all the memory
Reading from 27572: heap size 1613 MB, throughput 0.993205
Reading from 27572: heap size 1627 MB, throughput 0.992778
Recommendation: one client; give it all the memory
Reading from 27572: heap size 1629 MB, throughput 0.992267
Recommendation: one client; give it all the memory
Reading from 27572: heap size 1616 MB, throughput 0.991698
Reading from 27572: heap size 1434 MB, throughput 0.990784
Recommendation: one client; give it all the memory
Reading from 27572: heap size 1597 MB, throughput 0.99002
Reading from 27572: heap size 1464 MB, throughput 0.988893
Recommendation: one client; give it all the memory
Reading from 27572: heap size 1582 MB, throughput 0.988158
Recommendation: one client; give it all the memory
Reading from 27572: heap size 1592 MB, throughput 0.987243
Reading from 27572: heap size 1594 MB, throughput 0.986392
Recommendation: one client; give it all the memory
Reading from 27572: heap size 1594 MB, throughput 0.985075
Recommendation: one client; give it all the memory
Reading from 27572: heap size 1599 MB, throughput 0.984097
Reading from 27572: heap size 1605 MB, throughput 0.983275
Recommendation: one client; give it all the memory
Reading from 27572: heap size 1611 MB, throughput 0.981847
Reading from 27572: heap size 1622 MB, throughput 0.981243
Recommendation: one client; give it all the memory
Reading from 27572: heap size 1632 MB, throughput 0.981311
Recommendation: one client; give it all the memory
Reading from 27572: heap size 1639 MB, throughput 0.981103
Reading from 27572: heap size 1649 MB, throughput 0.981227
Recommendation: one client; give it all the memory
Reading from 27572: heap size 1652 MB, throughput 0.981027
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 27572: heap size 1663 MB, throughput 0.992213
Recommendation: one client; give it all the memory
Reading from 27572: heap size 1550 MB, throughput 0.990266
Reading from 27572: heap size 1653 MB, throughput 0.98252
Reading from 27572: heap size 1661 MB, throughput 0.965767
Reading from 27572: heap size 1691 MB, throughput 0.944312
Reading from 27572: heap size 1710 MB, throughput 0.911467
Client 27572 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
