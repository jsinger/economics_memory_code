while (($#)); do
    ghc -W -Wall -rtsopts -threaded "-i${ECONOMEM_ROOT}/haskell_common" --make "${1}.hs"
    shift
done
