economemd
    total memory: 7518 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub4_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
TimerThread: starting
ReadingThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 1471: heap size 9 MB, throughput 0.989717
Clients: 1
Client 1471 has a minimum heap size of 30 MB
Reading from 1470: heap size 9 MB, throughput 0.988582
Clients: 2
Client 1470 has a minimum heap size of 1223 MB
Reading from 1471: heap size 9 MB, throughput 0.96967
Reading from 1471: heap size 9 MB, throughput 0.96707
Reading from 1470: heap size 9 MB, throughput 0.963042
Reading from 1471: heap size 9 MB, throughput 0.959928
Reading from 1471: heap size 11 MB, throughput 0.604871
Reading from 1471: heap size 11 MB, throughput 0.506336
Reading from 1471: heap size 16 MB, throughput 0.755291
Reading from 1471: heap size 16 MB, throughput 0.746601
Reading from 1471: heap size 24 MB, throughput 0.858286
Reading from 1471: heap size 24 MB, throughput 0.86263
Reading from 1471: heap size 34 MB, throughput 0.898791
Reading from 1471: heap size 34 MB, throughput 0.897951
Reading from 1471: heap size 50 MB, throughput 0.925754
Reading from 1471: heap size 50 MB, throughput 0.929493
Reading from 1470: heap size 11 MB, throughput 0.971937
Reading from 1471: heap size 76 MB, throughput 0.96368
Reading from 1471: heap size 76 MB, throughput 0.949529
Reading from 1471: heap size 115 MB, throughput 0.965372
Reading from 1471: heap size 116 MB, throughput 0.907939
Reading from 1470: heap size 11 MB, throughput 0.968874
Reading from 1470: heap size 15 MB, throughput 0.868547
Reading from 1470: heap size 18 MB, throughput 0.935555
Reading from 1470: heap size 24 MB, throughput 0.963773
Reading from 1470: heap size 28 MB, throughput 0.712292
Reading from 1470: heap size 32 MB, throughput 0.491481
Reading from 1470: heap size 41 MB, throughput 0.924583
Reading from 1470: heap size 47 MB, throughput 0.847132
Reading from 1471: heap size 159 MB, throughput 0.955482
Reading from 1470: heap size 49 MB, throughput 0.232199
Reading from 1470: heap size 68 MB, throughput 0.315322
Reading from 1470: heap size 89 MB, throughput 0.75311
Reading from 1471: heap size 168 MB, throughput 0.873531
Reading from 1470: heap size 93 MB, throughput 0.668507
Reading from 1470: heap size 95 MB, throughput 0.208613
Reading from 1470: heap size 128 MB, throughput 0.770501
Reading from 1470: heap size 129 MB, throughput 0.851787
Reading from 1471: heap size 226 MB, throughput 0.986498
Reading from 1470: heap size 131 MB, throughput 0.817365
Reading from 1470: heap size 135 MB, throughput 0.197682
Reading from 1470: heap size 178 MB, throughput 0.778812
Reading from 1471: heap size 239 MB, throughput 0.978287
Reading from 1470: heap size 178 MB, throughput 0.721568
Reading from 1470: heap size 182 MB, throughput 0.680285
Reading from 1470: heap size 187 MB, throughput 0.665369
Reading from 1470: heap size 191 MB, throughput 0.682183
Reading from 1470: heap size 198 MB, throughput 0.193401
Reading from 1470: heap size 254 MB, throughput 0.727361
Reading from 1470: heap size 257 MB, throughput 0.660843
Reading from 1471: heap size 266 MB, throughput 0.98737
Reading from 1470: heap size 261 MB, throughput 0.699555
Reading from 1470: heap size 269 MB, throughput 0.712349
Reading from 1470: heap size 274 MB, throughput 0.61197
Reading from 1470: heap size 286 MB, throughput 0.618626
Reading from 1471: heap size 267 MB, throughput 0.962463
Reading from 1470: heap size 295 MB, throughput 0.512739
Reading from 1470: heap size 306 MB, throughput 0.164186
Reading from 1471: heap size 295 MB, throughput 0.870981
Reading from 1470: heap size 363 MB, throughput 0.484555
Reading from 1470: heap size 366 MB, throughput 0.564898
Reading from 1470: heap size 368 MB, throughput 0.58444
Reading from 1471: heap size 312 MB, throughput 0.996278
Reading from 1470: heap size 370 MB, throughput 0.0935787
Reading from 1470: heap size 425 MB, throughput 0.488862
Reading from 1470: heap size 354 MB, throughput 0.726885
Reading from 1470: heap size 418 MB, throughput 0.765996
Reading from 1470: heap size 421 MB, throughput 0.543217
Reading from 1471: heap size 336 MB, throughput 0.975845
Reading from 1470: heap size 423 MB, throughput 0.66737
Equal recommendation: 3759 MB each
Reading from 1470: heap size 428 MB, throughput 0.621337
Reading from 1470: heap size 432 MB, throughput 0.561087
Reading from 1470: heap size 443 MB, throughput 0.593987
Reading from 1471: heap size 337 MB, throughput 0.983913
Reading from 1470: heap size 450 MB, throughput 0.100031
Reading from 1470: heap size 512 MB, throughput 0.373905
Reading from 1470: heap size 524 MB, throughput 0.536662
Reading from 1470: heap size 526 MB, throughput 0.509418
Reading from 1471: heap size 364 MB, throughput 0.988848
Reading from 1470: heap size 531 MB, throughput 0.430505
Reading from 1470: heap size 537 MB, throughput 0.495329
Reading from 1471: heap size 365 MB, throughput 0.973767
Reading from 1470: heap size 546 MB, throughput 0.0984568
Reading from 1470: heap size 610 MB, throughput 0.438288
Reading from 1470: heap size 608 MB, throughput 0.588894
Reading from 1471: heap size 386 MB, throughput 0.985146
Reading from 1470: heap size 612 MB, throughput 0.103492
Reading from 1470: heap size 679 MB, throughput 0.452891
Reading from 1470: heap size 605 MB, throughput 0.543638
Reading from 1471: heap size 391 MB, throughput 0.988492
Reading from 1470: heap size 677 MB, throughput 0.569097
Reading from 1470: heap size 678 MB, throughput 0.825841
Reading from 1471: heap size 414 MB, throughput 0.991655
Reading from 1470: heap size 678 MB, throughput 0.840816
Reading from 1470: heap size 689 MB, throughput 0.845676
Reading from 1471: heap size 416 MB, throughput 0.971231
Reading from 1470: heap size 697 MB, throughput 0.532198
Reading from 1470: heap size 714 MB, throughput 0.581916
Equal recommendation: 3759 MB each
Reading from 1471: heap size 438 MB, throughput 0.98088
Reading from 1470: heap size 724 MB, throughput 0.0230818
Reading from 1470: heap size 802 MB, throughput 0.231096
Reading from 1471: heap size 437 MB, throughput 0.978506
Reading from 1470: heap size 792 MB, throughput 0.327437
Reading from 1470: heap size 702 MB, throughput 0.384576
Reading from 1470: heap size 787 MB, throughput 0.809236
Reading from 1471: heap size 465 MB, throughput 0.964555
Reading from 1470: heap size 793 MB, throughput 0.0493313
Reading from 1470: heap size 864 MB, throughput 0.365788
Reading from 1470: heap size 867 MB, throughput 0.624833
Reading from 1470: heap size 875 MB, throughput 0.78464
Reading from 1471: heap size 465 MB, throughput 0.985224
Reading from 1470: heap size 875 MB, throughput 0.83137
Reading from 1470: heap size 878 MB, throughput 0.649496
Reading from 1471: heap size 491 MB, throughput 0.982864
Reading from 1470: heap size 879 MB, throughput 0.17317
Reading from 1470: heap size 970 MB, throughput 0.691372
Reading from 1470: heap size 972 MB, throughput 0.874506
Reading from 1470: heap size 972 MB, throughput 0.818872
Reading from 1470: heap size 977 MB, throughput 0.808623
Reading from 1470: heap size 965 MB, throughput 0.814971
Reading from 1471: heap size 495 MB, throughput 0.991924
Reading from 1470: heap size 971 MB, throughput 0.801445
Reading from 1470: heap size 957 MB, throughput 0.771267
Reading from 1470: heap size 964 MB, throughput 0.739225
Reading from 1470: heap size 954 MB, throughput 0.799856
Reading from 1470: heap size 960 MB, throughput 0.774839
Reading from 1470: heap size 950 MB, throughput 0.868667
Reading from 1471: heap size 518 MB, throughput 0.986347
Equal recommendation: 3759 MB each
Reading from 1470: heap size 956 MB, throughput 0.971189
Reading from 1470: heap size 941 MB, throughput 0.816211
Reading from 1471: heap size 524 MB, throughput 0.989354
Reading from 1470: heap size 950 MB, throughput 0.796833
Reading from 1470: heap size 941 MB, throughput 0.788798
Reading from 1470: heap size 946 MB, throughput 0.816193
Reading from 1470: heap size 935 MB, throughput 0.814431
Reading from 1470: heap size 942 MB, throughput 0.825983
Reading from 1470: heap size 935 MB, throughput 0.829778
Reading from 1471: heap size 544 MB, throughput 0.993377
Reading from 1470: heap size 940 MB, throughput 0.884655
Reading from 1470: heap size 944 MB, throughput 0.882562
Reading from 1470: heap size 945 MB, throughput 0.906611
Reading from 1470: heap size 947 MB, throughput 0.721628
Reading from 1470: heap size 948 MB, throughput 0.676367
Reading from 1471: heap size 547 MB, throughput 0.985747
Reading from 1470: heap size 968 MB, throughput 0.690095
Reading from 1470: heap size 977 MB, throughput 0.64499
Reading from 1471: heap size 565 MB, throughput 0.991288
Reading from 1470: heap size 990 MB, throughput 0.0642497
Reading from 1470: heap size 1104 MB, throughput 0.535767
Reading from 1470: heap size 1101 MB, throughput 0.721834
Reading from 1470: heap size 1107 MB, throughput 0.687351
Reading from 1470: heap size 1110 MB, throughput 0.712719
Reading from 1471: heap size 568 MB, throughput 0.979293
Reading from 1470: heap size 1113 MB, throughput 0.724288
Reading from 1471: heap size 588 MB, throughput 0.991659
Equal recommendation: 3759 MB each
Reading from 1470: heap size 1123 MB, throughput 0.960163
Reading from 1471: heap size 589 MB, throughput 0.986458
Reading from 1470: heap size 1128 MB, throughput 0.97155
Reading from 1471: heap size 610 MB, throughput 0.988492
Reading from 1471: heap size 611 MB, throughput 0.990029
Reading from 1470: heap size 1143 MB, throughput 0.971543
Reading from 1471: heap size 631 MB, throughput 0.995852
Reading from 1470: heap size 1145 MB, throughput 0.966899
Reading from 1471: heap size 633 MB, throughput 0.988009
Equal recommendation: 3759 MB each
Reading from 1471: heap size 652 MB, throughput 0.992661
Reading from 1470: heap size 1147 MB, throughput 0.972356
Reading from 1471: heap size 653 MB, throughput 0.995553
Reading from 1471: heap size 668 MB, throughput 0.988278
Reading from 1470: heap size 1151 MB, throughput 0.956591
Reading from 1471: heap size 670 MB, throughput 0.98793
Reading from 1470: heap size 1143 MB, throughput 0.971044
Reading from 1471: heap size 691 MB, throughput 0.989581
Equal recommendation: 3759 MB each
Reading from 1471: heap size 692 MB, throughput 0.981716
Reading from 1470: heap size 1150 MB, throughput 0.974402
Reading from 1471: heap size 715 MB, throughput 0.989623
Reading from 1470: heap size 1145 MB, throughput 0.963396
Reading from 1471: heap size 715 MB, throughput 0.988189
Reading from 1470: heap size 1149 MB, throughput 0.97129
Reading from 1471: heap size 740 MB, throughput 0.990098
Equal recommendation: 3759 MB each
Reading from 1471: heap size 741 MB, throughput 0.989289
Reading from 1470: heap size 1154 MB, throughput 0.968192
Reading from 1471: heap size 766 MB, throughput 0.99215
Reading from 1470: heap size 1154 MB, throughput 0.968283
Reading from 1471: heap size 767 MB, throughput 0.996055
Reading from 1471: heap size 784 MB, throughput 0.988326
Reading from 1470: heap size 1159 MB, throughput 0.966724
Equal recommendation: 3759 MB each
Reading from 1471: heap size 787 MB, throughput 0.989168
Reading from 1470: heap size 1161 MB, throughput 0.96811
Reading from 1471: heap size 811 MB, throughput 0.991482
Reading from 1470: heap size 1166 MB, throughput 0.97336
Reading from 1471: heap size 811 MB, throughput 0.988939
Reading from 1471: heap size 834 MB, throughput 0.994926
Reading from 1470: heap size 1170 MB, throughput 0.971792
Equal recommendation: 3759 MB each
Reading from 1471: heap size 836 MB, throughput 0.991115
Reading from 1470: heap size 1175 MB, throughput 0.963516
Reading from 1471: heap size 858 MB, throughput 0.990791
Reading from 1470: heap size 1179 MB, throughput 0.973729
Reading from 1471: heap size 858 MB, throughput 0.990021
Equal recommendation: 3759 MB each
Reading from 1470: heap size 1184 MB, throughput 0.969076
Reading from 1471: heap size 883 MB, throughput 0.991386
Reading from 1471: heap size 883 MB, throughput 0.991708
Reading from 1470: heap size 1187 MB, throughput 0.969707
Reading from 1471: heap size 906 MB, throughput 0.991695
Reading from 1470: heap size 1192 MB, throughput 0.973968
Reading from 1471: heap size 907 MB, throughput 0.988247
Equal recommendation: 3759 MB each
Reading from 1470: heap size 1193 MB, throughput 0.967585
Reading from 1471: heap size 932 MB, throughput 0.990608
Reading from 1470: heap size 1198 MB, throughput 0.964806
Reading from 1471: heap size 932 MB, throughput 0.991558
Reading from 1471: heap size 959 MB, throughput 0.992557
Equal recommendation: 3759 MB each
Reading from 1470: heap size 1199 MB, throughput 0.626069
Reading from 1471: heap size 960 MB, throughput 0.992421
Reading from 1470: heap size 1294 MB, throughput 0.956026
Reading from 1471: heap size 984 MB, throughput 0.992375
Reading from 1470: heap size 1294 MB, throughput 0.985186
Reading from 1471: heap size 985 MB, throughput 0.990476
Equal recommendation: 3759 MB each
Reading from 1470: heap size 1304 MB, throughput 0.982578
Reading from 1471: heap size 1011 MB, throughput 0.988316
Reading from 1471: heap size 1011 MB, throughput 0.992438
Reading from 1470: heap size 1307 MB, throughput 0.987941
Reading from 1471: heap size 1039 MB, throughput 0.99386
Reading from 1470: heap size 1307 MB, throughput 0.982692
Equal recommendation: 3759 MB each
Reading from 1471: heap size 1040 MB, throughput 0.992221
Reading from 1470: heap size 1310 MB, throughput 0.981306
Reading from 1471: heap size 1067 MB, throughput 0.99075
Reading from 1470: heap size 1299 MB, throughput 0.975854
Reading from 1471: heap size 1068 MB, throughput 0.989254
Equal recommendation: 3759 MB each
Reading from 1470: heap size 1224 MB, throughput 0.978654
Reading from 1471: heap size 1097 MB, throughput 0.993952
Reading from 1470: heap size 1290 MB, throughput 0.97736
Reading from 1471: heap size 1098 MB, throughput 0.991726
Reading from 1470: heap size 1297 MB, throughput 0.976756
Equal recommendation: 3759 MB each
Reading from 1471: heap size 1127 MB, throughput 0.988803
Reading from 1470: heap size 1297 MB, throughput 0.977042
Reading from 1471: heap size 1127 MB, throughput 0.98936
Reading from 1470: heap size 1297 MB, throughput 0.974541
Reading from 1471: heap size 1163 MB, throughput 0.992122
Equal recommendation: 3759 MB each
Reading from 1470: heap size 1300 MB, throughput 0.968988
Reading from 1471: heap size 1165 MB, throughput 0.943315
Reading from 1471: heap size 1211 MB, throughput 0.993399
Reading from 1470: heap size 1303 MB, throughput 0.973087
Equal recommendation: 3759 MB each
Reading from 1471: heap size 1216 MB, throughput 0.989091
Reading from 1470: heap size 1307 MB, throughput 0.970161
Reading from 1471: heap size 1255 MB, throughput 0.989494
Reading from 1470: heap size 1313 MB, throughput 0.967965
Reading from 1471: heap size 1259 MB, throughput 0.968685
Reading from 1470: heap size 1321 MB, throughput 0.966747
Equal recommendation: 3759 MB each
Reading from 1471: heap size 1305 MB, throughput 0.993227
Reading from 1470: heap size 1326 MB, throughput 0.972538
Reading from 1471: heap size 1312 MB, throughput 0.993288
Equal recommendation: 3759 MB each
Reading from 1471: heap size 1362 MB, throughput 0.989588
Reading from 1471: heap size 1367 MB, throughput 0.993846
Reading from 1471: heap size 1419 MB, throughput 0.987144
Equal recommendation: 3759 MB each
Reading from 1470: heap size 1333 MB, throughput 0.836899
Reading from 1471: heap size 1423 MB, throughput 0.990457
Reading from 1471: heap size 1485 MB, throughput 0.994399
Equal recommendation: 3759 MB each
Reading from 1471: heap size 1486 MB, throughput 0.988252
Reading from 1470: heap size 1455 MB, throughput 0.916835
Reading from 1471: heap size 1549 MB, throughput 0.992091
Reading from 1470: heap size 1429 MB, throughput 0.99434
Reading from 1470: heap size 1433 MB, throughput 0.918667
Reading from 1470: heap size 1436 MB, throughput 0.851309
Reading from 1470: heap size 1439 MB, throughput 0.867349
Reading from 1470: heap size 1443 MB, throughput 0.883966
Reading from 1470: heap size 1446 MB, throughput 0.885737
Reading from 1470: heap size 1455 MB, throughput 0.96697
Equal recommendation: 3759 MB each
Reading from 1471: heap size 1548 MB, throughput 0.986627
Reading from 1470: heap size 1459 MB, throughput 0.994502
Reading from 1471: heap size 1617 MB, throughput 0.992968
Reading from 1470: heap size 1485 MB, throughput 0.991348
Equal recommendation: 3759 MB each
Reading from 1471: heap size 1617 MB, throughput 0.992072
Reading from 1470: heap size 1490 MB, throughput 0.9896
Reading from 1471: heap size 1683 MB, throughput 0.990141
Reading from 1470: heap size 1496 MB, throughput 0.989513
Equal recommendation: 3759 MB each
Reading from 1470: heap size 1501 MB, throughput 0.988214
Reading from 1471: heap size 1686 MB, throughput 0.992762
Reading from 1470: heap size 1492 MB, throughput 0.98443
Client 1471 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 1470: heap size 1500 MB, throughput 0.988396
Reading from 1470: heap size 1496 MB, throughput 0.988221
Recommendation: one client; give it all the memory
Reading from 1470: heap size 1499 MB, throughput 0.986795
Recommendation: one client; give it all the memory
Reading from 1470: heap size 1507 MB, throughput 0.987213
Reading from 1470: heap size 1508 MB, throughput 0.985402
Recommendation: one client; give it all the memory
Reading from 1470: heap size 1513 MB, throughput 0.98503
Reading from 1470: heap size 1521 MB, throughput 0.983785
Recommendation: one client; give it all the memory
Reading from 1470: heap size 1526 MB, throughput 0.982597
Reading from 1470: heap size 1537 MB, throughput 0.980482
Recommendation: one client; give it all the memory
Reading from 1470: heap size 1543 MB, throughput 0.980331
Reading from 1470: heap size 1557 MB, throughput 0.980214
Recommendation: one client; give it all the memory
Reading from 1470: heap size 1567 MB, throughput 0.9792
Recommendation: one client; give it all the memory
Reading from 1470: heap size 1576 MB, throughput 0.980948
Reading from 1470: heap size 1586 MB, throughput 0.978865
Recommendation: one client; give it all the memory
Reading from 1470: heap size 1591 MB, throughput 0.978833
Reading from 1470: heap size 1600 MB, throughput 0.97973
Recommendation: one client; give it all the memory
Reading from 1470: heap size 1603 MB, throughput 0.979509
Reading from 1470: heap size 1612 MB, throughput 0.979087
Recommendation: one client; give it all the memory
Reading from 1470: heap size 1612 MB, throughput 0.834481
Reading from 1470: heap size 1690 MB, throughput 0.996299
Recommendation: one client; give it all the memory
Reading from 1470: heap size 1691 MB, throughput 0.994977
Recommendation: one client; give it all the memory
Reading from 1470: heap size 1707 MB, throughput 0.993691
Reading from 1470: heap size 1712 MB, throughput 0.990666
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 1470: heap size 1711 MB, throughput 0.996357
Recommendation: one client; give it all the memory
Reading from 1470: heap size 1561 MB, throughput 0.988632
Reading from 1470: heap size 1697 MB, throughput 0.863079
Reading from 1470: heap size 1706 MB, throughput 0.76406
Reading from 1470: heap size 1731 MB, throughput 0.804356
Reading from 1470: heap size 1753 MB, throughput 0.785719
Client 1470 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
