economemd
    total memory: 2208 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub43_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run09_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 18773: heap size 9 MB, throughput 0.992066
Clients: 1
Client 18773 has a minimum heap size of 276 MB
Reading from 18774: heap size 9 MB, throughput 0.974279
Clients: 2
Client 18774 has a minimum heap size of 276 MB
Reading from 18773: heap size 9 MB, throughput 0.988946
Reading from 18774: heap size 9 MB, throughput 0.972299
Reading from 18773: heap size 9 MB, throughput 0.983315
Reading from 18773: heap size 9 MB, throughput 0.975384
Reading from 18774: heap size 11 MB, throughput 0.974456
Reading from 18773: heap size 11 MB, throughput 0.975488
Reading from 18774: heap size 11 MB, throughput 0.976589
Reading from 18773: heap size 11 MB, throughput 0.974616
Reading from 18774: heap size 15 MB, throughput 0.913804
Reading from 18773: heap size 17 MB, throughput 0.972338
Reading from 18774: heap size 18 MB, throughput 0.911237
Reading from 18773: heap size 17 MB, throughput 0.913657
Reading from 18773: heap size 30 MB, throughput 0.753324
Reading from 18774: heap size 23 MB, throughput 0.81047
Reading from 18773: heap size 31 MB, throughput 0.345813
Reading from 18774: heap size 28 MB, throughput 0.727083
Reading from 18774: heap size 36 MB, throughput 0.663057
Reading from 18773: heap size 35 MB, throughput 0.664982
Reading from 18774: heap size 38 MB, throughput 0.587517
Reading from 18774: heap size 41 MB, throughput 0.143552
Reading from 18773: heap size 47 MB, throughput 0.489989
Reading from 18774: heap size 42 MB, throughput 0.857554
Reading from 18773: heap size 48 MB, throughput 0.605637
Reading from 18773: heap size 64 MB, throughput 0.515319
Reading from 18774: heap size 46 MB, throughput 0.556912
Reading from 18773: heap size 71 MB, throughput 0.517428
Reading from 18774: heap size 59 MB, throughput 0.264267
Reading from 18774: heap size 79 MB, throughput 0.425206
Reading from 18773: heap size 71 MB, throughput 0.468931
Reading from 18774: heap size 82 MB, throughput 0.292258
Reading from 18773: heap size 101 MB, throughput 0.577226
Reading from 18774: heap size 84 MB, throughput 0.757403
Reading from 18774: heap size 87 MB, throughput 0.707987
Reading from 18773: heap size 101 MB, throughput 0.329573
Reading from 18774: heap size 90 MB, throughput 0.470756
Reading from 18773: heap size 134 MB, throughput 0.728648
Reading from 18774: heap size 115 MB, throughput 0.729537
Reading from 18773: heap size 134 MB, throughput 0.590728
Reading from 18774: heap size 118 MB, throughput 0.654077
Reading from 18773: heap size 136 MB, throughput 0.317507
Reading from 18774: heap size 121 MB, throughput 0.615376
Reading from 18774: heap size 124 MB, throughput 0.601294
Reading from 18773: heap size 139 MB, throughput 0.27658
Reading from 18774: heap size 130 MB, throughput 0.341396
Reading from 18773: heap size 142 MB, throughput 0.444193
Reading from 18774: heap size 163 MB, throughput 0.71239
Reading from 18774: heap size 165 MB, throughput 0.681174
Reading from 18773: heap size 182 MB, throughput 0.397767
Reading from 18774: heap size 167 MB, throughput 0.641532
Reading from 18773: heap size 185 MB, throughput 0.226927
Reading from 18774: heap size 170 MB, throughput 0.629441
Reading from 18773: heap size 188 MB, throughput 0.0266161
Reading from 18774: heap size 173 MB, throughput 0.651497
Reading from 18773: heap size 196 MB, throughput 0.752858
Reading from 18774: heap size 180 MB, throughput 0.610033
Reading from 18774: heap size 185 MB, throughput 0.553959
Reading from 18773: heap size 200 MB, throughput 0.551327
Reading from 18774: heap size 190 MB, throughput 0.432983
Reading from 18773: heap size 240 MB, throughput 0.895171
Reading from 18773: heap size 251 MB, throughput 0.871808
Reading from 18774: heap size 232 MB, throughput 0.870427
Reading from 18773: heap size 248 MB, throughput 0.867563
Reading from 18774: heap size 233 MB, throughput 0.84026
Reading from 18773: heap size 250 MB, throughput 0.832987
Reading from 18774: heap size 237 MB, throughput 0.754927
Reading from 18774: heap size 241 MB, throughput 0.623474
Reading from 18773: heap size 252 MB, throughput 0.787176
Reading from 18774: heap size 246 MB, throughput 0.595034
Reading from 18773: heap size 253 MB, throughput 0.774915
Reading from 18774: heap size 248 MB, throughput 0.576083
Reading from 18773: heap size 254 MB, throughput 0.897424
Reading from 18773: heap size 255 MB, throughput 0.847758
Reading from 18773: heap size 259 MB, throughput 0.828413
Reading from 18774: heap size 247 MB, throughput 0.539306
Reading from 18773: heap size 262 MB, throughput 0.750445
Reading from 18774: heap size 289 MB, throughput 0.689676
Reading from 18773: heap size 268 MB, throughput 0.709531
Reading from 18774: heap size 287 MB, throughput 0.777397
Reading from 18773: heap size 269 MB, throughput 0.75359
Reading from 18774: heap size 289 MB, throughput 0.822433
Reading from 18774: heap size 285 MB, throughput 0.805472
Reading from 18774: heap size 288 MB, throughput 0.791395
Reading from 18774: heap size 283 MB, throughput 0.807319
Reading from 18773: heap size 272 MB, throughput 0.911235
Reading from 18774: heap size 286 MB, throughput 0.816415
Reading from 18773: heap size 275 MB, throughput 0.864001
Reading from 18774: heap size 283 MB, throughput 0.8344
Reading from 18774: heap size 285 MB, throughput 0.896228
Reading from 18774: heap size 287 MB, throughput 0.871667
Reading from 18773: heap size 282 MB, throughput 0.703022
Reading from 18774: heap size 288 MB, throughput 0.86217
Reading from 18773: heap size 327 MB, throughput 0.781836
Reading from 18774: heap size 288 MB, throughput 0.857732
Reading from 18774: heap size 290 MB, throughput 0.846061
Reading from 18773: heap size 332 MB, throughput 0.824595
Reading from 18774: heap size 291 MB, throughput 0.898814
Reading from 18773: heap size 332 MB, throughput 0.853604
Reading from 18774: heap size 292 MB, throughput 0.899071
Reading from 18773: heap size 334 MB, throughput 0.797871
Reading from 18774: heap size 294 MB, throughput 0.87621
Reading from 18773: heap size 335 MB, throughput 0.765905
Reading from 18773: heap size 342 MB, throughput 0.698698
Numeric result:
Recommendation: 2 clients, utility 0.415564:
    h1: 276 MB (U(h) = 0.600282*h^0.00155374)
    h2: 1932 MB (U(h) = 0.587635*h^0.0205058)
Recommendation: 2 clients, utility 0.415564:
    h1: 276 MB (U(h) = 0.600282*h^0.00155374)
    h2: 1932 MB (U(h) = 0.587635*h^0.0205058)
Reading from 18773: heap size 345 MB, throughput 0.613829
Reading from 18773: heap size 354 MB, throughput 0.634992
Reading from 18773: heap size 354 MB, throughput 0.669023
Reading from 18774: heap size 295 MB, throughput 0.638879
Reading from 18774: heap size 336 MB, throughput 0.698314
Reading from 18774: heap size 336 MB, throughput 0.682358
Reading from 18774: heap size 337 MB, throughput 0.670362
Reading from 18774: heap size 338 MB, throughput 0.677084
Reading from 18774: heap size 340 MB, throughput 0.875633
Reading from 18773: heap size 351 MB, throughput 0.947768
Reading from 18774: heap size 341 MB, throughput 0.957817
Reading from 18773: heap size 320 MB, throughput 0.960193
Reading from 18773: heap size 344 MB, throughput 0.951749
Reading from 18774: heap size 347 MB, throughput 0.946309
Reading from 18774: heap size 347 MB, throughput 0.958319
Reading from 18773: heap size 323 MB, throughput 0.961011
Reading from 18773: heap size 342 MB, throughput 0.961713
Reading from 18774: heap size 348 MB, throughput 0.96405
Reading from 18773: heap size 311 MB, throughput 0.967574
Reading from 18774: heap size 350 MB, throughput 0.968608
Reading from 18773: heap size 336 MB, throughput 0.971245
Reading from 18773: heap size 313 MB, throughput 0.969528
Reading from 18774: heap size 353 MB, throughput 0.967715
Numeric result:
Recommendation: 2 clients, utility 0.553852:
    h1: 1097.89 MB (U(h) = 0.484642*h^0.0576456)
    h2: 1110.11 MB (U(h) = 0.507216*h^0.0582869)
Recommendation: 2 clients, utility 0.553852:
    h1: 1097.89 MB (U(h) = 0.484642*h^0.0576456)
    h2: 1110.11 MB (U(h) = 0.507216*h^0.0582869)
Reading from 18773: heap size 330 MB, throughput 0.967828
Reading from 18774: heap size 354 MB, throughput 0.971797
Reading from 18773: heap size 333 MB, throughput 0.966354
Reading from 18774: heap size 358 MB, throughput 0.969025
Reading from 18773: heap size 332 MB, throughput 0.970303
Reading from 18773: heap size 333 MB, throughput 0.228298
Reading from 18774: heap size 360 MB, throughput 0.919242
Reading from 18773: heap size 333 MB, throughput 0.456071
Reading from 18773: heap size 334 MB, throughput 0.590975
Reading from 18774: heap size 398 MB, throughput 0.982321
Reading from 18773: heap size 336 MB, throughput 0.610337
Reading from 18774: heap size 399 MB, throughput 0.987564
Reading from 18773: heap size 336 MB, throughput 0.419281
Reading from 18774: heap size 404 MB, throughput 0.988188
Reading from 18773: heap size 338 MB, throughput 0.391566
Reading from 18774: heap size 406 MB, throughput 0.988228
Numeric result:
Recommendation: 2 clients, utility 0.587432:
    h1: 707.978 MB (U(h) = 0.513474*h^0.0426989)
    h2: 1500.02 MB (U(h) = 0.446075*h^0.0904688)
Recommendation: 2 clients, utility 0.587432:
    h1: 707.974 MB (U(h) = 0.513474*h^0.0426989)
    h2: 1500.03 MB (U(h) = 0.446075*h^0.0904688)
Reading from 18773: heap size 338 MB, throughput 0.532075
Reading from 18774: heap size 406 MB, throughput 0.988438
Reading from 18773: heap size 338 MB, throughput 0.972042
Reading from 18773: heap size 340 MB, throughput 0.926596
Reading from 18773: heap size 380 MB, throughput 0.8789
Reading from 18773: heap size 387 MB, throughput 0.890889
Reading from 18773: heap size 392 MB, throughput 0.906641
Reading from 18773: heap size 395 MB, throughput 0.92282
Reading from 18773: heap size 399 MB, throughput 0.938546
Reading from 18774: heap size 407 MB, throughput 0.990542
Reading from 18773: heap size 400 MB, throughput 0.987233
Reading from 18774: heap size 401 MB, throughput 0.984187
Reading from 18774: heap size 404 MB, throughput 0.967898
Reading from 18774: heap size 400 MB, throughput 0.943553
Reading from 18774: heap size 407 MB, throughput 0.906826
Reading from 18774: heap size 415 MB, throughput 0.869036
Reading from 18774: heap size 419 MB, throughput 0.905119
Reading from 18773: heap size 399 MB, throughput 0.992334
Reading from 18773: heap size 401 MB, throughput 0.990065
Reading from 18774: heap size 424 MB, throughput 0.970346
Reading from 18773: heap size 397 MB, throughput 0.983439
Reading from 18774: heap size 428 MB, throughput 0.982443
Reading from 18773: heap size 354 MB, throughput 0.983917
Reading from 18774: heap size 422 MB, throughput 0.981513
Reading from 18773: heap size 392 MB, throughput 0.982165
Numeric result:
Recommendation: 2 clients, utility 0.728603:
    h1: 1021.5 MB (U(h) = 0.41342*h^0.0964938)
    h2: 1186.5 MB (U(h) = 0.408516*h^0.112065)
Recommendation: 2 clients, utility 0.728603:
    h1: 1021.58 MB (U(h) = 0.41342*h^0.0964938)
    h2: 1186.42 MB (U(h) = 0.408516*h^0.112065)
Reading from 18774: heap size 427 MB, throughput 0.980245
Reading from 18773: heap size 361 MB, throughput 0.982154
Reading from 18773: heap size 390 MB, throughput 0.984544
Reading from 18774: heap size 423 MB, throughput 0.980954
Reading from 18773: heap size 392 MB, throughput 0.982855
Reading from 18774: heap size 427 MB, throughput 0.977553
Reading from 18773: heap size 392 MB, throughput 0.86636
Reading from 18774: heap size 423 MB, throughput 0.982859
Reading from 18773: heap size 392 MB, throughput 0.807407
Reading from 18773: heap size 393 MB, throughput 0.456794
Reading from 18774: heap size 426 MB, throughput 0.983233
Reading from 18773: heap size 394 MB, throughput 0.976806
Reading from 18774: heap size 423 MB, throughput 0.984217
Reading from 18773: heap size 396 MB, throughput 0.974277
Numeric result:
Recommendation: 2 clients, utility 0.759921:
    h1: 1024.06 MB (U(h) = 0.402175*h^0.10336)
    h2: 1183.94 MB (U(h) = 0.396266*h^0.119486)
Recommendation: 2 clients, utility 0.759921:
    h1: 1024.11 MB (U(h) = 0.402175*h^0.10336)
    h2: 1183.89 MB (U(h) = 0.396266*h^0.119486)
Reading from 18774: heap size 425 MB, throughput 0.982052
Reading from 18773: heap size 398 MB, throughput 0.975578
Reading from 18773: heap size 400 MB, throughput 0.971187
Reading from 18774: heap size 423 MB, throughput 0.981505
Reading from 18773: heap size 402 MB, throughput 0.975286
Reading from 18773: heap size 406 MB, throughput 0.962128
Reading from 18773: heap size 408 MB, throughput 0.935218
Reading from 18773: heap size 413 MB, throughput 0.903324
Reading from 18773: heap size 417 MB, throughput 0.856197
Reading from 18773: heap size 425 MB, throughput 0.848078
Reading from 18774: heap size 425 MB, throughput 0.987342
Reading from 18773: heap size 426 MB, throughput 0.962684
Reading from 18774: heap size 426 MB, throughput 0.984145
Reading from 18774: heap size 427 MB, throughput 0.971538
Reading from 18774: heap size 426 MB, throughput 0.953778
Reading from 18774: heap size 429 MB, throughput 0.925953
Reading from 18773: heap size 427 MB, throughput 0.980166
Reading from 18774: heap size 436 MB, throughput 0.97294
Reading from 18773: heap size 430 MB, throughput 0.980208
Reading from 18774: heap size 438 MB, throughput 0.98114
Reading from 18773: heap size 429 MB, throughput 0.983122
Numeric result:
Recommendation: 2 clients, utility 0.836058:
    h1: 1116.54 MB (U(h) = 0.359921*h^0.129847)
    h2: 1091.46 MB (U(h) = 0.384295*h^0.126927)
Recommendation: 2 clients, utility 0.836058:
    h1: 1116.55 MB (U(h) = 0.359921*h^0.129847)
    h2: 1091.45 MB (U(h) = 0.384295*h^0.126927)
Reading from 18774: heap size 442 MB, throughput 0.984159
Reading from 18773: heap size 432 MB, throughput 0.983899
Reading from 18774: heap size 444 MB, throughput 0.984464
Reading from 18773: heap size 429 MB, throughput 0.981783
Reading from 18773: heap size 432 MB, throughput 0.981351
Reading from 18774: heap size 445 MB, throughput 0.986923
Reading from 18773: heap size 427 MB, throughput 0.984271
Reading from 18774: heap size 446 MB, throughput 0.986107
Reading from 18773: heap size 430 MB, throughput 0.982258
Reading from 18774: heap size 444 MB, throughput 0.986625
Reading from 18773: heap size 428 MB, throughput 0.981086
Numeric result:
Recommendation: 2 clients, utility 0.864834:
    h1: 1105.11 MB (U(h) = 0.352806*h^0.134543)
    h2: 1102.89 MB (U(h) = 0.37275*h^0.134267)
Recommendation: 2 clients, utility 0.864834:
    h1: 1105.13 MB (U(h) = 0.352806*h^0.134543)
    h2: 1102.87 MB (U(h) = 0.37275*h^0.134267)
Reading from 18774: heap size 446 MB, throughput 0.986259
Reading from 18773: heap size 430 MB, throughput 0.980348
Reading from 18773: heap size 430 MB, throughput 0.976655
Reading from 18774: heap size 447 MB, throughput 0.984362
Reading from 18774: heap size 448 MB, throughput 0.982809
Reading from 18773: heap size 431 MB, throughput 0.980014
Reading from 18773: heap size 434 MB, throughput 0.972231
Reading from 18773: heap size 435 MB, throughput 0.95655
Reading from 18773: heap size 439 MB, throughput 0.93655
Reading from 18773: heap size 442 MB, throughput 0.921473
Reading from 18773: heap size 452 MB, throughput 0.976703
Reading from 18774: heap size 450 MB, throughput 0.987891
Reading from 18774: heap size 451 MB, throughput 0.982552
Reading from 18774: heap size 452 MB, throughput 0.971181
Reading from 18774: heap size 454 MB, throughput 0.956374
Reading from 18773: heap size 452 MB, throughput 0.984013
Reading from 18774: heap size 460 MB, throughput 0.98056
Reading from 18773: heap size 455 MB, throughput 0.985906
Reading from 18774: heap size 461 MB, throughput 0.987568
Numeric result:
Recommendation: 2 clients, utility 0.923199:
    h1: 1111.08 MB (U(h) = 0.334187*h^0.147159)
    h2: 1096.92 MB (U(h) = 0.355978*h^0.145279)
Recommendation: 2 clients, utility 0.923199:
    h1: 1111.1 MB (U(h) = 0.334187*h^0.147159)
    h2: 1096.9 MB (U(h) = 0.355978*h^0.145279)
Reading from 18773: heap size 457 MB, throughput 0.987443
Reading from 18774: heap size 465 MB, throughput 0.987761
Reading from 18773: heap size 456 MB, throughput 0.986779
Reading from 18774: heap size 467 MB, throughput 0.987314
Reading from 18773: heap size 458 MB, throughput 0.985624
Reading from 18774: heap size 466 MB, throughput 0.986764
Reading from 18773: heap size 456 MB, throughput 0.986865
Reading from 18774: heap size 468 MB, throughput 0.986008
Reading from 18773: heap size 458 MB, throughput 0.984383
Numeric result:
Recommendation: 2 clients, utility 0.948626:
    h1: 1113.39 MB (U(h) = 0.326463*h^0.15252)
    h2: 1094.61 MB (U(h) = 0.349034*h^0.149943)
Recommendation: 2 clients, utility 0.948626:
    h1: 1113.41 MB (U(h) = 0.326463*h^0.15252)
    h2: 1094.59 MB (U(h) = 0.349034*h^0.149943)
Reading from 18774: heap size 467 MB, throughput 0.987002
Reading from 18773: heap size 460 MB, throughput 0.984232
Reading from 18773: heap size 461 MB, throughput 0.980701
Reading from 18774: heap size 469 MB, throughput 0.986106
Reading from 18773: heap size 460 MB, throughput 0.982527
Reading from 18773: heap size 464 MB, throughput 0.97405
Reading from 18773: heap size 465 MB, throughput 0.954536
Reading from 18773: heap size 467 MB, throughput 0.945424
Reading from 18774: heap size 471 MB, throughput 0.98381
Reading from 18773: heap size 476 MB, throughput 0.983589
Reading from 18774: heap size 471 MB, throughput 0.986622
Reading from 18774: heap size 475 MB, throughput 0.979836
Reading from 18774: heap size 475 MB, throughput 0.964718
Reading from 18774: heap size 481 MB, throughput 0.97306
Reading from 18773: heap size 476 MB, throughput 0.988232
Numeric result:
Recommendation: 2 clients, utility 0.979881:
    h1: 1129.33 MB (U(h) = 0.314628*h^0.160924)
    h2: 1078.67 MB (U(h) = 0.343505*h^0.153705)
Recommendation: 2 clients, utility 0.979881:
    h1: 1129.33 MB (U(h) = 0.314628*h^0.160924)
    h2: 1078.67 MB (U(h) = 0.343505*h^0.153705)
Reading from 18774: heap size 482 MB, throughput 0.986239
Reading from 18773: heap size 479 MB, throughput 0.988179
Reading from 18774: heap size 486 MB, throughput 0.98844
Reading from 18773: heap size 481 MB, throughput 0.989456
Reading from 18774: heap size 488 MB, throughput 0.989267
Reading from 18773: heap size 480 MB, throughput 0.988233
Reading from 18773: heap size 482 MB, throughput 0.987552
Reading from 18774: heap size 486 MB, throughput 0.989575
Numeric result:
Recommendation: 2 clients, utility 1.09316:
    h1: 948.4 MB (U(h) = 0.307057*h^0.166396)
    h2: 1259.6 MB (U(h) = 0.234959*h^0.220988)
Recommendation: 2 clients, utility 1.09316:
    h1: 948.419 MB (U(h) = 0.307057*h^0.166396)
    h2: 1259.58 MB (U(h) = 0.234959*h^0.220988)
Reading from 18773: heap size 482 MB, throughput 0.98676
Reading from 18774: heap size 489 MB, throughput 0.988927
Reading from 18773: heap size 483 MB, throughput 0.987701
Reading from 18774: heap size 488 MB, throughput 0.988651
Reading from 18773: heap size 486 MB, throughput 0.991089
Reading from 18773: heap size 487 MB, throughput 0.984378
Reading from 18773: heap size 487 MB, throughput 0.970322
Reading from 18774: heap size 489 MB, throughput 0.986867
Reading from 18773: heap size 489 MB, throughput 0.96947
Reading from 18773: heap size 498 MB, throughput 0.988792
Reading from 18774: heap size 492 MB, throughput 0.990433
Reading from 18774: heap size 493 MB, throughput 0.984791
Reading from 18774: heap size 492 MB, throughput 0.974917
Numeric result:
Recommendation: 2 clients, utility 1.70433:
    h1: 1221.3 MB (U(h) = 0.0734473*h^0.413555)
    h2: 986.698 MB (U(h) = 0.122657*h^0.334086)
Recommendation: 2 clients, utility 1.70433:
    h1: 1221.35 MB (U(h) = 0.0734473*h^0.413555)
    h2: 986.654 MB (U(h) = 0.122657*h^0.334086)
Reading from 18774: heap size 494 MB, throughput 0.977556
Reading from 18773: heap size 498 MB, throughput 0.991302
Reading from 18774: heap size 502 MB, throughput 0.988723
Reading from 18773: heap size 500 MB, throughput 0.991605
Reading from 18774: heap size 502 MB, throughput 0.991085
Reading from 18773: heap size 502 MB, throughput 0.990086
Reading from 18774: heap size 504 MB, throughput 0.990195
Reading from 18773: heap size 499 MB, throughput 0.989015
Numeric result:
Recommendation: 2 clients, utility 2.19223:
    h1: 1237.96 MB (U(h) = 0.0348306*h^0.540555)
    h2: 970.043 MB (U(h) = 0.0727851*h^0.423567)
Recommendation: 2 clients, utility 2.19223:
    h1: 1237.96 MB (U(h) = 0.0348306*h^0.540555)
    h2: 970.039 MB (U(h) = 0.0727851*h^0.423567)
Reading from 18774: heap size 505 MB, throughput 0.989023
Reading from 18773: heap size 502 MB, throughput 0.989355
Reading from 18773: heap size 504 MB, throughput 0.987358
Reading from 18774: heap size 504 MB, throughput 0.988634
Reading from 18773: heap size 504 MB, throughput 0.989584
Reading from 18774: heap size 506 MB, throughput 0.984203
Reading from 18773: heap size 507 MB, throughput 0.974002
Reading from 18773: heap size 508 MB, throughput 0.962432
Reading from 18773: heap size 516 MB, throughput 0.972812
Reading from 18774: heap size 508 MB, throughput 0.989223
Reading from 18773: heap size 518 MB, throughput 0.98624
Numeric result:
Recommendation: 2 clients, utility 3.41862:
    h1: 1457.39 MB (U(h) = 0.00479007*h^0.874165)
    h2: 750.609 MB (U(h) = 0.0621491*h^0.450226)
Recommendation: 2 clients, utility 3.41862:
    h1: 1457.39 MB (U(h) = 0.00479007*h^0.874165)
    h2: 750.609 MB (U(h) = 0.0621491*h^0.450226)
Reading from 18774: heap size 509 MB, throughput 0.986044
Reading from 18774: heap size 508 MB, throughput 0.978691
Reading from 18774: heap size 511 MB, throughput 0.975516
Reading from 18773: heap size 521 MB, throughput 0.988175
Reading from 18774: heap size 518 MB, throughput 0.987534
Reading from 18773: heap size 524 MB, throughput 0.989307
Reading from 18774: heap size 519 MB, throughput 0.989932
Reading from 18773: heap size 523 MB, throughput 0.989201
Reading from 18774: heap size 519 MB, throughput 0.989948
Numeric result:
Recommendation: 2 clients, utility 3.11994:
    h1: 1477.55 MB (U(h) = 0.0061752*h^0.830841)
    h2: 730.451 MB (U(h) = 0.078329*h^0.410738)
Recommendation: 2 clients, utility 3.11994:
    h1: 1477.55 MB (U(h) = 0.0061752*h^0.830841)
    h2: 730.449 MB (U(h) = 0.078329*h^0.410738)
Reading from 18773: heap size 526 MB, throughput 0.988912
Reading from 18774: heap size 521 MB, throughput 0.990993
Reading from 18773: heap size 529 MB, throughput 0.98972
Reading from 18774: heap size 520 MB, throughput 0.989729
Reading from 18773: heap size 529 MB, throughput 0.990659
Reading from 18773: heap size 532 MB, throughput 0.984035
Reading from 18773: heap size 533 MB, throughput 0.972828
Reading from 18774: heap size 522 MB, throughput 0.990394
Reading from 18773: heap size 539 MB, throughput 0.986596
Numeric result:
Recommendation: 2 clients, utility 1.86151:
    h1: 892.815 MB (U(h) = 0.139769*h^0.313037)
    h2: 1315.18 MB (U(h) = 0.057877*h^0.461127)
Recommendation: 2 clients, utility 1.86151:
    h1: 892.815 MB (U(h) = 0.139769*h^0.313037)
    h2: 1315.18 MB (U(h) = 0.057877*h^0.461127)
Reading from 18774: heap size 524 MB, throughput 0.991359
Reading from 18774: heap size 525 MB, throughput 0.984011
Reading from 18774: heap size 524 MB, throughput 0.975166
Reading from 18773: heap size 541 MB, throughput 0.990355
Reading from 18774: heap size 526 MB, throughput 0.987121
Reading from 18773: heap size 544 MB, throughput 0.990137
Reading from 18774: heap size 534 MB, throughput 0.99058
Reading from 18773: heap size 546 MB, throughput 0.990475
Reading from 18774: heap size 535 MB, throughput 0.99142
Numeric result:
Recommendation: 2 clients, utility 1.92361:
    h1: 933.227 MB (U(h) = 0.114211*h^0.346064)
    h2: 1274.77 MB (U(h) = 0.0537792*h^0.472717)
Recommendation: 2 clients, utility 1.92361:
    h1: 933.228 MB (U(h) = 0.114211*h^0.346064)
    h2: 1274.77 MB (U(h) = 0.0537792*h^0.472717)
Reading from 18773: heap size 544 MB, throughput 0.989409
Reading from 18774: heap size 535 MB, throughput 0.990766
Reading from 18773: heap size 547 MB, throughput 0.989975
Reading from 18774: heap size 537 MB, throughput 0.989067
Reading from 18773: heap size 550 MB, throughput 0.991139
Reading from 18773: heap size 551 MB, throughput 0.985026
Reading from 18773: heap size 552 MB, throughput 0.980544
Reading from 18774: heap size 537 MB, throughput 0.98831
Reading from 18773: heap size 554 MB, throughput 0.989602
Numeric result:
Recommendation: 2 clients, utility 1.98346:
    h1: 969.524 MB (U(h) = 0.0940216*h^0.377517)
    h2: 1238.48 MB (U(h) = 0.0507208*h^0.482246)
Recommendation: 2 clients, utility 1.98346:
    h1: 969.52 MB (U(h) = 0.0940216*h^0.377517)
    h2: 1238.48 MB (U(h) = 0.0507208*h^0.482246)
Reading from 18774: heap size 538 MB, throughput 0.99028
Reading from 18774: heap size 540 MB, throughput 0.98429
Reading from 18774: heap size 541 MB, throughput 0.971193
Reading from 18773: heap size 557 MB, throughput 0.991697
Reading from 18774: heap size 548 MB, throughput 0.987223
Reading from 18773: heap size 559 MB, throughput 0.991293
Reading from 18774: heap size 550 MB, throughput 0.989982
Reading from 18773: heap size 557 MB, throughput 0.990022
Numeric result:
Recommendation: 2 clients, utility 1.95564:
    h1: 984.503 MB (U(h) = 0.0925171*h^0.379988)
    h2: 1223.5 MB (U(h) = 0.0536497*h^0.472248)
Recommendation: 2 clients, utility 1.95564:
    h1: 984.485 MB (U(h) = 0.0925171*h^0.379988)
    h2: 1223.52 MB (U(h) = 0.0536497*h^0.472248)
Reading from 18774: heap size 553 MB, throughput 0.990806
Reading from 18773: heap size 560 MB, throughput 0.988889
Reading from 18774: heap size 555 MB, throughput 0.990875
Reading from 18773: heap size 563 MB, throughput 0.991606
Reading from 18773: heap size 563 MB, throughput 0.986684
Reading from 18773: heap size 562 MB, throughput 0.98124
Reading from 18774: heap size 554 MB, throughput 0.990001
Reading from 18773: heap size 565 MB, throughput 0.986719
Numeric result:
Recommendation: 2 clients, utility 1.90559:
    h1: 1027.11 MB (U(h) = 0.0878257*h^0.388052)
    h2: 1180.89 MB (U(h) = 0.0626722*h^0.446151)
Recommendation: 2 clients, utility 1.90559:
    h1: 1027.11 MB (U(h) = 0.0878257*h^0.388052)
    h2: 1180.89 MB (U(h) = 0.0626722*h^0.446151)
Reading from 18774: heap size 557 MB, throughput 0.989447
Reading from 18773: heap size 571 MB, throughput 0.990277
Reading from 18774: heap size 557 MB, throughput 0.987352
Reading from 18774: heap size 562 MB, throughput 0.980757
Reading from 18774: heap size 566 MB, throughput 0.986378
Reading from 18773: heap size 573 MB, throughput 0.990875
Reading from 18774: heap size 567 MB, throughput 0.990616
Reading from 18773: heap size 572 MB, throughput 0.99024
Numeric result:
Recommendation: 2 clients, utility 1.68866:
    h1: 1087.1 MB (U(h) = 0.114022*h^0.345485)
    h2: 1120.9 MB (U(h) = 0.108451*h^0.356226)
Recommendation: 2 clients, utility 1.68866:
    h1: 1087.1 MB (U(h) = 0.114022*h^0.345485)
    h2: 1120.9 MB (U(h) = 0.108451*h^0.356226)
Reading from 18774: heap size 570 MB, throughput 0.990925
Reading from 18773: heap size 574 MB, throughput 0.990968
Reading from 18774: heap size 571 MB, throughput 0.991111
Reading from 18773: heap size 577 MB, throughput 0.992449
Reading from 18773: heap size 577 MB, throughput 0.988805
Reading from 18773: heap size 576 MB, throughput 0.980811
Reading from 18774: heap size 570 MB, throughput 0.989697
Reading from 18773: heap size 579 MB, throughput 0.988171
Numeric result:
Recommendation: 2 clients, utility 1.57931:
    h1: 1188.25 MB (U(h) = 0.120441*h^0.336294)
    h2: 1019.75 MB (U(h) = 0.16419*h^0.288607)
Recommendation: 2 clients, utility 1.57931:
    h1: 1188.25 MB (U(h) = 0.120441*h^0.336294)
    h2: 1019.75 MB (U(h) = 0.16419*h^0.288607)
Reading from 18774: heap size 572 MB, throughput 0.985872
Reading from 18773: heap size 585 MB, throughput 0.988063
Reading from 18774: heap size 572 MB, throughput 0.986194
Reading from 18774: heap size 575 MB, throughput 0.979523
Reading from 18773: heap size 587 MB, throughput 0.990029
Reading from 18774: heap size 580 MB, throughput 0.986862
Reading from 18773: heap size 586 MB, throughput 0.990814
Numeric result:
Recommendation: 2 clients, utility 1.5972:
    h1: 1257.93 MB (U(h) = 0.101313*h^0.363868)
    h2: 950.073 MB (U(h) = 0.17844*h^0.274815)
Recommendation: 2 clients, utility 1.5972:
    h1: 1257.93 MB (U(h) = 0.101313*h^0.363868)
    h2: 950.066 MB (U(h) = 0.17844*h^0.274815)
Reading from 18774: heap size 580 MB, throughput 0.992116
Reading from 18773: heap size 589 MB, throughput 0.983946
Reading from 18774: heap size 583 MB, throughput 0.992315
Reading from 18773: heap size 591 MB, throughput 0.991639
Reading from 18773: heap size 591 MB, throughput 0.986426
Reading from 18774: heap size 585 MB, throughput 0.991822
Reading from 18773: heap size 595 MB, throughput 0.98575
Numeric result:
Recommendation: 2 clients, utility 1.65621:
    h1: 1342.39 MB (U(h) = 0.0750854*h^0.411663)
    h2: 865.607 MB (U(h) = 0.188883*h^0.265457)
Recommendation: 2 clients, utility 1.65621:
    h1: 1342.38 MB (U(h) = 0.0750854*h^0.411663)
    h2: 865.621 MB (U(h) = 0.188883*h^0.265457)
Reading from 18774: heap size 582 MB, throughput 0.991029
Reading from 18773: heap size 596 MB, throughput 0.992144
Reading from 18774: heap size 585 MB, throughput 0.99246
Reading from 18774: heap size 586 MB, throughput 0.985608
Reading from 18774: heap size 587 MB, throughput 0.983316
Reading from 18773: heap size 600 MB, throughput 0.992598
Reading from 18774: heap size 596 MB, throughput 0.990848
Reading from 18773: heap size 602 MB, throughput 0.991053
Numeric result:
Recommendation: 2 clients, utility 1.44008:
    h1: 1044.07 MB (U(h) = 0.209848*h^0.246446)
    h2: 1163.93 MB (U(h) = 0.177912*h^0.274736)
Recommendation: 2 clients, utility 1.44008:
    h1: 1044.07 MB (U(h) = 0.209848*h^0.246446)
    h2: 1163.93 MB (U(h) = 0.177912*h^0.274736)
Reading from 18774: heap size 596 MB, throughput 0.991779
Reading from 18773: heap size 602 MB, throughput 0.989939
Reading from 18774: heap size 596 MB, throughput 0.992134
Reading from 18773: heap size 604 MB, throughput 0.992059
Reading from 18773: heap size 605 MB, throughput 0.988766
Reading from 18773: heap size 607 MB, throughput 0.982789
Numeric result:
Recommendation: 2 clients, utility 1.39976:
    h1: 943.407 MB (U(h) = 0.271468*h^0.20502)
    h2: 1264.59 MB (U(h) = 0.177825*h^0.274818)
Recommendation: 2 clients, utility 1.39976:
    h1: 943.411 MB (U(h) = 0.271468*h^0.20502)
    h2: 1264.59 MB (U(h) = 0.177825*h^0.274818)
Reading from 18774: heap size 598 MB, throughput 0.991321
Reading from 18773: heap size 616 MB, throughput 0.991321
Reading from 18774: heap size 599 MB, throughput 0.992776
Reading from 18774: heap size 600 MB, throughput 0.986089
Reading from 18773: heap size 616 MB, throughput 0.991733
Reading from 18774: heap size 616 MB, throughput 0.989649
Reading from 18774: heap size 618 MB, throughput 0.994025
Numeric result:
Recommendation: 2 clients, utility 1.2173:
    h1: 1651.24 MB (U(h) = 0.282126*h^0.198823)
    h2: 556.756 MB (U(h) = 0.64735*h^0.0670298)
Recommendation: 2 clients, utility 1.2173:
    h1: 1651.29 MB (U(h) = 0.282126*h^0.198823)
    h2: 556.706 MB (U(h) = 0.64735*h^0.0670298)
Reading from 18773: heap size 616 MB, throughput 0.991545
Reading from 18774: heap size 624 MB, throughput 0.994828
Reading from 18773: heap size 619 MB, throughput 0.991571
Reading from 18774: heap size 573 MB, throughput 0.993833
Reading from 18773: heap size 622 MB, throughput 0.993252
Reading from 18773: heap size 624 MB, throughput 0.988799
Client 18773 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 18774: heap size 595 MB, throughput 0.99398
Reading from 18774: heap size 606 MB, throughput 0.993283
Reading from 18774: heap size 606 MB, throughput 0.992082
Reading from 18774: heap size 607 MB, throughput 0.986631
Client 18774 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
TimerThread: finished
ReadingThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
