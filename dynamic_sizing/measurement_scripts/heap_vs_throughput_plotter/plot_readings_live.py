"""Plot readings from stdin as they arrive, to an interactive window"""

import argparse
import matplotlib.pyplot as plt
import plotter
import sys
import time

class LivePlotter(plotter.Plotter):
    def __init__(self, throughput_model, pause_on_func):
        super(LivePlotter, self).__init__(throughput_model, sys.stdin, None, None, True)
        self.pause_on_func = pause_on_func
        self.last_msg_was_func = False
        self.pause_this_iteration = False
        plt.ion()

    def begin(self):
        plt.show()

    def after_new_reading(self):
        if self.scatter:
            self.scatter.remove()
        self.scatter = self.axes.scatter(self.heaps, self.throughputs, zorder=0)

        for func in self.curves:
            result = self.curves[func].draw(self.axes, self.heaps)
            self.update_max_y(result)

        self.axes.relim()
        self.axes.set_xlim(0, self.max_x * 1.1)
        self.axes.set_ylim(0, self.max_y)

        if self.last_msg_was_func:
            self.last_msg_was_func = False
            self.pause_this_iteration = True

    def after_func_update(self, func):
        result = func.draw(self.axes, self.heaps)
        self.update_max_y(result)
        self.axes.legend(loc="lower right", fontsize="medium", frameon=False)
        self.last_msg_was_func = True

    def end_iteration(self):
        plt.draw()
        if self.pause_on_func and self.pause_this_iteration:
            time.sleep(1)
            self.pause_this_iteration = False

    def end(self):
        plt.ioff()
        plt.show()


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot readings from stdin as they arrive, to an interactive window")
    parser.add_argument("throughput_model", help="The name of the throughput model used to generate the readings")
    parser.add_argument("--pause_on_func", action="store_true", help="Pause after reading a function definition (useful when input is coming from a file rather than a live program)")

    args = parser.parse_args()

    LivePlotter(args.throughput_model, args.pause_on_func).plot()
