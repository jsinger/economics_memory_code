economemd
    total memory: 7435 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub4_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run10_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 3876: heap size 9 MB, throughput 0.990798
Clients: 1
Client 3876 has a minimum heap size of 1211 MB
Reading from 3877: heap size 9 MB, throughput 0.992035
Clients: 2
Client 3877 has a minimum heap size of 276 MB
Reading from 3877: heap size 9 MB, throughput 0.987253
Reading from 3876: heap size 9 MB, throughput 0.986423
Reading from 3877: heap size 9 MB, throughput 0.981835
Reading from 3876: heap size 9 MB, throughput 0.980282
Reading from 3877: heap size 9 MB, throughput 0.971732
Reading from 3876: heap size 9 MB, throughput 0.971971
Reading from 3877: heap size 11 MB, throughput 0.970523
Reading from 3876: heap size 11 MB, throughput 0.971832
Reading from 3876: heap size 11 MB, throughput 0.922722
Reading from 3877: heap size 11 MB, throughput 0.973163
Reading from 3876: heap size 17 MB, throughput 0.950394
Reading from 3877: heap size 17 MB, throughput 0.962242
Reading from 3876: heap size 17 MB, throughput 0.889892
Reading from 3877: heap size 17 MB, throughput 0.889584
Reading from 3876: heap size 30 MB, throughput 0.863387
Reading from 3877: heap size 30 MB, throughput 0.819619
Reading from 3877: heap size 31 MB, throughput 0.393347
Reading from 3876: heap size 31 MB, throughput 0.499262
Reading from 3877: heap size 33 MB, throughput 0.739475
Reading from 3876: heap size 34 MB, throughput 0.730287
Reading from 3877: heap size 46 MB, throughput 0.542509
Reading from 3876: heap size 46 MB, throughput 0.492062
Reading from 3877: heap size 48 MB, throughput 0.503072
Reading from 3876: heap size 49 MB, throughput 0.426478
Reading from 3876: heap size 51 MB, throughput 0.463765
Reading from 3877: heap size 50 MB, throughput 0.467449
Reading from 3877: heap size 74 MB, throughput 0.391416
Reading from 3876: heap size 74 MB, throughput 0.342341
Reading from 3876: heap size 74 MB, throughput 0.322268
Reading from 3877: heap size 74 MB, throughput 0.309478
Reading from 3877: heap size 101 MB, throughput 0.609855
Reading from 3876: heap size 101 MB, throughput 0.533677
Reading from 3877: heap size 101 MB, throughput 0.431472
Reading from 3876: heap size 102 MB, throughput 0.463969
Reading from 3877: heap size 103 MB, throughput 0.275546
Reading from 3876: heap size 103 MB, throughput 0.267992
Reading from 3877: heap size 106 MB, throughput 0.211721
Reading from 3876: heap size 107 MB, throughput 0.202947
Reading from 3877: heap size 109 MB, throughput 0.443952
Reading from 3876: heap size 109 MB, throughput 0.414957
Reading from 3877: heap size 144 MB, throughput 0.335217
Reading from 3876: heap size 144 MB, throughput 0.219165
Reading from 3877: heap size 148 MB, throughput 0.254425
Reading from 3876: heap size 148 MB, throughput 0.0808349
Reading from 3877: heap size 150 MB, throughput 0.76301
Reading from 3876: heap size 151 MB, throughput 0.772591
Reading from 3877: heap size 157 MB, throughput 0.774933
Reading from 3876: heap size 159 MB, throughput 0.775731
Reading from 3877: heap size 159 MB, throughput 0.75405
Reading from 3876: heap size 161 MB, throughput 0.43649
Reading from 3877: heap size 162 MB, throughput 0.47433
Reading from 3876: heap size 195 MB, throughput 0.756402
Reading from 3876: heap size 202 MB, throughput 0.771988
Reading from 3877: heap size 204 MB, throughput 0.681024
Reading from 3876: heap size 203 MB, throughput 0.765449
Reading from 3876: heap size 206 MB, throughput 0.677967
Reading from 3877: heap size 208 MB, throughput 0.756359
Reading from 3876: heap size 209 MB, throughput 0.689373
Reading from 3876: heap size 216 MB, throughput 0.632928
Reading from 3876: heap size 221 MB, throughput 0.5911
Reading from 3876: heap size 230 MB, throughput 0.610979
Reading from 3877: heap size 212 MB, throughput 0.887941
Reading from 3876: heap size 236 MB, throughput 0.53503
Reading from 3877: heap size 213 MB, throughput 0.822746
Reading from 3877: heap size 223 MB, throughput 0.783532
Reading from 3877: heap size 228 MB, throughput 0.716285
Reading from 3877: heap size 231 MB, throughput 0.798463
Reading from 3877: heap size 235 MB, throughput 0.702996
Reading from 3876: heap size 244 MB, throughput 0.415253
Reading from 3877: heap size 237 MB, throughput 0.679735
Reading from 3876: heap size 284 MB, throughput 0.429139
Reading from 3877: heap size 243 MB, throughput 0.645766
Reading from 3876: heap size 289 MB, throughput 0.141684
Reading from 3876: heap size 326 MB, throughput 0.420615
Reading from 3876: heap size 270 MB, throughput 0.494103
Reading from 3876: heap size 319 MB, throughput 0.557955
Reading from 3877: heap size 243 MB, throughput 0.588312
Reading from 3876: heap size 323 MB, throughput 0.582746
Reading from 3877: heap size 277 MB, throughput 0.70363
Reading from 3876: heap size 323 MB, throughput 0.560624
Reading from 3877: heap size 281 MB, throughput 0.711921
Reading from 3876: heap size 326 MB, throughput 0.592995
Reading from 3877: heap size 280 MB, throughput 0.726433
Reading from 3876: heap size 328 MB, throughput 0.556034
Reading from 3877: heap size 282 MB, throughput 0.722661
Reading from 3877: heap size 282 MB, throughput 0.674162
Reading from 3876: heap size 334 MB, throughput 0.532981
Reading from 3876: heap size 340 MB, throughput 0.535907
Reading from 3877: heap size 284 MB, throughput 0.637667
Reading from 3876: heap size 345 MB, throughput 0.541098
Reading from 3876: heap size 350 MB, throughput 0.376871
Reading from 3876: heap size 396 MB, throughput 0.521141
Reading from 3877: heap size 285 MB, throughput 0.579453
Reading from 3876: heap size 397 MB, throughput 0.553285
Reading from 3877: heap size 331 MB, throughput 0.841389
Reading from 3877: heap size 336 MB, throughput 0.829374
Reading from 3876: heap size 400 MB, throughput 0.567463
Reading from 3877: heap size 337 MB, throughput 0.800758
Reading from 3876: heap size 402 MB, throughput 0.570346
Reading from 3877: heap size 344 MB, throughput 0.791325
Reading from 3876: heap size 404 MB, throughput 0.562054
Reading from 3876: heap size 407 MB, throughput 0.504315
Reading from 3877: heap size 344 MB, throughput 0.848089
Reading from 3876: heap size 411 MB, throughput 0.491357
Reading from 3877: heap size 346 MB, throughput 0.8788
Reading from 3877: heap size 348 MB, throughput 0.819
Reading from 3877: heap size 350 MB, throughput 0.774944
Reading from 3877: heap size 351 MB, throughput 0.694016
Reading from 3877: heap size 358 MB, throughput 0.680078
Reading from 3877: heap size 358 MB, throughput 0.634931
Numeric result:
Recommendation: 2 clients, utility 0.603679:
    h1: 1211 MB (U(h) = 0.826168*h^0.001)
    h2: 6224 MB (U(h) = 0.53134*h^0.0356561)
Recommendation: 2 clients, utility 0.603679:
    h1: 1211 MB (U(h) = 0.826168*h^0.001)
    h2: 6224 MB (U(h) = 0.53134*h^0.0356561)
Reading from 3876: heap size 417 MB, throughput 0.324655
Reading from 3876: heap size 469 MB, throughput 0.416099
Reading from 3877: heap size 366 MB, throughput 0.866838
Reading from 3876: heap size 464 MB, throughput 0.506428
Reading from 3876: heap size 469 MB, throughput 0.492389
Reading from 3876: heap size 467 MB, throughput 0.510328
Reading from 3876: heap size 468 MB, throughput 0.266124
Reading from 3876: heap size 527 MB, throughput 0.490903
Reading from 3877: heap size 367 MB, throughput 0.945326
Reading from 3876: heap size 527 MB, throughput 0.537766
Reading from 3876: heap size 528 MB, throughput 0.560872
Reading from 3876: heap size 528 MB, throughput 0.560447
Reading from 3876: heap size 532 MB, throughput 0.598694
Reading from 3877: heap size 369 MB, throughput 0.948699
Reading from 3876: heap size 535 MB, throughput 0.303445
Reading from 3876: heap size 599 MB, throughput 0.575967
Reading from 3876: heap size 606 MB, throughput 0.56207
Reading from 3876: heap size 607 MB, throughput 0.599556
Reading from 3876: heap size 614 MB, throughput 0.596276
Reading from 3876: heap size 617 MB, throughput 0.583753
Reading from 3877: heap size 372 MB, throughput 0.954167
Reading from 3876: heap size 629 MB, throughput 0.757687
Reading from 3876: heap size 634 MB, throughput 0.773526
Reading from 3876: heap size 646 MB, throughput 0.781591
Reading from 3877: heap size 372 MB, throughput 0.964985
Reading from 3876: heap size 648 MB, throughput 0.824491
Reading from 3877: heap size 374 MB, throughput 0.964121
Reading from 3876: heap size 663 MB, throughput 0.478163
Reading from 3876: heap size 736 MB, throughput 0.567624
Reading from 3876: heap size 744 MB, throughput 0.474599
Reading from 3876: heap size 736 MB, throughput 0.518304
Reading from 3876: heap size 660 MB, throughput 0.420145
Reading from 3877: heap size 373 MB, throughput 0.962828
Numeric result:
Recommendation: 2 clients, utility 0.569234:
    h1: 1211 MB (U(h) = 0.646733*h^0.001)
    h2: 6224 MB (U(h) = 0.45452*h^0.0748349)
Recommendation: 2 clients, utility 0.569234:
    h1: 1211 MB (U(h) = 0.646733*h^0.001)
    h2: 6224 MB (U(h) = 0.45452*h^0.0748349)
Reading from 3876: heap size 724 MB, throughput 0.155769
Reading from 3876: heap size 811 MB, throughput 0.275938
Reading from 3876: heap size 811 MB, throughput 0.335957
Reading from 3876: heap size 812 MB, throughput 0.360157
Reading from 3877: heap size 375 MB, throughput 0.96395
Reading from 3876: heap size 816 MB, throughput 0.108611
Reading from 3876: heap size 913 MB, throughput 0.560073
Reading from 3876: heap size 915 MB, throughput 0.788968
Reading from 3876: heap size 923 MB, throughput 0.784528
Reading from 3876: heap size 925 MB, throughput 0.784477
Reading from 3876: heap size 927 MB, throughput 0.783302
Reading from 3876: heap size 919 MB, throughput 0.800759
Reading from 3876: heap size 708 MB, throughput 0.77565
Reading from 3876: heap size 908 MB, throughput 0.902515
Reading from 3876: heap size 710 MB, throughput 0.894052
Reading from 3876: heap size 894 MB, throughput 0.856049
Reading from 3877: heap size 378 MB, throughput 0.96399
Reading from 3876: heap size 711 MB, throughput 0.87637
Reading from 3876: heap size 879 MB, throughput 0.852114
Reading from 3876: heap size 719 MB, throughput 0.906527
Reading from 3876: heap size 862 MB, throughput 0.874125
Reading from 3876: heap size 727 MB, throughput 0.894648
Reading from 3876: heap size 849 MB, throughput 0.889538
Reading from 3876: heap size 766 MB, throughput 0.831878
Reading from 3876: heap size 848 MB, throughput 0.818459
Reading from 3877: heap size 379 MB, throughput 0.967485
Reading from 3876: heap size 766 MB, throughput 0.809845
Reading from 3876: heap size 842 MB, throughput 0.780621
Reading from 3876: heap size 770 MB, throughput 0.78673
Reading from 3876: heap size 837 MB, throughput 0.794378
Reading from 3876: heap size 842 MB, throughput 0.78969
Reading from 3876: heap size 831 MB, throughput 0.802618
Reading from 3876: heap size 837 MB, throughput 0.807785
Reading from 3876: heap size 825 MB, throughput 0.818469
Reading from 3877: heap size 382 MB, throughput 0.970183
Reading from 3876: heap size 776 MB, throughput 0.819254
Reading from 3876: heap size 819 MB, throughput 0.843915
Reading from 3876: heap size 825 MB, throughput 0.842921
Reading from 3876: heap size 819 MB, throughput 0.47818
Reading from 3877: heap size 383 MB, throughput 0.972205
Reading from 3876: heap size 822 MB, throughput 0.973881
Reading from 3876: heap size 820 MB, throughput 0.89564
Reading from 3876: heap size 825 MB, throughput 0.916798
Reading from 3876: heap size 829 MB, throughput 0.854616
Reading from 3876: heap size 835 MB, throughput 0.827836
Reading from 3876: heap size 841 MB, throughput 0.813633
Reading from 3877: heap size 386 MB, throughput 0.965529
Reading from 3876: heap size 844 MB, throughput 0.76903
Reading from 3876: heap size 851 MB, throughput 0.757194
Reading from 3876: heap size 852 MB, throughput 0.750577
Reading from 3876: heap size 860 MB, throughput 0.754916
Numeric result:
Recommendation: 2 clients, utility 0.991328:
    h1: 5398.24 MB (U(h) = 0.105308*h^0.274302)
    h2: 2036.76 MB (U(h) = 0.405059*h^0.1035)
Recommendation: 2 clients, utility 0.991328:
    h1: 5398.16 MB (U(h) = 0.105308*h^0.274302)
    h2: 2036.84 MB (U(h) = 0.405059*h^0.1035)
Reading from 3876: heap size 860 MB, throughput 0.751734
Reading from 3876: heap size 866 MB, throughput 0.757981
Reading from 3876: heap size 866 MB, throughput 0.837477
Reading from 3876: heap size 866 MB, throughput 0.847934
Reading from 3877: heap size 388 MB, throughput 0.971156
Reading from 3876: heap size 870 MB, throughput 0.86333
Reading from 3877: heap size 391 MB, throughput 0.979085
Reading from 3877: heap size 393 MB, throughput 0.958855
Reading from 3877: heap size 389 MB, throughput 0.933358
Reading from 3877: heap size 394 MB, throughput 0.89292
Reading from 3876: heap size 869 MB, throughput 0.704322
Reading from 3877: heap size 402 MB, throughput 0.872255
Reading from 3876: heap size 964 MB, throughput 0.684124
Reading from 3876: heap size 975 MB, throughput 0.684691
Reading from 3876: heap size 976 MB, throughput 0.651821
Reading from 3876: heap size 986 MB, throughput 0.659829
Reading from 3876: heap size 992 MB, throughput 0.649695
Reading from 3876: heap size 1004 MB, throughput 0.639003
Reading from 3876: heap size 1009 MB, throughput 0.64645
Reading from 3877: heap size 405 MB, throughput 0.96582
Reading from 3876: heap size 1022 MB, throughput 0.636926
Reading from 3876: heap size 1026 MB, throughput 0.625109
Reading from 3876: heap size 1040 MB, throughput 0.631346
Reading from 3876: heap size 1044 MB, throughput 0.651014
Reading from 3876: heap size 1058 MB, throughput 0.639519
Reading from 3876: heap size 1061 MB, throughput 0.630453
Reading from 3876: heap size 1079 MB, throughput 0.704358
Reading from 3877: heap size 410 MB, throughput 0.976113
Reading from 3876: heap size 1080 MB, throughput 0.933679
Reading from 3877: heap size 412 MB, throughput 0.975469
Reading from 3876: heap size 1097 MB, throughput 0.957205
Numeric result:
Recommendation: 2 clients, utility 1.39904:
    h1: 5622.51 MB (U(h) = 0.0449723*h^0.402448)
    h2: 1812.49 MB (U(h) = 0.363914*h^0.129742)
Recommendation: 2 clients, utility 1.39904:
    h1: 5622.44 MB (U(h) = 0.0449723*h^0.402448)
    h2: 1812.56 MB (U(h) = 0.363914*h^0.129742)
Reading from 3877: heap size 412 MB, throughput 0.980988
Reading from 3876: heap size 1099 MB, throughput 0.952523
Reading from 3877: heap size 415 MB, throughput 0.983127
Reading from 3876: heap size 1103 MB, throughput 0.952163
Reading from 3877: heap size 414 MB, throughput 0.982857
Reading from 3876: heap size 1107 MB, throughput 0.960078
Reading from 3877: heap size 416 MB, throughput 0.982963
Reading from 3876: heap size 1099 MB, throughput 0.959352
Reading from 3877: heap size 412 MB, throughput 0.982853
Reading from 3876: heap size 1106 MB, throughput 0.958006
Reading from 3877: heap size 415 MB, throughput 0.975529
Reading from 3876: heap size 1094 MB, throughput 0.950665
Numeric result:
Recommendation: 2 clients, utility 1.52775:
    h1: 5612.71 MB (U(h) = 0.0396661*h^0.424089)
    h2: 1822.29 MB (U(h) = 0.352102*h^0.137698)
Recommendation: 2 clients, utility 1.52775:
    h1: 5612.63 MB (U(h) = 0.0396661*h^0.424089)
    h2: 1822.37 MB (U(h) = 0.352102*h^0.137698)
Reading from 3877: heap size 415 MB, throughput 0.977945
Reading from 3876: heap size 1101 MB, throughput 0.951962
Reading from 3876: heap size 1087 MB, throughput 0.952281
Reading from 3877: heap size 416 MB, throughput 0.978093
Reading from 3877: heap size 414 MB, throughput 0.981576
Reading from 3877: heap size 418 MB, throughput 0.96996
Reading from 3877: heap size 420 MB, throughput 0.925604
Reading from 3876: heap size 1094 MB, throughput 0.952662
Reading from 3877: heap size 421 MB, throughput 0.899865
Reading from 3877: heap size 430 MB, throughput 0.964384
Reading from 3876: heap size 1089 MB, throughput 0.950787
Reading from 3876: heap size 1092 MB, throughput 0.952694
Reading from 3877: heap size 430 MB, throughput 0.980959
Reading from 3876: heap size 1094 MB, throughput 0.950745
Numeric result:
Recommendation: 2 clients, utility 1.83984:
    h1: 5773.35 MB (U(h) = 0.0236232*h^0.502846)
    h2: 1661.65 MB (U(h) = 0.341918*h^0.144728)
Recommendation: 2 clients, utility 1.83984:
    h1: 5773.33 MB (U(h) = 0.0236232*h^0.502846)
    h2: 1661.67 MB (U(h) = 0.341918*h^0.144728)
Reading from 3877: heap size 436 MB, throughput 0.986849
Reading from 3876: heap size 1095 MB, throughput 0.952889
Reading from 3877: heap size 437 MB, throughput 0.987707
Reading from 3876: heap size 1099 MB, throughput 0.955251
Reading from 3877: heap size 437 MB, throughput 0.987641
Reading from 3876: heap size 1101 MB, throughput 0.951188
Reading from 3877: heap size 440 MB, throughput 0.986828
Reading from 3876: heap size 1108 MB, throughput 0.954885
Reading from 3877: heap size 437 MB, throughput 0.987067
Reading from 3876: heap size 1109 MB, throughput 0.951069
Numeric result:
Recommendation: 2 clients, utility 2.16897:
    h1: 5888.62 MB (U(h) = 0.0146357*h^0.574783)
    h2: 1546.38 MB (U(h) = 0.33305*h^0.150939)
Recommendation: 2 clients, utility 2.16897:
    h1: 5888.64 MB (U(h) = 0.0146357*h^0.574783)
    h2: 1546.36 MB (U(h) = 0.33305*h^0.150939)
Reading from 3877: heap size 440 MB, throughput 0.986303
Reading from 3876: heap size 1115 MB, throughput 0.949634
Reading from 3877: heap size 440 MB, throughput 0.98485
Reading from 3876: heap size 1116 MB, throughput 0.952741
Reading from 3877: heap size 441 MB, throughput 0.986946
Reading from 3877: heap size 444 MB, throughput 0.979337
Reading from 3877: heap size 444 MB, throughput 0.964557
Reading from 3876: heap size 1121 MB, throughput 0.954463
Reading from 3877: heap size 450 MB, throughput 0.976766
Reading from 3876: heap size 1122 MB, throughput 0.95333
Reading from 3877: heap size 451 MB, throughput 0.985941
Numeric result:
Recommendation: 2 clients, utility 2.52492:
    h1: 5963.43 MB (U(h) = 0.00949875*h^0.639612)
    h2: 1471.57 MB (U(h) = 0.323404*h^0.157834)
Recommendation: 2 clients, utility 2.52492:
    h1: 5963.43 MB (U(h) = 0.00949875*h^0.639612)
    h2: 1471.57 MB (U(h) = 0.323404*h^0.157834)
Reading from 3877: heap size 454 MB, throughput 0.980433
Reading from 3876: heap size 1128 MB, throughput 0.895068
Reading from 3877: heap size 455 MB, throughput 0.984687
Reading from 3876: heap size 1221 MB, throughput 0.949212
Reading from 3877: heap size 454 MB, throughput 0.987806
Reading from 3876: heap size 1224 MB, throughput 0.958139
Reading from 3877: heap size 456 MB, throughput 0.987589
Reading from 3876: heap size 1227 MB, throughput 0.962113
Reading from 3877: heap size 455 MB, throughput 0.986911
Numeric result:
Recommendation: 2 clients, utility 3.15183:
    h1: 6107.79 MB (U(h) = 0.0045521*h^0.747986)
    h2: 1327.21 MB (U(h) = 0.316949*h^0.16253)
Recommendation: 2 clients, utility 3.15183:
    h1: 6107.83 MB (U(h) = 0.0045521*h^0.747986)
    h2: 1327.17 MB (U(h) = 0.316949*h^0.16253)
Reading from 3876: heap size 1232 MB, throughput 0.969271
Reading from 3877: heap size 456 MB, throughput 0.986199
Reading from 3876: heap size 1233 MB, throughput 0.967774
Reading from 3877: heap size 458 MB, throughput 0.988567
Reading from 3877: heap size 459 MB, throughput 0.981611
Reading from 3877: heap size 458 MB, throughput 0.97084
Reading from 3876: heap size 1229 MB, throughput 0.967719
Reading from 3877: heap size 461 MB, throughput 0.974522
Reading from 3876: heap size 1232 MB, throughput 0.962614
Reading from 3877: heap size 467 MB, throughput 0.986898
Reading from 3876: heap size 1225 MB, throughput 0.965123
Numeric result:
Recommendation: 2 clients, utility 3.29554:
    h1: 6094.09 MB (U(h) = 0.00412672*h^0.762613)
    h2: 1340.91 MB (U(h) = 0.309811*h^0.167808)
Recommendation: 2 clients, utility 3.29554:
    h1: 6094.05 MB (U(h) = 0.00412672*h^0.762613)
    h2: 1340.95 MB (U(h) = 0.309811*h^0.167808)
Reading from 3877: heap size 468 MB, throughput 0.98927
Reading from 3876: heap size 1229 MB, throughput 0.962992
Reading from 3877: heap size 468 MB, throughput 0.989881
Reading from 3876: heap size 1230 MB, throughput 0.960763
Reading from 3877: heap size 470 MB, throughput 0.988743
Reading from 3876: heap size 1231 MB, throughput 0.960904
Reading from 3877: heap size 468 MB, throughput 0.988074
Reading from 3876: heap size 1236 MB, throughput 0.959178
Reading from 3877: heap size 469 MB, throughput 0.987685
Numeric result:
Recommendation: 2 clients, utility 3.07936:
    h1: 6003.45 MB (U(h) = 0.00556105*h^0.71933)
    h2: 1431.55 MB (U(h) = 0.304849*h^0.171526)
Recommendation: 2 clients, utility 3.07936:
    h1: 6003.46 MB (U(h) = 0.00556105*h^0.71933)
    h2: 1431.54 MB (U(h) = 0.304849*h^0.171526)
Reading from 3876: heap size 1238 MB, throughput 0.95888
Reading from 3877: heap size 471 MB, throughput 0.986461
Reading from 3877: heap size 471 MB, throughput 0.987535
Reading from 3877: heap size 473 MB, throughput 0.98021
Reading from 3876: heap size 1243 MB, throughput 0.95682
Reading from 3877: heap size 474 MB, throughput 0.963881
Reading from 3877: heap size 478 MB, throughput 0.983112
Reading from 3876: heap size 1248 MB, throughput 0.955131
Reading from 3877: heap size 480 MB, throughput 0.988194
Reading from 3876: heap size 1254 MB, throughput 0.955887
Numeric result:
Recommendation: 2 clients, utility 3.0059:
    h1: 5932.1 MB (U(h) = 0.00650045*h^0.696908)
    h2: 1502.9 MB (U(h) = 0.298225*h^0.176556)
Recommendation: 2 clients, utility 3.0059:
    h1: 5932.14 MB (U(h) = 0.00650045*h^0.696908)
    h2: 1502.86 MB (U(h) = 0.298225*h^0.176556)
Reading from 3877: heap size 483 MB, throughput 0.989211
Reading from 3876: heap size 1261 MB, throughput 0.955486
Reading from 3877: heap size 485 MB, throughput 0.989701
Reading from 3876: heap size 1269 MB, throughput 0.957538
Reading from 3877: heap size 484 MB, throughput 0.989384
Reading from 3876: heap size 1273 MB, throughput 0.956162
Reading from 3876: heap size 1281 MB, throughput 0.948629
Reading from 3877: heap size 486 MB, throughput 0.972485
Numeric result:
Recommendation: 2 clients, utility 3.1259:
    h1: 5935.19 MB (U(h) = 0.00581579*h^0.713009)
    h2: 1499.81 MB (U(h) = 0.293515*h^0.180175)
Recommendation: 2 clients, utility 3.1259:
    h1: 5935.2 MB (U(h) = 0.00581579*h^0.713009)
    h2: 1499.8 MB (U(h) = 0.293515*h^0.180175)
Reading from 3877: heap size 515 MB, throughput 0.980266
Reading from 3876: heap size 1284 MB, throughput 0.951104
Reading from 3877: heap size 515 MB, throughput 0.981707
Reading from 3877: heap size 520 MB, throughput 0.975675
Reading from 3877: heap size 521 MB, throughput 0.976154
Reading from 3876: heap size 1292 MB, throughput 0.921976
Reading from 3877: heap size 528 MB, throughput 0.989473
Numeric result:
Recommendation: 2 clients, utility 4.65395:
    h1: 5135 MB (U(h) = 0.00376845*h^0.775247)
    h2: 2300 MB (U(h) = 0.111601*h^0.347264)
Recommendation: 2 clients, utility 4.65395:
    h1: 5134.88 MB (U(h) = 0.00376845*h^0.775247)
    h2: 2300.12 MB (U(h) = 0.111601*h^0.347264)
Reading from 3877: heap size 529 MB, throughput 0.99038
Reading from 3877: heap size 529 MB, throughput 0.990844
Reading from 3877: heap size 531 MB, throughput 0.990695
Reading from 3877: heap size 531 MB, throughput 0.98988
Numeric result:
Recommendation: 2 clients, utility 5.22684:
    h1: 4872.46 MB (U(h) = 0.00376845*h^0.775247)
    h2: 2562.54 MB (U(h) = 0.0782567*h^0.407678)
Recommendation: 2 clients, utility 5.22684:
    h1: 4872.63 MB (U(h) = 0.00376845*h^0.775247)
    h2: 2562.37 MB (U(h) = 0.0782567*h^0.407678)
Reading from 3877: heap size 532 MB, throughput 0.987651
Reading from 3877: heap size 534 MB, throughput 0.987974
Reading from 3877: heap size 536 MB, throughput 0.977924
Reading from 3876: heap size 1402 MB, throughput 0.979938
Reading from 3877: heap size 539 MB, throughput 0.978153
Reading from 3877: heap size 542 MB, throughput 0.987192
Numeric result:
Recommendation: 2 clients, utility 8.02194:
    h1: 4269.69 MB (U(h) = 0.0030431*h^0.805771)
    h2: 3165.31 MB (U(h) = 0.0253947*h^0.597344)
Recommendation: 2 clients, utility 8.02194:
    h1: 4269.72 MB (U(h) = 0.0030431*h^0.805771)
    h2: 3165.28 MB (U(h) = 0.0253947*h^0.597344)
Reading from 3877: heap size 544 MB, throughput 0.990266
Reading from 3877: heap size 546 MB, throughput 0.989631
Reading from 3877: heap size 544 MB, throughput 0.990095
Reading from 3876: heap size 1393 MB, throughput 0.9876
Reading from 3877: heap size 547 MB, throughput 0.989481
Numeric result:
Recommendation: 2 clients, utility 7.10677:
    h1: 4635.61 MB (U(h) = 0.00230156*h^0.845681)
    h2: 2799.39 MB (U(h) = 0.0425488*h^0.510697)
Recommendation: 2 clients, utility 7.10677:
    h1: 4635.61 MB (U(h) = 0.00230156*h^0.845681)
    h2: 2799.39 MB (U(h) = 0.0425488*h^0.510697)
Reading from 3877: heap size 550 MB, throughput 0.989763
Reading from 3877: heap size 550 MB, throughput 0.985443
Reading from 3877: heap size 551 MB, throughput 0.977492
Reading from 3876: heap size 1411 MB, throughput 0.986626
Reading from 3876: heap size 1421 MB, throughput 0.977152
Reading from 3876: heap size 1429 MB, throughput 0.955578
Reading from 3877: heap size 554 MB, throughput 0.987242
Reading from 3876: heap size 1424 MB, throughput 0.928602
Reading from 3876: heap size 1456 MB, throughput 0.885344
Reading from 3876: heap size 1482 MB, throughput 0.838273
Reading from 3876: heap size 1494 MB, throughput 0.791744
Reading from 3876: heap size 1524 MB, throughput 0.763895
Reading from 3876: heap size 1530 MB, throughput 0.837518
Reading from 3877: heap size 560 MB, throughput 0.989695
Numeric result:
Recommendation: 2 clients, utility 2.61696:
    h1: 2969.66 MB (U(h) = 0.139167*h^0.260593)
    h2: 4465.34 MB (U(h) = 0.0869025*h^0.391869)
Recommendation: 2 clients, utility 2.61696:
    h1: 2969.54 MB (U(h) = 0.139167*h^0.260593)
    h2: 4465.46 MB (U(h) = 0.0869025*h^0.391869)
Reading from 3876: heap size 1546 MB, throughput 0.942985
Reading from 3877: heap size 561 MB, throughput 0.990595
Reading from 3876: heap size 1555 MB, throughput 0.959537
Reading from 3877: heap size 561 MB, throughput 0.990967
Reading from 3876: heap size 1550 MB, throughput 0.963494
Reading from 3877: heap size 564 MB, throughput 0.990309
Reading from 3876: heap size 1562 MB, throughput 0.965143
Numeric result:
Recommendation: 2 clients, utility 2.88479:
    h1: 2784.21 MB (U(h) = 0.14082*h^0.259259)
    h2: 4650.79 MB (U(h) = 0.0675314*h^0.433227)
Recommendation: 2 clients, utility 2.88479:
    h1: 2783.58 MB (U(h) = 0.14082*h^0.259259)
    h2: 4651.42 MB (U(h) = 0.0675314*h^0.433227)
Reading from 3877: heap size 565 MB, throughput 0.991938
Reading from 3877: heap size 566 MB, throughput 0.980244
Reading from 3877: heap size 566 MB, throughput 0.959989
Reading from 3876: heap size 1552 MB, throughput 0.954102
Reading from 3877: heap size 568 MB, throughput 0.982529
Reading from 3876: heap size 1527 MB, throughput 0.979099
Reading from 3877: heap size 578 MB, throughput 0.988761
Reading from 3876: heap size 1528 MB, throughput 0.983257
Numeric result:
Recommendation: 2 clients, utility 2.80661:
    h1: 2902.48 MB (U(h) = 0.131892*h^0.268669)
    h2: 4532.52 MB (U(h) = 0.0730373*h^0.419568)
Recommendation: 2 clients, utility 2.80661:
    h1: 2902.42 MB (U(h) = 0.131892*h^0.268669)
    h2: 4532.58 MB (U(h) = 0.0730373*h^0.419568)
Reading from 3877: heap size 580 MB, throughput 0.990224
Reading from 3876: heap size 1543 MB, throughput 0.985771
Reading from 3876: heap size 1563 MB, throughput 0.983674
Reading from 3877: heap size 581 MB, throughput 0.990943
Reading from 3876: heap size 1565 MB, throughput 0.981222
Reading from 3877: heap size 584 MB, throughput 0.990473
Numeric result:
Recommendation: 2 clients, utility 3.06957:
    h1: 3020.8 MB (U(h) = 0.102131*h^0.304845)
    h2: 4414.2 MB (U(h) = 0.0621565*h^0.445433)
Recommendation: 2 clients, utility 3.06957:
    h1: 3020.91 MB (U(h) = 0.102131*h^0.304845)
    h2: 4414.09 MB (U(h) = 0.0621565*h^0.445433)
Reading from 3876: heap size 1559 MB, throughput 0.979285
Reading from 3877: heap size 587 MB, throughput 0.991918
Reading from 3877: heap size 588 MB, throughput 0.986632
Reading from 3877: heap size 590 MB, throughput 0.987115
Reading from 3876: heap size 1566 MB, throughput 0.978026
Reading from 3877: heap size 592 MB, throughput 0.991225
Numeric result:
Recommendation: 2 clients, utility 2.91391:
    h1: 3282.26 MB (U(h) = 0.0870186*h^0.327371)
    h2: 4152.74 MB (U(h) = 0.0750464*h^0.414124)
Recommendation: 2 clients, utility 2.91391:
    h1: 3282.56 MB (U(h) = 0.0870186*h^0.327371)
    h2: 4152.44 MB (U(h) = 0.0750464*h^0.414124)
Reading from 3876: heap size 1544 MB, throughput 0.97461
Reading from 3877: heap size 595 MB, throughput 0.991844
Reading from 3876: heap size 1401 MB, throughput 0.972778
Reading from 3877: heap size 597 MB, throughput 0.991606
Reading from 3876: heap size 1528 MB, throughput 0.971113
Reading from 3877: heap size 596 MB, throughput 0.99126
Numeric result:
Recommendation: 2 clients, utility 2.69562:
    h1: 3647.29 MB (U(h) = 0.0727173*h^0.352585)
    h2: 3787.71 MB (U(h) = 0.100664*h^0.366157)
Recommendation: 2 clients, utility 2.69562:
    h1: 3647.3 MB (U(h) = 0.0727173*h^0.352585)
    h2: 3787.7 MB (U(h) = 0.100664*h^0.366157)
Reading from 3876: heap size 1541 MB, throughput 0.969206
Reading from 3877: heap size 598 MB, throughput 0.992849
Reading from 3877: heap size 601 MB, throughput 0.988869
Reading from 3877: heap size 602 MB, throughput 0.988002
Reading from 3876: heap size 1539 MB, throughput 0.966469
Reading from 3877: heap size 609 MB, throughput 0.988692
Reading from 3876: heap size 1542 MB, throughput 0.966094
Numeric result:
Recommendation: 2 clients, utility 2.14462:
    h1: 4540.62 MB (U(h) = 0.0632527*h^0.371999)
    h2: 2894.38 MB (U(h) = 0.223272*h^0.237175)
Recommendation: 2 clients, utility 2.14462:
    h1: 4540.27 MB (U(h) = 0.0632527*h^0.371999)
    h2: 2894.73 MB (U(h) = 0.223272*h^0.237175)
Reading from 3876: heap size 1551 MB, throughput 0.967003
Reading from 3877: heap size 610 MB, throughput 0.991357
Reading from 3876: heap size 1555 MB, throughput 0.966084
Reading from 3877: heap size 608 MB, throughput 0.991697
Reading from 3876: heap size 1566 MB, throughput 0.961921
Numeric result:
Recommendation: 2 clients, utility 2.06499:
    h1: 4748.8 MB (U(h) = 0.0615165*h^0.375858)
    h2: 2686.2 MB (U(h) = 0.259962*h^0.212632)
Recommendation: 2 clients, utility 2.06499:
    h1: 4748.61 MB (U(h) = 0.0615165*h^0.375858)
    h2: 2686.39 MB (U(h) = 0.259962*h^0.212632)
Reading from 3877: heap size 611 MB, throughput 0.991129
Reading from 3877: heap size 614 MB, throughput 0.99147
Reading from 3877: heap size 615 MB, throughput 0.98651
Reading from 3876: heap size 1575 MB, throughput 0.95969
Reading from 3877: heap size 616 MB, throughput 0.98867
Reading from 3876: heap size 1591 MB, throughput 0.960246
Reading from 3877: heap size 618 MB, throughput 0.992198
Numeric result:
Recommendation: 2 clients, utility 1.81658:
    h1: 5575.32 MB (U(h) = 0.060751*h^0.377488)
    h2: 1859.68 MB (U(h) = 0.446653*h^0.125881)
Recommendation: 2 clients, utility 1.81658:
    h1: 5575.68 MB (U(h) = 0.060751*h^0.377488)
    h2: 1859.32 MB (U(h) = 0.446653*h^0.125881)
Reading from 3876: heap size 1597 MB, throughput 0.958638
Reading from 3877: heap size 620 MB, throughput 0.992314
Reading from 3876: heap size 1613 MB, throughput 0.961239
Reading from 3877: heap size 622 MB, throughput 0.992203
Reading from 3876: heap size 1617 MB, throughput 0.958885
Numeric result:
Recommendation: 2 clients, utility 1.70086:
    h1: 6189.4 MB (U(h) = 0.0635175*h^0.371117)
    h2: 1245.6 MB (U(h) = 0.615842*h^0.0746789)
Recommendation: 2 clients, utility 1.70086:
    h1: 6189.5 MB (U(h) = 0.0635175*h^0.371117)
    h2: 1245.5 MB (U(h) = 0.615842*h^0.0746789)
Reading from 3877: heap size 624 MB, throughput 0.991114
Reading from 3877: heap size 624 MB, throughput 0.98979
Reading from 3877: heap size 627 MB, throughput 0.986517
Client 3877 died
Clients: 1
Reading from 3876: heap size 1633 MB, throughput 0.964825
Reading from 3876: heap size 1635 MB, throughput 0.966698
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 3876: heap size 1652 MB, throughput 0.991872
Recommendation: one client; give it all the memory
Reading from 3876: heap size 1653 MB, throughput 0.990452
Reading from 3876: heap size 1677 MB, throughput 0.983579
Reading from 3876: heap size 1679 MB, throughput 0.968001
Reading from 3876: heap size 1706 MB, throughput 0.948643
Reading from 3876: heap size 1724 MB, throughput 0.919402
Client 3876 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
