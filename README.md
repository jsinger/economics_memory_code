Scripts for Economic Heap Sizing
================================

Dynamic Sizing
--------------

See the readme in `dynamic_sizing` for details.

Static Sizing
-------------

See the readme in `static_sizing` for details.
