#include <unistd.h>
#include "CommandSender.hpp"
#include "Utilities.hpp"


economem::CommandSender::CommandSender(const char* daemonName) : SendSock(), m_DaemonName(daemonName) {}

bool economem::CommandSender::sendCommand(economem::CommandType t, unsigned long long i, unsigned long long j)
{
    economem::Command c(t, getpid(), i, j);
    return sendToDaemon(economem::TYPE_COMMAND, m_DaemonName, &c, sizeof(c));
}
