economemd
    total memory: 1728 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub60_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 19447: heap size 9 MB, throughput 0.990098
Clients: 1
Client 19447 has a minimum heap size of 12 MB
Reading from 19446: heap size 9 MB, throughput 0.986385
Clients: 2
Client 19446 has a minimum heap size of 276 MB
Reading from 19447: heap size 9 MB, throughput 0.982944
Reading from 19446: heap size 9 MB, throughput 0.976922
Reading from 19447: heap size 9 MB, throughput 0.966815
Reading from 19446: heap size 11 MB, throughput 0.978836
Reading from 19447: heap size 9 MB, throughput 0.979297
Reading from 19447: heap size 11 MB, throughput 0.975806
Reading from 19446: heap size 11 MB, throughput 0.948644
Reading from 19447: heap size 11 MB, throughput 0.981785
Reading from 19446: heap size 15 MB, throughput 0.912099
Reading from 19447: heap size 16 MB, throughput 0.988683
Reading from 19446: heap size 18 MB, throughput 0.943858
Reading from 19446: heap size 25 MB, throughput 0.962727
Reading from 19447: heap size 16 MB, throughput 0.98073
Reading from 19446: heap size 28 MB, throughput 0.442078
Reading from 19446: heap size 42 MB, throughput 0.940665
Reading from 19446: heap size 43 MB, throughput 0.832634
Reading from 19447: heap size 24 MB, throughput 0.986742
Reading from 19446: heap size 45 MB, throughput 0.954427
Reading from 19446: heap size 47 MB, throughput 0.271369
Reading from 19446: heap size 64 MB, throughput 0.823529
Reading from 19446: heap size 67 MB, throughput 0.690085
Reading from 19447: heap size 25 MB, throughput 0.819869
Reading from 19446: heap size 69 MB, throughput 0.231437
Reading from 19447: heap size 39 MB, throughput 0.989392
Reading from 19446: heap size 89 MB, throughput 0.624401
Reading from 19446: heap size 97 MB, throughput 0.753663
Reading from 19446: heap size 99 MB, throughput 0.820214
Reading from 19447: heap size 40 MB, throughput 0.978247
Reading from 19446: heap size 103 MB, throughput 0.228647
Reading from 19447: heap size 47 MB, throughput 0.990428
Reading from 19446: heap size 129 MB, throughput 0.616216
Reading from 19446: heap size 135 MB, throughput 0.660844
Reading from 19447: heap size 48 MB, throughput 0.992534
Reading from 19446: heap size 139 MB, throughput 0.69266
Reading from 19446: heap size 143 MB, throughput 0.662969
Reading from 19447: heap size 55 MB, throughput 0.990894
Reading from 19446: heap size 150 MB, throughput 0.640427
Reading from 19447: heap size 56 MB, throughput 0.989977
Reading from 19447: heap size 63 MB, throughput 0.981804
Reading from 19446: heap size 158 MB, throughput 0.138051
Reading from 19447: heap size 64 MB, throughput 0.985968
Reading from 19446: heap size 190 MB, throughput 0.551488
Reading from 19446: heap size 200 MB, throughput 0.707539
Reading from 19447: heap size 72 MB, throughput 0.985941
Reading from 19446: heap size 201 MB, throughput 0.730077
Reading from 19447: heap size 72 MB, throughput 0.994158
Reading from 19447: heap size 79 MB, throughput 0.996187
Reading from 19446: heap size 205 MB, throughput 0.57954
Reading from 19447: heap size 80 MB, throughput 0.973809
Reading from 19446: heap size 245 MB, throughput 0.750121
Reading from 19446: heap size 255 MB, throughput 0.591849
Reading from 19446: heap size 258 MB, throughput 0.854386
Reading from 19446: heap size 265 MB, throughput 0.640754
Reading from 19446: heap size 267 MB, throughput 0.639056
Reading from 19447: heap size 88 MB, throughput 0.995478
Reading from 19446: heap size 274 MB, throughput 0.770561
Reading from 19446: heap size 275 MB, throughput 0.821875
Reading from 19446: heap size 281 MB, throughput 0.72782
Reading from 19446: heap size 281 MB, throughput 0.754101
Reading from 19447: heap size 87 MB, throughput 0.987568
Reading from 19446: heap size 289 MB, throughput 0.725281
Reading from 19446: heap size 289 MB, throughput 0.89712
Reading from 19447: heap size 97 MB, throughput 0.995112
Reading from 19446: heap size 287 MB, throughput 0.325492
Reading from 19447: heap size 97 MB, throughput 0.994037
Reading from 19446: heap size 331 MB, throughput 0.649524
Reading from 19446: heap size 331 MB, throughput 0.738985
Reading from 19446: heap size 333 MB, throughput 0.888799
Reading from 19446: heap size 340 MB, throughput 0.947463
Reading from 19447: heap size 103 MB, throughput 0.995034
Reading from 19446: heap size 341 MB, throughput 0.871729
Reading from 19446: heap size 341 MB, throughput 0.69168
Reading from 19446: heap size 345 MB, throughput 0.676479
Reading from 19446: heap size 344 MB, throughput 0.676769
Reading from 19446: heap size 347 MB, throughput 0.689385
Equal recommendation: 864 MB each
Reading from 19447: heap size 103 MB, throughput 0.985553
Reading from 19447: heap size 110 MB, throughput 0.995743
Reading from 19446: heap size 354 MB, throughput 0.979325
Reading from 19447: heap size 110 MB, throughput 0.988283
Reading from 19447: heap size 116 MB, throughput 0.996313
Reading from 19446: heap size 355 MB, throughput 0.975551
Reading from 19447: heap size 116 MB, throughput 0.997278
Reading from 19447: heap size 122 MB, throughput 0.997383
Reading from 19446: heap size 359 MB, throughput 0.982505
Reading from 19447: heap size 123 MB, throughput 0.997162
Reading from 19447: heap size 127 MB, throughput 0.997472
Reading from 19446: heap size 362 MB, throughput 0.976698
Reading from 19447: heap size 128 MB, throughput 0.996813
Reading from 19447: heap size 132 MB, throughput 0.997449
Reading from 19447: heap size 132 MB, throughput 0.997439
Reading from 19446: heap size 361 MB, throughput 0.767826
Reading from 19447: heap size 135 MB, throughput 0.998066
Equal recommendation: 864 MB each
Reading from 19447: heap size 135 MB, throughput 0.998386
Reading from 19446: heap size 408 MB, throughput 0.984789
Reading from 19447: heap size 138 MB, throughput 0.997721
Reading from 19447: heap size 138 MB, throughput 0.998543
Reading from 19446: heap size 412 MB, throughput 0.989049
Reading from 19447: heap size 140 MB, throughput 0.998482
Reading from 19447: heap size 140 MB, throughput 0.996258
Reading from 19447: heap size 142 MB, throughput 0.708875
Reading from 19447: heap size 144 MB, throughput 0.993629
Reading from 19447: heap size 150 MB, throughput 0.992011
Reading from 19446: heap size 413 MB, throughput 0.986296
Reading from 19447: heap size 151 MB, throughput 0.994736
Reading from 19446: heap size 412 MB, throughput 0.986339
Reading from 19447: heap size 161 MB, throughput 0.997975
Reading from 19447: heap size 161 MB, throughput 0.997852
Reading from 19446: heap size 415 MB, throughput 0.987961
Reading from 19447: heap size 167 MB, throughput 0.997502
Equal recommendation: 864 MB each
Reading from 19447: heap size 168 MB, throughput 0.998036
Reading from 19446: heap size 410 MB, throughput 0.983357
Reading from 19447: heap size 174 MB, throughput 0.997977
Reading from 19447: heap size 175 MB, throughput 0.997878
Reading from 19446: heap size 413 MB, throughput 0.987747
Reading from 19447: heap size 179 MB, throughput 0.998151
Reading from 19446: heap size 415 MB, throughput 0.982849
Reading from 19446: heap size 416 MB, throughput 0.863066
Reading from 19446: heap size 411 MB, throughput 0.802836
Reading from 19446: heap size 417 MB, throughput 0.857523
Reading from 19447: heap size 180 MB, throughput 0.998086
Reading from 19446: heap size 427 MB, throughput 0.988235
Reading from 19447: heap size 185 MB, throughput 0.997598
Reading from 19447: heap size 185 MB, throughput 0.997897
Reading from 19446: heap size 429 MB, throughput 0.991103
Reading from 19447: heap size 190 MB, throughput 0.997974
Reading from 19447: heap size 190 MB, throughput 0.996863
Equal recommendation: 864 MB each
Reading from 19446: heap size 431 MB, throughput 0.991912
Reading from 19447: heap size 195 MB, throughput 0.998077
Reading from 19447: heap size 195 MB, throughput 0.997144
Reading from 19447: heap size 200 MB, throughput 0.993975
Reading from 19446: heap size 434 MB, throughput 0.974928
Reading from 19447: heap size 200 MB, throughput 0.995148
Reading from 19446: heap size 432 MB, throughput 0.989638
Reading from 19447: heap size 208 MB, throughput 0.998159
Reading from 19447: heap size 208 MB, throughput 0.997473
Reading from 19446: heap size 435 MB, throughput 0.989958
Reading from 19447: heap size 215 MB, throughput 0.997806
Reading from 19447: heap size 215 MB, throughput 0.99662
Reading from 19446: heap size 433 MB, throughput 0.988224
Equal recommendation: 864 MB each
Reading from 19447: heap size 222 MB, throughput 0.997848
Reading from 19446: heap size 435 MB, throughput 0.988382
Reading from 19447: heap size 222 MB, throughput 0.997796
Reading from 19447: heap size 228 MB, throughput 0.997982
Reading from 19446: heap size 437 MB, throughput 0.987616
Reading from 19447: heap size 228 MB, throughput 0.99767
Reading from 19447: heap size 234 MB, throughput 0.998085
Reading from 19446: heap size 438 MB, throughput 0.993493
Reading from 19446: heap size 438 MB, throughput 0.976177
Reading from 19446: heap size 440 MB, throughput 0.885505
Reading from 19446: heap size 444 MB, throughput 0.904151
Reading from 19447: heap size 234 MB, throughput 0.997368
Reading from 19446: heap size 444 MB, throughput 0.986464
Reading from 19447: heap size 240 MB, throughput 0.99832
Reading from 19447: heap size 240 MB, throughput 0.992509
Equal recommendation: 864 MB each
Reading from 19447: heap size 246 MB, throughput 0.996758
Reading from 19446: heap size 451 MB, throughput 0.993977
Reading from 19447: heap size 246 MB, throughput 0.998052
Reading from 19446: heap size 452 MB, throughput 0.99225
Reading from 19447: heap size 253 MB, throughput 0.998128
Reading from 19447: heap size 254 MB, throughput 0.998213
Reading from 19446: heap size 455 MB, throughput 0.992406
Reading from 19447: heap size 261 MB, throughput 0.998455
Reading from 19446: heap size 456 MB, throughput 0.991411
Reading from 19447: heap size 261 MB, throughput 0.997225
Equal recommendation: 864 MB each
Reading from 19447: heap size 267 MB, throughput 0.998354
Reading from 19446: heap size 455 MB, throughput 0.991187
Reading from 19447: heap size 267 MB, throughput 0.99533
Reading from 19446: heap size 457 MB, throughput 0.989955
Reading from 19447: heap size 274 MB, throughput 0.997358
Reading from 19446: heap size 456 MB, throughput 0.98953
Reading from 19447: heap size 274 MB, throughput 0.99814
Reading from 19447: heap size 280 MB, throughput 0.902426
Reading from 19447: heap size 287 MB, throughput 0.998054
Reading from 19446: heap size 457 MB, throughput 0.994822
Reading from 19446: heap size 460 MB, throughput 0.947218
Reading from 19446: heap size 460 MB, throughput 0.890657
Equal recommendation: 864 MB each
Reading from 19446: heap size 465 MB, throughput 0.972424
Reading from 19447: heap size 299 MB, throughput 0.99891
Reading from 19447: heap size 299 MB, throughput 0.998825
Reading from 19446: heap size 466 MB, throughput 0.994348
Reading from 19447: heap size 308 MB, throughput 0.998189
Reading from 19446: heap size 471 MB, throughput 0.993892
Reading from 19447: heap size 309 MB, throughput 0.999002
Reading from 19446: heap size 472 MB, throughput 0.993101
Reading from 19447: heap size 317 MB, throughput 0.999009
Equal recommendation: 864 MB each
Reading from 19447: heap size 318 MB, throughput 0.998669
Reading from 19446: heap size 472 MB, throughput 0.992222
Reading from 19447: heap size 325 MB, throughput 0.99877
Reading from 19446: heap size 474 MB, throughput 0.991321
Reading from 19447: heap size 326 MB, throughput 0.997208
Reading from 19447: heap size 333 MB, throughput 0.996189
Reading from 19446: heap size 472 MB, throughput 0.990948
Reading from 19447: heap size 333 MB, throughput 0.997996
Reading from 19446: heap size 474 MB, throughput 0.98874
Equal recommendation: 864 MB each
Reading from 19447: heap size 345 MB, throughput 0.998398
Reading from 19446: heap size 476 MB, throughput 0.993624
Reading from 19447: heap size 345 MB, throughput 0.998292
Reading from 19446: heap size 477 MB, throughput 0.921809
Reading from 19446: heap size 478 MB, throughput 0.920964
Reading from 19447: heap size 355 MB, throughput 0.998225
Reading from 19446: heap size 480 MB, throughput 0.993102
Reading from 19447: heap size 356 MB, throughput 0.998122
Reading from 19446: heap size 486 MB, throughput 0.993666
Reading from 19447: heap size 366 MB, throughput 0.99834
Equal recommendation: 864 MB each
Reading from 19446: heap size 487 MB, throughput 0.993756
Reading from 19447: heap size 366 MB, throughput 0.998522
Reading from 19447: heap size 374 MB, throughput 0.994867
Reading from 19446: heap size 488 MB, throughput 0.993339
Reading from 19447: heap size 375 MB, throughput 0.998302
Reading from 19446: heap size 490 MB, throughput 0.99198
Reading from 19447: heap size 388 MB, throughput 0.998631
Reading from 19446: heap size 489 MB, throughput 0.987237
Equal recommendation: 864 MB each
Reading from 19447: heap size 389 MB, throughput 0.998436
Reading from 19446: heap size 491 MB, throughput 0.987591
Reading from 19447: heap size 399 MB, throughput 0.998384
Reading from 19446: heap size 493 MB, throughput 0.992096
Reading from 19446: heap size 494 MB, throughput 0.925972
Reading from 19447: heap size 400 MB, throughput 0.998184
Reading from 19446: heap size 494 MB, throughput 0.950392
Reading from 19447: heap size 410 MB, throughput 0.998431
Reading from 19446: heap size 496 MB, throughput 0.994306
Reading from 19447: heap size 411 MB, throughput 0.99428
Equal recommendation: 864 MB each
Reading from 19447: heap size 421 MB, throughput 0.99837
Reading from 19446: heap size 500 MB, throughput 0.993131
Reading from 19447: heap size 422 MB, throughput 0.997979
Reading from 19446: heap size 501 MB, throughput 0.993886
Reading from 19447: heap size 435 MB, throughput 0.998601
Reading from 19446: heap size 500 MB, throughput 0.992611
Equal recommendation: 864 MB each
Reading from 19447: heap size 436 MB, throughput 0.998507
Reading from 19446: heap size 503 MB, throughput 0.992123
Reading from 19447: heap size 448 MB, throughput 0.998518
Reading from 19446: heap size 504 MB, throughput 0.991722
Reading from 19447: heap size 449 MB, throughput 0.998326
Reading from 19447: heap size 460 MB, throughput 0.996615
Reading from 19446: heap size 505 MB, throughput 0.995113
Reading from 19446: heap size 508 MB, throughput 0.934481
Reading from 19446: heap size 509 MB, throughput 0.952843
Equal recommendation: 864 MB each
Reading from 19447: heap size 460 MB, throughput 0.998487
Reading from 19446: heap size 517 MB, throughput 0.995476
Reading from 19447: heap size 473 MB, throughput 0.998625
Reading from 19446: heap size 517 MB, throughput 0.994312
Reading from 19447: heap size 474 MB, throughput 0.998281
Reading from 19446: heap size 518 MB, throughput 0.99403
Equal recommendation: 864 MB each
Reading from 19447: heap size 487 MB, throughput 0.998518
Reading from 19446: heap size 520 MB, throughput 0.992283
Reading from 19447: heap size 487 MB, throughput 0.997444
Reading from 19447: heap size 501 MB, throughput 0.997248
Reading from 19446: heap size 520 MB, throughput 0.99128
Reading from 19447: heap size 500 MB, throughput 0.99835
Reading from 19446: heap size 521 MB, throughput 0.990168
Equal recommendation: 864 MB each
Reading from 19446: heap size 522 MB, throughput 0.989866
Reading from 19446: heap size 524 MB, throughput 0.920188
Reading from 19447: heap size 515 MB, throughput 0.998161
Reading from 19446: heap size 528 MB, throughput 0.992021
Reading from 19447: heap size 516 MB, throughput 0.998658
Reading from 19446: heap size 530 MB, throughput 0.995272
Reading from 19447: heap size 529 MB, throughput 0.998653
Equal recommendation: 864 MB each
Reading from 19446: heap size 532 MB, throughput 0.992655
Reading from 19447: heap size 530 MB, throughput 0.957845
Reading from 19446: heap size 534 MB, throughput 0.993325
Reading from 19447: heap size 544 MB, throughput 0.999448
Reading from 19446: heap size 532 MB, throughput 0.992399
Reading from 19447: heap size 515 MB, throughput 0.999297
Equal recommendation: 864 MB each
Reading from 19446: heap size 534 MB, throughput 0.990886
Reading from 19447: heap size 491 MB, throughput 0.999455
Reading from 19446: heap size 536 MB, throughput 0.992135
Reading from 19446: heap size 537 MB, throughput 0.928595
Reading from 19447: heap size 468 MB, throughput 0.999404
Reading from 19446: heap size 540 MB, throughput 0.991842
Reading from 19447: heap size 446 MB, throughput 0.999164
Reading from 19447: heap size 426 MB, throughput 0.995003
Reading from 19446: heap size 542 MB, throughput 0.994154
Equal recommendation: 864 MB each
Reading from 19447: heap size 411 MB, throughput 0.998923
Reading from 19446: heap size 544 MB, throughput 0.994591
Reading from 19447: heap size 390 MB, throughput 0.998996
Reading from 19447: heap size 372 MB, throughput 0.99874
Reading from 19446: heap size 546 MB, throughput 0.994226
Reading from 19447: heap size 356 MB, throughput 0.998685
Equal recommendation: 864 MB each
Reading from 19447: heap size 340 MB, throughput 0.997943
Reading from 19446: heap size 544 MB, throughput 0.991882
Reading from 19447: heap size 325 MB, throughput 0.998583
Reading from 19447: heap size 310 MB, throughput 0.998265
Reading from 19446: heap size 546 MB, throughput 0.989552
Reading from 19447: heap size 297 MB, throughput 0.993425
Reading from 19446: heap size 548 MB, throughput 0.991048
Reading from 19447: heap size 313 MB, throughput 0.998239
Reading from 19446: heap size 549 MB, throughput 0.928507
Reading from 19447: heap size 298 MB, throughput 0.997177
Reading from 19446: heap size 552 MB, throughput 0.993032
Equal recommendation: 864 MB each
Reading from 19447: heap size 284 MB, throughput 0.998146
Reading from 19447: heap size 272 MB, throughput 0.997994
Reading from 19446: heap size 554 MB, throughput 0.994696
Reading from 19447: heap size 260 MB, throughput 0.997784
Reading from 19447: heap size 249 MB, throughput 0.997649
Reading from 19447: heap size 239 MB, throughput 0.996934
Reading from 19446: heap size 556 MB, throughput 0.994112
Reading from 19447: heap size 228 MB, throughput 0.997944
Equal recommendation: 864 MB each
Reading from 19447: heap size 219 MB, throughput 0.997933
Reading from 19446: heap size 558 MB, throughput 0.993271
Reading from 19447: heap size 210 MB, throughput 0.99753
Reading from 19447: heap size 201 MB, throughput 0.994497
Reading from 19447: heap size 212 MB, throughput 0.987428
Reading from 19447: heap size 226 MB, throughput 0.997756
Reading from 19446: heap size 556 MB, throughput 0.992679
Reading from 19447: heap size 241 MB, throughput 0.998037
Reading from 19447: heap size 255 MB, throughput 0.997987
Reading from 19447: heap size 268 MB, throughput 0.998496
Reading from 19446: heap size 558 MB, throughput 0.995815
Reading from 19446: heap size 561 MB, throughput 0.956894
Reading from 19446: heap size 561 MB, throughput 0.976843
Equal recommendation: 864 MB each
Reading from 19447: heap size 281 MB, throughput 0.998588
Reading from 19447: heap size 269 MB, throughput 0.998479
Reading from 19447: heap size 257 MB, throughput 0.998325
Reading from 19446: heap size 568 MB, throughput 0.995721
Reading from 19447: heap size 246 MB, throughput 0.998434
Reading from 19447: heap size 236 MB, throughput 0.997946
Reading from 19447: heap size 226 MB, throughput 0.997967
Reading from 19446: heap size 568 MB, throughput 0.994875
Reading from 19447: heap size 216 MB, throughput 0.996625
Reading from 19447: heap size 208 MB, throughput 0.990719
Equal recommendation: 864 MB each
Reading from 19447: heap size 221 MB, throughput 0.995137
Reading from 19447: heap size 234 MB, throughput 0.997251
Reading from 19446: heap size 568 MB, throughput 0.994035
Reading from 19447: heap size 247 MB, throughput 0.997946
Reading from 19447: heap size 259 MB, throughput 0.997434
Reading from 19446: heap size 570 MB, throughput 0.992836
Reading from 19447: heap size 272 MB, throughput 0.998301
Reading from 19447: heap size 285 MB, throughput 0.998353
Equal recommendation: 864 MB each
Reading from 19447: heap size 272 MB, throughput 0.99825
Reading from 19446: heap size 572 MB, throughput 0.993686
Reading from 19447: heap size 260 MB, throughput 0.998216
Reading from 19446: heap size 572 MB, throughput 0.983215
Reading from 19446: heap size 577 MB, throughput 0.962276
Reading from 19447: heap size 250 MB, throughput 0.997408
Reading from 19447: heap size 239 MB, throughput 0.998073
Reading from 19446: heap size 578 MB, throughput 0.995764
Client 19447 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 19446: heap size 581 MB, throughput 0.995013
Reading from 19446: heap size 582 MB, throughput 0.994046
Reading from 19446: heap size 581 MB, throughput 0.992845
Recommendation: one client; give it all the memory
Reading from 19446: heap size 583 MB, throughput 0.996061
Reading from 19446: heap size 584 MB, throughput 0.980253
Reading from 19446: heap size 585 MB, throughput 0.975663
Reading from 19446: heap size 591 MB, throughput 0.995376
Recommendation: one client; give it all the memory
Reading from 19446: heap size 591 MB, throughput 0.994938
Reading from 19446: heap size 590 MB, throughput 0.994066
Reading from 19446: heap size 592 MB, throughput 0.993322
Recommendation: one client; give it all the memory
Reading from 19446: heap size 593 MB, throughput 0.995716
Reading from 19446: heap size 594 MB, throughput 0.944437
Client 19446 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
