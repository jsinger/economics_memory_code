Heap Size vs Throughput Plotter {#heapVsThroughput}
===============================

Clients report measurements of heap size and throughput to the daemon. These scripts visualise the relationship between heap size and throughput, either live as readings come in, or offline based on a saved dataset. The idea is to demonstrate that the relationship looks similar to the shape of an economic throughput curve.

One graph is generated for each active client, with variations to compare different throughput models, and with additional information provided by the daemon.

Live plotting
-------------

 - `live_plot.sh` -- plots readings straight from the clients (no daemon). Clients must be started manually. Can specify the throughput model socket to listen on.
 - `plot_and_fit.sh` -- same as above, but with a daemon. Also gets curve-fitted throughput functions (hence the 'fit' in the name).
 - `plot_and_fit_all_models.sh` -- same as `plot_and_fit.sh`, but for all three throughput models side-by-side. Runs three instances of the daemon, so may be laggy! Clients must still be started manually, with appropriate settings to make them use all three models.


Offline plotting
----------------

 - `live_save.sh` -- take readings straight from clients (no daemon), but save them to files rather than plotting them.
 - `save_and_fit.sh` -- same as above, but with a daemon, and curve fitting.
 - `save_and_fit_all_models.sh` -- same as above, but with daemons for all three models
 - `save_and_fit_benchmarks.sh` -- runs `save_and_fit_all_models.sh` for many benchmark combinations, then generates graphs. See comments in file.
 - `file_plot.sh` -- plot graphs from saved data; takes the data on stdin
 - `replay_plot.sh` -- interactively plot previously saved data


Internals
---------

We want to generate separate graphs for each client/model. Because matplotlib only allows one active interactive graph per process (and is buggy even then), the scripts work in two stages:

1. The demux, which receives readings directly, and splits them up by client
   - `demux.py` -- base class
   - `demux_to_plotters.py` -- subclass for live readings from stdin; forks a subprocess for each client, and streams the readings to the correct subprocess
   - `demux_to_files.py` -- subclass for offline readings from a file; saves the readings to a file per client

2. The plotter, which receives readings from the demux, and draws a graph
   - `plotter.py` -- base class
   - `plot_readings_live.py` -- plots to an interactive window
   - `plot_readings_batch.py` -- plots to files
   - `merge_pdfs.py` -- used internally
