#include <CommandReceiver.hpp>
#include <CommandResultSender.hpp>
#include <Log.hpp>
#include <ReadingReceiver.hpp>
#include <RecommendationSender.hpp>
#include <SignalHandler.hpp>
#include <Utilities.hpp>
#include "Args.hpp"
#include "ClientList.hpp"
#include "CommandThread.hpp"
#include "PyOptimiser.hpp"
#include "ReadingThread.hpp"
#include "TimerThread.hpp"
#include "UtilityModel.hpp"

int main(int argc, char** argv)
{
    Args args(argc, argv);
    PyOptimiser::init();
    economem::Log log(args.logFileName(), args.logReadings(), args.logFunctions());
    log.logTotalMemMB(args.totalMemMB());

    ClientList clients(args.readingCacheNames(), args.forceFunctionValidity(), log);

    UtilityModel* model = UtilityModel::fromName(args.utilityModel(), args.alwaysNumericOptimisation());
    if (model == NULL)
        economem::die("Unknown utility model '%s'", args.utilityModel().c_str());

    economem::ReadingReceiver rRecv(args.readingReceiverName().c_str());
    economem::RecommendationSender rSender;
    economem::CommandReceiver cRecv(args.commandReceiverName().c_str());
    economem::CommandResultSender cSender;

    ReadingThread reader(rRecv, clients);
    TimerThread timer(args.totalMemMB(),
                      args.timerIntervalSeconds(),
                      args.timerRunsAtStart(),
                      args.makeRecommendations(),
                      args.equalRecommendations(),
                      args.randomRecommendations(),
                      rSender,
                      *model,
                      clients,
                      log);
    CommandThread commands(reader, timer, cRecv, cSender, rSender, clients, log, args.dumpFileName());

    economem::SignalHandler::init([&] () { reader.kill(); timer.kill(); commands.kill(); });

    if (!reader.start() || !timer.start() || !commands.start())
        economem::die("Failed to create threads");

    reader.wait();
    timer.wait();
    commands.wait();

    economem::info("Main", "All threads killed, cleaning up");

    delete model;
    PyOptimiser::deinit();

    return 0;
}
