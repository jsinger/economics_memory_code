economemd
    total memory: 3312 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub24_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 1366: heap size 9 MB, throughput 0.98578
Clients: 1
Client 1366 has a minimum heap size of 276 MB
Reading from 1367: heap size 9 MB, throughput 0.984742
Clients: 2
Client 1367 has a minimum heap size of 276 MB
Reading from 1366: heap size 9 MB, throughput 0.983644
Reading from 1367: heap size 9 MB, throughput 0.978703
Reading from 1366: heap size 11 MB, throughput 0.978143
Reading from 1367: heap size 11 MB, throughput 0.975658
Reading from 1366: heap size 11 MB, throughput 0.980434
Reading from 1367: heap size 11 MB, throughput 0.982027
Reading from 1367: heap size 15 MB, throughput 0.925718
Reading from 1366: heap size 15 MB, throughput 0.929358
Reading from 1366: heap size 19 MB, throughput 0.940175
Reading from 1367: heap size 18 MB, throughput 0.949091
Reading from 1366: heap size 24 MB, throughput 0.882689
Reading from 1367: heap size 23 MB, throughput 0.881122
Reading from 1366: heap size 28 MB, throughput 0.768643
Reading from 1367: heap size 27 MB, throughput 0.753652
Reading from 1366: heap size 28 MB, throughput 0.791268
Reading from 1366: heap size 37 MB, throughput 0.271533
Reading from 1366: heap size 43 MB, throughput 0.852495
Reading from 1367: heap size 27 MB, throughput 0.771148
Reading from 1366: heap size 43 MB, throughput 0.83291
Reading from 1367: heap size 36 MB, throughput 0.390792
Reading from 1367: heap size 40 MB, throughput 0.114294
Reading from 1366: heap size 48 MB, throughput 0.576325
Reading from 1367: heap size 41 MB, throughput 0.915129
Reading from 1366: heap size 62 MB, throughput 0.233547
Reading from 1366: heap size 84 MB, throughput 0.437834
Reading from 1367: heap size 44 MB, throughput 0.640068
Reading from 1367: heap size 58 MB, throughput 0.822834
Reading from 1366: heap size 86 MB, throughput 0.392122
Reading from 1367: heap size 62 MB, throughput 0.781581
Reading from 1366: heap size 89 MB, throughput 0.730046
Reading from 1366: heap size 92 MB, throughput 0.688273
Reading from 1367: heap size 65 MB, throughput 0.350782
Reading from 1367: heap size 88 MB, throughput 0.696113
Reading from 1367: heap size 90 MB, throughput 0.698254
Reading from 1367: heap size 93 MB, throughput 0.641306
Reading from 1366: heap size 95 MB, throughput 0.459971
Reading from 1367: heap size 94 MB, throughput 0.696408
Reading from 1366: heap size 121 MB, throughput 0.727249
Reading from 1367: heap size 100 MB, throughput 0.729031
Reading from 1366: heap size 126 MB, throughput 0.680646
Reading from 1366: heap size 128 MB, throughput 0.673835
Reading from 1366: heap size 136 MB, throughput 0.701621
Reading from 1366: heap size 138 MB, throughput 0.61609
Reading from 1367: heap size 103 MB, throughput 0.441531
Reading from 1367: heap size 141 MB, throughput 0.637134
Reading from 1367: heap size 143 MB, throughput 0.681231
Reading from 1367: heap size 147 MB, throughput 0.690505
Reading from 1366: heap size 142 MB, throughput 0.441251
Reading from 1366: heap size 177 MB, throughput 0.569595
Reading from 1366: heap size 184 MB, throughput 0.565742
Reading from 1367: heap size 149 MB, throughput 0.426852
Reading from 1366: heap size 188 MB, throughput 0.618085
Reading from 1367: heap size 184 MB, throughput 0.714939
Reading from 1366: heap size 194 MB, throughput 0.645182
Reading from 1367: heap size 188 MB, throughput 0.68694
Reading from 1367: heap size 190 MB, throughput 0.646463
Reading from 1367: heap size 195 MB, throughput 0.801862
Reading from 1366: heap size 199 MB, throughput 0.504855
Reading from 1367: heap size 201 MB, throughput 0.870648
Reading from 1366: heap size 238 MB, throughput 0.837603
Reading from 1367: heap size 208 MB, throughput 0.804304
Reading from 1366: heap size 243 MB, throughput 0.808534
Reading from 1367: heap size 215 MB, throughput 0.772194
Reading from 1366: heap size 248 MB, throughput 0.747909
Reading from 1367: heap size 223 MB, throughput 0.770781
Reading from 1366: heap size 252 MB, throughput 0.737971
Reading from 1367: heap size 228 MB, throughput 0.632294
Reading from 1366: heap size 256 MB, throughput 0.677394
Reading from 1366: heap size 258 MB, throughput 0.716808
Reading from 1366: heap size 263 MB, throughput 0.73106
Reading from 1367: heap size 234 MB, throughput 0.640283
Reading from 1367: heap size 273 MB, throughput 0.546492
Reading from 1366: heap size 264 MB, throughput 0.709811
Reading from 1367: heap size 275 MB, throughput 0.232026
Reading from 1366: heap size 268 MB, throughput 0.564265
Reading from 1366: heap size 306 MB, throughput 0.634354
Reading from 1367: heap size 316 MB, throughput 0.696607
Reading from 1366: heap size 305 MB, throughput 0.692278
Reading from 1367: heap size 318 MB, throughput 0.727678
Reading from 1366: heap size 307 MB, throughput 0.716323
Reading from 1367: heap size 314 MB, throughput 0.739897
Reading from 1367: heap size 317 MB, throughput 0.773973
Reading from 1367: heap size 312 MB, throughput 0.802454
Reading from 1367: heap size 315 MB, throughput 0.816799
Reading from 1366: heap size 310 MB, throughput 0.886503
Reading from 1366: heap size 311 MB, throughput 0.897473
Reading from 1367: heap size 311 MB, throughput 0.936988
Reading from 1366: heap size 311 MB, throughput 0.85651
Reading from 1367: heap size 314 MB, throughput 0.931632
Reading from 1366: heap size 313 MB, throughput 0.789639
Reading from 1367: heap size 309 MB, throughput 0.903829
Reading from 1366: heap size 313 MB, throughput 0.814484
Reading from 1367: heap size 312 MB, throughput 0.836524
Reading from 1367: heap size 309 MB, throughput 0.806412
Reading from 1367: heap size 311 MB, throughput 0.832354
Reading from 1366: heap size 315 MB, throughput 0.870723
Reading from 1367: heap size 309 MB, throughput 0.898309
Reading from 1366: heap size 316 MB, throughput 0.855118
Reading from 1367: heap size 311 MB, throughput 0.892281
Reading from 1366: heap size 318 MB, throughput 0.828388
Reading from 1367: heap size 307 MB, throughput 0.840297
Reading from 1367: heap size 310 MB, throughput 0.743457
Reading from 1366: heap size 324 MB, throughput 0.730766
Reading from 1367: heap size 314 MB, throughput 0.740056
Reading from 1366: heap size 324 MB, throughput 0.734677
Reading from 1367: heap size 314 MB, throughput 0.736816
Reading from 1367: heap size 319 MB, throughput 0.652274
Reading from 1367: heap size 320 MB, throughput 0.585094
Reading from 1366: heap size 333 MB, throughput 0.637761
Numeric result:
Recommendation: 2 clients, utility 0.499185:
    h1: 1659.26 MB (U(h) = 0.688714*h^0.001)
    h2: 1652.74 MB (U(h) = 0.714142*h^0.001)
Recommendation: 2 clients, utility 0.499185:
    h1: 1656 MB (U(h) = 0.688714*h^0.001)
    h2: 1656 MB (U(h) = 0.714142*h^0.001)
Reading from 1366: heap size 373 MB, throughput 0.67582
Reading from 1367: heap size 328 MB, throughput 0.931225
Reading from 1366: heap size 380 MB, throughput 0.957045
Reading from 1367: heap size 328 MB, throughput 0.964153
Reading from 1367: heap size 333 MB, throughput 0.971235
Reading from 1366: heap size 381 MB, throughput 0.972529
Reading from 1367: heap size 334 MB, throughput 0.974104
Reading from 1366: heap size 384 MB, throughput 0.97203
Reading from 1367: heap size 335 MB, throughput 0.972784
Reading from 1366: heap size 386 MB, throughput 0.973922
Reading from 1367: heap size 337 MB, throughput 0.971682
Reading from 1366: heap size 386 MB, throughput 0.971004
Reading from 1367: heap size 334 MB, throughput 0.974011
Reading from 1366: heap size 388 MB, throughput 0.975234
Reading from 1367: heap size 337 MB, throughput 0.9742
Numeric result:
Recommendation: 2 clients, utility 0.544191:
    h1: 1962.19 MB (U(h) = 0.596948*h^0.0298809)
    h2: 1349.81 MB (U(h) = 0.62673*h^0.0205555)
Recommendation: 2 clients, utility 0.544191:
    h1: 1962.18 MB (U(h) = 0.596948*h^0.0298809)
    h2: 1349.82 MB (U(h) = 0.62673*h^0.0205555)
Reading from 1366: heap size 389 MB, throughput 0.975293
Reading from 1367: heap size 333 MB, throughput 0.974904
Reading from 1367: heap size 336 MB, throughput 0.971995
Reading from 1366: heap size 390 MB, throughput 0.971287
Reading from 1366: heap size 394 MB, throughput 0.967339
Reading from 1367: heap size 337 MB, throughput 0.919068
Reading from 1367: heap size 376 MB, throughput 0.969238
Reading from 1366: heap size 396 MB, throughput 0.943531
Reading from 1367: heap size 379 MB, throughput 0.978904
Reading from 1366: heap size 439 MB, throughput 0.98187
Reading from 1367: heap size 379 MB, throughput 0.98362
Reading from 1366: heap size 440 MB, throughput 0.9874
Numeric result:
Recommendation: 2 clients, utility 0.638911:
    h1: 2046.66 MB (U(h) = 0.529623*h^0.0596635)
    h2: 1265.34 MB (U(h) = 0.588099*h^0.036901)
Recommendation: 2 clients, utility 0.638911:
    h1: 2046.36 MB (U(h) = 0.529623*h^0.0596635)
    h2: 1265.64 MB (U(h) = 0.588099*h^0.036901)
Reading from 1367: heap size 377 MB, throughput 0.976211
Reading from 1367: heap size 379 MB, throughput 0.980454
Reading from 1366: heap size 445 MB, throughput 0.987899
Reading from 1367: heap size 377 MB, throughput 0.968358
Reading from 1367: heap size 379 MB, throughput 0.948035
Reading from 1367: heap size 384 MB, throughput 0.920369
Reading from 1366: heap size 448 MB, throughput 0.979812
Reading from 1367: heap size 387 MB, throughput 0.885275
Reading from 1366: heap size 446 MB, throughput 0.965388
Reading from 1366: heap size 448 MB, throughput 0.949599
Reading from 1366: heap size 455 MB, throughput 0.916604
Reading from 1367: heap size 395 MB, throughput 0.914913
Reading from 1366: heap size 456 MB, throughput 0.973871
Reading from 1367: heap size 397 MB, throughput 0.974176
Reading from 1367: heap size 400 MB, throughput 0.981152
Reading from 1366: heap size 459 MB, throughput 0.985936
Reading from 1367: heap size 402 MB, throughput 0.985295
Reading from 1366: heap size 461 MB, throughput 0.985417
Reading from 1367: heap size 400 MB, throughput 0.985518
Reading from 1366: heap size 459 MB, throughput 0.982859
Numeric result:
Recommendation: 2 clients, utility 0.756472:
    h1: 1883.84 MB (U(h) = 0.479704*h^0.083658)
    h2: 1428.16 MB (U(h) = 0.529371*h^0.0634198)
Recommendation: 2 clients, utility 0.756472:
    h1: 1883.87 MB (U(h) = 0.479704*h^0.083658)
    h2: 1428.13 MB (U(h) = 0.529371*h^0.0634198)
Reading from 1367: heap size 403 MB, throughput 0.985285
Reading from 1366: heap size 463 MB, throughput 0.98619
Reading from 1367: heap size 401 MB, throughput 0.985477
Reading from 1366: heap size 460 MB, throughput 0.985465
Reading from 1367: heap size 403 MB, throughput 0.986722
Reading from 1366: heap size 463 MB, throughput 0.985111
Reading from 1367: heap size 402 MB, throughput 0.985878
Reading from 1366: heap size 465 MB, throughput 0.983819
Reading from 1367: heap size 403 MB, throughput 0.983269
Reading from 1367: heap size 406 MB, throughput 0.983993
Reading from 1366: heap size 466 MB, throughput 0.983107
Numeric result:
Recommendation: 2 clients, utility 0.811246:
    h1: 1862.41 MB (U(h) = 0.458574*h^0.094507)
    h2: 1449.59 MB (U(h) = 0.508326*h^0.0735647)
Recommendation: 2 clients, utility 0.811246:
    h1: 1862.34 MB (U(h) = 0.458574*h^0.094507)
    h2: 1449.66 MB (U(h) = 0.508326*h^0.0735647)
Reading from 1367: heap size 406 MB, throughput 0.983808
Reading from 1366: heap size 470 MB, throughput 0.97889
Reading from 1366: heap size 470 MB, throughput 0.980569
Reading from 1367: heap size 407 MB, throughput 0.985796
Reading from 1366: heap size 476 MB, throughput 0.971294
Reading from 1367: heap size 408 MB, throughput 0.976137
Reading from 1367: heap size 406 MB, throughput 0.962503
Reading from 1366: heap size 477 MB, throughput 0.935237
Reading from 1367: heap size 409 MB, throughput 0.937861
Reading from 1367: heap size 415 MB, throughput 0.949073
Reading from 1366: heap size 483 MB, throughput 0.961113
Reading from 1367: heap size 417 MB, throughput 0.981563
Reading from 1366: heap size 482 MB, throughput 0.985403
Reading from 1367: heap size 422 MB, throughput 0.987141
Reading from 1366: heap size 488 MB, throughput 0.987874
Reading from 1367: heap size 423 MB, throughput 0.987655
Numeric result:
Recommendation: 2 clients, utility 0.89217:
    h1: 1786.31 MB (U(h) = 0.436577*h^0.106232)
    h2: 1525.69 MB (U(h) = 0.474339*h^0.0907301)
Recommendation: 2 clients, utility 0.89217:
    h1: 1786.33 MB (U(h) = 0.436577*h^0.106232)
    h2: 1525.67 MB (U(h) = 0.474339*h^0.0907301)
Reading from 1366: heap size 490 MB, throughput 0.990503
Reading from 1367: heap size 423 MB, throughput 0.987294
Reading from 1366: heap size 491 MB, throughput 0.988056
Reading from 1367: heap size 425 MB, throughput 0.987419
Reading from 1366: heap size 493 MB, throughput 0.987699
Reading from 1367: heap size 423 MB, throughput 0.985965
Reading from 1366: heap size 490 MB, throughput 0.988254
Reading from 1367: heap size 425 MB, throughput 0.985636
Reading from 1367: heap size 424 MB, throughput 0.984847
Reading from 1366: heap size 493 MB, throughput 0.986685
Numeric result:
Recommendation: 2 clients, utility 0.920832:
    h1: 1788.92 MB (U(h) = 0.426761*h^0.111616)
    h2: 1523.08 MB (U(h) = 0.466126*h^0.0950296)
Recommendation: 2 clients, utility 0.920832:
    h1: 1788.92 MB (U(h) = 0.426761*h^0.111616)
    h2: 1523.08 MB (U(h) = 0.466126*h^0.0950296)
Reading from 1367: heap size 425 MB, throughput 0.984131
Reading from 1366: heap size 494 MB, throughput 0.989653
Reading from 1366: heap size 495 MB, throughput 0.985898
Reading from 1366: heap size 496 MB, throughput 0.976587
Reading from 1367: heap size 427 MB, throughput 0.983992
Reading from 1366: heap size 498 MB, throughput 0.969733
Reading from 1367: heap size 428 MB, throughput 0.976947
Reading from 1367: heap size 429 MB, throughput 0.965234
Reading from 1367: heap size 431 MB, throughput 0.942017
Reading from 1367: heap size 436 MB, throughput 0.95716
Reading from 1366: heap size 505 MB, throughput 0.986123
Reading from 1367: heap size 437 MB, throughput 0.981172
Reading from 1366: heap size 506 MB, throughput 0.991235
Reading from 1367: heap size 442 MB, throughput 0.987559
Numeric result:
Recommendation: 2 clients, utility 0.982294:
    h1: 1757.05 MB (U(h) = 0.411478*h^0.120207)
    h2: 1554.95 MB (U(h) = 0.445033*h^0.106356)
Recommendation: 2 clients, utility 0.982294:
    h1: 1757.24 MB (U(h) = 0.411478*h^0.120207)
    h2: 1554.76 MB (U(h) = 0.445033*h^0.106356)
Reading from 1366: heap size 507 MB, throughput 0.99085
Reading from 1367: heap size 444 MB, throughput 0.988788
Reading from 1367: heap size 443 MB, throughput 0.987722
Reading from 1366: heap size 509 MB, throughput 0.989622
Reading from 1367: heap size 446 MB, throughput 0.98716
Reading from 1366: heap size 507 MB, throughput 0.985956
Reading from 1367: heap size 444 MB, throughput 0.986615
Reading from 1366: heap size 510 MB, throughput 0.919562
Reading from 1367: heap size 446 MB, throughput 0.986172
Reading from 1366: heap size 512 MB, throughput 0.91875
Numeric result:
Recommendation: 2 clients, utility 1.0078:
    h1: 1746.94 MB (U(h) = 0.405371*h^0.123706)
    h2: 1565.06 MB (U(h) = 0.43688*h^0.110837)
Recommendation: 2 clients, utility 1.0078:
    h1: 1746.86 MB (U(h) = 0.405371*h^0.123706)
    h2: 1565.14 MB (U(h) = 0.43688*h^0.110837)
Reading from 1367: heap size 447 MB, throughput 0.985228
Reading from 1366: heap size 512 MB, throughput 0.990326
Reading from 1366: heap size 514 MB, throughput 0.879536
Reading from 1366: heap size 515 MB, throughput 0.968408
Reading from 1367: heap size 447 MB, throughput 0.982337
Reading from 1367: heap size 449 MB, throughput 0.976013
Reading from 1367: heap size 450 MB, throughput 0.961382
Reading from 1367: heap size 455 MB, throughput 0.953511
Reading from 1366: heap size 520 MB, throughput 0.981087
Reading from 1367: heap size 457 MB, throughput 0.986038
Reading from 1366: heap size 522 MB, throughput 0.985837
Reading from 1367: heap size 462 MB, throughput 0.987574
Numeric result:
Recommendation: 2 clients, utility 1.04114:
    h1: 1718.95 MB (U(h) = 0.399616*h^0.127031)
    h2: 1593.05 MB (U(h) = 0.424581*h^0.117707)
Recommendation: 2 clients, utility 1.04114:
    h1: 1719.09 MB (U(h) = 0.399616*h^0.127031)
    h2: 1592.91 MB (U(h) = 0.424581*h^0.117707)
Reading from 1366: heap size 527 MB, throughput 0.989258
Reading from 1367: heap size 463 MB, throughput 0.988139
Reading from 1367: heap size 464 MB, throughput 0.987988
Reading from 1366: heap size 528 MB, throughput 0.990036
Reading from 1367: heap size 466 MB, throughput 0.987597
Reading from 1366: heap size 528 MB, throughput 0.958255
Reading from 1367: heap size 465 MB, throughput 0.987346
Reading from 1366: heap size 530 MB, throughput 0.96226
Numeric result:
Recommendation: 2 clients, utility 1.0639:
    h1: 1705.77 MB (U(h) = 0.395233*h^0.129579)
    h2: 1606.23 MB (U(h) = 0.416997*h^0.122005)
Recommendation: 2 clients, utility 1.0639:
    h1: 1705.85 MB (U(h) = 0.395233*h^0.129579)
    h2: 1606.15 MB (U(h) = 0.416997*h^0.122005)
Reading from 1367: heap size 466 MB, throughput 0.985722
Reading from 1366: heap size 533 MB, throughput 0.932822
Reading from 1367: heap size 468 MB, throughput 0.986855
Reading from 1366: heap size 534 MB, throughput 0.986775
Reading from 1366: heap size 537 MB, throughput 0.980743
Reading from 1367: heap size 469 MB, throughput 0.982057
Reading from 1367: heap size 471 MB, throughput 0.972471
Reading from 1367: heap size 473 MB, throughput 0.961809
Reading from 1366: heap size 539 MB, throughput 0.977766
Reading from 1367: heap size 480 MB, throughput 0.988251
Reading from 1366: heap size 546 MB, throughput 0.990451
Reading from 1367: heap size 481 MB, throughput 0.98855
Numeric result:
Recommendation: 2 clients, utility 1.09319:
    h1: 1695.06 MB (U(h) = 0.389032*h^0.133207)
    h2: 1616.94 MB (U(h) = 0.408181*h^0.127063)
Recommendation: 2 clients, utility 1.09319:
    h1: 1695.09 MB (U(h) = 0.389032*h^0.133207)
    h2: 1616.91 MB (U(h) = 0.408181*h^0.127063)
Reading from 1366: heap size 547 MB, throughput 0.991324
Reading from 1367: heap size 483 MB, throughput 0.989986
Reading from 1366: heap size 547 MB, throughput 0.990977
Reading from 1367: heap size 485 MB, throughput 0.988851
Reading from 1366: heap size 549 MB, throughput 0.991602
Reading from 1367: heap size 483 MB, throughput 0.9865
Reading from 1366: heap size 549 MB, throughput 0.949273
Reading from 1367: heap size 485 MB, throughput 0.987189
Numeric result:
Recommendation: 2 clients, utility 1.33611:
    h1: 1709.33 MB (U(h) = 0.269366*h^0.196559)
    h2: 1602.67 MB (U(h) = 0.29473*h^0.184294)
Recommendation: 2 clients, utility 1.33611:
    h1: 1709.33 MB (U(h) = 0.269366*h^0.196559)
    h2: 1602.67 MB (U(h) = 0.29473*h^0.184294)
Reading from 1366: heap size 550 MB, throughput 0.932287
Reading from 1367: heap size 488 MB, throughput 0.986195
Reading from 1366: heap size 553 MB, throughput 0.854061
Reading from 1366: heap size 553 MB, throughput 0.980403
Reading from 1367: heap size 488 MB, throughput 0.984502
Reading from 1367: heap size 493 MB, throughput 0.976116
Reading from 1367: heap size 495 MB, throughput 0.96915
Reading from 1366: heap size 558 MB, throughput 0.987012
Reading from 1367: heap size 502 MB, throughput 0.987429
Reading from 1366: heap size 559 MB, throughput 0.990425
Reading from 1367: heap size 502 MB, throughput 0.990337
Numeric result:
Recommendation: 2 clients, utility 2.13832:
    h1: 1748.62 MB (U(h) = 0.102846*h^0.359688)
    h2: 1563.38 MB (U(h) = 0.133148*h^0.321594)
Recommendation: 2 clients, utility 2.13832:
    h1: 1748.6 MB (U(h) = 0.102846*h^0.359688)
    h2: 1563.4 MB (U(h) = 0.133148*h^0.321594)
Reading from 1366: heap size 561 MB, throughput 0.991492
Reading from 1367: heap size 504 MB, throughput 0.990983
Reading from 1366: heap size 563 MB, throughput 0.991379
Reading from 1367: heap size 506 MB, throughput 0.990839
Reading from 1366: heap size 562 MB, throughput 0.868231
Reading from 1367: heap size 503 MB, throughput 0.990448
Numeric result:
Recommendation: 2 clients, utility 2.16384:
    h1: 1975.27 MB (U(h) = 0.0763213*h^0.40917)
    h2: 1336.73 MB (U(h) = 0.173158*h^0.27692)
Recommendation: 2 clients, utility 2.16384:
    h1: 1975.21 MB (U(h) = 0.0763213*h^0.40917)
    h2: 1336.79 MB (U(h) = 0.173158*h^0.27692)
Reading from 1366: heap size 564 MB, throughput 0.923201
Reading from 1367: heap size 506 MB, throughput 0.98918
Reading from 1366: heap size 567 MB, throughput 0.990112
Reading from 1366: heap size 568 MB, throughput 0.984013
Reading from 1367: heap size 508 MB, throughput 0.990572
Reading from 1367: heap size 508 MB, throughput 0.983619
Reading from 1367: heap size 508 MB, throughput 0.976286
Reading from 1366: heap size 572 MB, throughput 0.98845
Reading from 1367: heap size 510 MB, throughput 0.987959
Reading from 1366: heap size 573 MB, throughput 0.991938
Numeric result:
Recommendation: 2 clients, utility 2.23282:
    h1: 1794.97 MB (U(h) = 0.0867856*h^0.387219)
    h2: 1517.03 MB (U(h) = 0.128636*h^0.327259)
Recommendation: 2 clients, utility 2.23282:
    h1: 1794.97 MB (U(h) = 0.0867856*h^0.387219)
    h2: 1517.03 MB (U(h) = 0.128636*h^0.327259)
Reading from 1367: heap size 516 MB, throughput 0.990244
Reading from 1366: heap size 576 MB, throughput 0.993253
Reading from 1367: heap size 517 MB, throughput 0.990595
Reading from 1366: heap size 578 MB, throughput 0.991978
Reading from 1367: heap size 516 MB, throughput 0.991749
Reading from 1366: heap size 577 MB, throughput 0.991117
Numeric result:
Recommendation: 2 clients, utility 2.34035:
    h1: 1828.53 MB (U(h) = 0.0737841*h^0.413595)
    h2: 1483.47 MB (U(h) = 0.12247*h^0.335544)
Recommendation: 2 clients, utility 2.34035:
    h1: 1828.53 MB (U(h) = 0.0737841*h^0.413595)
    h2: 1483.47 MB (U(h) = 0.12247*h^0.335544)
Reading from 1367: heap size 519 MB, throughput 0.989898
Reading from 1366: heap size 579 MB, throughput 0.989996
Reading from 1367: heap size 519 MB, throughput 0.98912
Reading from 1366: heap size 581 MB, throughput 0.989463
Reading from 1366: heap size 582 MB, throughput 0.982055
Reading from 1366: heap size 586 MB, throughput 0.979209
Reading from 1367: heap size 520 MB, throughput 0.987778
Reading from 1367: heap size 523 MB, throughput 0.982866
Reading from 1367: heap size 523 MB, throughput 0.978462
Reading from 1366: heap size 587 MB, throughput 0.988824
Reading from 1367: heap size 531 MB, throughput 0.990247
Numeric result:
Recommendation: 2 clients, utility 2.66328:
    h1: 1687.24 MB (U(h) = 0.0661144*h^0.43112)
    h2: 1624.76 MB (U(h) = 0.0760093*h^0.415148)
Recommendation: 2 clients, utility 2.66328:
    h1: 1687.26 MB (U(h) = 0.0661144*h^0.43112)
    h2: 1624.74 MB (U(h) = 0.0760093*h^0.415148)
Reading from 1366: heap size 591 MB, throughput 0.990545
Reading from 1367: heap size 531 MB, throughput 0.991715
Reading from 1367: heap size 532 MB, throughput 0.991991
Reading from 1366: heap size 593 MB, throughput 0.991348
Reading from 1367: heap size 534 MB, throughput 0.991487
Reading from 1366: heap size 592 MB, throughput 0.990688
Numeric result:
Recommendation: 2 clients, utility 2.66788:
    h1: 1608.89 MB (U(h) = 0.0745569*h^0.411365)
    h2: 1703.11 MB (U(h) = 0.0672291*h^0.435457)
Recommendation: 2 clients, utility 2.66788:
    h1: 1608.89 MB (U(h) = 0.0745569*h^0.411365)
    h2: 1703.11 MB (U(h) = 0.0672291*h^0.435457)
Reading from 1367: heap size 534 MB, throughput 0.990546
Reading from 1366: heap size 594 MB, throughput 0.992436
Reading from 1366: heap size 597 MB, throughput 0.987767
Reading from 1366: heap size 598 MB, throughput 0.985599
Reading from 1367: heap size 535 MB, throughput 0.991607
Reading from 1367: heap size 535 MB, throughput 0.987699
Reading from 1367: heap size 537 MB, throughput 0.976533
Reading from 1366: heap size 605 MB, throughput 0.991667
Reading from 1367: heap size 541 MB, throughput 0.987779
Numeric result:
Recommendation: 2 clients, utility 2.78304:
    h1: 1513.88 MB (U(h) = 0.0790531*h^0.401414)
    h2: 1798.12 MB (U(h) = 0.0522561*h^0.47681)
Recommendation: 2 clients, utility 2.78304:
    h1: 1513.83 MB (U(h) = 0.0790531*h^0.401414)
    h2: 1798.17 MB (U(h) = 0.0522561*h^0.47681)
Reading from 1366: heap size 606 MB, throughput 0.990724
Reading from 1367: heap size 543 MB, throughput 0.991228
Reading from 1366: heap size 606 MB, throughput 0.990802
Reading from 1367: heap size 545 MB, throughput 0.992911
Reading from 1367: heap size 547 MB, throughput 0.992789
Reading from 1366: heap size 608 MB, throughput 0.892828
Numeric result:
Recommendation: 2 clients, utility 2.61277:
    h1: 1525.13 MB (U(h) = 0.0878522*h^0.384038)
    h2: 1786.87 MB (U(h) = 0.0613157*h^0.449946)
Recommendation: 2 clients, utility 2.61277:
    h1: 1525.13 MB (U(h) = 0.0878522*h^0.384038)
    h2: 1786.87 MB (U(h) = 0.0613157*h^0.449946)
Reading from 1367: heap size 547 MB, throughput 0.991709
Reading from 1366: heap size 610 MB, throughput 0.992917
Reading from 1366: heap size 610 MB, throughput 0.81236
Reading from 1366: heap size 611 MB, throughput 0.98759
Reading from 1367: heap size 548 MB, throughput 0.992427
Reading from 1367: heap size 548 MB, throughput 0.988275
Reading from 1367: heap size 550 MB, throughput 0.982502
Reading from 1366: heap size 613 MB, throughput 0.993291
Numeric result:
Recommendation: 2 clients, utility 2.59361:
    h1: 1430.85 MB (U(h) = 0.104084*h^0.356394)
    h2: 1881.15 MB (U(h) = 0.0546497*h^0.46857)
Recommendation: 2 clients, utility 2.59361:
    h1: 1430.82 MB (U(h) = 0.104084*h^0.356394)
    h2: 1881.18 MB (U(h) = 0.0546497*h^0.46857)
Reading from 1367: heap size 557 MB, throughput 0.991262
Reading from 1366: heap size 615 MB, throughput 0.993051
Reading from 1367: heap size 557 MB, throughput 0.992612
Reading from 1366: heap size 617 MB, throughput 0.880549
Reading from 1367: heap size 559 MB, throughput 0.99239
Numeric result:
Recommendation: 2 clients, utility 2.61909:
    h1: 1114.42 MB (U(h) = 0.179958*h^0.268483)
    h2: 2197.58 MB (U(h) = 0.03763*h^0.529442)
Recommendation: 2 clients, utility 2.61909:
    h1: 1114.41 MB (U(h) = 0.179958*h^0.268483)
    h2: 2197.59 MB (U(h) = 0.03763*h^0.529442)
Reading from 1366: heap size 616 MB, throughput 0.956998
Reading from 1367: heap size 560 MB, throughput 0.991324
Reading from 1366: heap size 618 MB, throughput 0.899774
Reading from 1366: heap size 619 MB, throughput 0.915274
Reading from 1367: heap size 561 MB, throughput 0.990436
Reading from 1366: heap size 621 MB, throughput 0.990063
Reading from 1367: heap size 562 MB, throughput 0.989541
Reading from 1367: heap size 564 MB, throughput 0.984428
Reading from 1367: heap size 566 MB, throughput 0.985519
Numeric result:
Recommendation: 2 clients, utility 2.39985:
    h1: 968.696 MB (U(h) = 0.260496*h^0.208915)
    h2: 2343.3 MB (U(h) = 0.0433956*h^0.50539)
Recommendation: 2 clients, utility 2.39985:
    h1: 968.673 MB (U(h) = 0.260496*h^0.208915)
    h2: 2343.33 MB (U(h) = 0.0433956*h^0.50539)
Reading from 1366: heap size 628 MB, throughput 0.992634
Reading from 1367: heap size 572 MB, throughput 0.990707
Reading from 1366: heap size 628 MB, throughput 0.992168
Reading from 1367: heap size 573 MB, throughput 0.991674
Reading from 1366: heap size 627 MB, throughput 0.964814
Reading from 1367: heap size 573 MB, throughput 0.991127
Numeric result:
Recommendation: 2 clients, utility 1.75774:
    h1: 1263.78 MB (U(h) = 0.287792*h^0.192963)
    h2: 2048.22 MB (U(h) = 0.14184*h^0.312731)
Recommendation: 2 clients, utility 1.75774:
    h1: 1263.79 MB (U(h) = 0.287792*h^0.192963)
    h2: 2048.21 MB (U(h) = 0.14184*h^0.312731)
Reading from 1366: heap size 629 MB, throughput 0.962733
Reading from 1367: heap size 575 MB, throughput 0.991029
Reading from 1366: heap size 631 MB, throughput 0.873587
Reading from 1366: heap size 633 MB, throughput 0.982296
Reading from 1367: heap size 577 MB, throughput 0.99279
Reading from 1367: heap size 577 MB, throughput 0.98897
Reading from 1367: heap size 577 MB, throughput 0.985984
Reading from 1366: heap size 638 MB, throughput 0.987781
Numeric result:
Recommendation: 2 clients, utility 1.53839:
    h1: 436.588 MB (U(h) = 0.742787*h^0.0420414)
    h2: 2875.41 MB (U(h) = 0.176837*h^0.276878)
Recommendation: 2 clients, utility 1.53839:
    h1: 436.603 MB (U(h) = 0.742787*h^0.0420414)
    h2: 2875.4 MB (U(h) = 0.176837*h^0.276878)
Reading from 1367: heap size 579 MB, throughput 0.988456
Reading from 1366: heap size 639 MB, throughput 0.989843
Reading from 1367: heap size 582 MB, throughput 0.991177
Reading from 1366: heap size 603 MB, throughput 0.991668
Numeric result:
Recommendation: 2 clients, utility 1.70478:
    h1: 276 MB (U(h) = 1.08227*h^0.001)
    h2: 3036 MB (U(h) = 0.200616*h^0.256303)
Recommendation: 2 clients, utility 1.70478:
    h1: 276 MB (U(h) = 1.08227*h^0.001)
    h2: 3036 MB (U(h) = 0.200616*h^0.256303)
Reading from 1367: heap size 583 MB, throughput 0.992065
Reading from 1366: heap size 595 MB, throughput 0.99149
Reading from 1367: heap size 582 MB, throughput 0.991875
Reading from 1366: heap size 582 MB, throughput 0.993001
Reading from 1366: heap size 554 MB, throughput 0.990082
Reading from 1366: heap size 558 MB, throughput 0.98353
Client 1366 died
Clients: 1
Reading from 1367: heap size 583 MB, throughput 0.993295
Reading from 1367: heap size 584 MB, throughput 0.986283
Client 1367 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
