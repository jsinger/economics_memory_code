#include <cmath>
#include <cstdio>
#include "UnboundedCache.hpp"
#include "ReadingIterator.hpp"

const char* UnboundedCache::NAME = "unbounded";

class UnboundedCache::Iterator : public ReadingIterator
{
private:
    const std::map<unsigned long long, CacheReading>& m_Cache;
    std::map<unsigned long long, CacheReading>::const_iterator m_It;

public:
    Iterator(const std::map<unsigned long long, CacheReading>& c);
    virtual bool hasNext() const;
    virtual std::pair<unsigned long long, double> getNext();
};

UnboundedCache::Iterator::Iterator(const std::map<unsigned long long, CacheReading>& c) : m_Cache(c)
{
    m_It = m_Cache.begin();
}

bool UnboundedCache::Iterator::hasNext() const
{
    return m_It != m_Cache.end();
}

std::pair<unsigned long long, double> UnboundedCache::Iterator::getNext()
{
    std::pair<unsigned long long, double> p(m_It->second.getHeapSizeMB(), m_It->second.getThroughput());
    ++m_It;
    return p;
}

std::string UnboundedCache::getName() const
{
    return NAME;
}

bool UnboundedCache::isValid() const
{
    return m_Cache.size() >= UnboundedCache::MIN_READINGS_VALID;
}

void UnboundedCache::addReading(unsigned long long heapSizeMB, double throughput)
{
    std::map<unsigned long long, CacheReading>::iterator it = m_Cache.find(heapSizeMB);
    if (it == m_Cache.end())
    {
        // This heap size is not in the cache
        m_Cache.insert(std::pair<unsigned long long, CacheReading>(heapSizeMB, CacheReading(heapSizeMB, throughput)));
    }
    else
    {
        // This heap size is in the cache, so should be updated
        it->second.updateReading(throughput);
    }
}

ReadingIterator* UnboundedCache::newIterator() const
{
    return new Iterator(m_Cache);
}

void UnboundedCache::dumpTo(FILE* fd) const
{
    fprintf(fd, "    Readings: %zu (%s)\n", m_Cache.size(), (isValid() ? "valid" : "invalid"));

    for (std::map<unsigned long long, CacheReading>::const_iterator it = m_Cache.begin(); it != m_Cache.end(); ++it)
        it->second.dumpTo(fd);
}
