#ifndef CLIENTLIST_HPP
#define CLIENTLIST_HPP

#include <string>
#include <vector>
#include <Log.hpp>
#include <Messages.hpp>
#include <Mutex.hpp>
#include <RecommendationSender.hpp>

class Client;
class UtilityModel;

class ClientList
{
public:
    ClientList(const std::vector<std::string>& readingCacheNames, bool forceFunctionValidity, economem::Log& log);
    ~ClientList();

    void lock() EXCLUSIVE_LOCK_FUNCTION(m_Lock);
    void unlock() UNLOCK_FUNCTION(m_Lock);
    void addReading(const economem::Reading& reading) EXCLUSIVE_LOCKS_REQUIRED(m_Lock);
    void clientDied(pid_t pid) EXCLUSIVE_LOCKS_REQUIRED(m_Lock);
    size_t count() EXCLUSIVE_LOCKS_REQUIRED(m_Lock);
    Client* getClient(size_t i) EXCLUSIVE_LOCKS_REQUIRED(m_Lock);
    Client* getClientByPid(pid_t pid) EXCLUSIVE_LOCKS_REQUIRED(m_Lock);
    void calculateAllFunctions(const UtilityModel& u) EXCLUSIVE_LOCKS_REQUIRED(m_Lock);
    bool allValid() EXCLUSIVE_LOCKS_REQUIRED(m_Lock);
    void makeRecommendations(std::vector<economem::Recommendation>& l, economem::RecommendationSender& sender) EXCLUSIVE_LOCKS_REQUIRED(m_Lock);
    bool recommendToClient(size_t i, const economem::Recommendation& r, economem::RecommendationSender& sender) EXCLUSIVE_LOCKS_REQUIRED(m_Lock);
    void recommendToAll(const economem::Recommendation& r, economem::RecommendationSender& sender) EXCLUSIVE_LOCKS_REQUIRED(m_Lock);
    void dumpTo(FILE* fd) EXCLUSIVE_LOCKS_REQUIRED(m_Lock);
    void clear() EXCLUSIVE_LOCKS_REQUIRED(m_Lock);

private:
    economem::Mutex m_Lock;
    std::vector<Client*> m_Clients GUARDED_BY(m_Lock);
    const std::vector<std::string>& m_ReadingCacheNames;
    const bool m_ForceFunctionValidity;
    economem::Log& m_Log;
};

#endif
