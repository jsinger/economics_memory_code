economemd
    total memory: 1000 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: no
    equal recommendations: no

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/gcFactor_2014_09_24_12_05_40/daemon_log_22.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Reading from 6745: heap size 9 MB, throughput 0.992494
Clients: 1
Reading from 6745: heap size 9 MB, throughput 0.976473
Reading from 6745: heap size 9 MB, throughput 0.961661
Reading from 6745: heap size 9 MB, throughput 0.969724
Reading from 6745: heap size 11 MB, throughput 0.987562
Reading from 6745: heap size 11 MB, throughput 0.968808
Reading from 6745: heap size 17 MB, throughput 0.956497
Reading from 6745: heap size 17 MB, throughput 0.667854
Reading from 6745: heap size 30 MB, throughput 0.949884
Reading from 6745: heap size 31 MB, throughput 0.916557
Reading from 6745: heap size 35 MB, throughput 0.395876
Reading from 6745: heap size 48 MB, throughput 0.881176
Reading from 6745: heap size 49 MB, throughput 0.897325
Reading from 6745: heap size 53 MB, throughput 0.257196
Reading from 6745: heap size 74 MB, throughput 0.840826
Reading from 6745: heap size 75 MB, throughput 0.322942
Reading from 6745: heap size 102 MB, throughput 0.755609
Reading from 6745: heap size 102 MB, throughput 0.804958
Reading from 6745: heap size 103 MB, throughput 0.81437
Reading from 6745: heap size 106 MB, throughput 0.800473
Reading from 6745: heap size 108 MB, throughput 0.205513
Reading from 6745: heap size 140 MB, throughput 0.752786
Reading from 6745: heap size 143 MB, throughput 0.794283
Reading from 6745: heap size 145 MB, throughput 0.779401
Reading from 6745: heap size 152 MB, throughput 0.802632
Reading from 6745: heap size 153 MB, throughput 0.747184
Reading from 6745: heap size 155 MB, throughput 0.181805
Reading from 6745: heap size 197 MB, throughput 0.730153
Reading from 6745: heap size 201 MB, throughput 0.75048
Reading from 6745: heap size 204 MB, throughput 0.679447
Reading from 6745: heap size 207 MB, throughput 0.693756
Reading from 6745: heap size 213 MB, throughput 0.602395
Reading from 6745: heap size 217 MB, throughput 0.634375
Reading from 6745: heap size 226 MB, throughput 0.546802
Reading from 6745: heap size 231 MB, throughput 0.522624
Reading from 6745: heap size 238 MB, throughput 0.119475
Reading from 6745: heap size 285 MB, throughput 0.543326
Reading from 6745: heap size 233 MB, throughput 0.641374
Reading from 6745: heap size 275 MB, throughput 0.699268
Reading from 6745: heap size 280 MB, throughput 0.645421
Reading from 6745: heap size 273 MB, throughput 0.147136
Reading from 6745: heap size 321 MB, throughput 0.60401
Reading from 6745: heap size 316 MB, throughput 0.755003
Reading from 6745: heap size 319 MB, throughput 0.696338
Reading from 6745: heap size 320 MB, throughput 0.722547
Reading from 6745: heap size 322 MB, throughput 0.680118
Reading from 6745: heap size 324 MB, throughput 0.671494
Reading from 6745: heap size 331 MB, throughput 0.618184
Reading from 6745: heap size 334 MB, throughput 0.571523
Reading from 6745: heap size 343 MB, throughput 0.581098
Reading from 6745: heap size 347 MB, throughput 0.476147
Reading from 6745: heap size 356 MB, throughput 0.465816
Reading from 6745: heap size 363 MB, throughput 0.0839085
Reading from 6745: heap size 412 MB, throughput 0.449672
Reading from 6745: heap size 405 MB, throughput 0.570804
Reading from 6745: heap size 351 MB, throughput 0.0990329
Reading from 6745: heap size 460 MB, throughput 0.461887
Reading from 6745: heap size 463 MB, throughput 0.64064
Reading from 6745: heap size 464 MB, throughput 0.688874
Reading from 6745: heap size 465 MB, throughput 0.610922
Reading from 6745: heap size 457 MB, throughput 0.635741
Reading from 6745: heap size 410 MB, throughput 0.583046
Reading from 6745: heap size 456 MB, throughput 0.616028
Reading from 6745: heap size 458 MB, throughput 0.579113
Reading from 6745: heap size 460 MB, throughput 0.620343
Reading from 6745: heap size 460 MB, throughput 0.589128
Reading from 6745: heap size 463 MB, throughput 0.607545
Reading from 6745: heap size 465 MB, throughput 0.588646
Reading from 6745: heap size 468 MB, throughput 0.563385
Reading from 6745: heap size 473 MB, throughput 0.543268
Reading from 6745: heap size 476 MB, throughput 0.50257
Reading from 6745: heap size 485 MB, throughput 0.090003
Reading from 6745: heap size 546 MB, throughput 0.350694
Reading from 6745: heap size 555 MB, throughput 0.550178
Reading from 6745: heap size 560 MB, throughput 0.575289
Reading from 6745: heap size 563 MB, throughput 0.466504
Reading from 6745: heap size 567 MB, throughput 0.0831371
Reading from 6745: heap size 635 MB, throughput 0.43844
Reading from 6745: heap size 628 MB, throughput 0.585013
Reading from 6745: heap size 575 MB, throughput 0.584384
Reading from 6745: heap size 629 MB, throughput 0.555033
Reading from 6745: heap size 630 MB, throughput 0.580851
Reading from 6745: heap size 631 MB, throughput 0.797344
Reading from 6745: heap size 632 MB, throughput 0.872798
Reading from 6745: heap size 629 MB, throughput 0.243382
Reading from 6745: heap size 710 MB, throughput 0.841209
Reading from 6745: heap size 717 MB, throughput 0.651263
Reading from 6745: heap size 724 MB, throughput 0.507024
Reading from 6745: heap size 730 MB, throughput 0.714636
Reading from 6745: heap size 732 MB, throughput 0.422298
Reading from 6745: heap size 732 MB, throughput 0.464336
Reading from 6745: heap size 734 MB, throughput 0.470641
Reading from 6745: heap size 728 MB, throughput 0.519255
Reading from 6745: heap size 732 MB, throughput 0.195633
Reading from 6745: heap size 812 MB, throughput 0.483042
Reading from 6745: heap size 813 MB, throughput 0.680498
Reading from 6745: heap size 819 MB, throughput 0.692116
Reading from 6745: heap size 819 MB, throughput 0.885594
Reading from 6745: heap size 826 MB, throughput 0.835462
Reading from 6745: heap size 826 MB, throughput 0.894576
Reading from 6745: heap size 831 MB, throughput 0.0814132
Reading from 6745: heap size 926 MB, throughput 0.838689
Reading from 6745: heap size 935 MB, throughput 0.945645
Reading from 6745: heap size 936 MB, throughput 0.936318
Reading from 6745: heap size 923 MB, throughput 0.852959
Reading from 6745: heap size 832 MB, throughput 0.851544
Reading from 6745: heap size 913 MB, throughput 0.859008
Reading from 6745: heap size 837 MB, throughput 0.859774
Reading from 6745: heap size 907 MB, throughput 0.858641
Reading from 6745: heap size 913 MB, throughput 0.873598
Reading from 6745: heap size 904 MB, throughput 0.879174
Reading from 6745: heap size 908 MB, throughput 0.879439
Reading from 6745: heap size 901 MB, throughput 0.884844
Reading from 6745: heap size 905 MB, throughput 0.883361
Reading from 6745: heap size 898 MB, throughput 0.979471
Reading from 6745: heap size 902 MB, throughput 0.973648
Reading from 6745: heap size 906 MB, throughput 0.808218
Reading from 6745: heap size 909 MB, throughput 0.815868
Reading from 6745: heap size 915 MB, throughput 0.822491
Reading from 6745: heap size 915 MB, throughput 0.822213
Reading from 6745: heap size 920 MB, throughput 0.83043
Reading from 6745: heap size 921 MB, throughput 0.831001
Reading from 6745: heap size 924 MB, throughput 0.838906
Reading from 6745: heap size 926 MB, throughput 0.837183
Reading from 6745: heap size 929 MB, throughput 0.842932
Reading from 6745: heap size 931 MB, throughput 0.881985
Reading from 6745: heap size 933 MB, throughput 0.912969
Reading from 6745: heap size 935 MB, throughput 0.826529
Reading from 6745: heap size 941 MB, throughput 0.743858
Reading from 6745: heap size 941 MB, throughput 0.0751655
Reading from 6745: heap size 1076 MB, throughput 0.611003
Reading from 6745: heap size 1079 MB, throughput 0.815707
Reading from 6745: heap size 1081 MB, throughput 0.816412
Reading from 6745: heap size 1084 MB, throughput 0.789551
Reading from 6745: heap size 1086 MB, throughput 0.795963
Reading from 6745: heap size 1090 MB, throughput 0.774743
Reading from 6745: heap size 1097 MB, throughput 0.784677
Reading from 6745: heap size 1098 MB, throughput 0.757065
Reading from 6745: heap size 1113 MB, throughput 0.945994
Reading from 6745: heap size 1113 MB, throughput 0.978224
Reading from 6745: heap size 1126 MB, throughput 0.978789
Reading from 6745: heap size 1129 MB, throughput 0.976572
Reading from 6745: heap size 1139 MB, throughput 0.975942
Reading from 6745: heap size 1145 MB, throughput 0.971751
Reading from 6745: heap size 1159 MB, throughput 0.980528
Reading from 6745: heap size 1160 MB, throughput 0.978688
Reading from 6745: heap size 1156 MB, throughput 0.980662
Reading from 6745: heap size 1160 MB, throughput 0.980339
Reading from 6745: heap size 1153 MB, throughput 0.979612
Reading from 6745: heap size 1157 MB, throughput 0.979403
Reading from 6745: heap size 1158 MB, throughput 0.979032
Reading from 6745: heap size 1159 MB, throughput 0.979387
Reading from 6745: heap size 1162 MB, throughput 0.979345
Reading from 6745: heap size 1164 MB, throughput 0.979986
Reading from 6745: heap size 1166 MB, throughput 0.97892
Reading from 6745: heap size 1170 MB, throughput 0.698023
Reading from 6745: heap size 1248 MB, throughput 0.995785
Reading from 6745: heap size 1250 MB, throughput 0.994271
Reading from 6745: heap size 1258 MB, throughput 0.992669
Reading from 6745: heap size 1264 MB, throughput 0.991891
Reading from 6745: heap size 1266 MB, throughput 0.989911
Reading from 6745: heap size 1163 MB, throughput 0.989234
Reading from 6745: heap size 1258 MB, throughput 0.987966
Reading from 6745: heap size 1180 MB, throughput 0.98664
Reading from 6745: heap size 1246 MB, throughput 0.985902
Reading from 6745: heap size 1197 MB, throughput 0.986116
Reading from 6745: heap size 1250 MB, throughput 0.983845
Reading from 6745: heap size 1250 MB, throughput 0.982005
Reading from 6745: heap size 1251 MB, throughput 0.98312
Reading from 6745: heap size 1255 MB, throughput 0.979453
Reading from 6745: heap size 1257 MB, throughput 0.978192
Reading from 6745: heap size 1263 MB, throughput 0.976454
Reading from 6745: heap size 1269 MB, throughput 0.979348
Reading from 6745: heap size 1274 MB, throughput 0.977451
Reading from 6745: heap size 1280 MB, throughput 0.978345
Reading from 6745: heap size 1283 MB, throughput 0.976519
Reading from 6745: heap size 1287 MB, throughput 0.979069
Reading from 6745: heap size 1289 MB, throughput 0.978086
Reading from 6745: heap size 1293 MB, throughput 0.980115
Reading from 6745: heap size 1293 MB, throughput 0.978263
Reading from 6745: heap size 1297 MB, throughput 0.980294
Reading from 6745: heap size 1297 MB, throughput 0.979223
Reading from 6745: heap size 1298 MB, throughput 0.980522
Reading from 6745: heap size 1299 MB, throughput 0.978184
Reading from 6745: heap size 1299 MB, throughput 0.979551
Reading from 6745: heap size 1300 MB, throughput 0.966792
Reading from 6745: heap size 1326 MB, throughput 0.979989
Reading from 6745: heap size 1337 MB, throughput 0.988281
Reading from 6745: heap size 1333 MB, throughput 0.881863
Reading from 6745: heap size 1338 MB, throughput 0.773058
Reading from 6745: heap size 1353 MB, throughput 0.795743
Reading from 6745: heap size 1363 MB, throughput 0.782497
Reading from 6745: heap size 1386 MB, throughput 0.805943
Reading from 6745: heap size 1393 MB, throughput 0.790449
Reading from 6745: heap size 1420 MB, throughput 0.876542
Reading from 6745: heap size 1423 MB, throughput 0.987552
Reading from 6745: heap size 1443 MB, throughput 0.988977
Reading from 6745: heap size 1448 MB, throughput 0.988063
Reading from 6745: heap size 1451 MB, throughput 0.987857
Reading from 6745: heap size 1459 MB, throughput 0.987247
Reading from 6745: heap size 1451 MB, throughput 0.98764
Reading from 6745: heap size 1461 MB, throughput 0.986539
Reading from 6745: heap size 1464 MB, throughput 0.989504
Reading from 6745: heap size 1465 MB, throughput 0.988366
Reading from 6745: heap size 1462 MB, throughput 0.988012
Reading from 6745: heap size 1468 MB, throughput 0.98731
Reading from 6745: heap size 1457 MB, throughput 0.985889
Reading from 6745: heap size 1463 MB, throughput 0.985251
Reading from 6745: heap size 1466 MB, throughput 0.985164
Reading from 6745: heap size 1468 MB, throughput 0.982931
Reading from 6745: heap size 1472 MB, throughput 0.983081
Reading from 6745: heap size 1480 MB, throughput 0.981728
Reading from 6745: heap size 1487 MB, throughput 0.981961
Reading from 6745: heap size 1493 MB, throughput 0.981958
Reading from 6745: heap size 1500 MB, throughput 0.98118
Reading from 6745: heap size 1505 MB, throughput 0.836467
Reading from 6745: heap size 1630 MB, throughput 0.996087
Reading from 6745: heap size 1633 MB, throughput 0.99516
Reading from 6745: heap size 1647 MB, throughput 0.99372
Reading from 6745: heap size 1654 MB, throughput 0.992634
Reading from 6745: heap size 1654 MB, throughput 0.991728
Reading from 6745: heap size 1491 MB, throughput 0.990308
Reading from 6745: heap size 1640 MB, throughput 0.989697
Reading from 6745: heap size 1518 MB, throughput 0.989655
Reading from 6745: heap size 1622 MB, throughput 0.987726
Reading from 6745: heap size 1633 MB, throughput 0.989202
Reading from 6745: heap size 1593 MB, throughput 0.992818
Reading from 6745: heap size 1634 MB, throughput 0.889273
Reading from 6745: heap size 1646 MB, throughput 0.774948
Reading from 6745: heap size 1667 MB, throughput 0.687601
Reading from 6745: heap size 1708 MB, throughput 0.722893
Reading from 6745: heap size 1739 MB, throughput 0.716062
Reading from 6745: heap size 1791 MB, throughput 0.796573
Client 6745 died
Clients: 0
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
