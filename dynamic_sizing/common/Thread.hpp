#ifndef THREAD_HPP
#define THREAD_HPP

#include <pthread.h>
#include "Mutex.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
    //! Simple thread class.
    class Thread
    {
    public:
        //! Spawns the new thread and returns immediately.
        /*! Returns true if the thread is now running (even if it was running already). */
        bool start();

        //! Spawns the new thread and waits for it to complete.
        /*! If it is already running, just waits. Returns false if the thread failed to start. */
        bool startAndWait();

        bool isAlive();
        void kill();
        void wait();

        static const int THREAD_INTERRUPT_SIG;

    protected:
        Thread();
        virtual ~Thread();

        //! Implement this for the thread's behaviour.
        virtual void threadFunc() = 0;

        void condWaitOnLock(pthread_cond_t* cond) EXCLUSIVE_LOCKS_REQUIRED(m_Lock);

        //! Any extra work needed by subclasses when the thread is killed should be done here.
        virtual void onKill() EXCLUSIVE_LOCKS_REQUIRED(m_Lock) {}

        //! Send a signal to self; use to interrupt syscalls if needed in onKill.
        /*! Should only be used in programs where the SignalHandler has been set up. */
        void signal();

        economem::Mutex m_Lock;

    private:
        pthread_t m_Thread;
        bool m_Alive GUARDED_BY(m_Lock);

        static void* pthreadStub(void* arg);
    };
}

/*! @} */

#endif
