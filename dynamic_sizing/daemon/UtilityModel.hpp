#ifndef UTILITYMODEL_HPP
#define UTILITYMODEL_HPP

#include <cstddef>
#include <functional>
#include <string>
#include <utility>
#include <vector>

// Abstract base class for all utility models

class ReadingIterator;
class UtilityFunction;


class PartitionResult
{
public:
    PartitionResult(size_t numClients) : heapSizes(std::vector<double>(numClients, 0.0f)) {}
    PartitionResult(const std::vector<double>& heapSizes, double utility) : heapSizes(heapSizes), utility(utility) {}
    std::vector<double> heapSizes;
    double utility;

    void print(const std::vector<const UtilityFunction*>& functions) const;
};

class UtilityModel
{
public:

    UtilityModel() {}
    virtual ~UtilityModel() {}

    virtual UtilityFunction* calculateFuncFromData(ReadingIterator& it) const = 0;

    virtual PartitionResult* partitionMemory(unsigned long long totalMem, const std::vector<const UtilityFunction*>& functions, const std::vector<unsigned long long>& minHeaps) const = 0;


    // Static API; create a particular utility model, and die if it is unknown
    static const std::string DEFAULT_UTILITY_MODEL;
    static UtilityModel* fromName(const std::string& modelName, bool alwaysNumericOptimisation);
    static std::vector<std::string> allModelNames();

protected:
    virtual double combineUtilities(const std::vector<const UtilityFunction*>& functions, const std::vector<double>& heapSizes) const;
    virtual double combineRawUtilities(const std::vector<double>& utilities) const = 0;

    virtual PartitionResult* partitionMemoryNumeric(unsigned long long totalMem, const std::vector<const UtilityFunction*>& functions, const std::vector<unsigned long long>& minHeaps) const;

    // Generic curve-fitting using simple linear least-squares regression. The iterator
    // is used to get data, and the filters are applied to each element before use (i.e.
    // so you can do a log or exponential transform without having to copy lists).
    // Returns <a, b> such that y = a + bx
    static double id(double d) { return d; }
    static std::pair<double, double> fitStraightLine(ReadingIterator& it,
                                                     std::function<double(double)> xfilter=id,
                                                     std::function<double(double)> yfilter=id);
};

#endif
