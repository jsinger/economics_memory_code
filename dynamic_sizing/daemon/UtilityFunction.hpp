#ifndef UTILITYFUNCTION_HPP
#define UTILITYFUNCTION_HPP

#include <string>

class UtilityFunction
{
public:
    UtilityFunction() {}
    virtual ~UtilityFunction() {}

    virtual double evaluate(double heapSize) const = 0;
    virtual bool isValid() const = 0;
    virtual void forceValidity() = 0;
    virtual std::string toString() const = 0;
    virtual std::string toPython() const = 0;
};

#endif
