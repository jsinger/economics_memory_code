economemd
    total memory: 4892 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub16_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 29701: heap size 9 MB, throughput 0.989028
Clients: 1
Client 29701 has a minimum heap size of 12 MB
Reading from 29700: heap size 9 MB, throughput 0.991212
Clients: 2
Client 29700 has a minimum heap size of 1211 MB
Reading from 29700: heap size 9 MB, throughput 0.974449
Reading from 29700: heap size 9 MB, throughput 0.956508
Reading from 29700: heap size 9 MB, throughput 0.956054
Reading from 29701: heap size 9 MB, throughput 0.99207
Reading from 29701: heap size 9 MB, throughput 0.962505
Reading from 29701: heap size 9 MB, throughput 0.979927
Reading from 29700: heap size 11 MB, throughput 0.986079
Reading from 29701: heap size 11 MB, throughput 0.986381
Reading from 29700: heap size 11 MB, throughput 0.98357
Reading from 29701: heap size 11 MB, throughput 0.98369
Reading from 29700: heap size 17 MB, throughput 0.939015
Reading from 29701: heap size 16 MB, throughput 0.991921
Reading from 29700: heap size 17 MB, throughput 0.537936
Reading from 29700: heap size 30 MB, throughput 0.96621
Reading from 29700: heap size 31 MB, throughput 0.879533
Reading from 29701: heap size 16 MB, throughput 0.96409
Reading from 29700: heap size 34 MB, throughput 0.252853
Reading from 29700: heap size 46 MB, throughput 0.849392
Reading from 29701: heap size 24 MB, throughput 0.930145
Reading from 29700: heap size 46 MB, throughput 0.359808
Reading from 29700: heap size 63 MB, throughput 0.798319
Reading from 29700: heap size 68 MB, throughput 0.842096
Reading from 29701: heap size 30 MB, throughput 0.98757
Reading from 29700: heap size 68 MB, throughput 0.230427
Reading from 29700: heap size 94 MB, throughput 0.738534
Reading from 29701: heap size 35 MB, throughput 0.977247
Reading from 29701: heap size 40 MB, throughput 0.984389
Reading from 29700: heap size 95 MB, throughput 0.194519
Reading from 29700: heap size 125 MB, throughput 0.856013
Reading from 29700: heap size 125 MB, throughput 0.748966
Reading from 29701: heap size 43 MB, throughput 0.988722
Reading from 29700: heap size 125 MB, throughput 0.893026
Reading from 29701: heap size 42 MB, throughput 0.840862
Reading from 29700: heap size 128 MB, throughput 0.804142
Reading from 29701: heap size 49 MB, throughput 0.986293
Reading from 29701: heap size 49 MB, throughput 0.992709
Reading from 29700: heap size 129 MB, throughput 0.143255
Reading from 29700: heap size 167 MB, throughput 0.654475
Reading from 29701: heap size 60 MB, throughput 0.990521
Reading from 29700: heap size 170 MB, throughput 0.72646
Reading from 29701: heap size 62 MB, throughput 0.983024
Reading from 29700: heap size 172 MB, throughput 0.71313
Reading from 29700: heap size 172 MB, throughput 0.678057
Reading from 29701: heap size 72 MB, throughput 0.73791
Reading from 29700: heap size 178 MB, throughput 0.722414
Reading from 29700: heap size 180 MB, throughput 0.701796
Reading from 29701: heap size 76 MB, throughput 0.992638
Reading from 29700: heap size 185 MB, throughput 0.633534
Reading from 29700: heap size 188 MB, throughput 0.577663
Reading from 29700: heap size 196 MB, throughput 0.463174
Reading from 29700: heap size 200 MB, throughput 0.53641
Reading from 29700: heap size 208 MB, throughput 0.452841
Reading from 29700: heap size 213 MB, throughput 0.378362
Reading from 29701: heap size 86 MB, throughput 0.992073
Reading from 29700: heap size 220 MB, throughput 0.0654416
Reading from 29700: heap size 272 MB, throughput 0.451844
Reading from 29700: heap size 221 MB, throughput 0.463526
Reading from 29701: heap size 87 MB, throughput 0.996845
Reading from 29700: heap size 263 MB, throughput 0.112419
Reading from 29700: heap size 315 MB, throughput 0.602319
Reading from 29700: heap size 312 MB, throughput 0.626532
Reading from 29700: heap size 314 MB, throughput 0.714076
Reading from 29700: heap size 308 MB, throughput 0.61662
Reading from 29700: heap size 311 MB, throughput 0.662503
Reading from 29701: heap size 96 MB, throughput 0.993299
Reading from 29700: heap size 312 MB, throughput 0.705727
Reading from 29700: heap size 312 MB, throughput 0.559468
Reading from 29700: heap size 313 MB, throughput 0.591088
Reading from 29700: heap size 319 MB, throughput 0.543751
Reading from 29700: heap size 320 MB, throughput 0.558848
Reading from 29700: heap size 329 MB, throughput 0.51569
Reading from 29700: heap size 332 MB, throughput 0.506409
Reading from 29700: heap size 338 MB, throughput 0.503363
Reading from 29701: heap size 98 MB, throughput 0.997503
Reading from 29700: heap size 342 MB, throughput 0.491205
Reading from 29700: heap size 347 MB, throughput 0.492544
Reading from 29700: heap size 349 MB, throughput 0.358908
Reading from 29701: heap size 106 MB, throughput 0.997408
Reading from 29700: heap size 354 MB, throughput 0.0408741
Reading from 29700: heap size 400 MB, throughput 0.413503
Reading from 29700: heap size 338 MB, throughput 0.5459
Reading from 29700: heap size 394 MB, throughput 0.428521
Reading from 29701: heap size 107 MB, throughput 0.993834
Reading from 29700: heap size 334 MB, throughput 0.0653184
Reading from 29700: heap size 450 MB, throughput 0.401397
Reading from 29700: heap size 452 MB, throughput 0.624772
Reading from 29700: heap size 451 MB, throughput 0.655621
Reading from 29700: heap size 389 MB, throughput 0.581306
Reading from 29701: heap size 114 MB, throughput 0.997493
Reading from 29700: heap size 444 MB, throughput 0.538062
Reading from 29700: heap size 404 MB, throughput 0.555919
Equal recommendation: 2446 MB each
Reading from 29700: heap size 447 MB, throughput 0.505574
Reading from 29700: heap size 447 MB, throughput 0.457969
Reading from 29700: heap size 451 MB, throughput 0.564206
Reading from 29700: heap size 452 MB, throughput 0.57976
Reading from 29701: heap size 114 MB, throughput 0.995839
Reading from 29700: heap size 454 MB, throughput 0.526667
Reading from 29700: heap size 458 MB, throughput 0.549278
Reading from 29700: heap size 462 MB, throughput 0.397962
Reading from 29700: heap size 467 MB, throughput 0.514234
Reading from 29700: heap size 471 MB, throughput 0.464685
Reading from 29701: heap size 121 MB, throughput 0.997426
Reading from 29700: heap size 478 MB, throughput 0.0839556
Reading from 29701: heap size 121 MB, throughput 0.996814
Reading from 29700: heap size 539 MB, throughput 0.395576
Reading from 29700: heap size 543 MB, throughput 0.4927
Reading from 29700: heap size 545 MB, throughput 0.436863
Reading from 29701: heap size 125 MB, throughput 0.997474
Reading from 29700: heap size 545 MB, throughput 0.0784684
Reading from 29700: heap size 614 MB, throughput 0.484918
Reading from 29700: heap size 615 MB, throughput 0.63797
Reading from 29701: heap size 126 MB, throughput 0.997497
Reading from 29700: heap size 608 MB, throughput 0.552903
Reading from 29700: heap size 612 MB, throughput 0.599698
Reading from 29700: heap size 613 MB, throughput 0.616572
Reading from 29700: heap size 615 MB, throughput 0.544574
Reading from 29700: heap size 620 MB, throughput 0.538414
Reading from 29701: heap size 131 MB, throughput 0.997137
Reading from 29700: heap size 626 MB, throughput 0.842002
Reading from 29701: heap size 131 MB, throughput 0.997382
Reading from 29700: heap size 628 MB, throughput 0.859638
Reading from 29700: heap size 636 MB, throughput 0.83376
Reading from 29701: heap size 134 MB, throughput 0.99707
Reading from 29701: heap size 134 MB, throughput 0.996438
Reading from 29701: heap size 138 MB, throughput 0.997859
Reading from 29700: heap size 638 MB, throughput 0.198668
Reading from 29700: heap size 721 MB, throughput 0.379241
Reading from 29700: heap size 732 MB, throughput 0.41898
Reading from 29700: heap size 736 MB, throughput 0.635954
Reading from 29700: heap size 738 MB, throughput 0.402955
Reading from 29700: heap size 739 MB, throughput 0.354483
Reading from 29700: heap size 730 MB, throughput 0.395831
Reading from 29701: heap size 138 MB, throughput 0.997906
Reading from 29700: heap size 666 MB, throughput 0.0454245
Equal recommendation: 2446 MB each
Reading from 29700: heap size 809 MB, throughput 0.334048
Reading from 29701: heap size 141 MB, throughput 0.997561
Reading from 29700: heap size 811 MB, throughput 0.860348
Reading from 29700: heap size 818 MB, throughput 0.845729
Reading from 29700: heap size 819 MB, throughput 0.633056
Reading from 29700: heap size 815 MB, throughput 0.638309
Reading from 29701: heap size 142 MB, throughput 0.997843
Reading from 29701: heap size 145 MB, throughput 0.997847
Reading from 29700: heap size 818 MB, throughput 0.0527568
Reading from 29700: heap size 913 MB, throughput 0.669773
Reading from 29700: heap size 914 MB, throughput 0.853331
Reading from 29700: heap size 921 MB, throughput 0.942691
Reading from 29701: heap size 145 MB, throughput 0.998565
Reading from 29700: heap size 924 MB, throughput 0.820214
Reading from 29700: heap size 925 MB, throughput 0.944343
Reading from 29700: heap size 927 MB, throughput 0.883883
Reading from 29700: heap size 918 MB, throughput 0.910068
Reading from 29701: heap size 148 MB, throughput 0.996984
Reading from 29700: heap size 778 MB, throughput 0.858977
Reading from 29700: heap size 902 MB, throughput 0.604412
Reading from 29700: heap size 813 MB, throughput 0.557327
Reading from 29701: heap size 148 MB, throughput 0.991119
Reading from 29700: heap size 895 MB, throughput 0.685874
Reading from 29700: heap size 819 MB, throughput 0.715471
Reading from 29701: heap size 150 MB, throughput 0.989582
Reading from 29700: heap size 886 MB, throughput 0.707551
Reading from 29700: heap size 893 MB, throughput 0.62841
Reading from 29700: heap size 881 MB, throughput 0.740317
Reading from 29700: heap size 887 MB, throughput 0.774903
Reading from 29700: heap size 881 MB, throughput 0.820759
Reading from 29701: heap size 151 MB, throughput 0.996743
Reading from 29700: heap size 885 MB, throughput 0.83255
Reading from 29700: heap size 882 MB, throughput 0.823862
Reading from 29701: heap size 158 MB, throughput 0.99778
Reading from 29700: heap size 885 MB, throughput 0.983382
Reading from 29701: heap size 158 MB, throughput 0.997317
Reading from 29700: heap size 881 MB, throughput 0.921302
Reading from 29700: heap size 885 MB, throughput 0.808687
Reading from 29700: heap size 892 MB, throughput 0.808142
Reading from 29700: heap size 894 MB, throughput 0.811516
Reading from 29700: heap size 902 MB, throughput 0.818199
Reading from 29701: heap size 164 MB, throughput 0.997634
Reading from 29700: heap size 903 MB, throughput 0.812784
Reading from 29700: heap size 911 MB, throughput 0.832362
Reading from 29700: heap size 911 MB, throughput 0.826651
Reading from 29700: heap size 918 MB, throughput 0.913922
Reading from 29701: heap size 164 MB, throughput 0.996521
Reading from 29700: heap size 919 MB, throughput 0.905419
Equal recommendation: 2446 MB each
Reading from 29700: heap size 921 MB, throughput 0.919293
Reading from 29700: heap size 924 MB, throughput 0.821712
Reading from 29701: heap size 168 MB, throughput 0.995603
Reading from 29701: heap size 169 MB, throughput 0.995153
Reading from 29700: heap size 922 MB, throughput 0.118907
Reading from 29700: heap size 1027 MB, throughput 0.530087
Reading from 29700: heap size 1059 MB, throughput 0.768614
Reading from 29700: heap size 1059 MB, throughput 0.75978
Reading from 29701: heap size 174 MB, throughput 0.986459
Reading from 29700: heap size 1065 MB, throughput 0.779626
Reading from 29700: heap size 1067 MB, throughput 0.759446
Reading from 29700: heap size 1070 MB, throughput 0.756139
Reading from 29700: heap size 1073 MB, throughput 0.744566
Reading from 29700: heap size 1076 MB, throughput 0.698796
Reading from 29700: heap size 1080 MB, throughput 0.756769
Reading from 29701: heap size 174 MB, throughput 0.997085
Reading from 29701: heap size 181 MB, throughput 0.997425
Reading from 29700: heap size 1092 MB, throughput 0.941068
Reading from 29701: heap size 182 MB, throughput 0.996767
Reading from 29700: heap size 1092 MB, throughput 0.95984
Reading from 29701: heap size 187 MB, throughput 0.997088
Reading from 29701: heap size 188 MB, throughput 0.993995
Reading from 29700: heap size 1104 MB, throughput 0.956805
Equal recommendation: 2446 MB each
Reading from 29701: heap size 194 MB, throughput 0.997999
Reading from 29700: heap size 1105 MB, throughput 0.953717
Reading from 29701: heap size 194 MB, throughput 0.998181
Reading from 29701: heap size 198 MB, throughput 0.997453
Reading from 29700: heap size 1116 MB, throughput 0.922253
Reading from 29701: heap size 199 MB, throughput 0.992556
Reading from 29701: heap size 203 MB, throughput 0.997513
Reading from 29701: heap size 204 MB, throughput 0.996303
Reading from 29700: heap size 1121 MB, throughput 0.672896
Reading from 29701: heap size 210 MB, throughput 0.998159
Reading from 29700: heap size 1195 MB, throughput 0.990497
Reading from 29701: heap size 210 MB, throughput 0.997785
Reading from 29700: heap size 1200 MB, throughput 0.99006
Reading from 29701: heap size 214 MB, throughput 0.998203
Equal recommendation: 2446 MB each
Reading from 29701: heap size 214 MB, throughput 0.997811
Reading from 29700: heap size 1213 MB, throughput 0.987188
Reading from 29701: heap size 217 MB, throughput 0.998054
Reading from 29700: heap size 1216 MB, throughput 0.981426
Reading from 29701: heap size 208 MB, throughput 0.997573
Reading from 29701: heap size 198 MB, throughput 0.997922
Reading from 29700: heap size 1217 MB, throughput 0.982825
Reading from 29701: heap size 190 MB, throughput 0.997613
Reading from 29701: heap size 182 MB, throughput 0.997466
Reading from 29700: heap size 1220 MB, throughput 0.980321
Reading from 29701: heap size 174 MB, throughput 0.997533
Reading from 29701: heap size 167 MB, throughput 0.997261
Reading from 29700: heap size 1211 MB, throughput 0.977078
Reading from 29701: heap size 160 MB, throughput 0.994538
Reading from 29701: heap size 153 MB, throughput 0.989803
Equal recommendation: 2446 MB each
Reading from 29701: heap size 160 MB, throughput 0.854402
Reading from 29700: heap size 1159 MB, throughput 0.976047
Reading from 29701: heap size 168 MB, throughput 0.998794
Reading from 29701: heap size 174 MB, throughput 0.998709
Reading from 29700: heap size 1211 MB, throughput 0.97514
Reading from 29701: heap size 180 MB, throughput 0.999002
Reading from 29701: heap size 190 MB, throughput 0.998873
Reading from 29700: heap size 1213 MB, throughput 0.97212
Reading from 29701: heap size 191 MB, throughput 0.998906
Reading from 29701: heap size 199 MB, throughput 0.998975
Reading from 29700: heap size 1216 MB, throughput 0.969714
Reading from 29701: heap size 201 MB, throughput 0.998981
Reading from 29700: heap size 1219 MB, throughput 0.960067
Reading from 29701: heap size 201 MB, throughput 0.998983
Equal recommendation: 2446 MB each
Reading from 29701: heap size 204 MB, throughput 0.998899
Reading from 29700: heap size 1222 MB, throughput 0.966713
Reading from 29701: heap size 204 MB, throughput 0.998822
Reading from 29701: heap size 207 MB, throughput 0.998935
Reading from 29700: heap size 1228 MB, throughput 0.963819
Reading from 29701: heap size 207 MB, throughput 0.998781
Reading from 29701: heap size 210 MB, throughput 0.998901
Reading from 29700: heap size 1233 MB, throughput 0.962794
Reading from 29701: heap size 210 MB, throughput 0.997078
Reading from 29701: heap size 212 MB, throughput 0.991452
Reading from 29700: heap size 1242 MB, throughput 0.96261
Reading from 29701: heap size 213 MB, throughput 0.997439
Reading from 29701: heap size 221 MB, throughput 0.997922
Equal recommendation: 2446 MB each
Reading from 29700: heap size 1249 MB, throughput 0.958015
Reading from 29701: heap size 221 MB, throughput 0.997878
Reading from 29700: heap size 1257 MB, throughput 0.957281
Reading from 29701: heap size 227 MB, throughput 0.998162
Reading from 29701: heap size 228 MB, throughput 0.997895
Reading from 29700: heap size 1264 MB, throughput 0.959492
Reading from 29701: heap size 233 MB, throughput 0.998345
Reading from 29700: heap size 1268 MB, throughput 0.95845
Reading from 29701: heap size 234 MB, throughput 0.99779
Reading from 29701: heap size 240 MB, throughput 0.998191
Reading from 29700: heap size 1275 MB, throughput 0.962107
Reading from 29701: heap size 240 MB, throughput 0.997788
Equal recommendation: 2446 MB each
Reading from 29701: heap size 245 MB, throughput 0.99799
Reading from 29700: heap size 1278 MB, throughput 0.959967
Reading from 29701: heap size 245 MB, throughput 0.997861
Reading from 29701: heap size 251 MB, throughput 0.996108
Reading from 29700: heap size 1285 MB, throughput 0.940523
Reading from 29701: heap size 251 MB, throughput 0.992186
Reading from 29701: heap size 258 MB, throughput 0.997972
Reading from 29700: heap size 1286 MB, throughput 0.961995
Reading from 29701: heap size 258 MB, throughput 0.997938
Reading from 29700: heap size 1293 MB, throughput 0.954356
Reading from 29701: heap size 265 MB, throughput 0.998371
Equal recommendation: 2446 MB each
Reading from 29701: heap size 266 MB, throughput 0.998174
Reading from 29700: heap size 1293 MB, throughput 0.959423
Reading from 29701: heap size 272 MB, throughput 0.998333
Reading from 29700: heap size 1299 MB, throughput 0.961973
Reading from 29701: heap size 273 MB, throughput 0.998063
Reading from 29701: heap size 279 MB, throughput 0.998422
Reading from 29700: heap size 1300 MB, throughput 0.964287
Reading from 29701: heap size 280 MB, throughput 0.998129
Reading from 29700: heap size 1304 MB, throughput 0.964838
Reading from 29701: heap size 286 MB, throughput 0.998379
Equal recommendation: 2446 MB each
Reading from 29701: heap size 286 MB, throughput 0.996192
Reading from 29700: heap size 1305 MB, throughput 0.964132
Reading from 29701: heap size 291 MB, throughput 0.995775
Reading from 29701: heap size 292 MB, throughput 0.998025
Reading from 29701: heap size 300 MB, throughput 0.997939
Reading from 29700: heap size 1309 MB, throughput 0.563056
Reading from 29701: heap size 301 MB, throughput 0.998016
Reading from 29700: heap size 1434 MB, throughput 0.955855
Reading from 29701: heap size 308 MB, throughput 0.998022
Equal recommendation: 2446 MB each
Reading from 29701: heap size 308 MB, throughput 0.998162
Reading from 29700: heap size 1432 MB, throughput 0.985053
Reading from 29701: heap size 315 MB, throughput 0.998445
Reading from 29700: heap size 1441 MB, throughput 0.985128
Reading from 29701: heap size 316 MB, throughput 0.998264
Reading from 29700: heap size 1453 MB, throughput 0.982601
Reading from 29701: heap size 322 MB, throughput 0.998629
Reading from 29701: heap size 322 MB, throughput 0.99146
Reading from 29700: heap size 1454 MB, throughput 0.980511
Reading from 29701: heap size 327 MB, throughput 0.99827
Equal recommendation: 2446 MB each
Reading from 29701: heap size 329 MB, throughput 0.998073
Reading from 29700: heap size 1449 MB, throughput 0.97937
Reading from 29701: heap size 338 MB, throughput 0.998256
Reading from 29701: heap size 338 MB, throughput 0.998009
Reading from 29701: heap size 346 MB, throughput 0.997384
Reading from 29701: heap size 347 MB, throughput 0.998091
Equal recommendation: 2446 MB each
Reading from 29701: heap size 355 MB, throughput 0.998484
Reading from 29701: heap size 355 MB, throughput 0.996634
Reading from 29701: heap size 363 MB, throughput 0.997067
Reading from 29700: heap size 1453 MB, throughput 0.989528
Reading from 29701: heap size 364 MB, throughput 0.998208
Reading from 29701: heap size 373 MB, throughput 0.998343
Equal recommendation: 2446 MB each
Reading from 29701: heap size 373 MB, throughput 0.998114
Reading from 29701: heap size 382 MB, throughput 0.998435
Reading from 29701: heap size 382 MB, throughput 0.998257
Reading from 29700: heap size 1426 MB, throughput 0.991203
Reading from 29701: heap size 390 MB, throughput 0.998464
Reading from 29701: heap size 390 MB, throughput 0.994455
Equal recommendation: 2446 MB each
Reading from 29701: heap size 397 MB, throughput 0.998533
Reading from 29700: heap size 1453 MB, throughput 0.983455
Reading from 29700: heap size 1461 MB, throughput 0.853234
Reading from 29700: heap size 1482 MB, throughput 0.718271
Reading from 29700: heap size 1478 MB, throughput 0.660355
Reading from 29700: heap size 1518 MB, throughput 0.64516
Reading from 29701: heap size 398 MB, throughput 0.994532
Reading from 29700: heap size 1547 MB, throughput 0.636369
Reading from 29700: heap size 1572 MB, throughput 0.642675
Reading from 29700: heap size 1604 MB, throughput 0.678187
Reading from 29700: heap size 1618 MB, throughput 0.752795
Reading from 29701: heap size 409 MB, throughput 0.998517
Reading from 29700: heap size 1619 MB, throughput 0.961992
Equal recommendation: 2446 MB each
Reading from 29701: heap size 409 MB, throughput 0.998107
Reading from 29700: heap size 1636 MB, throughput 0.749278
Reading from 29701: heap size 419 MB, throughput 0.998238
Reading from 29700: heap size 1575 MB, throughput 0.993032
Reading from 29701: heap size 420 MB, throughput 0.998311
Reading from 29700: heap size 1584 MB, throughput 0.98306
Reading from 29701: heap size 430 MB, throughput 0.931831
Reading from 29700: heap size 1614 MB, throughput 0.988591
Reading from 29701: heap size 439 MB, throughput 0.999176
Equal recommendation: 2446 MB each
Reading from 29700: heap size 1617 MB, throughput 0.986811
Reading from 29701: heap size 453 MB, throughput 0.999154
Reading from 29700: heap size 1610 MB, throughput 0.985363
Reading from 29701: heap size 453 MB, throughput 0.999174
Reading from 29700: heap size 1622 MB, throughput 0.980969
Reading from 29701: heap size 463 MB, throughput 0.999256
Equal recommendation: 2446 MB each
Reading from 29701: heap size 465 MB, throughput 0.999126
Reading from 29700: heap size 1594 MB, throughput 0.978081
Reading from 29701: heap size 473 MB, throughput 0.997849
Reading from 29700: heap size 1349 MB, throughput 0.978642
Reading from 29701: heap size 474 MB, throughput 0.998434
Reading from 29700: heap size 1573 MB, throughput 0.978302
Reading from 29701: heap size 486 MB, throughput 0.999035
Equal recommendation: 2446 MB each
Reading from 29700: heap size 1382 MB, throughput 0.975595
Reading from 29701: heap size 486 MB, throughput 0.998778
Reading from 29700: heap size 1548 MB, throughput 0.974792
Reading from 29701: heap size 496 MB, throughput 0.99889
Reading from 29700: heap size 1414 MB, throughput 0.970856
Reading from 29701: heap size 496 MB, throughput 0.998777
Reading from 29701: heap size 506 MB, throughput 0.996952
Equal recommendation: 2446 MB each
Reading from 29700: heap size 1551 MB, throughput 0.97233
Reading from 29701: heap size 506 MB, throughput 0.998289
Reading from 29700: heap size 1558 MB, throughput 0.970332
Reading from 29701: heap size 519 MB, throughput 0.998691
Reading from 29700: heap size 1567 MB, throughput 0.96693
Reading from 29701: heap size 519 MB, throughput 0.99849
Equal recommendation: 2446 MB each
Reading from 29700: heap size 1567 MB, throughput 0.964709
Reading from 29701: heap size 531 MB, throughput 0.998465
Reading from 29700: heap size 1580 MB, throughput 0.963493
Reading from 29701: heap size 531 MB, throughput 0.998401
Reading from 29701: heap size 543 MB, throughput 0.997003
Reading from 29700: heap size 1583 MB, throughput 0.967229
Equal recommendation: 2446 MB each
Reading from 29701: heap size 543 MB, throughput 0.99836
Reading from 29700: heap size 1597 MB, throughput 0.965765
Reading from 29701: heap size 557 MB, throughput 0.998294
Reading from 29700: heap size 1599 MB, throughput 0.966721
Reading from 29701: heap size 558 MB, throughput 0.998422
Equal recommendation: 2446 MB each
Reading from 29700: heap size 1614 MB, throughput 0.967317
Reading from 29701: heap size 572 MB, throughput 0.998436
Reading from 29701: heap size 572 MB, throughput 0.996745
Reading from 29700: heap size 1615 MB, throughput 0.968581
Reading from 29701: heap size 585 MB, throughput 0.997647
Reading from 29700: heap size 1631 MB, throughput 0.968642
Equal recommendation: 2446 MB each
Reading from 29701: heap size 586 MB, throughput 0.998388
Reading from 29700: heap size 1631 MB, throughput 0.967089
Reading from 29701: heap size 602 MB, throughput 0.998517
Reading from 29700: heap size 1647 MB, throughput 0.965005
Equal recommendation: 2446 MB each
Client 29701 died
Clients: 1
Reading from 29700: heap size 1647 MB, throughput 0.96921
Reading from 29700: heap size 1664 MB, throughput 0.968513
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 29700: heap size 1664 MB, throughput 0.995862
Recommendation: one client; give it all the memory
Reading from 29700: heap size 1657 MB, throughput 0.987113
Reading from 29700: heap size 1669 MB, throughput 0.827974
Reading from 29700: heap size 1662 MB, throughput 0.800401
Reading from 29700: heap size 1690 MB, throughput 0.788194
Reading from 29700: heap size 1729 MB, throughput 0.393415
Client 29700 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
