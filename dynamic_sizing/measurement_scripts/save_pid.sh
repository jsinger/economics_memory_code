#!/bin/bash
# Hoop-jumping to save the PID of a process being timed, because time itself
# doesn't (and this is easier than hacking time!)

printf "\tPID: %s\n" "$$" > "${1}"
exec "${@:2}"
