economemd
    total memory: 864 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub57_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 8182: heap size 9 MB, throughput 0.988026
Clients: 1
Client 8182 has a minimum heap size of 12 MB
Reading from 8181: heap size 9 MB, throughput 0.974387
Clients: 2
Client 8181 has a minimum heap size of 276 MB
Reading from 8182: heap size 9 MB, throughput 0.987852
Reading from 8181: heap size 9 MB, throughput 0.988293
Reading from 8182: heap size 11 MB, throughput 0.977001
Reading from 8181: heap size 11 MB, throughput 0.973938
Reading from 8182: heap size 11 MB, throughput 0.975207
Reading from 8181: heap size 11 MB, throughput 0.973021
Reading from 8182: heap size 15 MB, throughput 0.977464
Reading from 8181: heap size 15 MB, throughput 0.827389
Reading from 8181: heap size 18 MB, throughput 0.944051
Reading from 8182: heap size 15 MB, throughput 0.981471
Reading from 8181: heap size 23 MB, throughput 0.962323
Reading from 8181: heap size 27 MB, throughput 0.828973
Reading from 8181: heap size 27 MB, throughput 0.636513
Reading from 8182: heap size 24 MB, throughput 0.927752
Reading from 8181: heap size 36 MB, throughput 0.896615
Reading from 8181: heap size 40 MB, throughput 0.823339
Reading from 8181: heap size 41 MB, throughput 0.824935
Reading from 8182: heap size 30 MB, throughput 0.956942
Reading from 8181: heap size 45 MB, throughput 0.297312
Reading from 8181: heap size 58 MB, throughput 0.759221
Reading from 8181: heap size 63 MB, throughput 0.863441
Reading from 8182: heap size 38 MB, throughput 0.983858
Reading from 8181: heap size 65 MB, throughput 0.209802
Reading from 8181: heap size 85 MB, throughput 0.667358
Reading from 8181: heap size 89 MB, throughput 0.839844
Reading from 8182: heap size 45 MB, throughput 0.988018
Reading from 8181: heap size 91 MB, throughput 0.569566
Reading from 8182: heap size 46 MB, throughput 0.983964
Reading from 8182: heap size 46 MB, throughput 0.950305
Reading from 8181: heap size 94 MB, throughput 0.135152
Reading from 8182: heap size 54 MB, throughput 0.93623
Reading from 8181: heap size 120 MB, throughput 0.697648
Reading from 8181: heap size 123 MB, throughput 0.71748
Reading from 8181: heap size 126 MB, throughput 0.615831
Reading from 8182: heap size 54 MB, throughput 0.985714
Reading from 8181: heap size 130 MB, throughput 0.642684
Reading from 8182: heap size 64 MB, throughput 0.978126
Reading from 8182: heap size 66 MB, throughput 0.987642
Reading from 8182: heap size 79 MB, throughput 0.416094
Reading from 8181: heap size 135 MB, throughput 0.072799
Reading from 8182: heap size 84 MB, throughput 0.952416
Reading from 8181: heap size 167 MB, throughput 0.571176
Reading from 8181: heap size 174 MB, throughput 0.692514
Reading from 8181: heap size 175 MB, throughput 0.514781
Reading from 8182: heap size 97 MB, throughput 0.994735
Reading from 8181: heap size 178 MB, throughput 0.193072
Reading from 8181: heap size 213 MB, throughput 0.572148
Reading from 8181: heap size 220 MB, throughput 0.917189
Reading from 8182: heap size 97 MB, throughput 0.997659
Reading from 8181: heap size 221 MB, throughput 0.90791
Reading from 8181: heap size 223 MB, throughput 0.791588
Reading from 8181: heap size 229 MB, throughput 0.601006
Reading from 8181: heap size 233 MB, throughput 0.816494
Reading from 8181: heap size 237 MB, throughput 0.484575
Reading from 8181: heap size 242 MB, throughput 0.722888
Reading from 8182: heap size 107 MB, throughput 0.997775
Reading from 8181: heap size 245 MB, throughput 0.685435
Reading from 8181: heap size 249 MB, throughput 0.694137
Reading from 8181: heap size 251 MB, throughput 0.790971
Reading from 8182: heap size 109 MB, throughput 0.996599
Reading from 8181: heap size 252 MB, throughput 0.0834745
Reading from 8181: heap size 287 MB, throughput 0.585199
Reading from 8181: heap size 283 MB, throughput 0.707888
Reading from 8181: heap size 286 MB, throughput 0.76114
Reading from 8181: heap size 285 MB, throughput 0.759348
Reading from 8181: heap size 287 MB, throughput 0.143745
Reading from 8182: heap size 119 MB, throughput 0.996844
Reading from 8181: heap size 326 MB, throughput 0.919183
Reading from 8181: heap size 327 MB, throughput 0.809198
Reading from 8181: heap size 327 MB, throughput 0.837574
Reading from 8181: heap size 328 MB, throughput 0.787879
Reading from 8181: heap size 327 MB, throughput 0.871032
Reading from 8182: heap size 120 MB, throughput 0.997111
Reading from 8181: heap size 329 MB, throughput 0.86822
Reading from 8181: heap size 328 MB, throughput 0.887049
Reading from 8181: heap size 330 MB, throughput 0.713842
Reading from 8181: heap size 332 MB, throughput 0.68511
Reading from 8181: heap size 332 MB, throughput 0.572634
Reading from 8181: heap size 333 MB, throughput 0.66182
Equal recommendation: 432 MB each
Reading from 8181: heap size 334 MB, throughput 0.71914
Reading from 8181: heap size 337 MB, throughput 0.70157
Reading from 8182: heap size 128 MB, throughput 0.997592
Reading from 8181: heap size 338 MB, throughput 0.95166
Reading from 8182: heap size 129 MB, throughput 0.996501
Reading from 8182: heap size 133 MB, throughput 0.99728
Reading from 8181: heap size 343 MB, throughput 0.965335
Reading from 8182: heap size 134 MB, throughput 0.996897
Reading from 8181: heap size 344 MB, throughput 0.981829
Reading from 8182: heap size 139 MB, throughput 0.997025
Reading from 8182: heap size 139 MB, throughput 0.996999
Reading from 8181: heap size 347 MB, throughput 0.9803
Reading from 8182: heap size 143 MB, throughput 0.997569
Reading from 8181: heap size 348 MB, throughput 0.980969
Reading from 8182: heap size 143 MB, throughput 0.997232
Reading from 8182: heap size 146 MB, throughput 0.997205
Reading from 8181: heap size 347 MB, throughput 0.981295
Reading from 8182: heap size 147 MB, throughput 0.99757
Reading from 8181: heap size 350 MB, throughput 0.978188
Reading from 8182: heap size 150 MB, throughput 0.998165
Equal recommendation: 432 MB each
Reading from 8182: heap size 150 MB, throughput 0.998063
Reading from 8181: heap size 345 MB, throughput 0.976128
Reading from 8182: heap size 152 MB, throughput 0.998182
Reading from 8181: heap size 348 MB, throughput 0.978886
Reading from 8182: heap size 152 MB, throughput 0.997536
Reading from 8181: heap size 346 MB, throughput 0.982218
Reading from 8182: heap size 155 MB, throughput 0.998525
Reading from 8182: heap size 155 MB, throughput 0.99308
Reading from 8182: heap size 156 MB, throughput 0.992849
Reading from 8181: heap size 347 MB, throughput 0.964627
Reading from 8182: heap size 157 MB, throughput 0.995682
Reading from 8182: heap size 161 MB, throughput 0.995726
Reading from 8181: heap size 349 MB, throughput 0.69914
Reading from 8182: heap size 162 MB, throughput 0.997405
Reading from 8181: heap size 389 MB, throughput 0.975868
Reading from 8182: heap size 165 MB, throughput 0.996745
Reading from 8181: heap size 389 MB, throughput 0.988342
Reading from 8182: heap size 166 MB, throughput 0.996657
Equal recommendation: 432 MB each
Reading from 8181: heap size 391 MB, throughput 0.988894
Reading from 8182: heap size 170 MB, throughput 0.997183
Reading from 8182: heap size 170 MB, throughput 0.996848
Reading from 8181: heap size 392 MB, throughput 0.990328
Reading from 8182: heap size 173 MB, throughput 0.997876
Reading from 8181: heap size 369 MB, throughput 0.978035
Reading from 8181: heap size 387 MB, throughput 0.878411
Reading from 8181: heap size 390 MB, throughput 0.817529
Reading from 8181: heap size 394 MB, throughput 0.851117
Reading from 8181: heap size 398 MB, throughput 0.826626
Reading from 8182: heap size 173 MB, throughput 0.997471
Reading from 8181: heap size 405 MB, throughput 0.988039
Reading from 8182: heap size 176 MB, throughput 0.997185
Reading from 8182: heap size 176 MB, throughput 0.997149
Reading from 8181: heap size 407 MB, throughput 0.989777
Reading from 8182: heap size 178 MB, throughput 0.997722
Reading from 8181: heap size 407 MB, throughput 0.989361
Reading from 8182: heap size 179 MB, throughput 0.99614
Reading from 8181: heap size 409 MB, throughput 0.987487
Reading from 8182: heap size 182 MB, throughput 0.99754
Equal recommendation: 432 MB each
Reading from 8182: heap size 182 MB, throughput 0.997785
Reading from 8181: heap size 406 MB, throughput 0.988864
Reading from 8182: heap size 185 MB, throughput 0.997985
Reading from 8182: heap size 185 MB, throughput 0.990536
Reading from 8181: heap size 408 MB, throughput 0.981966
Reading from 8182: heap size 187 MB, throughput 0.993785
Reading from 8182: heap size 188 MB, throughput 0.997439
Reading from 8181: heap size 405 MB, throughput 0.985705
Reading from 8182: heap size 191 MB, throughput 0.998327
Reading from 8181: heap size 407 MB, throughput 0.983138
Reading from 8182: heap size 192 MB, throughput 0.998009
Reading from 8181: heap size 407 MB, throughput 0.983105
Reading from 8182: heap size 194 MB, throughput 0.997819
Reading from 8182: heap size 195 MB, throughput 0.997515
Reading from 8181: heap size 408 MB, throughput 0.986067
Equal recommendation: 432 MB each
Reading from 8182: heap size 199 MB, throughput 0.998093
Reading from 8181: heap size 407 MB, throughput 0.98627
Reading from 8182: heap size 199 MB, throughput 0.997704
Reading from 8181: heap size 408 MB, throughput 0.980219
Reading from 8182: heap size 201 MB, throughput 0.99802
Reading from 8182: heap size 201 MB, throughput 0.997861
Reading from 8181: heap size 409 MB, throughput 0.991771
Reading from 8181: heap size 410 MB, throughput 0.927343
Reading from 8181: heap size 407 MB, throughput 0.849503
Reading from 8181: heap size 411 MB, throughput 0.842742
Reading from 8181: heap size 417 MB, throughput 0.838967
Reading from 8182: heap size 204 MB, throughput 0.99814
Reading from 8181: heap size 418 MB, throughput 0.988894
Reading from 8182: heap size 204 MB, throughput 0.997687
Reading from 8182: heap size 206 MB, throughput 0.998283
Reading from 8181: heap size 425 MB, throughput 0.988471
Reading from 8182: heap size 207 MB, throughput 0.997991
Equal recommendation: 432 MB each
Reading from 8182: heap size 209 MB, throughput 0.885164
Reading from 8181: heap size 426 MB, throughput 0.986222
Reading from 8182: heap size 216 MB, throughput 0.99498
Reading from 8181: heap size 427 MB, throughput 0.989171
Reading from 8182: heap size 222 MB, throughput 0.998175
Reading from 8182: heap size 221 MB, throughput 0.99824
Reading from 8181: heap size 429 MB, throughput 0.989336
Reading from 8182: heap size 226 MB, throughput 0.998858
Reading from 8181: heap size 428 MB, throughput 0.990281
Reading from 8182: heap size 227 MB, throughput 0.998541
Reading from 8182: heap size 230 MB, throughput 0.998071
Reading from 8181: heap size 430 MB, throughput 0.988275
Reading from 8182: heap size 231 MB, throughput 0.998586
Equal recommendation: 432 MB each
Reading from 8181: heap size 427 MB, throughput 0.988028
Reading from 8182: heap size 234 MB, throughput 0.998734
Reading from 8182: heap size 234 MB, throughput 0.99875
Reading from 8181: heap size 429 MB, throughput 0.987317
Reading from 8182: heap size 238 MB, throughput 0.998678
Reading from 8181: heap size 430 MB, throughput 0.986097
Reading from 8182: heap size 238 MB, throughput 0.998043
Reading from 8181: heap size 431 MB, throughput 0.990528
Reading from 8182: heap size 241 MB, throughput 0.998606
Reading from 8181: heap size 430 MB, throughput 0.963771
Reading from 8181: heap size 432 MB, throughput 0.860122
Reading from 8181: heap size 435 MB, throughput 0.844501
Reading from 8182: heap size 241 MB, throughput 0.994761
Reading from 8181: heap size 426 MB, throughput 0.957311
Reading from 8182: heap size 243 MB, throughput 0.99452
Reading from 8182: heap size 244 MB, throughput 0.997409
Equal recommendation: 432 MB each
Reading from 8181: heap size 434 MB, throughput 0.990043
Reading from 8182: heap size 250 MB, throughput 0.997775
Reading from 8181: heap size 403 MB, throughput 0.99113
Reading from 8182: heap size 251 MB, throughput 0.998066
Reading from 8181: heap size 433 MB, throughput 0.989675
Reading from 8182: heap size 255 MB, throughput 0.998209
Reading from 8182: heap size 256 MB, throughput 0.998105
Reading from 8181: heap size 405 MB, throughput 0.989075
Reading from 8182: heap size 260 MB, throughput 0.997423
Reading from 8181: heap size 431 MB, throughput 0.988817
Reading from 8182: heap size 260 MB, throughput 0.998231
Equal recommendation: 432 MB each
Reading from 8181: heap size 429 MB, throughput 0.988113
Reading from 8182: heap size 265 MB, throughput 0.997903
Reading from 8181: heap size 427 MB, throughput 0.987841
Reading from 8182: heap size 265 MB, throughput 0.997926
Reading from 8181: heap size 429 MB, throughput 0.987207
Reading from 8182: heap size 269 MB, throughput 0.997962
Reading from 8182: heap size 270 MB, throughput 0.991941
Reading from 8181: heap size 430 MB, throughput 0.98187
Reading from 8182: heap size 274 MB, throughput 0.997098
Reading from 8181: heap size 430 MB, throughput 0.989466
Reading from 8182: heap size 275 MB, throughput 0.99797
Reading from 8181: heap size 431 MB, throughput 0.984553
Reading from 8181: heap size 433 MB, throughput 0.901756
Reading from 8181: heap size 430 MB, throughput 0.887003
Reading from 8181: heap size 433 MB, throughput 0.920798
Equal recommendation: 432 MB each
Reading from 8182: heap size 280 MB, throughput 0.998306
Reading from 8181: heap size 424 MB, throughput 0.989645
Reading from 8182: heap size 281 MB, throughput 0.998157
Reading from 8181: heap size 432 MB, throughput 0.992172
Reading from 8182: heap size 286 MB, throughput 0.998089
Reading from 8181: heap size 433 MB, throughput 0.990234
Reading from 8182: heap size 287 MB, throughput 0.998153
Reading from 8181: heap size 406 MB, throughput 0.989029
Reading from 8182: heap size 292 MB, throughput 0.997429
Reading from 8181: heap size 433 MB, throughput 0.990195
Reading from 8182: heap size 292 MB, throughput 0.998337
Equal recommendation: 432 MB each
Reading from 8181: heap size 409 MB, throughput 0.989254
Reading from 8182: heap size 298 MB, throughput 0.997247
Reading from 8182: heap size 298 MB, throughput 0.994135
Reading from 8181: heap size 428 MB, throughput 0.986997
Reading from 8182: heap size 302 MB, throughput 0.996503
Reading from 8181: heap size 427 MB, throughput 0.98077
Reading from 8182: heap size 303 MB, throughput 0.998278
Reading from 8181: heap size 429 MB, throughput 0.98466
Reading from 8182: heap size 305 MB, throughput 0.998421
Reading from 8181: heap size 429 MB, throughput 0.985141
Reading from 8182: heap size 303 MB, throughput 0.998287
Equal recommendation: 432 MB each
Reading from 8181: heap size 430 MB, throughput 0.990788
Reading from 8181: heap size 431 MB, throughput 0.905377
Reading from 8181: heap size 430 MB, throughput 0.879749
Reading from 8181: heap size 432 MB, throughput 0.878594
Reading from 8182: heap size 304 MB, throughput 0.998252
Reading from 8181: heap size 426 MB, throughput 0.990463
Reading from 8182: heap size 304 MB, throughput 0.998414
Reading from 8181: heap size 433 MB, throughput 0.992656
Reading from 8182: heap size 305 MB, throughput 0.998383
Reading from 8181: heap size 422 MB, throughput 0.99176
Reading from 8182: heap size 305 MB, throughput 0.998585
Reading from 8181: heap size 430 MB, throughput 0.990411
Reading from 8182: heap size 305 MB, throughput 0.998452
Reading from 8181: heap size 429 MB, throughput 0.989585
Reading from 8182: heap size 304 MB, throughput 0.995234
Equal recommendation: 432 MB each
Reading from 8182: heap size 304 MB, throughput 0.997702
Reading from 8181: heap size 431 MB, throughput 0.988401
Reading from 8182: heap size 305 MB, throughput 0.997835
Reading from 8181: heap size 429 MB, throughput 0.98942
Reading from 8182: heap size 305 MB, throughput 0.998347
Reading from 8181: heap size 431 MB, throughput 0.988212
Reading from 8182: heap size 305 MB, throughput 0.99833
Reading from 8181: heap size 431 MB, throughput 0.98687
Reading from 8182: heap size 305 MB, throughput 0.998024
Reading from 8181: heap size 431 MB, throughput 0.979775
Equal recommendation: 432 MB each
Reading from 8182: heap size 305 MB, throughput 0.998307
Reading from 8181: heap size 433 MB, throughput 0.991591
Reading from 8181: heap size 408 MB, throughput 0.899228
Reading from 8181: heap size 429 MB, throughput 0.888038
Reading from 8181: heap size 431 MB, throughput 0.87232
Reading from 8182: heap size 304 MB, throughput 0.998633
Reading from 8181: heap size 437 MB, throughput 0.986798
Reading from 8182: heap size 304 MB, throughput 0.998313
Reading from 8181: heap size 405 MB, throughput 0.992543
Reading from 8182: heap size 305 MB, throughput 0.998067
Reading from 8181: heap size 436 MB, throughput 0.990726
Reading from 8182: heap size 305 MB, throughput 0.993641
Reading from 8182: heap size 304 MB, throughput 0.998055
Reading from 8181: heap size 406 MB, throughput 0.991954
Equal recommendation: 432 MB each
Reading from 8182: heap size 305 MB, throughput 0.997515
Reading from 8181: heap size 434 MB, throughput 0.990337
Reading from 8182: heap size 305 MB, throughput 0.998676
Reading from 8181: heap size 408 MB, throughput 0.988488
Reading from 8182: heap size 305 MB, throughput 0.998371
Reading from 8181: heap size 429 MB, throughput 0.990777
Reading from 8182: heap size 304 MB, throughput 0.998152
Reading from 8181: heap size 429 MB, throughput 0.987549
Reading from 8182: heap size 304 MB, throughput 0.998731
Reading from 8181: heap size 429 MB, throughput 0.985747
Equal recommendation: 432 MB each
Reading from 8182: heap size 305 MB, throughput 0.998143
Reading from 8181: heap size 429 MB, throughput 0.98369
Reading from 8182: heap size 305 MB, throughput 0.998587
Reading from 8181: heap size 431 MB, throughput 0.990127
Reading from 8182: heap size 305 MB, throughput 0.996728
Reading from 8181: heap size 432 MB, throughput 0.921306
Reading from 8181: heap size 432 MB, throughput 0.835741
Reading from 8181: heap size 434 MB, throughput 0.88122
Reading from 8181: heap size 428 MB, throughput 0.986318
Reading from 8182: heap size 305 MB, throughput 0.996859
Reading from 8181: heap size 435 MB, throughput 0.990599
Reading from 8182: heap size 304 MB, throughput 0.99846
Reading from 8182: heap size 305 MB, throughput 0.998261
Reading from 8181: heap size 424 MB, throughput 0.991902
Equal recommendation: 432 MB each
Reading from 8182: heap size 305 MB, throughput 0.997477
Reading from 8181: heap size 432 MB, throughput 0.990794
Reading from 8182: heap size 305 MB, throughput 0.99781
Reading from 8181: heap size 420 MB, throughput 0.990847
Reading from 8182: heap size 304 MB, throughput 0.998352
Reading from 8181: heap size 428 MB, throughput 0.987869
Reading from 8181: heap size 425 MB, throughput 0.987527
Reading from 8182: heap size 304 MB, throughput 0.998577
Reading from 8182: heap size 305 MB, throughput 0.998792
Reading from 8181: heap size 427 MB, throughput 0.98889
Equal recommendation: 432 MB each
Reading from 8182: heap size 305 MB, throughput 0.998299
Reading from 8181: heap size 427 MB, throughput 0.982505
Reading from 8182: heap size 305 MB, throughput 0.822687
Reading from 8182: heap size 308 MB, throughput 0.996261
Reading from 8181: heap size 427 MB, throughput 0.984331
Reading from 8182: heap size 307 MB, throughput 0.999085
Reading from 8181: heap size 429 MB, throughput 0.992474
Reading from 8181: heap size 430 MB, throughput 0.979035
Reading from 8181: heap size 430 MB, throughput 0.888703
Reading from 8181: heap size 431 MB, throughput 0.869306
Reading from 8181: heap size 435 MB, throughput 0.892578
Reading from 8182: heap size 308 MB, throughput 0.998922
Reading from 8181: heap size 405 MB, throughput 0.993438
Reading from 8182: heap size 308 MB, throughput 0.999107
Equal recommendation: 432 MB each
Reading from 8182: heap size 308 MB, throughput 0.998404
Reading from 8181: heap size 435 MB, throughput 0.99353
Reading from 8182: heap size 308 MB, throughput 0.999029
Reading from 8181: heap size 406 MB, throughput 0.990938
Reading from 8182: heap size 308 MB, throughput 0.99869
Reading from 8181: heap size 433 MB, throughput 0.990638
Reading from 8182: heap size 308 MB, throughput 0.999029
Reading from 8181: heap size 408 MB, throughput 0.989152
Reading from 8182: heap size 308 MB, throughput 0.998971
Reading from 8182: heap size 307 MB, throughput 0.992675
Reading from 8181: heap size 428 MB, throughput 0.988351
Equal recommendation: 432 MB each
Reading from 8182: heap size 307 MB, throughput 0.998195
Reading from 8181: heap size 428 MB, throughput 0.987842
Reading from 8182: heap size 306 MB, throughput 0.998449
Reading from 8181: heap size 428 MB, throughput 0.986947
Reading from 8182: heap size 307 MB, throughput 0.998465
Reading from 8181: heap size 429 MB, throughput 0.982779
Reading from 8182: heap size 307 MB, throughput 0.998324
Reading from 8181: heap size 430 MB, throughput 0.992058
Reading from 8181: heap size 431 MB, throughput 0.980367
Reading from 8181: heap size 431 MB, throughput 0.899527
Reading from 8181: heap size 432 MB, throughput 0.884534
Reading from 8182: heap size 307 MB, throughput 0.99829
Reading from 8181: heap size 430 MB, throughput 0.919784
Equal recommendation: 432 MB each
Reading from 8182: heap size 307 MB, throughput 0.998222
Reading from 8181: heap size 434 MB, throughput 0.992928
Reading from 8182: heap size 304 MB, throughput 0.99842
Reading from 8181: heap size 424 MB, throughput 0.991786
Reading from 8182: heap size 306 MB, throughput 0.997855
Reading from 8181: heap size 432 MB, throughput 0.9873
Reading from 8182: heap size 306 MB, throughput 0.997726
Reading from 8182: heap size 307 MB, throughput 0.9934
Reading from 8181: heap size 420 MB, throughput 0.991223
Reading from 8182: heap size 307 MB, throughput 0.997178
Reading from 8181: heap size 428 MB, throughput 0.988965
Equal recommendation: 432 MB each
Reading from 8182: heap size 307 MB, throughput 0.998418
Reading from 8181: heap size 425 MB, throughput 0.990005
Reading from 8182: heap size 307 MB, throughput 0.998541
Reading from 8181: heap size 427 MB, throughput 0.987994
Reading from 8182: heap size 307 MB, throughput 0.998339
Reading from 8181: heap size 428 MB, throughput 0.98638
Reading from 8182: heap size 307 MB, throughput 0.998302
Reading from 8181: heap size 428 MB, throughput 0.984967
Reading from 8182: heap size 308 MB, throughput 0.99841
Reading from 8181: heap size 429 MB, throughput 0.99344
Reading from 8181: heap size 431 MB, throughput 0.984788
Equal recommendation: 432 MB each
Reading from 8181: heap size 431 MB, throughput 0.909082
Reading from 8181: heap size 432 MB, throughput 0.88256
Reading from 8182: heap size 306 MB, throughput 0.998455
Reading from 8181: heap size 436 MB, throughput 0.922525
Reading from 8182: heap size 307 MB, throughput 0.998551
Reading from 8181: heap size 407 MB, throughput 0.992734
Reading from 8182: heap size 307 MB, throughput 0.994735
Reading from 8181: heap size 436 MB, throughput 0.990364
Reading from 8182: heap size 307 MB, throughput 0.99662
Reading from 8181: heap size 407 MB, throughput 0.990426
Reading from 8182: heap size 307 MB, throughput 0.998643
Reading from 8181: heap size 433 MB, throughput 0.990218
Reading from 8182: heap size 308 MB, throughput 0.998324
Reading from 8181: heap size 409 MB, throughput 0.989304
Equal recommendation: 432 MB each
Reading from 8182: heap size 307 MB, throughput 0.998356
Reading from 8181: heap size 429 MB, throughput 0.987578
Reading from 8182: heap size 307 MB, throughput 0.996988
Reading from 8181: heap size 428 MB, throughput 0.98875
Reading from 8182: heap size 307 MB, throughput 0.998632
Reading from 8181: heap size 428 MB, throughput 0.988062
Reading from 8182: heap size 308 MB, throughput 0.998568
Reading from 8181: heap size 429 MB, throughput 0.986544
Reading from 8182: heap size 306 MB, throughput 0.998547
Equal recommendation: 432 MB each
Reading from 8181: heap size 430 MB, throughput 0.991972
Reading from 8182: heap size 307 MB, throughput 0.998494
Reading from 8182: heap size 307 MB, throughput 0.992032
Reading from 8181: heap size 431 MB, throughput 0.98355
Reading from 8181: heap size 432 MB, throughput 0.906467
Reading from 8181: heap size 432 MB, throughput 0.841156
Reading from 8181: heap size 431 MB, throughput 0.92168
Reading from 8182: heap size 306 MB, throughput 0.996849
Reading from 8181: heap size 435 MB, throughput 0.99289
Reading from 8182: heap size 307 MB, throughput 0.998235
Reading from 8181: heap size 423 MB, throughput 0.988314
Reading from 8182: heap size 307 MB, throughput 0.997122
Reading from 8181: heap size 432 MB, throughput 0.990744
Reading from 8182: heap size 305 MB, throughput 0.997952
Reading from 8181: heap size 432 MB, throughput 0.990179
Equal recommendation: 432 MB each
Reading from 8182: heap size 307 MB, throughput 0.998113
Reading from 8181: heap size 433 MB, throughput 0.897443
Reading from 8182: heap size 307 MB, throughput 0.99779
Reading from 8181: heap size 420 MB, throughput 0.996651
Reading from 8182: heap size 308 MB, throughput 0.998498
Reading from 8181: heap size 427 MB, throughput 0.99457
Reading from 8182: heap size 308 MB, throughput 0.997591
Reading from 8181: heap size 433 MB, throughput 0.99426
Client 8182 died
Clients: 1
Reading from 8181: heap size 396 MB, throughput 0.991351
Recommendation: one client; give it all the memory
Reading from 8181: heap size 431 MB, throughput 0.99314
Reading from 8181: heap size 431 MB, throughput 0.98204
Reading from 8181: heap size 430 MB, throughput 0.896208
Reading from 8181: heap size 431 MB, throughput 0.864783
Reading from 8181: heap size 436 MB, throughput 0.909613
Reading from 8181: heap size 441 MB, throughput 0.992887
Reading from 8181: heap size 439 MB, throughput 0.99088
Reading from 8181: heap size 443 MB, throughput 0.989553
Recommendation: one client; give it all the memory
Reading from 8181: heap size 442 MB, throughput 0.99024
Reading from 8181: heap size 444 MB, throughput 0.989396
Reading from 8181: heap size 447 MB, throughput 0.987966
Reading from 8181: heap size 448 MB, throughput 0.987038
Reading from 8181: heap size 452 MB, throughput 0.98669
Recommendation: one client; give it all the memory
Reading from 8181: heap size 453 MB, throughput 0.986706
Reading from 8181: heap size 457 MB, throughput 0.989311
Reading from 8181: heap size 458 MB, throughput 0.904854
Reading from 8181: heap size 458 MB, throughput 0.897589
Client 8181 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
