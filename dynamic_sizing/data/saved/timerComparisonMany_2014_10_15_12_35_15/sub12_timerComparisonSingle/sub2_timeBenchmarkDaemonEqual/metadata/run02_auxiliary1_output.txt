economemd
    total memory: 3669 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub12_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 5820: heap size 9 MB, throughput 0.979893
Clients: 1
Client 5820 has a minimum heap size of 12 MB
Reading from 5819: heap size 9 MB, throughput 0.991417
Clients: 2
Client 5819 has a minimum heap size of 1211 MB
Reading from 5820: heap size 9 MB, throughput 0.979215
Reading from 5819: heap size 9 MB, throughput 0.966473
Reading from 5819: heap size 9 MB, throughput 0.944793
Reading from 5819: heap size 9 MB, throughput 0.923752
Reading from 5820: heap size 11 MB, throughput 0.982234
Reading from 5820: heap size 11 MB, throughput 0.963421
Reading from 5819: heap size 11 MB, throughput 0.9815
Reading from 5819: heap size 11 MB, throughput 0.963738
Reading from 5820: heap size 15 MB, throughput 0.988835
Reading from 5819: heap size 17 MB, throughput 0.952907
Reading from 5819: heap size 17 MB, throughput 0.64702
Reading from 5820: heap size 15 MB, throughput 0.97419
Reading from 5819: heap size 30 MB, throughput 0.911885
Reading from 5819: heap size 31 MB, throughput 0.934233
Reading from 5820: heap size 24 MB, throughput 0.933951
Reading from 5819: heap size 35 MB, throughput 0.450472
Reading from 5819: heap size 48 MB, throughput 0.88506
Reading from 5820: heap size 30 MB, throughput 0.988995
Reading from 5819: heap size 49 MB, throughput 0.325304
Reading from 5819: heap size 64 MB, throughput 0.828301
Reading from 5819: heap size 69 MB, throughput 0.884832
Reading from 5820: heap size 37 MB, throughput 0.987494
Reading from 5819: heap size 71 MB, throughput 0.216876
Reading from 5820: heap size 43 MB, throughput 0.982945
Reading from 5819: heap size 99 MB, throughput 0.730779
Reading from 5820: heap size 44 MB, throughput 0.974524
Reading from 5819: heap size 100 MB, throughput 0.199232
Reading from 5820: heap size 46 MB, throughput 0.986694
Reading from 5819: heap size 130 MB, throughput 0.890051
Reading from 5820: heap size 52 MB, throughput 0.986063
Reading from 5819: heap size 130 MB, throughput 0.847465
Reading from 5819: heap size 132 MB, throughput 0.80877
Reading from 5819: heap size 135 MB, throughput 0.738361
Reading from 5820: heap size 53 MB, throughput 0.977633
Reading from 5820: heap size 61 MB, throughput 0.968634
Reading from 5820: heap size 61 MB, throughput 0.983351
Reading from 5820: heap size 73 MB, throughput 0.360815
Reading from 5819: heap size 137 MB, throughput 0.119525
Reading from 5819: heap size 180 MB, throughput 0.665706
Reading from 5820: heap size 76 MB, throughput 0.991595
Reading from 5819: heap size 183 MB, throughput 0.769926
Reading from 5819: heap size 185 MB, throughput 0.796739
Reading from 5819: heap size 191 MB, throughput 0.71473
Reading from 5820: heap size 86 MB, throughput 0.995104
Reading from 5819: heap size 194 MB, throughput 0.181329
Reading from 5819: heap size 242 MB, throughput 0.730658
Reading from 5820: heap size 88 MB, throughput 0.997285
Reading from 5819: heap size 244 MB, throughput 0.792044
Reading from 5819: heap size 247 MB, throughput 0.784884
Reading from 5819: heap size 252 MB, throughput 0.693487
Reading from 5819: heap size 255 MB, throughput 0.615596
Reading from 5819: heap size 265 MB, throughput 0.65571
Reading from 5819: heap size 267 MB, throughput 0.629629
Reading from 5819: heap size 273 MB, throughput 0.582382
Reading from 5820: heap size 97 MB, throughput 0.996498
Reading from 5819: heap size 276 MB, throughput 0.481884
Reading from 5819: heap size 285 MB, throughput 0.517076
Reading from 5820: heap size 99 MB, throughput 0.997102
Reading from 5819: heap size 289 MB, throughput 0.0872132
Reading from 5819: heap size 342 MB, throughput 0.502684
Reading from 5820: heap size 107 MB, throughput 0.996716
Reading from 5819: heap size 341 MB, throughput 0.0873263
Reading from 5819: heap size 337 MB, throughput 0.483829
Reading from 5819: heap size 388 MB, throughput 0.649836
Reading from 5819: heap size 330 MB, throughput 0.614278
Reading from 5819: heap size 381 MB, throughput 0.580474
Reading from 5819: heap size 386 MB, throughput 0.581456
Reading from 5820: heap size 108 MB, throughput 0.99675
Reading from 5819: heap size 386 MB, throughput 0.613465
Reading from 5819: heap size 388 MB, throughput 0.550247
Reading from 5819: heap size 390 MB, throughput 0.501407
Reading from 5819: heap size 396 MB, throughput 0.455116
Reading from 5819: heap size 400 MB, throughput 0.540663
Reading from 5819: heap size 405 MB, throughput 0.473043
Reading from 5820: heap size 115 MB, throughput 0.996739
Reading from 5819: heap size 409 MB, throughput 0.49771
Reading from 5819: heap size 413 MB, throughput 0.403482
Reading from 5819: heap size 417 MB, throughput 0.475556
Equal recommendation: 1834 MB each
Reading from 5820: heap size 116 MB, throughput 0.996066
Reading from 5819: heap size 422 MB, throughput 0.073733
Reading from 5819: heap size 477 MB, throughput 0.487907
Reading from 5820: heap size 121 MB, throughput 0.997005
Reading from 5819: heap size 479 MB, throughput 0.590658
Reading from 5819: heap size 477 MB, throughput 0.58398
Reading from 5820: heap size 121 MB, throughput 0.996158
Reading from 5819: heap size 479 MB, throughput 0.109985
Reading from 5819: heap size 533 MB, throughput 0.499753
Reading from 5819: heap size 485 MB, throughput 0.609275
Reading from 5819: heap size 533 MB, throughput 0.649271
Reading from 5819: heap size 533 MB, throughput 0.52544
Reading from 5820: heap size 125 MB, throughput 0.995409
Reading from 5819: heap size 535 MB, throughput 0.559125
Reading from 5819: heap size 541 MB, throughput 0.54967
Reading from 5819: heap size 548 MB, throughput 0.506918
Reading from 5819: heap size 555 MB, throughput 0.490313
Reading from 5820: heap size 126 MB, throughput 0.995773
Reading from 5819: heap size 561 MB, throughput 0.0908599
Reading from 5820: heap size 130 MB, throughput 0.989133
Reading from 5819: heap size 623 MB, throughput 0.408827
Reading from 5819: heap size 632 MB, throughput 0.602816
Reading from 5819: heap size 633 MB, throughput 0.58055
Reading from 5819: heap size 637 MB, throughput 0.603291
Reading from 5820: heap size 130 MB, throughput 0.992571
Reading from 5819: heap size 638 MB, throughput 0.84192
Reading from 5820: heap size 135 MB, throughput 0.996763
Reading from 5819: heap size 641 MB, throughput 0.874181
Reading from 5820: heap size 135 MB, throughput 0.995797
Reading from 5820: heap size 139 MB, throughput 0.996427
Reading from 5819: heap size 646 MB, throughput 0.232846
Reading from 5820: heap size 140 MB, throughput 0.99682
Reading from 5819: heap size 709 MB, throughput 0.658822
Reading from 5819: heap size 722 MB, throughput 0.51519
Reading from 5819: heap size 731 MB, throughput 0.638414
Reading from 5819: heap size 732 MB, throughput 0.37739
Reading from 5820: heap size 143 MB, throughput 0.998118
Equal recommendation: 1834 MB each
Reading from 5820: heap size 144 MB, throughput 0.997496
Reading from 5819: heap size 728 MB, throughput 0.0281407
Reading from 5819: heap size 727 MB, throughput 0.315011
Reading from 5819: heap size 794 MB, throughput 0.480163
Reading from 5819: heap size 798 MB, throughput 0.758312
Reading from 5820: heap size 147 MB, throughput 0.998709
Reading from 5819: heap size 800 MB, throughput 0.873977
Reading from 5819: heap size 801 MB, throughput 0.597646
Reading from 5819: heap size 802 MB, throughput 0.648447
Reading from 5819: heap size 803 MB, throughput 0.674323
Reading from 5820: heap size 147 MB, throughput 0.998307
Reading from 5820: heap size 149 MB, throughput 0.997914
Reading from 5819: heap size 808 MB, throughput 0.11153
Reading from 5820: heap size 150 MB, throughput 0.993882
Reading from 5819: heap size 884 MB, throughput 0.475204
Reading from 5819: heap size 887 MB, throughput 0.749351
Reading from 5820: heap size 152 MB, throughput 0.993195
Reading from 5819: heap size 891 MB, throughput 0.925969
Reading from 5819: heap size 899 MB, throughput 0.825691
Reading from 5819: heap size 899 MB, throughput 0.922518
Reading from 5820: heap size 153 MB, throughput 0.996434
Reading from 5819: heap size 887 MB, throughput 0.868317
Reading from 5819: heap size 774 MB, throughput 0.806589
Reading from 5819: heap size 871 MB, throughput 0.769197
Reading from 5819: heap size 778 MB, throughput 0.79796
Reading from 5819: heap size 861 MB, throughput 0.822037
Reading from 5819: heap size 780 MB, throughput 0.8225
Reading from 5820: heap size 158 MB, throughput 0.997853
Reading from 5819: heap size 858 MB, throughput 0.850979
Reading from 5819: heap size 863 MB, throughput 0.843523
Reading from 5819: heap size 854 MB, throughput 0.852185
Reading from 5819: heap size 859 MB, throughput 0.849781
Reading from 5819: heap size 853 MB, throughput 0.861916
Reading from 5820: heap size 158 MB, throughput 0.997323
Reading from 5820: heap size 162 MB, throughput 0.997144
Reading from 5819: heap size 856 MB, throughput 0.98332
Reading from 5820: heap size 162 MB, throughput 0.996924
Equal recommendation: 1834 MB each
Reading from 5819: heap size 850 MB, throughput 0.309732
Reading from 5820: heap size 165 MB, throughput 0.997661
Reading from 5819: heap size 937 MB, throughput 0.610568
Reading from 5819: heap size 933 MB, throughput 0.793766
Reading from 5819: heap size 937 MB, throughput 0.768292
Reading from 5819: heap size 932 MB, throughput 0.803021
Reading from 5819: heap size 936 MB, throughput 0.777922
Reading from 5820: heap size 165 MB, throughput 0.997638
Reading from 5819: heap size 935 MB, throughput 0.73062
Reading from 5819: heap size 938 MB, throughput 0.826076
Reading from 5819: heap size 939 MB, throughput 0.829751
Reading from 5820: heap size 168 MB, throughput 0.990114
Reading from 5819: heap size 942 MB, throughput 0.871912
Reading from 5819: heap size 945 MB, throughput 0.885423
Reading from 5819: heap size 949 MB, throughput 0.643536
Reading from 5819: heap size 948 MB, throughput 0.632966
Reading from 5820: heap size 168 MB, throughput 0.994207
Reading from 5820: heap size 173 MB, throughput 0.990338
Reading from 5819: heap size 959 MB, throughput 0.0776627
Reading from 5819: heap size 1119 MB, throughput 0.551499
Reading from 5819: heap size 1122 MB, throughput 0.818619
Reading from 5819: heap size 1137 MB, throughput 0.800147
Reading from 5820: heap size 173 MB, throughput 0.996879
Reading from 5819: heap size 1138 MB, throughput 0.793155
Reading from 5819: heap size 1145 MB, throughput 0.797071
Reading from 5819: heap size 1148 MB, throughput 0.683828
Reading from 5819: heap size 1153 MB, throughput 0.772955
Reading from 5819: heap size 1156 MB, throughput 0.777657
Reading from 5820: heap size 177 MB, throughput 0.997614
Reading from 5819: heap size 1157 MB, throughput 0.9601
Reading from 5820: heap size 178 MB, throughput 0.996452
Reading from 5820: heap size 182 MB, throughput 0.997748
Equal recommendation: 1834 MB each
Reading from 5819: heap size 1162 MB, throughput 0.964558
Reading from 5820: heap size 182 MB, throughput 0.997818
Reading from 5820: heap size 185 MB, throughput 0.998499
Reading from 5819: heap size 1161 MB, throughput 0.950151
Reading from 5820: heap size 186 MB, throughput 0.99154
Reading from 5820: heap size 189 MB, throughput 0.993466
Reading from 5819: heap size 1167 MB, throughput 0.959682
Reading from 5820: heap size 189 MB, throughput 0.997553
Reading from 5820: heap size 193 MB, throughput 0.998027
Reading from 5819: heap size 1176 MB, throughput 0.954244
Reading from 5820: heap size 194 MB, throughput 0.997765
Reading from 5819: heap size 1178 MB, throughput 0.952768
Reading from 5820: heap size 197 MB, throughput 0.99805
Reading from 5820: heap size 197 MB, throughput 0.997608
Reading from 5819: heap size 1181 MB, throughput 0.964442
Equal recommendation: 1834 MB each
Reading from 5820: heap size 201 MB, throughput 0.99802
Reading from 5820: heap size 201 MB, throughput 0.997135
Reading from 5819: heap size 1186 MB, throughput 0.960785
Reading from 5820: heap size 203 MB, throughput 0.997327
Reading from 5819: heap size 1190 MB, throughput 0.962738
Reading from 5820: heap size 204 MB, throughput 0.997562
Reading from 5820: heap size 207 MB, throughput 0.997899
Reading from 5819: heap size 1191 MB, throughput 0.958852
Reading from 5820: heap size 207 MB, throughput 0.997416
Reading from 5819: heap size 1197 MB, throughput 0.96336
Reading from 5820: heap size 210 MB, throughput 0.997855
Reading from 5820: heap size 210 MB, throughput 0.998027
Reading from 5819: heap size 1199 MB, throughput 0.940689
Reading from 5820: heap size 213 MB, throughput 0.994056
Equal recommendation: 1834 MB each
Reading from 5820: heap size 213 MB, throughput 0.995494
Reading from 5819: heap size 1205 MB, throughput 0.958249
Reading from 5820: heap size 218 MB, throughput 0.998281
Reading from 5820: heap size 218 MB, throughput 0.99766
Reading from 5819: heap size 1208 MB, throughput 0.960752
Reading from 5820: heap size 222 MB, throughput 0.99815
Reading from 5819: heap size 1214 MB, throughput 0.962926
Reading from 5820: heap size 222 MB, throughput 0.997939
Reading from 5820: heap size 225 MB, throughput 0.998192
Reading from 5819: heap size 1219 MB, throughput 0.952428
Reading from 5820: heap size 225 MB, throughput 0.997652
Equal recommendation: 1834 MB each
Reading from 5819: heap size 1225 MB, throughput 0.957895
Reading from 5820: heap size 228 MB, throughput 0.998032
Reading from 5820: heap size 217 MB, throughput 0.997932
Reading from 5819: heap size 1230 MB, throughput 0.956575
Reading from 5820: heap size 207 MB, throughput 0.997917
Reading from 5820: heap size 198 MB, throughput 0.997828
Reading from 5819: heap size 1237 MB, throughput 0.960875
Reading from 5820: heap size 190 MB, throughput 0.99765
Reading from 5820: heap size 182 MB, throughput 0.906636
Reading from 5820: heap size 177 MB, throughput 0.991889
Reading from 5819: heap size 1240 MB, throughput 0.929805
Reading from 5820: heap size 183 MB, throughput 0.996005
Reading from 5820: heap size 188 MB, throughput 0.997944
Reading from 5819: heap size 1247 MB, throughput 0.95729
Reading from 5820: heap size 193 MB, throughput 0.998293
Equal recommendation: 1834 MB each
Reading from 5820: heap size 197 MB, throughput 0.998042
Reading from 5819: heap size 1249 MB, throughput 0.96137
Reading from 5820: heap size 202 MB, throughput 0.998
Reading from 5820: heap size 206 MB, throughput 0.997911
Reading from 5819: heap size 1256 MB, throughput 0.962379
Reading from 5820: heap size 210 MB, throughput 0.998189
Reading from 5820: heap size 214 MB, throughput 0.99769
Reading from 5820: heap size 214 MB, throughput 0.997565
Reading from 5819: heap size 1257 MB, throughput 0.573471
Reading from 5820: heap size 218 MB, throughput 0.998033
Reading from 5819: heap size 1324 MB, throughput 0.989115
Reading from 5820: heap size 218 MB, throughput 0.997562
Equal recommendation: 1834 MB each
Reading from 5820: heap size 222 MB, throughput 0.997924
Reading from 5819: heap size 1325 MB, throughput 0.98682
Reading from 5820: heap size 222 MB, throughput 0.997823
Reading from 5820: heap size 226 MB, throughput 0.993475
Reading from 5820: heap size 226 MB, throughput 0.992018
Reading from 5819: heap size 1335 MB, throughput 0.98472
Reading from 5820: heap size 232 MB, throughput 0.998134
Reading from 5820: heap size 233 MB, throughput 0.997627
Reading from 5819: heap size 1341 MB, throughput 0.982438
Reading from 5820: heap size 238 MB, throughput 0.998301
Reading from 5819: heap size 1343 MB, throughput 0.981252
Reading from 5820: heap size 239 MB, throughput 0.99786
Equal recommendation: 1834 MB each
Reading from 5820: heap size 243 MB, throughput 0.998226
Reading from 5819: heap size 1345 MB, throughput 0.979033
Reading from 5820: heap size 244 MB, throughput 0.997694
Reading from 5820: heap size 249 MB, throughput 0.998043
Reading from 5819: heap size 1333 MB, throughput 0.97928
Reading from 5820: heap size 249 MB, throughput 0.997645
Reading from 5819: heap size 1247 MB, throughput 0.976248
Reading from 5820: heap size 254 MB, throughput 0.998076
Reading from 5820: heap size 254 MB, throughput 0.997707
Reading from 5819: heap size 1322 MB, throughput 0.974671
Reading from 5820: heap size 258 MB, throughput 0.996148
Equal recommendation: 1834 MB each
Reading from 5820: heap size 258 MB, throughput 0.993075
Reading from 5820: heap size 264 MB, throughput 0.997749
Reading from 5819: heap size 1329 MB, throughput 0.970877
Reading from 5820: heap size 264 MB, throughput 0.998099
Reading from 5819: heap size 1330 MB, throughput 0.970215
Reading from 5820: heap size 270 MB, throughput 0.998311
Reading from 5820: heap size 271 MB, throughput 0.998017
Reading from 5819: heap size 1330 MB, throughput 0.970037
Reading from 5820: heap size 276 MB, throughput 0.998357
Equal recommendation: 1834 MB each
Reading from 5819: heap size 1333 MB, throughput 0.969097
Reading from 5820: heap size 276 MB, throughput 0.997885
Reading from 5820: heap size 281 MB, throughput 0.998301
Reading from 5819: heap size 1337 MB, throughput 0.966771
Reading from 5820: heap size 281 MB, throughput 0.997578
Reading from 5819: heap size 1341 MB, throughput 0.965579
Reading from 5820: heap size 286 MB, throughput 0.99832
Reading from 5820: heap size 286 MB, throughput 0.993315
Reading from 5820: heap size 290 MB, throughput 0.996974
Reading from 5819: heap size 1349 MB, throughput 0.961339
Reading from 5820: heap size 291 MB, throughput 0.997956
Equal recommendation: 1834 MB each
Reading from 5819: heap size 1357 MB, throughput 0.964575
Reading from 5820: heap size 298 MB, throughput 0.998067
Reading from 5820: heap size 298 MB, throughput 0.997341
Reading from 5819: heap size 1364 MB, throughput 0.958017
Reading from 5820: heap size 304 MB, throughput 0.998232
Reading from 5819: heap size 1371 MB, throughput 0.961744
Reading from 5820: heap size 304 MB, throughput 0.99806
Reading from 5820: heap size 310 MB, throughput 0.998387
Equal recommendation: 1834 MB each
Reading from 5820: heap size 310 MB, throughput 0.997934
Reading from 5820: heap size 316 MB, throughput 0.997917
Reading from 5820: heap size 316 MB, throughput 0.91454
Reading from 5820: heap size 327 MB, throughput 0.999129
Reading from 5820: heap size 328 MB, throughput 0.999025
Reading from 5820: heap size 335 MB, throughput 0.999177
Equal recommendation: 1834 MB each
Reading from 5820: heap size 336 MB, throughput 0.999078
Reading from 5819: heap size 1375 MB, throughput 0.848207
Reading from 5820: heap size 342 MB, throughput 0.999176
Reading from 5820: heap size 342 MB, throughput 0.9992
Reading from 5820: heap size 347 MB, throughput 0.999281
Reading from 5820: heap size 347 MB, throughput 0.996805
Equal recommendation: 1834 MB each
Reading from 5820: heap size 351 MB, throughput 0.997493
Reading from 5820: heap size 352 MB, throughput 0.998325
Reading from 5819: heap size 1484 MB, throughput 0.986535
Reading from 5820: heap size 358 MB, throughput 0.99844
Reading from 5820: heap size 359 MB, throughput 0.998384
Reading from 5820: heap size 365 MB, throughput 0.998618
Equal recommendation: 1834 MB each
Reading from 5819: heap size 1424 MB, throughput 0.987932
Reading from 5819: heap size 1520 MB, throughput 0.878413
Reading from 5820: heap size 365 MB, throughput 0.998268
Reading from 5819: heap size 1524 MB, throughput 0.776792
Reading from 5819: heap size 1505 MB, throughput 0.718186
Reading from 5819: heap size 1534 MB, throughput 0.695974
Reading from 5819: heap size 1559 MB, throughput 0.715525
Reading from 5819: heap size 1575 MB, throughput 0.702185
Reading from 5819: heap size 1604 MB, throughput 0.738882
Reading from 5819: heap size 1613 MB, throughput 0.788398
Reading from 5820: heap size 372 MB, throughput 0.998778
Reading from 5820: heap size 372 MB, throughput 0.996952
Reading from 5819: heap size 1626 MB, throughput 0.951757
Reading from 5820: heap size 377 MB, throughput 0.997035
Reading from 5819: heap size 1637 MB, throughput 0.969134
Equal recommendation: 1834 MB each
Reading from 5820: heap size 378 MB, throughput 0.998292
Reading from 5819: heap size 1632 MB, throughput 0.971343
Reading from 5820: heap size 387 MB, throughput 0.998467
Reading from 5819: heap size 1644 MB, throughput 0.969775
Reading from 5820: heap size 387 MB, throughput 0.998229
Reading from 5820: heap size 394 MB, throughput 0.997178
Reading from 5819: heap size 1634 MB, throughput 0.96291
Reading from 5820: heap size 394 MB, throughput 0.998061
Equal recommendation: 1834 MB each
Reading from 5819: heap size 1646 MB, throughput 0.9695
Reading from 5820: heap size 402 MB, throughput 0.998238
Reading from 5820: heap size 402 MB, throughput 0.993694
Reading from 5819: heap size 1659 MB, throughput 0.782816
Reading from 5820: heap size 410 MB, throughput 0.998519
Reading from 5819: heap size 1560 MB, throughput 0.993611
Reading from 5820: heap size 411 MB, throughput 0.998211
Equal recommendation: 1834 MB each
Reading from 5819: heap size 1568 MB, throughput 0.991652
Reading from 5820: heap size 420 MB, throughput 0.998397
Reading from 5819: heap size 1576 MB, throughput 0.989837
Reading from 5820: heap size 421 MB, throughput 0.997318
Reading from 5820: heap size 431 MB, throughput 0.998344
Reading from 5819: heap size 1581 MB, throughput 0.988023
Reading from 5820: heap size 431 MB, throughput 0.998562
Equal recommendation: 1834 MB each
Reading from 5820: heap size 438 MB, throughput 0.994906
Reading from 5819: heap size 1586 MB, throughput 0.986392
Reading from 5820: heap size 439 MB, throughput 0.998233
Reading from 5819: heap size 1569 MB, throughput 0.983844
Reading from 5820: heap size 450 MB, throughput 0.998364
Reading from 5819: heap size 1389 MB, throughput 0.982156
Reading from 5820: heap size 452 MB, throughput 0.998308
Equal recommendation: 1834 MB each
Reading from 5819: heap size 1550 MB, throughput 0.981647
Reading from 5820: heap size 460 MB, throughput 0.998361
Reading from 5819: heap size 1418 MB, throughput 0.97924
Reading from 5820: heap size 461 MB, throughput 0.998282
Reading from 5820: heap size 470 MB, throughput 0.996085
Reading from 5819: heap size 1538 MB, throughput 0.975849
Reading from 5820: heap size 471 MB, throughput 0.997927
Equal recommendation: 1834 MB each
Reading from 5819: heap size 1548 MB, throughput 0.973602
Reading from 5820: heap size 482 MB, throughput 0.998646
Reading from 5820: heap size 483 MB, throughput 0.998326
Reading from 5819: heap size 1551 MB, throughput 0.974321
Reading from 5820: heap size 492 MB, throughput 0.998431
Reading from 5819: heap size 1552 MB, throughput 0.970679
Equal recommendation: 1834 MB each
Reading from 5820: heap size 493 MB, throughput 0.998417
Reading from 5820: heap size 501 MB, throughput 0.994855
Reading from 5819: heap size 1558 MB, throughput 0.96757
Reading from 5820: heap size 502 MB, throughput 0.998318
Reading from 5819: heap size 1564 MB, throughput 0.96496
Reading from 5820: heap size 514 MB, throughput 0.998525
Equal recommendation: 1834 MB each
Reading from 5819: heap size 1574 MB, throughput 0.967607
Reading from 5820: heap size 515 MB, throughput 0.998296
Reading from 5819: heap size 1583 MB, throughput 0.964171
Reading from 5820: heap size 525 MB, throughput 0.998528
Reading from 5819: heap size 1597 MB, throughput 0.967417
Equal recommendation: 1834 MB each
Client 5820 died
Clients: 1
Reading from 5819: heap size 1603 MB, throughput 0.968069
Reading from 5819: heap size 1618 MB, throughput 0.969
Reading from 5819: heap size 1621 MB, throughput 0.967915
Recommendation: one client; give it all the memory
Reading from 5819: heap size 1636 MB, throughput 0.969325
Reading from 5819: heap size 1638 MB, throughput 0.967758
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 5819: heap size 1652 MB, throughput 0.996477
Recommendation: one client; give it all the memory
Reading from 5819: heap size 1653 MB, throughput 0.987495
Reading from 5819: heap size 1657 MB, throughput 0.875912
Reading from 5819: heap size 1658 MB, throughput 0.761937
Reading from 5819: heap size 1692 MB, throughput 0.804038
Reading from 5819: heap size 1711 MB, throughput 0.793354
Client 5819 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
