economemd
    total memory: 200 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_08_12_14_51_20/sub5_timerComparisonSingle/sub3_timeDacapoDaemon/daemon_run3_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 28036: heap size 9 MB, throughput 0.990632
Clients: 1
Reading from 28036: heap size 9 MB, throughput 0.985891
Reading from 28038: heap size 9 MB, throughput 0.991651
Clients: 2
Reading from 28036: heap size 9 MB, throughput 0.976701
Reading from 28036: heap size 9 MB, throughput 0.975553
Reading from 28038: heap size 9 MB, throughput 0.989138
Reading from 28038: heap size 9 MB, throughput 0.985535
Reading from 28038: heap size 9 MB, throughput 0.982232
Reading from 28036: heap size 11 MB, throughput 0.974671
Reading from 28038: heap size 11 MB, throughput 0.983703
Reading from 28036: heap size 11 MB, throughput 0.961306
Reading from 28038: heap size 11 MB, throughput 0.983648
Reading from 28036: heap size 16 MB, throughput 0.920288
Reading from 28038: heap size 15 MB, throughput 0.99591
Reading from 28038: heap size 16 MB, throughput 0.993053
Reading from 28038: heap size 23 MB, throughput 0.991111
Reading from 28038: heap size 24 MB, throughput 0.986733
Reading from 28036: heap size 23 MB, throughput 0.971991
Reading from 28038: heap size 33 MB, throughput 0.983047
Reading from 28038: heap size 35 MB, throughput 0.887725
Reading from 28038: heap size 43 MB, throughput 0.895967
Reading from 28038: heap size 47 MB, throughput 0.950117
Reading from 28038: heap size 58 MB, throughput 0.941608
Reading from 28036: heap size 23 MB, throughput 0.990374
Reading from 28038: heap size 58 MB, throughput 0.920828
Reading from 28038: heap size 79 MB, throughput 0.941689
Reading from 28038: heap size 79 MB, throughput 0.960852
Reading from 28038: heap size 99 MB, throughput 0.945333
Reading from 28036: heap size 25 MB, throughput 0.994593
Reading from 28038: heap size 100 MB, throughput 0.941154
Reading from 28038: heap size 135 MB, throughput 0.890691
Reading from 28036: heap size 28 MB, throughput 0.996253
Reading from 28038: heap size 136 MB, throughput 0.862771
Reading from 28038: heap size 167 MB, throughput 0.965998
Reading from 28038: heap size 167 MB, throughput 0.965779
Reading from 28038: heap size 180 MB, throughput 0.965433
Reading from 28036: heap size 28 MB, throughput 0.997117
Reading from 28038: heap size 180 MB, throughput 0.964228
Reading from 28038: heap size 195 MB, throughput 0.964324
Reading from 28038: heap size 194 MB, throughput 0.965619
Reading from 28038: heap size 211 MB, throughput 0.980646
Reading from 28038: heap size 211 MB, throughput 0.98435
Reading from 28038: heap size 226 MB, throughput 0.956732
Reading from 28036: heap size 28 MB, throughput 0.997662
Reading from 28038: heap size 227 MB, throughput 0.993869
Reading from 28038: heap size 242 MB, throughput 0.994647
Reading from 28036: heap size 28 MB, throughput 0.99811
Reading from 28038: heap size 245 MB, throughput 0.996777
Reading from 28036: heap size 29 MB, throughput 0.997922
Numeric result:
Recommendation: 2 clients, utility 1.02258:
    h1: 193.009 MB (U(h) = 0.901044*h^0.0276049)
    h2: 6.99132 MB (U(h) = 0.979519*h^0.001)
Recommendation: 2 clients, utility 1.02258:
    h1: 193.008 MB (U(h) = 0.901044*h^0.0276049)
    h2: 6.99182 MB (U(h) = 0.979519*h^0.001)
Reading from 28038: heap size 255 MB, throughput 0.996252
Reading from 28038: heap size 257 MB, throughput 0.995795
Reading from 28036: heap size 29 MB, throughput 0.998144
Reading from 28036: heap size 29 MB, throughput 0.998134
Reading from 28036: heap size 30 MB, throughput 0.997863
Reading from 28036: heap size 30 MB, throughput 0.9978
Reading from 28036: heap size 30 MB, throughput 0.997776
Reading from 28036: heap size 31 MB, throughput 0.986615
Reading from 28036: heap size 31 MB, throughput 0.937924
Reading from 28036: heap size 32 MB, throughput 0.910034
Reading from 28036: heap size 32 MB, throughput 0.997241
Numeric result:
Recommendation: 2 clients, utility 0.977906:
    h1: 187.757 MB (U(h) = 0.930083*h^0.0153363)
    h2: 12.2429 MB (U(h) = 0.967873*h^0.001)
Recommendation: 2 clients, utility 0.977906:
    h1: 187.757 MB (U(h) = 0.930083*h^0.0153363)
    h2: 12.2426 MB (U(h) = 0.967873*h^0.001)
Reading from 28038: heap size 5359 MB, throughput 0.998921
Reading from 28036: heap size 33 MB, throughput 0.995287
Reading from 28036: heap size 36 MB, throughput 0.965219
Reading from 28036: heap size 39 MB, throughput 0.982097
Reading from 28036: heap size 39 MB, throughput 0.990383
Reading from 28036: heap size 39 MB, throughput 0.997689
Reading from 28036: heap size 39 MB, throughput 0.998288
Reading from 28036: heap size 39 MB, throughput 0.996517
Reading from 28036: heap size 39 MB, throughput 0.997592
Reading from 28038: heap size 5359 MB, throughput 0.999444
Numeric result:
Recommendation: 2 clients, utility 0.968882:
    h1: 176.729 MB (U(h) = 0.926646*h^0.0167205)
    h2: 23.2712 MB (U(h) = 0.952295*h^0.00220183)
Recommendation: 2 clients, utility 0.968882:
    h1: 176.728 MB (U(h) = 0.926646*h^0.0167205)
    h2: 23.2724 MB (U(h) = 0.952295*h^0.00220183)
Reading from 28036: heap size 38 MB, throughput 0.998171
Reading from 28036: heap size 39 MB, throughput 0.998189
Reading from 28036: heap size 38 MB, throughput 0.998301
Reading from 28036: heap size 39 MB, throughput 0.995246
Reading from 28036: heap size 39 MB, throughput 0.992898
Reading from 28036: heap size 39 MB, throughput 0.994837
Reading from 28036: heap size 39 MB, throughput 0.955448
Reading from 28038: heap size 5359 MB, throughput 0.999641
Reading from 28036: heap size 40 MB, throughput 0.995161
Numeric result:
Recommendation: 2 clients, utility 0.96923:
    h1: 176.733 MB (U(h) = 0.926274*h^0.016871)
    h2: 23.2666 MB (U(h) = 0.95222*h^0.00222121)
Recommendation: 2 clients, utility 0.96923:
    h1: 176.732 MB (U(h) = 0.926274*h^0.016871)
    h2: 23.2683 MB (U(h) = 0.95222*h^0.00222121)
Reading from 28036: heap size 42 MB, throughput 0.995642
Reading from 28036: heap size 43 MB, throughput 0.996136
Reading from 28036: heap size 43 MB, throughput 0.996601
Reading from 28036: heap size 44 MB, throughput 0.996682
Reading from 28038: heap size 5359 MB, throughput 0.999702
Reading from 28036: heap size 44 MB, throughput 0.993556
Reading from 28036: heap size 45 MB, throughput 0.995125
Reading from 28036: heap size 46 MB, throughput 0.996247
Numeric result:
Recommendation: 2 clients, utility 0.978703:
    h1: 180.864 MB (U(h) = 0.915018*h^0.0211055)
    h2: 19.1363 MB (U(h) = 0.952172*h^0.00223345)
Recommendation: 2 clients, utility 0.978703:
    h1: 180.861 MB (U(h) = 0.915018*h^0.0211055)
    h2: 19.1393 MB (U(h) = 0.952172*h^0.00223345)
Reading from 28036: heap size 46 MB, throughput 0.996352
Reading from 28036: heap size 46 MB, throughput 0.996537
Reading from 28038: heap size 5360 MB, throughput 0.999728
Reading from 28036: heap size 47 MB, throughput 0.996539
Reading from 28036: heap size 47 MB, throughput 0.994773
Reading from 28036: heap size 47 MB, throughput 0.995062
Reading from 28038: heap size 5357 MB, throughput 0.99971
Reading from 28036: heap size 52 MB, throughput 0.997388
Reading from 28036: heap size 53 MB, throughput 0.998349
Numeric result:
Recommendation: 2 clients, utility 0.978547:
    h1: 162.586 MB (U(h) = 0.912486*h^0.0220238)
    h2: 37.4145 MB (U(h) = 0.941209*h^0.00506867)
Recommendation: 2 clients, utility 0.978547:
    h1: 162.582 MB (U(h) = 0.912486*h^0.0220238)
    h2: 37.4175 MB (U(h) = 0.941209*h^0.00506867)
Reading from 28036: heap size 54 MB, throughput 0.998809
Reading from 28036: heap size 54 MB, throughput 0.999007
Reading from 28038: heap size 5360 MB, throughput 0.999729
Reading from 28036: heap size 54 MB, throughput 0.998462
Reading from 28036: heap size 54 MB, throughput 0.998691
Reading from 28036: heap size 53 MB, throughput 0.998712
Reading from 28036: heap size 40 MB, throughput 0.998628
Reading from 28038: heap size 5360 MB, throughput 0.999729
Numeric result:
Recommendation: 2 clients, utility 0.97926:
    h1: 163.044 MB (U(h) = 0.911574*h^0.0223632)
    h2: 36.9564 MB (U(h) = 0.941208*h^0.00506873)
Recommendation: 2 clients, utility 0.97926:
    h1: 163.045 MB (U(h) = 0.911574*h^0.0223632)
    h2: 36.9549 MB (U(h) = 0.941208*h^0.00506873)
Reading from 28036: heap size 52 MB, throughput 0.998572
Reading from 28036: heap size 41 MB, throughput 0.998571
Reading from 28036: heap size 51 MB, throughput 0.997209
Reading from 28038: heap size 5360 MB, throughput 0.999258
Reading from 28036: heap size 52 MB, throughput 0.997306
Reading from 28036: heap size 53 MB, throughput 0.996493
Reading from 28038: heap size 5360 MB, throughput 0.999461
Reading from 28036: heap size 53 MB, throughput 0.996629
Reading from 28036: heap size 54 MB, throughput 0.996551
Numeric result:
Recommendation: 2 clients, utility 0.979974:
    h1: 163.474 MB (U(h) = 0.910986*h^0.0226316)
    h2: 36.5264 MB (U(h) = 0.941255*h^0.00505665)
Recommendation: 2 clients, utility 0.979974:
    h1: 163.474 MB (U(h) = 0.910986*h^0.0226316)
    h2: 36.5256 MB (U(h) = 0.941255*h^0.00505665)
Reading from 28036: heap size 55 MB, throughput 0.996682
Reading from 28038: heap size 5361 MB, throughput 0.999527
Reading from 28036: heap size 55 MB, throughput 0.996909
Reading from 28036: heap size 55 MB, throughput 0.994093
Reading from 28036: heap size 56 MB, throughput 0.996045
Reading from 28038: heap size 5359 MB, throughput 0.999636
Reading from 28036: heap size 56 MB, throughput 0.996812
Reading from 28036: heap size 55 MB, throughput 0.997328
Numeric result:
Recommendation: 2 clients, utility 0.979286:
    h1: 159.374 MB (U(h) = 0.911586*h^0.0224201)
    h2: 40.6258 MB (U(h) = 0.938727*h^0.00571507)
Recommendation: 2 clients, utility 0.979286:
    h1: 159.374 MB (U(h) = 0.911586*h^0.0224201)
    h2: 40.6258 MB (U(h) = 0.938727*h^0.00571507)
Reading from 28036: heap size 56 MB, throughput 0.997439
Reading from 28038: heap size 5361 MB, throughput 0.999685
Reading from 28036: heap size 55 MB, throughput 0.997715
Reading from 28036: heap size 55 MB, throughput 0.997911
Reading from 28038: heap size 5361 MB, throughput 0.999642
Reading from 28036: heap size 54 MB, throughput 0.998041
Reading from 28036: heap size 55 MB, throughput 0.998164
Reading from 28036: heap size 54 MB, throughput 0.996109
Numeric result:
Recommendation: 2 clients, utility 0.979494:
    h1: 159.51 MB (U(h) = 0.911275*h^0.0225296)
    h2: 40.49 MB (U(h) = 0.938712*h^0.00571889)
Recommendation: 2 clients, utility 0.979494:
    h1: 159.51 MB (U(h) = 0.911275*h^0.0225296)
    h2: 40.4899 MB (U(h) = 0.938712*h^0.00571889)
Reading from 28038: heap size 5362 MB, throughput 0.999669
Reading from 28036: heap size 55 MB, throughput 0.997045
Reading from 28036: heap size 55 MB, throughput 0.994914
Reading from 28038: heap size 5362 MB, throughput 0.999477
Reading from 28036: heap size 55 MB, throughput 0.997427
Reading from 28036: heap size 55 MB, throughput 0.997756
Reading from 28038: heap size 5361 MB, throughput 0.999534
Reading from 28036: heap size 55 MB, throughput 0.997683
Numeric result:
Recommendation: 2 clients, utility 0.979283:
    h1: 156.925 MB (U(h) = 0.911354*h^0.0225016)
    h2: 43.0751 MB (U(h) = 0.936958*h^0.00617676)
Recommendation: 2 clients, utility 0.979283:
    h1: 156.924 MB (U(h) = 0.911354*h^0.0225016)
    h2: 43.0761 MB (U(h) = 0.936958*h^0.00617676)
Reading from 28036: heap size 55 MB, throughput 0.997668
Reading from 28036: heap size 55 MB, throughput 0.998058
Reading from 28038: heap size 5361 MB, throughput 0.999626
Reading from 28036: heap size 55 MB, throughput 0.998207
Reading from 28036: heap size 55 MB, throughput 0.998321
Reading from 28038: heap size 5362 MB, throughput 0.999679
Reading from 28036: heap size 54 MB, throughput 0.998378
Numeric result:
Recommendation: 2 clients, utility 0.979496:
    h1: 157.081 MB (U(h) = 0.91104*h^0.022613)
    h2: 42.9187 MB (U(h) = 0.936948*h^0.00617933)
Recommendation: 2 clients, utility 0.979496:
    h1: 157.077 MB (U(h) = 0.91104*h^0.022613)
    h2: 42.9234 MB (U(h) = 0.936948*h^0.00617933)
Reading from 28036: heap size 55 MB, throughput 0.998512
Reading from 28038: heap size 5362 MB, throughput 0.999557
Reading from 28036: heap size 55 MB, throughput 0.998381
Reading from 28036: heap size 55 MB, throughput 0.998459
Reading from 28038: heap size 5362 MB, throughput 0.99968
Reading from 28036: heap size 55 MB, throughput 0.998507
Numeric result:
Recommendation: 2 clients, utility 0.979541:
    h1: 157.11 MB (U(h) = 0.910972*h^0.0226369)
    h2: 42.8896 MB (U(h) = 0.936947*h^0.00617966)
Recommendation: 2 clients, utility 0.979541:
    h1: 157.11 MB (U(h) = 0.910972*h^0.0226369)
    h2: 42.8897 MB (U(h) = 0.936947*h^0.00617966)
Reading from 28036: heap size 55 MB, throughput 0.998301
Reading from 28038: heap size 5362 MB, throughput 0.999698
Reading from 28036: heap size 55 MB, throughput 0.998466
Reading from 28036: heap size 55 MB, throughput 0.998694
Reading from 28038: heap size 5362 MB, throughput 0.999734
Reading from 28036: heap size 55 MB, throughput 0.998374
Reading from 28036: heap size 55 MB, throughput 0.998468
Numeric result:
Recommendation: 2 clients, utility 0.979542:
    h1: 157.1 MB (U(h) = 0.91097*h^0.0226376)
    h2: 42.9003 MB (U(h) = 0.936938*h^0.00618182)
Recommendation: 2 clients, utility 0.979542:
    h1: 157.1 MB (U(h) = 0.91097*h^0.0226376)
    h2: 42.9004 MB (U(h) = 0.936938*h^0.00618182)
Reading from 28038: heap size 5361 MB, throughput 0.999701
Reading from 28036: heap size 56 MB, throughput 0.998501
Reading from 28036: heap size 56 MB, throughput 0.998544
Reading from 28038: heap size 5362 MB, throughput 0.999716
Reading from 28036: heap size 56 MB, throughput 0.998631
Reading from 28036: heap size 56 MB, throughput 0.998623
Reading from 28038: heap size 5362 MB, throughput 0.999706
Numeric result:
Recommendation: 2 clients, utility 0.979794:
    h1: 157.286 MB (U(h) = 0.910592*h^0.0227708)
    h2: 42.7143 MB (U(h) = 0.936931*h^0.0061837)
Recommendation: 2 clients, utility 0.979794:
    h1: 157.287 MB (U(h) = 0.910592*h^0.0227708)
    h2: 42.7132 MB (U(h) = 0.936931*h^0.0061837)
Reading from 28036: heap size 56 MB, throughput 0.998685
Reading from 28036: heap size 56 MB, throughput 0.99698
Reading from 28038: heap size 5363 MB, throughput 0.999715
Reading from 28036: heap size 56 MB, throughput 0.996045
Reading from 28036: heap size 56 MB, throughput 0.997713
Reading from 28038: heap size 5363 MB, throughput 0.999717
Numeric result:
Recommendation: 2 clients, utility 0.9795:
    h1: 155.295 MB (U(h) = 0.910879*h^0.0226699)
    h2: 44.7053 MB (U(h) = 0.935622*h^0.00652609)
Recommendation: 2 clients, utility 0.9795:
    h1: 155.295 MB (U(h) = 0.910879*h^0.0226699)
    h2: 44.7053 MB (U(h) = 0.935622*h^0.00652609)
Reading from 28036: heap size 57 MB, throughput 0.998237
Reading from 28038: heap size 5363 MB, throughput 0.999674
Reading from 28036: heap size 57 MB, throughput 0.998477
Reading from 28036: heap size 58 MB, throughput 0.998658
Reading from 28038: heap size 5363 MB, throughput 0.999728
Reading from 28036: heap size 58 MB, throughput 0.998695
Numeric result:
Recommendation: 2 clients, utility 0.979503:
    h1: 155.297 MB (U(h) = 0.910875*h^0.0226712)
    h2: 44.7027 MB (U(h) = 0.935622*h^0.00652597)
Recommendation: 2 clients, utility 0.979503:
    h1: 155.297 MB (U(h) = 0.910875*h^0.0226712)
    h2: 44.7028 MB (U(h) = 0.935622*h^0.00652597)
Reading from 28036: heap size 58 MB, throughput 0.998766
Reading from 28038: heap size 5363 MB, throughput 0.999002
Reading from 28036: heap size 58 MB, throughput 0.998771
Reading from 28036: heap size 59 MB, throughput 0.998912
Reading from 28038: heap size 5363 MB, throughput 0.999346
Reading from 28036: heap size 59 MB, throughput 0.998897
Reading from 28038: heap size 5363 MB, throughput 0.999494
Numeric result:
Recommendation: 2 clients, utility 0.979488:
    h1: 155.328 MB (U(h) = 0.910902*h^0.022662)
    h2: 44.6718 MB (U(h) = 0.935654*h^0.00651756)
Recommendation: 2 clients, utility 0.979488:
    h1: 155.328 MB (U(h) = 0.910902*h^0.022662)
    h2: 44.6721 MB (U(h) = 0.935654*h^0.00651756)
Reading from 28036: heap size 59 MB, throughput 0.997364
Reading from 28036: heap size 59 MB, throughput 0.99808
Reading from 28038: heap size 5363 MB, throughput 0.999618
Reading from 28036: heap size 60 MB, throughput 0.998624
Reading from 28036: heap size 60 MB, throughput 0.998697
Reading from 28038: heap size 5363 MB, throughput 0.999679
Numeric result:
Recommendation: 2 clients, utility 0.979276:
    h1: 155.126 MB (U(h) = 0.911233*h^0.0225472)
    h2: 44.8743 MB (U(h) = 0.935635*h^0.00652271)
Recommendation: 2 clients, utility 0.979276:
    h1: 155.124 MB (U(h) = 0.911233*h^0.0225472)
    h2: 44.8761 MB (U(h) = 0.935635*h^0.00652271)
Reading from 28036: heap size 61 MB, throughput 0.99892
Reading from 28038: heap size 5363 MB, throughput 0.999702
Reading from 28036: heap size 61 MB, throughput 0.999007
Reading from 28036: heap size 61 MB, throughput 0.998568
Reading from 28038: heap size 5363 MB, throughput 0.999623
Reading from 28036: heap size 61 MB, throughput 0.997204
Numeric result:
Recommendation: 2 clients, utility 0.979052:
    h1: 154.931 MB (U(h) = 0.911592*h^0.022424)
    h2: 45.0689 MB (U(h) = 0.93563*h^0.0065238)
Recommendation: 2 clients, utility 0.979052:
    h1: 154.927 MB (U(h) = 0.911592*h^0.022424)
    h2: 45.0729 MB (U(h) = 0.93563*h^0.0065238)
Reading from 28038: heap size 5363 MB, throughput 0.999661
Reading from 28036: heap size 62 MB, throughput 0.998058
Client 28036 died
Clients: 1
Reading from 28038: heap size 5363 MB, throughput 0.999708
Recommendation: one client; give it all the memory
Reading from 28038: heap size 5363 MB, throughput 0.999661
Reading from 28038: heap size 5363 MB, throughput 0.999649
Reading from 28038: heap size 5363 MB, throughput 0.999563
Recommendation: one client; give it all the memory
Reading from 28038: heap size 5363 MB, throughput 0.999621
Reading from 28038: heap size 5363 MB, throughput 0.99959
Recommendation: one client; give it all the memory
Reading from 28038: heap size 5363 MB, throughput 0.999657
Reading from 28038: heap size 5363 MB, throughput 0.999589
Reading from 28038: heap size 5363 MB, throughput 0.999583
Recommendation: one client; give it all the memory
Reading from 28038: heap size 5363 MB, throughput 0.999592
Reading from 28038: heap size 5363 MB, throughput 0.999607
Recommendation: one client; give it all the memory
Reading from 28038: heap size 5363 MB, throughput 0.999689
Reading from 28038: heap size 5363 MB, throughput 0.99962
Reading from 28038: heap size 5363 MB, throughput 0.999575
Recommendation: one client; give it all the memory
Reading from 28038: heap size 5363 MB, throughput 0.999609
Reading from 28038: heap size 5363 MB, throughput 0.999597
Recommendation: one client; give it all the memory
Reading from 28038: heap size 5363 MB, throughput 0.999576
Reading from 28038: heap size 5363 MB, throughput 0.999585
Recommendation: one client; give it all the memory
Reading from 28038: heap size 5363 MB, throughput 0.999579
Reading from 28038: heap size 5363 MB, throughput 0.999598
Reading from 28038: heap size 5363 MB, throughput 0.999695
Recommendation: one client; give it all the memory
Reading from 28038: heap size 5363 MB, throughput 0.999662
Reading from 28038: heap size 5363 MB, throughput 0.999663
Recommendation: one client; give it all the memory
Reading from 28038: heap size 5363 MB, throughput 0.999643
Client 28038 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
TimerThread: finished
ReadingThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
