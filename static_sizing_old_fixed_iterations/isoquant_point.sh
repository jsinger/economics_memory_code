#!/bin/bash
# Measure the running times of two concurrent benchmarks at a particular heap size point
#
# Usage: isoquant_point.sh benchmark1 benchmark2 heap1 heap2 max_heap1 max_heap2 runs iterations out_dir avg_times_file
#

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DACAPO="${DIR}/../dacapo-9.12-bach.jar"

function usage()
{
    echo "Usage: $(basename "${0}") benchmark1 benchmark2 heap1 heap2 max_heap1 max_heap2 runs iterations out_dir avg_times_file"
    exit 1
}

test -z "${1}" && usage
test -z "${2}" && usage
test -z "${3}" && usage
test -z "${4}" && usage
test -z "${5}" && usage
test -z "${6}" && usage
test -z "${7}" && usage
test -z "${8}" && usage
test -z "${9}" && usage
test ! -d "${9}" && usage
test -z "${10}" && usage


B1="${1}"
B2="${2}"
HEAP1="${3}"
HEAP2="${4}"
MAXHEAP1="${5}"
MAXHEAP2="${6}"
RUNS="${7}"
ITERATIONS="${8}"
OUTDIR="$(readlink -f "${9}")"
AVGTIMES="$(readlink -f "${10}")"

PRETTY="$(printf "${B1}_%0${#MAXHEAP1}d_${B2}_%0${#MAXHEAP2}d" "${HEAP1}" "${HEAP2}")"
RUNTIMES="${OUTDIR}/${PRETTY}_runtimes.txt"
RUNAVGTIME="${OUTDIR}/${PRETTY}_avgTime.txt"

i='0'
while (( i < RUNS )); do
    PREFIX="${PRETTY}_run$(printf "%0${#RUNS}d" "${i}")"
    TIMESTAMPS="${OUTDIR}/${PREFIX}_timestamps.txt"
    RAWFILE1="${OUTDIR}/${PREFIX}_${B1}_raw.txt"
    RAWFILE2="${OUTDIR}/${PREFIX}_${B2}_raw.txt"

    echo "${PREFIX}"

    TEMPDIR1="$(readlink -f "$(mktemp -d)")"
    TEMPDIR2="$(readlink -f "$(mktemp -d)")"

    STARTTIME="$(date '+%s')"

    java "-Xms${HEAP1}m" "-Xmx${HEAP1}m" -jar "${DACAPO}" --scratch-directory "${TEMPDIR1}" -s large -n "${ITERATIONS}" "${B1}" 2>"${RAWFILE1}" &
    java "-Xms${HEAP2}m" "-Xmx${HEAP2}m" -jar "${DACAPO}" --scratch-directory "${TEMPDIR2}" -s large -n "${ITERATIONS}" "${B2}" 2>"${RAWFILE2}" &

    wait

    ENDTIME="$(date '+%s')"

    TOTAL_TIME=$(( ENDTIME - STARTTIME ))

    echo "${STARTTIME}" >> "${TIMESTAMPS}"
    echo "${ENDTIME}" >> "${TIMESTAMPS}"
    echo "${TOTAL_TIME}" >> "${RUNTIMES}"

    rm -r "${TEMPDIR1}"
    rm -r "${TEMPDIR2}"


    i=$(( i + 1 ))
done

python "${DIR}/../average.py" < "${RUNTIMES}" > "${RUNAVGTIME}"
echo "${HEAP1} ${HEAP2} $(cat "${RUNAVGTIME}")" >> "${AVGTIMES}"
