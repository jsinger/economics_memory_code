economemd
    total memory: 1420 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub33_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run02_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 2298: heap size 9 MB, throughput 0.989634
Clients: 1
Client 2298 has a minimum heap size of 8 MB
Reading from 2297: heap size 9 MB, throughput 0.990666
Clients: 2
Client 2297 has a minimum heap size of 276 MB
Reading from 2297: heap size 9 MB, throughput 0.978863
Reading from 2298: heap size 9 MB, throughput 0.979396
Reading from 2297: heap size 9 MB, throughput 0.931558
Reading from 2298: heap size 9 MB, throughput 0.918871
Reading from 2297: heap size 9 MB, throughput 0.928466
Reading from 2298: heap size 9 MB, throughput 0.940905
Reading from 2298: heap size 11 MB, throughput 0.932465
Reading from 2297: heap size 11 MB, throughput 0.986235
Reading from 2298: heap size 11 MB, throughput 0.981366
Reading from 2297: heap size 11 MB, throughput 0.973106
Reading from 2298: heap size 17 MB, throughput 0.974859
Reading from 2297: heap size 17 MB, throughput 0.95105
Reading from 2298: heap size 17 MB, throughput 0.973738
Reading from 2297: heap size 17 MB, throughput 0.513321
Reading from 2298: heap size 25 MB, throughput 0.651818
Reading from 2298: heap size 28 MB, throughput 0.971893
Reading from 2298: heap size 31 MB, throughput 0.917536
Reading from 2298: heap size 34 MB, throughput 0.962412
Reading from 2297: heap size 30 MB, throughput 0.944733
Reading from 2298: heap size 42 MB, throughput 0.97941
Reading from 2298: heap size 42 MB, throughput 0.989594
Reading from 2297: heap size 31 MB, throughput 0.956072
Reading from 2298: heap size 51 MB, throughput 0.978166
Reading from 2298: heap size 52 MB, throughput 0.971577
Reading from 2297: heap size 33 MB, throughput 0.311749
Reading from 2298: heap size 64 MB, throughput 0.980975
Reading from 2297: heap size 45 MB, throughput 0.886934
Reading from 2298: heap size 64 MB, throughput 0.978603
Reading from 2297: heap size 48 MB, throughput 0.801816
Reading from 2298: heap size 74 MB, throughput 0.982677
Reading from 2298: heap size 74 MB, throughput 0.987501
Reading from 2297: heap size 49 MB, throughput 0.384952
Reading from 2298: heap size 84 MB, throughput 0.989054
Reading from 2297: heap size 68 MB, throughput 0.711776
Reading from 2297: heap size 69 MB, throughput 0.732182
Reading from 2298: heap size 85 MB, throughput 0.986303
Reading from 2297: heap size 72 MB, throughput 0.843477
Reading from 2297: heap size 75 MB, throughput 0.742296
Reading from 2298: heap size 95 MB, throughput 0.99352
Reading from 2298: heap size 96 MB, throughput 0.990554
Reading from 2297: heap size 79 MB, throughput 0.175138
Reading from 2297: heap size 106 MB, throughput 0.656156
Reading from 2298: heap size 105 MB, throughput 0.994456
Reading from 2297: heap size 109 MB, throughput 0.50491
Reading from 2297: heap size 112 MB, throughput 0.653014
Reading from 2298: heap size 106 MB, throughput 0.99323
Reading from 2298: heap size 113 MB, throughput 0.993187
Reading from 2298: heap size 113 MB, throughput 0.990754
Reading from 2297: heap size 119 MB, throughput 0.187542
Reading from 2297: heap size 146 MB, throughput 0.619313
Reading from 2298: heap size 121 MB, throughput 0.991365
Reading from 2297: heap size 153 MB, throughput 0.637783
Reading from 2297: heap size 154 MB, throughput 0.654934
Reading from 2298: heap size 121 MB, throughput 0.994424
Reading from 2297: heap size 157 MB, throughput 0.695015
Reading from 2297: heap size 162 MB, throughput 0.589579
Reading from 2298: heap size 127 MB, throughput 0.995934
Reading from 2298: heap size 128 MB, throughput 0.990548
Reading from 2298: heap size 135 MB, throughput 0.990459
Reading from 2297: heap size 169 MB, throughput 0.128961
Reading from 2297: heap size 203 MB, throughput 0.494935
Reading from 2298: heap size 135 MB, throughput 0.993854
Reading from 2298: heap size 141 MB, throughput 0.992005
Reading from 2297: heap size 211 MB, throughput 0.825121
Reading from 2298: heap size 141 MB, throughput 0.994521
Reading from 2298: heap size 147 MB, throughput 0.9825
Reading from 2297: heap size 213 MB, throughput 0.912172
Reading from 2298: heap size 148 MB, throughput 0.991215
Reading from 2297: heap size 215 MB, throughput 0.708377
Reading from 2298: heap size 156 MB, throughput 0.992534
Reading from 2297: heap size 224 MB, throughput 0.597292
Reading from 2297: heap size 229 MB, throughput 0.735903
Reading from 2298: heap size 156 MB, throughput 0.993142
Reading from 2297: heap size 235 MB, throughput 0.464868
Reading from 2298: heap size 164 MB, throughput 0.990637
Reading from 2297: heap size 241 MB, throughput 0.635874
Reading from 2298: heap size 165 MB, throughput 0.99605
Reading from 2298: heap size 171 MB, throughput 0.997097
Reading from 2297: heap size 245 MB, throughput 0.124911
Reading from 2297: heap size 277 MB, throughput 0.73868
Reading from 2298: heap size 172 MB, throughput 0.995696
Reading from 2297: heap size 279 MB, throughput 0.748502
Reading from 2297: heap size 271 MB, throughput 0.694524
Reading from 2297: heap size 276 MB, throughput 0.678565
Reading from 2298: heap size 178 MB, throughput 0.995029
Reading from 2297: heap size 272 MB, throughput 0.644045
Reading from 2297: heap size 274 MB, throughput 0.726966
Reading from 2298: heap size 178 MB, throughput 0.996827
Reading from 2297: heap size 272 MB, throughput 0.770896
Reading from 2298: heap size 184 MB, throughput 0.997248
Reading from 2298: heap size 184 MB, throughput 0.997407
Reading from 2297: heap size 274 MB, throughput 0.390861
Reading from 2298: heap size 189 MB, throughput 0.996848
Reading from 2297: heap size 315 MB, throughput 0.868493
Reading from 2297: heap size 316 MB, throughput 0.76111
Reading from 2298: heap size 189 MB, throughput 0.996581
Reading from 2297: heap size 320 MB, throughput 0.77836
Reading from 2297: heap size 321 MB, throughput 0.777843
Reading from 2298: heap size 193 MB, throughput 0.996804
Reading from 2297: heap size 324 MB, throughput 0.941225
Reading from 2298: heap size 193 MB, throughput 0.994103
Reading from 2297: heap size 325 MB, throughput 0.926676
Reading from 2298: heap size 198 MB, throughput 0.997027
Reading from 2297: heap size 328 MB, throughput 0.838624
Reading from 2297: heap size 329 MB, throughput 0.599083
Reading from 2297: heap size 331 MB, throughput 0.645705
Reading from 2298: heap size 198 MB, throughput 0.996775
Reading from 2297: heap size 331 MB, throughput 0.68756
Reading from 2297: heap size 335 MB, throughput 0.646372
Reading from 2298: heap size 202 MB, throughput 0.994991
Reading from 2297: heap size 337 MB, throughput 0.687253
Equal recommendation: 710 MB each
Reading from 2298: heap size 202 MB, throughput 0.996939
Reading from 2298: heap size 207 MB, throughput 0.997234
Reading from 2298: heap size 207 MB, throughput 0.995317
Reading from 2298: heap size 212 MB, throughput 0.953194
Reading from 2298: heap size 213 MB, throughput 0.996044
Reading from 2297: heap size 344 MB, throughput 0.972138
Reading from 2298: heap size 219 MB, throughput 0.996074
Reading from 2298: heap size 219 MB, throughput 0.996177
Reading from 2298: heap size 228 MB, throughput 0.997645
Reading from 2297: heap size 345 MB, throughput 0.965751
Reading from 2298: heap size 228 MB, throughput 0.996005
Reading from 2298: heap size 235 MB, throughput 0.996723
Reading from 2298: heap size 235 MB, throughput 0.996864
Reading from 2297: heap size 346 MB, throughput 0.966335
Reading from 2298: heap size 243 MB, throughput 0.997029
Reading from 2298: heap size 243 MB, throughput 0.996758
Reading from 2298: heap size 252 MB, throughput 0.996989
Reading from 2298: heap size 252 MB, throughput 0.996781
Reading from 2298: heap size 260 MB, throughput 0.996225
Reading from 2297: heap size 349 MB, throughput 0.972979
Reading from 2298: heap size 260 MB, throughput 0.997482
Reading from 2298: heap size 268 MB, throughput 0.997859
Reading from 2298: heap size 269 MB, throughput 0.99664
Reading from 2297: heap size 350 MB, throughput 0.963505
Reading from 2298: heap size 277 MB, throughput 0.997879
Reading from 2298: heap size 277 MB, throughput 0.997018
Reading from 2298: heap size 285 MB, throughput 0.99831
Reading from 2298: heap size 285 MB, throughput 0.996639
Reading from 2297: heap size 353 MB, throughput 0.974301
Reading from 2298: heap size 292 MB, throughput 0.997841
Reading from 2298: heap size 292 MB, throughput 0.998152
Equal recommendation: 710 MB each
Reading from 2298: heap size 299 MB, throughput 0.997802
Reading from 2297: heap size 349 MB, throughput 0.955321
Reading from 2298: heap size 300 MB, throughput 0.997938
Reading from 2298: heap size 307 MB, throughput 0.998143
Reading from 2298: heap size 307 MB, throughput 0.997291
Reading from 2297: heap size 353 MB, throughput 0.959525
Reading from 2298: heap size 313 MB, throughput 0.996758
Reading from 2298: heap size 314 MB, throughput 0.996793
Reading from 2298: heap size 322 MB, throughput 0.997366
Reading from 2298: heap size 323 MB, throughput 0.997014
Reading from 2297: heap size 356 MB, throughput 0.96858
Reading from 2298: heap size 332 MB, throughput 0.997468
Reading from 2298: heap size 332 MB, throughput 0.99636
Reading from 2298: heap size 341 MB, throughput 0.99628
Reading from 2297: heap size 356 MB, throughput 0.963274
Reading from 2298: heap size 341 MB, throughput 0.996947
Reading from 2298: heap size 352 MB, throughput 0.997948
Reading from 2298: heap size 352 MB, throughput 0.996525
Reading from 2297: heap size 359 MB, throughput 0.958081
Reading from 2298: heap size 363 MB, throughput 0.997859
Reading from 2298: heap size 363 MB, throughput 0.997115
Reading from 2297: heap size 360 MB, throughput 0.959027
Reading from 2298: heap size 373 MB, throughput 0.995839
Reading from 2298: heap size 373 MB, throughput 0.996493
Reading from 2298: heap size 385 MB, throughput 0.997511
Reading from 2297: heap size 364 MB, throughput 0.956503
Reading from 2298: heap size 385 MB, throughput 0.997976
Equal recommendation: 710 MB each
Reading from 2298: heap size 396 MB, throughput 0.998634
Reading from 2298: heap size 397 MB, throughput 0.996806
Reading from 2298: heap size 407 MB, throughput 0.997248
Reading from 2297: heap size 366 MB, throughput 0.981202
Reading from 2297: heap size 369 MB, throughput 0.836233
Reading from 2298: heap size 407 MB, throughput 0.997044
Reading from 2297: heap size 370 MB, throughput 0.12008
Reading from 2298: heap size 419 MB, throughput 0.997492
Reading from 2297: heap size 413 MB, throughput 0.739599
Reading from 2297: heap size 416 MB, throughput 0.776494
Reading from 2298: heap size 419 MB, throughput 0.995969
Reading from 2298: heap size 432 MB, throughput 0.9963
Reading from 2298: heap size 432 MB, throughput 0.997014
Reading from 2297: heap size 425 MB, throughput 0.975091
Reading from 2298: heap size 445 MB, throughput 0.998168
Reading from 2298: heap size 446 MB, throughput 0.997522
Reading from 2297: heap size 424 MB, throughput 0.974027
Reading from 2298: heap size 459 MB, throughput 0.997498
Reading from 2298: heap size 459 MB, throughput 0.996897
Reading from 2298: heap size 472 MB, throughput 0.997621
Reading from 2297: heap size 425 MB, throughput 0.981645
Reading from 2298: heap size 472 MB, throughput 0.997632
Reading from 2298: heap size 478 MB, throughput 0.998072
Equal recommendation: 710 MB each
Reading from 2298: heap size 478 MB, throughput 0.997313
Reading from 2297: heap size 429 MB, throughput 0.975202
Reading from 2298: heap size 479 MB, throughput 0.998053
Reading from 2298: heap size 479 MB, throughput 0.99818
Reading from 2297: heap size 429 MB, throughput 0.975993
Reading from 2298: heap size 479 MB, throughput 0.998787
Reading from 2298: heap size 479 MB, throughput 0.998427
Reading from 2298: heap size 479 MB, throughput 0.997971
Reading from 2297: heap size 431 MB, throughput 0.968463
Reading from 2298: heap size 479 MB, throughput 0.998655
Reading from 2298: heap size 479 MB, throughput 0.998483
Reading from 2297: heap size 437 MB, throughput 0.980744
Reading from 2298: heap size 478 MB, throughput 0.998649
Reading from 2298: heap size 479 MB, throughput 0.998289
Reading from 2298: heap size 479 MB, throughput 0.998224
Reading from 2297: heap size 438 MB, throughput 0.974727
Reading from 2298: heap size 480 MB, throughput 0.998551
Reading from 2298: heap size 480 MB, throughput 0.998198
Reading from 2298: heap size 480 MB, throughput 0.997939
Reading from 2297: heap size 442 MB, throughput 0.975908
Equal recommendation: 710 MB each
Reading from 2298: heap size 480 MB, throughput 0.998484
Reading from 2298: heap size 480 MB, throughput 0.998286
Reading from 2298: heap size 479 MB, throughput 0.998477
Reading from 2297: heap size 445 MB, throughput 0.981222
Reading from 2298: heap size 479 MB, throughput 0.998076
Reading from 2297: heap size 448 MB, throughput 0.959447
Reading from 2298: heap size 479 MB, throughput 0.998092
Reading from 2297: heap size 450 MB, throughput 0.732552
Reading from 2297: heap size 462 MB, throughput 0.798697
Reading from 2298: heap size 480 MB, throughput 0.998548
Reading from 2297: heap size 461 MB, throughput 0.970632
Reading from 2298: heap size 480 MB, throughput 0.998494
Reading from 2298: heap size 480 MB, throughput 0.998809
Reading from 2298: heap size 480 MB, throughput 0.998362
Reading from 2297: heap size 470 MB, throughput 0.983883
Reading from 2298: heap size 480 MB, throughput 0.997885
Reading from 2298: heap size 480 MB, throughput 0.998378
Reading from 2298: heap size 480 MB, throughput 0.998253
Reading from 2297: heap size 471 MB, throughput 0.985651
Reading from 2298: heap size 480 MB, throughput 0.997911
Reading from 2298: heap size 480 MB, throughput 0.998372
Equal recommendation: 710 MB each
Reading from 2298: heap size 480 MB, throughput 0.998332
Reading from 2297: heap size 475 MB, throughput 0.98324
Reading from 2298: heap size 480 MB, throughput 0.998495
Reading from 2298: heap size 480 MB, throughput 0.99806
Reading from 2298: heap size 480 MB, throughput 0.997617
Reading from 2298: heap size 480 MB, throughput 0.998458
Reading from 2297: heap size 477 MB, throughput 0.986593
Reading from 2298: heap size 480 MB, throughput 0.998395
Reading from 2298: heap size 480 MB, throughput 0.998397
Reading from 2298: heap size 480 MB, throughput 0.998073
Reading from 2297: heap size 477 MB, throughput 0.983645
Reading from 2298: heap size 480 MB, throughput 0.998198
Reading from 2298: heap size 481 MB, throughput 0.998424
Reading from 2298: heap size 481 MB, throughput 0.99838
Reading from 2297: heap size 480 MB, throughput 0.981145
Reading from 2298: heap size 481 MB, throughput 0.998139
Reading from 2298: heap size 481 MB, throughput 0.99856
Reading from 2298: heap size 481 MB, throughput 0.998269
Equal recommendation: 710 MB each
Reading from 2297: heap size 478 MB, throughput 0.978869
Reading from 2298: heap size 481 MB, throughput 0.998593
Reading from 2298: heap size 481 MB, throughput 0.998061
Reading from 2298: heap size 481 MB, throughput 0.998422
Reading from 2298: heap size 481 MB, throughput 0.998373
Reading from 2297: heap size 480 MB, throughput 0.989443
Reading from 2298: heap size 481 MB, throughput 0.998343
Reading from 2297: heap size 481 MB, throughput 0.882092
Reading from 2297: heap size 483 MB, throughput 0.847672
Reading from 2298: heap size 481 MB, throughput 0.998613
Reading from 2297: heap size 490 MB, throughput 0.97434
Reading from 2298: heap size 481 MB, throughput 0.998207
Reading from 2298: heap size 481 MB, throughput 0.997885
Reading from 2298: heap size 481 MB, throughput 0.998723
Reading from 2298: heap size 481 MB, throughput 0.998408
Reading from 2297: heap size 490 MB, throughput 0.989457
Reading from 2298: heap size 481 MB, throughput 0.998244
Reading from 2298: heap size 481 MB, throughput 0.9982
Reading from 2298: heap size 481 MB, throughput 0.998601
Reading from 2297: heap size 498 MB, throughput 0.986789
Reading from 2298: heap size 481 MB, throughput 0.998275
Equal recommendation: 710 MB each
Reading from 2298: heap size 481 MB, throughput 0.997948
Reading from 2298: heap size 481 MB, throughput 0.997887
Reading from 2298: heap size 481 MB, throughput 0.998476
Reading from 2297: heap size 499 MB, throughput 0.985754
Reading from 2298: heap size 481 MB, throughput 0.998538
Reading from 2298: heap size 481 MB, throughput 0.998162
Reading from 2298: heap size 481 MB, throughput 0.998405
Reading from 2298: heap size 481 MB, throughput 0.997725
Reading from 2297: heap size 500 MB, throughput 0.984663
Reading from 2298: heap size 481 MB, throughput 0.998553
Reading from 2298: heap size 481 MB, throughput 0.997891
Reading from 2298: heap size 481 MB, throughput 0.997353
Reading from 2298: heap size 481 MB, throughput 0.998519
Reading from 2297: heap size 503 MB, throughput 0.984403
Reading from 2298: heap size 481 MB, throughput 0.998193
Reading from 2298: heap size 481 MB, throughput 0.998564
Reading from 2298: heap size 481 MB, throughput 0.998001
Equal recommendation: 710 MB each
Reading from 2298: heap size 481 MB, throughput 0.997657
Reading from 2297: heap size 503 MB, throughput 0.977689
Reading from 2298: heap size 481 MB, throughput 0.998558
Reading from 2298: heap size 481 MB, throughput 0.998397
Reading from 2298: heap size 481 MB, throughput 0.998303
Reading from 2297: heap size 505 MB, throughput 0.989352
Reading from 2297: heap size 509 MB, throughput 0.880191
Reading from 2298: heap size 481 MB, throughput 0.997461
Reading from 2297: heap size 509 MB, throughput 0.828668
Reading from 2298: heap size 481 MB, throughput 0.997956
Reading from 2298: heap size 481 MB, throughput 0.998056
Reading from 2298: heap size 481 MB, throughput 0.998151
Reading from 2298: heap size 481 MB, throughput 0.997995
Reading from 2297: heap size 519 MB, throughput 0.989489
Reading from 2298: heap size 481 MB, throughput 0.998448
Reading from 2298: heap size 481 MB, throughput 0.996992
Reading from 2298: heap size 481 MB, throughput 0.998507
Reading from 2298: heap size 481 MB, throughput 0.997875
Reading from 2297: heap size 519 MB, throughput 0.990592
Reading from 2298: heap size 481 MB, throughput 0.99769
Equal recommendation: 710 MB each
Reading from 2298: heap size 481 MB, throughput 0.998482
Reading from 2298: heap size 481 MB, throughput 0.998549
Reading from 2298: heap size 481 MB, throughput 0.998322
Reading from 2297: heap size 523 MB, throughput 0.988092
Reading from 2298: heap size 481 MB, throughput 0.997738
Reading from 2298: heap size 481 MB, throughput 0.998374
Reading from 2298: heap size 481 MB, throughput 0.998382
Reading from 2298: heap size 481 MB, throughput 0.998203
Reading from 2298: heap size 481 MB, throughput 0.998087
Reading from 2297: heap size 526 MB, throughput 0.988248
Reading from 2298: heap size 481 MB, throughput 0.99865
Reading from 2298: heap size 481 MB, throughput 0.998189
Reading from 2298: heap size 481 MB, throughput 0.998479
Reading from 2298: heap size 481 MB, throughput 0.997853
Reading from 2298: heap size 481 MB, throughput 0.997895
Reading from 2297: heap size 527 MB, throughput 0.984441
Reading from 2298: heap size 481 MB, throughput 0.998636
Equal recommendation: 710 MB each
Reading from 2298: heap size 481 MB, throughput 0.998479
Reading from 2298: heap size 481 MB, throughput 0.99791
Reading from 2298: heap size 458 MB, throughput 0.976277
Reading from 2297: heap size 530 MB, throughput 0.987905
Reading from 2298: heap size 468 MB, throughput 0.99651
Reading from 2298: heap size 481 MB, throughput 0.998772
Reading from 2297: heap size 534 MB, throughput 0.968577
Reading from 2297: heap size 536 MB, throughput 0.870959
Reading from 2298: heap size 483 MB, throughput 0.998761
Reading from 2298: heap size 483 MB, throughput 0.99763
Reading from 2298: heap size 483 MB, throughput 0.99803
Reading from 2297: heap size 543 MB, throughput 0.992723
Reading from 2298: heap size 482 MB, throughput 0.998415
Reading from 2298: heap size 483 MB, throughput 0.998564
Reading from 2298: heap size 483 MB, throughput 0.998241
Reading from 2298: heap size 483 MB, throughput 0.998114
Reading from 2298: heap size 483 MB, throughput 0.99823
Reading from 2297: heap size 544 MB, throughput 0.988758
Reading from 2298: heap size 483 MB, throughput 0.998436
Equal recommendation: 710 MB each
Reading from 2298: heap size 483 MB, throughput 0.998316
Reading from 2298: heap size 483 MB, throughput 0.997984
Reading from 2298: heap size 483 MB, throughput 0.997833
Reading from 2298: heap size 483 MB, throughput 0.998222
Reading from 2297: heap size 548 MB, throughput 0.991586
Reading from 2298: heap size 481 MB, throughput 0.998148
Reading from 2298: heap size 482 MB, throughput 0.998491
Reading from 2298: heap size 482 MB, throughput 0.998306
Reading from 2298: heap size 483 MB, throughput 0.99817
Reading from 2298: heap size 483 MB, throughput 0.998246
Reading from 2297: heap size 550 MB, throughput 0.987882
Reading from 2298: heap size 483 MB, throughput 0.998491
Reading from 2298: heap size 483 MB, throughput 0.998302
Reading from 2298: heap size 482 MB, throughput 0.998624
Reading from 2298: heap size 483 MB, throughput 0.998323
Reading from 2297: heap size 550 MB, throughput 0.985955
Reading from 2298: heap size 484 MB, throughput 0.998362
Equal recommendation: 710 MB each
Reading from 2298: heap size 484 MB, throughput 0.998081
Reading from 2298: heap size 484 MB, throughput 0.998024
Reading from 2298: heap size 484 MB, throughput 0.998415
Reading from 2298: heap size 484 MB, throughput 0.998049
Reading from 2298: heap size 484 MB, throughput 0.998108
Reading from 2297: heap size 552 MB, throughput 0.990301
Reading from 2298: heap size 484 MB, throughput 0.998341
Reading from 2297: heap size 552 MB, throughput 0.962652
Reading from 2297: heap size 555 MB, throughput 0.913663
Reading from 2298: heap size 484 MB, throughput 0.998394
Reading from 2298: heap size 484 MB, throughput 0.998351
Reading from 2298: heap size 484 MB, throughput 0.997641
Reading from 2298: heap size 484 MB, throughput 0.998269
Reading from 2298: heap size 484 MB, throughput 0.998483
Reading from 2297: heap size 563 MB, throughput 0.991023
Reading from 2298: heap size 484 MB, throughput 0.998553
Reading from 2298: heap size 484 MB, throughput 0.998541
Reading from 2298: heap size 484 MB, throughput 0.998041
Equal recommendation: 710 MB each
Reading from 2298: heap size 484 MB, throughput 0.998322
Reading from 2297: heap size 563 MB, throughput 0.989541
Reading from 2298: heap size 484 MB, throughput 0.9985
Reading from 2298: heap size 484 MB, throughput 0.998408
Reading from 2298: heap size 484 MB, throughput 0.997956
Reading from 2298: heap size 484 MB, throughput 0.998144
Reading from 2298: heap size 484 MB, throughput 0.998262
Reading from 2298: heap size 484 MB, throughput 0.998153
Reading from 2297: heap size 566 MB, throughput 0.988878
Reading from 2298: heap size 484 MB, throughput 0.997904
Reading from 2298: heap size 484 MB, throughput 0.998346
Reading from 2298: heap size 484 MB, throughput 0.998639
Reading from 2298: heap size 484 MB, throughput 0.9982
Reading from 2297: heap size 568 MB, throughput 0.988935
Reading from 2298: heap size 484 MB, throughput 0.998561
Reading from 2298: heap size 484 MB, throughput 0.997901
Reading from 2298: heap size 484 MB, throughput 0.998205
Equal recommendation: 710 MB each
Reading from 2298: heap size 483 MB, throughput 0.998227
Reading from 2298: heap size 483 MB, throughput 0.998289
Reading from 2298: heap size 483 MB, throughput 0.99814
Reading from 2297: heap size 570 MB, throughput 0.985108
Reading from 2298: heap size 484 MB, throughput 0.998494
Reading from 2298: heap size 484 MB, throughput 0.9986
Reading from 2298: heap size 484 MB, throughput 0.998168
Reading from 2297: heap size 571 MB, throughput 0.986216
Reading from 2297: heap size 575 MB, throughput 0.87251
Reading from 2298: heap size 484 MB, throughput 0.997774
Reading from 2298: heap size 484 MB, throughput 0.997977
Reading from 2298: heap size 484 MB, throughput 0.998583
Reading from 2298: heap size 484 MB, throughput 0.998454
Reading from 2297: heap size 577 MB, throughput 0.990514
Reading from 2298: heap size 484 MB, throughput 0.998842
Reading from 2298: heap size 484 MB, throughput 0.99815
Reading from 2298: heap size 484 MB, throughput 0.998779
Reading from 2298: heap size 484 MB, throughput 0.99855
Equal recommendation: 710 MB each
Reading from 2298: heap size 484 MB, throughput 0.998367
Reading from 2298: heap size 484 MB, throughput 0.99834
Reading from 2297: heap size 586 MB, throughput 0.99055
Reading from 2298: heap size 484 MB, throughput 0.998411
Reading from 2298: heap size 484 MB, throughput 0.998599
Reading from 2298: heap size 484 MB, throughput 0.998493
Reading from 2298: heap size 484 MB, throughput 0.998136
Reading from 2298: heap size 484 MB, throughput 0.998228
Reading from 2298: heap size 484 MB, throughput 0.998684
Reading from 2297: heap size 588 MB, throughput 0.989852
Reading from 2298: heap size 484 MB, throughput 0.998782
Reading from 2298: heap size 484 MB, throughput 0.998438
Reading from 2298: heap size 484 MB, throughput 0.998219
Reading from 2298: heap size 484 MB, throughput 0.998432
Reading from 2298: heap size 484 MB, throughput 0.998406
Reading from 2297: heap size 588 MB, throughput 0.987674
Reading from 2298: heap size 484 MB, throughput 0.997853
Equal recommendation: 710 MB each
Reading from 2298: heap size 484 MB, throughput 0.99776
Reading from 2298: heap size 484 MB, throughput 0.9983
Reading from 2298: heap size 484 MB, throughput 0.998402
Reading from 2298: heap size 484 MB, throughput 0.998313
Reading from 2297: heap size 591 MB, throughput 0.986455
Reading from 2298: heap size 484 MB, throughput 0.998117
Reading from 2298: heap size 484 MB, throughput 0.998259
Reading from 2298: heap size 484 MB, throughput 0.998171
Reading from 2297: heap size 596 MB, throughput 0.876507
Reading from 2297: heap size 624 MB, throughput 0.98389
Reading from 2298: heap size 484 MB, throughput 0.998713
Reading from 2298: heap size 484 MB, throughput 0.998493
Reading from 2298: heap size 484 MB, throughput 0.998061
Reading from 2298: heap size 484 MB, throughput 0.998143
Reading from 2298: heap size 484 MB, throughput 0.998274
Reading from 2297: heap size 626 MB, throughput 0.995955
Reading from 2298: heap size 484 MB, throughput 0.998676
Reading from 2298: heap size 483 MB, throughput 0.998561
Equal recommendation: 710 MB each
Reading from 2298: heap size 484 MB, throughput 0.998558
Reading from 2298: heap size 484 MB, throughput 0.99849
Reading from 2298: heap size 484 MB, throughput 0.998424
Reading from 2298: heap size 484 MB, throughput 0.997298
Reading from 2297: heap size 630 MB, throughput 0.993727
Reading from 2298: heap size 484 MB, throughput 0.997988
Reading from 2298: heap size 483 MB, throughput 0.998625
Reading from 2298: heap size 484 MB, throughput 0.998837
Reading from 2298: heap size 484 MB, throughput 0.998427
Reading from 2298: heap size 484 MB, throughput 0.998101
Reading from 2298: heap size 484 MB, throughput 0.998178
Reading from 2297: heap size 635 MB, throughput 0.990208
Reading from 2298: heap size 484 MB, throughput 0.998714
Reading from 2298: heap size 483 MB, throughput 0.998585
Reading from 2298: heap size 484 MB, throughput 0.997779
Reading from 2298: heap size 484 MB, throughput 0.998427
Equal recommendation: 710 MB each
Reading from 2298: heap size 484 MB, throughput 0.998282
Reading from 2298: heap size 484 MB, throughput 0.998343
Reading from 2297: heap size 636 MB, throughput 0.987952
Reading from 2298: heap size 484 MB, throughput 0.997845
Reading from 2298: heap size 484 MB, throughput 0.998206
Reading from 2298: heap size 484 MB, throughput 0.998537
Reading from 2298: heap size 484 MB, throughput 0.998746
Reading from 2298: heap size 484 MB, throughput 0.99835
Reading from 2297: heap size 637 MB, throughput 0.990996
Reading from 2298: heap size 484 MB, throughput 0.997849
Reading from 2297: heap size 637 MB, throughput 0.898747
Reading from 2298: heap size 484 MB, throughput 0.998541
Reading from 2298: heap size 483 MB, throughput 0.998345
Reading from 2297: heap size 636 MB, throughput 0.981095
Reading from 2298: heap size 484 MB, throughput 0.998363
Reading from 2298: heap size 484 MB, throughput 0.998211
Reading from 2298: heap size 484 MB, throughput 0.998433
Reading from 2298: heap size 484 MB, throughput 0.99837
Equal recommendation: 710 MB each
Reading from 2298: heap size 484 MB, throughput 0.998345
Reading from 2298: heap size 484 MB, throughput 0.998055
Reading from 2297: heap size 642 MB, throughput 0.987764
Reading from 2298: heap size 484 MB, throughput 0.998152
Reading from 2298: heap size 484 MB, throughput 0.99824
Reading from 2298: heap size 484 MB, throughput 0.998531
Reading from 2298: heap size 484 MB, throughput 0.998224
Reading from 2298: heap size 484 MB, throughput 0.998355
Reading from 2298: heap size 484 MB, throughput 0.998743
Reading from 2297: heap size 645 MB, throughput 0.989292
Reading from 2298: heap size 484 MB, throughput 0.998262
Reading from 2298: heap size 484 MB, throughput 0.998231
Reading from 2298: heap size 484 MB, throughput 0.99794
Reading from 2298: heap size 484 MB, throughput 0.998678
Reading from 2298: heap size 484 MB, throughput 0.998092
Reading from 2298: heap size 484 MB, throughput 0.998478
Reading from 2297: heap size 647 MB, throughput 0.986851
Equal recommendation: 710 MB each
Reading from 2298: heap size 484 MB, throughput 0.998005
Reading from 2298: heap size 484 MB, throughput 0.998478
Reading from 2298: heap size 484 MB, throughput 0.998511
Reading from 2298: heap size 484 MB, throughput 0.998569
Reading from 2298: heap size 484 MB, throughput 0.9986
Reading from 2297: heap size 653 MB, throughput 0.978066
Reading from 2298: heap size 484 MB, throughput 0.998058
Reading from 2298: heap size 484 MB, throughput 0.998354
Reading from 2297: heap size 656 MB, throughput 0.974589
Reading from 2298: heap size 484 MB, throughput 0.997868
Reading from 2297: heap size 635 MB, throughput 0.956502
Reading from 2298: heap size 484 MB, throughput 0.997835
Reading from 2298: heap size 484 MB, throughput 0.998258
Reading from 2298: heap size 484 MB, throughput 0.998603
Reading from 2298: heap size 484 MB, throughput 0.998651
Reading from 2298: heap size 484 MB, throughput 0.998345
Reading from 2297: heap size 645 MB, throughput 0.988834
Reading from 2298: heap size 484 MB, throughput 0.997952
Equal recommendation: 710 MB each
Reading from 2298: heap size 484 MB, throughput 0.997999
Reading from 2298: heap size 484 MB, throughput 0.998598
Reading from 2298: heap size 484 MB, throughput 0.998675
Reading from 2298: heap size 484 MB, throughput 0.998534
Reading from 2298: heap size 484 MB, throughput 0.998047
Reading from 2297: heap size 638 MB, throughput 0.988399
Reading from 2298: heap size 484 MB, throughput 0.998715
Reading from 2298: heap size 484 MB, throughput 0.998445
Reading from 2298: heap size 484 MB, throughput 0.998334
Reading from 2298: heap size 484 MB, throughput 0.998286
Reading from 2298: heap size 484 MB, throughput 0.998215
Reading from 2298: heap size 484 MB, throughput 0.998674
Reading from 2297: heap size 643 MB, throughput 0.985732
Reading from 2298: heap size 484 MB, throughput 0.998375
Reading from 2298: heap size 484 MB, throughput 0.996945
Reading from 2298: heap size 484 MB, throughput 0.9985
Equal recommendation: 710 MB each
Reading from 2298: heap size 484 MB, throughput 0.998457
Reading from 2298: heap size 484 MB, throughput 0.998798
Reading from 2297: heap size 647 MB, throughput 0.987003
Reading from 2298: heap size 484 MB, throughput 0.998326
Reading from 2298: heap size 484 MB, throughput 0.998118
Reading from 2298: heap size 484 MB, throughput 0.998452
Reading from 2298: heap size 484 MB, throughput 0.998166
Reading from 2298: heap size 484 MB, throughput 0.998234
Reading from 2298: heap size 484 MB, throughput 0.998128
Reading from 2297: heap size 647 MB, throughput 0.98937
Reading from 2297: heap size 651 MB, throughput 0.900402
Reading from 2298: heap size 484 MB, throughput 0.998675
Reading from 2298: heap size 484 MB, throughput 0.998513
Reading from 2298: heap size 484 MB, throughput 0.998301
Reading from 2298: heap size 484 MB, throughput 0.998008
Reading from 2297: heap size 652 MB, throughput 0.991601
Reading from 2298: heap size 484 MB, throughput 0.998655
Equal recommendation: 710 MB each
Reading from 2298: heap size 484 MB, throughput 0.998468
Reading from 2298: heap size 484 MB, throughput 0.998707
Reading from 2298: heap size 484 MB, throughput 0.998451
Reading from 2298: heap size 484 MB, throughput 0.997821
Reading from 2297: heap size 653 MB, throughput 0.990953
Reading from 2298: heap size 484 MB, throughput 0.998621
Reading from 2298: heap size 484 MB, throughput 0.998524
Reading from 2298: heap size 484 MB, throughput 0.998332
Reading from 2298: heap size 484 MB, throughput 0.998404
Reading from 2298: heap size 484 MB, throughput 0.99854
Reading from 2298: heap size 484 MB, throughput 0.998515
Reading from 2297: heap size 621 MB, throughput 0.989284
Reading from 2298: heap size 484 MB, throughput 0.998029
Reading from 2298: heap size 484 MB, throughput 0.998068
Reading from 2298: heap size 484 MB, throughput 0.998508
Reading from 2298: heap size 484 MB, throughput 0.9985
Equal recommendation: 710 MB each
Reading from 2298: heap size 484 MB, throughput 0.998466
Reading from 2298: heap size 484 MB, throughput 0.998132
Reading from 2297: heap size 654 MB, throughput 0.990727
Reading from 2298: heap size 484 MB, throughput 0.998169
Reading from 2298: heap size 484 MB, throughput 0.998615
Reading from 2298: heap size 484 MB, throughput 0.998381
Reading from 2298: heap size 484 MB, throughput 0.998245
Reading from 2298: heap size 484 MB, throughput 0.99835
Reading from 2298: heap size 484 MB, throughput 0.998384
Reading from 2298: heap size 484 MB, throughput 0.994892
Reading from 2297: heap size 652 MB, throughput 0.994817
Reading from 2297: heap size 652 MB, throughput 0.9266
Reading from 2298: heap size 484 MB, throughput 0.998128
Reading from 2298: heap size 484 MB, throughput 0.997938
Reading from 2298: heap size 484 MB, throughput 0.998491
Reading from 2297: heap size 652 MB, throughput 0.984993
Reading from 2298: heap size 484 MB, throughput 0.99833
Reading from 2298: heap size 484 MB, throughput 0.99857
Equal recommendation: 710 MB each
Reading from 2298: heap size 484 MB, throughput 0.998002
Reading from 2298: heap size 484 MB, throughput 0.998033
Reading from 2298: heap size 484 MB, throughput 0.997987
Reading from 2297: heap size 656 MB, throughput 0.990397
Reading from 2298: heap size 484 MB, throughput 0.998501
Reading from 2298: heap size 484 MB, throughput 0.997653
Reading from 2298: heap size 484 MB, throughput 0.998293
Reading from 2298: heap size 484 MB, throughput 0.998411
Reading from 2298: heap size 484 MB, throughput 0.998587
Reading from 2298: heap size 484 MB, throughput 0.998187
Reading from 2297: heap size 626 MB, throughput 0.990152
Reading from 2298: heap size 484 MB, throughput 0.997991
Reading from 2298: heap size 484 MB, throughput 0.998791
Reading from 2298: heap size 484 MB, throughput 0.99861
Reading from 2298: heap size 484 MB, throughput 0.997607
Reading from 2298: heap size 484 MB, throughput 0.998371
Equal recommendation: 710 MB each
Reading from 2298: heap size 484 MB, throughput 0.998506
Reading from 2297: heap size 657 MB, throughput 0.98912
Reading from 2298: heap size 484 MB, throughput 0.998407
Reading from 2298: heap size 484 MB, throughput 0.998081
Reading from 2298: heap size 484 MB, throughput 0.997896
Reading from 2298: heap size 484 MB, throughput 0.998349
Reading from 2298: heap size 484 MB, throughput 0.998454
Reading from 2298: heap size 484 MB, throughput 0.998515
Reading from 2298: heap size 484 MB, throughput 0.99811
Reading from 2297: heap size 655 MB, throughput 0.992053
Reading from 2298: heap size 484 MB, throughput 0.998107
Reading from 2297: heap size 657 MB, throughput 0.972947
Client 2297 died
Clients: 1
Reading from 2298: heap size 484 MB, throughput 0.998231
Reading from 2298: heap size 484 MB, throughput 0.998158
Reading from 2298: heap size 484 MB, throughput 0.998366
Reading from 2298: heap size 484 MB, throughput 0.997677
Reading from 2298: heap size 484 MB, throughput 0.997851
Recommendation: one client; give it all the memory
Reading from 2298: heap size 484 MB, throughput 0.997814
Reading from 2298: heap size 484 MB, throughput 0.998
Reading from 2298: heap size 484 MB, throughput 0.998078
Reading from 2298: heap size 484 MB, throughput 0.997766
Reading from 2298: heap size 484 MB, throughput 0.997956
Reading from 2298: heap size 484 MB, throughput 0.998123
Reading from 2298: heap size 484 MB, throughput 0.997699
Reading from 2298: heap size 484 MB, throughput 0.997615
Reading from 2298: heap size 484 MB, throughput 0.997896
Reading from 2298: heap size 484 MB, throughput 0.998005
Reading from 2298: heap size 484 MB, throughput 0.997913
Reading from 2298: heap size 484 MB, throughput 0.998187
Reading from 2298: heap size 484 MB, throughput 0.997661
Reading from 2298: heap size 484 MB, throughput 0.998141
Recommendation: one client; give it all the memory
Reading from 2298: heap size 484 MB, throughput 0.998061
Reading from 2298: heap size 484 MB, throughput 0.997652
Reading from 2298: heap size 484 MB, throughput 0.997723
Reading from 2298: heap size 484 MB, throughput 0.997796
Reading from 2298: heap size 484 MB, throughput 0.997936
Reading from 2298: heap size 484 MB, throughput 0.997631
Reading from 2298: heap size 484 MB, throughput 0.997519
Reading from 2298: heap size 484 MB, throughput 0.998479
Reading from 2298: heap size 484 MB, throughput 0.998333
Reading from 2298: heap size 484 MB, throughput 0.998178
Reading from 2298: heap size 484 MB, throughput 0.997592
Reading from 2298: heap size 484 MB, throughput 0.998113
Reading from 2298: heap size 484 MB, throughput 0.998447
Reading from 2298: heap size 484 MB, throughput 0.997747
Client 2298 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
