#include <unistd.h>
#include <Log.hpp>
#include <Messages.hpp>
#include <utility>
#include "ReadingIterator.hpp"
#include "UtilityModel.hpp"
#include "UtilityFunction.hpp"
#include "Client.hpp"

Client::Client(const economem::Reading& initialReading,
               const std::vector<std::string>& cacheNames,
               bool forceFunctionValidity,
               economem::Log& log,
               unsigned long long minHeap)
    : m_LastReading(initialReading),
      m_TotalReadingsReceived(0),
      m_Caches(cacheNames.size()),
      m_Functions(cacheNames.size()),
      m_ForceFunctionValidity(forceFunctionValidity),
      m_Log(log)
{
    for (size_t i = 0; i < cacheNames.size(); ++i)
    {
        m_Caches[i] = ReadingCache::fromName(cacheNames[i].c_str());
        m_Functions[i] = NULL;
    }

    m_MinHeapMB = std::max(minHeap, CLIENT_MIN_HEAP);
    addReading(initialReading);
}

Client::~Client()
{
    for (size_t i = 0; i < m_Functions.size(); ++i)
    {
        delete m_Functions[i];
        delete m_Caches[i];
    }
}

pid_t Client::getPid() const
{
    return m_LastReading.sender;
}

const economem::Reading& Client::getLastReading() const
{
    return m_LastReading;
}

unsigned long long Client::getMinHeapMB() const
{
    return m_MinHeapMB;
}

void Client::setMinHeapMB(unsigned long long minHeapMB)
{
    m_MinHeapMB = std::max(minHeapMB, CLIENT_MIN_HEAP);
    m_Log.logMinHeap(getPid(), m_MinHeapMB);
}

const UtilityFunction* Client::calculateFunction(const UtilityModel& u)
{
    for (size_t i = 0; i < m_Functions.size(); ++i)
    {
        delete m_Functions[i];
        if (m_Caches[i]->isValid())
        {
            ReadingIterator* it = m_Caches[i]->newIterator();
            m_Functions[i] = u.calculateFuncFromData(*it);
            if (m_ForceFunctionValidity)
                m_Functions[i]->forceValidity();
            delete it;
        }
        else
            m_Functions[i] = NULL;
    }
    return m_Functions[0];
}

const UtilityFunction* Client::getCurrentFunction() const
{
    return m_Functions[0];
}

bool Client::isValid() const
{
    return m_Caches[0]->isValid();
}

void Client::addReading(const economem::Reading& reading)
{
    m_LastReading = reading;
    for (ReadingCache* c : m_Caches)
        c->addReading(reading.heapSizeMB, reading.throughput);
    ++m_TotalReadingsReceived;
}

unsigned long long Client::getTotalReadingsReceived() const
{
    return m_TotalReadingsReceived;
}

void Client::dumpTo(FILE* fd) const
{
    fprintf(fd, "Client: pid %u, minHeap %llu\n", getPid(), getMinHeapMB());
    fprintf(fd, "    utility function: %s\n", (m_Functions[0] == NULL ? "(null)" : m_Functions[0]->toString().c_str()));
    m_Caches[0]->dumpTo(fd);
    fprintf(fd, "    (plus %zu other caches)\n", m_Caches.size() - 1);
}

void Client::logUtilityFunctions() const
{
    for (size_t i = 0; i < m_Functions.size(); ++i)
    {
        UtilityFunction* f = m_Functions[i];
        if (f != NULL)
            m_Log.logFunction(getPid(), m_Caches[i]->getName(), f->toPython());
    }
}
