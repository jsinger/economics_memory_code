{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, DeriveDataTypeable #-}
{-# OPTIONS_GHC -fno-warn-missing-fields -fno-warn-missing-methods #-}

import Economem.ArgHelper
import Economem.Common
import Economem.Files
import Economem.Records
import System.Console.CmdLib

data Args = Args { benchmark :: String,
                   size :: String,
                   threads :: Int,
                   time :: String,
                   inputFile :: FilePath }
          deriving (Typeable, Data, Eq, Show)

instance Attributes Args where
  attributes _ = group "Options" [
    benchmark %> [ Positional 0,
                   Required True,
                   Help "Name of benchmark to look up",
                   ArgHelp "BENCHMARK" ],
    size %> [ Positional 1,
              Required True,
              Help "Size of benchmark to look up",
              ArgHelp "SIZE" ],
    threads %> [ Positional 2,
                 Required True,
                 Help "Number of threads of benchmark to look up",
                 ArgHelp "THREADS" ],
    time %> [ Positional 3,
              Required True,
              Help "How many iterations are in this much time?",
              ArgHelp "TIME"],
    inputFile %> [ Positional 4,
                   Required True,
                   Help "File containing iteration lengths",
                   ArgHelp "INPUT_FILE" ]]

instance RecordCommand Args where
  mode_summary _ = "Calculate the number of iterations a benchmark should be run for to last for the given time."

main :: IO ()
main = getArgs >>= executeR Args {} >>= \opts -> do
  (n, s, t, seconds, input) <- validate opts
  bms <- get (CsvFile input)
  let target = BenchmarkThreads (Benchmark n s) t
  let interesting = filter (\b -> btrBenchmark b == target) bms
  case interesting of
    [] -> putStrLn "Benchmark not found"
    (b:[]) -> print (btiIters (iterationsInTime seconds b))
    _ -> putStrLn "Benchmark appears multiple times!"

  where
    validate :: Args -> IO (String, String, Int, Integer, FilePath)
    validate a =
      do input <- existingAbsoluteFile (inputFile a)
         thr <- positive (threads a)
         t <- timeToSeconds (time a)
         return (benchmark a, size a, fromIntegral thr, t, input)
