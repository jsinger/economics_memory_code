economemd
    total memory: 5948 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub3_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 32313: heap size 9 MB, throughput 0.991137
Clients: 1
Client 32313 has a minimum heap size of 276 MB
Reading from 32311: heap size 9 MB, throughput 0.982459
Clients: 2
Client 32311 has a minimum heap size of 1211 MB
Reading from 32313: heap size 9 MB, throughput 0.978463
Reading from 32311: heap size 9 MB, throughput 0.978537
Reading from 32313: heap size 9 MB, throughput 0.966721
Reading from 32313: heap size 9 MB, throughput 0.952129
Reading from 32311: heap size 11 MB, throughput 0.973964
Reading from 32311: heap size 11 MB, throughput 0.969852
Reading from 32313: heap size 11 MB, throughput 0.976824
Reading from 32313: heap size 11 MB, throughput 0.968732
Reading from 32311: heap size 15 MB, throughput 0.866202
Reading from 32313: heap size 17 MB, throughput 0.929557
Reading from 32311: heap size 18 MB, throughput 0.95952
Reading from 32311: heap size 23 MB, throughput 0.861159
Reading from 32313: heap size 17 MB, throughput 0.573826
Reading from 32311: heap size 28 MB, throughput 0.877578
Reading from 32313: heap size 30 MB, throughput 0.952348
Reading from 32313: heap size 31 MB, throughput 0.82661
Reading from 32311: heap size 30 MB, throughput 0.504441
Reading from 32313: heap size 34 MB, throughput 0.348817
Reading from 32311: heap size 39 MB, throughput 0.848645
Reading from 32313: heap size 45 MB, throughput 0.87815
Reading from 32311: heap size 46 MB, throughput 0.711832
Reading from 32313: heap size 48 MB, throughput 0.582015
Reading from 32311: heap size 47 MB, throughput 0.284893
Reading from 32313: heap size 62 MB, throughput 0.73231
Reading from 32311: heap size 64 MB, throughput 0.821176
Reading from 32313: heap size 69 MB, throughput 0.879924
Reading from 32311: heap size 67 MB, throughput 0.749069
Reading from 32313: heap size 69 MB, throughput 0.299058
Reading from 32311: heap size 72 MB, throughput 0.244402
Reading from 32313: heap size 100 MB, throughput 0.677648
Reading from 32311: heap size 95 MB, throughput 0.28538
Reading from 32313: heap size 100 MB, throughput 0.244113
Reading from 32311: heap size 122 MB, throughput 0.870388
Reading from 32313: heap size 132 MB, throughput 0.674716
Reading from 32311: heap size 125 MB, throughput 0.793727
Reading from 32313: heap size 132 MB, throughput 0.76545
Reading from 32311: heap size 127 MB, throughput 0.758073
Reading from 32313: heap size 134 MB, throughput 0.719257
Reading from 32311: heap size 130 MB, throughput 0.188464
Reading from 32311: heap size 160 MB, throughput 0.599209
Reading from 32313: heap size 139 MB, throughput 0.19406
Reading from 32311: heap size 164 MB, throughput 0.771071
Reading from 32313: heap size 171 MB, throughput 0.684285
Reading from 32311: heap size 168 MB, throughput 0.78018
Reading from 32313: heap size 176 MB, throughput 0.785037
Reading from 32313: heap size 179 MB, throughput 0.729056
Reading from 32311: heap size 170 MB, throughput 0.181516
Reading from 32311: heap size 213 MB, throughput 0.664275
Reading from 32313: heap size 182 MB, throughput 0.229217
Reading from 32311: heap size 216 MB, throughput 0.624455
Reading from 32311: heap size 218 MB, throughput 0.616459
Reading from 32313: heap size 223 MB, throughput 0.923668
Reading from 32311: heap size 225 MB, throughput 0.77281
Reading from 32311: heap size 229 MB, throughput 0.647078
Reading from 32311: heap size 235 MB, throughput 0.518184
Reading from 32313: heap size 227 MB, throughput 0.936088
Reading from 32313: heap size 231 MB, throughput 0.753206
Reading from 32313: heap size 239 MB, throughput 0.737066
Reading from 32313: heap size 243 MB, throughput 0.679814
Reading from 32311: heap size 239 MB, throughput 0.141282
Reading from 32313: heap size 245 MB, throughput 0.751792
Reading from 32311: heap size 287 MB, throughput 0.536035
Reading from 32311: heap size 300 MB, throughput 0.654197
Reading from 32313: heap size 251 MB, throughput 0.888239
Reading from 32311: heap size 303 MB, throughput 0.656253
Reading from 32313: heap size 252 MB, throughput 0.83434
Reading from 32311: heap size 306 MB, throughput 0.630391
Reading from 32313: heap size 254 MB, throughput 0.737975
Reading from 32313: heap size 258 MB, throughput 0.630836
Reading from 32313: heap size 265 MB, throughput 0.797675
Reading from 32311: heap size 315 MB, throughput 0.136829
Reading from 32311: heap size 363 MB, throughput 0.437384
Reading from 32313: heap size 268 MB, throughput 0.907195
Reading from 32311: heap size 370 MB, throughput 0.613729
Reading from 32311: heap size 373 MB, throughput 0.516942
Reading from 32311: heap size 374 MB, throughput 0.520141
Reading from 32313: heap size 272 MB, throughput 0.923741
Reading from 32313: heap size 274 MB, throughput 0.601172
Reading from 32311: heap size 381 MB, throughput 0.111715
Reading from 32311: heap size 431 MB, throughput 0.443185
Reading from 32313: heap size 281 MB, throughput 0.143431
Reading from 32311: heap size 429 MB, throughput 0.586977
Reading from 32313: heap size 323 MB, throughput 0.583151
Reading from 32311: heap size 435 MB, throughput 0.510813
Reading from 32313: heap size 327 MB, throughput 0.909335
Reading from 32311: heap size 440 MB, throughput 0.536173
Reading from 32313: heap size 328 MB, throughput 0.917035
Reading from 32313: heap size 327 MB, throughput 0.593754
Reading from 32313: heap size 330 MB, throughput 0.55853
Reading from 32313: heap size 338 MB, throughput 0.75123
Reading from 32313: heap size 339 MB, throughput 0.61712
Reading from 32313: heap size 350 MB, throughput 0.629337
Reading from 32311: heap size 440 MB, throughput 0.13236
Reading from 32311: heap size 494 MB, throughput 0.544279
Equal recommendation: 2974 MB each
Reading from 32311: heap size 498 MB, throughput 0.51827
Reading from 32313: heap size 350 MB, throughput 0.945897
Reading from 32311: heap size 500 MB, throughput 0.595104
Reading from 32311: heap size 505 MB, throughput 0.452615
Reading from 32311: heap size 512 MB, throughput 0.610326
Reading from 32311: heap size 518 MB, throughput 0.535876
Reading from 32313: heap size 356 MB, throughput 0.964281
Reading from 32311: heap size 525 MB, throughput 0.0935479
Reading from 32311: heap size 586 MB, throughput 0.525796
Reading from 32311: heap size 602 MB, throughput 0.633151
Reading from 32313: heap size 359 MB, throughput 0.970631
Reading from 32311: heap size 603 MB, throughput 0.0744015
Reading from 32311: heap size 668 MB, throughput 0.459875
Reading from 32311: heap size 672 MB, throughput 0.568947
Reading from 32313: heap size 358 MB, throughput 0.976833
Reading from 32311: heap size 673 MB, throughput 0.680805
Reading from 32311: heap size 676 MB, throughput 0.873949
Reading from 32311: heap size 673 MB, throughput 0.781634
Reading from 32311: heap size 691 MB, throughput 0.752122
Reading from 32313: heap size 362 MB, throughput 0.627428
Reading from 32311: heap size 698 MB, throughput 0.36857
Reading from 32311: heap size 719 MB, throughput 0.512848
Reading from 32313: heap size 410 MB, throughput 0.957017
Reading from 32311: heap size 728 MB, throughput 0.0239964
Reading from 32311: heap size 802 MB, throughput 0.250684
Reading from 32311: heap size 788 MB, throughput 0.330021
Reading from 32311: heap size 681 MB, throughput 0.351459
Equal recommendation: 2974 MB each
Reading from 32311: heap size 781 MB, throughput 0.0478411
Reading from 32313: heap size 411 MB, throughput 0.993033
Reading from 32311: heap size 864 MB, throughput 0.769957
Reading from 32311: heap size 860 MB, throughput 0.564582
Reading from 32311: heap size 864 MB, throughput 0.583026
Reading from 32311: heap size 858 MB, throughput 0.606762
Reading from 32311: heap size 862 MB, throughput 0.767548
Reading from 32313: heap size 418 MB, throughput 0.984603
Reading from 32311: heap size 864 MB, throughput 0.0908714
Reading from 32311: heap size 951 MB, throughput 0.602424
Reading from 32311: heap size 953 MB, throughput 0.892602
Reading from 32311: heap size 958 MB, throughput 0.937233
Reading from 32313: heap size 419 MB, throughput 0.96634
Reading from 32311: heap size 961 MB, throughput 0.769057
Reading from 32311: heap size 963 MB, throughput 0.906515
Reading from 32311: heap size 949 MB, throughput 0.875137
Reading from 32311: heap size 791 MB, throughput 0.767777
Reading from 32311: heap size 926 MB, throughput 0.813449
Reading from 32311: heap size 796 MB, throughput 0.732844
Reading from 32311: heap size 913 MB, throughput 0.785881
Reading from 32311: heap size 793 MB, throughput 0.738646
Reading from 32311: heap size 905 MB, throughput 0.810326
Reading from 32313: heap size 419 MB, throughput 0.963825
Reading from 32311: heap size 799 MB, throughput 0.675042
Reading from 32311: heap size 899 MB, throughput 0.825919
Reading from 32311: heap size 905 MB, throughput 0.813059
Reading from 32311: heap size 892 MB, throughput 0.812527
Reading from 32311: heap size 899 MB, throughput 0.978755
Reading from 32313: heap size 421 MB, throughput 0.985947
Reading from 32311: heap size 897 MB, throughput 0.961999
Reading from 32311: heap size 899 MB, throughput 0.817995
Reading from 32311: heap size 899 MB, throughput 0.812349
Reading from 32311: heap size 901 MB, throughput 0.771326
Reading from 32311: heap size 901 MB, throughput 0.835149
Reading from 32311: heap size 903 MB, throughput 0.787912
Reading from 32311: heap size 903 MB, throughput 0.811631
Reading from 32313: heap size 416 MB, throughput 0.984201
Reading from 32311: heap size 906 MB, throughput 0.799413
Reading from 32311: heap size 907 MB, throughput 0.80376
Reading from 32311: heap size 909 MB, throughput 0.849753
Equal recommendation: 2974 MB each
Reading from 32311: heap size 910 MB, throughput 0.853816
Reading from 32311: heap size 913 MB, throughput 0.882791
Reading from 32311: heap size 914 MB, throughput 0.732554
Reading from 32313: heap size 420 MB, throughput 0.964728
Reading from 32311: heap size 916 MB, throughput 0.0723108
Reading from 32311: heap size 1022 MB, throughput 0.474663
Reading from 32311: heap size 1026 MB, throughput 0.690903
Reading from 32313: heap size 419 MB, throughput 0.987415
Reading from 32311: heap size 1030 MB, throughput 0.74553
Reading from 32313: heap size 421 MB, throughput 0.851626
Reading from 32311: heap size 1035 MB, throughput 0.727191
Reading from 32313: heap size 415 MB, throughput 0.829464
Reading from 32313: heap size 421 MB, throughput 0.695926
Reading from 32311: heap size 1039 MB, throughput 0.671337
Reading from 32311: heap size 1041 MB, throughput 0.652122
Reading from 32313: heap size 429 MB, throughput 0.940572
Reading from 32311: heap size 1055 MB, throughput 0.718253
Reading from 32311: heap size 1055 MB, throughput 0.672961
Reading from 32311: heap size 1071 MB, throughput 0.759028
Reading from 32313: heap size 432 MB, throughput 0.988109
Reading from 32311: heap size 1072 MB, throughput 0.977474
Reading from 32313: heap size 432 MB, throughput 0.989533
Reading from 32311: heap size 1092 MB, throughput 0.958662
Reading from 32313: heap size 436 MB, throughput 0.975384
Equal recommendation: 2974 MB each
Reading from 32311: heap size 1094 MB, throughput 0.950667
Reading from 32313: heap size 435 MB, throughput 0.985595
Reading from 32311: heap size 1102 MB, throughput 0.969217
Reading from 32313: heap size 437 MB, throughput 0.983367
Reading from 32311: heap size 1105 MB, throughput 0.954453
Reading from 32313: heap size 435 MB, throughput 0.985419
Reading from 32311: heap size 1102 MB, throughput 0.944005
Reading from 32313: heap size 438 MB, throughput 0.98327
Reading from 32311: heap size 1107 MB, throughput 0.964237
Reading from 32311: heap size 1101 MB, throughput 0.958229
Reading from 32313: heap size 439 MB, throughput 0.985134
Equal recommendation: 2974 MB each
Reading from 32311: heap size 1106 MB, throughput 0.964407
Reading from 32313: heap size 440 MB, throughput 0.983159
Reading from 32311: heap size 1112 MB, throughput 0.952228
Reading from 32313: heap size 443 MB, throughput 0.985829
Reading from 32313: heap size 443 MB, throughput 0.950096
Reading from 32313: heap size 446 MB, throughput 0.876251
Reading from 32313: heap size 446 MB, throughput 0.85739
Reading from 32311: heap size 1112 MB, throughput 0.94726
Reading from 32313: heap size 454 MB, throughput 0.980926
Reading from 32311: heap size 1118 MB, throughput 0.946915
Reading from 32313: heap size 454 MB, throughput 0.989524
Reading from 32311: heap size 1121 MB, throughput 0.94817
Equal recommendation: 2974 MB each
Reading from 32313: heap size 460 MB, throughput 0.991676
Reading from 32311: heap size 1128 MB, throughput 0.952638
Reading from 32313: heap size 461 MB, throughput 0.988373
Reading from 32311: heap size 1131 MB, throughput 0.95795
Reading from 32313: heap size 461 MB, throughput 0.975448
Reading from 32311: heap size 1137 MB, throughput 0.940199
Reading from 32313: heap size 464 MB, throughput 0.974378
Reading from 32311: heap size 1143 MB, throughput 0.949198
Reading from 32311: heap size 1150 MB, throughput 0.95104
Equal recommendation: 2974 MB each
Reading from 32313: heap size 464 MB, throughput 0.986659
Reading from 32313: heap size 465 MB, throughput 0.964979
Reading from 32311: heap size 1154 MB, throughput 0.51457
Reading from 32313: heap size 468 MB, throughput 0.992387
Reading from 32311: heap size 1249 MB, throughput 0.92714
Reading from 32313: heap size 468 MB, throughput 0.973204
Reading from 32313: heap size 469 MB, throughput 0.878276
Reading from 32313: heap size 471 MB, throughput 0.869149
Reading from 32311: heap size 1251 MB, throughput 0.974377
Reading from 32313: heap size 478 MB, throughput 0.990622
Equal recommendation: 2974 MB each
Reading from 32311: heap size 1262 MB, throughput 0.971041
Reading from 32313: heap size 479 MB, throughput 0.991614
Reading from 32311: heap size 1263 MB, throughput 0.975306
Reading from 32313: heap size 485 MB, throughput 0.99062
Reading from 32311: heap size 1262 MB, throughput 0.969974
Reading from 32313: heap size 486 MB, throughput 0.98861
Reading from 32311: heap size 1265 MB, throughput 0.968165
Reading from 32313: heap size 486 MB, throughput 0.987461
Reading from 32311: heap size 1256 MB, throughput 0.965222
Equal recommendation: 2974 MB each
Reading from 32311: heap size 1261 MB, throughput 0.969528
Reading from 32313: heap size 488 MB, throughput 0.988842
Reading from 32311: heap size 1253 MB, throughput 0.966179
Reading from 32313: heap size 489 MB, throughput 0.987126
Reading from 32311: heap size 1257 MB, throughput 0.960988
Reading from 32313: heap size 490 MB, throughput 0.992701
Reading from 32313: heap size 492 MB, throughput 0.918104
Reading from 32313: heap size 493 MB, throughput 0.872634
Reading from 32311: heap size 1261 MB, throughput 0.959042
Reading from 32313: heap size 499 MB, throughput 0.986239
Equal recommendation: 2974 MB each
Reading from 32311: heap size 1262 MB, throughput 0.959482
Reading from 32313: heap size 500 MB, throughput 0.990466
Reading from 32311: heap size 1267 MB, throughput 0.954903
Reading from 32313: heap size 505 MB, throughput 0.992051
Reading from 32311: heap size 1272 MB, throughput 0.957738
Reading from 32313: heap size 507 MB, throughput 0.991179
Reading from 32311: heap size 1278 MB, throughput 0.9484
Equal recommendation: 2974 MB each
Reading from 32313: heap size 505 MB, throughput 0.989742
Reading from 32311: heap size 1285 MB, throughput 0.95501
Reading from 32313: heap size 508 MB, throughput 0.988344
Reading from 32311: heap size 1294 MB, throughput 0.948638
Reading from 32313: heap size 511 MB, throughput 0.987929
Reading from 32311: heap size 1301 MB, throughput 0.951726
Reading from 32313: heap size 511 MB, throughput 0.985687
Reading from 32313: heap size 515 MB, throughput 0.905704
Reading from 32313: heap size 517 MB, throughput 0.960331
Reading from 32311: heap size 1310 MB, throughput 0.947441
Equal recommendation: 2974 MB each
Reading from 32311: heap size 1314 MB, throughput 0.950264
Reading from 32313: heap size 524 MB, throughput 0.992656
Reading from 32313: heap size 525 MB, throughput 0.984264
Reading from 32311: heap size 1324 MB, throughput 0.533798
Reading from 32313: heap size 527 MB, throughput 0.992655
Equal recommendation: 2974 MB each
Reading from 32311: heap size 1428 MB, throughput 0.94162
Reading from 32313: heap size 528 MB, throughput 0.990577
Reading from 32313: heap size 527 MB, throughput 0.990265
Equal recommendation: 2974 MB each
Reading from 32313: heap size 529 MB, throughput 0.993173
Reading from 32313: heap size 530 MB, throughput 0.979135
Reading from 32313: heap size 532 MB, throughput 0.904352
Reading from 32313: heap size 536 MB, throughput 0.988248
Reading from 32311: heap size 1437 MB, throughput 0.992652
Reading from 32313: heap size 538 MB, throughput 0.993152
Reading from 32313: heap size 540 MB, throughput 0.992282
Equal recommendation: 2974 MB each
Reading from 32313: heap size 542 MB, throughput 0.98645
Reading from 32313: heap size 541 MB, throughput 0.990847
Reading from 32311: heap size 1438 MB, throughput 0.989061
Reading from 32313: heap size 543 MB, throughput 0.987732
Equal recommendation: 2974 MB each
Reading from 32311: heap size 1423 MB, throughput 0.933521
Reading from 32311: heap size 1449 MB, throughput 0.740169
Reading from 32311: heap size 1430 MB, throughput 0.702278
Reading from 32311: heap size 1463 MB, throughput 0.684508
Reading from 32313: heap size 545 MB, throughput 0.990028
Reading from 32313: heap size 546 MB, throughput 0.823061
Reading from 32311: heap size 1489 MB, throughput 0.683344
Reading from 32311: heap size 1503 MB, throughput 0.703204
Reading from 32311: heap size 1535 MB, throughput 0.74353
Reading from 32313: heap size 550 MB, throughput 0.985309
Reading from 32311: heap size 1542 MB, throughput 0.931125
Reading from 32313: heap size 551 MB, throughput 0.991198
Reading from 32311: heap size 1555 MB, throughput 0.961638
Equal recommendation: 2974 MB each
Reading from 32313: heap size 557 MB, throughput 0.99274
Reading from 32311: heap size 1568 MB, throughput 0.970471
Reading from 32311: heap size 1558 MB, throughput 0.970777
Reading from 32313: heap size 559 MB, throughput 0.990292
Reading from 32311: heap size 1572 MB, throughput 0.9546
Reading from 32313: heap size 558 MB, throughput 0.990953
Reading from 32311: heap size 1561 MB, throughput 0.972802
Equal recommendation: 2974 MB each
Reading from 32313: heap size 561 MB, throughput 0.989072
Reading from 32313: heap size 564 MB, throughput 0.983745
Reading from 32311: heap size 1573 MB, throughput 0.742487
Reading from 32313: heap size 565 MB, throughput 0.918027
Reading from 32313: heap size 569 MB, throughput 0.991331
Reading from 32311: heap size 1557 MB, throughput 0.990616
Reading from 32313: heap size 571 MB, throughput 0.993667
Equal recommendation: 2974 MB each
Reading from 32311: heap size 1562 MB, throughput 0.990598
Reading from 32313: heap size 573 MB, throughput 0.985724
Reading from 32311: heap size 1587 MB, throughput 0.980939
Reading from 32311: heap size 1595 MB, throughput 0.977724
Reading from 32313: heap size 575 MB, throughput 0.991798
Equal recommendation: 2974 MB each
Reading from 32311: heap size 1603 MB, throughput 0.982077
Reading from 32313: heap size 574 MB, throughput 0.991108
Reading from 32311: heap size 1607 MB, throughput 0.978512
Reading from 32313: heap size 576 MB, throughput 0.994092
Reading from 32313: heap size 578 MB, throughput 0.934587
Reading from 32313: heap size 579 MB, throughput 0.983076
Reading from 32311: heap size 1594 MB, throughput 0.981589
Equal recommendation: 2974 MB each
Reading from 32313: heap size 587 MB, throughput 0.994676
Reading from 32311: heap size 1603 MB, throughput 0.973352
Reading from 32313: heap size 587 MB, throughput 0.99193
Reading from 32311: heap size 1578 MB, throughput 0.978651
Reading from 32313: heap size 587 MB, throughput 0.992216
Reading from 32311: heap size 1591 MB, throughput 0.968832
Equal recommendation: 2974 MB each
Reading from 32313: heap size 589 MB, throughput 0.990805
Reading from 32311: heap size 1585 MB, throughput 0.969132
Reading from 32313: heap size 592 MB, throughput 0.994589
Reading from 32313: heap size 592 MB, throughput 0.937586
Reading from 32313: heap size 592 MB, throughput 0.970694
Reading from 32311: heap size 1589 MB, throughput 0.962147
Equal recommendation: 2974 MB each
Reading from 32311: heap size 1597 MB, throughput 0.962601
Reading from 32313: heap size 594 MB, throughput 0.994286
Reading from 32313: heap size 597 MB, throughput 0.985361
Reading from 32311: heap size 1602 MB, throughput 0.953447
Reading from 32313: heap size 599 MB, throughput 0.980218
Reading from 32311: heap size 1613 MB, throughput 0.95476
Equal recommendation: 2974 MB each
Reading from 32313: heap size 601 MB, throughput 0.990315
Reading from 32311: heap size 1623 MB, throughput 0.957725
Reading from 32313: heap size 601 MB, throughput 0.992733
Reading from 32313: heap size 605 MB, throughput 0.935001
Reading from 32311: heap size 1642 MB, throughput 0.957462
Reading from 32313: heap size 606 MB, throughput 0.992089
Equal recommendation: 2974 MB each
Reading from 32311: heap size 1652 MB, throughput 0.95476
Reading from 32313: heap size 613 MB, throughput 0.993821
Reading from 32311: heap size 1670 MB, throughput 0.962628
Reading from 32313: heap size 614 MB, throughput 0.992644
Reading from 32311: heap size 1677 MB, throughput 0.961771
Equal recommendation: 2974 MB each
Reading from 32313: heap size 613 MB, throughput 0.993045
Reading from 32313: heap size 616 MB, throughput 0.993714
Reading from 32313: heap size 618 MB, throughput 0.98
Reading from 32313: heap size 619 MB, throughput 0.973443
Equal recommendation: 2974 MB each
Reading from 32313: heap size 625 MB, throughput 0.993672
Reading from 32313: heap size 626 MB, throughput 0.992873
Equal recommendation: 2974 MB each
Reading from 32313: heap size 624 MB, throughput 0.991681
Reading from 32311: heap size 1695 MB, throughput 0.993914
Reading from 32313: heap size 626 MB, throughput 0.99067
Reading from 32313: heap size 628 MB, throughput 0.990294
Reading from 32313: heap size 630 MB, throughput 0.951981
Client 32313 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 32311: heap size 1699 MB, throughput 0.98631
Reading from 32311: heap size 1705 MB, throughput 0.843719
Reading from 32311: heap size 1707 MB, throughput 0.780011
Reading from 32311: heap size 1740 MB, throughput 0.820636
Client 32311 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
