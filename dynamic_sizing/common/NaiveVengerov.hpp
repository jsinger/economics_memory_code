#ifndef NAIVEVENGEROV_HPP
#define NAIVEVENGEROV_HPP

#include "AdaptiveWeightedAverage.hpp"
#include "Stopwatch.hpp"
#include "ThroughputCalculator.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
//! Throughput-calculation algorithm that smooths minor GC throughputs.
/*! This algorithm uses the throughput formula derived by David Vengerov in the paper [Modeling, analysis and throughput optimization of a generational garbage collector](http://dl.acm.org/citation.cfm?id=1542433). It is 'naive' because it applies Vengerov's formula directly, even though the conditions here don't quite match those for which he derived it.

  The algorithm attempts to address the problem encountered when the simple definition of throughput (see SimpleThroughput) is applied directly to a generational GC system: the throughputs of minor-GC periods and major-GC periods can be widely different. The basic idea of this algorithm is that the definition can be used directly after a major GC, but the throughput must instead be estimated after a minor GC (see the paper for details). By doing this, the throughput values are less spread out.

  This algorithm can only be used with generational GC systems. */
    class NaiveVengerov : public ThroughputCalculator
    {
    public:
        NaiveVengerov();

        virtual void gcBegin();
        virtual ThroughputCalculator::Result minorGCEnd();
        virtual ThroughputCalculator::Result majorGCEnd();

    protected:
        //! Return the current heap size in bytes.
        virtual unsigned long long getHeapSizeB() = 0;

        //! Return the current young generation size in bytes (this is the _whole_ of the young gen, not just one region inside it).
        virtual unsigned long long getYoungGenSizeB() = 0;

        //! Return the current old generation size in bytes.
        virtual unsigned long long getOldGenSizeB() = 0;

        //! Use if you want to log the calculated throughputs in your client.
        virtual void logReading(unsigned long long heapSizeB, double throughput) = 0;

    private:
        economem::Stopwatch m_GCTimer; // From the beginning of a GC to its end
        economem::Stopwatch m_MajorToMajorTimer; // From the end of one major GC to the end of the next
        economem::Stopwatch m_EdenFillTimer; // From the end of a GC to the beginning of the next

        unsigned long long m_InitialOldUsedB;
        unsigned long long m_MajorGCTime;
        unsigned long long m_MutatorTime;
        unsigned long long m_GCStartHeapSizeB;
        unsigned long long m_GCStartYoungSizeB;
        unsigned long long m_GCStartOldUsedB;

        economem::AdaptiveWeightedAverage m_AvgMinorGCTime;
        economem::AdaptiveWeightedAverage m_AvgEdenFillTime;
        economem::AdaptiveWeightedAverage m_AvgMinorPromoted;
    };
}

/*! @} */

#endif
