economemd
    total memory: 7338 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub18_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run10_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 3596: heap size 9 MB, throughput 0.98264
Clients: 1
Client 3596 has a minimum heap size of 12 MB
Reading from 3595: heap size 9 MB, throughput 0.991213
Clients: 2
Client 3595 has a minimum heap size of 1211 MB
Reading from 3596: heap size 9 MB, throughput 0.984428
Reading from 3595: heap size 9 MB, throughput 0.988347
Reading from 3595: heap size 9 MB, throughput 0.943723
Reading from 3596: heap size 11 MB, throughput 0.983803
Reading from 3595: heap size 9 MB, throughput 0.920223
Reading from 3596: heap size 11 MB, throughput 0.981754
Reading from 3595: heap size 11 MB, throughput 0.976841
Reading from 3596: heap size 15 MB, throughput 0.986102
Reading from 3595: heap size 11 MB, throughput 0.982642
Reading from 3595: heap size 17 MB, throughput 0.920532
Reading from 3596: heap size 15 MB, throughput 0.983396
Reading from 3595: heap size 17 MB, throughput 0.570081
Reading from 3595: heap size 30 MB, throughput 0.875791
Reading from 3595: heap size 31 MB, throughput 0.9148
Reading from 3596: heap size 25 MB, throughput 0.916573
Reading from 3595: heap size 35 MB, throughput 0.561812
Reading from 3595: heap size 48 MB, throughput 0.84657
Reading from 3596: heap size 30 MB, throughput 0.990107
Reading from 3595: heap size 51 MB, throughput 0.323836
Reading from 3595: heap size 65 MB, throughput 0.846396
Reading from 3595: heap size 69 MB, throughput 0.808113
Reading from 3596: heap size 38 MB, throughput 0.986333
Reading from 3595: heap size 72 MB, throughput 0.272215
Reading from 3595: heap size 103 MB, throughput 0.814707
Reading from 3596: heap size 44 MB, throughput 0.988727
Reading from 3595: heap size 103 MB, throughput 0.796709
Reading from 3596: heap size 45 MB, throughput 0.990671
Reading from 3596: heap size 46 MB, throughput 0.981362
Reading from 3595: heap size 105 MB, throughput 0.263777
Reading from 3595: heap size 132 MB, throughput 0.739127
Reading from 3595: heap size 136 MB, throughput 0.753899
Reading from 3596: heap size 53 MB, throughput 0.94344
Reading from 3595: heap size 138 MB, throughput 0.733565
Reading from 3595: heap size 146 MB, throughput 0.730544
Reading from 3596: heap size 53 MB, throughput 0.983793
Reading from 3596: heap size 64 MB, throughput 0.967947
Reading from 3596: heap size 64 MB, throughput 0.739067
Reading from 3596: heap size 83 MB, throughput 0.990467
Reading from 3595: heap size 149 MB, throughput 0.105166
Reading from 3595: heap size 194 MB, throughput 0.645823
Reading from 3595: heap size 195 MB, throughput 0.662197
Reading from 3596: heap size 83 MB, throughput 0.992656
Reading from 3595: heap size 198 MB, throughput 0.171298
Reading from 3595: heap size 241 MB, throughput 0.737437
Reading from 3595: heap size 250 MB, throughput 0.819246
Reading from 3596: heap size 91 MB, throughput 0.995918
Reading from 3595: heap size 250 MB, throughput 0.702806
Reading from 3595: heap size 253 MB, throughput 0.682931
Reading from 3595: heap size 260 MB, throughput 0.721428
Reading from 3595: heap size 264 MB, throughput 0.729196
Reading from 3595: heap size 274 MB, throughput 0.622738
Reading from 3595: heap size 278 MB, throughput 0.54058
Reading from 3596: heap size 93 MB, throughput 0.996342
Reading from 3595: heap size 291 MB, throughput 0.515327
Reading from 3595: heap size 295 MB, throughput 0.361426
Reading from 3595: heap size 307 MB, throughput 0.456776
Reading from 3596: heap size 100 MB, throughput 0.995976
Reading from 3595: heap size 315 MB, throughput 0.071496
Reading from 3596: heap size 102 MB, throughput 0.996435
Reading from 3595: heap size 363 MB, throughput 0.100756
Reading from 3595: heap size 399 MB, throughput 0.451119
Reading from 3595: heap size 305 MB, throughput 0.546353
Reading from 3595: heap size 397 MB, throughput 0.599263
Reading from 3595: heap size 318 MB, throughput 0.567535
Reading from 3595: heap size 386 MB, throughput 0.580712
Reading from 3595: heap size 332 MB, throughput 0.530165
Reading from 3596: heap size 107 MB, throughput 0.997004
Reading from 3595: heap size 387 MB, throughput 0.426003
Reading from 3595: heap size 389 MB, throughput 0.59309
Reading from 3596: heap size 109 MB, throughput 0.996032
Reading from 3595: heap size 385 MB, throughput 0.0873868
Reading from 3595: heap size 438 MB, throughput 0.51473
Reading from 3595: heap size 433 MB, throughput 0.61474
Reading from 3595: heap size 436 MB, throughput 0.609008
Reading from 3595: heap size 432 MB, throughput 0.549203
Reading from 3596: heap size 114 MB, throughput 0.997211
Reading from 3595: heap size 434 MB, throughput 0.614389
Equal recommendation: 3669 MB each
Reading from 3595: heap size 436 MB, throughput 0.509293
Reading from 3595: heap size 440 MB, throughput 0.49709
Reading from 3595: heap size 444 MB, throughput 0.54325
Reading from 3596: heap size 115 MB, throughput 0.994488
Reading from 3595: heap size 452 MB, throughput 0.511123
Reading from 3595: heap size 456 MB, throughput 0.505116
Reading from 3595: heap size 464 MB, throughput 0.43109
Reading from 3596: heap size 120 MB, throughput 0.996882
Reading from 3595: heap size 467 MB, throughput 0.0834272
Reading from 3595: heap size 520 MB, throughput 0.323733
Reading from 3596: heap size 120 MB, throughput 0.994835
Reading from 3595: heap size 530 MB, throughput 0.427892
Reading from 3595: heap size 531 MB, throughput 0.466558
Reading from 3596: heap size 123 MB, throughput 0.995617
Reading from 3595: heap size 538 MB, throughput 0.0827747
Reading from 3595: heap size 595 MB, throughput 0.516774
Reading from 3595: heap size 587 MB, throughput 0.609733
Reading from 3595: heap size 593 MB, throughput 0.660014
Reading from 3596: heap size 124 MB, throughput 0.996768
Reading from 3595: heap size 594 MB, throughput 0.641878
Reading from 3595: heap size 595 MB, throughput 0.614264
Reading from 3595: heap size 597 MB, throughput 0.578338
Reading from 3596: heap size 128 MB, throughput 0.996552
Reading from 3595: heap size 606 MB, throughput 0.0854398
Reading from 3596: heap size 128 MB, throughput 0.996308
Reading from 3595: heap size 674 MB, throughput 0.715709
Reading from 3596: heap size 131 MB, throughput 0.997388
Reading from 3595: heap size 680 MB, throughput 0.86743
Reading from 3595: heap size 680 MB, throughput 0.860835
Reading from 3596: heap size 132 MB, throughput 0.997314
Reading from 3595: heap size 685 MB, throughput 0.845329
Reading from 3595: heap size 688 MB, throughput 0.439863
Reading from 3595: heap size 698 MB, throughput 0.502653
Reading from 3596: heap size 134 MB, throughput 0.997377
Reading from 3596: heap size 135 MB, throughput 0.997399
Reading from 3596: heap size 137 MB, throughput 0.997412
Reading from 3595: heap size 706 MB, throughput 0.0773965
Reading from 3595: heap size 768 MB, throughput 0.231251
Equal recommendation: 3669 MB each
Reading from 3595: heap size 758 MB, throughput 0.357999
Reading from 3595: heap size 765 MB, throughput 0.414066
Reading from 3595: heap size 761 MB, throughput 0.487171
Reading from 3596: heap size 137 MB, throughput 0.998343
Reading from 3595: heap size 763 MB, throughput 0.882212
Reading from 3596: heap size 140 MB, throughput 0.998392
Reading from 3596: heap size 140 MB, throughput 0.998134
Reading from 3595: heap size 760 MB, throughput 0.0442249
Reading from 3595: heap size 833 MB, throughput 0.454829
Reading from 3595: heap size 836 MB, throughput 0.698188
Reading from 3595: heap size 838 MB, throughput 0.830461
Reading from 3596: heap size 143 MB, throughput 0.997694
Reading from 3595: heap size 843 MB, throughput 0.686663
Reading from 3596: heap size 143 MB, throughput 0.987693
Reading from 3596: heap size 145 MB, throughput 0.991936
Reading from 3595: heap size 844 MB, throughput 0.210347
Reading from 3595: heap size 930 MB, throughput 0.384939
Reading from 3596: heap size 145 MB, throughput 0.996022
Reading from 3595: heap size 934 MB, throughput 0.953752
Reading from 3595: heap size 943 MB, throughput 0.948587
Reading from 3595: heap size 944 MB, throughput 0.935046
Reading from 3595: heap size 928 MB, throughput 0.817282
Reading from 3596: heap size 150 MB, throughput 0.99515
Reading from 3595: heap size 812 MB, throughput 0.763571
Reading from 3595: heap size 916 MB, throughput 0.795059
Reading from 3595: heap size 816 MB, throughput 0.811917
Reading from 3595: heap size 906 MB, throughput 0.777264
Reading from 3595: heap size 914 MB, throughput 0.842095
Reading from 3595: heap size 903 MB, throughput 0.850181
Reading from 3596: heap size 151 MB, throughput 0.983752
Reading from 3595: heap size 908 MB, throughput 0.830955
Reading from 3595: heap size 898 MB, throughput 0.855357
Reading from 3595: heap size 903 MB, throughput 0.860103
Reading from 3595: heap size 895 MB, throughput 0.864945
Reading from 3596: heap size 156 MB, throughput 0.997592
Reading from 3596: heap size 156 MB, throughput 0.99583
Reading from 3595: heap size 900 MB, throughput 0.979173
Reading from 3595: heap size 894 MB, throughput 0.94542
Reading from 3595: heap size 897 MB, throughput 0.810911
Reading from 3596: heap size 161 MB, throughput 0.997623
Equal recommendation: 3669 MB each
Reading from 3595: heap size 902 MB, throughput 0.744444
Reading from 3595: heap size 902 MB, throughput 0.814469
Reading from 3595: heap size 908 MB, throughput 0.825625
Reading from 3595: heap size 908 MB, throughput 0.827021
Reading from 3595: heap size 912 MB, throughput 0.803566
Reading from 3596: heap size 162 MB, throughput 0.997104
Reading from 3595: heap size 913 MB, throughput 0.82811
Reading from 3595: heap size 916 MB, throughput 0.835778
Reading from 3595: heap size 918 MB, throughput 0.859739
Reading from 3595: heap size 920 MB, throughput 0.885137
Reading from 3596: heap size 166 MB, throughput 0.998251
Reading from 3595: heap size 922 MB, throughput 0.892556
Reading from 3595: heap size 925 MB, throughput 0.7548
Reading from 3596: heap size 166 MB, throughput 0.997035
Reading from 3596: heap size 170 MB, throughput 0.987707
Reading from 3595: heap size 927 MB, throughput 0.0636196
Reading from 3595: heap size 1046 MB, throughput 0.6044
Reading from 3595: heap size 1047 MB, throughput 0.784847
Reading from 3595: heap size 1047 MB, throughput 0.793476
Reading from 3596: heap size 170 MB, throughput 0.997088
Reading from 3595: heap size 1050 MB, throughput 0.722091
Reading from 3595: heap size 1058 MB, throughput 0.782516
Reading from 3595: heap size 1058 MB, throughput 0.746624
Reading from 3595: heap size 1071 MB, throughput 0.767734
Reading from 3595: heap size 1072 MB, throughput 0.707376
Reading from 3595: heap size 1086 MB, throughput 0.834647
Reading from 3596: heap size 175 MB, throughput 0.997858
Reading from 3596: heap size 176 MB, throughput 0.996881
Reading from 3595: heap size 1088 MB, throughput 0.965378
Reading from 3596: heap size 179 MB, throughput 0.997834
Reading from 3596: heap size 180 MB, throughput 0.997928
Equal recommendation: 3669 MB each
Reading from 3595: heap size 1105 MB, throughput 0.964145
Reading from 3596: heap size 183 MB, throughput 0.998473
Reading from 3595: heap size 1108 MB, throughput 0.940769
Reading from 3596: heap size 183 MB, throughput 0.997143
Reading from 3596: heap size 187 MB, throughput 0.992553
Reading from 3596: heap size 187 MB, throughput 0.995676
Reading from 3595: heap size 1115 MB, throughput 0.968855
Reading from 3596: heap size 191 MB, throughput 0.998189
Reading from 3595: heap size 1117 MB, throughput 0.962064
Reading from 3596: heap size 191 MB, throughput 0.997881
Reading from 3596: heap size 194 MB, throughput 0.998115
Reading from 3595: heap size 1113 MB, throughput 0.963489
Reading from 3596: heap size 195 MB, throughput 0.99773
Reading from 3596: heap size 198 MB, throughput 0.998136
Reading from 3595: heap size 1118 MB, throughput 0.962075
Equal recommendation: 3669 MB each
Reading from 3596: heap size 198 MB, throughput 0.997624
Reading from 3595: heap size 1116 MB, throughput 0.957661
Reading from 3596: heap size 200 MB, throughput 0.99783
Reading from 3596: heap size 201 MB, throughput 0.997674
Reading from 3595: heap size 1119 MB, throughput 0.958759
Reading from 3596: heap size 204 MB, throughput 0.997938
Reading from 3595: heap size 1124 MB, throughput 0.958641
Reading from 3596: heap size 204 MB, throughput 0.997826
Reading from 3596: heap size 207 MB, throughput 0.997856
Reading from 3595: heap size 1124 MB, throughput 0.958851
Reading from 3596: heap size 207 MB, throughput 0.997691
Reading from 3596: heap size 209 MB, throughput 0.996785
Reading from 3595: heap size 1129 MB, throughput 0.945274
Reading from 3596: heap size 209 MB, throughput 0.991804
Equal recommendation: 3669 MB each
Reading from 3596: heap size 212 MB, throughput 0.996539
Reading from 3595: heap size 1132 MB, throughput 0.962096
Reading from 3596: heap size 213 MB, throughput 0.997945
Reading from 3596: heap size 217 MB, throughput 0.998265
Reading from 3595: heap size 1138 MB, throughput 0.96274
Reading from 3596: heap size 218 MB, throughput 0.998041
Reading from 3595: heap size 1141 MB, throughput 0.959935
Reading from 3596: heap size 221 MB, throughput 0.998291
Reading from 3596: heap size 210 MB, throughput 0.998025
Reading from 3595: heap size 1145 MB, throughput 0.961844
Reading from 3596: heap size 201 MB, throughput 0.998055
Reading from 3595: heap size 1151 MB, throughput 0.961424
Reading from 3596: heap size 193 MB, throughput 0.997806
Equal recommendation: 3669 MB each
Reading from 3596: heap size 185 MB, throughput 0.997741
Reading from 3595: heap size 1158 MB, throughput 0.957713
Reading from 3596: heap size 177 MB, throughput 0.997662
Reading from 3596: heap size 169 MB, throughput 0.997551
Reading from 3595: heap size 1161 MB, throughput 0.961334
Reading from 3596: heap size 162 MB, throughput 0.996982
Reading from 3596: heap size 156 MB, throughput 0.997371
Reading from 3596: heap size 149 MB, throughput 0.997177
Reading from 3595: heap size 1167 MB, throughput 0.933185
Reading from 3596: heap size 143 MB, throughput 0.859588
Reading from 3596: heap size 142 MB, throughput 0.992416
Reading from 3596: heap size 145 MB, throughput 0.990546
Reading from 3596: heap size 152 MB, throughput 0.997943
Reading from 3595: heap size 1169 MB, throughput 0.959544
Reading from 3596: heap size 156 MB, throughput 0.998005
Reading from 3596: heap size 161 MB, throughput 0.997679
Equal recommendation: 3669 MB each
Reading from 3595: heap size 1176 MB, throughput 0.962182
Reading from 3596: heap size 166 MB, throughput 0.997974
Reading from 3596: heap size 170 MB, throughput 0.998047
Reading from 3596: heap size 174 MB, throughput 0.998057
Reading from 3595: heap size 1177 MB, throughput 0.958687
Reading from 3596: heap size 178 MB, throughput 0.998136
Reading from 3596: heap size 182 MB, throughput 0.998006
Reading from 3596: heap size 185 MB, throughput 0.994595
Reading from 3595: heap size 1182 MB, throughput 0.5486
Reading from 3596: heap size 190 MB, throughput 0.998124
Reading from 3596: heap size 195 MB, throughput 0.998111
Reading from 3595: heap size 1276 MB, throughput 0.948589
Reading from 3596: heap size 197 MB, throughput 0.998072
Equal recommendation: 3669 MB each
Reading from 3596: heap size 196 MB, throughput 0.998048
Reading from 3595: heap size 1275 MB, throughput 0.983887
Reading from 3596: heap size 200 MB, throughput 0.998179
Reading from 3596: heap size 200 MB, throughput 0.997392
Reading from 3596: heap size 204 MB, throughput 0.991626
Reading from 3595: heap size 1282 MB, throughput 0.981773
Reading from 3596: heap size 204 MB, throughput 0.995769
Reading from 3596: heap size 212 MB, throughput 0.998021
Reading from 3595: heap size 1291 MB, throughput 0.979807
Reading from 3596: heap size 212 MB, throughput 0.997976
Reading from 3596: heap size 217 MB, throughput 0.998199
Reading from 3595: heap size 1291 MB, throughput 0.979176
Reading from 3596: heap size 217 MB, throughput 0.997733
Equal recommendation: 3669 MB each
Reading from 3596: heap size 222 MB, throughput 0.998104
Reading from 3595: heap size 1286 MB, throughput 0.974747
Reading from 3596: heap size 222 MB, throughput 0.997683
Reading from 3596: heap size 227 MB, throughput 0.99798
Reading from 3595: heap size 1200 MB, throughput 0.973759
Reading from 3596: heap size 227 MB, throughput 0.99786
Reading from 3595: heap size 1277 MB, throughput 0.974167
Reading from 3596: heap size 231 MB, throughput 0.997978
Reading from 3596: heap size 231 MB, throughput 0.997723
Reading from 3595: heap size 1218 MB, throughput 0.96885
Reading from 3596: heap size 236 MB, throughput 0.998158
Reading from 3596: heap size 236 MB, throughput 0.994456
Equal recommendation: 3669 MB each
Reading from 3596: heap size 240 MB, throughput 0.994117
Reading from 3595: heap size 1276 MB, throughput 0.972916
Reading from 3596: heap size 240 MB, throughput 0.998108
Reading from 3596: heap size 246 MB, throughput 0.998261
Reading from 3595: heap size 1279 MB, throughput 0.970204
Reading from 3596: heap size 247 MB, throughput 0.997903
Reading from 3595: heap size 1282 MB, throughput 0.96664
Reading from 3596: heap size 252 MB, throughput 0.998184
Reading from 3596: heap size 253 MB, throughput 0.998004
Reading from 3595: heap size 1284 MB, throughput 0.966642
Reading from 3596: heap size 258 MB, throughput 0.998276
Equal recommendation: 3669 MB each
Reading from 3596: heap size 258 MB, throughput 0.997742
Reading from 3595: heap size 1287 MB, throughput 0.966307
Reading from 3596: heap size 263 MB, throughput 0.998296
Reading from 3595: heap size 1293 MB, throughput 0.963561
Reading from 3596: heap size 263 MB, throughput 0.998028
Reading from 3596: heap size 267 MB, throughput 0.998228
Reading from 3596: heap size 268 MB, throughput 0.990315
Reading from 3595: heap size 1298 MB, throughput 0.953634
Reading from 3596: heap size 272 MB, throughput 0.997408
Reading from 3595: heap size 1305 MB, throughput 0.962462
Reading from 3596: heap size 273 MB, throughput 0.997936
Equal recommendation: 3669 MB each
Reading from 3596: heap size 279 MB, throughput 0.997999
Reading from 3595: heap size 1314 MB, throughput 0.962159
Reading from 3596: heap size 280 MB, throughput 0.998125
Reading from 3595: heap size 1318 MB, throughput 0.961893
Reading from 3596: heap size 286 MB, throughput 0.998409
Reading from 3596: heap size 287 MB, throughput 0.998141
Reading from 3595: heap size 1325 MB, throughput 0.962904
Reading from 3596: heap size 292 MB, throughput 0.998482
Equal recommendation: 3669 MB each
Reading from 3596: heap size 292 MB, throughput 0.998157
Reading from 3596: heap size 298 MB, throughput 0.998425
Reading from 3596: heap size 298 MB, throughput 0.995108
Reading from 3596: heap size 302 MB, throughput 0.997387
Reading from 3596: heap size 303 MB, throughput 0.998076
Reading from 3596: heap size 309 MB, throughput 0.998252
Equal recommendation: 3669 MB each
Reading from 3596: heap size 310 MB, throughput 0.998121
Reading from 3596: heap size 315 MB, throughput 0.998356
Reading from 3596: heap size 315 MB, throughput 0.99831
Reading from 3596: heap size 320 MB, throughput 0.998412
Reading from 3596: heap size 321 MB, throughput 0.998081
Reading from 3595: heap size 1328 MB, throughput 0.891464
Reading from 3596: heap size 326 MB, throughput 0.997162
Equal recommendation: 3669 MB each
Reading from 3596: heap size 326 MB, throughput 0.996127
Reading from 3596: heap size 332 MB, throughput 0.998377
Reading from 3596: heap size 333 MB, throughput 0.998005
Reading from 3596: heap size 339 MB, throughput 0.998329
Reading from 3596: heap size 339 MB, throughput 0.998181
Equal recommendation: 3669 MB each
Reading from 3596: heap size 345 MB, throughput 0.998134
Reading from 3595: heap size 1424 MB, throughput 0.986417
Reading from 3596: heap size 345 MB, throughput 0.998162
Reading from 3596: heap size 351 MB, throughput 0.998664
Reading from 3596: heap size 351 MB, throughput 0.89325
Reading from 3595: heap size 1453 MB, throughput 0.982275
Reading from 3595: heap size 1456 MB, throughput 0.862585
Reading from 3595: heap size 1459 MB, throughput 0.735117
Reading from 3595: heap size 1473 MB, throughput 0.756488
Reading from 3595: heap size 1487 MB, throughput 0.741258
Reading from 3595: heap size 1511 MB, throughput 0.767483
Reading from 3596: heap size 364 MB, throughput 0.998986
Reading from 3595: heap size 1519 MB, throughput 0.747987
Reading from 3595: heap size 1547 MB, throughput 0.839857
Equal recommendation: 3669 MB each
Reading from 3596: heap size 365 MB, throughput 0.999242
Reading from 3595: heap size 1551 MB, throughput 0.962899
Reading from 3596: heap size 373 MB, throughput 0.999307
Reading from 3595: heap size 1561 MB, throughput 0.977331
Reading from 3596: heap size 375 MB, throughput 0.999169
Reading from 3595: heap size 1568 MB, throughput 0.975699
Reading from 3596: heap size 381 MB, throughput 0.999355
Equal recommendation: 3669 MB each
Reading from 3596: heap size 382 MB, throughput 0.999268
Reading from 3595: heap size 1566 MB, throughput 0.976337
Reading from 3596: heap size 387 MB, throughput 0.998857
Reading from 3595: heap size 1576 MB, throughput 0.971003
Reading from 3596: heap size 387 MB, throughput 0.99596
Reading from 3596: heap size 391 MB, throughput 0.998837
Reading from 3595: heap size 1565 MB, throughput 0.974253
Reading from 3596: heap size 392 MB, throughput 0.998643
Reading from 3595: heap size 1576 MB, throughput 0.971258
Equal recommendation: 3669 MB each
Reading from 3596: heap size 399 MB, throughput 0.99877
Reading from 3595: heap size 1583 MB, throughput 0.978062
Reading from 3596: heap size 399 MB, throughput 0.998572
Reading from 3596: heap size 405 MB, throughput 0.998744
Reading from 3595: heap size 1584 MB, throughput 0.97903
Reading from 3596: heap size 406 MB, throughput 0.998575
Reading from 3596: heap size 412 MB, throughput 0.995079
Equal recommendation: 3669 MB each
Reading from 3595: heap size 1585 MB, throughput 0.978537
Reading from 3596: heap size 412 MB, throughput 0.998276
Reading from 3595: heap size 1590 MB, throughput 0.976093
Reading from 3596: heap size 423 MB, throughput 0.998631
Reading from 3596: heap size 424 MB, throughput 0.998417
Reading from 3595: heap size 1577 MB, throughput 0.972537
Reading from 3596: heap size 431 MB, throughput 0.998526
Equal recommendation: 3669 MB each
Reading from 3595: heap size 1585 MB, throughput 0.972577
Reading from 3596: heap size 432 MB, throughput 0.998383
Reading from 3595: heap size 1584 MB, throughput 0.972323
Reading from 3596: heap size 440 MB, throughput 0.998592
Reading from 3596: heap size 440 MB, throughput 0.994949
Reading from 3595: heap size 1586 MB, throughput 0.969475
Reading from 3596: heap size 447 MB, throughput 0.998519
Equal recommendation: 3669 MB each
Reading from 3596: heap size 448 MB, throughput 0.998165
Reading from 3595: heap size 1593 MB, throughput 0.965796
Reading from 3596: heap size 458 MB, throughput 0.998567
Reading from 3595: heap size 1598 MB, throughput 0.963067
Reading from 3596: heap size 458 MB, throughput 0.998483
Equal recommendation: 3669 MB each
Reading from 3595: heap size 1609 MB, throughput 0.772345
Reading from 3596: heap size 467 MB, throughput 0.998552
Reading from 3596: heap size 467 MB, throughput 0.995743
Reading from 3595: heap size 1606 MB, throughput 0.992507
Reading from 3596: heap size 475 MB, throughput 0.998438
Reading from 3595: heap size 1609 MB, throughput 0.990121
Reading from 3596: heap size 476 MB, throughput 0.998649
Equal recommendation: 3669 MB each
Reading from 3596: heap size 485 MB, throughput 0.998652
Reading from 3595: heap size 1621 MB, throughput 0.988758
Reading from 3596: heap size 486 MB, throughput 0.998466
Reading from 3595: heap size 1636 MB, throughput 0.985469
Reading from 3596: heap size 494 MB, throughput 0.998652
Reading from 3595: heap size 1636 MB, throughput 0.985575
Client 3596 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 3595: heap size 1627 MB, throughput 0.983401
Reading from 3595: heap size 1491 MB, throughput 0.981695
Recommendation: one client; give it all the memory
Reading from 3595: heap size 1611 MB, throughput 0.979968
Reading from 3595: heap size 1622 MB, throughput 0.978338
Reading from 3595: heap size 1618 MB, throughput 0.977062
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 3595: heap size 1621 MB, throughput 0.990895
Recommendation: one client; give it all the memory
Reading from 3595: heap size 1613 MB, throughput 0.990212
Recommendation: one client; give it all the memory
Reading from 3595: heap size 1631 MB, throughput 0.97869
Reading from 3595: heap size 1664 MB, throughput 0.839582
Reading from 3595: heap size 1671 MB, throughput 0.758161
Reading from 3595: heap size 1701 MB, throughput 0.798172
Reading from 3595: heap size 1727 MB, throughput 0.839343
Client 3595 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
