{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, DeriveDataTypeable #-}
{-# OPTIONS_GHC -fno-warn-missing-fields -fno-warn-missing-methods #-}

import Control.Applicative
import Economem.ArgHelper
import Economem.Experiment
import Economem.Files
import qualified Economem.GCFactor as G
import Economem.Java
import Economem.Records
import Economem.Signals
import System.Console.CmdLib

data Args = Args { inputFile :: FilePath,
                   minHeapsFile :: FilePath,
                   factorsFile :: FilePath,
                   java :: String }
          deriving (Typeable, Data, Eq, Show)

instance Attributes Args where
  attributes _ = group "Options" [
    inputFile %> [ Positional 0,
                   Required True,
                   Help "Specifies what to run",
                   ArgHelp "INPUT_FILE" ],
    minHeapsFile %> [ Positional 1,
                   Required True,
                   Help "Min heaps of the benchmarks",
                   ArgHelp "MIN_HEAPS_FILE" ],
    factorsFile %> [ Positional 2,
                   Required True,
                   Help "Factors of min heap at which to measure",
                   ArgHelp "FACTORS_FILE" ],
    java %> [ Short ['j'],
              Long ["java"],
              Default (javaName defaultJava),
              Help ("Java version to use; available options: " ++ show validJavaNames),
              ArgHelp "JAVA" ]]


instance RecordCommand Args where
  mode_summary _ = "Observe GC behaviour over several factors of the min heap"

main :: IO ()
main = getArgs >>= executeR Args {} >>= \opts -> do
  (i, m, f, j) <- validate opts
  d <- G.newGCFactor i m f j Nothing
  h <- makeSignalHandler
  runExperiment d h

  where
    validate :: Args -> IO (Input BenchmarkThreadsIters,
                            Input BenchmarkThreadsMB,
                            [Double],
                            Java)
    validate a = do
      i <- fromFile <$> existingAbsoluteFile (inputFile a)
      m <- fromFile <$> existingAbsoluteFile (minHeapsFile a)
      f <- get =<< (LinesFile <$> existingAbsoluteFile (factorsFile a))
      j <- javaFromName (java a)
      return (i, m, f, j)
