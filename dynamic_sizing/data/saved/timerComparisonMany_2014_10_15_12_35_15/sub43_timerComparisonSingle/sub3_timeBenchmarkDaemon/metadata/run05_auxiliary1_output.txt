economemd
    total memory: 2208 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub43_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run05_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 18353: heap size 9 MB, throughput 0.991604
Clients: 1
Client 18353 has a minimum heap size of 276 MB
Reading from 18354: heap size 9 MB, throughput 0.987097
Clients: 2
Client 18354 has a minimum heap size of 276 MB
Reading from 18354: heap size 9 MB, throughput 0.984868
Reading from 18353: heap size 9 MB, throughput 0.983707
Reading from 18353: heap size 9 MB, throughput 0.977845
Reading from 18354: heap size 11 MB, throughput 0.974564
Reading from 18353: heap size 9 MB, throughput 0.964438
Reading from 18354: heap size 11 MB, throughput 0.973621
Reading from 18353: heap size 11 MB, throughput 0.960931
Reading from 18354: heap size 15 MB, throughput 0.912917
Reading from 18353: heap size 11 MB, throughput 0.96928
Reading from 18354: heap size 19 MB, throughput 0.916233
Reading from 18353: heap size 17 MB, throughput 0.958942
Reading from 18354: heap size 24 MB, throughput 0.849228
Reading from 18353: heap size 17 MB, throughput 0.909813
Reading from 18354: heap size 29 MB, throughput 0.637038
Reading from 18353: heap size 30 MB, throughput 0.800537
Reading from 18353: heap size 31 MB, throughput 0.536763
Reading from 18354: heap size 30 MB, throughput 0.737266
Reading from 18354: heap size 39 MB, throughput 0.422564
Reading from 18353: heap size 36 MB, throughput 0.644182
Reading from 18354: heap size 45 MB, throughput 0.818894
Reading from 18353: heap size 49 MB, throughput 0.509661
Reading from 18353: heap size 52 MB, throughput 0.422953
Reading from 18354: heap size 46 MB, throughput 0.477099
Reading from 18354: heap size 62 MB, throughput 0.787587
Reading from 18353: heap size 54 MB, throughput 0.571332
Reading from 18354: heap size 66 MB, throughput 0.787236
Reading from 18353: heap size 80 MB, throughput 0.450792
Reading from 18354: heap size 69 MB, throughput 0.465151
Reading from 18354: heap size 88 MB, throughput 0.711434
Reading from 18353: heap size 80 MB, throughput 0.381929
Reading from 18354: heap size 95 MB, throughput 0.722951
Reading from 18354: heap size 96 MB, throughput 0.701708
Reading from 18353: heap size 109 MB, throughput 0.700682
Reading from 18354: heap size 99 MB, throughput 0.727224
Reading from 18353: heap size 110 MB, throughput 0.494279
Reading from 18354: heap size 104 MB, throughput 0.736189
Reading from 18354: heap size 108 MB, throughput 0.638327
Reading from 18353: heap size 111 MB, throughput 0.507256
Reading from 18353: heap size 141 MB, throughput 0.46754
Reading from 18353: heap size 147 MB, throughput 0.47535
Reading from 18354: heap size 112 MB, throughput 0.445358
Reading from 18354: heap size 140 MB, throughput 0.575919
Reading from 18353: heap size 148 MB, throughput 0.086958
Reading from 18354: heap size 145 MB, throughput 0.64612
Reading from 18354: heap size 148 MB, throughput 0.629648
Reading from 18353: heap size 152 MB, throughput 0.445323
Reading from 18353: heap size 192 MB, throughput 0.270571
Reading from 18354: heap size 152 MB, throughput 0.333468
Reading from 18353: heap size 197 MB, throughput 0.209123
Reading from 18354: heap size 184 MB, throughput 0.569398
Reading from 18354: heap size 187 MB, throughput 0.563771
Reading from 18353: heap size 199 MB, throughput 0.670607
Reading from 18354: heap size 189 MB, throughput 0.617843
Reading from 18354: heap size 193 MB, throughput 0.579814
Reading from 18353: heap size 207 MB, throughput 0.823085
Reading from 18354: heap size 197 MB, throughput 0.485695
Reading from 18353: heap size 210 MB, throughput 0.548981
Reading from 18354: heap size 232 MB, throughput 0.838616
Reading from 18353: heap size 265 MB, throughput 0.849293
Reading from 18354: heap size 239 MB, throughput 0.81551
Reading from 18353: heap size 267 MB, throughput 0.849644
Reading from 18354: heap size 243 MB, throughput 0.682964
Reading from 18354: heap size 248 MB, throughput 0.700441
Reading from 18353: heap size 271 MB, throughput 0.864593
Reading from 18354: heap size 250 MB, throughput 0.652515
Reading from 18354: heap size 251 MB, throughput 0.689016
Reading from 18353: heap size 273 MB, throughput 0.890992
Reading from 18353: heap size 278 MB, throughput 0.859696
Reading from 18353: heap size 279 MB, throughput 0.846797
Reading from 18354: heap size 253 MB, throughput 0.567175
Reading from 18353: heap size 280 MB, throughput 0.815185
Reading from 18353: heap size 284 MB, throughput 0.792781
Reading from 18354: heap size 284 MB, throughput 0.767341
Reading from 18353: heap size 291 MB, throughput 0.78364
Reading from 18354: heap size 287 MB, throughput 0.760752
Reading from 18354: heap size 283 MB, throughput 0.770063
Reading from 18354: heap size 286 MB, throughput 0.773718
Reading from 18354: heap size 284 MB, throughput 0.728633
Reading from 18353: heap size 293 MB, throughput 0.86881
Reading from 18354: heap size 287 MB, throughput 0.816474
Reading from 18353: heap size 299 MB, throughput 0.886691
Reading from 18353: heap size 300 MB, throughput 0.832225
Reading from 18353: heap size 307 MB, throughput 0.786049
Reading from 18354: heap size 290 MB, throughput 0.915335
Reading from 18353: heap size 310 MB, throughput 0.787664
Reading from 18354: heap size 291 MB, throughput 0.872508
Reading from 18354: heap size 294 MB, throughput 0.61885
Reading from 18354: heap size 335 MB, throughput 0.703479
Reading from 18354: heap size 340 MB, throughput 0.794992
Reading from 18353: heap size 309 MB, throughput 0.678865
Reading from 18354: heap size 341 MB, throughput 0.838154
Reading from 18353: heap size 362 MB, throughput 0.841914
Reading from 18354: heap size 347 MB, throughput 0.85302
Reading from 18353: heap size 367 MB, throughput 0.821214
Reading from 18353: heap size 368 MB, throughput 0.773443
Reading from 18354: heap size 347 MB, throughput 0.72285
Reading from 18353: heap size 367 MB, throughput 0.667569
Reading from 18354: heap size 351 MB, throughput 0.655428
Reading from 18353: heap size 369 MB, throughput 0.646345
Numeric result:
Recommendation: 2 clients, utility 0.541613:
    h1: 1932 MB (U(h) = 0.588178*h^0.0135746)
    h2: 276 MB (U(h) = 0.826291*h^0.001)
Recommendation: 2 clients, utility 0.541613:
    h1: 1932 MB (U(h) = 0.588178*h^0.0135746)
    h2: 276 MB (U(h) = 0.826291*h^0.001)
Reading from 18354: heap size 351 MB, throughput 0.655558
Reading from 18353: heap size 374 MB, throughput 0.687131
Reading from 18354: heap size 358 MB, throughput 0.666577
Reading from 18354: heap size 337 MB, throughput 0.687406
Reading from 18353: heap size 375 MB, throughput 0.952606
Reading from 18354: heap size 353 MB, throughput 0.951496
Reading from 18353: heap size 383 MB, throughput 0.966081
Reading from 18354: heap size 317 MB, throughput 0.962693
Reading from 18354: heap size 346 MB, throughput 0.950515
Reading from 18353: heap size 385 MB, throughput 0.975281
Reading from 18354: heap size 320 MB, throughput 0.964954
Reading from 18353: heap size 386 MB, throughput 0.982085
Reading from 18354: heap size 343 MB, throughput 0.964443
Reading from 18353: heap size 389 MB, throughput 0.980082
Reading from 18354: heap size 314 MB, throughput 0.965246
Reading from 18354: heap size 336 MB, throughput 0.965462
Reading from 18353: heap size 387 MB, throughput 0.981439
Numeric result:
Recommendation: 2 clients, utility 0.553816:
    h1: 1932 MB (U(h) = 0.48656*h^0.0614333)
    h2: 276 MB (U(h) = 0.702818*h^0.00307903)
Recommendation: 2 clients, utility 0.553816:
    h1: 1932 MB (U(h) = 0.48656*h^0.0614333)
    h2: 276 MB (U(h) = 0.702818*h^0.00307903)
Reading from 18354: heap size 316 MB, throughput 0.967979
Reading from 18353: heap size 391 MB, throughput 0.979018
Reading from 18354: heap size 331 MB, throughput 0.961689
Reading from 18354: heap size 317 MB, throughput 0.960264
Reading from 18353: heap size 394 MB, throughput 0.978078
Reading from 18354: heap size 328 MB, throughput 0.962312
Reading from 18354: heap size 316 MB, throughput 0.961732
Reading from 18353: heap size 395 MB, throughput 0.973982
Reading from 18354: heap size 326 MB, throughput 0.966181
Reading from 18353: heap size 397 MB, throughput 0.975516
Reading from 18354: heap size 316 MB, throughput 0.964138
Reading from 18353: heap size 400 MB, throughput 0.97167
Reading from 18354: heap size 325 MB, throughput 0.967306
Reading from 18354: heap size 316 MB, throughput 0.965889
Reading from 18353: heap size 402 MB, throughput 0.971772
Reading from 18354: heap size 323 MB, throughput 0.963132
Reading from 18354: heap size 312 MB, throughput 0.961328
Numeric result:
Recommendation: 2 clients, utility 0.651422:
    h1: 1677.63 MB (U(h) = 0.430614*h^0.0919382)
    h2: 530.374 MB (U(h) = 0.636957*h^0.0290658)
Recommendation: 2 clients, utility 0.651422:
    h1: 1677.63 MB (U(h) = 0.430614*h^0.0919382)
    h2: 530.373 MB (U(h) = 0.636957*h^0.0290658)
Reading from 18353: heap size 403 MB, throughput 0.975589
Reading from 18354: heap size 319 MB, throughput 0.975747
Reading from 18353: heap size 404 MB, throughput 0.974095
Reading from 18353: heap size 406 MB, throughput 0.95785
Reading from 18353: heap size 402 MB, throughput 0.916865
Reading from 18353: heap size 407 MB, throughput 0.866469
Reading from 18354: heap size 321 MB, throughput 0.954726
Reading from 18354: heap size 322 MB, throughput 0.940455
Reading from 18353: heap size 417 MB, throughput 0.947304
Reading from 18354: heap size 322 MB, throughput 0.92805
Reading from 18354: heap size 362 MB, throughput 0.913626
Reading from 18354: heap size 369 MB, throughput 0.929226
Reading from 18354: heap size 375 MB, throughput 0.946239
Reading from 18354: heap size 378 MB, throughput 0.962374
Reading from 18354: heap size 382 MB, throughput 0.972281
Reading from 18354: heap size 383 MB, throughput 0.98122
Reading from 18354: heap size 383 MB, throughput 0.992429
Reading from 18353: heap size 419 MB, throughput 0.975775
Reading from 18354: heap size 384 MB, throughput 0.98967
Reading from 18353: heap size 422 MB, throughput 0.982186
Reading from 18354: heap size 380 MB, throughput 0.990066
Reading from 18354: heap size 334 MB, throughput 0.989215
Reading from 18354: heap size 375 MB, throughput 0.983261
Reading from 18353: heap size 425 MB, throughput 0.985493
Reading from 18354: heap size 340 MB, throughput 0.978853
Numeric result:
Recommendation: 2 clients, utility 0.767939:
    h1: 1449.64 MB (U(h) = 0.38874*h^0.117105)
    h2: 758.361 MB (U(h) = 0.561121*h^0.0612565)
Recommendation: 2 clients, utility 0.767939:
    h1: 1449.69 MB (U(h) = 0.38874*h^0.117105)
    h2: 758.315 MB (U(h) = 0.561121*h^0.0612565)
Reading from 18353: heap size 425 MB, throughput 0.986606
Reading from 18354: heap size 370 MB, throughput 0.981267
Reading from 18354: heap size 346 MB, throughput 0.979257
Reading from 18353: heap size 427 MB, throughput 0.981849
Reading from 18354: heap size 370 MB, throughput 0.979982
Reading from 18354: heap size 371 MB, throughput 0.979083
Reading from 18353: heap size 426 MB, throughput 0.982052
Reading from 18354: heap size 373 MB, throughput 0.974999
Reading from 18354: heap size 373 MB, throughput 0.97442
Reading from 18353: heap size 428 MB, throughput 0.982625
Reading from 18354: heap size 374 MB, throughput 0.973419
Reading from 18354: heap size 376 MB, throughput 0.97148
Reading from 18353: heap size 426 MB, throughput 0.97766
Reading from 18354: heap size 378 MB, throughput 0.971365
Reading from 18353: heap size 428 MB, throughput 0.978778
Reading from 18354: heap size 380 MB, throughput 0.967907
Numeric result:
Recommendation: 2 clients, utility 0.808665:
    h1: 1409.26 MB (U(h) = 0.375856*h^0.125289)
    h2: 798.742 MB (U(h) = 0.539611*h^0.0710212)
Recommendation: 2 clients, utility 0.808665:
    h1: 1409.19 MB (U(h) = 0.375856*h^0.125289)
    h2: 798.811 MB (U(h) = 0.539611*h^0.0710212)
Reading from 18354: heap size 383 MB, throughput 0.971011
Reading from 18354: heap size 384 MB, throughput 0.969502
Reading from 18353: heap size 428 MB, throughput 0.985261
Reading from 18353: heap size 429 MB, throughput 0.980652
Reading from 18353: heap size 431 MB, throughput 0.970147
Reading from 18353: heap size 432 MB, throughput 0.952267
Reading from 18354: heap size 387 MB, throughput 0.97882
Reading from 18354: heap size 388 MB, throughput 0.965212
Reading from 18354: heap size 389 MB, throughput 0.942845
Reading from 18353: heap size 437 MB, throughput 0.96129
Reading from 18354: heap size 389 MB, throughput 0.906854
Reading from 18354: heap size 396 MB, throughput 0.883199
Reading from 18354: heap size 400 MB, throughput 0.843372
Reading from 18354: heap size 409 MB, throughput 0.813747
Reading from 18354: heap size 410 MB, throughput 0.942848
Reading from 18353: heap size 437 MB, throughput 0.985317
Reading from 18354: heap size 416 MB, throughput 0.977058
Reading from 18354: heap size 418 MB, throughput 0.978408
Reading from 18353: heap size 443 MB, throughput 0.986279
Reading from 18354: heap size 418 MB, throughput 0.983599
Numeric result:
Recommendation: 2 clients, utility 0.85634:
    h1: 1390.06 MB (U(h) = 0.359222*h^0.136204)
    h2: 817.937 MB (U(h) = 0.519704*h^0.0801427)
Recommendation: 2 clients, utility 0.85634:
    h1: 1390.08 MB (U(h) = 0.359222*h^0.136204)
    h2: 817.924 MB (U(h) = 0.519704*h^0.0801427)
Reading from 18353: heap size 444 MB, throughput 0.981146
Reading from 18354: heap size 421 MB, throughput 0.978348
Reading from 18354: heap size 420 MB, throughput 0.980497
Reading from 18353: heap size 445 MB, throughput 0.984407
Reading from 18354: heap size 422 MB, throughput 0.98111
Reading from 18354: heap size 419 MB, throughput 0.969837
Reading from 18353: heap size 446 MB, throughput 0.977037
Reading from 18354: heap size 422 MB, throughput 0.975055
Reading from 18353: heap size 444 MB, throughput 0.981
Reading from 18354: heap size 420 MB, throughput 0.978683
Reading from 18353: heap size 446 MB, throughput 0.695287
Reading from 18354: heap size 422 MB, throughput 0.983563
Numeric result:
Recommendation: 2 clients, utility 0.879176:
    h1: 1370.81 MB (U(h) = 0.353157*h^0.140271)
    h2: 837.194 MB (U(h) = 0.507799*h^0.0856674)
Recommendation: 2 clients, utility 0.879176:
    h1: 1370.81 MB (U(h) = 0.353157*h^0.140271)
    h2: 837.193 MB (U(h) = 0.507799*h^0.0856674)
Reading from 18353: heap size 447 MB, throughput 0.852372
Reading from 18354: heap size 420 MB, throughput 0.981324
Reading from 18354: heap size 422 MB, throughput 0.978495
Reading from 18353: heap size 448 MB, throughput 0.714573
Reading from 18353: heap size 449 MB, throughput 0.803395
Reading from 18353: heap size 450 MB, throughput 0.964119
Reading from 18353: heap size 456 MB, throughput 0.968433
Reading from 18354: heap size 424 MB, throughput 0.985028
Reading from 18354: heap size 425 MB, throughput 0.975411
Reading from 18354: heap size 421 MB, throughput 0.962401
Reading from 18354: heap size 426 MB, throughput 0.932807
Reading from 18354: heap size 433 MB, throughput 0.905346
Reading from 18354: heap size 435 MB, throughput 0.963613
Reading from 18353: heap size 457 MB, throughput 0.98677
Reading from 18354: heap size 443 MB, throughput 0.979697
Reading from 18353: heap size 461 MB, throughput 0.989638
Reading from 18354: heap size 445 MB, throughput 0.985714
Numeric result:
Recommendation: 2 clients, utility 0.932763:
    h1: 1229.56 MB (U(h) = 0.346024*h^0.145076)
    h2: 978.441 MB (U(h) = 0.4337*h^0.11544)
Recommendation: 2 clients, utility 0.932763:
    h1: 1229.59 MB (U(h) = 0.346024*h^0.145076)
    h2: 978.411 MB (U(h) = 0.4337*h^0.11544)
Reading from 18353: heap size 462 MB, throughput 0.982382
Reading from 18354: heap size 447 MB, throughput 0.979631
Reading from 18354: heap size 449 MB, throughput 0.98135
Reading from 18353: heap size 463 MB, throughput 0.985886
Reading from 18354: heap size 448 MB, throughput 0.982549
Reading from 18353: heap size 464 MB, throughput 0.879818
Reading from 18354: heap size 450 MB, throughput 0.981381
Reading from 18354: heap size 449 MB, throughput 0.983879
Reading from 18353: heap size 463 MB, throughput 0.935169
Reading from 18354: heap size 451 MB, throughput 0.980301
Reading from 18353: heap size 465 MB, throughput 0.946622
Numeric result:
Recommendation: 2 clients, utility 1.14257:
    h1: 827.768 MB (U(h) = 0.339865*h^0.149288)
    h2: 1380.23 MB (U(h) = 0.203933*h^0.248883)
Recommendation: 2 clients, utility 1.14257:
    h1: 827.855 MB (U(h) = 0.339865*h^0.149288)
    h2: 1380.15 MB (U(h) = 0.203933*h^0.248883)
Reading from 18354: heap size 453 MB, throughput 0.981693
Reading from 18353: heap size 466 MB, throughput 0.920437
Reading from 18353: heap size 467 MB, throughput 0.87213
Reading from 18353: heap size 468 MB, throughput 0.971495
Reading from 18354: heap size 453 MB, throughput 0.986229
Reading from 18353: heap size 470 MB, throughput 0.975343
Reading from 18354: heap size 457 MB, throughput 0.982939
Reading from 18354: heap size 457 MB, throughput 0.971831
Reading from 18354: heap size 460 MB, throughput 0.955487
Reading from 18354: heap size 462 MB, throughput 0.946849
Reading from 18353: heap size 477 MB, throughput 0.98965
Reading from 18354: heap size 471 MB, throughput 0.984582
Reading from 18353: heap size 478 MB, throughput 0.990394
Reading from 18354: heap size 471 MB, throughput 0.98768
Numeric result:
Recommendation: 2 clients, utility 1.3213:
    h1: 697.143 MB (U(h) = 0.331308*h^0.155215)
    h2: 1510.86 MB (U(h) = 0.123035*h^0.336376)
Recommendation: 2 clients, utility 1.3213:
    h1: 697.156 MB (U(h) = 0.331308*h^0.155215)
    h2: 1510.84 MB (U(h) = 0.123035*h^0.336376)
Reading from 18354: heap size 474 MB, throughput 0.988039
Reading from 18353: heap size 479 MB, throughput 0.990151
Reading from 18354: heap size 476 MB, throughput 0.987487
Reading from 18353: heap size 480 MB, throughput 0.906011
Reading from 18354: heap size 475 MB, throughput 0.987972
Reading from 18353: heap size 479 MB, throughput 0.939803
Reading from 18354: heap size 478 MB, throughput 0.9876
Reading from 18353: heap size 480 MB, throughput 0.942754
Reading from 18354: heap size 477 MB, throughput 0.985867
Numeric result:
Recommendation: 2 clients, utility 1.56236:
    h1: 574.015 MB (U(h) = 0.329058*h^0.156788)
    h2: 1633.98 MB (U(h) = 0.0645434*h^0.446305)
Recommendation: 2 clients, utility 1.56236:
    h1: 574.022 MB (U(h) = 0.329058*h^0.156788)
    h2: 1633.98 MB (U(h) = 0.0645434*h^0.446305)
Reading from 18353: heap size 482 MB, throughput 0.924102
Reading from 18354: heap size 479 MB, throughput 0.984241
Reading from 18353: heap size 483 MB, throughput 0.985681
Reading from 18353: heap size 486 MB, throughput 0.977567
Reading from 18353: heap size 488 MB, throughput 0.972788
Reading from 18354: heap size 482 MB, throughput 0.987303
Reading from 18354: heap size 483 MB, throughput 0.979493
Reading from 18354: heap size 483 MB, throughput 0.962954
Reading from 18354: heap size 486 MB, throughput 0.955136
Reading from 18353: heap size 494 MB, throughput 0.989685
Reading from 18354: heap size 496 MB, throughput 0.986174
Reading from 18353: heap size 495 MB, throughput 0.989991
Reading from 18354: heap size 496 MB, throughput 0.98852
Numeric result:
Recommendation: 2 clients, utility 1.85989:
    h1: 498.841 MB (U(h) = 0.321068*h^0.162413)
    h2: 1709.16 MB (U(h) = 0.033556*h^0.556466)
Recommendation: 2 clients, utility 1.85989:
    h1: 498.844 MB (U(h) = 0.321068*h^0.162413)
    h2: 1709.16 MB (U(h) = 0.033556*h^0.556466)
Reading from 18353: heap size 496 MB, throughput 0.990944
Reading from 18354: heap size 499 MB, throughput 0.98958
Reading from 18354: heap size 501 MB, throughput 0.988627
Reading from 18353: heap size 497 MB, throughput 0.990303
Reading from 18354: heap size 499 MB, throughput 0.988419
Reading from 18353: heap size 494 MB, throughput 0.988696
Reading from 18354: heap size 502 MB, throughput 0.989086
Reading from 18353: heap size 497 MB, throughput 0.986902
Numeric result:
Recommendation: 2 clients, utility 1.76907:
    h1: 528.5 MB (U(h) = 0.318349*h^0.164345)
    h2: 1679.5 MB (U(h) = 0.0410149*h^0.522259)
Recommendation: 2 clients, utility 1.76907:
    h1: 528.504 MB (U(h) = 0.318349*h^0.164345)
    h2: 1679.5 MB (U(h) = 0.0410149*h^0.522259)
Reading from 18354: heap size 505 MB, throughput 0.98734
Reading from 18353: heap size 499 MB, throughput 0.99053
Reading from 18353: heap size 500 MB, throughput 0.984837
Reading from 18353: heap size 499 MB, throughput 0.97577
Reading from 18354: heap size 505 MB, throughput 0.991071
Reading from 18354: heap size 508 MB, throughput 0.98536
Reading from 18354: heap size 509 MB, throughput 0.974355
Reading from 18353: heap size 502 MB, throughput 0.98566
Reading from 18354: heap size 515 MB, throughput 0.979078
Reading from 18353: heap size 508 MB, throughput 0.990369
Reading from 18354: heap size 517 MB, throughput 0.988198
Numeric result:
Recommendation: 2 clients, utility 1.80123:
    h1: 1090.85 MB (U(h) = 0.0885073*h^0.383119)
    h2: 1117.15 MB (U(h) = 0.0888828*h^0.392356)
Recommendation: 2 clients, utility 1.80123:
    h1: 1090.85 MB (U(h) = 0.0885073*h^0.383119)
    h2: 1117.15 MB (U(h) = 0.0888828*h^0.392356)
Reading from 18354: heap size 520 MB, throughput 0.990191
Reading from 18353: heap size 509 MB, throughput 0.992236
Reading from 18354: heap size 522 MB, throughput 0.991306
Reading from 18353: heap size 508 MB, throughput 0.991069
Reading from 18354: heap size 520 MB, throughput 0.988718
Reading from 18353: heap size 510 MB, throughput 0.990464
Reading from 18354: heap size 523 MB, throughput 0.988765
Reading from 18353: heap size 509 MB, throughput 0.952149
Numeric result:
Recommendation: 2 clients, utility 2.00118:
    h1: 1216.71 MB (U(h) = 0.0499879*h^0.479561)
    h2: 991.285 MB (U(h) = 0.089585*h^0.390712)
Recommendation: 2 clients, utility 2.00118:
    h1: 1216.71 MB (U(h) = 0.0499879*h^0.479561)
    h2: 991.289 MB (U(h) = 0.089585*h^0.390712)
Reading from 18354: heap size 525 MB, throughput 0.988165
Reading from 18353: heap size 511 MB, throughput 0.991259
Reading from 18353: heap size 513 MB, throughput 0.674124
Reading from 18353: heap size 514 MB, throughput 0.976272
Reading from 18354: heap size 525 MB, throughput 0.9805
Reading from 18354: heap size 511 MB, throughput 0.987274
Reading from 18354: heap size 512 MB, throughput 0.984138
Reading from 18353: heap size 519 MB, throughput 0.986695
Reading from 18354: heap size 516 MB, throughput 0.99024
Reading from 18353: heap size 520 MB, throughput 0.990219
Reading from 18354: heap size 516 MB, throughput 0.993406
Numeric result:
Recommendation: 2 clients, utility 2.45254:
    h1: 1584.79 MB (U(h) = 0.0130571*h^0.702947)
    h2: 623.213 MB (U(h) = 0.178581*h^0.276432)
Recommendation: 2 clients, utility 2.45254:
    h1: 1584.79 MB (U(h) = 0.0130571*h^0.702947)
    h2: 623.214 MB (U(h) = 0.178581*h^0.276432)
Reading from 18353: heap size 522 MB, throughput 0.991198
Reading from 18354: heap size 522 MB, throughput 0.994367
Reading from 18353: heap size 524 MB, throughput 0.990751
Reading from 18354: heap size 523 MB, throughput 0.993419
Reading from 18353: heap size 522 MB, throughput 0.840564
Reading from 18354: heap size 519 MB, throughput 0.990518
Numeric result:
Recommendation: 2 clients, utility 2.72093:
    h1: 1650.73 MB (U(h) = 0.00800115*h^0.783774)
    h2: 557.269 MB (U(h) = 0.191878*h^0.2646)
Recommendation: 2 clients, utility 2.72093:
    h1: 1650.72 MB (U(h) = 0.00800115*h^0.783774)
    h2: 557.279 MB (U(h) = 0.191878*h^0.2646)
Reading from 18353: heap size 524 MB, throughput 0.907553
Reading from 18354: heap size 522 MB, throughput 0.989502
Reading from 18353: heap size 527 MB, throughput 0.98876
Reading from 18353: heap size 528 MB, throughput 0.981703
Reading from 18354: heap size 523 MB, throughput 0.99034
Reading from 18353: heap size 531 MB, throughput 0.982798
Reading from 18354: heap size 525 MB, throughput 0.984516
Reading from 18354: heap size 522 MB, throughput 0.975245
Reading from 18354: heap size 529 MB, throughput 0.98255
Reading from 18353: heap size 533 MB, throughput 0.988832
Reading from 18354: heap size 534 MB, throughput 0.988516
Numeric result:
Recommendation: 2 clients, utility 2.05733:
    h1: 1580.88 MB (U(h) = 0.0264614*h^0.585452)
    h2: 627.121 MB (U(h) = 0.23346*h^0.232247)
Recommendation: 2 clients, utility 2.05733:
    h1: 1580.87 MB (U(h) = 0.0264614*h^0.585452)
    h2: 627.127 MB (U(h) = 0.23346*h^0.232247)
Reading from 18353: heap size 536 MB, throughput 0.990572
Reading from 18354: heap size 539 MB, throughput 0.988418
Reading from 18353: heap size 537 MB, throughput 0.992271
Reading from 18354: heap size 541 MB, throughput 0.988855
Reading from 18353: heap size 535 MB, throughput 0.990819
Reading from 18354: heap size 543 MB, throughput 0.98844
Numeric result:
Recommendation: 2 clients, utility 1.46685:
    h1: 1286.16 MB (U(h) = 0.142532*h^0.308623)
    h2: 921.845 MB (U(h) = 0.249501*h^0.221206)
Recommendation: 2 clients, utility 1.46685:
    h1: 1286.15 MB (U(h) = 0.142532*h^0.308623)
    h2: 921.85 MB (U(h) = 0.249501*h^0.221206)
Reading from 18353: heap size 537 MB, throughput 0.991512
Reading from 18354: heap size 547 MB, throughput 0.987281
Reading from 18353: heap size 539 MB, throughput 0.992032
Reading from 18353: heap size 540 MB, throughput 0.983319
Reading from 18353: heap size 540 MB, throughput 0.981466
Reading from 18354: heap size 549 MB, throughput 0.990207
Reading from 18354: heap size 551 MB, throughput 0.985791
Reading from 18354: heap size 554 MB, throughput 0.974038
Reading from 18353: heap size 551 MB, throughput 0.991253
Reading from 18354: heap size 562 MB, throughput 0.988705
Numeric result:
Recommendation: 2 clients, utility 1.33784:
    h1: 1570.6 MB (U(h) = 0.171534*h^0.278219)
    h2: 637.404 MB (U(h) = 0.485503*h^0.112913)
Recommendation: 2 clients, utility 1.33784:
    h1: 1570.59 MB (U(h) = 0.171534*h^0.278219)
    h2: 637.411 MB (U(h) = 0.485503*h^0.112913)
Reading from 18353: heap size 554 MB, throughput 0.992815
Reading from 18354: heap size 564 MB, throughput 0.991615
Reading from 18353: heap size 556 MB, throughput 0.992363
Reading from 18354: heap size 566 MB, throughput 0.99157
Reading from 18353: heap size 556 MB, throughput 0.992461
Reading from 18354: heap size 570 MB, throughput 0.990717
Numeric result:
Recommendation: 2 clients, utility 1.35927:
    h1: 1790.4 MB (U(h) = 0.15699*h^0.292635)
    h2: 417.597 MB (U(h) = 0.640647*h^0.068258)
Recommendation: 2 clients, utility 1.35927:
    h1: 1790.39 MB (U(h) = 0.15699*h^0.292635)
    h2: 417.613 MB (U(h) = 0.640647*h^0.068258)
Reading from 18353: heap size 559 MB, throughput 0.991201
Reading from 18354: heap size 569 MB, throughput 0.991084
Reading from 18353: heap size 560 MB, throughput 0.989389
Reading from 18353: heap size 561 MB, throughput 0.981549
Reading from 18353: heap size 565 MB, throughput 0.984702
Reading from 18354: heap size 543 MB, throughput 0.992216
Reading from 18354: heap size 542 MB, throughput 0.988927
Reading from 18354: heap size 533 MB, throughput 0.982761
Reading from 18353: heap size 570 MB, throughput 0.989499
Reading from 18354: heap size 531 MB, throughput 0.987746
Numeric result:
Recommendation: 2 clients, utility 1.39714:
    h1: 1783.46 MB (U(h) = 0.136945*h^0.314644)
    h2: 424.535 MB (U(h) = 0.615005*h^0.074898)
Recommendation: 2 clients, utility 1.39714:
    h1: 1783.46 MB (U(h) = 0.136945*h^0.314644)
    h2: 424.537 MB (U(h) = 0.615005*h^0.074898)
Reading from 18354: heap size 498 MB, throughput 0.990357
Reading from 18353: heap size 567 MB, throughput 0.991723
Reading from 18354: heap size 517 MB, throughput 0.990474
Reading from 18353: heap size 571 MB, throughput 0.990832
Reading from 18354: heap size 491 MB, throughput 0.990416
Reading from 18354: heap size 501 MB, throughput 0.989686
Numeric result:
Recommendation: 2 clients, utility 1.3969:
    h1: 1765.28 MB (U(h) = 0.136584*h^0.314989)
    h2: 442.718 MB (U(h) = 0.599808*h^0.0789989)
Recommendation: 2 clients, utility 1.3969:
    h1: 1765.27 MB (U(h) = 0.136584*h^0.314989)
    h2: 442.727 MB (U(h) = 0.599808*h^0.0789989)
Reading from 18353: heap size 575 MB, throughput 0.989005
Reading from 18354: heap size 484 MB, throughput 0.988754
Reading from 18353: heap size 576 MB, throughput 0.99142
Reading from 18353: heap size 581 MB, throughput 0.98689
Reading from 18353: heap size 582 MB, throughput 0.980462
Reading from 18354: heap size 491 MB, throughput 0.985209
Reading from 18354: heap size 490 MB, throughput 0.983667
Reading from 18354: heap size 488 MB, throughput 0.977942
Reading from 18354: heap size 474 MB, throughput 0.965799
Reading from 18353: heap size 592 MB, throughput 0.989202
Reading from 18354: heap size 480 MB, throughput 0.985824
Numeric result:
Recommendation: 2 clients, utility 1.28009:
    h1: 1617.01 MB (U(h) = 0.209315*h^0.245733)
    h2: 590.986 MB (U(h) = 0.561118*h^0.0898032)
Recommendation: 2 clients, utility 1.28009:
    h1: 1617.05 MB (U(h) = 0.209315*h^0.245733)
    h2: 590.951 MB (U(h) = 0.561118*h^0.0898032)
Reading from 18354: heap size 445 MB, throughput 0.989706
Reading from 18353: heap size 591 MB, throughput 0.989811
Reading from 18354: heap size 480 MB, throughput 0.989167
Reading from 18354: heap size 480 MB, throughput 0.98917
Reading from 18353: heap size 590 MB, throughput 0.990471
Reading from 18354: heap size 482 MB, throughput 0.988218
Reading from 18353: heap size 593 MB, throughput 0.989977
Numeric result:
Recommendation: 2 clients, utility 1.19017:
    h1: 1475.81 MB (U(h) = 0.312774*h^0.181139)
    h2: 732.195 MB (U(h) = 0.560919*h^0.0898696)
Recommendation: 2 clients, utility 1.19017:
    h1: 1475.8 MB (U(h) = 0.312774*h^0.181139)
    h2: 732.198 MB (U(h) = 0.560919*h^0.0898696)
Reading from 18354: heap size 483 MB, throughput 0.989895
Reading from 18354: heap size 481 MB, throughput 0.987793
Reading from 18353: heap size 594 MB, throughput 0.98916
Reading from 18353: heap size 595 MB, throughput 0.986969
Reading from 18353: heap size 602 MB, throughput 0.984062
Reading from 18354: heap size 483 MB, throughput 0.985949
Reading from 18354: heap size 487 MB, throughput 0.987628
Reading from 18354: heap size 487 MB, throughput 0.978362
Reading from 18354: heap size 488 MB, throughput 0.957929
Reading from 18354: heap size 490 MB, throughput 0.9762
Reading from 18353: heap size 602 MB, throughput 0.992914
Numeric result:
Recommendation: 2 clients, utility 1.09469:
    h1: 1021.97 MB (U(h) = 0.573781*h^0.0838683)
    h2: 1186.03 MB (U(h) = 0.5357*h^0.0973376)
Recommendation: 2 clients, utility 1.09469:
    h1: 1021.94 MB (U(h) = 0.573781*h^0.0838683)
    h2: 1186.06 MB (U(h) = 0.5357*h^0.0973376)
Reading from 18354: heap size 500 MB, throughput 0.986698
Reading from 18353: heap size 606 MB, throughput 0.994135
Reading from 18354: heap size 502 MB, throughput 0.989828
Reading from 18353: heap size 607 MB, throughput 0.992992
Reading from 18354: heap size 506 MB, throughput 0.988695
Reading from 18354: heap size 508 MB, throughput 0.988351
Numeric result:
Recommendation: 2 clients, utility 1.11398:
    h1: 1031.59 MB (U(h) = 0.536375*h^0.0947131)
    h2: 1176.41 MB (U(h) = 0.501603*h^0.108003)
Recommendation: 2 clients, utility 1.11398:
    h1: 1031.62 MB (U(h) = 0.536375*h^0.0947131)
    h2: 1176.38 MB (U(h) = 0.501603*h^0.108003)
Reading from 18353: heap size 606 MB, throughput 0.992698
Reading from 18354: heap size 509 MB, throughput 0.988966
Reading from 18354: heap size 511 MB, throughput 0.988306
Reading from 18353: heap size 608 MB, throughput 0.993956
Reading from 18353: heap size 609 MB, throughput 0.990033
Client 18353 died
Clients: 1
Reading from 18354: heap size 516 MB, throughput 0.989989
Reading from 18354: heap size 516 MB, throughput 0.984342
Reading from 18354: heap size 518 MB, throughput 0.979213
Client 18354 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
