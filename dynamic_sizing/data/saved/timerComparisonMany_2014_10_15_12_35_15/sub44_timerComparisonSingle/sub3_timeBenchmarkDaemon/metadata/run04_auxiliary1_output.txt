economemd
    total memory: 2760 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub44_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 21327: heap size 9 MB, throughput 0.990982
Clients: 1
Client 21327 has a minimum heap size of 276 MB
Reading from 21328: heap size 9 MB, throughput 0.99379
Clients: 2
Client 21328 has a minimum heap size of 276 MB
Reading from 21327: heap size 9 MB, throughput 0.987145
Reading from 21328: heap size 9 MB, throughput 0.988735
Reading from 21327: heap size 9 MB, throughput 0.980468
Reading from 21328: heap size 9 MB, throughput 0.980845
Reading from 21327: heap size 9 MB, throughput 0.97112
Reading from 21328: heap size 9 MB, throughput 0.972646
Reading from 21327: heap size 11 MB, throughput 0.978021
Reading from 21328: heap size 11 MB, throughput 0.97466
Reading from 21327: heap size 11 MB, throughput 0.963002
Reading from 21328: heap size 11 MB, throughput 0.947767
Reading from 21327: heap size 17 MB, throughput 0.967139
Reading from 21328: heap size 17 MB, throughput 0.958787
Reading from 21328: heap size 17 MB, throughput 0.900086
Reading from 21327: heap size 17 MB, throughput 0.873214
Reading from 21328: heap size 30 MB, throughput 0.718474
Reading from 21327: heap size 30 MB, throughput 0.678002
Reading from 21327: heap size 31 MB, throughput 0.346303
Reading from 21328: heap size 31 MB, throughput 0.287249
Reading from 21327: heap size 34 MB, throughput 0.69878
Reading from 21327: heap size 48 MB, throughput 0.521546
Reading from 21328: heap size 37 MB, throughput 0.651845
Reading from 21328: heap size 49 MB, throughput 0.347554
Reading from 21327: heap size 51 MB, throughput 0.460697
Reading from 21328: heap size 52 MB, throughput 0.358639
Reading from 21327: heap size 52 MB, throughput 0.483027
Reading from 21328: heap size 67 MB, throughput 0.221736
Reading from 21327: heap size 79 MB, throughput 0.367646
Reading from 21327: heap size 79 MB, throughput 0.306246
Reading from 21328: heap size 73 MB, throughput 0.470673
Reading from 21328: heap size 90 MB, throughput 0.338004
Reading from 21327: heap size 82 MB, throughput 0.489379
Reading from 21328: heap size 97 MB, throughput 0.483841
Reading from 21327: heap size 111 MB, throughput 0.544729
Reading from 21327: heap size 114 MB, throughput 0.373386
Reading from 21327: heap size 115 MB, throughput 0.239843
Reading from 21328: heap size 97 MB, throughput 0.484852
Reading from 21328: heap size 131 MB, throughput 0.498924
Reading from 21328: heap size 131 MB, throughput 0.321753
Reading from 21327: heap size 123 MB, throughput 0.501117
Reading from 21327: heap size 150 MB, throughput 0.0376358
Reading from 21327: heap size 159 MB, throughput 0.0748462
Reading from 21327: heap size 160 MB, throughput 0.774087
Reading from 21328: heap size 134 MB, throughput 0.357525
Reading from 21328: heap size 169 MB, throughput 0.389013
Reading from 21328: heap size 174 MB, throughput 0.488334
Reading from 21328: heap size 175 MB, throughput 0.151501
Reading from 21327: heap size 163 MB, throughput 0.432583
Reading from 21328: heap size 178 MB, throughput 0.724104
Reading from 21327: heap size 200 MB, throughput 0.745639
Reading from 21327: heap size 206 MB, throughput 0.723377
Reading from 21328: heap size 184 MB, throughput 0.47318
Reading from 21328: heap size 231 MB, throughput 0.834844
Reading from 21327: heap size 210 MB, throughput 0.900281
Reading from 21328: heap size 233 MB, throughput 0.893881
Reading from 21328: heap size 235 MB, throughput 0.875938
Reading from 21328: heap size 241 MB, throughput 0.831648
Reading from 21328: heap size 246 MB, throughput 0.795805
Reading from 21328: heap size 250 MB, throughput 0.753618
Reading from 21327: heap size 211 MB, throughput 0.601047
Reading from 21327: heap size 262 MB, throughput 0.823046
Reading from 21328: heap size 257 MB, throughput 0.750991
Reading from 21327: heap size 264 MB, throughput 0.826536
Reading from 21328: heap size 260 MB, throughput 0.83427
Reading from 21327: heap size 266 MB, throughput 0.772255
Reading from 21328: heap size 265 MB, throughput 0.75401
Reading from 21327: heap size 270 MB, throughput 0.808438
Reading from 21328: heap size 267 MB, throughput 0.729391
Reading from 21327: heap size 270 MB, throughput 0.847957
Reading from 21328: heap size 273 MB, throughput 0.728769
Reading from 21328: heap size 277 MB, throughput 0.731455
Reading from 21327: heap size 273 MB, throughput 0.873874
Reading from 21328: heap size 283 MB, throughput 0.741591
Reading from 21327: heap size 274 MB, throughput 0.845457
Reading from 21327: heap size 279 MB, throughput 0.833506
Reading from 21328: heap size 285 MB, throughput 0.839844
Reading from 21327: heap size 282 MB, throughput 0.822982
Reading from 21327: heap size 288 MB, throughput 0.800457
Reading from 21328: heap size 282 MB, throughput 0.896309
Reading from 21327: heap size 289 MB, throughput 0.892708
Reading from 21327: heap size 294 MB, throughput 0.830611
Reading from 21327: heap size 296 MB, throughput 0.754955
Reading from 21328: heap size 286 MB, throughput 0.697436
Reading from 21328: heap size 326 MB, throughput 0.686003
Reading from 21328: heap size 328 MB, throughput 0.753448
Reading from 21328: heap size 332 MB, throughput 0.770125
Reading from 21327: heap size 295 MB, throughput 0.679946
Reading from 21328: heap size 332 MB, throughput 0.849093
Reading from 21327: heap size 340 MB, throughput 0.724435
Reading from 21328: heap size 330 MB, throughput 0.818244
Reading from 21328: heap size 333 MB, throughput 0.683362
Reading from 21327: heap size 346 MB, throughput 0.803888
Reading from 21328: heap size 337 MB, throughput 0.687831
Reading from 21327: heap size 346 MB, throughput 0.704731
Reading from 21328: heap size 336 MB, throughput 0.638325
Reading from 21327: heap size 353 MB, throughput 0.702329
Reading from 21328: heap size 343 MB, throughput 0.620037
Reading from 21328: heap size 343 MB, throughput 0.615807
Reading from 21327: heap size 353 MB, throughput 0.653803
Reading from 21327: heap size 362 MB, throughput 0.697196
Reading from 21328: heap size 352 MB, throughput 0.705728
Reading from 21327: heap size 362 MB, throughput 0.653176
Numeric result:
Recommendation: 2 clients, utility 0.4573:
    h1: 905.205 MB (U(h) = 0.484149*h^0.0364148)
    h2: 1854.8 MB (U(h) = 0.420434*h^0.0746123)
Recommendation: 2 clients, utility 0.4573:
    h1: 905.228 MB (U(h) = 0.484149*h^0.0364148)
    h2: 1854.77 MB (U(h) = 0.420434*h^0.0746123)
Reading from 21327: heap size 372 MB, throughput 0.926859
Reading from 21328: heap size 353 MB, throughput 0.95542
Reading from 21327: heap size 373 MB, throughput 0.971693
Reading from 21328: heap size 354 MB, throughput 0.970659
Reading from 21327: heap size 377 MB, throughput 0.965532
Reading from 21328: heap size 358 MB, throughput 0.844093
Reading from 21327: heap size 380 MB, throughput 0.973962
Reading from 21328: heap size 401 MB, throughput 0.984239
Reading from 21327: heap size 380 MB, throughput 0.973914
Reading from 21328: heap size 402 MB, throughput 0.988281
Reading from 21327: heap size 383 MB, throughput 0.976936
Reading from 21328: heap size 411 MB, throughput 0.991181
Numeric result:
Recommendation: 2 clients, utility 0.610677:
    h1: 1181.19 MB (U(h) = 0.397779*h^0.0861457)
    h2: 1578.81 MB (U(h) = 0.357517*h^0.115124)
Recommendation: 2 clients, utility 0.610677:
    h1: 1181.31 MB (U(h) = 0.397779*h^0.0861457)
    h2: 1578.69 MB (U(h) = 0.357517*h^0.115124)
Reading from 21327: heap size 385 MB, throughput 0.980294
Reading from 21328: heap size 412 MB, throughput 0.99127
Reading from 21327: heap size 387 MB, throughput 0.971368
Reading from 21328: heap size 411 MB, throughput 0.981194
Reading from 21328: heap size 413 MB, throughput 0.983198
Reading from 21327: heap size 387 MB, throughput 0.973991
Reading from 21328: heap size 409 MB, throughput 0.976766
Reading from 21327: heap size 389 MB, throughput 0.971557
Reading from 21327: heap size 392 MB, throughput 0.974782
Reading from 21328: heap size 412 MB, throughput 0.982091
Reading from 21327: heap size 393 MB, throughput 0.969885
Reading from 21328: heap size 405 MB, throughput 0.981823
Numeric result:
Recommendation: 2 clients, utility 0.728874:
    h1: 1292.74 MB (U(h) = 0.347048*h^0.120376)
    h2: 1467.26 MB (U(h) = 0.327402*h^0.136627)
Recommendation: 2 clients, utility 0.728874:
    h1: 1292.74 MB (U(h) = 0.347048*h^0.120376)
    h2: 1467.26 MB (U(h) = 0.327402*h^0.136627)
Reading from 21328: heap size 408 MB, throughput 0.978617
Reading from 21327: heap size 397 MB, throughput 0.97992
Reading from 21328: heap size 409 MB, throughput 0.983623
Reading from 21327: heap size 398 MB, throughput 0.975716
Reading from 21327: heap size 397 MB, throughput 0.961764
Reading from 21328: heap size 409 MB, throughput 0.970059
Reading from 21327: heap size 398 MB, throughput 0.9376
Reading from 21328: heap size 405 MB, throughput 0.949188
Reading from 21327: heap size 408 MB, throughput 0.88441
Reading from 21328: heap size 414 MB, throughput 0.895117
Reading from 21328: heap size 422 MB, throughput 0.850998
Reading from 21328: heap size 427 MB, throughput 0.938639
Reading from 21327: heap size 410 MB, throughput 0.960128
Reading from 21328: heap size 430 MB, throughput 0.977585
Reading from 21327: heap size 418 MB, throughput 0.982006
Reading from 21328: heap size 436 MB, throughput 0.982822
Reading from 21327: heap size 420 MB, throughput 0.984433
Reading from 21328: heap size 432 MB, throughput 0.979685
Numeric result:
Recommendation: 2 clients, utility 0.851881:
    h1: 1325.16 MB (U(h) = 0.311276*h^0.147267)
    h2: 1434.84 MB (U(h) = 0.297887*h^0.159457)
Recommendation: 2 clients, utility 0.851881:
    h1: 1325.16 MB (U(h) = 0.311276*h^0.147267)
    h2: 1434.84 MB (U(h) = 0.297887*h^0.159457)
Reading from 21327: heap size 421 MB, throughput 0.986931
Reading from 21328: heap size 436 MB, throughput 0.982909
Reading from 21327: heap size 424 MB, throughput 0.986347
Reading from 21328: heap size 431 MB, throughput 0.983151
Reading from 21327: heap size 423 MB, throughput 0.986271
Reading from 21328: heap size 435 MB, throughput 0.983177
Reading from 21328: heap size 431 MB, throughput 0.981268
Reading from 21327: heap size 425 MB, throughput 0.98564
Reading from 21328: heap size 434 MB, throughput 0.982375
Reading from 21327: heap size 423 MB, throughput 0.987169
Numeric result:
Recommendation: 2 clients, utility 0.91839:
    h1: 1353.79 MB (U(h) = 0.292646*h^0.162345)
    h2: 1406.21 MB (U(h) = 0.286728*h^0.16862)
Recommendation: 2 clients, utility 0.91839:
    h1: 1353.83 MB (U(h) = 0.292646*h^0.162345)
    h2: 1406.17 MB (U(h) = 0.286728*h^0.16862)
Reading from 21328: heap size 432 MB, throughput 0.981876
Reading from 21327: heap size 425 MB, throughput 0.984923
Reading from 21328: heap size 434 MB, throughput 0.98122
Reading from 21327: heap size 427 MB, throughput 0.986871
Reading from 21328: heap size 433 MB, throughput 0.985551
Reading from 21328: heap size 434 MB, throughput 0.972517
Reading from 21328: heap size 436 MB, throughput 0.95697
Reading from 21327: heap size 427 MB, throughput 0.958606
Reading from 21328: heap size 437 MB, throughput 0.940441
Reading from 21327: heap size 469 MB, throughput 0.957639
Reading from 21327: heap size 470 MB, throughput 0.960328
Reading from 21328: heap size 444 MB, throughput 0.971025
Reading from 21327: heap size 476 MB, throughput 0.982436
Reading from 21328: heap size 445 MB, throughput 0.98644
Reading from 21327: heap size 479 MB, throughput 0.992125
Numeric result:
Recommendation: 2 clients, utility 0.986398:
    h1: 1373.96 MB (U(h) = 0.275693*h^0.176513)
    h2: 1386.04 MB (U(h) = 0.275596*h^0.178071)
Recommendation: 2 clients, utility 0.986398:
    h1: 1373.93 MB (U(h) = 0.275693*h^0.176513)
    h2: 1386.07 MB (U(h) = 0.275596*h^0.178071)
Reading from 21328: heap size 449 MB, throughput 0.989037
Reading from 21327: heap size 483 MB, throughput 0.992261
Reading from 21328: heap size 451 MB, throughput 0.988776
Reading from 21327: heap size 484 MB, throughput 0.992875
Reading from 21328: heap size 451 MB, throughput 0.98868
Reading from 21327: heap size 479 MB, throughput 0.992683
Reading from 21328: heap size 453 MB, throughput 0.988136
Reading from 21327: heap size 447 MB, throughput 0.990884
Reading from 21328: heap size 451 MB, throughput 0.986521
Reading from 21327: heap size 478 MB, throughput 0.990638
Numeric result:
Recommendation: 2 clients, utility 1.03701:
    h1: 1385.51 MB (U(h) = 0.264401*h^0.186383)
    h2: 1374.49 MB (U(h) = 0.26775*h^0.184902)
Recommendation: 2 clients, utility 1.03701:
    h1: 1385.51 MB (U(h) = 0.264401*h^0.186383)
    h2: 1374.49 MB (U(h) = 0.26775*h^0.184902)
Reading from 21328: heap size 453 MB, throughput 0.986092
Reading from 21327: heap size 479 MB, throughput 0.987917
Reading from 21328: heap size 454 MB, throughput 0.983853
Reading from 21327: heap size 480 MB, throughput 0.986265
Reading from 21328: heap size 454 MB, throughput 0.9893
Reading from 21328: heap size 456 MB, throughput 0.983318
Reading from 21327: heap size 481 MB, throughput 0.98681
Reading from 21328: heap size 457 MB, throughput 0.970912
Reading from 21327: heap size 485 MB, throughput 0.977372
Reading from 21328: heap size 462 MB, throughput 0.957936
Reading from 21327: heap size 486 MB, throughput 0.958762
Reading from 21327: heap size 493 MB, throughput 0.964931
Reading from 21328: heap size 463 MB, throughput 0.983904
Reading from 21327: heap size 498 MB, throughput 0.982047
Numeric result:
Recommendation: 2 clients, utility 1.09834:
    h1: 1390.95 MB (U(h) = 0.252766*h^0.1969)
    h2: 1369.05 MB (U(h) = 0.257823*h^0.193782)
Recommendation: 2 clients, utility 1.09834:
    h1: 1391.01 MB (U(h) = 0.252766*h^0.1969)
    h2: 1368.99 MB (U(h) = 0.257823*h^0.193782)
Reading from 21328: heap size 469 MB, throughput 0.987537
Reading from 21327: heap size 495 MB, throughput 0.985858
Reading from 21328: heap size 470 MB, throughput 0.98867
Reading from 21327: heap size 499 MB, throughput 0.986687
Reading from 21328: heap size 470 MB, throughput 0.988145
Reading from 21327: heap size 501 MB, throughput 0.988849
Reading from 21328: heap size 472 MB, throughput 0.987753
Reading from 21327: heap size 502 MB, throughput 0.989124
Reading from 21328: heap size 471 MB, throughput 0.986886
Numeric result:
Recommendation: 2 clients, utility 1.13909:
    h1: 1389.69 MB (U(h) = 0.246206*h^0.202991)
    h2: 1370.31 MB (U(h) = 0.250858*h^0.200153)
Recommendation: 2 clients, utility 1.13909:
    h1: 1389.71 MB (U(h) = 0.246206*h^0.202991)
    h2: 1370.29 MB (U(h) = 0.250858*h^0.200153)
Reading from 21327: heap size 505 MB, throughput 0.98788
Reading from 21328: heap size 473 MB, throughput 0.985519
Reading from 21327: heap size 506 MB, throughput 0.986156
Reading from 21328: heap size 475 MB, throughput 0.985386
Reading from 21328: heap size 476 MB, throughput 0.987305
Reading from 21328: heap size 477 MB, throughput 0.978983
Reading from 21328: heap size 479 MB, throughput 0.966512
Reading from 21327: heap size 509 MB, throughput 0.987028
Reading from 21327: heap size 510 MB, throughput 0.977678
Reading from 21327: heap size 511 MB, throughput 0.967166
Reading from 21328: heap size 485 MB, throughput 0.985104
Reading from 21327: heap size 512 MB, throughput 0.9792
Numeric result:
Recommendation: 2 clients, utility 1.18787:
    h1: 1387.67 MB (U(h) = 0.238846*h^0.209976)
    h2: 1372.33 MB (U(h) = 0.242855*h^0.207653)
Recommendation: 2 clients, utility 1.18787:
    h1: 1387.68 MB (U(h) = 0.238846*h^0.209976)
    h2: 1372.32 MB (U(h) = 0.242855*h^0.207653)
Reading from 21328: heap size 487 MB, throughput 0.990232
Reading from 21327: heap size 521 MB, throughput 0.991215
Reading from 21328: heap size 491 MB, throughput 0.990799
Reading from 21327: heap size 521 MB, throughput 0.991947
Reading from 21328: heap size 492 MB, throughput 0.990506
Reading from 21327: heap size 523 MB, throughput 0.990998
Reading from 21328: heap size 492 MB, throughput 0.99041
Numeric result:
Recommendation: 2 clients, utility 1.20693:
    h1: 1383.4 MB (U(h) = 0.236586*h^0.212142)
    h2: 1376.6 MB (U(h) = 0.23926*h^0.211065)
Recommendation: 2 clients, utility 1.20693:
    h1: 1383.51 MB (U(h) = 0.236586*h^0.212142)
    h2: 1376.49 MB (U(h) = 0.23926*h^0.211065)
Reading from 21327: heap size 525 MB, throughput 0.99033
Reading from 21328: heap size 494 MB, throughput 0.989019
Reading from 21327: heap size 522 MB, throughput 0.989419
Reading from 21328: heap size 495 MB, throughput 0.988192
Reading from 21327: heap size 525 MB, throughput 0.987939
Reading from 21328: heap size 495 MB, throughput 0.990056
Reading from 21328: heap size 496 MB, throughput 0.984146
Reading from 21328: heap size 498 MB, throughput 0.973071
Reading from 21327: heap size 527 MB, throughput 0.991031
Reading from 21327: heap size 527 MB, throughput 0.985392
Reading from 21327: heap size 528 MB, throughput 0.978424
Numeric result:
Recommendation: 2 clients, utility 1.23267:
    h1: 1383.16 MB (U(h) = 0.232792*h^0.215819)
    h2: 1376.84 MB (U(h) = 0.235334*h^0.214832)
Recommendation: 2 clients, utility 1.23267:
    h1: 1383.16 MB (U(h) = 0.232792*h^0.215819)
    h2: 1376.84 MB (U(h) = 0.235334*h^0.214832)
Reading from 21328: heap size 504 MB, throughput 0.985386
Reading from 21327: heap size 530 MB, throughput 0.986607
Reading from 21328: heap size 505 MB, throughput 0.990395
Reading from 21327: heap size 536 MB, throughput 0.990535
Reading from 21328: heap size 508 MB, throughput 0.991926
Reading from 21327: heap size 537 MB, throughput 0.990698
Reading from 21328: heap size 510 MB, throughput 0.9924
Numeric result:
Recommendation: 2 clients, utility 1.25338:
    h1: 1380.09 MB (U(h) = 0.230249*h^0.2183)
    h2: 1379.91 MB (U(h) = 0.231744*h^0.218302)
Recommendation: 2 clients, utility 1.25338:
    h1: 1380 MB (U(h) = 0.230249*h^0.2183)
    h2: 1380 MB (U(h) = 0.231744*h^0.218302)
Reading from 21327: heap size 539 MB, throughput 0.991146
Reading from 21328: heap size 508 MB, throughput 0.990912
Reading from 21327: heap size 540 MB, throughput 0.990158
Reading from 21328: heap size 511 MB, throughput 0.98967
Reading from 21327: heap size 540 MB, throughput 0.98961
Reading from 21328: heap size 513 MB, throughput 0.989968
Reading from 21328: heap size 514 MB, throughput 0.986826
Reading from 21328: heap size 518 MB, throughput 0.979867
Numeric result:
Recommendation: 2 clients, utility 1.37505:
    h1: 1222.04 MB (U(h) = 0.228695*h^0.219825)
    h2: 1537.96 MB (U(h) = 0.165485*h^0.276654)
Recommendation: 2 clients, utility 1.37505:
    h1: 1222.04 MB (U(h) = 0.228695*h^0.219825)
    h2: 1537.96 MB (U(h) = 0.165485*h^0.276654)
Reading from 21327: heap size 541 MB, throughput 0.990186
Reading from 21328: heap size 520 MB, throughput 0.981703
Reading from 21327: heap size 545 MB, throughput 0.986121
Reading from 21327: heap size 545 MB, throughput 0.974945
Reading from 21327: heap size 550 MB, throughput 0.984687
Reading from 21328: heap size 526 MB, throughput 0.990164
Reading from 21327: heap size 551 MB, throughput 0.988946
Reading from 21328: heap size 527 MB, throughput 0.991204
Reading from 21327: heap size 556 MB, throughput 0.990777
Reading from 21328: heap size 528 MB, throughput 0.99106
Numeric result:
Recommendation: 2 clients, utility 2.06016:
    h1: 981.818 MB (U(h) = 0.166746*h^0.274372)
    h2: 1778.18 MB (U(h) = 0.0452917*h^0.496896)
Recommendation: 2 clients, utility 2.06016:
    h1: 981.847 MB (U(h) = 0.166746*h^0.274372)
    h2: 1778.15 MB (U(h) = 0.0452917*h^0.496896)
Reading from 21328: heap size 529 MB, throughput 0.98824
Reading from 21327: heap size 557 MB, throughput 0.988712
Reading from 21328: heap size 528 MB, throughput 0.988031
Reading from 21327: heap size 557 MB, throughput 0.989351
Reading from 21327: heap size 559 MB, throughput 0.988653
Reading from 21328: heap size 530 MB, throughput 0.991553
Reading from 21328: heap size 530 MB, throughput 0.987168
Numeric result:
Recommendation: 2 clients, utility 2.72507:
    h1: 1134.25 MB (U(h) = 0.0747892*h^0.410256)
    h2: 1625.75 MB (U(h) = 0.0263108*h^0.588029)
Recommendation: 2 clients, utility 2.72507:
    h1: 1134.25 MB (U(h) = 0.0747892*h^0.410256)
    h2: 1625.75 MB (U(h) = 0.0263108*h^0.588029)
Reading from 21328: heap size 532 MB, throughput 0.979131
Reading from 21327: heap size 563 MB, throughput 0.989521
Reading from 21327: heap size 563 MB, throughput 0.983257
Reading from 21328: heap size 536 MB, throughput 0.987365
Reading from 21327: heap size 568 MB, throughput 0.984595
Reading from 21328: heap size 538 MB, throughput 0.991433
Reading from 21327: heap size 569 MB, throughput 0.989568
Reading from 21328: heap size 540 MB, throughput 0.991898
Reading from 21327: heap size 573 MB, throughput 0.99133
Numeric result:
Recommendation: 2 clients, utility 3.40786:
    h1: 1340.28 MB (U(h) = 0.0270132*h^0.580093)
    h2: 1419.72 MB (U(h) = 0.0223818*h^0.614474)
Recommendation: 2 clients, utility 3.40786:
    h1: 1340.28 MB (U(h) = 0.0270132*h^0.580093)
    h2: 1419.72 MB (U(h) = 0.0223818*h^0.614474)
Reading from 21328: heap size 542 MB, throughput 0.991999
Reading from 21327: heap size 575 MB, throughput 0.99139
Reading from 21328: heap size 539 MB, throughput 0.991657
Reading from 21327: heap size 574 MB, throughput 0.992513
Reading from 21328: heap size 542 MB, throughput 0.990673
Reading from 21327: heap size 576 MB, throughput 0.990778
Numeric result:
Recommendation: 2 clients, utility 3.99751:
    h1: 1440.78 MB (U(h) = 0.0134823*h^0.694813)
    h2: 1319.22 MB (U(h) = 0.0196008*h^0.63619)
Recommendation: 2 clients, utility 3.99751:
    h1: 1440.78 MB (U(h) = 0.0134823*h^0.694813)
    h2: 1319.22 MB (U(h) = 0.0196008*h^0.63619)
Reading from 21328: heap size 544 MB, throughput 0.990695
Reading from 21328: heap size 545 MB, throughput 0.984912
Reading from 21327: heap size 578 MB, throughput 0.991175
Reading from 21327: heap size 579 MB, throughput 0.983901
Reading from 21328: heap size 548 MB, throughput 0.987157
Reading from 21327: heap size 583 MB, throughput 0.986674
Reading from 21328: heap size 550 MB, throughput 0.991661
Reading from 21327: heap size 585 MB, throughput 0.991286
Numeric result:
Recommendation: 2 clients, utility 3.42102:
    h1: 1795.09 MB (U(h) = 0.00938834*h^0.753275)
    h2: 964.909 MB (U(h) = 0.0797827*h^0.404906)
Recommendation: 2 clients, utility 3.42102:
    h1: 1795.09 MB (U(h) = 0.00938834*h^0.753275)
    h2: 964.91 MB (U(h) = 0.0797827*h^0.404906)
Reading from 21328: heap size 553 MB, throughput 0.993516
Reading from 21327: heap size 589 MB, throughput 0.993074
Reading from 21328: heap size 554 MB, throughput 0.993456
Reading from 21327: heap size 590 MB, throughput 0.991617
Reading from 21328: heap size 551 MB, throughput 0.992357
Reading from 21327: heap size 590 MB, throughput 0.991395
Numeric result:
Recommendation: 2 clients, utility 2.54979:
    h1: 1621.68 MB (U(h) = 0.0332526*h^0.546609)
    h2: 1138.32 MB (U(h) = 0.0906691*h^0.383679)
Recommendation: 2 clients, utility 2.54979:
    h1: 1621.69 MB (U(h) = 0.0332526*h^0.546609)
    h2: 1138.31 MB (U(h) = 0.0906691*h^0.383679)
Reading from 21328: heap size 554 MB, throughput 0.991121
Reading from 21328: heap size 556 MB, throughput 0.991243
Reading from 21327: heap size 592 MB, throughput 0.991935
Reading from 21328: heap size 556 MB, throughput 0.98532
Reading from 21327: heap size 592 MB, throughput 0.987262
Reading from 21327: heap size 594 MB, throughput 0.984964
Reading from 21328: heap size 559 MB, throughput 0.986719
Reading from 21327: heap size 601 MB, throughput 0.991259
Reading from 21328: heap size 561 MB, throughput 0.991582
Numeric result:
Recommendation: 2 clients, utility 2.04978:
    h1: 1124.23 MB (U(h) = 0.151717*h^0.299197)
    h2: 1635.77 MB (U(h) = 0.0658974*h^0.43532)
Recommendation: 2 clients, utility 2.04978:
    h1: 1124.25 MB (U(h) = 0.151717*h^0.299197)
    h2: 1635.75 MB (U(h) = 0.0658974*h^0.43532)
Reading from 21327: heap size 602 MB, throughput 0.992672
Reading from 21328: heap size 564 MB, throughput 0.991823
Reading from 21328: heap size 565 MB, throughput 0.991712
Reading from 21327: heap size 603 MB, throughput 0.992894
Reading from 21327: heap size 605 MB, throughput 0.992179
Reading from 21328: heap size 563 MB, throughput 0.990788
Numeric result:
Recommendation: 2 clients, utility 2.09499:
    h1: 1136.85 MB (U(h) = 0.139248*h^0.312803)
    h2: 1623.15 MB (U(h) = 0.06134*h^0.446622)
Recommendation: 2 clients, utility 2.09499:
    h1: 1136.83 MB (U(h) = 0.139248*h^0.312803)
    h2: 1623.17 MB (U(h) = 0.06134*h^0.446622)
Reading from 21328: heap size 565 MB, throughput 0.989573
Reading from 21327: heap size 606 MB, throughput 0.993128
Reading from 21327: heap size 607 MB, throughput 0.989604
Reading from 21328: heap size 566 MB, throughput 0.988053
Reading from 21327: heap size 607 MB, throughput 0.984816
Reading from 21328: heap size 568 MB, throughput 0.982354
Reading from 21328: heap size 571 MB, throughput 0.990195
Reading from 21327: heap size 610 MB, throughput 0.99295
Numeric result:
Recommendation: 2 clients, utility 2.05702:
    h1: 1057.54 MB (U(h) = 0.167677*h^0.282536)
    h2: 1702.46 MB (U(h) = 0.058171*h^0.454829)
Recommendation: 2 clients, utility 2.05702:
    h1: 1057.55 MB (U(h) = 0.167677*h^0.282536)
    h2: 1702.45 MB (U(h) = 0.058171*h^0.454829)
Reading from 21328: heap size 573 MB, throughput 0.992624
Reading from 21327: heap size 613 MB, throughput 0.993911
Reading from 21328: heap size 575 MB, throughput 0.993005
Reading from 21327: heap size 615 MB, throughput 0.992697
Reading from 21328: heap size 576 MB, throughput 0.98609
Numeric result:
Recommendation: 2 clients, utility 1.97951:
    h1: 1073.4 MB (U(h) = 0.176078*h^0.27453)
    h2: 1686.6 MB (U(h) = 0.0671154*h^0.431357)
Recommendation: 2 clients, utility 1.97951:
    h1: 1073.4 MB (U(h) = 0.176078*h^0.27453)
    h2: 1686.6 MB (U(h) = 0.0671154*h^0.431357)
Reading from 21327: heap size 615 MB, throughput 0.992147
Reading from 21328: heap size 592 MB, throughput 0.994421
Reading from 21327: heap size 617 MB, throughput 0.993787
Reading from 21327: heap size 619 MB, throughput 0.990012
Reading from 21327: heap size 620 MB, throughput 0.989003
Reading from 21328: heap size 593 MB, throughput 0.994362
Reading from 21328: heap size 597 MB, throughput 0.990979
Reading from 21328: heap size 598 MB, throughput 0.99111
Numeric result:
Recommendation: 2 clients, utility 1.93872:
    h1: 1116.46 MB (U(h) = 0.168674*h^0.281148)
    h2: 1643.54 MB (U(h) = 0.0745752*h^0.413889)
Recommendation: 2 clients, utility 1.93872:
    h1: 1116.44 MB (U(h) = 0.168674*h^0.281148)
    h2: 1643.56 MB (U(h) = 0.0745752*h^0.413889)
Reading from 21327: heap size 628 MB, throughput 0.992973
Reading from 21328: heap size 603 MB, throughput 0.992714
Reading from 21327: heap size 629 MB, throughput 0.992356
Reading from 21328: heap size 605 MB, throughput 0.99268
Reading from 21327: heap size 627 MB, throughput 0.991468
Numeric result:
Recommendation: 2 clients, utility 1.8637:
    h1: 1158.99 MB (U(h) = 0.171173*h^0.278525)
    h2: 1601.01 MB (U(h) = 0.0892578*h^0.384744)
Recommendation: 2 clients, utility 1.8637:
    h1: 1159 MB (U(h) = 0.171173*h^0.278525)
    h2: 1601 MB (U(h) = 0.0892578*h^0.384744)
Reading from 21328: heap size 602 MB, throughput 0.992169
Reading from 21327: heap size 630 MB, throughput 0.991359
Reading from 21328: heap size 605 MB, throughput 0.99153
Reading from 21327: heap size 632 MB, throughput 0.99114
Reading from 21327: heap size 634 MB, throughput 0.985958
Reading from 21328: heap size 607 MB, throughput 0.993258
Reading from 21328: heap size 608 MB, throughput 0.9895
Reading from 21327: heap size 638 MB, throughput 0.989926
Reading from 21328: heap size 607 MB, throughput 0.986921
Numeric result:
Recommendation: 2 clients, utility 1.54417:
    h1: 1051.54 MB (U(h) = 0.320091*h^0.178531)
    h2: 1708.46 MB (U(h) = 0.160771*h^0.290083)
Recommendation: 2 clients, utility 1.54417:
    h1: 1051.49 MB (U(h) = 0.320091*h^0.178531)
    h2: 1708.51 MB (U(h) = 0.160771*h^0.290083)
Reading from 21328: heap size 610 MB, throughput 0.991926
Reading from 21327: heap size 640 MB, throughput 0.992441
Reading from 21328: heap size 610 MB, throughput 0.992621
Reading from 21327: heap size 642 MB, throughput 0.992278
Numeric result:
Recommendation: 2 clients, utility 1.36358:
    h1: 868.739 MB (U(h) = 0.513433*h^0.103506)
    h2: 1891.26 MB (U(h) = 0.240799*h^0.22533)
Recommendation: 2 clients, utility 1.36358:
    h1: 868.751 MB (U(h) = 0.513433*h^0.103506)
    h2: 1891.25 MB (U(h) = 0.240799*h^0.22533)
Reading from 21328: heap size 613 MB, throughput 0.992431
Reading from 21327: heap size 644 MB, throughput 0.991905
Reading from 21328: heap size 616 MB, throughput 0.991812
Reading from 21327: heap size 646 MB, throughput 0.993337
Reading from 21327: heap size 647 MB, throughput 0.988961
Client 21327 died
Clients: 1
Reading from 21328: heap size 616 MB, throughput 0.99348
Recommendation: one client; give it all the memory
Reading from 21328: heap size 618 MB, throughput 0.98999
Client 21328 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
