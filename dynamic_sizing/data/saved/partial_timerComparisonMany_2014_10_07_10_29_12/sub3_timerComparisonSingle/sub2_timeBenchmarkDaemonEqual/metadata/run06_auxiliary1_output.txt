economemd
    total memory: 6265 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub3_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 20617: heap size 9 MB, throughput 0.991234
Clients: 1
Client 20617 has a minimum heap size of 30 MB
Reading from 20615: heap size 9 MB, throughput 0.975514
Clients: 2
Client 20615 has a minimum heap size of 1223 MB
Reading from 20617: heap size 9 MB, throughput 0.973157
Reading from 20617: heap size 9 MB, throughput 0.938513
Reading from 20615: heap size 9 MB, throughput 0.961477
Reading from 20617: heap size 9 MB, throughput 0.947261
Reading from 20617: heap size 11 MB, throughput 0.858271
Reading from 20617: heap size 11 MB, throughput 0.508315
Reading from 20617: heap size 16 MB, throughput 0.651444
Reading from 20617: heap size 16 MB, throughput 0.727814
Reading from 20617: heap size 24 MB, throughput 0.86978
Reading from 20617: heap size 24 MB, throughput 0.818651
Reading from 20617: heap size 34 MB, throughput 0.909401
Reading from 20617: heap size 34 MB, throughput 0.881669
Reading from 20617: heap size 50 MB, throughput 0.929762
Reading from 20617: heap size 50 MB, throughput 0.921195
Reading from 20615: heap size 11 MB, throughput 0.971429
Reading from 20617: heap size 76 MB, throughput 0.953497
Reading from 20617: heap size 76 MB, throughput 0.869533
Reading from 20617: heap size 115 MB, throughput 0.960755
Reading from 20617: heap size 116 MB, throughput 0.954755
Reading from 20615: heap size 11 MB, throughput 0.978367
Reading from 20615: heap size 15 MB, throughput 0.83758
Reading from 20615: heap size 18 MB, throughput 0.954789
Reading from 20615: heap size 24 MB, throughput 0.930792
Reading from 20615: heap size 28 MB, throughput 0.459851
Reading from 20615: heap size 39 MB, throughput 0.927842
Reading from 20615: heap size 40 MB, throughput 0.882378
Reading from 20615: heap size 43 MB, throughput 0.874437
Reading from 20615: heap size 44 MB, throughput 0.763778
Reading from 20617: heap size 159 MB, throughput 0.956377
Reading from 20615: heap size 48 MB, throughput 0.202708
Reading from 20615: heap size 63 MB, throughput 0.73393
Reading from 20615: heap size 69 MB, throughput 0.772423
Reading from 20617: heap size 166 MB, throughput 0.836204
Reading from 20615: heap size 71 MB, throughput 0.182986
Reading from 20615: heap size 95 MB, throughput 0.728358
Reading from 20615: heap size 97 MB, throughput 0.647993
Reading from 20615: heap size 101 MB, throughput 0.153039
Reading from 20617: heap size 229 MB, throughput 0.993526
Reading from 20615: heap size 125 MB, throughput 0.284771
Reading from 20615: heap size 159 MB, throughput 0.665103
Reading from 20615: heap size 161 MB, throughput 0.84514
Reading from 20615: heap size 164 MB, throughput 0.78535
Reading from 20615: heap size 165 MB, throughput 0.698216
Reading from 20617: heap size 238 MB, throughput 0.994113
Reading from 20615: heap size 169 MB, throughput 0.106915
Reading from 20615: heap size 209 MB, throughput 0.702188
Reading from 20615: heap size 217 MB, throughput 0.710968
Reading from 20615: heap size 218 MB, throughput 0.745467
Reading from 20615: heap size 220 MB, throughput 0.737788
Reading from 20615: heap size 228 MB, throughput 0.628679
Reading from 20617: heap size 262 MB, throughput 0.991442
Reading from 20615: heap size 231 MB, throughput 0.716069
Reading from 20615: heap size 238 MB, throughput 0.658892
Reading from 20615: heap size 242 MB, throughput 0.581529
Reading from 20615: heap size 252 MB, throughput 0.526893
Reading from 20615: heap size 256 MB, throughput 0.491137
Reading from 20617: heap size 266 MB, throughput 0.971668
Reading from 20615: heap size 268 MB, throughput 0.0919015
Reading from 20615: heap size 315 MB, throughput 0.440667
Reading from 20617: heap size 293 MB, throughput 0.862535
Reading from 20615: heap size 322 MB, throughput 0.386637
Reading from 20615: heap size 327 MB, throughput 0.0732951
Reading from 20615: heap size 376 MB, throughput 0.489255
Reading from 20615: heap size 365 MB, throughput 0.690801
Reading from 20617: heap size 306 MB, throughput 0.997408
Reading from 20615: heap size 309 MB, throughput 0.683248
Reading from 20615: heap size 363 MB, throughput 0.638521
Reading from 20615: heap size 366 MB, throughput 0.65605
Reading from 20615: heap size 367 MB, throughput 0.49393
Reading from 20615: heap size 370 MB, throughput 0.530065
Reading from 20615: heap size 373 MB, throughput 0.458062
Reading from 20617: heap size 328 MB, throughput 0.988868
Equal recommendation: 3132 MB each
Reading from 20615: heap size 380 MB, throughput 0.100405
Reading from 20615: heap size 434 MB, throughput 0.402
Reading from 20615: heap size 439 MB, throughput 0.528072
Reading from 20615: heap size 440 MB, throughput 0.532448
Reading from 20617: heap size 329 MB, throughput 0.992021
Reading from 20615: heap size 442 MB, throughput 0.537875
Reading from 20615: heap size 447 MB, throughput 0.544506
Reading from 20615: heap size 450 MB, throughput 0.504288
Reading from 20615: heap size 454 MB, throughput 0.44194
Reading from 20617: heap size 350 MB, throughput 0.984302
Reading from 20615: heap size 457 MB, throughput 0.0876474
Reading from 20615: heap size 518 MB, throughput 0.386922
Reading from 20615: heap size 519 MB, throughput 0.497385
Reading from 20615: heap size 514 MB, throughput 0.577865
Reading from 20615: heap size 517 MB, throughput 0.545929
Reading from 20617: heap size 352 MB, throughput 0.979149
Reading from 20615: heap size 521 MB, throughput 0.106666
Reading from 20615: heap size 578 MB, throughput 0.440136
Reading from 20617: heap size 368 MB, throughput 0.999998
Reading from 20617: heap size 368 MB, throughput 0.947368
Reading from 20617: heap size 368 MB, throughput 0.00955066
Reading from 20615: heap size 585 MB, throughput 0.617539
Reading from 20615: heap size 587 MB, throughput 0.529815
Reading from 20615: heap size 589 MB, throughput 0.559551
Reading from 20615: heap size 599 MB, throughput 0.550812
Reading from 20615: heap size 604 MB, throughput 0.547
Reading from 20615: heap size 614 MB, throughput 0.438061
Reading from 20615: heap size 620 MB, throughput 0.554582
Reading from 20617: heap size 374 MB, throughput 0.982032
Reading from 20617: heap size 394 MB, throughput 0.968543
Reading from 20617: heap size 397 MB, throughput 0.988617
Reading from 20615: heap size 629 MB, throughput 0.303359
Reading from 20615: heap size 691 MB, throughput 0.707146
Reading from 20615: heap size 701 MB, throughput 0.824377
Reading from 20617: heap size 421 MB, throughput 0.975263
Reading from 20615: heap size 703 MB, throughput 0.750191
Equal recommendation: 3132 MB each
Reading from 20615: heap size 713 MB, throughput 0.469041
Reading from 20615: heap size 722 MB, throughput 0.554664
Reading from 20615: heap size 723 MB, throughput 0.542173
Reading from 20617: heap size 422 MB, throughput 0.978152
Reading from 20617: heap size 451 MB, throughput 0.973538
Reading from 20615: heap size 721 MB, throughput 0.0165461
Reading from 20615: heap size 705 MB, throughput 0.217106
Reading from 20615: heap size 783 MB, throughput 0.395387
Reading from 20615: heap size 787 MB, throughput 0.42675
Reading from 20617: heap size 451 MB, throughput 0.979018
Reading from 20615: heap size 786 MB, throughput 0.117518
Reading from 20617: heap size 480 MB, throughput 0.981154
Reading from 20615: heap size 866 MB, throughput 0.566571
Reading from 20615: heap size 870 MB, throughput 0.616924
Reading from 20615: heap size 871 MB, throughput 0.656452
Reading from 20615: heap size 878 MB, throughput 0.736468
Reading from 20615: heap size 878 MB, throughput 0.886798
Reading from 20617: heap size 482 MB, throughput 0.9888
Reading from 20615: heap size 882 MB, throughput 0.0887871
Reading from 20615: heap size 972 MB, throughput 0.604287
Reading from 20615: heap size 979 MB, throughput 0.895314
Reading from 20617: heap size 506 MB, throughput 0.976469
Reading from 20615: heap size 980 MB, throughput 0.947774
Reading from 20615: heap size 973 MB, throughput 0.924565
Reading from 20615: heap size 774 MB, throughput 0.921249
Reading from 20615: heap size 948 MB, throughput 0.795035
Reading from 20615: heap size 823 MB, throughput 0.800371
Reading from 20617: heap size 510 MB, throughput 0.935181
Reading from 20615: heap size 931 MB, throughput 0.81977
Equal recommendation: 3132 MB each
Reading from 20615: heap size 827 MB, throughput 0.840425
Reading from 20615: heap size 919 MB, throughput 0.825493
Reading from 20615: heap size 826 MB, throughput 0.837634
Reading from 20615: heap size 911 MB, throughput 0.797646
Reading from 20615: heap size 831 MB, throughput 0.835316
Reading from 20615: heap size 904 MB, throughput 0.821236
Reading from 20615: heap size 910 MB, throughput 0.861544
Reading from 20617: heap size 534 MB, throughput 0.993608
Reading from 20615: heap size 900 MB, throughput 0.86772
Reading from 20615: heap size 905 MB, throughput 0.983312
Reading from 20617: heap size 538 MB, throughput 0.989786
Reading from 20615: heap size 903 MB, throughput 0.931322
Reading from 20615: heap size 905 MB, throughput 0.794659
Reading from 20615: heap size 909 MB, throughput 0.791549
Reading from 20615: heap size 909 MB, throughput 0.782398
Reading from 20617: heap size 558 MB, throughput 0.986392
Reading from 20615: heap size 914 MB, throughput 0.762727
Reading from 20615: heap size 914 MB, throughput 0.795494
Reading from 20615: heap size 919 MB, throughput 0.816734
Reading from 20615: heap size 919 MB, throughput 0.827434
Reading from 20615: heap size 922 MB, throughput 0.800278
Reading from 20615: heap size 923 MB, throughput 0.882564
Reading from 20615: heap size 927 MB, throughput 0.866373
Reading from 20617: heap size 561 MB, throughput 0.995366
Reading from 20615: heap size 928 MB, throughput 0.894513
Reading from 20615: heap size 929 MB, throughput 0.763998
Reading from 20615: heap size 931 MB, throughput 0.700322
Reading from 20617: heap size 580 MB, throughput 0.994215
Reading from 20615: heap size 914 MB, throughput 0.0688443
Reading from 20615: heap size 1040 MB, throughput 0.462956
Reading from 20615: heap size 1037 MB, throughput 0.730469
Reading from 20615: heap size 1040 MB, throughput 0.693407
Reading from 20615: heap size 1039 MB, throughput 0.724228
Reading from 20615: heap size 1042 MB, throughput 0.683588
Reading from 20615: heap size 1047 MB, throughput 0.718469
Reading from 20617: heap size 584 MB, throughput 0.986722
Reading from 20615: heap size 1049 MB, throughput 0.702364
Reading from 20615: heap size 1060 MB, throughput 0.712974
Equal recommendation: 3132 MB each
Reading from 20615: heap size 1061 MB, throughput 0.653149
Reading from 20615: heap size 1075 MB, throughput 0.730103
Reading from 20617: heap size 605 MB, throughput 0.986842
Reading from 20615: heap size 1076 MB, throughput 0.967688
Reading from 20617: heap size 605 MB, throughput 0.993405
Reading from 20615: heap size 1092 MB, throughput 0.977108
Reading from 20617: heap size 621 MB, throughput 0.988223
Reading from 20617: heap size 625 MB, throughput 0.990522
Reading from 20615: heap size 1095 MB, throughput 0.968067
Reading from 20617: heap size 647 MB, throughput 0.995089
Equal recommendation: 3132 MB each
Reading from 20615: heap size 1102 MB, throughput 0.971916
Reading from 20617: heap size 648 MB, throughput 0.987726
Reading from 20615: heap size 1105 MB, throughput 0.969783
Reading from 20617: heap size 669 MB, throughput 0.990318
Reading from 20617: heap size 670 MB, throughput 0.992167
Reading from 20615: heap size 1102 MB, throughput 0.973153
Reading from 20617: heap size 688 MB, throughput 0.990821
Reading from 20615: heap size 1107 MB, throughput 0.964695
Reading from 20617: heap size 691 MB, throughput 0.986751
Equal recommendation: 3132 MB each
Reading from 20615: heap size 1103 MB, throughput 0.969196
Reading from 20617: heap size 715 MB, throughput 0.980619
Reading from 20617: heap size 715 MB, throughput 0.992425
Reading from 20615: heap size 1106 MB, throughput 0.974928
Reading from 20617: heap size 742 MB, throughput 0.995567
Reading from 20615: heap size 1109 MB, throughput 0.970691
Reading from 20617: heap size 745 MB, throughput 0.990036
Equal recommendation: 3132 MB each
Reading from 20615: heap size 1110 MB, throughput 0.974519
Reading from 20617: heap size 770 MB, throughput 0.990229
Reading from 20617: heap size 770 MB, throughput 0.98844
Reading from 20615: heap size 1114 MB, throughput 0.972897
Reading from 20617: heap size 800 MB, throughput 0.990555
Reading from 20615: heap size 1117 MB, throughput 0.971757
Reading from 20617: heap size 800 MB, throughput 0.989182
Equal recommendation: 3132 MB each
Reading from 20615: heap size 1120 MB, throughput 0.969737
Reading from 20617: heap size 830 MB, throughput 0.99361
Reading from 20615: heap size 1124 MB, throughput 0.967905
Reading from 20617: heap size 830 MB, throughput 0.991044
Reading from 20615: heap size 1127 MB, throughput 0.970519
Reading from 20617: heap size 855 MB, throughput 0.992314
Reading from 20615: heap size 1132 MB, throughput 0.965904
Reading from 20617: heap size 857 MB, throughput 0.992265
Equal recommendation: 3132 MB each
Reading from 20617: heap size 881 MB, throughput 0.992125
Reading from 20615: heap size 1137 MB, throughput 0.956821
Reading from 20617: heap size 883 MB, throughput 0.992056
Reading from 20615: heap size 1140 MB, throughput 0.968342
Reading from 20617: heap size 909 MB, throughput 0.99266
Reading from 20615: heap size 1144 MB, throughput 0.970594
Equal recommendation: 3132 MB each
Reading from 20617: heap size 910 MB, throughput 0.991627
Reading from 20615: heap size 1146 MB, throughput 0.967676
Reading from 20617: heap size 937 MB, throughput 0.991262
Reading from 20615: heap size 1150 MB, throughput 0.966304
Reading from 20617: heap size 937 MB, throughput 0.990825
Reading from 20615: heap size 1151 MB, throughput 0.97012
Equal recommendation: 3132 MB each
Reading from 20617: heap size 969 MB, throughput 0.993174
Reading from 20615: heap size 1155 MB, throughput 0.970128
Reading from 20617: heap size 970 MB, throughput 0.999999
Reading from 20617: heap size 970 MB, throughput 1
Reading from 20617: heap size 970 MB, throughput 0.00467177
Reading from 20615: heap size 1156 MB, throughput 0.971202
Reading from 20617: heap size 999 MB, throughput 0.991697
Reading from 20615: heap size 1159 MB, throughput 0.967069
Reading from 20617: heap size 1001 MB, throughput 0.99216
Equal recommendation: 3132 MB each
Reading from 20617: heap size 1034 MB, throughput 0.984337
Reading from 20615: heap size 1159 MB, throughput 0.602507
Reading from 20617: heap size 1034 MB, throughput 0.991645
Reading from 20615: heap size 1261 MB, throughput 0.96299
Equal recommendation: 3132 MB each
Reading from 20617: heap size 1071 MB, throughput 0.993039
Reading from 20615: heap size 1262 MB, throughput 0.988589
Reading from 20617: heap size 1072 MB, throughput 0.991777
Reading from 20615: heap size 1270 MB, throughput 0.990647
Reading from 20617: heap size 1108 MB, throughput 0.994437
Reading from 20615: heap size 1276 MB, throughput 0.985193
Equal recommendation: 3132 MB each
Reading from 20617: heap size 1109 MB, throughput 0.988059
Reading from 20615: heap size 1279 MB, throughput 0.982581
Reading from 20617: heap size 1139 MB, throughput 0.992873
Reading from 20615: heap size 1175 MB, throughput 0.980658
Reading from 20617: heap size 1143 MB, throughput 0.990675
Reading from 20615: heap size 1270 MB, throughput 0.981066
Equal recommendation: 3132 MB each
Reading from 20615: heap size 1192 MB, throughput 0.977052
Reading from 20617: heap size 1182 MB, throughput 0.988339
Reading from 20615: heap size 1260 MB, throughput 0.976117
Reading from 20617: heap size 1183 MB, throughput 0.990699
Equal recommendation: 3132 MB each
Reading from 20615: heap size 1209 MB, throughput 0.974796
Reading from 20617: heap size 1228 MB, throughput 0.989242
Reading from 20615: heap size 1262 MB, throughput 0.97564
Reading from 20617: heap size 1231 MB, throughput 0.990374
Reading from 20615: heap size 1264 MB, throughput 0.97092
Reading from 20617: heap size 1278 MB, throughput 0.993116
Equal recommendation: 3132 MB each
Reading from 20615: heap size 1266 MB, throughput 0.974126
Reading from 20617: heap size 1280 MB, throughput 0.986757
Reading from 20615: heap size 1269 MB, throughput 0.973624
Reading from 20617: heap size 1322 MB, throughput 0.992619
Reading from 20615: heap size 1271 MB, throughput 0.968072
Equal recommendation: 3132 MB each
Reading from 20617: heap size 1327 MB, throughput 0.987719
Reading from 20615: heap size 1277 MB, throughput 0.967849
Reading from 20615: heap size 1283 MB, throughput 0.967821
Reading from 20617: heap size 1371 MB, throughput 0.990892
Reading from 20615: heap size 1289 MB, throughput 0.969021
Equal recommendation: 3132 MB each
Reading from 20617: heap size 1374 MB, throughput 0.990977
Reading from 20615: heap size 1295 MB, throughput 0.960868
Reading from 20617: heap size 1423 MB, throughput 0.989734
Reading from 20615: heap size 1298 MB, throughput 0.968836
Reading from 20617: heap size 1424 MB, throughput 0.991914
Equal recommendation: 3132 MB each
Reading from 20615: heap size 1304 MB, throughput 0.964758
Reading from 20617: heap size 1473 MB, throughput 0.965701
Reading from 20617: heap size 1494 MB, throughput 0.991365
Equal recommendation: 3132 MB each
Reading from 20615: heap size 1305 MB, throughput 0.989565
Reading from 20617: heap size 1549 MB, throughput 0.99136
Reading from 20617: heap size 1547 MB, throughput 0.991238
Equal recommendation: 3132 MB each
Reading from 20617: heap size 1607 MB, throughput 0.99002
Reading from 20617: heap size 1610 MB, throughput 0.988201
Equal recommendation: 3132 MB each
Reading from 20617: heap size 1679 MB, throughput 0.990354
Reading from 20615: heap size 1304 MB, throughput 0.992405
Reading from 20617: heap size 1682 MB, throughput 0.989364
Reading from 20615: heap size 1311 MB, throughput 0.775607
Equal recommendation: 3132 MB each
Reading from 20615: heap size 1401 MB, throughput 0.992712
Reading from 20617: heap size 1761 MB, throughput 0.99163
Reading from 20615: heap size 1455 MB, throughput 0.977179
Reading from 20615: heap size 1458 MB, throughput 0.90786
Reading from 20615: heap size 1314 MB, throughput 0.826206
Reading from 20615: heap size 1442 MB, throughput 0.790958
Reading from 20615: heap size 1449 MB, throughput 0.848519
Reading from 20615: heap size 1436 MB, throughput 0.844271
Reading from 20615: heap size 1445 MB, throughput 0.845192
Reading from 20615: heap size 1439 MB, throughput 0.851735
Reading from 20615: heap size 1446 MB, throughput 0.942327
Reading from 20615: heap size 1452 MB, throughput 0.993101
Reading from 20617: heap size 1761 MB, throughput 0.990889
Equal recommendation: 3132 MB each
Reading from 20615: heap size 1458 MB, throughput 0.991267
Reading from 20617: heap size 1840 MB, throughput 0.992068
Reading from 20615: heap size 1472 MB, throughput 0.990555
Client 20617 died
Clients: 1
Reading from 20615: heap size 1475 MB, throughput 0.991848
Recommendation: one client; give it all the memory
Reading from 20615: heap size 1468 MB, throughput 0.990688
Reading from 20615: heap size 1475 MB, throughput 0.989339
Recommendation: one client; give it all the memory
Reading from 20615: heap size 1457 MB, throughput 0.989478
Reading from 20615: heap size 1367 MB, throughput 0.987984
Reading from 20615: heap size 1447 MB, throughput 0.987803
Recommendation: one client; give it all the memory
Reading from 20615: heap size 1456 MB, throughput 0.987098
Reading from 20615: heap size 1455 MB, throughput 0.985078
Recommendation: one client; give it all the memory
Reading from 20615: heap size 1456 MB, throughput 0.985067
Reading from 20615: heap size 1459 MB, throughput 0.983982
Recommendation: one client; give it all the memory
Reading from 20615: heap size 1463 MB, throughput 0.982417
Reading from 20615: heap size 1466 MB, throughput 0.982476
Reading from 20615: heap size 1473 MB, throughput 0.98129
Recommendation: one client; give it all the memory
Reading from 20615: heap size 1480 MB, throughput 0.981766
Reading from 20615: heap size 1487 MB, throughput 0.980552
Recommendation: one client; give it all the memory
Reading from 20615: heap size 1493 MB, throughput 0.981038
Reading from 20615: heap size 1497 MB, throughput 0.9797
Recommendation: one client; give it all the memory
Reading from 20615: heap size 1502 MB, throughput 0.980526
Reading from 20615: heap size 1504 MB, throughput 0.980232
Recommendation: one client; give it all the memory
Reading from 20615: heap size 1510 MB, throughput 0.981403
Reading from 20615: heap size 1510 MB, throughput 0.979126
Reading from 20615: heap size 1514 MB, throughput 0.979796
Recommendation: one client; give it all the memory
Reading from 20615: heap size 1515 MB, throughput 0.978724
Reading from 20615: heap size 1517 MB, throughput 0.979371
Recommendation: one client; give it all the memory
Reading from 20615: heap size 1518 MB, throughput 0.98066
Reading from 20615: heap size 1519 MB, throughput 0.980854
Recommendation: one client; give it all the memory
Reading from 20615: heap size 1521 MB, throughput 0.981093
Reading from 20615: heap size 1522 MB, throughput 0.979772
Recommendation: one client; give it all the memory
Reading from 20615: heap size 1523 MB, throughput 0.806259
Reading from 20615: heap size 1549 MB, throughput 0.995909
Recommendation: one client; give it all the memory
Reading from 20615: heap size 1551 MB, throughput 0.993906
Reading from 20615: heap size 1563 MB, throughput 0.993341
Recommendation: one client; give it all the memory
Reading from 20615: heap size 1570 MB, throughput 0.991933
Reading from 20615: heap size 1573 MB, throughput 0.990861
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 20615: heap size 1428 MB, throughput 0.995283
Recommendation: one client; give it all the memory
Reading from 20615: heap size 1556 MB, throughput 0.995494
Recommendation: one client; give it all the memory
Reading from 20615: heap size 1565 MB, throughput 0.987678
Reading from 20615: heap size 1554 MB, throughput 0.872229
Reading from 20615: heap size 1570 MB, throughput 0.739032
Reading from 20615: heap size 1598 MB, throughput 0.772989
Reading from 20615: heap size 1620 MB, throughput 0.750768
Reading from 20615: heap size 1652 MB, throughput 0.783776
Reading from 20615: heap size 1664 MB, throughput 0.761782
Client 20615 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
