economemd
    total memory: 1000 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: no
    equal recommendations: no

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/gcFactor_2014_09_24_12_05_40/daemon_log_33.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Reading from 7575: heap size 9 MB, throughput 0.992984
Clients: 1
Reading from 7575: heap size 9 MB, throughput 0.978602
Reading from 7575: heap size 9 MB, throughput 0.968977
Reading from 7575: heap size 9 MB, throughput 0.972698
Reading from 7575: heap size 11 MB, throughput 0.986489
Reading from 7575: heap size 11 MB, throughput 0.96732
Reading from 7575: heap size 17 MB, throughput 0.944036
Reading from 7575: heap size 17 MB, throughput 0.605181
Reading from 7575: heap size 30 MB, throughput 0.944143
Reading from 7575: heap size 31 MB, throughput 0.935728
Reading from 7575: heap size 35 MB, throughput 0.325555
Reading from 7575: heap size 47 MB, throughput 0.909824
Reading from 7575: heap size 47 MB, throughput 0.902764
Reading from 7575: heap size 51 MB, throughput 0.279321
Reading from 7575: heap size 72 MB, throughput 0.830874
Reading from 7575: heap size 72 MB, throughput 0.31611
Reading from 7575: heap size 100 MB, throughput 0.826575
Reading from 7575: heap size 100 MB, throughput 0.833872
Reading from 7575: heap size 101 MB, throughput 0.832186
Reading from 7575: heap size 103 MB, throughput 0.788699
Reading from 7575: heap size 104 MB, throughput 0.210724
Reading from 7575: heap size 136 MB, throughput 0.823017
Reading from 7575: heap size 139 MB, throughput 0.80437
Reading from 7575: heap size 141 MB, throughput 0.80749
Reading from 7575: heap size 143 MB, throughput 0.805252
Reading from 7575: heap size 148 MB, throughput 0.721745
Reading from 7575: heap size 151 MB, throughput 0.736995
Reading from 7575: heap size 156 MB, throughput 0.139518
Reading from 7575: heap size 198 MB, throughput 0.683896
Reading from 7575: heap size 200 MB, throughput 0.775079
Reading from 7575: heap size 202 MB, throughput 0.745376
Reading from 7575: heap size 206 MB, throughput 0.85965
Reading from 7575: heap size 207 MB, throughput 0.921024
Reading from 7575: heap size 215 MB, throughput 0.682554
Reading from 7575: heap size 219 MB, throughput 0.721374
Reading from 7575: heap size 227 MB, throughput 0.446079
Reading from 7575: heap size 230 MB, throughput 0.395392
Reading from 7575: heap size 236 MB, throughput 0.720694
Reading from 7575: heap size 236 MB, throughput 0.533857
Reading from 7575: heap size 238 MB, throughput 0.0957169
Reading from 7575: heap size 271 MB, throughput 0.525504
Reading from 7575: heap size 275 MB, throughput 0.8691
Reading from 7575: heap size 277 MB, throughput 0.90574
Reading from 7575: heap size 277 MB, throughput 0.89895
Reading from 7575: heap size 275 MB, throughput 0.869564
Reading from 7575: heap size 277 MB, throughput 0.90764
Reading from 7575: heap size 270 MB, throughput 0.807099
Reading from 7575: heap size 224 MB, throughput 0.75966
Reading from 7575: heap size 269 MB, throughput 0.849068
Reading from 7575: heap size 271 MB, throughput 0.856139
Reading from 7575: heap size 268 MB, throughput 0.875981
Reading from 7575: heap size 270 MB, throughput 0.855624
Reading from 7575: heap size 266 MB, throughput 0.869188
Reading from 7575: heap size 268 MB, throughput 0.975541
Reading from 7575: heap size 262 MB, throughput 0.843864
Reading from 7575: heap size 266 MB, throughput 0.842745
Reading from 7575: heap size 261 MB, throughput 0.846971
Reading from 7575: heap size 263 MB, throughput 0.857972
Reading from 7575: heap size 260 MB, throughput 0.867814
Reading from 7575: heap size 262 MB, throughput 0.87049
Reading from 7575: heap size 261 MB, throughput 0.916232
Reading from 7575: heap size 262 MB, throughput 0.870188
Reading from 7575: heap size 262 MB, throughput 0.844766
Reading from 7575: heap size 263 MB, throughput 0.741338
Reading from 7575: heap size 268 MB, throughput 0.73732
Reading from 7575: heap size 270 MB, throughput 0.702855
Reading from 7575: heap size 276 MB, throughput 0.719057
Reading from 7575: heap size 278 MB, throughput 0.687108
Reading from 7575: heap size 284 MB, throughput 0.0754598
Reading from 7575: heap size 327 MB, throughput 0.758743
Reading from 7575: heap size 331 MB, throughput 0.962394
Reading from 7575: heap size 332 MB, throughput 0.983011
Reading from 7575: heap size 335 MB, throughput 0.984186
Reading from 7575: heap size 336 MB, throughput 0.982639
Reading from 7575: heap size 333 MB, throughput 0.98257
Reading from 7575: heap size 336 MB, throughput 0.978348
Reading from 7575: heap size 331 MB, throughput 0.982246
Reading from 7575: heap size 334 MB, throughput 0.980162
Reading from 7575: heap size 333 MB, throughput 0.983393
Reading from 7575: heap size 334 MB, throughput 0.978298
Reading from 7575: heap size 334 MB, throughput 0.982086
Reading from 7575: heap size 335 MB, throughput 0.978499
Reading from 7575: heap size 336 MB, throughput 0.982683
Reading from 7575: heap size 337 MB, throughput 0.983205
Reading from 7575: heap size 338 MB, throughput 0.981954
Reading from 7575: heap size 339 MB, throughput 0.983793
Reading from 7575: heap size 340 MB, throughput 0.984217
Reading from 7575: heap size 341 MB, throughput 0.979656
Reading from 7575: heap size 343 MB, throughput 0.987549
Reading from 7575: heap size 344 MB, throughput 0.975727
Reading from 7575: heap size 342 MB, throughput 0.877009
Reading from 7575: heap size 344 MB, throughput 0.766287
Reading from 7575: heap size 353 MB, throughput 0.803414
Reading from 7575: heap size 355 MB, throughput 0.787507
Reading from 7575: heap size 362 MB, throughput 0.816984
Reading from 7575: heap size 363 MB, throughput 0.897237
Reading from 7575: heap size 369 MB, throughput 0.990297
Reading from 7575: heap size 369 MB, throughput 0.991032
Reading from 7575: heap size 370 MB, throughput 0.989599
Reading from 7575: heap size 372 MB, throughput 0.987148
Reading from 7575: heap size 371 MB, throughput 0.833128
Reading from 7575: heap size 400 MB, throughput 0.997104
Reading from 7575: heap size 398 MB, throughput 0.995539
Reading from 7575: heap size 401 MB, throughput 0.994195
Reading from 7575: heap size 404 MB, throughput 0.993282
Reading from 7575: heap size 405 MB, throughput 0.991756
Reading from 7575: heap size 402 MB, throughput 0.992596
Reading from 7575: heap size 370 MB, throughput 0.990258
Reading from 7575: heap size 397 MB, throughput 0.991781
Reading from 7575: heap size 400 MB, throughput 0.985164
Reading from 7575: heap size 398 MB, throughput 0.972042
Reading from 7575: heap size 400 MB, throughput 0.860267
Reading from 7575: heap size 396 MB, throughput 0.840973
Reading from 7575: heap size 402 MB, throughput 0.82223
Reading from 7575: heap size 410 MB, throughput 0.894123
Reading from 7575: heap size 413 MB, throughput 0.991253
Reading from 7575: heap size 411 MB, throughput 0.992964
Reading from 7575: heap size 415 MB, throughput 0.991479
Reading from 7575: heap size 412 MB, throughput 0.991373
Reading from 7575: heap size 415 MB, throughput 0.991854
Reading from 7575: heap size 412 MB, throughput 0.991532
Reading from 7575: heap size 415 MB, throughput 0.988651
Reading from 7575: heap size 413 MB, throughput 0.990715
Reading from 7575: heap size 415 MB, throughput 0.989856
Reading from 7575: heap size 416 MB, throughput 0.989382
Reading from 7575: heap size 416 MB, throughput 0.988794
Reading from 7575: heap size 417 MB, throughput 0.993264
Reading from 7575: heap size 418 MB, throughput 0.984057
Reading from 7575: heap size 419 MB, throughput 0.904124
Reading from 7575: heap size 419 MB, throughput 0.856207
Reading from 7575: heap size 424 MB, throughput 0.877421
Reading from 7575: heap size 427 MB, throughput 0.98741
Reading from 7575: heap size 434 MB, throughput 0.993761
Reading from 7575: heap size 436 MB, throughput 0.993249
Reading from 7575: heap size 436 MB, throughput 0.992843
Reading from 7575: heap size 438 MB, throughput 0.99296
Reading from 7575: heap size 437 MB, throughput 0.992273
Reading from 7575: heap size 439 MB, throughput 0.990631
Reading from 7575: heap size 437 MB, throughput 0.991047
Reading from 7575: heap size 439 MB, throughput 0.991196
Reading from 7575: heap size 441 MB, throughput 0.990633
Reading from 7575: heap size 441 MB, throughput 0.993304
Reading from 7575: heap size 444 MB, throughput 0.941763
Reading from 7575: heap size 444 MB, throughput 0.871928
Reading from 7575: heap size 448 MB, throughput 0.896839
Reading from 7575: heap size 450 MB, throughput 0.991147
Reading from 7575: heap size 456 MB, throughput 0.994239
Reading from 7575: heap size 458 MB, throughput 0.994653
Reading from 7575: heap size 460 MB, throughput 0.993261
Reading from 7575: heap size 461 MB, throughput 0.993627
Reading from 7575: heap size 459 MB, throughput 0.992722
Reading from 7575: heap size 461 MB, throughput 0.992396
Reading from 7575: heap size 461 MB, throughput 0.991516
Reading from 7575: heap size 462 MB, throughput 0.991062
Reading from 7575: heap size 464 MB, throughput 0.992254
Reading from 7575: heap size 465 MB, throughput 0.916551
Reading from 7575: heap size 464 MB, throughput 0.903211
Reading from 7575: heap size 467 MB, throughput 0.987013
Reading from 7575: heap size 474 MB, throughput 0.995939
Reading from 7575: heap size 475 MB, throughput 0.99456
Reading from 7575: heap size 475 MB, throughput 0.993672
Reading from 7575: heap size 477 MB, throughput 0.993937
Reading from 7575: heap size 474 MB, throughput 0.993541
Reading from 7575: heap size 477 MB, throughput 0.991896
Reading from 7575: heap size 478 MB, throughput 0.991779
Reading from 7575: heap size 478 MB, throughput 0.993632
Reading from 7575: heap size 478 MB, throughput 0.975758
Reading from 7575: heap size 480 MB, throughput 0.894149
Reading from 7575: heap size 483 MB, throughput 0.94087
Reading from 7575: heap size 485 MB, throughput 0.995708
Reading from 7575: heap size 488 MB, throughput 0.994575
Reading from 7575: heap size 490 MB, throughput 0.994494
Reading from 7575: heap size 490 MB, throughput 0.993712
Reading from 7575: heap size 492 MB, throughput 0.993638
Reading from 7575: heap size 492 MB, throughput 0.994269
Reading from 7575: heap size 493 MB, throughput 0.991884
Reading from 7575: heap size 495 MB, throughput 0.993896
Reading from 7575: heap size 496 MB, throughput 0.930067
Reading from 7575: heap size 494 MB, throughput 0.916928
Reading from 7575: heap size 497 MB, throughput 0.994597
Reading from 7575: heap size 505 MB, throughput 0.995868
Reading from 7575: heap size 505 MB, throughput 0.994872
Reading from 7575: heap size 506 MB, throughput 0.994265
Reading from 7575: heap size 508 MB, throughput 0.993918
Reading from 7575: heap size 506 MB, throughput 0.99323
Reading from 7575: heap size 508 MB, throughput 0.991987
Reading from 7575: heap size 510 MB, throughput 0.992907
Reading from 7575: heap size 511 MB, throughput 0.927792
Reading from 7575: heap size 511 MB, throughput 0.922165
Reading from 7575: heap size 513 MB, throughput 0.994322
Reading from 7575: heap size 519 MB, throughput 0.995409
Reading from 7575: heap size 521 MB, throughput 0.995228
Reading from 7575: heap size 521 MB, throughput 0.994668
Reading from 7575: heap size 523 MB, throughput 0.993559
Reading from 7575: heap size 522 MB, throughput 0.993515
Reading from 7575: heap size 523 MB, throughput 0.996274
Reading from 7575: heap size 526 MB, throughput 0.955731
Reading from 7575: heap size 527 MB, throughput 0.908728
Reading from 7575: heap size 531 MB, throughput 0.994665
Reading from 7575: heap size 533 MB, throughput 0.99574
Reading from 7575: heap size 536 MB, throughput 0.995559
Reading from 7575: heap size 537 MB, throughput 0.994691
Reading from 7575: heap size 535 MB, throughput 0.99445
Reading from 7575: heap size 537 MB, throughput 0.993721
Reading from 7575: heap size 539 MB, throughput 0.995001
Reading from 7575: heap size 540 MB, throughput 0.939153
Reading from 7575: heap size 539 MB, throughput 0.951269
Reading from 7575: heap size 541 MB, throughput 0.996579
Reading from 7575: heap size 545 MB, throughput 0.996268
Reading from 7575: heap size 546 MB, throughput 0.995544
Reading from 7575: heap size 544 MB, throughput 0.994156
Reading from 7575: heap size 547 MB, throughput 0.99422
Reading from 7575: heap size 549 MB, throughput 0.99583
Reading from 7575: heap size 549 MB, throughput 0.979679
Reading from 7575: heap size 548 MB, throughput 0.931281
Client 7575 died
Clients: 0
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
