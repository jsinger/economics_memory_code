# Main file for all utility models

import numpy as np
import scipy.optimize
import staticutility as su

class UtilityModel:
    def testingParams(self):
        raise NotImplementedError

    def loadParams(self, filename):
        with open(filename) as f:
            return self.Params(float(f.readline().strip()), float(f.readline().strip()))

    def curveFit(self, xdata, ydata):
        raise NotImplementedError

    def calculateBest(self, totalMem, params, minheaps):
        raise NotImplementedError

    def calculateBestNumeric(self, totalMem, params, minheaps):
        if len(params) < 2 or not su.allSameLength(params, minheaps):
            raise su.UtilityException("Invalid parameters to calculateBestNumeric")

        def U(hs):
            return -self.combinedUtility(params, list(hs) + [totalMem - sum(hs)])

        s = sum(minheaps)
        bounds = map(lambda m: (m, totalMem - (s - m)), minheaps)[:-1]

        res = scipy.optimize.minimize(U, np.array([0]*(len(params) - 1)), method="L-BFGS-B", bounds=bounds, tol=1e-15)
        hs = list(res["x"]) + [totalMem - sum(res["x"])]

        return (hs, self.combinedUtility(params, hs))

    def combinedUtility(self, params, heaps):
        return self.combineRawUtilities(*map(lambda (p, h): p.calculate(h), zip(params, heaps)))

    def combineRawUtilities(self, *u):
        raise NotImplementedError

    def propagateRawErrors(self, itersPerSecs, U, stddevItersPerSecs):
        raise NotImplementedError

    def plotThroughput(self, heap, meanThroughputs, meanThroughputStddevs, medianThroughputs, bid, runs, outDir, cleaned=False):
        outDir.plotThroughput(heap, meanThroughputs, meanThroughputStddevs, medianThroughputs, self.curveFit(heap, meanThroughputs), self.curveFit(heap, medianThroughputs), bid, runs, self.modelName(), cleaned=cleaned)


import rootutilitymodel
import logutilitymodel

models = { rootutilitymodel.name(): rootutilitymodel.RootUtilityModel(), logutilitymodel.name(): logutilitymodel.LogUtilityModel() }

defaultModel = rootutilitymodel.name()

def get(s):
    return models[s]
