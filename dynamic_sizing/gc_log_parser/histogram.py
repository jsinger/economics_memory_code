#!/usr/bin/env python

"""Plot a histogram"""

import matplotlib.pyplot as plt
import sys

def plot():
    """Foo"""
    title = sys.argv[1]
    num_bins = 10
    throughputs = []
    for line in sys.stdin:
        throughputs.append(float(line.strip()))

    plt.hist(throughputs, bins=num_bins, color="blue")


    plt.title("Throughput Frequencies: %s (%d values)"
              % (title, len(throughputs)))
    plt.xlabel("Throughput bins")
    plt.ylabel("Frequency")
    plt.savefig(title + ".svg")

if len(sys.argv) < 1:
    print "Bad arg"
    sys.exit(0)

plot()
