economemd
    total memory: 2974 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub2_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
CommandThread: starting
ReadingThread: starting
TimerThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 2789: heap size 9 MB, throughput 0.992431
Clients: 1
Client 2789 has a minimum heap size of 276 MB
Reading from 2788: heap size 9 MB, throughput 0.992767
Clients: 2
Client 2788 has a minimum heap size of 1211 MB
Reading from 2788: heap size 9 MB, throughput 0.989326
Reading from 2789: heap size 9 MB, throughput 0.989822
Reading from 2788: heap size 9 MB, throughput 0.979769
Reading from 2789: heap size 9 MB, throughput 0.983757
Reading from 2788: heap size 9 MB, throughput 0.968823
Reading from 2789: heap size 9 MB, throughput 0.973758
Reading from 2788: heap size 11 MB, throughput 0.979439
Reading from 2789: heap size 11 MB, throughput 0.984725
Reading from 2788: heap size 11 MB, throughput 0.968574
Reading from 2789: heap size 11 MB, throughput 0.976941
Reading from 2788: heap size 17 MB, throughput 0.957977
Reading from 2789: heap size 17 MB, throughput 0.922683
Reading from 2788: heap size 17 MB, throughput 0.882261
Reading from 2789: heap size 17 MB, throughput 0.888845
Reading from 2788: heap size 30 MB, throughput 0.801246
Reading from 2789: heap size 30 MB, throughput 0.824075
Reading from 2789: heap size 31 MB, throughput 0.488162
Reading from 2788: heap size 31 MB, throughput 0.535767
Reading from 2788: heap size 35 MB, throughput 0.76396
Reading from 2789: heap size 35 MB, throughput 0.72609
Reading from 2788: heap size 48 MB, throughput 0.645518
Reading from 2789: heap size 48 MB, throughput 0.676959
Reading from 2788: heap size 50 MB, throughput 0.546848
Reading from 2789: heap size 51 MB, throughput 0.512819
Reading from 2788: heap size 65 MB, throughput 0.486298
Reading from 2789: heap size 67 MB, throughput 0.764156
Reading from 2788: heap size 71 MB, throughput 0.42654
Reading from 2789: heap size 71 MB, throughput 0.499353
Reading from 2789: heap size 72 MB, throughput 0.22642
Reading from 2788: heap size 72 MB, throughput 0.470096
Reading from 2788: heap size 102 MB, throughput 0.520484
Reading from 2789: heap size 75 MB, throughput 0.465138
Reading from 2789: heap size 109 MB, throughput 0.572829
Reading from 2789: heap size 112 MB, throughput 0.649604
Reading from 2788: heap size 102 MB, throughput 0.403163
Reading from 2789: heap size 112 MB, throughput 0.393651
Reading from 2788: heap size 134 MB, throughput 0.677345
Reading from 2789: heap size 115 MB, throughput 0.16572
Reading from 2788: heap size 134 MB, throughput 0.542843
Reading from 2789: heap size 119 MB, throughput 0.753408
Reading from 2788: heap size 136 MB, throughput 0.315347
Reading from 2788: heap size 140 MB, throughput 0.307155
Reading from 2789: heap size 123 MB, throughput 0.475039
Reading from 2789: heap size 156 MB, throughput 0.684546
Reading from 2789: heap size 162 MB, throughput 0.693211
Reading from 2789: heap size 163 MB, throughput 0.674003
Reading from 2789: heap size 166 MB, throughput 0.664453
Reading from 2788: heap size 144 MB, throughput 0.451107
Reading from 2789: heap size 171 MB, throughput 0.629848
Reading from 2788: heap size 183 MB, throughput 0.318145
Reading from 2788: heap size 187 MB, throughput 0.0738845
Reading from 2789: heap size 179 MB, throughput 0.438067
Reading from 2789: heap size 216 MB, throughput 0.7343
Reading from 2788: heap size 191 MB, throughput 0.367554
Reading from 2788: heap size 239 MB, throughput 0.666616
Reading from 2788: heap size 241 MB, throughput 0.71306
Reading from 2788: heap size 244 MB, throughput 0.707443
Reading from 2789: heap size 224 MB, throughput 0.89676
Reading from 2788: heap size 248 MB, throughput 0.530986
Reading from 2789: heap size 226 MB, throughput 0.860549
Reading from 2788: heap size 254 MB, throughput 0.580902
Reading from 2789: heap size 231 MB, throughput 0.79883
Reading from 2788: heap size 261 MB, throughput 0.630726
Reading from 2789: heap size 237 MB, throughput 0.75678
Reading from 2789: heap size 242 MB, throughput 0.721558
Reading from 2789: heap size 246 MB, throughput 0.688984
Reading from 2789: heap size 252 MB, throughput 0.636929
Reading from 2788: heap size 265 MB, throughput 0.464467
Reading from 2789: heap size 255 MB, throughput 0.735342
Reading from 2788: heap size 321 MB, throughput 0.557621
Reading from 2788: heap size 330 MB, throughput 0.60967
Reading from 2788: heap size 332 MB, throughput 0.58699
Reading from 2788: heap size 337 MB, throughput 0.582166
Reading from 2789: heap size 253 MB, throughput 0.608585
Reading from 2788: heap size 346 MB, throughput 0.571305
Reading from 2789: heap size 295 MB, throughput 0.565074
Reading from 2789: heap size 293 MB, throughput 0.641766
Reading from 2789: heap size 296 MB, throughput 0.642414
Reading from 2789: heap size 295 MB, throughput 0.761578
Reading from 2788: heap size 354 MB, throughput 0.387346
Reading from 2788: heap size 411 MB, throughput 0.555453
Reading from 2789: heap size 297 MB, throughput 0.889501
Reading from 2789: heap size 301 MB, throughput 0.852585
Reading from 2788: heap size 412 MB, throughput 0.626248
Reading from 2789: heap size 302 MB, throughput 0.803604
Reading from 2789: heap size 305 MB, throughput 0.777999
Reading from 2788: heap size 415 MB, throughput 0.637513
Reading from 2789: heap size 307 MB, throughput 0.733204
Reading from 2788: heap size 417 MB, throughput 0.592843
Reading from 2788: heap size 422 MB, throughput 0.575334
Reading from 2789: heap size 311 MB, throughput 0.837939
Reading from 2789: heap size 312 MB, throughput 0.758057
Reading from 2789: heap size 317 MB, throughput 0.697031
Reading from 2789: heap size 318 MB, throughput 0.672013
Reading from 2789: heap size 329 MB, throughput 0.647714
Reading from 2788: heap size 430 MB, throughput 0.351942
Reading from 2788: heap size 490 MB, throughput 0.542371
Reading from 2788: heap size 486 MB, throughput 0.58446
Reading from 2789: heap size 330 MB, throughput 0.577842
Reading from 2788: heap size 491 MB, throughput 0.54138
Numeric result:
Recommendation: 2 clients, utility 0.613774:
    h1: 1763 MB (U(h) = 0.631276*h^0.00662445)
    h2: 1211 MB (U(h) = 0.918759*h^0.001)
Recommendation: 2 clients, utility 0.613774:
    h1: 1763 MB (U(h) = 0.631276*h^0.00662445)
    h2: 1211 MB (U(h) = 0.918759*h^0.001)
Reading from 2788: heap size 497 MB, throughput 0.559399
Reading from 2789: heap size 378 MB, throughput 0.847039
Reading from 2788: heap size 498 MB, throughput 0.301151
Reading from 2788: heap size 565 MB, throughput 0.537464
Reading from 2788: heap size 568 MB, throughput 0.544005
Reading from 2788: heap size 569 MB, throughput 0.576004
Reading from 2789: heap size 381 MB, throughput 0.955619
Reading from 2788: heap size 570 MB, throughput 0.334688
Reading from 2788: heap size 645 MB, throughput 0.576923
Reading from 2788: heap size 652 MB, throughput 0.61122
Reading from 2789: heap size 389 MB, throughput 0.971135
Reading from 2788: heap size 653 MB, throughput 0.605512
Reading from 2788: heap size 657 MB, throughput 0.691125
Reading from 2789: heap size 391 MB, throughput 0.965143
Reading from 2788: heap size 659 MB, throughput 0.800545
Reading from 2789: heap size 394 MB, throughput 0.959902
Reading from 2788: heap size 674 MB, throughput 0.540092
Reading from 2788: heap size 762 MB, throughput 0.753948
Reading from 2788: heap size 779 MB, throughput 0.73096
Reading from 2789: heap size 397 MB, throughput 0.961995
Reading from 2788: heap size 783 MB, throughput 0.637987
Reading from 2788: heap size 790 MB, throughput 0.553837
Reading from 2788: heap size 798 MB, throughput 0.466282
Reading from 2788: heap size 802 MB, throughput 0.698187
Reading from 2788: heap size 797 MB, throughput 0.711441
Reading from 2788: heap size 804 MB, throughput 0.649134
Reading from 2789: heap size 397 MB, throughput 0.965736
Numeric result:
Recommendation: 2 clients, utility 0.517933:
    h1: 1763 MB (U(h) = 0.530718*h^0.0490637)
    h2: 1211 MB (U(h) = 0.671512*h^0.001)
Recommendation: 2 clients, utility 0.517933:
    h1: 1763 MB (U(h) = 0.530718*h^0.0490637)
    h2: 1211 MB (U(h) = 0.671512*h^0.001)
Reading from 2788: heap size 790 MB, throughput 0.392198
Reading from 2788: heap size 877 MB, throughput 0.666492
Reading from 2788: heap size 876 MB, throughput 0.727183
Reading from 2788: heap size 879 MB, throughput 0.754295
Reading from 2788: heap size 882 MB, throughput 0.833909
Reading from 2789: heap size 400 MB, throughput 0.970926
Reading from 2788: heap size 884 MB, throughput 0.556545
Reading from 2788: heap size 977 MB, throughput 0.793771
Reading from 2788: heap size 980 MB, throughput 0.819587
Reading from 2789: heap size 405 MB, throughput 0.974898
Reading from 2788: heap size 987 MB, throughput 0.844134
Reading from 2788: heap size 988 MB, throughput 0.851742
Reading from 2788: heap size 988 MB, throughput 0.868407
Reading from 2788: heap size 991 MB, throughput 0.877378
Reading from 2788: heap size 984 MB, throughput 0.89162
Reading from 2788: heap size 989 MB, throughput 0.888795
Reading from 2789: heap size 405 MB, throughput 0.9703
Reading from 2788: heap size 980 MB, throughput 0.893611
Reading from 2788: heap size 986 MB, throughput 0.940695
Reading from 2789: heap size 409 MB, throughput 0.941496
Reading from 2788: heap size 967 MB, throughput 0.946376
Reading from 2788: heap size 977 MB, throughput 0.902236
Reading from 2788: heap size 985 MB, throughput 0.857882
Reading from 2788: heap size 993 MB, throughput 0.822726
Reading from 2788: heap size 1004 MB, throughput 0.795206
Reading from 2789: heap size 453 MB, throughput 0.981491
Reading from 2788: heap size 1008 MB, throughput 0.766472
Reading from 2788: heap size 1110 MB, throughput 0.681022
Reading from 2788: heap size 1117 MB, throughput 0.794481
Numeric result:
Recommendation: 2 clients, utility 0.522248:
    h1: 1763 MB (U(h) = 0.48422*h^0.0711257)
    h2: 1211 MB (U(h) = 0.451248*h^0.0478492)
Recommendation: 2 clients, utility 0.522248:
    h1: 1763 MB (U(h) = 0.48422*h^0.0711257)
    h2: 1211 MB (U(h) = 0.451248*h^0.0478492)
Reading from 2788: heap size 1132 MB, throughput 0.869238
Reading from 2788: heap size 1135 MB, throughput 0.88923
Reading from 2788: heap size 1127 MB, throughput 0.861955
Reading from 2789: heap size 451 MB, throughput 0.98583
Reading from 2788: heap size 1135 MB, throughput 0.834264
Reading from 2788: heap size 1141 MB, throughput 0.779315
Reading from 2788: heap size 1142 MB, throughput 0.698452
Reading from 2789: heap size 455 MB, throughput 0.980992
Reading from 2788: heap size 1157 MB, throughput 0.690134
Reading from 2789: heap size 455 MB, throughput 0.969213
Reading from 2788: heap size 1159 MB, throughput 0.684261
Reading from 2789: heap size 457 MB, throughput 0.944364
Reading from 2789: heap size 463 MB, throughput 0.889789
Reading from 2788: heap size 1178 MB, throughput 0.680006
Reading from 2788: heap size 1179 MB, throughput 0.641599
Reading from 2788: heap size 1200 MB, throughput 0.683535
Reading from 2788: heap size 1201 MB, throughput 0.694981
Reading from 2789: heap size 464 MB, throughput 0.965313
Reading from 2788: heap size 1225 MB, throughput 0.832006
Reading from 2789: heap size 471 MB, throughput 0.978569
Reading from 2788: heap size 1069 MB, throughput 0.934805
Reading from 2789: heap size 474 MB, throughput 0.985193
Reading from 2788: heap size 1227 MB, throughput 0.942228
Reading from 2789: heap size 474 MB, throughput 0.986856
Numeric result:
Recommendation: 2 clients, utility 0.602221:
    h1: 1734.71 MB (U(h) = 0.430273*h^0.0988874)
    h2: 1239.29 MB (U(h) = 0.404736*h^0.0706463)
Recommendation: 2 clients, utility 0.602221:
    h1: 1734.71 MB (U(h) = 0.430273*h^0.0988874)
    h2: 1239.29 MB (U(h) = 0.404736*h^0.0706463)
Reading from 2788: heap size 1086 MB, throughput 0.94096
Reading from 2789: heap size 478 MB, throughput 0.986342
Reading from 2788: heap size 1222 MB, throughput 0.934238
Reading from 2789: heap size 476 MB, throughput 0.98686
Reading from 2788: heap size 1215 MB, throughput 0.944262
Reading from 2789: heap size 479 MB, throughput 0.98447
Reading from 2788: heap size 1204 MB, throughput 0.948031
Reading from 2789: heap size 481 MB, throughput 0.984358
Reading from 2788: heap size 1214 MB, throughput 0.951087
Reading from 2789: heap size 482 MB, throughput 0.983194
Numeric result:
Recommendation: 2 clients, utility 0.65302:
    h1: 1714.13 MB (U(h) = 0.40229*h^0.114553)
    h2: 1259.87 MB (U(h) = 0.379226*h^0.0841907)
Recommendation: 2 clients, utility 0.65302:
    h1: 1714.17 MB (U(h) = 0.40229*h^0.114553)
    h2: 1259.83 MB (U(h) = 0.379226*h^0.0841907)
Reading from 2788: heap size 1203 MB, throughput 0.954031
Reading from 2789: heap size 486 MB, throughput 0.985883
Reading from 2789: heap size 487 MB, throughput 0.969316
Reading from 2788: heap size 1211 MB, throughput 0.953172
Reading from 2789: heap size 489 MB, throughput 0.95939
Reading from 2789: heap size 489 MB, throughput 0.954949
Reading from 2788: heap size 1212 MB, throughput 0.959989
Reading from 2789: heap size 499 MB, throughput 0.985381
Reading from 2788: heap size 1215 MB, throughput 0.958792
Reading from 2789: heap size 500 MB, throughput 0.989356
Reading from 2788: heap size 1225 MB, throughput 0.96266
Numeric result:
Recommendation: 2 clients, utility 0.748695:
    h1: 1202.57 MB (U(h) = 0.382687*h^0.126109)
    h2: 1771.43 MB (U(h) = 0.199355*h^0.185759)
Recommendation: 2 clients, utility 0.748695:
    h1: 1202.59 MB (U(h) = 0.382687*h^0.126109)
    h2: 1771.41 MB (U(h) = 0.199355*h^0.185759)
Reading from 2789: heap size 504 MB, throughput 0.989527
Reading from 2788: heap size 1225 MB, throughput 0.96189
Reading from 2789: heap size 506 MB, throughput 0.988559
Reading from 2788: heap size 1233 MB, throughput 0.960563
Reading from 2789: heap size 504 MB, throughput 0.988902
Reading from 2788: heap size 1236 MB, throughput 0.959065
Reading from 2789: heap size 507 MB, throughput 0.987733
Reading from 2788: heap size 1243 MB, throughput 0.959191
Reading from 2789: heap size 508 MB, throughput 0.987361
Numeric result:
Recommendation: 2 clients, utility 0.83951:
    h1: 983.555 MB (U(h) = 0.369603*h^0.134095)
    h2: 1990.44 MB (U(h) = 0.114747*h^0.271368)
Recommendation: 2 clients, utility 0.83951:
    h1: 983.563 MB (U(h) = 0.369603*h^0.134095)
    h2: 1990.44 MB (U(h) = 0.114747*h^0.271368)
Reading from 2788: heap size 1248 MB, throughput 0.957906
Reading from 2789: heap size 509 MB, throughput 0.99051
Reading from 2789: heap size 512 MB, throughput 0.984779
Reading from 2789: heap size 513 MB, throughput 0.973248
Reading from 2788: heap size 1258 MB, throughput 0.959345
Reading from 2789: heap size 519 MB, throughput 0.982313
Reading from 2788: heap size 1261 MB, throughput 0.961131
Reading from 2789: heap size 520 MB, throughput 0.989705
Reading from 2788: heap size 1270 MB, throughput 0.959347
Reading from 2789: heap size 524 MB, throughput 0.990346
Numeric result:
Recommendation: 2 clients, utility 0.953861:
    h1: 834.242 MB (U(h) = 0.354699*h^0.143491)
    h2: 2139.76 MB (U(h) = 0.0609185*h^0.368037)
Recommendation: 2 clients, utility 0.953861:
    h1: 834.248 MB (U(h) = 0.354699*h^0.143491)
    h2: 2139.75 MB (U(h) = 0.0609185*h^0.368037)
Reading from 2788: heap size 1272 MB, throughput 0.957862
Reading from 2789: heap size 526 MB, throughput 0.990943
Reading from 2788: heap size 1281 MB, throughput 0.957016
Reading from 2789: heap size 524 MB, throughput 0.989968
Reading from 2789: heap size 527 MB, throughput 0.942715
Reading from 2788: heap size 1282 MB, throughput 0.909755
Numeric result:
Recommendation: 2 clients, utility 1.03102:
    h1: 740.692 MB (U(h) = 0.351234*h^0.145719)
    h2: 2233.31 MB (U(h) = 0.0378501*h^0.439375)
Recommendation: 2 clients, utility 1.03102:
    h1: 740.68 MB (U(h) = 0.351234*h^0.145719)
    h2: 2233.32 MB (U(h) = 0.0378501*h^0.439375)
Reading from 2789: heap size 530 MB, throughput 0.936519
Reading from 2788: heap size 1386 MB, throughput 0.947788
Reading from 2789: heap size 530 MB, throughput 0.620197
Reading from 2789: heap size 533 MB, throughput 0.809701
Reading from 2789: heap size 534 MB, throughput 0.968875
Reading from 2788: heap size 1387 MB, throughput 0.964862
Reading from 2789: heap size 541 MB, throughput 0.985324
Reading from 2788: heap size 1402 MB, throughput 0.971872
Reading from 2789: heap size 543 MB, throughput 0.990753
Reading from 2788: heap size 1406 MB, throughput 0.974233
Numeric result:
Recommendation: 2 clients, utility 1.04412:
    h1: 737.346 MB (U(h) = 0.349026*h^0.147128)
    h2: 2236.65 MB (U(h) = 0.0362302*h^0.446294)
Recommendation: 2 clients, utility 1.04412:
    h1: 737.348 MB (U(h) = 0.349026*h^0.147128)
    h2: 2236.65 MB (U(h) = 0.0362302*h^0.446294)
Reading from 2789: heap size 546 MB, throughput 0.991159
Reading from 2788: heap size 1407 MB, throughput 0.973203
Reading from 2789: heap size 549 MB, throughput 0.98939
Reading from 2788: heap size 1411 MB, throughput 0.970064
Reading from 2789: heap size 549 MB, throughput 0.955431
Reading from 2788: heap size 1401 MB, throughput 0.969558
Numeric result:
Recommendation: 2 clients, utility 0.992035:
    h1: 842.352 MB (U(h) = 0.345752*h^0.149241)
    h2: 2131.65 MB (U(h) = 0.0580802*h^0.377662)
Recommendation: 2 clients, utility 0.992035:
    h1: 842.359 MB (U(h) = 0.345752*h^0.149241)
    h2: 2131.64 MB (U(h) = 0.0580802*h^0.377662)
Reading from 2789: heap size 551 MB, throughput 0.964431
Reading from 2788: heap size 1408 MB, throughput 0.96792
Reading from 2789: heap size 555 MB, throughput 0.665404
Reading from 2789: heap size 556 MB, throughput 0.471764
Reading from 2789: heap size 558 MB, throughput 0.981599
Reading from 2788: heap size 1398 MB, throughput 0.965686
Reading from 2789: heap size 560 MB, throughput 0.99065
Reading from 2788: heap size 1403 MB, throughput 0.964781
Numeric result:
Recommendation: 2 clients, utility 1.03117:
    h1: 731.903 MB (U(h) = 0.354883*h^0.143423)
    h2: 2242.1 MB (U(h) = 0.0380483*h^0.439341)
Recommendation: 2 clients, utility 1.03117:
    h1: 731.928 MB (U(h) = 0.354883*h^0.143423)
    h2: 2242.07 MB (U(h) = 0.0380483*h^0.439341)
Reading from 2789: heap size 563 MB, throughput 0.991243
Reading from 2788: heap size 1410 MB, throughput 0.965763
Reading from 2789: heap size 565 MB, throughput 0.689477
Reading from 2788: heap size 1411 MB, throughput 0.962337
Reading from 2789: heap size 563 MB, throughput 0.953895
Reading from 2788: heap size 1418 MB, throughput 0.959053
Reading from 2789: heap size 566 MB, throughput 0.971067
Numeric result:
Recommendation: 2 clients, utility 1.09646:
    h1: 800.647 MB (U(h) = 0.290223*h^0.177753)
    h2: 2173.35 MB (U(h) = 0.0282467*h^0.48251)
Recommendation: 2 clients, utility 1.09646:
    h1: 800.646 MB (U(h) = 0.290223*h^0.177753)
    h2: 2173.35 MB (U(h) = 0.0282467*h^0.48251)
Reading from 2789: heap size 569 MB, throughput 0.980262
Reading from 2789: heap size 570 MB, throughput 0.952941
Reading from 2789: heap size 570 MB, throughput 0.981748
Reading from 2789: heap size 573 MB, throughput 0.990036
Reading from 2789: heap size 577 MB, throughput 0.624002
Numeric result:
Recommendation: 2 clients, utility 1.24194:
    h1: 1154.59 MB (U(h) = 0.135673*h^0.306191)
    h2: 1819.41 MB (U(h) = 0.0282467*h^0.48251)
Recommendation: 2 clients, utility 1.24194:
    h1: 1154.57 MB (U(h) = 0.135673*h^0.306191)
    h2: 1819.43 MB (U(h) = 0.0282467*h^0.48251)
Reading from 2788: heap size 1425 MB, throughput 0.982257
Reading from 2789: heap size 578 MB, throughput 0.956357
Reading from 2789: heap size 578 MB, throughput 0.975021
Reading from 2789: heap size 580 MB, throughput 0.967597
Numeric result:
Recommendation: 2 clients, utility 1.34443:
    h1: 1282.03 MB (U(h) = 0.0916167*h^0.37189)
    h2: 1691.97 MB (U(h) = 0.0266838*h^0.490807)
Recommendation: 2 clients, utility 1.34443:
    h1: 1282.03 MB (U(h) = 0.0916167*h^0.37189)
    h2: 1691.97 MB (U(h) = 0.0266838*h^0.490807)
Reading from 2789: heap size 584 MB, throughput 0.974364
Reading from 2789: heap size 585 MB, throughput 0.965706
Reading from 2789: heap size 585 MB, throughput 0.985592
Reading from 2789: heap size 588 MB, throughput 0.990321
Reading from 2789: heap size 590 MB, throughput 0.889501
Reading from 2788: heap size 1427 MB, throughput 0.986787
Numeric result:
Recommendation: 2 clients, utility 1.38559:
    h1: 1303.2 MB (U(h) = 0.0804402*h^0.393305)
    h2: 1670.8 MB (U(h) = 0.0243138*h^0.50425)
Recommendation: 2 clients, utility 1.38559:
    h1: 1303.19 MB (U(h) = 0.0804402*h^0.393305)
    h2: 1670.81 MB (U(h) = 0.0243138*h^0.50425)
Reading from 2789: heap size 592 MB, throughput 0.96472
Reading from 2788: heap size 1444 MB, throughput 0.982605
Reading from 2789: heap size 593 MB, throughput 0.969808
Reading from 2788: heap size 1448 MB, throughput 0.963114
Reading from 2788: heap size 1571 MB, throughput 0.968693
Reading from 2788: heap size 1623 MB, throughput 0.970853
Reading from 2788: heap size 1631 MB, throughput 0.974123
Reading from 2788: heap size 1643 MB, throughput 0.978606
Reading from 2788: heap size 1334 MB, throughput 0.981361
Numeric result:
Recommendation: 2 clients, utility 1.45659:
    h1: 1204.78 MB (U(h) = 0.0748318*h^0.405071)
    h2: 1769.22 MB (U(h) = 0.0128639*h^0.594835)
Recommendation: 2 clients, utility 1.45659:
    h1: 1204.79 MB (U(h) = 0.0748318*h^0.405071)
    h2: 1769.21 MB (U(h) = 0.0128639*h^0.594835)
Reading from 2789: heap size 594 MB, throughput 0.956159
Reading from 2789: heap size 595 MB, throughput 0.987174
Reading from 2789: heap size 600 MB, throughput 0.981124
Reading from 2788: heap size 1642 MB, throughput 0.989813
Reading from 2789: heap size 604 MB, throughput 0.989579
Reading from 2788: heap size 1352 MB, throughput 0.987902
Reading from 2788: heap size 1614 MB, throughput 0.985074
Reading from 2789: heap size 606 MB, throughput 0.992178
Numeric result:
Recommendation: 2 clients, utility 1.4028:
    h1: 997.857 MB (U(h) = 0.11739*h^0.331367)
    h2: 1976.14 MB (U(h) = 0.00833066*h^0.656254)
Recommendation: 2 clients, utility 1.4028:
    h1: 997.838 MB (U(h) = 0.11739*h^0.331367)
    h2: 1976.16 MB (U(h) = 0.00833066*h^0.656254)
Reading from 2788: heap size 1381 MB, throughput 0.981213
Reading from 2789: heap size 607 MB, throughput 0.99243
Reading from 2788: heap size 1589 MB, throughput 0.979596
Reading from 2789: heap size 610 MB, throughput 0.991842
Reading from 2788: heap size 1410 MB, throughput 0.977031
Reading from 2789: heap size 610 MB, throughput 0.990845
Numeric result:
Recommendation: 2 clients, utility 1.40547:
    h1: 1040.91 MB (U(h) = 0.107911*h^0.345049)
    h2: 1933.09 MB (U(h) = 0.00928635*h^0.640778)
Recommendation: 2 clients, utility 1.40547:
    h1: 1040.93 MB (U(h) = 0.107911*h^0.345049)
    h2: 1933.07 MB (U(h) = 0.00928635*h^0.640778)
Reading from 2788: heap size 1564 MB, throughput 0.974413
Reading from 2789: heap size 612 MB, throughput 0.989803
Reading from 2789: heap size 614 MB, throughput 0.984481
Reading from 2788: heap size 1440 MB, throughput 0.973303
Reading from 2789: heap size 616 MB, throughput 0.990299
Reading from 2788: heap size 1567 MB, throughput 0.972633
Reading from 2789: heap size 623 MB, throughput 0.992354
Numeric result:
Recommendation: 2 clients, utility 1.39454:
    h1: 943.335 MB (U(h) = 0.131453*h^0.313191)
    h2: 2030.66 MB (U(h) = 0.00731247*h^0.674183)
Recommendation: 2 clients, utility 1.39454:
    h1: 943.341 MB (U(h) = 0.131453*h^0.313191)
    h2: 2030.66 MB (U(h) = 0.00731247*h^0.674183)
Reading from 2788: heap size 1573 MB, throughput 0.970672
Reading from 2789: heap size 624 MB, throughput 0.992374
Reading from 2788: heap size 1581 MB, throughput 0.968117
Reading from 2789: heap size 624 MB, throughput 0.992021
Reading from 2788: heap size 1582 MB, throughput 0.9658
Reading from 2789: heap size 626 MB, throughput 0.990437
Numeric result:
Recommendation: 2 clients, utility 1.38615:
    h1: 1083.88 MB (U(h) = 0.103657*h^0.351311)
    h2: 1890.12 MB (U(h) = 0.0112903*h^0.612631)
Recommendation: 2 clients, utility 1.38615:
    h1: 1083.88 MB (U(h) = 0.103657*h^0.351311)
    h2: 1890.12 MB (U(h) = 0.0112903*h^0.612631)
Reading from 2789: heap size 626 MB, throughput 0.988872
Reading from 2789: heap size 630 MB, throughput 0.985351
Reading from 2788: heap size 1591 MB, throughput 0.965922
Reading from 2788: heap size 1598 MB, throughput 0.96231
Reading from 2789: heap size 635 MB, throughput 0.992729
Reading from 2788: heap size 1610 MB, throughput 0.961766
Reading from 2789: heap size 635 MB, throughput 0.993282
Numeric result:
Recommendation: 2 clients, utility 1.36662:
    h1: 1205.5 MB (U(h) = 0.0920798*h^0.370222)
    h2: 1768.5 MB (U(h) = 0.0184917*h^0.543105)
Recommendation: 2 clients, utility 1.36662:
    h1: 1205.53 MB (U(h) = 0.0920798*h^0.370222)
    h2: 1768.47 MB (U(h) = 0.0184917*h^0.543105)
Reading from 2788: heap size 1620 MB, throughput 0.956838
Reading from 2789: heap size 635 MB, throughput 0.992931
Reading from 2788: heap size 1637 MB, throughput 0.955232
Reading from 2789: heap size 638 MB, throughput 0.991792
Reading from 2788: heap size 1647 MB, throughput 0.956812
Numeric result:
Recommendation: 2 clients, utility 1.36084:
    h1: 1220.31 MB (U(h) = 0.0908848*h^0.372277)
    h2: 1753.69 MB (U(h) = 0.0195334*h^0.534999)
Recommendation: 2 clients, utility 1.36084:
    h1: 1220.3 MB (U(h) = 0.0908848*h^0.372277)
    h2: 1753.7 MB (U(h) = 0.0195334*h^0.534999)
Reading from 2789: heap size 640 MB, throughput 0.993353
Reading from 2789: heap size 640 MB, throughput 0.988008
Reading from 2789: heap size 641 MB, throughput 0.989586
Reading from 2788: heap size 1666 MB, throughput 0.95822
Reading from 2788: heap size 1671 MB, throughput 0.954815
Reading from 2789: heap size 642 MB, throughput 0.992265
Numeric result:
Recommendation: 2 clients, utility 1.3397:
    h1: 1189.93 MB (U(h) = 0.0999868*h^0.356937)
    h2: 1784.07 MB (U(h) = 0.0194667*h^0.535154)
Recommendation: 2 clients, utility 1.3397:
    h1: 1189.94 MB (U(h) = 0.0999868*h^0.356937)
    h2: 1784.06 MB (U(h) = 0.0194667*h^0.535154)
Reading from 2788: heap size 1681 MB, throughput 0.95461
Reading from 2789: heap size 645 MB, throughput 0.992754
Reading from 2788: heap size 1588 MB, throughput 0.95505
Reading from 2789: heap size 647 MB, throughput 0.992062
Reading from 2788: heap size 1680 MB, throughput 0.954057
Reading from 2789: heap size 649 MB, throughput 0.991747
Numeric result:
Recommendation: 2 clients, utility 1.27277:
    h1: 1059.29 MB (U(h) = 0.148484*h^0.294007)
    h2: 1914.71 MB (U(h) = 0.0199279*h^0.531437)
Recommendation: 2 clients, utility 1.27277:
    h1: 1059.28 MB (U(h) = 0.148484*h^0.294007)
    h2: 1914.72 MB (U(h) = 0.0199279*h^0.531437)
Reading from 2789: heap size 650 MB, throughput 0.989281
Reading from 2789: heap size 656 MB, throughput 0.98823
Reading from 2788: heap size 1585 MB, throughput 0.956157
Reading from 2789: heap size 658 MB, throughput 0.99285
Reading from 2788: heap size 1681 MB, throughput 0.951062
Numeric result:
Recommendation: 2 clients, utility 1.25235:
    h1: 1120.07 MB (U(h) = 0.149099*h^0.293283)
    h2: 1853.93 MB (U(h) = 0.0277654*h^0.485438)
Recommendation: 2 clients, utility 1.25235:
    h1: 1120.07 MB (U(h) = 0.149099*h^0.293283)
    h2: 1853.93 MB (U(h) = 0.0277654*h^0.485438)
Reading from 2789: heap size 659 MB, throughput 0.993096
Reading from 2788: heap size 1649 MB, throughput 0.973028
Reading from 2789: heap size 661 MB, throughput 0.992279
Reading from 2788: heap size 1724 MB, throughput 0.979748
Reading from 2789: heap size 661 MB, throughput 0.991893
Reading from 2788: heap size 1480 MB, throughput 0.980631
Numeric result:
Recommendation: 2 clients, utility 1.24025:
    h1: 1143.02 MB (U(h) = 0.152167*h^0.290023)
    h2: 1830.98 MB (U(h) = 0.0322475*h^0.464583)
Recommendation: 2 clients, utility 1.24025:
    h1: 1143.02 MB (U(h) = 0.152167*h^0.290023)
    h2: 1830.98 MB (U(h) = 0.0322475*h^0.464583)
Reading from 2789: heap size 662 MB, throughput 0.990795
Client 2789 died
Clients: 1
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 2788: heap size 1732 MB, throughput 0.995579
Recommendation: one client; give it all the memory
Reading from 2788: heap size 1530 MB, throughput 0.993834
Reading from 2788: heap size 1722 MB, throughput 0.987993
Reading from 2788: heap size 1719 MB, throughput 0.976129
Reading from 2788: heap size 1686 MB, throughput 0.957283
Reading from 2788: heap size 1614 MB, throughput 0.928928
Reading from 2788: heap size 1679 MB, throughput 0.891228
Reading from 2788: heap size 1685 MB, throughput 0.8661
Client 2788 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
