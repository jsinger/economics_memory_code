Using scaled threading model. 4 processors detected, 4 threads used to drive the workload, in a possible range of [1,64000]
java.lang.reflect.InvocationTargetException
java.lang.reflect.InvocationTargetException
	at sun.reflect.NativeMethodAccessorImpl.invoke0(Native Method)
	at sun.reflect.NativeMethodAccessorImpl.invoke(NativeMethodAccessorImpl.java:62)
	at sun.reflect.DelegatingMethodAccessorImpl.invoke(DelegatingMethodAccessorImpl.java:43)
	at java.lang.reflect.Method.invoke(Method.java:483)
	at org.dacapo.harness.H2.preIteration(H2.java:77)
	at org.dacapo.harness.Benchmark.run(Benchmark.java:152)
	at org.dacapo.harness.TestHarness.runBenchmark(TestHarness.java:218)
	at org.dacapo.harness.TestHarness.main(TestHarness.java:171)
	at Harness.main(Harness.java:17)
Caused by: org.h2.jdbc.JdbcSQLException: Out of memory.; SQL statement:
INSERT INTO HISTORY (H_C_ID, H_C_D_ID, H_C_W_ID, H_D_ID, H_W_ID, H_DATE, H_AMOUNT, H_DATA, H_INITIAL) VALUES (?, ?, ?, ?, ?, CURRENT TIMESTAMP, 10.00, ?, TRUE) [90108-123]
	at org.h2.message.Message.getSQLException(Message.java:111)
	at org.h2.message.Message.convertThrowable(Message.java:303)
	at org.h2.command.Command.executeUpdate(Command.java:231)
	at org.h2.jdbc.JdbcPreparedStatement.executeUpdateInternal(JdbcPreparedStatement.java:139)
	at org.h2.jdbc.JdbcPreparedStatement.executeUpdate(JdbcPreparedStatement.java:128)
	at org.apache.derbyTesting.system.oe.load.SimpleInsert.customerTable(SimpleInsert.java:350)
	at org.apache.derbyTesting.system.oe.load.SimpleInsert.populateForOneWarehouse(SimpleInsert.java:148)
	at org.apache.derbyTesting.system.oe.load.SimpleInsert.populateAllTables(SimpleInsert.java:127)
	at org.apache.derbyTesting.system.oe.load.ThreadInsert.populateAllTables(ThreadInsert.java:127)
	at org.dacapo.h2.TPCC.loadData(TPCC.java:400)
	at org.dacapo.h2.TPCC.preIterationMemoryDB(TPCC.java:215)
	at org.dacapo.h2.TPCC.preIteration(TPCC.java:254)
	... 9 more
Caused by: java.lang.OutOfMemoryError: GC overhead limit exceeded
	at org.h2.value.ValueTimestamp.convertScale(ValueTimestamp.java:149)
	at org.h2.table.Column.validateConvertUpdateSequence(Column.java:311)
	at org.h2.table.Table.validateConvertUpdateSequence(Table.java:599)
	at org.h2.command.dml.Insert.insertRows(Insert.java:116)
	at org.h2.command.dml.Insert.update(Insert.java:82)
	at org.h2.command.CommandContainer.update(CommandContainer.java:72)
	at org.h2.command.Command.executeUpdate(Command.java:209)
	... 18 more
