#!/usr/bin/env python
# Measure the number of iterations a benchmark can complete in a fixed time

import argparse
import argtypes
import staticutility as su
from measureBenchmark import measureBenchmark
from measurePostprocess import postprocess

RUNS = 5 # default number of runs


parser = argparse.ArgumentParser(description="Measure the number of iterations a benchmark can complete in a fixed time")

parser.add_argument("configFile", type=argtypes.existing_absolute_path, help="Each line in config file should be of the form 'benchmark input_size threads time min_heap max_heap increment', where time should be in a format suitable for use with 'sleep'")

parser.add_argument("-r", "--runs", type=argtypes.positive_int, default=RUNS, help="how many times to run the measurement, for taking averages")

args = parser.parse_args()

configFile = args.configFile
runs = args.runs

su.checkDaCapo()

outdir = su.MeasureDir(su.outputDir.join(su.measureDirName()))
outdir.makedir()
outdir.writeRuns(runs)
outdir.machineInfo()
outdir.pullInConfigFile(configFile)

inputLines = outdir.genInputFromConfig()

starttime = outdir.timestampStartLocal(1, sum(map(lambda l: su.timeToSeconds(l.b.time), inputLines)), runs)

for line in inputLines:
    measureBenchmark(runs, outdir, outdir.hostResultsFile(line.b, su.me()), line.b, line.heap, line.maxHeap)

postprocess(outdir)

outdir.timestampEndLocal(starttime)
