GCBench: 2 threads, 3 iters



##########
GCBench iteration 1
##########

[1] Garbage Collector Test
[1]  Stretching memory with a binary tree of depth 22
[0] Garbage Collector Test
[0]  Stretching memory with a binary tree of depth 22
 Total memory available=1318584320 bytes Total memory available=1318584320 bytes  Free memory=1284190784 bytes
  Free memory=1284190784 bytes
[0]  Creating a long-lived binary tree of depth 22
[1]  Creating a long-lived binary tree of depth 22
[0]  Creating a long-lived array of 700000 doubles
[1]  Creating a long-lived array of 700000 doubles
 Total memory available=1318584320 bytes  Free memory=761348360 bytes
Creating 8196 trees of depth 10
 Total memory available=1318584320 bytes  Free memory=729146376 bytes
Creating 8196 trees of depth 10
	Top down construction took 1438msecs
	Top down construction took 1845msecs
	Bottom up construction took 535msecs
Creating 2048 trees of depth 12
	Bottom up construction took 136msecs
Creating 2048 trees of depth 12
	Top down construction took 141msecs
	Top down construction took 141msecs
	Bottom up construction took 144msecs
Creating 512 trees of depth 14
	Bottom up construction took 135msecs
Creating 512 trees of depth 14
	Top down construction took 133msecs
	Top down construction took 134msecs
	Bottom up construction took 130msecs
Creating 128 trees of depth 16
	Bottom up construction took 129msecs
Creating 128 trees of depth 16
	Top down construction took 140msecs
	Top down construction took 137msecs
	Bottom up construction took 136msecs
Creating 32 trees of depth 18
	Bottom up construction took 135msecs
Creating 32 trees of depth 18
	Top down construction took 135msecs
	Top down construction took 133msecs
	Bottom up construction took 141msecs
Creating 8 trees of depth 20
	Bottom up construction took 141msecs
Creating 8 trees of depth 20
	Top down construction took 138msecs
	Top down construction took 137msecs
	Bottom up construction took 235msecs
Creating 2 trees of depth 22
	Bottom up construction took 236msecs
Creating 2 trees of depth 22
	Top down construction took 150msecs
	Top down construction took 153msecs
Pool did not terminate
[harness] Finished all threads


##########
GCBench iteration 2
##########

Exception in thread "pool-1-thread-1" [0] Garbage Collector Test
[0]  Stretching memory with a binary tree of depth 22
 Total memory available=1223163904 bytes  Free memory=686813888 bytes
java.lang.OutOfMemoryError: GC overhead limit exceeded
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.MakeTree(GCBench.java:83)
	at GCBench.TimeConstruction(GCBench.java:116)
	at GCBenchRunner.run(GCBenchRunner.java:111)
	at java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1142)
	at java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:617)
	at java.lang.Thread.run(Thread.java:745)
[1] Garbage Collector Test
[1]  Stretching memory with a binary tree of depth 22
 Total memory available=1223163904 bytes  Free memory=670784592 bytes
	Bottom up construction took 134702msecs
 Total memory available=1223163904 bytes  Free memory=665329960 bytes
[1] Completed in 139683ms.
