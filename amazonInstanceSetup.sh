#!/bin/bash

echo "THIS HASN'T BEEN UPDATED TO THE NEW DISTRIBUTE!"
exit 1

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") keyfile instance"
    exit 1
}

test -z "${1}" && usage
test ! -f "${1}" && usage
test -z "${2}" && usage

KEYFILE="$(readlink -f "${1}")"
INSTANCE="${2}"

scp -i "${KEYFILE}" -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "${DIR}/amazonInstance_remote.sh" "${INSTANCE}:amazonInstance_remote.sh"
scp -i "${KEYFILE}" -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "${HOME}/.ssh/id_rsa.pub" "${INSTANCE}:pubkey"
scp -i "${KEYFILE}" -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "${DIR}/deployment_key" "${INSTANCE}:deployment_key"
scp -i "${KEYFILE}" -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "${DIR}/deployment_key.pub" "${INSTANCE}:deployment_key.pub"
ssh -i "${KEYFILE}" -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "${INSTANCE}" "mkdir -p bin"
scp -i "${KEYFILE}" -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "${LINUXCONFIG_DIR}/LinuxBin/distribute_receiver" "${INSTANCE}:bin/distribute_receiver"
scp -i "${KEYFILE}" -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no "${LINUXCONFIG_DIR}/LinuxBin/distribute_run" "${INSTANCE}:bin/distribute_run"
ssh -i "${KEYFILE}" -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -t "${INSTANCE}" "bash amazonInstance_remote.sh"
