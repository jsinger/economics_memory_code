#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DACAPO='dacapo-9.12-bach.jar'

"${DIR}/checkDacapo.sh" || exit 1

DACAPODIR="$(readlink -f "$(mktemp -d)")"
cp "${DIR}/${DACAPO}" "${DACAPODIR}/${DACAPO}"
cd "${DACAPODIR}"
unzip "${DACAPO}" &>/dev/null

cd 'cnf'

for i in *.cnf; do
    FILE="$(readlink -f "${i}")"
    NAME="$(grep benchmark "${FILE}" | cut '--delimiter= ' -f 2)"

    if grep 'thread-model per_cpu' "${FILE}" >/dev/null; then
        MULTI_THREADED='yes'
    else
        MULTI_THREADED='no'
    fi

    grep 'size ' "${FILE}" | cut '--delimiter= ' -f 2 | while read size; do
        echo "${NAME} ${size} ${MULTI_THREADED}"
    done
done

cd
rm -r "${DACAPODIR}"
