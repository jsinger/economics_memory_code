module Economem.GCFactor where

import Control.Applicative
import qualified Data.ByteString.Char8 as B
import qualified Data.Csv as C
import Data.Maybe
import Economem.Common
import qualified Economem.Daemon as D
import qualified Economem.BenchmarkTimer as T
import Economem.Experiment
import Economem.Files
import Economem.Java
import Economem.Log
import qualified Economem.MetadataDir as M
import qualified Economem.MinHeap as MH
import Economem.Process
import Economem.Records
import Economem.Run
import System.Directory

name :: String
name = "gcFactor"


data GCFactorDir =
  GCFactorDir { dirPath :: FilePath,
                benchmarksFile :: CsvFile BenchmarkThreadsIters,
                minHeapsFile :: CsvFile BenchmarkThreadsMB,
                factorsFile :: LinesFile Double,
                javaNameFile :: StringFile,
                daemonLogFile :: ExperimentNum -> LinesFile String,
                subexpFactorFile :: ExperimentNum -> DoubleFile,
                resultsFile :: CsvFile GCFactorResult }

instance Path GCFactorDir where
  getPath = dirPath

instance Dir GCFactorDir where
  asDir path =
    GCFactorDir { dirPath = getPath path,
                  benchmarksFile = CsvFile (path </> "benchmarks.csv"),
                  minHeapsFile = CsvFile (path </> "min_heaps.csv"),
                  factorsFile = LinesFile (path </> "factors.txt"),
                  javaNameFile = WholeFile (path </> "java_name.txt"),
                  daemonLogFile = getSubFile "daemon_log" LinesFile,
                  subexpFactorFile = getSubFile "factor" WholeFile,
                  resultsFile = CsvFile (path </> "results.csv") }
    where
      getSubFile :: String -> (String -> a) -> ExperimentNum -> a
      getSubFile s f num = f (path </> (s ++ "_" ++ show (getNum num) ++ ".txt"))

newGCFactor :: Input BenchmarkThreadsIters ->
               Input BenchmarkThreadsMB ->
               [Double] ->
               Java ->
               Maybe SubexpInfo ->
               IO Experiment
newGCFactor input minHeaps factors j info = do
  (expDir, num, parentLog) <- newExperimentProperties name info
  makeGCFactor expDir num input minHeaps factors j parentLog

resumeGCFactor :: FilePath -> IO Experiment
resumeGCFactor =
  resumeExperiment
  (\expDir -> do let d = asDir expDir
                 let input = FromFile (benchmarksFile d)
                 let minHeaps = FromFile (minHeapsFile d)
                 factors <- get (factorsFile d)
                 j <- javaFromName =<< get (javaNameFile d)
                 makeGCFactor
                   expDir
                   topLevel
                   input
                   minHeaps
                   factors
                   j
                   Nothing)

makeGCFactor :: ExperimentDir ->
                ExperimentNum ->
                Input BenchmarkThreadsIters ->
                Input BenchmarkThreadsMB ->
                [Double] ->
                Java ->
                Maybe Log ->
                IO Experiment
makeGCFactor expDir expNum inputLoader minHeapsLoader factors j parentLog = do
  let ex = makeExperiment name expDir expNum 0 parentLog
  input <- loadInput inputLoader
  minHeaps <- loadInput minHeapsLoader
  let d = asDir expDir
  return (ex { saveAdditionalStaticMetadata = saveStatic d input minHeaps,
               canResume = canResumeFactor d,
               createSubexperiments = createSubexps input minHeaps,
               subexperimentCompleted = subexpCompleted })

  where
    saveStatic :: GCFactorDir -> [BenchmarkThreadsIters] -> [BenchmarkThreadsMB] -> IO ()
    saveStatic d input minHeaps = do
      input |> benchmarksFile d
      minHeaps |> minHeapsFile d
      factors |> factorsFile d
      javaName j |> javaNameFile d

    canResumeFactor :: GCFactorDir -> IO Bool
    canResumeFactor d =
      and <$> mapM doesFileExist [getPath (benchmarksFile d),
                                  getPath (minHeapsFile d),
                                  getPath (factorsFile d)]

    createSubexps :: [BenchmarkThreadsIters] -> [BenchmarkThreadsMB] -> Experiment -> IO [Experiment]
    createSubexps input minHeaps e = do
      let combinations = concatMap (\bm -> zip (repeat bm) factors) input
      let infos = seqSubexpInfos Static combinations e
      mapM makeSubexp (zip combinations infos)
      where
        makeSubexp :: ((BenchmarkThreadsIters, Double), SubexpInfo) -> IO Experiment
        makeSubexp ((bm, factor), info) = do
          minHeap <- fromIntegral . sizeToInt . toMB <$> MH.lookupMinHeap (btiBenchmark bm) minHeaps
          let xmx = DataSize (round (factor * minHeap)) MB
          ex <- T.newBenchmarkTimer
                "gcFactorTimer"
                (Raw [bm])
                xmx
                1
                j
                Nothing
                20
                [createDaemon (subexpNum info)]
                [D.simpleThroughput]
                Nothing
                False
                (Just info)
                []
          return (addStaticMetadataSaver ex
                  (factor |> subexpFactorFile (asDir (experimentDir e)) (subexpNum info)))

        createDaemon :: ExperimentNum -> Experiment -> RunNum -> ProcessNum -> IO ProcessLauncher
        createDaemon num subExp run auxNum = do
          let d = (D.makeDaemon (DataSize 1000 MB)) {
                D.recommendOnTimer = False,
                D.readingRecv = Just (D.throughputSocketName D.simpleThroughput),
                D.commandRecv = Just (D.throughputSocketName D.simpleThroughput),
                D.logFile = Just (getPath (daemonLogFile (asDir (experimentDir e)) num)),
                D.logReadings = True,
                D.logFunctions = True,
                D.verbose = True }
          D.daemonProcessLauncher
            d
            (M.auxOutputFile (metadataDir (experimentDir subExp)) run auxNum)
            (getPath (experimentDir subExp))
            Nothing


    subexpCompleted :: Experiment -> Experiment -> IO ()
    subexpCompleted e sub = do
      bm <- headIO =<< get (T.benchmarksFile (asDir (experimentDir sub)))
      heap <- fromIntegral <$> get (T.maxHeapFile (asDir (experimentDir sub)))
      factor <- get (subexpFactorFile (asDir (experimentDir e)) (experimentNum sub))
      numGCs <- getGCs (asDir (experimentDir e))
      runtime <- get (M.medianTimeFile (M.mainRuntimes (metadataDir (experimentDir sub))))
      GCFactorResult bm heap factor numGCs runtime |>> resultsFile (asDir (experimentDir e))

      where
        getGCs :: GCFactorDir -> IO Int
        getGCs d = do
          val <- headIO =<<
                 (filter (\l -> l !! 2 == "DEATH") .
                  filter (\l -> length l >= 4) .
                  map words <$>
                  get (daemonLogFile d (experimentNum sub)))
          readIO (val !! 3)

        headIO :: [a] -> IO a
        headIO xs = case listToMaybe xs of
          Just x -> return x
          Nothing -> fail "Head of empty list"

data GCFactorResult =
  GCFactorResult { gcfBenchmark :: BenchmarkThreadsIters,
                   gcfHeapSizeMB :: Int,
                   gcfFactor :: Double,
                   gcfNumGCs :: Int,
                   gcfRuntime :: Runtime }
  deriving (Read, Show)

instance C.FromNamedRecord GCFactorResult where
  parseNamedRecord r = GCFactorResult <$>
                       C.parseNamedRecord r <*>
                       r C..: B.pack "HEAP_SIZE_MB" <*>
                       r C..: B.pack "FACTOR" <*>
                       r C..: B.pack "NUM_GCS" <*>
                       C.parseNamedRecord r
instance C.ToNamedRecord GCFactorResult where
  toNamedRecord gcf = C.namedRecord (getNamedRecordInput gcf)
instance CsvFields GCFactorResult where
  getCsvFieldNames _ = getCsvFieldNames (Nothing :: Maybe BenchmarkThreadsIters) ++
                       [B.pack "HEAP_SIZE_MB",
                        B.pack "FACTOR",
                        B.pack "NUM_GCS"] ++
                       getCsvFieldNames (Nothing :: Maybe Runtime)
  getNamedRecordInput (GCFactorResult bm h f gcs rt) =
    getNamedRecordInput bm ++
    [B.pack "HEAP_SIZE_MB" C..= h,
     B.pack "FACTOR" C..= f,
     B.pack "NUM_GCS" C..= gcs] ++
    getNamedRecordInput rt
