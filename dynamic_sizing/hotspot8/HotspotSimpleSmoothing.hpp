#ifndef HOTSPOTSIMPLESMOOTHING_HPP
#define HOTSPOTSIMPLESMOOTHING_HPP

#include "gc_implementation/shared/SimpleSmoothingThroughput.hpp"

class HotspotSimpleSmoothing : public economem::SimpleSmoothingThroughput
{
public:
    HotspotSimpleSmoothing();

private:
    virtual unsigned long long getHeapSizeB();
    virtual void logReading(unsigned long long heapSizeB, double throughput, unsigned long long gcTime, unsigned long long interGCTime);
};

#endif
