#!/bin/bash
# Measure min heaps, on a remote machine

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") out_dir"
    exit 1
}

test -z "${1}" && usage

OUTDIR="$(readlink -f "${1}")"

ME="$(whoami)@$(hostname)"

mkdir -p "${OUTDIR}"

RESULTS="${OUTDIR}/results_${ME}.txt"

while read name size threads; do
    "${DIR}/minHeap_benchmark.sh" "${name}" "${size}" "${threads}" "${RESULTS}"
done
