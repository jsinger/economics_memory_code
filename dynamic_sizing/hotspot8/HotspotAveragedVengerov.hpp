#ifndef HOTSPOTAVERAGEDVENGEROV_HPP
#define HOTSPOTAVERAGEDVENGEROV_HPP

#include "gc_implementation/shared/AveragedVengerov.hpp"
#include "gc_implementation/parallelScavenge/parallelScavengeHeap.hpp"

class HotspotAveragedVengerov : public economem::AveragedVengerov
{
public:
    HotspotAveragedVengerov();

private:
    virtual unsigned long long getHeapSizeB();
    virtual unsigned long long getYoungGenSizeB();
    virtual unsigned long long getOldGenSizeB();
    virtual void logReading(unsigned long long heapSizeB, double throughput);
};

#endif
