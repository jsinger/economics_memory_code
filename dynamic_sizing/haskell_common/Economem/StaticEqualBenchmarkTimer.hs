module Economem.StaticEqualBenchmarkTimer where

import Control.Applicative
import Economem.Common
import Economem.BenchmarkTimer
import Economem.Experiment
import Economem.Files
import Economem.Java
import Economem.Records

newStaticEqualBenchmarkTimer :: Input BenchmarkThreadsIters ->
                                Integer ->
                                Java ->
                                DataSize ->
                                Bool ->
                                Maybe SubexpInfo ->
                                [String] ->
                                IO Experiment
newStaticEqualBenchmarkTimer input runs j totalMem forceGrowth info extraVMArgs = do
  numBMs <- fromIntegral . length <$> loadInput input
  newBenchmarkTimer
    "timeBenchmarkStaticEqual"
    input
    (DataSize (sizeToInt (toMB totalMem) `div` numBMs) MB)
    runs
    j
    Nothing
    0
    []
    []
    Nothing
    forceGrowth
    info
    extraVMArgs
