economemd
    total memory: 1656 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub21_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run10_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 24035: heap size 9 MB, throughput 0.991465
Clients: 1
Client 24035 has a minimum heap size of 276 MB
Reading from 24033: heap size 9 MB, throughput 0.990957
Clients: 2
Client 24033 has a minimum heap size of 276 MB
Reading from 24035: heap size 9 MB, throughput 0.979881
Reading from 24033: heap size 9 MB, throughput 0.982378
Reading from 24035: heap size 9 MB, throughput 0.960489
Reading from 24033: heap size 9 MB, throughput 0.954365
Reading from 24035: heap size 9 MB, throughput 0.949263
Reading from 24033: heap size 9 MB, throughput 0.959562
Reading from 24035: heap size 11 MB, throughput 0.969769
Reading from 24033: heap size 11 MB, throughput 0.990022
Reading from 24035: heap size 11 MB, throughput 0.979602
Reading from 24033: heap size 11 MB, throughput 0.982273
Reading from 24035: heap size 17 MB, throughput 0.942594
Reading from 24033: heap size 17 MB, throughput 0.971045
Reading from 24035: heap size 17 MB, throughput 0.577229
Reading from 24033: heap size 17 MB, throughput 0.533238
Reading from 24035: heap size 30 MB, throughput 0.921737
Reading from 24033: heap size 30 MB, throughput 0.867523
Reading from 24035: heap size 31 MB, throughput 0.923921
Reading from 24033: heap size 31 MB, throughput 0.798815
Reading from 24033: heap size 36 MB, throughput 0.379742
Reading from 24035: heap size 34 MB, throughput 0.501216
Reading from 24033: heap size 47 MB, throughput 0.852872
Reading from 24035: heap size 46 MB, throughput 0.914405
Reading from 24035: heap size 49 MB, throughput 0.886748
Reading from 24033: heap size 50 MB, throughput 0.55491
Reading from 24033: heap size 65 MB, throughput 0.815881
Reading from 24035: heap size 51 MB, throughput 0.262213
Reading from 24035: heap size 68 MB, throughput 0.738425
Reading from 24035: heap size 70 MB, throughput 0.613008
Reading from 24033: heap size 71 MB, throughput 0.281358
Reading from 24033: heap size 90 MB, throughput 0.705311
Reading from 24035: heap size 75 MB, throughput 0.298811
Reading from 24033: heap size 100 MB, throughput 0.754587
Reading from 24035: heap size 94 MB, throughput 0.629633
Reading from 24035: heap size 102 MB, throughput 0.771992
Reading from 24035: heap size 102 MB, throughput 0.609408
Reading from 24033: heap size 100 MB, throughput 0.280811
Reading from 24035: heap size 105 MB, throughput 0.261775
Reading from 24033: heap size 136 MB, throughput 0.887387
Reading from 24035: heap size 132 MB, throughput 0.753173
Reading from 24035: heap size 140 MB, throughput 0.705235
Reading from 24035: heap size 141 MB, throughput 0.715227
Reading from 24033: heap size 136 MB, throughput 0.262735
Reading from 24033: heap size 176 MB, throughput 0.809461
Reading from 24033: heap size 176 MB, throughput 0.779072
Reading from 24035: heap size 146 MB, throughput 0.155261
Reading from 24033: heap size 178 MB, throughput 0.835606
Reading from 24035: heap size 180 MB, throughput 0.660285
Reading from 24033: heap size 182 MB, throughput 0.767035
Reading from 24035: heap size 188 MB, throughput 0.666556
Reading from 24035: heap size 189 MB, throughput 0.593369
Reading from 24035: heap size 193 MB, throughput 0.776532
Reading from 24033: heap size 184 MB, throughput 0.213072
Reading from 24033: heap size 230 MB, throughput 0.914577
Reading from 24035: heap size 200 MB, throughput 0.91829
Reading from 24033: heap size 231 MB, throughput 0.920825
Reading from 24033: heap size 237 MB, throughput 0.824943
Reading from 24035: heap size 201 MB, throughput 0.175514
Reading from 24035: heap size 246 MB, throughput 0.565789
Reading from 24033: heap size 241 MB, throughput 0.9386
Reading from 24035: heap size 253 MB, throughput 0.755995
Reading from 24033: heap size 241 MB, throughput 0.830302
Reading from 24035: heap size 255 MB, throughput 0.487705
Reading from 24035: heap size 260 MB, throughput 0.641595
Reading from 24035: heap size 262 MB, throughput 0.717953
Reading from 24033: heap size 245 MB, throughput 0.95326
Reading from 24033: heap size 246 MB, throughput 0.781018
Reading from 24033: heap size 250 MB, throughput 0.898923
Reading from 24033: heap size 250 MB, throughput 0.725397
Reading from 24035: heap size 257 MB, throughput 0.160599
Reading from 24033: heap size 256 MB, throughput 0.827491
Reading from 24035: heap size 301 MB, throughput 0.784103
Reading from 24033: heap size 259 MB, throughput 0.799088
Reading from 24035: heap size 297 MB, throughput 0.608023
Reading from 24035: heap size 301 MB, throughput 0.808609
Reading from 24035: heap size 298 MB, throughput 0.821259
Reading from 24035: heap size 301 MB, throughput 0.822536
Reading from 24033: heap size 264 MB, throughput 0.924908
Reading from 24033: heap size 267 MB, throughput 0.925466
Reading from 24035: heap size 299 MB, throughput 0.96069
Reading from 24033: heap size 267 MB, throughput 0.748489
Reading from 24035: heap size 302 MB, throughput 0.77409
Reading from 24033: heap size 271 MB, throughput 0.753384
Reading from 24035: heap size 301 MB, throughput 0.803014
Reading from 24035: heap size 303 MB, throughput 0.807031
Reading from 24035: heap size 306 MB, throughput 0.714772
Reading from 24035: heap size 307 MB, throughput 0.877424
Reading from 24035: heap size 310 MB, throughput 0.764445
Reading from 24035: heap size 311 MB, throughput 0.706235
Reading from 24033: heap size 277 MB, throughput 0.091361
Reading from 24035: heap size 317 MB, throughput 0.751576
Reading from 24035: heap size 317 MB, throughput 0.681199
Reading from 24033: heap size 326 MB, throughput 0.739929
Reading from 24035: heap size 325 MB, throughput 0.722973
Reading from 24035: heap size 325 MB, throughput 0.657544
Reading from 24033: heap size 325 MB, throughput 0.82192
Reading from 24033: heap size 327 MB, throughput 0.736989
Reading from 24033: heap size 330 MB, throughput 0.707951
Reading from 24033: heap size 331 MB, throughput 0.630643
Reading from 24033: heap size 339 MB, throughput 0.576338
Equal recommendation: 828 MB each
Reading from 24033: heap size 340 MB, throughput 0.579271
Reading from 24033: heap size 350 MB, throughput 0.568932
Reading from 24035: heap size 334 MB, throughput 0.933888
Reading from 24033: heap size 351 MB, throughput 0.938286
Reading from 24035: heap size 334 MB, throughput 0.967552
Reading from 24033: heap size 358 MB, throughput 0.979096
Reading from 24035: heap size 339 MB, throughput 0.972997
Reading from 24033: heap size 361 MB, throughput 0.975265
Reading from 24035: heap size 341 MB, throughput 0.954848
Reading from 24033: heap size 360 MB, throughput 0.977822
Reading from 24035: heap size 343 MB, throughput 0.975665
Reading from 24033: heap size 364 MB, throughput 0.973227
Reading from 24035: heap size 344 MB, throughput 0.978633
Reading from 24033: heap size 367 MB, throughput 0.975443
Reading from 24035: heap size 343 MB, throughput 0.973347
Reading from 24033: heap size 369 MB, throughput 0.982305
Equal recommendation: 828 MB each
Reading from 24035: heap size 346 MB, throughput 0.979698
Reading from 24033: heap size 366 MB, throughput 0.972194
Reading from 24035: heap size 346 MB, throughput 0.97126
Reading from 24033: heap size 370 MB, throughput 0.971994
Reading from 24035: heap size 347 MB, throughput 0.975144
Reading from 24033: heap size 368 MB, throughput 0.975166
Reading from 24035: heap size 349 MB, throughput 0.97391
Reading from 24033: heap size 370 MB, throughput 0.970763
Reading from 24035: heap size 350 MB, throughput 0.97118
Reading from 24033: heap size 373 MB, throughput 0.972978
Reading from 24035: heap size 353 MB, throughput 0.675304
Reading from 24033: heap size 373 MB, throughput 0.970895
Reading from 24035: heap size 394 MB, throughput 0.972832
Equal recommendation: 828 MB each
Reading from 24033: heap size 376 MB, throughput 0.976364
Reading from 24035: heap size 397 MB, throughput 0.983162
Reading from 24035: heap size 398 MB, throughput 0.831079
Reading from 24035: heap size 398 MB, throughput 0.815889
Reading from 24035: heap size 400 MB, throughput 0.812854
Reading from 24035: heap size 407 MB, throughput 0.87265
Reading from 24033: heap size 377 MB, throughput 0.981282
Reading from 24033: heap size 382 MB, throughput 0.815546
Reading from 24033: heap size 384 MB, throughput 0.767911
Reading from 24033: heap size 390 MB, throughput 0.84281
Reading from 24033: heap size 393 MB, throughput 0.908068
Reading from 24035: heap size 408 MB, throughput 0.989504
Reading from 24033: heap size 400 MB, throughput 0.987971
Reading from 24035: heap size 414 MB, throughput 0.988214
Reading from 24035: heap size 415 MB, throughput 0.986421
Reading from 24033: heap size 401 MB, throughput 0.75636
Reading from 24035: heap size 416 MB, throughput 0.986145
Reading from 24033: heap size 432 MB, throughput 0.996287
Reading from 24035: heap size 419 MB, throughput 0.980298
Equal recommendation: 828 MB each
Reading from 24033: heap size 435 MB, throughput 0.993511
Reading from 24035: heap size 417 MB, throughput 0.984343
Reading from 24033: heap size 442 MB, throughput 0.990875
Reading from 24035: heap size 419 MB, throughput 0.982925
Reading from 24033: heap size 443 MB, throughput 0.990689
Reading from 24035: heap size 420 MB, throughput 0.982583
Reading from 24033: heap size 442 MB, throughput 0.989148
Reading from 24035: heap size 421 MB, throughput 0.982646
Reading from 24035: heap size 423 MB, throughput 0.981573
Reading from 24033: heap size 445 MB, throughput 0.988389
Reading from 24035: heap size 424 MB, throughput 0.991219
Reading from 24033: heap size 438 MB, throughput 0.985808
Reading from 24035: heap size 427 MB, throughput 0.920629
Equal recommendation: 828 MB each
Reading from 24035: heap size 427 MB, throughput 0.871193
Reading from 24035: heap size 431 MB, throughput 0.895418
Reading from 24035: heap size 433 MB, throughput 0.980792
Reading from 24033: heap size 442 MB, throughput 0.984909
Reading from 24035: heap size 440 MB, throughput 0.99078
Reading from 24033: heap size 440 MB, throughput 0.986474
Reading from 24033: heap size 441 MB, throughput 0.902369
Reading from 24033: heap size 438 MB, throughput 0.829592
Reading from 24033: heap size 443 MB, throughput 0.855785
Reading from 24035: heap size 441 MB, throughput 0.989236
Reading from 24033: heap size 451 MB, throughput 0.983291
Reading from 24035: heap size 442 MB, throughput 0.988336
Reading from 24033: heap size 454 MB, throughput 0.986507
Reading from 24035: heap size 444 MB, throughput 0.990514
Reading from 24033: heap size 451 MB, throughput 0.988521
Equal recommendation: 828 MB each
Reading from 24035: heap size 441 MB, throughput 0.987272
Reading from 24033: heap size 455 MB, throughput 0.986131
Reading from 24035: heap size 444 MB, throughput 0.984949
Reading from 24033: heap size 453 MB, throughput 0.982501
Reading from 24035: heap size 444 MB, throughput 0.986363
Reading from 24033: heap size 455 MB, throughput 0.984704
Reading from 24035: heap size 445 MB, throughput 0.983045
Reading from 24033: heap size 454 MB, throughput 0.983778
Reading from 24033: heap size 456 MB, throughput 0.981724
Reading from 24035: heap size 446 MB, throughput 0.989651
Reading from 24035: heap size 447 MB, throughput 0.93479
Reading from 24035: heap size 447 MB, throughput 0.878217
Equal recommendation: 828 MB each
Reading from 24035: heap size 449 MB, throughput 0.879754
Reading from 24033: heap size 459 MB, throughput 0.982104
Reading from 24035: heap size 456 MB, throughput 0.98955
Reading from 24035: heap size 457 MB, throughput 0.98617
Reading from 24033: heap size 459 MB, throughput 0.989611
Reading from 24033: heap size 461 MB, throughput 0.905481
Reading from 24033: heap size 462 MB, throughput 0.88861
Reading from 24033: heap size 469 MB, throughput 0.923053
Reading from 24035: heap size 461 MB, throughput 0.991363
Reading from 24033: heap size 469 MB, throughput 0.991767
Reading from 24035: heap size 462 MB, throughput 0.990355
Reading from 24033: heap size 474 MB, throughput 0.990384
Equal recommendation: 828 MB each
Reading from 24033: heap size 476 MB, throughput 0.988668
Reading from 24035: heap size 461 MB, throughput 0.985424
Reading from 24035: heap size 463 MB, throughput 0.986454
Reading from 24033: heap size 476 MB, throughput 0.98752
Reading from 24035: heap size 463 MB, throughput 0.984857
Reading from 24033: heap size 479 MB, throughput 0.98709
Reading from 24035: heap size 464 MB, throughput 0.983978
Reading from 24033: heap size 477 MB, throughput 0.987709
Reading from 24035: heap size 467 MB, throughput 0.990873
Reading from 24035: heap size 468 MB, throughput 0.87996
Reading from 24035: heap size 469 MB, throughput 0.896668
Reading from 24033: heap size 479 MB, throughput 0.983401
Equal recommendation: 828 MB each
Reading from 24035: heap size 471 MB, throughput 0.987407
Reading from 24033: heap size 482 MB, throughput 0.990264
Reading from 24035: heap size 479 MB, throughput 0.991
Reading from 24033: heap size 482 MB, throughput 0.971486
Reading from 24033: heap size 482 MB, throughput 0.907386
Reading from 24033: heap size 484 MB, throughput 0.966143
Reading from 24035: heap size 479 MB, throughput 0.99106
Reading from 24033: heap size 490 MB, throughput 0.993779
Reading from 24035: heap size 479 MB, throughput 0.989412
Reading from 24033: heap size 491 MB, throughput 0.986617
Equal recommendation: 828 MB each
Reading from 24035: heap size 482 MB, throughput 0.98726
Reading from 24033: heap size 493 MB, throughput 0.990565
Reading from 24035: heap size 480 MB, throughput 0.986433
Reading from 24033: heap size 494 MB, throughput 0.988575
Reading from 24035: heap size 482 MB, throughput 0.985267
Reading from 24033: heap size 493 MB, throughput 0.986749
Reading from 24035: heap size 485 MB, throughput 0.991974
Reading from 24035: heap size 486 MB, throughput 0.969451
Reading from 24035: heap size 486 MB, throughput 0.902603
Reading from 24035: heap size 488 MB, throughput 0.973992
Reading from 24033: heap size 495 MB, throughput 0.989388
Equal recommendation: 828 MB each
Reading from 24035: heap size 495 MB, throughput 0.992796
Reading from 24033: heap size 497 MB, throughput 0.98453
Reading from 24033: heap size 498 MB, throughput 0.974355
Reading from 24033: heap size 500 MB, throughput 0.892016
Reading from 24033: heap size 506 MB, throughput 0.941854
Reading from 24035: heap size 496 MB, throughput 0.992963
Reading from 24033: heap size 515 MB, throughput 0.990916
Reading from 24035: heap size 497 MB, throughput 0.990293
Reading from 24033: heap size 516 MB, throughput 0.989492
Reading from 24035: heap size 499 MB, throughput 0.989572
Equal recommendation: 828 MB each
Reading from 24035: heap size 497 MB, throughput 0.989311
Reading from 24033: heap size 514 MB, throughput 0.989436
Reading from 24035: heap size 499 MB, throughput 0.985964
Reading from 24033: heap size 517 MB, throughput 0.987574
Reading from 24035: heap size 501 MB, throughput 0.99171
Reading from 24035: heap size 502 MB, throughput 0.923202
Reading from 24035: heap size 502 MB, throughput 0.890514
Reading from 24033: heap size 520 MB, throughput 0.987939
Reading from 24035: heap size 504 MB, throughput 0.991191
Equal recommendation: 828 MB each
Reading from 24033: heap size 521 MB, throughput 0.986576
Reading from 24035: heap size 511 MB, throughput 0.991181
Reading from 24033: heap size 525 MB, throughput 0.990975
Reading from 24033: heap size 527 MB, throughput 0.910528
Reading from 24035: heap size 512 MB, throughput 0.991242
Reading from 24033: heap size 527 MB, throughput 0.879941
Reading from 24033: heap size 529 MB, throughput 0.992734
Reading from 24035: heap size 512 MB, throughput 0.988136
Equal recommendation: 828 MB each
Reading from 24033: heap size 537 MB, throughput 0.991516
Reading from 24035: heap size 515 MB, throughput 0.986607
Reading from 24035: heap size 516 MB, throughput 0.988146
Reading from 24033: heap size 538 MB, throughput 0.991361
Reading from 24035: heap size 517 MB, throughput 0.985346
Reading from 24033: heap size 540 MB, throughput 0.98548
Reading from 24035: heap size 521 MB, throughput 0.929679
Reading from 24035: heap size 521 MB, throughput 0.898612
Reading from 24035: heap size 526 MB, throughput 0.991834
Reading from 24033: heap size 542 MB, throughput 0.990241
Equal recommendation: 828 MB each
Reading from 24035: heap size 528 MB, throughput 0.992421
Reading from 24033: heap size 541 MB, throughput 0.991889
Reading from 24035: heap size 530 MB, throughput 0.992069
Reading from 24033: heap size 544 MB, throughput 0.923943
Reading from 24033: heap size 551 MB, throughput 0.966827
Reading from 24033: heap size 551 MB, throughput 0.995247
Reading from 24035: heap size 533 MB, throughput 0.990899
Equal recommendation: 828 MB each
Reading from 24033: heap size 553 MB, throughput 0.995453
Reading from 24035: heap size 531 MB, throughput 0.990894
Reading from 24033: heap size 556 MB, throughput 0.994924
Reading from 24035: heap size 534 MB, throughput 0.987018
Reading from 24035: heap size 536 MB, throughput 0.986768
Reading from 24035: heap size 537 MB, throughput 0.863673
Reading from 24033: heap size 558 MB, throughput 0.992512
Reading from 24035: heap size 541 MB, throughput 0.989873
Equal recommendation: 828 MB each
Reading from 24033: heap size 559 MB, throughput 0.991043
Reading from 24035: heap size 543 MB, throughput 0.99441
Reading from 24033: heap size 555 MB, throughput 0.990559
Reading from 24035: heap size 546 MB, throughput 0.991889
Reading from 24033: heap size 557 MB, throughput 0.994605
Reading from 24033: heap size 557 MB, throughput 0.907778
Reading from 24033: heap size 559 MB, throughput 0.912302
Reading from 24035: heap size 548 MB, throughput 0.990825
Equal recommendation: 828 MB each
Reading from 24033: heap size 572 MB, throughput 0.991927
Reading from 24035: heap size 546 MB, throughput 0.99085
Reading from 24033: heap size 572 MB, throughput 0.99188
Reading from 24035: heap size 549 MB, throughput 0.988361
Reading from 24035: heap size 551 MB, throughput 0.987308
Reading from 24035: heap size 552 MB, throughput 0.917535
Reading from 24033: heap size 576 MB, throughput 0.990884
Reading from 24035: heap size 556 MB, throughput 0.990864
Equal recommendation: 828 MB each
Reading from 24033: heap size 578 MB, throughput 0.990032
Reading from 24035: heap size 557 MB, throughput 0.994738
Reading from 24033: heap size 584 MB, throughput 0.988715
Reading from 24035: heap size 560 MB, throughput 0.992411
Reading from 24033: heap size 586 MB, throughput 0.988697
Reading from 24033: heap size 590 MB, throughput 0.895255
Reading from 24035: heap size 562 MB, throughput 0.99147
Equal recommendation: 828 MB each
Reading from 24033: heap size 594 MB, throughput 0.987978
Reading from 24035: heap size 560 MB, throughput 0.992758
Reading from 24033: heap size 602 MB, throughput 0.992236
Reading from 24035: heap size 562 MB, throughput 0.994011
Reading from 24035: heap size 563 MB, throughput 0.94635
Reading from 24035: heap size 564 MB, throughput 0.934065
Reading from 24033: heap size 605 MB, throughput 0.990293
Equal recommendation: 828 MB each
Reading from 24035: heap size 572 MB, throughput 0.992624
Reading from 24033: heap size 605 MB, throughput 0.991027
Reading from 24035: heap size 572 MB, throughput 0.992984
Reading from 24033: heap size 608 MB, throughput 0.990223
Reading from 24035: heap size 572 MB, throughput 0.99172
Reading from 24033: heap size 608 MB, throughput 0.993322
Reading from 24033: heap size 610 MB, throughput 0.924174
Equal recommendation: 828 MB each
Reading from 24035: heap size 575 MB, throughput 0.990232
Reading from 24033: heap size 614 MB, throughput 0.98868
Reading from 24035: heap size 577 MB, throughput 0.987613
Reading from 24033: heap size 615 MB, throughput 0.993825
Reading from 24035: heap size 578 MB, throughput 0.984408
Reading from 24035: heap size 581 MB, throughput 0.929574
Reading from 24035: heap size 583 MB, throughput 0.994753
Reading from 24033: heap size 618 MB, throughput 0.991079
Equal recommendation: 828 MB each
Reading from 24035: heap size 588 MB, throughput 0.993956
Reading from 24033: heap size 620 MB, throughput 0.992094
Reading from 24035: heap size 590 MB, throughput 0.992787
Reading from 24033: heap size 620 MB, throughput 0.992827
Equal recommendation: 828 MB each
Reading from 24033: heap size 622 MB, throughput 0.990704
Reading from 24033: heap size 626 MB, throughput 0.914835
Reading from 24035: heap size 589 MB, throughput 0.991949
Reading from 24033: heap size 627 MB, throughput 0.993778
Reading from 24035: heap size 591 MB, throughput 0.989453
Reading from 24035: heap size 592 MB, throughput 0.981956
Reading from 24035: heap size 594 MB, throughput 0.920899
Reading from 24033: heap size 634 MB, throughput 0.993322
Reading from 24035: heap size 598 MB, throughput 0.993778
Equal recommendation: 828 MB each
Reading from 24033: heap size 636 MB, throughput 0.989508
Reading from 24035: heap size 599 MB, throughput 0.994332
Reading from 24033: heap size 637 MB, throughput 0.992952
Reading from 24035: heap size 601 MB, throughput 0.993121
Equal recommendation: 828 MB each
Reading from 24033: heap size 640 MB, throughput 0.995163
Reading from 24033: heap size 643 MB, throughput 0.933317
Reading from 24035: heap size 603 MB, throughput 0.991619
Reading from 24033: heap size 644 MB, throughput 0.991937
Reading from 24035: heap size 602 MB, throughput 0.995284
Reading from 24035: heap size 604 MB, throughput 0.946297
Client 24035 died
Clients: 1
Reading from 24033: heap size 652 MB, throughput 0.995634
Recommendation: one client; give it all the memory
Reading from 24033: heap size 654 MB, throughput 0.99471
Reading from 24033: heap size 655 MB, throughput 0.994487
Recommendation: one client; give it all the memory
Reading from 24033: heap size 657 MB, throughput 0.99584
Reading from 24033: heap size 660 MB, throughput 0.952495
Client 24033 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
