economemd
    total memory: 864 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub57_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run05_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 8435: heap size 9 MB, throughput 0.99088
Clients: 1
Client 8435 has a minimum heap size of 12 MB
Reading from 8434: heap size 9 MB, throughput 0.991163
Clients: 2
Client 8434 has a minimum heap size of 276 MB
Reading from 8435: heap size 9 MB, throughput 0.989042
Reading from 8434: heap size 9 MB, throughput 0.984117
Reading from 8435: heap size 9 MB, throughput 0.95686
Reading from 8434: heap size 9 MB, throughput 0.960607
Reading from 8435: heap size 9 MB, throughput 0.961787
Reading from 8434: heap size 9 MB, throughput 0.917768
Reading from 8435: heap size 11 MB, throughput 0.980004
Reading from 8434: heap size 11 MB, throughput 0.975979
Reading from 8435: heap size 11 MB, throughput 0.975912
Reading from 8434: heap size 11 MB, throughput 0.985051
Reading from 8435: heap size 16 MB, throughput 0.9888
Reading from 8434: heap size 17 MB, throughput 0.940393
Reading from 8434: heap size 17 MB, throughput 0.564036
Reading from 8435: heap size 16 MB, throughput 0.983344
Reading from 8434: heap size 30 MB, throughput 0.953497
Reading from 8434: heap size 31 MB, throughput 0.974865
Reading from 8435: heap size 23 MB, throughput 0.926737
Reading from 8434: heap size 35 MB, throughput 0.430887
Reading from 8434: heap size 45 MB, throughput 0.859116
Reading from 8434: heap size 48 MB, throughput 0.890915
Reading from 8435: heap size 29 MB, throughput 0.983474
Reading from 8434: heap size 50 MB, throughput 0.309971
Reading from 8434: heap size 69 MB, throughput 0.826727
Reading from 8435: heap size 32 MB, throughput 0.981268
Reading from 8434: heap size 70 MB, throughput 0.276957
Reading from 8435: heap size 37 MB, throughput 0.979693
Reading from 8434: heap size 94 MB, throughput 0.84487
Reading from 8434: heap size 96 MB, throughput 0.752605
Reading from 8435: heap size 39 MB, throughput 0.984282
Reading from 8434: heap size 98 MB, throughput 0.767458
Reading from 8434: heap size 101 MB, throughput 0.674225
Reading from 8434: heap size 107 MB, throughput 0.744988
Reading from 8435: heap size 39 MB, throughput 0.982558
Reading from 8435: heap size 45 MB, throughput 0.979567
Reading from 8434: heap size 111 MB, throughput 0.679247
Reading from 8435: heap size 45 MB, throughput 0.984856
Reading from 8435: heap size 51 MB, throughput 0.979442
Reading from 8434: heap size 115 MB, throughput 0.170216
Reading from 8435: heap size 51 MB, throughput 0.976126
Reading from 8434: heap size 145 MB, throughput 0.603239
Reading from 8434: heap size 150 MB, throughput 0.63575
Reading from 8435: heap size 58 MB, throughput 0.978951
Reading from 8434: heap size 154 MB, throughput 0.518631
Reading from 8435: heap size 58 MB, throughput 0.408762
Reading from 8434: heap size 158 MB, throughput 0.524465
Reading from 8435: heap size 69 MB, throughput 0.994564
Reading from 8434: heap size 164 MB, throughput 0.112442
Reading from 8435: heap size 69 MB, throughput 0.96407
Reading from 8434: heap size 204 MB, throughput 0.49085
Reading from 8434: heap size 205 MB, throughput 0.713725
Reading from 8434: heap size 205 MB, throughput 0.152769
Reading from 8435: heap size 80 MB, throughput 0.997567
Reading from 8434: heap size 243 MB, throughput 0.909192
Reading from 8435: heap size 80 MB, throughput 0.996719
Reading from 8434: heap size 241 MB, throughput 0.903778
Reading from 8434: heap size 244 MB, throughput 0.748837
Reading from 8434: heap size 248 MB, throughput 0.823147
Reading from 8434: heap size 248 MB, throughput 0.708005
Reading from 8434: heap size 252 MB, throughput 0.710561
Reading from 8435: heap size 89 MB, throughput 0.997421
Reading from 8434: heap size 253 MB, throughput 0.892885
Reading from 8434: heap size 257 MB, throughput 0.805556
Reading from 8434: heap size 258 MB, throughput 0.682579
Reading from 8434: heap size 264 MB, throughput 0.70069
Reading from 8434: heap size 268 MB, throughput 0.693807
Reading from 8435: heap size 91 MB, throughput 0.995978
Reading from 8434: heap size 275 MB, throughput 0.676856
Reading from 8434: heap size 275 MB, throughput 0.828355
Reading from 8435: heap size 98 MB, throughput 0.995868
Reading from 8434: heap size 276 MB, throughput 0.94443
Reading from 8434: heap size 278 MB, throughput 0.120097
Reading from 8435: heap size 99 MB, throughput 0.993345
Reading from 8434: heap size 315 MB, throughput 0.740357
Reading from 8434: heap size 318 MB, throughput 0.88181
Reading from 8434: heap size 323 MB, throughput 0.915547
Reading from 8434: heap size 324 MB, throughput 0.890645
Reading from 8434: heap size 328 MB, throughput 0.735794
Reading from 8435: heap size 105 MB, throughput 0.997181
Reading from 8434: heap size 328 MB, throughput 0.689224
Reading from 8434: heap size 333 MB, throughput 0.635568
Reading from 8434: heap size 333 MB, throughput 0.696729
Reading from 8434: heap size 340 MB, throughput 0.671869
Reading from 8434: heap size 340 MB, throughput 0.750447
Equal recommendation: 432 MB each
Reading from 8435: heap size 105 MB, throughput 0.996227
Reading from 8435: heap size 110 MB, throughput 0.995223
Reading from 8435: heap size 110 MB, throughput 0.99617
Reading from 8434: heap size 348 MB, throughput 0.985215
Reading from 8435: heap size 115 MB, throughput 0.996503
Reading from 8434: heap size 349 MB, throughput 0.978044
Reading from 8435: heap size 115 MB, throughput 0.996236
Reading from 8435: heap size 118 MB, throughput 0.996561
Reading from 8434: heap size 349 MB, throughput 0.981409
Reading from 8435: heap size 118 MB, throughput 0.995718
Reading from 8435: heap size 122 MB, throughput 0.996466
Reading from 8435: heap size 122 MB, throughput 0.988604
Reading from 8434: heap size 352 MB, throughput 0.684255
Reading from 8435: heap size 125 MB, throughput 0.997265
Reading from 8434: heap size 400 MB, throughput 0.983135
Reading from 8435: heap size 125 MB, throughput 0.99778
Reading from 8435: heap size 128 MB, throughput 0.998412
Reading from 8434: heap size 401 MB, throughput 0.994707
Reading from 8435: heap size 128 MB, throughput 0.998133
Equal recommendation: 432 MB each
Reading from 8435: heap size 130 MB, throughput 0.998144
Reading from 8435: heap size 130 MB, throughput 0.998084
Reading from 8434: heap size 408 MB, throughput 0.992646
Reading from 8435: heap size 132 MB, throughput 0.997938
Reading from 8434: heap size 409 MB, throughput 0.992008
Reading from 8435: heap size 132 MB, throughput 0.998691
Reading from 8435: heap size 134 MB, throughput 0.995284
Reading from 8435: heap size 134 MB, throughput 0.99256
Reading from 8434: heap size 408 MB, throughput 0.980645
Reading from 8435: heap size 136 MB, throughput 0.984296
Reading from 8435: heap size 137 MB, throughput 0.995343
Reading from 8434: heap size 410 MB, throughput 0.985312
Reading from 8435: heap size 142 MB, throughput 0.997508
Reading from 8435: heap size 142 MB, throughput 0.996974
Reading from 8434: heap size 404 MB, throughput 0.989009
Reading from 8435: heap size 146 MB, throughput 0.993047
Reading from 8435: heap size 146 MB, throughput 0.996919
Reading from 8434: heap size 376 MB, throughput 0.984453
Reading from 8435: heap size 150 MB, throughput 0.99749
Equal recommendation: 432 MB each
Reading from 8435: heap size 150 MB, throughput 0.997285
Reading from 8434: heap size 403 MB, throughput 0.986948
Reading from 8435: heap size 153 MB, throughput 0.996758
Reading from 8435: heap size 154 MB, throughput 0.997094
Reading from 8434: heap size 405 MB, throughput 0.985246
Reading from 8434: heap size 405 MB, throughput 0.899407
Reading from 8434: heap size 407 MB, throughput 0.788466
Reading from 8434: heap size 413 MB, throughput 0.832404
Reading from 8434: heap size 418 MB, throughput 0.821017
Reading from 8435: heap size 156 MB, throughput 0.996975
Reading from 8434: heap size 426 MB, throughput 0.978421
Reading from 8435: heap size 157 MB, throughput 0.99735
Reading from 8435: heap size 160 MB, throughput 0.997704
Reading from 8434: heap size 430 MB, throughput 0.986481
Reading from 8435: heap size 160 MB, throughput 0.997064
Reading from 8435: heap size 162 MB, throughput 0.997876
Reading from 8434: heap size 424 MB, throughput 0.987984
Reading from 8435: heap size 162 MB, throughput 0.9973
Reading from 8434: heap size 429 MB, throughput 0.989034
Reading from 8435: heap size 165 MB, throughput 0.998577
Equal recommendation: 432 MB each
Reading from 8435: heap size 165 MB, throughput 0.998622
Reading from 8434: heap size 424 MB, throughput 0.98778
Reading from 8435: heap size 167 MB, throughput 0.998269
Reading from 8435: heap size 167 MB, throughput 0.984826
Reading from 8435: heap size 168 MB, throughput 0.993162
Reading from 8434: heap size 428 MB, throughput 0.985438
Reading from 8435: heap size 169 MB, throughput 0.996515
Reading from 8435: heap size 174 MB, throughput 0.99757
Reading from 8434: heap size 424 MB, throughput 0.987639
Reading from 8435: heap size 174 MB, throughput 0.997245
Reading from 8435: heap size 178 MB, throughput 0.996804
Reading from 8434: heap size 427 MB, throughput 0.986677
Reading from 8435: heap size 178 MB, throughput 0.996727
Reading from 8434: heap size 425 MB, throughput 0.987251
Reading from 8435: heap size 182 MB, throughput 0.99799
Equal recommendation: 432 MB each
Reading from 8435: heap size 182 MB, throughput 0.997317
Reading from 8434: heap size 426 MB, throughput 0.985046
Reading from 8435: heap size 185 MB, throughput 0.997951
Reading from 8435: heap size 185 MB, throughput 0.997002
Reading from 8434: heap size 426 MB, throughput 0.987258
Reading from 8435: heap size 188 MB, throughput 0.997863
Reading from 8434: heap size 427 MB, throughput 0.992636
Reading from 8435: heap size 188 MB, throughput 0.997131
Reading from 8434: heap size 428 MB, throughput 0.92498
Reading from 8434: heap size 428 MB, throughput 0.881921
Reading from 8434: heap size 432 MB, throughput 0.882539
Reading from 8435: heap size 191 MB, throughput 0.996042
Reading from 8434: heap size 433 MB, throughput 0.987333
Reading from 8435: heap size 191 MB, throughput 0.997204
Reading from 8434: heap size 424 MB, throughput 0.992727
Reading from 8435: heap size 194 MB, throughput 0.998102
Reading from 8435: heap size 194 MB, throughput 0.91113
Equal recommendation: 432 MB each
Reading from 8435: heap size 201 MB, throughput 0.990075
Reading from 8434: heap size 432 MB, throughput 0.991445
Reading from 8435: heap size 201 MB, throughput 0.996025
Reading from 8434: heap size 422 MB, throughput 0.990084
Reading from 8435: heap size 206 MB, throughput 0.998254
Reading from 8435: heap size 207 MB, throughput 0.998014
Reading from 8434: heap size 429 MB, throughput 0.989126
Reading from 8435: heap size 212 MB, throughput 0.997445
Reading from 8434: heap size 428 MB, throughput 0.989751
Reading from 8435: heap size 213 MB, throughput 0.99823
Reading from 8435: heap size 217 MB, throughput 0.99803
Reading from 8434: heap size 430 MB, throughput 0.989083
Reading from 8435: heap size 218 MB, throughput 0.997954
Equal recommendation: 432 MB each
Reading from 8434: heap size 427 MB, throughput 0.988234
Reading from 8435: heap size 222 MB, throughput 0.998393
Reading from 8435: heap size 222 MB, throughput 0.991459
Reading from 8434: heap size 429 MB, throughput 0.986315
Reading from 8435: heap size 226 MB, throughput 0.998321
Reading from 8434: heap size 430 MB, throughput 0.985939
Reading from 8435: heap size 226 MB, throughput 0.997913
Reading from 8434: heap size 430 MB, throughput 0.990611
Reading from 8435: heap size 231 MB, throughput 0.998186
Reading from 8434: heap size 430 MB, throughput 0.95346
Reading from 8434: heap size 431 MB, throughput 0.804127
Reading from 8434: heap size 434 MB, throughput 0.840306
Reading from 8435: heap size 231 MB, throughput 0.994817
Reading from 8434: heap size 425 MB, throughput 0.980193
Reading from 8435: heap size 235 MB, throughput 0.994094
Reading from 8435: heap size 236 MB, throughput 0.998098
Equal recommendation: 432 MB each
Reading from 8434: heap size 433 MB, throughput 0.994022
Reading from 8435: heap size 243 MB, throughput 0.998184
Reading from 8434: heap size 403 MB, throughput 0.992295
Reading from 8435: heap size 243 MB, throughput 0.997161
Reading from 8435: heap size 248 MB, throughput 0.998041
Reading from 8434: heap size 433 MB, throughput 0.991411
Reading from 8435: heap size 249 MB, throughput 0.997638
Reading from 8434: heap size 404 MB, throughput 0.990465
Reading from 8435: heap size 254 MB, throughput 0.998304
Reading from 8434: heap size 430 MB, throughput 0.990921
Reading from 8435: heap size 254 MB, throughput 0.997999
Equal recommendation: 432 MB each
Reading from 8434: heap size 429 MB, throughput 0.988923
Reading from 8435: heap size 259 MB, throughput 0.998309
Reading from 8435: heap size 259 MB, throughput 0.997968
Reading from 8434: heap size 426 MB, throughput 0.988438
Reading from 8435: heap size 263 MB, throughput 0.997379
Reading from 8434: heap size 428 MB, throughput 0.983483
Reading from 8435: heap size 263 MB, throughput 0.993939
Reading from 8435: heap size 268 MB, throughput 0.995026
Reading from 8434: heap size 430 MB, throughput 0.985996
Reading from 8435: heap size 268 MB, throughput 0.997842
Reading from 8434: heap size 430 MB, throughput 0.992532
Reading from 8435: heap size 274 MB, throughput 0.998109
Reading from 8434: heap size 431 MB, throughput 0.97611
Reading from 8434: heap size 432 MB, throughput 0.888812
Equal recommendation: 432 MB each
Reading from 8434: heap size 435 MB, throughput 0.89189
Reading from 8434: heap size 426 MB, throughput 0.978007
Reading from 8435: heap size 275 MB, throughput 0.997896
Reading from 8435: heap size 281 MB, throughput 0.996934
Reading from 8434: heap size 435 MB, throughput 0.995065
Reading from 8435: heap size 281 MB, throughput 0.997398
Reading from 8434: heap size 404 MB, throughput 0.991842
Reading from 8435: heap size 288 MB, throughput 0.998017
Reading from 8434: heap size 433 MB, throughput 0.991121
Reading from 8435: heap size 288 MB, throughput 0.998202
Reading from 8434: heap size 405 MB, throughput 0.990164
Equal recommendation: 432 MB each
Reading from 8435: heap size 292 MB, throughput 0.998307
Reading from 8434: heap size 430 MB, throughput 0.989901
Reading from 8435: heap size 293 MB, throughput 0.997359
Reading from 8435: heap size 298 MB, throughput 0.992134
Reading from 8434: heap size 429 MB, throughput 0.990183
Reading from 8435: heap size 298 MB, throughput 0.997238
Reading from 8434: heap size 427 MB, throughput 0.988798
Reading from 8435: heap size 302 MB, throughput 0.998442
Reading from 8434: heap size 429 MB, throughput 0.988266
Reading from 8435: heap size 301 MB, throughput 0.99747
Equal recommendation: 432 MB each
Reading from 8434: heap size 430 MB, throughput 0.988632
Reading from 8435: heap size 301 MB, throughput 0.99825
Reading from 8434: heap size 430 MB, throughput 0.98544
Reading from 8434: heap size 433 MB, throughput 0.969499
Reading from 8434: heap size 426 MB, throughput 0.892675
Reading from 8435: heap size 301 MB, throughput 0.99821
Reading from 8434: heap size 431 MB, throughput 0.895536
Reading from 8434: heap size 433 MB, throughput 0.981888
Reading from 8435: heap size 302 MB, throughput 0.998713
Reading from 8434: heap size 424 MB, throughput 0.991812
Reading from 8435: heap size 300 MB, throughput 0.998596
Reading from 8434: heap size 432 MB, throughput 0.991237
Reading from 8435: heap size 301 MB, throughput 0.998449
Reading from 8435: heap size 301 MB, throughput 0.995392
Reading from 8434: heap size 422 MB, throughput 0.985672
Equal recommendation: 432 MB each
Reading from 8435: heap size 300 MB, throughput 0.997271
Reading from 8434: heap size 429 MB, throughput 0.990873
Reading from 8435: heap size 301 MB, throughput 0.997347
Reading from 8434: heap size 427 MB, throughput 0.989296
Reading from 8435: heap size 301 MB, throughput 0.998356
Reading from 8434: heap size 429 MB, throughput 0.988434
Reading from 8435: heap size 301 MB, throughput 0.997808
Reading from 8434: heap size 426 MB, throughput 0.988253
Reading from 8435: heap size 300 MB, throughput 0.998299
Reading from 8434: heap size 428 MB, throughput 0.987357
Equal recommendation: 432 MB each
Reading from 8435: heap size 301 MB, throughput 0.997511
Reading from 8434: heap size 429 MB, throughput 0.986293
Reading from 8435: heap size 302 MB, throughput 0.998468
Reading from 8434: heap size 430 MB, throughput 0.988773
Reading from 8434: heap size 431 MB, throughput 0.969276
Reading from 8434: heap size 432 MB, throughput 0.869446
Reading from 8434: heap size 435 MB, throughput 0.888189
Reading from 8435: heap size 302 MB, throughput 0.998498
Reading from 8434: heap size 426 MB, throughput 0.983275
Reading from 8435: heap size 302 MB, throughput 0.99758
Reading from 8435: heap size 302 MB, throughput 0.887004
Reading from 8434: heap size 435 MB, throughput 0.994313
Reading from 8435: heap size 308 MB, throughput 0.998997
Reading from 8434: heap size 404 MB, throughput 0.993054
Equal recommendation: 432 MB each
Reading from 8435: heap size 304 MB, throughput 0.998746
Reading from 8434: heap size 433 MB, throughput 0.991987
Reading from 8435: heap size 308 MB, throughput 0.999209
Reading from 8434: heap size 405 MB, throughput 0.990996
Reading from 8435: heap size 308 MB, throughput 0.99927
Reading from 8434: heap size 430 MB, throughput 0.989829
Reading from 8435: heap size 308 MB, throughput 0.999215
Reading from 8434: heap size 429 MB, throughput 0.989297
Reading from 8435: heap size 308 MB, throughput 0.999142
Reading from 8434: heap size 427 MB, throughput 0.98774
Equal recommendation: 432 MB each
Reading from 8435: heap size 308 MB, throughput 0.999178
Reading from 8434: heap size 429 MB, throughput 0.986173
Reading from 8435: heap size 308 MB, throughput 0.998649
Reading from 8434: heap size 430 MB, throughput 0.976156
Reading from 8435: heap size 308 MB, throughput 0.997714
Reading from 8435: heap size 308 MB, throughput 0.996701
Reading from 8434: heap size 430 MB, throughput 0.987044
Reading from 8434: heap size 433 MB, throughput 0.966937
Reading from 8434: heap size 425 MB, throughput 0.888022
Reading from 8434: heap size 431 MB, throughput 0.888612
Reading from 8435: heap size 306 MB, throughput 0.998705
Reading from 8434: heap size 433 MB, throughput 0.9822
Reading from 8435: heap size 307 MB, throughput 0.997859
Reading from 8434: heap size 424 MB, throughput 0.993625
Equal recommendation: 432 MB each
Reading from 8435: heap size 306 MB, throughput 0.998617
Reading from 8434: heap size 432 MB, throughput 0.992134
Reading from 8435: heap size 307 MB, throughput 0.998022
Reading from 8434: heap size 433 MB, throughput 0.99153
Reading from 8435: heap size 307 MB, throughput 0.998446
Reading from 8434: heap size 407 MB, throughput 0.99175
Reading from 8435: heap size 307 MB, throughput 0.998651
Reading from 8434: heap size 432 MB, throughput 0.989099
Reading from 8435: heap size 307 MB, throughput 0.998536
Reading from 8434: heap size 410 MB, throughput 0.987209
Equal recommendation: 432 MB each
Reading from 8435: heap size 307 MB, throughput 0.998211
Reading from 8435: heap size 308 MB, throughput 0.995243
Reading from 8434: heap size 428 MB, throughput 0.983101
Reading from 8435: heap size 308 MB, throughput 0.995953
Reading from 8434: heap size 427 MB, throughput 0.98593
Reading from 8435: heap size 306 MB, throughput 0.99837
Reading from 8434: heap size 429 MB, throughput 0.985918
Reading from 8435: heap size 307 MB, throughput 0.998302
Reading from 8434: heap size 429 MB, throughput 0.987523
Reading from 8434: heap size 432 MB, throughput 0.966656
Reading from 8435: heap size 307 MB, throughput 0.998259
Reading from 8434: heap size 432 MB, throughput 0.883658
Reading from 8434: heap size 429 MB, throughput 0.890654
Reading from 8434: heap size 433 MB, throughput 0.981788
Equal recommendation: 432 MB each
Reading from 8435: heap size 307 MB, throughput 0.998081
Reading from 8434: heap size 425 MB, throughput 0.993511
Reading from 8435: heap size 308 MB, throughput 0.998403
Reading from 8434: heap size 432 MB, throughput 0.992295
Reading from 8435: heap size 308 MB, throughput 0.998408
Reading from 8434: heap size 422 MB, throughput 0.992729
Reading from 8435: heap size 307 MB, throughput 0.998015
Reading from 8435: heap size 308 MB, throughput 0.9973
Reading from 8434: heap size 429 MB, throughput 0.98163
Reading from 8435: heap size 309 MB, throughput 0.993099
Reading from 8434: heap size 428 MB, throughput 0.990078
Equal recommendation: 432 MB each
Reading from 8435: heap size 309 MB, throughput 0.998286
Reading from 8434: heap size 430 MB, throughput 0.990172
Reading from 8435: heap size 308 MB, throughput 0.998384
Reading from 8434: heap size 427 MB, throughput 0.988783
Reading from 8435: heap size 308 MB, throughput 0.998422
Reading from 8435: heap size 308 MB, throughput 0.998323
Reading from 8434: heap size 429 MB, throughput 0.988337
Reading from 8435: heap size 306 MB, throughput 0.997905
Reading from 8434: heap size 430 MB, throughput 0.989049
Equal recommendation: 432 MB each
Reading from 8435: heap size 308 MB, throughput 0.998494
Reading from 8434: heap size 430 MB, throughput 0.990952
Reading from 8434: heap size 430 MB, throughput 0.974112
Reading from 8434: heap size 432 MB, throughput 0.869915
Reading from 8434: heap size 434 MB, throughput 0.8931
Reading from 8435: heap size 308 MB, throughput 0.998487
Reading from 8434: heap size 425 MB, throughput 0.979657
Reading from 8435: heap size 309 MB, throughput 0.998518
Reading from 8434: heap size 434 MB, throughput 0.993245
Reading from 8435: heap size 309 MB, throughput 0.997176
Reading from 8435: heap size 309 MB, throughput 0.995918
Reading from 8434: heap size 403 MB, throughput 0.992612
Reading from 8435: heap size 309 MB, throughput 0.998201
Reading from 8434: heap size 433 MB, throughput 0.991173
Equal recommendation: 432 MB each
Reading from 8435: heap size 308 MB, throughput 0.997944
Reading from 8434: heap size 405 MB, throughput 0.989576
Reading from 8435: heap size 308 MB, throughput 0.998067
Reading from 8434: heap size 430 MB, throughput 0.990744
Reading from 8435: heap size 308 MB, throughput 0.998506
Reading from 8434: heap size 429 MB, throughput 0.990202
Reading from 8435: heap size 306 MB, throughput 0.997615
Reading from 8434: heap size 426 MB, throughput 0.987888
Reading from 8435: heap size 308 MB, throughput 0.998566
Reading from 8434: heap size 428 MB, throughput 0.987988
Equal recommendation: 432 MB each
Reading from 8435: heap size 308 MB, throughput 0.998197
Reading from 8434: heap size 430 MB, throughput 0.984972
Reading from 8435: heap size 309 MB, throughput 0.998707
Reading from 8435: heap size 309 MB, throughput 0.993613
Reading from 8434: heap size 430 MB, throughput 0.985066
Reading from 8434: heap size 432 MB, throughput 0.966646
Reading from 8434: heap size 425 MB, throughput 0.871148
Reading from 8434: heap size 430 MB, throughput 0.888467
Reading from 8434: heap size 432 MB, throughput 0.978128
Reading from 8435: heap size 307 MB, throughput 0.996513
Reading from 8435: heap size 308 MB, throughput 0.997987
Reading from 8434: heap size 439 MB, throughput 0.993148
Reading from 8435: heap size 307 MB, throughput 0.998388
Reading from 8434: heap size 407 MB, throughput 0.987119
Equal recommendation: 432 MB each
Reading from 8435: heap size 307 MB, throughput 0.998316
Reading from 8434: heap size 437 MB, throughput 0.991883
Reading from 8435: heap size 308 MB, throughput 0.998104
Reading from 8434: heap size 409 MB, throughput 0.990215
Reading from 8435: heap size 308 MB, throughput 0.99841
Reading from 8434: heap size 435 MB, throughput 0.989671
Reading from 8435: heap size 308 MB, throughput 0.995248
Reading from 8434: heap size 412 MB, throughput 0.988293
Reading from 8435: heap size 306 MB, throughput 0.998249
Reading from 8434: heap size 430 MB, throughput 0.987943
Reading from 8435: heap size 308 MB, throughput 0.996797
Equal recommendation: 432 MB each
Reading from 8435: heap size 308 MB, throughput 0.993205
Reading from 8434: heap size 429 MB, throughput 0.988604
Reading from 8435: heap size 307 MB, throughput 0.99837
Reading from 8434: heap size 431 MB, throughput 0.985658
Reading from 8435: heap size 308 MB, throughput 0.99817
Reading from 8434: heap size 431 MB, throughput 0.992614
Reading from 8434: heap size 434 MB, throughput 0.50614
Reading from 8434: heap size 447 MB, throughput 0.992983
Reading from 8434: heap size 450 MB, throughput 0.993079
Reading from 8435: heap size 307 MB, throughput 0.99824
Reading from 8434: heap size 452 MB, throughput 0.99597
Reading from 8435: heap size 308 MB, throughput 0.998224
Reading from 8434: heap size 452 MB, throughput 0.995778
Reading from 8434: heap size 400 MB, throughput 0.99442
Reading from 8435: heap size 307 MB, throughput 0.998113
Equal recommendation: 432 MB each
Reading from 8434: heap size 450 MB, throughput 0.991445
Reading from 8435: heap size 308 MB, throughput 0.998454
Reading from 8434: heap size 406 MB, throughput 0.992258
Reading from 8435: heap size 309 MB, throughput 0.998509
Reading from 8434: heap size 443 MB, throughput 0.991692
Reading from 8435: heap size 309 MB, throughput 0.997776
Reading from 8434: heap size 414 MB, throughput 0.989839
Client 8435 died
Clients: 1
Reading from 8434: heap size 440 MB, throughput 0.98913
Recommendation: one client; give it all the memory
Reading from 8434: heap size 420 MB, throughput 0.988829
Reading from 8434: heap size 437 MB, throughput 0.984384
Reading from 8434: heap size 438 MB, throughput 0.98253
Reading from 8434: heap size 440 MB, throughput 0.987607
Reading from 8434: heap size 443 MB, throughput 0.874511
Reading from 8434: heap size 440 MB, throughput 0.850223
Reading from 8434: heap size 446 MB, throughput 0.834629
Reading from 8434: heap size 455 MB, throughput 0.965495
Reading from 8434: heap size 458 MB, throughput 0.988894
Reading from 8434: heap size 455 MB, throughput 0.989725
Recommendation: one client; give it all the memory
Reading from 8434: heap size 459 MB, throughput 0.989285
Reading from 8434: heap size 455 MB, throughput 0.988887
Reading from 8434: heap size 459 MB, throughput 0.989519
Reading from 8434: heap size 456 MB, throughput 0.987002
Reading from 8434: heap size 458 MB, throughput 0.988167
Recommendation: one client; give it all the memory
Reading from 8434: heap size 458 MB, throughput 0.98729
Reading from 8434: heap size 459 MB, throughput 0.987036
Reading from 8434: heap size 462 MB, throughput 0.992425
Reading from 8434: heap size 462 MB, throughput 0.977036
Reading from 8434: heap size 462 MB, throughput 0.892418
Reading from 8434: heap size 464 MB, throughput 0.884001
Client 8434 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
