import economemd
import numpy as np
import scipy.optimize

def optimise(totalMem, minheaps):
    s = sum(minheaps)
    bounds = map(lambda m: (m, totalMem - (s - m)), minheaps)[:-1]

    def U(hs):
        return -economemd.combineUtilities(list(hs) + [totalMem - sum(hs)])

    res = scipy.optimize.minimize(U, np.array([0]*(len(minheaps) - 1)), method="L-BFGS-B", bounds=bounds, tol=1e-15)
    hs = list(res["x"]) + [totalMem - sum(res["x"])]
    return hs
