module Economem.DaemonBenchmarkTimer (DaemonTimerDir(..),
                                      isDaemonTimerDir,
                                      newDaemonBenchmarkTimer) where

import Prelude hiding(exp)
import Control.Applicative
import Control.Conditional hiding ((|>))
import Data.List
import qualified Economem.BenchmarkTimer as B
import Economem.Common
import Economem.Daemon
import Economem.Experiment
import Economem.Files
import Economem.Java
import qualified Economem.MetadataDir as M
import Economem.Process
import Economem.Records
import Economem.Run
import Text.Printf

name :: String
name = "timeBenchmarkDaemon"

equalName :: String
equalName = "Equal"

isDaemonTimerDir :: ExperimentDir -> IO Bool
isDaemonTimerDir d = do
  let metaDir = metadataDir d
  expName <- get (M.experimentNameFile metaDir)
  return (name `isPrefixOf` expName)


data DaemonTimerDir =
  DaemonTimerDir { dirPath :: FilePath,
                   daemonLogFile :: RunNum -> FilePath,
                   runBehaviourFile :: RunNum -> FilePath,
                   runHighestUsedMemFile :: RunNum -> IntFile,
                   behaviourFile :: FilePath,
                   alwaysNumericFile :: WholeFile Bool }

instance Path DaemonTimerDir where
  getPath = dirPath
instance Dir DaemonTimerDir where
  asDir path =
    DaemonTimerDir { dirPath = getPath path,
                     daemonLogFile = \run -> path </> (printf "daemon_run%s_log.txt" (pretty run) :: FilePath),
                     runBehaviourFile = \run -> path </> (printf "behaviour_plot_run%s.pdf" (pretty run) :: FilePath),
                     runHighestUsedMemFile = \run -> WholeFile (path </> (printf "highest_total_mem_run%s.txt" (pretty run) :: FilePath)),
                     behaviourFile = path </> "behaviour_plot.pdf",
                     alwaysNumericFile = WholeFile (path </> "always_numeric_optimisation.txt") }

  postprocessingActions = [postprocessBM,
                           postprocessDaemon]
    where
      postprocessBM :: DaemonTimerDir -> IO ()
      postprocessBM d = postprocessDir (asDir d :: B.BenchmarkTimerDir)

      postprocessDaemon :: DaemonTimerDir -> IO ()
      postprocessDaemon d = do
        let bmDir = asDir d
        let metaDir = metadataDir (asDir d)
        runs <- get (M.numRunsFile metaDir)
        pdfs <- mapM (doRun d bmDir metaDir) (upTo runs)
        mergePdfs pdfs (behaviourFile d)

      doRun :: DaemonTimerDir ->
               B.BenchmarkTimerDir ->
               M.MetadataDir ->
               RunNum ->
               IO FilePath
      doRun d bmDir metaDir run = do
        throughput <- head <$> get (B.throughputsFile bmDir)
        pids <- ifM (exists (B.pidNamesFile bmDir run))
                (return (Just (B.pidNamesFile bmDir run)))
                (return Nothing)
        forcedGrowth <- ifM (exists (B.forceGrowthFile bmDir))
                        (get (B.forceGrowthFile bmDir))
                        (return False)
        j <- get (B.javaNameFile bmDir)
        expName <- get (M.experimentNameFile metaDir)
        let title = "Run " ++ numOf run ++
                    " (" ++ j ++
                    (if forcedGrowth then ", forced growth" else "") ++
                    (if equalName `isInfixOf` expName then ", forced equal" else "") ++
                    ")"
        let inFile = daemonLogFile d run
        let outFile = runBehaviourFile d run
        let highestMemFile = getPath (runHighestUsedMemFile d run)
        plotBehaviour title throughput inFile outFile highestMemFile pids
        return outFile


newDaemonBenchmarkTimer :: Input BenchmarkThreadsIters ->
                           DataSize ->
                           ThroughputType ->
                           Integer ->
                           Java ->
                           RecommendationType ->
                           Maybe (Input BenchmarkThreadsMB) ->
                           Bool ->
                           Bool ->
                           Maybe SubexpInfo ->
                           [String] ->
                           IO Experiment
newDaemonBenchmarkTimer input maxHeap throughput runs j recomType minHeaps forceGrowth alwaysNumeric info extraVMArgs = do
  exp <- B.newBenchmarkTimer
         (name ++ if recomType == Equal then equalName else "")
         input
         maxHeap
         runs
         j
         Nothing
         20
         [createDaemon maxHeap recomType throughput alwaysNumeric]
         [throughput]
         minHeaps
         forceGrowth
         info
         extraVMArgs
  return (addToIntro
          (addStaticMetadataSaver exp (alwaysNumeric |> alwaysNumericFile (asDir (experimentDir exp))))
          ["Daemon-managed memory: " ++ pretty maxHeap])

createDaemon :: DataSize ->
                RecommendationType ->
                ThroughputType ->
                Bool ->
                Experiment ->
                RunNum ->
                ProcessNum ->
                IO ProcessLauncher
createDaemon maxHeap recomType throughput alwaysNumeric e run auxNum = do
  let daemonDir = asDir (experimentDir e)
  let d = (makeDaemon maxHeap) { recommendationType = recomType,
                                 readingRecv = Just $ throughputSocketName throughput,
                                 commandRecv = Just $ throughputSocketName throughput,
                                 logFile = Just ((daemonLogFile daemonDir) run),
                                 logReadings = True,
                                 logFunctions = True,
                                 alwaysNumericOptimisation = alwaysNumeric,
                                 verbose = True }
  daemonProcessLauncher
    d
    (M.auxOutputFile (metadataDir (experimentDir e)) run auxNum)
    (getPath daemonDir)
    (Just (M.auxResourceUsageFile (metadataDir (experimentDir e)) run auxNum))
