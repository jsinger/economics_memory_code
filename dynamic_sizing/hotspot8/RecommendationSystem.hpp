#ifndef RECOMMENDATIONSYSTEM_HPP
#define RECOMMENDATIONSYSTEM_HPP

#include "gc_implementation/shared/DaemonProxy.hpp"
#include "gc_implementation/shared/Messages.hpp"

class RecommendationSystem
{
public:
    static void init();
    static void gc_begin(const char* source);
    static void gc_end(const char* source);
    static void report_major_collection();
    static bool should_adjust_eden_size();
    static bool should_adjust_old_size();
    static unsigned long long recommendation_size_bytes();

private:
    static bool _has_recommendation;
    static economem::Recommendation _recommendation;
    static bool _collection_was_major;
    static unsigned long long _heap_size_at_begin_collection;
    static void __attribute__ ((destructor)) daemon_proxy_cleanup();
    static bool should_adjust_size(const char* name);

    static economem::DaemonProxy* _proxy;
};

#endif
