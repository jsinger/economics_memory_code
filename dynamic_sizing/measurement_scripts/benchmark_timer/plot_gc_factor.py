#!/usr/bin/env python

import argparse
import argtypes
import csv
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description="Plot the results from a gc_factor run")
parser.add_argument("inFile", type=argtypes.existing_path, help="File containing results to plot")
parser.add_argument("outFile", help="File to save graph in")
parser.add_argument("--tinyText", action="store_true")

args = parser.parse_args()

infile = args.inFile
outfile = args.outFile

data = {}

with open(infile) as f:
    reader = csv.DictReader(f)
    for line in reader:
        bm = "%s, %s, %s, %s" % (line["BENCHMARK"], line["SIZE"], line["THREADS"], line["ITERATIONS"])
        reading = (float(line["FACTOR"]), int(line["NUM_GCS"]))
        if bm in data:
            data[bm].append(reading)
        else:
            data[bm] = [reading]

for key in data:
    data[key].sort()
    (factor, gcs) = zip(*data[key])
    plt.plot(factor, gcs, label=key)

plt.xlabel("Heap size (multiples of minimum heap size)")
plt.ylabel("Number of GCs")
if args.tinyText:
    legend = plt.legend(title="Benchmark, size, threads, iterations", fontsize="x-small")
    legend.get_title().set_fontsize("x-small")
else:
    legend = plt.legend(title="Benchmark, size, threads, iterations")
plt.savefig(outfile)
