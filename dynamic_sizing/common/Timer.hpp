#ifndef TIMER_HPP
#define TIMER_HPP

#include <sys/time.h>

/*! \addtogroup common
  @{
*/

namespace economem
{
    //! Check whether a specific time interval has passed.
    class Timer
    {
    public:
        Timer(double seconds);
        void setTimer(double seconds);
        bool hasElapsed() const;
    private:
        timeval m_Target;
    };
}

/*! @} */

#endif
