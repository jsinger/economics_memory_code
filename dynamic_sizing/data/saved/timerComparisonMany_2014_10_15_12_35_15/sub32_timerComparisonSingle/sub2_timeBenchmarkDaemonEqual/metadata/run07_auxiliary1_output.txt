economemd
    total memory: 3705 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub32_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run07_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 14187: heap size 9 MB, throughput 0.989245
Clients: 1
Client 14187 has a minimum heap size of 12 MB
Reading from 14185: heap size 9 MB, throughput 0.991133
Clients: 2
Client 14185 has a minimum heap size of 1223 MB
Reading from 14185: heap size 9 MB, throughput 0.960076
Reading from 14187: heap size 9 MB, throughput 0.982729
Reading from 14185: heap size 9 MB, throughput 0.958364
Reading from 14185: heap size 9 MB, throughput 0.934019
Reading from 14187: heap size 9 MB, throughput 0.972079
Reading from 14187: heap size 9 MB, throughput 0.974473
Reading from 14185: heap size 11 MB, throughput 0.973635
Reading from 14187: heap size 11 MB, throughput 0.970688
Reading from 14185: heap size 11 MB, throughput 0.97007
Reading from 14187: heap size 11 MB, throughput 0.975717
Reading from 14185: heap size 17 MB, throughput 0.946709
Reading from 14187: heap size 16 MB, throughput 0.978145
Reading from 14185: heap size 17 MB, throughput 0.531549
Reading from 14185: heap size 29 MB, throughput 0.949798
Reading from 14185: heap size 30 MB, throughput 0.898967
Reading from 14187: heap size 16 MB, throughput 0.985287
Reading from 14185: heap size 35 MB, throughput 0.492833
Reading from 14185: heap size 47 MB, throughput 0.875786
Reading from 14187: heap size 24 MB, throughput 0.974677
Reading from 14185: heap size 49 MB, throughput 0.539596
Reading from 14185: heap size 64 MB, throughput 0.780418
Reading from 14185: heap size 70 MB, throughput 0.802619
Reading from 14187: heap size 25 MB, throughput 0.811229
Reading from 14185: heap size 70 MB, throughput 0.227159
Reading from 14185: heap size 101 MB, throughput 0.788523
Reading from 14187: heap size 40 MB, throughput 0.986526
Reading from 14185: heap size 101 MB, throughput 0.257811
Reading from 14187: heap size 41 MB, throughput 0.985415
Reading from 14185: heap size 131 MB, throughput 0.810595
Reading from 14185: heap size 131 MB, throughput 0.844043
Reading from 14187: heap size 46 MB, throughput 0.992721
Reading from 14185: heap size 133 MB, throughput 0.766154
Reading from 14187: heap size 47 MB, throughput 0.973765
Reading from 14187: heap size 53 MB, throughput 0.986948
Reading from 14185: heap size 137 MB, throughput 0.220601
Reading from 14185: heap size 168 MB, throughput 0.637985
Reading from 14187: heap size 53 MB, throughput 0.984347
Reading from 14185: heap size 172 MB, throughput 0.646602
Reading from 14185: heap size 173 MB, throughput 0.658353
Reading from 14187: heap size 61 MB, throughput 0.984675
Reading from 14187: heap size 61 MB, throughput 0.984453
Reading from 14187: heap size 67 MB, throughput 0.982443
Reading from 14185: heap size 176 MB, throughput 0.164608
Reading from 14187: heap size 68 MB, throughput 0.983707
Reading from 14185: heap size 215 MB, throughput 0.684968
Reading from 14185: heap size 219 MB, throughput 0.704107
Reading from 14185: heap size 220 MB, throughput 0.650706
Reading from 14185: heap size 227 MB, throughput 0.644161
Reading from 14185: heap size 230 MB, throughput 0.703606
Reading from 14185: heap size 236 MB, throughput 0.589239
Reading from 14187: heap size 75 MB, throughput 0.995556
Reading from 14185: heap size 241 MB, throughput 0.660045
Reading from 14185: heap size 250 MB, throughput 0.578931
Reading from 14185: heap size 254 MB, throughput 0.574727
Reading from 14187: heap size 75 MB, throughput 0.994415
Reading from 14185: heap size 266 MB, throughput 0.0844989
Reading from 14185: heap size 311 MB, throughput 0.532874
Reading from 14187: heap size 81 MB, throughput 0.991161
Reading from 14185: heap size 318 MB, throughput 0.491968
Reading from 14185: heap size 320 MB, throughput 0.512986
Reading from 14187: heap size 82 MB, throughput 0.989459
Reading from 14185: heap size 325 MB, throughput 0.101817
Reading from 14185: heap size 375 MB, throughput 0.467684
Reading from 14185: heap size 308 MB, throughput 0.720558
Reading from 14185: heap size 365 MB, throughput 0.614814
Reading from 14185: heap size 370 MB, throughput 0.571931
Reading from 14185: heap size 371 MB, throughput 0.636513
Reading from 14187: heap size 89 MB, throughput 0.995086
Reading from 14187: heap size 89 MB, throughput 0.992914
Reading from 14185: heap size 372 MB, throughput 0.0955613
Reading from 14185: heap size 428 MB, throughput 0.487697
Reading from 14185: heap size 429 MB, throughput 0.643008
Reading from 14185: heap size 429 MB, throughput 0.546281
Reading from 14187: heap size 94 MB, throughput 0.993139
Reading from 14185: heap size 429 MB, throughput 0.561035
Reading from 14185: heap size 431 MB, throughput 0.592272
Reading from 14185: heap size 437 MB, throughput 0.469031
Reading from 14185: heap size 442 MB, throughput 0.506307
Reading from 14187: heap size 94 MB, throughput 0.993152
Reading from 14185: heap size 450 MB, throughput 0.500686
Reading from 14185: heap size 455 MB, throughput 0.495609
Equal recommendation: 1852 MB each
Reading from 14187: heap size 97 MB, throughput 0.995023
Reading from 14185: heap size 462 MB, throughput 0.0848443
Reading from 14185: heap size 521 MB, throughput 0.419913
Reading from 14187: heap size 98 MB, throughput 0.994627
Reading from 14185: heap size 525 MB, throughput 0.52937
Reading from 14185: heap size 522 MB, throughput 0.559103
Reading from 14185: heap size 524 MB, throughput 0.509462
Reading from 14187: heap size 102 MB, throughput 0.99306
Reading from 14187: heap size 102 MB, throughput 0.992971
Reading from 14185: heap size 528 MB, throughput 0.0993069
Reading from 14185: heap size 586 MB, throughput 0.401952
Reading from 14185: heap size 578 MB, throughput 0.661114
Reading from 14185: heap size 584 MB, throughput 0.563749
Reading from 14187: heap size 107 MB, throughput 0.996482
Reading from 14185: heap size 585 MB, throughput 0.504828
Reading from 14185: heap size 589 MB, throughput 0.592472
Reading from 14187: heap size 107 MB, throughput 0.994692
Reading from 14185: heap size 595 MB, throughput 0.108457
Reading from 14187: heap size 110 MB, throughput 0.995769
Reading from 14185: heap size 661 MB, throughput 0.416769
Reading from 14185: heap size 671 MB, throughput 0.808341
Reading from 14187: heap size 110 MB, throughput 0.995943
Reading from 14185: heap size 672 MB, throughput 0.843683
Reading from 14187: heap size 114 MB, throughput 0.99799
Reading from 14185: heap size 669 MB, throughput 0.846192
Reading from 14187: heap size 114 MB, throughput 0.997524
Reading from 14185: heap size 682 MB, throughput 0.817237
Reading from 14185: heap size 695 MB, throughput 0.471313
Reading from 14185: heap size 706 MB, throughput 0.626129
Reading from 14187: heap size 116 MB, throughput 0.995881
Reading from 14187: heap size 116 MB, throughput 0.997076
Reading from 14185: heap size 713 MB, throughput 0.04345
Reading from 14187: heap size 119 MB, throughput 0.998096
Reading from 14185: heap size 785 MB, throughput 0.278457
Reading from 14185: heap size 774 MB, throughput 0.377879
Reading from 14185: heap size 700 MB, throughput 0.415314
Reading from 14185: heap size 772 MB, throughput 0.741746
Reading from 14187: heap size 119 MB, throughput 0.997952
Equal recommendation: 1852 MB each
Reading from 14187: heap size 120 MB, throughput 0.99782
Reading from 14185: heap size 776 MB, throughput 0.195843
Reading from 14187: heap size 121 MB, throughput 0.997174
Reading from 14185: heap size 853 MB, throughput 0.467803
Reading from 14185: heap size 855 MB, throughput 0.617915
Reading from 14185: heap size 861 MB, throughput 0.661603
Reading from 14185: heap size 861 MB, throughput 0.855287
Reading from 14185: heap size 861 MB, throughput 0.781893
Reading from 14187: heap size 122 MB, throughput 0.998319
Reading from 14187: heap size 122 MB, throughput 0.998136
Reading from 14185: heap size 864 MB, throughput 0.168785
Reading from 14185: heap size 958 MB, throughput 0.752407
Reading from 14185: heap size 961 MB, throughput 0.91528
Reading from 14187: heap size 124 MB, throughput 0.997258
Reading from 14185: heap size 970 MB, throughput 0.902091
Reading from 14185: heap size 810 MB, throughput 0.791048
Reading from 14187: heap size 124 MB, throughput 0.992232
Reading from 14187: heap size 126 MB, throughput 0.317605
Reading from 14185: heap size 953 MB, throughput 0.720499
Reading from 14185: heap size 826 MB, throughput 0.745896
Reading from 14185: heap size 943 MB, throughput 0.744271
Reading from 14187: heap size 128 MB, throughput 0.994591
Reading from 14185: heap size 832 MB, throughput 0.631675
Reading from 14185: heap size 937 MB, throughput 0.693021
Reading from 14187: heap size 135 MB, throughput 0.995787
Reading from 14185: heap size 944 MB, throughput 0.767162
Reading from 14185: heap size 932 MB, throughput 0.84381
Reading from 14185: heap size 939 MB, throughput 0.849583
Reading from 14185: heap size 928 MB, throughput 0.856879
Reading from 14187: heap size 135 MB, throughput 0.998118
Reading from 14185: heap size 934 MB, throughput 0.983909
Reading from 14187: heap size 143 MB, throughput 0.997291
Reading from 14187: heap size 144 MB, throughput 0.995551
Reading from 14185: heap size 935 MB, throughput 0.970059
Reading from 14185: heap size 936 MB, throughput 0.808654
Reading from 14185: heap size 939 MB, throughput 0.768072
Reading from 14185: heap size 941 MB, throughput 0.814177
Reading from 14185: heap size 942 MB, throughput 0.814991
Reading from 14187: heap size 151 MB, throughput 0.998189
Reading from 14185: heap size 944 MB, throughput 0.817303
Reading from 14185: heap size 945 MB, throughput 0.843003
Reading from 14185: heap size 947 MB, throughput 0.800426
Reading from 14185: heap size 949 MB, throughput 0.919233
Reading from 14187: heap size 151 MB, throughput 0.996159
Reading from 14185: heap size 951 MB, throughput 0.86712
Equal recommendation: 1852 MB each
Reading from 14185: heap size 952 MB, throughput 0.903254
Reading from 14185: heap size 955 MB, throughput 0.78507
Reading from 14185: heap size 948 MB, throughput 0.69495
Reading from 14187: heap size 156 MB, throughput 0.998032
Reading from 14187: heap size 157 MB, throughput 0.997442
Reading from 14185: heap size 962 MB, throughput 0.0731186
Reading from 14187: heap size 161 MB, throughput 0.995992
Reading from 14185: heap size 1084 MB, throughput 0.558701
Reading from 14185: heap size 1085 MB, throughput 0.695462
Reading from 14185: heap size 1087 MB, throughput 0.675134
Reading from 14185: heap size 1090 MB, throughput 0.734349
Reading from 14185: heap size 1092 MB, throughput 0.733426
Reading from 14187: heap size 162 MB, throughput 0.997667
Reading from 14185: heap size 1096 MB, throughput 0.726839
Reading from 14185: heap size 1105 MB, throughput 0.673517
Reading from 14185: heap size 1107 MB, throughput 0.711591
Reading from 14187: heap size 166 MB, throughput 0.99239
Reading from 14187: heap size 167 MB, throughput 0.997313
Reading from 14187: heap size 172 MB, throughput 0.997638
Reading from 14185: heap size 1125 MB, throughput 0.977083
Reading from 14187: heap size 172 MB, throughput 0.997727
Reading from 14187: heap size 177 MB, throughput 0.997344
Equal recommendation: 1852 MB each
Reading from 14187: heap size 177 MB, throughput 0.997913
Reading from 14185: heap size 1125 MB, throughput 0.974618
Reading from 14187: heap size 182 MB, throughput 0.998851
Reading from 14187: heap size 182 MB, throughput 0.995557
Reading from 14187: heap size 186 MB, throughput 0.994219
Reading from 14187: heap size 187 MB, throughput 0.997133
Reading from 14185: heap size 1141 MB, throughput 0.973726
Reading from 14187: heap size 193 MB, throughput 0.997829
Reading from 14187: heap size 193 MB, throughput 0.996374
Reading from 14187: heap size 199 MB, throughput 0.998314
Reading from 14185: heap size 1145 MB, throughput 0.972243
Reading from 14187: heap size 199 MB, throughput 0.997693
Equal recommendation: 1852 MB each
Reading from 14187: heap size 203 MB, throughput 0.997731
Reading from 14187: heap size 204 MB, throughput 0.99653
Reading from 14185: heap size 1158 MB, throughput 0.968372
Reading from 14187: heap size 209 MB, throughput 0.99748
Reading from 14187: heap size 209 MB, throughput 0.99748
Reading from 14185: heap size 1165 MB, throughput 0.973338
Reading from 14187: heap size 214 MB, throughput 0.997691
Reading from 14187: heap size 214 MB, throughput 0.997493
Reading from 14187: heap size 218 MB, throughput 0.997603
Reading from 14185: heap size 1168 MB, throughput 0.97998
Reading from 14187: heap size 219 MB, throughput 0.99733
Equal recommendation: 1852 MB each
Reading from 14187: heap size 224 MB, throughput 0.992466
Reading from 14187: heap size 224 MB, throughput 0.99654
Reading from 14187: heap size 232 MB, throughput 0.995939
Reading from 14185: heap size 1171 MB, throughput 0.977611
Reading from 14187: heap size 232 MB, throughput 0.99787
Reading from 14187: heap size 238 MB, throughput 0.998124
Reading from 14185: heap size 1165 MB, throughput 0.977395
Reading from 14187: heap size 239 MB, throughput 0.99779
Reading from 14187: heap size 245 MB, throughput 0.998122
Equal recommendation: 1852 MB each
Reading from 14187: heap size 245 MB, throughput 0.997773
Reading from 14185: heap size 1170 MB, throughput 0.744239
Reading from 14187: heap size 251 MB, throughput 0.998047
Reading from 14187: heap size 251 MB, throughput 0.997373
Reading from 14187: heap size 256 MB, throughput 0.998039
Reading from 14185: heap size 1231 MB, throughput 0.996494
Reading from 14187: heap size 257 MB, throughput 0.997408
Reading from 14187: heap size 262 MB, throughput 0.878251
Reading from 14185: heap size 1233 MB, throughput 0.993954
Reading from 14187: heap size 269 MB, throughput 0.997562
Equal recommendation: 1852 MB each
Reading from 14187: heap size 283 MB, throughput 0.999081
Reading from 14187: heap size 283 MB, throughput 0.998732
Reading from 14185: heap size 1243 MB, throughput 0.992726
Reading from 14187: heap size 294 MB, throughput 0.998528
Reading from 14187: heap size 295 MB, throughput 0.998655
Reading from 14185: heap size 1248 MB, throughput 0.992394
Reading from 14187: heap size 303 MB, throughput 0.998568
Equal recommendation: 1852 MB each
Reading from 14187: heap size 304 MB, throughput 0.998689
Reading from 14185: heap size 1250 MB, throughput 0.99146
Reading from 14187: heap size 311 MB, throughput 0.998838
Reading from 14187: heap size 312 MB, throughput 0.998041
Reading from 14185: heap size 1160 MB, throughput 0.982398
Reading from 14187: heap size 319 MB, throughput 0.994349
Reading from 14187: heap size 319 MB, throughput 0.998206
Reading from 14185: heap size 1241 MB, throughput 0.986488
Equal recommendation: 1852 MB each
Reading from 14187: heap size 330 MB, throughput 0.997901
Reading from 14187: heap size 331 MB, throughput 0.998354
Reading from 14185: heap size 1179 MB, throughput 0.987483
Reading from 14187: heap size 340 MB, throughput 0.998285
Reading from 14187: heap size 341 MB, throughput 0.998294
Reading from 14185: heap size 1235 MB, throughput 0.986734
Reading from 14187: heap size 349 MB, throughput 0.997378
Equal recommendation: 1852 MB each
Reading from 14187: heap size 350 MB, throughput 0.998546
Reading from 14185: heap size 1239 MB, throughput 0.982777
Reading from 14187: heap size 358 MB, throughput 0.996721
Reading from 14187: heap size 359 MB, throughput 0.997189
Reading from 14185: heap size 1240 MB, throughput 0.983925
Reading from 14187: heap size 370 MB, throughput 0.998308
Reading from 14187: heap size 370 MB, throughput 0.997857
Equal recommendation: 1852 MB each
Reading from 14185: heap size 1242 MB, throughput 0.981735
Reading from 14187: heap size 380 MB, throughput 0.998191
Reading from 14187: heap size 380 MB, throughput 0.998055
Reading from 14185: heap size 1243 MB, throughput 0.981219
Reading from 14187: heap size 389 MB, throughput 0.997692
Reading from 14185: heap size 1249 MB, throughput 0.978157
Reading from 14187: heap size 390 MB, throughput 0.997969
Equal recommendation: 1852 MB each
Reading from 14187: heap size 400 MB, throughput 0.994611
Reading from 14185: heap size 1251 MB, throughput 0.978197
Reading from 14187: heap size 400 MB, throughput 0.99813
Reading from 14187: heap size 414 MB, throughput 0.998356
Reading from 14185: heap size 1259 MB, throughput 0.977488
Reading from 14187: heap size 415 MB, throughput 0.998382
Equal recommendation: 1852 MB each
Reading from 14185: heap size 1265 MB, throughput 0.976973
Reading from 14187: heap size 425 MB, throughput 0.998037
Reading from 14187: heap size 426 MB, throughput 0.998197
Reading from 14185: heap size 1272 MB, throughput 0.975602
Reading from 14187: heap size 436 MB, throughput 0.99841
Reading from 14187: heap size 437 MB, throughput 0.995292
Reading from 14185: heap size 1278 MB, throughput 0.975054
Equal recommendation: 1852 MB each
Reading from 14187: heap size 447 MB, throughput 0.997945
Reading from 14185: heap size 1282 MB, throughput 0.977936
Reading from 14187: heap size 448 MB, throughput 0.997793
Reading from 14185: heap size 1288 MB, throughput 0.979663
Reading from 14187: heap size 460 MB, throughput 0.996687
Equal recommendation: 1852 MB each
Reading from 14187: heap size 461 MB, throughput 0.99833
Reading from 14185: heap size 1289 MB, throughput 0.9762
Reading from 14187: heap size 474 MB, throughput 0.997817
Reading from 14187: heap size 475 MB, throughput 0.994916
Reading from 14185: heap size 1294 MB, throughput 0.977513
Reading from 14187: heap size 488 MB, throughput 0.975669
Equal recommendation: 1852 MB each
Reading from 14185: heap size 1294 MB, throughput 0.978924
Reading from 14187: heap size 492 MB, throughput 0.999272
Reading from 14185: heap size 1296 MB, throughput 0.977168
Reading from 14187: heap size 509 MB, throughput 0.999174
Reading from 14185: heap size 1297 MB, throughput 0.977351
Reading from 14187: heap size 509 MB, throughput 0.999272
Equal recommendation: 1852 MB each
Reading from 14187: heap size 521 MB, throughput 0.998903
Reading from 14185: heap size 1297 MB, throughput 0.977184
Reading from 14187: heap size 523 MB, throughput 0.99728
Reading from 14187: heap size 536 MB, throughput 0.996306
Reading from 14185: heap size 1299 MB, throughput 0.668529
Equal recommendation: 1852 MB each
Reading from 14187: heap size 536 MB, throughput 0.998837
Reading from 14185: heap size 1414 MB, throughput 0.973065
Reading from 14187: heap size 552 MB, throughput 0.998412
Reading from 14185: heap size 1416 MB, throughput 0.990194
Reading from 14187: heap size 554 MB, throughput 0.998526
Equal recommendation: 1852 MB each
Reading from 14187: heap size 570 MB, throughput 0.997117
Reading from 14185: heap size 1426 MB, throughput 0.989713
Reading from 14187: heap size 571 MB, throughput 0.998565
Reading from 14185: heap size 1430 MB, throughput 0.988628
Reading from 14187: heap size 588 MB, throughput 0.998467
Equal recommendation: 1852 MB each
Reading from 14187: heap size 590 MB, throughput 0.998588
Reading from 14187: heap size 605 MB, throughput 0.998624
Reading from 14187: heap size 606 MB, throughput 0.997352
Equal recommendation: 1852 MB each
Reading from 14185: heap size 1431 MB, throughput 0.991936
Reading from 14187: heap size 623 MB, throughput 0.998604
Reading from 14187: heap size 623 MB, throughput 0.998343
Equal recommendation: 1852 MB each
Reading from 14187: heap size 640 MB, throughput 0.998541
Reading from 14187: heap size 641 MB, throughput 0.996569
Reading from 14185: heap size 1433 MB, throughput 0.992428
Reading from 14187: heap size 658 MB, throughput 0.998615
Equal recommendation: 1852 MB each
Reading from 14187: heap size 659 MB, throughput 0.998532
Reading from 14185: heap size 1436 MB, throughput 0.986301
Reading from 14185: heap size 1451 MB, throughput 0.85402
Reading from 14185: heap size 1457 MB, throughput 0.757747
Reading from 14185: heap size 1474 MB, throughput 0.667194
Reading from 14185: heap size 1503 MB, throughput 0.692542
Reading from 14185: heap size 1529 MB, throughput 0.683289
Reading from 14185: heap size 1561 MB, throughput 0.716149
Reading from 14185: heap size 1576 MB, throughput 0.706062
Reading from 14187: heap size 676 MB, throughput 0.998517
Reading from 14185: heap size 1610 MB, throughput 0.949048
Reading from 14187: heap size 678 MB, throughput 0.997413
Equal recommendation: 1852 MB each
Reading from 14185: heap size 1618 MB, throughput 0.9832
Reading from 14187: heap size 696 MB, throughput 0.998483
Reading from 14185: heap size 1599 MB, throughput 0.980954
Reading from 14187: heap size 697 MB, throughput 0.99851
Equal recommendation: 1852 MB each
Reading from 14185: heap size 1616 MB, throughput 0.979144
Reading from 14187: heap size 716 MB, throughput 0.998569
Reading from 14185: heap size 1594 MB, throughput 0.984453
Client 14187 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 14185: heap size 1611 MB, throughput 0.981517
Reading from 14185: heap size 1601 MB, throughput 0.856856
Recommendation: one client; give it all the memory
Reading from 14185: heap size 1583 MB, throughput 0.996074
Reading from 14185: heap size 1589 MB, throughput 0.994583
Recommendation: one client; give it all the memory
Reading from 14185: heap size 1599 MB, throughput 0.993445
Reading from 14185: heap size 1607 MB, throughput 0.99188
Recommendation: one client; give it all the memory
Reading from 14185: heap size 1611 MB, throughput 0.990992
Reading from 14185: heap size 1593 MB, throughput 0.989753
Recommendation: one client; give it all the memory
Reading from 14185: heap size 1383 MB, throughput 0.989125
Reading from 14185: heap size 1572 MB, throughput 0.988812
Recommendation: one client; give it all the memory
Reading from 14185: heap size 1411 MB, throughput 0.987489
Reading from 14185: heap size 1547 MB, throughput 0.986052
Recommendation: one client; give it all the memory
Reading from 14185: heap size 1439 MB, throughput 0.984782
Recommendation: one client; give it all the memory
Reading from 14185: heap size 1550 MB, throughput 0.985068
Reading from 14185: heap size 1555 MB, throughput 0.982832
Recommendation: one client; give it all the memory
Reading from 14185: heap size 1559 MB, throughput 0.983108
Reading from 14185: heap size 1561 MB, throughput 0.980752
Recommendation: one client; give it all the memory
Reading from 14185: heap size 1567 MB, throughput 0.980589
Reading from 14185: heap size 1574 MB, throughput 0.981246
Recommendation: one client; give it all the memory
Reading from 14185: heap size 1584 MB, throughput 0.980371
Recommendation: one client; give it all the memory
Reading from 14185: heap size 1589 MB, throughput 0.979926
Reading from 14185: heap size 1598 MB, throughput 0.979955
Recommendation: one client; give it all the memory
Reading from 14185: heap size 1600 MB, throughput 0.980557
Reading from 14185: heap size 1610 MB, throughput 0.980855
Recommendation: one client; give it all the memory
Reading from 14185: heap size 1610 MB, throughput 0.980637
Recommendation: one client; give it all the memory
Reading from 14185: heap size 1619 MB, throughput 0.989681
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 14185: heap size 1619 MB, throughput 0.991099
Reading from 14185: heap size 1624 MB, throughput 0.983663
Reading from 14185: heap size 1633 MB, throughput 0.811524
Reading from 14185: heap size 1614 MB, throughput 0.780254
Reading from 14185: heap size 1639 MB, throughput 0.769331
Reading from 14185: heap size 1677 MB, throughput 0.805523
Client 14185 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
Recommendation: no clients; no recommendation
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
