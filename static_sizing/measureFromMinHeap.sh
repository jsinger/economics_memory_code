#!/bin/bash
# Generate a measure config file for all the benchmarks in a min heap file

function usage()
{
    echo "Usage: $(basename "${0}") max inc time min_heap_file"
    exit 1
}

test -z "${1}" && usage
test -z "${2}" && usage
test -z "${3}" && usage
test -z "${4}" && usage
test ! -f "${4}" && usage

MAX="${1}"
INC="${2}"
TIME="${3}"
MIN_HEAP_FILE="$(readlink -f "${4}")"

MAX_HEAP=$(( MAX - INC ))

while read name size threads min_heap; do
    echo "${name} ${size} ${threads} ${TIME} ${min_heap} ${MAX_HEAP} ${INC}"
done < "${MIN_HEAP_FILE}"
