	Command being timed: "cgexec -g memory:economem /home/callum/economics_memory_code/dynamic_sizing/hotspot8/openjdk8/jdk8/build/linux-x86_64-normal-server-release/jdk/bin/java -XX:+EconomemVengerovThroughput -XX:EconomemMinHeap=30 -Xms10m -Xmx2506m -XX:+DisableExplicitGC -XX:+UseAdaptiveSizePolicyWithSystemGC -jar /home/callum/economics_memory_code/dynamic_sizing/haskell_common/dacapo-9.12-bach.jar --no-pre-iteration-gc --scratch-directory /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_09_25_14_54_37/sub26_timerComparisonSingle/sub3_timeBenchmarkDaemon/scratch2 -t 1 -n 90 -s large pmd"
	User time (seconds): 339.45
	System time (seconds): 27.76
	Percent of CPU this job got: 141%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 4:20.00
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 710892
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 2
	Minor (reclaiming a frame) page faults: 158782
	Voluntary context switches: 1202093
	Involuntary context switches: 118921
	Swaps: 0
	File system inputs: 17024
	File system outputs: 43872
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
