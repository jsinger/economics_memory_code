#!/usr/bin/env python

"""Plot readings as a batch, and save to a file"""

import argparse
import argtypes as h
import matplotlib.pyplot as plt
import plotter

class BatchPlotter(plotter.Plotter):
    def __init__(self, throughput_model, input_file, output_file, title, pid_names_file, loose_y):
        super(BatchPlotter, self).__init__(throughput_model, input_file, title, pid_names_file, False)
        self.output_file = output_file
        self.loose_y = loose_y

    def end(self):
        self.scatter = self.axes.scatter(self.heaps, self.throughputs, zorder=0)

        for func in self.curves:
            result = self.curves[func].draw(self.axes, self.heaps)
            self.update_max_y(result)

        self.axes.relim()
        self.axes.set_xlim(0, self.max_x * 1.1)
        self.axes.set_ylim(max(self.min_y - 0.02, 0) if self.loose_y else 0, self.max_y)

        if self.curves:
            self.axes.legend(loc="lower right", fontsize="medium", frameon=False)

        plt.savefig(self.output_file)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Plot readings for a single VM as a batch, and save to a file")
    parser.add_argument("--title", default=None, help="Override the auto-generated title")
    parser.add_argument("--pidNamesFile", type=h.existing_absolute_path, default=None, help="The file containing the mapping of PIDs to names")
    parser.add_argument("--looseY", action="store_true", help="Scale the y axis based on the data rather than forcing it to min 0")
    parser.add_argument("throughput_model", help="The name of the throughput model used to generate the readings")
    parser.add_argument("input_file", type=h.existing_absolute_path, help="File containing the readings to plot")
    parser.add_argument("outFile", help="The file to save to")

    args = parser.parse_args()

    with open(args.input_file) as f:
        BatchPlotter(args.throughput_model, f, args.outFile, args.title, args.pidNamesFile, args.looseY).plot()
