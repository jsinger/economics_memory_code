economemd
    total memory: 2208 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub22_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 27574: heap size 9 MB, throughput 0.991015
Clients: 1
Client 27574 has a minimum heap size of 276 MB
Reading from 27575: heap size 9 MB, throughput 0.976341
Clients: 2
Client 27575 has a minimum heap size of 276 MB
Reading from 27574: heap size 9 MB, throughput 0.985024
Reading from 27575: heap size 9 MB, throughput 0.964046
Reading from 27574: heap size 9 MB, throughput 0.977921
Reading from 27574: heap size 9 MB, throughput 0.968586
Reading from 27575: heap size 11 MB, throughput 0.963359
Reading from 27574: heap size 11 MB, throughput 0.979695
Reading from 27575: heap size 11 MB, throughput 0.975195
Reading from 27574: heap size 11 MB, throughput 0.976697
Reading from 27575: heap size 15 MB, throughput 0.90325
Reading from 27575: heap size 18 MB, throughput 0.88529
Reading from 27574: heap size 17 MB, throughput 0.968723
Reading from 27575: heap size 24 MB, throughput 0.746828
Reading from 27574: heap size 17 MB, throughput 0.905156
Reading from 27574: heap size 30 MB, throughput 0.873165
Reading from 27575: heap size 28 MB, throughput 0.710498
Reading from 27574: heap size 31 MB, throughput 0.529082
Reading from 27575: heap size 41 MB, throughput 0.775102
Reading from 27575: heap size 42 MB, throughput 0.653177
Reading from 27575: heap size 45 MB, throughput 0.23071
Reading from 27574: heap size 34 MB, throughput 0.646306
Reading from 27574: heap size 46 MB, throughput 0.438884
Reading from 27574: heap size 47 MB, throughput 0.438011
Reading from 27575: heap size 47 MB, throughput 0.660735
Reading from 27575: heap size 64 MB, throughput 0.842166
Reading from 27575: heap size 67 MB, throughput 0.759505
Reading from 27574: heap size 50 MB, throughput 0.587066
Reading from 27575: heap size 70 MB, throughput 0.748901
Reading from 27574: heap size 75 MB, throughput 0.535882
Reading from 27575: heap size 73 MB, throughput 0.428555
Reading from 27574: heap size 75 MB, throughput 0.308227
Reading from 27575: heap size 99 MB, throughput 0.749667
Reading from 27574: heap size 103 MB, throughput 0.628242
Reading from 27575: heap size 101 MB, throughput 0.676527
Reading from 27574: heap size 103 MB, throughput 0.65469
Reading from 27574: heap size 104 MB, throughput 0.467158
Reading from 27575: heap size 104 MB, throughput 0.38953
Reading from 27575: heap size 132 MB, throughput 0.691725
Reading from 27574: heap size 107 MB, throughput 0.523408
Reading from 27575: heap size 136 MB, throughput 0.728567
Reading from 27575: heap size 140 MB, throughput 0.71187
Reading from 27574: heap size 143 MB, throughput 0.582969
Reading from 27575: heap size 144 MB, throughput 0.670305
Reading from 27574: heap size 143 MB, throughput 0.483576
Reading from 27575: heap size 150 MB, throughput 0.698708
Reading from 27574: heap size 145 MB, throughput 0.203291
Reading from 27575: heap size 155 MB, throughput 0.676622
Reading from 27574: heap size 150 MB, throughput 0.236068
Reading from 27574: heap size 153 MB, throughput 0.452369
Reading from 27574: heap size 189 MB, throughput 0.719188
Reading from 27575: heap size 160 MB, throughput 0.412945
Reading from 27574: heap size 197 MB, throughput 0.0181803
Reading from 27575: heap size 196 MB, throughput 0.63404
Reading from 27575: heap size 202 MB, throughput 0.635503
Reading from 27574: heap size 199 MB, throughput 0.733722
Reading from 27575: heap size 206 MB, throughput 0.427754
Reading from 27574: heap size 201 MB, throughput 0.619037
Reading from 27574: heap size 247 MB, throughput 0.842571
Reading from 27575: heap size 244 MB, throughput 0.900416
Reading from 27574: heap size 261 MB, throughput 0.836134
Reading from 27575: heap size 250 MB, throughput 0.900102
Reading from 27574: heap size 261 MB, throughput 0.841274
Reading from 27575: heap size 252 MB, throughput 0.856581
Reading from 27574: heap size 265 MB, throughput 0.829003
Reading from 27575: heap size 257 MB, throughput 0.795301
Reading from 27574: heap size 266 MB, throughput 0.806969
Reading from 27575: heap size 258 MB, throughput 0.731728
Reading from 27574: heap size 270 MB, throughput 0.834107
Reading from 27575: heap size 264 MB, throughput 0.78518
Reading from 27574: heap size 271 MB, throughput 0.803322
Reading from 27575: heap size 265 MB, throughput 0.789846
Reading from 27574: heap size 276 MB, throughput 0.800121
Reading from 27574: heap size 280 MB, throughput 0.778866
Reading from 27575: heap size 270 MB, throughput 0.806262
Reading from 27574: heap size 287 MB, throughput 0.780251
Reading from 27575: heap size 271 MB, throughput 0.740194
Reading from 27575: heap size 278 MB, throughput 0.721206
Reading from 27575: heap size 280 MB, throughput 0.702242
Reading from 27575: heap size 280 MB, throughput 0.711758
Reading from 27574: heap size 288 MB, throughput 0.908012
Reading from 27574: heap size 293 MB, throughput 0.860925
Reading from 27574: heap size 296 MB, throughput 0.785822
Reading from 27574: heap size 302 MB, throughput 0.737673
Reading from 27575: heap size 282 MB, throughput 0.669715
Reading from 27575: heap size 321 MB, throughput 0.829879
Reading from 27574: heap size 303 MB, throughput 0.66235
Reading from 27575: heap size 323 MB, throughput 0.79971
Reading from 27575: heap size 328 MB, throughput 0.783726
Reading from 27575: heap size 328 MB, throughput 0.806
Reading from 27574: heap size 352 MB, throughput 0.813721
Reading from 27574: heap size 352 MB, throughput 0.8021
Reading from 27575: heap size 333 MB, throughput 0.871137
Reading from 27574: heap size 354 MB, throughput 0.752514
Reading from 27574: heap size 357 MB, throughput 0.730799
Reading from 27575: heap size 334 MB, throughput 0.867647
Reading from 27574: heap size 360 MB, throughput 0.744149
Reading from 27575: heap size 338 MB, throughput 0.854821
Reading from 27574: heap size 362 MB, throughput 0.747491
Reading from 27575: heap size 339 MB, throughput 0.81374
Reading from 27575: heap size 335 MB, throughput 0.726241
Reading from 27575: heap size 338 MB, throughput 0.656493
Reading from 27575: heap size 340 MB, throughput 0.685634
Reading from 27574: heap size 368 MB, throughput 0.898085
Numeric result:
Recommendation: 2 clients, utility 0.433078:
    h1: 1322.92 MB (U(h) = 0.603459*h^0.00160978)
    h2: 885.08 MB (U(h) = 0.70424*h^0.00107654)
Recommendation: 2 clients, utility 0.433078:
    h1: 1323.15 MB (U(h) = 0.603459*h^0.00160978)
    h2: 884.854 MB (U(h) = 0.70424*h^0.00107654)
Reading from 27575: heap size 342 MB, throughput 0.946524
Reading from 27574: heap size 369 MB, throughput 0.972139
Reading from 27575: heap size 348 MB, throughput 0.965834
Reading from 27574: heap size 374 MB, throughput 0.975126
Reading from 27575: heap size 349 MB, throughput 0.967726
Reading from 27574: heap size 376 MB, throughput 0.977222
Reading from 27575: heap size 349 MB, throughput 0.97332
Reading from 27574: heap size 376 MB, throughput 0.974481
Reading from 27575: heap size 352 MB, throughput 0.976766
Reading from 27574: heap size 379 MB, throughput 0.975839
Reading from 27575: heap size 352 MB, throughput 0.975892
Reading from 27574: heap size 378 MB, throughput 0.975245
Numeric result:
Recommendation: 2 clients, utility 0.535299:
    h1: 1471.38 MB (U(h) = 0.506799*h^0.0462566)
    h2: 736.616 MB (U(h) = 0.6469*h^0.0231563)
Recommendation: 2 clients, utility 0.535299:
    h1: 1471.41 MB (U(h) = 0.506799*h^0.0462566)
    h2: 736.594 MB (U(h) = 0.6469*h^0.0231563)
Reading from 27575: heap size 354 MB, throughput 0.976314
Reading from 27574: heap size 381 MB, throughput 0.975374
Reading from 27575: heap size 351 MB, throughput 0.973525
Reading from 27574: heap size 384 MB, throughput 0.97402
Reading from 27575: heap size 354 MB, throughput 0.973088
Reading from 27574: heap size 384 MB, throughput 0.973803
Reading from 27575: heap size 356 MB, throughput 0.971644
Reading from 27574: heap size 387 MB, throughput 0.977726
Reading from 27575: heap size 356 MB, throughput 0.973705
Reading from 27574: heap size 389 MB, throughput 0.973915
Reading from 27575: heap size 359 MB, throughput 0.972876
Reading from 27574: heap size 392 MB, throughput 0.974423
Reading from 27575: heap size 360 MB, throughput 0.967771
Numeric result:
Recommendation: 2 clients, utility 0.629208:
    h1: 1405.17 MB (U(h) = 0.448284*h^0.0773346)
    h2: 802.829 MB (U(h) = 0.596314*h^0.0441838)
Recommendation: 2 clients, utility 0.629208:
    h1: 1405.18 MB (U(h) = 0.448284*h^0.0773346)
    h2: 802.824 MB (U(h) = 0.596314*h^0.0441838)
Reading from 27574: heap size 394 MB, throughput 0.97964
Reading from 27574: heap size 397 MB, throughput 0.950004
Reading from 27574: heap size 398 MB, throughput 0.923162
Reading from 27574: heap size 406 MB, throughput 0.903654
Reading from 27574: heap size 410 MB, throughput 0.87662
Reading from 27575: heap size 362 MB, throughput 0.940104
Reading from 27575: heap size 404 MB, throughput 0.966902
Reading from 27575: heap size 403 MB, throughput 0.960883
Reading from 27575: heap size 404 MB, throughput 0.950505
Reading from 27575: heap size 407 MB, throughput 0.94387
Reading from 27574: heap size 421 MB, throughput 0.970348
Reading from 27575: heap size 407 MB, throughput 0.982442
Reading from 27574: heap size 422 MB, throughput 0.9798
Reading from 27575: heap size 409 MB, throughput 0.989928
Reading from 27574: heap size 425 MB, throughput 0.98496
Reading from 27575: heap size 411 MB, throughput 0.991151
Reading from 27574: heap size 427 MB, throughput 0.985138
Reading from 27575: heap size 413 MB, throughput 0.990092
Numeric result:
Recommendation: 2 clients, utility 0.745723:
    h1: 1386.57 MB (U(h) = 0.390087*h^0.111811)
    h2: 821.425 MB (U(h) = 0.54582*h^0.0662406)
Recommendation: 2 clients, utility 0.745723:
    h1: 1386.56 MB (U(h) = 0.390087*h^0.111811)
    h2: 821.443 MB (U(h) = 0.54582*h^0.0662406)
Reading from 27574: heap size 426 MB, throughput 0.984906
Reading from 27575: heap size 415 MB, throughput 0.987786
Reading from 27574: heap size 429 MB, throughput 0.985648
Reading from 27575: heap size 413 MB, throughput 0.989038
Reading from 27574: heap size 426 MB, throughput 0.982044
Reading from 27575: heap size 415 MB, throughput 0.987359
Reading from 27574: heap size 429 MB, throughput 0.981209
Reading from 27575: heap size 414 MB, throughput 0.986195
Reading from 27574: heap size 427 MB, throughput 0.979778
Reading from 27575: heap size 415 MB, throughput 0.984235
Numeric result:
Recommendation: 2 clients, utility 0.772176:
    h1: 1376.15 MB (U(h) = 0.379719*h^0.118392)
    h2: 831.853 MB (U(h) = 0.534178*h^0.0715659)
Recommendation: 2 clients, utility 0.772176:
    h1: 1376.14 MB (U(h) = 0.379719*h^0.118392)
    h2: 831.856 MB (U(h) = 0.534178*h^0.0715659)
Reading from 27574: heap size 429 MB, throughput 0.979747
Reading from 27575: heap size 417 MB, throughput 0.983201
Reading from 27574: heap size 429 MB, throughput 0.979739
Reading from 27574: heap size 432 MB, throughput 0.970792
Reading from 27574: heap size 435 MB, throughput 0.955859
Reading from 27574: heap size 435 MB, throughput 0.951257
Reading from 27575: heap size 418 MB, throughput 0.986654
Reading from 27575: heap size 418 MB, throughput 0.981696
Reading from 27574: heap size 440 MB, throughput 0.977137
Reading from 27575: heap size 421 MB, throughput 0.951154
Reading from 27575: heap size 424 MB, throughput 0.935915
Reading from 27575: heap size 428 MB, throughput 0.918249
Reading from 27574: heap size 441 MB, throughput 0.984802
Reading from 27575: heap size 439 MB, throughput 0.982293
Reading from 27574: heap size 444 MB, throughput 0.983062
Reading from 27575: heap size 440 MB, throughput 0.983977
Numeric result:
Recommendation: 2 clients, utility 0.83322:
    h1: 1345.21 MB (U(h) = 0.359164*h^0.131857)
    h2: 862.786 MB (U(h) = 0.506558*h^0.084568)
Recommendation: 2 clients, utility 0.83322:
    h1: 1345.23 MB (U(h) = 0.359164*h^0.131857)
    h2: 862.775 MB (U(h) = 0.506558*h^0.084568)
Reading from 27575: heap size 441 MB, throughput 0.986186
Reading from 27574: heap size 445 MB, throughput 0.985959
Reading from 27575: heap size 444 MB, throughput 0.986473
Reading from 27574: heap size 443 MB, throughput 0.985713
Reading from 27575: heap size 443 MB, throughput 0.986798
Reading from 27574: heap size 446 MB, throughput 0.985334
Reading from 27575: heap size 446 MB, throughput 0.985634
Reading from 27574: heap size 443 MB, throughput 0.985298
Reading from 27575: heap size 446 MB, throughput 0.98474
Reading from 27574: heap size 445 MB, throughput 0.984431
Numeric result:
Recommendation: 2 clients, utility 0.867056:
    h1: 1331.78 MB (U(h) = 0.348678*h^0.138987)
    h2: 876.221 MB (U(h) = 0.492365*h^0.0914427)
Recommendation: 2 clients, utility 0.867056:
    h1: 1331.79 MB (U(h) = 0.348678*h^0.138987)
    h2: 876.213 MB (U(h) = 0.492365*h^0.0914427)
Reading from 27575: heap size 448 MB, throughput 0.984577
Reading from 27574: heap size 447 MB, throughput 0.986243
Reading from 27574: heap size 447 MB, throughput 0.97894
Reading from 27574: heap size 448 MB, throughput 0.966565
Reading from 27574: heap size 449 MB, throughput 0.963576
Reading from 27575: heap size 450 MB, throughput 0.988414
Reading from 27575: heap size 451 MB, throughput 0.984254
Reading from 27575: heap size 452 MB, throughput 0.973127
Reading from 27575: heap size 454 MB, throughput 0.957013
Reading from 27574: heap size 457 MB, throughput 0.987196
Reading from 27575: heap size 461 MB, throughput 0.982202
Reading from 27574: heap size 457 MB, throughput 0.989973
Reading from 27575: heap size 461 MB, throughput 0.987924
Reading from 27574: heap size 459 MB, throughput 0.988769
Numeric result:
Recommendation: 2 clients, utility 0.911213:
    h1: 1323.6 MB (U(h) = 0.334867*h^0.148646)
    h2: 884.399 MB (U(h) = 0.476484*h^0.099321)
Recommendation: 2 clients, utility 0.911213:
    h1: 1323.6 MB (U(h) = 0.334867*h^0.148646)
    h2: 884.397 MB (U(h) = 0.476484*h^0.099321)
Reading from 27575: heap size 467 MB, throughput 0.989034
Reading from 27574: heap size 461 MB, throughput 0.989493
Reading from 27575: heap size 468 MB, throughput 0.988982
Reading from 27574: heap size 458 MB, throughput 0.988769
Reading from 27575: heap size 467 MB, throughput 0.987488
Reading from 27574: heap size 461 MB, throughput 0.987276
Reading from 27575: heap size 470 MB, throughput 0.987217
Numeric result:
Recommendation: 2 clients, utility 0.929536:
    h1: 1317.38 MB (U(h) = 0.329851*h^0.152227)
    h2: 890.62 MB (U(h) = 0.469328*h^0.102915)
Recommendation: 2 clients, utility 0.929536:
    h1: 1317.38 MB (U(h) = 0.329851*h^0.152227)
    h2: 890.623 MB (U(h) = 0.469328*h^0.102915)
Reading from 27574: heap size 463 MB, throughput 0.987321
Reading from 27575: heap size 469 MB, throughput 0.986867
Reading from 27574: heap size 463 MB, throughput 0.989521
Reading from 27574: heap size 464 MB, throughput 0.982752
Reading from 27574: heap size 465 MB, throughput 0.962853
Reading from 27575: heap size 471 MB, throughput 0.982991
Reading from 27574: heap size 471 MB, throughput 0.974806
Reading from 27575: heap size 474 MB, throughput 0.985757
Reading from 27575: heap size 474 MB, throughput 0.979733
Reading from 27575: heap size 477 MB, throughput 0.967307
Reading from 27574: heap size 472 MB, throughput 0.985854
Reading from 27575: heap size 478 MB, throughput 0.986087
Reading from 27574: heap size 477 MB, throughput 0.989522
Numeric result:
Recommendation: 2 clients, utility 0.965779:
    h1: 1322.99 MB (U(h) = 0.317715*h^0.16105)
    h2: 885.008 MB (U(h) = 0.459868*h^0.107732)
Recommendation: 2 clients, utility 0.965779:
    h1: 1323 MB (U(h) = 0.317715*h^0.16105)
    h2: 885.002 MB (U(h) = 0.459868*h^0.107732)
Reading from 27575: heap size 485 MB, throughput 0.989475
Reading from 27574: heap size 478 MB, throughput 0.989601
Reading from 27575: heap size 486 MB, throughput 0.990011
Reading from 27574: heap size 478 MB, throughput 0.989789
Reading from 27575: heap size 487 MB, throughput 0.989861
Reading from 27574: heap size 480 MB, throughput 0.988378
Reading from 27575: heap size 489 MB, throughput 0.990656
Numeric result:
Recommendation: 2 clients, utility 0.982251:
    h1: 1314.39 MB (U(h) = 0.314048*h^0.163756)
    h2: 893.613 MB (U(h) = 0.452855*h^0.111332)
Recommendation: 2 clients, utility 0.982251:
    h1: 1314.39 MB (U(h) = 0.314048*h^0.163756)
    h2: 893.612 MB (U(h) = 0.452855*h^0.111332)
Reading from 27574: heap size 482 MB, throughput 0.988223
Reading from 27575: heap size 489 MB, throughput 0.990734
Reading from 27574: heap size 482 MB, throughput 0.990922
Reading from 27574: heap size 485 MB, throughput 0.985021
Reading from 27574: heap size 485 MB, throughput 0.974381
Reading from 27575: heap size 490 MB, throughput 0.985912
Reading from 27574: heap size 491 MB, throughput 0.981551
Reading from 27575: heap size 493 MB, throughput 0.987613
Reading from 27575: heap size 493 MB, throughput 0.981334
Reading from 27575: heap size 493 MB, throughput 0.969025
Reading from 27574: heap size 492 MB, throughput 0.990415
Reading from 27575: heap size 495 MB, throughput 0.986848
Numeric result:
Recommendation: 2 clients, utility 1.00122:
    h1: 1319.13 MB (U(h) = 0.307669*h^0.168504)
    h2: 888.869 MB (U(h) = 0.448581*h^0.113544)
Recommendation: 2 clients, utility 1.00122:
    h1: 1319.12 MB (U(h) = 0.307669*h^0.168504)
    h2: 888.877 MB (U(h) = 0.448581*h^0.113544)
Reading from 27574: heap size 495 MB, throughput 0.991638
Reading from 27575: heap size 501 MB, throughput 0.988382
Reading from 27574: heap size 497 MB, throughput 0.991084
Reading from 27575: heap size 502 MB, throughput 0.989889
Reading from 27574: heap size 495 MB, throughput 0.990502
Reading from 27575: heap size 503 MB, throughput 0.990222
Reading from 27574: heap size 497 MB, throughput 0.989334
Numeric result:
Recommendation: 2 clients, utility 1.0391:
    h1: 1218.09 MB (U(h) = 0.304681*h^0.170745)
    h2: 989.909 MB (U(h) = 0.389185*h^0.138801)
Recommendation: 2 clients, utility 1.0391:
    h1: 1217.93 MB (U(h) = 0.304681*h^0.170745)
    h2: 990.073 MB (U(h) = 0.389185*h^0.138801)
Reading from 27575: heap size 505 MB, throughput 0.990141
Reading from 27574: heap size 499 MB, throughput 0.989894
Reading from 27574: heap size 499 MB, throughput 0.986951
Reading from 27574: heap size 504 MB, throughput 0.979887
Reading from 27575: heap size 507 MB, throughput 0.988542
Reading from 27574: heap size 506 MB, throughput 0.981651
Reading from 27575: heap size 508 MB, throughput 0.989987
Reading from 27575: heap size 510 MB, throughput 0.985499
Reading from 27575: heap size 511 MB, throughput 0.972611
Reading from 27574: heap size 512 MB, throughput 0.990558
Numeric result:
Recommendation: 2 clients, utility 1.24817:
    h1: 859.478 MB (U(h) = 0.299556*h^0.17461)
    h2: 1348.52 MB (U(h) = 0.177817*h^0.273966)
Recommendation: 2 clients, utility 1.24817:
    h1: 859.471 MB (U(h) = 0.299556*h^0.17461)
    h2: 1348.53 MB (U(h) = 0.177817*h^0.273966)
Reading from 27575: heap size 517 MB, throughput 0.989843
Reading from 27574: heap size 512 MB, throughput 0.99149
Reading from 27575: heap size 519 MB, throughput 0.992477
Reading from 27574: heap size 512 MB, throughput 0.991151
Reading from 27575: heap size 523 MB, throughput 0.9933
Reading from 27574: heap size 514 MB, throughput 0.99099
Reading from 27575: heap size 524 MB, throughput 0.991656
Numeric result:
Recommendation: 2 clients, utility 1.28078:
    h1: 829.515 MB (U(h) = 0.298308*h^0.175554)
    h2: 1378.49 MB (U(h) = 0.160162*h^0.291724)
Recommendation: 2 clients, utility 1.28078:
    h1: 829.535 MB (U(h) = 0.298308*h^0.175554)
    h2: 1378.46 MB (U(h) = 0.160162*h^0.291724)
Reading from 27574: heap size 512 MB, throughput 0.989907
Reading from 27575: heap size 524 MB, throughput 0.991028
Reading from 27574: heap size 514 MB, throughput 0.98927
Reading from 27574: heap size 516 MB, throughput 0.985644
Reading from 27574: heap size 516 MB, throughput 0.974961
Reading from 27575: heap size 526 MB, throughput 0.989119
Reading from 27574: heap size 521 MB, throughput 0.98571
Reading from 27575: heap size 529 MB, throughput 0.987533
Reading from 27575: heap size 530 MB, throughput 0.982538
Reading from 27574: heap size 522 MB, throughput 0.989442
Reading from 27575: heap size 535 MB, throughput 0.985199
Numeric result:
Recommendation: 2 clients, utility 1.42847:
    h1: 715.209 MB (U(h) = 0.295044*h^0.178032)
    h2: 1492.79 MB (U(h) = 0.099387*h^0.371606)
Recommendation: 2 clients, utility 1.42847:
    h1: 715.187 MB (U(h) = 0.295044*h^0.178032)
    h2: 1492.81 MB (U(h) = 0.099387*h^0.371606)
Reading from 27575: heap size 536 MB, throughput 0.986914
Reading from 27574: heap size 525 MB, throughput 0.979301
Reading from 27575: heap size 540 MB, throughput 0.989781
Reading from 27574: heap size 550 MB, throughput 0.994258
Reading from 27575: heap size 541 MB, throughput 0.990228
Numeric result:
Recommendation: 2 clients, utility 1.50877:
    h1: 812.013 MB (U(h) = 0.225667*h^0.224217)
    h2: 1395.99 MB (U(h) = 0.0913032*h^0.385482)
Recommendation: 2 clients, utility 1.50877:
    h1: 811.992 MB (U(h) = 0.225667*h^0.224217)
    h2: 1396.01 MB (U(h) = 0.0913032*h^0.385482)
Reading from 27574: heap size 550 MB, throughput 0.995039
Reading from 27575: heap size 539 MB, throughput 0.990587
Reading from 27574: heap size 553 MB, throughput 0.992693
Reading from 27574: heap size 553 MB, throughput 0.99066
Reading from 27574: heap size 555 MB, throughput 0.981359
Reading from 27575: heap size 542 MB, throughput 0.989898
Reading from 27574: heap size 559 MB, throughput 0.983056
Reading from 27575: heap size 544 MB, throughput 0.99011
Reading from 27575: heap size 545 MB, throughput 0.984342
Reading from 27574: heap size 562 MB, throughput 0.989419
Numeric result:
Recommendation: 2 clients, utility 1.96951:
    h1: 1096.01 MB (U(h) = 0.0642421*h^0.437107)
    h2: 1111.99 MB (U(h) = 0.0641076*h^0.443486)
Recommendation: 2 clients, utility 1.96951:
    h1: 1096 MB (U(h) = 0.0642421*h^0.437107)
    h2: 1112 MB (U(h) = 0.0641076*h^0.443486)
Reading from 27575: heap size 548 MB, throughput 0.986291
Reading from 27574: heap size 560 MB, throughput 0.990987
Reading from 27575: heap size 550 MB, throughput 0.991684
Reading from 27574: heap size 564 MB, throughput 0.991267
Reading from 27575: heap size 553 MB, throughput 0.992641
Numeric result:
Recommendation: 2 clients, utility 2.12698:
    h1: 1224.67 MB (U(h) = 0.0376111*h^0.526729)
    h2: 983.328 MB (U(h) = 0.0724727*h^0.422937)
Recommendation: 2 clients, utility 2.12698:
    h1: 1224.66 MB (U(h) = 0.0376111*h^0.526729)
    h2: 983.34 MB (U(h) = 0.0724727*h^0.422937)
Reading from 27574: heap size 566 MB, throughput 0.990371
Reading from 27575: heap size 554 MB, throughput 0.991154
Reading from 27574: heap size 567 MB, throughput 0.988107
Reading from 27575: heap size 552 MB, throughput 0.990895
Reading from 27574: heap size 570 MB, throughput 0.987963
Reading from 27574: heap size 572 MB, throughput 0.98084
Reading from 27574: heap size 577 MB, throughput 0.984559
Reading from 27575: heap size 554 MB, throughput 0.990767
Reading from 27575: heap size 556 MB, throughput 0.988018
Reading from 27575: heap size 558 MB, throughput 0.980118
Numeric result:
Recommendation: 2 clients, utility 2.70803:
    h1: 1528.74 MB (U(h) = 0.00851073*h^0.772644)
    h2: 679.264 MB (U(h) = 0.117511*h^0.343305)
Recommendation: 2 clients, utility 2.70803:
    h1: 1528.74 MB (U(h) = 0.00851073*h^0.772644)
    h2: 679.257 MB (U(h) = 0.117511*h^0.343305)
Reading from 27574: heap size 580 MB, throughput 0.988859
Reading from 27575: heap size 562 MB, throughput 0.988991
Reading from 27574: heap size 579 MB, throughput 0.990301
Reading from 27575: heap size 563 MB, throughput 0.99249
Reading from 27574: heap size 582 MB, throughput 0.990502
Numeric result:
Recommendation: 2 clients, utility 2.76195:
    h1: 1508.72 MB (U(h) = 0.00776229*h^0.786946)
    h2: 699.278 MB (U(h) = 0.102875*h^0.364728)
Recommendation: 2 clients, utility 2.76195:
    h1: 1508.74 MB (U(h) = 0.00776229*h^0.786946)
    h2: 699.259 MB (U(h) = 0.102875*h^0.364728)
Reading from 27575: heap size 566 MB, throughput 0.992029
Reading from 27574: heap size 580 MB, throughput 0.989896
Reading from 27575: heap size 568 MB, throughput 0.992478
Reading from 27574: heap size 583 MB, throughput 0.990602
Reading from 27574: heap size 585 MB, throughput 0.987275
Reading from 27574: heap size 586 MB, throughput 0.982412
Reading from 27575: heap size 567 MB, throughput 0.991196
Reading from 27574: heap size 592 MB, throughput 0.989852
Numeric result:
Recommendation: 2 clients, utility 1.66367:
    h1: 1063.96 MB (U(h) = 0.129684*h^0.325971)
    h2: 1144.04 MB (U(h) = 0.11207*h^0.350505)
Recommendation: 2 clients, utility 1.66367:
    h1: 1063.96 MB (U(h) = 0.129684*h^0.325971)
    h2: 1144.04 MB (U(h) = 0.11207*h^0.350505)
Reading from 27575: heap size 569 MB, throughput 0.992483
Reading from 27575: heap size 569 MB, throughput 0.988681
Reading from 27575: heap size 571 MB, throughput 0.989747
Reading from 27574: heap size 592 MB, throughput 0.99219
Reading from 27575: heap size 577 MB, throughput 0.991776
Reading from 27574: heap size 594 MB, throughput 0.993301
Numeric result:
Recommendation: 2 clients, utility 1.6302:
    h1: 1092.47 MB (U(h) = 0.130816*h^0.324451)
    h2: 1115.53 MB (U(h) = 0.125936*h^0.331294)
Recommendation: 2 clients, utility 1.6302:
    h1: 1092.48 MB (U(h) = 0.130816*h^0.324451)
    h2: 1115.52 MB (U(h) = 0.125936*h^0.331294)
Reading from 27575: heap size 578 MB, throughput 0.99216
Reading from 27574: heap size 596 MB, throughput 0.991921
Reading from 27575: heap size 577 MB, throughput 0.992309
Reading from 27574: heap size 596 MB, throughput 0.989996
Reading from 27574: heap size 597 MB, throughput 0.987872
Reading from 27574: heap size 604 MB, throughput 0.980633
Reading from 27575: heap size 579 MB, throughput 0.991128
Numeric result:
Recommendation: 2 clients, utility 1.53338:
    h1: 1137.09 MB (U(h) = 0.149789*h^0.302007)
    h2: 1070.91 MB (U(h) = 0.168089*h^0.284432)
Recommendation: 2 clients, utility 1.53338:
    h1: 1137.09 MB (U(h) = 0.149789*h^0.302007)
    h2: 1070.91 MB (U(h) = 0.168089*h^0.284432)
Reading from 27574: heap size 605 MB, throughput 0.99044
Reading from 27575: heap size 581 MB, throughput 0.992488
Reading from 27575: heap size 582 MB, throughput 0.98683
Reading from 27575: heap size 583 MB, throughput 0.987241
Reading from 27574: heap size 611 MB, throughput 0.992091
Reading from 27575: heap size 585 MB, throughput 0.991475
Reading from 27574: heap size 613 MB, throughput 0.992487
Numeric result:
Recommendation: 2 clients, utility 1.43883:
    h1: 1316.44 MB (U(h) = 0.152747*h^0.298498)
    h2: 891.564 MB (U(h) = 0.279583*h^0.202156)
Recommendation: 2 clients, utility 1.43883:
    h1: 1316.45 MB (U(h) = 0.152747*h^0.298498)
    h2: 891.553 MB (U(h) = 0.279583*h^0.202156)
Reading from 27575: heap size 587 MB, throughput 0.992445
Reading from 27574: heap size 613 MB, throughput 0.992054
Reading from 27575: heap size 589 MB, throughput 0.991571
Reading from 27574: heap size 615 MB, throughput 0.992854
Reading from 27574: heap size 614 MB, throughput 0.989388
Reading from 27574: heap size 616 MB, throughput 0.986334
Reading from 27575: heap size 590 MB, throughput 0.991248
Numeric result:
Recommendation: 2 clients, utility 1.38002:
    h1: 1364.53 MB (U(h) = 0.174907*h^0.276335)
    h2: 843.47 MB (U(h) = 0.33959*h^0.170814)
Recommendation: 2 clients, utility 1.38002:
    h1: 1364.53 MB (U(h) = 0.174907*h^0.276335)
    h2: 843.471 MB (U(h) = 0.33959*h^0.170814)
Reading from 27575: heap size 591 MB, throughput 0.991365
Reading from 27574: heap size 622 MB, throughput 0.992073
Reading from 27575: heap size 593 MB, throughput 0.98674
Reading from 27575: heap size 595 MB, throughput 0.989487
Reading from 27574: heap size 623 MB, throughput 0.993081
Reading from 27575: heap size 601 MB, throughput 0.990776
Numeric result:
Recommendation: 2 clients, utility 1.34381:
    h1: 1862.98 MB (U(h) = 0.179934*h^0.271574)
    h2: 345.024 MB (U(h) = 0.720252*h^0.0502931)
Recommendation: 2 clients, utility 1.34381:
    h1: 1862.99 MB (U(h) = 0.179934*h^0.271574)
    h2: 345.01 MB (U(h) = 0.720252*h^0.0502931)
Reading from 27574: heap size 623 MB, throughput 0.992713
Reading from 27575: heap size 602 MB, throughput 0.991058
Reading from 27574: heap size 625 MB, throughput 0.991736
Reading from 27575: heap size 566 MB, throughput 0.991641
Reading from 27574: heap size 627 MB, throughput 0.991791
Reading from 27574: heap size 627 MB, throughput 0.985408
Client 27574 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 27575: heap size 556 MB, throughput 0.991969
Reading from 27575: heap size 567 MB, throughput 0.991394
Reading from 27575: heap size 573 MB, throughput 0.986884
Client 27575 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
