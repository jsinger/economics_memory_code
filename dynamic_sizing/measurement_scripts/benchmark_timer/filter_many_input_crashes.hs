{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, DeriveDataTypeable #-}
{-# OPTIONS_GHC -fno-warn-missing-fields -fno-warn-missing-methods #-}

import Control.Applicative
import Control.Monad
import Economem.ArgHelper
import Economem.Common
import Economem.Files
import Economem.MinHeap
import Economem.Records
import System.Console.CmdLib

data Args = Args { inputFile :: FilePath,
                   minHeapsFile :: FilePath,
                   outputPath :: FilePath }
          deriving (Typeable, Data, Eq, Show)

instance Attributes Args where
  attributes _ = group "Options" [
    inputFile %> [ Positional 0,
                   Required True,
                   Help "Benchmark combinations to run",
                   ArgHelp "INPUT_FILE" ],
    minHeapsFile %> [ Positional 1,
                   Required True,
                   Help "Min heaps of the benchmarks",
                   ArgHelp "MIN_HEAPS_FILE" ],
    outputPath %> [ Positional 2,
                   Required True,
                   Help "File to save to",
                   ArgHelp "OUTPUT_FILE" ]]

instance RecordCommand Args where
  mode_summary _ = "Filter input for timer_comparison_many based on which combinations will crash when the budget is split evenly"

main :: IO ()
main = getArgs >>= executeR Args {} >>= \opts -> do
  (input, minHeaps, outputFile) <- validate opts
  filterM (\bm -> not <$> willCrash minHeaps bm) input >>= put outputFile

  where
    validate :: Args -> IO ([BenchmarkCombinationMB],
                            [BenchmarkThreadsMB],
                            CsvFile BenchmarkCombinationMB)
    validate a = do
      input <- get =<< CsvFile <$> existingAbsoluteFile (inputFile a)
      minHeaps <- get =<< CsvFile <$> existingAbsoluteFile (minHeapsFile a)
      let outputFile = CsvFile (outputPath a)
      return (input, minHeaps, outputFile)

willCrash :: [BenchmarkThreadsMB] -> BenchmarkCombinationMB -> IO Bool
willCrash minHeaps (BenchmarkCombinationMB (BenchmarkCombination bms) m) = do
  relevantMinHeaps <- mapM (\bm -> (fromIntegral . sizeToInt . toMB) <$>
                                   lookupMinHeap (btiBenchmark bm) minHeaps) bms
  let evenShare = m `div` length bms
  return (or (map (\minHeap -> minHeap > evenShare) relevantMinHeaps))
