economemd
    total memory: 2760 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub44_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run06_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 21538: heap size 9 MB, throughput 0.991573
Clients: 1
Client 21538 has a minimum heap size of 276 MB
Reading from 21539: heap size 9 MB, throughput 0.992414
Clients: 2
Client 21539 has a minimum heap size of 276 MB
Reading from 21539: heap size 9 MB, throughput 0.988954
Reading from 21538: heap size 9 MB, throughput 0.989364
Reading from 21539: heap size 9 MB, throughput 0.983756
Reading from 21538: heap size 9 MB, throughput 0.982409
Reading from 21538: heap size 9 MB, throughput 0.973766
Reading from 21539: heap size 9 MB, throughput 0.964586
Reading from 21539: heap size 11 MB, throughput 0.972008
Reading from 21538: heap size 11 MB, throughput 0.979298
Reading from 21539: heap size 11 MB, throughput 0.974871
Reading from 21538: heap size 11 MB, throughput 0.985034
Reading from 21538: heap size 17 MB, throughput 0.971248
Reading from 21539: heap size 17 MB, throughput 0.963903
Reading from 21539: heap size 17 MB, throughput 0.892612
Reading from 21538: heap size 17 MB, throughput 0.895703
Reading from 21539: heap size 29 MB, throughput 0.678329
Reading from 21538: heap size 30 MB, throughput 0.822508
Reading from 21538: heap size 31 MB, throughput 0.551941
Reading from 21539: heap size 30 MB, throughput 0.928265
Reading from 21539: heap size 35 MB, throughput 0.786134
Reading from 21538: heap size 34 MB, throughput 0.642331
Reading from 21539: heap size 48 MB, throughput 0.530784
Reading from 21538: heap size 46 MB, throughput 0.378667
Reading from 21538: heap size 46 MB, throughput 0.475271
Reading from 21539: heap size 52 MB, throughput 0.488841
Reading from 21539: heap size 65 MB, throughput 0.286233
Reading from 21539: heap size 69 MB, throughput 0.282883
Reading from 21538: heap size 49 MB, throughput 0.615471
Reading from 21538: heap size 72 MB, throughput 0.669325
Reading from 21538: heap size 72 MB, throughput 0.48567
Reading from 21539: heap size 71 MB, throughput 0.513926
Reading from 21539: heap size 102 MB, throughput 0.579196
Reading from 21539: heap size 103 MB, throughput 0.341819
Reading from 21538: heap size 74 MB, throughput 0.451361
Reading from 21538: heap size 98 MB, throughput 0.517116
Reading from 21538: heap size 102 MB, throughput 0.554168
Reading from 21538: heap size 103 MB, throughput 0.283245
Reading from 21539: heap size 104 MB, throughput 0.399094
Reading from 21538: heap size 107 MB, throughput 0.110143
Reading from 21539: heap size 130 MB, throughput 0.267418
Reading from 21539: heap size 136 MB, throughput 0.143139
Reading from 21539: heap size 138 MB, throughput 0.769437
Reading from 21539: heap size 144 MB, throughput 0.684338
Reading from 21538: heap size 110 MB, throughput 0.419035
Reading from 21538: heap size 142 MB, throughput 0.711319
Reading from 21538: heap size 145 MB, throughput 0.728124
Reading from 21538: heap size 149 MB, throughput 0.737091
Reading from 21538: heap size 151 MB, throughput 0.682584
Reading from 21538: heap size 159 MB, throughput 0.678992
Reading from 21539: heap size 147 MB, throughput 0.409895
Reading from 21538: heap size 161 MB, throughput 0.679317
Reading from 21539: heap size 191 MB, throughput 0.710813
Reading from 21539: heap size 192 MB, throughput 0.649286
Reading from 21539: heap size 194 MB, throughput 0.630271
Reading from 21538: heap size 165 MB, throughput 0.460978
Reading from 21538: heap size 206 MB, throughput 0.606444
Reading from 21539: heap size 202 MB, throughput 0.855571
Reading from 21538: heap size 211 MB, throughput 0.841736
Reading from 21538: heap size 216 MB, throughput 0.861741
Reading from 21538: heap size 220 MB, throughput 0.828289
Reading from 21539: heap size 203 MB, throughput 0.580455
Reading from 21538: heap size 229 MB, throughput 0.739282
Reading from 21538: heap size 234 MB, throughput 0.705068
Reading from 21539: heap size 253 MB, throughput 0.825247
Reading from 21538: heap size 240 MB, throughput 0.719107
Reading from 21539: heap size 259 MB, throughput 0.807606
Reading from 21539: heap size 260 MB, throughput 0.742357
Reading from 21539: heap size 265 MB, throughput 0.806666
Reading from 21539: heap size 266 MB, throughput 0.732239
Reading from 21538: heap size 244 MB, throughput 0.609236
Reading from 21539: heap size 271 MB, throughput 0.791767
Reading from 21538: heap size 282 MB, throughput 0.708833
Reading from 21539: heap size 272 MB, throughput 0.75786
Reading from 21538: heap size 277 MB, throughput 0.771204
Reading from 21539: heap size 278 MB, throughput 0.7468
Reading from 21538: heap size 280 MB, throughput 0.763728
Reading from 21539: heap size 281 MB, throughput 0.714474
Reading from 21539: heap size 289 MB, throughput 0.728338
Reading from 21538: heap size 277 MB, throughput 0.833254
Reading from 21538: heap size 233 MB, throughput 0.819866
Reading from 21538: heap size 271 MB, throughput 0.816956
Reading from 21538: heap size 275 MB, throughput 0.814664
Reading from 21538: heap size 270 MB, throughput 0.821235
Reading from 21539: heap size 290 MB, throughput 0.89161
Reading from 21539: heap size 296 MB, throughput 0.804941
Reading from 21539: heap size 299 MB, throughput 0.696243
Reading from 21538: heap size 273 MB, throughput 0.622533
Reading from 21538: heap size 315 MB, throughput 0.823961
Reading from 21539: heap size 298 MB, throughput 0.615919
Reading from 21538: heap size 316 MB, throughput 0.902311
Reading from 21539: heap size 344 MB, throughput 0.676718
Reading from 21538: heap size 317 MB, throughput 0.867819
Reading from 21538: heap size 320 MB, throughput 0.807457
Reading from 21538: heap size 319 MB, throughput 0.76762
Reading from 21539: heap size 350 MB, throughput 0.838677
Reading from 21538: heap size 321 MB, throughput 0.749494
Reading from 21539: heap size 350 MB, throughput 0.822249
Reading from 21539: heap size 350 MB, throughput 0.760924
Reading from 21538: heap size 321 MB, throughput 0.839238
Reading from 21539: heap size 353 MB, throughput 0.730058
Reading from 21539: heap size 358 MB, throughput 0.690965
Reading from 21538: heap size 323 MB, throughput 0.820938
Reading from 21539: heap size 359 MB, throughput 0.701839
Reading from 21538: heap size 325 MB, throughput 0.797196
Reading from 21539: heap size 367 MB, throughput 0.687664
Reading from 21538: heap size 326 MB, throughput 0.668251
Reading from 21538: heap size 335 MB, throughput 0.650458
Reading from 21538: heap size 335 MB, throughput 0.616131
Reading from 21538: heap size 343 MB, throughput 0.62314
Reading from 21538: heap size 344 MB, throughput 0.646618
Numeric result:
Recommendation: 2 clients, utility 0.467473:
    h1: 1952.52 MB (U(h) = 0.558532*h^0.0347367)
    h2: 807.476 MB (U(h) = 0.584307*h^0.0143656)
Recommendation: 2 clients, utility 0.467473:
    h1: 1952.52 MB (U(h) = 0.558532*h^0.0347367)
    h2: 807.477 MB (U(h) = 0.584307*h^0.0143656)
Reading from 21539: heap size 368 MB, throughput 0.961004
Reading from 21538: heap size 354 MB, throughput 0.96832
Reading from 21539: heap size 373 MB, throughput 0.967905
Reading from 21538: heap size 355 MB, throughput 0.97961
Reading from 21539: heap size 375 MB, throughput 0.975377
Reading from 21538: heap size 362 MB, throughput 0.979277
Reading from 21539: heap size 375 MB, throughput 0.97281
Reading from 21538: heap size 363 MB, throughput 0.976875
Reading from 21538: heap size 363 MB, throughput 0.976254
Reading from 21539: heap size 379 MB, throughput 0.97791
Reading from 21538: heap size 366 MB, throughput 0.975878
Reading from 21539: heap size 382 MB, throughput 0.977062
Numeric result:
Recommendation: 2 clients, utility 0.587182:
    h1: 1496.41 MB (U(h) = 0.492898*h^0.0656669)
    h2: 1263.59 MB (U(h) = 0.496044*h^0.0554552)
Recommendation: 2 clients, utility 0.587182:
    h1: 1496.35 MB (U(h) = 0.492898*h^0.0656669)
    h2: 1263.65 MB (U(h) = 0.496044*h^0.0554552)
Reading from 21538: heap size 364 MB, throughput 0.977977
Reading from 21539: heap size 384 MB, throughput 0.980169
Reading from 21538: heap size 367 MB, throughput 0.976138
Reading from 21539: heap size 381 MB, throughput 0.976317
Reading from 21538: heap size 370 MB, throughput 0.975538
Reading from 21539: heap size 384 MB, throughput 0.975953
Reading from 21538: heap size 371 MB, throughput 0.975918
Reading from 21539: heap size 385 MB, throughput 0.978712
Reading from 21538: heap size 374 MB, throughput 0.976751
Reading from 21539: heap size 386 MB, throughput 0.976478
Reading from 21538: heap size 376 MB, throughput 0.973592
Reading from 21539: heap size 389 MB, throughput 0.974657
Reading from 21538: heap size 379 MB, throughput 0.973486
Numeric result:
Recommendation: 2 clients, utility 0.709284:
    h1: 1467.8 MB (U(h) = 0.435495*h^0.0960084)
    h2: 1292.2 MB (U(h) = 0.441427*h^0.084514)
Recommendation: 2 clients, utility 0.709284:
    h1: 1467.87 MB (U(h) = 0.435495*h^0.0960084)
    h2: 1292.13 MB (U(h) = 0.441427*h^0.084514)
Reading from 21539: heap size 390 MB, throughput 0.98004
Reading from 21539: heap size 390 MB, throughput 0.967761
Reading from 21538: heap size 380 MB, throughput 0.978931
Reading from 21539: heap size 392 MB, throughput 0.946841
Reading from 21539: heap size 397 MB, throughput 0.923481
Reading from 21538: heap size 384 MB, throughput 0.961634
Reading from 21539: heap size 401 MB, throughput 0.92111
Reading from 21538: heap size 385 MB, throughput 0.942885
Reading from 21538: heap size 391 MB, throughput 0.907945
Reading from 21538: heap size 394 MB, throughput 0.877523
Reading from 21538: heap size 404 MB, throughput 0.946509
Reading from 21539: heap size 409 MB, throughput 0.978111
Reading from 21538: heap size 405 MB, throughput 0.980502
Reading from 21539: heap size 410 MB, throughput 0.95045
Reading from 21538: heap size 408 MB, throughput 0.985243
Reading from 21539: heap size 439 MB, throughput 0.989215
Reading from 21538: heap size 410 MB, throughput 0.984799
Reading from 21539: heap size 441 MB, throughput 0.991486
Numeric result:
Recommendation: 2 clients, utility 0.838005:
    h1: 1409.08 MB (U(h) = 0.394641*h^0.119645)
    h2: 1350.92 MB (U(h) = 0.390095*h^0.114711)
Recommendation: 2 clients, utility 0.838005:
    h1: 1409.06 MB (U(h) = 0.394641*h^0.119645)
    h2: 1350.94 MB (U(h) = 0.390095*h^0.114711)
Reading from 21538: heap size 409 MB, throughput 0.985296
Reading from 21539: heap size 449 MB, throughput 0.989116
Reading from 21538: heap size 412 MB, throughput 0.985699
Reading from 21539: heap size 451 MB, throughput 0.989733
Reading from 21538: heap size 408 MB, throughput 0.985434
Reading from 21539: heap size 450 MB, throughput 0.988076
Reading from 21538: heap size 411 MB, throughput 0.953186
Reading from 21539: heap size 452 MB, throughput 0.987769
Reading from 21538: heap size 443 MB, throughput 0.98958
Reading from 21539: heap size 446 MB, throughput 0.987261
Numeric result:
Recommendation: 2 clients, utility 0.903197:
    h1: 1372.07 MB (U(h) = 0.37941*h^0.128908)
    h2: 1387.93 MB (U(h) = 0.365127*h^0.13041)
Recommendation: 2 clients, utility 0.903197:
    h1: 1372.01 MB (U(h) = 0.37941*h^0.128908)
    h2: 1387.99 MB (U(h) = 0.365127*h^0.13041)
Reading from 21538: heap size 446 MB, throughput 0.991262
Reading from 21539: heap size 450 MB, throughput 0.985591
Reading from 21539: heap size 449 MB, throughput 0.9797
Reading from 21539: heap size 450 MB, throughput 0.965787
Reading from 21538: heap size 451 MB, throughput 0.992053
Reading from 21539: heap size 455 MB, throughput 0.947764
Reading from 21538: heap size 453 MB, throughput 0.988522
Reading from 21538: heap size 450 MB, throughput 0.975281
Reading from 21539: heap size 458 MB, throughput 0.970483
Reading from 21538: heap size 453 MB, throughput 0.957078
Reading from 21538: heap size 459 MB, throughput 0.94465
Reading from 21539: heap size 462 MB, throughput 0.983566
Reading from 21538: heap size 461 MB, throughput 0.984546
Reading from 21539: heap size 466 MB, throughput 0.984899
Reading from 21538: heap size 462 MB, throughput 0.988504
Reading from 21539: heap size 462 MB, throughput 0.986529
Reading from 21538: heap size 465 MB, throughput 0.988623
Numeric result:
Recommendation: 2 clients, utility 0.968052:
    h1: 1401.45 MB (U(h) = 0.355606*h^0.143763)
    h2: 1358.55 MB (U(h) = 0.351512*h^0.13936)
Recommendation: 2 clients, utility 0.968052:
    h1: 1401.46 MB (U(h) = 0.355606*h^0.143763)
    h2: 1358.54 MB (U(h) = 0.351512*h^0.13936)
Reading from 21539: heap size 466 MB, throughput 0.985084
Reading from 21538: heap size 462 MB, throughput 0.989235
Reading from 21539: heap size 465 MB, throughput 0.983112
Reading from 21538: heap size 466 MB, throughput 0.986319
Reading from 21539: heap size 467 MB, throughput 0.983555
Reading from 21538: heap size 467 MB, throughput 0.985281
Reading from 21539: heap size 470 MB, throughput 0.985888
Reading from 21538: heap size 468 MB, throughput 0.986427
Numeric result:
Recommendation: 2 clients, utility 0.999275:
    h1: 1392.95 MB (U(h) = 0.348436*h^0.148398)
    h2: 1367.05 MB (U(h) = 0.342216*h^0.145644)
Recommendation: 2 clients, utility 0.999275:
    h1: 1392.92 MB (U(h) = 0.348436*h^0.148398)
    h2: 1367.08 MB (U(h) = 0.342216*h^0.145644)
Reading from 21539: heap size 470 MB, throughput 0.987997
Reading from 21538: heap size 471 MB, throughput 0.984495
Reading from 21539: heap size 470 MB, throughput 0.983626
Reading from 21539: heap size 472 MB, throughput 0.972152
Reading from 21539: heap size 476 MB, throughput 0.958574
Reading from 21538: heap size 472 MB, throughput 0.985639
Reading from 21539: heap size 477 MB, throughput 0.983696
Reading from 21538: heap size 477 MB, throughput 0.983001
Reading from 21538: heap size 477 MB, throughput 0.9713
Reading from 21538: heap size 481 MB, throughput 0.961499
Reading from 21539: heap size 483 MB, throughput 0.988008
Reading from 21538: heap size 483 MB, throughput 0.985202
Reading from 21539: heap size 484 MB, throughput 0.990083
Reading from 21538: heap size 487 MB, throughput 0.990392
Numeric result:
Recommendation: 2 clients, utility 1.04603:
    h1: 1387.68 MB (U(h) = 0.337124*h^0.155851)
    h2: 1372.32 MB (U(h) = 0.329963*h^0.154126)
Recommendation: 2 clients, utility 1.04603:
    h1: 1387.68 MB (U(h) = 0.337124*h^0.155851)
    h2: 1372.32 MB (U(h) = 0.329963*h^0.154126)
Reading from 21539: heap size 486 MB, throughput 0.991276
Reading from 21538: heap size 489 MB, throughput 0.990211
Reading from 21539: heap size 488 MB, throughput 0.989349
Reading from 21538: heap size 488 MB, throughput 0.989819
Reading from 21539: heap size 486 MB, throughput 0.988662
Reading from 21538: heap size 491 MB, throughput 0.990519
Reading from 21539: heap size 489 MB, throughput 0.987544
Reading from 21538: heap size 489 MB, throughput 0.988899
Numeric result:
Recommendation: 2 clients, utility 1.07033:
    h1: 1382.2 MB (U(h) = 0.332009*h^0.159278)
    h2: 1377.8 MB (U(h) = 0.323398*h^0.15877)
Recommendation: 2 clients, utility 1.07033:
    h1: 1382.2 MB (U(h) = 0.332009*h^0.159278)
    h2: 1377.8 MB (U(h) = 0.323398*h^0.15877)
Reading from 21538: heap size 491 MB, throughput 0.987719
Reading from 21539: heap size 491 MB, throughput 0.989013
Reading from 21539: heap size 491 MB, throughput 0.983199
Reading from 21539: heap size 492 MB, throughput 0.972197
Reading from 21539: heap size 494 MB, throughput 0.978513
Reading from 21538: heap size 494 MB, throughput 0.989922
Reading from 21538: heap size 495 MB, throughput 0.984747
Reading from 21538: heap size 495 MB, throughput 0.974956
Reading from 21538: heap size 497 MB, throughput 0.966669
Reading from 21539: heap size 502 MB, throughput 0.987875
Reading from 21539: heap size 502 MB, throughput 0.988685
Reading from 21538: heap size 505 MB, throughput 0.9881
Reading from 21539: heap size 503 MB, throughput 0.988696
Reading from 21538: heap size 506 MB, throughput 0.991514
Numeric result:
Recommendation: 2 clients, utility 1.10376:
    h1: 1375 MB (U(h) = 0.325213*h^0.163884)
    h2: 1385 MB (U(h) = 0.31464*h^0.165076)
Recommendation: 2 clients, utility 1.10376:
    h1: 1375 MB (U(h) = 0.325213*h^0.163884)
    h2: 1385 MB (U(h) = 0.31464*h^0.165076)
Reading from 21539: heap size 505 MB, throughput 0.988956
Reading from 21538: heap size 507 MB, throughput 0.990569
Reading from 21539: heap size 504 MB, throughput 0.988921
Reading from 21538: heap size 509 MB, throughput 0.990612
Reading from 21539: heap size 506 MB, throughput 0.988476
Reading from 21538: heap size 507 MB, throughput 0.989307
Reading from 21539: heap size 509 MB, throughput 0.989634
Reading from 21539: heap size 510 MB, throughput 0.982845
Numeric result:
Recommendation: 2 clients, utility 1.30943:
    h1: 1670.73 MB (U(h) = 0.185682*h^0.261554)
    h2: 1089.27 MB (U(h) = 0.307201*h^0.17053)
Recommendation: 2 clients, utility 1.30943:
    h1: 1670.71 MB (U(h) = 0.185682*h^0.261554)
    h2: 1089.29 MB (U(h) = 0.307201*h^0.17053)
Reading from 21539: heap size 510 MB, throughput 0.973706
Reading from 21538: heap size 510 MB, throughput 0.988282
Reading from 21539: heap size 513 MB, throughput 0.988107
Reading from 21538: heap size 513 MB, throughput 0.990823
Reading from 21538: heap size 514 MB, throughput 0.986092
Reading from 21538: heap size 513 MB, throughput 0.980189
Reading from 21539: heap size 516 MB, throughput 0.990812
Reading from 21538: heap size 516 MB, throughput 0.979892
Reading from 21539: heap size 518 MB, throughput 0.990812
Reading from 21538: heap size 524 MB, throughput 0.990888
Numeric result:
Recommendation: 2 clients, utility 1.81379:
    h1: 1993.28 MB (U(h) = 0.0615545*h^0.450735)
    h2: 766.725 MB (U(h) = 0.303364*h^0.173377)
Recommendation: 2 clients, utility 1.81379:
    h1: 1993.28 MB (U(h) = 0.0615545*h^0.450735)
    h2: 766.723 MB (U(h) = 0.303364*h^0.173377)
Reading from 21539: heap size 518 MB, throughput 0.990963
Reading from 21538: heap size 524 MB, throughput 0.991847
Reading from 21539: heap size 520 MB, throughput 0.989684
Reading from 21538: heap size 524 MB, throughput 0.991337
Reading from 21539: heap size 523 MB, throughput 0.988447
Reading from 21538: heap size 526 MB, throughput 0.990706
Reading from 21539: heap size 524 MB, throughput 0.989842
Reading from 21539: heap size 527 MB, throughput 0.98212
Numeric result:
Recommendation: 2 clients, utility 1.97188:
    h1: 2034.57 MB (U(h) = 0.0472301*h^0.495587)
    h2: 725.435 MB (U(h) = 0.298917*h^0.176704)
Recommendation: 2 clients, utility 1.97188:
    h1: 2034.57 MB (U(h) = 0.0472301*h^0.495587)
    h2: 725.434 MB (U(h) = 0.298917*h^0.176704)
Reading from 21539: heap size 528 MB, throughput 0.979726
Reading from 21538: heap size 526 MB, throughput 0.989896
Reading from 21539: heap size 537 MB, throughput 0.98972
Reading from 21538: heap size 527 MB, throughput 0.988558
Reading from 21538: heap size 530 MB, throughput 0.990418
Reading from 21538: heap size 531 MB, throughput 0.982376
Reading from 21539: heap size 537 MB, throughput 0.988388
Reading from 21538: heap size 532 MB, throughput 0.97857
Reading from 21539: heap size 538 MB, throughput 0.989665
Reading from 21538: heap size 534 MB, throughput 0.990627
Numeric result:
Recommendation: 2 clients, utility 2.24732:
    h1: 1810.26 MB (U(h) = 0.0399007*h^0.523243)
    h2: 949.741 MB (U(h) = 0.169314*h^0.274517)
Recommendation: 2 clients, utility 2.24732:
    h1: 1810.26 MB (U(h) = 0.0399007*h^0.523243)
    h2: 949.744 MB (U(h) = 0.169314*h^0.274517)
Reading from 21539: heap size 540 MB, throughput 0.991306
Reading from 21538: heap size 538 MB, throughput 0.993201
Reading from 21539: heap size 541 MB, throughput 0.98981
Reading from 21538: heap size 540 MB, throughput 0.992017
Reading from 21538: heap size 540 MB, throughput 0.991039
Reading from 21539: heap size 542 MB, throughput 0.992458
Reading from 21539: heap size 545 MB, throughput 0.988835
Reading from 21539: heap size 546 MB, throughput 0.982848
Numeric result:
Recommendation: 2 clients, utility 2.49804:
    h1: 1122.56 MB (U(h) = 0.104596*h^0.362116)
    h2: 1637.44 MB (U(h) = 0.0376568*h^0.528196)
Recommendation: 2 clients, utility 2.49804:
    h1: 1122.57 MB (U(h) = 0.104596*h^0.362116)
    h2: 1637.43 MB (U(h) = 0.0376568*h^0.528196)
Reading from 21538: heap size 542 MB, throughput 0.990377
Reading from 21539: heap size 553 MB, throughput 0.991818
Reading from 21538: heap size 544 MB, throughput 0.990654
Reading from 21539: heap size 553 MB, throughput 0.992563
Reading from 21538: heap size 545 MB, throughput 0.9877
Reading from 21538: heap size 550 MB, throughput 0.978103
Reading from 21538: heap size 552 MB, throughput 0.985882
Reading from 21539: heap size 554 MB, throughput 0.993129
Numeric result:
Recommendation: 2 clients, utility 2.7037:
    h1: 1177.08 MB (U(h) = 0.0776279*h^0.41099)
    h2: 1582.92 MB (U(h) = 0.0324768*h^0.552686)
Recommendation: 2 clients, utility 2.7037:
    h1: 1177.09 MB (U(h) = 0.0776279*h^0.41099)
    h2: 1582.91 MB (U(h) = 0.0324768*h^0.552686)
Reading from 21538: heap size 558 MB, throughput 0.990523
Reading from 21539: heap size 556 MB, throughput 0.991605
Reading from 21538: heap size 560 MB, throughput 0.992719
Reading from 21539: heap size 559 MB, throughput 0.990535
Reading from 21538: heap size 560 MB, throughput 0.991455
Reading from 21539: heap size 559 MB, throughput 0.991366
Reading from 21539: heap size 560 MB, throughput 0.985551
Numeric result:
Recommendation: 2 clients, utility 2.76522:
    h1: 1069.6 MB (U(h) = 0.0951192*h^0.377255)
    h2: 1690.4 MB (U(h) = 0.0248943*h^0.596214)
Recommendation: 2 clients, utility 2.76522:
    h1: 1069.6 MB (U(h) = 0.0951192*h^0.377255)
    h2: 1690.4 MB (U(h) = 0.0248943*h^0.596214)
Reading from 21539: heap size 562 MB, throughput 0.986039
Reading from 21538: heap size 563 MB, throughput 0.991126
Reading from 21539: heap size 570 MB, throughput 0.991556
Reading from 21538: heap size 563 MB, throughput 0.990445
Reading from 21538: heap size 564 MB, throughput 0.988529
Reading from 21538: heap size 568 MB, throughput 0.982602
Reading from 21539: heap size 571 MB, throughput 0.992199
Numeric result:
Recommendation: 2 clients, utility 2.51862:
    h1: 1173.49 MB (U(h) = 0.0893041*h^0.387192)
    h2: 1586.51 MB (U(h) = 0.0385939*h^0.523457)
Recommendation: 2 clients, utility 2.51862:
    h1: 1173.5 MB (U(h) = 0.0893041*h^0.387192)
    h2: 1586.5 MB (U(h) = 0.0385939*h^0.523457)
Reading from 21538: heap size 569 MB, throughput 0.988381
Reading from 21539: heap size 571 MB, throughput 0.991937
Reading from 21538: heap size 577 MB, throughput 0.991365
Reading from 21539: heap size 573 MB, throughput 0.990468
Reading from 21538: heap size 578 MB, throughput 0.991574
Reading from 21539: heap size 576 MB, throughput 0.992638
Reading from 21539: heap size 576 MB, throughput 0.988289
Numeric result:
Recommendation: 2 clients, utility 2.22969:
    h1: 1321.53 MB (U(h) = 0.087268*h^0.390571)
    h2: 1438.47 MB (U(h) = 0.0701329*h^0.425113)
Recommendation: 2 clients, utility 2.22969:
    h1: 1321.56 MB (U(h) = 0.087268*h^0.390571)
    h2: 1438.44 MB (U(h) = 0.0701329*h^0.425113)
Reading from 21539: heap size 577 MB, throughput 0.985723
Reading from 21538: heap size 577 MB, throughput 0.991502
Reading from 21538: heap size 580 MB, throughput 0.990765
Reading from 21539: heap size 580 MB, throughput 0.992338
Reading from 21539: heap size 581 MB, throughput 0.992586
Reading from 21538: heap size 582 MB, throughput 0.992297
Reading from 21538: heap size 582 MB, throughput 0.987705
Reading from 21538: heap size 581 MB, throughput 0.984638
Numeric result:
Recommendation: 2 clients, utility 2.05577:
    h1: 1274.01 MB (U(h) = 0.116294*h^0.34352)
    h2: 1485.99 MB (U(h) = 0.0812403*h^0.400678)
Recommendation: 2 clients, utility 2.05577:
    h1: 1274.01 MB (U(h) = 0.116294*h^0.34352)
    h2: 1485.99 MB (U(h) = 0.0812403*h^0.400678)
Reading from 21539: heap size 583 MB, throughput 0.992976
Reading from 21538: heap size 584 MB, throughput 0.991841
Reading from 21539: heap size 583 MB, throughput 0.992001
Reading from 21538: heap size 587 MB, throughput 0.992552
Reading from 21539: heap size 584 MB, throughput 0.992254
Reading from 21539: heap size 588 MB, throughput 0.987843
Numeric result:
Recommendation: 2 clients, utility 2.04917:
    h1: 1237.03 MB (U(h) = 0.124043*h^0.332853)
    h2: 1522.97 MB (U(h) = 0.0766383*h^0.409794)
Recommendation: 2 clients, utility 2.04917:
    h1: 1237.03 MB (U(h) = 0.124043*h^0.332853)
    h2: 1522.97 MB (U(h) = 0.0766383*h^0.409794)
Reading from 21538: heap size 589 MB, throughput 0.993422
Reading from 21539: heap size 588 MB, throughput 0.986144
Reading from 21538: heap size 588 MB, throughput 0.992646
Reading from 21539: heap size 595 MB, throughput 0.991014
Reading from 21538: heap size 590 MB, throughput 0.9911
Reading from 21539: heap size 596 MB, throughput 0.99234
Reading from 21538: heap size 593 MB, throughput 0.99025
Reading from 21538: heap size 594 MB, throughput 0.98386
Numeric result:
Recommendation: 2 clients, utility 1.94438:
    h1: 1202.95 MB (U(h) = 0.148511*h^0.303262)
    h2: 1557.05 MB (U(h) = 0.0850776*h^0.392533)
Recommendation: 2 clients, utility 1.94438:
    h1: 1202.94 MB (U(h) = 0.148511*h^0.303262)
    h2: 1557.06 MB (U(h) = 0.0850776*h^0.392533)
Reading from 21539: heap size 594 MB, throughput 0.989923
Reading from 21538: heap size 598 MB, throughput 0.990049
Reading from 21539: heap size 597 MB, throughput 0.988522
Reading from 21538: heap size 600 MB, throughput 0.992832
Reading from 21539: heap size 600 MB, throughput 0.992036
Numeric result:
Recommendation: 2 clients, utility 1.93987:
    h1: 1189.98 MB (U(h) = 0.151136*h^0.300227)
    h2: 1570.02 MB (U(h) = 0.0830031*h^0.396116)
Recommendation: 2 clients, utility 1.93987:
    h1: 1189.97 MB (U(h) = 0.151136*h^0.300227)
    h2: 1570.03 MB (U(h) = 0.0830031*h^0.396116)
Reading from 21539: heap size 601 MB, throughput 0.988388
Reading from 21538: heap size 602 MB, throughput 0.992597
Reading from 21539: heap size 602 MB, throughput 0.988705
Reading from 21538: heap size 604 MB, throughput 0.991784
Reading from 21539: heap size 605 MB, throughput 0.991449
Reading from 21538: heap size 606 MB, throughput 0.990651
Reading from 21539: heap size 606 MB, throughput 0.992061
Numeric result:
Recommendation: 2 clients, utility 1.84526:
    h1: 1165.16 MB (U(h) = 0.177068*h^0.274526)
    h2: 1594.84 MB (U(h) = 0.0938901*h^0.375772)
Recommendation: 2 clients, utility 1.84526:
    h1: 1165.15 MB (U(h) = 0.177068*h^0.274526)
    h2: 1594.85 MB (U(h) = 0.0938901*h^0.375772)
Reading from 21538: heap size 607 MB, throughput 0.989652
Reading from 21538: heap size 611 MB, throughput 0.985709
Reading from 21539: heap size 608 MB, throughput 0.992851
Reading from 21538: heap size 612 MB, throughput 0.99051
Reading from 21539: heap size 610 MB, throughput 0.992031
Reading from 21538: heap size 616 MB, throughput 0.992451
Reading from 21539: heap size 610 MB, throughput 0.990701
Numeric result:
Recommendation: 2 clients, utility 1.67191:
    h1: 1010.68 MB (U(h) = 0.284234*h^0.198454)
    h2: 1749.32 MB (U(h) = 0.114647*h^0.34349)
Recommendation: 2 clients, utility 1.67191:
    h1: 1010.68 MB (U(h) = 0.284234*h^0.198454)
    h2: 1749.32 MB (U(h) = 0.114647*h^0.34349)
Reading from 21539: heap size 613 MB, throughput 0.984193
Reading from 21538: heap size 618 MB, throughput 0.991909
Reading from 21539: heap size 615 MB, throughput 0.990172
Reading from 21538: heap size 617 MB, throughput 0.993203
Reading from 21539: heap size 618 MB, throughput 0.991971
Numeric result:
Recommendation: 2 clients, utility 1.55118:
    h1: 660.887 MB (U(h) = 0.520887*h^0.101715)
    h2: 2099.11 MB (U(h) = 0.129967*h^0.323066)
Recommendation: 2 clients, utility 1.55118:
    h1: 660.891 MB (U(h) = 0.520887*h^0.101715)
    h2: 2099.11 MB (U(h) = 0.129967*h^0.323066)
Reading from 21538: heap size 619 MB, throughput 0.995065
Reading from 21538: heap size 620 MB, throughput 0.991162
Reading from 21539: heap size 621 MB, throughput 0.992049
Reading from 21538: heap size 622 MB, throughput 0.990251
Reading from 21539: heap size 622 MB, throughput 0.991822
Reading from 21538: heap size 629 MB, throughput 0.99284
Reading from 21539: heap size 623 MB, throughput 0.99249
Numeric result:
Recommendation: 2 clients, utility 1.3152:
    h1: 577.404 MB (U(h) = 0.693337*h^0.0560944)
    h2: 2182.6 MB (U(h) = 0.260188*h^0.211998)
Recommendation: 2 clients, utility 1.3152:
    h1: 577.489 MB (U(h) = 0.693337*h^0.0560944)
    h2: 2182.51 MB (U(h) = 0.260188*h^0.211998)
Reading from 21539: heap size 628 MB, throughput 0.988061
Client 21539 died
Clients: 1
Reading from 21538: heap size 630 MB, throughput 0.993633
Reading from 21538: heap size 602 MB, throughput 0.994053
Reading from 21538: heap size 595 MB, throughput 0.993468
Recommendation: one client; give it all the memory
Reading from 21538: heap size 593 MB, throughput 0.993424
Reading from 21538: heap size 601 MB, throughput 0.989583
Client 21538 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
