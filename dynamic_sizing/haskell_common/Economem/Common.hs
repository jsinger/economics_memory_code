module Economem.Common where

import Control.Applicative
import Data.Char
import Data.List
import Data.Time.Clock
import Data.Time.Format
import Data.Time.LocalTime
import Economem.Files
import System.Directory
import System.Environment
import System.Locale
import System.Process
import Text.Printf

economemRoot :: IO FilePath
economemRoot = getEnv "ECONOMEM_ROOT"

dataRoot :: IO FilePath
dataRoot = (</> "data" </> "output") <$> economemRoot

measurementScriptsDir :: IO FilePath
measurementScriptsDir = (</> "measurement_scripts") <$> economemRoot

devnull :: FilePath
devnull = "/dev/null"

nowRaw :: IO LocalTime
nowRaw =  do
  utcTime <- getCurrentTime
  tz <- getCurrentTimeZone
  return (utcToLocalTime tz utcTime)

nowPretty :: IO String
nowPretty = do
  now <- nowRaw
  let locale = defaultTimeLocale
  return (formatTime locale "%Y-%m-%d %H:%M:%S" now)

nowPathSafe :: IO String
nowPathSafe = do
  now <- nowRaw
  let locale = defaultTimeLocale
  return (formatTime locale "%Y_%m_%d_%H_%M_%S" now)

timeSince :: UTCTime -> IO NominalDiffTime
timeSince startTime = do
  endTime <- getCurrentTime
  return (diffUTCTime endTime startTime)

timeToSeconds :: String -> IO Integer
timeToSeconds s = do
  let (body, suffix) = span isDigit s
  case body of
    [] -> badParse
    _ -> do multiplier <- case suffix of
              [] -> return 1
              's':[] -> return 1
              'm':[] -> return 60
              'h':[] -> return (60 * 60)
              'd':[] -> return (60 * 60 * 24)
              _ -> badParse
            (* multiplier) <$> readIO body
  where
    badParse = fail $ "'" ++ s ++ "' is not a valid time"

secondsToTime :: Double -> String
secondsToTime s =
  let (text, remainder) = (convertUnit "m" 60 . convertUnit "h" (60 * 60) . convertUnit "d" (24 * 60 * 60)) ([], s) in
  intercalate ", "
  (text ++ if remainder > 0 then
              [show remainder ++ " s"]
           else
             [])

  where
    convertUnit :: String -> Integer -> ([String], Double) -> ([String], Double)
    convertUnit name factor (text, d)
      | wholeUnits >= 1 = (text ++ [show wholeUnits ++ " " ++ name], d - fromIntegral (wholeUnits * factor))
      | otherwise = (text, d)
      where
        wholeUnits :: Integer
        wholeUnits = floor (d / fromIntegral factor)

prettyInt :: (Integral a, Show a, PrintfArg a) => a -> a -> String
prettyInt n maxNum = printf ("%0" ++ show (length (show maxNum)) ++ "d") n

class Pretty a where
  pretty :: Pretty a => a -> String

class NumAndMax a where
  makeNumAndMax :: NumAndMax a => Integer -> Integer -> a
  getNum :: NumAndMax a => a -> Integer
  getMax :: NumAndMax a => a -> Integer

upTo :: NumAndMax a => Integer -> [a]
upTo i = map (`makeNumAndMax` i) [1..i]

zipNumAndMax :: NumAndMax b => [a] -> [(a, b)]
zipNumAndMax xs = zip xs (upTo (fromIntegral (length xs)))

numOf :: (NumAndMax a, Pretty a) => a -> String
numOf a = printf "%s of %d" (pretty a) (getMax a)

data DataUnit = B | KB | MB | GB deriving (Read, Show)
data DataSize = DataSize Integer DataUnit deriving (Read, Show)

instance Eq DataSize where
  (==) a b = sizeToInt (toB a) == sizeToInt (toB b)

instance Ord DataSize where
  compare a b = compare (sizeToInt (toB a)) (sizeToInt (toB b))

toB :: DataSize -> DataSize
toB (DataSize i B) = DataSize i B
toB (DataSize i KB) = DataSize (i * 1024) B
toB (DataSize i MB) = DataSize (i * 1024 * 1024) B
toB (DataSize i GB) = DataSize (i * 1024 * 1024 * 1024) B

toKB :: DataSize -> DataSize
toKB s = DataSize (b `div` 1024) KB
  where (DataSize b _) = toB s

toMB :: DataSize -> DataSize
toMB s = DataSize (kb `div` 1024) MB
  where (DataSize kb _) = toKB s

toGB :: DataSize -> DataSize
toGB s = DataSize (mb `div` 1024) GB
  where (DataSize mb _) = toMB s

sizeToInt :: DataSize -> Integer
sizeToInt (DataSize i _) = i

instance Pretty DataSize where
  pretty (DataSize i B) = show i ++ " B"
  pretty (DataSize i KB) = show i ++ " kB"
  pretty (DataSize i MB) = show i ++ " MB"
  pretty (DataSize i GB) = show i ++ " GB"

systemMemory :: IO DataSize
systemMemory = do
  raw <- readFile "/proc/meminfo"
  let l = lines raw
  case find (\s -> "MemTotal" `isPrefixOf` s) l of
    Just line -> DataSize <$> (readIO =<< headM (drop 1 (words line))) <*> return KB
    Nothing -> fail "/proc/meminfo parsing failed"

numProcessors :: IO Integer
numProcessors = readIO =<< readProcess "nproc" [] ""

headM :: Monad m => [a] -> m a
headM (x:_) = return x
headM [] = fail "Empty list"

mergePdfs :: [FilePath] -> FilePath -> IO ()
mergePdfs [] _ = return ()
mergePdfs (x:[]) outFile = copyFile x outFile
mergePdfs xs outFile = do
  h <- runProcess
       "pdfunite"
       (xs ++ [outFile])
       Nothing
       Nothing
       Nothing
       Nothing
       Nothing
  _ <- waitForProcess h
  return ()
