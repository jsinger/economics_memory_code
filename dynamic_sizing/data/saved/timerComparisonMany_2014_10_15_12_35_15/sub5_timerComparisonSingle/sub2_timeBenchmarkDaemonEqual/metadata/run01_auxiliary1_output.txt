economemd
    total memory: 8922 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub5_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 4689: heap size 9 MB, throughput 0.991994
Clients: 1
Client 4689 has a minimum heap size of 1211 MB
Reading from 4690: heap size 9 MB, throughput 0.989448
Clients: 2
Client 4690 has a minimum heap size of 276 MB
Reading from 4690: heap size 9 MB, throughput 0.978692
Reading from 4689: heap size 9 MB, throughput 0.978484
Reading from 4690: heap size 9 MB, throughput 0.963754
Reading from 4689: heap size 9 MB, throughput 0.955315
Reading from 4690: heap size 9 MB, throughput 0.957812
Reading from 4689: heap size 9 MB, throughput 0.947621
Reading from 4690: heap size 11 MB, throughput 0.979365
Reading from 4689: heap size 11 MB, throughput 0.929988
Reading from 4690: heap size 11 MB, throughput 0.985426
Reading from 4689: heap size 11 MB, throughput 0.977441
Reading from 4690: heap size 17 MB, throughput 0.777301
Reading from 4689: heap size 17 MB, throughput 0.952744
Reading from 4690: heap size 17 MB, throughput 0.50816
Reading from 4689: heap size 17 MB, throughput 0.446391
Reading from 4690: heap size 29 MB, throughput 0.949916
Reading from 4689: heap size 30 MB, throughput 0.905847
Reading from 4690: heap size 30 MB, throughput 0.833804
Reading from 4689: heap size 31 MB, throughput 0.781788
Reading from 4689: heap size 34 MB, throughput 0.369717
Reading from 4690: heap size 35 MB, throughput 0.448636
Reading from 4689: heap size 47 MB, throughput 0.916098
Reading from 4690: heap size 47 MB, throughput 0.746193
Reading from 4689: heap size 49 MB, throughput 0.885329
Reading from 4689: heap size 51 MB, throughput 0.271301
Reading from 4690: heap size 49 MB, throughput 0.441614
Reading from 4689: heap size 72 MB, throughput 0.843022
Reading from 4690: heap size 63 MB, throughput 0.86596
Reading from 4690: heap size 68 MB, throughput 0.77242
Reading from 4689: heap size 72 MB, throughput 0.269581
Reading from 4689: heap size 99 MB, throughput 0.83047
Reading from 4690: heap size 70 MB, throughput 0.28996
Reading from 4689: heap size 99 MB, throughput 0.857935
Reading from 4689: heap size 102 MB, throughput 0.70817
Reading from 4690: heap size 101 MB, throughput 0.844038
Reading from 4689: heap size 105 MB, throughput 0.204033
Reading from 4689: heap size 132 MB, throughput 0.740056
Reading from 4690: heap size 101 MB, throughput 0.21873
Reading from 4689: heap size 136 MB, throughput 0.767499
Reading from 4690: heap size 132 MB, throughput 0.778685
Reading from 4689: heap size 139 MB, throughput 0.804479
Reading from 4690: heap size 132 MB, throughput 0.870413
Reading from 4690: heap size 134 MB, throughput 0.76083
Reading from 4689: heap size 140 MB, throughput 0.126879
Reading from 4689: heap size 180 MB, throughput 0.651857
Reading from 4690: heap size 137 MB, throughput 0.177898
Reading from 4689: heap size 182 MB, throughput 0.643058
Reading from 4690: heap size 168 MB, throughput 0.608131
Reading from 4690: heap size 173 MB, throughput 0.750925
Reading from 4689: heap size 184 MB, throughput 0.711378
Reading from 4690: heap size 174 MB, throughput 0.633487
Reading from 4689: heap size 189 MB, throughput 0.564579
Reading from 4690: heap size 177 MB, throughput 0.682582
Reading from 4689: heap size 198 MB, throughput 0.688293
Reading from 4690: heap size 183 MB, throughput 0.802301
Reading from 4689: heap size 202 MB, throughput 0.606764
Reading from 4689: heap size 206 MB, throughput 0.63054
Reading from 4689: heap size 214 MB, throughput 0.502221
Reading from 4689: heap size 219 MB, throughput 0.56624
Reading from 4689: heap size 230 MB, throughput 0.0872697
Reading from 4690: heap size 187 MB, throughput 0.359684
Reading from 4689: heap size 272 MB, throughput 0.512688
Reading from 4689: heap size 280 MB, throughput 0.656322
Reading from 4689: heap size 281 MB, throughput 0.407979
Reading from 4690: heap size 240 MB, throughput 0.820548
Reading from 4690: heap size 243 MB, throughput 0.723591
Reading from 4690: heap size 249 MB, throughput 0.610496
Reading from 4690: heap size 252 MB, throughput 0.575377
Reading from 4689: heap size 283 MB, throughput 0.106701
Reading from 4689: heap size 331 MB, throughput 0.406834
Reading from 4690: heap size 258 MB, throughput 0.856975
Reading from 4689: heap size 274 MB, throughput 0.68748
Reading from 4690: heap size 261 MB, throughput 0.828254
Reading from 4689: heap size 326 MB, throughput 0.661159
Reading from 4690: heap size 265 MB, throughput 0.806737
Reading from 4689: heap size 328 MB, throughput 0.625286
Reading from 4690: heap size 267 MB, throughput 0.571368
Reading from 4689: heap size 329 MB, throughput 0.537323
Reading from 4690: heap size 274 MB, throughput 0.720847
Reading from 4689: heap size 334 MB, throughput 0.640269
Reading from 4690: heap size 278 MB, throughput 0.765256
Reading from 4690: heap size 284 MB, throughput 0.621764
Reading from 4690: heap size 287 MB, throughput 0.93621
Reading from 4689: heap size 339 MB, throughput 0.10982
Reading from 4689: heap size 393 MB, throughput 0.462276
Reading from 4690: heap size 293 MB, throughput 0.716924
Reading from 4690: heap size 295 MB, throughput 0.53912
Reading from 4689: heap size 397 MB, throughput 0.46743
Reading from 4689: heap size 399 MB, throughput 0.470918
Reading from 4689: heap size 403 MB, throughput 0.564062
Reading from 4689: heap size 406 MB, throughput 0.412973
Reading from 4689: heap size 410 MB, throughput 0.454336
Reading from 4690: heap size 295 MB, throughput 0.0827359
Reading from 4690: heap size 342 MB, throughput 0.794778
Reading from 4690: heap size 347 MB, throughput 0.864848
Reading from 4690: heap size 347 MB, throughput 0.77526
Reading from 4689: heap size 415 MB, throughput 0.0830124
Reading from 4690: heap size 350 MB, throughput 0.770799
Reading from 4690: heap size 351 MB, throughput 0.51718
Equal recommendation: 4461 MB each
Reading from 4689: heap size 476 MB, throughput 0.472071
Reading from 4690: heap size 355 MB, throughput 0.713407
Reading from 4689: heap size 477 MB, throughput 0.640208
Reading from 4690: heap size 356 MB, throughput 0.782255
Reading from 4689: heap size 475 MB, throughput 0.558708
Reading from 4690: heap size 361 MB, throughput 0.677257
Reading from 4689: heap size 477 MB, throughput 0.462605
Reading from 4690: heap size 362 MB, throughput 0.918774
Reading from 4689: heap size 482 MB, throughput 0.110179
Reading from 4689: heap size 543 MB, throughput 0.419962
Reading from 4689: heap size 547 MB, throughput 0.580471
Reading from 4689: heap size 548 MB, throughput 0.515235
Reading from 4689: heap size 550 MB, throughput 0.539028
Reading from 4689: heap size 559 MB, throughput 0.603103
Reading from 4690: heap size 367 MB, throughput 0.959769
Reading from 4689: heap size 567 MB, throughput 0.549448
Reading from 4689: heap size 578 MB, throughput 0.107142
Reading from 4689: heap size 648 MB, throughput 0.409088
Reading from 4690: heap size 369 MB, throughput 0.958595
Reading from 4689: heap size 657 MB, throughput 0.417681
Reading from 4689: heap size 660 MB, throughput 0.406759
Reading from 4690: heap size 370 MB, throughput 0.972019
Reading from 4689: heap size 663 MB, throughput 0.29696
Reading from 4690: heap size 373 MB, throughput 0.960736
Reading from 4689: heap size 745 MB, throughput 0.757347
Reading from 4689: heap size 747 MB, throughput 0.9071
Reading from 4689: heap size 746 MB, throughput 0.530872
Reading from 4689: heap size 753 MB, throughput 0.59642
Reading from 4689: heap size 759 MB, throughput 0.38807
Reading from 4690: heap size 373 MB, throughput 0.977109
Reading from 4689: heap size 765 MB, throughput 0.350401
Reading from 4689: heap size 775 MB, throughput 0.401693
Equal recommendation: 4461 MB each
Reading from 4690: heap size 375 MB, throughput 0.957301
Reading from 4689: heap size 775 MB, throughput 0.178712
Reading from 4689: heap size 855 MB, throughput 0.452117
Reading from 4690: heap size 372 MB, throughput 0.97948
Reading from 4689: heap size 859 MB, throughput 0.641141
Reading from 4689: heap size 858 MB, throughput 0.625443
Reading from 4689: heap size 861 MB, throughput 0.88467
Reading from 4689: heap size 862 MB, throughput 0.837399
Reading from 4689: heap size 864 MB, throughput 0.837854
Reading from 4690: heap size 375 MB, throughput 0.97042
Reading from 4689: heap size 866 MB, throughput 0.748543
Reading from 4689: heap size 868 MB, throughput 0.26891
Reading from 4690: heap size 375 MB, throughput 0.974829
Reading from 4689: heap size 959 MB, throughput 0.609507
Reading from 4689: heap size 964 MB, throughput 0.823912
Reading from 4689: heap size 967 MB, throughput 0.82605
Reading from 4689: heap size 969 MB, throughput 0.834326
Reading from 4689: heap size 967 MB, throughput 0.826703
Reading from 4689: heap size 970 MB, throughput 0.833864
Reading from 4690: heap size 376 MB, throughput 0.970453
Reading from 4689: heap size 966 MB, throughput 0.832294
Reading from 4689: heap size 970 MB, throughput 0.860934
Reading from 4689: heap size 968 MB, throughput 0.94233
Reading from 4690: heap size 378 MB, throughput 0.969999
Reading from 4689: heap size 972 MB, throughput 0.950898
Reading from 4689: heap size 963 MB, throughput 0.760191
Reading from 4690: heap size 379 MB, throughput 0.972103
Reading from 4689: heap size 974 MB, throughput 0.778823
Reading from 4689: heap size 984 MB, throughput 0.761813
Reading from 4689: heap size 989 MB, throughput 0.753985
Equal recommendation: 4461 MB each
Reading from 4689: heap size 999 MB, throughput 0.156862
Reading from 4690: heap size 382 MB, throughput 0.970745
Reading from 4689: heap size 1105 MB, throughput 0.578889
Reading from 4689: heap size 1104 MB, throughput 0.88824
Reading from 4689: heap size 1110 MB, throughput 0.884951
Reading from 4690: heap size 383 MB, throughput 0.957134
Reading from 4689: heap size 1120 MB, throughput 0.894785
Reading from 4690: heap size 381 MB, throughput 0.836527
Reading from 4690: heap size 383 MB, throughput 0.797201
Reading from 4690: heap size 390 MB, throughput 0.766555
Reading from 4689: heap size 1122 MB, throughput 0.899067
Reading from 4689: heap size 1118 MB, throughput 0.820115
Reading from 4689: heap size 1121 MB, throughput 0.681189
Reading from 4690: heap size 393 MB, throughput 0.942105
Reading from 4689: heap size 1132 MB, throughput 0.664915
Reading from 4689: heap size 1135 MB, throughput 0.670043
Reading from 4689: heap size 1150 MB, throughput 0.691371
Reading from 4689: heap size 1152 MB, throughput 0.676376
Reading from 4689: heap size 1167 MB, throughput 0.673852
Reading from 4689: heap size 1168 MB, throughput 0.665935
Reading from 4689: heap size 1187 MB, throughput 0.648167
Reading from 4689: heap size 1187 MB, throughput 0.778939
Reading from 4690: heap size 400 MB, throughput 0.981625
Reading from 4690: heap size 401 MB, throughput 0.978336
Reading from 4689: heap size 1209 MB, throughput 0.962455
Reading from 4690: heap size 402 MB, throughput 0.988962
Reading from 4689: heap size 1210 MB, throughput 0.953151
Equal recommendation: 4461 MB each
Reading from 4690: heap size 404 MB, throughput 0.986498
Reading from 4689: heap size 1215 MB, throughput 0.950918
Reading from 4690: heap size 401 MB, throughput 0.982315
Reading from 4689: heap size 1222 MB, throughput 0.952203
Reading from 4690: heap size 404 MB, throughput 0.984334
Reading from 4689: heap size 1216 MB, throughput 0.955762
Reading from 4690: heap size 401 MB, throughput 0.982393
Reading from 4689: heap size 1225 MB, throughput 0.953054
Reading from 4690: heap size 404 MB, throughput 0.780345
Reading from 4689: heap size 1211 MB, throughput 0.958993
Equal recommendation: 4461 MB each
Reading from 4690: heap size 440 MB, throughput 0.994222
Reading from 4690: heap size 441 MB, throughput 0.991981
Reading from 4689: heap size 1221 MB, throughput 0.955303
Reading from 4690: heap size 445 MB, throughput 0.986113
Reading from 4690: heap size 447 MB, throughput 0.87813
Reading from 4690: heap size 446 MB, throughput 0.871589
Reading from 4690: heap size 448 MB, throughput 0.859425
Reading from 4689: heap size 1216 MB, throughput 0.959676
Reading from 4690: heap size 455 MB, throughput 0.980213
Reading from 4689: heap size 1221 MB, throughput 0.958748
Reading from 4690: heap size 456 MB, throughput 0.990578
Reading from 4690: heap size 456 MB, throughput 0.987986
Reading from 4689: heap size 1227 MB, throughput 0.959793
Equal recommendation: 4461 MB each
Reading from 4690: heap size 459 MB, throughput 0.989254
Reading from 4689: heap size 1228 MB, throughput 0.9623
Reading from 4690: heap size 455 MB, throughput 0.987399
Reading from 4689: heap size 1236 MB, throughput 0.958313
Reading from 4690: heap size 458 MB, throughput 0.987056
Reading from 4689: heap size 1239 MB, throughput 0.961118
Reading from 4690: heap size 458 MB, throughput 0.986815
Reading from 4689: heap size 1246 MB, throughput 0.955867
Reading from 4690: heap size 459 MB, throughput 0.982524
Equal recommendation: 4461 MB each
Reading from 4690: heap size 461 MB, throughput 0.982685
Reading from 4689: heap size 1251 MB, throughput 0.958047
Reading from 4690: heap size 462 MB, throughput 0.990012
Reading from 4690: heap size 464 MB, throughput 0.914948
Reading from 4690: heap size 465 MB, throughput 0.834279
Reading from 4690: heap size 470 MB, throughput 0.882187
Reading from 4689: heap size 1259 MB, throughput 0.954775
Reading from 4690: heap size 472 MB, throughput 0.990482
Reading from 4689: heap size 1264 MB, throughput 0.956387
Reading from 4690: heap size 476 MB, throughput 0.98973
Reading from 4689: heap size 1273 MB, throughput 0.952123
Equal recommendation: 4461 MB each
Reading from 4690: heap size 478 MB, throughput 0.989179
Reading from 4689: heap size 1276 MB, throughput 0.953518
Reading from 4690: heap size 477 MB, throughput 0.989029
Reading from 4689: heap size 1286 MB, throughput 0.951564
Reading from 4690: heap size 480 MB, throughput 0.981026
Reading from 4690: heap size 479 MB, throughput 0.987196
Reading from 4689: heap size 1288 MB, throughput 0.550291
Reading from 4690: heap size 481 MB, throughput 0.98778
Equal recommendation: 4461 MB each
Reading from 4690: heap size 483 MB, throughput 0.984012
Reading from 4689: heap size 1396 MB, throughput 0.943508
Reading from 4690: heap size 484 MB, throughput 0.988397
Reading from 4690: heap size 486 MB, throughput 0.900328
Reading from 4690: heap size 487 MB, throughput 0.883045
Reading from 4689: heap size 1398 MB, throughput 0.978639
Reading from 4690: heap size 493 MB, throughput 0.989225
Reading from 4689: heap size 1412 MB, throughput 0.977643
Reading from 4690: heap size 495 MB, throughput 0.992479
Equal recommendation: 4461 MB each
Reading from 4689: heap size 1415 MB, throughput 0.97985
Reading from 4690: heap size 499 MB, throughput 0.991519
Reading from 4690: heap size 500 MB, throughput 0.987738
Reading from 4689: heap size 1415 MB, throughput 0.972494
Reading from 4690: heap size 499 MB, throughput 0.989153
Reading from 4689: heap size 1419 MB, throughput 0.971576
Reading from 4690: heap size 501 MB, throughput 0.988284
Equal recommendation: 4461 MB each
Reading from 4689: heap size 1406 MB, throughput 0.973178
Reading from 4690: heap size 503 MB, throughput 0.987179
Reading from 4689: heap size 1325 MB, throughput 0.967047
Reading from 4690: heap size 503 MB, throughput 0.992216
Reading from 4690: heap size 504 MB, throughput 0.927774
Reading from 4690: heap size 505 MB, throughput 0.88247
Reading from 4689: heap size 1403 MB, throughput 0.96412
Reading from 4690: heap size 511 MB, throughput 0.990358
Reading from 4690: heap size 512 MB, throughput 0.99218
Reading from 4689: heap size 1408 MB, throughput 0.968926
Equal recommendation: 4461 MB each
Reading from 4690: heap size 515 MB, throughput 0.992156
Reading from 4689: heap size 1413 MB, throughput 0.964074
Reading from 4690: heap size 517 MB, throughput 0.99158
Reading from 4689: heap size 1414 MB, throughput 0.971626
Reading from 4690: heap size 517 MB, throughput 0.989787
Reading from 4689: heap size 1421 MB, throughput 0.958381
Equal recommendation: 4461 MB each
Reading from 4690: heap size 519 MB, throughput 0.988467
Reading from 4689: heap size 1427 MB, throughput 0.950934
Reading from 4690: heap size 520 MB, throughput 0.986817
Reading from 4690: heap size 521 MB, throughput 0.984135
Reading from 4690: heap size 524 MB, throughput 0.907845
Reading from 4690: heap size 526 MB, throughput 0.978377
Reading from 4690: heap size 533 MB, throughput 0.992959
Equal recommendation: 4461 MB each
Reading from 4690: heap size 533 MB, throughput 0.99237
Reading from 4690: heap size 534 MB, throughput 0.990877
Reading from 4690: heap size 536 MB, throughput 0.989446
Equal recommendation: 4461 MB each
Reading from 4690: heap size 534 MB, throughput 0.989567
Reading from 4690: heap size 536 MB, throughput 0.9852
Reading from 4690: heap size 534 MB, throughput 0.980532
Reading from 4690: heap size 539 MB, throughput 0.896806
Reading from 4690: heap size 543 MB, throughput 0.982192
Reading from 4690: heap size 545 MB, throughput 0.99148
Equal recommendation: 4461 MB each
Reading from 4690: heap size 546 MB, throughput 0.991812
Reading from 4689: heap size 1435 MB, throughput 0.995162
Reading from 4690: heap size 548 MB, throughput 0.990999
Reading from 4690: heap size 546 MB, throughput 0.989285
Equal recommendation: 4461 MB each
Reading from 4689: heap size 1444 MB, throughput 0.976838
Reading from 4689: heap size 1438 MB, throughput 0.37991
Reading from 4689: heap size 1578 MB, throughput 0.992752
Reading from 4690: heap size 548 MB, throughput 0.989459
Reading from 4689: heap size 1627 MB, throughput 0.993013
Reading from 4689: heap size 1636 MB, throughput 0.992249
Reading from 4689: heap size 1650 MB, throughput 0.993239
Reading from 4689: heap size 1651 MB, throughput 0.986064
Reading from 4690: heap size 551 MB, throughput 0.993508
Reading from 4690: heap size 552 MB, throughput 0.909215
Reading from 4690: heap size 551 MB, throughput 0.913003
Reading from 4689: heap size 1644 MB, throughput 0.990493
Reading from 4690: heap size 553 MB, throughput 0.992681
Reading from 4689: heap size 1369 MB, throughput 0.989546
Equal recommendation: 4461 MB each
Reading from 4690: heap size 557 MB, throughput 0.992423
Reading from 4689: heap size 1623 MB, throughput 0.985447
Reading from 4690: heap size 559 MB, throughput 0.991548
Reading from 4689: heap size 1398 MB, throughput 0.982532
Reading from 4690: heap size 559 MB, throughput 0.990899
Reading from 4689: heap size 1597 MB, throughput 0.982501
Equal recommendation: 4461 MB each
Reading from 4690: heap size 561 MB, throughput 0.989879
Reading from 4689: heap size 1425 MB, throughput 0.980247
Reading from 4690: heap size 561 MB, throughput 0.992221
Reading from 4689: heap size 1573 MB, throughput 0.975775
Reading from 4690: heap size 562 MB, throughput 0.972666
Reading from 4690: heap size 562 MB, throughput 0.90628
Reading from 4689: heap size 1453 MB, throughput 0.973037
Reading from 4690: heap size 564 MB, throughput 0.99259
Equal recommendation: 4461 MB each
Reading from 4690: heap size 571 MB, throughput 0.992517
Reading from 4689: heap size 1575 MB, throughput 0.969776
Reading from 4690: heap size 572 MB, throughput 0.991897
Reading from 4689: heap size 1580 MB, throughput 0.969948
Reading from 4690: heap size 572 MB, throughput 0.990502
Reading from 4689: heap size 1586 MB, throughput 0.966496
Equal recommendation: 4461 MB each
Reading from 4690: heap size 574 MB, throughput 0.989267
Reading from 4689: heap size 1588 MB, throughput 0.966441
Reading from 4690: heap size 576 MB, throughput 0.99433
Reading from 4690: heap size 576 MB, throughput 0.937237
Reading from 4690: heap size 577 MB, throughput 0.941367
Reading from 4689: heap size 1596 MB, throughput 0.96222
Equal recommendation: 4461 MB each
Reading from 4690: heap size 579 MB, throughput 0.988663
Reading from 4689: heap size 1603 MB, throughput 0.957071
Reading from 4689: heap size 1613 MB, throughput 0.964242
Reading from 4690: heap size 583 MB, throughput 0.992391
Reading from 4689: heap size 1624 MB, throughput 0.955208
Reading from 4690: heap size 585 MB, throughput 0.991982
Equal recommendation: 4461 MB each
Reading from 4690: heap size 584 MB, throughput 0.990628
Reading from 4689: heap size 1639 MB, throughput 0.958417
Reading from 4690: heap size 586 MB, throughput 0.985542
Reading from 4689: heap size 1648 MB, throughput 0.958619
Reading from 4690: heap size 586 MB, throughput 0.983203
Reading from 4690: heap size 591 MB, throughput 0.90102
Reading from 4689: heap size 1664 MB, throughput 0.959773
Reading from 4690: heap size 596 MB, throughput 0.992786
Equal recommendation: 4461 MB each
Reading from 4689: heap size 1669 MB, throughput 0.955893
Reading from 4690: heap size 598 MB, throughput 0.993129
Reading from 4689: heap size 1685 MB, throughput 0.958314
Reading from 4690: heap size 599 MB, throughput 0.991115
Reading from 4689: heap size 1687 MB, throughput 0.955752
Equal recommendation: 4461 MB each
Reading from 4690: heap size 601 MB, throughput 0.98918
Reading from 4690: heap size 602 MB, throughput 0.988972
Reading from 4689: heap size 1703 MB, throughput 0.961564
Reading from 4690: heap size 603 MB, throughput 0.988059
Reading from 4690: heap size 606 MB, throughput 0.919453
Reading from 4689: heap size 1704 MB, throughput 0.957863
Reading from 4690: heap size 608 MB, throughput 0.993339
Equal recommendation: 4461 MB each
Reading from 4689: heap size 1721 MB, throughput 0.96439
Reading from 4690: heap size 614 MB, throughput 0.993702
Reading from 4689: heap size 1721 MB, throughput 0.723261
Reading from 4690: heap size 615 MB, throughput 0.914133
Equal recommendation: 4461 MB each
Reading from 4690: heap size 617 MB, throughput 0.995729
Reading from 4689: heap size 1804 MB, throughput 0.990494
Reading from 4690: heap size 619 MB, throughput 0.990247
Reading from 4690: heap size 623 MB, throughput 0.984368
Client 4690 died
Clients: 1
Recommendation: one client; give it all the memory
Reading from 4689: heap size 1804 MB, throughput 0.993707
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 4689: heap size 1809 MB, throughput 0.99541
Reading from 4689: heap size 1821 MB, throughput 0.900363
Reading from 4689: heap size 1806 MB, throughput 0.839525
Reading from 4689: heap size 1823 MB, throughput 0.822773
Reading from 4689: heap size 1859 MB, throughput 0.847099
Client 4689 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
