#include <sys/time.h>
#include "FakeVMSimpleThroughput.hpp"

FakeVMSimpleThroughput::FakeVMSimpleThroughput(std::function<unsigned long long(void)> getCapacityMB)
    : SimpleThroughput(), m_GetCapacityMB(getCapacityMB) {}

unsigned long long FakeVMSimpleThroughput::getHeapSizeB()
{
    return m_GetCapacityMB() * 1024 * 1024;
}

void FakeVMSimpleThroughput::logReading(unsigned long long heapSizeB, double throughput, unsigned long long gcTime, unsigned long long interGCTime)
{
    (void)heapSizeB;
    (void)gcTime;
    (void)interGCTime;
    printf("Throughput %g\n", throughput);
}
