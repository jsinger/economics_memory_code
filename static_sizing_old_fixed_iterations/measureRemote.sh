#!/bin/bash
# Measure benchmark runtimes on remote machines.
#
# Usage: measureRemote.sh hosts_file config_file
#

function usage()
{
    echo "Usage: $(basename "${0}") hosts_file config_file"
    exit 1
}

test -z "${1}" && usage
test ! -f "${1}" && usage
test -z "${2}" && usage
test ! -f "${2}" && usage

HOSTS_FILE="$(readlink -f "${1}")"
CONFIG_FILE="$(readlink -f "${2}")"
ITERATIONS='10'
N_FOR_NOVER='1'

HOST='callumc@sibu.dcs.gla.ac.uk'

REMOTE_DIR="economics_memory_code/static_sizing_old_fixed_iterations"
REMOTE_OUTDIR="${REMOTE_DIR}/measure_$(whoami)@$(hostname)_$(date '+%Y_%m_%d_%H_%M_%S')"
BENCHMARKS_FILE="$(readlink -f "$(mktemp --tmpdir=.)")"
ITERATIONS_FILE="$(readlink -f "$(mktemp --tmpdir=.)")"
INPUT_FILE="$(readlink -f "$(mktemp --tmpdir=.)")"
TIMESTAMPS_FILE="$(readlink -f "$(mktemp --tmpdir=.)")"

function setupBenchmark()
{
    BENCHMARK="${1}"
    MIN_HEAP="${2}"
    MAX_HEAP="${3}"
    INCREMENT="${4}"
    HEAP_SIZE="${MIN_HEAP}"

    echo "${BENCHMARK}" >> "${BENCHMARKS_FILE}"

    while [ "${HEAP_SIZE}" -le $(( MAX_HEAP + INCREMENT )) ]; do

        echo "${BENCHMARK}" "${HEAP_SIZE}" $(( MAX_HEAP + INCREMENT )) >> "${INPUT_FILE}"
        HEAP_SIZE=$(( HEAP_SIZE + INCREMENT ))

    done
}

while read benchmark min max inc; do
    setupBenchmark "${benchmark}" "${min}" "${max}" "${inc}"
done < "${CONFIG_FILE}"

echo "Remote run started by $(whoami)@$(hostname)" >> "${TIMESTAMPS_FILE}"
echo "Started: $(date '+%Y %m %d, %H:%M:%S')" >> "${TIMESTAMPS_FILE}"

ssh "${HOST}" mkdir -p "${REMOTE_OUTDIR}"
ssh "${HOST}" "${REMOTE_DIR}/../checkDacapo.sh"

scp "${BENCHMARKS_FILE}" "${HOST}:${REMOTE_OUTDIR}/benchmarks.txt"
scp "${ITERATIONS_FILE}" "${HOST}:${REMOTE_OUTDIR}/iterations.txt"
scp "${INPUT_FILE}" "${HOST}:${REMOTE_OUTDIR}/input.txt"
scp "${TIMESTAMPS_FILE}" "${HOST}:${REMOTE_OUTDIR}/timestamps.txt"
scp "${CONFIG_FILE}" "${HOST}:${REMOTE_OUTDIR}/config.txt"

distribute "${HOSTS_FILE}" "${INPUT_FILE}" 'ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -A callumc@sibu.dcs.gla.ac.uk ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no' "${REMOTE_DIR}/measure_remoteHarness.sh" "${REMOTE_OUTDIR}" "${ITERATIONS}" "${N_FOR_NOVER}"

rm "${BENCHMARKS_FILE}" "${ITERATIONS_FILE}" "${INPUT_FILE}" "${TIMESTAMPS_FILE}"

echo "Don't forget to collect and process the data once it's finished"
