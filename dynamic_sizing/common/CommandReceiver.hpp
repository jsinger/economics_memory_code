#ifndef COMMANDRECEIVER_HPP
#define COMMANDRECEIVER_HPP

#include "Messages.hpp"
#include "RecvSock.hpp"
#include "Utilities.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
    //! Receive commands from a management program (for use in the daemon).
    class CommandReceiver : public RecvSock<economem::Command>
    {
    public:
        CommandReceiver(const char* daemonName=economem::NAME_DEFAULT);
    };
}

/*! @} */

#endif
