economemd
    total memory: 1656 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub21_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 24462: heap size 9 MB, throughput 0.991536
Clients: 1
Client 24462 has a minimum heap size of 276 MB
Reading from 24463: heap size 9 MB, throughput 0.990348
Clients: 2
Client 24463 has a minimum heap size of 276 MB
Reading from 24463: heap size 9 MB, throughput 0.985014
Reading from 24462: heap size 9 MB, throughput 0.98313
Reading from 24462: heap size 9 MB, throughput 0.975034
Reading from 24463: heap size 9 MB, throughput 0.975344
Reading from 24463: heap size 9 MB, throughput 0.965039
Reading from 24462: heap size 9 MB, throughput 0.959011
Reading from 24463: heap size 11 MB, throughput 0.979967
Reading from 24462: heap size 11 MB, throughput 0.975288
Reading from 24462: heap size 11 MB, throughput 0.984671
Reading from 24463: heap size 11 MB, throughput 0.971211
Reading from 24462: heap size 17 MB, throughput 0.972314
Reading from 24463: heap size 17 MB, throughput 0.954828
Reading from 24462: heap size 17 MB, throughput 0.893017
Reading from 24463: heap size 17 MB, throughput 0.890356
Reading from 24463: heap size 29 MB, throughput 0.649277
Reading from 24462: heap size 30 MB, throughput 0.795452
Reading from 24463: heap size 31 MB, throughput 0.224587
Reading from 24462: heap size 31 MB, throughput 0.489496
Reading from 24463: heap size 35 MB, throughput 0.669791
Reading from 24462: heap size 34 MB, throughput 0.754307
Reading from 24463: heap size 48 MB, throughput 0.528239
Reading from 24462: heap size 44 MB, throughput 0.378047
Reading from 24462: heap size 50 MB, throughput 0.217666
Reading from 24463: heap size 50 MB, throughput 0.534273
Reading from 24463: heap size 64 MB, throughput 0.433032
Reading from 24462: heap size 50 MB, throughput 0.56498
Reading from 24462: heap size 70 MB, throughput 0.190158
Reading from 24463: heap size 69 MB, throughput 0.353953
Reading from 24462: heap size 71 MB, throughput 0.426498
Reading from 24462: heap size 96 MB, throughput 0.40142
Reading from 24463: heap size 71 MB, throughput 0.478923
Reading from 24462: heap size 97 MB, throughput 0.296272
Reading from 24463: heap size 100 MB, throughput 0.485665
Reading from 24462: heap size 100 MB, throughput 0.705404
Reading from 24462: heap size 103 MB, throughput 0.67431
Reading from 24463: heap size 100 MB, throughput 0.411184
Reading from 24463: heap size 131 MB, throughput 0.661432
Reading from 24462: heap size 107 MB, throughput 0.425013
Reading from 24463: heap size 131 MB, throughput 0.578138
Reading from 24462: heap size 137 MB, throughput 0.636705
Reading from 24462: heap size 144 MB, throughput 0.697786
Reading from 24463: heap size 132 MB, throughput 0.509618
Reading from 24462: heap size 144 MB, throughput 0.786626
Reading from 24463: heap size 166 MB, throughput 0.592512
Reading from 24463: heap size 172 MB, throughput 0.647938
Reading from 24463: heap size 172 MB, throughput 0.341143
Reading from 24462: heap size 148 MB, throughput 0.470173
Reading from 24463: heap size 175 MB, throughput 0.0917763
Reading from 24462: heap size 186 MB, throughput 0.722945
Reading from 24462: heap size 192 MB, throughput 0.664346
Reading from 24462: heap size 194 MB, throughput 0.605469
Reading from 24463: heap size 181 MB, throughput 0.501046
Reading from 24462: heap size 197 MB, throughput 0.647443
Reading from 24463: heap size 224 MB, throughput 0.77016
Reading from 24462: heap size 205 MB, throughput 0.858287
Reading from 24463: heap size 228 MB, throughput 0.905587
Reading from 24463: heap size 228 MB, throughput 0.891893
Reading from 24462: heap size 210 MB, throughput 0.849143
Reading from 24463: heap size 235 MB, throughput 0.83472
Reading from 24462: heap size 218 MB, throughput 0.805948
Reading from 24463: heap size 240 MB, throughput 0.820557
Reading from 24462: heap size 224 MB, throughput 0.774328
Reading from 24463: heap size 241 MB, throughput 0.762768
Reading from 24462: heap size 230 MB, throughput 0.753521
Reading from 24463: heap size 247 MB, throughput 0.690764
Reading from 24462: heap size 235 MB, throughput 0.661272
Reading from 24462: heap size 240 MB, throughput 0.674581
Reading from 24463: heap size 248 MB, throughput 0.756276
Reading from 24463: heap size 253 MB, throughput 0.758047
Reading from 24463: heap size 255 MB, throughput 0.70753
Reading from 24463: heap size 262 MB, throughput 0.697223
Reading from 24462: heap size 248 MB, throughput 0.656263
Reading from 24463: heap size 266 MB, throughput 0.715311
Reading from 24462: heap size 283 MB, throughput 0.706379
Reading from 24463: heap size 273 MB, throughput 0.802225
Reading from 24462: heap size 277 MB, throughput 0.768772
Reading from 24462: heap size 282 MB, throughput 0.742958
Reading from 24462: heap size 274 MB, throughput 0.725599
Reading from 24463: heap size 275 MB, throughput 0.875044
Reading from 24463: heap size 276 MB, throughput 0.837265
Reading from 24462: heap size 278 MB, throughput 0.480973
Reading from 24462: heap size 318 MB, throughput 0.609632
Reading from 24462: heap size 319 MB, throughput 0.683331
Reading from 24462: heap size 326 MB, throughput 0.885687
Reading from 24463: heap size 280 MB, throughput 0.677907
Reading from 24462: heap size 326 MB, throughput 0.860688
Reading from 24463: heap size 321 MB, throughput 0.754635
Reading from 24462: heap size 321 MB, throughput 0.845876
Reading from 24462: heap size 324 MB, throughput 0.781826
Reading from 24463: heap size 324 MB, throughput 0.832066
Reading from 24462: heap size 322 MB, throughput 0.757097
Reading from 24463: heap size 327 MB, throughput 0.840032
Reading from 24462: heap size 325 MB, throughput 0.851603
Reading from 24463: heap size 328 MB, throughput 0.748408
Reading from 24462: heap size 324 MB, throughput 0.835463
Reading from 24463: heap size 332 MB, throughput 0.732432
Reading from 24462: heap size 327 MB, throughput 0.803349
Reading from 24463: heap size 333 MB, throughput 0.682389
Reading from 24462: heap size 333 MB, throughput 0.773236
Reading from 24462: heap size 334 MB, throughput 0.668825
Reading from 24463: heap size 342 MB, throughput 0.701011
Reading from 24462: heap size 339 MB, throughput 0.642014
Reading from 24463: heap size 343 MB, throughput 0.676713
Reading from 24462: heap size 341 MB, throughput 0.632175
Reading from 24463: heap size 353 MB, throughput 0.872137
Numeric result:
Recommendation: 2 clients, utility 0.488814:
    h1: 710.903 MB (U(h) = 0.522998*h^0.042416)
    h2: 945.097 MB (U(h) = 0.480676*h^0.056404)
Recommendation: 2 clients, utility 0.488814:
    h1: 710.796 MB (U(h) = 0.522998*h^0.042416)
    h2: 945.204 MB (U(h) = 0.480676*h^0.056404)
Reading from 24462: heap size 346 MB, throughput 0.958876
Reading from 24463: heap size 353 MB, throughput 0.970513
Reading from 24462: heap size 348 MB, throughput 0.967223
Reading from 24463: heap size 355 MB, throughput 0.962123
Reading from 24462: heap size 355 MB, throughput 0.973704
Reading from 24463: heap size 359 MB, throughput 0.968998
Reading from 24462: heap size 357 MB, throughput 0.87259
Reading from 24463: heap size 358 MB, throughput 0.873269
Reading from 24462: heap size 404 MB, throughput 0.979804
Reading from 24463: heap size 405 MB, throughput 0.982914
Reading from 24462: heap size 406 MB, throughput 0.986621
Reading from 24463: heap size 409 MB, throughput 0.987707
Reading from 24462: heap size 415 MB, throughput 0.98805
Numeric result:
Recommendation: 2 clients, utility 0.603447:
    h1: 803.787 MB (U(h) = 0.439231*h^0.0856869)
    h2: 852.213 MB (U(h) = 0.41955*h^0.0908464)
Recommendation: 2 clients, utility 0.603447:
    h1: 803.8 MB (U(h) = 0.439231*h^0.0856869)
    h2: 852.2 MB (U(h) = 0.41955*h^0.0908464)
Reading from 24463: heap size 412 MB, throughput 0.989879
Reading from 24462: heap size 415 MB, throughput 0.987688
Reading from 24462: heap size 416 MB, throughput 0.989114
Reading from 24463: heap size 416 MB, throughput 0.991098
Reading from 24462: heap size 418 MB, throughput 0.987433
Reading from 24463: heap size 417 MB, throughput 0.991463
Reading from 24463: heap size 414 MB, throughput 0.988577
Reading from 24462: heap size 414 MB, throughput 0.986996
Reading from 24462: heap size 384 MB, throughput 0.984857
Reading from 24463: heap size 416 MB, throughput 0.986884
Reading from 24462: heap size 413 MB, throughput 0.97676
Reading from 24463: heap size 409 MB, throughput 0.984026
Numeric result:
Recommendation: 2 clients, utility 0.681024:
    h1: 813.246 MB (U(h) = 0.398331*h^0.109347)
    h2: 842.754 MB (U(h) = 0.382978*h^0.113314)
Recommendation: 2 clients, utility 0.681024:
    h1: 813.247 MB (U(h) = 0.398331*h^0.109347)
    h2: 842.753 MB (U(h) = 0.382978*h^0.113314)
Reading from 24462: heap size 415 MB, throughput 0.97582
Reading from 24462: heap size 417 MB, throughput 0.961546
Reading from 24462: heap size 421 MB, throughput 0.934
Reading from 24462: heap size 429 MB, throughput 0.881482
Reading from 24462: heap size 436 MB, throughput 0.909203
Reading from 24463: heap size 413 MB, throughput 0.985717
Reading from 24463: heap size 413 MB, throughput 0.980906
Reading from 24463: heap size 413 MB, throughput 0.961253
Reading from 24463: heap size 409 MB, throughput 0.937248
Reading from 24463: heap size 418 MB, throughput 0.898371
Reading from 24463: heap size 428 MB, throughput 0.926096
Reading from 24462: heap size 442 MB, throughput 0.973296
Reading from 24463: heap size 432 MB, throughput 0.97396
Reading from 24462: heap size 447 MB, throughput 0.981446
Reading from 24463: heap size 431 MB, throughput 0.98058
Reading from 24462: heap size 446 MB, throughput 0.981795
Reading from 24463: heap size 435 MB, throughput 0.983343
Reading from 24462: heap size 450 MB, throughput 0.980992
Reading from 24463: heap size 433 MB, throughput 0.981207
Numeric result:
Recommendation: 2 clients, utility 0.767161:
    h1: 812.122 MB (U(h) = 0.362202*h^0.131936)
    h2: 843.878 MB (U(h) = 0.347412*h^0.137106)
Recommendation: 2 clients, utility 0.767161:
    h1: 812.089 MB (U(h) = 0.362202*h^0.131936)
    h2: 843.911 MB (U(h) = 0.347412*h^0.137106)
Reading from 24462: heap size 448 MB, throughput 0.984228
Reading from 24463: heap size 436 MB, throughput 0.981902
Reading from 24462: heap size 451 MB, throughput 0.983447
Reading from 24463: heap size 433 MB, throughput 0.981774
Reading from 24462: heap size 448 MB, throughput 0.984015
Reading from 24463: heap size 436 MB, throughput 0.981605
Reading from 24462: heap size 451 MB, throughput 0.985237
Reading from 24463: heap size 433 MB, throughput 0.982209
Reading from 24462: heap size 449 MB, throughput 0.984115
Reading from 24463: heap size 435 MB, throughput 0.981408
Reading from 24463: heap size 433 MB, throughput 0.98049
Reading from 24462: heap size 451 MB, throughput 0.986286
Numeric result:
Recommendation: 2 clients, utility 0.788126:
    h1: 826.598 MB (U(h) = 0.350492*h^0.139675)
    h2: 829.402 MB (U(h) = 0.343096*h^0.140137)
Recommendation: 2 clients, utility 0.788126:
    h1: 826.633 MB (U(h) = 0.350492*h^0.139675)
    h2: 829.367 MB (U(h) = 0.343096*h^0.140137)
Reading from 24462: heap size 451 MB, throughput 0.981636
Reading from 24462: heap size 452 MB, throughput 0.968747
Reading from 24462: heap size 455 MB, throughput 0.949329
Reading from 24463: heap size 435 MB, throughput 0.986473
Reading from 24462: heap size 455 MB, throughput 0.971141
Reading from 24463: heap size 436 MB, throughput 0.972736
Reading from 24463: heap size 437 MB, throughput 0.959918
Reading from 24463: heap size 443 MB, throughput 0.937101
Reading from 24463: heap size 443 MB, throughput 0.962252
Reading from 24462: heap size 462 MB, throughput 0.984958
Reading from 24463: heap size 452 MB, throughput 0.987475
Reading from 24462: heap size 463 MB, throughput 0.988197
Reading from 24463: heap size 453 MB, throughput 0.988683
Reading from 24462: heap size 467 MB, throughput 0.989631
Reading from 24463: heap size 457 MB, throughput 0.988877
Numeric result:
Recommendation: 2 clients, utility 0.834616:
    h1: 820.574 MB (U(h) = 0.335366*h^0.149989)
    h2: 835.426 MB (U(h) = 0.325609*h^0.152704)
Recommendation: 2 clients, utility 0.834616:
    h1: 820.574 MB (U(h) = 0.335366*h^0.149989)
    h2: 835.426 MB (U(h) = 0.325609*h^0.152704)
Reading from 24462: heap size 468 MB, throughput 0.988798
Reading from 24463: heap size 458 MB, throughput 0.987769
Reading from 24462: heap size 469 MB, throughput 0.987689
Reading from 24463: heap size 458 MB, throughput 0.987954
Reading from 24462: heap size 470 MB, throughput 0.986877
Reading from 24463: heap size 460 MB, throughput 0.988864
Reading from 24462: heap size 469 MB, throughput 0.986216
Reading from 24463: heap size 458 MB, throughput 0.987364
Reading from 24463: heap size 460 MB, throughput 0.986081
Reading from 24462: heap size 470 MB, throughput 0.989567
Numeric result:
Recommendation: 2 clients, utility 0.855539:
    h1: 823.42 MB (U(h) = 0.327489*h^0.15551)
    h2: 832.58 MB (U(h) = 0.319476*h^0.157236)
Recommendation: 2 clients, utility 0.855539:
    h1: 823.432 MB (U(h) = 0.327489*h^0.15551)
    h2: 832.568 MB (U(h) = 0.319476*h^0.157236)
Reading from 24462: heap size 473 MB, throughput 0.984838
Reading from 24462: heap size 473 MB, throughput 0.974495
Reading from 24462: heap size 477 MB, throughput 0.966089
Reading from 24463: heap size 463 MB, throughput 0.987682
Reading from 24463: heap size 463 MB, throughput 0.979598
Reading from 24463: heap size 464 MB, throughput 0.963531
Reading from 24463: heap size 466 MB, throughput 0.95769
Reading from 24462: heap size 478 MB, throughput 0.986041
Reading from 24463: heap size 475 MB, throughput 0.983792
Reading from 24462: heap size 484 MB, throughput 0.988474
Reading from 24463: heap size 475 MB, throughput 0.989417
Reading from 24462: heap size 485 MB, throughput 0.989983
Reading from 24463: heap size 477 MB, throughput 0.986652
Numeric result:
Recommendation: 2 clients, utility 0.890381:
    h1: 820.138 MB (U(h) = 0.317046*h^0.162983)
    h2: 835.862 MB (U(h) = 0.307714*h^0.166108)
Recommendation: 2 clients, utility 0.890381:
    h1: 820.137 MB (U(h) = 0.317046*h^0.162983)
    h2: 835.863 MB (U(h) = 0.307714*h^0.166108)
Reading from 24462: heap size 485 MB, throughput 0.989632
Reading from 24463: heap size 479 MB, throughput 0.98721
Reading from 24462: heap size 487 MB, throughput 0.988725
Reading from 24463: heap size 479 MB, throughput 0.987083
Reading from 24462: heap size 486 MB, throughput 0.988123
Reading from 24463: heap size 481 MB, throughput 0.989014
Reading from 24462: heap size 488 MB, throughput 0.988377
Reading from 24463: heap size 483 MB, throughput 0.988054
Numeric result:
Recommendation: 2 clients, utility 0.909603:
    h1: 818.305 MB (U(h) = 0.311551*h^0.166994)
    h2: 837.695 MB (U(h) = 0.301415*h^0.170956)
Recommendation: 2 clients, utility 0.909603:
    h1: 818.292 MB (U(h) = 0.311551*h^0.166994)
    h2: 837.708 MB (U(h) = 0.301415*h^0.170956)
Reading from 24462: heap size 491 MB, throughput 0.990763
Reading from 24462: heap size 491 MB, throughput 0.984602
Reading from 24462: heap size 492 MB, throughput 0.973634
Reading from 24463: heap size 483 MB, throughput 0.990282
Reading from 24463: heap size 485 MB, throughput 0.984681
Reading from 24463: heap size 486 MB, throughput 0.972465
Reading from 24462: heap size 494 MB, throughput 0.981389
Reading from 24463: heap size 491 MB, throughput 0.978367
Reading from 24463: heap size 493 MB, throughput 0.988348
Reading from 24462: heap size 501 MB, throughput 0.989042
Reading from 24463: heap size 496 MB, throughput 0.990445
Reading from 24462: heap size 503 MB, throughput 0.99062
Numeric result:
Recommendation: 2 clients, utility 0.936142:
    h1: 816.343 MB (U(h) = 0.30405*h^0.172547)
    h2: 839.657 MB (U(h) = 0.293083*h^0.177481)
Recommendation: 2 clients, utility 0.936142:
    h1: 816.328 MB (U(h) = 0.30405*h^0.172547)
    h2: 839.672 MB (U(h) = 0.293083*h^0.177481)
Reading from 24463: heap size 498 MB, throughput 0.986447
Reading from 24462: heap size 503 MB, throughput 0.988968
Reading from 24462: heap size 506 MB, throughput 0.988598
Reading from 24463: heap size 498 MB, throughput 0.989428
Reading from 24462: heap size 506 MB, throughput 0.987718
Reading from 24463: heap size 500 MB, throughput 0.988764
Reading from 24462: heap size 507 MB, throughput 0.987047
Reading from 24463: heap size 502 MB, throughput 0.98854
Reading from 24462: heap size 510 MB, throughput 0.988739
Numeric result:
Recommendation: 2 clients, utility 0.950365:
    h1: 815.347 MB (U(h) = 0.3001*h^0.175502)
    h2: 840.653 MB (U(h) = 0.288716*h^0.180949)
Recommendation: 2 clients, utility 0.950365:
    h1: 815.348 MB (U(h) = 0.3001*h^0.175502)
    h2: 840.652 MB (U(h) = 0.288716*h^0.180949)
Reading from 24462: heap size 511 MB, throughput 0.980962
Reading from 24462: heap size 511 MB, throughput 0.974681
Reading from 24463: heap size 502 MB, throughput 0.992031
Reading from 24463: heap size 505 MB, throughput 0.988197
Reading from 24463: heap size 506 MB, throughput 0.973839
Reading from 24462: heap size 514 MB, throughput 0.989914
Reading from 24463: heap size 511 MB, throughput 0.983344
Reading from 24462: heap size 518 MB, throughput 0.992163
Reading from 24463: heap size 513 MB, throughput 0.991137
Reading from 24462: heap size 519 MB, throughput 0.993019
Reading from 24463: heap size 517 MB, throughput 0.991498
Numeric result:
Recommendation: 2 clients, utility 0.968847:
    h1: 812.659 MB (U(h) = 0.295473*h^0.178994)
    h2: 843.341 MB (U(h) = 0.282729*h^0.185751)
Recommendation: 2 clients, utility 0.968847:
    h1: 812.662 MB (U(h) = 0.295473*h^0.178994)
    h2: 843.338 MB (U(h) = 0.282729*h^0.185751)
Reading from 24462: heap size 518 MB, throughput 0.991062
Reading from 24463: heap size 518 MB, throughput 0.990693
Reading from 24462: heap size 521 MB, throughput 0.990897
Reading from 24463: heap size 517 MB, throughput 0.988973
Reading from 24462: heap size 523 MB, throughput 0.989572
Reading from 24463: heap size 519 MB, throughput 0.989175
Reading from 24462: heap size 524 MB, throughput 0.991025
Reading from 24462: heap size 527 MB, throughput 0.985326
Reading from 24462: heap size 528 MB, throughput 0.979424
Numeric result:
Recommendation: 2 clients, utility 1.15129:
    h1: 1066.78 MB (U(h) = 0.116391*h^0.339429)
    h2: 589.224 MB (U(h) = 0.280585*h^0.187486)
Recommendation: 2 clients, utility 1.15129:
    h1: 1066.77 MB (U(h) = 0.116391*h^0.339429)
    h2: 589.235 MB (U(h) = 0.280585*h^0.187486)
Reading from 24463: heap size 522 MB, throughput 0.987614
Reading from 24463: heap size 523 MB, throughput 0.984339
Reading from 24463: heap size 529 MB, throughput 0.976749
Reading from 24462: heap size 537 MB, throughput 0.989777
Reading from 24463: heap size 531 MB, throughput 0.983113
Reading from 24462: heap size 537 MB, throughput 0.991049
Reading from 24463: heap size 537 MB, throughput 0.989509
Reading from 24462: heap size 537 MB, throughput 0.991067
Reading from 24463: heap size 538 MB, throughput 0.990293
Numeric result:
Recommendation: 2 clients, utility 1.21702:
    h1: 1103.46 MB (U(h) = 0.0900693*h^0.382885)
    h2: 552.543 MB (U(h) = 0.275368*h^0.191725)
Recommendation: 2 clients, utility 1.21702:
    h1: 1103.46 MB (U(h) = 0.0900693*h^0.382885)
    h2: 552.543 MB (U(h) = 0.275368*h^0.191725)
Reading from 24462: heap size 539 MB, throughput 0.990441
Reading from 24463: heap size 538 MB, throughput 0.991924
Reading from 24462: heap size 540 MB, throughput 0.990251
Reading from 24463: heap size 540 MB, throughput 0.990163
Reading from 24462: heap size 541 MB, throughput 0.991205
Reading from 24463: heap size 539 MB, throughput 0.989977
Reading from 24462: heap size 546 MB, throughput 0.986502
Reading from 24462: heap size 546 MB, throughput 0.978104
Numeric result:
Recommendation: 2 clients, utility 1.37246:
    h1: 1187.65 MB (U(h) = 0.0478229*h^0.489474)
    h2: 468.352 MB (U(h) = 0.273776*h^0.193025)
Recommendation: 2 clients, utility 1.37246:
    h1: 1187.65 MB (U(h) = 0.0478229*h^0.489474)
    h2: 468.352 MB (U(h) = 0.273776*h^0.193025)
Reading from 24463: heap size 541 MB, throughput 0.992449
Reading from 24463: heap size 522 MB, throughput 0.986834
Reading from 24463: heap size 520 MB, throughput 0.981212
Reading from 24462: heap size 551 MB, throughput 0.98784
Reading from 24463: heap size 517 MB, throughput 0.98925
Reading from 24462: heap size 553 MB, throughput 0.990771
Reading from 24463: heap size 488 MB, throughput 0.990023
Reading from 24462: heap size 555 MB, throughput 0.99115
Reading from 24463: heap size 503 MB, throughput 0.990569
Numeric result:
Recommendation: 2 clients, utility 1.30934:
    h1: 1053.8 MB (U(h) = 0.0712058*h^0.422759)
    h2: 602.202 MB (U(h) = 0.206581*h^0.241586)
Recommendation: 2 clients, utility 1.30934:
    h1: 1053.8 MB (U(h) = 0.0712058*h^0.422759)
    h2: 602.197 MB (U(h) = 0.206581*h^0.241586)
Reading from 24462: heap size 557 MB, throughput 0.9911
Reading from 24463: heap size 482 MB, throughput 0.99188
Reading from 24463: heap size 504 MB, throughput 0.990565
Reading from 24462: heap size 557 MB, throughput 0.99046
Reading from 24463: heap size 502 MB, throughput 0.988799
Reading from 24462: heap size 559 MB, throughput 0.991947
Reading from 24462: heap size 561 MB, throughput 0.98707
Reading from 24462: heap size 562 MB, throughput 0.978193
Numeric result:
Recommendation: 2 clients, utility 1.40229:
    h1: 872.011 MB (U(h) = 0.0771919*h^0.409004)
    h2: 783.989 MB (U(h) = 0.0982366*h^0.367724)
Recommendation: 2 clients, utility 1.40229:
    h1: 872.005 MB (U(h) = 0.0771919*h^0.409004)
    h2: 783.995 MB (U(h) = 0.0982366*h^0.367724)
Reading from 24463: heap size 505 MB, throughput 0.990746
Reading from 24463: heap size 505 MB, throughput 0.987912
Reading from 24463: heap size 504 MB, throughput 0.978738
Reading from 24463: heap size 507 MB, throughput 0.985367
Reading from 24462: heap size 570 MB, throughput 0.990907
Reading from 24463: heap size 513 MB, throughput 0.99008
Reading from 24462: heap size 571 MB, throughput 0.991732
Reading from 24463: heap size 514 MB, throughput 0.990696
Numeric result:
Recommendation: 2 clients, utility 1.44786:
    h1: 883.609 MB (U(h) = 0.0640334*h^0.439539)
    h2: 772.391 MB (U(h) = 0.0890779*h^0.384217)
Recommendation: 2 clients, utility 1.44786:
    h1: 883.607 MB (U(h) = 0.0640334*h^0.439539)
    h2: 772.393 MB (U(h) = 0.0890779*h^0.384217)
Reading from 24462: heap size 571 MB, throughput 0.99196
Reading from 24463: heap size 515 MB, throughput 0.991073
Reading from 24462: heap size 574 MB, throughput 0.988743
Reading from 24463: heap size 517 MB, throughput 0.990537
Reading from 24462: heap size 577 MB, throughput 0.988794
Reading from 24463: heap size 517 MB, throughput 0.990119
Reading from 24462: heap size 578 MB, throughput 0.985418
Reading from 24462: heap size 581 MB, throughput 0.980219
Numeric result:
Recommendation: 2 clients, utility 1.47165:
    h1: 804.806 MB (U(h) = 0.0756656*h^0.411654)
    h2: 851.194 MB (U(h) = 0.0656269*h^0.435383)
Recommendation: 2 clients, utility 1.47165:
    h1: 804.805 MB (U(h) = 0.0756656*h^0.411654)
    h2: 851.195 MB (U(h) = 0.0656269*h^0.435383)
Reading from 24463: heap size 518 MB, throughput 0.989185
Reading from 24462: heap size 583 MB, throughput 0.987303
Reading from 24463: heap size 519 MB, throughput 0.98844
Reading from 24463: heap size 521 MB, throughput 0.983041
Reading from 24463: heap size 524 MB, throughput 0.979395
Reading from 24462: heap size 591 MB, throughput 0.991625
Reading from 24463: heap size 526 MB, throughput 0.989759
Reading from 24462: heap size 592 MB, throughput 0.992538
Reading from 24463: heap size 529 MB, throughput 0.992623
Numeric result:
Recommendation: 2 clients, utility 1.60579:
    h1: 763.896 MB (U(h) = 0.0591635*h^0.45126)
    h2: 892.104 MB (U(h) = 0.0378244*h^0.526999)
Recommendation: 2 clients, utility 1.60579:
    h1: 763.895 MB (U(h) = 0.0591635*h^0.45126)
    h2: 892.105 MB (U(h) = 0.0378244*h^0.526999)
Reading from 24463: heap size 531 MB, throughput 0.985624
Reading from 24462: heap size 591 MB, throughput 0.991116
Reading from 24463: heap size 541 MB, throughput 0.994259
Reading from 24462: heap size 593 MB, throughput 0.990302
Reading from 24463: heap size 543 MB, throughput 0.993721
Reading from 24462: heap size 597 MB, throughput 0.989926
Reading from 24462: heap size 597 MB, throughput 0.984194
Numeric result:
Recommendation: 2 clients, utility 1.62454:
    h1: 790.714 MB (U(h) = 0.0491327*h^0.481067)
    h2: 865.286 MB (U(h) = 0.037932*h^0.526431)
Recommendation: 2 clients, utility 1.62454:
    h1: 790.718 MB (U(h) = 0.0491327*h^0.481067)
    h2: 865.282 MB (U(h) = 0.037932*h^0.526431)
Reading from 24463: heap size 548 MB, throughput 0.993698
Reading from 24462: heap size 602 MB, throughput 0.990341
Reading from 24463: heap size 549 MB, throughput 0.98864
Reading from 24463: heap size 547 MB, throughput 0.979366
Reading from 24463: heap size 551 MB, throughput 0.984858
Reading from 24462: heap size 603 MB, throughput 0.992074
Reading from 24463: heap size 556 MB, throughput 0.990179
Reading from 24462: heap size 605 MB, throughput 0.992576
Numeric result:
Recommendation: 2 clients, utility 1.72563:
    h1: 719.96 MB (U(h) = 0.0500352*h^0.477604)
    h2: 936.04 MB (U(h) = 0.0212824*h^0.620938)
Recommendation: 2 clients, utility 1.72563:
    h1: 719.966 MB (U(h) = 0.0500352*h^0.477604)
    h2: 936.034 MB (U(h) = 0.0212824*h^0.620938)
Reading from 24463: heap size 560 MB, throughput 0.991129
Reading from 24463: heap size 559 MB, throughput 0.990708
Reading from 24462: heap size 607 MB, throughput 0.99243
Reading from 24463: heap size 561 MB, throughput 0.989293
Reading from 24462: heap size 608 MB, throughput 0.993552
Reading from 24462: heap size 609 MB, throughput 0.990886
Reading from 24462: heap size 611 MB, throughput 0.989392
Reading from 24463: heap size 565 MB, throughput 0.99095
Numeric result:
Recommendation: 2 clients, utility 1.44425:
    h1: 908.318 MB (U(h) = 0.0595061*h^0.448993)
    h2: 747.682 MB (U(h) = 0.0988006*h^0.369589)
Recommendation: 2 clients, utility 1.44425:
    h1: 908.318 MB (U(h) = 0.0595061*h^0.448993)
    h2: 747.682 MB (U(h) = 0.0988006*h^0.369589)
Reading from 24463: heap size 566 MB, throughput 0.989463
Reading from 24463: heap size 570 MB, throughput 0.982774
Reading from 24463: heap size 573 MB, throughput 0.985949
Reading from 24462: heap size 613 MB, throughput 0.993195
Reading from 24463: heap size 580 MB, throughput 0.990663
Reading from 24462: heap size 614 MB, throughput 0.993052
Numeric result:
Recommendation: 2 clients, utility 1.41442:
    h1: 786.131 MB (U(h) = 0.0951984*h^0.373233)
    h2: 869.869 MB (U(h) = 0.0753904*h^0.412981)
Recommendation: 2 clients, utility 1.41442:
    h1: 786.14 MB (U(h) = 0.0951984*h^0.373233)
    h2: 869.86 MB (U(h) = 0.0753904*h^0.412981)
Reading from 24463: heap size 582 MB, throughput 0.99244
Reading from 24462: heap size 615 MB, throughput 0.992661
Reading from 24463: heap size 579 MB, throughput 0.99113
Reading from 24462: heap size 614 MB, throughput 0.986841
Reading from 24463: heap size 583 MB, throughput 0.990178
Reading from 24462: heap size 638 MB, throughput 0.992891
Reading from 24462: heap size 641 MB, throughput 0.988711
Numeric result:
Recommendation: 2 clients, utility 1.36488:
    h1: 715.974 MB (U(h) = 0.143854*h^0.306797)
    h2: 940.026 MB (U(h) = 0.0801155*h^0.402801)
Recommendation: 2 clients, utility 1.36488:
    h1: 715.976 MB (U(h) = 0.143854*h^0.306797)
    h2: 940.024 MB (U(h) = 0.0801155*h^0.402801)
Reading from 24463: heap size 584 MB, throughput 0.988446
Reading from 24463: heap size 586 MB, throughput 0.985962
Reading from 24463: heap size 594 MB, throughput 0.980365
Reading from 24462: heap size 642 MB, throughput 0.991534
Reading from 24463: heap size 595 MB, throughput 0.988899
Reading from 24462: heap size 649 MB, throughput 0.994383
Reading from 24463: heap size 601 MB, throughput 0.991925
Numeric result:
Recommendation: 2 clients, utility 1.32352:
    h1: 713.076 MB (U(h) = 0.166154*h^0.283536)
    h2: 942.924 MB (U(h) = 0.0948622*h^0.37491)
Recommendation: 2 clients, utility 1.32352:
    h1: 713.097 MB (U(h) = 0.166154*h^0.283536)
    h2: 942.903 MB (U(h) = 0.0948622*h^0.37491)
Reading from 24462: heap size 650 MB, throughput 0.993868
Reading from 24463: heap size 603 MB, throughput 0.991919
Reading from 24462: heap size 651 MB, throughput 0.993728
Reading from 24463: heap size 604 MB, throughput 0.991459
Reading from 24462: heap size 653 MB, throughput 0.994255
Reading from 24462: heap size 655 MB, throughput 0.990478
Numeric result:
Recommendation: 2 clients, utility 1.28032:
    h1: 655.4 MB (U(h) = 0.231934*h^0.22999)
    h2: 1000.6 MB (U(h) = 0.109843*h^0.351107)
Recommendation: 2 clients, utility 1.28032:
    h1: 655.421 MB (U(h) = 0.231934*h^0.22999)
    h2: 1000.58 MB (U(h) = 0.109843*h^0.351107)
Reading from 24463: heap size 606 MB, throughput 0.991806
Client 24462 died
Clients: 1
Reading from 24463: heap size 608 MB, throughput 0.991527
Reading from 24463: heap size 609 MB, throughput 0.986837
Client 24463 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
