module Economem.BenchmarkTimer (BenchmarkTimerDir(..),
                                newBenchmarkTimer,
                                resumeBenchmarkTimer,
                                defaultRuns,
                                AuxiliaryProcessLauncher) where

import Control.Applicative
import Control.Exception
import Economem.Cgroup
import Economem.Common
import Economem.Benchmark
import Economem.Daemon
import Economem.Experiment
import Economem.Files
import Economem.Java
import Economem.Log
import qualified Economem.MetadataDir as M
import qualified Economem.MinHeap as MH
import Economem.Process
import Economem.Records
import Economem.ResourceUsageFile
import Economem.Run
import Text.Printf


defaultRuns :: Integer
defaultRuns = 5


type AuxiliaryProcessLauncher = Experiment -> RunNum -> ProcessNum -> IO ProcessLauncher


data BenchmarkTimerDir =
  BenchmarkTimerDir { dirPath :: FilePath,
                      benchmarksFile :: CsvFile BenchmarkThreadsIters,
                      maxHeapFile :: IntFile,
                      javaVersionFile :: StringFile,
                      javaNameFile :: StringFile,
                      throughputsFile :: LinesFile ThroughputType,
                      minHeapsFile :: CsvFile BenchmarkThreadsMB,
                      forceGrowthFile :: WholeFile Bool,
                      pidNamesFile :: RunNum -> CsvFile PidName }

instance Path BenchmarkTimerDir where
  getPath = dirPath
instance Dir BenchmarkTimerDir where
  asDir path =
    BenchmarkTimerDir { dirPath = getPath path,
                        benchmarksFile = CsvFile (path </> "benchmarks.csv"),
                        maxHeapFile = WholeFile (path </> "max_heap_MB.txt"),
                        javaVersionFile = WholeFile (path </> "java_version.txt"),
                        javaNameFile = WholeFile (path </> "java_name.txt"),
                        throughputsFile = LinesFile (path </> "throughput_types.txt"),
                        minHeapsFile = CsvFile (path </> "min_heaps.csv"),
                        forceGrowthFile = WholeFile (path </> "force_growth.txt"),
                        pidNamesFile = \run -> CsvFile (path </> (printf "pid_names_run%s.pdf" (pretty run) :: FilePath)) }

  postprocessingActions = [generatePidNamesFile]

newBenchmarkTimer :: String ->
                     Input BenchmarkThreadsIters ->
                     DataSize ->
                     Integer ->
                     Java ->
                     Maybe (Cgroup, DataSize) ->
                     Seconds ->
                     [AuxiliaryProcessLauncher] ->
                     [ThroughputType] ->
                     Maybe (Input BenchmarkThreadsMB) ->
                     Bool ->
                     Maybe SubexpInfo ->
                     [String] ->
                     IO Experiment
newBenchmarkTimer name input maxHeap runs j cg auxProcDelay auxProcs throughputs minHeaps forceGrowth info extraVMArgs = do
  (expDir, num, parentLog) <- newExperimentProperties name info
  makeBenchmarkTimer
    name
    expDir
    num
    input
    maxHeap
    runs
    j
    cg
    auxProcDelay
    auxProcs
    parentLog
    throughputs
    minHeaps
    forceGrowth
    extraVMArgs

resumeBenchmarkTimer :: FilePath -> IO Experiment
resumeBenchmarkTimer =
  resumeExperiment
  (\expDir -> do let d = asDir expDir
                 name <- get (M.experimentNameFile (metadataDir expDir))
                 let input = FromFile (benchmarksFile d)
                 maxHeap <- (`DataSize` MB) <$> get (maxHeapFile d)
                 runs <- get (M.numRunsFile (metadataDir expDir))
                 java <- javaFromName =<< get (javaNameFile d)
                 throughputs <- get (throughputsFile d)
                 minHeaps <- maybeGet (minHeapsFile d)
                 forceGrowth <- get (forceGrowthFile d)
                 makeBenchmarkTimer
                   name
                   expDir
                   topLevel
                   input
                   maxHeap
                   runs
                   java
                   Nothing
                   0
                   []
                   Nothing
                   throughputs
                   minHeaps
                   forceGrowth
                   [])


makeBenchmarkTimer :: String ->
                      ExperimentDir ->
                      ExperimentNum ->
                      Input BenchmarkThreadsIters ->
                      DataSize ->
                      Integer ->
                      Java ->
                      Maybe (Cgroup, DataSize) ->
                      Seconds ->
                      [AuxiliaryProcessLauncher] ->
                      Maybe Log ->
                      [ThroughputType] ->
                      Maybe (Input BenchmarkThreadsMB) ->
                      Bool ->
                      [String] ->
                      IO Experiment
makeBenchmarkTimer name expDir expNum inputLoader maxHeap runs j cg auxProcDelay auxProcs parentLog throughputs minHeapsLoader forceGrowth extraVMArgs = do
  let ex = makeExperiment name expDir expNum runs parentLog
  let bmDir = asDir expDir
  input <- loadInput inputLoader
  minHeaps <- case minHeapsLoader of
               Just l -> Just <$> loadInput l
               Nothing -> return Nothing
  return (ex { experimentIntro = intro input,
               saveAdditionalStaticMetadata = saveStatic bmDir input minHeaps,
               canResume = canResumeBenchmark bmDir,
               setup = setupBenchmark,
               createRun = createRunBenchmark bmDir input minHeaps })

  where
    intro :: [BenchmarkThreadsIters] -> [String]
    intro input =
      ["Running benchmarks:"] ++
      map (\s -> "    " ++ pretty s) input ++
      ["With max heap: " ++ pretty (toMB maxHeap)] ++
      if forceGrowth then ["Forcing heap growth"] else []

    saveStatic :: BenchmarkTimerDir -> [BenchmarkThreadsIters] -> Maybe [BenchmarkThreadsMB] -> IO ()
    saveStatic d input minHeaps = do
      input |> benchmarksFile d
      sizeToInt (toMB maxHeap) |> maxHeapFile d
      javaVersion j (getPath (javaVersionFile d))
      javaName j |> javaNameFile d
      throughputs |> throughputsFile d
      case minHeaps of
       Just m -> put (minHeapsFile d) m
       Nothing -> return ()
      forceGrowth |> forceGrowthFile d

    canResumeBenchmark :: BenchmarkTimerDir -> IO Bool
    canResumeBenchmark d =
      allExistInDir d [getPath . benchmarksFile,
                       getPath . maxHeapFile,
                       getPath . javaNameFile,
                       getPath . throughputsFile,
                       getPath . forceGrowthFile]

    setupBenchmark :: Experiment -> IO ()
    setupBenchmark e = do
      checkBenchmarkPrerequisites
      putLog (experimentLog e) ("Using Java version: " ++ javaName j)


    createRunBenchmark :: BenchmarkTimerDir ->
                          [BenchmarkThreadsIters] ->
                          Maybe [BenchmarkThreadsMB] ->
                          Experiment ->
                          RunNum ->
                          IO Run
    createRunBenchmark bmDir input minHeaps e run = do
      launchers <- mapM createLauncher (zip input (upTo (fromIntegral (length input))))
      auxLaunchers <- mapM (\(f, num) -> f e run num) (zip auxProcs (upTo (fromIntegral (length auxProcs))))
      return Run { runNum = run,
                   runType = RunToCompletion,
                   mainProcesses = launchers,
                   auxiliaryProcesses = auxLaunchers,
                   auxiliaryProcessDelay = auxProcDelay,
                   runSetupActions = case cg of
                     Nothing -> []
                     Just (cgroup, size) -> [CgroupSize cgroup size],
                   runCleanupActions = [] }
      where
        createLauncher :: (BenchmarkThreadsIters, ProcessNum) -> IO ProcessLauncher
        createLauncher (bm, vm) = do
          benchmark <- (\b -> b { benchmarkExtraVMArgs = extraVMArgs }) <$> makeBenchmark bm maxHeap
          minHeap <- case minHeaps of
            Just mhs -> Just <$> MH.lookupMinHeap (btiBenchmark bm) mhs
            Nothing -> return Nothing
          benchmarkProcessLauncher
            benchmark
            j
            (Just devnull)
            (Just (M.procOutputFile (metadataDir expDir) run vm))
            (getPath bmDir </> (printf "working%d" (getNum vm) :: FilePath))
            (getPath bmDir </> (printf "scratch%d" (getNum vm) :: FilePath))
            (case cg of
                Nothing -> Nothing
                Just (cgroup, _) -> Just cgroup)
            (Just (M.procResourceUsageFile (metadataDir expDir) run vm))
            Nothing
            throughputs
            minHeap
            forceGrowth


generatePidNamesFile :: BenchmarkTimerDir -> IO ()
generatePidNamesFile d = do
  let metaDir = metadataDir (asDir d)
  runs <- get (M.numRunsFile metaDir)
  bms <- get (benchmarksFile d)
  mapM_ (doRun metaDir bms) (upTo runs)

  where
    doRun :: M.MetadataDir -> [BenchmarkThreadsIters] -> RunNum -> IO ()
    doRun metaDir bms run =
      catch (do pids <- mapM (\pNum -> getPid (M.procResourceUsageFile metaDir run pNum)) (upTo (fromIntegral (length bms)))
                put (pidNamesFile d run) (map makePidName (zip bms pids)))
        handler
      where
        makePidName :: (BenchmarkThreadsIters, Integer) -> PidName
        makePidName (bm, pid) = PidName pid (pretty bm)

        handler :: IOException -> IO ()
        handler _ = return ()
