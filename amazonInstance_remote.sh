#!/bin/bash

sudo apt-get --assume-yes update
sudo apt-get --assume-yes upgrade
sudo apt-get --assume-yes dist-upgrade

if ! grep "$(cat "${HOME}/pubkey")" "${HOME}/.ssh/authorized_keys" &>/dev/null; then
    cat "${HOME}/pubkey" >> "${HOME}/.ssh/authorized_keys"
fi
rm "${HOME}/pubkey"

mv "${HOME}/deployment_key" "${HOME}/.ssh/id_rsa"
chmod go-rwx "${HOME}/.ssh/id_rsa"
mv "${HOME}/deployment_key.pub" "${HOME}/.ssh/id_rsa.pub"

sudo apt-get --assume-yes install git openjdk-7-jdk octave epstool transfig ssmtp

(
    echo "Host bitbucket.org"
    echo "HostName bitbucket.org"
    echo "User git"
    echo "UserKnownHostsFile /dev/null"
    echo "StrictHostKeyChecking no"
) > "${HOME}/.ssh/config"


if [ ! -e "${HOME}/economics_memory_code" ]; then
    git clone 'git@bitbucket.org:CallumCameron/economics_memory_code.git'
else
    cd "${HOME}/economics_memory_code"
    git pull
    cd
fi

"${HOME}/economics_memory_code/checkDacapo.sh"

sudo reboot
