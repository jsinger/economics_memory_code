#ifndef LRUCACHE_HPP
#define LRUCACHE_HPP

#include "ReadingCache.hpp"

class LRUCache : public ReadingCache
{
public:
    static const size_t DEFAULT_CAPACITY = 100;
    static const char* NAME;

    LRUCache(size_t capacity=DEFAULT_CAPACITY);
    virtual ~LRUCache();
    virtual std::string getName() const;
    virtual bool isValid() const;
    virtual void addReading(unsigned long long heapSizeMB, double throughput);
    virtual ReadingIterator* newIterator() const;
    virtual void dumpTo(FILE* fd) const;

private:
    // Jumping through hoops to stop namespace leakage over-complicating the makefile
    class Iterator;
    class Cache;

    Cache* m_Cache;
    size_t m_Capacity;
    static const size_t MIN_READINGS_VALID = 2;
};

#endif
