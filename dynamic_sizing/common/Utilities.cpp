#include <cstdarg>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <sys/time.h>
#include <unistd.h>
#include "Utilities.hpp"

#define DAEMON_SOCKET_NAME "/tmp/economem.%u.%s.%s" // uid.type.name
#define CLIENT_SOCKET_NAME "/tmp/economem.%u.%u.%s" // uid.pid.type

static bool s_Verbose = false;

namespace economem
{
    unsigned long long randInt(unsigned long long min, unsigned long long max)
    {
        static bool init = false;

        if (!init)
        {
            srand(time(NULL));
            init = true;
        }

        return (rand() % (max - min)) + min;
    }

    char* allocDaemonSocketName(const char* type, const char* daemonName)
    {
        char* p;
        if (asprintf(&p, DAEMON_SOCKET_NAME, (unsigned int)getuid(), type, daemonName) != -1)
            return p;
        else
            return NULL;
    }

    char* allocClientSocketName(pid_t pid, const char* type)
    {
        char* p;
        if (asprintf(&p, CLIENT_SOCKET_NAME, (unsigned int)getuid(), (unsigned int)pid, type) != -1)
            return p;
        else
            return NULL;
    }

    void info(const char* caller, const char* fmt, ...)
    {
        if (s_Verbose)
        {
            char* p;
            if (asprintf(&p, "%s: %s\n", caller, fmt) != -1)
            {
                va_list args;
                va_start(args, fmt);
                vprintf(p, args);
                va_end(args);
                free(p);
            }
        }
    }

    void warning(const char* fmt, ...)
    {
        char* p;
        if (asprintf(&p, "Warning: %s\n", fmt) != -1)
        {
            va_list args;
            va_start(args, fmt);
            vfprintf(stderr, p, args);
            va_end(args);
            free(p);
        }
    }

    void die(const char* fmt, ...)
    {
        char* p;
        if (asprintf(&p, "%s\n", fmt) != -1)
        {
            va_list args;
            va_start(args, fmt);
            vfprintf(stderr, p, args);
            va_end(args);
            free(p);
        }
        exit(1);
    }

    bool isInfoVerbose()
    {
        return s_Verbose;
    }

    void makeInfoVerbose()
    {
        s_Verbose = true;
    }

    unsigned long long milliTime()
    {
        timeval t;
        gettimeofday(&t, NULL);
        return (t.tv_sec * 1000) + (t.tv_usec / 1000);
    }

    unsigned long long microTime()
    {
        timeval t;
        gettimeofday(&t, NULL);
        return (t.tv_sec * 1000000) + t.tv_usec;
    }
}
