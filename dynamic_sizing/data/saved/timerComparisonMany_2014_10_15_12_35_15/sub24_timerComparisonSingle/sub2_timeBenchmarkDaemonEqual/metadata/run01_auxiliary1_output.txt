economemd
    total memory: 6265 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub24_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run01_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 9725: heap size 9 MB, throughput 0.991646
Clients: 1
Client 9725 has a minimum heap size of 30 MB
Reading from 9724: heap size 9 MB, throughput 0.992235
Clients: 2
Client 9724 has a minimum heap size of 1223 MB
Reading from 9725: heap size 9 MB, throughput 0.970178
Reading from 9725: heap size 9 MB, throughput 0.96564
Reading from 9724: heap size 9 MB, throughput 0.976331
Reading from 9725: heap size 9 MB, throughput 0.960281
Reading from 9725: heap size 11 MB, throughput 0.793687
Reading from 9725: heap size 11 MB, throughput 0.365867
Reading from 9725: heap size 16 MB, throughput 0.746386
Reading from 9725: heap size 16 MB, throughput 0.703875
Reading from 9725: heap size 24 MB, throughput 0.860899
Reading from 9725: heap size 24 MB, throughput 0.848098
Reading from 9725: heap size 34 MB, throughput 0.900499
Reading from 9725: heap size 34 MB, throughput 0.885187
Reading from 9725: heap size 50 MB, throughput 0.93397
Reading from 9724: heap size 9 MB, throughput 0.965449
Reading from 9725: heap size 50 MB, throughput 0.925382
Reading from 9725: heap size 75 MB, throughput 0.956675
Reading from 9724: heap size 9 MB, throughput 0.958198
Reading from 9725: heap size 75 MB, throughput 0.949234
Reading from 9725: heap size 116 MB, throughput 0.973782
Reading from 9725: heap size 116 MB, throughput 0.940661
Reading from 9724: heap size 11 MB, throughput 0.981825
Reading from 9724: heap size 11 MB, throughput 0.98157
Reading from 9724: heap size 17 MB, throughput 0.900808
Reading from 9724: heap size 17 MB, throughput 0.514879
Reading from 9724: heap size 30 MB, throughput 0.970453
Reading from 9724: heap size 31 MB, throughput 0.873379
Reading from 9724: heap size 33 MB, throughput 0.495181
Reading from 9725: heap size 160 MB, throughput 0.95777
Reading from 9724: heap size 45 MB, throughput 0.928854
Reading from 9724: heap size 49 MB, throughput 0.946227
Reading from 9724: heap size 49 MB, throughput 0.399144
Reading from 9725: heap size 166 MB, throughput 0.820662
Reading from 9724: heap size 67 MB, throughput 0.700632
Reading from 9724: heap size 69 MB, throughput 0.691469
Reading from 9724: heap size 75 MB, throughput 0.202834
Reading from 9724: heap size 94 MB, throughput 0.744014
Reading from 9724: heap size 98 MB, throughput 0.843734
Reading from 9725: heap size 225 MB, throughput 0.988615
Reading from 9724: heap size 100 MB, throughput 0.196872
Reading from 9724: heap size 126 MB, throughput 0.727383
Reading from 9724: heap size 129 MB, throughput 0.855361
Reading from 9724: heap size 131 MB, throughput 0.745634
Reading from 9725: heap size 240 MB, throughput 0.992693
Reading from 9724: heap size 134 MB, throughput 0.213223
Reading from 9724: heap size 169 MB, throughput 0.669199
Reading from 9724: heap size 172 MB, throughput 0.691049
Reading from 9724: heap size 173 MB, throughput 0.68627
Reading from 9724: heap size 178 MB, throughput 0.714435
Reading from 9724: heap size 181 MB, throughput 0.691622
Reading from 9724: heap size 186 MB, throughput 0.675245
Reading from 9724: heap size 190 MB, throughput 0.650284
Reading from 9724: heap size 198 MB, throughput 0.480772
Reading from 9724: heap size 202 MB, throughput 0.437334
Reading from 9725: heap size 257 MB, throughput 0.991403
Reading from 9724: heap size 212 MB, throughput 0.0955378
Reading from 9724: heap size 263 MB, throughput 0.509665
Reading from 9725: heap size 258 MB, throughput 0.868002
Reading from 9724: heap size 266 MB, throughput 0.0765393
Reading from 9724: heap size 305 MB, throughput 0.588778
Reading from 9724: heap size 307 MB, throughput 0.632523
Reading from 9725: heap size 286 MB, throughput 0.991427
Reading from 9724: heap size 299 MB, throughput 0.453569
Reading from 9724: heap size 303 MB, throughput 0.556005
Reading from 9724: heap size 304 MB, throughput 0.630299
Reading from 9724: heap size 306 MB, throughput 0.713825
Reading from 9724: heap size 306 MB, throughput 0.669956
Reading from 9724: heap size 312 MB, throughput 0.554573
Reading from 9725: heap size 291 MB, throughput 0.99239
Reading from 9724: heap size 314 MB, throughput 0.561068
Reading from 9724: heap size 323 MB, throughput 0.570075
Reading from 9724: heap size 328 MB, throughput 0.519102
Reading from 9724: heap size 336 MB, throughput 0.439367
Reading from 9724: heap size 341 MB, throughput 0.478965
Reading from 9725: heap size 307 MB, throughput 0.98381
Reading from 9724: heap size 348 MB, throughput 0.0449618
Reading from 9724: heap size 396 MB, throughput 0.386609
Reading from 9724: heap size 335 MB, throughput 0.441468
Equal recommendation: 3132 MB each
Reading from 9725: heap size 309 MB, throughput 0.970143
Reading from 9724: heap size 388 MB, throughput 0.0541655
Reading from 9724: heap size 375 MB, throughput 0.440513
Reading from 9724: heap size 439 MB, throughput 0.565825
Reading from 9724: heap size 442 MB, throughput 0.636288
Reading from 9724: heap size 432 MB, throughput 0.606824
Reading from 9725: heap size 327 MB, throughput 0.989419
Reading from 9724: heap size 437 MB, throughput 0.538741
Reading from 9724: heap size 437 MB, throughput 0.600563
Reading from 9724: heap size 437 MB, throughput 0.548868
Reading from 9724: heap size 441 MB, throughput 0.528718
Reading from 9724: heap size 444 MB, throughput 0.58286
Reading from 9724: heap size 447 MB, throughput 0.543441
Reading from 9725: heap size 331 MB, throughput 0.976064
Reading from 9724: heap size 450 MB, throughput 0.080589
Reading from 9724: heap size 505 MB, throughput 0.414021
Reading from 9724: heap size 510 MB, throughput 0.49717
Reading from 9724: heap size 513 MB, throughput 0.466736
Reading from 9725: heap size 349 MB, throughput 0.98549
Reading from 9724: heap size 515 MB, throughput 0.501962
Reading from 9724: heap size 518 MB, throughput 0.425919
Reading from 9724: heap size 521 MB, throughput 0.454127
Reading from 9724: heap size 523 MB, throughput 0.503845
Reading from 9725: heap size 352 MB, throughput 0.990918
Reading from 9724: heap size 529 MB, throughput 0.0737507
Reading from 9724: heap size 585 MB, throughput 0.476063
Reading from 9724: heap size 590 MB, throughput 0.52035
Reading from 9725: heap size 368 MB, throughput 0.982757
Reading from 9724: heap size 592 MB, throughput 0.612162
Reading from 9725: heap size 371 MB, throughput 0.999999
Reading from 9725: heap size 371 MB, throughput 1
Reading from 9725: heap size 371 MB, throughput 0.0104628
Reading from 9725: heap size 386 MB, throughput 0.331021
Reading from 9724: heap size 593 MB, throughput 0.084164
Reading from 9724: heap size 657 MB, throughput 0.380582
Reading from 9724: heap size 657 MB, throughput 0.564619
Reading from 9724: heap size 658 MB, throughput 0.765727
Reading from 9725: heap size 389 MB, throughput 0.987273
Reading from 9724: heap size 658 MB, throughput 0.811087
Reading from 9724: heap size 658 MB, throughput 0.809924
Reading from 9725: heap size 437 MB, throughput 0.971039
Reading from 9724: heap size 668 MB, throughput 0.847903
Equal recommendation: 3132 MB each
Reading from 9724: heap size 674 MB, throughput 0.472003
Reading from 9725: heap size 442 MB, throughput 0.99113
Reading from 9724: heap size 685 MB, throughput 0.0846694
Reading from 9724: heap size 755 MB, throughput 0.576795
Reading from 9724: heap size 758 MB, throughput 0.335723
Reading from 9724: heap size 747 MB, throughput 0.390545
Reading from 9724: heap size 753 MB, throughput 0.382706
Reading from 9724: heap size 747 MB, throughput 0.396088
Reading from 9725: heap size 486 MB, throughput 0.975206
Reading from 9725: heap size 494 MB, throughput 0.990429
Reading from 9724: heap size 750 MB, throughput 0.199921
Reading from 9724: heap size 821 MB, throughput 0.372917
Reading from 9724: heap size 824 MB, throughput 0.617873
Reading from 9724: heap size 831 MB, throughput 0.64055
Reading from 9724: heap size 832 MB, throughput 0.78567
Reading from 9724: heap size 837 MB, throughput 0.867089
Reading from 9725: heap size 539 MB, throughput 0.985595
Reading from 9724: heap size 839 MB, throughput 0.203482
Reading from 9724: heap size 930 MB, throughput 0.340259
Reading from 9724: heap size 935 MB, throughput 0.948252
Reading from 9724: heap size 942 MB, throughput 0.861043
Reading from 9725: heap size 546 MB, throughput 0.985157
Reading from 9724: heap size 942 MB, throughput 0.91611
Reading from 9724: heap size 933 MB, throughput 0.875568
Reading from 9724: heap size 816 MB, throughput 0.814032
Reading from 9724: heap size 911 MB, throughput 0.816774
Reading from 9724: heap size 820 MB, throughput 0.787396
Reading from 9724: heap size 902 MB, throughput 0.768879
Reading from 9724: heap size 817 MB, throughput 0.815573
Reading from 9725: heap size 586 MB, throughput 0.994703
Reading from 9724: heap size 896 MB, throughput 0.875254
Equal recommendation: 3132 MB each
Reading from 9724: heap size 902 MB, throughput 0.816331
Reading from 9724: heap size 892 MB, throughput 0.788366
Reading from 9724: heap size 897 MB, throughput 0.851468
Reading from 9724: heap size 889 MB, throughput 0.856475
Reading from 9724: heap size 894 MB, throughput 0.977253
Reading from 9725: heap size 588 MB, throughput 0.991621
Reading from 9724: heap size 892 MB, throughput 0.981208
Reading from 9724: heap size 831 MB, throughput 0.897023
Reading from 9724: heap size 892 MB, throughput 0.788597
Reading from 9725: heap size 614 MB, throughput 0.989868
Reading from 9724: heap size 894 MB, throughput 0.763887
Reading from 9724: heap size 900 MB, throughput 0.778889
Reading from 9724: heap size 901 MB, throughput 0.794294
Reading from 9724: heap size 907 MB, throughput 0.755804
Reading from 9724: heap size 907 MB, throughput 0.800206
Reading from 9724: heap size 912 MB, throughput 0.784539
Reading from 9724: heap size 913 MB, throughput 0.798361
Reading from 9724: heap size 917 MB, throughput 0.909436
Reading from 9725: heap size 619 MB, throughput 0.991394
Reading from 9724: heap size 919 MB, throughput 0.831095
Reading from 9724: heap size 923 MB, throughput 0.865885
Reading from 9724: heap size 924 MB, throughput 0.713153
Reading from 9724: heap size 930 MB, throughput 0.71273
Reading from 9725: heap size 642 MB, throughput 0.983182
Reading from 9724: heap size 930 MB, throughput 0.0660189
Reading from 9724: heap size 1059 MB, throughput 0.578313
Reading from 9724: heap size 1063 MB, throughput 0.717085
Reading from 9724: heap size 1063 MB, throughput 0.709584
Reading from 9724: heap size 1066 MB, throughput 0.636293
Reading from 9724: heap size 1077 MB, throughput 0.688203
Reading from 9725: heap size 647 MB, throughput 0.981734
Reading from 9724: heap size 1077 MB, throughput 0.684066
Reading from 9724: heap size 1092 MB, throughput 0.749173
Equal recommendation: 3132 MB each
Reading from 9724: heap size 1093 MB, throughput 0.711253
Reading from 9725: heap size 677 MB, throughput 0.992084
Reading from 9724: heap size 1109 MB, throughput 0.951175
Reading from 9725: heap size 679 MB, throughput 0.994142
Reading from 9724: heap size 1112 MB, throughput 0.97409
Reading from 9725: heap size 695 MB, throughput 0.988089
Reading from 9725: heap size 699 MB, throughput 0.987746
Reading from 9724: heap size 1126 MB, throughput 0.973203
Equal recommendation: 3132 MB each
Reading from 9725: heap size 723 MB, throughput 0.991951
Reading from 9724: heap size 1128 MB, throughput 0.972047
Reading from 9725: heap size 725 MB, throughput 0.995222
Reading from 9724: heap size 1131 MB, throughput 0.975951
Reading from 9725: heap size 741 MB, throughput 0.985184
Reading from 9725: heap size 744 MB, throughput 0.986922
Reading from 9724: heap size 1135 MB, throughput 0.972414
Equal recommendation: 3132 MB each
Reading from 9725: heap size 766 MB, throughput 0.990258
Reading from 9724: heap size 1130 MB, throughput 0.970845
Reading from 9725: heap size 767 MB, throughput 0.988899
Reading from 9725: heap size 789 MB, throughput 0.993928
Reading from 9724: heap size 1135 MB, throughput 0.975018
Reading from 9725: heap size 790 MB, throughput 0.997809
Reading from 9724: heap size 1139 MB, throughput 0.975333
Equal recommendation: 3132 MB each
Reading from 9725: heap size 803 MB, throughput 0.992203
Reading from 9724: heap size 1140 MB, throughput 0.96611
Reading from 9725: heap size 806 MB, throughput 0.995585
Reading from 9725: heap size 818 MB, throughput 0.988814
Reading from 9724: heap size 1145 MB, throughput 0.973769
Reading from 9725: heap size 820 MB, throughput 0.990648
Reading from 9724: heap size 1148 MB, throughput 0.968298
Equal recommendation: 3132 MB each
Reading from 9725: heap size 838 MB, throughput 0.989889
Reading from 9724: heap size 1152 MB, throughput 0.972733
Reading from 9725: heap size 838 MB, throughput 0.990953
Reading from 9725: heap size 858 MB, throughput 0.990441
Reading from 9724: heap size 1156 MB, throughput 0.970743
Reading from 9725: heap size 858 MB, throughput 0.991199
Equal recommendation: 3132 MB each
Reading from 9724: heap size 1159 MB, throughput 0.967467
Reading from 9725: heap size 878 MB, throughput 0.991271
Reading from 9724: heap size 1164 MB, throughput 0.969757
Reading from 9725: heap size 878 MB, throughput 0.990333
Reading from 9725: heap size 900 MB, throughput 0.988528
Reading from 9724: heap size 1169 MB, throughput 0.971615
Equal recommendation: 3132 MB each
Reading from 9725: heap size 900 MB, throughput 0.990798
Reading from 9724: heap size 1174 MB, throughput 0.966916
Reading from 9725: heap size 923 MB, throughput 0.992847
Reading from 9724: heap size 1180 MB, throughput 0.966479
Reading from 9725: heap size 923 MB, throughput 0.992108
Reading from 9724: heap size 1183 MB, throughput 0.972869
Reading from 9725: heap size 942 MB, throughput 0.989116
Equal recommendation: 3132 MB each
Reading from 9724: heap size 1188 MB, throughput 0.97189
Reading from 9725: heap size 943 MB, throughput 0.991665
Reading from 9725: heap size 965 MB, throughput 0.990165
Reading from 9724: heap size 1190 MB, throughput 0.968605
Reading from 9725: heap size 966 MB, throughput 0.992297
Equal recommendation: 3132 MB each
Reading from 9724: heap size 1194 MB, throughput 0.971394
Reading from 9725: heap size 987 MB, throughput 0.989152
Reading from 9724: heap size 1195 MB, throughput 0.969948
Reading from 9725: heap size 988 MB, throughput 0.994496
Reading from 9725: heap size 1009 MB, throughput 0.99005
Equal recommendation: 3132 MB each
Reading from 9724: heap size 1199 MB, throughput 0.623478
Reading from 9725: heap size 1011 MB, throughput 0.990599
Reading from 9724: heap size 1299 MB, throughput 0.955596
Reading from 9725: heap size 1035 MB, throughput 0.996124
Reading from 9724: heap size 1296 MB, throughput 0.987692
Reading from 9725: heap size 1035 MB, throughput 0.98925
Equal recommendation: 3132 MB each
Reading from 9724: heap size 1304 MB, throughput 0.98617
Reading from 9725: heap size 1055 MB, throughput 0.992009
Reading from 9724: heap size 1311 MB, throughput 0.984642
Reading from 9725: heap size 1057 MB, throughput 0.995857
Reading from 9725: heap size 1075 MB, throughput 0.993698
Reading from 9724: heap size 1312 MB, throughput 0.979866
Equal recommendation: 3132 MB each
Reading from 9725: heap size 1078 MB, throughput 0.990936
Reading from 9724: heap size 1307 MB, throughput 0.979745
Reading from 9725: heap size 1100 MB, throughput 0.992406
Reading from 9724: heap size 1212 MB, throughput 0.974847
Equal recommendation: 3132 MB each
Reading from 9725: heap size 1099 MB, throughput 0.992474
Reading from 9724: heap size 1297 MB, throughput 0.980978
Reading from 9725: heap size 1121 MB, throughput 0.99376
Reading from 9724: heap size 1230 MB, throughput 0.97728
Reading from 9725: heap size 1123 MB, throughput 0.9896
Reading from 9724: heap size 1292 MB, throughput 0.975815
Equal recommendation: 3132 MB each
Reading from 9725: heap size 1144 MB, throughput 0.992041
Reading from 9724: heap size 1296 MB, throughput 0.973303
Reading from 9725: heap size 1145 MB, throughput 0.993479
Reading from 9724: heap size 1298 MB, throughput 0.971123
Reading from 9725: heap size 1166 MB, throughput 0.992007
Equal recommendation: 3132 MB each
Reading from 9724: heap size 1299 MB, throughput 0.974543
Reading from 9725: heap size 1168 MB, throughput 0.992156
Reading from 9724: heap size 1302 MB, throughput 0.971175
Reading from 9725: heap size 1193 MB, throughput 0.993916
Reading from 9724: heap size 1307 MB, throughput 0.962973
Equal recommendation: 3132 MB each
Reading from 9725: heap size 1193 MB, throughput 0.955733
Reading from 9724: heap size 1311 MB, throughput 0.971267
Reading from 9725: heap size 1230 MB, throughput 0.994599
Reading from 9724: heap size 1318 MB, throughput 0.968677
Reading from 9725: heap size 1234 MB, throughput 0.988941
Equal recommendation: 3132 MB each
Reading from 9724: heap size 1325 MB, throughput 0.97104
Reading from 9725: heap size 1261 MB, throughput 0.966083
Reading from 9724: heap size 1329 MB, throughput 0.971486
Reading from 9725: heap size 1275 MB, throughput 0.996265
Equal recommendation: 3132 MB each
Reading from 9725: heap size 1318 MB, throughput 0.988871
Reading from 9725: heap size 1318 MB, throughput 0.991511
Reading from 9725: heap size 1366 MB, throughput 0.989008
Equal recommendation: 3132 MB each
Reading from 9725: heap size 1367 MB, throughput 0.989706
Reading from 9725: heap size 1421 MB, throughput 0.987978
Reading from 9724: heap size 1335 MB, throughput 0.863459
Equal recommendation: 3132 MB each
Reading from 9725: heap size 1420 MB, throughput 0.990089
Reading from 9725: heap size 1473 MB, throughput 0.993631
Reading from 9725: heap size 1477 MB, throughput 0.988862
Equal recommendation: 3132 MB each
Reading from 9724: heap size 1500 MB, throughput 0.983196
Reading from 9725: heap size 1528 MB, throughput 0.990638
Reading from 9725: heap size 1530 MB, throughput 0.990889
Reading from 9724: heap size 1552 MB, throughput 0.981484
Equal recommendation: 3132 MB each
Reading from 9724: heap size 1553 MB, throughput 0.818153
Reading from 9724: heap size 1546 MB, throughput 0.723439
Reading from 9724: heap size 1562 MB, throughput 0.692126
Reading from 9724: heap size 1586 MB, throughput 0.721126
Reading from 9724: heap size 1600 MB, throughput 0.700913
Reading from 9724: heap size 1631 MB, throughput 0.744221
Reading from 9724: heap size 1637 MB, throughput 0.778677
Reading from 9725: heap size 1588 MB, throughput 0.994145
Client 9725 died
Clients: 1
Reading from 9724: heap size 1660 MB, throughput 0.986714
Recommendation: one client; give it all the memory
Reading from 9724: heap size 1666 MB, throughput 0.986501
Reading from 9724: heap size 1668 MB, throughput 0.986462
Recommendation: one client; give it all the memory
Reading from 9724: heap size 1680 MB, throughput 0.986538
Reading from 9724: heap size 1672 MB, throughput 0.986281
Recommendation: one client; give it all the memory
Reading from 9724: heap size 1684 MB, throughput 0.985892
Reading from 9724: heap size 1673 MB, throughput 0.984819
Recommendation: one client; give it all the memory
Reading from 9724: heap size 1683 MB, throughput 0.988693
Recommendation: one client; give it all the memory
Reading from 9724: heap size 1687 MB, throughput 0.987556
Reading from 9724: heap size 1691 MB, throughput 0.987452
Recommendation: one client; give it all the memory
Reading from 9724: heap size 1678 MB, throughput 0.986907
Reading from 9724: heap size 1583 MB, throughput 0.985302
Recommendation: one client; give it all the memory
Reading from 9724: heap size 1672 MB, throughput 0.985392
Recommendation: one client; give it all the memory
Reading from 9724: heap size 1679 MB, throughput 0.98386
Reading from 9724: heap size 1682 MB, throughput 0.983187
Recommendation: one client; give it all the memory
Reading from 9724: heap size 1684 MB, throughput 0.98193
Reading from 9724: heap size 1690 MB, throughput 0.86618
Recommendation: one client; give it all the memory
Reading from 9724: heap size 1636 MB, throughput 0.996116
Recommendation: one client; give it all the memory
Reading from 9724: heap size 1634 MB, throughput 0.994639
Reading from 9724: heap size 1647 MB, throughput 0.993248
Recommendation: one client; give it all the memory
Reading from 9724: heap size 1658 MB, throughput 0.99264
Recommendation: one client; give it all the memory
Reading from 9724: heap size 1659 MB, throughput 0.991102
Reading from 9724: heap size 1647 MB, throughput 0.990029
Recommendation: one client; give it all the memory
Reading from 9724: heap size 1504 MB, throughput 0.989562
Reading from 9724: heap size 1627 MB, throughput 0.988548
Recommendation: one client; give it all the memory
Reading from 9724: heap size 1533 MB, throughput 0.987077
Recommendation: one client; give it all the memory
Reading from 9724: heap size 1627 MB, throughput 0.987462
Reading from 9724: heap size 1631 MB, throughput 0.985198
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 9724: heap size 1634 MB, throughput 0.996752
Recommendation: one client; give it all the memory
Reading from 9724: heap size 1638 MB, throughput 0.98602
Reading from 9724: heap size 1646 MB, throughput 0.835001
Reading from 9724: heap size 1660 MB, throughput 0.776087
Reading from 9724: heap size 1699 MB, throughput 0.812802
Reading from 9724: heap size 1720 MB, throughput 0.798955
Client 9724 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
