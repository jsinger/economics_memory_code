economemd
    total memory: 1152 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub37_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 21624: heap size 9 MB, throughput 0.986646
Clients: 1
Client 21624 has a minimum heap size of 12 MB
Reading from 21623: heap size 9 MB, throughput 0.98631
Clients: 2
Client 21623 has a minimum heap size of 276 MB
Reading from 21624: heap size 9 MB, throughput 0.978392
Reading from 21623: heap size 9 MB, throughput 0.981975
Reading from 21623: heap size 11 MB, throughput 0.971004
Reading from 21624: heap size 11 MB, throughput 0.987736
Reading from 21623: heap size 11 MB, throughput 0.986572
Reading from 21624: heap size 11 MB, throughput 0.971529
Reading from 21623: heap size 15 MB, throughput 0.800484
Reading from 21624: heap size 15 MB, throughput 0.98786
Reading from 21623: heap size 18 MB, throughput 0.959814
Reading from 21624: heap size 15 MB, throughput 0.966027
Reading from 21623: heap size 23 MB, throughput 0.908689
Reading from 21623: heap size 28 MB, throughput 0.829172
Reading from 21623: heap size 29 MB, throughput 0.270372
Reading from 21623: heap size 39 MB, throughput 0.920409
Reading from 21624: heap size 25 MB, throughput 0.924353
Reading from 21623: heap size 45 MB, throughput 0.841281
Reading from 21623: heap size 46 MB, throughput 0.829257
Reading from 21624: heap size 30 MB, throughput 0.989579
Reading from 21623: heap size 51 MB, throughput 0.208012
Reading from 21624: heap size 37 MB, throughput 0.987068
Reading from 21623: heap size 65 MB, throughput 0.231695
Reading from 21623: heap size 89 MB, throughput 0.720485
Reading from 21623: heap size 91 MB, throughput 0.706792
Reading from 21624: heap size 43 MB, throughput 0.978565
Reading from 21623: heap size 93 MB, throughput 0.827755
Reading from 21623: heap size 96 MB, throughput 0.696542
Reading from 21624: heap size 44 MB, throughput 0.986763
Reading from 21623: heap size 100 MB, throughput 0.670201
Reading from 21624: heap size 46 MB, throughput 0.978227
Reading from 21624: heap size 52 MB, throughput 0.990659
Reading from 21623: heap size 104 MB, throughput 0.204628
Reading from 21623: heap size 134 MB, throughput 0.658682
Reading from 21624: heap size 52 MB, throughput 0.973128
Reading from 21623: heap size 136 MB, throughput 0.628104
Reading from 21623: heap size 140 MB, throughput 0.636977
Reading from 21624: heap size 60 MB, throughput 0.944394
Reading from 21623: heap size 145 MB, throughput 0.598154
Reading from 21624: heap size 60 MB, throughput 0.96423
Reading from 21623: heap size 149 MB, throughput 0.489044
Reading from 21623: heap size 157 MB, throughput 0.644882
Reading from 21624: heap size 73 MB, throughput 0.616857
Reading from 21624: heap size 76 MB, throughput 0.993317
Reading from 21623: heap size 162 MB, throughput 0.0757519
Reading from 21623: heap size 194 MB, throughput 0.44958
Reading from 21624: heap size 89 MB, throughput 0.997346
Reading from 21623: heap size 200 MB, throughput 0.683463
Reading from 21623: heap size 201 MB, throughput 0.605396
Reading from 21623: heap size 204 MB, throughput 0.804641
Reading from 21624: heap size 89 MB, throughput 0.997687
Reading from 21623: heap size 207 MB, throughput 0.893811
Reading from 21623: heap size 211 MB, throughput 0.630501
Reading from 21623: heap size 217 MB, throughput 0.534981
Reading from 21623: heap size 221 MB, throughput 0.636893
Reading from 21624: heap size 99 MB, throughput 0.996425
Reading from 21623: heap size 226 MB, throughput 0.175531
Reading from 21623: heap size 264 MB, throughput 0.574388
Reading from 21623: heap size 264 MB, throughput 0.830777
Reading from 21623: heap size 258 MB, throughput 0.754459
Reading from 21624: heap size 101 MB, throughput 0.9972
Reading from 21623: heap size 261 MB, throughput 0.718366
Reading from 21623: heap size 255 MB, throughput 0.793382
Reading from 21623: heap size 259 MB, throughput 0.742468
Reading from 21623: heap size 257 MB, throughput 0.150561
Reading from 21624: heap size 110 MB, throughput 0.996845
Reading from 21623: heap size 296 MB, throughput 0.648184
Reading from 21623: heap size 298 MB, throughput 0.855364
Reading from 21623: heap size 298 MB, throughput 0.874559
Reading from 21624: heap size 112 MB, throughput 0.996568
Reading from 21623: heap size 303 MB, throughput 0.964361
Reading from 21623: heap size 303 MB, throughput 0.808184
Reading from 21623: heap size 301 MB, throughput 0.826017
Reading from 21623: heap size 303 MB, throughput 0.807213
Reading from 21623: heap size 303 MB, throughput 0.803837
Reading from 21623: heap size 304 MB, throughput 0.859882
Reading from 21623: heap size 304 MB, throughput 0.873311
Reading from 21624: heap size 119 MB, throughput 0.997299
Reading from 21623: heap size 306 MB, throughput 0.816036
Reading from 21623: heap size 309 MB, throughput 0.616731
Equal recommendation: 576 MB each
Reading from 21624: heap size 121 MB, throughput 0.997054
Reading from 21623: heap size 309 MB, throughput 0.0741752
Reading from 21623: heap size 356 MB, throughput 0.644814
Reading from 21623: heap size 358 MB, throughput 0.743315
Reading from 21623: heap size 364 MB, throughput 0.766424
Reading from 21624: heap size 125 MB, throughput 0.995561
Reading from 21624: heap size 126 MB, throughput 0.996651
Reading from 21623: heap size 364 MB, throughput 0.970429
Reading from 21624: heap size 131 MB, throughput 0.995337
Reading from 21623: heap size 366 MB, throughput 0.942313
Reading from 21624: heap size 132 MB, throughput 0.996882
Reading from 21624: heap size 135 MB, throughput 0.994462
Reading from 21623: heap size 368 MB, throughput 0.97823
Reading from 21624: heap size 136 MB, throughput 0.996764
Reading from 21623: heap size 369 MB, throughput 0.978976
Reading from 21624: heap size 141 MB, throughput 0.997604
Reading from 21623: heap size 371 MB, throughput 0.979945
Reading from 21624: heap size 141 MB, throughput 0.99755
Reading from 21624: heap size 144 MB, throughput 0.996499
Reading from 21623: heap size 368 MB, throughput 0.983929
Reading from 21624: heap size 145 MB, throughput 0.997766
Equal recommendation: 576 MB each
Reading from 21623: heap size 371 MB, throughput 0.978547
Reading from 21624: heap size 147 MB, throughput 0.99866
Reading from 21624: heap size 148 MB, throughput 0.997051
Reading from 21623: heap size 370 MB, throughput 0.981703
Reading from 21624: heap size 151 MB, throughput 0.998245
Reading from 21624: heap size 151 MB, throughput 0.997893
Reading from 21623: heap size 371 MB, throughput 0.964638
Reading from 21624: heap size 154 MB, throughput 0.994963
Reading from 21624: heap size 154 MB, throughput 0.991759
Reading from 21624: heap size 158 MB, throughput 0.995908
Reading from 21623: heap size 371 MB, throughput 0.97685
Reading from 21624: heap size 158 MB, throughput 0.997458
Reading from 21623: heap size 373 MB, throughput 0.975861
Reading from 21624: heap size 163 MB, throughput 0.998279
Reading from 21624: heap size 163 MB, throughput 0.997492
Reading from 21623: heap size 371 MB, throughput 0.977179
Reading from 21624: heap size 168 MB, throughput 0.997831
Equal recommendation: 576 MB each
Reading from 21623: heap size 373 MB, throughput 0.975874
Reading from 21624: heap size 168 MB, throughput 0.997246
Reading from 21624: heap size 171 MB, throughput 0.997353
Reading from 21623: heap size 374 MB, throughput 0.980009
Reading from 21624: heap size 171 MB, throughput 0.997295
Reading from 21624: heap size 175 MB, throughput 0.997122
Reading from 21623: heap size 375 MB, throughput 0.987838
Reading from 21623: heap size 377 MB, throughput 0.978047
Reading from 21624: heap size 175 MB, throughput 0.994833
Reading from 21623: heap size 377 MB, throughput 0.20361
Reading from 21623: heap size 412 MB, throughput 0.991703
Reading from 21623: heap size 417 MB, throughput 0.993619
Reading from 21623: heap size 423 MB, throughput 0.992585
Reading from 21624: heap size 178 MB, throughput 0.997312
Reading from 21623: heap size 426 MB, throughput 0.993467
Reading from 21624: heap size 178 MB, throughput 0.997009
Reading from 21623: heap size 430 MB, throughput 0.996242
Reading from 21624: heap size 182 MB, throughput 0.997735
Reading from 21624: heap size 182 MB, throughput 0.997657
Reading from 21623: heap size 431 MB, throughput 0.993396
Equal recommendation: 576 MB each
Reading from 21624: heap size 185 MB, throughput 0.99772
Reading from 21623: heap size 428 MB, throughput 0.993086
Reading from 21624: heap size 186 MB, throughput 0.997751
Reading from 21624: heap size 189 MB, throughput 0.994148
Reading from 21623: heap size 381 MB, throughput 0.987462
Reading from 21624: heap size 189 MB, throughput 0.995727
Reading from 21624: heap size 193 MB, throughput 0.998121
Reading from 21623: heap size 423 MB, throughput 0.992547
Reading from 21624: heap size 194 MB, throughput 0.998042
Reading from 21623: heap size 388 MB, throughput 0.991001
Reading from 21624: heap size 196 MB, throughput 0.998213
Reading from 21623: heap size 419 MB, throughput 0.987839
Reading from 21624: heap size 197 MB, throughput 0.997274
Reading from 21624: heap size 201 MB, throughput 0.996189
Reading from 21623: heap size 421 MB, throughput 0.986574
Equal recommendation: 576 MB each
Reading from 21624: heap size 201 MB, throughput 0.998018
Reading from 21623: heap size 422 MB, throughput 0.989287
Reading from 21624: heap size 204 MB, throughput 0.998024
Reading from 21623: heap size 422 MB, throughput 0.983141
Reading from 21624: heap size 204 MB, throughput 0.997196
Reading from 21623: heap size 422 MB, throughput 0.98175
Reading from 21624: heap size 208 MB, throughput 0.998072
Reading from 21624: heap size 208 MB, throughput 0.997396
Reading from 21623: heap size 424 MB, throughput 0.979178
Reading from 21624: heap size 210 MB, throughput 0.997797
Reading from 21623: heap size 421 MB, throughput 0.986733
Reading from 21623: heap size 428 MB, throughput 0.896711
Reading from 21623: heap size 425 MB, throughput 0.819095
Reading from 21623: heap size 430 MB, throughput 0.821667
Reading from 21623: heap size 436 MB, throughput 0.843533
Reading from 21624: heap size 201 MB, throughput 0.991362
Reading from 21623: heap size 439 MB, throughput 0.976659
Reading from 21624: heap size 205 MB, throughput 0.915481
Equal recommendation: 576 MB each
Reading from 21623: heap size 442 MB, throughput 0.962648
Reading from 21624: heap size 212 MB, throughput 0.993276
Reading from 21624: heap size 217 MB, throughput 0.998117
Reading from 21623: heap size 445 MB, throughput 0.988591
Reading from 21624: heap size 218 MB, throughput 0.996968
Reading from 21623: heap size 442 MB, throughput 0.990013
Reading from 21624: heap size 225 MB, throughput 0.998576
Reading from 21623: heap size 446 MB, throughput 0.986385
Reading from 21624: heap size 226 MB, throughput 0.998268
Reading from 21624: heap size 232 MB, throughput 0.998561
Reading from 21623: heap size 441 MB, throughput 0.986182
Reading from 21624: heap size 232 MB, throughput 0.998067
Reading from 21623: heap size 445 MB, throughput 0.989493
Equal recommendation: 576 MB each
Reading from 21624: heap size 236 MB, throughput 0.99859
Reading from 21623: heap size 440 MB, throughput 0.985087
Reading from 21624: heap size 237 MB, throughput 0.997261
Reading from 21623: heap size 443 MB, throughput 0.98757
Reading from 21624: heap size 242 MB, throughput 0.99838
Reading from 21623: heap size 439 MB, throughput 0.988064
Reading from 21624: heap size 242 MB, throughput 0.997964
Reading from 21623: heap size 441 MB, throughput 0.985314
Reading from 21624: heap size 247 MB, throughput 0.998284
Reading from 21624: heap size 247 MB, throughput 0.995863
Reading from 21624: heap size 251 MB, throughput 0.994509
Reading from 21623: heap size 440 MB, throughput 0.985663
Reading from 21624: heap size 252 MB, throughput 0.997405
Reading from 21623: heap size 441 MB, throughput 0.990246
Reading from 21623: heap size 443 MB, throughput 0.911232
Equal recommendation: 576 MB each
Reading from 21623: heap size 444 MB, throughput 0.864264
Reading from 21623: heap size 448 MB, throughput 0.880377
Reading from 21623: heap size 450 MB, throughput 0.979915
Reading from 21624: heap size 260 MB, throughput 0.99814
Reading from 21623: heap size 457 MB, throughput 0.992968
Reading from 21624: heap size 260 MB, throughput 0.997622
Reading from 21624: heap size 265 MB, throughput 0.998437
Reading from 21623: heap size 458 MB, throughput 0.990729
Reading from 21624: heap size 266 MB, throughput 0.998137
Reading from 21623: heap size 459 MB, throughput 0.990912
Reading from 21624: heap size 272 MB, throughput 0.998419
Reading from 21623: heap size 461 MB, throughput 0.991524
Reading from 21624: heap size 272 MB, throughput 0.997228
Equal recommendation: 576 MB each
Reading from 21623: heap size 459 MB, throughput 0.989577
Reading from 21624: heap size 277 MB, throughput 0.997778
Reading from 21624: heap size 277 MB, throughput 0.998205
Reading from 21623: heap size 461 MB, throughput 0.988611
Reading from 21624: heap size 282 MB, throughput 0.996623
Reading from 21624: heap size 283 MB, throughput 0.994961
Reading from 21623: heap size 459 MB, throughput 0.988441
Reading from 21624: heap size 289 MB, throughput 0.998281
Reading from 21623: heap size 461 MB, throughput 0.9892
Reading from 21624: heap size 290 MB, throughput 0.9982
Equal recommendation: 576 MB each
Reading from 21623: heap size 463 MB, throughput 0.988134
Reading from 21624: heap size 296 MB, throughput 0.998251
Reading from 21623: heap size 463 MB, throughput 0.993894
Reading from 21623: heap size 465 MB, throughput 0.936535
Reading from 21624: heap size 296 MB, throughput 0.992533
Reading from 21623: heap size 465 MB, throughput 0.840236
Reading from 21623: heap size 469 MB, throughput 0.90362
Reading from 21623: heap size 471 MB, throughput 0.991021
Reading from 21624: heap size 303 MB, throughput 0.998495
Reading from 21624: heap size 303 MB, throughput 0.997997
Reading from 21623: heap size 478 MB, throughput 0.993899
Reading from 21624: heap size 309 MB, throughput 0.998352
Reading from 21623: heap size 479 MB, throughput 0.992519
Equal recommendation: 576 MB each
Reading from 21624: heap size 310 MB, throughput 0.998468
Reading from 21624: heap size 317 MB, throughput 0.995062
Reading from 21623: heap size 480 MB, throughput 0.991863
Reading from 21624: heap size 317 MB, throughput 0.997551
Reading from 21623: heap size 482 MB, throughput 0.991734
Reading from 21624: heap size 327 MB, throughput 0.998339
Reading from 21623: heap size 480 MB, throughput 0.990924
Reading from 21624: heap size 327 MB, throughput 0.998334
Reading from 21623: heap size 482 MB, throughput 0.98983
Equal recommendation: 576 MB each
Reading from 21624: heap size 333 MB, throughput 0.998389
Reading from 21623: heap size 483 MB, throughput 0.989301
Reading from 21624: heap size 334 MB, throughput 0.998087
Reading from 21624: heap size 341 MB, throughput 0.998478
Reading from 21623: heap size 483 MB, throughput 0.993779
Reading from 21623: heap size 483 MB, throughput 0.97764
Reading from 21623: heap size 485 MB, throughput 0.811107
Reading from 21623: heap size 487 MB, throughput 0.913071
Reading from 21624: heap size 341 MB, throughput 0.996804
Reading from 21623: heap size 489 MB, throughput 0.991872
Reading from 21624: heap size 347 MB, throughput 0.997955
Reading from 21624: heap size 347 MB, throughput 0.98946
Reading from 21623: heap size 497 MB, throughput 0.99315
Equal recommendation: 576 MB each
Reading from 21624: heap size 354 MB, throughput 0.994731
Reading from 21623: heap size 498 MB, throughput 0.992869
Reading from 21624: heap size 355 MB, throughput 0.996599
Reading from 21623: heap size 499 MB, throughput 0.992942
Reading from 21624: heap size 368 MB, throughput 0.995979
Reading from 21623: heap size 501 MB, throughput 0.990396
Reading from 21624: heap size 368 MB, throughput 0.997491
Equal recommendation: 576 MB each
Reading from 21623: heap size 500 MB, throughput 0.991371
Reading from 21624: heap size 379 MB, throughput 0.996869
Reading from 21623: heap size 502 MB, throughput 0.968543
Reading from 21624: heap size 381 MB, throughput 0.997645
Reading from 21623: heap size 504 MB, throughput 0.992388
Reading from 21623: heap size 504 MB, throughput 0.674648
Reading from 21624: heap size 393 MB, throughput 0.99761
Reading from 21623: heap size 508 MB, throughput 0.436171
Reading from 21623: heap size 509 MB, throughput 0.943639
Equal recommendation: 576 MB each
Reading from 21623: heap size 523 MB, throughput 0.983994
Reading from 21624: heap size 393 MB, throughput 0.994267
Equal recommendation: 576 MB each
Equal recommendation: 576 MB each
Equal recommendation: 576 MB each
Equal recommendation: 576 MB each
Equal recommendation: 576 MB each
Reading from 21624: heap size 397 MB, throughput 0.681991
Equal recommendation: 576 MB each
Equal recommendation: 576 MB each
Reading from 21623: heap size 524 MB, throughput 0.0379213
Equal recommendation: 576 MB each
Equal recommendation: 576 MB each
Equal recommendation: 576 MB each
Reading from 21624: heap size 397 MB, throughput 0.998987
Reading from 21624: heap size 397 MB, throughput 0.996938
Equal recommendation: 576 MB each
Reading from 21624: heap size 397 MB, throughput 0.226132
Equal recommendation: 576 MB each
Reading from 21624: heap size 399 MB, throughput 0.999185
Reading from 21624: heap size 399 MB, throughput 0.997475
Reading from 21624: heap size 399 MB, throughput 0.90078
Reading from 21623: heap size 537 MB, throughput 0.975878
Equal recommendation: 576 MB each
Reading from 21624: heap size 399 MB, throughput 0.998768
Reading from 21623: heap size 536 MB, throughput 0.989426
Reading from 21624: heap size 398 MB, throughput 0.998985
Reading from 21624: heap size 398 MB, throughput 0.995776
Reading from 21623: heap size 548 MB, throughput 0.835745
Reading from 21624: heap size 397 MB, throughput 0.998929
Reading from 21623: heap size 553 MB, throughput 0.990556
Equal recommendation: 576 MB each
Reading from 21624: heap size 398 MB, throughput 0.998855
Reading from 21624: heap size 399 MB, throughput 0.998779
Reading from 21623: heap size 569 MB, throughput 0.987823
Reading from 21623: heap size 572 MB, throughput 0.918657
Reading from 21624: heap size 399 MB, throughput 0.810404
Reading from 21623: heap size 579 MB, throughput 0.959095
Reading from 21624: heap size 397 MB, throughput 0.998755
Reading from 21623: heap size 545 MB, throughput 0.995095
Equal recommendation: 576 MB each
Reading from 21624: heap size 398 MB, throughput 0.98209
Reading from 21624: heap size 397 MB, throughput 0.988185
Reading from 21623: heap size 575 MB, throughput 0.993538
Reading from 21624: heap size 398 MB, throughput 0.99864
Reading from 21623: heap size 575 MB, throughput 0.992306
Reading from 21624: heap size 397 MB, throughput 0.99882
Equal recommendation: 576 MB each
Reading from 21624: heap size 398 MB, throughput 0.998866
Reading from 21623: heap size 579 MB, throughput 0.990363
Reading from 21624: heap size 399 MB, throughput 0.958391
Reading from 21624: heap size 399 MB, throughput 0.997995
Reading from 21623: heap size 560 MB, throughput 0.990777
Reading from 21624: heap size 397 MB, throughput 0.998658
Reading from 21623: heap size 571 MB, throughput 0.994852
Reading from 21623: heap size 572 MB, throughput 0.913765
Reading from 21624: heap size 398 MB, throughput 0.997741
Reading from 21623: heap size 573 MB, throughput 0.979484
Equal recommendation: 576 MB each
Reading from 21624: heap size 397 MB, throughput 0.998565
Reading from 21623: heap size 576 MB, throughput 0.995775
Reading from 21624: heap size 397 MB, throughput 0.997704
Reading from 21624: heap size 398 MB, throughput 0.998766
Reading from 21623: heap size 557 MB, throughput 0.992996
Reading from 21624: heap size 396 MB, throughput 0.979876
Equal recommendation: 576 MB each
Reading from 21624: heap size 397 MB, throughput 0.992216
Reading from 21623: heap size 569 MB, throughput 0.99322
Reading from 21624: heap size 398 MB, throughput 0.998525
Reading from 21623: heap size 569 MB, throughput 0.992498
Reading from 21624: heap size 398 MB, throughput 0.998682
Reading from 21623: heap size 570 MB, throughput 0.992804
Reading from 21624: heap size 395 MB, throughput 0.998766
Equal recommendation: 576 MB each
Reading from 21624: heap size 397 MB, throughput 0.998454
Reading from 21623: heap size 573 MB, throughput 0.994551
Reading from 21623: heap size 574 MB, throughput 0.941819
Reading from 21623: heap size 572 MB, throughput 0.964921
Reading from 21624: heap size 397 MB, throughput 0.998753
Reading from 21624: heap size 397 MB, throughput 0.985805
Reading from 21623: heap size 575 MB, throughput 0.995639
Reading from 21624: heap size 398 MB, throughput 0.992561
Equal recommendation: 576 MB each
Reading from 21624: heap size 398 MB, throughput 0.998914
Reading from 21623: heap size 578 MB, throughput 0.994915
Reading from 21624: heap size 398 MB, throughput 0.998091
Reading from 21623: heap size 552 MB, throughput 0.99278
Reading from 21624: heap size 397 MB, throughput 0.998369
Reading from 21624: heap size 397 MB, throughput 0.997912
Reading from 21623: heap size 572 MB, throughput 0.992797
Equal recommendation: 576 MB each
Reading from 21624: heap size 398 MB, throughput 0.998598
Reading from 21623: heap size 571 MB, throughput 0.992633
Reading from 21624: heap size 398 MB, throughput 0.997478
Reading from 21624: heap size 398 MB, throughput 0.982099
Reading from 21623: heap size 573 MB, throughput 0.995802
Reading from 21623: heap size 574 MB, throughput 0.938718
Reading from 21623: heap size 574 MB, throughput 0.977847
Reading from 21624: heap size 398 MB, throughput 0.998691
Reading from 21624: heap size 398 MB, throughput 0.998755
Equal recommendation: 576 MB each
Reading from 21623: heap size 577 MB, throughput 0.995471
Reading from 21624: heap size 398 MB, throughput 0.998624
Reading from 21623: heap size 558 MB, throughput 0.994335
Reading from 21624: heap size 398 MB, throughput 0.998655
Reading from 21624: heap size 398 MB, throughput 0.99861
Reading from 21623: heap size 569 MB, throughput 0.993364
Equal recommendation: 576 MB each
Client 21624 died
Clients: 1
Reading from 21623: heap size 567 MB, throughput 0.993123
Reading from 21623: heap size 569 MB, throughput 0.992188
Reading from 21623: heap size 572 MB, throughput 0.994062
Reading from 21623: heap size 573 MB, throughput 0.939571
Reading from 21623: heap size 572 MB, throughput 0.978186
Recommendation: one client; give it all the memory
Reading from 21623: heap size 575 MB, throughput 0.994926
Reading from 21623: heap size 576 MB, throughput 0.994776
Reading from 21623: heap size 578 MB, throughput 0.992778
Recommendation: one client; give it all the memory
Reading from 21623: heap size 577 MB, throughput 0.993169
Reading from 21623: heap size 578 MB, throughput 0.992807
Reading from 21623: heap size 580 MB, throughput 0.994054
Recommendation: one client; give it all the memory
Reading from 21623: heap size 581 MB, throughput 0.937587
Reading from 21623: heap size 577 MB, throughput 0.984407
Reading from 21623: heap size 580 MB, throughput 0.995054
Reading from 21623: heap size 577 MB, throughput 0.993977
Recommendation: one client; give it all the memory
Reading from 21623: heap size 579 MB, throughput 0.59109
Reading from 21623: heap size 568 MB, throughput 0.996419
Recommendation: one client; give it all the memory
Reading from 21623: heap size 569 MB, throughput 0.995689
Reading from 21623: heap size 571 MB, throughput 0.992073
Reading from 21623: heap size 565 MB, throughput 0.933676
Client 21623 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
