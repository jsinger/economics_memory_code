module Economem.UnconstrainedBenchmarkTimer where

import Economem.Common
import Economem.BenchmarkTimer
import Economem.Experiment
import Economem.Files
import Economem.Java
import Economem.Records

newUnconstrainedBenchmarkTimer :: Input BenchmarkThreadsIters ->
                                  Integer ->
                                  Java ->
                                  Maybe DataSize ->
                                  Bool ->
                                  Maybe SubexpInfo ->
                                  [String] ->
                                  IO Experiment
newUnconstrainedBenchmarkTimer input runs j maxHeap forceGrowth info extraVMArgs = do
  xmx <- case maxHeap of
    Just h -> return h
    Nothing -> systemMemory
  newBenchmarkTimer "timeBenchmarkUnconstrained" input xmx runs j Nothing 0 [] [] Nothing forceGrowth info extraVMArgs
