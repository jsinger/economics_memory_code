economemd
    total memory: 7435 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: no

    reading receiver name: vengerov
    command receiver name: vengerov

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub4_timerComparisonSingle/sub3_timeBenchmarkDaemon/daemon_run05_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
TimerThread: starting
CommandThread: starting
ReadingThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 3468: heap size 9 MB, throughput 0.990092
Clients: 1
Reading from 3470: heap size 9 MB, throughput 0.99
Clients: 2
Client 3468 has a minimum heap size of 1211 MB
Client 3470 has a minimum heap size of 276 MB
Reading from 3470: heap size 9 MB, throughput 0.986752
Reading from 3468: heap size 9 MB, throughput 0.988153
Reading from 3470: heap size 9 MB, throughput 0.981216
Reading from 3468: heap size 9 MB, throughput 0.980966
Reading from 3470: heap size 9 MB, throughput 0.97352
Reading from 3468: heap size 9 MB, throughput 0.973214
Reading from 3470: heap size 11 MB, throughput 0.981289
Reading from 3468: heap size 11 MB, throughput 0.974272
Reading from 3470: heap size 11 MB, throughput 0.972829
Reading from 3468: heap size 11 MB, throughput 0.965952
Reading from 3470: heap size 17 MB, throughput 0.944237
Reading from 3470: heap size 17 MB, throughput 0.897639
Reading from 3468: heap size 17 MB, throughput 0.965244
Reading from 3470: heap size 30 MB, throughput 0.746465
Reading from 3468: heap size 17 MB, throughput 0.898826
Reading from 3470: heap size 31 MB, throughput 0.299945
Reading from 3468: heap size 30 MB, throughput 0.857056
Reading from 3468: heap size 31 MB, throughput 0.607737
Reading from 3470: heap size 35 MB, throughput 0.709836
Reading from 3470: heap size 47 MB, throughput 0.497479
Reading from 3468: heap size 34 MB, throughput 0.78052
Reading from 3468: heap size 44 MB, throughput 0.298193
Reading from 3470: heap size 49 MB, throughput 0.504893
Reading from 3468: heap size 49 MB, throughput 0.117429
Reading from 3470: heap size 51 MB, throughput 0.601102
Reading from 3468: heap size 50 MB, throughput 0.420206
Reading from 3470: heap size 76 MB, throughput 0.539523
Reading from 3468: heap size 71 MB, throughput 0.278684
Reading from 3470: heap size 76 MB, throughput 0.50024
Reading from 3468: heap size 91 MB, throughput 0.586314
Reading from 3470: heap size 103 MB, throughput 0.735564
Reading from 3468: heap size 95 MB, throughput 0.54842
Reading from 3470: heap size 104 MB, throughput 0.573329
Reading from 3468: heap size 96 MB, throughput 0.21162
Reading from 3470: heap size 105 MB, throughput 0.361978
Reading from 3470: heap size 108 MB, throughput 0.354302
Reading from 3468: heap size 102 MB, throughput 0.497023
Reading from 3468: heap size 127 MB, throughput 0.128844
Reading from 3468: heap size 132 MB, throughput 0.355518
Reading from 3470: heap size 112 MB, throughput 0.446269
Reading from 3468: heap size 134 MB, throughput 0.263348
Reading from 3470: heap size 147 MB, throughput 0.363897
Reading from 3470: heap size 151 MB, throughput 0.177505
Reading from 3468: heap size 137 MB, throughput 0.5298
Reading from 3468: heap size 173 MB, throughput 0.265707
Reading from 3470: heap size 154 MB, throughput 0.373669
Reading from 3468: heap size 178 MB, throughput 0.688738
Reading from 3468: heap size 181 MB, throughput 0.657271
Reading from 3470: heap size 198 MB, throughput 0.309417
Reading from 3468: heap size 189 MB, throughput 0.703715
Reading from 3470: heap size 199 MB, throughput 0.408686
Reading from 3468: heap size 192 MB, throughput 0.680753
Reading from 3470: heap size 200 MB, throughput 0.715631
Reading from 3470: heap size 204 MB, throughput 0.814217
Reading from 3468: heap size 195 MB, throughput 0.400785
Reading from 3468: heap size 242 MB, throughput 0.678567
Reading from 3468: heap size 248 MB, throughput 0.675349
Reading from 3470: heap size 209 MB, throughput 0.869648
Reading from 3468: heap size 253 MB, throughput 0.662891
Reading from 3468: heap size 256 MB, throughput 0.664735
Reading from 3470: heap size 217 MB, throughput 0.839372
Reading from 3470: heap size 223 MB, throughput 0.733342
Reading from 3468: heap size 264 MB, throughput 0.621815
Reading from 3468: heap size 268 MB, throughput 0.559518
Reading from 3470: heap size 228 MB, throughput 0.795199
Reading from 3468: heap size 278 MB, throughput 0.551464
Reading from 3470: heap size 234 MB, throughput 0.764641
Reading from 3470: heap size 238 MB, throughput 0.729585
Reading from 3470: heap size 244 MB, throughput 0.720491
Reading from 3468: heap size 285 MB, throughput 0.432247
Reading from 3470: heap size 246 MB, throughput 0.762358
Reading from 3468: heap size 339 MB, throughput 0.499649
Reading from 3470: heap size 248 MB, throughput 0.742511
Reading from 3468: heap size 332 MB, throughput 0.52976
Reading from 3470: heap size 253 MB, throughput 0.696411
Reading from 3470: heap size 260 MB, throughput 0.672159
Reading from 3470: heap size 263 MB, throughput 0.680933
Reading from 3468: heap size 276 MB, throughput 0.255542
Reading from 3468: heap size 383 MB, throughput 0.522108
Reading from 3468: heap size 387 MB, throughput 0.570871
Reading from 3468: heap size 383 MB, throughput 0.649311
Reading from 3468: heap size 386 MB, throughput 0.619501
Reading from 3468: heap size 381 MB, throughput 0.595147
Reading from 3468: heap size 384 MB, throughput 0.603561
Reading from 3470: heap size 270 MB, throughput 0.666679
Reading from 3468: heap size 387 MB, throughput 0.632307
Reading from 3468: heap size 390 MB, throughput 0.620474
Reading from 3468: heap size 394 MB, throughput 0.563559
Reading from 3470: heap size 307 MB, throughput 0.82675
Reading from 3470: heap size 300 MB, throughput 0.775175
Reading from 3468: heap size 400 MB, throughput 0.551264
Reading from 3468: heap size 404 MB, throughput 0.54989
Reading from 3470: heap size 304 MB, throughput 0.45288
Reading from 3470: heap size 353 MB, throughput 0.641804
Reading from 3470: heap size 353 MB, throughput 0.688465
Reading from 3468: heap size 413 MB, throughput 0.374797
Reading from 3470: heap size 360 MB, throughput 0.859604
Reading from 3470: heap size 360 MB, throughput 0.821677
Reading from 3468: heap size 477 MB, throughput 0.525482
Reading from 3470: heap size 357 MB, throughput 0.775629
Reading from 3468: heap size 480 MB, throughput 0.568937
Reading from 3470: heap size 361 MB, throughput 0.725129
Reading from 3468: heap size 476 MB, throughput 0.609882
Reading from 3470: heap size 363 MB, throughput 0.743053
Reading from 3470: heap size 364 MB, throughput 0.6915
Numeric result:
Recommendation: 2 clients, utility 0.363396:
    h1: 1211 MB (U(h) = 0.575272*h^0.001)
    h2: 6224 MB (U(h) = 0.595739*h^0.0058955)
Recommendation: 2 clients, utility 0.363396:
    h1: 1211 MB (U(h) = 0.575272*h^0.001)
    h2: 6224 MB (U(h) = 0.595739*h^0.0058955)
Reading from 3470: heap size 370 MB, throughput 0.663322
Reading from 3468: heap size 479 MB, throughput 0.246231
Reading from 3468: heap size 539 MB, throughput 0.544271
Reading from 3468: heap size 541 MB, throughput 0.561049
Reading from 3468: heap size 539 MB, throughput 0.473597
Reading from 3470: heap size 370 MB, throughput 0.916995
Reading from 3468: heap size 540 MB, throughput 0.484152
Reading from 3468: heap size 546 MB, throughput 0.535282
Reading from 3468: heap size 549 MB, throughput 0.56384
Reading from 3468: heap size 555 MB, throughput 0.587608
Reading from 3470: heap size 374 MB, throughput 0.945631
Reading from 3468: heap size 562 MB, throughput 0.331987
Reading from 3468: heap size 632 MB, throughput 0.518375
Reading from 3468: heap size 639 MB, throughput 0.564826
Reading from 3468: heap size 641 MB, throughput 0.582703
Reading from 3468: heap size 647 MB, throughput 0.714061
Reading from 3470: heap size 377 MB, throughput 0.953447
Reading from 3468: heap size 650 MB, throughput 0.828226
Reading from 3468: heap size 662 MB, throughput 0.809505
Reading from 3468: heap size 664 MB, throughput 0.819809
Reading from 3470: heap size 377 MB, throughput 0.957277
Reading from 3468: heap size 684 MB, throughput 0.740733
Reading from 3468: heap size 695 MB, throughput 0.646433
Reading from 3470: heap size 380 MB, throughput 0.963687
Reading from 3468: heap size 707 MB, throughput 0.499212
Reading from 3468: heap size 783 MB, throughput 0.473995
Reading from 3468: heap size 680 MB, throughput 0.509388
Reading from 3468: heap size 759 MB, throughput 0.437661
Reading from 3468: heap size 659 MB, throughput 0.187389
Reading from 3468: heap size 841 MB, throughput 0.317047
Reading from 3470: heap size 380 MB, throughput 0.966521
Reading from 3468: heap size 844 MB, throughput 0.355478
Numeric result:
Recommendation: 2 clients, utility 0.40361:
    h1: 1211 MB (U(h) = 0.548813*h^0.001)
    h2: 6224 MB (U(h) = 0.523736*h^0.038044)
Recommendation: 2 clients, utility 0.40361:
    h1: 1211 MB (U(h) = 0.548813*h^0.001)
    h2: 6224 MB (U(h) = 0.523736*h^0.038044)
Reading from 3468: heap size 848 MB, throughput 0.416482
Reading from 3468: heap size 849 MB, throughput 0.470546
Reading from 3470: heap size 383 MB, throughput 0.965161
Reading from 3468: heap size 852 MB, throughput 0.281891
Reading from 3468: heap size 946 MB, throughput 0.64244
Reading from 3468: heap size 949 MB, throughput 0.686294
Reading from 3468: heap size 950 MB, throughput 0.730576
Reading from 3468: heap size 950 MB, throughput 0.784052
Reading from 3468: heap size 952 MB, throughput 0.797802
Reading from 3468: heap size 943 MB, throughput 0.922318
Reading from 3468: heap size 745 MB, throughput 0.916975
Reading from 3468: heap size 927 MB, throughput 0.917823
Reading from 3468: heap size 751 MB, throughput 0.894942
Reading from 3470: heap size 380 MB, throughput 0.969525
Reading from 3468: heap size 911 MB, throughput 0.926336
Reading from 3468: heap size 759 MB, throughput 0.899812
Reading from 3468: heap size 895 MB, throughput 0.879344
Reading from 3468: heap size 759 MB, throughput 0.903241
Reading from 3468: heap size 891 MB, throughput 0.872966
Reading from 3468: heap size 895 MB, throughput 0.821978
Reading from 3468: heap size 881 MB, throughput 0.809965
Reading from 3468: heap size 799 MB, throughput 0.805283
Reading from 3470: heap size 383 MB, throughput 0.971321
Reading from 3468: heap size 875 MB, throughput 0.803272
Reading from 3468: heap size 803 MB, throughput 0.800671
Reading from 3468: heap size 868 MB, throughput 0.819845
Reading from 3468: heap size 808 MB, throughput 0.789066
Reading from 3468: heap size 862 MB, throughput 0.812183
Reading from 3468: heap size 809 MB, throughput 0.807222
Reading from 3468: heap size 857 MB, throughput 0.827938
Reading from 3468: heap size 862 MB, throughput 0.833415
Reading from 3470: heap size 385 MB, throughput 0.971505
Reading from 3468: heap size 855 MB, throughput 0.847027
Reading from 3468: heap size 859 MB, throughput 0.194748
Reading from 3470: heap size 385 MB, throughput 0.969913
Reading from 3468: heap size 859 MB, throughput 0.96555
Reading from 3468: heap size 861 MB, throughput 0.935155
Reading from 3468: heap size 866 MB, throughput 0.875702
Reading from 3470: heap size 389 MB, throughput 0.961901
Reading from 3468: heap size 870 MB, throughput 0.843486
Reading from 3468: heap size 876 MB, throughput 0.815419
Reading from 3468: heap size 879 MB, throughput 0.768272
Reading from 3468: heap size 886 MB, throughput 0.770018
Reading from 3468: heap size 887 MB, throughput 0.771174
Reading from 3468: heap size 894 MB, throughput 0.770745
Reading from 3468: heap size 894 MB, throughput 0.780787
Numeric result:
Recommendation: 2 clients, utility 0.833476:
    h1: 6049.91 MB (U(h) = 0.114992*h^0.262076)
    h2: 1385.09 MB (U(h) = 0.479357*h^0.0599913)
Recommendation: 2 clients, utility 0.833476:
    h1: 6050.09 MB (U(h) = 0.114992*h^0.262076)
    h2: 1384.91 MB (U(h) = 0.479357*h^0.0599913)
Reading from 3470: heap size 390 MB, throughput 0.963358
Reading from 3468: heap size 900 MB, throughput 0.747186
Reading from 3468: heap size 901 MB, throughput 0.814737
Reading from 3468: heap size 904 MB, throughput 0.823456
Reading from 3468: heap size 906 MB, throughput 0.85632
Reading from 3468: heap size 907 MB, throughput 0.817824
Reading from 3470: heap size 392 MB, throughput 0.975386
Reading from 3470: heap size 394 MB, throughput 0.960216
Reading from 3470: heap size 391 MB, throughput 0.936188
Reading from 3470: heap size 396 MB, throughput 0.88249
Reading from 3470: heap size 403 MB, throughput 0.879118
Reading from 3468: heap size 909 MB, throughput 0.70497
Reading from 3468: heap size 1005 MB, throughput 0.532899
Reading from 3468: heap size 1013 MB, throughput 0.547786
Reading from 3468: heap size 1018 MB, throughput 0.598002
Reading from 3468: heap size 1018 MB, throughput 0.617503
Reading from 3468: heap size 1029 MB, throughput 0.610667
Reading from 3468: heap size 1029 MB, throughput 0.611497
Reading from 3470: heap size 406 MB, throughput 0.960119
Reading from 3468: heap size 1044 MB, throughput 0.609878
Reading from 3468: heap size 1045 MB, throughput 0.570369
Reading from 3468: heap size 1060 MB, throughput 0.573625
Reading from 3468: heap size 1062 MB, throughput 0.577388
Reading from 3468: heap size 1081 MB, throughput 0.58358
Reading from 3470: heap size 412 MB, throughput 0.971322
Reading from 3468: heap size 1083 MB, throughput 0.903469
Reading from 3470: heap size 414 MB, throughput 0.97556
Reading from 3468: heap size 1107 MB, throughput 0.918872
Numeric result:
Recommendation: 2 clients, utility 0.907187:
    h1: 5187.2 MB (U(h) = 0.136253*h^0.235548)
    h2: 2247.8 MB (U(h) = 0.403811*h^0.102075)
Recommendation: 2 clients, utility 0.907187:
    h1: 5187.15 MB (U(h) = 0.136253*h^0.235548)
    h2: 2247.85 MB (U(h) = 0.403811*h^0.102075)
Reading from 3470: heap size 415 MB, throughput 0.979586
Reading from 3468: heap size 1110 MB, throughput 0.92867
Reading from 3470: heap size 417 MB, throughput 0.980618
Reading from 3470: heap size 416 MB, throughput 0.974576
Reading from 3468: heap size 1129 MB, throughput 0.77941
Reading from 3468: heap size 1191 MB, throughput 0.963084
Reading from 3470: heap size 418 MB, throughput 0.98038
Reading from 3468: heap size 1198 MB, throughput 0.975295
Reading from 3470: heap size 415 MB, throughput 0.979948
Reading from 3468: heap size 1206 MB, throughput 0.978448
Reading from 3470: heap size 418 MB, throughput 0.981884
Numeric result:
Recommendation: 2 clients, utility 1.20882:
    h1: 5570.44 MB (U(h) = 0.0647068*h^0.349544)
    h2: 1864.56 MB (U(h) = 0.379656*h^0.117004)
Recommendation: 2 clients, utility 1.20882:
    h1: 5570.4 MB (U(h) = 0.0647068*h^0.349544)
    h2: 1864.6 MB (U(h) = 0.379656*h^0.117004)
Reading from 3468: heap size 1219 MB, throughput 0.980112
Reading from 3470: heap size 416 MB, throughput 0.981364
Reading from 3468: heap size 1220 MB, throughput 0.97957
Reading from 3470: heap size 418 MB, throughput 0.984646
Reading from 3470: heap size 416 MB, throughput 0.97972
Reading from 3470: heap size 418 MB, throughput 0.967444
Reading from 3470: heap size 421 MB, throughput 0.937428
Reading from 3468: heap size 1216 MB, throughput 0.977846
Reading from 3470: heap size 422 MB, throughput 0.953763
Reading from 3468: heap size 1222 MB, throughput 0.976887
Reading from 3470: heap size 429 MB, throughput 0.981887
Reading from 3468: heap size 1208 MB, throughput 0.972056
Numeric result:
Recommendation: 2 clients, utility 1.61416:
    h1: 5886.47 MB (U(h) = 0.0278438*h^0.477155)
    h2: 1548.53 MB (U(h) = 0.366441*h^0.125524)
Recommendation: 2 clients, utility 1.61416:
    h1: 5886.46 MB (U(h) = 0.0278438*h^0.477155)
    h2: 1548.54 MB (U(h) = 0.366441*h^0.125524)
Reading from 3470: heap size 430 MB, throughput 0.984652
Reading from 3468: heap size 1147 MB, throughput 0.965295
Reading from 3470: heap size 434 MB, throughput 0.985636
Reading from 3468: heap size 1209 MB, throughput 0.96916
Reading from 3470: heap size 435 MB, throughput 0.985597
Reading from 3468: heap size 1212 MB, throughput 0.970096
Reading from 3470: heap size 434 MB, throughput 0.985828
Reading from 3468: heap size 1216 MB, throughput 0.96593
Reading from 3470: heap size 436 MB, throughput 0.985077
Reading from 3468: heap size 1218 MB, throughput 0.9662
Numeric result:
Recommendation: 2 clients, utility 1.74081:
    h1: 5824.26 MB (U(h) = 0.0250297*h^0.494077)
    h2: 1610.74 MB (U(h) = 0.349766*h^0.136637)
Recommendation: 2 clients, utility 1.74081:
    h1: 5824.29 MB (U(h) = 0.0250297*h^0.494077)
    h2: 1610.71 MB (U(h) = 0.349766*h^0.136637)
Reading from 3470: heap size 435 MB, throughput 0.98497
Reading from 3468: heap size 1222 MB, throughput 0.962856
Reading from 3470: heap size 436 MB, throughput 0.983987
Reading from 3468: heap size 1228 MB, throughput 0.959465
Reading from 3470: heap size 438 MB, throughput 0.986594
Reading from 3470: heap size 438 MB, throughput 0.980072
Reading from 3468: heap size 1233 MB, throughput 0.95623
Reading from 3470: heap size 439 MB, throughput 0.966647
Reading from 3470: heap size 441 MB, throughput 0.954473
Reading from 3468: heap size 1241 MB, throughput 0.952667
Reading from 3470: heap size 447 MB, throughput 0.985778
Numeric result:
Recommendation: 2 clients, utility 2.06838:
    h1: 5923.18 MB (U(h) = 0.0150358*h^0.569515)
    h2: 1511.82 MB (U(h) = 0.337132*h^0.145361)
Recommendation: 2 clients, utility 2.06838:
    h1: 5923.19 MB (U(h) = 0.0150358*h^0.569515)
    h2: 1511.81 MB (U(h) = 0.337132*h^0.145361)
Reading from 3468: heap size 1249 MB, throughput 0.953271
Reading from 3470: heap size 448 MB, throughput 0.988114
Reading from 3468: heap size 1254 MB, throughput 0.951634
Reading from 3470: heap size 450 MB, throughput 0.988705
Reading from 3468: heap size 1262 MB, throughput 0.954205
Reading from 3470: heap size 451 MB, throughput 0.986911
Reading from 3468: heap size 1265 MB, throughput 0.953351
Reading from 3470: heap size 450 MB, throughput 0.987016
Reading from 3468: heap size 1272 MB, throughput 0.957335
Numeric result:
Recommendation: 2 clients, utility 2.79786:
    h1: 6141.65 MB (U(h) = 0.00540419*h^0.719621)
    h2: 1293.35 MB (U(h) = 0.328385*h^0.151551)
Recommendation: 2 clients, utility 2.79786:
    h1: 6141.59 MB (U(h) = 0.00540419*h^0.719621)
    h2: 1293.41 MB (U(h) = 0.328385*h^0.151551)
Reading from 3470: heap size 452 MB, throughput 0.98712
Reading from 3468: heap size 1274 MB, throughput 0.956666
Reading from 3470: heap size 453 MB, throughput 0.986186
Reading from 3468: heap size 1282 MB, throughput 0.955963
Reading from 3470: heap size 453 MB, throughput 0.967597
Reading from 3468: heap size 1283 MB, throughput 0.955571
Reading from 3470: heap size 475 MB, throughput 0.985156
Reading from 3470: heap size 475 MB, throughput 0.979135
Reading from 3470: heap size 477 MB, throughput 0.974499
Reading from 3468: heap size 1290 MB, throughput 0.95514
Reading from 3470: heap size 477 MB, throughput 0.989156
Numeric result:
Recommendation: 2 clients, utility 3.18434:
    h1: 6177.48 MB (U(h) = 0.00359423*h^0.778898)
    h2: 1257.52 MB (U(h) = 0.318649*h^0.158556)
Recommendation: 2 clients, utility 3.18434:
    h1: 6177.48 MB (U(h) = 0.00359423*h^0.778898)
    h2: 1257.52 MB (U(h) = 0.318649*h^0.158556)
Reading from 3470: heap size 484 MB, throughput 0.990949
Reading from 3468: heap size 1291 MB, throughput 0.955725
Reading from 3470: heap size 485 MB, throughput 0.991396
Reading from 3468: heap size 1297 MB, throughput 0.958505
Reading from 3470: heap size 485 MB, throughput 0.99089
Reading from 3468: heap size 1298 MB, throughput 0.956691
Reading from 3470: heap size 487 MB, throughput 0.989966
Numeric result:
Recommendation: 2 clients, utility 2.95875:
    h1: 6075.63 MB (U(h) = 0.00501809*h^0.730621)
    h2: 1359.37 MB (U(h) = 0.311885*h^0.16347)
Recommendation: 2 clients, utility 2.95875:
    h1: 6075.63 MB (U(h) = 0.00501809*h^0.730621)
    h2: 1359.37 MB (U(h) = 0.311885*h^0.16347)
Reading from 3468: heap size 1305 MB, throughput 0.931695
Reading from 3470: heap size 487 MB, throughput 0.98937
Reading from 3468: heap size 1420 MB, throughput 0.945795
Reading from 3470: heap size 487 MB, throughput 0.984446
Reading from 3470: heap size 489 MB, throughput 0.987403
Reading from 3470: heap size 491 MB, throughput 0.981092
Reading from 3468: heap size 1419 MB, throughput 0.962915
Reading from 3470: heap size 488 MB, throughput 0.970552
Reading from 3470: heap size 492 MB, throughput 0.984207
Numeric result:
Recommendation: 2 clients, utility 3.03184:
    h1: 6049.89 MB (U(h) = 0.00478455*h^0.737162)
    h2: 1385.11 MB (U(h) = 0.304719*h^0.168775)
Recommendation: 2 clients, utility 3.03184:
    h1: 6049.87 MB (U(h) = 0.00478455*h^0.737162)
    h2: 1385.13 MB (U(h) = 0.304719*h^0.168775)
Reading from 3468: heap size 1428 MB, throughput 0.97031
Reading from 3470: heap size 499 MB, throughput 0.990068
Reading from 3468: heap size 1440 MB, throughput 0.973031
Reading from 3470: heap size 501 MB, throughput 0.989679
Reading from 3468: heap size 1441 MB, throughput 0.973912
Reading from 3470: heap size 498 MB, throughput 0.989174
Reading from 3468: heap size 1437 MB, throughput 0.973443
Reading from 3470: heap size 501 MB, throughput 0.988359
Numeric result:
Recommendation: 2 clients, utility 2.79913:
    h1: 5940.75 MB (U(h) = 0.00681883*h^0.686012)
    h2: 1494.25 MB (U(h) = 0.299687*h^0.17255)
Recommendation: 2 clients, utility 2.79913:
    h1: 5940.75 MB (U(h) = 0.00681883*h^0.686012)
    h2: 1494.25 MB (U(h) = 0.299687*h^0.17255)
Reading from 3468: heap size 1441 MB, throughput 0.971525
Reading from 3470: heap size 504 MB, throughput 0.987789
Reading from 3468: heap size 1427 MB, throughput 0.970049
Reading from 3470: heap size 505 MB, throughput 0.986862
Reading from 3470: heap size 505 MB, throughput 0.985677
Reading from 3470: heap size 509 MB, throughput 0.977381
Reading from 3470: heap size 513 MB, throughput 0.974208
Numeric result:
Recommendation: 2 clients, utility 2.82826:
    h1: 5911.16 MB (U(h) = 0.00684745*h^0.685346)
    h2: 1523.84 MB (U(h) = 0.294228*h^0.176694)
Recommendation: 2 clients, utility 2.82826:
    h1: 5911.04 MB (U(h) = 0.00684745*h^0.685346)
    h2: 1523.96 MB (U(h) = 0.294228*h^0.176694)
Reading from 3470: heap size 514 MB, throughput 0.98792
Reading from 3470: heap size 518 MB, throughput 0.99008
Reading from 3470: heap size 519 MB, throughput 0.990754
Reading from 3470: heap size 517 MB, throughput 0.989991
Numeric result:
Recommendation: 2 clients, utility 2.86016:
    h1: 5884.26 MB (U(h) = 0.00684745*h^0.685346)
    h2: 1550.74 MB (U(h) = 0.289138*h^0.1806)
Recommendation: 2 clients, utility 2.86016:
    h1: 5884.37 MB (U(h) = 0.00684745*h^0.685346)
    h2: 1550.63 MB (U(h) = 0.289138*h^0.1806)
Reading from 3470: heap size 520 MB, throughput 0.989431
Reading from 3468: heap size 1435 MB, throughput 0.984148
Reading from 3470: heap size 522 MB, throughput 0.988574
Reading from 3470: heap size 522 MB, throughput 0.9902
Reading from 3470: heap size 524 MB, throughput 0.984515
Reading from 3470: heap size 525 MB, throughput 0.980219
Numeric result:
Recommendation: 2 clients, utility 4.30899:
    h1: 4788.31 MB (U(h) = 0.00600371*h^0.704046)
    h2: 2646.69 MB (U(h) = 0.0857028*h^0.38915)
Recommendation: 2 clients, utility 4.30899:
    h1: 4788.33 MB (U(h) = 0.00600371*h^0.704046)
    h2: 2646.67 MB (U(h) = 0.0857028*h^0.38915)
Reading from 3470: heap size 533 MB, throughput 0.990479
Reading from 3470: heap size 533 MB, throughput 0.99102
Reading from 3468: heap size 1394 MB, throughput 0.987656
Reading from 3470: heap size 532 MB, throughput 0.990987
Reading from 3470: heap size 534 MB, throughput 0.990171
Numeric result:
Recommendation: 2 clients, utility 5.24193:
    h1: 4576.51 MB (U(h) = 0.00467689*h^0.739617)
    h2: 2858.49 MB (U(h) = 0.0556598*h^0.461961)
Recommendation: 2 clients, utility 5.24193:
    h1: 4576.52 MB (U(h) = 0.00467689*h^0.739617)
    h2: 2858.48 MB (U(h) = 0.0556598*h^0.461961)
Reading from 3468: heap size 1435 MB, throughput 0.984444
Reading from 3468: heap size 1452 MB, throughput 0.973466
Reading from 3468: heap size 1468 MB, throughput 0.945951
Reading from 3468: heap size 1487 MB, throughput 0.910359
Reading from 3468: heap size 1514 MB, throughput 0.855207
Reading from 3468: heap size 1544 MB, throughput 0.784645
Reading from 3470: heap size 535 MB, throughput 0.990448
Reading from 3468: heap size 1560 MB, throughput 0.95204
Reading from 3468: heap size 1628 MB, throughput 0.822674
Reading from 3468: heap size 1629 MB, throughput 0.956961
Reading from 3470: heap size 535 MB, throughput 0.992584
Reading from 3470: heap size 537 MB, throughput 0.989428
Reading from 3470: heap size 538 MB, throughput 0.980607
Reading from 3468: heap size 1669 MB, throughput 0.980096
Reading from 3470: heap size 542 MB, throughput 0.987276
Numeric result:
Recommendation: 2 clients, utility 5.62312:
    h1: 3327.28 MB (U(h) = 0.0230521*h^0.511386)
    h2: 4107.72 MB (U(h) = 0.020171*h^0.631335)
Recommendation: 2 clients, utility 5.62312:
    h1: 3327.28 MB (U(h) = 0.0230521*h^0.511386)
    h2: 4107.72 MB (U(h) = 0.020171*h^0.631335)
Reading from 3468: heap size 1673 MB, throughput 0.982488
Reading from 3470: heap size 544 MB, throughput 0.991134
Reading from 3468: heap size 1678 MB, throughput 0.982491
Reading from 3470: heap size 545 MB, throughput 0.991637
Reading from 3468: heap size 1688 MB, throughput 0.980963
Reading from 3470: heap size 547 MB, throughput 0.991203
Numeric result:
Recommendation: 2 clients, utility 5.38001:
    h1: 2849.86 MB (U(h) = 0.0490014*h^0.406048)
    h2: 4585.14 MB (U(h) = 0.0176133*h^0.653287)
Recommendation: 2 clients, utility 5.38001:
    h1: 2849.87 MB (U(h) = 0.0490014*h^0.406048)
    h2: 4585.13 MB (U(h) = 0.0176133*h^0.653287)
Reading from 3468: heap size 1668 MB, throughput 0.979485
Reading from 3470: heap size 546 MB, throughput 0.990378
Reading from 3468: heap size 1684 MB, throughput 0.978058
Reading from 3470: heap size 548 MB, throughput 0.989354
Reading from 3468: heap size 1649 MB, throughput 0.976275
Reading from 3470: heap size 550 MB, throughput 0.989201
Reading from 3470: heap size 552 MB, throughput 0.983183
Reading from 3470: heap size 556 MB, throughput 0.985563
Numeric result:
Recommendation: 2 clients, utility 3.02108:
    h1: 3271.24 MB (U(h) = 0.0807374*h^0.336834)
    h2: 4163.76 MB (U(h) = 0.0687177*h^0.42883)
Recommendation: 2 clients, utility 3.02108:
    h1: 3270.84 MB (U(h) = 0.0807374*h^0.336834)
    h2: 4164.16 MB (U(h) = 0.0687177*h^0.42883)
Reading from 3468: heap size 1453 MB, throughput 0.973925
Reading from 3470: heap size 557 MB, throughput 0.990585
Reading from 3468: heap size 1627 MB, throughput 0.972716
Reading from 3470: heap size 560 MB, throughput 0.990534
Reading from 3468: heap size 1646 MB, throughput 0.970573
Reading from 3470: heap size 561 MB, throughput 0.990739
Numeric result:
Recommendation: 2 clients, utility 2.82289:
    h1: 3423.8 MB (U(h) = 0.0793302*h^0.339316)
    h2: 4011.2 MB (U(h) = 0.0830486*h^0.397585)
Recommendation: 2 clients, utility 2.82289:
    h1: 3423.54 MB (U(h) = 0.0793302*h^0.339316)
    h2: 4011.46 MB (U(h) = 0.0830486*h^0.397585)
Reading from 3468: heap size 1630 MB, throughput 0.968316
Reading from 3470: heap size 560 MB, throughput 0.990344
Reading from 3468: heap size 1638 MB, throughput 0.967778
Reading from 3470: heap size 562 MB, throughput 0.991935
Reading from 3470: heap size 563 MB, throughput 0.989058
Reading from 3470: heap size 564 MB, throughput 0.981507
Reading from 3468: heap size 1648 MB, throughput 0.96572
Numeric result:
Recommendation: 2 clients, utility 3.22139:
    h1: 3399.17 MB (U(h) = 0.0615166*h^0.374543)
    h2: 4035.83 MB (U(h) = 0.0620837*h^0.444662)
Recommendation: 2 clients, utility 3.22139:
    h1: 3399.3 MB (U(h) = 0.0615166*h^0.374543)
    h2: 4035.7 MB (U(h) = 0.0620837*h^0.444662)
Reading from 3470: heap size 568 MB, throughput 0.989336
Reading from 3468: heap size 1649 MB, throughput 0.966608
Reading from 3470: heap size 570 MB, throughput 0.990706
Reading from 3468: heap size 1661 MB, throughput 0.965036
Reading from 3470: heap size 572 MB, throughput 0.991171
Numeric result:
Recommendation: 2 clients, utility 3.41452:
    h1: 3321.58 MB (U(h) = 0.0589853*h^0.380218)
    h2: 4113.42 MB (U(h) = 0.0527145*h^0.470859)
Recommendation: 2 clients, utility 3.41452:
    h1: 3321.58 MB (U(h) = 0.0589853*h^0.380218)
    h2: 4113.42 MB (U(h) = 0.0527145*h^0.470859)
Reading from 3468: heap size 1667 MB, throughput 0.962341
Reading from 3470: heap size 574 MB, throughput 0.991188
Reading from 3468: heap size 1683 MB, throughput 0.961047
Reading from 3470: heap size 574 MB, throughput 0.990207
Reading from 3470: heap size 575 MB, throughput 0.990772
Reading from 3470: heap size 578 MB, throughput 0.985669
Reading from 3468: heap size 1688 MB, throughput 0.962849
Reading from 3470: heap size 580 MB, throughput 0.988388
Numeric result:
Recommendation: 2 clients, utility 3.46857:
    h1: 3330.76 MB (U(h) = 0.0558727*h^0.387514)
    h2: 4104.24 MB (U(h) = 0.0504332*h^0.477467)
Recommendation: 2 clients, utility 3.46857:
    h1: 3330.9 MB (U(h) = 0.0558727*h^0.387514)
    h2: 4104.1 MB (U(h) = 0.0504332*h^0.477467)
Reading from 3468: heap size 1705 MB, throughput 0.96054
Reading from 3470: heap size 587 MB, throughput 0.991413
Reading from 3468: heap size 1709 MB, throughput 0.959761
Reading from 3470: heap size 587 MB, throughput 0.991376
Numeric result:
Recommendation: 2 clients, utility 3.34906:
    h1: 3407.07 MB (U(h) = 0.0547481*h^0.390075)
    h2: 4027.93 MB (U(h) = 0.0557344*h^0.461165)
Recommendation: 2 clients, utility 3.34906:
    h1: 3407.04 MB (U(h) = 0.0547481*h^0.390075)
    h2: 4027.96 MB (U(h) = 0.0557344*h^0.461165)
Reading from 3470: heap size 588 MB, throughput 0.991187
Reading from 3468: heap size 1726 MB, throughput 0.963633
Reading from 3470: heap size 590 MB, throughput 0.990804
Reading from 3468: heap size 1729 MB, throughput 0.966728
Reading from 3470: heap size 591 MB, throughput 0.992184
Reading from 3470: heap size 592 MB, throughput 0.986197
Reading from 3468: heap size 1746 MB, throughput 0.964368
Reading from 3470: heap size 592 MB, throughput 0.987836
Numeric result:
Recommendation: 2 clients, utility 3.01537:
    h1: 3663.65 MB (U(h) = 0.0515931*h^0.397963)
    h2: 3771.35 MB (U(h) = 0.0764136*h^0.409695)
Recommendation: 2 clients, utility 3.01537:
    h1: 3663.5 MB (U(h) = 0.0515931*h^0.397963)
    h2: 3771.5 MB (U(h) = 0.0764136*h^0.409695)
Reading from 3470: heap size 595 MB, throughput 0.989039
Reading from 3468: heap size 1747 MB, throughput 0.961849
Reading from 3470: heap size 597 MB, throughput 0.990825
Reading from 3468: heap size 1765 MB, throughput 0.963808
Numeric result:
Recommendation: 2 clients, utility 2.25434:
    h1: 4551.31 MB (U(h) = 0.0498527*h^0.40245)
    h2: 2883.69 MB (U(h) = 0.199939*h^0.254979)
Recommendation: 2 clients, utility 2.25434:
    h1: 4551.39 MB (U(h) = 0.0498527*h^0.40245)
    h2: 2883.61 MB (U(h) = 0.199939*h^0.254979)
Reading from 3470: heap size 599 MB, throughput 0.991439
Reading from 3468: heap size 1766 MB, throughput 0.955987
Reading from 3470: heap size 600 MB, throughput 0.991499
Reading from 3470: heap size 601 MB, throughput 0.991484
Reading from 3470: heap size 602 MB, throughput 0.986366
Client 3470 died
Clients: 1
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 3468: heap size 1863 MB, throughput 0.99316
Recommendation: one client; give it all the memory
Reading from 3468: heap size 1864 MB, throughput 0.99264
Reading from 3468: heap size 1862 MB, throughput 0.987469
Reading from 3468: heap size 1873 MB, throughput 0.976175
Reading from 3468: heap size 1903 MB, throughput 0.961346
Client 3468 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
