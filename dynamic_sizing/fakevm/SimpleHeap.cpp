#include <Utilities.hpp>
#include <unistd.h>
#include "SimpleHeap.hpp"

SimpleHeap::SimpleHeap(unsigned long long capacityMB, unsigned long long minHeapMB, economem::DaemonProxy& proxy) : m_Capacity(capacityMB), m_UsedB(0), m_MinHeapB(minHeapMB * 1024 * 1024), m_Proxy(proxy)
{
    printf("Created a simple heap with capacity %llu MB, minHeap %llu MB\n", capacityMB, minHeapMB);
}

unsigned long long SimpleHeap::getCapacityMB() const
{
    return m_Capacity.getHeapSizeMB();
}

void SimpleHeap::alloc(unsigned long long bytes)
{
    if (!allocInternal(bytes))
    {
        // GC
        printf("Allocating failed, GCing...\n");
        m_Proxy.gcBegin();
        sleep(1);
        garbageCollect();
        m_Proxy.minorGCEnd();
        resizeHeap();

        if (!allocInternal(bytes))
            economem::die("Still can't alloc after GCing -- Out of memory!");
    }
}

bool SimpleHeap::allocInternal(unsigned long long bytes)
{
    if (m_UsedB + bytes <= m_Capacity.getHeapSizeBytes())
    {
        m_UsedB += bytes;
        economem::info("SimpleHeap", "Allocated %llu bytes; size now %llu bytes of %llu bytes (%f%%)", bytes, m_UsedB, m_Capacity.getHeapSizeBytes(), (m_UsedB * 100.0f)/m_Capacity.getHeapSizeBytes());
        return true;
    }
    else
        return false;
}

void SimpleHeap::garbageCollect()
{
    m_UsedB = (unsigned long long)(m_UsedB * 0.2);
    printf("Collected: Heap now at %llu of %llu bytes (%g%%)\n", m_UsedB, m_Capacity.getHeapSizeBytes(), (m_UsedB * 100.0)/m_Capacity.getHeapSizeBytes());
}

void SimpleHeap::resizeHeap()
{
    if (m_Proxy.getRecommendation(m_Capacity))
    {
        if (m_Capacity.getHeapSizeBytes() < m_UsedB)
            m_Capacity.setHeapSizeMB(m_UsedB / (1024 * 1024) + 1);
        else if (m_Capacity.getHeapSizeBytes() < m_MinHeapB)
            m_Capacity.setHeapSizeMB(m_MinHeapB / (1024 * 1024));
        printf("Resized: Heap now at %llu of %llu bytes (%g%%)\n", m_UsedB, m_Capacity.getHeapSizeBytes(), (m_UsedB * 100.0)/m_Capacity.getHeapSizeBytes());
    }
}
