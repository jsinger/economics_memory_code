#ifndef RECOMMENDATIONRECEIVER_HPP
#define RECOMMENDATIONRECEIVER_HPP

#include "Messages.hpp"
#include "RecvSock.hpp"

/*! \addtogroup common
  @{
*/

namespace economem
{
    //! Receive recommendations from a daemon (for use in clients).
    class RecommendationReceiver : public RecvSock<economem::Recommendation>
    {
    public:
        RecommendationReceiver();
    };
}

/*! @} */

#endif
