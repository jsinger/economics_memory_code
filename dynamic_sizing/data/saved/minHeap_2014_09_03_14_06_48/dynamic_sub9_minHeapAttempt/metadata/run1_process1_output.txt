GCBench: 1 threads, 3 iters



##########
GCBench iteration 1
##########

[0] Garbage Collector Test
[0]  Stretching memory with a binary tree of depth 22
 Total memory available=1288699904 bytes  Free memory=1261772248 bytes
[0]  Creating a long-lived binary tree of depth 22
[0]  Creating a long-lived array of 700000 doubles
 Total memory available=1288699904 bytes  Free memory=1005960456 bytes
Creating 8196 trees of depth 10
	Top down construction took 277msecs
	Bottom up construction took 93msecs
Creating 2048 trees of depth 12
	Top down construction took 157msecs
	Bottom up construction took 77msecs
Creating 512 trees of depth 14
	Top down construction took 94msecs
	Bottom up construction took 94msecs
Creating 128 trees of depth 16
	Top down construction took 91msecs
	Bottom up construction took 85msecs
Creating 32 trees of depth 18
	Top down construction took 90msecs
	Bottom up construction took 85msecs
Creating 8 trees of depth 20
	Top down construction took 103msecs
	Bottom up construction took 102msecs
Creating 2 trees of depth 22
	Top down construction took 281msecs
	Bottom up construction took 751msecs
 Total memory available=1195376640 bytes  Free memory=649884544 bytes
[0] Completed in 2505ms.
[harness] Finished all threads


##########
GCBench iteration 2
##########

[0] Garbage Collector Test
[0]  Stretching memory with a binary tree of depth 22
 Total memory available=1195376640 bytes  Free memory=645386144 bytes
[0]  Creating a long-lived binary tree of depth 22
[0]  Creating a long-lived array of 700000 doubles
 Total memory available=1195376640 bytes  Free memory=373769008 bytes
Creating 8196 trees of depth 10
	Top down construction took 567msecs
	Bottom up construction took 81msecs
Creating 2048 trees of depth 12
	Top down construction took 87msecs
	Bottom up construction took 93msecs
Creating 512 trees of depth 14
	Top down construction took 80msecs
	Bottom up construction took 77msecs
Creating 128 trees of depth 16
	Top down construction took 92msecs
	Bottom up construction took 79msecs
Creating 32 trees of depth 18
	Top down construction took 79msecs
	Bottom up construction took 82msecs
Creating 8 trees of depth 20
	Top down construction took 92msecs
	Bottom up construction took 77msecs
Creating 2 trees of depth 22
	Top down construction took 141msecs
	Bottom up construction took 251msecs
 Total memory available=1176502272 bytes  Free memory=518043064 bytes
[0] Completed in 2140ms.
[harness] Finished all threads


##########
GCBench iteration 3
##########

[0] Garbage Collector Test
[0]  Stretching memory with a binary tree of depth 22
 Total memory available=1176502272 bytes  Free memory=514069328 bytes
[0]  Creating a long-lived binary tree of depth 22
[0]  Creating a long-lived array of 700000 doubles
 Total memory available=1195376640 bytes  Free memory=312715528 bytes
Creating 8196 trees of depth 10
	Top down construction took 593msecs
	Bottom up construction took 86msecs
Creating 2048 trees of depth 12
	Top down construction took 90msecs
	Bottom up construction took 89msecs
Creating 512 trees of depth 14
	Top down construction took 75msecs
	Bottom up construction took 86msecs
Creating 128 trees of depth 16
	Top down construction took 85msecs
	Bottom up construction took 82msecs
Creating 32 trees of depth 18
	Top down construction took 78msecs
	Bottom up construction took 76msecs
Creating 8 trees of depth 20
	Top down construction took 83msecs
	Bottom up construction took 114msecs
Creating 2 trees of depth 22
	Top down construction took 89msecs
	Bottom up construction took 75msecs
 Total memory available=1270874112 bytes  Free memory=717364728 bytes
[0] Completed in 1979ms.
[harness] Finished all threads
