#include <cstdlib>
#include <sys/socket.h>
#include <unistd.h>
#include "SendSock.hpp"
#include "Utilities.hpp"

economem::SendSock::SendSock() : m_Socket(-1)
{
    if ((m_Socket = socket(AF_UNIX, SOCK_DGRAM, 0)) == -1)
        economem::die("Failed to create sender socket");
}

economem::SendSock::~SendSock()
{
    if (m_Socket != -1)
        close(m_Socket);
}

bool economem::SendSock::sendToDaemon(const char* type, const char* daemonName, const void* buf, size_t len)
{
    sockaddr_un addr;
    return makeDaemonAddr(addr, type, daemonName) && send(buf, len, addr);
}

bool economem::SendSock::sendToClient(const char* type, pid_t dest, const void* buf, size_t len)
{
    sockaddr_un addr;
    return makeClientAddr(addr, dest, type) && send(buf, len, addr);
}

bool economem::SendSock::send(const void* buf, size_t len, const sockaddr_un& addr)
{
    bool retval;

    m_Lock.lock();

    if (m_Socket == -1)
        retval = false;
    else if (sendto(m_Socket, buf, len, 0, (const sockaddr*)&addr, sizeof(addr)) == -1)
    {
        economem::warning("Failed to send message to %s", addr.sun_path);
        retval = false;
    }
    else
        retval = true;

    m_Lock.unlock();

    return retval;
}

bool economem::SendSock::makeDaemonAddr(sockaddr_un& addr, const char* type, const char* name)
{
    return makeAddr(addr, economem::allocDaemonSocketName(type, name));
}

bool economem::SendSock::makeClientAddr(sockaddr_un& addr, pid_t pid, const char* type)
{
    return makeAddr(addr, economem::allocClientSocketName(pid, type));
}

bool economem::SendSock::makeAddr(sockaddr_un& addr, char* sockName)
{
    if (sockName == NULL)
    {
        economem::warning("Failed to allocate string for socket name");
        return false;
    }

    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    strcpy(addr.sun_path, sockName);
    free(sockName);

    return true;
}
