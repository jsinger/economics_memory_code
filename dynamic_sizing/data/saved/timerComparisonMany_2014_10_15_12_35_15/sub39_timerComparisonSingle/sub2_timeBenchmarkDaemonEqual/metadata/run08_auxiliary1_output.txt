economemd
    total memory: 6130 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub39_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run08_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 4201: heap size 9 MB, throughput 0.987955
Clients: 1
Client 4201 has a minimum heap size of 3 MB
Reading from 4200: heap size 9 MB, throughput 0.987712
Clients: 2
Client 4200 has a minimum heap size of 1223 MB
Reading from 4201: heap size 9 MB, throughput 0.968801
Reading from 4200: heap size 9 MB, throughput 0.972661
Reading from 4201: heap size 11 MB, throughput 0.974113
Reading from 4200: heap size 11 MB, throughput 0.972214
Reading from 4201: heap size 11 MB, throughput 0.976254
Reading from 4200: heap size 11 MB, throughput 0.979472
Reading from 4200: heap size 15 MB, throughput 0.832866
Reading from 4201: heap size 15 MB, throughput 0.929852
Reading from 4200: heap size 18 MB, throughput 0.917214
Reading from 4201: heap size 18 MB, throughput 0.99225
Reading from 4200: heap size 24 MB, throughput 0.952159
Reading from 4201: heap size 22 MB, throughput 0.971718
Reading from 4200: heap size 28 MB, throughput 0.506109
Reading from 4201: heap size 27 MB, throughput 0.989216
Reading from 4200: heap size 40 MB, throughput 0.879764
Reading from 4201: heap size 28 MB, throughput 0.992636
Reading from 4200: heap size 41 MB, throughput 0.886993
Reading from 4201: heap size 28 MB, throughput 0.986183
Reading from 4200: heap size 44 MB, throughput 0.897953
Reading from 4201: heap size 31 MB, throughput 0.989498
Reading from 4201: heap size 31 MB, throughput 0.990476
Reading from 4201: heap size 34 MB, throughput 0.993069
Reading from 4200: heap size 46 MB, throughput 0.208287
Reading from 4201: heap size 34 MB, throughput 0.987467
Reading from 4200: heap size 62 MB, throughput 0.794882
Reading from 4201: heap size 38 MB, throughput 0.990489
Reading from 4200: heap size 65 MB, throughput 0.769346
Reading from 4201: heap size 38 MB, throughput 0.980165
Reading from 4201: heap size 42 MB, throughput 0.989152
Reading from 4201: heap size 42 MB, throughput 0.990121
Reading from 4200: heap size 68 MB, throughput 0.347673
Reading from 4201: heap size 45 MB, throughput 0.943054
Reading from 4200: heap size 89 MB, throughput 0.718412
Reading from 4200: heap size 95 MB, throughput 0.770592
Reading from 4201: heap size 45 MB, throughput 0.990251
Reading from 4201: heap size 50 MB, throughput 0.988027
Reading from 4200: heap size 97 MB, throughput 0.738382
Reading from 4201: heap size 50 MB, throughput 0.98268
Reading from 4200: heap size 100 MB, throughput 0.6614
Reading from 4201: heap size 56 MB, throughput 0.985661
Reading from 4201: heap size 56 MB, throughput 0.989494
Reading from 4201: heap size 61 MB, throughput 0.993773
Reading from 4200: heap size 105 MB, throughput 0.234076
Reading from 4201: heap size 62 MB, throughput 0.990438
Reading from 4200: heap size 134 MB, throughput 0.705214
Reading from 4201: heap size 66 MB, throughput 0.992018
Reading from 4200: heap size 137 MB, throughput 0.692231
Reading from 4201: heap size 66 MB, throughput 0.991771
Reading from 4200: heap size 141 MB, throughput 0.603161
Reading from 4200: heap size 144 MB, throughput 0.588929
Reading from 4201: heap size 69 MB, throughput 0.978846
Reading from 4201: heap size 70 MB, throughput 0.993574
Reading from 4201: heap size 74 MB, throughput 0.992326
Reading from 4201: heap size 75 MB, throughput 0.995196
Reading from 4200: heap size 150 MB, throughput 0.0902605
Reading from 4201: heap size 79 MB, throughput 0.977685
Reading from 4200: heap size 184 MB, throughput 0.631067
Reading from 4200: heap size 189 MB, throughput 0.678249
Reading from 4201: heap size 79 MB, throughput 0.991379
Reading from 4200: heap size 191 MB, throughput 0.749171
Reading from 4201: heap size 83 MB, throughput 0.975419
Reading from 4200: heap size 194 MB, throughput 0.60256
Reading from 4201: heap size 84 MB, throughput 0.990835
Reading from 4200: heap size 199 MB, throughput 0.607154
Reading from 4201: heap size 89 MB, throughput 0.992345
Reading from 4201: heap size 89 MB, throughput 0.992516
Reading from 4200: heap size 205 MB, throughput 0.152816
Reading from 4201: heap size 94 MB, throughput 0.99324
Reading from 4201: heap size 94 MB, throughput 0.990367
Reading from 4200: heap size 245 MB, throughput 0.472575
Reading from 4201: heap size 99 MB, throughput 0.990121
Reading from 4200: heap size 252 MB, throughput 0.599058
Reading from 4201: heap size 99 MB, throughput 0.989572
Reading from 4200: heap size 256 MB, throughput 0.593675
Reading from 4201: heap size 104 MB, throughput 0.993465
Reading from 4200: heap size 261 MB, throughput 0.661956
Reading from 4201: heap size 104 MB, throughput 0.990734
Reading from 4201: heap size 109 MB, throughput 0.993721
Reading from 4201: heap size 109 MB, throughput 0.990986
Reading from 4200: heap size 266 MB, throughput 0.122002
Reading from 4201: heap size 114 MB, throughput 0.995174
Reading from 4200: heap size 311 MB, throughput 0.560419
Reading from 4201: heap size 114 MB, throughput 0.993818
Reading from 4200: heap size 314 MB, throughput 0.642442
Reading from 4201: heap size 119 MB, throughput 0.994679
Reading from 4200: heap size 316 MB, throughput 0.644357
Reading from 4201: heap size 119 MB, throughput 0.99251
Reading from 4201: heap size 123 MB, throughput 0.995245
Reading from 4201: heap size 123 MB, throughput 0.989815
Reading from 4201: heap size 127 MB, throughput 0.993843
Reading from 4200: heap size 318 MB, throughput 0.141413
Reading from 4201: heap size 127 MB, throughput 0.990208
Reading from 4200: heap size 368 MB, throughput 0.439152
Reading from 4201: heap size 131 MB, throughput 0.992552
Reading from 4200: heap size 372 MB, throughput 0.65098
Reading from 4201: heap size 131 MB, throughput 0.991738
Reading from 4200: heap size 373 MB, throughput 0.575259
Reading from 4201: heap size 136 MB, throughput 0.993192
Reading from 4201: heap size 136 MB, throughput 0.987615
Reading from 4200: heap size 377 MB, throughput 0.498055
Reading from 4201: heap size 141 MB, throughput 0.990088
Reading from 4200: heap size 380 MB, throughput 0.527888
Reading from 4201: heap size 141 MB, throughput 0.989357
Reading from 4200: heap size 389 MB, throughput 0.48975
Reading from 4201: heap size 147 MB, throughput 0.992488
Reading from 4201: heap size 147 MB, throughput 0.993606
Equal recommendation: 3065 MB each
Reading from 4201: heap size 153 MB, throughput 0.995616
Reading from 4201: heap size 154 MB, throughput 0.994118
Reading from 4200: heap size 396 MB, throughput 0.085312
Reading from 4201: heap size 159 MB, throughput 0.978207
Reading from 4200: heap size 453 MB, throughput 0.45447
Reading from 4201: heap size 159 MB, throughput 0.992926
Reading from 4200: heap size 458 MB, throughput 0.520706
Reading from 4201: heap size 166 MB, throughput 0.994485
Reading from 4201: heap size 166 MB, throughput 0.991965
Reading from 4201: heap size 173 MB, throughput 0.994242
Reading from 4201: heap size 173 MB, throughput 0.996294
Reading from 4200: heap size 461 MB, throughput 0.107184
Reading from 4200: heap size 521 MB, throughput 0.521464
Reading from 4201: heap size 178 MB, throughput 0.995001
Reading from 4200: heap size 521 MB, throughput 0.628656
Reading from 4201: heap size 179 MB, throughput 0.993346
Reading from 4200: heap size 520 MB, throughput 0.595242
Reading from 4201: heap size 185 MB, throughput 0.993694
Reading from 4200: heap size 521 MB, throughput 0.575961
Reading from 4201: heap size 185 MB, throughput 0.994725
Reading from 4200: heap size 523 MB, throughput 0.557819
Reading from 4201: heap size 191 MB, throughput 0.995156
Reading from 4200: heap size 532 MB, throughput 0.480434
Reading from 4201: heap size 191 MB, throughput 0.993972
Reading from 4200: heap size 539 MB, throughput 0.434115
Reading from 4201: heap size 197 MB, throughput 0.994353
Reading from 4201: heap size 197 MB, throughput 0.993834
Reading from 4201: heap size 203 MB, throughput 0.99597
Reading from 4201: heap size 203 MB, throughput 0.995661
Reading from 4200: heap size 550 MB, throughput 0.108764
Reading from 4201: heap size 208 MB, throughput 0.994349
Reading from 4200: heap size 616 MB, throughput 0.395103
Reading from 4201: heap size 209 MB, throughput 0.994337
Reading from 4200: heap size 627 MB, throughput 0.506019
Reading from 4201: heap size 215 MB, throughput 0.993665
Reading from 4200: heap size 631 MB, throughput 0.476219
Reading from 4201: heap size 215 MB, throughput 0.993556
Reading from 4201: heap size 221 MB, throughput 0.995081
Reading from 4201: heap size 221 MB, throughput 0.994129
Reading from 4201: heap size 228 MB, throughput 0.995646
Reading from 4200: heap size 632 MB, throughput 0.0965883
Reading from 4201: heap size 228 MB, throughput 0.991504
Reading from 4200: heap size 703 MB, throughput 0.423853
Reading from 4201: heap size 234 MB, throughput 0.995905
Reading from 4201: heap size 234 MB, throughput 0.99417
Reading from 4200: heap size 624 MB, throughput 0.827431
Reading from 4201: heap size 241 MB, throughput 0.994894
Reading from 4201: heap size 241 MB, throughput 0.991983
Reading from 4200: heap size 704 MB, throughput 0.828663
Reading from 4201: heap size 248 MB, throughput 0.999998
Reading from 4201: heap size 248 MB, throughput 0.944444
Reading from 4201: heap size 248 MB, throughput 0.663627
Reading from 4201: heap size 248 MB, throughput 0.993253
Reading from 4200: heap size 704 MB, throughput 0.883224
Reading from 4200: heap size 708 MB, throughput 0.391954
Reading from 4201: heap size 256 MB, throughput 0.996366
Reading from 4200: heap size 722 MB, throughput 0.50126
Reading from 4200: heap size 728 MB, throughput 0.263074
Reading from 4201: heap size 256 MB, throughput 0.993939
Equal recommendation: 3065 MB each
Reading from 4201: heap size 263 MB, throughput 0.995379
Reading from 4201: heap size 263 MB, throughput 0.988384
Reading from 4201: heap size 270 MB, throughput 0.9944
Reading from 4201: heap size 270 MB, throughput 0.994897
Reading from 4200: heap size 739 MB, throughput 0.023785
Reading from 4200: heap size 818 MB, throughput 0.219602
Reading from 4201: heap size 279 MB, throughput 0.994783
Reading from 4201: heap size 279 MB, throughput 0.988402
Reading from 4200: heap size 821 MB, throughput 0.795729
Reading from 4200: heap size 808 MB, throughput 0.479298
Reading from 4201: heap size 288 MB, throughput 0.991597
Reading from 4200: heap size 815 MB, throughput 0.504073
Reading from 4200: heap size 811 MB, throughput 0.544772
Reading from 4201: heap size 288 MB, throughput 0.994832
Reading from 4201: heap size 298 MB, throughput 0.993904
Reading from 4201: heap size 299 MB, throughput 0.995942
Reading from 4201: heap size 308 MB, throughput 0.99397
Reading from 4201: heap size 308 MB, throughput 0.995621
Reading from 4200: heap size 814 MB, throughput 0.138459
Reading from 4201: heap size 317 MB, throughput 0.993465
Reading from 4200: heap size 896 MB, throughput 0.628583
Reading from 4200: heap size 898 MB, throughput 0.722014
Reading from 4201: heap size 318 MB, throughput 0.995007
Reading from 4200: heap size 906 MB, throughput 0.844478
Reading from 4201: heap size 327 MB, throughput 0.995486
Reading from 4201: heap size 327 MB, throughput 0.994638
Reading from 4201: heap size 337 MB, throughput 0.997026
Reading from 4200: heap size 907 MB, throughput 0.267176
Reading from 4200: heap size 1003 MB, throughput 0.639723
Reading from 4201: heap size 337 MB, throughput 0.995427
Reading from 4200: heap size 1004 MB, throughput 0.763302
Reading from 4200: heap size 1009 MB, throughput 0.799291
Reading from 4201: heap size 345 MB, throughput 0.995407
Reading from 4200: heap size 1010 MB, throughput 0.774909
Reading from 4200: heap size 1008 MB, throughput 0.794242
Reading from 4201: heap size 345 MB, throughput 0.994538
Reading from 4200: heap size 1012 MB, throughput 0.765124
Reading from 4200: heap size 1005 MB, throughput 0.8299
Reading from 4201: heap size 354 MB, throughput 0.995508
Reading from 4200: heap size 1010 MB, throughput 0.807211
Reading from 4200: heap size 1002 MB, throughput 0.815398
Reading from 4201: heap size 354 MB, throughput 0.995875
Reading from 4201: heap size 363 MB, throughput 0.995473
Equal recommendation: 3065 MB each
Reading from 4201: heap size 346 MB, throughput 0.99542
Reading from 4201: heap size 328 MB, throughput 0.995301
Reading from 4201: heap size 314 MB, throughput 0.995107
Reading from 4201: heap size 299 MB, throughput 0.995432
Reading from 4200: heap size 1008 MB, throughput 0.962873
Reading from 4200: heap size 1003 MB, throughput 0.673612
Reading from 4201: heap size 285 MB, throughput 0.995999
Reading from 4200: heap size 1011 MB, throughput 0.771153
Reading from 4200: heap size 1022 MB, throughput 0.76729
Reading from 4200: heap size 1027 MB, throughput 0.761006
Reading from 4200: heap size 1038 MB, throughput 0.695055
Reading from 4201: heap size 271 MB, throughput 0.997921
Reading from 4200: heap size 1040 MB, throughput 0.753612
Reading from 4200: heap size 1028 MB, throughput 0.788873
Reading from 4201: heap size 259 MB, throughput 0.994635
Reading from 4201: heap size 247 MB, throughput 0.994023
Reading from 4200: heap size 1035 MB, throughput 0.882394
Reading from 4201: heap size 236 MB, throughput 0.99467
Reading from 4201: heap size 224 MB, throughput 0.99018
Reading from 4200: heap size 1032 MB, throughput 0.855594
Reading from 4201: heap size 231 MB, throughput 0.990499
Reading from 4200: heap size 1036 MB, throughput 0.827253
Reading from 4201: heap size 239 MB, throughput 0.989618
Reading from 4200: heap size 1035 MB, throughput 0.715086
Reading from 4200: heap size 1038 MB, throughput 0.569006
Reading from 4201: heap size 249 MB, throughput 0.994254
Reading from 4200: heap size 1052 MB, throughput 0.59093
Reading from 4201: heap size 258 MB, throughput 0.994353
Reading from 4200: heap size 1062 MB, throughput 0.562956
Reading from 4201: heap size 267 MB, throughput 0.994552
Reading from 4201: heap size 275 MB, throughput 0.995749
Reading from 4201: heap size 284 MB, throughput 0.996061
Reading from 4201: heap size 271 MB, throughput 0.99449
Reading from 4201: heap size 259 MB, throughput 0.994432
Reading from 4201: heap size 246 MB, throughput 0.995057
Reading from 4200: heap size 1079 MB, throughput 0.0692032
Reading from 4201: heap size 235 MB, throughput 0.991126
Reading from 4200: heap size 1196 MB, throughput 0.523226
Reading from 4201: heap size 242 MB, throughput 0.995904
Reading from 4200: heap size 1202 MB, throughput 0.605889
Reading from 4200: heap size 1204 MB, throughput 0.648732
Reading from 4201: heap size 231 MB, throughput 0.994894
Reading from 4201: heap size 221 MB, throughput 0.993024
Reading from 4200: heap size 1214 MB, throughput 0.617771
Reading from 4201: heap size 211 MB, throughput 0.994881
Reading from 4201: heap size 201 MB, throughput 0.978814
Reading from 4200: heap size 1218 MB, throughput 0.909496
Reading from 4201: heap size 210 MB, throughput 0.996083
Equal recommendation: 3065 MB each
Reading from 4201: heap size 219 MB, throughput 0.996464
Reading from 4201: heap size 226 MB, throughput 0.994316
Reading from 4201: heap size 234 MB, throughput 0.995614
Reading from 4201: heap size 223 MB, throughput 0.992966
Reading from 4201: heap size 213 MB, throughput 0.994427
Reading from 4201: heap size 204 MB, throughput 0.994704
Reading from 4201: heap size 195 MB, throughput 0.994649
Reading from 4201: heap size 186 MB, throughput 0.994291
Reading from 4201: heap size 177 MB, throughput 0.99541
Reading from 4201: heap size 169 MB, throughput 0.994946
Reading from 4201: heap size 162 MB, throughput 0.993353
Reading from 4201: heap size 155 MB, throughput 0.994244
Reading from 4201: heap size 148 MB, throughput 0.992764
Reading from 4200: heap size 1219 MB, throughput 0.965293
Reading from 4201: heap size 141 MB, throughput 0.993483
Reading from 4201: heap size 135 MB, throughput 0.993554
Reading from 4201: heap size 129 MB, throughput 0.992985
Reading from 4201: heap size 123 MB, throughput 0.992649
Reading from 4201: heap size 118 MB, throughput 0.993248
Reading from 4201: heap size 114 MB, throughput 0.991521
Reading from 4201: heap size 117 MB, throughput 0.992799
Reading from 4201: heap size 121 MB, throughput 0.992829
Reading from 4201: heap size 125 MB, throughput 0.99341
Reading from 4201: heap size 120 MB, throughput 0.992562
Reading from 4201: heap size 115 MB, throughput 0.99141
Reading from 4201: heap size 119 MB, throughput 0.992486
Reading from 4201: heap size 123 MB, throughput 0.992996
Reading from 4201: heap size 127 MB, throughput 0.99295
Reading from 4201: heap size 131 MB, throughput 0.993422
Reading from 4201: heap size 126 MB, throughput 0.99381
Reading from 4201: heap size 120 MB, throughput 0.99316
Reading from 4201: heap size 116 MB, throughput 0.991755
Reading from 4201: heap size 111 MB, throughput 0.990086
Reading from 4201: heap size 114 MB, throughput 0.992217
Reading from 4201: heap size 119 MB, throughput 0.992848
Reading from 4200: heap size 1227 MB, throughput 0.96043
Reading from 4201: heap size 123 MB, throughput 0.995372
Reading from 4201: heap size 119 MB, throughput 0.992902
Reading from 4201: heap size 113 MB, throughput 0.99242
Reading from 4201: heap size 118 MB, throughput 0.99357
Reading from 4201: heap size 112 MB, throughput 0.992233
Reading from 4201: heap size 116 MB, throughput 0.993121
Reading from 4201: heap size 121 MB, throughput 0.992646
Reading from 4201: heap size 125 MB, throughput 0.99297
Reading from 4201: heap size 128 MB, throughput 0.993703
Reading from 4201: heap size 124 MB, throughput 0.993874
Reading from 4201: heap size 118 MB, throughput 0.983934
Reading from 4201: heap size 124 MB, throughput 0.992995
Reading from 4201: heap size 128 MB, throughput 0.992956
Reading from 4201: heap size 133 MB, throughput 0.993376
Reading from 4201: heap size 139 MB, throughput 0.993357
Reading from 4201: heap size 144 MB, throughput 0.993818
Reading from 4201: heap size 148 MB, throughput 0.994171
Reading from 4201: heap size 142 MB, throughput 0.993701
Reading from 4201: heap size 136 MB, throughput 0.993017
Reading from 4201: heap size 130 MB, throughput 0.888843
Reading from 4201: heap size 124 MB, throughput 0.991827
Reading from 4201: heap size 132 MB, throughput 0.993458
Reading from 4201: heap size 139 MB, throughput 0.993672
Reading from 4200: heap size 1217 MB, throughput 0.956647
Reading from 4201: heap size 147 MB, throughput 0.994265
Reading from 4201: heap size 155 MB, throughput 0.958976
Equal recommendation: 3065 MB each
Reading from 4201: heap size 170 MB, throughput 0.993552
Reading from 4201: heap size 184 MB, throughput 0.994383
Reading from 4201: heap size 199 MB, throughput 0.994863
Reading from 4201: heap size 213 MB, throughput 0.994628
Reading from 4201: heap size 228 MB, throughput 0.995431
Reading from 4201: heap size 241 MB, throughput 0.99502
Reading from 4201: heap size 256 MB, throughput 0.995477
Reading from 4201: heap size 269 MB, throughput 0.995287
Reading from 4201: heap size 284 MB, throughput 0.995438
Reading from 4201: heap size 298 MB, throughput 0.993749
Reading from 4200: heap size 1228 MB, throughput 0.955017
Reading from 4201: heap size 311 MB, throughput 0.995439
Reading from 4201: heap size 313 MB, throughput 0.996236
Reading from 4201: heap size 313 MB, throughput 0.995235
Reading from 4201: heap size 328 MB, throughput 0.994029
Reading from 4201: heap size 328 MB, throughput 0.995128
Reading from 4201: heap size 343 MB, throughput 0.995654
Reading from 4201: heap size 343 MB, throughput 0.994941
Reading from 4201: heap size 359 MB, throughput 0.995169
Reading from 4200: heap size 1232 MB, throughput 0.958478
Reading from 4201: heap size 359 MB, throughput 0.995646
Reading from 4201: heap size 375 MB, throughput 0.995431
Reading from 4201: heap size 375 MB, throughput 0.994914
Reading from 4201: heap size 392 MB, throughput 0.994989
Reading from 4201: heap size 392 MB, throughput 0.994834
Reading from 4201: heap size 409 MB, throughput 0.995181
Reading from 4201: heap size 410 MB, throughput 0.994296
Reading from 4201: heap size 429 MB, throughput 0.993685
Reading from 4200: heap size 1236 MB, throughput 0.947711
Equal recommendation: 3065 MB each
Reading from 4201: heap size 429 MB, throughput 0.995117
Reading from 4201: heap size 450 MB, throughput 0.995526
Reading from 4201: heap size 450 MB, throughput 0.994939
Reading from 4201: heap size 472 MB, throughput 0.996239
Reading from 4201: heap size 472 MB, throughput 0.995392
Reading from 4201: heap size 493 MB, throughput 0.995628
Reading from 4200: heap size 1242 MB, throughput 0.969185
Reading from 4201: heap size 493 MB, throughput 0.995477
Reading from 4201: heap size 515 MB, throughput 0.995421
Reading from 4201: heap size 515 MB, throughput 0.995701
Reading from 4201: heap size 537 MB, throughput 0.995332
Reading from 4201: heap size 538 MB, throughput 0.995702
Reading from 4201: heap size 561 MB, throughput 0.996096
Reading from 4200: heap size 1245 MB, throughput 0.97302
Reading from 4201: heap size 561 MB, throughput 0.996076
Reading from 4201: heap size 584 MB, throughput 0.995886
Reading from 4201: heap size 585 MB, throughput 0.995754
Reading from 4201: heap size 609 MB, throughput 0.99752
Equal recommendation: 3065 MB each
Reading from 4201: heap size 609 MB, throughput 0.995866
Reading from 4200: heap size 1239 MB, throughput 0.966807
Reading from 4201: heap size 632 MB, throughput 0.995873
Reading from 4201: heap size 632 MB, throughput 0.995902
Reading from 4201: heap size 657 MB, throughput 0.995521
Reading from 4201: heap size 657 MB, throughput 0.995642
Reading from 4201: heap size 684 MB, throughput 0.994866
Reading from 4200: heap size 1245 MB, throughput 0.963334
Reading from 4201: heap size 684 MB, throughput 0.995997
Reading from 4201: heap size 713 MB, throughput 0.995863
Reading from 4201: heap size 713 MB, throughput 0.995958
Reading from 4201: heap size 743 MB, throughput 0.996181
Reading from 4200: heap size 1245 MB, throughput 0.961324
Reading from 4201: heap size 743 MB, throughput 0.996272
Reading from 4201: heap size 773 MB, throughput 0.995296
Equal recommendation: 3065 MB each
Reading from 4201: heap size 773 MB, throughput 0.996088
Reading from 4201: heap size 804 MB, throughput 0.9956
Reading from 4200: heap size 1247 MB, throughput 0.961501
Reading from 4201: heap size 805 MB, throughput 0.996019
Reading from 4201: heap size 838 MB, throughput 0.996029
Reading from 4201: heap size 838 MB, throughput 0.99598
Reading from 4201: heap size 872 MB, throughput 0.995246
Reading from 4200: heap size 1253 MB, throughput 0.961223
Reading from 4201: heap size 872 MB, throughput 0.996129
Reading from 4201: heap size 908 MB, throughput 0.996226
Reading from 4201: heap size 908 MB, throughput 0.996186
Reading from 4200: heap size 1255 MB, throughput 0.959977
Reading from 4201: heap size 944 MB, throughput 0.996094
Equal recommendation: 3065 MB each
Reading from 4201: heap size 945 MB, throughput 0.996198
Reading from 4201: heap size 983 MB, throughput 0.996282
Reading from 4201: heap size 983 MB, throughput 0.996145
Reading from 4200: heap size 1261 MB, throughput 0.95677
Reading from 4201: heap size 1021 MB, throughput 0.996312
Reading from 4201: heap size 1021 MB, throughput 0.996315
Reading from 4201: heap size 1061 MB, throughput 0.996204
Reading from 4200: heap size 1265 MB, throughput 0.956978
Reading from 4201: heap size 1061 MB, throughput 0.996129
Reading from 4201: heap size 1102 MB, throughput 0.996356
Equal recommendation: 3065 MB each
Reading from 4201: heap size 1102 MB, throughput 0.996245
Reading from 4201: heap size 1145 MB, throughput 0.997362
Reading from 4200: heap size 1272 MB, throughput 0.645206
Reading from 4201: heap size 1145 MB, throughput 0.99654
Reading from 4201: heap size 1186 MB, throughput 0.99648
Reading from 4200: heap size 1350 MB, throughput 0.993585
Reading from 4201: heap size 1186 MB, throughput 0.996257
Reading from 4201: heap size 1229 MB, throughput 0.996321
Equal recommendation: 3065 MB each
Reading from 4201: heap size 1229 MB, throughput 0.996026
Reading from 4200: heap size 1350 MB, throughput 0.991663
Reading from 4201: heap size 1275 MB, throughput 0.996397
Reading from 4201: heap size 1275 MB, throughput 0.996406
Reading from 4200: heap size 1360 MB, throughput 0.988521
Reading from 4201: heap size 1323 MB, throughput 0.996235
Reading from 4201: heap size 1323 MB, throughput 0.996359
Reading from 4201: heap size 1373 MB, throughput 0.99529
Reading from 4200: heap size 1370 MB, throughput 0.981544
Reading from 4201: heap size 1373 MB, throughput 0.996173
Equal recommendation: 3065 MB each
Reading from 4201: heap size 1428 MB, throughput 0.996422
Reading from 4200: heap size 1371 MB, throughput 0.981328
Reading from 4201: heap size 1428 MB, throughput 0.996403
Reading from 4201: heap size 1482 MB, throughput 0.996456
Reading from 4200: heap size 1363 MB, throughput 0.977792
Reading from 4201: heap size 1483 MB, throughput 0.996382
Reading from 4201: heap size 1540 MB, throughput 0.996377
Equal recommendation: 3065 MB each
Reading from 4200: heap size 1257 MB, throughput 0.981925
Reading from 4201: heap size 1540 MB, throughput 0.996464
Reading from 4201: heap size 1598 MB, throughput 0.996144
Reading from 4201: heap size 1598 MB, throughput 0.996245
Reading from 4200: heap size 1350 MB, throughput 0.982668
Reading from 4201: heap size 1659 MB, throughput 0.996565
Reading from 4201: heap size 1659 MB, throughput 0.995972
Reading from 4200: heap size 1280 MB, throughput 0.975281
Equal recommendation: 3065 MB each
Reading from 4201: heap size 1722 MB, throughput 0.996253
Reading from 4200: heap size 1351 MB, throughput 0.974528
Reading from 4201: heap size 1722 MB, throughput 0.996306
Reading from 4201: heap size 1788 MB, throughput 0.996216
Reading from 4200: heap size 1354 MB, throughput 0.978438
Reading from 4201: heap size 1788 MB, throughput 0.996389
Reading from 4201: heap size 1856 MB, throughput 0.996342
Equal recommendation: 3065 MB each
Reading from 4200: heap size 1356 MB, throughput 0.972802
Reading from 4201: heap size 1857 MB, throughput 0.996443
Reading from 4201: heap size 1927 MB, throughput 0.996253
Reading from 4200: heap size 1359 MB, throughput 0.968747
Reading from 4201: heap size 1928 MB, throughput 0.996377
Reading from 4201: heap size 2001 MB, throughput 0.995999
Reading from 4200: heap size 1363 MB, throughput 0.967428
Equal recommendation: 3065 MB each
Reading from 4201: heap size 2001 MB, throughput 0.997501
Reading from 4200: heap size 1370 MB, throughput 0.971807
Reading from 4201: heap size 2043 MB, throughput 0.996433
Reading from 4201: heap size 2042 MB, throughput 0.996724
Reading from 4200: heap size 1375 MB, throughput 0.955338
Reading from 4201: heap size 2043 MB, throughput 0.996694
Equal recommendation: 3065 MB each
Reading from 4201: heap size 2043 MB, throughput 0.996674
Reading from 4200: heap size 1385 MB, throughput 0.965046
Reading from 4201: heap size 2043 MB, throughput 0.996708
Reading from 4200: heap size 1393 MB, throughput 0.956873
Reading from 4201: heap size 2043 MB, throughput 0.996638
Reading from 4201: heap size 2043 MB, throughput 0.996743
Reading from 4200: heap size 1399 MB, throughput 0.970704
Equal recommendation: 3065 MB each
Reading from 4201: heap size 2043 MB, throughput 0.996749
Reading from 4201: heap size 2043 MB, throughput 0.996619
Reading from 4200: heap size 1408 MB, throughput 0.964117
Reading from 4201: heap size 2043 MB, throughput 0.996681
Reading from 4201: heap size 2043 MB, throughput 0.996574
Equal recommendation: 3065 MB each
Reading from 4201: heap size 2043 MB, throughput 0.996454
Reading from 4201: heap size 2042 MB, throughput 0.996753
Reading from 4201: heap size 2042 MB, throughput 0.996547
Reading from 4201: heap size 2043 MB, throughput 0.996593
Reading from 4201: heap size 2043 MB, throughput 0.996281
Equal recommendation: 3065 MB each
Reading from 4201: heap size 2043 MB, throughput 0.996456
Reading from 4201: heap size 2042 MB, throughput 0.997833
Reading from 4200: heap size 1411 MB, throughput 0.867191
Reading from 4201: heap size 2043 MB, throughput 0.996343
Equal recommendation: 3065 MB each
Reading from 4201: heap size 2043 MB, throughput 0.999999
Reading from 4201: heap size 2043 MB, throughput 0.952381
Reading from 4201: heap size 2043 MB, throughput 0.00709463
Reading from 4201: heap size 2044 MB, throughput 0.996306
Reading from 4201: heap size 2044 MB, throughput 0.996354
Reading from 4201: heap size 2044 MB, throughput 0.996252
Reading from 4201: heap size 2043 MB, throughput 0.996623
Equal recommendation: 3065 MB each
Reading from 4200: heap size 1512 MB, throughput 0.982822
Reading from 4201: heap size 2043 MB, throughput 0.99615
Reading from 4201: heap size 2043 MB, throughput 0.996548
Reading from 4200: heap size 1560 MB, throughput 0.97691
Reading from 4200: heap size 1543 MB, throughput 0.783162
Reading from 4200: heap size 1552 MB, throughput 0.642615
Reading from 4201: heap size 2043 MB, throughput 0.995039
Reading from 4200: heap size 1571 MB, throughput 0.694024
Reading from 4200: heap size 1579 MB, throughput 0.70602
Reading from 4200: heap size 1611 MB, throughput 0.725778
Reading from 4200: heap size 1612 MB, throughput 0.74488
Reading from 4201: heap size 2044 MB, throughput 0.996684
Equal recommendation: 3065 MB each
Reading from 4200: heap size 1644 MB, throughput 0.954184
Reading from 4201: heap size 2044 MB, throughput 0.996548
Reading from 4201: heap size 2044 MB, throughput 0.996647
Reading from 4200: heap size 1645 MB, throughput 0.9773
Reading from 4201: heap size 2044 MB, throughput 0.996579
Reading from 4201: heap size 2044 MB, throughput 0.996585
Reading from 4200: heap size 1668 MB, throughput 0.980057
Equal recommendation: 3065 MB each
Reading from 4201: heap size 2044 MB, throughput 0.996655
Reading from 4201: heap size 2044 MB, throughput 0.996543
Reading from 4200: heap size 1675 MB, throughput 0.980352
Reading from 4201: heap size 2044 MB, throughput 0.996682
Reading from 4201: heap size 2044 MB, throughput 0.99679
Reading from 4200: heap size 1678 MB, throughput 0.979577
Equal recommendation: 3065 MB each
Reading from 4201: heap size 2044 MB, throughput 0.996737
Reading from 4201: heap size 2044 MB, throughput 0.996649
Reading from 4200: heap size 1688 MB, throughput 0.977625
Reading from 4201: heap size 2044 MB, throughput 0.996686
Reading from 4201: heap size 2043 MB, throughput 0.996616
Equal recommendation: 3065 MB each
Reading from 4200: heap size 1680 MB, throughput 0.977935
Reading from 4201: heap size 2043 MB, throughput 0.99674
Reading from 4201: heap size 2043 MB, throughput 0.996673
Reading from 4200: heap size 1691 MB, throughput 0.978808
Reading from 4201: heap size 2044 MB, throughput 0.9966
Reading from 4201: heap size 2044 MB, throughput 0.996586
Equal recommendation: 3065 MB each
Reading from 4201: heap size 2044 MB, throughput 0.995956
Reading from 4200: heap size 1692 MB, throughput 0.978834
Reading from 4201: heap size 2044 MB, throughput 0.996604
Reading from 4201: heap size 2044 MB, throughput 0.996666
Reading from 4200: heap size 1696 MB, throughput 0.978244
Reading from 4201: heap size 2044 MB, throughput 0.996617
Equal recommendation: 3065 MB each
Reading from 4201: heap size 2044 MB, throughput 0.99677
Reading from 4200: heap size 1693 MB, throughput 0.981968
Reading from 4201: heap size 2044 MB, throughput 0.99699
Reading from 4201: heap size 2044 MB, throughput 0.996656
Reading from 4201: heap size 2044 MB, throughput 0.996675
Reading from 4200: heap size 1700 MB, throughput 0.979071
Equal recommendation: 3065 MB each
Reading from 4201: heap size 2044 MB, throughput 0.996783
Reading from 4201: heap size 2044 MB, throughput 0.996679
Reading from 4200: heap size 1694 MB, throughput 0.975658
Reading from 4201: heap size 2044 MB, throughput 0.996732
Reading from 4201: heap size 2044 MB, throughput 0.996683
Equal recommendation: 3065 MB each
Reading from 4201: heap size 2044 MB, throughput 0.996585
Reading from 4200: heap size 1699 MB, throughput 0.970229
Reading from 4201: heap size 2044 MB, throughput 0.996739
Reading from 4201: heap size 2044 MB, throughput 0.996595
Reading from 4200: heap size 1706 MB, throughput 0.97265
Reading from 4201: heap size 2044 MB, throughput 0.996728
Equal recommendation: 3065 MB each
Reading from 4201: heap size 2044 MB, throughput 0.996567
Reading from 4201: heap size 2044 MB, throughput 0.997256
Reading from 4200: heap size 1710 MB, throughput 0.964915
Reading from 4201: heap size 2044 MB, throughput 0.996674
Reading from 4201: heap size 2044 MB, throughput 0.996677
Equal recommendation: 3065 MB each
Reading from 4200: heap size 1721 MB, throughput 0.96812
Reading from 4201: heap size 2044 MB, throughput 0.996756
Client 4201 died
Clients: 1
Reading from 4200: heap size 1730 MB, throughput 0.980398
Recommendation: one client; give it all the memory
Reading from 4200: heap size 1743 MB, throughput 0.860057
Recommendation: one client; give it all the memory
Reading from 4200: heap size 1727 MB, throughput 0.996323
Reading from 4200: heap size 1732 MB, throughput 0.994832
Recommendation: one client; give it all the memory
Reading from 4200: heap size 1745 MB, throughput 0.993792
Recommendation: one client; give it all the memory
Reading from 4200: heap size 1760 MB, throughput 0.992884
Reading from 4200: heap size 1760 MB, throughput 0.992011
Recommendation: one client; give it all the memory
Reading from 4200: heap size 1746 MB, throughput 0.990834
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 4200: heap size 1593 MB, throughput 0.992059
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 4200: heap size 1732 MB, throughput 0.995419
Reading from 4200: heap size 1751 MB, throughput 0.888587
Reading from 4200: heap size 1731 MB, throughput 0.815184
Reading from 4200: heap size 1761 MB, throughput 0.799872
Reading from 4200: heap size 1796 MB, throughput 0.8236
Client 4200 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
