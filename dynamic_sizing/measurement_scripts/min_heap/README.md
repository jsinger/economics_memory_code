Minimum Heap Size {#minHeap}
=================

Scripts to measure the minimum heap size at which a benchmark can run without crashing.


Basic scripts
-------------

 - `min_heap` -- measure the minimum heap for many benchmarks.
 - `resume_min_heap` -- resume an incomplete minimum heap run.

Internals
---------

 - `kill_on_exception.sh` -- used to reliably detect when a benchmark has crashed. Harder than it sounds!
