economemd
    total memory: 2974 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub1_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run09_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 28242: heap size 9 MB, throughput 0.990124
Clients: 1
Client 28242 has a minimum heap size of 1211 MB
Reading from 28243: heap size 9 MB, throughput 0.984041
Clients: 2
Client 28243 has a minimum heap size of 276 MB
Reading from 28243: heap size 9 MB, throughput 0.956076
Reading from 28242: heap size 9 MB, throughput 0.966612
Reading from 28242: heap size 9 MB, throughput 0.967145
Reading from 28243: heap size 11 MB, throughput 0.971403
Reading from 28242: heap size 9 MB, throughput 0.948702
Reading from 28243: heap size 11 MB, throughput 0.977433
Reading from 28242: heap size 11 MB, throughput 0.972259
Reading from 28242: heap size 11 MB, throughput 0.984773
Reading from 28243: heap size 15 MB, throughput 0.857996
Reading from 28242: heap size 17 MB, throughput 0.94212
Reading from 28243: heap size 18 MB, throughput 0.966904
Reading from 28242: heap size 17 MB, throughput 0.453213
Reading from 28243: heap size 23 MB, throughput 0.806546
Reading from 28242: heap size 30 MB, throughput 0.930888
Reading from 28243: heap size 28 MB, throughput 0.957104
Reading from 28242: heap size 31 MB, throughput 0.942307
Reading from 28243: heap size 29 MB, throughput 0.397893
Reading from 28242: heap size 35 MB, throughput 0.546241
Reading from 28243: heap size 39 MB, throughput 0.69279
Reading from 28242: heap size 47 MB, throughput 0.786902
Reading from 28243: heap size 45 MB, throughput 0.879316
Reading from 28242: heap size 50 MB, throughput 0.851771
Reading from 28243: heap size 46 MB, throughput 0.690086
Reading from 28242: heap size 51 MB, throughput 0.205118
Reading from 28242: heap size 71 MB, throughput 0.776188
Reading from 28243: heap size 49 MB, throughput 0.163311
Reading from 28242: heap size 72 MB, throughput 0.768335
Reading from 28242: heap size 78 MB, throughput 0.734271
Reading from 28243: heap size 64 MB, throughput 0.184551
Reading from 28243: heap size 87 MB, throughput 0.793789
Reading from 28243: heap size 89 MB, throughput 0.788603
Reading from 28242: heap size 80 MB, throughput 0.174793
Reading from 28243: heap size 92 MB, throughput 0.768723
Reading from 28242: heap size 112 MB, throughput 0.657149
Reading from 28243: heap size 94 MB, throughput 0.740467
Reading from 28242: heap size 112 MB, throughput 0.751768
Reading from 28243: heap size 99 MB, throughput 0.680869
Reading from 28242: heap size 116 MB, throughput 0.745325
Reading from 28242: heap size 120 MB, throughput 0.793549
Reading from 28242: heap size 124 MB, throughput 0.743592
Reading from 28243: heap size 103 MB, throughput 0.174816
Reading from 28243: heap size 134 MB, throughput 0.737158
Reading from 28243: heap size 136 MB, throughput 0.587858
Reading from 28243: heap size 139 MB, throughput 0.669443
Reading from 28243: heap size 144 MB, throughput 0.574009
Reading from 28242: heap size 128 MB, throughput 0.117653
Reading from 28243: heap size 148 MB, throughput 0.678282
Reading from 28242: heap size 170 MB, throughput 0.653964
Reading from 28243: heap size 155 MB, throughput 0.553157
Reading from 28242: heap size 172 MB, throughput 0.55146
Reading from 28242: heap size 174 MB, throughput 0.583095
Reading from 28242: heap size 181 MB, throughput 0.684661
Reading from 28243: heap size 161 MB, throughput 0.0976812
Reading from 28242: heap size 187 MB, throughput 0.670678
Reading from 28243: heap size 194 MB, throughput 0.586368
Reading from 28243: heap size 200 MB, throughput 0.590732
Reading from 28243: heap size 201 MB, throughput 0.563972
Reading from 28242: heap size 194 MB, throughput 0.136205
Reading from 28242: heap size 233 MB, throughput 0.567807
Reading from 28242: heap size 237 MB, throughput 0.612799
Reading from 28242: heap size 240 MB, throughput 0.64123
Reading from 28242: heap size 242 MB, throughput 0.502678
Reading from 28243: heap size 206 MB, throughput 0.453702
Reading from 28242: heap size 248 MB, throughput 0.721583
Reading from 28243: heap size 241 MB, throughput 0.835922
Reading from 28242: heap size 251 MB, throughput 0.117366
Reading from 28243: heap size 247 MB, throughput 0.784675
Reading from 28243: heap size 249 MB, throughput 0.413603
Reading from 28242: heap size 293 MB, throughput 0.589476
Reading from 28242: heap size 296 MB, throughput 0.61673
Reading from 28243: heap size 254 MB, throughput 0.853662
Reading from 28243: heap size 255 MB, throughput 0.447872
Reading from 28242: heap size 298 MB, throughput 0.596443
Reading from 28242: heap size 300 MB, throughput 0.529571
Reading from 28243: heap size 260 MB, throughput 0.731683
Reading from 28242: heap size 305 MB, throughput 0.602106
Reading from 28243: heap size 260 MB, throughput 0.843181
Reading from 28243: heap size 255 MB, throughput 0.781771
Reading from 28243: heap size 259 MB, throughput 0.546407
Reading from 28243: heap size 265 MB, throughput 0.608937
Reading from 28243: heap size 265 MB, throughput 0.758954
Reading from 28242: heap size 309 MB, throughput 0.138564
Reading from 28243: heap size 269 MB, throughput 0.584998
Reading from 28242: heap size 354 MB, throughput 0.487663
Reading from 28242: heap size 359 MB, throughput 0.561225
Reading from 28242: heap size 361 MB, throughput 0.570091
Reading from 28242: heap size 365 MB, throughput 0.468371
Reading from 28242: heap size 369 MB, throughput 0.101167
Reading from 28243: heap size 270 MB, throughput 0.327624
Reading from 28243: heap size 304 MB, throughput 0.539241
Reading from 28242: heap size 420 MB, throughput 0.416566
Reading from 28243: heap size 308 MB, throughput 0.655443
Reading from 28242: heap size 421 MB, throughput 0.623659
Reading from 28243: heap size 313 MB, throughput 0.86957
Reading from 28242: heap size 425 MB, throughput 0.615785
Reading from 28243: heap size 313 MB, throughput 0.922542
Reading from 28243: heap size 320 MB, throughput 0.87668
Equal recommendation: 1487 MB each
Reading from 28243: heap size 321 MB, throughput 0.803993
Reading from 28243: heap size 322 MB, throughput 0.694665
Reading from 28243: heap size 325 MB, throughput 0.548591
Reading from 28242: heap size 427 MB, throughput 0.121101
Reading from 28243: heap size 327 MB, throughput 0.842878
Reading from 28243: heap size 328 MB, throughput 0.612416
Reading from 28242: heap size 479 MB, throughput 0.484506
Reading from 28242: heap size 481 MB, throughput 0.661967
Reading from 28242: heap size 484 MB, throughput 0.566
Reading from 28242: heap size 485 MB, throughput 0.547266
Reading from 28242: heap size 493 MB, throughput 0.497983
Reading from 28242: heap size 500 MB, throughput 0.534024
Reading from 28243: heap size 334 MB, throughput 0.975144
Reading from 28242: heap size 510 MB, throughput 0.570983
Reading from 28242: heap size 516 MB, throughput 0.0845878
Reading from 28243: heap size 334 MB, throughput 0.967979
Reading from 28242: heap size 579 MB, throughput 0.356559
Reading from 28242: heap size 591 MB, throughput 0.56573
Reading from 28242: heap size 593 MB, throughput 0.593755
Reading from 28242: heap size 599 MB, throughput 0.10496
Reading from 28243: heap size 341 MB, throughput 0.982417
Reading from 28242: heap size 665 MB, throughput 0.534888
Reading from 28242: heap size 667 MB, throughput 0.570028
Reading from 28242: heap size 670 MB, throughput 0.598909
Reading from 28242: heap size 672 MB, throughput 0.849593
Reading from 28243: heap size 343 MB, throughput 0.980579
Reading from 28242: heap size 679 MB, throughput 0.819164
Reading from 28242: heap size 678 MB, throughput 0.752473
Reading from 28242: heap size 700 MB, throughput 0.469515
Reading from 28243: heap size 343 MB, throughput 0.614904
Reading from 28242: heap size 713 MB, throughput 0.114926
Equal recommendation: 1487 MB each
Reading from 28242: heap size 793 MB, throughput 0.245734
Reading from 28242: heap size 786 MB, throughput 0.333534
Reading from 28242: heap size 693 MB, throughput 0.352552
Reading from 28242: heap size 772 MB, throughput 0.397371
Reading from 28243: heap size 388 MB, throughput 0.960513
Reading from 28243: heap size 392 MB, throughput 0.981383
Reading from 28242: heap size 780 MB, throughput 0.113961
Reading from 28242: heap size 857 MB, throughput 0.727456
Reading from 28242: heap size 859 MB, throughput 0.5369
Reading from 28242: heap size 863 MB, throughput 0.610299
Reading from 28242: heap size 864 MB, throughput 0.641875
Reading from 28242: heap size 869 MB, throughput 0.868571
Reading from 28243: heap size 392 MB, throughput 0.969094
Reading from 28242: heap size 869 MB, throughput 0.15225
Reading from 28242: heap size 963 MB, throughput 0.574726
Reading from 28242: heap size 965 MB, throughput 0.951313
Reading from 28242: heap size 976 MB, throughput 0.940393
Reading from 28242: heap size 978 MB, throughput 0.93168
Reading from 28243: heap size 392 MB, throughput 0.982878
Reading from 28242: heap size 966 MB, throughput 0.872615
Reading from 28242: heap size 800 MB, throughput 0.80544
Reading from 28242: heap size 950 MB, throughput 0.798626
Reading from 28242: heap size 804 MB, throughput 0.774939
Reading from 28242: heap size 938 MB, throughput 0.792398
Reading from 28242: heap size 810 MB, throughput 0.814782
Reading from 28242: heap size 927 MB, throughput 0.827043
Reading from 28242: heap size 816 MB, throughput 0.800097
Reading from 28242: heap size 919 MB, throughput 0.82816
Reading from 28243: heap size 394 MB, throughput 0.976857
Reading from 28242: heap size 822 MB, throughput 0.839321
Reading from 28242: heap size 913 MB, throughput 0.971863
Reading from 28243: heap size 393 MB, throughput 0.975737
Reading from 28242: heap size 920 MB, throughput 0.972473
Reading from 28242: heap size 917 MB, throughput 0.825667
Reading from 28242: heap size 921 MB, throughput 0.76072
Reading from 28242: heap size 924 MB, throughput 0.777931
Reading from 28242: heap size 925 MB, throughput 0.804342
Reading from 28243: heap size 394 MB, throughput 0.9668
Reading from 28242: heap size 925 MB, throughput 0.729574
Equal recommendation: 1487 MB each
Reading from 28242: heap size 928 MB, throughput 0.834599
Reading from 28242: heap size 929 MB, throughput 0.808865
Reading from 28242: heap size 931 MB, throughput 0.811023
Reading from 28242: heap size 933 MB, throughput 0.893778
Reading from 28242: heap size 935 MB, throughput 0.846501
Reading from 28242: heap size 936 MB, throughput 0.848805
Reading from 28242: heap size 939 MB, throughput 0.732649
Reading from 28242: heap size 926 MB, throughput 0.662081
Reading from 28243: heap size 397 MB, throughput 0.979341
Reading from 28242: heap size 945 MB, throughput 0.588162
Reading from 28243: heap size 398 MB, throughput 0.965145
Reading from 28243: heap size 398 MB, throughput 0.780717
Reading from 28243: heap size 399 MB, throughput 0.747455
Reading from 28243: heap size 409 MB, throughput 0.782756
Reading from 28243: heap size 412 MB, throughput 0.971703
Reading from 28242: heap size 957 MB, throughput 0.0494163
Reading from 28242: heap size 1064 MB, throughput 0.484148
Reading from 28242: heap size 1061 MB, throughput 0.695806
Reading from 28242: heap size 1066 MB, throughput 0.654588
Reading from 28242: heap size 1073 MB, throughput 0.658464
Reading from 28242: heap size 1075 MB, throughput 0.664242
Reading from 28242: heap size 1089 MB, throughput 0.678953
Reading from 28242: heap size 1089 MB, throughput 0.666192
Reading from 28243: heap size 419 MB, throughput 0.978644
Reading from 28242: heap size 1104 MB, throughput 0.94028
Reading from 28243: heap size 422 MB, throughput 0.977594
Reading from 28242: heap size 1107 MB, throughput 0.957605
Equal recommendation: 1487 MB each
Reading from 28243: heap size 423 MB, throughput 0.984957
Reading from 28242: heap size 1121 MB, throughput 0.948046
Reading from 28243: heap size 426 MB, throughput 0.984955
Reading from 28242: heap size 1124 MB, throughput 0.950022
Reading from 28243: heap size 425 MB, throughput 0.983329
Reading from 28242: heap size 1126 MB, throughput 0.953271
Reading from 28243: heap size 427 MB, throughput 0.983209
Reading from 28242: heap size 1131 MB, throughput 0.949662
Reading from 28243: heap size 428 MB, throughput 0.981923
Reading from 28242: heap size 1124 MB, throughput 0.941083
Equal recommendation: 1487 MB each
Reading from 28242: heap size 1131 MB, throughput 0.947674
Reading from 28243: heap size 429 MB, throughput 0.955746
Reading from 28242: heap size 1125 MB, throughput 0.949474
Reading from 28243: heap size 432 MB, throughput 0.980469
Reading from 28243: heap size 433 MB, throughput 0.977825
Reading from 28243: heap size 438 MB, throughput 0.87444
Reading from 28242: heap size 1130 MB, throughput 0.940359
Reading from 28243: heap size 440 MB, throughput 0.825871
Reading from 28243: heap size 447 MB, throughput 0.955834
Reading from 28242: heap size 1135 MB, throughput 0.962546
Reading from 28243: heap size 450 MB, throughput 0.988439
Reading from 28242: heap size 1135 MB, throughput 0.959152
Reading from 28243: heap size 452 MB, throughput 0.986934
Reading from 28242: heap size 1141 MB, throughput 0.938716
Equal recommendation: 1487 MB each
Reading from 28243: heap size 455 MB, throughput 0.986902
Reading from 28242: heap size 1143 MB, throughput 0.946663
Reading from 28243: heap size 454 MB, throughput 0.98628
Reading from 28242: heap size 1149 MB, throughput 0.959499
Reading from 28243: heap size 457 MB, throughput 0.982774
Reading from 28242: heap size 1153 MB, throughput 0.956782
Reading from 28242: heap size 1159 MB, throughput 0.956003
Reading from 28243: heap size 456 MB, throughput 0.984128
Reading from 28242: heap size 1164 MB, throughput 0.953254
Equal recommendation: 1487 MB each
Reading from 28243: heap size 458 MB, throughput 0.982252
Reading from 28242: heap size 1171 MB, throughput 0.948159
Reading from 28243: heap size 460 MB, throughput 0.988812
Reading from 28243: heap size 460 MB, throughput 0.974548
Reading from 28243: heap size 463 MB, throughput 0.886156
Reading from 28242: heap size 1174 MB, throughput 0.940545
Reading from 28243: heap size 464 MB, throughput 0.886104
Reading from 28243: heap size 471 MB, throughput 0.988582
Reading from 28242: heap size 1181 MB, throughput 0.953601
Reading from 28243: heap size 470 MB, throughput 0.990458
Reading from 28242: heap size 1183 MB, throughput 0.956535
Equal recommendation: 1487 MB each
Reading from 28243: heap size 477 MB, throughput 0.977643
Reading from 28242: heap size 1190 MB, throughput 0.95076
Reading from 28242: heap size 1191 MB, throughput 0.959793
Reading from 28243: heap size 478 MB, throughput 0.988666
Reading from 28243: heap size 480 MB, throughput 0.981697
Reading from 28242: heap size 1198 MB, throughput 0.520993
Reading from 28243: heap size 481 MB, throughput 0.98876
Reading from 28242: heap size 1296 MB, throughput 0.951742
Equal recommendation: 1487 MB each
Reading from 28243: heap size 480 MB, throughput 0.986655
Reading from 28242: heap size 1297 MB, throughput 0.978043
Reading from 28243: heap size 482 MB, throughput 0.991162
Reading from 28242: heap size 1303 MB, throughput 0.975978
Reading from 28243: heap size 483 MB, throughput 0.92816
Reading from 28243: heap size 484 MB, throughput 0.868245
Reading from 28243: heap size 490 MB, throughput 0.987023
Reading from 28242: heap size 1312 MB, throughput 0.977019
Reading from 28243: heap size 490 MB, throughput 0.991132
Reading from 28242: heap size 1313 MB, throughput 0.970197
Equal recommendation: 1487 MB each
Reading from 28242: heap size 1309 MB, throughput 0.971568
Reading from 28243: heap size 495 MB, throughput 0.990184
Reading from 28242: heap size 1313 MB, throughput 0.967259
Reading from 28243: heap size 497 MB, throughput 0.988959
Reading from 28242: heap size 1302 MB, throughput 0.966068
Reading from 28243: heap size 496 MB, throughput 0.988249
Reading from 28242: heap size 1242 MB, throughput 0.965918
Reading from 28243: heap size 498 MB, throughput 0.98847
Equal recommendation: 1487 MB each
Reading from 28242: heap size 1301 MB, throughput 0.968089
Reading from 28243: heap size 500 MB, throughput 0.984214
Reading from 28243: heap size 500 MB, throughput 0.966546
Reading from 28242: heap size 1304 MB, throughput 0.958209
Reading from 28243: heap size 507 MB, throughput 0.912359
Reading from 28243: heap size 508 MB, throughput 0.983169
Reading from 28242: heap size 1309 MB, throughput 0.961393
Reading from 28243: heap size 514 MB, throughput 0.99207
Reading from 28242: heap size 1311 MB, throughput 0.963502
Equal recommendation: 1487 MB each
Reading from 28243: heap size 515 MB, throughput 0.991247
Reading from 28242: heap size 1316 MB, throughput 0.959954
Reading from 28243: heap size 515 MB, throughput 0.909335
Reading from 28242: heap size 1322 MB, throughput 0.958192
Reading from 28243: heap size 546 MB, throughput 0.995082
Reading from 28242: heap size 1328 MB, throughput 0.958645
Reading from 28243: heap size 546 MB, throughput 0.99333
Equal recommendation: 1487 MB each
Reading from 28242: heap size 1336 MB, throughput 0.954494
Reading from 28243: heap size 548 MB, throughput 0.992039
Reading from 28242: heap size 1344 MB, throughput 0.955475
Reading from 28243: heap size 550 MB, throughput 0.973481
Reading from 28243: heap size 551 MB, throughput 0.897793
Reading from 28243: heap size 556 MB, throughput 0.987579
Reading from 28243: heap size 558 MB, throughput 0.992871
Equal recommendation: 1487 MB each
Reading from 28243: heap size 556 MB, throughput 0.991309
Reading from 28243: heap size 560 MB, throughput 0.989966
Reading from 28243: heap size 561 MB, throughput 0.98846
Equal recommendation: 1487 MB each
Reading from 28243: heap size 562 MB, throughput 0.987104
Reading from 28243: heap size 566 MB, throughput 0.988743
Reading from 28243: heap size 568 MB, throughput 0.906489
Reading from 28243: heap size 571 MB, throughput 0.978144
Reading from 28243: heap size 574 MB, throughput 0.991129
Reading from 28243: heap size 574 MB, throughput 0.989589
Equal recommendation: 1487 MB each
Reading from 28242: heap size 1349 MB, throughput 0.995227
Reading from 28243: heap size 577 MB, throughput 0.98957
Reading from 28243: heap size 575 MB, throughput 0.989688
Reading from 28242: heap size 1343 MB, throughput 0.880729
Reading from 28242: heap size 1402 MB, throughput 0.992982
Reading from 28243: heap size 578 MB, throughput 0.988505
Reading from 28242: heap size 1435 MB, throughput 0.922284
Equal recommendation: 1487 MB each
Reading from 28242: heap size 1444 MB, throughput 0.887917
Reading from 28242: heap size 1434 MB, throughput 0.893739
Reading from 28242: heap size 1439 MB, throughput 0.893783
Reading from 28242: heap size 1433 MB, throughput 0.900514
Reading from 28242: heap size 1439 MB, throughput 0.986571
Reading from 28243: heap size 581 MB, throughput 0.989146
Reading from 28243: heap size 581 MB, throughput 0.914687
Reading from 28243: heap size 585 MB, throughput 0.985482
Reading from 28242: heap size 1443 MB, throughput 0.982084
Reading from 28242: heap size 1450 MB, throughput 0.98768
Reading from 28243: heap size 587 MB, throughput 0.992687
Reading from 28242: heap size 1459 MB, throughput 0.983837
Equal recommendation: 1487 MB each
Reading from 28243: heap size 590 MB, throughput 0.991874
Reading from 28242: heap size 1462 MB, throughput 0.986197
Reading from 28243: heap size 592 MB, throughput 0.990745
Reading from 28242: heap size 1451 MB, throughput 0.98509
Reading from 28243: heap size 591 MB, throughput 0.98889
Reading from 28242: heap size 1319 MB, throughput 0.975999
Equal recommendation: 1487 MB each
Reading from 28242: heap size 1437 MB, throughput 0.975814
Reading from 28243: heap size 594 MB, throughput 0.994977
Reading from 28243: heap size 597 MB, throughput 0.952591
Reading from 28243: heap size 598 MB, throughput 0.939729
Reading from 28242: heap size 1347 MB, throughput 0.970237
Reading from 28243: heap size 606 MB, throughput 0.993969
Reading from 28242: heap size 1438 MB, throughput 0.971002
Reading from 28243: heap size 606 MB, throughput 0.991976
Equal recommendation: 1487 MB each
Reading from 28242: heap size 1442 MB, throughput 0.976342
Reading from 28243: heap size 607 MB, throughput 0.991644
Reading from 28242: heap size 1447 MB, throughput 0.967185
Reading from 28242: heap size 1450 MB, throughput 0.964563
Reading from 28243: heap size 609 MB, throughput 0.990447
Reading from 28242: heap size 1457 MB, throughput 0.963436
Equal recommendation: 1487 MB each
Reading from 28243: heap size 612 MB, throughput 0.994423
Reading from 28243: heap size 613 MB, throughput 0.971832
Reading from 28243: heap size 613 MB, throughput 0.9658
Reading from 28242: heap size 1464 MB, throughput 0.961552
Reading from 28242: heap size 1472 MB, throughput 0.956021
Reading from 28243: heap size 615 MB, throughput 0.995725
Reading from 28242: heap size 1483 MB, throughput 0.955039
Equal recommendation: 1487 MB each
Reading from 28243: heap size 618 MB, throughput 0.992152
Reading from 28242: heap size 1497 MB, throughput 0.955131
Reading from 28243: heap size 620 MB, throughput 0.991316
Reading from 28242: heap size 1501 MB, throughput 0.956278
Reading from 28243: heap size 621 MB, throughput 0.989547
Reading from 28242: heap size 1504 MB, throughput 0.955761
Equal recommendation: 1487 MB each
Reading from 28243: heap size 622 MB, throughput 0.993962
Reading from 28243: heap size 623 MB, throughput 0.939778
Reading from 28242: heap size 1433 MB, throughput 0.958412
Reading from 28243: heap size 625 MB, throughput 0.988725
Reading from 28242: heap size 1506 MB, throughput 0.962012
Reading from 28243: heap size 634 MB, throughput 0.993944
Reading from 28242: heap size 1426 MB, throughput 0.961333
Equal recommendation: 1487 MB each
Reading from 28242: heap size 1505 MB, throughput 0.956049
Reading from 28243: heap size 634 MB, throughput 0.992723
Reading from 28242: heap size 1421 MB, throughput 0.950508
Reading from 28243: heap size 635 MB, throughput 0.990452
Reading from 28242: heap size 1502 MB, throughput 0.955419
Equal recommendation: 1487 MB each
Reading from 28243: heap size 637 MB, throughput 0.989224
Reading from 28242: heap size 1416 MB, throughput 0.952409
Reading from 28243: heap size 638 MB, throughput 0.984902
Reading from 28243: heap size 641 MB, throughput 0.960917
Reading from 28242: heap size 1497 MB, throughput 0.693263
Reading from 28243: heap size 647 MB, throughput 0.994433
Reading from 28242: heap size 1530 MB, throughput 0.991346
Equal recommendation: 1487 MB each
Reading from 28243: heap size 648 MB, throughput 0.993182
Reading from 28242: heap size 1591 MB, throughput 0.989052
Reading from 28242: heap size 1599 MB, throughput 0.984831
Reading from 28243: heap size 648 MB, throughput 0.992447
Reading from 28242: heap size 1601 MB, throughput 0.982029
Reading from 28243: heap size 650 MB, throughput 0.991316
Equal recommendation: 1487 MB each
Reading from 28242: heap size 1394 MB, throughput 0.979387
Reading from 28243: heap size 654 MB, throughput 0.990722
Reading from 28243: heap size 654 MB, throughput 0.930704
Client 28243 died
Clients: 1
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 28242: heap size 1599 MB, throughput 0.996594
Recommendation: one client; give it all the memory
Reading from 28242: heap size 1438 MB, throughput 0.988208
Reading from 28242: heap size 1584 MB, throughput 0.888809
Reading from 28242: heap size 1592 MB, throughput 0.807924
Reading from 28242: heap size 1577 MB, throughput 0.764162
Reading from 28242: heap size 1608 MB, throughput 0.744759
Reading from 28242: heap size 1640 MB, throughput 0.771609
Reading from 28242: heap size 1659 MB, throughput 0.764178
Client 28242 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
