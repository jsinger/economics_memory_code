#include "MajorOnlyThroughput.hpp"

economem::MajorOnlyThroughput::MajorOnlyThroughput()
    : ThroughputCalculator(),
      m_HeapSizeAtBeginCollectionB(0),
      m_GCTime(0)
{
    m_TotalTimer.start();
}

void economem::MajorOnlyThroughput::gcBegin()
{
    m_HeapSizeAtBeginCollectionB = getHeapSizeB();
    m_GCTimer.start();
}

economem::ThroughputCalculator::Result economem::MajorOnlyThroughput::minorGCEnd()
{
    m_GCTimer.stop();
    m_GCTime += m_GCTimer.microseconds();
    return ThroughputCalculator::Result();
}

economem::ThroughputCalculator::Result economem::MajorOnlyThroughput::majorGCEnd()
{
    m_GCTimer.stop();
    m_TotalTimer.stop();

    m_GCTime += m_GCTimer.microseconds();
    unsigned long long totalTime = m_TotalTimer.microseconds();
    double throughput = 1.0 - ((double)m_GCTime/(double)totalTime);

    logReading(m_HeapSizeAtBeginCollectionB, throughput, m_GCTime, totalTime);
    m_TotalTimer.start();

    return ThroughputCalculator::Result(m_HeapSizeAtBeginCollectionB / (1024 * 1024), throughput);
}
