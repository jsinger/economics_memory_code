economemd
    total memory: 5012 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub2_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run04_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
TimerThread: starting
ReadingThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 7339: heap size 9 MB, throughput 0.99163
Clients: 1
Client 7339 has a minimum heap size of 30 MB
Reading from 7338: heap size 9 MB, throughput 0.958481
Clients: 2
Client 7338 has a minimum heap size of 1223 MB
Reading from 7339: heap size 9 MB, throughput 0.95814
Reading from 7339: heap size 9 MB, throughput 0.966466
Reading from 7338: heap size 9 MB, throughput 0.980423
Reading from 7339: heap size 9 MB, throughput 0.967136
Reading from 7339: heap size 11 MB, throughput 0.520985
Reading from 7339: heap size 11 MB, throughput 0.478799
Reading from 7339: heap size 16 MB, throughput 0.625975
Reading from 7339: heap size 16 MB, throughput 0.874559
Reading from 7339: heap size 24 MB, throughput 0.885025
Reading from 7339: heap size 24 MB, throughput 0.830189
Reading from 7339: heap size 34 MB, throughput 0.918367
Reading from 7339: heap size 34 MB, throughput 0.900958
Reading from 7339: heap size 50 MB, throughput 0.937885
Reading from 7339: heap size 50 MB, throughput 0.934042
Reading from 7339: heap size 76 MB, throughput 0.963927
Reading from 7338: heap size 11 MB, throughput 0.970028
Reading from 7339: heap size 76 MB, throughput 0.930377
Reading from 7339: heap size 115 MB, throughput 0.964699
Reading from 7339: heap size 116 MB, throughput 0.960044
Reading from 7338: heap size 11 MB, throughput 0.989769
Reading from 7338: heap size 15 MB, throughput 0.802151
Reading from 7338: heap size 18 MB, throughput 0.963203
Reading from 7338: heap size 23 MB, throughput 0.952499
Reading from 7338: heap size 27 MB, throughput 0.932928
Reading from 7338: heap size 27 MB, throughput 0.372649
Reading from 7338: heap size 37 MB, throughput 0.860324
Reading from 7338: heap size 41 MB, throughput 0.861308
Reading from 7338: heap size 42 MB, throughput 0.858277
Reading from 7339: heap size 160 MB, throughput 0.965388
Reading from 7338: heap size 47 MB, throughput 0.222695
Reading from 7338: heap size 59 MB, throughput 0.806741
Reading from 7338: heap size 66 MB, throughput 0.823977
Reading from 7338: heap size 68 MB, throughput 0.815057
Reading from 7339: heap size 169 MB, throughput 0.881803
Reading from 7338: heap size 74 MB, throughput 0.614128
Reading from 7338: heap size 77 MB, throughput 0.0931654
Reading from 7339: heap size 226 MB, throughput 0.975233
Reading from 7338: heap size 101 MB, throughput 0.181943
Reading from 7338: heap size 126 MB, throughput 0.799123
Reading from 7338: heap size 130 MB, throughput 0.823684
Reading from 7338: heap size 131 MB, throughput 0.763923
Reading from 7338: heap size 133 MB, throughput 0.6982
Reading from 7338: heap size 137 MB, throughput 0.687036
Reading from 7339: heap size 238 MB, throughput 0.9889
Reading from 7338: heap size 143 MB, throughput 0.679509
Reading from 7338: heap size 148 MB, throughput 0.587406
Reading from 7338: heap size 150 MB, throughput 0.545757
Reading from 7338: heap size 157 MB, throughput 0.526705
Reading from 7338: heap size 160 MB, throughput 0.503674
Reading from 7338: heap size 169 MB, throughput 0.109653
Reading from 7338: heap size 200 MB, throughput 0.502811
Reading from 7338: heap size 206 MB, throughput 0.534902
Reading from 7338: heap size 211 MB, throughput 0.649525
Reading from 7339: heap size 267 MB, throughput 0.989669
Reading from 7338: heap size 211 MB, throughput 0.0747902
Reading from 7338: heap size 243 MB, throughput 0.425902
Reading from 7338: heap size 199 MB, throughput 0.557617
Reading from 7338: heap size 241 MB, throughput 0.52178
Reading from 7339: heap size 268 MB, throughput 0.857821
Reading from 7338: heap size 243 MB, throughput 0.574066
Reading from 7338: heap size 247 MB, throughput 0.152314
Reading from 7338: heap size 283 MB, throughput 0.553649
Reading from 7338: heap size 285 MB, throughput 0.517267
Reading from 7339: heap size 311 MB, throughput 0.995388
Reading from 7338: heap size 287 MB, throughput 0.618724
Reading from 7338: heap size 288 MB, throughput 0.421956
Reading from 7338: heap size 292 MB, throughput 0.608473
Reading from 7338: heap size 296 MB, throughput 0.613571
Reading from 7338: heap size 302 MB, throughput 0.523536
Reading from 7338: heap size 307 MB, throughput 0.460969
Reading from 7339: heap size 316 MB, throughput 0.990173
Reading from 7338: heap size 314 MB, throughput 0.0979776
Reading from 7338: heap size 355 MB, throughput 0.300851
Reading from 7338: heap size 361 MB, throughput 0.472407
Reading from 7338: heap size 360 MB, throughput 0.404138
Reading from 7339: heap size 333 MB, throughput 0.983124
Equal recommendation: 2506 MB each
Reading from 7338: heap size 362 MB, throughput 0.0629459
Reading from 7338: heap size 405 MB, throughput 0.428788
Reading from 7339: heap size 336 MB, throughput 0.987706
Reading from 7338: heap size 409 MB, throughput 0.457517
Reading from 7338: heap size 402 MB, throughput 0.572103
Reading from 7338: heap size 406 MB, throughput 0.0995987
Reading from 7338: heap size 454 MB, throughput 0.453745
Reading from 7339: heap size 360 MB, throughput 0.986988
Reading from 7338: heap size 456 MB, throughput 0.579922
Reading from 7338: heap size 458 MB, throughput 0.673544
Reading from 7338: heap size 458 MB, throughput 0.509515
Reading from 7338: heap size 460 MB, throughput 0.579601
Reading from 7338: heap size 468 MB, throughput 0.50961
Reading from 7339: heap size 363 MB, throughput 0.986375
Reading from 7338: heap size 471 MB, throughput 0.507634
Reading from 7338: heap size 483 MB, throughput 0.463775
Reading from 7338: heap size 489 MB, throughput 0.433843
Reading from 7339: heap size 386 MB, throughput 0.980587
Reading from 7338: heap size 498 MB, throughput 0.0891101
Reading from 7338: heap size 555 MB, throughput 0.339569
Reading from 7338: heap size 561 MB, throughput 0.492714
Reading from 7338: heap size 567 MB, throughput 0.533297
Reading from 7339: heap size 388 MB, throughput 0.972391
Reading from 7338: heap size 567 MB, throughput 0.0685053
Reading from 7338: heap size 632 MB, throughput 0.362767
Reading from 7338: heap size 633 MB, throughput 0.571791
Reading from 7338: heap size 622 MB, throughput 0.583465
Reading from 7339: heap size 414 MB, throughput 0.972175
Reading from 7338: heap size 627 MB, throughput 0.424801
Reading from 7338: heap size 628 MB, throughput 0.533291
Reading from 7339: heap size 416 MB, throughput 0.976599
Equal recommendation: 2506 MB each
Reading from 7338: heap size 631 MB, throughput 0.234315
Reading from 7339: heap size 444 MB, throughput 0.981616
Reading from 7338: heap size 703 MB, throughput 0.813334
Reading from 7339: heap size 445 MB, throughput 0.973134
Reading from 7338: heap size 707 MB, throughput 0.868388
Reading from 7338: heap size 707 MB, throughput 0.570443
Reading from 7338: heap size 716 MB, throughput 0.559825
Reading from 7338: heap size 724 MB, throughput 0.59729
Reading from 7338: heap size 724 MB, throughput 0.297886
Reading from 7338: heap size 728 MB, throughput 0.309178
Reading from 7339: heap size 475 MB, throughput 0.977189
Reading from 7338: heap size 729 MB, throughput 0.0305094
Reading from 7339: heap size 475 MB, throughput 0.988422
Reading from 7338: heap size 793 MB, throughput 0.778925
Reading from 7338: heap size 796 MB, throughput 0.562644
Reading from 7338: heap size 798 MB, throughput 0.592918
Reading from 7338: heap size 800 MB, throughput 0.554491
Reading from 7339: heap size 506 MB, throughput 0.982899
Reading from 7338: heap size 807 MB, throughput 0.148795
Reading from 7338: heap size 885 MB, throughput 0.688111
Reading from 7338: heap size 893 MB, throughput 0.734309
Reading from 7338: heap size 896 MB, throughput 0.872972
Reading from 7338: heap size 903 MB, throughput 0.884115
Reading from 7339: heap size 510 MB, throughput 0.992612
Reading from 7338: heap size 905 MB, throughput 0.858248
Reading from 7338: heap size 904 MB, throughput 0.806681
Reading from 7338: heap size 909 MB, throughput 0.755696
Reading from 7338: heap size 900 MB, throughput 0.791725
Reading from 7338: heap size 905 MB, throughput 0.822424
Reading from 7338: heap size 895 MB, throughput 0.805069
Reading from 7338: heap size 901 MB, throughput 0.797717
Reading from 7339: heap size 535 MB, throughput 0.982674
Reading from 7338: heap size 896 MB, throughput 0.851417
Reading from 7338: heap size 899 MB, throughput 0.842674
Equal recommendation: 2506 MB each
Reading from 7338: heap size 894 MB, throughput 0.981895
Reading from 7339: heap size 541 MB, throughput 0.988754
Reading from 7338: heap size 898 MB, throughput 0.962911
Reading from 7338: heap size 900 MB, throughput 0.763342
Reading from 7338: heap size 903 MB, throughput 0.76831
Reading from 7339: heap size 566 MB, throughput 0.99346
Reading from 7338: heap size 901 MB, throughput 0.15031
Reading from 7338: heap size 987 MB, throughput 0.504551
Reading from 7338: heap size 985 MB, throughput 0.756674
Reading from 7339: heap size 569 MB, throughput 0.986843
Reading from 7338: heap size 988 MB, throughput 0.799541
Reading from 7338: heap size 987 MB, throughput 0.831755
Reading from 7338: heap size 991 MB, throughput 0.912139
Reading from 7338: heap size 990 MB, throughput 0.888654
Reading from 7338: heap size 994 MB, throughput 0.790282
Reading from 7338: heap size 1007 MB, throughput 0.690114
Reading from 7339: heap size 592 MB, throughput 0.991151
Reading from 7338: heap size 1008 MB, throughput 0.63464
Reading from 7338: heap size 1037 MB, throughput 0.538017
Reading from 7338: heap size 1047 MB, throughput 0.577421
Reading from 7339: heap size 593 MB, throughput 0.994338
Reading from 7338: heap size 1067 MB, throughput 0.0892791
Reading from 7338: heap size 1188 MB, throughput 0.523734
Reading from 7338: heap size 1199 MB, throughput 0.773389
Reading from 7338: heap size 1201 MB, throughput 0.732713
Reading from 7339: heap size 610 MB, throughput 0.988907
Reading from 7338: heap size 1212 MB, throughput 0.714847
Equal recommendation: 2506 MB each
Reading from 7339: heap size 613 MB, throughput 0.994382
Reading from 7338: heap size 1215 MB, throughput 0.962993
Reading from 7339: heap size 632 MB, throughput 0.986029
Reading from 7338: heap size 1216 MB, throughput 0.977223
Reading from 7339: heap size 635 MB, throughput 0.986262
Reading from 7339: heap size 659 MB, throughput 0.990921
Reading from 7338: heap size 1223 MB, throughput 0.97603
Reading from 7339: heap size 659 MB, throughput 0.994678
Equal recommendation: 2506 MB each
Reading from 7338: heap size 1214 MB, throughput 0.976091
Reading from 7339: heap size 677 MB, throughput 0.986233
Reading from 7339: heap size 680 MB, throughput 0.985771
Reading from 7338: heap size 1224 MB, throughput 0.969754
Reading from 7339: heap size 708 MB, throughput 0.993324
Reading from 7338: heap size 1228 MB, throughput 0.967868
Reading from 7339: heap size 708 MB, throughput 0.994657
Equal recommendation: 2506 MB each
Reading from 7339: heap size 727 MB, throughput 0.986988
Reading from 7338: heap size 1230 MB, throughput 0.96872
Reading from 7339: heap size 730 MB, throughput 0.988348
Reading from 7339: heap size 755 MB, throughput 0.983058
Reading from 7338: heap size 1233 MB, throughput 0.975622
Reading from 7339: heap size 756 MB, throughput 0.990083
Reading from 7338: heap size 1237 MB, throughput 0.968752
Equal recommendation: 2506 MB each
Reading from 7339: heap size 784 MB, throughput 0.99203
Reading from 7338: heap size 1231 MB, throughput 0.974612
Reading from 7339: heap size 785 MB, throughput 0.994037
Reading from 7339: heap size 807 MB, throughput 0.990218
Reading from 7338: heap size 1236 MB, throughput 0.973962
Reading from 7339: heap size 810 MB, throughput 0.990032
Reading from 7338: heap size 1239 MB, throughput 0.973457
Equal recommendation: 2506 MB each
Reading from 7339: heap size 837 MB, throughput 0.990533
Reading from 7339: heap size 837 MB, throughput 0.988852
Reading from 7338: heap size 1241 MB, throughput 0.969371
Reading from 7339: heap size 865 MB, throughput 0.991395
Reading from 7338: heap size 1245 MB, throughput 0.970766
Reading from 7339: heap size 865 MB, throughput 0.990215
Equal recommendation: 2506 MB each
Reading from 7338: heap size 1249 MB, throughput 0.974744
Reading from 7339: heap size 895 MB, throughput 0.995064
Reading from 7339: heap size 895 MB, throughput 0.990948
Reading from 7338: heap size 1254 MB, throughput 0.973313
Reading from 7339: heap size 921 MB, throughput 0.992806
Reading from 7338: heap size 1259 MB, throughput 0.971835
Equal recommendation: 2506 MB each
Reading from 7339: heap size 922 MB, throughput 0.990468
Reading from 7338: heap size 1265 MB, throughput 0.966955
Reading from 7339: heap size 949 MB, throughput 0.992194
Reading from 7339: heap size 949 MB, throughput 0.990757
Reading from 7338: heap size 1269 MB, throughput 0.972217
Reading from 7339: heap size 977 MB, throughput 0.993078
Equal recommendation: 2506 MB each
Reading from 7338: heap size 1275 MB, throughput 0.97274
Reading from 7339: heap size 977 MB, throughput 0.989345
Reading from 7339: heap size 1004 MB, throughput 0.999999
Reading from 7339: heap size 1004 MB, throughput 0.941176
Reading from 7338: heap size 1277 MB, throughput 0.663626
Reading from 7339: heap size 1004 MB, throughput 0.00222152
Reading from 7339: heap size 1005 MB, throughput 0.994391
Equal recommendation: 2506 MB each
Reading from 7338: heap size 1350 MB, throughput 0.993709
Reading from 7339: heap size 1032 MB, throughput 0.992252
Reading from 7338: heap size 1352 MB, throughput 0.98917
Reading from 7339: heap size 1035 MB, throughput 0.992121
Reading from 7338: heap size 1362 MB, throughput 0.988855
Reading from 7339: heap size 1064 MB, throughput 0.992193
Equal recommendation: 2506 MB each
Reading from 7338: heap size 1367 MB, throughput 0.988145
Reading from 7339: heap size 1065 MB, throughput 0.988668
Reading from 7338: heap size 1367 MB, throughput 0.981305
Reading from 7339: heap size 1093 MB, throughput 0.991523
Reading from 7338: heap size 1242 MB, throughput 0.985514
Equal recommendation: 2506 MB each
Reading from 7339: heap size 1096 MB, throughput 0.990551
Reading from 7338: heap size 1355 MB, throughput 0.983476
Reading from 7339: heap size 1131 MB, throughput 0.99027
Reading from 7339: heap size 1133 MB, throughput 0.990284
Reading from 7338: heap size 1262 MB, throughput 0.979347
Equal recommendation: 2506 MB each
Reading from 7339: heap size 1171 MB, throughput 0.993426
Reading from 7338: heap size 1342 MB, throughput 0.976352
Reading from 7339: heap size 1172 MB, throughput 0.992213
Reading from 7338: heap size 1350 MB, throughput 0.979679
Reading from 7339: heap size 1206 MB, throughput 0.992038
Equal recommendation: 2506 MB each
Reading from 7338: heap size 1349 MB, throughput 0.976264
Reading from 7339: heap size 1208 MB, throughput 0.992149
Reading from 7338: heap size 1350 MB, throughput 0.975154
Reading from 7339: heap size 1245 MB, throughput 0.988877
Reading from 7338: heap size 1351 MB, throughput 0.975475
Equal recommendation: 2506 MB each
Reading from 7339: heap size 1246 MB, throughput 0.99132
Reading from 7338: heap size 1356 MB, throughput 0.970287
Reading from 7339: heap size 1289 MB, throughput 0.994164
Reading from 7338: heap size 1359 MB, throughput 0.972903
Reading from 7339: heap size 1289 MB, throughput 0.989147
Equal recommendation: 2506 MB each
Reading from 7338: heap size 1367 MB, throughput 0.964953
Reading from 7339: heap size 1326 MB, throughput 0.990905
Reading from 7338: heap size 1374 MB, throughput 0.969932
Reading from 7339: heap size 1326 MB, throughput 0.990192
Equal recommendation: 2506 MB each
Reading from 7338: heap size 1381 MB, throughput 0.972298
Reading from 7339: heap size 1370 MB, throughput 0.9689
Reading from 7338: heap size 1388 MB, throughput 0.969352
Reading from 7339: heap size 1384 MB, throughput 0.994635
Equal recommendation: 2506 MB each
Reading from 7339: heap size 1426 MB, throughput 0.989055
Reading from 7339: heap size 1426 MB, throughput 0.988477
Equal recommendation: 2506 MB each
Reading from 7339: heap size 1487 MB, throughput 0.983866
Reading from 7338: heap size 1392 MB, throughput 0.834561
Reading from 7339: heap size 1486 MB, throughput 0.988258
Reading from 7339: heap size 1552 MB, throughput 0.989444
Equal recommendation: 2506 MB each
Reading from 7338: heap size 1501 MB, throughput 0.984811
Reading from 7339: heap size 1556 MB, throughput 0.990892
Reading from 7339: heap size 1626 MB, throughput 0.99291
Reading from 7338: heap size 1421 MB, throughput 0.98713
Equal recommendation: 2506 MB each
Reading from 7338: heap size 1526 MB, throughput 0.883772
Reading from 7338: heap size 1534 MB, throughput 0.798921
Reading from 7338: heap size 1515 MB, throughput 0.700961
Reading from 7338: heap size 1534 MB, throughput 0.709987
Reading from 7338: heap size 1561 MB, throughput 0.729449
Reading from 7338: heap size 1571 MB, throughput 0.702695
Reading from 7338: heap size 1602 MB, throughput 0.777834
Reading from 7339: heap size 1630 MB, throughput 0.990199
Reading from 7338: heap size 1606 MB, throughput 0.96679
Reading from 7338: heap size 1628 MB, throughput 0.984909
Reading from 7339: heap size 1667 MB, throughput 0.990846
Equal recommendation: 2506 MB each
Reading from 7338: heap size 1634 MB, throughput 0.979198
Reading from 7339: heap size 1659 MB, throughput 0.989578
Reading from 7338: heap size 1638 MB, throughput 0.979536
Equal recommendation: 2506 MB each
Reading from 7339: heap size 1665 MB, throughput 0.990843
Reading from 7338: heap size 1648 MB, throughput 0.975055
Reading from 7339: heap size 1659 MB, throughput 0.991498
Client 7339 died
Clients: 1
Reading from 7338: heap size 1642 MB, throughput 0.98624
Recommendation: one client; give it all the memory
Reading from 7338: heap size 1652 MB, throughput 0.983754
Reading from 7338: heap size 1654 MB, throughput 0.988116
Recommendation: one client; give it all the memory
Reading from 7338: heap size 1657 MB, throughput 0.987575
Reading from 7338: heap size 1655 MB, throughput 0.987169
Recommendation: one client; give it all the memory
Reading from 7338: heap size 1661 MB, throughput 0.985841
Recommendation: one client; give it all the memory
Reading from 7338: heap size 1644 MB, throughput 0.984994
Reading from 7338: heap size 1654 MB, throughput 0.984353
Recommendation: one client; give it all the memory
Reading from 7338: heap size 1646 MB, throughput 0.984267
Reading from 7338: heap size 1650 MB, throughput 0.983197
Recommendation: one client; give it all the memory
Reading from 7338: heap size 1654 MB, throughput 0.981735
Reading from 7338: heap size 1657 MB, throughput 0.980051
Recommendation: one client; give it all the memory
Reading from 7338: heap size 1665 MB, throughput 0.980552
Recommendation: one client; give it all the memory
Reading from 7338: heap size 1670 MB, throughput 0.980323
Reading from 7338: heap size 1678 MB, throughput 0.846465
Recommendation: one client; give it all the memory
Reading from 7338: heap size 1637 MB, throughput 0.995922
Reading from 7338: heap size 1635 MB, throughput 0.994112
Recommendation: one client; give it all the memory
Reading from 7338: heap size 1648 MB, throughput 0.992787
Reading from 7338: heap size 1660 MB, throughput 0.991606
Recommendation: one client; give it all the memory
Reading from 7338: heap size 1660 MB, throughput 0.990517
Recommendation: one client; give it all the memory
Reading from 7338: heap size 1649 MB, throughput 0.989874
Reading from 7338: heap size 1501 MB, throughput 0.988487
Recommendation: one client; give it all the memory
Reading from 7338: heap size 1630 MB, throughput 0.987006
Reading from 7338: heap size 1531 MB, throughput 0.986226
Recommendation: one client; give it all the memory
Recommendation: one client; give it all the memory
Reading from 7338: heap size 1628 MB, throughput 0.986493
Recommendation: one client; give it all the memory
Reading from 7338: heap size 1633 MB, throughput 0.991182
Reading from 7338: heap size 1649 MB, throughput 0.875452
Reading from 7338: heap size 1678 MB, throughput 0.733973
Reading from 7338: heap size 1697 MB, throughput 0.748462
Recommendation: one client; give it all the memory
Reading from 7338: heap size 1736 MB, throughput 0.723267
Reading from 7338: heap size 1780 MB, throughput 0.757743
Client 7338 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
