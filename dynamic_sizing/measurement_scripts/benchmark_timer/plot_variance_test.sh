#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") folder outfile"
    exit 1
}

test -z "${1}" && usage
test ! -d "${1}" && usage
test -z "${1}" && usage

IN_DIR="${1}"
OUTFILE="${2}"
TMP_DIR="$(mktemp -d)"

function doOne()
{
    BENCHMARK="$(tail -n +2 "${1}/benchmarks.csv")"
    TMPOUT="$(mktemp --tmpdir="${TMP_DIR}" --suffix=.pdf)"
    METADATA="${1}/sub1_timeBenchmarkUnconstrained/metadata"
    VARIANCE="$(cat "${METADATA}/runtimes_main_variance.txt")"
    tail -n +2 "${METADATA}/runtimes_main.csv" | python "${DIR}/plot_variance_one.py" "${BENCHMARK}" "${TMPOUT}" "${VARIANCE}"
}

find "${IN_DIR}" -name 'sub*_timerComparisonSingle' | sort | while read line; do
    doOne "${line}"
done

pdfunite ${TMP_DIR}/*.pdf "${OUTFILE}"
rm -r "${TMP_DIR}"
