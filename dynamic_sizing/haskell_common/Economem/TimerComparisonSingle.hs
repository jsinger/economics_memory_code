module Economem.TimerComparisonSingle (ComparisonSingleDir(..),
                                       newTimerComparisonSingle,
                                       resumeTimerComparisonSingle) where

import Control.Applicative
import Control.Conditional (ifM)
import Control.Monad
import Data.Maybe
import qualified Economem.BenchmarkTimer as B
import qualified Economem.CgroupBenchmarkTimer as C
import Economem.Common
import Economem.Daemon
import qualified Economem.DaemonBenchmarkTimer as D
import Economem.Experiment
import Economem.Files
import Economem.Log
import Economem.Java
import qualified Economem.MetadataDir as M
import Economem.Records
import qualified Economem.StaticEqualBenchmarkTimer as S
import qualified Economem.UnconstrainedBenchmarkTimer as U


data ComparisonSingleDir =
  ComparisonSingleDir { dirPath :: FilePath,
                        benchmarksFile :: CsvFile BenchmarkThreadsIters,
                        timersFile :: LinesFile TimerType,
                        maxHeapFile :: IntFile,
                        runsFile :: IntFile,
                        javaNameFile :: StringFile,
                        minHeapsFile :: CsvFile BenchmarkThreadsMB,
                        forceGrowthFile :: WholeFile Bool,
                        alwaysNumericFile :: WholeFile Bool,
                        resultsFile :: CsvFile TimerRuntime,
                        fastestFile :: WholeFile TimerType,
                        daemonBehaviourPlotFile :: FilePath }

instance Path ComparisonSingleDir where
  getPath = dirPath

instance Dir ComparisonSingleDir where
  asDir path =
    ComparisonSingleDir { dirPath = getPath path,
                          benchmarksFile = CsvFile (path </> "benchmarks.csv"),
                          timersFile = LinesFile (path </> "timers.txt"),
                          maxHeapFile = WholeFile (path </> "max_heap_MB.txt"),
                          runsFile = WholeFile (path </> "runs.txt"),
                          javaNameFile = WholeFile (path </> "java_name.txt"),
                          minHeapsFile = CsvFile (path </> "min_heaps.csv"),
                          forceGrowthFile = WholeFile (path </> "force_growth.txt"),
                          alwaysNumericFile = WholeFile (path </> "always_numeric_optimisation.txt"),
                          resultsFile = CsvFile (path </> "results.csv"),
                          fastestFile = WholeFile (path </> "fastest.txt"),
                          daemonBehaviourPlotFile = path </> "daemon_behaviour_plot.pdf" }

  postprocessingActions = [postprocessSubdirs]
    where
      postprocessSubdirs :: ComparisonSingleDir -> IO ()
      postprocessSubdirs d = do
        subdirs <- getAllSubexpDirs (asDir d) Static
        plotFiles <- filterM exists =<< catMaybes <$> mapM doDir subdirs
        mergePdfs plotFiles (daemonBehaviourPlotFile d)

      doDir :: ExperimentDir -> IO (Maybe FilePath)
      doDir d =
        ifM (D.isDaemonTimerDir d)
        (do postprocessDir (asDir d :: D.DaemonTimerDir)
            return (Just (D.behaviourFile (asDir d :: D.DaemonTimerDir))))
        (do postprocessDir (asDir d :: B.BenchmarkTimerDir)
            return Nothing)

newTimerComparisonSingle :: Input BenchmarkThreadsIters ->
                            [TimerType] ->
                            DataSize ->
                            Integer ->
                            Java ->
                            Maybe (Input BenchmarkThreadsMB) ->
                            Bool ->
                            Bool ->
                            Maybe SubexpInfo ->
                            [String] ->
                            IO Experiment
newTimerComparisonSingle input timers maxHeap runs j minHeaps forceGrowth alwaysNumeric info extraVMArgs = do
  (expDir, num, parentLog) <- newExperimentProperties "timerComparisonSingle" info
  makeTimerComparisonSingle expDir num input timers maxHeap runs j minHeaps forceGrowth alwaysNumeric parentLog extraVMArgs


resumeTimerComparisonSingle :: FilePath -> IO Experiment
resumeTimerComparisonSingle =
  resumeExperiment
  (\expDir -> do let d = asDir expDir
                 let input = FromFile (benchmarksFile d)
                 timers <- get (timersFile d)
                 maxHeap <- (`DataSize` MB) <$> get (maxHeapFile d)
                 runs <- get (runsFile d)
                 java <- javaFromName =<< get (javaNameFile d)
                 minHeaps <- maybeGet (minHeapsFile d)
                 forceGrowth <- get (forceGrowthFile d)
                 alwaysNumeric <- get (alwaysNumericFile d)
                 makeTimerComparisonSingle
                   expDir
                   topLevel
                   input
                   timers
                   maxHeap
                   runs
                   java
                   minHeaps
                   forceGrowth
                   alwaysNumeric
                   Nothing
                   [])


makeTimerComparisonSingle :: ExperimentDir ->
                             ExperimentNum ->
                             Input BenchmarkThreadsIters ->
                             [TimerType] ->
                             DataSize ->
                             Integer ->
                             Java ->
                             Maybe (Input BenchmarkThreadsMB) ->
                             Bool ->
                             Bool ->
                             Maybe Log ->
                             [String] ->
                             IO Experiment
makeTimerComparisonSingle expDir expNum inputLoader timers maxHeap runs j minHeaps forceGrowth alwaysNumeric parentLog extraVMArgs = do
  let ex = makeExperiment "timerComparisonSingle" expDir expNum 0 parentLog
  let timerDir = asDir expDir
  input <- loadInput inputLoader
  return (ex { saveAdditionalStaticMetadata = saveStatic timerDir input,
               canResume = canResumeComparison timerDir,
               createSubexperiments = createSubexps,
               subexperimentCompleted = subexpCompleted timerDir,
               cleanup = cleanupComparison timerDir })

  where
    saveStatic :: ComparisonSingleDir -> [BenchmarkThreadsIters] -> IO ()
    saveStatic d input = do
      input |> benchmarksFile d
      timers |> timersFile d
      sizeToInt (toMB maxHeap) |> maxHeapFile d
      runs |> runsFile d
      javaName j |> javaNameFile d
      maybePut (minHeapsFile d) minHeaps
      forceGrowth |> forceGrowthFile d
      alwaysNumeric |> alwaysNumericFile d

    canResumeComparison :: ComparisonSingleDir -> IO Bool
    canResumeComparison d =
      allExistInDir d [getPath . benchmarksFile,
                       getPath . timersFile,
                       getPath . maxHeapFile,
                       getPath . runsFile,
                       getPath . javaNameFile,
                       getPath . forceGrowthFile,
                       getPath . alwaysNumericFile]

    createSubexps :: Experiment -> IO [Experiment]
    createSubexps e = case timers of
      [] -> fail "No timers are defined"
      _ -> do
        let infos = seqSubexpInfos Static timers e
        mapM makeTimerSubexp (zip infos timers)

      where
        makeTimerSubexp :: (SubexpInfo, TimerType) -> IO Experiment
        makeTimerSubexp (info, timer) = case timer of
          Unconstrained -> U.newUnconstrainedBenchmarkTimer inputLoader runs j (Just maxHeap) forceGrowth (Just info) extraVMArgs
          StaticEqual -> S.newStaticEqualBenchmarkTimer inputLoader runs j maxHeap forceGrowth (Just info) extraVMArgs
          CgroupSimple -> C.newCgroupBenchmarkTimer C.simple inputLoader maxHeap runs j forceGrowth (Just info) extraVMArgs
          CgroupFair -> C.newCgroupBenchmarkTimer C.fair inputLoader maxHeap runs j forceGrowth (Just info) extraVMArgs
          DaemonEqual -> D.newDaemonBenchmarkTimer
                         inputLoader
                         maxHeap
                         simpleThroughput
                         runs
                         j
                         Equal
                         minHeaps
                         forceGrowth
                         alwaysNumeric
                         (Just info)
                         extraVMArgs
          DaemonSimple -> D.newDaemonBenchmarkTimer
                          inputLoader
                          maxHeap
                          simpleThroughput
                          runs
                          j
                          Normal
                          minHeaps
                          forceGrowth
                          alwaysNumeric
                          (Just info)
                          extraVMArgs
          DaemonVengerov -> D.newDaemonBenchmarkTimer
                            inputLoader
                            maxHeap
                            vengerov
                            runs
                            j
                            Normal
                            minHeaps
                            forceGrowth
                            alwaysNumeric
                            (Just info)
                            extraVMArgs
          DaemonAvgVengerov -> D.newDaemonBenchmarkTimer
                               inputLoader
                               maxHeap
                               avgVengerov
                               runs
                               j
                               Normal
                               minHeaps
                               forceGrowth
                               alwaysNumeric
                               (Just info)
                               extraVMArgs
          DaemonMajorOnly -> D.newDaemonBenchmarkTimer
                             inputLoader
                             maxHeap
                             majorOnly
                             runs
                             j
                             Normal
                             minHeaps
                             forceGrowth
                             alwaysNumeric
                             (Just info)
                             extraVMArgs

    subexpCompleted :: ComparisonSingleDir -> Experiment -> Experiment -> IO ()
    subexpCompleted d _ subExp = do
      avg <- get (M.avgTimeFile (M.mainRuntimes (metadataDir (experimentDir subExp))))
      numCompleted <- length <$> (getOrDefault (resultsFile d) [] :: IO [TimerRuntime])
      case drop numCompleted timers of
        (t:_) -> append (resultsFile d) (TimerRuntime t avg)
        [] -> fail "Length mismatch in the timers file"

    cleanupComparison :: ComparisonSingleDir -> IO ()
    cleanupComparison d = do
      results <- get (resultsFile d)
      let (_, fastest) = minimum (map (\(TimerRuntime t r) -> (r, t)) results)
      put (fastestFile d) fastest
