	Command being timed: "cgexec -g memory:economem /home/callum/economics_memory_code/dynamic_sizing/hotspot8/openjdk8/jdk8/build/linux-x86_64-normal-server-release/jdk/bin/java -XX:+EconomemSimpleThroughput -XX:-EconomemVengerovThroughput -XX:EconomemMinHeap=22 -Xms10m -Xmx176m -XX:+DisableExplicitGC -XX:+UseAdaptiveSizePolicyWithSystemGC -jar /home/callum/economics_memory_code/dynamic_sizing/haskell_common/dacapo-9.12-bach.jar --no-pre-iteration-gc --scratch-directory /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub47_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/scratch1 -t 1 -n 28 -s large jython"
	User time (seconds): 399.96
	System time (seconds): 6.77
	Percent of CPU this job got: 72%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 9:19.51
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 210572
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 38707
	Minor (reclaiming a frame) page faults: 429946
	Voluntary context switches: 170252
	Involuntary context switches: 76078
	Swaps: 0
	File system inputs: 1553584
	File system outputs: 90272
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
