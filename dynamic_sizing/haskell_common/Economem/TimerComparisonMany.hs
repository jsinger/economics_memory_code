module Economem.TimerComparisonMany (ComparisonManyDir(..),
                                     TimerComparisonResult(..),
                                     newTimerComparisonMany,
                                     resumeTimerComparisonMany) where

import Control.Applicative
import Control.Monad
import qualified Data.ByteString.Char8 as B
import Data.Char
import qualified Data.Csv as C
import Data.List
import Economem.Common
import Economem.Experiment
import Economem.Files
import Economem.Log
import Economem.Java
import Economem.Records
import qualified Economem.TimerComparisonSingle as S

name :: String
name = "timerComparisonMany"


data ComparisonManyDir =
  ComparisonManyDir { dirPath :: FilePath,
                      benchmarksFile :: CsvFile BenchmarkCombinationMB,
                      timersFile :: LinesFile TimerType,
                      runsFile :: IntFile,
                      javaNameFile :: StringFile,
                      minHeapsFile :: CsvFile BenchmarkThreadsMB,
                      forceGrowthFile :: WholeFile Bool,
                      alwaysNumericFile :: WholeFile Bool,
                      resultsFile :: CsvFile TimerComparisonResult,
                      prettyResultsFile :: LinesFile String,
                      daemonBehaviourPlotFile :: FilePath }

instance Path ComparisonManyDir where
  getPath = dirPath

instance Dir ComparisonManyDir where
  asDir path =
    ComparisonManyDir { dirPath = getPath path,
                        benchmarksFile = CsvFile (path </> "benchmarks.txt"),
                        timersFile = LinesFile (path </> "timers.txt"),
                        runsFile = WholeFile (path </> "runs.txt"),
                        javaNameFile = WholeFile (path </> "java_name.txt"),
                        minHeapsFile = CsvFile (path </> "min_heaps.csv"),
                        forceGrowthFile = WholeFile (path </> "force_growth.txt"),
                        alwaysNumericFile = WholeFile (path </> "always_numeric_optimisation.txt"),
                        resultsFile = CsvFile (path </> "results.csv"),
                        prettyResultsFile = LinesFile (path </> "results_pretty.txt"),
                        daemonBehaviourPlotFile = path </> "daemon_behaviour_plot_many.pdf" }

  postprocessingActions = [postprocessSubdirs]
    where
      postprocessSubdirs :: ComparisonManyDir -> IO ()
      postprocessSubdirs d = do
        subdirs <- getAllSubexpDirs (asDir d) Static
        mapM_ (\s -> postprocessDir (asDir s :: S.ComparisonSingleDir)) subdirs
        plotFiles <- filterM exists
                     (map (\s -> S.daemonBehaviourPlotFile (asDir s :: S.ComparisonSingleDir)) subdirs)
        mergePdfs plotFiles (daemonBehaviourPlotFile d)


newTimerComparisonMany :: Input BenchmarkCombinationMB ->
                          [TimerType] ->
                          Integer ->
                          Java ->
                          Maybe (Input BenchmarkThreadsMB) ->
                          Bool ->
                          Bool ->
                          Maybe SubexpInfo ->
                          [[String]] ->
                          IO Experiment
newTimerComparisonMany input timers runs j minHeaps forceGrowth alwaysNumeric info extraVMArgs = do
  (expDir, num, parentLog) <- newExperimentProperties name info
  makeTimerComparisonMany expDir num input timers runs j minHeaps forceGrowth alwaysNumeric parentLog extraVMArgs


resumeTimerComparisonMany :: FilePath -> IO Experiment
resumeTimerComparisonMany =
  resumeExperiment
  (\expDir -> do let d = asDir expDir
                 let input = FromFile (benchmarksFile d)
                 timers <- get (timersFile d)
                 runs <- get (runsFile d)
                 java <- javaFromName =<< get (javaNameFile d)
                 minHeaps <- maybeGet (minHeapsFile d)
                 forceGrowth <- get (forceGrowthFile d)
                 alwaysNumeric <- get (alwaysNumericFile d)
                 makeTimerComparisonMany
                   expDir
                   topLevel
                   input
                   timers
                   runs
                   java
                   minHeaps
                   forceGrowth
                   alwaysNumeric
                   Nothing
                   [])


makeTimerComparisonMany :: ExperimentDir ->
                           ExperimentNum ->
                           Input BenchmarkCombinationMB ->
                           [TimerType] ->
                           Integer ->
                           Java ->
                           Maybe (Input BenchmarkThreadsMB) ->
                           Bool ->
                           Bool ->
                           Maybe Log ->
                           [[String]] ->
                           IO Experiment
makeTimerComparisonMany expDir expNum inputLoader timers runs j minHeaps forceGrowth alwaysNumeric parentLog extraVMArgs = do
  let ex = makeExperiment name expDir expNum 0 parentLog
  input <- loadInput inputLoader
  let timerDir = asDir expDir
  return (ex { saveAdditionalStaticMetadata = saveStatic timerDir input,
               canResume = canResumeComparison timerDir,
               createSubexperiments = createSubexps input,
               subexperimentCompleted = subexpCompleted timerDir,
               cleanup = cleanupComparison timerDir })

  where
    saveStatic :: ComparisonManyDir -> [BenchmarkCombinationMB] -> IO ()
    saveStatic d input = do
      input |> benchmarksFile d
      timers |> timersFile d
      runs |> runsFile d
      javaName j |> javaNameFile d
      maybePut (minHeapsFile d) minHeaps
      forceGrowth |> forceGrowthFile d
      alwaysNumeric |> alwaysNumericFile d

    canResumeComparison :: ComparisonManyDir -> IO Bool
    canResumeComparison d =
      allExistInDir d [getPath . benchmarksFile,
                       getPath . timersFile,
                       getPath . runsFile,
                       getPath . javaNameFile,
                       getPath . forceGrowthFile,
                       getPath . alwaysNumericFile]

    createSubexps :: [BenchmarkCombinationMB] -> Experiment -> IO [Experiment]
    createSubexps input e = do
      let infos = seqSubexpInfos Static input e
      mapM makeSubexp (zip3 infos input (extend extraVMArgs (length infos)))

      where
        makeSubexp :: (SubexpInfo, BenchmarkCombinationMB, [String]) -> IO Experiment
        makeSubexp (info, inp, extra) =
          S.newTimerComparisonSingle
          (Raw (bcBenchmarks (bcmBenchmarks inp)))
          timers
          (DataSize (fromIntegral (bcmMB inp)) MB)
          runs
          j
          minHeaps
          forceGrowth
          alwaysNumeric
          (Just info)
          extra

        extend :: [[a]] -> Int -> [[a]]
        extend l i = l ++ (if i > length l then replicate (i - length l) [] else [])

    subexpCompleted :: ComparisonManyDir -> Experiment -> Experiment -> IO ()
    subexpCompleted d _ subExp = do
      let subDir = asDir (experimentDir subExp)
      benchmarks <- get (S.benchmarksFile subDir)
      maxHeap <- get (S.maxHeapFile subDir)
      results <- get (S.resultsFile subDir)
      append
        (resultsFile d)
        TimerComparisonResult { tcrBenchmarks = BenchmarkCombination benchmarks,
                                tcrMaxHeap = fromIntegral maxHeap,
                                tcrUnconstrained = getResult results Unconstrained,
                                tcrStaticEqual = getResult results StaticEqual,
                                tcrCgroupSimple = getResult results CgroupSimple,
                                tcrCgroupFair = getResult results CgroupFair,
                                tcrDaemonEqual = getResult results DaemonEqual,
                                tcrDaemonSimple = getResult results DaemonSimple,
                                tcrDaemonVengerov = getResult results DaemonVengerov,
                                tcrDaemonAvgVengerov = getResult results DaemonAvgVengerov,
                                tcrDaemonMajorOnly = getResult results DaemonMajorOnly,
                                tcrFastest = getFastest results }

      where
        getResult :: [TimerRuntime] -> TimerType -> Maybe UncertainRuntime
        getResult results t = case find (\tr -> trTimer tr == t) results of
          Just r -> Just (trRuntime r)
          Nothing -> Nothing

        getFastest :: [TimerRuntime] -> String
        getFastest results = timerToName (snd (minimum (map (\tr -> (trRuntime tr, trTimer tr)) results)))

    cleanupComparison :: ComparisonManyDir -> IO ()
    cleanupComparison d = do
      results <- get (resultsFile d)
      put (prettyResultsFile d) ["-*- truncate-lines:t -*-"]
      let rawColumns =
            ["BENCHMARKS":map (pretty . tcrBenchmarks) results,
             "MAX_HEAP":map (show . tcrMaxHeap) results,
             "UNCONSTRAINED":doTime results tcrUnconstrained,
             "STATIC_EQUAL":doTime results tcrStaticEqual,
             "CGROUP_SIMPLE":doTime results tcrCgroupSimple,
             "CGROUP_FAIR":doTime results tcrCgroupFair,
             "DAEMON_EQUAL":doTime results tcrDaemonEqual,
             "DAEMON_SIMPLE":doTime results tcrDaemonSimple,
             "DAEMON_VENGEROV":doTime results tcrDaemonVengerov,
             "DAEMON_AVG_VENGEROV":doTime results tcrDaemonAvgVengerov,
             "DAEMON_MAJOR_ONLY":doTime results tcrDaemonMajorOnly,
             "FASTEST":map tcrFastest results]
      let padded = map (trim . concat) (transpose (map pad rawColumns))
      mapM_ (append (prettyResultsFile d)) padded

      where
        doTime :: [TimerComparisonResult] -> (TimerComparisonResult -> Maybe UncertainRuntime) -> [String]
        doTime results f =
          map (\r -> case f r of
                  Just sec -> show sec
                  Nothing -> "-")
          results

        pad :: [String] -> [String]
        pad ss = let width = columnWidth ss in
          map (\s -> s ++ replicate (width - length s) ' ') ss

        columnWidth :: [String] -> Int
        columnWidth ss = maximum (map length ss) + 3

        trim :: String -> String
        trim s = reverse (dropWhile isSpace (reverse s))


data TimerComparisonResult =
  TimerComparisonResult { tcrBenchmarks :: BenchmarkCombination,
                          tcrMaxHeap :: Int,
                          tcrUnconstrained :: Maybe UncertainRuntime,
                          tcrStaticEqual :: Maybe UncertainRuntime,
                          tcrCgroupSimple :: Maybe UncertainRuntime,
                          tcrCgroupFair :: Maybe UncertainRuntime,
                          tcrDaemonEqual :: Maybe UncertainRuntime,
                          tcrDaemonSimple :: Maybe UncertainRuntime,
                          tcrDaemonVengerov :: Maybe UncertainRuntime,
                          tcrDaemonAvgVengerov :: Maybe UncertainRuntime,
                          tcrDaemonMajorOnly :: Maybe UncertainRuntime,
                          tcrFastest :: String }
  deriving (Read, Show)

instance C.FromNamedRecord TimerComparisonResult where
  parseNamedRecord r = TimerComparisonResult <$>
                       C.parseNamedRecord r <*>
                       r C..: B.pack "MAX_HEAP" <*>
                       r C..: B.pack "UNCONSTRAINED" <*>
                       r C..: B.pack "STATIC_EQUAL" <*>
                       r C..: B.pack "CGROUP_SIMPLE" <*>
                       r C..: B.pack "CGROUP_FAIR" <*>
                       r C..: B.pack "DAEMON_EQUAL" <*>
                       r C..: B.pack "DAEMON_SIMPLE" <*>
                       r C..: B.pack "DAEMON_VENGEROV" <*>
                       r C..: B.pack "DAEMON_AVG_VENGEROV" <*>
                       r C..: B.pack "DAEMON_MAJOR_ONLY" <*>
                       r C..: B.pack "FASTEST"
instance C.ToNamedRecord TimerComparisonResult where
  toNamedRecord tr = C.namedRecord (getNamedRecordInput tr)
instance CsvFields TimerComparisonResult where
  getCsvFieldNames _ = getCsvFieldNames (Nothing :: Maybe BenchmarkCombination) ++
                       [B.pack "MAX_HEAP",
                        B.pack "UNCONSTRAINED",
                        B.pack "STATIC_EQUAL",
                        B.pack "CGROUP_SIMPLE",
                        B.pack "CGROUP_FAIR",
                        B.pack "DAEMON_EQUAL",
                        B.pack "DAEMON_SIMPLE",
                        B.pack "DAEMON_VENGEROV",
                        B.pack "DAEMON_AVG_VENGEROV",
                        B.pack "DAEMON_MAJOR_ONLY",
                        B.pack "FASTEST"]
  getNamedRecordInput (TimerComparisonResult bms mb un se cs cf de ds dv da dm f) =
    getNamedRecordInput bms ++
    [B.pack "MAX_HEAP" C..= mb,
     B.pack "UNCONSTRAINED" C..= un,
     B.pack "STATIC_EQUAL" C..= se,
     B.pack "CGROUP_SIMPLE" C..= cs,
     B.pack "CGROUP_FAIR" C..= cf,
     B.pack "DAEMON_EQUAL" C..= de,
     B.pack "DAEMON_SIMPLE" C..= ds,
     B.pack "DAEMON_VENGEROV" C..= dv,
     B.pack "DAEMON_AVG_VENGEROV" C..= da,
     B.pack "DAEMON_MAJOR_ONLY" C..= dm,
     B.pack "FASTEST" C..= f]
