import sys
from math import sqrt

l = []

for line in sys.stdin:
    l.append(int(line)/1000.0) # Input is in milliseconds, but we want output in seconds

mean = sum(l)/len(l)

for i in range(len(l)):
    l[i] = (l[i] - mean) ** 2

stddev = sqrt(sum(l)/len(l))

print stddev
