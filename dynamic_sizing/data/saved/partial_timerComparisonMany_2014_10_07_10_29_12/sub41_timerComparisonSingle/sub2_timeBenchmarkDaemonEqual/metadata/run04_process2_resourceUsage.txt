	Command being timed: "cgexec -g memory:economem /home/callum/economics_memory_code/dynamic_sizing/hotspot8/openjdk8/jdk8/build/linux-x86_64-normal-server-release/jdk/bin/java -XX:+EconomemSimpleThroughput -XX:-EconomemVengerovThroughput -XX:EconomemMinHeap=3 -Xms10m -Xmx837m -XX:+DisableExplicitGC -XX:+UseAdaptiveSizePolicyWithSystemGC -jar /home/callum/economics_memory_code/dynamic_sizing/haskell_common/dacapo-9.12-bach.jar --no-pre-iteration-gc --scratch-directory /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_07_10_29_12/sub41_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/scratch2 -t 2 -n 27 -s large xalan"
	User time (seconds): 447.37
	System time (seconds): 160.91
	Percent of CPU this job got: 149%
	Elapsed (wall clock) time (h:mm:ss or m:ss): 6:47.30
	Average shared text size (kbytes): 0
	Average unshared data size (kbytes): 0
	Average stack size (kbytes): 0
	Average total size (kbytes): 0
	Maximum resident set size (kbytes): 361476
	Average resident set size (kbytes): 0
	Major (requiring I/O) page faults: 14622
	Minor (reclaiming a frame) page faults: 221894
	Voluntary context switches: 77339
	Involuntary context switches: 99130
	Swaps: 0
	File system inputs: 970424
	File system outputs: 12724200
	Socket messages sent: 0
	Socket messages received: 0
	Signals delivered: 0
	Page size (bytes): 4096
	Exit status: 0
