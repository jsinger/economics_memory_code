#include "SimpleThroughput.hpp"

economem::SimpleThroughput::SimpleThroughput() : ThroughputCalculator(), m_HeapSizeAtBeginCollectionB(0)
{
    m_InterGCTimer.start();
}

void economem::SimpleThroughput::gcBegin()
{
    m_HeapSizeAtBeginCollectionB = getHeapSizeB();
    m_GCTimer.start();
}

economem::ThroughputCalculator::Result economem::SimpleThroughput::minorGCEnd()
{
    return gcEnd();
}

economem::ThroughputCalculator::Result economem::SimpleThroughput::majorGCEnd()
{
    return gcEnd();
}

economem::ThroughputCalculator::Result economem::SimpleThroughput::gcEnd()
{
    m_GCTimer.stop();
    m_InterGCTimer.stop();

    unsigned long long gcTime = m_GCTimer.microseconds();
    unsigned long long interGCTime = m_InterGCTimer.microseconds();
    double throughput = 1.0 - ((double)gcTime/(double)interGCTime);

    logReading(m_HeapSizeAtBeginCollectionB, throughput, gcTime, interGCTime);
    m_InterGCTimer.start();

    return ThroughputCalculator::Result(m_HeapSizeAtBeginCollectionB / (1024 * 1024), throughput);
}
