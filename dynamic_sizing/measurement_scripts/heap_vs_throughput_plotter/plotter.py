"""Take readings from a file, and plot a graph"""

import economem.common
import economem.dacapo
import economem.reading_log
import economem.signals
import numpy as np
import matplotlib.pyplot as plt

class Plotter(object):
    def __init__(self, throughput_model, reading_stream, title, pid_names_file, interactive):
        self.colours = ["r", "g", "b", "c", "m", "y", "k"]
        self.current_colour = 0
        self.interactive = interactive
        self.pid_names = economem.dacapo.PidNames(pid_names_file, interactive)
        self.pid = 0
        self.throughput_model = throughput_model
        self.reading_stream = reading_stream
        self.title = title
        self.heaps = []
        self.throughputs = []
        self.scatter = None
        self.curves = {}
        self.num_readings = 0
        self.max_x = 10
        self.min_y = 1.1
        self.max_y = 1.1
        self.axes = plt.subplot(111)
        self.axes.set_xlabel("Heap size (MB)", fontsize="medium")
        self.axes.set_ylabel("Throughput", fontsize="medium")
        self.set_title()

        economem.signals.exit_on_signal()

    def next_colour(self):
        colour = self.colours[self.current_colour]
        self.current_colour = (self.current_colour + 1) % len(self.colours)
        return colour

    def begin(self):
        pass

    def after_new_reading(self):
        pass

    def after_func_update(self, func):
        pass

    def end_iteration(self):
        pass

    def end(self):
        pass

    def set_title(self, dead=False):
        if self.title:
            self.axes.set_title(self.title, fontsize="medium")
        elif self.interactive:
            self.axes.set_title("Client %s: %s model (%d readings)%s" %
                                (self.pid_names.get_name(self.pid),
                                 self.throughput_model,
                                 self.num_readings,
                                 " (DEAD)" if dead else ""),
                                fontsize="medium")
        else:
            self.axes.set_title(self.pid_names.get_name(self.pid), fontsize="medium")

    def update_max_x(self, new_x):
        if new_x > self.max_x:
            self.max_x = new_x

    def update_max_y(self, new_y):
        if new_y > self.max_y:
            self.max_y = new_y
        if new_y < self.min_y:
            self.min_y = new_y

    def plot(self):
        """Plot readings"""

        dead = False
        self.begin()

        while True:
            # Have to do it this way because 'for line in self.reading_stream' is buffered
            # and unresponsive in the interactive case
            line = self.reading_stream.readline()
            if not line:
                break

            event = economem.reading_log.event_from_str(line)
            if self.pid == 0:
                self.pid = event.pid
                self.set_title()

            if isinstance(event, economem.reading_log.Reading):
                self.heaps.append(event.heap_size_MB)
                self.throughputs.append(event.throughput)
                self.update_max_x(event.heap_size_MB)
                self.update_max_y(event.throughput)
                self.num_readings += 1
                self.set_title()
                self.after_new_reading()

            elif isinstance(event, economem.reading_log.Function):
                if event.cache_name in self.curves:
                    self.curves[event.cache_name].set_func(event.func_str)
                else:
                    self.curves[event.cache_name] = Func(event.cache_name, event.func_str, self.next_colour())

                self.after_func_update(self.curves[event.cache_name])

            elif isinstance(event, economem.reading_log.Death):
                dead = True
                break

            self.end_iteration()

        self.set_title(dead)
        self.end()


class Func(object):
    def __init__(self, cache_name, func_str, colour):
        self.cache_name = cache_name
        self.func = eval(func_str)
        self.func_string = func_str
        self.colour = colour
        self.line = None

    def set_func(self, func_str):
        self.func = eval(func_str)
        self.func_string = func_str

    def draw(self, axes, heaps):
        """Returns the y value of the highest point drawn"""
        if self.line:
            self.line.remove()

        if len(heaps) > 0:
            fitx = np.linspace(min(heaps), max(heaps), 100)
        else:
            fitx = np.linspace(1, 2, 100)

        fity = map(self.func, fitx)
        self.line = axes.plot(fitx,
                              fity,
                              self.colour,
                              zorder=100,
                              label="%s: %s" % (prettify_cache_name(self.cache_name),
                                                prettify_function(self.func_string)))[0]
        return max(fity)

def prettify_cache_name(name):
    if name == "lru":
        return "Last 100 readings"
    elif name == "unbounded":
        return "All readings"
    else:
        return name

def prettify_function(func):
    func = func.split(" ")
    if (len(func) == 7 and
        func[0] == "lambda" and
        func[1] == "h:" and
        func[3] == "*" and
        func[4] == "(h" and
        func[5] == "**"):
        return "T = %.2fh^%.2f" % (float(func[2]), float(func[6].strip(")")))
    else:
        return func
