economemd
    total memory: 1440 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub59_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run03_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 15608: heap size 9 MB, throughput 0.979737
Clients: 1
Client 15608 has a minimum heap size of 12 MB
Reading from 15607: heap size 9 MB, throughput 0.992862
Clients: 2
Client 15607 has a minimum heap size of 276 MB
Reading from 15607: heap size 9 MB, throughput 0.98279
Reading from 15608: heap size 9 MB, throughput 0.985193
Reading from 15607: heap size 9 MB, throughput 0.972127
Reading from 15607: heap size 9 MB, throughput 0.961649
Reading from 15608: heap size 11 MB, throughput 0.979848
Reading from 15608: heap size 11 MB, throughput 0.956077
Reading from 15607: heap size 11 MB, throughput 0.87685
Reading from 15607: heap size 11 MB, throughput 0.976715
Reading from 15608: heap size 15 MB, throughput 0.987363
Reading from 15607: heap size 17 MB, throughput 0.95943
Reading from 15608: heap size 15 MB, throughput 0.988108
Reading from 15607: heap size 17 MB, throughput 0.470211
Reading from 15607: heap size 30 MB, throughput 0.945901
Reading from 15607: heap size 31 MB, throughput 0.810716
Reading from 15607: heap size 34 MB, throughput 0.309046
Reading from 15608: heap size 24 MB, throughput 0.930147
Reading from 15607: heap size 46 MB, throughput 0.939074
Reading from 15607: heap size 47 MB, throughput 0.889835
Reading from 15608: heap size 30 MB, throughput 0.988751
Reading from 15607: heap size 50 MB, throughput 0.325552
Reading from 15607: heap size 73 MB, throughput 0.800478
Reading from 15608: heap size 36 MB, throughput 0.962745
Reading from 15607: heap size 73 MB, throughput 0.288098
Reading from 15607: heap size 99 MB, throughput 0.802646
Reading from 15607: heap size 100 MB, throughput 0.679099
Reading from 15608: heap size 42 MB, throughput 0.990918
Reading from 15607: heap size 101 MB, throughput 0.702491
Reading from 15607: heap size 104 MB, throughput 0.714492
Reading from 15608: heap size 43 MB, throughput 0.982686
Reading from 15608: heap size 46 MB, throughput 0.965805
Reading from 15607: heap size 106 MB, throughput 0.195839
Reading from 15608: heap size 53 MB, throughput 0.973539
Reading from 15607: heap size 140 MB, throughput 0.685427
Reading from 15608: heap size 53 MB, throughput 0.986414
Reading from 15607: heap size 143 MB, throughput 0.795015
Reading from 15607: heap size 145 MB, throughput 0.61953
Reading from 15608: heap size 62 MB, throughput 0.952411
Reading from 15607: heap size 152 MB, throughput 0.759562
Reading from 15607: heap size 154 MB, throughput 0.69645
Reading from 15608: heap size 62 MB, throughput 0.926563
Reading from 15608: heap size 76 MB, throughput 0.390146
Reading from 15608: heap size 79 MB, throughput 0.988907
Reading from 15607: heap size 157 MB, throughput 0.107232
Reading from 15607: heap size 196 MB, throughput 0.587135
Reading from 15607: heap size 201 MB, throughput 0.607418
Reading from 15608: heap size 93 MB, throughput 0.997007
Reading from 15607: heap size 205 MB, throughput 0.903306
Reading from 15607: heap size 208 MB, throughput 0.909548
Reading from 15608: heap size 94 MB, throughput 0.995799
Reading from 15607: heap size 216 MB, throughput 0.713023
Reading from 15607: heap size 226 MB, throughput 0.571747
Reading from 15607: heap size 230 MB, throughput 0.741573
Reading from 15607: heap size 234 MB, throughput 0.512293
Reading from 15607: heap size 236 MB, throughput 0.658903
Reading from 15608: heap size 106 MB, throughput 0.996476
Reading from 15607: heap size 241 MB, throughput 0.126936
Reading from 15607: heap size 279 MB, throughput 0.799473
Reading from 15607: heap size 272 MB, throughput 0.872472
Reading from 15607: heap size 277 MB, throughput 0.72405
Reading from 15607: heap size 280 MB, throughput 0.727821
Reading from 15607: heap size 280 MB, throughput 0.707463
Reading from 15608: heap size 108 MB, throughput 0.995697
Reading from 15607: heap size 282 MB, throughput 0.733719
Reading from 15607: heap size 283 MB, throughput 0.779592
Reading from 15607: heap size 282 MB, throughput 0.962336
Reading from 15608: heap size 120 MB, throughput 0.996423
Reading from 15607: heap size 284 MB, throughput 0.136071
Reading from 15607: heap size 332 MB, throughput 0.730071
Reading from 15607: heap size 332 MB, throughput 0.816335
Reading from 15607: heap size 338 MB, throughput 0.89513
Reading from 15607: heap size 338 MB, throughput 0.918298
Reading from 15608: heap size 122 MB, throughput 0.996283
Reading from 15607: heap size 338 MB, throughput 0.890983
Reading from 15607: heap size 340 MB, throughput 0.734459
Reading from 15607: heap size 342 MB, throughput 0.783142
Reading from 15607: heap size 342 MB, throughput 0.647777
Reading from 15607: heap size 349 MB, throughput 0.685362
Reading from 15607: heap size 350 MB, throughput 0.568762
Equal recommendation: 720 MB each
Reading from 15607: heap size 358 MB, throughput 0.913673
Reading from 15608: heap size 131 MB, throughput 0.993851
Reading from 15608: heap size 133 MB, throughput 0.99162
Reading from 15607: heap size 359 MB, throughput 0.983952
Reading from 15608: heap size 141 MB, throughput 0.996454
Reading from 15608: heap size 142 MB, throughput 0.997274
Reading from 15607: heap size 359 MB, throughput 0.980492
Reading from 15608: heap size 147 MB, throughput 0.997112
Reading from 15607: heap size 363 MB, throughput 0.970708
Reading from 15608: heap size 148 MB, throughput 0.996626
Reading from 15608: heap size 154 MB, throughput 0.996984
Reading from 15607: heap size 365 MB, throughput 0.972348
Reading from 15608: heap size 154 MB, throughput 0.996593
Reading from 15607: heap size 366 MB, throughput 0.975742
Reading from 15608: heap size 158 MB, throughput 0.995297
Reading from 15608: heap size 159 MB, throughput 0.996641
Reading from 15607: heap size 365 MB, throughput 0.980737
Equal recommendation: 720 MB each
Reading from 15608: heap size 163 MB, throughput 0.997081
Reading from 15607: heap size 368 MB, throughput 0.969518
Reading from 15608: heap size 163 MB, throughput 0.996905
Reading from 15607: heap size 365 MB, throughput 0.978092
Reading from 15608: heap size 167 MB, throughput 0.997905
Reading from 15608: heap size 167 MB, throughput 0.99779
Reading from 15607: heap size 367 MB, throughput 0.956611
Reading from 15608: heap size 170 MB, throughput 0.991615
Reading from 15608: heap size 170 MB, throughput 0.989947
Reading from 15607: heap size 369 MB, throughput 0.977992
Reading from 15608: heap size 175 MB, throughput 0.997695
Reading from 15607: heap size 369 MB, throughput 0.97882
Reading from 15608: heap size 176 MB, throughput 0.99736
Reading from 15608: heap size 181 MB, throughput 0.997873
Reading from 15607: heap size 371 MB, throughput 0.979918
Reading from 15608: heap size 181 MB, throughput 0.997219
Reading from 15607: heap size 372 MB, throughput 0.978336
Equal recommendation: 720 MB each
Reading from 15608: heap size 185 MB, throughput 0.997776
Reading from 15607: heap size 374 MB, throughput 0.97539
Reading from 15608: heap size 185 MB, throughput 0.997239
Reading from 15607: heap size 375 MB, throughput 0.981626
Reading from 15608: heap size 189 MB, throughput 0.997995
Reading from 15607: heap size 380 MB, throughput 0.876938
Reading from 15607: heap size 381 MB, throughput 0.784314
Reading from 15607: heap size 386 MB, throughput 0.778361
Reading from 15607: heap size 389 MB, throughput 0.831053
Reading from 15608: heap size 189 MB, throughput 0.99723
Reading from 15607: heap size 397 MB, throughput 0.985562
Reading from 15608: heap size 193 MB, throughput 0.996899
Reading from 15607: heap size 398 MB, throughput 0.990821
Reading from 15608: heap size 193 MB, throughput 0.997215
Reading from 15608: heap size 196 MB, throughput 0.99719
Reading from 15607: heap size 400 MB, throughput 0.986494
Reading from 15608: heap size 196 MB, throughput 0.99719
Equal recommendation: 720 MB each
Reading from 15607: heap size 402 MB, throughput 0.987301
Reading from 15608: heap size 199 MB, throughput 0.997616
Reading from 15608: heap size 199 MB, throughput 0.997192
Reading from 15607: heap size 401 MB, throughput 0.98031
Reading from 15608: heap size 203 MB, throughput 0.993467
Reading from 15608: heap size 203 MB, throughput 0.994623
Reading from 15607: heap size 404 MB, throughput 0.986309
Reading from 15608: heap size 207 MB, throughput 0.998505
Reading from 15608: heap size 208 MB, throughput 0.997599
Reading from 15607: heap size 401 MB, throughput 0.987576
Reading from 15608: heap size 211 MB, throughput 0.997993
Reading from 15607: heap size 403 MB, throughput 0.988329
Reading from 15608: heap size 211 MB, throughput 0.996323
Reading from 15607: heap size 399 MB, throughput 0.985162
Equal recommendation: 720 MB each
Reading from 15608: heap size 215 MB, throughput 0.998019
Reading from 15608: heap size 215 MB, throughput 0.996344
Reading from 15607: heap size 402 MB, throughput 0.989304
Reading from 15608: heap size 218 MB, throughput 0.998056
Reading from 15607: heap size 399 MB, throughput 0.984057
Reading from 15608: heap size 218 MB, throughput 0.997756
Reading from 15608: heap size 222 MB, throughput 0.997937
Reading from 15607: heap size 401 MB, throughput 0.992265
Reading from 15607: heap size 401 MB, throughput 0.927699
Reading from 15607: heap size 402 MB, throughput 0.864711
Reading from 15607: heap size 405 MB, throughput 0.878394
Reading from 15607: heap size 406 MB, throughput 0.970152
Reading from 15608: heap size 222 MB, throughput 0.997021
Reading from 15608: heap size 224 MB, throughput 0.996667
Reading from 15607: heap size 412 MB, throughput 0.99219
Reading from 15608: heap size 225 MB, throughput 0.997665
Equal recommendation: 720 MB each
Reading from 15608: heap size 228 MB, throughput 0.868582
Reading from 15607: heap size 413 MB, throughput 0.801205
Reading from 15608: heap size 233 MB, throughput 0.998121
Reading from 15608: heap size 238 MB, throughput 0.998972
Reading from 15607: heap size 437 MB, throughput 0.99524
Reading from 15608: heap size 239 MB, throughput 0.998534
Reading from 15607: heap size 438 MB, throughput 0.995882
Reading from 15608: heap size 243 MB, throughput 0.998773
Reading from 15608: heap size 244 MB, throughput 0.998753
Reading from 15607: heap size 444 MB, throughput 0.993164
Reading from 15608: heap size 247 MB, throughput 0.998207
Reading from 15607: heap size 445 MB, throughput 0.992856
Equal recommendation: 720 MB each
Reading from 15608: heap size 248 MB, throughput 0.998861
Reading from 15607: heap size 444 MB, throughput 0.991127
Reading from 15608: heap size 251 MB, throughput 0.998772
Reading from 15608: heap size 251 MB, throughput 0.998861
Reading from 15607: heap size 411 MB, throughput 0.990867
Reading from 15608: heap size 255 MB, throughput 0.998834
Reading from 15607: heap size 440 MB, throughput 0.987744
Reading from 15608: heap size 255 MB, throughput 0.998436
Reading from 15608: heap size 258 MB, throughput 0.991592
Reading from 15607: heap size 442 MB, throughput 0.991055
Reading from 15607: heap size 442 MB, throughput 0.92287
Reading from 15607: heap size 443 MB, throughput 0.836089
Reading from 15607: heap size 448 MB, throughput 0.886015
Reading from 15608: heap size 258 MB, throughput 0.997001
Equal recommendation: 720 MB each
Reading from 15608: heap size 266 MB, throughput 0.998035
Reading from 15607: heap size 453 MB, throughput 0.985699
Reading from 15608: heap size 266 MB, throughput 0.99784
Reading from 15607: heap size 456 MB, throughput 0.992613
Reading from 15608: heap size 272 MB, throughput 0.997625
Reading from 15607: heap size 461 MB, throughput 0.989154
Reading from 15608: heap size 272 MB, throughput 0.998375
Reading from 15608: heap size 276 MB, throughput 0.998495
Reading from 15607: heap size 458 MB, throughput 0.991707
Reading from 15608: heap size 277 MB, throughput 0.998295
Equal recommendation: 720 MB each
Reading from 15607: heap size 461 MB, throughput 0.989589
Reading from 15608: heap size 282 MB, throughput 0.998391
Reading from 15608: heap size 282 MB, throughput 0.997903
Reading from 15607: heap size 463 MB, throughput 0.989063
Reading from 15608: heap size 287 MB, throughput 0.997876
Reading from 15608: heap size 287 MB, throughput 0.992565
Reading from 15607: heap size 464 MB, throughput 0.988379
Reading from 15608: heap size 291 MB, throughput 0.998285
Reading from 15607: heap size 466 MB, throughput 0.987822
Reading from 15608: heap size 292 MB, throughput 0.99822
Equal recommendation: 720 MB each
Reading from 15607: heap size 466 MB, throughput 0.993084
Reading from 15608: heap size 297 MB, throughput 0.998511
Reading from 15607: heap size 467 MB, throughput 0.97497
Reading from 15607: heap size 468 MB, throughput 0.874869
Reading from 15607: heap size 472 MB, throughput 0.900432
Reading from 15608: heap size 298 MB, throughput 0.997787
Reading from 15607: heap size 473 MB, throughput 0.991612
Reading from 15608: heap size 304 MB, throughput 0.998029
Reading from 15607: heap size 478 MB, throughput 0.993288
Reading from 15608: heap size 304 MB, throughput 0.998383
Reading from 15607: heap size 480 MB, throughput 0.992216
Reading from 15608: heap size 309 MB, throughput 0.998573
Equal recommendation: 720 MB each
Reading from 15608: heap size 309 MB, throughput 0.997693
Reading from 15607: heap size 481 MB, throughput 0.9923
Reading from 15608: heap size 314 MB, throughput 0.996175
Reading from 15608: heap size 314 MB, throughput 0.995541
Reading from 15607: heap size 483 MB, throughput 0.990739
Reading from 15608: heap size 321 MB, throughput 0.997468
Reading from 15607: heap size 480 MB, throughput 0.991761
Reading from 15608: heap size 321 MB, throughput 0.998317
Reading from 15607: heap size 483 MB, throughput 0.990522
Reading from 15608: heap size 328 MB, throughput 0.998353
Equal recommendation: 720 MB each
Reading from 15607: heap size 484 MB, throughput 0.994552
Reading from 15608: heap size 328 MB, throughput 0.998219
Reading from 15607: heap size 484 MB, throughput 0.970785
Reading from 15607: heap size 485 MB, throughput 0.912627
Reading from 15607: heap size 487 MB, throughput 0.952958
Reading from 15608: heap size 333 MB, throughput 0.998474
Reading from 15607: heap size 493 MB, throughput 0.993761
Reading from 15608: heap size 334 MB, throughput 0.997243
Reading from 15607: heap size 494 MB, throughput 0.99253
Reading from 15608: heap size 339 MB, throughput 0.998423
Reading from 15608: heap size 339 MB, throughput 0.995514
Equal recommendation: 720 MB each
Reading from 15607: heap size 494 MB, throughput 0.992855
Reading from 15608: heap size 345 MB, throughput 0.997823
Reading from 15608: heap size 345 MB, throughput 0.998456
Reading from 15607: heap size 496 MB, throughput 0.993154
Reading from 15608: heap size 352 MB, throughput 0.995347
Reading from 15607: heap size 494 MB, throughput 0.990286
Reading from 15608: heap size 352 MB, throughput 0.998459
Reading from 15607: heap size 496 MB, throughput 0.990731
Equal recommendation: 720 MB each
Reading from 15608: heap size 359 MB, throughput 0.997834
Reading from 15608: heap size 360 MB, throughput 0.998135
Reading from 15607: heap size 497 MB, throughput 0.988293
Reading from 15607: heap size 498 MB, throughput 0.974453
Reading from 15607: heap size 501 MB, throughput 0.888632
Reading from 15607: heap size 506 MB, throughput 0.960016
Reading from 15608: heap size 366 MB, throughput 0.997544
Reading from 15608: heap size 366 MB, throughput 0.996654
Reading from 15607: heap size 513 MB, throughput 0.994077
Reading from 15608: heap size 373 MB, throughput 0.997888
Equal recommendation: 720 MB each
Reading from 15608: heap size 373 MB, throughput 0.998338
Reading from 15607: heap size 515 MB, throughput 0.990542
Reading from 15608: heap size 379 MB, throughput 0.998101
Reading from 15607: heap size 513 MB, throughput 0.991945
Reading from 15608: heap size 380 MB, throughput 0.996885
Reading from 15607: heap size 516 MB, throughput 0.990981
Reading from 15608: heap size 387 MB, throughput 0.997705
Equal recommendation: 720 MB each
Reading from 15607: heap size 518 MB, throughput 0.991939
Reading from 15608: heap size 387 MB, throughput 0.998132
Reading from 15608: heap size 393 MB, throughput 0.99864
Reading from 15607: heap size 518 MB, throughput 0.98334
Reading from 15608: heap size 394 MB, throughput 0.995591
Reading from 15607: heap size 521 MB, throughput 0.992831
Reading from 15607: heap size 522 MB, throughput 0.922479
Reading from 15607: heap size 522 MB, throughput 0.915231
Reading from 15608: heap size 399 MB, throughput 0.998684
Reading from 15607: heap size 524 MB, throughput 0.993128
Reading from 15608: heap size 400 MB, throughput 0.997677
Equal recommendation: 720 MB each
Reading from 15608: heap size 408 MB, throughput 0.998618
Reading from 15607: heap size 531 MB, throughput 0.993881
Reading from 15608: heap size 408 MB, throughput 0.998156
Reading from 15607: heap size 532 MB, throughput 0.994228
Reading from 15608: heap size 414 MB, throughput 0.998676
Reading from 15607: heap size 533 MB, throughput 0.993486
Equal recommendation: 720 MB each
Reading from 15608: heap size 414 MB, throughput 0.998309
Reading from 15608: heap size 421 MB, throughput 0.784654
Reading from 15607: heap size 535 MB, throughput 0.991829
Reading from 15608: heap size 424 MB, throughput 0.998273
Reading from 15607: heap size 533 MB, throughput 0.991434
Reading from 15608: heap size 440 MB, throughput 0.999192
Reading from 15607: heap size 535 MB, throughput 0.993069
Reading from 15607: heap size 537 MB, throughput 0.923809
Reading from 15608: heap size 440 MB, throughput 0.999156
Reading from 15607: heap size 537 MB, throughput 0.968664
Equal recommendation: 720 MB each
Reading from 15608: heap size 450 MB, throughput 0.999284
Reading from 15607: heap size 543 MB, throughput 0.994404
Reading from 15608: heap size 452 MB, throughput 0.999153
Reading from 15607: heap size 544 MB, throughput 0.994786
Reading from 15608: heap size 460 MB, throughput 0.999065
Reading from 15608: heap size 462 MB, throughput 0.996643
Reading from 15607: heap size 544 MB, throughput 0.993329
Equal recommendation: 720 MB each
Reading from 15608: heap size 469 MB, throughput 0.998676
Reading from 15607: heap size 546 MB, throughput 0.992854
Reading from 15608: heap size 470 MB, throughput 0.998611
Reading from 15607: heap size 544 MB, throughput 0.992028
Reading from 15608: heap size 479 MB, throughput 0.998691
Reading from 15607: heap size 546 MB, throughput 0.995485
Reading from 15607: heap size 547 MB, throughput 0.953593
Equal recommendation: 720 MB each
Reading from 15607: heap size 548 MB, throughput 0.953318
Reading from 15608: heap size 479 MB, throughput 0.997701
Reading from 15607: heap size 555 MB, throughput 0.9951
Reading from 15608: heap size 488 MB, throughput 0.998385
Reading from 15608: heap size 488 MB, throughput 0.996466
Reading from 15607: heap size 555 MB, throughput 0.994791
Reading from 15608: heap size 494 MB, throughput 0.99896
Equal recommendation: 720 MB each
Reading from 15607: heap size 554 MB, throughput 0.99301
Reading from 15608: heap size 496 MB, throughput 0.99875
Reading from 15607: heap size 557 MB, throughput 0.993202
Reading from 15608: heap size 495 MB, throughput 0.997932
Reading from 15607: heap size 557 MB, throughput 0.933865
Reading from 15608: heap size 496 MB, throughput 0.998771
Equal recommendation: 720 MB each
Reading from 15607: heap size 578 MB, throughput 0.990281
Reading from 15608: heap size 495 MB, throughput 0.997939
Reading from 15607: heap size 582 MB, throughput 0.936898
Reading from 15607: heap size 582 MB, throughput 0.986168
Reading from 15608: heap size 496 MB, throughput 0.997271
Reading from 15607: heap size 589 MB, throughput 0.99544
Reading from 15608: heap size 496 MB, throughput 0.998706
Equal recommendation: 720 MB each
Reading from 15608: heap size 496 MB, throughput 0.99828
Reading from 15607: heap size 590 MB, throughput 0.994521
Reading from 15608: heap size 495 MB, throughput 0.998285
Reading from 15607: heap size 590 MB, throughput 0.994146
Reading from 15608: heap size 496 MB, throughput 0.998774
Reading from 15607: heap size 592 MB, throughput 0.988013
Reading from 15608: heap size 497 MB, throughput 0.997278
Equal recommendation: 720 MB each
Reading from 15608: heap size 497 MB, throughput 0.998546
Reading from 15607: heap size 595 MB, throughput 0.992402
Reading from 15607: heap size 595 MB, throughput 0.98598
Reading from 15607: heap size 599 MB, throughput 0.938708
Reading from 15608: heap size 496 MB, throughput 0.99817
Reading from 15607: heap size 602 MB, throughput 0.993628
Reading from 15608: heap size 496 MB, throughput 0.997376
Equal recommendation: 720 MB each
Reading from 15607: heap size 607 MB, throughput 0.993979
Reading from 15608: heap size 495 MB, throughput 0.998639
Reading from 15608: heap size 496 MB, throughput 0.998017
Reading from 15607: heap size 609 MB, throughput 0.994384
Client 15608 died
Clients: 1
Reading from 15607: heap size 608 MB, throughput 0.993898
Recommendation: one client; give it all the memory
Reading from 15607: heap size 611 MB, throughput 0.992119
Reading from 15607: heap size 614 MB, throughput 0.989272
Reading from 15607: heap size 615 MB, throughput 0.930552
Reading from 15607: heap size 619 MB, throughput 0.99434
Recommendation: one client; give it all the memory
Reading from 15607: heap size 621 MB, throughput 0.994792
Reading from 15607: heap size 623 MB, throughput 0.993904
Recommendation: one client; give it all the memory
Reading from 15607: heap size 625 MB, throughput 0.993713
Reading from 15607: heap size 626 MB, throughput 0.992529
Reading from 15607: heap size 627 MB, throughput 0.981013
Client 15607 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
