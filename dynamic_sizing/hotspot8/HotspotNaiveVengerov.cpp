#include "gc_implementation/shared/HotspotNaiveVengerov.hpp"

HotspotNaiveVengerov::HotspotNaiveVengerov() : NaiveVengerov() {}

unsigned long long HotspotNaiveVengerov::getHeapSizeB()
{
    return ParallelScavengeHeap::heap()->capacity();
}

unsigned long long HotspotNaiveVengerov::getYoungGenSizeB()
{
    return ParallelScavengeHeap::young_gen()->eden_space()->capacity_in_bytes() +
        ParallelScavengeHeap::young_gen()->from_space()->capacity_in_bytes() +
        ParallelScavengeHeap::young_gen()->to_space()->capacity_in_bytes();
}

unsigned long long HotspotNaiveVengerov::getOldGenSizeB()
{
    return ParallelScavengeHeap::old_gen()->used_in_bytes();
}

void HotspotNaiveVengerov::logReading(unsigned long long heapSizeB, double throughput)
{
    if (PrintGC)
        gclog_or_tty->print("#END NaiveVengerov: heapSize %llu (%f MB), throughput %f\n",
                            heapSizeB,
                            (double)heapSizeB/(1024.0*1024.0),
                            throughput);
}
