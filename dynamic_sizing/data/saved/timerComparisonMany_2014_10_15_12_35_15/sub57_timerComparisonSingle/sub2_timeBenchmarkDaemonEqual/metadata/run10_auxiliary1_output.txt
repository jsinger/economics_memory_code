economemd
    total memory: 864 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: yes
    equal recommendations: yes

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/timerComparisonMany_2014_10_15_12_35_15/sub57_timerComparisonSingle/sub2_timeBenchmarkDaemonEqual/daemon_run10_log.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
Reading from 9066: heap size 9 MB, throughput 0.972015
Clients: 1
Client 9066 has a minimum heap size of 12 MB
Reading from 9065: heap size 9 MB, throughput 0.992112
Clients: 2
Client 9065 has a minimum heap size of 276 MB
Reading from 9065: heap size 9 MB, throughput 0.963875
Reading from 9066: heap size 9 MB, throughput 0.989864
Reading from 9065: heap size 9 MB, throughput 0.973449
Reading from 9065: heap size 9 MB, throughput 0.964276
Reading from 9066: heap size 11 MB, throughput 0.974115
Reading from 9065: heap size 11 MB, throughput 0.986201
Reading from 9066: heap size 11 MB, throughput 0.976301
Reading from 9065: heap size 11 MB, throughput 0.986909
Reading from 9066: heap size 15 MB, throughput 0.990063
Reading from 9065: heap size 17 MB, throughput 0.971725
Reading from 9065: heap size 17 MB, throughput 0.619398
Reading from 9066: heap size 15 MB, throughput 0.988884
Reading from 9065: heap size 30 MB, throughput 0.958325
Reading from 9065: heap size 31 MB, throughput 0.955704
Reading from 9066: heap size 24 MB, throughput 0.921692
Reading from 9065: heap size 35 MB, throughput 0.244759
Reading from 9065: heap size 48 MB, throughput 0.838205
Reading from 9065: heap size 50 MB, throughput 0.327848
Reading from 9065: heap size 67 MB, throughput 0.769161
Reading from 9065: heap size 73 MB, throughput 0.795103
Reading from 9066: heap size 29 MB, throughput 0.981854
Reading from 9065: heap size 74 MB, throughput 0.251507
Reading from 9065: heap size 100 MB, throughput 0.815409
Reading from 9066: heap size 36 MB, throughput 0.986304
Reading from 9066: heap size 42 MB, throughput 0.988815
Reading from 9065: heap size 101 MB, throughput 0.211612
Reading from 9065: heap size 134 MB, throughput 0.804887
Reading from 9066: heap size 43 MB, throughput 0.985748
Reading from 9065: heap size 134 MB, throughput 0.829107
Reading from 9066: heap size 45 MB, throughput 0.980534
Reading from 9065: heap size 135 MB, throughput 0.766661
Reading from 9065: heap size 138 MB, throughput 0.731178
Reading from 9066: heap size 51 MB, throughput 0.988491
Reading from 9066: heap size 51 MB, throughput 0.909461
Reading from 9066: heap size 58 MB, throughput 0.981635
Reading from 9065: heap size 140 MB, throughput 0.111761
Reading from 9066: heap size 58 MB, throughput 0.987808
Reading from 9065: heap size 179 MB, throughput 0.690078
Reading from 9066: heap size 69 MB, throughput 0.660654
Reading from 9065: heap size 182 MB, throughput 0.67356
Reading from 9065: heap size 185 MB, throughput 0.626005
Reading from 9066: heap size 75 MB, throughput 0.989462
Reading from 9065: heap size 192 MB, throughput 0.742556
Reading from 9065: heap size 195 MB, throughput 0.828756
Reading from 9066: heap size 85 MB, throughput 0.996848
Reading from 9066: heap size 85 MB, throughput 0.995825
Reading from 9065: heap size 196 MB, throughput 0.483039
Reading from 9065: heap size 249 MB, throughput 0.77757
Reading from 9065: heap size 253 MB, throughput 0.844536
Reading from 9066: heap size 94 MB, throughput 0.997028
Reading from 9065: heap size 255 MB, throughput 0.853947
Reading from 9065: heap size 259 MB, throughput 0.79882
Reading from 9065: heap size 261 MB, throughput 0.782484
Reading from 9065: heap size 265 MB, throughput 0.790085
Reading from 9065: heap size 266 MB, throughput 0.814787
Reading from 9065: heap size 265 MB, throughput 0.68339
Reading from 9065: heap size 270 MB, throughput 0.663464
Reading from 9066: heap size 95 MB, throughput 0.997522
Reading from 9065: heap size 276 MB, throughput 0.744096
Reading from 9065: heap size 278 MB, throughput 0.726902
Reading from 9065: heap size 284 MB, throughput 0.754621
Reading from 9066: heap size 101 MB, throughput 0.997678
Reading from 9065: heap size 285 MB, throughput 0.928605
Reading from 9065: heap size 284 MB, throughput 0.784242
Reading from 9066: heap size 103 MB, throughput 0.996837
Reading from 9065: heap size 286 MB, throughput 0.0940049
Reading from 9065: heap size 333 MB, throughput 0.749085
Reading from 9065: heap size 334 MB, throughput 0.906525
Reading from 9065: heap size 337 MB, throughput 0.914051
Reading from 9065: heap size 338 MB, throughput 0.915593
Reading from 9065: heap size 339 MB, throughput 0.869432
Reading from 9066: heap size 108 MB, throughput 0.9956
Reading from 9065: heap size 340 MB, throughput 0.702915
Reading from 9065: heap size 343 MB, throughput 0.730047
Reading from 9065: heap size 343 MB, throughput 0.632201
Reading from 9065: heap size 349 MB, throughput 0.726177
Reading from 9065: heap size 349 MB, throughput 0.616952
Reading from 9065: heap size 356 MB, throughput 0.638431
Equal recommendation: 432 MB each
Reading from 9066: heap size 109 MB, throughput 0.9872
Reading from 9065: heap size 357 MB, throughput 0.950016
Reading from 9066: heap size 115 MB, throughput 0.995695
Reading from 9066: heap size 114 MB, throughput 0.991809
Reading from 9065: heap size 361 MB, throughput 0.966435
Reading from 9066: heap size 120 MB, throughput 0.99698
Reading from 9066: heap size 120 MB, throughput 0.996604
Reading from 9065: heap size 364 MB, throughput 0.979846
Reading from 9066: heap size 124 MB, throughput 0.996953
Reading from 9065: heap size 368 MB, throughput 0.986462
Reading from 9066: heap size 125 MB, throughput 0.99612
Reading from 9066: heap size 130 MB, throughput 0.997197
Reading from 9065: heap size 369 MB, throughput 0.979192
Reading from 9066: heap size 130 MB, throughput 0.994827
Reading from 9066: heap size 134 MB, throughput 0.997142
Reading from 9065: heap size 368 MB, throughput 0.980474
Reading from 9066: heap size 134 MB, throughput 0.997688
Reading from 9065: heap size 370 MB, throughput 0.976582
Reading from 9066: heap size 137 MB, throughput 0.997462
Reading from 9066: heap size 138 MB, throughput 0.998108
Equal recommendation: 432 MB each
Reading from 9065: heap size 365 MB, throughput 0.97609
Reading from 9066: heap size 140 MB, throughput 0.998546
Reading from 9066: heap size 140 MB, throughput 0.998038
Reading from 9065: heap size 368 MB, throughput 0.983613
Reading from 9066: heap size 143 MB, throughput 0.998213
Reading from 9065: heap size 364 MB, throughput 0.978679
Reading from 9066: heap size 143 MB, throughput 0.996441
Reading from 9066: heap size 146 MB, throughput 0.991791
Reading from 9066: heap size 146 MB, throughput 0.979958
Reading from 9065: heap size 366 MB, throughput 0.945641
Reading from 9066: heap size 149 MB, throughput 0.995842
Reading from 9065: heap size 367 MB, throughput 0.975079
Reading from 9066: heap size 150 MB, throughput 0.997386
Reading from 9066: heap size 156 MB, throughput 0.997305
Reading from 9065: heap size 367 MB, throughput 0.973428
Reading from 9066: heap size 157 MB, throughput 0.997384
Reading from 9065: heap size 370 MB, throughput 0.981004
Reading from 9066: heap size 162 MB, throughput 0.997198
Equal recommendation: 432 MB each
Reading from 9066: heap size 162 MB, throughput 0.995046
Reading from 9065: heap size 370 MB, throughput 0.971106
Reading from 9066: heap size 167 MB, throughput 0.997723
Reading from 9066: heap size 167 MB, throughput 0.997296
Reading from 9065: heap size 372 MB, throughput 0.990323
Reading from 9065: heap size 373 MB, throughput 0.977448
Reading from 9065: heap size 369 MB, throughput 0.843174
Reading from 9066: heap size 170 MB, throughput 0.997713
Reading from 9065: heap size 371 MB, throughput 0.808093
Reading from 9065: heap size 377 MB, throughput 0.831053
Reading from 9065: heap size 380 MB, throughput 0.814232
Reading from 9065: heap size 387 MB, throughput 0.979149
Reading from 9066: heap size 171 MB, throughput 0.997368
Reading from 9066: heap size 174 MB, throughput 0.996854
Reading from 9065: heap size 389 MB, throughput 0.989097
Reading from 9066: heap size 174 MB, throughput 0.993499
Reading from 9065: heap size 390 MB, throughput 0.988372
Reading from 9066: heap size 177 MB, throughput 0.997373
Reading from 9066: heap size 177 MB, throughput 0.997393
Reading from 9065: heap size 392 MB, throughput 0.988268
Equal recommendation: 432 MB each
Reading from 9066: heap size 181 MB, throughput 0.998311
Reading from 9065: heap size 391 MB, throughput 0.986496
Reading from 9066: heap size 181 MB, throughput 0.997459
Reading from 9066: heap size 185 MB, throughput 0.996586
Reading from 9065: heap size 393 MB, throughput 0.981066
Reading from 9066: heap size 185 MB, throughput 0.993427
Reading from 9066: heap size 188 MB, throughput 0.996869
Reading from 9065: heap size 391 MB, throughput 0.987555
Reading from 9066: heap size 188 MB, throughput 0.99785
Reading from 9066: heap size 192 MB, throughput 0.997074
Reading from 9065: heap size 393 MB, throughput 0.985651
Reading from 9066: heap size 192 MB, throughput 0.998036
Reading from 9065: heap size 388 MB, throughput 0.986092
Reading from 9066: heap size 196 MB, throughput 0.99694
Reading from 9066: heap size 196 MB, throughput 0.997789
Reading from 9065: heap size 391 MB, throughput 0.986102
Equal recommendation: 432 MB each
Reading from 9066: heap size 199 MB, throughput 0.997807
Reading from 9065: heap size 389 MB, throughput 0.986558
Reading from 9066: heap size 199 MB, throughput 0.997419
Reading from 9066: heap size 202 MB, throughput 0.997927
Reading from 9065: heap size 390 MB, throughput 0.987808
Reading from 9066: heap size 202 MB, throughput 0.997842
Reading from 9065: heap size 390 MB, throughput 0.99267
Reading from 9066: heap size 206 MB, throughput 0.997878
Reading from 9065: heap size 390 MB, throughput 0.928928
Reading from 9065: heap size 389 MB, throughput 0.864873
Reading from 9065: heap size 392 MB, throughput 0.856887
Reading from 9065: heap size 397 MB, throughput 0.866895
Reading from 9066: heap size 206 MB, throughput 0.997648
Reading from 9065: heap size 398 MB, throughput 0.988359
Reading from 9066: heap size 208 MB, throughput 0.997458
Reading from 9065: heap size 404 MB, throughput 0.988438
Reading from 9066: heap size 209 MB, throughput 0.995383
Equal recommendation: 432 MB each
Reading from 9066: heap size 212 MB, throughput 0.989404
Reading from 9065: heap size 405 MB, throughput 0.98987
Reading from 9066: heap size 212 MB, throughput 0.995064
Reading from 9066: heap size 220 MB, throughput 0.998256
Reading from 9065: heap size 405 MB, throughput 0.990765
Reading from 9066: heap size 220 MB, throughput 0.997909
Reading from 9065: heap size 407 MB, throughput 0.990149
Reading from 9066: heap size 223 MB, throughput 0.998413
Reading from 9066: heap size 224 MB, throughput 0.997805
Reading from 9065: heap size 405 MB, throughput 0.805006
Reading from 9066: heap size 213 MB, throughput 0.998032
Reading from 9065: heap size 435 MB, throughput 0.996118
Reading from 9066: heap size 204 MB, throughput 0.99804
Equal recommendation: 432 MB each
Reading from 9065: heap size 431 MB, throughput 0.99511
Reading from 9066: heap size 195 MB, throughput 0.997903
Reading from 9066: heap size 187 MB, throughput 0.997749
Reading from 9065: heap size 434 MB, throughput 0.993372
Reading from 9066: heap size 179 MB, throughput 0.996033
Reading from 9066: heap size 172 MB, throughput 0.997609
Reading from 9065: heap size 429 MB, throughput 0.99039
Reading from 9066: heap size 164 MB, throughput 0.997753
Reading from 9066: heap size 157 MB, throughput 0.997495
Reading from 9065: heap size 433 MB, throughput 0.990128
Reading from 9066: heap size 151 MB, throughput 0.909271
Reading from 9066: heap size 146 MB, throughput 0.988249
Reading from 9066: heap size 152 MB, throughput 0.990952
Reading from 9065: heap size 422 MB, throughput 0.98509
Reading from 9065: heap size 428 MB, throughput 0.901417
Reading from 9065: heap size 424 MB, throughput 0.872248
Reading from 9065: heap size 429 MB, throughput 0.85789
Reading from 9066: heap size 159 MB, throughput 0.993725
Reading from 9065: heap size 435 MB, throughput 0.943332
Reading from 9066: heap size 165 MB, throughput 0.998014
Equal recommendation: 432 MB each
Reading from 9065: heap size 397 MB, throughput 0.989507
Reading from 9066: heap size 172 MB, throughput 0.998104
Reading from 9066: heap size 178 MB, throughput 0.997971
Reading from 9065: heap size 436 MB, throughput 0.991194
Reading from 9066: heap size 183 MB, throughput 0.992357
Reading from 9065: heap size 402 MB, throughput 0.9876
Reading from 9066: heap size 190 MB, throughput 0.998159
Reading from 9066: heap size 197 MB, throughput 0.997564
Reading from 9065: heap size 431 MB, throughput 0.989111
Reading from 9066: heap size 203 MB, throughput 0.998234
Reading from 9065: heap size 429 MB, throughput 0.990175
Reading from 9066: heap size 209 MB, throughput 0.997553
Reading from 9066: heap size 210 MB, throughput 0.997782
Reading from 9065: heap size 429 MB, throughput 0.989519
Equal recommendation: 432 MB each
Reading from 9066: heap size 209 MB, throughput 0.997516
Reading from 9065: heap size 430 MB, throughput 0.986413
Reading from 9066: heap size 214 MB, throughput 0.998114
Reading from 9066: heap size 214 MB, throughput 0.997359
Reading from 9065: heap size 431 MB, throughput 0.986533
Reading from 9066: heap size 219 MB, throughput 0.995349
Reading from 9066: heap size 219 MB, throughput 0.992704
Reading from 9065: heap size 432 MB, throughput 0.979018
Reading from 9066: heap size 224 MB, throughput 0.997079
Reading from 9065: heap size 433 MB, throughput 0.983866
Reading from 9066: heap size 225 MB, throughput 0.997838
Reading from 9066: heap size 232 MB, throughput 0.998118
Reading from 9065: heap size 419 MB, throughput 0.991754
Reading from 9065: heap size 432 MB, throughput 0.974839
Reading from 9065: heap size 432 MB, throughput 0.89272
Reading from 9065: heap size 434 MB, throughput 0.89068
Equal recommendation: 432 MB each
Reading from 9065: heap size 423 MB, throughput 0.888039
Reading from 9066: heap size 232 MB, throughput 0.996315
Reading from 9065: heap size 433 MB, throughput 0.991041
Reading from 9066: heap size 239 MB, throughput 0.997252
Reading from 9065: heap size 400 MB, throughput 0.990676
Reading from 9066: heap size 239 MB, throughput 0.99782
Reading from 9066: heap size 245 MB, throughput 0.997383
Reading from 9065: heap size 433 MB, throughput 0.990853
Reading from 9066: heap size 245 MB, throughput 0.998212
Reading from 9065: heap size 401 MB, throughput 0.991129
Reading from 9066: heap size 250 MB, throughput 0.998191
Reading from 9065: heap size 431 MB, throughput 0.990771
Reading from 9066: heap size 251 MB, throughput 0.997335
Equal recommendation: 432 MB each
Reading from 9065: heap size 430 MB, throughput 0.986559
Reading from 9066: heap size 256 MB, throughput 0.998148
Reading from 9066: heap size 256 MB, throughput 0.993533
Reading from 9065: heap size 427 MB, throughput 0.979634
Reading from 9066: heap size 261 MB, throughput 0.996198
Reading from 9065: heap size 429 MB, throughput 0.984112
Reading from 9066: heap size 262 MB, throughput 0.997499
Reading from 9065: heap size 428 MB, throughput 0.986831
Reading from 9066: heap size 269 MB, throughput 0.997262
Reading from 9065: heap size 429 MB, throughput 0.985691
Reading from 9066: heap size 270 MB, throughput 0.997882
Reading from 9065: heap size 430 MB, throughput 0.982236
Equal recommendation: 432 MB each
Reading from 9066: heap size 276 MB, throughput 0.998473
Reading from 9065: heap size 431 MB, throughput 0.983561
Reading from 9065: heap size 435 MB, throughput 0.873653
Reading from 9065: heap size 434 MB, throughput 0.834128
Reading from 9065: heap size 435 MB, throughput 0.852679
Reading from 9066: heap size 277 MB, throughput 0.998045
Reading from 9065: heap size 437 MB, throughput 0.978764
Reading from 9066: heap size 282 MB, throughput 0.997978
Reading from 9065: heap size 434 MB, throughput 0.989699
Reading from 9066: heap size 282 MB, throughput 0.998051
Reading from 9065: heap size 405 MB, throughput 0.989273
Reading from 9066: heap size 288 MB, throughput 0.99834
Reading from 9065: heap size 435 MB, throughput 0.98903
Reading from 9066: heap size 288 MB, throughput 0.997448
Reading from 9066: heap size 294 MB, throughput 0.992365
Equal recommendation: 432 MB each
Reading from 9065: heap size 406 MB, throughput 0.988019
Reading from 9066: heap size 294 MB, throughput 0.998093
Reading from 9065: heap size 432 MB, throughput 0.988037
Reading from 9066: heap size 301 MB, throughput 0.997714
Reading from 9065: heap size 430 MB, throughput 0.987683
Reading from 9066: heap size 301 MB, throughput 0.99806
Reading from 9065: heap size 427 MB, throughput 0.987028
Reading from 9066: heap size 301 MB, throughput 0.998379
Reading from 9065: heap size 429 MB, throughput 0.984568
Reading from 9066: heap size 301 MB, throughput 0.998351
Reading from 9065: heap size 427 MB, throughput 0.984023
Equal recommendation: 432 MB each
Reading from 9065: heap size 429 MB, throughput 0.984033
Reading from 9066: heap size 301 MB, throughput 0.998815
Reading from 9066: heap size 301 MB, throughput 0.998413
Reading from 9065: heap size 429 MB, throughput 0.993717
Reading from 9065: heap size 429 MB, throughput 0.935111
Reading from 9065: heap size 432 MB, throughput 0.853232
Reading from 9065: heap size 433 MB, throughput 0.883306
Reading from 9065: heap size 426 MB, throughput 0.928574
Reading from 9066: heap size 300 MB, throughput 0.998728
Reading from 9065: heap size 433 MB, throughput 0.991435
Reading from 9066: heap size 301 MB, throughput 0.996218
Reading from 9065: heap size 424 MB, throughput 0.990748
Reading from 9066: heap size 301 MB, throughput 0.996832
Reading from 9065: heap size 432 MB, throughput 0.992208
Reading from 9066: heap size 301 MB, throughput 0.998461
Equal recommendation: 432 MB each
Reading from 9066: heap size 300 MB, throughput 0.998247
Reading from 9065: heap size 433 MB, throughput 0.989803
Reading from 9066: heap size 301 MB, throughput 0.997276
Reading from 9065: heap size 407 MB, throughput 0.990645
Reading from 9065: heap size 432 MB, throughput 0.988378
Reading from 9066: heap size 300 MB, throughput 0.998276
Reading from 9066: heap size 300 MB, throughput 0.997964
Reading from 9065: heap size 409 MB, throughput 0.990054
Reading from 9066: heap size 301 MB, throughput 0.998397
Reading from 9065: heap size 428 MB, throughput 0.988487
Equal recommendation: 432 MB each
Reading from 9065: heap size 427 MB, throughput 0.984736
Reading from 9066: heap size 301 MB, throughput 0.998585
Reading from 9065: heap size 429 MB, throughput 0.98578
Reading from 9066: heap size 301 MB, throughput 0.997878
Reading from 9066: heap size 301 MB, throughput 0.887649
Reading from 9065: heap size 429 MB, throughput 0.990933
Reading from 9066: heap size 308 MB, throughput 0.998839
Reading from 9065: heap size 431 MB, throughput 0.983491
Reading from 9065: heap size 431 MB, throughput 0.901128
Reading from 9065: heap size 430 MB, throughput 0.89253
Reading from 9065: heap size 432 MB, throughput 0.862073
Reading from 9066: heap size 309 MB, throughput 0.998871
Reading from 9065: heap size 427 MB, throughput 0.989803
Reading from 9066: heap size 309 MB, throughput 0.998933
Equal recommendation: 432 MB each
Reading from 9065: heap size 433 MB, throughput 0.99139
Reading from 9066: heap size 309 MB, throughput 0.999047
Reading from 9065: heap size 424 MB, throughput 0.990091
Reading from 9066: heap size 309 MB, throughput 0.999148
Reading from 9065: heap size 431 MB, throughput 0.99068
Reading from 9066: heap size 309 MB, throughput 0.999132
Reading from 9065: heap size 431 MB, throughput 0.990235
Reading from 9066: heap size 309 MB, throughput 0.999029
Reading from 9065: heap size 433 MB, throughput 0.986187
Reading from 9066: heap size 309 MB, throughput 0.998838
Reading from 9065: heap size 420 MB, throughput 0.987082
Equal recommendation: 432 MB each
Reading from 9066: heap size 309 MB, throughput 0.998128
Reading from 9065: heap size 427 MB, throughput 0.980822
Reading from 9066: heap size 309 MB, throughput 0.995403
Reading from 9065: heap size 428 MB, throughput 0.99032
Reading from 9066: heap size 307 MB, throughput 0.998673
Reading from 9065: heap size 428 MB, throughput 0.985749
Reading from 9066: heap size 308 MB, throughput 0.998291
Reading from 9066: heap size 307 MB, throughput 0.998515
Reading from 9065: heap size 430 MB, throughput 0.991282
Reading from 9065: heap size 431 MB, throughput 0.985332
Reading from 9065: heap size 434 MB, throughput 0.908541
Reading from 9065: heap size 425 MB, throughput 0.848279
Reading from 9065: heap size 432 MB, throughput 0.889202
Reading from 9066: heap size 308 MB, throughput 0.998028
Equal recommendation: 432 MB each
Reading from 9065: heap size 434 MB, throughput 0.991315
Reading from 9066: heap size 309 MB, throughput 0.99848
Reading from 9065: heap size 425 MB, throughput 0.992323
Reading from 9066: heap size 306 MB, throughput 0.998183
Reading from 9065: heap size 433 MB, throughput 0.992565
Reading from 9066: heap size 308 MB, throughput 0.998358
Reading from 9065: heap size 422 MB, throughput 0.990322
Reading from 9066: heap size 308 MB, throughput 0.998286
Reading from 9065: heap size 430 MB, throughput 0.986891
Reading from 9066: heap size 309 MB, throughput 0.996544
Reading from 9066: heap size 309 MB, throughput 0.995221
Equal recommendation: 432 MB each
Reading from 9065: heap size 428 MB, throughput 0.98944
Reading from 9066: heap size 308 MB, throughput 0.998435
Reading from 9065: heap size 430 MB, throughput 0.990211
Reading from 9066: heap size 309 MB, throughput 0.998411
Reading from 9065: heap size 429 MB, throughput 0.988908
Reading from 9066: heap size 309 MB, throughput 0.998366
Reading from 9065: heap size 430 MB, throughput 0.987072
Reading from 9066: heap size 309 MB, throughput 0.996513
Reading from 9065: heap size 431 MB, throughput 0.985561
Reading from 9066: heap size 309 MB, throughput 0.998282
Reading from 9065: heap size 431 MB, throughput 0.983047
Equal recommendation: 432 MB each
Reading from 9065: heap size 430 MB, throughput 0.989604
Reading from 9065: heap size 434 MB, throughput 0.892097
Reading from 9065: heap size 432 MB, throughput 0.891284
Reading from 9066: heap size 309 MB, throughput 0.998484
Reading from 9065: heap size 434 MB, throughput 0.867891
Reading from 9065: heap size 427 MB, throughput 0.98544
Reading from 9066: heap size 309 MB, throughput 0.998478
Reading from 9065: heap size 434 MB, throughput 0.993586
Reading from 9066: heap size 309 MB, throughput 0.998141
Reading from 9066: heap size 310 MB, throughput 0.993166
Reading from 9065: heap size 424 MB, throughput 0.991485
Reading from 9066: heap size 310 MB, throughput 0.997341
Reading from 9065: heap size 432 MB, throughput 0.989782
Reading from 9066: heap size 308 MB, throughput 0.998381
Reading from 9065: heap size 432 MB, throughput 0.991275
Equal recommendation: 432 MB each
Reading from 9066: heap size 309 MB, throughput 0.998267
Reading from 9065: heap size 433 MB, throughput 0.991226
Reading from 9066: heap size 309 MB, throughput 0.998365
Reading from 9065: heap size 421 MB, throughput 0.882139
Reading from 9066: heap size 309 MB, throughput 0.998383
Reading from 9065: heap size 438 MB, throughput 0.996026
Reading from 9065: heap size 435 MB, throughput 0.994661
Reading from 9066: heap size 310 MB, throughput 0.998448
Reading from 9065: heap size 393 MB, throughput 0.993988
Reading from 9066: heap size 310 MB, throughput 0.998527
Equal recommendation: 432 MB each
Reading from 9065: heap size 436 MB, throughput 0.990865
Reading from 9066: heap size 309 MB, throughput 0.99851
Reading from 9066: heap size 309 MB, throughput 0.997848
Reading from 9065: heap size 398 MB, throughput 0.989387
Reading from 9065: heap size 432 MB, throughput 0.918648
Reading from 9065: heap size 431 MB, throughput 0.870939
Reading from 9066: heap size 310 MB, throughput 0.994681
Reading from 9065: heap size 432 MB, throughput 0.867645
Reading from 9065: heap size 434 MB, throughput 0.869884
Reading from 9065: heap size 435 MB, throughput 0.988149
Reading from 9066: heap size 310 MB, throughput 0.997687
Reading from 9065: heap size 397 MB, throughput 0.991896
Reading from 9066: heap size 309 MB, throughput 0.998382
Reading from 9065: heap size 436 MB, throughput 0.990699
Reading from 9066: heap size 310 MB, throughput 0.998406
Reading from 9065: heap size 401 MB, throughput 0.989582
Equal recommendation: 432 MB each
Reading from 9066: heap size 310 MB, throughput 0.998249
Reading from 9065: heap size 431 MB, throughput 0.988862
Reading from 9066: heap size 308 MB, throughput 0.997098
Reading from 9065: heap size 430 MB, throughput 0.988343
Reading from 9066: heap size 309 MB, throughput 0.997602
Reading from 9065: heap size 428 MB, throughput 0.988045
Reading from 9065: heap size 430 MB, throughput 0.987242
Reading from 9066: heap size 309 MB, throughput 0.998798
Reading from 9065: heap size 430 MB, throughput 0.985448
Reading from 9066: heap size 310 MB, throughput 0.997684
Reading from 9066: heap size 310 MB, throughput 0.995081
Equal recommendation: 432 MB each
Reading from 9065: heap size 431 MB, throughput 0.98381
Reading from 9066: heap size 309 MB, throughput 0.997258
Reading from 9065: heap size 433 MB, throughput 0.985582
Reading from 9066: heap size 310 MB, throughput 0.9979
Reading from 9065: heap size 418 MB, throughput 0.979206
Reading from 9065: heap size 428 MB, throughput 0.989447
Reading from 9066: heap size 310 MB, throughput 0.998353
Reading from 9065: heap size 431 MB, throughput 0.902009
Reading from 9065: heap size 429 MB, throughput 0.873184
Reading from 9065: heap size 432 MB, throughput 0.863756
Reading from 9065: heap size 439 MB, throughput 0.898883
Reading from 9066: heap size 310 MB, throughput 0.997648
Reading from 9065: heap size 407 MB, throughput 0.987745
Reading from 9065: heap size 438 MB, throughput 0.991632
Reading from 9066: heap size 310 MB, throughput 0.998258
Equal recommendation: 432 MB each
Reading from 9065: heap size 406 MB, throughput 0.992575
Reading from 9066: heap size 310 MB, throughput 0.997503
Reading from 9065: heap size 436 MB, throughput 0.988218
Reading from 9066: heap size 309 MB, throughput 0.998588
Reading from 9065: heap size 407 MB, throughput 0.99016
Reading from 9066: heap size 309 MB, throughput 0.998556
Reading from 9065: heap size 433 MB, throughput 0.988385
Reading from 9065: heap size 410 MB, throughput 0.980582
Client 9066 died
Clients: 1
Reading from 9065: heap size 429 MB, throughput 0.986586
Recommendation: one client; give it all the memory
Reading from 9065: heap size 428 MB, throughput 0.987082
Reading from 9065: heap size 429 MB, throughput 0.985754
Reading from 9065: heap size 429 MB, throughput 0.982565
Reading from 9065: heap size 431 MB, throughput 0.990755
Reading from 9065: heap size 432 MB, throughput 0.9277
Reading from 9065: heap size 432 MB, throughput 0.87773
Reading from 9065: heap size 434 MB, throughput 0.866865
Reading from 9065: heap size 441 MB, throughput 0.917266
Reading from 9065: heap size 443 MB, throughput 0.989201
Reading from 9065: heap size 449 MB, throughput 0.992004
Recommendation: one client; give it all the memory
Reading from 9065: heap size 450 MB, throughput 0.991969
Reading from 9065: heap size 451 MB, throughput 0.990662
Reading from 9065: heap size 453 MB, throughput 0.988882
Reading from 9065: heap size 452 MB, throughput 0.989474
Reading from 9065: heap size 454 MB, throughput 0.986755
Reading from 9065: heap size 454 MB, throughput 0.987677
Recommendation: one client; give it all the memory
Reading from 9065: heap size 455 MB, throughput 0.985752
Reading from 9065: heap size 458 MB, throughput 0.985268
Reading from 9065: heap size 458 MB, throughput 0.989107
Reading from 9065: heap size 464 MB, throughput 0.912696
Reading from 9065: heap size 464 MB, throughput 0.874807
Reading from 9065: heap size 470 MB, throughput 0.934834
Client 9065 died
Clients: 0
Recommendation: no clients; no recommendation
Recommendation: no clients; no recommendation
TimerThread: finished
ReadingThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
