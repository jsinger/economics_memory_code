module Economem.Signals (SignalHandler,
                         makeSignalHandler,
                         killProcessOnSignal,
                         clearProcesses) where

import Control.Concurrent
import Economem.Process
import System.Exit
import System.Posix.Signals

data SignalHandler = SignalHandler { lock :: MVar (),
                                     processesToCleanup :: MVar [RunningProcess],
                                     targetThread :: ThreadId }

makeSignalHandler :: IO SignalHandler
makeSignalHandler = do
  t <- myThreadId
  l <- newEmptyMVar
  ps <- newMVar []
  let s = SignalHandler { lock = l,
                          processesToCleanup = ps,
                          targetThread = t }
  let h = handler s
  _ <- installHandler sigINT h Nothing
  _ <- installHandler sigTERM h Nothing
  _ <- installHandler sigHUP h Nothing
  return s

killProcessOnSignal :: SignalHandler -> RunningProcess -> IO ()
killProcessOnSignal h p = do
  putMVar (lock h) ()
  ps <- takeMVar (processesToCleanup h)
  putMVar (processesToCleanup h) (p:ps)
  takeMVar (lock h)

clearProcesses :: SignalHandler -> IO ()
clearProcesses h = do
  putMVar (lock h) ()
  _ <- takeMVar (processesToCleanup h)
  putMVar (processesToCleanup h) []
  takeMVar (lock h)

handler :: SignalHandler -> Handler
handler h = Catch (do putMVar (lock h) ()
                      ps <- takeMVar (processesToCleanup h)
                      mapM_ kill ps
                      putMVar (processesToCleanup h) []
                      throwTo (targetThread h) ExitSuccess
                      takeMVar (lock h))
