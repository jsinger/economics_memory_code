#!/bin/bash
# Calculate min heap for a particular benchmark/size combination

function usage()
{
    echo "Usage: $(basename "${0}") benchmark size threads results_file [startsize]"
    exit 1
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DACAPO="${DIR}/dacapo-9.12-bach.jar"

test -z "${1}" && usage
test -z "${2}" && usage
test -z "${3}" && usage
test -z "${4}" && usage

NAME="${1}"
SIZE="${2}"
THREADS="${3}"
RESULTS="$(readlink -f "${4}")"

i='0'

if [ ! -z "${5}" ]; then
    i="${5}"
fi

if [ "${NAME}" = 'eclipse' ]; then
    if [ "${SIZE}" = 'large' ]; then
        echo "eclipse large is buggy; skipping"
        exit 0
    fi
fi

result='1'

while [ "${result}" -ne '0' ]; do
    i=$(( i + 1 ))
    
    echo "Testing ${NAME} ${SIZE} (${THREADS} thread(s)) with ${i} MB..."

    TEMPDIR="$(readlink -f "$(mktemp -d)")"
    FIFO="$(readlink -f "$(mktemp -u)")"
    mkfifo "${FIFO}"

    java "-Xms${i}m" "-Xmx${i}m" -jar "${DACAPO}" --scratch-directory "${TEMPDIR}" -s "${SIZE}" -t "${THREADS}" -n 10 "${NAME}" &> "${FIFO}" &
    PID="${!}"
    
    while read line; do
        echo "${line}"
        if echo "${line}" | grep -i Exception &>/dev/null; then
            kill -9 "${PID}"
        fi
    done < "${FIFO}"

    wait "%1"
    result="${?}"
    rm "${FIFO}"
    rm -r "${TEMPDIR}"
done

echo "${NAME} ${SIZE} ${THREADS} ${i}" >> "${RESULTS}"
echo "${NAME} ${SIZE} ${THREADS} has a min heap of ${i} MB"
