module Economem.BenchmarkIterationLength (defaultIterations,
                                          IterationLengthDir(..),
                                          newIterationLength,
                                          resumeIterationLength) where

import Control.Applicative
import qualified Economem.BenchmarkTimer as B
import Economem.Common
import Economem.Experiment
import Economem.Files
import Economem.Java
import Economem.Log
import qualified Economem.MetadataDir as M
import Economem.Records
import Economem.UnconstrainedBenchmarkTimer
import System.Directory

defaultIterations :: Integer
defaultIterations = 20

name :: String
name = "benchmarkIterationLength"

data IterationLengthDir =
  IterationLengthDir { dirPath :: FilePath,
                       benchmarksFile :: CsvFile BenchmarkThreads,
                       runsFile :: IntFile,
                       iterationsFile :: IntFile,
                       resultsFile :: CsvFile BenchmarkThreadsRuntime }

instance Path IterationLengthDir where
  getPath = dirPath
instance Dir IterationLengthDir where
  asDir path =
    IterationLengthDir { dirPath = getPath path,
                         benchmarksFile = CsvFile (path </> "benchmarks.csv"),
                         runsFile = WholeFile (path </> "runs.txt"),
                         iterationsFile = WholeFile (path </> "iterations.txt"),
                         resultsFile = CsvFile (path </> "results.csv") }

newIterationLength :: Input BenchmarkThreads -> Integer -> Integer -> Java -> Maybe SubexpInfo -> IO Experiment
newIterationLength inputPath iterations runs j info = do
  input <- loadInput inputPath
  (expDir, num, parentLog) <- newExperimentProperties name info
  makeIterationLength expDir num input iterations runs j parentLog

resumeIterationLength :: FilePath -> IO Experiment
resumeIterationLength =
  resumeExperiment
  (\expDir -> do let timerDir = asDir expDir
                 input <- get (benchmarksFile timerDir)
                 iterations <- get (iterationsFile timerDir)
                 runs <- get (runsFile timerDir)
                 makeIterationLength expDir topLevel input iterations runs defaultJava Nothing)


makeIterationLength :: ExperimentDir ->
                       ExperimentNum ->
                       [BenchmarkThreads] ->
                       Integer ->
                       Integer ->
                       Java ->
                       Maybe Log ->
                       IO Experiment
makeIterationLength expDir num input iterations runs j parentLog = do
  let ex = makeExperiment name expDir num 0 parentLog
  let timerDir = asDir expDir
  return (ex { saveAdditionalStaticMetadata = saveStatic timerDir,
               canResume = canResumeTimer timerDir,
               createSubexperiments = createSubexps,
               subexperimentCompleted = subexpCompleted timerDir })

  where
    saveStatic :: IterationLengthDir -> IO ()
    saveStatic d = do
      put (benchmarksFile d) input
      put (iterationsFile d) iterations
      put (runsFile d) runs

    canResumeTimer :: IterationLengthDir -> IO Bool
    canResumeTimer d =
      and <$> mapM doesFileExist [getPath (benchmarksFile d),
                                  getPath (iterationsFile d)]

    createSubexps :: Experiment -> IO [Experiment]
    createSubexps e = do
      let infos = seqSubexpInfos Static input e
      let inputs = map (\bt -> BenchmarkThreadsIters bt (fromIntegral iterations)) input
      mapM (\(info, inp) -> newUnconstrainedBenchmarkTimer (Raw [inp]) runs j Nothing False (Just info) []) (zip infos inputs)

    subexpCompleted :: IterationLengthDir -> Experiment -> Experiment -> IO ()
    subexpCompleted d _ subExp = do
      runtime <- uncVal <$> get (M.avgTimeFile (M.mainRuntimes (metadataDir (experimentDir subExp))))
      let avg = runtime / fromIntegral iterations
      let bmDir = asDir (experimentDir subExp)
      benchmark <- headM =<< get (B.benchmarksFile bmDir)
      append (resultsFile d)
        (BenchmarkThreadsRuntime
         (btiBenchmark benchmark)
         (Runtime avg))
