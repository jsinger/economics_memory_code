#ifndef UTILITIES_HPP
#define UTILITIES_HPP

/*! \defgroup common Common C++ code */

#include <sys/types.h>

/*! \addtogroup common
  @{
*/

//! Main namespace for all common code.
namespace economem
{
    const char* const TYPE_READING = "reading";
    const char* const TYPE_RECOMMENDATION = "recommendation";
    const char* const TYPE_COMMAND = "command";
    const char* const TYPE_CMDRESULT = "cmdresult";

    const char* const NAME_DEFAULT = "simple";

    //! Generate a random number between min and max. Calls srand the first time it is called
    unsigned long long randInt(unsigned long long min, unsigned long long max);

    //! Allocates a string that should be freed; returns NULL on failure.
    char* allocDaemonSocketName(const char* type, const char* daemonName=NAME_DEFAULT);

    //! Allocates a string that should be freed; returns NULL on failure.
    char* allocClientSocketName(pid_t pid, const char* type);

    //! Print a message, but only if isInfoVerbose() is true.
    void info(const char* caller, const char* fmt, ...) __attribute__((format (printf, 2, 3)));

    //! Whether info() should print; see also makeInfoVerbose().
    bool isInfoVerbose();

    //! Make calls to info() print.
    void makeInfoVerbose();

    void warning(const char* fmt, ...) __attribute__((format (printf, 1, 2)));
    void die(const char* fmt, ...) __attribute__((format (printf, 1, 2))) __attribute__((noreturn));


    unsigned long long milliTime();
    unsigned long long microTime();
}

/*! @} */

#endif
