#!/usr/bin/env python
# Process an existing measure dataset

import argparse
import argtypes
import staticutility as su
import utilitymodel


def postprocess(dataDir):
    runs = dataDir.runs()

    for bid in dataDir.parseBenchmarksFile():
        resultsFile = dataDir.gatherResults(bid)
        results = su.MeasureResultsColumns(su.loadMeasureResultsFile(resultsFile))

        dataDir.plotIterations(results.hs, results.means, results.stddevs, results.medians, bid, runs)

        for model in utilitymodel.models:
            try:
                utilitymodel.get(model).plotThroughput(results.hs, results.itersPerSecs, results.stddevItersPerSecs, results.medianItersPerSecs, bid, runs, dataDir)
                utilitymodel.get(model).plotThroughput(results.hs, results.itersPerSecs, results.stddevItersPerSecs, results.medianItersPerSecs, bid, runs, dataDir, cleaned=True)
            except (ValueError, RuntimeWarning):
                print "'" + str(bid) + "' has invalid data for the '" + model + "' utility model."


    su.mergePdfs(dataDir.globIterationsGraphs(), dataDir.overallIterationsGraph())
    su.mergePdfs(dataDir.globThroughputGraphs(), dataDir.overallThroughputGraph())
    su.mergePdfs(dataDir.globCleanedThroughputGraphs(), dataDir.overallCleanedThroughputGraph())


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Process an existing measure dataset")
    parser.add_argument("dir", type=argtypes.existing_absolute_dir, help="Directory containing data")
    args = parser.parse_args()
    postprocess(su.MeasureDir(args.dir))
