economemd
    total memory: 1000 MB
    timer interval: 10 s
    timer start state: running
    utility model: root

    caches:
        lru

    force utility function validity: yes
    make recommendations: no
    equal recommendations: no

    reading receiver name: simple
    command receiver name: simple

    dump file: economemd.dump.log
    log file: /home/callum/economics_memory_code/dynamic_sizing/data/output/gcFactor_2014_09_24_12_05_40/daemon_log_30.txt
    log readings: yes
    log functions: yes

PyOptimiser: initialising...
PyOptimiser: initialised
ReadingThread: starting
TimerThread: starting
CommandThread: starting
Reading from 7369: heap size 9 MB, throughput 0.990627
Clients: 1
Reading from 7369: heap size 9 MB, throughput 0.982563
Reading from 7369: heap size 9 MB, throughput 0.960303
Reading from 7369: heap size 9 MB, throughput 0.958971
Reading from 7369: heap size 11 MB, throughput 0.987447
Reading from 7369: heap size 11 MB, throughput 0.965739
Reading from 7369: heap size 17 MB, throughput 0.943506
Reading from 7369: heap size 17 MB, throughput 0.636983
Reading from 7369: heap size 30 MB, throughput 0.924417
Reading from 7369: heap size 31 MB, throughput 0.942155
Reading from 7369: heap size 35 MB, throughput 0.497279
Reading from 7369: heap size 48 MB, throughput 0.859825
Reading from 7369: heap size 50 MB, throughput 0.907642
Reading from 7369: heap size 52 MB, throughput 0.287232
Reading from 7369: heap size 76 MB, throughput 0.865511
Reading from 7369: heap size 76 MB, throughput 0.320651
Reading from 7369: heap size 102 MB, throughput 0.864565
Reading from 7369: heap size 103 MB, throughput 0.819412
Reading from 7369: heap size 104 MB, throughput 0.825753
Reading from 7369: heap size 107 MB, throughput 0.81284
Reading from 7369: heap size 109 MB, throughput 0.207748
Reading from 7369: heap size 144 MB, throughput 0.737007
Reading from 7369: heap size 148 MB, throughput 0.755932
Reading from 7369: heap size 150 MB, throughput 0.755534
Reading from 7369: heap size 158 MB, throughput 0.826272
Reading from 7369: heap size 159 MB, throughput 0.733421
Reading from 7369: heap size 162 MB, throughput 0.209614
Reading from 7369: heap size 204 MB, throughput 0.703622
Reading from 7369: heap size 207 MB, throughput 0.830685
Reading from 7369: heap size 212 MB, throughput 0.910201
Reading from 7369: heap size 212 MB, throughput 0.771628
Reading from 7369: heap size 223 MB, throughput 0.757901
Reading from 7369: heap size 227 MB, throughput 0.602922
Reading from 7369: heap size 228 MB, throughput 0.814862
Reading from 7369: heap size 232 MB, throughput 0.594797
Reading from 7369: heap size 233 MB, throughput 0.783462
Reading from 7369: heap size 238 MB, throughput 0.776262
Reading from 7369: heap size 239 MB, throughput 0.80441
Reading from 7369: heap size 243 MB, throughput 0.845937
Reading from 7369: heap size 244 MB, throughput 0.669949
Reading from 7369: heap size 250 MB, throughput 0.109193
Reading from 7369: heap size 291 MB, throughput 0.703227
Reading from 7369: heap size 289 MB, throughput 0.807487
Reading from 7369: heap size 293 MB, throughput 0.839436
Reading from 7369: heap size 293 MB, throughput 0.966599
Reading from 7369: heap size 295 MB, throughput 0.871804
Reading from 7369: heap size 295 MB, throughput 0.872799
Reading from 7369: heap size 296 MB, throughput 0.870681
Reading from 7369: heap size 295 MB, throughput 0.879964
Reading from 7369: heap size 297 MB, throughput 0.91968
Reading from 7369: heap size 298 MB, throughput 0.915944
Reading from 7369: heap size 299 MB, throughput 0.851574
Reading from 7369: heap size 300 MB, throughput 0.783633
Reading from 7369: heap size 301 MB, throughput 0.77095
Reading from 7369: heap size 306 MB, throughput 0.770735
Reading from 7369: heap size 306 MB, throughput 0.748393
Reading from 7369: heap size 312 MB, throughput 0.751265
Reading from 7369: heap size 312 MB, throughput 0.934983
Reading from 7369: heap size 317 MB, throughput 0.980608
Reading from 7369: heap size 319 MB, throughput 0.633882
Reading from 7369: heap size 364 MB, throughput 0.979228
Reading from 7369: heap size 365 MB, throughput 0.986145
Reading from 7369: heap size 364 MB, throughput 0.982057
Reading from 7369: heap size 367 MB, throughput 0.983078
Reading from 7369: heap size 363 MB, throughput 0.984119
Reading from 7369: heap size 366 MB, throughput 0.985474
Reading from 7369: heap size 364 MB, throughput 0.980606
Reading from 7369: heap size 365 MB, throughput 0.981641
Reading from 7369: heap size 366 MB, throughput 0.980677
Reading from 7369: heap size 367 MB, throughput 0.981946
Reading from 7369: heap size 369 MB, throughput 0.983921
Reading from 7369: heap size 370 MB, throughput 0.976449
Reading from 7369: heap size 371 MB, throughput 0.987191
Reading from 7369: heap size 373 MB, throughput 0.953354
Reading from 7369: heap size 375 MB, throughput 0.861682
Reading from 7369: heap size 375 MB, throughput 0.803956
Reading from 7369: heap size 381 MB, throughput 0.840534
Reading from 7369: heap size 384 MB, throughput 0.883593
Reading from 7369: heap size 391 MB, throughput 0.990065
Reading from 7369: heap size 393 MB, throughput 0.988462
Reading from 7369: heap size 394 MB, throughput 0.989323
Reading from 7369: heap size 396 MB, throughput 0.987935
Reading from 7369: heap size 394 MB, throughput 0.988064
Reading from 7369: heap size 397 MB, throughput 0.987064
Reading from 7369: heap size 394 MB, throughput 0.979655
Reading from 7369: heap size 396 MB, throughput 0.986405
Reading from 7369: heap size 394 MB, throughput 0.987313
Reading from 7369: heap size 396 MB, throughput 0.986464
Reading from 7369: heap size 395 MB, throughput 0.985779
Reading from 7369: heap size 396 MB, throughput 0.987468
Reading from 7369: heap size 395 MB, throughput 0.972465
Reading from 7369: heap size 397 MB, throughput 0.865988
Reading from 7369: heap size 397 MB, throughput 0.874595
Reading from 7369: heap size 400 MB, throughput 0.864669
Reading from 7369: heap size 407 MB, throughput 0.989934
Reading from 7369: heap size 409 MB, throughput 0.990705
Reading from 7369: heap size 414 MB, throughput 0.991277
Reading from 7369: heap size 415 MB, throughput 0.99061
Reading from 7369: heap size 415 MB, throughput 0.990867
Reading from 7369: heap size 417 MB, throughput 0.990082
Reading from 7369: heap size 415 MB, throughput 0.98803
Reading from 7369: heap size 417 MB, throughput 0.988847
Reading from 7369: heap size 417 MB, throughput 0.988119
Reading from 7369: heap size 418 MB, throughput 0.985898
Reading from 7369: heap size 420 MB, throughput 0.991188
Reading from 7369: heap size 421 MB, throughput 0.9124
Reading from 7369: heap size 421 MB, throughput 0.895495
Reading from 7369: heap size 423 MB, throughput 0.888624
Reading from 7369: heap size 430 MB, throughput 0.990633
Reading from 7369: heap size 431 MB, throughput 0.993082
Reading from 7369: heap size 434 MB, throughput 0.991451
Reading from 7369: heap size 436 MB, throughput 0.992055
Reading from 7369: heap size 435 MB, throughput 0.991386
Reading from 7369: heap size 437 MB, throughput 0.98925
Reading from 7369: heap size 435 MB, throughput 0.988732
Reading from 7369: heap size 437 MB, throughput 0.988041
Reading from 7369: heap size 439 MB, throughput 0.993055
Reading from 7369: heap size 439 MB, throughput 0.978654
Reading from 7369: heap size 438 MB, throughput 0.904953
Reading from 7369: heap size 441 MB, throughput 0.892748
Reading from 7369: heap size 447 MB, throughput 0.991023
Reading from 7369: heap size 448 MB, throughput 0.993197
Reading from 7369: heap size 451 MB, throughput 0.991905
Reading from 7369: heap size 453 MB, throughput 0.992487
Reading from 7369: heap size 452 MB, throughput 0.991606
Reading from 7369: heap size 454 MB, throughput 0.991094
Reading from 7369: heap size 452 MB, throughput 0.9904
Reading from 7369: heap size 454 MB, throughput 0.98805
Reading from 7369: heap size 456 MB, throughput 0.993602
Reading from 7369: heap size 456 MB, throughput 0.936834
Reading from 7369: heap size 456 MB, throughput 0.912098
Reading from 7369: heap size 458 MB, throughput 0.980621
Reading from 7369: heap size 465 MB, throughput 0.993914
Reading from 7369: heap size 466 MB, throughput 0.99311
Reading from 7369: heap size 466 MB, throughput 0.99178
Reading from 7369: heap size 468 MB, throughput 0.992864
Reading from 7369: heap size 466 MB, throughput 0.990795
Reading from 7369: heap size 468 MB, throughput 0.991176
Reading from 7369: heap size 470 MB, throughput 0.990387
Reading from 7369: heap size 470 MB, throughput 0.988642
Reading from 7369: heap size 473 MB, throughput 0.916892
Reading from 7369: heap size 474 MB, throughput 0.908986
Reading from 7369: heap size 479 MB, throughput 0.992746
Reading from 7369: heap size 481 MB, throughput 0.993775
Reading from 7369: heap size 484 MB, throughput 0.993545
Reading from 7369: heap size 485 MB, throughput 0.992385
Reading from 7369: heap size 484 MB, throughput 0.99101
Reading from 7369: heap size 486 MB, throughput 0.990638
Reading from 7369: heap size 488 MB, throughput 0.991438
Reading from 7369: heap size 488 MB, throughput 0.985231
Reading from 7369: heap size 491 MB, throughput 0.92268
Reading from 7369: heap size 493 MB, throughput 0.556653
Reading from 7369: heap size 514 MB, throughput 0.997194
Reading from 7369: heap size 515 MB, throughput 0.995896
Reading from 7369: heap size 522 MB, throughput 0.994867
Reading from 7369: heap size 523 MB, throughput 0.993555
Reading from 7369: heap size 520 MB, throughput 0.993406
Reading from 7369: heap size 523 MB, throughput 0.990991
Reading from 7369: heap size 524 MB, throughput 0.990593
Reading from 7369: heap size 525 MB, throughput 0.915497
Reading from 7369: heap size 522 MB, throughput 0.939512
Reading from 7369: heap size 527 MB, throughput 0.993015
Reading from 7369: heap size 526 MB, throughput 0.993473
Reading from 7369: heap size 529 MB, throughput 0.991985
Reading from 7369: heap size 533 MB, throughput 0.99178
Reading from 7369: heap size 534 MB, throughput 0.990502
Reading from 7369: heap size 538 MB, throughput 0.989739
Reading from 7369: heap size 540 MB, throughput 0.982616
Reading from 7369: heap size 546 MB, throughput 0.914746
Reading from 7369: heap size 549 MB, throughput 0.988283
Reading from 7369: heap size 555 MB, throughput 0.993953
Reading from 7369: heap size 557 MB, throughput 0.993449
Reading from 7369: heap size 556 MB, throughput 0.992906
Reading from 7369: heap size 559 MB, throughput 0.992648
Reading from 7369: heap size 555 MB, throughput 0.992228
Reading from 7369: heap size 558 MB, throughput 0.990855
Reading from 7369: heap size 561 MB, throughput 0.932773
Reading from 7369: heap size 561 MB, throughput 0.989808
Reading from 7369: heap size 568 MB, throughput 0.995166
Reading from 7369: heap size 568 MB, throughput 0.994195
Reading from 7369: heap size 569 MB, throughput 0.99416
Reading from 7369: heap size 571 MB, throughput 0.993519
Reading from 7369: heap size 569 MB, throughput 0.990971
Reading from 7369: heap size 571 MB, throughput 0.985738
Reading from 7369: heap size 576 MB, throughput 0.938804
Reading from 7369: heap size 577 MB, throughput 0.994661
Reading from 7369: heap size 583 MB, throughput 0.994523
Reading from 7369: heap size 584 MB, throughput 0.994297
Reading from 7369: heap size 584 MB, throughput 0.992337
Reading from 7369: heap size 586 MB, throughput 0.992582
Reading from 7369: heap size 586 MB, throughput 0.992804
Reading from 7369: heap size 587 MB, throughput 0.934136
Reading from 7369: heap size 592 MB, throughput 0.993528
Reading from 7369: heap size 592 MB, throughput 0.994981
Reading from 7369: heap size 596 MB, throughput 0.994752
Reading from 7369: heap size 598 MB, throughput 0.993625
Reading from 7369: heap size 597 MB, throughput 0.992725
Reading from 7369: heap size 599 MB, throughput 0.991951
Reading from 7369: heap size 601 MB, throughput 0.943534
Client 7369 died
Clients: 0
ReadingThread: finished
TimerThread: finished
CommandThread: finished
Main: All threads killed, cleaning up
