#!/bin/bash

function usage()
{
    echo "Usage: ${0} dir benchmark1 benchmark2 total_memory"
}

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
DACAPO='dacapo-9.12-bach.jar'
cd "${DIR}"

if [ -z "${1}" ]; then
    usage
    exit 1
fi

if [ ! -d "${1}" ]; then
    usage
    exit 1
fi

RUNDIR="${1}"

if [ -z "${2}" ]; then
    usage
    exit 1
fi

B1="${2}"

if [ -z "${3}" ]; then
    usage
    exit 1
fi

B2="${3}"

if [ -z "${4}" ]; then
    usage
    exit 1
fi

TOTAL="${4}"
ATTEMPTS='20'
ACCUM="accum_$(date '+%Y_%m_%d_%H_%M_%S').txt"

TEMPFILE="$(mktemp --tmpdir=.)"

python basicBalancing.py "${RUNDIR}" "${B1}" "${B2}" "${TOTAL}" >"${TEMPFILE}"

head -n "${ATTEMPTS}" "${TEMPFILE}" | while read line; do
    echo "Attempting balance: ${line}"

    B1_HEAP="$(echo "${line}" | cut '--delimiter= ' -f 2)"
    B2_HEAP="$(echo "${line}" | cut '--delimiter= ' -f 3)"

    echo "${B1_HEAP}"
    echo "${B2_HEAP}"

    ITERATIONS="$(head -n 1 "${RUNDIR}/iterations.txt")"

    TEMPDIR1="$(mktemp -d --tmpdir=.)"
    TEMPDIR2="$(mktemp -d --tmpdir=.)"
    
    STARTTIME="$(date '+%s')"
    
    java "-Xms${B1_HEAP}m" "-Xmx${B1_HEAP}m" -jar "${DIR}/${DACAPO}" --scratch-directory "${TEMPDIR1}" -s large -n "${ITERATIONS}" "${B1}" &
    java "-Xms${B2_HEAP}m" "-Xmx${B2_HEAP}m" -jar "${DIR}/${DACAPO}" --scratch-directory "${TEMPDIR2}" -s large -n "${ITERATIONS}" "${B2}" &
    
    wait
    
    ENDTIME="$(date '+%s')"

    RUNTIME=$(( ENDTIME - STARTTIME ))

    echo "${line} ${RUNTIME}" >> "${ACCUM}"
    
    rm -r "${TEMPDIR1}"
    rm -r "${TEMPDIR2}"
done
    
rm "${TEMPFILE}"
