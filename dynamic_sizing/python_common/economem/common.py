import datetime
import errno
import getpass
import glob
import os
import os.path
import platform
import subprocess
import time


class EconomemException(Exception):
    def __init__(self, message, cause=None):
        super(EconomemException, self).__init__()
        self.message = message
        self.cause = cause

    def __str__(self):
        s = self.message
        if self.cause:
            s += "\n" + str(self.cause)
        return s

class Dir(object):
    def __init__(self, path):
        super(Dir, self).__init__()
        self.path = os.path.abspath(path)

    def join(self, *s):
        return os.path.realpath(os.path.join(self.path, *s))

    def exists(self):
        return os.path.isdir(self.path)

    def makedir(self):
        try:
            os.makedirs(self.path)
        except OSError as e:
            if e.errno == errno.EEXIST and os.path.isdir(self.path):
                pass
            else:
                raise EconomemException("Cannot create directory", e)

    def find_files(self, name_glob="*"):
        return filter(os.path.isfile, glob.glob(self.join(name_glob)))

    def find_subdirs(self, name_glob="*"):
        return filter(os.path.isdir, glob.glob(self.join(name_glob)))

    def __str__(self):
        return self.path

this_dir = Dir(os.path.dirname(os.path.abspath(__file__)))
dynamic_sizing_dir = Dir(this_dir.join("..", ".."))
hotspot7_dir = Dir(dynamic_sizing_dir.join("hotspot7"))
hotspot8_dir = Dir(dynamic_sizing_dir.join("hotspot8"))
hotspot8_unmodified_dir = Dir(dynamic_sizing_dir.join("hotspot8_unmodified"))
daemon_dir = Dir(dynamic_sizing_dir.join("daemon"))
root_data_dir = Dir(dynamic_sizing_dir.join("data"))


class File(object):
    def __init__(self, path, append_when_write=False):
        super(File, self).__init__()
        self.path = os.path.realpath(path)
        self.write_mode = "a" if append_when_write else "w"

    def exists(self):
        return os.path.isfile(self.path)

    def get(self):
        with open(self.path) as f:
            return f.read().strip()

    def get_list(self, map_func=lambda x: x):
        l = []
        with open(self.path) as f:
            for line in f:
                l.append(map_func(line.strip()))
        return l

    def put(self, s):
        with open(self.path, self.write_mode) as f:
            f.write(str(s) + "\n")

    def put_list(self, l):
        with open(self.path, self.write_mode) as f:
            for item in l:
                f.write(str(item) + "\n")

    def line_count(self):
        return len(self.get_list())

    def average_of_lines(self):
        l = self.get_list(float)
        return sum(l) / len(l)

    def __str__(self):
        return self.path


class MachineSpecificFilename(object):
    def __init__(self, parent_dir, pattern):
        super(MachineSpecificFilename, self).__init__()
        self.parent_dir = parent_dir
        self.pattern = pattern

    def filename(self, machine=None):
        if not machine:
            machine = me()

        return self.parent_dir.join(self.pattern % machine)

    def all_filenames(self):
        return glob.glob(self.filename("*"))


def is64bit():
    return str(subprocess.check_output(["uname", "-m"])).strip() == "x86_64"

def num_processors():
    return int(str(subprocess.check_output(["nproc"])).strip())

def system_memory_kB():
    with open("/proc/meminfo") as f:
        for line in f:
            if "MemTotal" in line:
                return int(line.strip().split()[1])
    raise EconomemException("/proc/meminfo parsing failed")

def system_memory_MB():
    return system_memory_kB() / 1024

def get_system_mem_GB():
    return system_memory_kB() / (1024 * 1024)

def user_name():
    return str(subprocess.check_output(["id", "-nu"])).strip()

def group_name():
    return str(subprocess.check_output(["id", "-ng"])).strip()

def now():
    return datetime.datetime.now().strftime("%Y_%m_%d_%H_%M_%S")

def now_pretty():
    return datetime.datetime.now().strftime("%Y/%m/%d %H:%M:%S")

def now_stamp():
    return int(time.time())

def me():
    return getpass.getuser() + "@" + platform.node()

def pretty_number(n, max_num):
    return str(n).zfill(len(str(max_num)))
