module StandardGCLog where

import Text.Regex.Posix


data GCType = MAJOR | MINOR deriving (Eq, Show)

gcTypeFromStr :: String -> GCType
gcTypeFromStr "Full GC" = MAJOR
gcTypeFromStr _         = MINOR



data GC = GC { gcType :: GCType, gcStartTime :: Double, cause :: String, startSize :: Integer, endSize :: Integer, totalSize :: Integer, duration :: Double } deriving (Eq, Show)

isMajor :: GC -> Bool
isMajor g = gcType g == MAJOR

endTime :: GC -> Double
endTime g = gcStartTime g + duration g

gcFromStr :: String -> Maybe GC
gcFromStr s =
  case m of
    []   -> Nothing
    [xs] -> Just (GC (gcTypeFromStr (xs !! 2)) (read (xs !! 1)) (xs !! 5) (read (xs !! 6)) (read (xs !! 7)) (read (xs !! 8)) (read (xs !! 9)))
  where m = s =~ "^([0-9]+\\.[0-9]+): \\[((GC)|(Full GC)) \\((.*)\\)  ([0-9]+)K->([0-9]+)K\\(([0-9]+)K\\), ([0-9]+\\.[0-9]+) secs\\]$" :: [[String]]

parseGCs :: [String] -> [GC]
parseGCs xs = foldr f [] (map gcFromStr xs)
  where f :: Maybe GC -> [GC] -> [GC]
        f (Just y) ys = y:ys
        f Nothing ys = ys

majorOnly :: [GC] -> [GC]
majorOnly gs = filter isMajor gs

minorOnly :: [GC] -> [GC]
minorOnly gs = filter (not . isMajor) gs



data GCPeriod = GCPeriod { startTime :: Double, gc :: GC, throughput :: Double } deriving Show

firstGCPeriod :: GC -> GCPeriod
firstGCPeriod g = GCPeriod 0.0 g t
  where t = 1.0 - (duration g / endTime g)

gcPeriod :: GC -> GC -> GCPeriod
gcPeriod lastGC thisGC = GCPeriod (endTime lastGC) thisGC t
  where t = 1.0 - (duration thisGC / (endTime thisGC - endTime lastGC))

gcPeriods :: [GC] -> [GCPeriod]
gcPeriods [] = []
gcPeriods (g:gs) = (firstGCPeriod g):(gcPeriodsHelper (g:gs))
  where gcPeriodsHelper :: [GC] -> [GCPeriod]
        gcPeriodsHelper [] = []
        gcPeriodsHelper (_:[]) = []
        gcPeriodsHelper (lastGC:thisGC:rest) = (gcPeriod lastGC thisGC):(gcPeriodsHelper (thisGC:rest))



getThroughputs :: [String] -> [String]
getThroughputs xs = map (show . throughput) (gcPeriods (parseGCs xs))

parseAndInfo :: [String] -> [String]
parseAndInfo xs = map show gcs ++ [""] ++ map show gcps ++ ["", show sameLen, show same]
  where gcs = parseGCs xs
        gcps = gcPeriods gcs
        sameLen = length gcs == length gcps
        same = and (map (\(g, gp) -> g == gc gp) (zip gcs gcps))
