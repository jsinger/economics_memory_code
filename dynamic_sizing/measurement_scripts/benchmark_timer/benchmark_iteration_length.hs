{-# LANGUAGE FlexibleInstances, MultiParamTypeClasses, DeriveDataTypeable #-}
{-# OPTIONS_GHC -fno-warn-missing-fields -fno-warn-missing-methods #-}

import Economem.ArgHelper
import Economem.BenchmarkIterationLength
import Economem.BenchmarkTimer
import Economem.Experiment
import Economem.Files
import Economem.Java
import Economem.Signals
import System.Console.CmdLib

data Args = Args { inputFile :: FilePath,
                   iterations :: Int,
                   runs :: Int,
                   java :: String }
          deriving (Typeable, Data, Eq, Show)

instance Attributes Args where
  attributes _ = group "Options" [
    inputFile %> [ Positional 0,
                   Required True,
                   Help "Specifies which benchmarks to run",
                   ArgHelp "INPUT_FILE" ],
    iterations %> [ Short ['i'],
              Long ["iterations"],
              Default ((fromIntegral defaultIterations)::Int),
              Help "How many iterations to run each benchmark for, for taking the average",
              ArgHelp "ITERATIONS" ],
    runs %> [ Short ['r'],
              Long ["runs"],
              Default ((fromIntegral defaultRuns)::Int),
              Help "How many times to repeat, for taking the average",
              ArgHelp "RUNS" ],
    java %> [ Short ['j'],
              Long ["java"],
              Default (javaName defaultJava),
              Help ("Java version to use; available options: " ++ show validJavaNames),
              ArgHelp "JAVA" ]]

instance RecordCommand Args where
  mode_summary _ = "Measure the length of one iteration of benchmarks."

main :: IO ()
main = getArgs >>= executeR Args {} >>= \opts -> do
  (i, iters, r, j) <- validate opts
  d <- newIterationLength (fromFile i) iters r j Nothing
  h <- makeSignalHandler
  runExperiment d h

  where validate :: Args -> IO (FilePath, Integer, Integer, Java)
        validate a =
          do i <- existingAbsoluteFile (inputFile a)
             iters <- positive (iterations a)
             r <- positive (runs a)
             j <- javaFromName (java a)
             return (i, fromIntegral iters, fromIntegral r, j)
