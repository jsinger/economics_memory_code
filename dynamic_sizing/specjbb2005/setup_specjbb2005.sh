#!/bin/bash
# Setup SPECjbb 2005 in the specified directory

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function usage()
{
    echo "Usage: $(basename "${0}") dir threads"
    exit 1
}

test -z "${1}" && usage
test -z "${2}" && usage
TARGET="${1}"
THREADS="${2}"
PROPS='SPECjbb.props'

mkdir -p "${TARGET}" &&
cp "${DIR}/${PROPS}" "${TARGET}/${PROPS}" &&
sed -i "s/@@@@@1@@@@@/${THREADS}/g" "${TARGET}/${PROPS}" &&
sed -i "s|@@@@@2@@@@@|${TARGET}/results|g" "${TARGET}/${PROPS}"
